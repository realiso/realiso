--
-- PostgreSQL database dump
--

-- Started on 2009-07-27 15:29:03 BRT

SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

--
-- TOC entry 3183 (class 0 OID 0)
-- Dependencies: 1967
-- Name: ci_nc_nseqnumber_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('ci_nc_nseqnumber_seq', 1, true);


--
-- TOC entry 3184 (class 0 OID 0)
-- Dependencies: 2000
-- Name: isms_context_classification_pkclassification_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('isms_context_classification_pkclassification_seq', 200, false);


--
-- TOC entry 3185 (class 0 OID 0)
-- Dependencies: 2002
-- Name: isms_context_history_pkid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('isms_context_history_pkid_seq', 2095, true);


--
-- TOC entry 3186 (class 0 OID 0)
-- Dependencies: 2003
-- Name: isms_context_pkcontext_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('isms_context_pkcontext_seq', 15107, true);


--
-- TOC entry 3187 (class 0 OID 0)
-- Dependencies: 2007
-- Name: isms_saas_pkconfig_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('isms_saas_pkconfig_seq', 1, false);


--
-- TOC entry 3188 (class 0 OID 0)
-- Dependencies: 2017
-- Name: pm_doc_reference_pkreference_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('pm_doc_reference_pkreference_seq', 1, false);


--
-- TOC entry 3189 (class 0 OID 0)
-- Dependencies: 2021
-- Name: pm_instance_comment_pkcomment_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('pm_instance_comment_pkcomment_seq', 1, false);


--
-- TOC entry 3190 (class 0 OID 0)
-- Dependencies: 2036
-- Name: rm_parameter_name_pkparametername_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('rm_parameter_name_pkparametername_seq', 10, false);


--
-- TOC entry 3191 (class 0 OID 0)
-- Dependencies: 2038
-- Name: rm_parameter_value_name_pkvaluename_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('rm_parameter_value_name_pkvaluename_seq', 10, false);


--
-- TOC entry 3192 (class 0 OID 0)
-- Dependencies: 2040
-- Name: rm_rc_parameter_value_name_pkrcvaluename_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('rm_rc_parameter_value_name_pkrcvaluename_seq', 10, false);


--
-- TOC entry 3193 (class 0 OID 0)
-- Dependencies: 2094
-- Name: wkf_alert_pkalert_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('wkf_alert_pkalert_seq', 12, true);


--
-- TOC entry 3194 (class 0 OID 0)
-- Dependencies: 2098
-- Name: wkf_schedule_pkschedule_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('wkf_schedule_pkschedule_seq', 1, true);


--
-- TOC entry 3195 (class 0 OID 0)
-- Dependencies: 2100
-- Name: wkf_task_pktask_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('wkf_task_pktask_seq', 5, true);


--
-- TOC entry 3100 (class 0 OID 9034446)
-- Dependencies: 1956
-- Data for Name: ci_action_plan; Type: TABLE DATA; Schema: public; Owner: postgres
--

-- INSERT INTO ci_action_plan (fkcontext, fkresponsible, sname, tactionplan, nactiontype, ddatedeadline, ddateconclusion, bisefficient, ddateefficiencyrevision, ddateefficiencymeasured, ndaysbefore, bflagrevisionalert, bflagrevisionalertlate, sdocument) VALUES (15103, 13, 'Funcionário sem EPI.', 'Advertência e treinamento.', 4780, '2009-07-10 00:00:00', '2009-07-10 00:00:00', 0, '2009-07-17 00:00:00', NULL, 2, 1, 1, '');
-- INSERT INTO ci_action_plan (fkcontext, fkresponsible, sname, tactionplan, nactiontype, ddatedeadline, ddateconclusion, bisefficient, ddateefficiencyrevision, ddateefficiencymeasured, ndaysbefore, bflagrevisionalert, bflagrevisionalertlate, sdocument) VALUES (15106, 13, 'Plano de Ação XX', 'Prevenção', 4780, '2009-07-10 00:00:00', '2009-07-10 00:00:00', 0, '2009-07-17 00:00:00', NULL, 3, 1, 1, '');


--
-- TOC entry 3101 (class 0 OID 9034460)
-- Dependencies: 1957
-- Data for Name: ci_category; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO ci_category (fkcontext, sname) VALUES (15042, 'Anormal');
INSERT INTO ci_category (fkcontext, sname) VALUES (15043, 'Emergencial');
INSERT INTO ci_category (fkcontext, sname) VALUES (15044, 'Normal');


--
-- TOC entry 3102 (class 0 OID 9034463)
-- Dependencies: 1958
-- Data for Name: ci_incident; Type: TABLE DATA; Schema: public; Owner: postgres
--

-- INSERT INTO ci_incident (fkcontext, fkcategory, fkresponsible, sname, taccountsplan, tevidences, nlosstype, tevidencerequirementcomment, ddatelimit, ddatefinish, tdisposaldescription, tsolutiondescription, tproductservice, bnotemaildp, bemaildpsent, ddate) VALUES (15099, 15044, 13, 'Corte na palma da mão esquerda por ferramenta manual.', 'Produção', 'Não utilização de luva de proteção.', 4580, NULL, '2009-07-10 00:00:00', '2009-07-10 00:00:00', 'Enviado para a enfermaria.', 'Advertência por falta de uso de EPI. Treinamento.', 'Montagem de gabarito.', 0, 0, '2009-07-10 13:58:00');


--
-- TOC entry 3103 (class 0 OID 9034474)
-- Dependencies: 1959
-- Data for Name: ci_incident_control; Type: TABLE DATA; Schema: public; Owner: postgres
--

-- INSERT INTO ci_incident_control (fkincident, fkcontrol, fkcontext) VALUES (15099, 15089, 15101);


--
-- TOC entry 3104 (class 0 OID 9034477)
-- Dependencies: 1960
-- Data for Name: ci_incident_financial_impact; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3105 (class 0 OID 9034481)
-- Dependencies: 1961
-- Data for Name: ci_incident_process; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3106 (class 0 OID 9034484)
-- Dependencies: 1962
-- Data for Name: ci_incident_risk; Type: TABLE DATA; Schema: public; Owner: postgres
--

-- INSERT INTO ci_incident_risk (fkcontext, fkrisk, fkincident) VALUES (15100, 15084, 15099);


--
-- TOC entry 3107 (class 0 OID 9034487)
-- Dependencies: 1963
-- Data for Name: ci_incident_risk_value; Type: TABLE DATA; Schema: public; Owner: postgres
--

-- INSERT INTO ci_incident_risk_value (fkparametername, fkvaluename, fkincrisk) VALUES (1, 2, 15100);


--
-- TOC entry 3108 (class 0 OID 9034490)
-- Dependencies: 1964
-- Data for Name: ci_incident_user; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3109 (class 0 OID 9034496)
-- Dependencies: 1965
-- Data for Name: ci_nc; Type: TABLE DATA; Schema: public; Owner: postgres
--

-- INSERT INTO ci_nc (fkcontext, fkcontrol, fkresponsible, fksender, sname, nseqnumber, tdescription, tcause, tdenialjustification, nclassification, ddatesent, ncapability) VALUES (15102, NULL, 13, 13, 'Funcionário não utiliza EPI luva de proteção.', 1, 'Evidenciado funcionário sem a utilização de EPI', 'Não observação das regras internas para utilização de EPIs.', NULL, 62251, '2009-07-10 00:00:00', 62282);


--
-- TOC entry 3110 (class 0 OID 9034504)
-- Dependencies: 1966
-- Data for Name: ci_nc_action_plan; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3111 (class 0 OID 9034509)
-- Dependencies: 1968
-- Data for Name: ci_nc_process; Type: TABLE DATA; Schema: public; Owner: postgres
--

-- INSERT INTO ci_nc_process (fknc, fkprocess) VALUES (15102, 15051);


--
-- TOC entry 3112 (class 0 OID 9034512)
-- Dependencies: 1969
-- Data for Name: ci_nc_seed; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3113 (class 0 OID 9034516)
-- Dependencies: 1970
-- Data for Name: ci_occurrence; Type: TABLE DATA; Schema: public; Owner: postgres
--

-- INSERT INTO ci_occurrence (fkcontext, fkincident, tdescription, tdenialjustification, ddate) VALUES (15098, 15099, 'Queda de ferramenta do gabarito.', NULL, '2009-07-10 13:57:00');


--
-- TOC entry 3114 (class 0 OID 9034522)
-- Dependencies: 1971
-- Data for Name: ci_risk_probability; Type: TABLE DATA; Schema: public; Owner: postgres
--

-- INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (15084, 1, 10);
-- INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount) VALUES (15084, 2, 30);


--
-- TOC entry 3115 (class 0 OID 9034526)
-- Dependencies: 1972
-- Data for Name: ci_risk_schedule; Type: TABLE DATA; Schema: public; Owner: postgres
--

-- INSERT INTO ci_risk_schedule (fkrisk, nperiod, nvalue, ddatelastcheck) VALUES (15084, 7801, 30, '2009-07-21 17:09:09');


--
-- TOC entry 3116 (class 0 OID 9034531)
-- Dependencies: 1973
-- Data for Name: ci_solution; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3138 (class 0 OID 9034716)
-- Dependencies: 1997
-- Data for Name: isms_audit_log; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 11:46:17', 'Realiso Online (189.114.113.183)', 2611, 'Realiso Online (189.114.113.183)', 'Usuário <b>Realiso Online (189.114.113.183)</b> tentou efetuar login com um nome de usuário inválido.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 11:46:33', 'Realiso Online (189.114.113.183)', 2611, 'Realiso Online (189.114.113.183)', 'Usuário <b>Realiso Online (189.114.113.183)</b> tentou efetuar login com um nome de usuário inválido.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 11:47:18', 'Realiso Online', 2606, 'Realiso Online', 'Usuário <b>Realiso Online</b> entrou no sistema.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 11:49:07', 'Realiso Online', 2606, 'Realiso Online', 'Usuário <b>Realiso Online</b> entrou no sistema.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 11:50:26', 'Realiso Online', 2601, 'Ruído', 'Criado(a) Categoria <b>Ruído</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 11:51:29', 'Realiso Online', 2601, 'Perda Auditiva', 'Criado(a) Evento <b>Perda Auditiva</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 11:51:56', 'Realiso Online', 2601, 'Vibração', 'Criado(a) Categoria <b>Vibração</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 11:52:39', 'Realiso Online', 2601, 'Problemas na coluna vertebral.', 'Criado(a) Evento <b>Problemas na coluna vertebral.</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 11:53:09', 'Realiso Online', 2601, 'Radiação não ionizante', 'Criado(a) Categoria <b>Radiação não ionizante</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 11:53:34', 'Realiso Online', 2601, 'Lesões nos olhos.', 'Criado(a) Evento <b>Lesões nos olhos.</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 11:53:48', 'Realiso Online', 2601, 'Queimaduras.', 'Criado(a) Evento <b>Queimaduras.</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 11:54:52', 'Realiso Online', 2601, 'Poeiras vegetais.', 'Criado(a) Categoria <b>Poeiras vegetais.</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 11:55:50', 'Realiso Online', 2601, 'Insuficiência respiratória.', 'Criado(a) Evento <b>Insuficiência respiratória.</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 11:56:17', 'Realiso Online', 2601, 'Nevoas, gases e vapores.', 'Criado(a) Categoria <b>Nevoas, gases e vapores.</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 11:56:56', 'Realiso Online', 2601, 'Irritação vias respiratórias', 'Criado(a) Evento <b>Irritação vias respiratórias</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 11:57:40', 'Realiso Online', 2606, 'Realiso Online', 'Usuário <b>Realiso Online</b> entrou no sistema.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 11:58:19', 'Realiso Online', 2601, 'Dermatites', 'Criado(a) Evento <b>Dermatites</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 11:58:33', 'Realiso Online', 2601, 'Irritação ocular', 'Criado(a) Evento <b>Irritação ocular</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 11:58:55', 'Realiso Online', 2601, 'Fungos', 'Criado(a) Categoria <b>Fungos</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 11:59:11', 'Realiso Online', 2601, 'Dermatites', 'Criado(a) Evento <b>Dermatites</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 11:59:29', 'Realiso Online', 2601, 'Irritação vias respiratórias', 'Criado(a) Evento <b>Irritação vias respiratórias</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:00:29', 'Realiso Online', 2601, 'Acidente', 'Criado(a) Categoria <b>Acidente</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:00:52', 'Realiso Online', 2601, 'Corte', 'Criado(a) Evento <b>Corte</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:01:00', 'Realiso Online', 2601, 'Luxação', 'Criado(a) Evento <b>Luxação</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:01:17', 'Realiso Online', 2601, 'Esmagamento', 'Criado(a) Evento <b>Esmagamento</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:01:36', 'Realiso Online', 2601, 'Fratura', 'Criado(a) Evento <b>Fratura</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:01:46', 'Realiso Online', 2601, 'Amputação', 'Criado(a) Evento <b>Amputação</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:02:11', 'Realiso Online', 2601, 'Acidente sem afastamento', 'Criado(a) Evento <b>Acidente sem afastamento</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:02:32', 'Realiso Online', 2601, 'Queimadura', 'Criado(a) Evento <b>Queimadura</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:02:46', 'Realiso Online', 2601, 'Choque elétrico', 'Criado(a) Evento <b>Choque elétrico</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:03:15', 'Realiso Online', 2601, 'Esforço físico', 'Criado(a) Categoria <b>Esforço físico</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:03:49', 'Realiso Online', 2601, 'Problemas coluna vertebral', 'Criado(a) Evento <b>Problemas coluna vertebral</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:04:04', 'Realiso Online', 2601, 'Distensão muscular', 'Criado(a) Evento <b>Distensão muscular</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:04:31', 'Realiso Online', 2601, 'Problemas ergonômicos', 'Criado(a) Evento <b>Problemas ergonômicos</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:05:24', 'Realiso Online', 2601, 'Monotonia e repetitividade', 'Criado(a) Categoria <b>Monotonia e repetitividade</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:05:43', 'Realiso Online', 2601, 'Tendinite', 'Criado(a) Evento <b>Tendinite</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:05:54', 'Realiso Online', 2601, 'Distenção muscular', 'Criado(a) Evento <b>Distenção muscular</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:06:29', 'Realiso Online', 2601, 'Choque elétrico', 'Criado(a) Categoria <b>Choque elétrico</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:07:01', 'Realiso Online', 2601, 'Lesões nos olhos', 'Criado(a) Evento <b>Lesões nos olhos</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:07:14', 'Realiso Online', 2601, 'Queimadura', 'Criado(a) Evento <b>Queimadura</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:07:28', 'Realiso Online', 2601, 'Parada cardio respiratória', 'Criado(a) Evento <b>Parada cardio respiratória</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:07:51', 'Realiso Online', 2601, 'Acidente sem afastamento', 'Criado(a) Evento <b>Acidente sem afastamento</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:13:53', 'Realiso Online', 2606, 'Realiso Online', 'Usuário <b>Realiso Online</b> entrou no sistema.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:14:52', 'Realiso Online', 2601, 'Acordos coletivos', 'Criado(a) Seção de Requisito Legal <b>Acordos coletivos</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:15:10', 'Realiso Online', 2601, 'Estadual', 'Criado(a) Seção de Requisito Legal <b>Estadual</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:16:32', 'Realiso Online', 2601, 'Municipal', 'Criado(a) Seção de Requisito Legal <b>Municipal</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:16:44', 'Realiso Online', 2601, 'Federal', 'Criado(a) Seção de Requisito Legal <b>Federal</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:17:01', 'Realiso Online', 2601, 'Requisitos Internos', 'Criado(a) Seção de Requisito Legal <b>Requisitos Internos</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:25:23', 'Realiso Online', 2606, 'Realiso Online', 'Usuário <b>Realiso Online</b> entrou no sistema.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:27:45', 'Realiso Online', 2606, 'Realiso Online', 'Usuário <b>Realiso Online</b> entrou no sistema.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:29:45', 'Realiso Online', 2606, 'Realiso Online', 'Usuário <b>Realiso Online</b> entrou no sistema.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:30:15', 'Realiso Online', 2601, 'Anormal', 'Criado(a) Categoria <b>Anormal</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:30:24', 'Realiso Online', 2601, 'Emergencial', 'Criado(a) Categoria <b>Emergencial</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:30:32', 'Realiso Online', 2601, 'Normal', 'Criado(a) Categoria <b>Normal</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:30:51', 'Realiso Online', 2606, 'Realiso Online', 'Usuário <b>Realiso Online</b> entrou no sistema.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:32:43', 'Realiso Online', 2606, 'Realiso Online', 'Usuário <b>Realiso Online</b> entrou no sistema.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:34:09', 'Realiso Online', 2601, 'Acordo coletivo', 'Criado(a) Legislação <b>Acordo coletivo</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:34:30', 'Realiso Online', 2601, 'Lei Estadual', 'Criado(a) Legislação <b>Lei Estadual</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:35:12', 'Realiso Online', 2601, 'Lei federal', 'Criado(a) Legislação <b>Lei federal</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:35:22', 'Realiso Online', 2601, 'Lei municipal', 'Criado(a) Legislação <b>Lei municipal</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:35:37', 'Realiso Online', 2601, 'Requisitos Internos', 'Criado(a) Legislação <b>Requisitos Internos</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:40:39', 'Realiso Online', 2601, 'Produção', 'Criado(a) área de <b>Produção</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:41:01', 'Realiso Online', 2601, 'Produtivo', 'Criado(a) Processo <b>Produtivo</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:44:49', 'Realiso Online', 2603, 'Corte', 'Removido(a) do sistema Evento <b>Corte</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:44:49', 'Realiso Online', 2603, 'Corte', 'Removido(a) do sistema Evento <b>Corte</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:44:49', 'Realiso Online', 2603, 'Corte', 'Removido(a) do sistema Evento <b>Corte</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:44:49', 'Realiso Online', 2603, 'Corte', 'Removido(a) do sistema Evento <b>Corte</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:44:49', 'Realiso Online', 2603, 'Corte', 'Removido(a) do sistema Evento <b>Corte</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:44:49', 'Realiso Online', 2603, 'Corte', 'Removido(a) do sistema Evento <b>Corte</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:44:49', 'Realiso Online', 2603, 'Corte', 'Removido(a) do sistema Evento <b>Corte</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:44:49', 'Realiso Online', 2603, 'Corte', 'Removido(a) do sistema Evento <b>Corte</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:44:49', 'Realiso Online', 2603, 'Acidente', 'Removido(a) do sistema Categoria <b>Acidente</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:44:56', 'Realiso Online', 2603, 'Lesões nos olhos', 'Removido(a) do sistema Evento <b>Lesões nos olhos</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:44:56', 'Realiso Online', 2603, 'Lesões nos olhos', 'Removido(a) do sistema Evento <b>Lesões nos olhos</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:44:56', 'Realiso Online', 2603, 'Lesões nos olhos', 'Removido(a) do sistema Evento <b>Lesões nos olhos</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:44:56', 'Realiso Online', 2603, 'Lesões nos olhos', 'Removido(a) do sistema Evento <b>Lesões nos olhos</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:44:56', 'Realiso Online', 2603, 'Choque elétrico', 'Removido(a) do sistema Categoria <b>Choque elétrico</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:45:02', 'Realiso Online', 2603, 'Problemas coluna vertebral', 'Removido(a) do sistema Evento <b>Problemas coluna vertebral</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:45:02', 'Realiso Online', 2603, 'Problemas coluna vertebral', 'Removido(a) do sistema Evento <b>Problemas coluna vertebral</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:45:02', 'Realiso Online', 2603, 'Problemas coluna vertebral', 'Removido(a) do sistema Evento <b>Problemas coluna vertebral</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:45:02', 'Realiso Online', 2603, 'Esforço físico', 'Removido(a) do sistema Categoria <b>Esforço físico</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:45:10', 'Realiso Online', 2603, 'Dermatites', 'Removido(a) do sistema Evento <b>Dermatites</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:45:10', 'Realiso Online', 2603, 'Dermatites', 'Removido(a) do sistema Evento <b>Dermatites</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:45:10', 'Realiso Online', 2603, 'Fungos', 'Removido(a) do sistema Categoria <b>Fungos</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:45:16', 'Realiso Online', 2603, 'Tendinite', 'Removido(a) do sistema Evento <b>Tendinite</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:45:16', 'Realiso Online', 2603, 'Tendinite', 'Removido(a) do sistema Evento <b>Tendinite</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:45:16', 'Realiso Online', 2603, 'Monotonia e repetitividade', 'Removido(a) do sistema Categoria <b>Monotonia e repetitividade</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:45:22', 'Realiso Online', 2603, 'Insuficiência respiratória.', 'Removido(a) do sistema Evento <b>Insuficiência respiratória.</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:45:22', 'Realiso Online', 2603, 'Poeiras vegetais.', 'Removido(a) do sistema Categoria <b>Poeiras vegetais.</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:45:28', 'Realiso Online', 2603, 'Irritação vias respiratórias', 'Removido(a) do sistema Evento <b>Irritação vias respiratórias</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:45:28', 'Realiso Online', 2603, 'Irritação vias respiratórias', 'Removido(a) do sistema Evento <b>Irritação vias respiratórias</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:45:28', 'Realiso Online', 2603, 'Irritação vias respiratórias', 'Removido(a) do sistema Evento <b>Irritação vias respiratórias</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:45:28', 'Realiso Online', 2603, 'Nevoas, gases e vapores.', 'Removido(a) do sistema Categoria <b>Nevoas, gases e vapores.</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:45:34', 'Realiso Online', 2603, 'Lesões nos olhos.', 'Removido(a) do sistema Evento <b>Lesões nos olhos.</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:45:34', 'Realiso Online', 2603, 'Lesões nos olhos.', 'Removido(a) do sistema Evento <b>Lesões nos olhos.</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:45:34', 'Realiso Online', 2603, 'Radiação não ionizante', 'Removido(a) do sistema Categoria <b>Radiação não ionizante</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:45:40', 'Realiso Online', 2603, 'Perda Auditiva', 'Removido(a) do sistema Evento <b>Perda Auditiva</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:45:40', 'Realiso Online', 2603, 'Ruído', 'Removido(a) do sistema Categoria <b>Ruído</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:45:46', 'Realiso Online', 2603, 'Problemas na coluna vertebral.', 'Removido(a) do sistema Evento <b>Problemas na coluna vertebral.</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:45:47', 'Realiso Online', 2603, 'Vibração', 'Removido(a) do sistema Categoria <b>Vibração</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:46:03', 'Realiso Online', 2601, 'Risco de acidente', 'Criado(a) Categoria <b>Risco de acidente</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:46:17', 'Realiso Online', 2601, 'Risco Químico', 'Criado(a) Categoria <b>Risco Químico</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:46:33', 'Realiso Online', 2601, 'Risco ergonômico', 'Criado(a) Categoria <b>Risco ergonômico</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:46:59', 'Realiso Online', 2601, 'Risco físico', 'Criado(a) Categoria <b>Risco físico</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:47:52', 'Realiso Online', 2601, 'Risco biológico', 'Criado(a) Categoria <b>Risco biológico</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:50:27', 'Realiso Online', 2601, 'Ruído', 'Criado(a) Evento <b>Ruído</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:51:07', 'Realiso Online', 2601, 'Vibração', 'Criado(a) Evento <b>Vibração</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:52:27', 'Realiso Online', 2601, 'Radiação não ionizante', 'Criado(a) Evento <b>Radiação não ionizante</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:52:51', 'Realiso Online', 2601, 'Radiação não ionizante', 'Criado(a) Evento <b>Radiação não ionizante</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:54:08', 'Realiso Online', 2601, 'Poeiras vegetais', 'Criado(a) Evento <b>Poeiras vegetais</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:55:06', 'Realiso Online', 2601, 'Nevoas, gases e vapores', 'Criado(a) Evento <b>Nevoas, gases e vapores</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:55:29', 'Realiso Online', 2601, 'Nevoas, gases e vapores', 'Criado(a) Evento <b>Nevoas, gases e vapores</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:55:53', 'Realiso Online', 2601, 'Nevoas, gases e vapores', 'Criado(a) Evento <b>Nevoas, gases e vapores</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:56:28', 'Realiso Online', 2601, 'Fungos', 'Criado(a) Evento <b>Fungos</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:56:51', 'Realiso Online', 2601, 'Fungos', 'Criado(a) Evento <b>Fungos</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:57:29', 'Realiso Online', 2601, 'Acidente', 'Criado(a) Evento <b>Acidente</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:57:42', 'Realiso Online', 2601, 'Acidente', 'Criado(a) Evento <b>Acidente</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:57:54', 'Realiso Online', 2601, 'Acidente', 'Criado(a) Evento <b>Acidente</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:58:12', 'Realiso Online', 2601, 'Acidente', 'Criado(a) Evento <b>Acidente</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:58:32', 'Realiso Online', 2601, 'Acidente', 'Criado(a) Evento <b>Acidente</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:58:47', 'Realiso Online', 2601, 'Acidente', 'Criado(a) Evento <b>Acidente</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:59:30', 'Realiso Online', 2601, 'Esforço físico', 'Criado(a) Evento <b>Esforço físico</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 12:59:54', 'Realiso Online', 2601, 'Esforço físico', 'Criado(a) Evento <b>Esforço físico</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 13:00:23', 'Realiso Online', 2601, 'Esforço físico', 'Criado(a) Evento <b>Esforço físico</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 13:01:01', 'Realiso Online', 2601, 'Monotonia e repetitividade', 'Criado(a) Evento <b>Monotonia e repetitividade</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 13:01:17', 'Realiso Online', 2601, 'Monotonia e repetitividade', 'Criado(a) Evento <b>Monotonia e repetitividade</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 13:03:04', 'Realiso Online', 2603, 'Acidente', 'Removido(a) do sistema Evento <b>Acidente</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 13:03:25', 'Realiso Online', 2601, 'Choque elétrico', 'Criado(a) Evento <b>Choque elétrico</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 13:03:40', 'Realiso Online', 2601, 'Choque elétrico', 'Criado(a) Evento <b>Choque elétrico</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 13:03:57', 'Realiso Online', 2601, 'Choque elétrico', 'Criado(a) Evento <b>Choque elétrico</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 13:05:33', 'Realiso Online', 2601, 'Choque elétrico', 'Criado(a) Evento <b>Choque elétrico</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 13:09:44', 'Realiso Online', 2601, 'Montagem de gabarito', 'Criado(a) A.P.S. <b>Montagem de gabarito</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 13:10:37', 'Realiso Online', 2601, 'Montagem de gabarito -> Produtivo', 'Criado(a) Associação de A.P.S. a Processo <b>Montagem de gabarito -> Produtivo</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 13:15:35', 'Realiso Online', 2601, 'Acidente', 'Criado(a) Risco <b>Acidente</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 13:15:35', 'Realiso Online', 2602, 'Acidente', 'Editado(a) Risco <b>Acidente</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 13:22:04', 'Realiso Online', 2601, 'Instrução de Trabalho 00', 'Criado(a) Requisitos Legais <b>Instrução de Trabalho 00</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 13:22:04', 'Realiso Online', 2601, 'Instrução de Trabalho 00 -> Requisitos Internos', 'Criado(a) Associação de Requisito Legal a Legislação <b>Instrução de Trabalho 00 -> Requisitos Internos</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 13:23:32', 'Realiso Online', 2601, 'Norma Regulamentadora 6', 'Criado(a) Requisitos Legais <b>Norma Regulamentadora 6</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 13:23:32', 'Realiso Online', 2601, 'Norma Regulamentadora 6 -> Lei federal', 'Criado(a) Associação de Requisito Legal a Legislação <b>Norma Regulamentadora 6 -> Lei federal</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 13:32:19', 'Realiso Online', 2601, 'Utilização de EPIs', 'Criado(a) Controle <b>Utilização de EPIs</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 13:32:19', 'Realiso Online', 2601, 'Instrução de Trabalho 00 -> Utilização de EPIs', 'Criado(a) Associação de Requisitos Legais a Controle <b>Instrução de Trabalho 00 -> Utilização de EPIs</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 13:32:19', 'Realiso Online', 2601, 'Norma Regulamentadora 6 -> Utilização de EPIs', 'Criado(a) Associação de Requisitos Legais a Controle <b>Norma Regulamentadora 6 -> Utilização de EPIs</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 13:32:19', 'Realiso Online', 2602, 'Utilização de EPIs', 'Editado(a) Controle <b>Utilização de EPIs</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 13:34:24', 'Realiso Online', 2601, 'Utilização de EPIs -> Acidente', 'Criado(a) Associação de Controle a Risco <b>Utilização de EPIs -> Acidente</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 13:46:00', 'Realiso Online', 2601, 'Prevenção de acidentes', 'Criado(a) Documento <b>Prevenção de acidentes</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 13:46:03', 'Realiso Online', 2601, 'Prevenção de acidentes (1)', 'Criado(a) Instância de Documento <b>Prevenção de acidentes (1)</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 13:46:03', 'Realiso Online', 2602, 'Prevenção de acidentes', 'Editado(a) Documento <b>Prevenção de acidentes</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 13:46:27', 'Realiso Online', 2602, 'Prevenção de acidentes (0.1)', 'Editado(a) Instância de Documento <b>Prevenção de acidentes (0.1)</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 13:46:27', 'Realiso Online', 2602, 'Prevenção de acidentes', 'Editado(a) Documento <b>Prevenção de acidentes</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 13:46:27', 'Realiso Online', 2602, 'Prevenção de acidentes', 'Editado(a) Documento <b>Prevenção de acidentes</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 13:46:27', 'Realiso Online', 2602, 'Prevenção de acidentes', 'Editado(a) Documento <b>Prevenção de acidentes</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 13:46:27', 'Realiso Online', 2602, 'Prevenção de acidentes (1.0)', 'Editado(a) Instância de Documento <b>Prevenção de acidentes (1.0)</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 13:46:27', 'Realiso Online', 2602, 'Prevenção de acidentes', 'Editado(a) Documento <b>Prevenção de acidentes</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 13:50:17', 'Realiso Online', 2601, 'Comunicação de acidente de trabalho', 'Criado(a) Registro <b>Comunicação de acidente de trabalho</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 13:54:42', 'Realiso Online', 2601, 'Comunicação de acidente de trabalho', 'Criado(a) Documento <b>Comunicação de acidente de trabalho</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 13:54:42', 'Realiso Online', 2602, 'Comunicação de acidente de trabalho', 'Editado(a) Registro <b>Comunicação de acidente de trabalho</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 13:54:44', 'Realiso Online', 2601, 'Comunicação de acidente de trabalho (1)', 'Criado(a) Instância de Documento <b>Comunicação de acidente de trabalho (1)</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 13:54:44', 'Realiso Online', 2602, 'Comunicação de acidente de trabalho', 'Editado(a) Documento <b>Comunicação de acidente de trabalho</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 13:55:01', 'Realiso Online', 2602, 'Comunicação de acidente de trabalho (0.1)', 'Editado(a) Instância de Documento <b>Comunicação de acidente de trabalho (0.1)</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 13:55:01', 'Realiso Online', 2602, 'Comunicação de acidente de trabalho', 'Editado(a) Documento <b>Comunicação de acidente de trabalho</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 13:55:01', 'Realiso Online', 2602, 'Comunicação de acidente de trabalho', 'Editado(a) Documento <b>Comunicação de acidente de trabalho</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 13:55:01', 'Realiso Online', 2602, 'Comunicação de acidente de trabalho', 'Editado(a) Documento <b>Comunicação de acidente de trabalho</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 13:55:01', 'Realiso Online', 2602, 'Comunicação de acidente de trabalho (1.0)', 'Editado(a) Instância de Documento <b>Comunicação de acidente de trabalho (1.0)</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 13:55:01', 'Realiso Online', 2602, 'Comunicação de acidente de trabalho', 'Editado(a) Documento <b>Comunicação de acidente de trabalho</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 13:58:05', 'Realiso Online', 2601, 'Queda de ferramenta do gabarito.', 'Criado(a) Incidente <b>Queda de ferramenta do gabarito.</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 14:08:57', 'Realiso Online', 2601, 'Corte na palma da mão esquerda por ferramenta manual.', 'Criado(a) Acidente <b>Corte na palma da mão esquerda por ferramenta manual.</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 14:08:57', 'Realiso Online', 2602, 'Queda de ferramenta do gabarito.', 'Editado(a) Incidente <b>Queda de ferramenta do gabarito.</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 14:11:35', 'Realiso Online', 2601, '', 'Criado(a)  <b></b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 14:11:35', 'Realiso Online', 2601, 'Utilização de EPIs -> Corte na palma da mão esquerda por ferramenta manual.', 'Criado(a) Associação de Controle a Acidente <b>Utilização de EPIs -> Corte na palma da mão esquerda por ferramenta manual.</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 14:13:35', 'Realiso Online', 2601, 'Funcionário não utiliza EPI luva de proteção.', 'Criado(a) Não Conformidade <b>Funcionário não utiliza EPI luva de proteção.</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 14:16:53', 'Realiso Online', 2602, 'Funcionário não utiliza EPI luva de proteção.', 'Editado(a) Não Conformidade <b>Funcionário não utiliza EPI luva de proteção.</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 14:18:04', 'Realiso Online', 2601, 'Funcionário sem EPI.', 'Criado(a) Plano de Ação <b>Funcionário sem EPI.</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 14:18:48', 'Realiso Online', 2602, 'Funcionário sem EPI.', 'Editado(a) Plano de Ação <b>Funcionário sem EPI.</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 14:21:53', 'Realiso Online', 2602, 'Utilização de EPIs', 'Editado(a) Controle <b>Utilização de EPIs</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 14:25:00', 'Realiso Online', 2601, 'Plano de ação XX', 'Criado(a) Documento <b>Plano de ação XX</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 14:25:03', 'Realiso Online', 2601, 'Plano de ação XX (0.0)', 'Criado(a) Instância de Documento <b>Plano de ação XX (0.0)</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 14:25:03', 'Realiso Online', 2602, 'Plano de ação XX', 'Editado(a) Documento <b>Plano de ação XX</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 14:25:28', 'Realiso Online', 2602, 'Plano de ação XX (0.1)', 'Editado(a) Instância de Documento <b>Plano de ação XX (0.1)</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 14:25:28', 'Realiso Online', 2602, 'Plano de ação XX', 'Editado(a) Documento <b>Plano de ação XX</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 14:25:28', 'Realiso Online', 2602, 'Plano de ação XX', 'Editado(a) Documento <b>Plano de ação XX</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 14:25:28', 'Realiso Online', 2602, 'Plano de ação XX', 'Editado(a) Documento <b>Plano de ação XX</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 14:25:28', 'Realiso Online', 2602, 'Plano de ação XX (1.0)', 'Editado(a) Instância de Documento <b>Plano de ação XX (1.0)</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 14:25:28', 'Realiso Online', 2602, 'Plano de ação XX', 'Editado(a) Documento <b>Plano de ação XX</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 14:27:48', 'Realiso Online', 2601, 'Plano de Ação XX', 'Criado(a) Plano de Ação <b>Plano de Ação XX</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 14:29:20', 'Realiso Online', 2602, 'Plano de Ação XX', 'Editado(a) Plano de Ação <b>Plano de Ação XX</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 14:35:19', 'Realiso Online', 2607, 'Realiso Online', 'Usuário <b>Realiso Online</b> saiu do sistema.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-10 14:35:19', 'Realiso Online', 2602, 'Realiso Online', 'Editado(a) Usuário <b>Realiso Online</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-14 13:54:12', 'Realiso Online (201.86.248.81)', 2611, 'Realiso Online (201.86.248.81)', 'Usuário <b>Realiso Online (201.86.248.81)</b> tentou efetuar login com um nome de usuário inválido.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-14 13:54:23', 'Realiso Online', 2606, 'Realiso Online', 'Usuário <b>Realiso Online</b> entrou no sistema.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-14 14:11:30', 'Realiso Online', 2601, 'Plano de Ação', 'Criado(a) Modelo de Documento <b>Plano de Ação</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-14 14:11:32', 'Realiso Online', 2602, 'Plano de Ação', 'Editado(a) Modelo de Documento <b>Plano de Ação</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-14 14:17:58', 'Realiso Online', 2607, 'Realiso Online', 'Usuário <b>Realiso Online</b> saiu do sistema.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-14 14:17:58', 'Realiso Online', 2602, 'Realiso Online', 'Editado(a) Usuário <b>Realiso Online</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-14 18:15:08', 'Realiso Online', 2606, 'Realiso Online', 'Usuário <b>Realiso Online</b> entrou no sistema.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-14 18:37:37', 'Realiso Online', 2606, 'Realiso Online', 'Usuário <b>Realiso Online</b> entrou no sistema.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-16 14:20:28', 'Realiso Online', 2606, 'Realiso Online', 'Usuário <b>Realiso Online</b> entrou no sistema.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-16 14:20:28', 'Realiso Online', 2602, 'Funcionário sem EPI.', 'Editado(a) Plano de Ação <b>Funcionário sem EPI.</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-16 14:20:29', 'Realiso Online', 2602, 'Plano de Ação XX', 'Editado(a) Plano de Ação <b>Plano de Ação XX</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-21 17:08:58', 'Realiso Online (189.114.114.207)', 2608, 'Realiso Online (189.114.114.207)', 'Usuário <b>Realiso Online (189.114.114.207)</b> tentou efetuar login com uma senha inválida.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-21 17:09:08', 'Realiso Online', 2606, 'Realiso Online', 'Usuário <b>Realiso Online</b> entrou no sistema.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-21 17:09:08', 'Realiso Online', 2602, 'Funcionário sem EPI.', 'Editado(a) Plano de Ação <b>Funcionário sem EPI.</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-21 17:09:09', 'Realiso Online', 2602, 'Plano de Ação XX', 'Editado(a) Plano de Ação <b>Plano de Ação XX</b> por  Realiso Online.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-21 17:09:48', 'Realiso Online', 2607, 'Realiso Online', 'Usuário <b>Realiso Online</b> saiu do sistema.');
INSERT INTO isms_audit_log (ddate, suser, naction, starget, tdescription) VALUES ('2009-07-21 17:09:48', 'Realiso Online', 2602, 'Realiso Online', 'Editado(a) Usuário <b>Realiso Online</b> por  Realiso Online.');


--
-- TOC entry 3139 (class 0 OID 9034723)
-- Dependencies: 1998
-- Data for Name: isms_config; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO isms_config (pkconfig, svalue) VALUES (201, 'Treinamento');
INSERT INTO isms_config (pkconfig, svalue) VALUES (202, 'Obras');
INSERT INTO isms_config (pkconfig, svalue) VALUES (203, 'Consultoria');
INSERT INTO isms_config (pkconfig, svalue) VALUES (204, 'Aquisições');
INSERT INTO isms_config (pkconfig, svalue) VALUES (205, 'Manutenções');
INSERT INTO isms_config (pkconfig, svalue) VALUES (402, '1');
INSERT INTO isms_config (pkconfig, svalue) VALUES (403, 'localhost');
INSERT INTO isms_config (pkconfig, svalue) VALUES (404, '1');
INSERT INTO isms_config (pkconfig, svalue) VALUES (405, '1');
INSERT INTO isms_config (pkconfig, svalue) VALUES (406, '0');
INSERT INTO isms_config (pkconfig, svalue) VALUES (407, '0');
INSERT INTO isms_config (pkconfig, svalue) VALUES (408, '1');
INSERT INTO isms_config (pkconfig, svalue) VALUES (409, '0');
INSERT INTO isms_config (pkconfig, svalue) VALUES (410, '00:00');
INSERT INTO isms_config (pkconfig, svalue) VALUES (411, '1970-01-01 00:00:00');
INSERT INTO isms_config (pkconfig, svalue) VALUES (415, '%default_sender%');
INSERT INTO isms_config (pkconfig, svalue) VALUES (416, '0');
INSERT INTO isms_config (pkconfig, svalue) VALUES (417, '1');
INSERT INTO isms_config (pkconfig, svalue) VALUES (418, '7801');
INSERT INTO isms_config (pkconfig, svalue) VALUES (419, '00:00');
INSERT INTO isms_config (pkconfig, svalue) VALUES (421, '1');
INSERT INTO isms_config (pkconfig, svalue) VALUES (426, '1');
INSERT INTO isms_config (pkconfig, svalue) VALUES (427, 'Confidencial');
INSERT INTO isms_config (pkconfig, svalue) VALUES (428, '');
INSERT INTO isms_config (pkconfig, svalue) VALUES (429, '1');
INSERT INTO isms_config (pkconfig, svalue) VALUES (430, '%default_sender_name%');
INSERT INTO isms_config (pkconfig, svalue) VALUES (431, 'CURR_REAL');
--INSERT INTO isms_config (pkconfig, svalue) VALUES (7404, '0');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7500, '0');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7501, '');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7502, '');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7503, '');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7504, '');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7510, '');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7511, '');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7512, '');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7201, '5');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7202, '0');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7203, '0');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7204, '0');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7205, '3');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7206, '365');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7207, '5');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7208, '7');
INSERT INTO isms_config (pkconfig, svalue) VALUES (801, '13');
INSERT INTO isms_config (pkconfig, svalue) VALUES (803, '13');
INSERT INTO isms_config (pkconfig, svalue) VALUES (804, '13');
INSERT INTO isms_config (pkconfig, svalue) VALUES (805, '13');
INSERT INTO isms_config (pkconfig, svalue) VALUES (806, '13');
INSERT INTO isms_config (pkconfig, svalue) VALUES (807, '13');
INSERT INTO isms_config (pkconfig, svalue) VALUES (808, '13');
INSERT INTO isms_config (pkconfig, svalue) VALUES (809, '13');
INSERT INTO isms_config (pkconfig, svalue) VALUES (810, '13');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7403, '3');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7209, '%error_report_email%');
INSERT INTO isms_config (pkconfig, svalue) VALUES (5701, '8301');
INSERT INTO isms_config (pkconfig, svalue) VALUES (401, '1');
INSERT INTO isms_config (pkconfig, svalue) VALUES (420, '2009-7-24');

/***** AD *****/
INSERT INTO isms_config (pkconfig, svalue) VALUES (7600, '0');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7601, 'ad-server.local');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7602, '389');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7603, '0');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7604, 'DC=ad-server, DC=local');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7605, 'ad-server\%user%');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7606, 'admin');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7607, 'adminpass');

INSERT INTO isms_config (pkconfig, svalue) VALUES (7406, '');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7407, '');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7405, 3);

/* FUSO HORARIO */
INSERT INTO isms_config (pkconfig, svalue) VALUES (7404, '0');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7514, '1');

/* CORES CUSTOMIZADAS */
INSERT INTO isms_config (pkconfig, svalue) VALUES (7515, '');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7516, '');

--
-- TOC entry 3118 (class 0 OID 9034545)
-- Dependencies: 1976
-- Data for Name: isms_context; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (1, 2817, 2700);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (2, 2817, 2700);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (3, 2817, 2700);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (4, 2817, 2700);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (5, 2817, 2700);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (13, 2816, 0);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (14, 2822, 2702);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15037, 2811, 2702);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15038, 2811, 2702);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15039, 2811, 2702);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15040, 2811, 2702);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15041, 2811, 2702);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15042, 2830, 2700);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15043, 2830, 2700);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15044, 2830, 2700);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15045, 2809, 2702);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15046, 2809, 2702);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15047, 2809, 2702);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15048, 2809, 2702);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15049, 2809, 2702);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15050, 2801, 2702);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15051, 2802, 2702);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15017, 2807, 2705);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15018, 2807, 2705);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15019, 2807, 2705);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15020, 2807, 2705);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15021, 2807, 2705);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15022, 2807, 2705);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15023, 2807, 2705);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15024, 2807, 2705);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15016, 2806, 2705);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15033, 2807, 2705);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15034, 2807, 2705);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15035, 2807, 2705);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15036, 2807, 2705);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15032, 2806, 2705);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15026, 2807, 2705);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15027, 2807, 2705);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15028, 2807, 2705);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15025, 2806, 2705);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15014, 2807, 2705);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15015, 2807, 2705);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15013, 2806, 2705);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15030, 2807, 2705);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15031, 2807, 2705);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15029, 2806, 2705);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15008, 2807, 2705);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15007, 2806, 2705);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15010, 2807, 2705);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15011, 2807, 2705);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15012, 2807, 2705);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15009, 2806, 2705);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15005, 2807, 2705);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15006, 2807, 2705);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15004, 2806, 2705);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15001, 2807, 2705);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15000, 2806, 2705);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15003, 2807, 2705);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15002, 2806, 2705);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15052, 2806, 2702);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15053, 2806, 2702);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15054, 2806, 2702);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15055, 2806, 2702);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15056, 2806, 2702);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15057, 2807, 2702);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15058, 2807, 2702);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15059, 2807, 2702);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15060, 2807, 2702);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15061, 2807, 2702);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15062, 2807, 2702);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15063, 2807, 2702);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15064, 2807, 2702);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15065, 2807, 2702);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15066, 2807, 2702);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15067, 2807, 2702);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15068, 2807, 2702);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15069, 2807, 2702);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15070, 2807, 2702);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15071, 2807, 2702);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15073, 2807, 2702);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15074, 2807, 2702);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15075, 2807, 2702);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15076, 2807, 2702);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15077, 2807, 2702);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15072, 2807, 2705);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15078, 2807, 2702);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15079, 2807, 2702);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15080, 2807, 2702);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15081, 2807, 2702);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15082, 2803, 2702);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15083, 2810, 2702);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15084, 2804, 2702);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15085, 2808, 2702);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15086, 2814, 2700);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15087, 2808, 2702);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15088, 2814, 2700);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15089, 2805, 2702);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15090, 2815, 2700);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15091, 2815, 2700);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15092, 2813, 2702);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15094, 2824, 2700);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15093, 2823, 2752);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15095, 2826, 2700);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15097, 2824, 2700);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15096, 2823, 2752);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15098, 2829, 2702);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15099, 2831, 2784);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15100, 2834, 2700);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15101, 2835, 2700);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15102, 2832, 2772);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15103, 2837, 2788);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15105, 2824, 2700);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15104, 2823, 2752);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15106, 2837, 2788);
INSERT INTO isms_context (pkcontext, ntype, nstate) VALUES (15107, 2827, 2702);


--
-- TOC entry 3140 (class 0 OID 9034726)
-- Dependencies: 1999
-- Data for Name: isms_context_classification; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (1, 2801, 'Produção', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (2, 2801, 'Administrativo', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (3, 2801, 'Auxiliares', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (4, 2801, 'Area de Apoio', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype, nweight) VALUES (5, 2801, 'Alta', 5502, 1);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype, nweight) VALUES (6, 2801, 'Média', 5502, 2);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype, nweight) VALUES (7, 2801, 'Baixa', 5502, 3);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (8, 2802, 'Produção e Entrega', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (9, 2802, 'Logística e Transporte', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (10, 2802, 'Administrativo', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (11, 2802, 'Processos de Apoio', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype, nweight) VALUES (12, 2802, 'Alta', 5502, 1);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype, nweight) VALUES (13, 2802, 'Média', 5502, 2);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype, nweight) VALUES (14, 2802, 'Baixa', 5502, 3);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (15, 2804, 'Agua', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (16, 2804, 'Ar', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (17, 2804, 'Solo', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (18, 2804, 'Recursos Naturais', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (19, 2804, 'Degradação Ambiental', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (20, 2804, 'Ruído e Vibrações', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (21, 2804, 'Resíduos Reaproveitáveis', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (22, 2804, 'Residuos Não Aproveitáveis', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (23, 2807, 'Interno', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (24, 2807, 'Externo', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (25, 2807, 'Ambos', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (26, 2805, 'Requisitos Legais', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (27, 2805, 'Melhoria Interna', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (28, 2805, 'Outros', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (29, 2823, 'Confidential', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (30, 2823, 'Restrito', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (31, 2823, 'Institucional', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (32, 2823, 'Público', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (33, 2826, 'Confidencial', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (34, 2826, 'Restrito', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (35, 2826, 'Institucional', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (36, 2826, 'Público', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (37, 2831, 'Multas', 5503);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (38, 2831, 'Recuperação do Ambiente', 5503);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (39, 2831, 'Danos Morais', 5503);


--
-- TOC entry 3117 (class 0 OID 9034537)
-- Dependencies: 1974
-- Data for Name: isms_context_date; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (1, 2901, '2009-07-09 10:22:03.868587', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (1, 2902, '2009-07-09 10:22:03.868587', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (2, 2901, '2009-07-09 10:22:03.868587', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (2, 2902, '2009-07-09 10:22:03.868587', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (3, 2901, '2009-07-09 10:22:03.868587', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (3, 2902, '2009-07-09 10:22:03.868587', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (4, 2901, '2009-07-09 10:22:03.868587', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (4, 2902, '2009-07-09 10:22:03.868587', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (5, 2901, '2009-07-09 10:22:03.868587', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (5, 2902, '2009-07-09 10:22:03.868587', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (13, 2901, '2009-07-09 10:22:03.868587', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15000, 2901, '2009-07-10 11:50:26', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15000, 2902, '2009-07-10 11:50:26', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15001, 2901, '2009-07-10 11:51:29', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15001, 2902, '2009-07-10 11:51:29', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15002, 2901, '2009-07-10 11:51:56', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15002, 2902, '2009-07-10 11:51:56', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15003, 2901, '2009-07-10 11:52:39', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15003, 2902, '2009-07-10 11:52:39', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15004, 2901, '2009-07-10 11:53:09', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15004, 2902, '2009-07-10 11:53:09', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15005, 2901, '2009-07-10 11:53:34', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15005, 2902, '2009-07-10 11:53:34', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15006, 2901, '2009-07-10 11:53:48', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15006, 2902, '2009-07-10 11:53:48', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15007, 2901, '2009-07-10 11:54:52', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15007, 2902, '2009-07-10 11:54:52', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15008, 2901, '2009-07-10 11:55:50', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15008, 2902, '2009-07-10 11:55:50', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15009, 2901, '2009-07-10 11:56:17', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15009, 2902, '2009-07-10 11:56:17', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15010, 2901, '2009-07-10 11:56:56', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15010, 2902, '2009-07-10 11:56:56', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15011, 2901, '2009-07-10 11:58:19', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15011, 2902, '2009-07-10 11:58:19', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15012, 2901, '2009-07-10 11:58:33', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15012, 2902, '2009-07-10 11:58:33', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15013, 2901, '2009-07-10 11:58:55', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15013, 2902, '2009-07-10 11:58:55', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15014, 2901, '2009-07-10 11:59:11', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15014, 2902, '2009-07-10 11:59:11', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15015, 2901, '2009-07-10 11:59:29', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15015, 2902, '2009-07-10 11:59:29', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15016, 2901, '2009-07-10 12:00:29', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15016, 2902, '2009-07-10 12:00:29', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15017, 2901, '2009-07-10 12:00:52', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15017, 2902, '2009-07-10 12:00:52', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15018, 2901, '2009-07-10 12:01:00', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15018, 2902, '2009-07-10 12:01:00', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15019, 2901, '2009-07-10 12:01:17', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15019, 2902, '2009-07-10 12:01:17', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15020, 2901, '2009-07-10 12:01:36', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15020, 2902, '2009-07-10 12:01:36', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15021, 2901, '2009-07-10 12:01:46', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15021, 2902, '2009-07-10 12:01:46', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15022, 2901, '2009-07-10 12:02:11', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15022, 2902, '2009-07-10 12:02:11', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15023, 2901, '2009-07-10 12:02:32', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15023, 2902, '2009-07-10 12:02:32', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15024, 2901, '2009-07-10 12:02:46', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15024, 2902, '2009-07-10 12:02:46', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15025, 2901, '2009-07-10 12:03:15', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15025, 2902, '2009-07-10 12:03:15', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15026, 2901, '2009-07-10 12:03:49', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15026, 2902, '2009-07-10 12:03:49', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15027, 2901, '2009-07-10 12:04:04', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15027, 2902, '2009-07-10 12:04:04', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15028, 2901, '2009-07-10 12:04:30', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15028, 2902, '2009-07-10 12:04:30', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15029, 2901, '2009-07-10 12:05:24', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15029, 2902, '2009-07-10 12:05:24', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15030, 2901, '2009-07-10 12:05:43', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15030, 2902, '2009-07-10 12:05:43', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15031, 2901, '2009-07-10 12:05:54', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15031, 2902, '2009-07-10 12:05:54', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15032, 2901, '2009-07-10 12:06:29', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15032, 2902, '2009-07-10 12:06:29', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15033, 2901, '2009-07-10 12:07:01', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15033, 2902, '2009-07-10 12:07:01', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15034, 2901, '2009-07-10 12:07:14', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15034, 2902, '2009-07-10 12:07:14', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15035, 2901, '2009-07-10 12:07:28', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15035, 2902, '2009-07-10 12:07:28', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15036, 2901, '2009-07-10 12:07:51', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15036, 2902, '2009-07-10 12:07:51', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15037, 2901, '2009-07-10 12:14:52', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15037, 2902, '2009-07-10 12:14:52', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15038, 2901, '2009-07-10 12:15:10', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15038, 2902, '2009-07-10 12:15:10', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15039, 2901, '2009-07-10 12:16:32', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15039, 2902, '2009-07-10 12:16:32', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15040, 2901, '2009-07-10 12:16:44', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15040, 2902, '2009-07-10 12:16:44', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15041, 2901, '2009-07-10 12:17:01', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15041, 2902, '2009-07-10 12:17:01', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15042, 2901, '2009-07-10 12:30:15', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15042, 2902, '2009-07-10 12:30:15', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15043, 2901, '2009-07-10 12:30:24', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15043, 2902, '2009-07-10 12:30:24', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15044, 2901, '2009-07-10 12:30:32', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15044, 2902, '2009-07-10 12:30:32', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15045, 2901, '2009-07-10 12:34:09', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15045, 2902, '2009-07-10 12:34:09', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15046, 2901, '2009-07-10 12:34:30', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15046, 2902, '2009-07-10 12:34:30', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15047, 2901, '2009-07-10 12:35:12', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15047, 2902, '2009-07-10 12:35:12', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15048, 2901, '2009-07-10 12:35:22', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15048, 2902, '2009-07-10 12:35:22', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15049, 2901, '2009-07-10 12:35:37', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15049, 2902, '2009-07-10 12:35:37', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15050, 2901, '2009-07-10 12:40:39', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15050, 2902, '2009-07-10 12:40:39', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15051, 2901, '2009-07-10 12:41:01', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15051, 2902, '2009-07-10 12:41:01', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15052, 2901, '2009-07-10 12:46:03', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15052, 2902, '2009-07-10 12:46:03', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15053, 2901, '2009-07-10 12:46:17', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15053, 2902, '2009-07-10 12:46:17', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15054, 2901, '2009-07-10 12:46:33', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15054, 2902, '2009-07-10 12:46:33', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15055, 2901, '2009-07-10 12:46:59', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15055, 2902, '2009-07-10 12:46:59', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15056, 2901, '2009-07-10 12:47:52', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15056, 2902, '2009-07-10 12:47:52', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15057, 2901, '2009-07-10 12:50:27', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15057, 2902, '2009-07-10 12:50:27', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15058, 2901, '2009-07-10 12:51:07', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15058, 2902, '2009-07-10 12:51:07', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15059, 2901, '2009-07-10 12:52:27', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15059, 2902, '2009-07-10 12:52:27', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15060, 2901, '2009-07-10 12:52:51', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15060, 2902, '2009-07-10 12:52:51', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15061, 2901, '2009-07-10 12:54:08', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15061, 2902, '2009-07-10 12:54:08', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15062, 2901, '2009-07-10 12:55:06', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15062, 2902, '2009-07-10 12:55:06', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15063, 2901, '2009-07-10 12:55:29', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15063, 2902, '2009-07-10 12:55:29', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15064, 2901, '2009-07-10 12:55:53', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15064, 2902, '2009-07-10 12:55:53', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15065, 2901, '2009-07-10 12:56:28', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15065, 2902, '2009-07-10 12:56:28', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15066, 2901, '2009-07-10 12:56:51', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15066, 2902, '2009-07-10 12:56:51', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15067, 2901, '2009-07-10 12:57:29', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15067, 2902, '2009-07-10 12:57:29', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15068, 2901, '2009-07-10 12:57:42', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15068, 2902, '2009-07-10 12:57:42', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15069, 2901, '2009-07-10 12:57:54', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15069, 2902, '2009-07-10 12:57:54', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15070, 2901, '2009-07-10 12:58:12', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15070, 2902, '2009-07-10 12:58:12', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15071, 2901, '2009-07-10 12:58:32', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15071, 2902, '2009-07-10 12:58:32', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15072, 2901, '2009-07-10 12:58:47', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15072, 2902, '2009-07-10 12:58:47', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15073, 2901, '2009-07-10 12:59:30', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15073, 2902, '2009-07-10 12:59:30', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15074, 2901, '2009-07-10 12:59:54', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15074, 2902, '2009-07-10 12:59:54', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15075, 2901, '2009-07-10 13:00:23', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15075, 2902, '2009-07-10 13:00:23', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15076, 2901, '2009-07-10 13:01:01', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15076, 2902, '2009-07-10 13:01:01', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15077, 2901, '2009-07-10 13:01:17', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15077, 2902, '2009-07-10 13:01:17', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15078, 2901, '2009-07-10 13:03:25', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15078, 2902, '2009-07-10 13:03:25', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15079, 2901, '2009-07-10 13:03:40', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15079, 2902, '2009-07-10 13:03:40', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15080, 2901, '2009-07-10 13:03:57', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15080, 2902, '2009-07-10 13:03:57', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15081, 2901, '2009-07-10 13:05:33', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15081, 2902, '2009-07-10 13:05:33', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15082, 2901, '2009-07-10 13:09:44', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15082, 2902, '2009-07-10 13:09:44', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15083, 2901, '2009-07-10 13:10:36', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15083, 2902, '2009-07-10 13:10:36', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15084, 2901, '2009-07-10 13:15:35', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15084, 2902, '2009-07-10 13:15:35', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15085, 2901, '2009-07-10 13:22:04', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15085, 2902, '2009-07-10 13:22:04', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15086, 2901, '2009-07-10 13:22:04', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15086, 2902, '2009-07-10 13:22:04', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15087, 2901, '2009-07-10 13:23:32', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15087, 2902, '2009-07-10 13:23:32', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15088, 2901, '2009-07-10 13:23:32', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15088, 2902, '2009-07-10 13:23:32', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15089, 2901, '2009-07-10 13:32:19', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15090, 2901, '2009-07-10 13:32:19', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15090, 2902, '2009-07-10 13:32:19', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15091, 2901, '2009-07-10 13:32:19', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15091, 2902, '2009-07-10 13:32:19', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15092, 2901, '2009-07-10 13:34:24', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15092, 2902, '2009-07-10 13:34:24', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15093, 2901, '2009-07-10 13:46:00', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15094, 2901, '2009-07-10 13:46:03', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15094, 2902, '2009-07-10 13:46:27', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15093, 2902, '2009-07-10 13:46:27', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15095, 2901, '2009-07-10 13:50:17', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15096, 2901, '2009-07-10 13:54:42', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15095, 2902, '2009-07-10 13:54:42', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15097, 2901, '2009-07-10 13:54:44', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15097, 2902, '2009-07-10 13:55:01', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15096, 2902, '2009-07-10 13:55:01', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15098, 2901, '2009-07-10 13:58:05', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15099, 2901, '2009-07-10 14:08:57', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15099, 2902, '2009-07-10 14:08:57', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15098, 2902, '2009-07-10 14:08:57', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15100, 2901, '2009-07-10 14:11:35', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15100, 2902, '2009-07-10 14:11:35', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15101, 2901, '2009-07-10 14:11:35', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15101, 2902, '2009-07-10 14:11:35', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15102, 2901, '2009-07-10 14:13:35', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15102, 2902, '2009-07-10 14:16:53', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15103, 2901, '2009-07-10 14:18:04', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15089, 2902, '2009-07-10 14:21:53', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15104, 2901, '2009-07-10 14:25:00', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15105, 2901, '2009-07-10 14:25:03', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15105, 2902, '2009-07-10 14:25:28', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15104, 2902, '2009-07-10 14:25:28', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15106, 2901, '2009-07-10 14:27:48', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15107, 2901, '2009-07-14 14:11:30', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15107, 2902, '2009-07-14 14:11:32', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15103, 2902, '2009-07-21 17:09:08', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (15106, 2902, '2009-07-21 17:09:09', 0, 'Realiso Online');
INSERT INTO isms_context_date (fkcontext, naction, ddate, nuserid, susername) VALUES (13, 2902, '2009-07-21 17:09:48', 0, 'Realiso Online');


--
-- TOC entry 3141 (class 0 OID 9034733)
-- Dependencies: 2001
-- Data for Name: isms_context_history; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1, NULL, 'areas_total', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2, NULL, 'areas_with_high_risk', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (3, NULL, 'areas_with_medium_risk', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (4, NULL, 'areas_with_low_risk', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (5, NULL, 'areas_with_no_value_risk', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (6, NULL, 'complete_areas', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (7, NULL, 'partial_areas', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (8, NULL, 'not_managed_areas', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (9, NULL, 'areas_without_processes', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (10, NULL, 'process_total', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (11, NULL, 'processes_with_high_risk', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (12, NULL, 'processes_with_medium_risk', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (13, NULL, 'processes_with_low_risk', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (14, NULL, 'processes_with_no_value_risk', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (15, NULL, 'complete_processes', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (16, NULL, 'partial_processes', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (17, NULL, 'not_managed_processes', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (18, NULL, 'processes_without_assets', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (19, NULL, 'asset_total', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (20, NULL, 'assets_with_high_risk', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (21, NULL, 'assets_with_medium_risk', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (22, NULL, 'assets_with_low_risk', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (23, NULL, 'assets_with_no_value_risk', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (24, NULL, 'complete_assets', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (25, NULL, 'partial_assets', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (26, NULL, 'not_managed_assets', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (27, NULL, 'assets_without_risks', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (28, NULL, 'risk_total', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (29, NULL, 'risks_with_high_real_risk', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (30, NULL, 'risks_with_high_residual_risk', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (31, NULL, 'risks_with_medium_real_risk', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (32, NULL, 'risks_with_medium_residual_risk', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (33, NULL, 'risks_with_low_real_risk', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (34, NULL, 'risks_with_low_residual_risk', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (35, NULL, 'risks_with_no_value_risk', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (36, NULL, 'risks_treated', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (37, NULL, 'risks_not_treated', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (38, NULL, 'risks_accepted', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (39, NULL, 'risks_transfered', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (40, NULL, 'risks_avoided', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (41, NULL, 'control_total', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (42, NULL, 'planned_implantation_controls', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (43, NULL, 'late_implantation_controls', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (44, NULL, 'adequate_implemented_controls', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (45, NULL, 'inadequate_implemented_controls', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (46, NULL, 'not_measured_implemented_controls', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (47, NULL, 'tested_ok_controls', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (48, NULL, 'tested_not_ok_controls', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (49, NULL, 'revision_ok_controls', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (50, NULL, 'revision_not_ok_controls', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (51, NULL, 'controls_without_risks', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (52, NULL, 'asset_total_cost', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (53, NULL, 'control_total_cost1', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (54, NULL, 'control_total_cost2', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (55, NULL, 'control_total_cost3', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (56, NULL, 'control_total_cost4', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (57, NULL, 'control_total_cost5', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (58, NULL, 'potentially_low_risk_cost', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (59, NULL, 'mitigated_risk_cost', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (60, NULL, 'avoided_risk_cost', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (61, NULL, 'transfered_risk_cost', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (62, NULL, 'accepted_risk_cost', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (63, NULL, 'not_treated_and_medium_high_risk', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (64, NULL, 'document_total', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (65, NULL, 'documents_by_state_approved', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (66, NULL, 'documents_by_state_developing', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (67, NULL, 'documents_by_state_pendant', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (68, NULL, 'documents_by_type_area', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (69, NULL, 'documents_by_type_asset', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (70, NULL, 'documents_by_type_control', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (71, NULL, 'documents_by_type_management', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (72, NULL, 'documents_by_type_none', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (73, NULL, 'documents_by_type_others', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (74, NULL, 'documents_by_type_policy', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (75, NULL, 'documents_by_type_process', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (76, NULL, 'documents_by_type_scope', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (77, NULL, 'register_total', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (78, NULL, 'documents_read', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (79, NULL, 'documents_not_read', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (80, NULL, 'documents_scheduled', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (81, NULL, 'occupation_documents', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (82, NULL, 'occupation_template', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (83, NULL, 'occupation_registers', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (84, NULL, 'occupation_total', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (85, NULL, 'incident_total', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (86, NULL, 'non_conformity_total', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (87, NULL, 'incidents_by_state_directed', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (88, NULL, 'incidents_by_state_open', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (89, NULL, 'incidents_by_state_pendant_disposal', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (90, NULL, 'incidents_by_state_pendant_solution', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (91, NULL, 'incidents_by_state_solved', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (92, NULL, 'incidents_by_state_waiting_solution', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (93, NULL, 'nc_by_state_ci_ap_pendant', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (94, NULL, 'nc_by_state_ci_closed', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (95, NULL, 'nc_by_state_ci_directed', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (96, NULL, 'nc_by_state_ci_finished', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (97, NULL, 'nc_by_state_ci_nc_pendant', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (98, NULL, 'nc_by_state_ci_open', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (99, NULL, 'nc_by_state_ci_sent', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (100, NULL, 'nc_by_state_ci_waiting_conclusion', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (101, NULL, 'nc_by_state_denied', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (102, NULL, 'ap_total', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (103, NULL, 'ap_finished', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (104, NULL, 'ap_finished_late', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (105, NULL, 'ap_efficient', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (106, NULL, 'ap_non_efficient', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (107, NULL, 'nc_per_ap_average', '2009-07-10 00:06:52.898904', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (108, 15050, 'area_value', '2009-07-11 00:07:26.615978', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (109, NULL, 'areas_total', '2009-07-11 00:07:26.615978', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (110, NULL, 'areas_with_high_risk', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (111, NULL, 'areas_with_medium_risk', '2009-07-11 00:07:26.615978', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (112, NULL, 'areas_with_low_risk', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (113, NULL, 'areas_with_no_value_risk', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (114, NULL, 'complete_areas', '2009-07-11 00:07:26.615978', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (115, NULL, 'partial_areas', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (116, NULL, 'not_managed_areas', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (117, NULL, 'areas_without_processes', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (118, 15050, 'risk_amount_by_area', '2009-07-11 00:07:26.615978', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (119, 15050, 'total_area_impact', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (120, 15050, 'control_amount_by_area', '2009-07-11 00:07:26.615978', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (121, 15050, 'control_cost_by_area', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (122, 15050, 'process_amount_by_area', '2009-07-11 00:07:26.615978', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (123, 15050, 'asset_amount_by_area', '2009-07-11 00:07:26.615978', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (124, 15050, 'asset_cost_by_area', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (125, 15051, 'process_value', '2009-07-11 00:07:26.615978', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (126, 15051, 'asset_amount_by_process', '2009-07-11 00:07:26.615978', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (127, 15051, 'asset_cost_by_process', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (128, 15051, 'risk_amount_by_process', '2009-07-11 00:07:26.615978', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (129, NULL, 'process_total', '2009-07-11 00:07:26.615978', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (130, NULL, 'processes_with_high_risk', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (131, NULL, 'processes_with_medium_risk', '2009-07-11 00:07:26.615978', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (132, NULL, 'processes_with_low_risk', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (133, NULL, 'processes_with_no_value_risk', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (134, NULL, 'complete_processes', '2009-07-11 00:07:26.615978', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (135, NULL, 'partial_processes', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (136, NULL, 'not_managed_processes', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (137, NULL, 'processes_without_assets', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (138, 15051, 'total_process_impact', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (139, 15051, 'control_amount_by_process', '2009-07-11 00:07:26.615978', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (140, 15051, 'control_cost_by_process', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (141, 15082, 'asset_value', '2009-07-11 00:07:26.615978', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (142, 15082, 'process_amount_by_asset', '2009-07-11 00:07:26.615978', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (143, 15082, 'risk_amount_by_asset', '2009-07-11 00:07:26.615978', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (144, NULL, 'asset_total', '2009-07-11 00:07:26.615978', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (145, NULL, 'assets_with_high_risk', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (146, NULL, 'assets_with_medium_risk', '2009-07-11 00:07:26.615978', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (147, NULL, 'assets_with_low_risk', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (148, NULL, 'assets_with_no_value_risk', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (149, NULL, 'complete_assets', '2009-07-11 00:07:26.615978', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (150, NULL, 'partial_assets', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (151, NULL, 'not_managed_assets', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (152, NULL, 'assets_without_risks', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (153, 15082, 'total_asset_impact', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (154, 15082, 'control_amount_by_asset', '2009-07-11 00:07:26.615978', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (155, 15082, 'control_cost_by_asset', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (156, 15084, 'risk_value', '2009-07-11 00:07:26.615978', 5);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (157, 15084, 'risk_residual_value', '2009-07-11 00:07:26.615978', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (158, 15084, 'control_amount_by_risk', '2009-07-11 00:07:26.615978', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (159, NULL, 'risk_total', '2009-07-11 00:07:26.615978', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (160, NULL, 'risks_with_high_real_risk', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (161, NULL, 'risks_with_high_residual_risk', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (162, NULL, 'risks_with_medium_real_risk', '2009-07-11 00:07:26.615978', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (163, NULL, 'risks_with_medium_residual_risk', '2009-07-11 00:07:26.615978', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (164, NULL, 'risks_with_low_real_risk', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (165, NULL, 'risks_with_low_residual_risk', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (166, NULL, 'risks_with_no_value_risk', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (167, NULL, 'risks_treated', '2009-07-11 00:07:26.615978', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (168, NULL, 'risks_not_treated', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (169, NULL, 'risks_accepted', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (170, NULL, 'risks_transfered', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (171, NULL, 'risks_avoided', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (172, 15089, 'risk_amount_by_control', '2009-07-11 00:07:26.615978', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (173, NULL, 'control_total', '2009-07-11 00:07:26.615978', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (174, NULL, 'planned_implantation_controls', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (175, NULL, 'late_implantation_controls', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (176, NULL, 'adequate_implemented_controls', '2009-07-11 00:07:26.615978', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (177, NULL, 'inadequate_implemented_controls', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (178, NULL, 'not_measured_implemented_controls', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (179, NULL, 'tested_ok_controls', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (180, NULL, 'tested_not_ok_controls', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (181, NULL, 'revision_ok_controls', '2009-07-11 00:07:26.615978', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (182, NULL, 'revision_not_ok_controls', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (183, NULL, 'controls_without_risks', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (184, 15049, 'best_practice_amount_by_standard', '2009-07-11 00:07:26.615978', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (185, 15048, 'best_practice_amount_by_standard', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (186, 15047, 'best_practice_amount_by_standard', '2009-07-11 00:07:26.615978', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (187, 15046, 'best_practice_amount_by_standard', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (188, 15045, 'best_practice_amount_by_standard', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (189, 15049, 'applied_best_practice_amount_by_standard', '2009-07-11 00:07:26.615978', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (190, 15048, 'applied_best_practice_amount_by_standard', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (191, 15047, 'applied_best_practice_amount_by_standard', '2009-07-11 00:07:26.615978', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (192, 15046, 'applied_best_practice_amount_by_standard', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (193, 15045, 'applied_best_practice_amount_by_standard', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (194, NULL, 'asset_total_cost', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (195, NULL, 'control_total_cost1', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (196, NULL, 'control_total_cost2', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (197, NULL, 'control_total_cost3', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (198, NULL, 'control_total_cost4', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (199, NULL, 'control_total_cost5', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (200, NULL, 'potentially_low_risk_cost', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (201, NULL, 'mitigated_risk_cost', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (202, NULL, 'avoided_risk_cost', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (203, NULL, 'transfered_risk_cost', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (204, NULL, 'accepted_risk_cost', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (205, NULL, 'not_treated_and_medium_high_risk', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (206, NULL, 'document_total', '2009-07-11 00:07:26.615978', 3);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (207, NULL, 'documents_by_state_approved', '2009-07-11 00:07:26.615978', 3);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (208, NULL, 'documents_by_state_developing', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (209, NULL, 'documents_by_state_pendant', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (210, NULL, 'documents_by_type_area', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (211, NULL, 'documents_by_type_asset', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (212, NULL, 'documents_by_type_control', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (213, NULL, 'documents_by_type_management', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (214, NULL, 'documents_by_type_none', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (215, NULL, 'documents_by_type_others', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (216, NULL, 'documents_by_type_policy', '2009-07-11 00:07:26.615978', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (217, NULL, 'documents_by_type_process', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (218, NULL, 'documents_by_type_scope', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (219, NULL, 'register_total', '2009-07-11 00:07:26.615978', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (220, NULL, 'documents_read', '2009-07-11 00:07:26.615978', 3);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (221, NULL, 'documents_not_read', '2009-07-11 00:07:26.615978', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (222, NULL, 'documents_scheduled', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (223, NULL, 'occupation_documents', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (224, NULL, 'occupation_template', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (225, NULL, 'occupation_registers', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (226, NULL, 'occupation_total', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (227, NULL, 'incident_total', '2009-07-11 00:07:26.615978', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (228, NULL, 'non_conformity_total', '2009-07-11 00:07:26.615978', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (229, NULL, 'incidents_by_state_directed', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (230, NULL, 'incidents_by_state_open', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (231, NULL, 'incidents_by_state_pendant_disposal', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (232, NULL, 'incidents_by_state_pendant_solution', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (233, NULL, 'incidents_by_state_solved', '2009-07-11 00:07:26.615978', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (234, NULL, 'incidents_by_state_waiting_solution', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (235, NULL, 'nc_by_state_ci_ap_pendant', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (236, NULL, 'nc_by_state_ci_closed', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (237, NULL, 'nc_by_state_ci_directed', '2009-07-11 00:07:26.615978', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (238, NULL, 'nc_by_state_ci_finished', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (239, NULL, 'nc_by_state_ci_nc_pendant', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (240, NULL, 'nc_by_state_ci_open', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (241, NULL, 'nc_by_state_ci_sent', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (242, NULL, 'nc_by_state_ci_waiting_conclusion', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (243, NULL, 'nc_by_state_denied', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (244, NULL, 'ap_total', '2009-07-11 00:07:26.615978', 2);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (245, NULL, 'ap_finished', '2009-07-11 00:07:26.615978', 2);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (246, NULL, 'ap_finished_late', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (247, NULL, 'ap_efficient', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (248, NULL, 'ap_non_efficient', '2009-07-11 00:07:26.615978', 2);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (249, NULL, 'nc_per_ap_average', '2009-07-11 00:07:26.615978', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (250, 15050, 'area_value', '2009-07-12 00:07:25.919417', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (251, NULL, 'areas_total', '2009-07-12 00:07:25.919417', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (252, NULL, 'areas_with_high_risk', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (253, NULL, 'areas_with_medium_risk', '2009-07-12 00:07:25.919417', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (254, NULL, 'areas_with_low_risk', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (255, NULL, 'areas_with_no_value_risk', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (256, NULL, 'complete_areas', '2009-07-12 00:07:25.919417', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (257, NULL, 'partial_areas', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (258, NULL, 'not_managed_areas', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (259, NULL, 'areas_without_processes', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (260, 15050, 'risk_amount_by_area', '2009-07-12 00:07:25.919417', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (261, 15050, 'total_area_impact', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (262, 15050, 'control_amount_by_area', '2009-07-12 00:07:25.919417', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (263, 15050, 'control_cost_by_area', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (264, 15050, 'process_amount_by_area', '2009-07-12 00:07:25.919417', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (265, 15050, 'asset_amount_by_area', '2009-07-12 00:07:25.919417', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (266, 15050, 'asset_cost_by_area', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (267, 15051, 'process_value', '2009-07-12 00:07:25.919417', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (268, 15051, 'asset_amount_by_process', '2009-07-12 00:07:25.919417', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (269, 15051, 'asset_cost_by_process', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (270, 15051, 'risk_amount_by_process', '2009-07-12 00:07:25.919417', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (271, NULL, 'process_total', '2009-07-12 00:07:25.919417', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (272, NULL, 'processes_with_high_risk', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (273, NULL, 'processes_with_medium_risk', '2009-07-12 00:07:25.919417', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (274, NULL, 'processes_with_low_risk', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (275, NULL, 'processes_with_no_value_risk', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (276, NULL, 'complete_processes', '2009-07-12 00:07:25.919417', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (277, NULL, 'partial_processes', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (278, NULL, 'not_managed_processes', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (279, NULL, 'processes_without_assets', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (280, 15051, 'total_process_impact', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (281, 15051, 'control_amount_by_process', '2009-07-12 00:07:25.919417', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (282, 15051, 'control_cost_by_process', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (283, 15082, 'asset_value', '2009-07-12 00:07:25.919417', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (284, 15082, 'process_amount_by_asset', '2009-07-12 00:07:25.919417', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (285, 15082, 'risk_amount_by_asset', '2009-07-12 00:07:25.919417', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (286, NULL, 'asset_total', '2009-07-12 00:07:25.919417', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (287, NULL, 'assets_with_high_risk', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (288, NULL, 'assets_with_medium_risk', '2009-07-12 00:07:25.919417', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (289, NULL, 'assets_with_low_risk', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (290, NULL, 'assets_with_no_value_risk', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (291, NULL, 'complete_assets', '2009-07-12 00:07:25.919417', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (292, NULL, 'partial_assets', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (293, NULL, 'not_managed_assets', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (294, NULL, 'assets_without_risks', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (295, 15082, 'total_asset_impact', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (296, 15082, 'control_amount_by_asset', '2009-07-12 00:07:25.919417', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (297, 15082, 'control_cost_by_asset', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (298, 15084, 'risk_value', '2009-07-12 00:07:25.919417', 5);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (299, 15084, 'risk_residual_value', '2009-07-12 00:07:25.919417', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (300, 15084, 'control_amount_by_risk', '2009-07-12 00:07:25.919417', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (301, NULL, 'risk_total', '2009-07-12 00:07:25.919417', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (302, NULL, 'risks_with_high_real_risk', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (303, NULL, 'risks_with_high_residual_risk', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (304, NULL, 'risks_with_medium_real_risk', '2009-07-12 00:07:25.919417', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (305, NULL, 'risks_with_medium_residual_risk', '2009-07-12 00:07:25.919417', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (306, NULL, 'risks_with_low_real_risk', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (307, NULL, 'risks_with_low_residual_risk', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (308, NULL, 'risks_with_no_value_risk', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (309, NULL, 'risks_treated', '2009-07-12 00:07:25.919417', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (310, NULL, 'risks_not_treated', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (311, NULL, 'risks_accepted', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (312, NULL, 'risks_transfered', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (313, NULL, 'risks_avoided', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (314, 15089, 'risk_amount_by_control', '2009-07-12 00:07:25.919417', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (315, NULL, 'control_total', '2009-07-12 00:07:25.919417', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (316, NULL, 'planned_implantation_controls', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (317, NULL, 'late_implantation_controls', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (318, NULL, 'adequate_implemented_controls', '2009-07-12 00:07:25.919417', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (319, NULL, 'inadequate_implemented_controls', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (320, NULL, 'not_measured_implemented_controls', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (321, NULL, 'tested_ok_controls', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (322, NULL, 'tested_not_ok_controls', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (323, NULL, 'revision_ok_controls', '2009-07-12 00:07:25.919417', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (324, NULL, 'revision_not_ok_controls', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (325, NULL, 'controls_without_risks', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (326, 15049, 'best_practice_amount_by_standard', '2009-07-12 00:07:25.919417', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (327, 15048, 'best_practice_amount_by_standard', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (328, 15047, 'best_practice_amount_by_standard', '2009-07-12 00:07:25.919417', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (329, 15046, 'best_practice_amount_by_standard', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (330, 15045, 'best_practice_amount_by_standard', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (331, 15049, 'applied_best_practice_amount_by_standard', '2009-07-12 00:07:25.919417', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (332, 15048, 'applied_best_practice_amount_by_standard', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (333, 15047, 'applied_best_practice_amount_by_standard', '2009-07-12 00:07:25.919417', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (334, 15046, 'applied_best_practice_amount_by_standard', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (335, 15045, 'applied_best_practice_amount_by_standard', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (336, NULL, 'asset_total_cost', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (337, NULL, 'control_total_cost1', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (338, NULL, 'control_total_cost2', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (339, NULL, 'control_total_cost3', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (340, NULL, 'control_total_cost4', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (341, NULL, 'control_total_cost5', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (342, NULL, 'potentially_low_risk_cost', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (343, NULL, 'mitigated_risk_cost', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (344, NULL, 'avoided_risk_cost', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (345, NULL, 'transfered_risk_cost', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (346, NULL, 'accepted_risk_cost', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (347, NULL, 'not_treated_and_medium_high_risk', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (348, NULL, 'document_total', '2009-07-12 00:07:25.919417', 3);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (349, NULL, 'documents_by_state_approved', '2009-07-12 00:07:25.919417', 3);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (350, NULL, 'documents_by_state_developing', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (351, NULL, 'documents_by_state_pendant', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (352, NULL, 'documents_by_type_area', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (353, NULL, 'documents_by_type_asset', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (354, NULL, 'documents_by_type_control', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (355, NULL, 'documents_by_type_management', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (356, NULL, 'documents_by_type_none', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (357, NULL, 'documents_by_type_others', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (358, NULL, 'documents_by_type_policy', '2009-07-12 00:07:25.919417', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (359, NULL, 'documents_by_type_process', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (360, NULL, 'documents_by_type_scope', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (361, NULL, 'register_total', '2009-07-12 00:07:25.919417', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (362, NULL, 'documents_read', '2009-07-12 00:07:25.919417', 3);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (363, NULL, 'documents_not_read', '2009-07-12 00:07:25.919417', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (364, NULL, 'documents_scheduled', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (365, NULL, 'occupation_documents', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (366, NULL, 'occupation_template', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (367, NULL, 'occupation_registers', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (368, NULL, 'occupation_total', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (369, NULL, 'incident_total', '2009-07-12 00:07:25.919417', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (370, NULL, 'non_conformity_total', '2009-07-12 00:07:25.919417', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (371, NULL, 'incidents_by_state_directed', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (372, NULL, 'incidents_by_state_open', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (373, NULL, 'incidents_by_state_pendant_disposal', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (374, NULL, 'incidents_by_state_pendant_solution', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (375, NULL, 'incidents_by_state_solved', '2009-07-12 00:07:25.919417', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (376, NULL, 'incidents_by_state_waiting_solution', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (377, NULL, 'nc_by_state_ci_ap_pendant', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (378, NULL, 'nc_by_state_ci_closed', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (379, NULL, 'nc_by_state_ci_directed', '2009-07-12 00:07:25.919417', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (380, NULL, 'nc_by_state_ci_finished', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (381, NULL, 'nc_by_state_ci_nc_pendant', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (382, NULL, 'nc_by_state_ci_open', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (383, NULL, 'nc_by_state_ci_sent', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (384, NULL, 'nc_by_state_ci_waiting_conclusion', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (385, NULL, 'nc_by_state_denied', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (386, NULL, 'ap_total', '2009-07-12 00:07:25.919417', 2);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (387, NULL, 'ap_finished', '2009-07-12 00:07:25.919417', 2);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (388, NULL, 'ap_finished_late', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (389, NULL, 'ap_efficient', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (390, NULL, 'ap_non_efficient', '2009-07-12 00:07:25.919417', 2);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (391, NULL, 'nc_per_ap_average', '2009-07-12 00:07:25.919417', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (392, 15050, 'area_value', '2009-07-13 00:06:09.512075', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (393, NULL, 'areas_total', '2009-07-13 00:06:09.512075', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (394, NULL, 'areas_with_high_risk', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (395, NULL, 'areas_with_medium_risk', '2009-07-13 00:06:09.512075', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (396, NULL, 'areas_with_low_risk', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (397, NULL, 'areas_with_no_value_risk', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (398, NULL, 'complete_areas', '2009-07-13 00:06:09.512075', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (399, NULL, 'partial_areas', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (400, NULL, 'not_managed_areas', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (401, NULL, 'areas_without_processes', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (402, 15050, 'risk_amount_by_area', '2009-07-13 00:06:09.512075', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (403, 15050, 'total_area_impact', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (404, 15050, 'control_amount_by_area', '2009-07-13 00:06:09.512075', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (405, 15050, 'control_cost_by_area', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (406, 15050, 'process_amount_by_area', '2009-07-13 00:06:09.512075', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (407, 15050, 'asset_amount_by_area', '2009-07-13 00:06:09.512075', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (408, 15050, 'asset_cost_by_area', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (409, 15051, 'process_value', '2009-07-13 00:06:09.512075', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (410, 15051, 'asset_amount_by_process', '2009-07-13 00:06:09.512075', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (411, 15051, 'asset_cost_by_process', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (412, 15051, 'risk_amount_by_process', '2009-07-13 00:06:09.512075', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (413, NULL, 'process_total', '2009-07-13 00:06:09.512075', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (414, NULL, 'processes_with_high_risk', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (415, NULL, 'processes_with_medium_risk', '2009-07-13 00:06:09.512075', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (416, NULL, 'processes_with_low_risk', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (417, NULL, 'processes_with_no_value_risk', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (418, NULL, 'complete_processes', '2009-07-13 00:06:09.512075', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (419, NULL, 'partial_processes', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (420, NULL, 'not_managed_processes', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (421, NULL, 'processes_without_assets', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (422, 15051, 'total_process_impact', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (423, 15051, 'control_amount_by_process', '2009-07-13 00:06:09.512075', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (424, 15051, 'control_cost_by_process', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (425, 15082, 'asset_value', '2009-07-13 00:06:09.512075', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (426, 15082, 'process_amount_by_asset', '2009-07-13 00:06:09.512075', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (427, 15082, 'risk_amount_by_asset', '2009-07-13 00:06:09.512075', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (428, NULL, 'asset_total', '2009-07-13 00:06:09.512075', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (429, NULL, 'assets_with_high_risk', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (430, NULL, 'assets_with_medium_risk', '2009-07-13 00:06:09.512075', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (431, NULL, 'assets_with_low_risk', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (432, NULL, 'assets_with_no_value_risk', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (433, NULL, 'complete_assets', '2009-07-13 00:06:09.512075', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (434, NULL, 'partial_assets', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (435, NULL, 'not_managed_assets', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (436, NULL, 'assets_without_risks', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (437, 15082, 'total_asset_impact', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (438, 15082, 'control_amount_by_asset', '2009-07-13 00:06:09.512075', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (439, 15082, 'control_cost_by_asset', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (440, 15084, 'risk_value', '2009-07-13 00:06:09.512075', 5);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (441, 15084, 'risk_residual_value', '2009-07-13 00:06:09.512075', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (442, 15084, 'control_amount_by_risk', '2009-07-13 00:06:09.512075', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (443, NULL, 'risk_total', '2009-07-13 00:06:09.512075', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (444, NULL, 'risks_with_high_real_risk', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (445, NULL, 'risks_with_high_residual_risk', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (446, NULL, 'risks_with_medium_real_risk', '2009-07-13 00:06:09.512075', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (447, NULL, 'risks_with_medium_residual_risk', '2009-07-13 00:06:09.512075', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (448, NULL, 'risks_with_low_real_risk', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (449, NULL, 'risks_with_low_residual_risk', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (450, NULL, 'risks_with_no_value_risk', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (451, NULL, 'risks_treated', '2009-07-13 00:06:09.512075', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (452, NULL, 'risks_not_treated', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (453, NULL, 'risks_accepted', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (454, NULL, 'risks_transfered', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (455, NULL, 'risks_avoided', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (456, 15089, 'risk_amount_by_control', '2009-07-13 00:06:09.512075', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (457, NULL, 'control_total', '2009-07-13 00:06:09.512075', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (458, NULL, 'planned_implantation_controls', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (459, NULL, 'late_implantation_controls', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (460, NULL, 'adequate_implemented_controls', '2009-07-13 00:06:09.512075', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (461, NULL, 'inadequate_implemented_controls', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (462, NULL, 'not_measured_implemented_controls', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (463, NULL, 'tested_ok_controls', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (464, NULL, 'tested_not_ok_controls', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (465, NULL, 'revision_ok_controls', '2009-07-13 00:06:09.512075', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (466, NULL, 'revision_not_ok_controls', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (467, NULL, 'controls_without_risks', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (468, 15049, 'best_practice_amount_by_standard', '2009-07-13 00:06:09.512075', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (469, 15048, 'best_practice_amount_by_standard', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (470, 15047, 'best_practice_amount_by_standard', '2009-07-13 00:06:09.512075', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (471, 15046, 'best_practice_amount_by_standard', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (472, 15045, 'best_practice_amount_by_standard', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (473, 15049, 'applied_best_practice_amount_by_standard', '2009-07-13 00:06:09.512075', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (474, 15048, 'applied_best_practice_amount_by_standard', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (475, 15047, 'applied_best_practice_amount_by_standard', '2009-07-13 00:06:09.512075', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (476, 15046, 'applied_best_practice_amount_by_standard', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (477, 15045, 'applied_best_practice_amount_by_standard', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (478, NULL, 'asset_total_cost', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (479, NULL, 'control_total_cost1', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (480, NULL, 'control_total_cost2', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (481, NULL, 'control_total_cost3', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (482, NULL, 'control_total_cost4', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (483, NULL, 'control_total_cost5', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (484, NULL, 'potentially_low_risk_cost', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (485, NULL, 'mitigated_risk_cost', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (486, NULL, 'avoided_risk_cost', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (487, NULL, 'transfered_risk_cost', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (488, NULL, 'accepted_risk_cost', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (489, NULL, 'not_treated_and_medium_high_risk', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (490, NULL, 'document_total', '2009-07-13 00:06:09.512075', 3);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (491, NULL, 'documents_by_state_approved', '2009-07-13 00:06:09.512075', 3);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (492, NULL, 'documents_by_state_developing', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (493, NULL, 'documents_by_state_pendant', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (494, NULL, 'documents_by_type_area', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (495, NULL, 'documents_by_type_asset', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (496, NULL, 'documents_by_type_control', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (497, NULL, 'documents_by_type_management', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (498, NULL, 'documents_by_type_none', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (499, NULL, 'documents_by_type_others', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (500, NULL, 'documents_by_type_policy', '2009-07-13 00:06:09.512075', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (501, NULL, 'documents_by_type_process', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (502, NULL, 'documents_by_type_scope', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (503, NULL, 'register_total', '2009-07-13 00:06:09.512075', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (504, NULL, 'documents_read', '2009-07-13 00:06:09.512075', 3);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (505, NULL, 'documents_not_read', '2009-07-13 00:06:09.512075', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (506, NULL, 'documents_scheduled', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (507, NULL, 'occupation_documents', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (508, NULL, 'occupation_template', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (509, NULL, 'occupation_registers', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (510, NULL, 'occupation_total', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (511, NULL, 'incident_total', '2009-07-13 00:06:09.512075', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (512, NULL, 'non_conformity_total', '2009-07-13 00:06:09.512075', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (513, NULL, 'incidents_by_state_directed', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (514, NULL, 'incidents_by_state_open', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (515, NULL, 'incidents_by_state_pendant_disposal', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (516, NULL, 'incidents_by_state_pendant_solution', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (517, NULL, 'incidents_by_state_solved', '2009-07-13 00:06:09.512075', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (518, NULL, 'incidents_by_state_waiting_solution', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (519, NULL, 'nc_by_state_ci_ap_pendant', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (520, NULL, 'nc_by_state_ci_closed', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (521, NULL, 'nc_by_state_ci_directed', '2009-07-13 00:06:09.512075', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (522, NULL, 'nc_by_state_ci_finished', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (523, NULL, 'nc_by_state_ci_nc_pendant', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (524, NULL, 'nc_by_state_ci_open', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (525, NULL, 'nc_by_state_ci_sent', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (526, NULL, 'nc_by_state_ci_waiting_conclusion', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (527, NULL, 'nc_by_state_denied', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (528, NULL, 'ap_total', '2009-07-13 00:06:09.512075', 2);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (529, NULL, 'ap_finished', '2009-07-13 00:06:09.512075', 2);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (530, NULL, 'ap_finished_late', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (531, NULL, 'ap_efficient', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (532, NULL, 'ap_non_efficient', '2009-07-13 00:06:09.512075', 2);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (533, NULL, 'nc_per_ap_average', '2009-07-13 00:06:09.512075', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (534, 15050, 'area_value', '2009-07-14 00:07:19.813482', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (535, NULL, 'areas_total', '2009-07-14 00:07:19.813482', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (536, NULL, 'areas_with_high_risk', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (537, NULL, 'areas_with_medium_risk', '2009-07-14 00:07:19.813482', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (538, NULL, 'areas_with_low_risk', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (539, NULL, 'areas_with_no_value_risk', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (540, NULL, 'complete_areas', '2009-07-14 00:07:19.813482', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (541, NULL, 'partial_areas', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (542, NULL, 'not_managed_areas', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (543, NULL, 'areas_without_processes', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (544, 15050, 'risk_amount_by_area', '2009-07-14 00:07:19.813482', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (545, 15050, 'total_area_impact', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (546, 15050, 'control_amount_by_area', '2009-07-14 00:07:19.813482', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (547, 15050, 'control_cost_by_area', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (548, 15050, 'process_amount_by_area', '2009-07-14 00:07:19.813482', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (549, 15050, 'asset_amount_by_area', '2009-07-14 00:07:19.813482', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (550, 15050, 'asset_cost_by_area', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (551, 15051, 'process_value', '2009-07-14 00:07:19.813482', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (552, 15051, 'asset_amount_by_process', '2009-07-14 00:07:19.813482', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (553, 15051, 'asset_cost_by_process', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (554, 15051, 'risk_amount_by_process', '2009-07-14 00:07:19.813482', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (555, NULL, 'process_total', '2009-07-14 00:07:19.813482', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (556, NULL, 'processes_with_high_risk', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (557, NULL, 'processes_with_medium_risk', '2009-07-14 00:07:19.813482', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (558, NULL, 'processes_with_low_risk', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (559, NULL, 'processes_with_no_value_risk', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (560, NULL, 'complete_processes', '2009-07-14 00:07:19.813482', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (561, NULL, 'partial_processes', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (562, NULL, 'not_managed_processes', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (563, NULL, 'processes_without_assets', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (564, 15051, 'total_process_impact', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (565, 15051, 'control_amount_by_process', '2009-07-14 00:07:19.813482', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (566, 15051, 'control_cost_by_process', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (567, 15082, 'asset_value', '2009-07-14 00:07:19.813482', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (568, 15082, 'process_amount_by_asset', '2009-07-14 00:07:19.813482', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (569, 15082, 'risk_amount_by_asset', '2009-07-14 00:07:19.813482', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (570, NULL, 'asset_total', '2009-07-14 00:07:19.813482', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (571, NULL, 'assets_with_high_risk', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (572, NULL, 'assets_with_medium_risk', '2009-07-14 00:07:19.813482', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (573, NULL, 'assets_with_low_risk', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (574, NULL, 'assets_with_no_value_risk', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (575, NULL, 'complete_assets', '2009-07-14 00:07:19.813482', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (576, NULL, 'partial_assets', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (577, NULL, 'not_managed_assets', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (578, NULL, 'assets_without_risks', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (579, 15082, 'total_asset_impact', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (580, 15082, 'control_amount_by_asset', '2009-07-14 00:07:19.813482', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (581, 15082, 'control_cost_by_asset', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (582, 15084, 'risk_value', '2009-07-14 00:07:19.813482', 5);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (583, 15084, 'risk_residual_value', '2009-07-14 00:07:19.813482', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (584, 15084, 'control_amount_by_risk', '2009-07-14 00:07:19.813482', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (585, NULL, 'risk_total', '2009-07-14 00:07:19.813482', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (586, NULL, 'risks_with_high_real_risk', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (587, NULL, 'risks_with_high_residual_risk', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (588, NULL, 'risks_with_medium_real_risk', '2009-07-14 00:07:19.813482', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (589, NULL, 'risks_with_medium_residual_risk', '2009-07-14 00:07:19.813482', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (590, NULL, 'risks_with_low_real_risk', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (591, NULL, 'risks_with_low_residual_risk', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (592, NULL, 'risks_with_no_value_risk', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (593, NULL, 'risks_treated', '2009-07-14 00:07:19.813482', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (594, NULL, 'risks_not_treated', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (595, NULL, 'risks_accepted', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (596, NULL, 'risks_transfered', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (597, NULL, 'risks_avoided', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (598, 15089, 'risk_amount_by_control', '2009-07-14 00:07:19.813482', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (599, NULL, 'control_total', '2009-07-14 00:07:19.813482', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (600, NULL, 'planned_implantation_controls', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (601, NULL, 'late_implantation_controls', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (602, NULL, 'adequate_implemented_controls', '2009-07-14 00:07:19.813482', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (603, NULL, 'inadequate_implemented_controls', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (604, NULL, 'not_measured_implemented_controls', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (605, NULL, 'tested_ok_controls', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (606, NULL, 'tested_not_ok_controls', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (607, NULL, 'revision_ok_controls', '2009-07-14 00:07:19.813482', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (608, NULL, 'revision_not_ok_controls', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (609, NULL, 'controls_without_risks', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (610, 15049, 'best_practice_amount_by_standard', '2009-07-14 00:07:19.813482', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (611, 15048, 'best_practice_amount_by_standard', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (612, 15047, 'best_practice_amount_by_standard', '2009-07-14 00:07:19.813482', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (613, 15046, 'best_practice_amount_by_standard', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (614, 15045, 'best_practice_amount_by_standard', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (615, 15049, 'applied_best_practice_amount_by_standard', '2009-07-14 00:07:19.813482', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (616, 15048, 'applied_best_practice_amount_by_standard', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (617, 15047, 'applied_best_practice_amount_by_standard', '2009-07-14 00:07:19.813482', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (618, 15046, 'applied_best_practice_amount_by_standard', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (619, 15045, 'applied_best_practice_amount_by_standard', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (620, NULL, 'asset_total_cost', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (621, NULL, 'control_total_cost1', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (622, NULL, 'control_total_cost2', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (623, NULL, 'control_total_cost3', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (624, NULL, 'control_total_cost4', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (625, NULL, 'control_total_cost5', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (626, NULL, 'potentially_low_risk_cost', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (627, NULL, 'mitigated_risk_cost', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (628, NULL, 'avoided_risk_cost', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (629, NULL, 'transfered_risk_cost', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (630, NULL, 'accepted_risk_cost', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (631, NULL, 'not_treated_and_medium_high_risk', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (632, NULL, 'document_total', '2009-07-14 00:07:19.813482', 3);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (633, NULL, 'documents_by_state_approved', '2009-07-14 00:07:19.813482', 3);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (634, NULL, 'documents_by_state_developing', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (635, NULL, 'documents_by_state_pendant', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (636, NULL, 'documents_by_type_area', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (637, NULL, 'documents_by_type_asset', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (638, NULL, 'documents_by_type_control', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (639, NULL, 'documents_by_type_management', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (640, NULL, 'documents_by_type_none', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (641, NULL, 'documents_by_type_others', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (642, NULL, 'documents_by_type_policy', '2009-07-14 00:07:19.813482', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (643, NULL, 'documents_by_type_process', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (644, NULL, 'documents_by_type_scope', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (645, NULL, 'register_total', '2009-07-14 00:07:19.813482', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (646, NULL, 'documents_read', '2009-07-14 00:07:19.813482', 3);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (647, NULL, 'documents_not_read', '2009-07-14 00:07:19.813482', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (648, NULL, 'documents_scheduled', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (649, NULL, 'occupation_documents', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (650, NULL, 'occupation_template', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (651, NULL, 'occupation_registers', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (652, NULL, 'occupation_total', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (653, NULL, 'incident_total', '2009-07-14 00:07:19.813482', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (654, NULL, 'non_conformity_total', '2009-07-14 00:07:19.813482', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (655, NULL, 'incidents_by_state_directed', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (656, NULL, 'incidents_by_state_open', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (657, NULL, 'incidents_by_state_pendant_disposal', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (658, NULL, 'incidents_by_state_pendant_solution', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (659, NULL, 'incidents_by_state_solved', '2009-07-14 00:07:19.813482', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (660, NULL, 'incidents_by_state_waiting_solution', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (661, NULL, 'nc_by_state_ci_ap_pendant', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (662, NULL, 'nc_by_state_ci_closed', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (663, NULL, 'nc_by_state_ci_directed', '2009-07-14 00:07:19.813482', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (664, NULL, 'nc_by_state_ci_finished', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (665, NULL, 'nc_by_state_ci_nc_pendant', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (666, NULL, 'nc_by_state_ci_open', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (667, NULL, 'nc_by_state_ci_sent', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (668, NULL, 'nc_by_state_ci_waiting_conclusion', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (669, NULL, 'nc_by_state_denied', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (670, NULL, 'ap_total', '2009-07-14 00:07:19.813482', 2);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (671, NULL, 'ap_finished', '2009-07-14 00:07:19.813482', 2);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (672, NULL, 'ap_finished_late', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (673, NULL, 'ap_efficient', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (674, NULL, 'ap_non_efficient', '2009-07-14 00:07:19.813482', 2);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (675, NULL, 'nc_per_ap_average', '2009-07-14 00:07:19.813482', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (676, 15050, 'area_value', '2009-07-15 00:07:14.529879', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (677, NULL, 'areas_total', '2009-07-15 00:07:14.529879', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (678, NULL, 'areas_with_high_risk', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (679, NULL, 'areas_with_medium_risk', '2009-07-15 00:07:14.529879', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (680, NULL, 'areas_with_low_risk', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (681, NULL, 'areas_with_no_value_risk', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (682, NULL, 'complete_areas', '2009-07-15 00:07:14.529879', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (683, NULL, 'partial_areas', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (684, NULL, 'not_managed_areas', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (685, NULL, 'areas_without_processes', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (686, 15050, 'risk_amount_by_area', '2009-07-15 00:07:14.529879', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (687, 15050, 'total_area_impact', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (688, 15050, 'control_amount_by_area', '2009-07-15 00:07:14.529879', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (689, 15050, 'control_cost_by_area', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (690, 15050, 'process_amount_by_area', '2009-07-15 00:07:14.529879', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (691, 15050, 'asset_amount_by_area', '2009-07-15 00:07:14.529879', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (692, 15050, 'asset_cost_by_area', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (693, 15051, 'process_value', '2009-07-15 00:07:14.529879', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (694, 15051, 'asset_amount_by_process', '2009-07-15 00:07:14.529879', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (695, 15051, 'asset_cost_by_process', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (696, 15051, 'risk_amount_by_process', '2009-07-15 00:07:14.529879', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (697, NULL, 'process_total', '2009-07-15 00:07:14.529879', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (698, NULL, 'processes_with_high_risk', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (699, NULL, 'processes_with_medium_risk', '2009-07-15 00:07:14.529879', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (700, NULL, 'processes_with_low_risk', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (701, NULL, 'processes_with_no_value_risk', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (702, NULL, 'complete_processes', '2009-07-15 00:07:14.529879', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (703, NULL, 'partial_processes', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (704, NULL, 'not_managed_processes', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (705, NULL, 'processes_without_assets', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (706, 15051, 'total_process_impact', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (707, 15051, 'control_amount_by_process', '2009-07-15 00:07:14.529879', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (708, 15051, 'control_cost_by_process', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (709, 15082, 'asset_value', '2009-07-15 00:07:14.529879', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (710, 15082, 'process_amount_by_asset', '2009-07-15 00:07:14.529879', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (711, 15082, 'risk_amount_by_asset', '2009-07-15 00:07:14.529879', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (712, NULL, 'asset_total', '2009-07-15 00:07:14.529879', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (713, NULL, 'assets_with_high_risk', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (714, NULL, 'assets_with_medium_risk', '2009-07-15 00:07:14.529879', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (715, NULL, 'assets_with_low_risk', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (716, NULL, 'assets_with_no_value_risk', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (717, NULL, 'complete_assets', '2009-07-15 00:07:14.529879', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (718, NULL, 'partial_assets', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (719, NULL, 'not_managed_assets', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (720, NULL, 'assets_without_risks', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (721, 15082, 'total_asset_impact', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (722, 15082, 'control_amount_by_asset', '2009-07-15 00:07:14.529879', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (723, 15082, 'control_cost_by_asset', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (724, 15084, 'risk_value', '2009-07-15 00:07:14.529879', 5);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (725, 15084, 'risk_residual_value', '2009-07-15 00:07:14.529879', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (726, 15084, 'control_amount_by_risk', '2009-07-15 00:07:14.529879', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (727, NULL, 'risk_total', '2009-07-15 00:07:14.529879', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (728, NULL, 'risks_with_high_real_risk', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (729, NULL, 'risks_with_high_residual_risk', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (730, NULL, 'risks_with_medium_real_risk', '2009-07-15 00:07:14.529879', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (731, NULL, 'risks_with_medium_residual_risk', '2009-07-15 00:07:14.529879', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (732, NULL, 'risks_with_low_real_risk', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (733, NULL, 'risks_with_low_residual_risk', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (734, NULL, 'risks_with_no_value_risk', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (735, NULL, 'risks_treated', '2009-07-15 00:07:14.529879', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (736, NULL, 'risks_not_treated', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (737, NULL, 'risks_accepted', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (738, NULL, 'risks_transfered', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (739, NULL, 'risks_avoided', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (740, 15089, 'risk_amount_by_control', '2009-07-15 00:07:14.529879', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (741, NULL, 'control_total', '2009-07-15 00:07:14.529879', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (742, NULL, 'planned_implantation_controls', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (743, NULL, 'late_implantation_controls', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (744, NULL, 'adequate_implemented_controls', '2009-07-15 00:07:14.529879', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (745, NULL, 'inadequate_implemented_controls', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (746, NULL, 'not_measured_implemented_controls', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (747, NULL, 'tested_ok_controls', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (748, NULL, 'tested_not_ok_controls', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (749, NULL, 'revision_ok_controls', '2009-07-15 00:07:14.529879', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (750, NULL, 'revision_not_ok_controls', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (751, NULL, 'controls_without_risks', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (752, 15049, 'best_practice_amount_by_standard', '2009-07-15 00:07:14.529879', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (753, 15048, 'best_practice_amount_by_standard', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (754, 15047, 'best_practice_amount_by_standard', '2009-07-15 00:07:14.529879', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (755, 15046, 'best_practice_amount_by_standard', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (756, 15045, 'best_practice_amount_by_standard', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (757, 15049, 'applied_best_practice_amount_by_standard', '2009-07-15 00:07:14.529879', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (758, 15048, 'applied_best_practice_amount_by_standard', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (759, 15047, 'applied_best_practice_amount_by_standard', '2009-07-15 00:07:14.529879', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (760, 15046, 'applied_best_practice_amount_by_standard', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (761, 15045, 'applied_best_practice_amount_by_standard', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (762, NULL, 'asset_total_cost', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (763, NULL, 'control_total_cost1', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (764, NULL, 'control_total_cost2', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (765, NULL, 'control_total_cost3', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (766, NULL, 'control_total_cost4', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (767, NULL, 'control_total_cost5', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (768, NULL, 'potentially_low_risk_cost', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (769, NULL, 'mitigated_risk_cost', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (770, NULL, 'avoided_risk_cost', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (771, NULL, 'transfered_risk_cost', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (772, NULL, 'accepted_risk_cost', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (773, NULL, 'not_treated_and_medium_high_risk', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (774, NULL, 'document_total', '2009-07-15 00:07:14.529879', 3);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (775, NULL, 'documents_by_state_approved', '2009-07-15 00:07:14.529879', 3);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (776, NULL, 'documents_by_state_developing', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (777, NULL, 'documents_by_state_pendant', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (778, NULL, 'documents_by_type_area', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (779, NULL, 'documents_by_type_asset', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (780, NULL, 'documents_by_type_control', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (781, NULL, 'documents_by_type_management', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (782, NULL, 'documents_by_type_none', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (783, NULL, 'documents_by_type_others', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (784, NULL, 'documents_by_type_policy', '2009-07-15 00:07:14.529879', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (785, NULL, 'documents_by_type_process', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (786, NULL, 'documents_by_type_scope', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (787, NULL, 'register_total', '2009-07-15 00:07:14.529879', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (788, NULL, 'documents_read', '2009-07-15 00:07:14.529879', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (789, NULL, 'documents_not_read', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (790, NULL, 'documents_scheduled', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (791, NULL, 'occupation_documents', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (792, NULL, 'occupation_template', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (793, NULL, 'occupation_registers', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (794, NULL, 'occupation_total', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (795, NULL, 'incident_total', '2009-07-15 00:07:14.529879', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (796, NULL, 'non_conformity_total', '2009-07-15 00:07:14.529879', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (797, NULL, 'incidents_by_state_directed', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (798, NULL, 'incidents_by_state_open', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (799, NULL, 'incidents_by_state_pendant_disposal', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (800, NULL, 'incidents_by_state_pendant_solution', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (801, NULL, 'incidents_by_state_solved', '2009-07-15 00:07:14.529879', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (802, NULL, 'incidents_by_state_waiting_solution', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (803, NULL, 'nc_by_state_ci_ap_pendant', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (804, NULL, 'nc_by_state_ci_closed', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (805, NULL, 'nc_by_state_ci_directed', '2009-07-15 00:07:14.529879', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (806, NULL, 'nc_by_state_ci_finished', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (807, NULL, 'nc_by_state_ci_nc_pendant', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (808, NULL, 'nc_by_state_ci_open', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (809, NULL, 'nc_by_state_ci_sent', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (810, NULL, 'nc_by_state_ci_waiting_conclusion', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (811, NULL, 'nc_by_state_denied', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (812, NULL, 'ap_total', '2009-07-15 00:07:14.529879', 2);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (813, NULL, 'ap_finished', '2009-07-15 00:07:14.529879', 2);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (814, NULL, 'ap_finished_late', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (815, NULL, 'ap_efficient', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (816, NULL, 'ap_non_efficient', '2009-07-15 00:07:14.529879', 2);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (817, NULL, 'nc_per_ap_average', '2009-07-15 00:07:14.529879', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (818, 15050, 'area_value', '2009-07-16 00:08:33.073339', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (819, NULL, 'areas_total', '2009-07-16 00:08:33.073339', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (820, NULL, 'areas_with_high_risk', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (821, NULL, 'areas_with_medium_risk', '2009-07-16 00:08:33.073339', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (822, NULL, 'areas_with_low_risk', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (823, NULL, 'areas_with_no_value_risk', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (824, NULL, 'complete_areas', '2009-07-16 00:08:33.073339', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (825, NULL, 'partial_areas', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (826, NULL, 'not_managed_areas', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (827, NULL, 'areas_without_processes', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (828, 15050, 'risk_amount_by_area', '2009-07-16 00:08:33.073339', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (829, 15050, 'total_area_impact', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (830, 15050, 'control_amount_by_area', '2009-07-16 00:08:33.073339', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (831, 15050, 'control_cost_by_area', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (832, 15050, 'process_amount_by_area', '2009-07-16 00:08:33.073339', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (833, 15050, 'asset_amount_by_area', '2009-07-16 00:08:33.073339', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (834, 15050, 'asset_cost_by_area', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (835, 15051, 'process_value', '2009-07-16 00:08:33.073339', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (836, 15051, 'asset_amount_by_process', '2009-07-16 00:08:33.073339', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (837, 15051, 'asset_cost_by_process', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (838, 15051, 'risk_amount_by_process', '2009-07-16 00:08:33.073339', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (839, NULL, 'process_total', '2009-07-16 00:08:33.073339', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (840, NULL, 'processes_with_high_risk', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (841, NULL, 'processes_with_medium_risk', '2009-07-16 00:08:33.073339', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (842, NULL, 'processes_with_low_risk', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (843, NULL, 'processes_with_no_value_risk', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (844, NULL, 'complete_processes', '2009-07-16 00:08:33.073339', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (845, NULL, 'partial_processes', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (846, NULL, 'not_managed_processes', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (847, NULL, 'processes_without_assets', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (848, 15051, 'total_process_impact', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (849, 15051, 'control_amount_by_process', '2009-07-16 00:08:33.073339', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (850, 15051, 'control_cost_by_process', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (851, 15082, 'asset_value', '2009-07-16 00:08:33.073339', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (852, 15082, 'process_amount_by_asset', '2009-07-16 00:08:33.073339', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (853, 15082, 'risk_amount_by_asset', '2009-07-16 00:08:33.073339', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (854, NULL, 'asset_total', '2009-07-16 00:08:33.073339', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (855, NULL, 'assets_with_high_risk', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (856, NULL, 'assets_with_medium_risk', '2009-07-16 00:08:33.073339', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (857, NULL, 'assets_with_low_risk', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (858, NULL, 'assets_with_no_value_risk', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (859, NULL, 'complete_assets', '2009-07-16 00:08:33.073339', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (860, NULL, 'partial_assets', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (861, NULL, 'not_managed_assets', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (862, NULL, 'assets_without_risks', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (863, 15082, 'total_asset_impact', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (864, 15082, 'control_amount_by_asset', '2009-07-16 00:08:33.073339', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (865, 15082, 'control_cost_by_asset', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (866, 15084, 'risk_value', '2009-07-16 00:08:33.073339', 5);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (867, 15084, 'risk_residual_value', '2009-07-16 00:08:33.073339', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (868, 15084, 'control_amount_by_risk', '2009-07-16 00:08:33.073339', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (869, NULL, 'risk_total', '2009-07-16 00:08:33.073339', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (870, NULL, 'risks_with_high_real_risk', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (871, NULL, 'risks_with_high_residual_risk', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (872, NULL, 'risks_with_medium_real_risk', '2009-07-16 00:08:33.073339', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (873, NULL, 'risks_with_medium_residual_risk', '2009-07-16 00:08:33.073339', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (874, NULL, 'risks_with_low_real_risk', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (875, NULL, 'risks_with_low_residual_risk', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (876, NULL, 'risks_with_no_value_risk', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (877, NULL, 'risks_treated', '2009-07-16 00:08:33.073339', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (878, NULL, 'risks_not_treated', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (879, NULL, 'risks_accepted', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (880, NULL, 'risks_transfered', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (881, NULL, 'risks_avoided', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (882, 15089, 'risk_amount_by_control', '2009-07-16 00:08:33.073339', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (883, NULL, 'control_total', '2009-07-16 00:08:33.073339', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (884, NULL, 'planned_implantation_controls', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (885, NULL, 'late_implantation_controls', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (886, NULL, 'adequate_implemented_controls', '2009-07-16 00:08:33.073339', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (887, NULL, 'inadequate_implemented_controls', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (888, NULL, 'not_measured_implemented_controls', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (889, NULL, 'tested_ok_controls', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (890, NULL, 'tested_not_ok_controls', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (891, NULL, 'revision_ok_controls', '2009-07-16 00:08:33.073339', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (892, NULL, 'revision_not_ok_controls', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (893, NULL, 'controls_without_risks', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (894, 15049, 'best_practice_amount_by_standard', '2009-07-16 00:08:33.073339', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (895, 15048, 'best_practice_amount_by_standard', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (896, 15047, 'best_practice_amount_by_standard', '2009-07-16 00:08:33.073339', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (897, 15046, 'best_practice_amount_by_standard', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (898, 15045, 'best_practice_amount_by_standard', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (899, 15049, 'applied_best_practice_amount_by_standard', '2009-07-16 00:08:33.073339', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (900, 15048, 'applied_best_practice_amount_by_standard', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (901, 15047, 'applied_best_practice_amount_by_standard', '2009-07-16 00:08:33.073339', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (902, 15046, 'applied_best_practice_amount_by_standard', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (903, 15045, 'applied_best_practice_amount_by_standard', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (904, NULL, 'asset_total_cost', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (905, NULL, 'control_total_cost1', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (906, NULL, 'control_total_cost2', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (907, NULL, 'control_total_cost3', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (908, NULL, 'control_total_cost4', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (909, NULL, 'control_total_cost5', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (910, NULL, 'potentially_low_risk_cost', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (911, NULL, 'mitigated_risk_cost', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (912, NULL, 'avoided_risk_cost', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (913, NULL, 'transfered_risk_cost', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (914, NULL, 'accepted_risk_cost', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (915, NULL, 'not_treated_and_medium_high_risk', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (916, NULL, 'document_total', '2009-07-16 00:08:33.073339', 3);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (917, NULL, 'documents_by_state_approved', '2009-07-16 00:08:33.073339', 3);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (918, NULL, 'documents_by_state_developing', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (919, NULL, 'documents_by_state_pendant', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (920, NULL, 'documents_by_type_area', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (921, NULL, 'documents_by_type_asset', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (922, NULL, 'documents_by_type_control', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (923, NULL, 'documents_by_type_management', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (924, NULL, 'documents_by_type_none', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (925, NULL, 'documents_by_type_others', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (926, NULL, 'documents_by_type_policy', '2009-07-16 00:08:33.073339', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (927, NULL, 'documents_by_type_process', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (928, NULL, 'documents_by_type_scope', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (929, NULL, 'register_total', '2009-07-16 00:08:33.073339', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (930, NULL, 'documents_read', '2009-07-16 00:08:33.073339', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (931, NULL, 'documents_not_read', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (932, NULL, 'documents_scheduled', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (933, NULL, 'occupation_documents', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (934, NULL, 'occupation_template', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (935, NULL, 'occupation_registers', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (936, NULL, 'occupation_total', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (937, NULL, 'incident_total', '2009-07-16 00:08:33.073339', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (938, NULL, 'non_conformity_total', '2009-07-16 00:08:33.073339', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (939, NULL, 'incidents_by_state_directed', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (940, NULL, 'incidents_by_state_open', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (941, NULL, 'incidents_by_state_pendant_disposal', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (942, NULL, 'incidents_by_state_pendant_solution', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (943, NULL, 'incidents_by_state_solved', '2009-07-16 00:08:33.073339', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (944, NULL, 'incidents_by_state_waiting_solution', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (945, NULL, 'nc_by_state_ci_ap_pendant', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (946, NULL, 'nc_by_state_ci_closed', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (947, NULL, 'nc_by_state_ci_directed', '2009-07-16 00:08:33.073339', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (948, NULL, 'nc_by_state_ci_finished', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (949, NULL, 'nc_by_state_ci_nc_pendant', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (950, NULL, 'nc_by_state_ci_open', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (951, NULL, 'nc_by_state_ci_sent', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (952, NULL, 'nc_by_state_ci_waiting_conclusion', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (953, NULL, 'nc_by_state_denied', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (954, NULL, 'ap_total', '2009-07-16 00:08:33.073339', 2);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (955, NULL, 'ap_finished', '2009-07-16 00:08:33.073339', 2);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (956, NULL, 'ap_finished_late', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (957, NULL, 'ap_efficient', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (958, NULL, 'ap_non_efficient', '2009-07-16 00:08:33.073339', 2);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (959, NULL, 'nc_per_ap_average', '2009-07-16 00:08:33.073339', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (960, 15050, 'area_value', '2009-07-17 00:07:59.722997', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (961, NULL, 'areas_total', '2009-07-17 00:07:59.722997', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (962, NULL, 'areas_with_high_risk', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (963, NULL, 'areas_with_medium_risk', '2009-07-17 00:07:59.722997', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (964, NULL, 'areas_with_low_risk', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (965, NULL, 'areas_with_no_value_risk', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (966, NULL, 'complete_areas', '2009-07-17 00:07:59.722997', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (967, NULL, 'partial_areas', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (968, NULL, 'not_managed_areas', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (969, NULL, 'areas_without_processes', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (970, 15050, 'risk_amount_by_area', '2009-07-17 00:07:59.722997', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (971, 15050, 'total_area_impact', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (972, 15050, 'control_amount_by_area', '2009-07-17 00:07:59.722997', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (973, 15050, 'control_cost_by_area', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (974, 15050, 'process_amount_by_area', '2009-07-17 00:07:59.722997', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (975, 15050, 'asset_amount_by_area', '2009-07-17 00:07:59.722997', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (976, 15050, 'asset_cost_by_area', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (977, 15051, 'process_value', '2009-07-17 00:07:59.722997', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (978, 15051, 'asset_amount_by_process', '2009-07-17 00:07:59.722997', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (979, 15051, 'asset_cost_by_process', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (980, 15051, 'risk_amount_by_process', '2009-07-17 00:07:59.722997', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (981, NULL, 'process_total', '2009-07-17 00:07:59.722997', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (982, NULL, 'processes_with_high_risk', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (983, NULL, 'processes_with_medium_risk', '2009-07-17 00:07:59.722997', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (984, NULL, 'processes_with_low_risk', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (985, NULL, 'processes_with_no_value_risk', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (986, NULL, 'complete_processes', '2009-07-17 00:07:59.722997', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (987, NULL, 'partial_processes', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (988, NULL, 'not_managed_processes', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (989, NULL, 'processes_without_assets', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (990, 15051, 'total_process_impact', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (991, 15051, 'control_amount_by_process', '2009-07-17 00:07:59.722997', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (992, 15051, 'control_cost_by_process', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (993, 15082, 'asset_value', '2009-07-17 00:07:59.722997', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (994, 15082, 'process_amount_by_asset', '2009-07-17 00:07:59.722997', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (995, 15082, 'risk_amount_by_asset', '2009-07-17 00:07:59.722997', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (996, NULL, 'asset_total', '2009-07-17 00:07:59.722997', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (997, NULL, 'assets_with_high_risk', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (998, NULL, 'assets_with_medium_risk', '2009-07-17 00:07:59.722997', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (999, NULL, 'assets_with_low_risk', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1000, NULL, 'assets_with_no_value_risk', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1001, NULL, 'complete_assets', '2009-07-17 00:07:59.722997', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1002, NULL, 'partial_assets', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1003, NULL, 'not_managed_assets', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1004, NULL, 'assets_without_risks', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1005, 15082, 'total_asset_impact', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1006, 15082, 'control_amount_by_asset', '2009-07-17 00:07:59.722997', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1007, 15082, 'control_cost_by_asset', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1008, 15084, 'risk_value', '2009-07-17 00:07:59.722997', 5);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1009, 15084, 'risk_residual_value', '2009-07-17 00:07:59.722997', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1010, 15084, 'control_amount_by_risk', '2009-07-17 00:07:59.722997', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1011, NULL, 'risk_total', '2009-07-17 00:07:59.722997', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1012, NULL, 'risks_with_high_real_risk', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1013, NULL, 'risks_with_high_residual_risk', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1014, NULL, 'risks_with_medium_real_risk', '2009-07-17 00:07:59.722997', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1015, NULL, 'risks_with_medium_residual_risk', '2009-07-17 00:07:59.722997', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1016, NULL, 'risks_with_low_real_risk', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1017, NULL, 'risks_with_low_residual_risk', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1018, NULL, 'risks_with_no_value_risk', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1019, NULL, 'risks_treated', '2009-07-17 00:07:59.722997', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1020, NULL, 'risks_not_treated', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1021, NULL, 'risks_accepted', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1022, NULL, 'risks_transfered', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1023, NULL, 'risks_avoided', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1024, 15089, 'risk_amount_by_control', '2009-07-17 00:07:59.722997', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1025, NULL, 'control_total', '2009-07-17 00:07:59.722997', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1026, NULL, 'planned_implantation_controls', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1027, NULL, 'late_implantation_controls', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1028, NULL, 'adequate_implemented_controls', '2009-07-17 00:07:59.722997', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1029, NULL, 'inadequate_implemented_controls', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1030, NULL, 'not_measured_implemented_controls', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1031, NULL, 'tested_ok_controls', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1032, NULL, 'tested_not_ok_controls', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1033, NULL, 'revision_ok_controls', '2009-07-17 00:07:59.722997', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1034, NULL, 'revision_not_ok_controls', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1035, NULL, 'controls_without_risks', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1036, 15049, 'best_practice_amount_by_standard', '2009-07-17 00:07:59.722997', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1037, 15048, 'best_practice_amount_by_standard', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1038, 15047, 'best_practice_amount_by_standard', '2009-07-17 00:07:59.722997', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1039, 15046, 'best_practice_amount_by_standard', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1040, 15045, 'best_practice_amount_by_standard', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1041, 15049, 'applied_best_practice_amount_by_standard', '2009-07-17 00:07:59.722997', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1042, 15048, 'applied_best_practice_amount_by_standard', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1043, 15047, 'applied_best_practice_amount_by_standard', '2009-07-17 00:07:59.722997', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1044, 15046, 'applied_best_practice_amount_by_standard', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1045, 15045, 'applied_best_practice_amount_by_standard', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1046, NULL, 'asset_total_cost', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1047, NULL, 'control_total_cost1', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1048, NULL, 'control_total_cost2', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1049, NULL, 'control_total_cost3', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1050, NULL, 'control_total_cost4', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1051, NULL, 'control_total_cost5', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1052, NULL, 'potentially_low_risk_cost', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1053, NULL, 'mitigated_risk_cost', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1054, NULL, 'avoided_risk_cost', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1055, NULL, 'transfered_risk_cost', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1056, NULL, 'accepted_risk_cost', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1057, NULL, 'not_treated_and_medium_high_risk', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1058, NULL, 'document_total', '2009-07-17 00:07:59.722997', 3);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1059, NULL, 'documents_by_state_approved', '2009-07-17 00:07:59.722997', 3);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1060, NULL, 'documents_by_state_developing', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1061, NULL, 'documents_by_state_pendant', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1062, NULL, 'documents_by_type_area', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1063, NULL, 'documents_by_type_asset', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1064, NULL, 'documents_by_type_control', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1065, NULL, 'documents_by_type_management', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1066, NULL, 'documents_by_type_none', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1067, NULL, 'documents_by_type_others', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1068, NULL, 'documents_by_type_policy', '2009-07-17 00:07:59.722997', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1069, NULL, 'documents_by_type_process', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1070, NULL, 'documents_by_type_scope', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1071, NULL, 'register_total', '2009-07-17 00:07:59.722997', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1072, NULL, 'documents_read', '2009-07-17 00:07:59.722997', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1073, NULL, 'documents_not_read', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1074, NULL, 'documents_scheduled', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1075, NULL, 'occupation_documents', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1076, NULL, 'occupation_template', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1077, NULL, 'occupation_registers', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1078, NULL, 'occupation_total', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1079, NULL, 'incident_total', '2009-07-17 00:07:59.722997', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1080, NULL, 'non_conformity_total', '2009-07-17 00:07:59.722997', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1081, NULL, 'incidents_by_state_directed', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1082, NULL, 'incidents_by_state_open', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1083, NULL, 'incidents_by_state_pendant_disposal', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1084, NULL, 'incidents_by_state_pendant_solution', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1085, NULL, 'incidents_by_state_solved', '2009-07-17 00:07:59.722997', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1086, NULL, 'incidents_by_state_waiting_solution', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1087, NULL, 'nc_by_state_ci_ap_pendant', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1088, NULL, 'nc_by_state_ci_closed', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1089, NULL, 'nc_by_state_ci_directed', '2009-07-17 00:07:59.722997', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1090, NULL, 'nc_by_state_ci_finished', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1091, NULL, 'nc_by_state_ci_nc_pendant', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1092, NULL, 'nc_by_state_ci_open', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1093, NULL, 'nc_by_state_ci_sent', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1094, NULL, 'nc_by_state_ci_waiting_conclusion', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1095, NULL, 'nc_by_state_denied', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1096, NULL, 'ap_total', '2009-07-17 00:07:59.722997', 2);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1097, NULL, 'ap_finished', '2009-07-17 00:07:59.722997', 2);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1098, NULL, 'ap_finished_late', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1099, NULL, 'ap_efficient', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1100, NULL, 'ap_non_efficient', '2009-07-17 00:07:59.722997', 2);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1101, NULL, 'nc_per_ap_average', '2009-07-17 00:07:59.722997', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1102, 15050, 'area_value', '2009-07-18 00:07:57.037839', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1103, NULL, 'areas_total', '2009-07-18 00:07:57.037839', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1104, NULL, 'areas_with_high_risk', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1105, NULL, 'areas_with_medium_risk', '2009-07-18 00:07:57.037839', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1106, NULL, 'areas_with_low_risk', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1107, NULL, 'areas_with_no_value_risk', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1108, NULL, 'complete_areas', '2009-07-18 00:07:57.037839', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1109, NULL, 'partial_areas', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1110, NULL, 'not_managed_areas', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1111, NULL, 'areas_without_processes', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1112, 15050, 'risk_amount_by_area', '2009-07-18 00:07:57.037839', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1113, 15050, 'total_area_impact', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1114, 15050, 'control_amount_by_area', '2009-07-18 00:07:57.037839', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1115, 15050, 'control_cost_by_area', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1116, 15050, 'process_amount_by_area', '2009-07-18 00:07:57.037839', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1117, 15050, 'asset_amount_by_area', '2009-07-18 00:07:57.037839', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1118, 15050, 'asset_cost_by_area', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1119, 15051, 'process_value', '2009-07-18 00:07:57.037839', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1120, 15051, 'asset_amount_by_process', '2009-07-18 00:07:57.037839', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1121, 15051, 'asset_cost_by_process', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1122, 15051, 'risk_amount_by_process', '2009-07-18 00:07:57.037839', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1123, NULL, 'process_total', '2009-07-18 00:07:57.037839', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1124, NULL, 'processes_with_high_risk', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1125, NULL, 'processes_with_medium_risk', '2009-07-18 00:07:57.037839', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1126, NULL, 'processes_with_low_risk', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1127, NULL, 'processes_with_no_value_risk', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1128, NULL, 'complete_processes', '2009-07-18 00:07:57.037839', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1129, NULL, 'partial_processes', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1130, NULL, 'not_managed_processes', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1131, NULL, 'processes_without_assets', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1132, 15051, 'total_process_impact', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1133, 15051, 'control_amount_by_process', '2009-07-18 00:07:57.037839', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1134, 15051, 'control_cost_by_process', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1135, 15082, 'asset_value', '2009-07-18 00:07:57.037839', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1136, 15082, 'process_amount_by_asset', '2009-07-18 00:07:57.037839', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1137, 15082, 'risk_amount_by_asset', '2009-07-18 00:07:57.037839', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1138, NULL, 'asset_total', '2009-07-18 00:07:57.037839', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1139, NULL, 'assets_with_high_risk', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1140, NULL, 'assets_with_medium_risk', '2009-07-18 00:07:57.037839', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1141, NULL, 'assets_with_low_risk', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1142, NULL, 'assets_with_no_value_risk', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1143, NULL, 'complete_assets', '2009-07-18 00:07:57.037839', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1144, NULL, 'partial_assets', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1145, NULL, 'not_managed_assets', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1146, NULL, 'assets_without_risks', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1147, 15082, 'total_asset_impact', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1148, 15082, 'control_amount_by_asset', '2009-07-18 00:07:57.037839', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1149, 15082, 'control_cost_by_asset', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1150, 15084, 'risk_value', '2009-07-18 00:07:57.037839', 5);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1151, 15084, 'risk_residual_value', '2009-07-18 00:07:57.037839', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1152, 15084, 'control_amount_by_risk', '2009-07-18 00:07:57.037839', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1153, NULL, 'risk_total', '2009-07-18 00:07:57.037839', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1154, NULL, 'risks_with_high_real_risk', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1155, NULL, 'risks_with_high_residual_risk', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1156, NULL, 'risks_with_medium_real_risk', '2009-07-18 00:07:57.037839', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1157, NULL, 'risks_with_medium_residual_risk', '2009-07-18 00:07:57.037839', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1158, NULL, 'risks_with_low_real_risk', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1159, NULL, 'risks_with_low_residual_risk', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1160, NULL, 'risks_with_no_value_risk', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1161, NULL, 'risks_treated', '2009-07-18 00:07:57.037839', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1162, NULL, 'risks_not_treated', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1163, NULL, 'risks_accepted', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1164, NULL, 'risks_transfered', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1165, NULL, 'risks_avoided', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1166, 15089, 'risk_amount_by_control', '2009-07-18 00:07:57.037839', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1167, NULL, 'control_total', '2009-07-18 00:07:57.037839', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1168, NULL, 'planned_implantation_controls', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1169, NULL, 'late_implantation_controls', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1170, NULL, 'adequate_implemented_controls', '2009-07-18 00:07:57.037839', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1171, NULL, 'inadequate_implemented_controls', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1172, NULL, 'not_measured_implemented_controls', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1173, NULL, 'tested_ok_controls', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1174, NULL, 'tested_not_ok_controls', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1175, NULL, 'revision_ok_controls', '2009-07-18 00:07:57.037839', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1176, NULL, 'revision_not_ok_controls', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1177, NULL, 'controls_without_risks', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1178, 15049, 'best_practice_amount_by_standard', '2009-07-18 00:07:57.037839', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1179, 15048, 'best_practice_amount_by_standard', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1180, 15047, 'best_practice_amount_by_standard', '2009-07-18 00:07:57.037839', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1181, 15046, 'best_practice_amount_by_standard', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1182, 15045, 'best_practice_amount_by_standard', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1183, 15049, 'applied_best_practice_amount_by_standard', '2009-07-18 00:07:57.037839', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1184, 15048, 'applied_best_practice_amount_by_standard', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1185, 15047, 'applied_best_practice_amount_by_standard', '2009-07-18 00:07:57.037839', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1186, 15046, 'applied_best_practice_amount_by_standard', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1187, 15045, 'applied_best_practice_amount_by_standard', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1188, NULL, 'asset_total_cost', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1189, NULL, 'control_total_cost1', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1190, NULL, 'control_total_cost2', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1191, NULL, 'control_total_cost3', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1192, NULL, 'control_total_cost4', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1193, NULL, 'control_total_cost5', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1194, NULL, 'potentially_low_risk_cost', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1195, NULL, 'mitigated_risk_cost', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1196, NULL, 'avoided_risk_cost', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1197, NULL, 'transfered_risk_cost', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1198, NULL, 'accepted_risk_cost', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1199, NULL, 'not_treated_and_medium_high_risk', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1200, NULL, 'document_total', '2009-07-18 00:07:57.037839', 3);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1201, NULL, 'documents_by_state_approved', '2009-07-18 00:07:57.037839', 3);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1202, NULL, 'documents_by_state_developing', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1203, NULL, 'documents_by_state_pendant', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1204, NULL, 'documents_by_type_area', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1205, NULL, 'documents_by_type_asset', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1206, NULL, 'documents_by_type_control', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1207, NULL, 'documents_by_type_management', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1208, NULL, 'documents_by_type_none', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1209, NULL, 'documents_by_type_others', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1210, NULL, 'documents_by_type_policy', '2009-07-18 00:07:57.037839', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1211, NULL, 'documents_by_type_process', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1212, NULL, 'documents_by_type_scope', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1213, NULL, 'register_total', '2009-07-18 00:07:57.037839', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1214, NULL, 'documents_read', '2009-07-18 00:07:57.037839', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1215, NULL, 'documents_not_read', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1216, NULL, 'documents_scheduled', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1217, NULL, 'occupation_documents', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1218, NULL, 'occupation_template', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1219, NULL, 'occupation_registers', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1220, NULL, 'occupation_total', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1221, NULL, 'incident_total', '2009-07-18 00:07:57.037839', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1222, NULL, 'non_conformity_total', '2009-07-18 00:07:57.037839', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1223, NULL, 'incidents_by_state_directed', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1224, NULL, 'incidents_by_state_open', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1225, NULL, 'incidents_by_state_pendant_disposal', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1226, NULL, 'incidents_by_state_pendant_solution', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1227, NULL, 'incidents_by_state_solved', '2009-07-18 00:07:57.037839', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1228, NULL, 'incidents_by_state_waiting_solution', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1229, NULL, 'nc_by_state_ci_ap_pendant', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1230, NULL, 'nc_by_state_ci_closed', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1231, NULL, 'nc_by_state_ci_directed', '2009-07-18 00:07:57.037839', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1232, NULL, 'nc_by_state_ci_finished', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1233, NULL, 'nc_by_state_ci_nc_pendant', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1234, NULL, 'nc_by_state_ci_open', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1235, NULL, 'nc_by_state_ci_sent', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1236, NULL, 'nc_by_state_ci_waiting_conclusion', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1237, NULL, 'nc_by_state_denied', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1238, NULL, 'ap_total', '2009-07-18 00:07:57.037839', 2);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1239, NULL, 'ap_finished', '2009-07-18 00:07:57.037839', 2);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1240, NULL, 'ap_finished_late', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1241, NULL, 'ap_efficient', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1242, NULL, 'ap_non_efficient', '2009-07-18 00:07:57.037839', 2);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1243, NULL, 'nc_per_ap_average', '2009-07-18 00:07:57.037839', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1244, 15050, 'area_value', '2009-07-19 00:07:50.635565', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1245, NULL, 'areas_total', '2009-07-19 00:07:50.635565', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1246, NULL, 'areas_with_high_risk', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1247, NULL, 'areas_with_medium_risk', '2009-07-19 00:07:50.635565', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1248, NULL, 'areas_with_low_risk', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1249, NULL, 'areas_with_no_value_risk', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1250, NULL, 'complete_areas', '2009-07-19 00:07:50.635565', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1251, NULL, 'partial_areas', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1252, NULL, 'not_managed_areas', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1253, NULL, 'areas_without_processes', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1254, 15050, 'risk_amount_by_area', '2009-07-19 00:07:50.635565', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1255, 15050, 'total_area_impact', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1256, 15050, 'control_amount_by_area', '2009-07-19 00:07:50.635565', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1257, 15050, 'control_cost_by_area', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1258, 15050, 'process_amount_by_area', '2009-07-19 00:07:50.635565', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1259, 15050, 'asset_amount_by_area', '2009-07-19 00:07:50.635565', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1260, 15050, 'asset_cost_by_area', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1261, 15051, 'process_value', '2009-07-19 00:07:50.635565', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1262, 15051, 'asset_amount_by_process', '2009-07-19 00:07:50.635565', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1263, 15051, 'asset_cost_by_process', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1264, 15051, 'risk_amount_by_process', '2009-07-19 00:07:50.635565', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1265, NULL, 'process_total', '2009-07-19 00:07:50.635565', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1266, NULL, 'processes_with_high_risk', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1267, NULL, 'processes_with_medium_risk', '2009-07-19 00:07:50.635565', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1268, NULL, 'processes_with_low_risk', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1269, NULL, 'processes_with_no_value_risk', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1270, NULL, 'complete_processes', '2009-07-19 00:07:50.635565', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1271, NULL, 'partial_processes', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1272, NULL, 'not_managed_processes', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1273, NULL, 'processes_without_assets', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1274, 15051, 'total_process_impact', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1275, 15051, 'control_amount_by_process', '2009-07-19 00:07:50.635565', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1276, 15051, 'control_cost_by_process', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1277, 15082, 'asset_value', '2009-07-19 00:07:50.635565', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1278, 15082, 'process_amount_by_asset', '2009-07-19 00:07:50.635565', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1279, 15082, 'risk_amount_by_asset', '2009-07-19 00:07:50.635565', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1280, NULL, 'asset_total', '2009-07-19 00:07:50.635565', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1281, NULL, 'assets_with_high_risk', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1282, NULL, 'assets_with_medium_risk', '2009-07-19 00:07:50.635565', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1283, NULL, 'assets_with_low_risk', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1284, NULL, 'assets_with_no_value_risk', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1285, NULL, 'complete_assets', '2009-07-19 00:07:50.635565', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1286, NULL, 'partial_assets', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1287, NULL, 'not_managed_assets', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1288, NULL, 'assets_without_risks', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1289, 15082, 'total_asset_impact', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1290, 15082, 'control_amount_by_asset', '2009-07-19 00:07:50.635565', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1291, 15082, 'control_cost_by_asset', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1292, 15084, 'risk_value', '2009-07-19 00:07:50.635565', 5);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1293, 15084, 'risk_residual_value', '2009-07-19 00:07:50.635565', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1294, 15084, 'control_amount_by_risk', '2009-07-19 00:07:50.635565', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1295, NULL, 'risk_total', '2009-07-19 00:07:50.635565', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1296, NULL, 'risks_with_high_real_risk', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1297, NULL, 'risks_with_high_residual_risk', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1298, NULL, 'risks_with_medium_real_risk', '2009-07-19 00:07:50.635565', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1299, NULL, 'risks_with_medium_residual_risk', '2009-07-19 00:07:50.635565', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1300, NULL, 'risks_with_low_real_risk', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1301, NULL, 'risks_with_low_residual_risk', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1302, NULL, 'risks_with_no_value_risk', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1303, NULL, 'risks_treated', '2009-07-19 00:07:50.635565', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1304, NULL, 'risks_not_treated', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1305, NULL, 'risks_accepted', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1306, NULL, 'risks_transfered', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1307, NULL, 'risks_avoided', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1308, 15089, 'risk_amount_by_control', '2009-07-19 00:07:50.635565', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1309, NULL, 'control_total', '2009-07-19 00:07:50.635565', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1310, NULL, 'planned_implantation_controls', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1311, NULL, 'late_implantation_controls', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1312, NULL, 'adequate_implemented_controls', '2009-07-19 00:07:50.635565', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1313, NULL, 'inadequate_implemented_controls', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1314, NULL, 'not_measured_implemented_controls', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1315, NULL, 'tested_ok_controls', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1316, NULL, 'tested_not_ok_controls', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1317, NULL, 'revision_ok_controls', '2009-07-19 00:07:50.635565', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1318, NULL, 'revision_not_ok_controls', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1319, NULL, 'controls_without_risks', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1320, 15049, 'best_practice_amount_by_standard', '2009-07-19 00:07:50.635565', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1321, 15048, 'best_practice_amount_by_standard', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1322, 15047, 'best_practice_amount_by_standard', '2009-07-19 00:07:50.635565', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1323, 15046, 'best_practice_amount_by_standard', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1324, 15045, 'best_practice_amount_by_standard', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1325, 15049, 'applied_best_practice_amount_by_standard', '2009-07-19 00:07:50.635565', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1326, 15048, 'applied_best_practice_amount_by_standard', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1327, 15047, 'applied_best_practice_amount_by_standard', '2009-07-19 00:07:50.635565', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1328, 15046, 'applied_best_practice_amount_by_standard', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1329, 15045, 'applied_best_practice_amount_by_standard', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1330, NULL, 'asset_total_cost', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1331, NULL, 'control_total_cost1', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1332, NULL, 'control_total_cost2', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1333, NULL, 'control_total_cost3', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1334, NULL, 'control_total_cost4', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1335, NULL, 'control_total_cost5', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1336, NULL, 'potentially_low_risk_cost', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1337, NULL, 'mitigated_risk_cost', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1338, NULL, 'avoided_risk_cost', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1339, NULL, 'transfered_risk_cost', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1340, NULL, 'accepted_risk_cost', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1341, NULL, 'not_treated_and_medium_high_risk', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1342, NULL, 'document_total', '2009-07-19 00:07:50.635565', 3);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1343, NULL, 'documents_by_state_approved', '2009-07-19 00:07:50.635565', 3);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1344, NULL, 'documents_by_state_developing', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1345, NULL, 'documents_by_state_pendant', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1346, NULL, 'documents_by_type_area', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1347, NULL, 'documents_by_type_asset', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1348, NULL, 'documents_by_type_control', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1349, NULL, 'documents_by_type_management', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1350, NULL, 'documents_by_type_none', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1351, NULL, 'documents_by_type_others', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1352, NULL, 'documents_by_type_policy', '2009-07-19 00:07:50.635565', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1353, NULL, 'documents_by_type_process', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1354, NULL, 'documents_by_type_scope', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1355, NULL, 'register_total', '2009-07-19 00:07:50.635565', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1356, NULL, 'documents_read', '2009-07-19 00:07:50.635565', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1357, NULL, 'documents_not_read', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1358, NULL, 'documents_scheduled', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1359, NULL, 'occupation_documents', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1360, NULL, 'occupation_template', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1361, NULL, 'occupation_registers', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1362, NULL, 'occupation_total', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1363, NULL, 'incident_total', '2009-07-19 00:07:50.635565', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1364, NULL, 'non_conformity_total', '2009-07-19 00:07:50.635565', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1365, NULL, 'incidents_by_state_directed', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1366, NULL, 'incidents_by_state_open', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1367, NULL, 'incidents_by_state_pendant_disposal', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1368, NULL, 'incidents_by_state_pendant_solution', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1369, NULL, 'incidents_by_state_solved', '2009-07-19 00:07:50.635565', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1370, NULL, 'incidents_by_state_waiting_solution', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1371, NULL, 'nc_by_state_ci_ap_pendant', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1372, NULL, 'nc_by_state_ci_closed', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1373, NULL, 'nc_by_state_ci_directed', '2009-07-19 00:07:50.635565', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1374, NULL, 'nc_by_state_ci_finished', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1375, NULL, 'nc_by_state_ci_nc_pendant', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1376, NULL, 'nc_by_state_ci_open', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1377, NULL, 'nc_by_state_ci_sent', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1378, NULL, 'nc_by_state_ci_waiting_conclusion', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1379, NULL, 'nc_by_state_denied', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1380, NULL, 'ap_total', '2009-07-19 00:07:50.635565', 2);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1381, NULL, 'ap_finished', '2009-07-19 00:07:50.635565', 2);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1382, NULL, 'ap_finished_late', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1383, NULL, 'ap_efficient', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1384, NULL, 'ap_non_efficient', '2009-07-19 00:07:50.635565', 2);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1385, NULL, 'nc_per_ap_average', '2009-07-19 00:07:50.635565', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1386, 15050, 'area_value', '2009-07-20 00:08:15.238195', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1387, NULL, 'areas_total', '2009-07-20 00:08:15.238195', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1388, NULL, 'areas_with_high_risk', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1389, NULL, 'areas_with_medium_risk', '2009-07-20 00:08:15.238195', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1390, NULL, 'areas_with_low_risk', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1391, NULL, 'areas_with_no_value_risk', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1392, NULL, 'complete_areas', '2009-07-20 00:08:15.238195', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1393, NULL, 'partial_areas', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1394, NULL, 'not_managed_areas', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1395, NULL, 'areas_without_processes', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1396, 15050, 'risk_amount_by_area', '2009-07-20 00:08:15.238195', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1397, 15050, 'total_area_impact', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1398, 15050, 'control_amount_by_area', '2009-07-20 00:08:15.238195', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1399, 15050, 'control_cost_by_area', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1400, 15050, 'process_amount_by_area', '2009-07-20 00:08:15.238195', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1401, 15050, 'asset_amount_by_area', '2009-07-20 00:08:15.238195', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1402, 15050, 'asset_cost_by_area', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1403, 15051, 'process_value', '2009-07-20 00:08:15.238195', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1404, 15051, 'asset_amount_by_process', '2009-07-20 00:08:15.238195', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1405, 15051, 'asset_cost_by_process', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1406, 15051, 'risk_amount_by_process', '2009-07-20 00:08:15.238195', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1407, NULL, 'process_total', '2009-07-20 00:08:15.238195', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1408, NULL, 'processes_with_high_risk', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1409, NULL, 'processes_with_medium_risk', '2009-07-20 00:08:15.238195', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1410, NULL, 'processes_with_low_risk', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1411, NULL, 'processes_with_no_value_risk', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1412, NULL, 'complete_processes', '2009-07-20 00:08:15.238195', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1413, NULL, 'partial_processes', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1414, NULL, 'not_managed_processes', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1415, NULL, 'processes_without_assets', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1416, 15051, 'total_process_impact', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1417, 15051, 'control_amount_by_process', '2009-07-20 00:08:15.238195', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1418, 15051, 'control_cost_by_process', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1419, 15082, 'asset_value', '2009-07-20 00:08:15.238195', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1420, 15082, 'process_amount_by_asset', '2009-07-20 00:08:15.238195', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1421, 15082, 'risk_amount_by_asset', '2009-07-20 00:08:15.238195', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1422, NULL, 'asset_total', '2009-07-20 00:08:15.238195', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1423, NULL, 'assets_with_high_risk', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1424, NULL, 'assets_with_medium_risk', '2009-07-20 00:08:15.238195', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1425, NULL, 'assets_with_low_risk', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1426, NULL, 'assets_with_no_value_risk', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1427, NULL, 'complete_assets', '2009-07-20 00:08:15.238195', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1428, NULL, 'partial_assets', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1429, NULL, 'not_managed_assets', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1430, NULL, 'assets_without_risks', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1431, 15082, 'total_asset_impact', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1432, 15082, 'control_amount_by_asset', '2009-07-20 00:08:15.238195', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1433, 15082, 'control_cost_by_asset', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1434, 15084, 'risk_value', '2009-07-20 00:08:15.238195', 5);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1435, 15084, 'risk_residual_value', '2009-07-20 00:08:15.238195', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1436, 15084, 'control_amount_by_risk', '2009-07-20 00:08:15.238195', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1437, NULL, 'risk_total', '2009-07-20 00:08:15.238195', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1438, NULL, 'risks_with_high_real_risk', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1439, NULL, 'risks_with_high_residual_risk', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1440, NULL, 'risks_with_medium_real_risk', '2009-07-20 00:08:15.238195', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1441, NULL, 'risks_with_medium_residual_risk', '2009-07-20 00:08:15.238195', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1442, NULL, 'risks_with_low_real_risk', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1443, NULL, 'risks_with_low_residual_risk', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1444, NULL, 'risks_with_no_value_risk', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1445, NULL, 'risks_treated', '2009-07-20 00:08:15.238195', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1446, NULL, 'risks_not_treated', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1447, NULL, 'risks_accepted', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1448, NULL, 'risks_transfered', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1449, NULL, 'risks_avoided', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1450, 15089, 'risk_amount_by_control', '2009-07-20 00:08:15.238195', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1451, NULL, 'control_total', '2009-07-20 00:08:15.238195', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1452, NULL, 'planned_implantation_controls', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1453, NULL, 'late_implantation_controls', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1454, NULL, 'adequate_implemented_controls', '2009-07-20 00:08:15.238195', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1455, NULL, 'inadequate_implemented_controls', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1456, NULL, 'not_measured_implemented_controls', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1457, NULL, 'tested_ok_controls', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1458, NULL, 'tested_not_ok_controls', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1459, NULL, 'revision_ok_controls', '2009-07-20 00:08:15.238195', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1460, NULL, 'revision_not_ok_controls', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1461, NULL, 'controls_without_risks', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1462, 15049, 'best_practice_amount_by_standard', '2009-07-20 00:08:15.238195', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1463, 15048, 'best_practice_amount_by_standard', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1464, 15047, 'best_practice_amount_by_standard', '2009-07-20 00:08:15.238195', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1465, 15046, 'best_practice_amount_by_standard', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1466, 15045, 'best_practice_amount_by_standard', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1467, 15049, 'applied_best_practice_amount_by_standard', '2009-07-20 00:08:15.238195', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1468, 15048, 'applied_best_practice_amount_by_standard', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1469, 15047, 'applied_best_practice_amount_by_standard', '2009-07-20 00:08:15.238195', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1470, 15046, 'applied_best_practice_amount_by_standard', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1471, 15045, 'applied_best_practice_amount_by_standard', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1472, NULL, 'asset_total_cost', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1473, NULL, 'control_total_cost1', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1474, NULL, 'control_total_cost2', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1475, NULL, 'control_total_cost3', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1476, NULL, 'control_total_cost4', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1477, NULL, 'control_total_cost5', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1478, NULL, 'potentially_low_risk_cost', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1479, NULL, 'mitigated_risk_cost', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1480, NULL, 'avoided_risk_cost', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1481, NULL, 'transfered_risk_cost', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1482, NULL, 'accepted_risk_cost', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1483, NULL, 'not_treated_and_medium_high_risk', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1484, NULL, 'document_total', '2009-07-20 00:08:15.238195', 3);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1485, NULL, 'documents_by_state_approved', '2009-07-20 00:08:15.238195', 3);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1486, NULL, 'documents_by_state_developing', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1487, NULL, 'documents_by_state_pendant', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1488, NULL, 'documents_by_type_area', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1489, NULL, 'documents_by_type_asset', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1490, NULL, 'documents_by_type_control', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1491, NULL, 'documents_by_type_management', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1492, NULL, 'documents_by_type_none', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1493, NULL, 'documents_by_type_others', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1494, NULL, 'documents_by_type_policy', '2009-07-20 00:08:15.238195', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1495, NULL, 'documents_by_type_process', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1496, NULL, 'documents_by_type_scope', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1497, NULL, 'register_total', '2009-07-20 00:08:15.238195', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1498, NULL, 'documents_read', '2009-07-20 00:08:15.238195', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1499, NULL, 'documents_not_read', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1500, NULL, 'documents_scheduled', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1501, NULL, 'occupation_documents', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1502, NULL, 'occupation_template', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1503, NULL, 'occupation_registers', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1504, NULL, 'occupation_total', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1505, NULL, 'incident_total', '2009-07-20 00:08:15.238195', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1506, NULL, 'non_conformity_total', '2009-07-20 00:08:15.238195', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1507, NULL, 'incidents_by_state_directed', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1508, NULL, 'incidents_by_state_open', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1509, NULL, 'incidents_by_state_pendant_disposal', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1510, NULL, 'incidents_by_state_pendant_solution', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1511, NULL, 'incidents_by_state_solved', '2009-07-20 00:08:15.238195', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1512, NULL, 'incidents_by_state_waiting_solution', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1513, NULL, 'nc_by_state_ci_ap_pendant', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1514, NULL, 'nc_by_state_ci_closed', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1515, NULL, 'nc_by_state_ci_directed', '2009-07-20 00:08:15.238195', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1516, NULL, 'nc_by_state_ci_finished', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1517, NULL, 'nc_by_state_ci_nc_pendant', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1518, NULL, 'nc_by_state_ci_open', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1519, NULL, 'nc_by_state_ci_sent', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1520, NULL, 'nc_by_state_ci_waiting_conclusion', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1521, NULL, 'nc_by_state_denied', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1522, NULL, 'ap_total', '2009-07-20 00:08:15.238195', 2);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1523, NULL, 'ap_finished', '2009-07-20 00:08:15.238195', 2);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1524, NULL, 'ap_finished_late', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1525, NULL, 'ap_efficient', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1526, NULL, 'ap_non_efficient', '2009-07-20 00:08:15.238195', 2);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1527, NULL, 'nc_per_ap_average', '2009-07-20 00:08:15.238195', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1528, 15050, 'area_value', '2009-07-21 00:08:53.174123', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1529, NULL, 'areas_total', '2009-07-21 00:08:53.174123', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1530, NULL, 'areas_with_high_risk', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1531, NULL, 'areas_with_medium_risk', '2009-07-21 00:08:53.174123', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1532, NULL, 'areas_with_low_risk', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1533, NULL, 'areas_with_no_value_risk', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1534, NULL, 'complete_areas', '2009-07-21 00:08:53.174123', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1535, NULL, 'partial_areas', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1536, NULL, 'not_managed_areas', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1537, NULL, 'areas_without_processes', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1538, 15050, 'risk_amount_by_area', '2009-07-21 00:08:53.174123', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1539, 15050, 'total_area_impact', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1540, 15050, 'control_amount_by_area', '2009-07-21 00:08:53.174123', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1541, 15050, 'control_cost_by_area', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1542, 15050, 'process_amount_by_area', '2009-07-21 00:08:53.174123', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1543, 15050, 'asset_amount_by_area', '2009-07-21 00:08:53.174123', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1544, 15050, 'asset_cost_by_area', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1545, 15051, 'process_value', '2009-07-21 00:08:53.174123', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1546, 15051, 'asset_amount_by_process', '2009-07-21 00:08:53.174123', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1547, 15051, 'asset_cost_by_process', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1548, 15051, 'risk_amount_by_process', '2009-07-21 00:08:53.174123', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1549, NULL, 'process_total', '2009-07-21 00:08:53.174123', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1550, NULL, 'processes_with_high_risk', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1551, NULL, 'processes_with_medium_risk', '2009-07-21 00:08:53.174123', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1552, NULL, 'processes_with_low_risk', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1553, NULL, 'processes_with_no_value_risk', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1554, NULL, 'complete_processes', '2009-07-21 00:08:53.174123', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1555, NULL, 'partial_processes', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1556, NULL, 'not_managed_processes', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1557, NULL, 'processes_without_assets', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1558, 15051, 'total_process_impact', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1559, 15051, 'control_amount_by_process', '2009-07-21 00:08:53.174123', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1560, 15051, 'control_cost_by_process', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1561, 15082, 'asset_value', '2009-07-21 00:08:53.174123', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1562, 15082, 'process_amount_by_asset', '2009-07-21 00:08:53.174123', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1563, 15082, 'risk_amount_by_asset', '2009-07-21 00:08:53.174123', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1564, NULL, 'asset_total', '2009-07-21 00:08:53.174123', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1565, NULL, 'assets_with_high_risk', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1566, NULL, 'assets_with_medium_risk', '2009-07-21 00:08:53.174123', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1567, NULL, 'assets_with_low_risk', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1568, NULL, 'assets_with_no_value_risk', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1569, NULL, 'complete_assets', '2009-07-21 00:08:53.174123', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1570, NULL, 'partial_assets', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1571, NULL, 'not_managed_assets', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1572, NULL, 'assets_without_risks', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1573, 15082, 'total_asset_impact', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1574, 15082, 'control_amount_by_asset', '2009-07-21 00:08:53.174123', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1575, 15082, 'control_cost_by_asset', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1576, 15084, 'risk_value', '2009-07-21 00:08:53.174123', 5);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1577, 15084, 'risk_residual_value', '2009-07-21 00:08:53.174123', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1578, 15084, 'control_amount_by_risk', '2009-07-21 00:08:53.174123', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1579, NULL, 'risk_total', '2009-07-21 00:08:53.174123', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1580, NULL, 'risks_with_high_real_risk', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1581, NULL, 'risks_with_high_residual_risk', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1582, NULL, 'risks_with_medium_real_risk', '2009-07-21 00:08:53.174123', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1583, NULL, 'risks_with_medium_residual_risk', '2009-07-21 00:08:53.174123', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1584, NULL, 'risks_with_low_real_risk', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1585, NULL, 'risks_with_low_residual_risk', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1586, NULL, 'risks_with_no_value_risk', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1587, NULL, 'risks_treated', '2009-07-21 00:08:53.174123', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1588, NULL, 'risks_not_treated', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1589, NULL, 'risks_accepted', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1590, NULL, 'risks_transfered', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1591, NULL, 'risks_avoided', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1592, 15089, 'risk_amount_by_control', '2009-07-21 00:08:53.174123', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1593, NULL, 'control_total', '2009-07-21 00:08:53.174123', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1594, NULL, 'planned_implantation_controls', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1595, NULL, 'late_implantation_controls', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1596, NULL, 'adequate_implemented_controls', '2009-07-21 00:08:53.174123', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1597, NULL, 'inadequate_implemented_controls', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1598, NULL, 'not_measured_implemented_controls', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1599, NULL, 'tested_ok_controls', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1600, NULL, 'tested_not_ok_controls', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1601, NULL, 'revision_ok_controls', '2009-07-21 00:08:53.174123', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1602, NULL, 'revision_not_ok_controls', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1603, NULL, 'controls_without_risks', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1604, 15049, 'best_practice_amount_by_standard', '2009-07-21 00:08:53.174123', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1605, 15048, 'best_practice_amount_by_standard', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1606, 15047, 'best_practice_amount_by_standard', '2009-07-21 00:08:53.174123', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1607, 15046, 'best_practice_amount_by_standard', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1608, 15045, 'best_practice_amount_by_standard', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1609, 15049, 'applied_best_practice_amount_by_standard', '2009-07-21 00:08:53.174123', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1610, 15048, 'applied_best_practice_amount_by_standard', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1611, 15047, 'applied_best_practice_amount_by_standard', '2009-07-21 00:08:53.174123', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1612, 15046, 'applied_best_practice_amount_by_standard', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1613, 15045, 'applied_best_practice_amount_by_standard', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1614, NULL, 'asset_total_cost', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1615, NULL, 'control_total_cost1', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1616, NULL, 'control_total_cost2', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1617, NULL, 'control_total_cost3', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1618, NULL, 'control_total_cost4', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1619, NULL, 'control_total_cost5', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1620, NULL, 'potentially_low_risk_cost', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1621, NULL, 'mitigated_risk_cost', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1622, NULL, 'avoided_risk_cost', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1623, NULL, 'transfered_risk_cost', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1624, NULL, 'accepted_risk_cost', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1625, NULL, 'not_treated_and_medium_high_risk', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1626, NULL, 'document_total', '2009-07-21 00:08:53.174123', 3);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1627, NULL, 'documents_by_state_approved', '2009-07-21 00:08:53.174123', 3);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1628, NULL, 'documents_by_state_developing', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1629, NULL, 'documents_by_state_pendant', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1630, NULL, 'documents_by_type_area', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1631, NULL, 'documents_by_type_asset', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1632, NULL, 'documents_by_type_control', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1633, NULL, 'documents_by_type_management', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1634, NULL, 'documents_by_type_none', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1635, NULL, 'documents_by_type_others', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1636, NULL, 'documents_by_type_policy', '2009-07-21 00:08:53.174123', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1637, NULL, 'documents_by_type_process', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1638, NULL, 'documents_by_type_scope', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1639, NULL, 'register_total', '2009-07-21 00:08:53.174123', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1640, NULL, 'documents_read', '2009-07-21 00:08:53.174123', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1641, NULL, 'documents_not_read', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1642, NULL, 'documents_scheduled', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1643, NULL, 'occupation_documents', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1644, NULL, 'occupation_template', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1645, NULL, 'occupation_registers', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1646, NULL, 'occupation_total', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1647, NULL, 'incident_total', '2009-07-21 00:08:53.174123', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1648, NULL, 'non_conformity_total', '2009-07-21 00:08:53.174123', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1649, NULL, 'incidents_by_state_directed', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1650, NULL, 'incidents_by_state_open', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1651, NULL, 'incidents_by_state_pendant_disposal', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1652, NULL, 'incidents_by_state_pendant_solution', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1653, NULL, 'incidents_by_state_solved', '2009-07-21 00:08:53.174123', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1654, NULL, 'incidents_by_state_waiting_solution', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1655, NULL, 'nc_by_state_ci_ap_pendant', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1656, NULL, 'nc_by_state_ci_closed', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1657, NULL, 'nc_by_state_ci_directed', '2009-07-21 00:08:53.174123', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1658, NULL, 'nc_by_state_ci_finished', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1659, NULL, 'nc_by_state_ci_nc_pendant', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1660, NULL, 'nc_by_state_ci_open', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1661, NULL, 'nc_by_state_ci_sent', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1662, NULL, 'nc_by_state_ci_waiting_conclusion', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1663, NULL, 'nc_by_state_denied', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1664, NULL, 'ap_total', '2009-07-21 00:08:53.174123', 2);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1665, NULL, 'ap_finished', '2009-07-21 00:08:53.174123', 2);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1666, NULL, 'ap_finished_late', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1667, NULL, 'ap_efficient', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1668, NULL, 'ap_non_efficient', '2009-07-21 00:08:53.174123', 2);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1669, NULL, 'nc_per_ap_average', '2009-07-21 00:08:53.174123', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1670, 15050, 'area_value', '2009-07-22 00:08:44.764646', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1671, NULL, 'areas_total', '2009-07-22 00:08:44.764646', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1672, NULL, 'areas_with_high_risk', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1673, NULL, 'areas_with_medium_risk', '2009-07-22 00:08:44.764646', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1674, NULL, 'areas_with_low_risk', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1675, NULL, 'areas_with_no_value_risk', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1676, NULL, 'complete_areas', '2009-07-22 00:08:44.764646', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1677, NULL, 'partial_areas', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1678, NULL, 'not_managed_areas', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1679, NULL, 'areas_without_processes', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1680, 15050, 'risk_amount_by_area', '2009-07-22 00:08:44.764646', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1681, 15050, 'total_area_impact', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1682, 15050, 'control_amount_by_area', '2009-07-22 00:08:44.764646', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1683, 15050, 'control_cost_by_area', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1684, 15050, 'process_amount_by_area', '2009-07-22 00:08:44.764646', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1685, 15050, 'asset_amount_by_area', '2009-07-22 00:08:44.764646', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1686, 15050, 'asset_cost_by_area', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1687, 15051, 'process_value', '2009-07-22 00:08:44.764646', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1688, 15051, 'asset_amount_by_process', '2009-07-22 00:08:44.764646', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1689, 15051, 'asset_cost_by_process', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1690, 15051, 'risk_amount_by_process', '2009-07-22 00:08:44.764646', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1691, NULL, 'process_total', '2009-07-22 00:08:44.764646', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1692, NULL, 'processes_with_high_risk', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1693, NULL, 'processes_with_medium_risk', '2009-07-22 00:08:44.764646', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1694, NULL, 'processes_with_low_risk', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1695, NULL, 'processes_with_no_value_risk', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1696, NULL, 'complete_processes', '2009-07-22 00:08:44.764646', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1697, NULL, 'partial_processes', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1698, NULL, 'not_managed_processes', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1699, NULL, 'processes_without_assets', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1700, 15051, 'total_process_impact', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1701, 15051, 'control_amount_by_process', '2009-07-22 00:08:44.764646', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1702, 15051, 'control_cost_by_process', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1703, 15082, 'asset_value', '2009-07-22 00:08:44.764646', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1704, 15082, 'process_amount_by_asset', '2009-07-22 00:08:44.764646', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1705, 15082, 'risk_amount_by_asset', '2009-07-22 00:08:44.764646', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1706, NULL, 'asset_total', '2009-07-22 00:08:44.764646', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1707, NULL, 'assets_with_high_risk', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1708, NULL, 'assets_with_medium_risk', '2009-07-22 00:08:44.764646', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1709, NULL, 'assets_with_low_risk', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1710, NULL, 'assets_with_no_value_risk', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1711, NULL, 'complete_assets', '2009-07-22 00:08:44.764646', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1712, NULL, 'partial_assets', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1713, NULL, 'not_managed_assets', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1714, NULL, 'assets_without_risks', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1715, 15082, 'total_asset_impact', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1716, 15082, 'control_amount_by_asset', '2009-07-22 00:08:44.764646', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1717, 15082, 'control_cost_by_asset', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1718, 15084, 'risk_value', '2009-07-22 00:08:44.764646', 5);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1719, 15084, 'risk_residual_value', '2009-07-22 00:08:44.764646', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1720, 15084, 'control_amount_by_risk', '2009-07-22 00:08:44.764646', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1721, NULL, 'risk_total', '2009-07-22 00:08:44.764646', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1722, NULL, 'risks_with_high_real_risk', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1723, NULL, 'risks_with_high_residual_risk', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1724, NULL, 'risks_with_medium_real_risk', '2009-07-22 00:08:44.764646', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1725, NULL, 'risks_with_medium_residual_risk', '2009-07-22 00:08:44.764646', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1726, NULL, 'risks_with_low_real_risk', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1727, NULL, 'risks_with_low_residual_risk', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1728, NULL, 'risks_with_no_value_risk', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1729, NULL, 'risks_treated', '2009-07-22 00:08:44.764646', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1730, NULL, 'risks_not_treated', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1731, NULL, 'risks_accepted', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1732, NULL, 'risks_transfered', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1733, NULL, 'risks_avoided', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1734, 15089, 'risk_amount_by_control', '2009-07-22 00:08:44.764646', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1735, NULL, 'control_total', '2009-07-22 00:08:44.764646', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1736, NULL, 'planned_implantation_controls', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1737, NULL, 'late_implantation_controls', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1738, NULL, 'adequate_implemented_controls', '2009-07-22 00:08:44.764646', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1739, NULL, 'inadequate_implemented_controls', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1740, NULL, 'not_measured_implemented_controls', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1741, NULL, 'tested_ok_controls', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1742, NULL, 'tested_not_ok_controls', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1743, NULL, 'revision_ok_controls', '2009-07-22 00:08:44.764646', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1744, NULL, 'revision_not_ok_controls', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1745, NULL, 'controls_without_risks', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1746, 15049, 'best_practice_amount_by_standard', '2009-07-22 00:08:44.764646', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1747, 15048, 'best_practice_amount_by_standard', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1748, 15047, 'best_practice_amount_by_standard', '2009-07-22 00:08:44.764646', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1749, 15046, 'best_practice_amount_by_standard', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1750, 15045, 'best_practice_amount_by_standard', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1751, 15049, 'applied_best_practice_amount_by_standard', '2009-07-22 00:08:44.764646', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1752, 15048, 'applied_best_practice_amount_by_standard', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1753, 15047, 'applied_best_practice_amount_by_standard', '2009-07-22 00:08:44.764646', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1754, 15046, 'applied_best_practice_amount_by_standard', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1755, 15045, 'applied_best_practice_amount_by_standard', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1756, NULL, 'asset_total_cost', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1757, NULL, 'control_total_cost1', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1758, NULL, 'control_total_cost2', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1759, NULL, 'control_total_cost3', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1760, NULL, 'control_total_cost4', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1761, NULL, 'control_total_cost5', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1762, NULL, 'potentially_low_risk_cost', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1763, NULL, 'mitigated_risk_cost', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1764, NULL, 'avoided_risk_cost', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1765, NULL, 'transfered_risk_cost', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1766, NULL, 'accepted_risk_cost', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1767, NULL, 'not_treated_and_medium_high_risk', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1768, NULL, 'document_total', '2009-07-22 00:08:44.764646', 3);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1769, NULL, 'documents_by_state_approved', '2009-07-22 00:08:44.764646', 3);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1770, NULL, 'documents_by_state_developing', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1771, NULL, 'documents_by_state_pendant', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1772, NULL, 'documents_by_type_area', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1773, NULL, 'documents_by_type_asset', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1774, NULL, 'documents_by_type_control', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1775, NULL, 'documents_by_type_management', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1776, NULL, 'documents_by_type_none', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1777, NULL, 'documents_by_type_others', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1778, NULL, 'documents_by_type_policy', '2009-07-22 00:08:44.764646', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1779, NULL, 'documents_by_type_process', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1780, NULL, 'documents_by_type_scope', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1781, NULL, 'register_total', '2009-07-22 00:08:44.764646', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1782, NULL, 'documents_read', '2009-07-22 00:08:44.764646', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1783, NULL, 'documents_not_read', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1784, NULL, 'documents_scheduled', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1785, NULL, 'occupation_documents', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1786, NULL, 'occupation_template', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1787, NULL, 'occupation_registers', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1788, NULL, 'occupation_total', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1789, NULL, 'incident_total', '2009-07-22 00:08:44.764646', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1790, NULL, 'non_conformity_total', '2009-07-22 00:08:44.764646', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1791, NULL, 'incidents_by_state_directed', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1792, NULL, 'incidents_by_state_open', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1793, NULL, 'incidents_by_state_pendant_disposal', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1794, NULL, 'incidents_by_state_pendant_solution', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1795, NULL, 'incidents_by_state_solved', '2009-07-22 00:08:44.764646', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1796, NULL, 'incidents_by_state_waiting_solution', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1797, NULL, 'nc_by_state_ci_ap_pendant', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1798, NULL, 'nc_by_state_ci_closed', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1799, NULL, 'nc_by_state_ci_directed', '2009-07-22 00:08:44.764646', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1800, NULL, 'nc_by_state_ci_finished', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1801, NULL, 'nc_by_state_ci_nc_pendant', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1802, NULL, 'nc_by_state_ci_open', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1803, NULL, 'nc_by_state_ci_sent', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1804, NULL, 'nc_by_state_ci_waiting_conclusion', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1805, NULL, 'nc_by_state_denied', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1806, NULL, 'ap_total', '2009-07-22 00:08:44.764646', 2);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1807, NULL, 'ap_finished', '2009-07-22 00:08:44.764646', 2);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1808, NULL, 'ap_finished_late', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1809, NULL, 'ap_efficient', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1810, NULL, 'ap_non_efficient', '2009-07-22 00:08:44.764646', 2);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1811, NULL, 'nc_per_ap_average', '2009-07-22 00:08:44.764646', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1812, 15050, 'area_value', '2009-07-23 00:09:05.582433', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1813, NULL, 'areas_total', '2009-07-23 00:09:05.582433', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1814, NULL, 'areas_with_high_risk', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1815, NULL, 'areas_with_medium_risk', '2009-07-23 00:09:05.582433', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1816, NULL, 'areas_with_low_risk', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1817, NULL, 'areas_with_no_value_risk', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1818, NULL, 'complete_areas', '2009-07-23 00:09:05.582433', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1819, NULL, 'partial_areas', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1820, NULL, 'not_managed_areas', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1821, NULL, 'areas_without_processes', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1822, 15050, 'risk_amount_by_area', '2009-07-23 00:09:05.582433', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1823, 15050, 'total_area_impact', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1824, 15050, 'control_amount_by_area', '2009-07-23 00:09:05.582433', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1825, 15050, 'control_cost_by_area', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1826, 15050, 'process_amount_by_area', '2009-07-23 00:09:05.582433', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1827, 15050, 'asset_amount_by_area', '2009-07-23 00:09:05.582433', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1828, 15050, 'asset_cost_by_area', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1829, 15051, 'process_value', '2009-07-23 00:09:05.582433', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1830, 15051, 'asset_amount_by_process', '2009-07-23 00:09:05.582433', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1831, 15051, 'asset_cost_by_process', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1832, 15051, 'risk_amount_by_process', '2009-07-23 00:09:05.582433', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1833, NULL, 'process_total', '2009-07-23 00:09:05.582433', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1834, NULL, 'processes_with_high_risk', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1835, NULL, 'processes_with_medium_risk', '2009-07-23 00:09:05.582433', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1836, NULL, 'processes_with_low_risk', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1837, NULL, 'processes_with_no_value_risk', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1838, NULL, 'complete_processes', '2009-07-23 00:09:05.582433', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1839, NULL, 'partial_processes', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1840, NULL, 'not_managed_processes', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1841, NULL, 'processes_without_assets', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1842, 15051, 'total_process_impact', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1843, 15051, 'control_amount_by_process', '2009-07-23 00:09:05.582433', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1844, 15051, 'control_cost_by_process', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1845, 15082, 'asset_value', '2009-07-23 00:09:05.582433', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1846, 15082, 'process_amount_by_asset', '2009-07-23 00:09:05.582433', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1847, 15082, 'risk_amount_by_asset', '2009-07-23 00:09:05.582433', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1848, NULL, 'asset_total', '2009-07-23 00:09:05.582433', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1849, NULL, 'assets_with_high_risk', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1850, NULL, 'assets_with_medium_risk', '2009-07-23 00:09:05.582433', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1851, NULL, 'assets_with_low_risk', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1852, NULL, 'assets_with_no_value_risk', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1853, NULL, 'complete_assets', '2009-07-23 00:09:05.582433', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1854, NULL, 'partial_assets', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1855, NULL, 'not_managed_assets', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1856, NULL, 'assets_without_risks', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1857, 15082, 'total_asset_impact', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1858, 15082, 'control_amount_by_asset', '2009-07-23 00:09:05.582433', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1859, 15082, 'control_cost_by_asset', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1860, 15084, 'risk_value', '2009-07-23 00:09:05.582433', 5);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1861, 15084, 'risk_residual_value', '2009-07-23 00:09:05.582433', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1862, 15084, 'control_amount_by_risk', '2009-07-23 00:09:05.582433', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1863, NULL, 'risk_total', '2009-07-23 00:09:05.582433', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1864, NULL, 'risks_with_high_real_risk', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1865, NULL, 'risks_with_high_residual_risk', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1866, NULL, 'risks_with_medium_real_risk', '2009-07-23 00:09:05.582433', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1867, NULL, 'risks_with_medium_residual_risk', '2009-07-23 00:09:05.582433', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1868, NULL, 'risks_with_low_real_risk', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1869, NULL, 'risks_with_low_residual_risk', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1870, NULL, 'risks_with_no_value_risk', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1871, NULL, 'risks_treated', '2009-07-23 00:09:05.582433', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1872, NULL, 'risks_not_treated', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1873, NULL, 'risks_accepted', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1874, NULL, 'risks_transfered', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1875, NULL, 'risks_avoided', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1876, 15089, 'risk_amount_by_control', '2009-07-23 00:09:05.582433', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1877, NULL, 'control_total', '2009-07-23 00:09:05.582433', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1878, NULL, 'planned_implantation_controls', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1879, NULL, 'late_implantation_controls', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1880, NULL, 'adequate_implemented_controls', '2009-07-23 00:09:05.582433', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1881, NULL, 'inadequate_implemented_controls', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1882, NULL, 'not_measured_implemented_controls', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1883, NULL, 'tested_ok_controls', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1884, NULL, 'tested_not_ok_controls', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1885, NULL, 'revision_ok_controls', '2009-07-23 00:09:05.582433', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1886, NULL, 'revision_not_ok_controls', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1887, NULL, 'controls_without_risks', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1888, 15049, 'best_practice_amount_by_standard', '2009-07-23 00:09:05.582433', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1889, 15048, 'best_practice_amount_by_standard', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1890, 15047, 'best_practice_amount_by_standard', '2009-07-23 00:09:05.582433', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1891, 15046, 'best_practice_amount_by_standard', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1892, 15045, 'best_practice_amount_by_standard', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1893, 15049, 'applied_best_practice_amount_by_standard', '2009-07-23 00:09:05.582433', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1894, 15048, 'applied_best_practice_amount_by_standard', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1895, 15047, 'applied_best_practice_amount_by_standard', '2009-07-23 00:09:05.582433', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1896, 15046, 'applied_best_practice_amount_by_standard', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1897, 15045, 'applied_best_practice_amount_by_standard', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1898, NULL, 'asset_total_cost', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1899, NULL, 'control_total_cost1', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1900, NULL, 'control_total_cost2', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1901, NULL, 'control_total_cost3', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1902, NULL, 'control_total_cost4', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1903, NULL, 'control_total_cost5', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1904, NULL, 'potentially_low_risk_cost', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1905, NULL, 'mitigated_risk_cost', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1906, NULL, 'avoided_risk_cost', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1907, NULL, 'transfered_risk_cost', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1908, NULL, 'accepted_risk_cost', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1909, NULL, 'not_treated_and_medium_high_risk', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1910, NULL, 'document_total', '2009-07-23 00:09:05.582433', 3);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1911, NULL, 'documents_by_state_approved', '2009-07-23 00:09:05.582433', 3);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1912, NULL, 'documents_by_state_developing', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1913, NULL, 'documents_by_state_pendant', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1914, NULL, 'documents_by_type_area', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1915, NULL, 'documents_by_type_asset', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1916, NULL, 'documents_by_type_control', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1917, NULL, 'documents_by_type_management', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1918, NULL, 'documents_by_type_none', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1919, NULL, 'documents_by_type_others', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1920, NULL, 'documents_by_type_policy', '2009-07-23 00:09:05.582433', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1921, NULL, 'documents_by_type_process', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1922, NULL, 'documents_by_type_scope', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1923, NULL, 'register_total', '2009-07-23 00:09:05.582433', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1924, NULL, 'documents_read', '2009-07-23 00:09:05.582433', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1925, NULL, 'documents_not_read', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1926, NULL, 'documents_scheduled', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1927, NULL, 'occupation_documents', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1928, NULL, 'occupation_template', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1929, NULL, 'occupation_registers', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1930, NULL, 'occupation_total', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1931, NULL, 'incident_total', '2009-07-23 00:09:05.582433', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1932, NULL, 'non_conformity_total', '2009-07-23 00:09:05.582433', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1933, NULL, 'incidents_by_state_directed', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1934, NULL, 'incidents_by_state_open', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1935, NULL, 'incidents_by_state_pendant_disposal', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1936, NULL, 'incidents_by_state_pendant_solution', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1937, NULL, 'incidents_by_state_solved', '2009-07-23 00:09:05.582433', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1938, NULL, 'incidents_by_state_waiting_solution', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1939, NULL, 'nc_by_state_ci_ap_pendant', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1940, NULL, 'nc_by_state_ci_closed', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1941, NULL, 'nc_by_state_ci_directed', '2009-07-23 00:09:05.582433', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1942, NULL, 'nc_by_state_ci_finished', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1943, NULL, 'nc_by_state_ci_nc_pendant', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1944, NULL, 'nc_by_state_ci_open', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1945, NULL, 'nc_by_state_ci_sent', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1946, NULL, 'nc_by_state_ci_waiting_conclusion', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1947, NULL, 'nc_by_state_denied', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1948, NULL, 'ap_total', '2009-07-23 00:09:05.582433', 2);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1949, NULL, 'ap_finished', '2009-07-23 00:09:05.582433', 2);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1950, NULL, 'ap_finished_late', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1951, NULL, 'ap_efficient', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1952, NULL, 'ap_non_efficient', '2009-07-23 00:09:05.582433', 2);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1953, NULL, 'nc_per_ap_average', '2009-07-23 00:09:05.582433', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1954, 15050, 'area_value', '2009-07-24 00:08:46.211913', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1955, NULL, 'areas_total', '2009-07-24 00:08:46.211913', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1956, NULL, 'areas_with_high_risk', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1957, NULL, 'areas_with_medium_risk', '2009-07-24 00:08:46.211913', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1958, NULL, 'areas_with_low_risk', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1959, NULL, 'areas_with_no_value_risk', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1960, NULL, 'complete_areas', '2009-07-24 00:08:46.211913', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1961, NULL, 'partial_areas', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1962, NULL, 'not_managed_areas', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1963, NULL, 'areas_without_processes', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1964, 15050, 'risk_amount_by_area', '2009-07-24 00:08:46.211913', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1965, 15050, 'total_area_impact', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1966, 15050, 'control_amount_by_area', '2009-07-24 00:08:46.211913', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1967, 15050, 'control_cost_by_area', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1968, 15050, 'process_amount_by_area', '2009-07-24 00:08:46.211913', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1969, 15050, 'asset_amount_by_area', '2009-07-24 00:08:46.211913', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1970, 15050, 'asset_cost_by_area', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1971, 15051, 'process_value', '2009-07-24 00:08:46.211913', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1972, 15051, 'asset_amount_by_process', '2009-07-24 00:08:46.211913', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1973, 15051, 'asset_cost_by_process', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1974, 15051, 'risk_amount_by_process', '2009-07-24 00:08:46.211913', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1975, NULL, 'process_total', '2009-07-24 00:08:46.211913', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1976, NULL, 'processes_with_high_risk', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1977, NULL, 'processes_with_medium_risk', '2009-07-24 00:08:46.211913', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1978, NULL, 'processes_with_low_risk', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1979, NULL, 'processes_with_no_value_risk', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1980, NULL, 'complete_processes', '2009-07-24 00:08:46.211913', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1981, NULL, 'partial_processes', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1982, NULL, 'not_managed_processes', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1983, NULL, 'processes_without_assets', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1984, 15051, 'total_process_impact', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1985, 15051, 'control_amount_by_process', '2009-07-24 00:08:46.211913', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1986, 15051, 'control_cost_by_process', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1987, 15082, 'asset_value', '2009-07-24 00:08:46.211913', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1988, 15082, 'process_amount_by_asset', '2009-07-24 00:08:46.211913', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1989, 15082, 'risk_amount_by_asset', '2009-07-24 00:08:46.211913', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1990, NULL, 'asset_total', '2009-07-24 00:08:46.211913', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1991, NULL, 'assets_with_high_risk', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1992, NULL, 'assets_with_medium_risk', '2009-07-24 00:08:46.211913', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1993, NULL, 'assets_with_low_risk', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1994, NULL, 'assets_with_no_value_risk', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1995, NULL, 'complete_assets', '2009-07-24 00:08:46.211913', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1996, NULL, 'partial_assets', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1997, NULL, 'not_managed_assets', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1998, NULL, 'assets_without_risks', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (1999, 15082, 'total_asset_impact', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2000, 15082, 'control_amount_by_asset', '2009-07-24 00:08:46.211913', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2001, 15082, 'control_cost_by_asset', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2002, 15084, 'risk_value', '2009-07-24 00:08:46.211913', 5);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2003, 15084, 'risk_residual_value', '2009-07-24 00:08:46.211913', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2004, 15084, 'control_amount_by_risk', '2009-07-24 00:08:46.211913', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2005, NULL, 'risk_total', '2009-07-24 00:08:46.211913', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2006, NULL, 'risks_with_high_real_risk', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2007, NULL, 'risks_with_high_residual_risk', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2008, NULL, 'risks_with_medium_real_risk', '2009-07-24 00:08:46.211913', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2009, NULL, 'risks_with_medium_residual_risk', '2009-07-24 00:08:46.211913', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2010, NULL, 'risks_with_low_real_risk', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2011, NULL, 'risks_with_low_residual_risk', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2012, NULL, 'risks_with_no_value_risk', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2013, NULL, 'risks_treated', '2009-07-24 00:08:46.211913', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2014, NULL, 'risks_not_treated', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2015, NULL, 'risks_accepted', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2016, NULL, 'risks_transfered', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2017, NULL, 'risks_avoided', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2018, 15089, 'risk_amount_by_control', '2009-07-24 00:08:46.211913', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2019, NULL, 'control_total', '2009-07-24 00:08:46.211913', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2020, NULL, 'planned_implantation_controls', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2021, NULL, 'late_implantation_controls', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2022, NULL, 'adequate_implemented_controls', '2009-07-24 00:08:46.211913', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2023, NULL, 'inadequate_implemented_controls', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2024, NULL, 'not_measured_implemented_controls', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2025, NULL, 'tested_ok_controls', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2026, NULL, 'tested_not_ok_controls', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2027, NULL, 'revision_ok_controls', '2009-07-24 00:08:46.211913', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2028, NULL, 'revision_not_ok_controls', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2029, NULL, 'controls_without_risks', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2030, 15049, 'best_practice_amount_by_standard', '2009-07-24 00:08:46.211913', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2031, 15048, 'best_practice_amount_by_standard', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2032, 15047, 'best_practice_amount_by_standard', '2009-07-24 00:08:46.211913', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2033, 15046, 'best_practice_amount_by_standard', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2034, 15045, 'best_practice_amount_by_standard', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2035, 15049, 'applied_best_practice_amount_by_standard', '2009-07-24 00:08:46.211913', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2036, 15048, 'applied_best_practice_amount_by_standard', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2037, 15047, 'applied_best_practice_amount_by_standard', '2009-07-24 00:08:46.211913', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2038, 15046, 'applied_best_practice_amount_by_standard', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2039, 15045, 'applied_best_practice_amount_by_standard', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2040, NULL, 'asset_total_cost', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2041, NULL, 'control_total_cost1', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2042, NULL, 'control_total_cost2', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2043, NULL, 'control_total_cost3', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2044, NULL, 'control_total_cost4', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2045, NULL, 'control_total_cost5', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2046, NULL, 'potentially_low_risk_cost', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2047, NULL, 'mitigated_risk_cost', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2048, NULL, 'avoided_risk_cost', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2049, NULL, 'transfered_risk_cost', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2050, NULL, 'accepted_risk_cost', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2051, NULL, 'not_treated_and_medium_high_risk', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2052, NULL, 'document_total', '2009-07-24 00:08:46.211913', 3);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2053, NULL, 'documents_by_state_approved', '2009-07-24 00:08:46.211913', 3);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2054, NULL, 'documents_by_state_developing', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2055, NULL, 'documents_by_state_pendant', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2056, NULL, 'documents_by_type_area', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2057, NULL, 'documents_by_type_asset', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2058, NULL, 'documents_by_type_control', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2059, NULL, 'documents_by_type_management', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2060, NULL, 'documents_by_type_none', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2061, NULL, 'documents_by_type_others', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2062, NULL, 'documents_by_type_policy', '2009-07-24 00:08:46.211913', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2063, NULL, 'documents_by_type_process', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2064, NULL, 'documents_by_type_scope', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2065, NULL, 'register_total', '2009-07-24 00:08:46.211913', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2066, NULL, 'documents_read', '2009-07-24 00:08:46.211913', 4);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2067, NULL, 'documents_not_read', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2068, NULL, 'documents_scheduled', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2069, NULL, 'occupation_documents', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2070, NULL, 'occupation_template', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2071, NULL, 'occupation_registers', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2072, NULL, 'occupation_total', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2073, NULL, 'incident_total', '2009-07-24 00:08:46.211913', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2074, NULL, 'non_conformity_total', '2009-07-24 00:08:46.211913', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2075, NULL, 'incidents_by_state_directed', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2076, NULL, 'incidents_by_state_open', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2077, NULL, 'incidents_by_state_pendant_disposal', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2078, NULL, 'incidents_by_state_pendant_solution', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2079, NULL, 'incidents_by_state_solved', '2009-07-24 00:08:46.211913', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2080, NULL, 'incidents_by_state_waiting_solution', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2081, NULL, 'nc_by_state_ci_ap_pendant', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2082, NULL, 'nc_by_state_ci_closed', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2083, NULL, 'nc_by_state_ci_directed', '2009-07-24 00:08:46.211913', 1);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2084, NULL, 'nc_by_state_ci_finished', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2085, NULL, 'nc_by_state_ci_nc_pendant', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2086, NULL, 'nc_by_state_ci_open', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2087, NULL, 'nc_by_state_ci_sent', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2088, NULL, 'nc_by_state_ci_waiting_conclusion', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2089, NULL, 'nc_by_state_denied', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2090, NULL, 'ap_total', '2009-07-24 00:08:46.211913', 2);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2091, NULL, 'ap_finished', '2009-07-24 00:08:46.211913', 2);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2092, NULL, 'ap_finished_late', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2093, NULL, 'ap_efficient', '2009-07-24 00:08:46.211913', 0);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2094, NULL, 'ap_non_efficient', '2009-07-24 00:08:46.211913', 2);
INSERT INTO isms_context_history (pkid, fkcontext, stype, ddate, nvalue) VALUES (2095, NULL, 'nc_per_ap_average', '2009-07-24 00:08:46.211913', 0);


--
-- TOC entry 3142 (class 0 OID 9034741)
-- Dependencies: 2004
-- Data for Name: isms_policy; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3119 (class 0 OID 9034550)
-- Dependencies: 1977
-- Data for Name: isms_profile; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO isms_profile (fkcontext, sname, nid) VALUES (1, 'Gestor de Saúde e Segurança', 8801);
INSERT INTO isms_profile (fkcontext, sname, nid) VALUES (2, 'Gestor', 8802);
INSERT INTO isms_profile (fkcontext, sname, nid) VALUES (3, 'Administrador', 8803);
INSERT INTO isms_profile (fkcontext, sname, nid) VALUES (4, 'Sem Permissµes', 8804);
INSERT INTO isms_profile (fkcontext, sname, nid) VALUES (5, 'Leitor de Documentos', 8805);


--
-- TOC entry 3143 (class 0 OID 9034747)
-- Dependencies: 2005
-- Data for Name: isms_profile_acl; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'A');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'A.A');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (3, 'A.A');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'A.A.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (3, 'A.A.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'A.A.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (3, 'A.A.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'A.A.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (3, 'A.A.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'A.C');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (3, 'A.C');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'A.C.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (3, 'A.C.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'A.C.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (3, 'A.C.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'A.C.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (3, 'A.C.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'A.C.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (3, 'A.C.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'A.C.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (3, 'A.C.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'A.C.6');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (3, 'A.C.6');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'A.C.7');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (3, 'A.C.7');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'A.C.7.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (3, 'A.C.7.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'A.C.7.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (3, 'A.C.7.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'A.C.7.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (3, 'A.C.7.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'A.C.7.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (3, 'A.C.7.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'A.C.7.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (3, 'A.C.7.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'A.C.7.6');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (3, 'A.C.7.6');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'A.C.7.7');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (3, 'A.C.7.7');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'A.C.8');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (3, 'A.C.8');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'A.C.9');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (3, 'A.C.9');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'A.C.10');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (3, 'A.C.10');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'A.C.11');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (3, 'A.C.11');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'A.L');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (3, 'A.L');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'A.L.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (3, 'A.L.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'A.L.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (3, 'A.L.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'A.L.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (3, 'A.L.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'A.MA');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (3, 'A.MA');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'A.P');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (3, 'A.P');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'A.P.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (3, 'A.P.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'A.P.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (3, 'A.P.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'A.P.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (3, 'A.P.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'A.S');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (3, 'A.S');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'A.S.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (3, 'A.S.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'A.S.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (3, 'A.S.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'A.U');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (3, 'A.U');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'A.U.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (3, 'A.U.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'A.U.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (3, 'A.U.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'A.U.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (3, 'A.U.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'A.U.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (3, 'A.U.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'A.U.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (3, 'A.U.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.CI');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.CI');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.CI.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.CI.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.CI.1.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.CI.1.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.CI.1.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.CI.1.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.CI.1.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.CI.1.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.CI.1.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.CI.1.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.CI.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.CI.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.CI.2.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.CI.2.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.CI.2.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.CI.2.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.CI.2.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.CI.2.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.CI.2.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.CI.2.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.CI.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.CI.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.CI.3.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.CI.3.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.CI.3.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.CI.3.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.CI.3.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.CI.3.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.CI.3.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.CI.3.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.CI.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.CI.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.CI.5.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.CI.5.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.CI.5.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.CI.5.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.CI.5.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.CI.5.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.CI.6');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.CI.6');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.CI.6.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.CI.6.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.CI.6.1.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.CI.6.1.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.CI.6.1.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.CI.6.1.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.CI.6.1.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.CI.6.1.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.CI.6.1.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.CI.6.1.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.CI.6.1.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.CI.6.1.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.CI.6.1.6');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.CI.6.1.6');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.CI.6.1.7');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.CI.6.1.7');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.CI.6.1.8');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.CI.6.1.8');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.CI.6.1.9');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.CI.6.1.9');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.CI.6.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.CI.6.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.CI.6.2.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.CI.6.2.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.CI.6.2.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.CI.6.2.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.CI.6.2.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.CI.6.2.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.CI.6.2.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.CI.6.2.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.CI.6.2.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.CI.6.2.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.CI.6.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.CI.6.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.CI.6.3.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.CI.6.3.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.CI.6.3.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.CI.6.3.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.CI.6.3.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.CI.6.3.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.CI.6.3.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.CI.6.3.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.CI.6.3.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.CI.6.3.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.CI.7');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.CI.7');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.CI.7.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.CI.7.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.CI.7.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.CI.7.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.CI.7.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.CI.7.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.CI.7.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.CI.7.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.D');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.D');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.D.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.D.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.D.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.D.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.D.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.D.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.D.6');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.D.6');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.D.6.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.D.6.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.D.6.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.D.6.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.D.6.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.D.6.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.D.6.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.D.6.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.D.6.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.D.6.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.D.6.6');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.D.6.6');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.D.6.7');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.D.6.7');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.D.6.8');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.D.6.8');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.D.6.9');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.D.6.9');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.D.6.11');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.D.6.11');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.D.6.12');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.D.6.12');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.D.6.13');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.D.6.13');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.L');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.L');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.L.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.L.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.L.1.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.L.1.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.L.1.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.L.1.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.L.1.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.L.1.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.L.1.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.L.1.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.L.1.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.L.1.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.L.1.6');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.L.1.6');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.L.1.7');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.L.1.7');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.L.1.8');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.L.1.8');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.L.1.9');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.L.1.9');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.L.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.L.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.L.2.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.L.2.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.L.2.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.L.2.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.L.2.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.L.2.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.L.2.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.L.2.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.L.2.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.L.2.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.L.2.6');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.L.2.6');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.L.2.7');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.L.2.7');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.L.2.8');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.L.2.8');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.L.2.9');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.L.2.9');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.L.2.10');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.L.2.10');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.L.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.L.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.L.3.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.L.3.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.L.3.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.L.3.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.L.3.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.L.3.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.L.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.L.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.L.4.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.L.4.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.L.4.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.L.4.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.L.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.L.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.L.5.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.L.5.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.L.5.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.L.5.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.L.5.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.L.5.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.L.6');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.L.6');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.L.6.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.L.6.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.L.6.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.L.6.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.L.6.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.L.6.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.L.6.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.L.6.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.L.6.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.L.6.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.L.6.6');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.L.6.6');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.PM');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.PM');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.PM.10');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.PM.10');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.PM.10.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.PM.10.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.PM.11');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.PM.11');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.PM.12');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.PM.12');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.PM.13');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.PM.13');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.PM.14');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.PM.14');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.PM.15');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.PM.15');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.PM.16');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.PM.16');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.PM.17');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.PM.17');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.PM.17.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.PM.17.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.PM.17.1.16');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.PM.17.1.16');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.PM.17.1.17');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.PM.17.1.17');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.PM.17.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.PM.17.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.PM.17.2.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.PM.17.2.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.PM.17.2.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.PM.17.2.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.PM.17.2.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.PM.17.2.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.PM.17.2.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.PM.17.2.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.PM.17.2.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.PM.17.2.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.PM.17.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.PM.17.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.PM.17.3.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.PM.17.3.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.PM.17.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.PM.17.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.PM.17.4.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.PM.17.4.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.PM.17.4.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.PM.17.4.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.PM.17.4.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.PM.17.4.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.PM.17.4.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.PM.17.4.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.PM.17.4.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.PM.17.4.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.PM.17.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.PM.17.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.PM.17.5.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.PM.17.5.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.PM.17.5.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.PM.17.5.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.PM.17.5.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.PM.17.5.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.PM.17.5.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.PM.17.5.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.PM.17.5.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.PM.17.5.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.PM.17.5.6');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.PM.17.5.6');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.PM.17.5.7');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.PM.17.5.7');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.PM.17.5.8');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.PM.17.5.8');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.PM.17.6');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.PM.17.6');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.PM.17.6.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.PM.17.6.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.PM.17.6.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.PM.17.6.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.PM.17.6.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.PM.17.6.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.PM.17.6.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.PM.17.6.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.PM.17.6.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.PM.17.6.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.PM.18');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.PM.18');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.1.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.1.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.1.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.1.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.1.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.1.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.1.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.1.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.1.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.1.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.1.6');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.1.6');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.1.7');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.1.7');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.1.8');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.1.8');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.1.9');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.1.9');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.2.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.2.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.2.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.2.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.2.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.2.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.2.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.2.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.2.6');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.2.6');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.2.7');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.2.7');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.2.8');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.2.8');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.3.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.3.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.3.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.3.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.3.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.3.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.3.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.3.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.3.6');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.3.6');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.3.7');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.3.7');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.3.8');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.3.8');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.3.9');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.3.9');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.3.10');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.3.10');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.4.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.4.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.4.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.4.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.4.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.4.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.4.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.4.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.4.6');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.4.6');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.4.8');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.4.8');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.4.9');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.4.9');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.4.10');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.4.10');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.5.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.5.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.5.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.5.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.5.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.5.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.5.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.5.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.5.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.5.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.5.6');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.5.6');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.5.7');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.5.7');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.5.8');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.5.8');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.5.9');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.5.9');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.6');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.6');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.6.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.6.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.6.1.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.6.1.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.6.1.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.6.1.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.6.1.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.6.1.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.6.1.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.6.1.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.6.1.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.6.1.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.6.1.6');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.6.1.6');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.6.1.7');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.6.1.7');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.6.1.8');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.6.1.8');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.6.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.6.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.6.2.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.6.2.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.6.2.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.6.2.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.6.2.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.6.2.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.6.2.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.6.2.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.6.2.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.6.2.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.6.2.6');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.6.2.6');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.6.2.7');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.6.2.7');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.6.2.8');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.6.2.8');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.6.2.9');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.6.2.9');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.6.2.10');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.6.2.10');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.6.2.11');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.6.2.11');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.6.2.12');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.6.2.12');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.6.2.13');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.6.2.13');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.6.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.6.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.6.3.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.6.3.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.6.3.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.6.3.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.6.3.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.6.3.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.6.3.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.6.3.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.6.3.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.6.3.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.6.3.6');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.6.3.6');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.6.3.7');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.6.3.7');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.6.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.6.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.6.4.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.6.4.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.6.4.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.6.4.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.6.4.6');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.6.4.6');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.6.4.7');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.6.4.7');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.6.4.8');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.6.4.8');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.6.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.6.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.6.5.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.6.5.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.6.5.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.6.5.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.6.6');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.6.6');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.6.6.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.6.6.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.6.6.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.6.6.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.6.6.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.6.6.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.6.6.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.6.6.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.6.6.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.6.6.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.6.6.6');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.6.6.6');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.6.6.7');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.6.6.7');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.6.6.8');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.6.6.8');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.6.7');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.6.7');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.6.7.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.6.7.1');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.6.7.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.6.7.2');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.6.7.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.6.7.3');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.6.7.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.6.7.4');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.6.7.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.6.7.5');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.RM.6.7.6');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.RM.6.7.6');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (1, 'M.S');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (2, 'M.S');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (5, 'M.PM');
INSERT INTO isms_profile_acl (fkprofile, stag) VALUES (5, 'M.PM.12');


--
-- TOC entry 3144 (class 0 OID 9034750)
-- Dependencies: 2006
-- Data for Name: isms_saas; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO isms_saas (pkconfig, svalue) VALUES (711701, '%saas_report_email%');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711714, '%config_path%');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711703, '');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711704, '');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711705, '');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711706, '');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711707, '');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711708, '');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711709, '1');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711710, '');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711712, '');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711713, '%user_email%');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711715, '%max_file_size%');

--
-- TOC entry 3145 (class 0 OID 9034755)
-- Dependencies: 2008
-- Data for Name: isms_scope; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3146 (class 0 OID 9034761)
-- Dependencies: 2009
-- Data for Name: isms_solicitor; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3120 (class 0 OID 9034554)
-- Dependencies: 1978
-- Data for Name: isms_user; Type: TABLE DATA; Schema: public; Owner: postgres
--
INSERT INTO isms_user (fkcontext, fkprofile, sname, slogin, spassword, semail, nip, nlanguage, dlastlogin, nwronglogonattempts, bisblocked, bmustchangepassword, srequestpassword, ssession, bansweredsurvey) VALUES (13, 1, '%user_fullname%', '%user_login%', '%user_passwordmd5%', '%user_email%', 0, %user_language%, '2008-04-11 16:31:42', 0, 0, 0, '', '', 0);


--
-- TOC entry 3147 (class 0 OID 9034764)
-- Dependencies: 2010
-- Data for Name: isms_user_password_history; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO isms_user_password_history (fkuser, spassword, ddatepasswordchanged) VALUES (13, 'f0a9f84c4d4989ce2c5ccd48c7b34be5', '2009-07-10 11:47:18');


--
-- TOC entry 3148 (class 0 OID 9034767)
-- Dependencies: 2011
-- Data for Name: isms_user_preference; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO isms_user_preference (fkuser, spreference, tvalue) VALUES (13, 'dashboard_sumary_pref', 'a:20:{i:0;s:17:"grid_risk_summary";i:1;s:18:"grid_risk_summary2";i:2;s:26:"grid_residual_risk_summary";i:3;s:31:"grid_isms_reports_and_documents";i:4;s:26:"grid_best_practice_summary";i:5;s:20:"grid_control_summary";i:6;s:25:"grid_abnormalitys_summary";i:7;s:15:"grid_doc_sumary";i:8;s:31:"grid_doc_context_with_documents";i:9;s:22:"grid_doc_approver_time";i:10;s:14:"grid_doc_top10";i:11;s:24:"grid_doc_period_revision";i:12;s:19:"grid_abnormality_pm";i:13;s:21:"grid_incident_summary";i:14;s:15:"grid_nc_summary";i:15;s:23:"grid_incidents_per_user";i:16;s:24:"grid_incidents_per_asset";i:17;s:26:"grid_incidents_per_process";i:18;s:19:"grid_nc_per_process";i:19;s:19:"grid_abnormality_ci";}');


--
-- TOC entry 3149 (class 0 OID 9034773)
-- Dependencies: 2012
-- Data for Name: pm_doc_approvers; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3150 (class 0 OID 9034782)
-- Dependencies: 2013
-- Data for Name: pm_doc_context; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3121 (class 0 OID 9034569)
-- Dependencies: 1979
-- Data for Name: pm_doc_instance; Type: TABLE DATA; Schema: public; Owner: postgres
--

-- INSERT INTO pm_doc_instance (fkcontext, fkdocument, nmajorversion, nrevisionversion, trevisionjustification, spath, tmodifycomment, dbeginproduction, dendproduction, sfilename, bislink, slink, dbeginapprover, dbeginrevision, nfilesize, smanualversion) VALUES (15094, 15093, 1, 0, NULL, '/mnt/realiso/newisms/instances/saas/qualyohs/files/4bb29de7c4e53305273dec88ad47af20.aef', 'Criado para auxiliar na prevenção de acidentes.', '2009-07-10 00:00:00', NULL, 'PROCEDIMENTO X  PREVENÇÃO DE ACIDENTES.docx', 0, '', '2009-07-10 13:46:27', NULL, 10660, '1');
-- INSERT INTO pm_doc_instance (fkcontext, fkdocument, nmajorversion, nrevisionversion, trevisionjustification, spath, tmodifycomment, dbeginproduction, dendproduction, sfilename, bislink, slink, dbeginapprover, dbeginrevision, nfilesize, smanualversion) VALUES (15097, 15096, 1, 0, NULL, '/mnt/realiso/newisms/instances/saas/qualyohs/files/c4ed1be4d256df91b2516d33aff97b36.aef', 'Informação de acidente', '2009-07-10 00:00:00', NULL, 'COMUNICAÇÃO DE ACIDENTE DE TRABALHO.docx', 0, '', '2009-07-10 13:55:01', NULL, 10305, '1');
-- INSERT INTO pm_doc_instance (fkcontext, fkdocument, nmajorversion, nrevisionversion, trevisionjustification, spath, tmodifycomment, dbeginproduction, dendproduction, sfilename, bislink, slink, dbeginapprover, dbeginrevision, nfilesize, smanualversion) VALUES (15105, 15104, 1, 0, NULL, '/mnt/realiso/newisms/instances/saas/qualyohs/files/08dd07069af808a7fe20415595597449.aef', 'Ação criada a partir de uma não conformidade.', '2009-07-10 00:00:00', NULL, 'PLANO DE AÇÃO XX.docx', 0, '', '2009-07-10 14:25:28', NULL, 10128, '0');


--
-- TOC entry 3151 (class 0 OID 9034785)
-- Dependencies: 2014
-- Data for Name: pm_doc_read_history; Type: TABLE DATA; Schema: public; Owner: postgres
--

-- INSERT INTO pm_doc_read_history (ddate, fkinstance, fkuser) VALUES ('2009-07-10 13:46:45', 15094, 13);
-- INSERT INTO pm_doc_read_history (ddate, fkinstance, fkuser) VALUES ('2009-07-10 13:56:55', 15097, 13);
-- INSERT INTO pm_doc_read_history (ddate, fkinstance, fkuser) VALUES ('2009-07-10 13:57:14', 15097, 13);
-- INSERT INTO pm_doc_read_history (ddate, fkinstance, fkuser) VALUES ('2009-07-14 14:09:47', 15105, 13);


--
-- TOC entry 3152 (class 0 OID 9034788)
-- Dependencies: 2015
-- Data for Name: pm_doc_readers; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3153 (class 0 OID 9034797)
-- Dependencies: 2016
-- Data for Name: pm_doc_reference; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3154 (class 0 OID 9034805)
-- Dependencies: 2018
-- Data for Name: pm_doc_registers; Type: TABLE DATA; Schema: public; Owner: postgres
--

-- INSERT INTO pm_doc_registers (fkregister, fkdocument) VALUES (15095, 15096);


--
-- TOC entry 3122 (class 0 OID 9034580)
-- Dependencies: 1980
-- Data for Name: pm_document; Type: TABLE DATA; Schema: public; Owner: postgres
--

-- INSERT INTO pm_document (fkcontext, fkschedule, fkclassification, fkcurrentversion, fkparent, fkauthor, fkmainapprover, sname, tdescription, ntype, skeywords, ddateproduction, ddeadline, bflagdeadlinealert, bflagdeadlineexpired, ndaysbefore, bhasapproved) VALUES (15093, NULL, 32, 15094, NULL, 13, 13, 'Prevenção de acidentes', 'Utilizado como instrução para prevenção de acdentes.', 2821, 'acidente', '2009-07-10 00:00:00', '2009-07-10 00:00:00', 1, 1, 1, 1);
-- INSERT INTO pm_document (fkcontext, fkschedule, fkclassification, fkcurrentversion, fkparent, fkauthor, fkmainapprover, sname, tdescription, ntype, skeywords, ddateproduction, ddeadline, bflagdeadlinealert, bflagdeadlineexpired, ndaysbefore, bhasapproved) VALUES (15096, NULL, 29, 15097, NULL, 13, 13, 'Comunicação de acidente de trabalho', 'Comunicação de acidente', 2826, 'comunicação', '2009-07-10 00:00:00', '2009-07-10 00:00:00', 1, 1, 1, 1);
-- INSERT INTO pm_document (fkcontext, fkschedule, fkclassification, fkcurrentversion, fkparent, fkauthor, fkmainapprover, sname, tdescription, ntype, skeywords, ddateproduction, ddeadline, bflagdeadlinealert, bflagdeadlineexpired, ndaysbefore, bhasapproved) VALUES (15104, NULL, 32, 15105, NULL, 13, 13, 'Plano de ação XX', 'Ação para acidente.', 2837, 'acidente', '2009-07-10 00:00:00', '2009-07-10 00:00:00', 1, 1, 0, 1);


--
-- TOC entry 3155 (class 0 OID 9034808)
-- Dependencies: 2019
-- Data for Name: pm_document_revision_history; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3156 (class 0 OID 9034816)
-- Dependencies: 2020
-- Data for Name: pm_instance_comment; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3157 (class 0 OID 9034824)
-- Dependencies: 2022
-- Data for Name: pm_instance_content; Type: TABLE DATA; Schema: public; Owner: postgres
--

-- INSERT INTO pm_instance_content (fkinstance, tcontent) VALUES (15094, '');
-- INSERT INTO pm_instance_content (fkinstance, tcontent) VALUES (15097, '');
-- INSERT INTO pm_instance_content (fkinstance, tcontent) VALUES (15105, '');


--
-- TOC entry 3158 (class 0 OID 9034830)
-- Dependencies: 2023
-- Data for Name: pm_process_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

-- INSERT INTO pm_process_user (fkprocess, fkuser) VALUES (15051, 13);


--
-- TOC entry 3123 (class 0 OID 9034594)
-- Dependencies: 1981
-- Data for Name: pm_register; Type: TABLE DATA; Schema: public; Owner: postgres
--

-- INSERT INTO pm_register (fkcontext, fkclassification, fkdocument, fkresponsible, sname, nperiod, nvalue, sstorageplace, sstoragetype, sindexingtype, sdisposition, sprotectionrequirements, sorigin) VALUES (15095, 33, 15096, 13, 'Comunicação de acidente de trabalho', 7803, 12, 'Enfermaria', 'Arquivo', 'Alfabética', 'Arquivo morto', 'Acesso somente de pessoal autorizado', 'Setor de Segurança');


--
-- TOC entry 3159 (class 0 OID 9034833)
-- Dependencies: 2024
-- Data for Name: pm_register_readers; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3124 (class 0 OID 9034602)
-- Dependencies: 1982
-- Data for Name: pm_template; Type: TABLE DATA; Schema: public; Owner: postgres
--

-- INSERT INTO pm_template (fkcontext, sname, ncontexttype, spath, sfilename, tdescription, skeywords, nfilesize) VALUES (15107, 'Plano de Ação', 2837, '/mnt/realiso/newisms/instances/saas/qualyohs/files/fe8bde82ec3e86a7ee793d5e4eb63d49.aef', 'PLANO DE AÇÃO XX.docx', 'Documento utilizado para elaboração de um plano de ação.', 'ação', 10178);


--
-- TOC entry 3160 (class 0 OID 9034840)
-- Dependencies: 2025
-- Data for Name: pm_template_best_practice; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3161 (class 0 OID 9034843)
-- Dependencies: 2026
-- Data for Name: pm_template_content; Type: TABLE DATA; Schema: public; Owner: postgres
--

-- INSERT INTO pm_template_content (fkcontext, tcontent) VALUES (15107, '');


--
-- TOC entry 3125 (class 0 OID 9034610)
-- Dependencies: 1983
-- Data for Name: rm_area; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO rm_area (fkcontext, fkpriority, fktype, fkparent, fkresponsible, sname, tdescription, sdocument, nvalue) VALUES (15050, 5, 1, NULL, 13, 'Produção', '', NULL, 4);


--
-- TOC entry 3126 (class 0 OID 9034617)
-- Dependencies: 1984
-- Data for Name: rm_asset; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO rm_asset (fkcontext, fkcategory, fkresponsible, fksecurityresponsible, sname, tdescription, sdocument, ncost, nvalue, blegality, tjustification) VALUES (15082, 15052, 13, 13, 'Montagem de gabarito', '', NULL, 0, 4, 0, '');


--
-- TOC entry 3162 (class 0 OID 9034849)
-- Dependencies: 2027
-- Data for Name: rm_asset_asset; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3163 (class 0 OID 9034852)
-- Dependencies: 2028
-- Data for Name: rm_asset_value; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO rm_asset_value (fkasset, fkvaluename, fkparametername, tjustification) VALUES (15082, 3, 1, '');


--
-- TOC entry 3127 (class 0 OID 9034627)
-- Dependencies: 1985
-- Data for Name: rm_best_practice; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO rm_best_practice (fkcontext, fksectionbestpractice, sname, tdescription, ncontroltype, sclassification, timplementationguide, tmetric, sdocument, tjustification) VALUES (15085, 15041, 'Instrução de Trabalho 00', 'Prevenir acidentes com ferramentas manuais', 1, ':prevention:', '', '', '', NULL);
INSERT INTO rm_best_practice (fkcontext, fksectionbestpractice, sname, tdescription, ncontroltype, sclassification, timplementationguide, tmetric, sdocument, tjustification) VALUES (15087, 15040, 'Norma Regulamentadora 6', 'Equipamentos de Proteção Individual - EPI', 2, ':prevention:', '', '', '', NULL);


--
-- TOC entry 3164 (class 0 OID 9034858)
-- Dependencies: 2029
-- Data for Name: rm_best_practice_event; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3165 (class 0 OID 9034861)
-- Dependencies: 2030
-- Data for Name: rm_best_practice_standard; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO rm_best_practice_standard (fkcontext, fkstandard, fkbestpractice) VALUES (15086, 15049, 15085);
INSERT INTO rm_best_practice_standard (fkcontext, fkstandard, fkbestpractice) VALUES (15088, 15047, 15087);


--
-- TOC entry 3128 (class 0 OID 9034634)
-- Dependencies: 1986
-- Data for Name: rm_category; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO rm_category (fkcontext, fkparent, sname, tdescription) VALUES (15000, NULL, 'Ruído', '');
INSERT INTO rm_category (fkcontext, fkparent, sname, tdescription) VALUES (15002, NULL, 'Vibração', '');
INSERT INTO rm_category (fkcontext, fkparent, sname, tdescription) VALUES (15004, NULL, 'Radiação não ionizante', '');
INSERT INTO rm_category (fkcontext, fkparent, sname, tdescription) VALUES (15007, NULL, 'Poeiras vegetais.', '');
INSERT INTO rm_category (fkcontext, fkparent, sname, tdescription) VALUES (15009, NULL, 'Nevoas, gases e vapores.', '');
INSERT INTO rm_category (fkcontext, fkparent, sname, tdescription) VALUES (15013, NULL, 'Fungos', '');
INSERT INTO rm_category (fkcontext, fkparent, sname, tdescription) VALUES (15016, NULL, 'Acidente', '');
INSERT INTO rm_category (fkcontext, fkparent, sname, tdescription) VALUES (15025, NULL, 'Esforço físico', '');
INSERT INTO rm_category (fkcontext, fkparent, sname, tdescription) VALUES (15029, NULL, 'Monotonia e repetitividade', '');
INSERT INTO rm_category (fkcontext, fkparent, sname, tdescription) VALUES (15032, NULL, 'Choque elétrico', '');
INSERT INTO rm_category (fkcontext, fkparent, sname, tdescription) VALUES (15052, NULL, 'Risco de acidente', '');
INSERT INTO rm_category (fkcontext, fkparent, sname, tdescription) VALUES (15053, NULL, 'Risco Químico', '');
INSERT INTO rm_category (fkcontext, fkparent, sname, tdescription) VALUES (15054, NULL, 'Risco ergonômico', '');
INSERT INTO rm_category (fkcontext, fkparent, sname, tdescription) VALUES (15055, NULL, 'Risco físico', '');
INSERT INTO rm_category (fkcontext, fkparent, sname, tdescription) VALUES (15056, NULL, 'Risco biológico', '');


--
-- TOC entry 3129 (class 0 OID 9034640)
-- Dependencies: 1987
-- Data for Name: rm_control; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO rm_control (fkcontext, fktype, fkresponsible, sname, tdescription, sdocument, sevidence, ndaysbefore, bisactive, bflagimplalert, bflagimplexpired, ddatedeadline, ddateimplemented, nimplementationstate, bimplementationislate, brevisionislate, btestislate, befficiencynotok, btestnotok) VALUES (15089, 26, 13, 'Utilização de EPIs', 'Prevenir acidentes com a utilização de EPIs', NULL, 'Treinamento, relatório de auditoria, ficha de entrega de EPI, inspeção.', 0, 1, 0, 0, '2009-07-10 00:00:00', '2009-07-10 00:00:00', 0, 0, 0, 0, 0, 0);


--
-- TOC entry 3166 (class 0 OID 9034864)
-- Dependencies: 2031
-- Data for Name: rm_control_best_practice; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO rm_control_best_practice (fkcontext, fkbestpractice, fkcontrol) VALUES (15090, 15085, 15089);
INSERT INTO rm_control_best_practice (fkcontext, fkbestpractice, fkcontrol) VALUES (15091, 15087, 15089);


--
-- TOC entry 3167 (class 0 OID 9034867)
-- Dependencies: 2032
-- Data for Name: rm_control_cost; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3168 (class 0 OID 9034875)
-- Dependencies: 2033
-- Data for Name: rm_control_efficiency_history; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO rm_control_efficiency_history (fkcontrol, ddatetodo, ddateaccomplishment, nrealefficiency, nexpectedefficiency, bincident) VALUES (15089, '2009-07-10 00:00:00', '2009-07-10 14:21:53', 4, 4, 0);


--
-- TOC entry 3169 (class 0 OID 9034882)
-- Dependencies: 2034
-- Data for Name: rm_control_test_history; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3130 (class 0 OID 9034664)
-- Dependencies: 1988
-- Data for Name: rm_event; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO rm_event (fkcontext, fktype, fkcategory, sdescription, tobservation, bpropagate, timpact) VALUES (15001, 25, 15000, 'Perda Auditiva', '', 0, '');
INSERT INTO rm_event (fkcontext, fktype, fkcategory, sdescription, tobservation, bpropagate, timpact) VALUES (15003, NULL, 15002, 'Problemas na coluna vertebral.', '', 0, '');
INSERT INTO rm_event (fkcontext, fktype, fkcategory, sdescription, tobservation, bpropagate, timpact) VALUES (15005, NULL, 15004, 'Lesões nos olhos.', '', 0, '');
INSERT INTO rm_event (fkcontext, fktype, fkcategory, sdescription, tobservation, bpropagate, timpact) VALUES (15006, NULL, 15004, 'Queimaduras.', '', 0, '');
INSERT INTO rm_event (fkcontext, fktype, fkcategory, sdescription, tobservation, bpropagate, timpact) VALUES (15008, 23, 15007, 'Insuficiência respiratória.', '', 0, '');
INSERT INTO rm_event (fkcontext, fktype, fkcategory, sdescription, tobservation, bpropagate, timpact) VALUES (15010, NULL, 15009, 'Irritação vias respiratórias', '', 0, '');
INSERT INTO rm_event (fkcontext, fktype, fkcategory, sdescription, tobservation, bpropagate, timpact) VALUES (15011, NULL, 15009, 'Dermatites', '', 0, '');
INSERT INTO rm_event (fkcontext, fktype, fkcategory, sdescription, tobservation, bpropagate, timpact) VALUES (15012, NULL, 15009, 'Irritação ocular', '', 0, '');
INSERT INTO rm_event (fkcontext, fktype, fkcategory, sdescription, tobservation, bpropagate, timpact) VALUES (15014, NULL, 15013, 'Dermatites', '', 0, '');
INSERT INTO rm_event (fkcontext, fktype, fkcategory, sdescription, tobservation, bpropagate, timpact) VALUES (15015, NULL, 15013, 'Irritação vias respiratórias', '', 0, '');
INSERT INTO rm_event (fkcontext, fktype, fkcategory, sdescription, tobservation, bpropagate, timpact) VALUES (15017, NULL, 15016, 'Corte', '', 0, '');
INSERT INTO rm_event (fkcontext, fktype, fkcategory, sdescription, tobservation, bpropagate, timpact) VALUES (15018, NULL, 15016, 'Luxação', '', 0, '');
INSERT INTO rm_event (fkcontext, fktype, fkcategory, sdescription, tobservation, bpropagate, timpact) VALUES (15019, NULL, 15016, 'Esmagamento', '', 0, '');
INSERT INTO rm_event (fkcontext, fktype, fkcategory, sdescription, tobservation, bpropagate, timpact) VALUES (15020, NULL, 15016, 'Fratura', '', 0, '');
INSERT INTO rm_event (fkcontext, fktype, fkcategory, sdescription, tobservation, bpropagate, timpact) VALUES (15021, NULL, 15016, 'Amputação', '', 0, '');
INSERT INTO rm_event (fkcontext, fktype, fkcategory, sdescription, tobservation, bpropagate, timpact) VALUES (15022, NULL, 15016, 'Acidente sem afastamento', '', 0, '');
INSERT INTO rm_event (fkcontext, fktype, fkcategory, sdescription, tobservation, bpropagate, timpact) VALUES (15023, NULL, 15016, 'Queimadura', '', 0, '');
INSERT INTO rm_event (fkcontext, fktype, fkcategory, sdescription, tobservation, bpropagate, timpact) VALUES (15024, NULL, 15016, 'Choque elétrico', '', 0, '');
INSERT INTO rm_event (fkcontext, fktype, fkcategory, sdescription, tobservation, bpropagate, timpact) VALUES (15026, NULL, 15025, 'Problemas coluna vertebral', '', 0, '');
INSERT INTO rm_event (fkcontext, fktype, fkcategory, sdescription, tobservation, bpropagate, timpact) VALUES (15027, NULL, 15025, 'Distensão muscular', '', 0, '');
INSERT INTO rm_event (fkcontext, fktype, fkcategory, sdescription, tobservation, bpropagate, timpact) VALUES (15028, NULL, 15025, 'Problemas ergonômicos', '', 0, '');
INSERT INTO rm_event (fkcontext, fktype, fkcategory, sdescription, tobservation, bpropagate, timpact) VALUES (15030, NULL, 15029, 'Tendinite', '', 0, '');
INSERT INTO rm_event (fkcontext, fktype, fkcategory, sdescription, tobservation, bpropagate, timpact) VALUES (15031, NULL, 15029, 'Distenção muscular', '', 0, '');
INSERT INTO rm_event (fkcontext, fktype, fkcategory, sdescription, tobservation, bpropagate, timpact) VALUES (15033, NULL, 15032, 'Lesões nos olhos', '', 0, '');
INSERT INTO rm_event (fkcontext, fktype, fkcategory, sdescription, tobservation, bpropagate, timpact) VALUES (15034, NULL, 15032, 'Queimadura', '', 0, '');
INSERT INTO rm_event (fkcontext, fktype, fkcategory, sdescription, tobservation, bpropagate, timpact) VALUES (15035, NULL, 15032, 'Parada cardio respiratória', '', 0, '');
INSERT INTO rm_event (fkcontext, fktype, fkcategory, sdescription, tobservation, bpropagate, timpact) VALUES (15036, NULL, 15032, 'Acidente sem afastamento', '', 0, '');
INSERT INTO rm_event (fkcontext, fktype, fkcategory, sdescription, tobservation, bpropagate, timpact) VALUES (15057, 25, 15055, 'Ruído', '', 0, 'Perda auditiva');
INSERT INTO rm_event (fkcontext, fktype, fkcategory, sdescription, tobservation, bpropagate, timpact) VALUES (15058, 25, 15055, 'Vibração', '', 0, 'Problemas na coluna vertebral');
INSERT INTO rm_event (fkcontext, fktype, fkcategory, sdescription, tobservation, bpropagate, timpact) VALUES (15059, 25, 15055, 'Radiação não ionizante', '', 0, 'Lesão nos olhos');
INSERT INTO rm_event (fkcontext, fktype, fkcategory, sdescription, tobservation, bpropagate, timpact) VALUES (15060, 25, 15055, 'Radiação não ionizante', '', 0, 'Queimadura');
INSERT INTO rm_event (fkcontext, fktype, fkcategory, sdescription, tobservation, bpropagate, timpact) VALUES (15061, 25, 15055, 'Poeiras vegetais', '', 0, 'Insuficiência respiratória');
INSERT INTO rm_event (fkcontext, fktype, fkcategory, sdescription, tobservation, bpropagate, timpact) VALUES (15062, 25, 15053, 'Nevoas, gases e vapores', '', 0, 'Irritação vias respiratórias');
INSERT INTO rm_event (fkcontext, fktype, fkcategory, sdescription, tobservation, bpropagate, timpact) VALUES (15063, 25, 15053, 'Nevoas, gases e vapores', '', 0, 'Dermatites');
INSERT INTO rm_event (fkcontext, fktype, fkcategory, sdescription, tobservation, bpropagate, timpact) VALUES (15064, 25, 15053, 'Nevoas, gases e vapores', '', 0, 'Irritação ocular');
INSERT INTO rm_event (fkcontext, fktype, fkcategory, sdescription, tobservation, bpropagate, timpact) VALUES (15065, NULL, 15056, 'Fungos', '', 0, 'Dermatites');
INSERT INTO rm_event (fkcontext, fktype, fkcategory, sdescription, tobservation, bpropagate, timpact) VALUES (15066, 25, 15056, 'Fungos', '', 0, 'Irritação vias respiratórias');
INSERT INTO rm_event (fkcontext, fktype, fkcategory, sdescription, tobservation, bpropagate, timpact) VALUES (15067, NULL, 15052, 'Acidente', '', 0, 'Corte');
INSERT INTO rm_event (fkcontext, fktype, fkcategory, sdescription, tobservation, bpropagate, timpact) VALUES (15068, NULL, 15052, 'Acidente', '', 0, 'Luxação');
INSERT INTO rm_event (fkcontext, fktype, fkcategory, sdescription, tobservation, bpropagate, timpact) VALUES (15069, NULL, 15052, 'Acidente', '', 0, 'Esmagamento');
INSERT INTO rm_event (fkcontext, fktype, fkcategory, sdescription, tobservation, bpropagate, timpact) VALUES (15070, NULL, 15052, 'Acidente', '', 0, 'Fratura');
INSERT INTO rm_event (fkcontext, fktype, fkcategory, sdescription, tobservation, bpropagate, timpact) VALUES (15071, NULL, 15052, 'Acidente', '', 0, 'Queimadura');
INSERT INTO rm_event (fkcontext, fktype, fkcategory, sdescription, tobservation, bpropagate, timpact) VALUES (15072, NULL, 15052, 'Acidente', '', 0, 'Choque elétrico');
INSERT INTO rm_event (fkcontext, fktype, fkcategory, sdescription, tobservation, bpropagate, timpact) VALUES (15073, NULL, 15054, 'Esforço físico', '', 0, 'Problemas na coluna vertebral');
INSERT INTO rm_event (fkcontext, fktype, fkcategory, sdescription, tobservation, bpropagate, timpact) VALUES (15074, NULL, 15054, 'Esforço físico', '', 0, 'Distenção muscular');
INSERT INTO rm_event (fkcontext, fktype, fkcategory, sdescription, tobservation, bpropagate, timpact) VALUES (15075, NULL, 15054, 'Esforço físico', '', 0, 'Problemas posturais');
INSERT INTO rm_event (fkcontext, fktype, fkcategory, sdescription, tobservation, bpropagate, timpact) VALUES (15076, NULL, 15054, 'Monotonia e repetitividade', '', 0, 'Tendinite');
INSERT INTO rm_event (fkcontext, fktype, fkcategory, sdescription, tobservation, bpropagate, timpact) VALUES (15077, NULL, 15054, 'Monotonia e repetitividade', '', 0, 'Distensão muscular');
INSERT INTO rm_event (fkcontext, fktype, fkcategory, sdescription, tobservation, bpropagate, timpact) VALUES (15078, NULL, 15052, 'Choque elétrico', '', 0, 'Lesão nos olhos');
INSERT INTO rm_event (fkcontext, fktype, fkcategory, sdescription, tobservation, bpropagate, timpact) VALUES (15079, NULL, 15052, 'Choque elétrico', '', 0, 'Queimadura');
INSERT INTO rm_event (fkcontext, fktype, fkcategory, sdescription, tobservation, bpropagate, timpact) VALUES (15080, NULL, 15052, 'Choque elétrico', '', 0, 'Parada cardio respiratória');
INSERT INTO rm_event (fkcontext, fktype, fkcategory, sdescription, tobservation, bpropagate, timpact) VALUES (15081, NULL, 15052, 'Choque elétrico', '', 0, 'Acidente com afastamento');


--
-- TOC entry 3170 (class 0 OID 9034890)
-- Dependencies: 2035
-- Data for Name: rm_parameter_name; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO rm_parameter_name (pkparametername, sname, nweight) VALUES (1, 'Severidade', 1);


--
-- TOC entry 3171 (class 0 OID 9034896)
-- Dependencies: 2037
-- Data for Name: rm_parameter_value_name; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO rm_parameter_value_name (pkvaluename, nvalue, simportance, simpact, sriskprobability) VALUES (1, 1, 'Baixa', 'Desprezível', 'Remota');
INSERT INTO rm_parameter_value_name (pkvaluename, nvalue, simportance, simpact, sriskprobability) VALUES (2, 2, 'Média', 'Crítico', 'Provável');
INSERT INTO rm_parameter_value_name (pkvaluename, nvalue, simportance, simpact, sriskprobability) VALUES (3, 3, 'Alta', 'Catastrófico', 'Frequente');


--
-- TOC entry 3131 (class 0 OID 9034672)
-- Dependencies: 1989
-- Data for Name: rm_process; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO rm_process (fkcontext, fkpriority, fktype, fkarea, fkresponsible, sname, tdescription, sdocument, nvalue, tjustification) VALUES (15051, 12, 8, 15050, 13, 'Produtivo', '', NULL, 4, NULL);


--
-- TOC entry 3132 (class 0 OID 9034679)
-- Dependencies: 1990
-- Data for Name: rm_process_asset; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO rm_process_asset (fkcontext, fkprocess, fkasset) VALUES (15083, 15051, 15082);


--
-- TOC entry 3172 (class 0 OID 9034906)
-- Dependencies: 2039
-- Data for Name: rm_rc_parameter_value_name; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO rm_rc_parameter_value_name (pkrcvaluename, nvalue, srcprobability, scontrolimpact) VALUES (1, 0, 'Não reduz', 'Não reduz');
INSERT INTO rm_rc_parameter_value_name (pkrcvaluename, nvalue, srcprobability, scontrolimpact) VALUES (2, 1, 'Reduz um nível', 'Reduz um nível');
INSERT INTO rm_rc_parameter_value_name (pkrcvaluename, nvalue, srcprobability, scontrolimpact) VALUES (3, 2, 'Reduz dois níveis', 'Reduz dois níveis');


--
-- TOC entry 3133 (class 0 OID 9034682)
-- Dependencies: 1991
-- Data for Name: rm_risk; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO rm_risk (fkcontext, fkprobabilityvaluename, fktype, fkevent, fkasset, sname, tdescription, nvalue, nvalueresidual, tjustification, nacceptmode, nacceptstate, sacceptjustification, ncost, timpact) VALUES (15084, 2, NULL, 15067, 15082, 'Acidente', '', 5, 4, '', 0, 0, NULL, 0, 'Corte');


--
-- TOC entry 3134 (class 0 OID 9034693)
-- Dependencies: 1992
-- Data for Name: rm_risk_control; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO rm_risk_control (fkcontext, fkprobabilityvaluename, fkrisk, fkcontrol, tjustification) VALUES (15092, 1, 15084, 15089, '');


--
-- TOC entry 3173 (class 0 OID 9034915)
-- Dependencies: 2041
-- Data for Name: rm_risk_control_value; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO rm_risk_control_value (fkriskcontrol, fkparametername, fkrcvaluename, tjustification) VALUES (15092, 1, 2, '');


--
-- TOC entry 3135 (class 0 OID 9034699)
-- Dependencies: 1993
-- Data for Name: rm_risk_limits; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO rm_risk_limits (fkcontext, nlow, nhigh, nmidlow, nmidhigh) VALUES (14, 3, 6, 0, 0);


--
-- TOC entry 3174 (class 0 OID 9034921)
-- Dependencies: 2042
-- Data for Name: rm_risk_value; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO rm_risk_value (fkrisk, fkvaluename, fkparametername, tjustification) VALUES (15084, 2, 1, '');


--
-- TOC entry 3136 (class 0 OID 9034702)
-- Dependencies: 1994
-- Data for Name: rm_section_best_practice; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (15037, NULL, 'Acordos coletivos');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (15038, NULL, 'Estadual');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (15039, NULL, 'Municipal');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (15040, NULL, 'Federal');
INSERT INTO rm_section_best_practice (fkcontext, fkparent, sname) VALUES (15041, NULL, 'Requisitos Internos');


--
-- TOC entry 3137 (class 0 OID 9034705)
-- Dependencies: 1995
-- Data for Name: rm_standard; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO rm_standard (fkcontext, sname, tdescription, tapplication, tobjective) VALUES (15045, 'Acordo coletivo', '', '', '');
INSERT INTO rm_standard (fkcontext, sname, tdescription, tapplication, tobjective) VALUES (15046, 'Lei Estadual', '', '', '');
INSERT INTO rm_standard (fkcontext, sname, tdescription, tapplication, tobjective) VALUES (15047, 'Lei federal', '', '', '');
INSERT INTO rm_standard (fkcontext, sname, tdescription, tapplication, tobjective) VALUES (15048, 'Lei municipal', '', '', '');
INSERT INTO rm_standard (fkcontext, sname, tdescription, tapplication, tobjective) VALUES (15049, 'Requisitos Internos', '', '', '');


--
-- TOC entry 3175 (class 0 OID 9035131)
-- Dependencies: 2093
-- Data for Name: wkf_alert; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (1, 15099, 13, 13, '', 9224, 1, '2009-07-10 14:08:57');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (2, 15099, 13, 13, '', 9226, 1, '2009-07-10 14:09:02');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (3, 15093, 13, 13, ' ', 9213, 1, '2009-07-14 13:54:23');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (4, 15096, 13, 13, ' ', 9213, 1, '2009-07-14 13:54:24');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (5, 15104, 13, 13, ' ', 9213, 1, '2009-07-14 13:54:24');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (6, 15084, 13, 13, '', 9222, 1, '2009-07-14 13:54:24');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (7, 15103, 13, 13, ' ', 9219, 1, '2009-07-16 14:20:28');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (8, 15106, 13, 13, ' ', 9219, 1, '2009-07-16 14:20:29');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (9, 15084, 13, 13, '', 9222, 1, '2009-07-16 14:20:29');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (10, 15103, 13, 13, ' ', 9220, 1, '2009-07-21 17:09:08');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (11, 15106, 13, 13, ' ', 9220, 1, '2009-07-21 17:09:09');
INSERT INTO wkf_alert (pkalert, fkcontext, fkreceiver, fkcreator, tjustification, ntype, bemailsent, ddate) VALUES (12, 15084, 13, 13, '', 9222, 1, '2009-07-21 17:09:09');


--
-- TOC entry 3176 (class 0 OID 9035142)
-- Dependencies: 2095
-- Data for Name: wkf_control_efficiency; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO wkf_control_efficiency (fkcontrolefficiency, fkschedule, nrealefficiency, nexpectedefficiency, svalue1, svalue2, svalue3, svalue4, svalue5, tmetric) VALUES (15089, 1, 4, 4, 'Acidente grave', 'Acidente médio', 'Acidente leve', 'luxação', 'não houve acidente', 'Inspeção, auditoria.');


--
-- TOC entry 3177 (class 0 OID 9035150)
-- Dependencies: 2096
-- Data for Name: wkf_control_test; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3178 (class 0 OID 9035156)
-- Dependencies: 2097
-- Data for Name: wkf_schedule; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO wkf_schedule (pkschedule, dstart, dend, ntype, nperiodicity, nbitmap, nweek, nday, dnextoccurrence, ndaystofinish, ddatelimit) VALUES (1, '2009-07-13 00:00:00', NULL, 7803, 0, 585, 0, 13, '2009-10-13 00:00:00', 1, NULL);


--
-- TOC entry 3179 (class 0 OID 9035167)
-- Dependencies: 2099
-- Data for Name: wkf_task; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (2, 15103, 13, 13, 2226, 0, 1, '2009-07-10 14:18:48', '2009-07-10 14:18:04');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (1, 15101, 13, 13, 2231, 0, 1, '2009-07-10 14:21:53', '2009-07-10 00:00:00');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (3, 15106, 13, 13, 2226, 0, 1, '2009-07-10 14:29:20', '2009-07-10 14:27:48');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (4, 15103, 13, 13, 2227, 1, 1, NULL, '2009-07-16 14:20:28');
INSERT INTO wkf_task (pktask, fkcontext, fkcreator, fkreceiver, nactivity, bvisible, bemailsent, ddateaccomplished, ddatecreated) VALUES (5, 15106, 13, 13, 2227, 0, 1, '2009-07-16 14:23:14', '2009-07-16 14:20:29');


--
-- TOC entry 3180 (class 0 OID 9035175)
-- Dependencies: 2101
-- Data for Name: wkf_task_schedule; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO wkf_task_schedule (fkcontext, nactivity, fkschedule, balertsent, nalerttype) VALUES (15089, 2208, 1, 0, 9206);


-- Completed on 2009-07-27 15:29:05 BRT

--
-- PostgreSQL database dump complete
--

-- Non Conformity
INSERT INTO isms_non_conformity_types(pkclassification, sname) VALUES (62251, 'Auditoria Interna');
INSERT INTO isms_non_conformity_types(pkclassification, sname) VALUES (62252, 'Auditoria Externa');
INSERT INTO isms_non_conformity_types(pkclassification, sname) VALUES (62253, 'Controle de Segurança');

