CREATE OR REPLACE FUNCTION delete_user()
  RETURNS "trigger" AS
'BEGIN

IF (TG_OP = ''DELETE'') THEN
    UPDATE isms_context_date
    SET nUserId = 0
    WHERE nUserId = OLD.fkContext;
END IF;
RETURN NULL;
END'
  LANGUAGE 'plpgsql' VOLATILE;

CREATE TRIGGER delete_user
  AFTER DELETE
  ON isms_user
  FOR EACH ROW
  EXECUTE PROCEDURE delete_user();

CREATE OR REPLACE FUNCTION recalculation_of_area_by_area()
  RETURNS "trigger" AS
'DECLARE
    miVchg double precision;
BEGIN

IF (TG_OP = ''UPDATE'') THEN
    miVchg := (NEW.nValue - OLD.nValue);
    IF (miVchg <> 0) THEN
        UPDATE rm_area
        SET nValue = get_area_value(fkContext)
        WHERE fkContext = OLD.fkParent;
    END IF;
END IF;
RETURN NEW;
END'
  LANGUAGE 'plpgsql' VOLATILE;

CREATE TRIGGER recalculation_of_area_by_area
  AFTER UPDATE
  ON rm_area
  FOR EACH ROW
  EXECUTE PROCEDURE recalculation_of_area_by_area();

CREATE OR REPLACE FUNCTION recalculation_of_area_by_process_delete()
  RETURNS "trigger" AS
'BEGIN

IF (TG_OP = ''DELETE'') THEN
    UPDATE rm_area
    SET nValue = get_area_value(fkContext)
    WHERE fkContext = OLD.fkArea;
END IF;
RETURN NEW;
END'
  LANGUAGE 'plpgsql' VOLATILE;

CREATE TRIGGER recalculation_of_area_by_process_delete
  AFTER DELETE
  ON rm_process
  FOR EACH ROW
  EXECUTE PROCEDURE recalculation_of_area_by_process_delete();

CREATE OR REPLACE FUNCTION recalculation_of_area_by_process_update()
  RETURNS "trigger" AS
'DECLARE
    miArea integer;
BEGIN
   miArea := NEW.fkArea;
IF (TG_OP = ''UPDATE'')  THEN
  UPDATE rm_area SET nValue = get_area_value(fkContext) WHERE fkContext = miArea;
  
  IF (NEW.fkArea <> OLD.fkArea) THEN
      UPDATE rm_area SET nValue = get_area_value(fkContext) WHERE fkContext = OLD.fkArea;
  END IF;
END IF;
RETURN NEW;
END'
  LANGUAGE 'plpgsql' VOLATILE;

CREATE TRIGGER recalculation_of_area_by_process_update
  AFTER UPDATE
  ON rm_process
  FOR EACH ROW
  EXECUTE PROCEDURE recalculation_of_area_by_process_update();

CREATE OR REPLACE FUNCTION recalculation_of_asset_association()
  RETURNS "trigger" AS
'DECLARE
    miAsset integer;
BEGIN

IF (TG_OP = ''DELETE'') THEN
    miAsset := OLD.fkDependent;
ELSE
   IF (NEW.fkDependent IS NOT NULL) THEN
       miAsset := NEW.fkDependent;
   ELSE
       miAsset := OLD.fkDependent;
   END IF;
END IF;

UPDATE rm_asset
  SET nValue = get_asset_value(fkContext)
     WHERE fkContext = miAsset;

RETURN NEW;
END'
  LANGUAGE 'plpgsql' VOLATILE;

CREATE TRIGGER recalculation_of_asset_association
  AFTER INSERT OR UPDATE OR DELETE
  ON rm_asset_asset
  FOR EACH ROW
  EXECUTE PROCEDURE recalculation_of_asset_association();

CREATE OR REPLACE FUNCTION recalculation_of_asset_for_risk_delete()
  RETURNS "trigger" AS
'DECLARE
BEGIN
IF (TG_OP = ''DELETE'') THEN
    UPDATE rm_asset
    SET nValue = get_asset_value(fkContext)
    WHERE fkContext = OLD.fkAsset;
END IF;
RETURN NEW;
END'
  LANGUAGE 'plpgsql' VOLATILE;

CREATE TRIGGER recalculation_of_asset_for_risk_delete
  AFTER DELETE
  ON rm_risk
  FOR EACH ROW
  EXECUTE PROCEDURE recalculation_of_asset_for_risk_delete();

CREATE OR REPLACE FUNCTION recalculation_of_context_value()
  RETURNS "trigger" AS
'DECLARE    
BEGIN
IF (TG_OP = ''UPDATE'') THEN
       IF( (NEW.nState = 2705) AND (OLD.nState <> 2705) )  THEN
            IF (OLD.nType = 2801) THEN
			UPDATE rm_area
			   SET nValue = get_area_value(fkContext)
			   WHERE fkParent = NEW.pkContext;
            END IF;
	    IF (OLD.nType =  2802) THEN
                       UPDATE rm_area
                           SET nValue = get_area_value(fkContext)
                           WHERE fkContext IN
			      (SELECT fkArea FROM rm_process WHERE fkContext = NEW.pkContext);
	    END IF;
	    IF (OLD.nType =  2803) THEN
		       UPDATE rm_process
			   SET nValue = get_process_value(fkContext)
			   WHERE fkContext IN
			     (SELECT fkProcess FROM rm_process_asset WHERE fkAsset = NEW.pkContext);
	    END IF;
	    IF (OLD.nType =  2804) THEN
		UPDATE rm_asset
                    SET nValue = get_asset_value(fkContext)
                       WHERE fkContext IN 
                           (SELECT fkAsset FROM rm_risk WHERE fkContext = NEW.pkContext);
	    END IF;
	    IF (OLD.nType =  2805) THEN
		       UPDATE rm_risk SET
			  nValue = get_risk_value(fkContext),
			  nValueResidual = get_risk_value_residual(fkContext)
                          WHERE fkContext IN 
                                (SELECT fkRisk FROM rm_risk_control WHERE fkControl = NEW.pkContext);
	    END IF;
         END IF;
        
	 IF ((NEW.nState <> 2705) AND (OLD.nState = 2705)) THEN
		IF (OLD.nType =  2801 ) THEN --area
			UPDATE rm_area
			  SET nValue = get_area_value(fkContext)
			  WHERE fkContext = NEW.pkContext;
                END IF;
		IF (OLD.nType =  2802 )  THEN --process
		      UPDATE rm_process
			SET nValue = get_process_value(fkContext)
			WHERE fkContext = NEW.pkContext;
                END IF;
		IF (OLD.nType = 2803 ) THEN --asset
		       UPDATE rm_asset
			  SET nValue = get_asset_value(fkContext)
			  WHERE fkContext = NEW.pkContext;
	        END IF;
		IF (OLD.nType =  2804 ) THEN --risk
		       UPDATE rm_risk 
			  SET nValue = get_risk_value(fkContext),
			  nValueResidual = get_risk_value_residual(fkContext)
			  WHERE fkContext = NEW.pkContext;
		       UPDATE rm_asset
                           SET nValue = get_asset_value(fkContext)
                           WHERE fkContext IN 
                               (SELECT fkAsset FROM rm_risk WHERE fkContext = NEW.pkContext);
                END IF;
		IF (OLD.nType = 2805 ) THEN --control
		    UPDATE rm_risk SET
			nValue = get_risk_value(fkContext),
			nValueResidual = get_risk_value_residual(fkContext)
			WHERE fkContext IN 
			   (SELECT fkRisk FROM rm_risk_control rc WHERE fkControl = NEW.pkContext);
		END IF;
          END IF;

END IF;
RETURN NEW;
END;'
  LANGUAGE 'plpgsql' VOLATILE;

CREATE TRIGGER recalculation_of_context_value
  AFTER UPDATE
  ON isms_context
  FOR EACH ROW
  EXECUTE PROCEDURE recalculation_of_context_value();

CREATE OR REPLACE FUNCTION recalculation_of_dependent_asset()
  RETURNS "trigger" AS
'DECLARE
    miVchg double precision;
BEGIN

IF (TG_OP = ''UPDATE'') THEN
    miVchg := (NEW.nValue - OLD.nValue);
    IF (miVchg <> 0) THEN
      UPDATE rm_asset
        SET nValue = get_asset_value(fkContext)
        WHERE
            fkContext IN 
                (select  fkDependent from rm_asset_asset where fkAsset = OLD.fkContext);

    END IF;
END IF;
RETURN NEW;
END'
  LANGUAGE 'plpgsql' VOLATILE;

CREATE TRIGGER recalculation_of_dependent_asset
  AFTER UPDATE
  ON rm_asset
  FOR EACH ROW
  EXECUTE PROCEDURE recalculation_of_dependent_asset();

CREATE OR REPLACE FUNCTION recalculation_of_process()
  RETURNS "trigger" AS
'DECLARE
  miVchg double precision;
BEGIN
IF (TG_OP = ''UPDATE'') THEN
    miVchg := (NEW.nValue - OLD.nValue);
    IF (miVchg <> 0) THEN
        UPDATE rm_process
        SET nValue = get_process_value(rm_process.fkContext)
        FROM rm_process_asset pa
        WHERE rm_process.fkContext = pa.fkProcess
        AND pa.fkAsset = OLD.fkContext;
    END IF;
END IF;
RETURN NEW;
END;'
  LANGUAGE 'plpgsql' VOLATILE;

CREATE TRIGGER recalculation_of_process
  AFTER UPDATE
  ON rm_asset
  FOR EACH ROW
  EXECUTE PROCEDURE recalculation_of_process();

CREATE OR REPLACE FUNCTION recalculation_of_process_association()
  RETURNS "trigger" AS
'DECLARE
 miProcess integer;
BEGIN

IF( TG_OP = ''DELETE'') THEN
  miProcess= OLD.fkProcess;
ELSE
  miProcess= NEW.fkProcess;
END IF;

  UPDATE rm_process
   -- atualiza valor do risco do processo como sendo o valor do seu maior ativo
    SET nValue = get_process_value(fkContext)
    WHERE fkContext = miProcess;
RETURN NEW;
END;'
  LANGUAGE 'plpgsql' VOLATILE;

CREATE TRIGGER recalculation_of_process_association
  AFTER INSERT OR UPDATE OR DELETE
  ON rm_process_asset
  FOR EACH ROW
  EXECUTE PROCEDURE recalculation_of_process_association();

CREATE OR REPLACE FUNCTION recalculation_of_risk_for_insert()
  RETURNS "trigger" AS
'DECLARE
  mbAcceptRisk boolean;
BEGIN

IF (TG_OP = ''INSERT'') THEN

    UPDATE rm_risk 
      SET nValue = get_risk_value(fkContext),
      nValueResidual = get_risk_value_residual(fkContext)
      WHERE fkContext = NEW.fkContext;
    UPDATE rm_asset
	  SET nValue = get_asset_value(fkContext)
	  WHERE fkContext = NEW.fkAsset;
END IF;
RETURN NEW;
END;'
  LANGUAGE 'plpgsql' VOLATILE;

CREATE TRIGGER recalculation_of_risk_for_insert
  AFTER INSERT
  ON rm_risk
  FOR EACH ROW
  EXECUTE PROCEDURE recalculation_of_risk_for_insert();

CREATE OR REPLACE FUNCTION recalculation_of_risk_for_update()
  RETURNS "trigger" AS
'DECLARE
    mbAcceptRisk boolean;
    mbAssetChanged boolean;
    mfValueResidual double precision;
    mbCondition boolean;
BEGIN
IF (TG_OP = ''UPDATE'') THEN
    IF(NEW.nAcceptMode <> OLD.nAcceptMode) THEN
        mbAcceptRisk := true;
    ELSE
        mbAcceptRisk := false;
    END IF;
    IF(NEW.fkAsset <> OLD.fkAsset) THEN
        mbAssetChanged := true;
    ELSE
        mbAssetChanged := false;
    END IF;
    mfValueResidual := NEW.nValueResidual - OLD.nValueResidual;
    IF(  ( (mfValueResidual <> 0) AND 
           ( (NEW.nValueResidual IS NOT NULL) OR (OLD.nValueResidual IS NOT NULL) )  
         ) 
	 OR 
	 (mbAcceptRisk)
	 OR
	 (mbAssetChanged)
      ) 
    THEN
	mbCondition := true;
    ELSE
	mbCondition := false;
    END IF;
   IF (mbCondition) THEN
       UPDATE rm_risk 
       SET nValue = get_risk_value(fkContext),
       nValueResidual = get_risk_value_residual(fkContext)
       WHERE fkContext = NEW.fkContext;
       
       UPDATE rm_asset
          SET nValue = get_asset_value(fkContext)
          WHERE fkContext = NEW.fkAsset;

       IF (mbAssetChanged) THEN
          UPDATE rm_asset
          SET nValue = get_asset_value(fkContext)
          WHERE fkContext = OLD.fkAsset;
       END IF;
    END IF;
END IF;
RETURN NEW;
END'
  LANGUAGE 'plpgsql' VOLATILE;

CREATE TRIGGER recalculation_of_risk_for_update
  AFTER UPDATE
  ON rm_risk
  FOR EACH ROW
  EXECUTE PROCEDURE recalculation_of_risk_for_update();

CREATE OR REPLACE FUNCTION recauculate_risk_for_risk_control_association()
  RETURNS "trigger" AS
'DECLARE
BEGIN
    IF (TG_OP = ''DELETE'') THEN
	UPDATE rm_risk
	  SET nvalue = get_risk_value(OLD.fkRisk),
          nValueResidual = get_risk_value_residual(OLD.fkRisk)
             WHERE fkContext = OLD.fkRisk;
    END IF;
RETURN NEW;
END'
  LANGUAGE 'plpgsql' VOLATILE;

CREATE TRIGGER recauculate_risk_for_risk_control_association
  AFTER DELETE
  ON rm_risk_control
  FOR EACH ROW
  EXECUTE PROCEDURE recauculate_risk_for_risk_control_association();

CREATE OR REPLACE FUNCTION recalculation_of_risks_values()
  RETURNS "trigger" AS
'DECLARE
      mbAcceptRisk boolean;
      mfValueResidual double precision;
      mbCondition boolean;
  BEGIN
    IF TG_OP = ''UPDATE'' AND NEW.bIsActive != OLD.bIsActive THEN
      UPDATE rm_risk SET
        nValue = get_risk_value(NEW.fkContext),
        nValueResidual = get_risk_value_residual(NEW.fkContext)
      WHERE fkContext IN (
        SELECT fkRisk
        FROM rm_risk_control rc
        WHERE fkControl = NEW.fkContext
      );
    END IF;
    RETURN NEW;
  END'
  LANGUAGE 'plpgsql' VOLATILE;

CREATE TRIGGER recalculation_of_risks_values
  AFTER UPDATE
  ON rm_control
  FOR EACH ROW
  EXECUTE PROCEDURE recalculation_of_risks_values();

CREATE OR REPLACE FUNCTION update_control_is_active() RETURNS "trigger"
    AS '
DECLARE
  mbHasRevision INTEGER;
  mbHasTest INTEGER;
  mdDateLimit TIMESTAMP;
  mdDateTodo TIMESTAMP;
  mdDateNext TIMESTAMP;
  mbImplementationIsLate INTEGER;
  mbEfficiencyNotOk INTEGER;
  mbRevisionIsLate INTEGER;
  mbTestNotOk INTEGER;
  mbTestIsLate INTEGER;
BEGIN
  
  mbImplementationIsLate = 0;
  mbEfficiencyNotOk = 0;
  mbRevisionIsLate = 0;
  mbTestNotOk = 0;
  mbTestIsLate = 0;
  
  -- Testa a data de implementação
  IF NEW.dDateImplemented IS NULL AND NEW.dDateDeadline <= NOW() THEN
    mbImplementationIsLate = 1;
  END IF;

  -- Testa se tem revisão
  SELECT COUNT(*)
  INTO mbHasRevision
  FROM
    isms_config cfg,
    wkf_control_efficiency ce
  WHERE
    cfg.pkConfig = 404
    AND cfg.sValue = ''1''
    AND ce.fkControlEfficiency = NEW.fkContext;

  -- Se tem revisão, testa se ela tá ok e em dia.
  IF mbHasRevision = 1 THEN
    SELECT CASE WHEN COUNT(*) = 0 THEN 1 ELSE 0 END
    INTO mbEfficiencyNotOk
    FROM wkf_control_efficiency ce
    WHERE
      ce.fkControlEfficiency = NEW.fkContext
      AND (
        ce.nRealEfficiency >= ce.nExpectedEfficiency
        OR ce.nRealEfficiency = 0
      );
    
    SELECT COUNT(*)
    INTO mbRevisionIsLate
    FROM
      wkf_control_efficiency ce
      JOIN wkf_schedule s ON (s.pkschedule = ce.fkschedule)
      JOIN wkf_task_schedule ts ON (ts.fkschedule = s.pkschedule)

      JOIN (

      SELECT * FROM wkf_task WHERE pktask IN (
	SELECT tunique.pktask FROM 
	(SELECT max(pktask) AS pktask, fkcontext, max(ddatecreated) AS ddatecreated FROM wkf_task GROUP BY fkcontext ORDER BY pktask) tunique)

      )  t ON (t.nactivity = ts.nactivity AND t.fkcontext = ts.fkcontext AND t.bvisible = 1)

      JOIN (
        SELECT
          s2.pkschedule AS schedule_id,
          COUNT(*) AS pendant_tasks
        FROM
          wkf_schedule s2
          JOIN wkf_task_schedule ts2 ON (ts2.fkschedule = s2.pkschedule)
          JOIN wkf_task t2 ON (t2.nactivity = ts2.nactivity AND t2.fkcontext = ts2.fkcontext AND t2.bvisible = 1)
        GROUP BY s2.pkschedule
      ) p ON (p.schedule_id = s.pkschedule)
    WHERE
      ce.fkControlEfficiency = NEW.fkContext
      AND (
        p.pendant_tasks > 1
        OR (
          p.pendant_tasks = 1
          AND CURRENT_DATE > s.dDateLimit
        )
      );
    IF mbRevisionIsLate > 1 THEN mbRevisionIsLate = 1; END IF;
    
  END IF;

  -- Testa se tem teste
  SELECT COUNT(*)
  INTO mbHasTest
  FROM
    isms_config cfg,
    wkf_control_test ct
  WHERE
    cfg.pkConfig = 405
    AND cfg.sValue = ''1''
    AND ct.fkControlTest = NEW.fkContext;

  -- Se tem teste, testa se ele tá ok e em dia.
  IF mbHasTest = 1 THEN
    SELECT CASE WHEN COUNT(*) = 1 THEN 1 ELSE 0 END
    INTO mbTestNotOk
    FROM
      wkf_control_test ct
      LEFT JOIN rm_control_test_history th ON (
        th.fkControl = ct.fkControlTest
        AND NOT EXISTS (
          SELECT *
          FROM rm_control_test_history th2
          WHERE
            th2.fkControl = th.fkControl
            AND th2.dDateAccomplishment > th.dDateAccomplishment
        )
      )
    WHERE
      ct.fkControlTest = NEW.fkContext
      AND th.bTestedValue = 0;
    
    SELECT COUNT(*)
    INTO mbTestIsLate
    FROM
      wkf_control_test ct
      JOIN wkf_schedule s ON (s.pkschedule = ct.fkschedule)
      JOIN wkf_task_schedule ts ON (ts.fkschedule = s.pkschedule)
      JOIN wkf_task t ON (t.nactivity = ts.nactivity AND t.fkcontext = ts.fkcontext AND t.bvisible = 1)
      JOIN (
        SELECT
          s2.pkschedule AS schedule_id,
          COUNT(*) AS pendant_tasks
        FROM
          wkf_schedule s2
          JOIN wkf_task_schedule ts2 ON (ts2.fkschedule = s2.pkschedule)
          JOIN wkf_task t2 ON (t2.nactivity = ts2.nactivity AND t2.fkcontext = ts2.fkcontext AND t2.bvisible = 1)
        GROUP BY s2.pkschedule
      ) p ON (p.schedule_id = s.pkschedule)
    WHERE
      ct.fkControlTest = NEW.fkContext
      AND (
        p.pendant_tasks > 1
        OR (
          p.pendant_tasks = 1
          AND CURRENT_DATE > s.dDateLimit
        )
      );
    IF mbTestIsLate > 1 THEN mbTestIsLate = 1; END IF;
    
  END IF;

  -- Atualiza as flags e cria as seeds de Não-Conformidade que forem necessárias
  NEW.bImplementationIsLate = mbImplementationIsLate;
  NEW.bEfficiencyNotOk = mbEfficiencyNotOk;
  NEW.bRevisionIsLate = mbRevisionIsLate;
  NEW.bTestNotOk = mbTestNotOk;
  NEW.bTestIsLate = mbTestIsLate;

  IF mbImplementationIsLate + mbEfficiencyNotOk + mbRevisionIsLate + mbTestNotOk + mbTestIsLate > 0 THEN
    NEW.bIsActive = 0;
    
    IF OLD.bImplementationIsLate = 0 AND NEW.bImplementationIsLate = 1 THEN
      IF NOT EXISTS (SELECT * FROM ci_nc_seed WHERE fkControl = NEW.fkContext AND nDeactivationReason = 1) THEN
        INSERT INTO ci_nc_seed (fkControl,nDeactivationReason,dDateSent) VALUES (NEW.fkContext,1,NOW());
      END IF;
    END IF;
    
    IF OLD.bEfficiencyNotOk = 0 AND NEW.bEfficiencyNotOk = 1 THEN
      IF NOT EXISTS (SELECT * FROM ci_nc_seed WHERE fkControl = NEW.fkContext AND nDeactivationReason = 2) THEN
        INSERT INTO ci_nc_seed (fkControl,nDeactivationReason,dDateSent) VALUES (NEW.fkContext,2,NOW());
      END IF;
    END IF;
    
    IF OLD.bRevisionIsLate = 0 AND NEW.bRevisionIsLate = 1 THEN
      IF NOT EXISTS (SELECT * FROM ci_nc_seed WHERE fkControl = NEW.fkContext AND nDeactivationReason = 4) THEN
        INSERT INTO ci_nc_seed (fkControl,nDeactivationReason,dDateSent) VALUES (NEW.fkContext,4,NOW());
      END IF;
    END IF;
    
    IF OLD.bTestNotOk = 0 AND NEW.bTestNotOk = 1 THEN
      IF NOT EXISTS (SELECT * FROM ci_nc_seed WHERE fkControl = NEW.fkContext AND nDeactivationReason = 8) THEN
        INSERT INTO ci_nc_seed (fkControl,nDeactivationReason,dDateSent) VALUES (NEW.fkContext,8,NOW());
      END IF;
    END IF;
    
    IF OLD.bTestIsLate = 0 AND NEW.bTestIsLate = 1 THEN
      IF NOT EXISTS (SELECT * FROM ci_nc_seed WHERE fkControl = NEW.fkContext AND nDeactivationReason = 16) THEN
        INSERT INTO ci_nc_seed (fkControl,nDeactivationReason,dDateSent) VALUES (NEW.fkContext,16,NOW());
      END IF;
    END IF;
    
  ELSE
    NEW.bIsActive = 1;
  END IF;

  RETURN NEW;
END
'
  LANGUAGE plpgsql;

CREATE TRIGGER update_control_is_active
  BEFORE UPDATE ON rm_control
  FOR EACH ROW
  EXECUTE PROCEDURE update_control_is_active();

CREATE OR REPLACE FUNCTION update_alert_sent()
  RETURNS "trigger" AS
'BEGIN
  IF OLD.ddateaccomplished IS NULL AND NEW.ddateaccomplished IS NOT NULL THEN
    PERFORM *
    FROM
      wkf_schedule s
      JOIN wkf_task_schedule ts ON (ts.fkschedule = s.pkschedule)
      JOIN wkf_task t ON (t.nactivity = ts.nactivity AND t.fkcontext = ts.fkcontext AND t.bvisible = 1)
      JOIN (
        SELECT
          s.pkschedule AS schedule_id,
          COUNT(*) AS pendant_tasks
        FROM
          wkf_schedule s
          JOIN wkf_task_schedule ts ON (ts.fkschedule = s.pkschedule)
          JOIN wkf_task t ON (t.nactivity = ts.nactivity AND t.fkcontext = ts.fkcontext AND t.bvisible = 1)
        WHERE
          ts.fkcontext = NEW.fkcontext
          AND ts.nactivity = NEW.nactivity
        GROUP BY s.pkschedule
      ) p ON (p.schedule_id = s.pkschedule)
    WHERE
      ts.fkcontext = NEW.fkcontext
      AND ts.nactivity = NEW.nactivity
      AND p.pendant_tasks > 1
      OR (
        p.pendant_tasks = 1
        AND CURRENT_DATE > s.dDateLimit
      );
      
    IF NOT FOUND THEN
      UPDATE wkf_task_schedule
      SET bAlertSent = 0
      WHERE
        nactivity = NEW.nactivity
        AND fkcontext = NEW.fkcontext;
    END IF;
  END IF;
  RETURN NEW;
END'
  LANGUAGE 'plpgsql' VOLATILE;

CREATE TRIGGER update_alert_sent
  AFTER UPDATE
  ON wkf_task
  FOR EACH ROW
  EXECUTE PROCEDURE update_alert_sent();


