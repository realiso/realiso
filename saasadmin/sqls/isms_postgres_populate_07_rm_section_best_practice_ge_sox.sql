INSERT INTO isms_context (pkContext,nType,nState) VALUES (10361,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (10361,null,'CobiT');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10361,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10361,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10362,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (10362,10361,'Plan and Organise');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10362,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10362,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10363,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (10363,10362,'PO1Define a Strategic IT Plan');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10363,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10363,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10370,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (10370,10362,'PO2Define the Information Architecture');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10370,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10370,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10375,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (10375,10362,'PO3 Determine Technological Direction');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10375,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10375,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10381,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (10381,10362,'PO4 Define the IT Processes, Organisation and Relationships');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10381,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10381,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10397,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (10397,10362,'PO5 Manage the IT Investment');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10397,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10397,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10403,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (10403,10362,'PO6 Communicate Management Aims and Direction');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10403,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10403,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10409,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (10409,10362,'PO7 Manage IT Human Resources');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10409,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10409,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10418,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (10418,10362,'PO8 Manage Quality');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10418,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10418,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10425,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (10425,10362,'PO9 Assess and Manage IT Risks');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10425,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10425,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10432,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (10432,10362,'PO10 Manage Projects');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10432,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10432,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10447,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (10447,10361,'Acquire and Implement');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10447,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10447,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10448,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (10448,10447,'AI1 Identify Automated Solutions');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10448,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10448,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10453,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (10453,10447,'AI2 Acquire and Maintain Application Software');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10453,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10453,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10464,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (10464,10447,'AI3 Acquire and Maintain Technology Infrastructure');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10464,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10464,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10469,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (10469,10447,'AI4 Enable Operation and Use');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10469,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10469,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10474,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (10474,10447,'AI5 Procure IT Resources');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10474,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10474,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10479,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (10479,10447,'AI6 Manage Changes');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10479,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10479,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10485,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (10485,10447,'AI7 Install and Accredit Solutions and Changes');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10485,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10485,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10495,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (10495,10361,'Deliver and Support');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10495,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10495,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10496,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (10496,10495,'DS1 Define and Manage Service Levels');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10496,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10496,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10503,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (10503,10495,'DS2 Manage Third-party Services');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10503,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10503,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10508,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (10508,10495,'DS3 Manage Performance and Capacity');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10508,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10508,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10514,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (10514,10495,'DS4 Ensure Continuous Service');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10514,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10514,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10525,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (10525,10495,'DS5 Ensure Systems Security');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10525,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10525,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10537,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (10537,10495,'DS6 Identify and Allocate Costs');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10537,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10537,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10542,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (10542,10495,'DS7 Educate and Train Users');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10542,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10542,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10546,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (10546,10495,'DS8 Manage Service Desk and Incidents');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10546,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10546,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10552,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (10552,10495,'DS9 Manage the Configuration');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10552,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10552,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10556,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (10556,10495,'DS10 Manage Problems');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10556,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10556,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10561,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (10561,10495,'DS11 Manage Data');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10561,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10561,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10568,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (10568,10495,'DS12 Manage the Physical Environment');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10568,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10568,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10574,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (10574,10495,'DS13 Manage Operations');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10574,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10574,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10580,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (10580,10361,'Monitor and Evaluate');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10580,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10580,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10581,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (10581,10580,'ME1 Monitor and Evaluate IT Performance');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10581,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10581,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10588,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (10588,10580,'ME2 Monitor and Evaluate Internal Control');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10588,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10588,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10596,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (10596,10580,'ME3 Ensure Compliance With External Requirements');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10596,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10596,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10602,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (10602,10580,'ME4 Provide IT Governance');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10602,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10602,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10610,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (10610,null,'COSO - General Entity-Wide Controls');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10610,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10610,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10611,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (10611,10610,'B - Commitment to Competence');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10611,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10611,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10614,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (10614,10610,'C - Board of Directors or Audit Committee');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10614,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10614,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10634,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (10634,10610,'D - Management Philosophy and Operating Style');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10634,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10634,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10645,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (10645,10610,'E - Organizational Structure');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10645,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10645,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10651,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (10651,10610,'F - Assignment of Authority and Responsibility');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10651,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10651,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10656,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (10656,10610,'A - Integrity and Ethical Values');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10656,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10656,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10669,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (10669,10610,'G - Human Resource Policies and Practices');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10669,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10669,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10676,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (10676,10610,'H - Entity-Wide Objectives');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10676,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10676,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10681,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (10681,10610,'I - Activity-Wide Objectives');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10681,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10681,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10687,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (10687,10610,'J - Risk Assessment');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10687,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10687,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10695,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (10695,10610,'K - Managing Change');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10695,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10695,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10698,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (10698,10610,'L - Control Activities');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10698,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10698,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10702,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (10702,10610,'M - Information');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10702,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10702,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10710,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (10710,10610,'N - Communication');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10710,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10710,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10725,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (10725,10610,'O - Ongoing Monitoring');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10725,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10725,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10738,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (10738,10610,'P - Separate Evaluation');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10738,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10738,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (10746,2811,2702);
INSERT INTO rm_section_best_practice (fkContext,fkParent,sName) VALUES (10746,10610,'Q - Reporting');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10746,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,10746,2902,NOW());

