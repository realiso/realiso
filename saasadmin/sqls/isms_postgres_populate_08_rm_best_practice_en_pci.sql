INSERT INTO isms_context (pkContext,nType,nState) VALUES (6263,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6263,6262,1,'1.1','Establish firewall and router
configuration standards','1.1 Obtain and inspect the firewall and router configuration
standards and other documentation specified below to verify
that standards are complete.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6263,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6263,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6265,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6265,6262,1,'1.1.1','A formal process for approving
and testing all network connections and
changes to the firewall and router
configurations','1.1.1 Verify that there is a formal process for testing and
approval of all network connections and changes to
firewall and router configurations.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6265,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6265,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6267,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6267,6262,1,'1.1.2','Current network diagram with all
connections to cardholder data, including
any wireless networks','1.1.2.a Verify that a current network diagram (for
example, one that shows cardholder data flows over the
network) exists and that it documents all connections to
cardholder data, including any wireless networks.
1.1.2.b Verify that the diagram is kept current.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6267,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6267,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6269,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6269,6262,1,'1.1.3','Requirements for a firewall at
each Internet connection and between any
demilitarized zone (DMZ) and the internal
network zone','1.1.3 Verify that firewall configuration standards include
requirements for a firewall at each Internet connection and
between any DMZ and the internal network zone. Verify
that the current network diagram is consistent with the
firewall configuration standards.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6269,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6269,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6271,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6271,6262,1,'1.1.4','Description of groups, roles, and
responsibilities for logical management of
network components','1.1.4 Verify that firewall and router configuration
standards include a description of groups, roles, and
responsibilities for logical management of network
components.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6271,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6271,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6273,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6273,6262,1,'1.1.5','Documentation and business
justification for use of all services,
protocols, and ports allowed, including
documentation of security features
implemented for those protocols
considered to be insecure','1.1.5.a Verify that firewall and router configuration
standards include a documented list of services, protocols
and ports necessary for businessâfor example, hypertext
transfer protocol (HTTP) and Secure Sockets Layer (SSL),
Secure Shell (SSH), and Virtual Private Network (VPN)
protocols.
1.1.5.b Identify insecure services, protocols, and ports
allowed; and verify they are necessary and that security
features are documented and implemented by examining
firewall and router configuration standards and settings for
each service. An example of an insecure service, protocol,
or port is FTP, which passes user credentials in clear-text.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6273,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6273,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6275,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6275,6262,1,'1.1.6','Requirement to review firewall
and router rule sets at least every six
months','1.1.6.a Verify that firewall and router configuration
standards require review of firewall and router rule sets at
least every six months.
1.1.6.b Obtain and examine documentation to verify that
the rule sets are reviewed at least every six months.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6275,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6275,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6277,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6277,6262,1,'1.2','Build a firewall configuration that
restricts connections between untrusted
networks and any system components in
the cardholder data environment.','Examine firewall and router configurations to verify
that connections are restricted between untrusted networks
and system components in the cardholder data
environment.

Note: An âuntrusted networkâ is any network that is external to the networks belonging to the entity under
review, and/or which is out of the entity''s ability to control or manage.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6277,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6277,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6279,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6279,6262,1,'1.2.1','Restrict inbound and outbound
traffic to that which is necessary for the
cardholder data environment.','1.2.1.a Verify that inbound and outbound traffic is limited
to that which is necessary for the cardholder data
environment, and that the restrictions are documented.
1.2.1.b Verify that all other inbound and outbound traffic is
specifically denied, for example by using an explicit âdeny
allâ or an implicit deny after allow statement.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6279,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6279,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6281,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6281,6262,1,'1.2.2','Secure and synchronize router
configuration files.','1.2.2 Verify that router configuration files are secure
and synchronizedâfor example, running configuration
files (used for normal running of the routers) and start-up
configuration files (used when machines are re-booted),
have the same, secure configurations.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6281,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6281,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6283,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6283,6262,1,'1.2.3','Install perimeter firewalls between
any wireless networks and the cardholder
data environment, and configure these
firewalls to deny or control (if such traffic is
necessary for business purposes) any
traffic from the wireless environment into
the cardholder data environment.','1.2.3 Verify that there are perimeter firewalls installed
between any wireless networks and systems that store
cardholder data, and that these firewalls deny or control (if
such traffic is necessary for business purposes) any traffic
from the wireless environment into the cardholder data
environment.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6283,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6283,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6285,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6285,6262,1,'1.3','Prohibit direct public access between
the Internet and any system component in
the cardholder data environment.','1.3 Examine firewall and router configurations, as detailed
below, to determine that there is no direct access between
the Internet and system components, including the choke
router at the Internet, the DMZ router and firewall, the DMZ
cardholder segment, the perimeter router, and the internal
cardholder network segment.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6285,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6285,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6287,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6287,6262,1,'1.3.1','Implement a DMZ to limit inbound
and outbound traffic to only protocols that
are necessary for the cardholder data
environment.','1.3.1 Verify that a DMZ is implemented to limit inbound
and outbound traffic to only protocols that are necessary
for the cardholder data environment.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6287,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6287,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6289,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6289,6262,1,'1.3.2','Limit inbound Internet traffic to IP
addresses within the DMZ.','1.3.2 Verify that inbound Internet traffic is limited to IP
addresses within the DMZ.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6289,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6289,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6291,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6291,6262,1,'1.3.3','Do not allow any direct routes
inbound or outbound for traffic between
the Internet and the cardholder data
environment.','1.3.3 Verify there is no direct route inbound or outbound
for traffic between the Internet and the cardholder data
environment.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6291,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6291,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6293,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6293,6262,1,'1.3.4','Do not allow internal addresses to
pass from the Internet into the DMZ.','1.3.4 Verify that internal addresses cannot pass from the
Internet into the DMZ.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6293,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6293,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6295,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6295,6262,1,'1.3.5','Restrict outbound traffic from the
cardholder data environment to the
Internet such that outbound traffic can
only access IP addresses within the DMZ.','1.3.5 Verify that outbound traffic from the cardholder data
environment to the Internet can only access IP addresses
within the DMZ.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6295,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6295,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6297,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6297,6262,1,'1.3.6','Implement stateful inspection,
also known as dynamic packet filtering.
(That is, only âestablishedâ connections
are allowed into the network.)','1.3.6 Verify that the firewall performs stateful inspection
(dynamic packet filtering). [Only established connections
should be allowed in, and only if they are associated with
a previously established session (run a port scanner on all
TCP ports with âsyn resetâ or âsyn ackâ bits setâa
response means packets are allowed through even if they
are not part of a previously established session).]');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6297,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6297,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6299,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6299,6262,1,'1.3.7','Place the database in an internal
network zone, segregated from the DMZ.','1.3.7 Verify that the database is on an internal network
zone, segregated from the DMZ.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6299,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6299,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6301,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6301,6262,1,'1.3.8','Implement IP masquerading to
prevent internal addresses from being
translated and revealed on the Internet,
using RFC 1918 address space. Use
network address translation (NAT)
technologiesâfor example, port address
translation (PAT).','1.3.8 For the sample of firewall and router components,
verify that NAT or other technology using RFC 1918
address space is used to restrict broadcast of IP
addresses from the internal network to the Internet (IP
masquerading).');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6301,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6301,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6303,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6303,6262,1,'1.4','1.4 Install personal firewall software on
any mobile and/or employee-owned
computers with direct connectivity to the
Internet (for example, laptops used by
employees), which are used to access the
organizationâs network.','1.4.a Verify that mobile and/or employee-owned
computers with direct connectivity to the Internet (for
example, laptops used by employees), and which are used
to access the organizationâs network, have personal firewall
software installed and active.
1.4.b Verify that the personal firewall software is configured
by the organization to specific standards and is not alterable
by mobile computer users.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6303,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6303,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6306,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6306,6305,1,'2.1','Always change vendor-supplied
defaults before installing a system on the
networkâfor example, include passwords,
simple network management protocol
(SNMP) community strings, and elimination
of unnecessary accounts.','2.1 Choose a sample of system components, critical
servers, and wireless access points, and attempt to log on
(with system administrator help) to the devices using default
vendor-supplied accounts and passwords, to verify that
default accounts and passwords have been changed. (Use
vendor manuals and sources on the Internet to find vendorsupplied
accounts/passwords.)');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6306,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6306,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6308,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6308,6305,1,'2.1.1','For wireless environments
connected to the cardholder data
environment or transmitting cardholder
data, change wireless vendor defaults,
including but not limited to default wireless
encryption keys, passwords, and SNMP
community strings. Ensure wireless device
security settings are enabled for strong
encryption technology for authentication
and transmission.','2.1.1 Verify the following regarding vendor default
settings for wireless environments and ensure that all
wireless networks implement strong encryption
mechanisms (for example, AES):

- Encryption keys were changed from default at
installation, and are changed anytime anyone with
knowledge of the keys leaves the company or
changes positions

- Default SNMP community strings on wireless
devices were changed

- Default passwords/passphrases on access points
were changed

- Firmware on wireless devices is updated to support
strong encryption for authentication and
transmission over wireless networks (for example,
WPA/WPA2)

- Other security-related wireless vendor defaults, if
applicable');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6308,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6308,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6310,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6310,6305,1,'2.2','Develop configuration standards for
all system components. Assure that these
standards address all known security
vulnerabilities and are consistent with
industry-accepted system hardening
standards.','2.2.a Examine the organizationâs system configuration
standards for all types of system components and verify the
system configuration standards are consistent with industryaccepted
hardening standardsâfor example, SysAdmin
Audit Network Security (SANS), National Institute of
Standards Technology (NIST), and Center for Internet
Security (CIS).
2.2.b Verify that system configuration standards include
each item below (at 2.2.1 â 2.2.4).
2.2.c Verify that system configuration standards are
applied when new systems are configured.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6310,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6310,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6312,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6312,6305,1,'2.2.1','Implement only one primary
function per server.','2.2.1 For a sample of system components, verify that
only one primary function is implemented per server. For
example, web servers, database servers, and DNS should
be implemented on separate servers.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6312,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6312,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6314,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6314,6305,1,'2.2.2','Disable all unnecessary and
insecure services and protocols (services
and protocols not directly needed to
perform the deviceâs specified function).','2.2.2 For a sample of system components, inspect
enabled system services, daemons, and protocols. Verify
that unnecessary or insecure services or protocols are not
enabled, or are justified and documented as to appropriate
use of the service. For example, FTP is not used, or is
encrypted via SSH or other technology.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6314,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6314,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6316,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6316,6305,1,'2.2.3','2.2.3 Configure system security
parameters to prevent misuse.','2.2.3.a Interview system administrators and/or security
managers to verify that they have knowledge of common
security parameter settings for system components.
2.2.3.b Verify that common security parameter settings
are included in the system configuration standards.
2.2.3.c For a sample of system components, verify that
common security parameters are set appropriately.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6316,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6316,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6318,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6318,6305,1,'2.2.4','Remove all unnecessary
functionality, such as scripts, drivers,
features, subsystems, file systems, and
unnecessary web servers.','2.2.4 For a sample of system components, verify that all
unnecessary functionality (for example, scripts, drivers,
features, subsystems, file systems, etc.) is removed. Verify
enabled functions are documented and support secure
configuration, and that only documented functionality is
present on the sampled machines.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6318,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6318,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6320,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6320,6305,1,'2.3','Encrypt all non-console
administrative access. Use technologies
such as SSH, VPN, or SSL/TLS for webbased
management and other non-console
administrative access.','For a sample of system components, verify that nonconsole
administrative access is encrypted by:

- Observing an administrator log on to each system
to verify that a strong encryption method is invoked
before the administratorâs password is requested;

- Reviewing services and parameter files on systems
to determine that Telnet and other remote log-in
commands are not available for use internally; and

- Verifying that administrator access to the webbased
management interfaces is encrypted with
strong cryptography.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6320,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6320,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6322,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6322,6305,1,'2.4','Shared hosting providers must
protect each entityâs hosted environment
and cardholder data. These providers must
meet specific requirements as detailed in
Appendix A: Additional PCI DSS
Requirements for Shared Hosting Providers.','Perform testing procedures A.1.1 through A.1.4
detailed in Appendix A: Additional PCI DSS Requirements
for Shared Hosting Providers for PCI DSS assessments of
shared hosting providers, to verify that shared hosting
providers protect their entitiesâ (merchants and service
providers) hosted environment and data.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6322,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6322,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6325,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6325,6324,1,'3.1','Keep cardholder data storage to
a minimum. Develop a data retention and
disposal policy. Limit storage amount
and retention time to that which is
required for business, legal, and/or
regulatory purposes, as documented in
the data retention policy.','3.1 Obtain and examine the company policies and
procedures for data retention and disposal, and perform the
following

- Verify that policies and procedures include legal,
regulatory, and business requirements for data
retention, including specific requirements for
retention of cardholder data (for example,
cardholder data needs to be held for X period for Y
business reasons)

- Verify that policies and procedures include
provisions for disposal of data when no longer
needed for legal, regulatory, or business reasons,
including disposal of cardholder data

- Verify that policies and procedures include
coverage for all storage of cardholder data

- Verify that policies and procedures include a
programmatic (automatic) process to remove, at
least on a quarterly basis, stored cardholder data
that exceeds business retention requirements, or,
alternatively, requirements for a review, conducted
at least on a quarterly basis, to verify that stored
cardholder data does not exceed business
retention requirements');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6325,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6325,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6327,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6327,6324,1,'3.2','Do not store sensitive
authentication data after authorization
(even if encrypted).
Sensitive authentication data includes the
data as cited in the Requirements
3.2.1 through 3.2.3.','3.2 If sensitive authentication data is received and
deleted, obtain and review the processes for deleting the
data to verify that the data is unrecoverable.
');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6327,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6327,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6329,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6329,6324,1,'3.2.1','Do not store the full contents of
any track from the magnetic stripe
(located on the back of a card, contained
in a chip, or elsewhere). This data is
alternatively called full track, track, track
1, track 2, and magnetic-stripe data.
Note: In the normal course of business,
the following data elements from the
magnetic stripe may need to be retained:
- The cardholderâs name,
- Primary account number (PAN),
- Expiration date, and
- Service code
To minimize risk, store only these data
elements as needed for business.
Note: See PCI DSS Glossary of Terms,
Abbreviations, and Acronyms for
additional information.','3.2.1 For a sample of system components, examine the
following and verify that the full contents of any track from
the magnetic stripe on the back of card are not stored
under any circumstance:
- Incoming transaction data
- All logs (for example, transaction, history,
debugging, error)
- History files
- Trace files
- Several database schemas
- Database contents');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6329,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6329,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6331,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6331,6324,1,'3.2.2','Do not store the cardverification
code or value (threedigit
or four-digit number printed on
the front or back of a payment
card) used to verify card-notpresent
transactions.
Note: See PCI DSS Glossary of
Terms, Abbreviations, and
Acronyms for additional
information.','3.2.2 For a sample of system components, verify that
the three-digit or four-digit card-verification code or value
printed on the front of the card or the signature panel
(CVV2, CVC2, CID, CAV2 data) is not stored under any
circumstance:
- Incoming transaction data
- All logs (for example, transaction, history,
debugging, error)
- History files
- Trace files
- Several database schemas
- Database contents');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6331,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6331,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6333,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6333,6324,1,'3.2.3','Do not store the personal
identification number (PIN) or the
encrypted PIN block.','3.2.3 For a sample of system components, examine the
following and verify that PINs and encrypted PIN blocks
are not stored under any circumstance:
- Incoming transaction data
- All logs (for example, transaction, history,
debugging, error)
- History files
- Trace files
- Several database schemas
- Database contents');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6333,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6333,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6335,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6335,6324,1,'3.3','Mask PAN when displayed
(the first six and last four digits are
the maximum number of digits to be
displayed).
Notes:
ô This requirement does not apply to
employees and other parties with
a legitimate business need to see
the full PAN.
ô This requirement does not
supersede stricter requirements in
place for displays of cardholder
dataâfor example, for point-ofsale
(POS) receipts.','3.3 Obtain and examine written policies and examine
displays of PAN (for example, on screen, on paper receipts)
to verify that primary account numbers (PANs) are masked
when displaying cardholder data, except for those with a
legitimate business need to see full PAN.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6335,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6335,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6337,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6337,6324,1,'3.4','Render PAN, at minimum,
unreadable anywhere it is stored
(including on portable digital media,
backup media, in logs) by using any
of the following approaches:
- One-way hashes based on
strong cryptography
- Truncation
- Index tokens and pads (pads
must be securely stored)
- Strong cryptography with
associated key-management
processes and procedures
The MINIMUM account information
that must be rendered unreadable is
the PAN.
Notes:
- If for some reason, a company is
unable render the PAN
unreadable, refer to Appendix B:
Compensating Controls.
- âStrong cryptographyâ is defined
in the PCI DSS Glossary of
Terms, Abbreviations, and
Acronyms.','3.4.a Obtain and examine documentation about the system used to protect the PAN, including the vendor, type of system/process, and the encryption algorithms (if applicable). Verify that the PAN is rendered unreadable using one of the following methods:
- One-way hashes based on strong cryptography
- Truncation
- Index tokens and pads, with the pads being
securely stored
- Strong cryptography, with associated keymanagement
processes and procedures
3.4.b Examine several tables or files from a sample of
data repositories to verify the PAN is rendered unreadable
(that is, not stored in plain-text).
3.4.c Examine a sample of removable media (for
example, back-up tapes) to confirm that the PAN is
rendered unreadable.
3.4.d Examine a sample of audit logs to confirm that the
PAN is sanitized or removed from the logs.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6337,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6337,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6339,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6339,6324,1,'3.4.1','If disk encryption is used
(rather than file- or column-level
database encryption), logical
access must be managed
independently of native operating
system access control
mechanisms (for example, by not
using local user account
databases). Decryption keys must not be tied to user accounts.','3.4.1.a If disk encryption is used, verify that logical
access to encrypted file systems is implemented via a
mechanism that is separate from the native operating
systems mechanism (for example, not using local user
account databases).
3.4.1.b Verify that cryptographic keys are stored
securely (for example, stored on removable media that is
adequately protected with strong access controls).
3.4.1.c Verify that cardholder data on removable media
is encrypted wherever stored.
Note: Disk encryption often cannot encrypt removable
media, so data stored on this media will need to be
encrypted separately.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6339,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6339,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6341,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6341,6324,1,'3.5','Protect cryptographic keys
used for encryption of cardholder
data against both disclosure and
misuse','3.5 Verify processes to protect keys used for encryption of
cardholder data against disclosure and misuse');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6341,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6341,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6343,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6343,6324,1,'3.5.1','Restrict access to cryptographic keys to the fewest
number of custodians necessary.','3.5.1 Examine user access lists to verify that access to
keys is restricted to very few custodians.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6343,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6343,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6345,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6345,6324,1,'3.5.2','Store cryptographic keys
securely in the fewest possible
locations and forms.','3.5.2 Examine system configuration files to verify that
keys are stored in encrypted format and that keyencrypting
keys are stored separately from dataencrypting
keys.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6345,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6345,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6347,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6347,6324,1,'3.6','Fully document and
implement all key-management
processes and procedures for
cryptographic keys used for
encryption of cardholder data','3.6.a Verify the existence of key-management procedures
for keys used for encryption of cardholder data.
Note: Numerous industry standards for key management
are available from various resources including NIST, which
can be found at http://csrc.nist.gov.
3.6.b For service providers only: If the service provider
shares keys with their customers for transmission of
cardholder data, verify that the service provider provides
documentation to customers that includes guidance on how
to securely store and change customerâs keys (used to
transmit data between customer and service provider).
3.6.c Examine the key-management procedures...');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6347,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6347,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6349,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6349,6324,1,'3.6.1','Generation of strong
cryptographic keys','3.6.1 Verify that key-management procedures are
implemented to require the generation of strong keys.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6349,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6349,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6351,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6351,6324,1,'3.6.2','Secure cryptographic key
distribution','3.6.2 Verify that key-management procedures are
implemented to require secure key distribution.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6351,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6351,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6353,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6353,6324,1,'3.6.3','Secure cryptographic key
storage','3.6.3 Verify that key-management procedures are
implemented to require secure key storage.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6353,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6353,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6355,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6355,6324,1,'3.6.4','Periodic cryptographic key
changes
- As deemed necessary and
recommended by the
associated application (for
example, re-keying);
preferably automatically
- At least annually','3.6.4 Verify that key-management procedures are
implemented to require periodic key changes at least
annually.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6355,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6355,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6357,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6357,6324,1,'3.6.5','Retirement or replacement
of old or suspected compromised
cryptographic keys','3.6.5.a Verify that key-management procedures are
implemented to require the retirement of old keys (for
example: archiving, destruction, and revocation as
applicable).
3.6.5.b Verify that the key-management procedures are
implemented to require the replacement of known or
suspected compromised keys.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6357,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6357,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6359,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6359,6324,1,'3.6.6','Split knowledge and
establishment of dual control of
cryptographic keys','3.6.6 Verify that key-management procedures are
implemented to require split knowledge and dual control of
keys (for example, requiring two or three people, each
knowing only their own part of the key, to reconstruct the
whole key).');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6359,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6359,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6361,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6361,6324,1,'3.6.7','Prevention of unauthorized
substitution of cryptographic keys','3.6.7 Verify that key-management procedures are
implemented to require the prevention of unauthorized
substitution of keys.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6361,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6361,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6363,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6363,6324,1,'3.6.8','Requirement for
cryptographic key custodians to
sign a form stating that they
understand and accept their keycustodian
responsibilities','3.6.8 Verify that key-management procedures are
implemented to require key custodians to sign a form
specifying that they understand and accept their keycustodian
responsibilities.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6363,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6363,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6366,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6366,6365,1,'4.1','Use strong cryptography and
security protocols such as SSL/TLS or
IPSEC to safeguard sensitive cardholder
data during transmission over open,
public networks.
Examples of open, public networks that
are in scope of the PCI DSS are:
- The Internet,
- Wireless technologies,
- Global System for Mobile
communications (GSM), and
- General Packet Radio Service
(GPRS).','4.1.a Verify the use of encryption (for example, SSL/TLS
or IPSEC) wherever cardholder data is transmitted or
received over open, public networks
- Verify that strong encryption is used during data
transmission
- For SSL implementations:
   -- Verify that the server supports the latest
patched versions.
   -- Verify that HTTPS appears as a part of the
browser Universal Record Locator (URL).
   -- Verify that no cardholder data is required when
HTTPS does not appear in the URL.
- Select a sample of transactions as they are received
and observe transactions as they occur to verify that
cardholder data is encrypted during transit.
- Verify that only trusted SSL/TLS keys/certificates are
accepted.
- Verify that the proper encryption strength is
implemented for the encryption methodology in use.
(Check vendor recommendations/best practices.)');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6366,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6366,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6368,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6368,6365,1,'4.1.1','Ensure wireless networks
transmitting cardholder data or
connected to the cardholder data
environment, use industry best
practices (for example, IEEE 802.11i)
to implement strong encryption for
authentication and transmission.
- For new wireless implementations,
it is prohibited to implement WEP
after March 31, 2009.
- For current wireless
implementations, it is prohibited to
use WEP after June 30, 2010.','4.1.1 For wireless networks transmitting cardholder data
or connected to the cardholder data environment, verify
that industry best practices (for example, IEEE 802.11i)
are used to implement strong encryption for
authentication and transmission.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6368,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6368,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6370,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6370,6365,1,'4.2','Never send unencrypted PANs by
end-user messaging technologies (for
example, e-mail, instant messaging,
chat).','4.2.a Verify that strong cryptography is used whenever
cardholder data is sent via end-user messaging
technologies.
4.2.b Verify the existence of a policy stating that
unencrypted PANs are not to be sent via end-user
messaging technologies.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6370,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6370,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6373,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6373,6372,1,'5.1','Deploy anti-virus software on all
systems commonly affected by
malicious software (particularly personal
computers and servers).','5.1 For a sample of system components including all
operating system types commonly affected by malicious
software, verify that anti-virus software is deployed if
applicable anti-virus technology exists.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6373,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6373,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6375,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6375,6372,1,'5.1.1','Ensure that all anti-virus
programs are capable of detecting,
removing, and protecting against all
known types of malicious software.','5.1.1 For a sample of system components, verify that all
anti-virus programs detect, remove, and protect against all
known types of malicious software (for example, viruses,
Trojans, worms, spyware, adware, and rootkits).');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6375,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6375,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6377,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6377,6372,1,'5.2','5.2 Ensure that all anti-virus
mechanisms are current, actively
running, and capable of generating audit
logs.','5.2 Verify that all anti-virus software is current, actively
running, and capable of generating logs by performing the
following:
5.2.a Obtain and examine the policy and verify that it
requires updating of anti-virus software and definitions.
5.2.b Verify that the master installation of the software
is enabled for automatic updates and periodic scans.
5.2.c For a sample of system components including all
operating system types commonly affected by
malicious software, verify that automatic updates and
periodic scans are enabled.
5.2d For a sample of system components, verify that
antivirus software log generation is enabled and that
such logs are retained in accordance with PCI DSS
Requirement 10.7');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6377,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6377,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6380,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6380,6379,1,'6.1','Ensure that all system
components and software have the latest
vendor-supplied security patches
installed. Install critical security patches
within one month of release.
Note: An organization may consider
applying a risk-based approach to
prioritize their patch installations. For
example, by prioritizing critical
infrastructure (for example, public-facing
devices and systems, databases) higher
than less-critical internal devices, to
ensure high-priority systems and devices
are addressed within one month, and
addressing less critical devices and
systems within three months.','6.1.a For a sample of system components and related
software, compare the list of security patches installed on
each system to the most recent vendor security patch list,
to verify that current vendor patches are installed.
6.1.b Examine policies related to security patch
installation to verify they require installation of all critical
new security patches within one month.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6380,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6380,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6382,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6382,6379,1,'6.2','6.2 Establish a process to identify
newly discovered security vulnerabilities
(for example, subscribe to alert services
freely available on the Internet). Update
configuration standards as required by
PCI DSS Requirement 2.2 to address new
vulnerability issues.','6.2.a Interview responsible personnel to verify that
processes are implemented to identify new security
vulnerabilities.
6.2.b Verify that processes to identify new security
vulnerabilities include using outside sources for security
vulnerability information and updating the system
configuration standards reviewed in Requirement 2.2 as
new vulnerability issues are found.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6382,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6382,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6384,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6384,6379,1,'6.3','Develop software applications in
accordance with PCI DSS (for example,
secure authentication and logging) and
based on industry best practices, and
incorporate information security
throughout the software development life
cycle.','6.3.a Obtain and examine written software development
processes to verify that the processes are based on
industry standards, security is included throughout the life
cycle, and software applications are developed in
accordance with PCI DSS.
6.3.b From an examination of written software
development processes, interviews of software developers,
and examination of relevant data (network configuration
documentation, production and test data, etc.)');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6384,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6384,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6386,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6386,6379,1,'6.3.1','Testing of all security patches,
and system and software configuration
changes before deployment','6.3.1 All changes (including patches) are tested before
being deployed into production.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6386,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6386,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6388,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6388,6379,1,'6.3.1.1','Validation of all input (to
prevent cross-site scripting, injection
flaws, malicious file execution, etc.)','6.3.1.1 Validation of all input (to prevent cross-site
scripting, injection flaws, malicious file execution,
etc.)');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6388,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6388,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6390,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6390,6379,1,'6.3.1.2','Validation of proper error
handling','6.3.1.2 Validation of proper error handling');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6390,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6390,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6392,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6392,6379,1,'6.3.1.3','Validation of secure
cryptographic storage','6.3.1.3 Validation of secure cryptographic storage');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6392,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6392,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6394,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6394,6379,1,'6.3.1.4','Validation of secure
communications','6.3.1.4 Validation of secure communications');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6394,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6394,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6396,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6396,6379,1,'6.3.1.5','Validation of proper rolebased
access control (RBAC)','6.3.1.5 Validation of proper role-based access
control (RBAC)');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6396,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6396,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6398,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6398,6379,1,'6.3.2','Separate development/test and
production environments','6.3.2 The development/test environments are separate
from the production environment, with access control in
place to enforce the separation.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6398,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6398,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6400,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6400,6379,1,'6.3.3','Separation of duties between
development/test and production
environments','6.3.3 There is a separation of duties between
personnel assigned to the development/test
environments and those assigned to the production
environment.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6400,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6400,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6402,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6402,6379,1,'6.3.4','Production data (live PANs) are
not used for testing or development','6.3.4 Production data (live PANs) are not used for
testing and development, or are sanitized before use.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6402,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6402,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6404,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6404,6379,1,'6.3.5','Removal of test data and
accounts before production systems
become active','6.3.5 Test data and accounts are removed before a
production system becomes active.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6404,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6404,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6406,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6406,6379,1,'6.3.6','Removal of custom application
accounts, user IDs, and passwords
before applications become active or are
released to customers','6.3.6 Custom application accounts, user IDs and/or
passwords are removed before system goes into
production or is released to customers.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6406,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6406,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6408,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6408,6379,1,'6.3.7','Review of custom code prior to
release to production or customers in
order to identify any potential coding
vulnerability
Note: This requirement for code reviews
applies to all custom code (both internal
and public-facing), as part of the system
development life cycle required by PCI
DSS Requirement 6.3. Code reviews can
be conducted by knowledgeable internal
personnel or third parties. Web
applications are also subject to additional
controls, if they are public facing, to
address ongoing threats and
vulnerabilities after implementation, as
defined at PCI DSS Requirement 6.6.','6.3.7.a Obtain and review policies to confirm all
custom application code changes for internal applications
must be reviewed (either using manual or automated
processes), as follows:
- Code changes are reviewed by individuals other
then the originating code author, and by
individuals who are knowledgeable in code review
techniques and secure coding practices.
- Appropriate corrections are implemented prior to
release.
- Code review results are reviewed and approved
by management prior to release.
6.3.7.b Obtain and review policies to confirm that all
custom application code changes for web applications
must be reviewed (using either manual or automated
processes) as follows:
- Code changes are reviewed by individuals other
then the originating code author, and by
individuals who are knowledgeable in code review
techniques and secure coding practices.
- Code reviews ensure code is developed according
to secure coding guidelines such as the Open
Web Security Project Guide (see PCI DSS
Requirement 6.5).
- Appropriate corrections are implemented prior to
release.
- Code review results are reviewed and approved
by management prior to release.
6.3.7.c Select a sample of recent custom application
changes and verify that custom application code is
reviewed according to 6.3.7a and 6.3.7b above.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6408,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6408,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6410,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6410,6379,1,'6.4','6.4 Follow change control
procedures for all changes to system
components.','6.4.a Obtain and examine company change-control
procedures related to implementing security patches and
software modifications, and verify that the procedures
require items 6.4.1 â 6.4.4 below.
6.4.b For a sample of system components and recent
changes/security patches, trace those changes back to
related change control documentation.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6410,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6410,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6412,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6412,6379,1,'6.4.1','Documentation of impact','6.4.1 Verify that documentation of customer impact is
included in the change control documentation for each
sampled change.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6412,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6412,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6414,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6414,6379,1,'6.4.2','Management sign-off by
appropriate parties','6.4.2 Verify that management sign-off by appropriate
parties is present for each sampled change.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6414,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6414,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6416,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6416,6379,1,'6.4.3','Testing of operational
functionality','6.4.3 Verify that operational functionality testing is
performed for each sampled change.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6416,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6416,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6418,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6418,6379,1,'6.4.4','Back-out procedures','6.4.4 Verify that back-out procedures are prepared for
each sampled change');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6418,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6418,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6420,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6420,6379,1,'6.5','Develop all web applications
(internal and external, and including web
administrative access to application)
based on secure coding guidelines such
as the Open Web Application Security
Project Guide. Cover prevention of
common coding vulnerabilities in
software development processes, to
include the following:
Note: The vulnerabilities listed at 6.5.1
through 6.5.10 were current in the
OWASP guide when PCI DSS v1.2 was
published. However, if and when the
OWASP guide is updated, the current
version must be used for these
requirements.','6.5.a Obtain and review software development
processes for any web-based applications. Verify that
processes require training in secure coding techniques for
developers, and are based on guidance such as the
OWASP guide (http://www.owasp.org).
6.5.b Interview a sample of developers and obtain
evidence that they are knowledgeable in secure coding
techniques.
6.5.c Verify that processes are in place to ensure that
web applications are not vulnerable to 6.5.X');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6420,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6420,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6422,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6422,6379,1,'6.5.1','Cross-site scripting (XSS)','6.5.1 Cross-site scripting (XSS) (Validate all
parameters before inclusion.)');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6422,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6422,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6424,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6424,6379,1,'6.5.2','Injection flaws, particularly
SQL injection. Also consider LDAP
and Xpath injection flaws as well as
other injection flaws.','6.5.2 Injection flaws, particularly SQL injection
(Validate input to verify user data cannot modify meaning
of commands and queries.)');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6424,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6424,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6426,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6426,6379,1,'6.5.3','Malicious file execution','6.5.3 Malicious file execution (Validate input to verify
application does not accept filenames or files from
users.)');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6426,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6426,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6428,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6428,6379,1,'6.5.4','Insecure direct object
references','6.5.4 Insecure direct object references (Do not expose
internal object references to users.)');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6428,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6428,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6430,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6430,6379,1,'6.5.5','Cross-site request forgery
(CSRF)','6.5.5 Cross-site request forgery (CSRF) (Do not reply
on authorization credentials and tokens automatically
submitted by browsers.)');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6430,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6430,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6432,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6432,6379,1,'6.5.6','Information leakage and
improper error handling','6.5.6 Information leakage and improper error handling
(Do not leak information via error messages or other
means.)');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6432,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6432,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6434,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6434,6379,1,'6.5.7','Broken authentication and
session management','6.5.7 Broken authentication and session management
(Properly authenticate users and protect account
credentials and session tokens.)');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6434,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6434,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6436,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6436,6379,1,'6.5.8','Insecure cryptographic
storage','6.5.8 Insecure cryptographic storage (Prevent
cryptographic flaws.)');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6436,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6436,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6438,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6438,6379,1,'6.5.9','Insecure communications','6.5.9 Insecure communications (Properly encrypt all
authenticated and sensitive communications.)');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6438,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6438,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6440,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6440,6379,1,'6.5.10','Failure to restrict URL access','6.5.10 Failure to restrict URL access (Consistently
enforce access control in presentation layer and business
logic for all URLs.)');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6440,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6440,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6442,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6442,6379,1,'6.6','For public-facing web
applications, address new threats and
vulnerabilities on an ongoing basis and
ensure these applications are protected
against known attacks by either of the
following methods:
- Reviewing public-facing web
applications via manual or
automated application vulnerability
security assessment tools or
methods, at least annually and
after any changes
- Installing a web-application firewall
in front of public-facing web
applications','6.6 For public-facing web applications, ensure that
either one of the following methods are in place as follows:
- Verify that public-facing web applications are
reviewed (using either manual or automated
vulnerability security assessment tools or methods),
as follows:
   -- At least annually
   -- After any changes
   -- By an organization that specializes in application
security
   -- That all vulnerabilities are corrected
   -- That the application is re-evaluated after the
corrections
- Verify that a web-application firewall is in place in
front of public-facing web applications to detect and
prevent web-based attacks.
Note: âAn organization that specializes in application
securityâ can be either a third-party company or an
internal organization, as long as the reviewers specialize
in application security and can demonstrate
independence from the development team.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6442,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6442,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6445,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6445,6444,1,'7.1','Limit access to system
components and cardholder data to only
those individuals whose job requires
such access.','7.1 Obtain and examine written policy for data control,
and verify 7.1.1 to 1.1.4.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6445,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6445,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6447,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6447,6444,1,'7.1.1','Restriction of access rights to
privileged user IDs to least privileges
necessary to perform job
responsibilities','7.1.1 Confirm that access rights for privileged user
IDs are restricted to least privileges necessary to
perform job responsibilities.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6447,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6447,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6449,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6449,6444,1,'7.1.2','Assignment of privileges is
based on individual personnelâs job
classification and function','7.1.2 Confirm that privileges are assigned to
individuals based on job classification and function (also
called ârole-based access controlâ or RBAC).');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6449,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6449,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6451,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6451,6444,1,'7.1.3','Requirement for an
authorization form signed by
management that specifies required
privileges','7.1.3 Confirm that an authorization form is required
for all access, that it must specify required privileges,
and that it must be signed by management.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6451,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6451,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6453,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6453,6444,1,'7.1.4','Implementation of an automated
access control system','7.1.4 Confirm that access controls are implemented
via an automated access control system.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6453,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6453,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6455,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6455,6444,1,'7.2','Establish an access control
system for systems components with
multiple users that restricts access based
on a userâs need to know, and is set to
âdeny allâ unless specifically allowed.','7.2 Examine system settings and vendor
documentation to verify that an access control system is
implemented.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6455,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6455,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6457,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6457,6444,1,'7.2.1','Coverage of all system
components','7.2.1 Confirm that access control systems are in
place on all system components.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6457,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6457,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6459,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6459,6444,1,'7.2.2','Assignment of privileges to
individuals based on job classification
and function','7.2.2 Confirm that access control systems are
configured to enforce privileges assigned to individuals
based on job classification and function.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6459,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6459,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6461,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6461,6444,1,'7.2.3','Default âdeny-allâ setting','7.2.3 Confirm that the access control systems has a
default âdeny-allâ setting.

Note: Some access control systems are set by default to
âallow-all,â thereby permitting access unless/until a rule is
written to specifically deny it.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6461,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6461,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6464,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6464,6463,1,'8.1','Assign all users a unique ID
before allowing them to access system
components or cardholder data.','8.1 Verify that all users are assigned a unique ID for
access to system components or cardholder data.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6464,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6464,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6466,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6466,6463,1,'8.2','In addition to assigning a unique
ID, employ at least one of the following
methods to authenticate all users:
- Password or passphrase
- Two-factor authentication (for
example, token devices, smart
cards, biometrics, or public keys)','8.2 To verify that users are authenticated using unique
ID and additional authentication (for example, a password)
for access to the cardholder data environment, perform the
following:
- Obtain and examine documentation describing the
authentication method(s) used.
- For each type of authentication method used and for
each type of system component, observe an
authentication to verify authentication is functioning
consistent with documented authentication method(s).');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6466,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6466,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6468,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6468,6463,1,'8.3','Incorporate two-factor
authentication for remote access
(network-level access originating from
outside the network) to the network by
employees, administrators, and third
parties. Use technologies such as remote
authentication and dial-in service
(RADIUS); terminal access controller
access control system (TACACS) with
tokens; or VPN (based on SSL/TLS or
IPSEC) with individual certificates.','8.3 To verify that two-factor authentication is
implemented for all remote network access, observe an
employee (for example, an administrator) connecting
remotely to the network and verify that both a password
and an additional authentication item (for example, smart
card, token, PIN) are required.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6468,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6468,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6470,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6470,6463,1,'8.4','Render all passwords
unreadable during transmission and
storage on all system components using
strong cryptography (defined in PCI DSS
Glossary of Terms, Abbreviations, and
Acronyms).Render all passwords
unreadable during transmission and
storage on all system components using
strong cryptography (defined in PCI DSS
Glossary of Terms, Abbreviations, and
Acronyms).','8.4.a For a sample of system components, examine
password files to verify that passwords are unreadable
during transmission and storage.
8.4.b For service providers only, observe password files
to verify that customer passwords are encrypted.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6470,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6470,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6472,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6472,6463,1,'8.5','Ensure proper user authentication
and password management for nonconsumer
users and administrators on all
system components','8.5 Review procedures and interview personnel to
verify that procedures are implemented for user
authentication and password management.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6472,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6472,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6474,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6474,6463,1,'8.5.1','Control addition, deletion, and
modification of user IDs, credentials,
and other identifier objects.','8.5.1.a Select a sample of user IDs, including both
administrators and general users. Verify that each user is
authorized to use the system according to company
policy by performing the following:
- Obtain and examine an authorization form for
each ID.
- Verify that the sampled user IDs are implemented
in accordance with the authorization form
(including with privileges as specified and all
signatures obtained), by tracing information from
the authorization form to the system.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6474,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6474,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6476,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6476,6463,1,'8.5.2','Verify user identity before
performing password resets.','8.5.2 Examine password procedures and observe
security personnel to verify that, if a user requests a
password reset by phone, e-mail, web, or other non-faceto-
face method, the userâs identity is verified before the
password is reset.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6476,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6476,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6478,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6478,6463,1,'8.5.3','Set first-time passwords to a
unique value for each user and change
immediately after the first use.','8.5.3 Examine password procedures and observe
security personnel to verify that first-time passwords for
new users are set to a unique value for each user and
changed after first use.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6478,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6478,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6480,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6480,6463,1,'8.5.4','Immediately revoke access for
any terminated users.','8.5.4 Select a sample of employees terminated in
the past six months, and review current user access lists
to verify that their IDs have been deactivated or removed.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6480,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6480,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6482,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6482,6463,1,'8.5.5','Remove/disable inactive user
accounts at least every 90 days.','8.5.5 Verify that inactive accounts over 90 days old are
either removed or disabled.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6482,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6482,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6484,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6484,6463,1,'8.5.6','Enable accounts used by
vendors for remote maintenance only
during the time period needed.','8.5.6 Verify that any accounts used by vendors to
support and maintain system components are disabled,
enabled only when needed by the vendor, and monitored
while being used.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6484,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6484,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6486,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6486,6463,1,'8.5.7','Communicate password
procedures and policies to all users
who have access to cardholder data.','8.5.7 Interview the users from a sample of user IDs, to
verify that they are familiar with password procedures
and policies.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6486,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6486,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6488,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6488,6463,1,'8.5.8','8.5.8 Do not use group, shared, or
generic accounts and passwords.','8.5.8.a For a sample of system components, examine
user ID lists to verify the following
- Generic user IDs and accounts are disabled or
removed.
- Shared user IDs for system administration activities
and other critical functions do not exist.
- Shared and generic user IDs are not used to
administer any system components.
8.5.8.b Examine password policies/procedures to
verify that group and shared passwords are explicitly
prohibited.
8.5.8.c Interview system administrators to verify that
group and shared passwords are not distributed, even if
requested.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6488,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6488,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6490,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6490,6463,1,'8.5.9','Change user passwords at
least every 90 days.','8.5.9 For a sample of system components, obtain and
inspect system configuration settings to verify that user
password parameters are set to require users to change
passwords at least every 90 days.
For service providers only, review internal processes and
customer/user documentation to verify that customer
passwords are required to change periodically and that
customers are given guidance as to when, and under
what circumstances, passwords must change.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6490,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6490,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6492,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6492,6463,1,'8.5.10','Require a minimum password
length of at least seven characters.','8.5.10 For a sample of system components, obtain and
inspect system configuration settings to verify that
password parameters are set to require passwords to be
at least seven characters long.
For service providers only, review internal processes and
customer/user documentation to verify that customer
passwords are required to meet minimum length
requirements.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6492,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6492,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6494,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6494,6463,1,'8.5.11','Use passwords containing both
numeric and alphabetic characters.','8.5.11 For a sample of system components, obtain and
inspect system configuration settings to verify that
password parameters are set to require passwords to
contain both numeric and alphabetic characters.
For service providers only, review internal processes and
customer/user documentation to verify that customer
passwords are required to contain both numeric and
alphabetic characters.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6494,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6494,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6496,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6496,6463,1,'8.5.12','Do not allow an individual to
submit a new password that is the
same as any of the last four
passwords he or she has used.','8.5.12 For a sample of system components, obtain and
inspect system configuration settings to verify that
password parameters are set to require that new
passwords cannot be the same as the four previously
used passwords.
For service providers only, review internal processes and
customer/user documentation to verify that new customer
passwords cannot be the same as the previous four
passwords.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6496,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6496,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6498,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6498,6463,1,'8.5.13','Limit repeated access attempts
by locking out the user ID after not
more than six attempts.','8.5.13 For a sample of system components, obtain and
inspect system configuration settings to verify that
password parameters are set to require that a userâs
account is locked out after not more than six invalid logon
attempts.
For service providers only, review internal processes and
customer/user documentation to verify that customer
accounts are temporarily locked-out after not more than
six invalid access attempts.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6498,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6498,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6501,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6501,6463,1,'8.5.14','Set the lockout duration to a
minimum of 30 minutes or until
administrator enables the user ID.','8.5.14 For a sample of system components, obtain and
inspect system configuration settings to verify that
password parameters are set to require that once a user
account is locked out, it remains locked for a minimum of
30 minutes or until a system administrator resets the
account.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6501,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6501,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6503,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6503,6463,1,'8.5.15','If a session has been idle for
more than 15 minutes, require the
user to re-enter the password to reactivate
the terminal.','8.5.15 For a sample of system components, obtain and
inspect system configuration settings to verify that
system/session idle time out features have been set to
15 minutes or less.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6503,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6503,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6505,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6505,6463,1,'8.5.16','Authenticate all access to any
database containing cardholder data.
This includes access by applications,
administrators, and all other users.','8.5.16.a Review database and application
configuration settings and verify that user authentication
and access to databases includes the following:
- All users are authenticated prior to access.
- All user access to, user queries of, and user
actions on (for example, move, copy, delete), the
database are through programmatic methods only
(for example, through stored procedures).
- Direct access or queries to databases are restricted
to database administrators.
8.5.16.b Review database applications and the
related application IDs to verify that application IDs can
only be used by the applications (and not by individual
users or other processes).');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6505,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6505,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6508,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6508,6507,1,'9.1','Use appropriate facility entry
controls to limit and monitor physical
access to systems in the cardholder data
environment.','9.1 Verify the existence of physical security controls for
each computer room, data center, and other physical areas
with systems in the cardholder data environment.
- Verify that access is controlled with badge readers or
other devices including authorized badges and lock
and key.
- Observe a system administratorâs attempt to log into
consoles for randomly selected systems in the
cardholder environment and verify that they are
âlockedâ to prevent unauthorized use.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6508,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6508,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6510,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6510,6507,1,'9.1.1','Use video cameras or other
access control mechanisms to monitor
individual physical access to sensitive
areas. Review collected data and
correlate with other entries. Store for at
least three months, unless otherwise
restricted by law.
Note: âSensitive areasâ refers to any data
center, server room or any area that
houses systems that store, process, or
transmit cardholder data. This excludes
the areas where only point-of-sale
terminals are present, such as the
cashier areas in a retail store.','9.1.1 Verify that video cameras or other access control
mechanisms are in place to monitor the entry/exit points
to sensitive areas. Video cameras or other mechanisms
should be protected from tampering or disabling. Verify
that video cameras or other mechanisms are monitored
and that data from cameras or other mechanisms is
stored for at least three months.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6510,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6510,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6512,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6512,6507,1,'9.1.2','Restrict physical access to
publicly accessible network jacks.','9.1.2 Verify by interviewing network administrators and
by observation that network jacks are enabled only when
needed by authorized employees. For example,
conference rooms used to host visitors should not have
network ports enabled with DHCP. Alternatively, verify
that visitors are escorted at all times in areas with active
network jacks.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6512,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6512,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6514,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6514,6507,1,'9.1.3','Restrict physical access to
wireless access points, gateways, and
handheld devices.','9.1.3 Verify that physical access to wireless access
points, gateways, and handheld devices is appropriately
restricted.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6514,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6514,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6516,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6516,6507,1,'9.2','Develop procedures to help all
personnel easily distinguish between
employees and visitors, especially in
areas where cardholder data is
accessible.
For purposes of this requirement,
âemployeeâ refers to full-time and parttime
employees, temporary employees
and personnel, and contractors and
consultants who are âresidentâ on the
entityâs site. A âvisitorâ is defined as a
vendor, guest of an employee, service
personnel, or anyone who needs to enter
the facility for a shor','9.2.a Review processes and procedures for assigning
badges to employees, and visitors, and verify these
processes include the following:
- Granting new badges, changing access
requirements, and revoking terminated employee
and expired visitor badges
- Limited access to badge system
9.2.b Observe people within the facility to verify that it is
easy to distinguish between employees and visitors.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6516,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6516,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6518,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6518,6507,1,'9.3','Make sure all visitors are handled
according to 9.3.1, 9.3.2 and 9.3.3.','Verify that employee/visitor controls are in place according to 9.3.1, 9.3.2 and 9.3.3.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6518,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6518,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6520,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6520,6507,1,'9.3.1','Authorized before entering
areas where cardholder data is
processed or maintained','9.3.1 Observe visitors to verify the use of visitor ID
badges. Attempt to gain access to the data center to
verify that a visitor ID badge does not permit unescorted
access to physical areas that store cardholder data.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6520,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6520,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6522,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6522,6507,1,'9.3.2','Given a physical token (for
example, a badge or access device)
that expires and that identifies the
visitors as non-employee','9.3.2 Examine employee and visitor badges to verify
that ID badges clearly distinguish employees from
visitors/outsiders and that visitor badges expire.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6522,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6522,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6524,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6524,6507,1,'9.3.3','Asked to surrender the physical
token before leaving the facility or at the
date of expiration','9.3.3 Observe visitors leaving the facility to verify
visitors are asked to surrender their ID badge upon
departure or expiration.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6524,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6524,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6526,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6526,6507,1,'9.4','Use a visitor log to maintain a
physical audit trail of visitor activity.
Document the visitorâs name, the firm
represented, and the employee
authorizing physical access on the log.
Retain this log for a minimum of three
months, unless otherwise restricted by
law.','9.4.a Verify that a visitor log is in use to record physical
access to the facility as well as for computer rooms and
data centers where cardholder data is stored or
transmitted.
9.4.b Verify that the log contains the visitorâs name, the
firm represented, and the employee authorizing physical
access, and is retained for at least three months.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6526,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6526,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6528,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6528,6507,1,'9.5','Store media back-ups in a secure
location, preferably an off-site facility,
such as an alternate or back-up site, or a
commercial storage facility. Review the
locationâs security at least annually.','9.5 Verify that the storage location is reviewed at least
annually to determine that back-up media storage is
secure.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6528,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6528,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6530,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6530,6507,1,'9.6','Physically secure all paper and
electronic media that contain cardholder
data.','9.6 Verify that procedures for protecting cardholder
data include controls for physically securing paper and
electronic media (including computers, removable
electronic media, networking, and communications
hardware, telecommunication lines, paper receipts, paper
reports, and faxes).');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6530,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6530,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6532,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6532,6507,1,'9.7','Maintain strict control over the
internal or external distribution of any kind
of media that contains cardholder data.','9.7 Verify that a policy exists to control distribution of

media containing cardholder data, and that the policy

covers all distributed media including that distributed to

individuals.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6532,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6532,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6534,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6534,6507,1,'9.7.1','Classify the media so it can be
identified as confidential.','9.7.1 Verify that all media is classified so that it can be
identified as âconfidential.â');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6534,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6534,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6536,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6536,6507,1,'9.7.2','Send the media by secured
courier or other delivery method that
can be accurately tracked.','Send the media by secured
courier or other delivery method that
can be accurately tracked.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6536,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6536,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6538,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6538,6507,1,'9.8','Ensure management approves
any and all media containing cardholder
data that is moved from a secured area
(especially when media is distributed to
individuals).','9.8 Select a recent sample of several days of offsite
tracking logs for all media containing cardholder data, and
verify the presence in the logs of tracking details and
proper management authorization.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6538,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6538,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6540,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6540,6507,1,'9.9','Maintain strict control over the
storage and accessibility of media that
contains cardholder data.','9.9 Obtain and examine the policy for controlling
storage and maintenance of hardcopy and electronic media
and verify that the policy requires periodic media
inventories.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6540,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6540,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6542,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6542,6507,1,'9.9.1','Properly maintain inventory logs
of all media and conduct media
inventories at least annually.','9.9.1 Obtain and review the media inventory log to verify
that periodic media inventories are performed at least
annually.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6542,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6542,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6544,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6544,6507,1,'9.10','Destroy media containing
cardholder data when it is no longer
needed for business or legal reasons.','9.10 Obtain and examine the periodic media destruction
policy and verify that it covers all media containing
cardholder data and confirm 9.10.1 and 9.10.2.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6544,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6544,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6546,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6546,6507,1,'9.10.1','Shred, incinerate, or pulp
hardcopy materials so that cardholder
data cannot be reconstructed.','9.10.1.a Verify that hard-copy materials are cross-cut
shredded, incinerated, or pulped such that there is
reasonable assurance the hard-copy materials cannot be
reconstructed.
9.10.1.b Examine storage containers used for
information to be destroyed to verify that the containers
are secured. For example, verify that a âto-be-shreddedâ
container has a lock preventing access to its contents.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6546,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6546,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6548,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6548,6507,1,'9.10.2','Render cardholder data on
electronic media unrecoverable so that
cardholder data cannot be
reconstructed.','9.10.2 Verify that cardholder data on electronic media is
rendered unrecoverable via a secure wipe program in
accordance with industry-accepted standards for secure
deletion, or otherwise physically destroying the media (for
example, degaussing).');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6548,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6548,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6552,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6552,6550,1,'10.1','Establish a process for linking all
access to system components (especially
access done with administrative privileges
such as root) to each individual user.','10.1 Verify through observation and interviewing the
system administrator, that audit trails are enabled and
active for system components.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6552,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6552,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6554,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6554,6550,1,'10.2','Implement automated audit trails
for all system components to reconstruct events from 10.2.1 to 10.2.7','10.2 Through interviews, examination of audit logs, and
examination of audit log settings.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6554,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6554,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6556,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6556,6550,1,'10.2.1','All individual accesses to
cardholder data','10.2.1 Verify all individual access to cardholder data is
logged.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6556,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6556,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6558,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6558,6550,1,'10.2.2','All actions taken by any
individual with root or administrative
privileges','10.2.2 Verify actions taken by any individual with root or
administrative privileges is logged.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6558,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6558,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6560,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6560,6550,1,'10.2.3','Access to all audit trails','10.2.3 Verify access to all audit trails is logged.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6560,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6560,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6562,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6562,6550,1,'10.2.4','Invalid logical access attempts','10.2.4 Verify invalid logical access attempts are logged.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6562,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6562,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6564,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6564,6550,1,'10.2.5','Use of identification and
authentication mechanisms','10.2.5 Verify use of identification and authentication
mechanisms is logged.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6564,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6564,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6566,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6566,6550,1,'10.2.6','Initialization of the audit logs','10.2.6 Verify initialization of audit logs is logged.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6566,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6566,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6568,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6568,6550,1,'10.2.6','Creation and deletion of
system-level objects','10.2.7 Verify creation and deletion of system level
objects are logged.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6568,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6568,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6570,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6570,6550,1,'10.3','Record at least the following audit
trail entries for all system components for
each event: 10.3.1 to 10.3.6.','10.3 Through interviews and observation, for each
auditable event (from 10.2), perform the following: 10.3.1 to 10.3.6.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6570,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6570,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6572,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6572,6550,1,'10.3.1','User identification','10.3.1 Verify user identification is included in log entries.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6572,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6572,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6574,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6574,6550,1,'10.3.2','Type of event','10.3.2 Verify type of event is included in log entries.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6574,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6574,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6576,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6576,6550,1,'10.3.3','Date and time','10.3.3 Verify date and time stamp is included in log
entries.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6576,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6576,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6578,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6578,6550,1,'10.3.4','Success or failure indication','10.3.4 Verify success or failure indication is included in
log entries.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6578,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6578,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6580,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6580,6550,1,'10.3.5','Origination of event','10.3.5 Verify origination of event is included in log
entries.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6580,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6580,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6582,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6582,6550,1,'10.3.6','Identity or name of affected
data, system component, or resource','10.3.6 Verify identity or name of affected data, system
component, or resources is included in log entries.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6582,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6582,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6584,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6584,6550,1,'10.4','Synchronize all critical system
clocks and times.','10.4 Obtain and review the process for acquiring and
distributing the correct time within the organization, as well
as the time-related system-parameter settings for a sample
of system components. Verify the following is included in
the process and implemented:
10.4.a Verify that a known, stable version of NTP
(Network Time Protocol) or similar technology, kept current
per PCI DSS Requirements 6.1 and 6.2, is used for time
synchronization.
10.4.b Verify that internal servers are not all receiving time
signals from external sources. [Two or three central time
servers within the organization receive external time
signals [directly from a special radio, GPS satellites, or
other external sources based on International Atomic Time
and UTC (formerly GMT)], peer with each other to keep
accurate time, and share the time with other internal
servers.]
10.4.c Verify that specific external hosts are designated
from which the timeservers will accept NTP time updates
(to prevent a malicious individual from changing the clock).
Optionally, those updates can be encrypted with a
symmetric key, and access control lists can be created that
specify the IP addresses of client machines that will be
provided with the NTP service (to prevent unauthorized use
of internal time servers).
See www.ntp.org for more information');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6584,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6584,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6586,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6586,6550,1,'10.5','Secure audit trails so they cannot
be altered.','10.5 Interview system administrator and examine
permissions to verify that audit trails are secured so that
they cannot be altered...');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6586,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6586,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6588,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6588,6550,1,'10.5.1','Limit viewing of audit trails to
those with a job-related need.','10.5.1 Verify that only individuals who have a jobrelated
need can view audit trail files.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6588,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6588,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6590,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6590,6550,1,'10.5.2','Protect audit trail files from
unauthorized modifications.','10.5.2 Verify that current audit trail files are protected
from unauthorized modifications via access control
mechanisms, physical segregation, and/or network
segregation.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6590,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6590,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6592,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6592,6550,1,'10.5.3','Promptly back up audit trail files
to a centralized log server or media that
is difficult to alter.','10.5.3 Verify that current audit trail files are promptly
backed up to a centralized log server or media that is
difficult to alter.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6592,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6592,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6594,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6594,6550,1,'10.5.4','Write logs for external-facing
technologies onto a log server on the
internal LAN.','10.5.4 Verify that logs for external-facing technologies
(for example, wireless, firewalls, DNS, mail) are offloaded
or copied onto a secure centralized internal log server or
media.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6594,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6594,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6596,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6596,6550,1,'10.5.5','Use file-integrity monitoring or
change-detection software on logs to
ensure that existing log data cannot be
changed without generating alerts
(although new data being added should
not cause an alert).','10.5.5 Verify the use of file-integrity monitoring or
change-detection software for logs by examining system
settings and monitored files and results from monitoring
activities.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6596,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6596,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6598,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6598,6550,1,'10.6','Review logs for all system
components at least daily. Log reviews
must include those servers that perform
security functions like intrusion-detection
system (IDS) and authentication,
authorization, and accounting protocol
(AAA) servers (for example, RADIUS).
Note: Log harvesting, parsing, and
alerting tools may be used to meet
compliance with Requirement 10.6','10.6.a Obtain and examine security policies and
procedures to verify that they include procedures to review
security logs at least daily and that follow-up to exceptions
is required.
10.6.b Through observation and interviews, verify that
regular log reviews are performed for all system
components.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6598,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6598,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6600,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6600,6550,1,'10.7','Retain audit trail history for at
least one year, with a minimum of three
months immediately available for analysis
(for example, online, archived, or
restorable from back-up).','10.7.a Obtain and examine security policies and
procedures and verify that they include audit log retention
policies and require audit log retention for at least one year.
10.7.b Verify that audit logs are available for at least one
year and processes are in place to restore at least the last
three monthsâ logs for immediate analysis.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6600,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6600,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6602,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6602,6551,1,'11.1','Test for the presence of wireless
access points by using a wireless
analyzer at least quarterly or deploying a
wireless IDS/IPS to identify all wireless
devices in use.','11.1.a Verify that a wireless analyzer is used at least
quarterly, or that a wireless IDS/IPS is implemented and
configured to identify all wireless devices.
11.1.b If a wireless IDS/IPS is implemented, verify the
configuration will generate alerts to personnel.
11.1 c Verify the organizationâs Incident Response Plan
(Requirement 12.9) includes a response in the event
unauthorized wireless devices are detected.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6602,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6602,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6604,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6604,6551,1,'11.2','11.2 Run internal and external
network vulnerability scans at least
quarterly and after any significant
change in the network (such as new
system component installations, changes
in network topology, firewall rule
modifications, product upgrades).
Note: Quarterly external vulnerability
scans must be performed by an
Approved Scanning Vendor (ASV)
qualified by Payment Card Industry
Security Standards Council (PCI SSC).
Scans conducted after network changes
may be performed by the companyâs
internal staff.','11.2.a Inspect output from the most recent four quarters
of internal network, host, and application vulnerability
scans to verify that periodic security testing of the devices
within the cardholder data environment occurs. Verify that
the scan process includes rescans until passing results are
obtained.
Note: External scans conducted after network changes,
and internal scans, may be performed by the companyâs
qualified internal personnel or third parties.

11.2.b Verify that external scanning is occurring on a
quarterly basis in accordance with the PCI Security
Scanning Procedures, by inspecting output from the four
most recent quarters of external vulnerability scans to
verify that:

- Four quarterly scans occurred in the most recent 12-
month period;

- The results of each scan satisfy the PCI Security
Scanning Procedures (for example, no urgent, critical,
or high vulnerabilities);

- The scans were completed by an Approved Scanning
Vendor (ASV) qualified by PCI SSC.

Note: It is not required that four passing quarterly scans
must be completed for initial PCI DSS compliance if the assessor verifies 1) the most recent scan result was a
passing scan, 2) the entity has documented policies and
procedures requiring quarterly scanning, and 3)
vulnerabilities noted in the scan results have been
corrected as shown in a re-scan. For subsequent years
after the initial PCI DSS review, four passing quarterly
scans must have occurred.

11.2.c Verify that internal and/or external scanning is
performed after any significant change in the network, by
inspecting scan results for the last year. Verify that the
scan process includes rescans until passing results are
obtained.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6604,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6604,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6606,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6606,6551,1,'11.3','Perform external and internal
penetration testing at least once a year
and after any significant infrastructure or
application upgrade or modification (such
as an operating system upgrade, a subnetwork
added to the environment, or a
web server added to the environment).','11.3.a Obtain and examine the results from the most
recent penetration test to verify that penetration testing is
performed at least annually and after any significant
changes to the environment. Verify that noted
vulnerabilities were corrected and testing repeated.
11.3.b Verify that the test was performed by a qualified
internal resource or qualified external third party, and if
applicable, organizational independence of the tester exists
(not required to be a QSA or ASV).');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6606,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6606,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6608,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6608,6551,1,'11.3.1','Network-layer penetration
tests','11.3.1 Verify that the penetration test includes networklayer
penetration tests. These tests should include
components that support network functions as well as
operating systems.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6608,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6608,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6610,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6610,6551,1,'11.3.2','Application-layer penetration
tests','11.3.2 Verify that the penetration test includes
application-layer penetration tests. For web applications,
the tests should include, at a minimum, the vulnerabilities
listed in Requirement 6.5.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6610,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6610,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6614,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6614,6551,1,'11.5','Deploy file-integrity monitoring
software to alert personnel to
unauthorized modification of critical
system files, configuration files, or
content files; and configure the software
to perform critical file comparisons at
least weekly.
Note: For file-integrity monitoring
purposes, critical files are usually those
that do not regularly change, but the
modification of which could indicate a
system compromise or risk of
compromise. File-integrity monitoring
products usually come pre-configured
with critical files for the related operating
system. Other critical files, such as those
for custom applications, must be
evaluated and defined by the entity (that
is, the merchant or service provider).','11.5 Verify the use of file-integrity monitoring products
within the cardholder data environment by observing
system settings and monitored files, as well as reviewing
results from monitoring activities.
Examples of files that should be monitored:
- System executables
- Application executables
- Configuration and parameter files
- Centrally stored, historical or archived, log and audit
files');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6614,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6614,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6612,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6612,6551,1,'11.4','11.4 Use intrusion-detection systems,
and/or intrusion-prevention systems to
monitor all traffic in the cardholder data
environment and alert personnel to
suspected compromises. Keep all
intrusion-detection and prevention
engines up-to-date.','11.4.a Verify the use of intrusion-detection systems and/or
intrusion-prevention systems and that all traffic in the
cardholder data environment is monitored.
11.4.b Confirm IDS and/or IPS are configured to alert
personnel of suspected compromises.
11.4.c Examine IDS/IPS configurations and confirm
IDS/IPS devices are configured, maintained, and updated
per vendor instructions to ensure optimal protection.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6612,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6612,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6617,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6617,6616,1,'12.1','Establish, publish, maintain,
and disseminate a security policy','12.1 Examine the information security policy and verify
that the policy is published and disseminated to all relevant
system users (including vendors, contractors, and business
partners).');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6617,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6617,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6619,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6619,6616,1,'12.1.1','Addresses all PCI DSS
requirements.','12.1.1 Verify that the policy addresses all PCI DSS
requirements.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6619,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6619,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6621,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6621,6616,1,'12.1.2','Includes an annual process
that identifies threats, and
vulnerabilities, and results in a
formal risk assessment.','12.1.2 Verify that the information security policy includes
an annual risk assessment process that identifies threats,
vulnerabilities, and results in a formal risk assessment.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6621,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6621,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6623,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6623,6616,1,'12.1.3','Includes a review at least
once a year and updates when the
environment changes.','12.1.3 Verify that the information security policy is
reviewed at least annually and updated as needed to
reflect changes to business objectives or the risk
environment.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6623,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6623,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6625,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6625,6616,1,'12.2','Develop daily operational
security procedures that are consistent
with requirements in this specification
(for example, user account
maintenance procedures, and log
review procedures).','12.2.a Examine the daily operational security procedures.
Verify that they are consistent with this specification, and
include administrative and technical procedures for each of
the requirements.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6625,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6625,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6627,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6627,6616,1,'12.3','Develop usage policies for
critical employee-facing technologies
(for example, remote-access
technologies, wireless technologies,
removable electronic media, laptops,
personal data/digital assistants
(PDAs), e-mail usage and Internet
usage) to define proper use of these
technologies for all employees and
contractors.','12.3 Obtain and examine the policy for critical
employee-facing technologies and perform the following: 12.3.1 to 12.3.10.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6627,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6627,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6629,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6629,6616,1,'12.3.1','Explicit management
approval','12.3.1 Verify that the usage policies require explicit
management approval to use the technologies.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6629,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6629,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6631,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6631,6616,1,'12.3.2','Authentication for use of the
technology','12.3.2 Verify that the usage policies require that all
technology use be authenticated with user ID and
password or other authentication item (for example,
token).');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6631,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6631,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6633,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6633,6616,1,'12.3.3','A list of all such devices and
personnel with access','12.3.3 Verify that the usage policies require a list of all
devices and personnel authorized to use the devices.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6633,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6633,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6635,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6635,6616,1,'12.3.4','Labeling of devices with
owner, contact information, and
purpose','12.3.4 Verify that the usage policies require labeling of
devices with owner, contact information, and purpose.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6635,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6635,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6637,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6637,6616,1,'12.3.5','Acceptable uses of the
technology','12.3.5 Verify that the usage policies require acceptable
uses for the technology.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6637,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6637,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6639,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6639,6616,1,'12.3.6','Acceptable network
locations for the technologies','12.3.6 Verify that the usage policies require acceptable
network locations for the technology.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6639,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6639,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6641,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6641,6616,1,'12.3.7','List of company-approved
products','12.3.7 Verify that the usage policies require a list of
company-approved products.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6641,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6641,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6643,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6643,6616,1,'12.3.8','Automatic disconnect of
sessions for remote-access
technologies after a specific period
of inactivity','12.3.8 Verify that the usage policies require automatic
disconnect of sessions for remote-access technologies
after a specific period of inactivity.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6643,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6643,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6645,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6645,6616,1,'12.3.9','Activation of remote-access
technologies for vendors only when
needed by vendors, with immediate
deactivation after use','12.3.9 Verify that the usage policies require activation of
remote-access technologies used by vendors only when
needed by vendors, with immediate deactivation after
use.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6645,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6645,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6647,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6647,6616,1,'12.3.10','When accessing
cardholder data via remote-access
technologies, prohibit copy, move,
and storage of cardholder data onto
local hard drives and removable
electronic media.','12.3.10 Verify that the usage policies prohibit copying,
moving, or storing of cardholder data onto local hard
drives, and removable electronic media when accessing
such data via remote-access technologies.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6647,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6647,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6649,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6649,6616,1,'12.4','Ensure that the security policy
and procedures clearly define
information security responsibilities for
all employees and contractors.','12.4 Verify that information security policies clearly
define information security responsibilities for both
employees and contractors.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6649,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6649,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6651,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6651,6616,1,'12.5','Assign to an individual or team
the following information security
management responsibilities:12.5.1 to 12.5.5.','12.5 Verify the formal assignment of information security
to a Chief Security Officer or other security-knowledgeable
member of management. Obtain and examine information
security policies and procedures to verify that the following
information security responsibilities are specifically and
formally assigned: 12.5.1 to 12.5.5.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6651,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6651,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6653,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6653,6616,1,'12.5.1','Establish, document, and
distribute security policies and
procedures.','12.5.1 Verify that responsibility for creating and
distributing security policies and procedures is formally
assigned.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6653,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6653,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6655,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6655,6616,1,'12.5.2','Monitor and analyze security
alerts and information, and distribute
to appropriate personnel.','12.5.2 Verify that responsibility for monitoring and
analyzing security alerts and distributing information to
appropriate information security and business unit
management personnel is formally assigned.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6655,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6655,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6657,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6657,6616,1,'12.5.3','Establish, document, and
distribute security incident response
and escalation procedures to ensure
timely and effective handling of all
situations.','12.5.3 Verify that responsibility for creating and
distributing security incident response and escalation
procedures is formally assigned.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6657,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6657,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6659,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6659,6616,1,'12.5.4','Administer user accounts,
including additions, deletions, and
modifications','12.5.4 Verify that responsibility for administering user
account and authentication management is formally
assigned.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6659,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6659,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6661,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6661,6616,1,'12.5.5','Monitor and control all
access to data.','12.5.5 Verify that responsibility for monitoring and
controlling all access to data is formally assigned.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6661,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6661,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6663,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6663,6616,1,'12.6','Implement a formal security
awareness program to make all
employees aware of the importance of
cardholder data security.','12.6.a Verify the existence of a formal security
awareness program for all employees.
12.6.b Obtain and examine security awareness
program procedures and documentation and perform the
following: 12.6.1 and 12.6.2.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6663,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6663,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6665,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6665,6616,1,'12.6.1','Educate employees upon
hire and at least annually.','12.6.1.a Verify that the security awareness program
provides multiple methods of communicating awareness
and educating employees (for example, posters, letters,
memos, web based training, meetings, and promotions).
12.6.1.b Verify that employees attend awareness
training upon hire and at least annually.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6665,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6665,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6667,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6667,6616,1,'12.6.2','Require employees to
acknowledge at least annually that
they have read and understood the
companyâs security policy and
procedures.','12.6.2 Verify that the security awareness program
requires employees to acknowledge (for example, in
writing or electronically) at least annually that they have
read and understand the companyâs information security
policy.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6667,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6667,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6669,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6669,6616,1,'12.7','Screen potential employees
(see definition of âemployeeâ at 9.2
above) prior to hire to minimize the
risk of attacks from internal sources.
For those employees such as store
cashiers who only have access to one
card number at a time when facilitating
a transaction, this requirement is a
recommendation only.','12.7 Inquire with Human Resource department
management and verify that background checks are
conducted (within the constraints of local laws) on
employees prior to hire who will have access to cardholder
data or the cardholder data environment. (Examples of
background checks include previous employment history,
criminal record, credit history, and reference checks.)');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6669,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6669,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6671,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6671,6616,1,'12.8','If cardholder data is shared
with service providers, maintain and
implement policies and procedures to
manage service providers.','12.8 If the entity being assessed shares cardholder data
with service providers (for example, back-up tape storage
facilities, managed service providers such as Web hosting
companies or security service providers, or those that
receive data for fraud modeling purposes), through
observation, review of policies and procedures, and review
of supporting documentation...');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6671,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6671,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6673,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6673,6616,1,'12.8.1','Maintain a list of service
providers.','12.8.1 Verify that a list of service providers is
maintained.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6673,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6673,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6675,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6675,6616,1,'12.8.2','Maintain a written
agreement that includes an
acknowledgement that the service
providers are responsible for the
security of cardholder data the
service providers possess.','12.8.2 Verify that the written agreement includes an
acknowledgement by the service providers of their
responsibility for securing cardholder data.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6675,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6675,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6677,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6677,6616,1,'12.8.3','Ensure there is an
established process for engaging
service providers including proper
due diligence prior to engagement.','12.8.3 Verify that policies and procedures are
documented and were followed including proper due
diligence prior to engaging any service provider.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6677,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6677,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6679,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6679,6616,1,'12.8.4','Maintain a program to
monitor service providersâ PCI DSS
compliance status.','12.8.4 Verify that the entity assessed maintains a
program to monitor its service providersâ PCI DSS
compliance status.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6679,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6679,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6681,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6681,6616,1,'12.9','Implement an incident
response plan. Be prepared to
respond immediately to a system
breach.','12.9 Obtain and examine the Incident Response Plan
and related procedures and perform the following: 12.9.1 to 12.9.6.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6681,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6681,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6683,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6683,6616,1,'12.9.1','Create the incident
response plan to be implemented in
the event of system breach. Ensure
the plan addresses the following, at
a minimum:
- Roles, responsibilities, and
communication and contact
strategies in the event of a
compromise including
notification of the payment
brands, at a minimum
- Specific incident response
procedures
- Business recovery and
continuity procedures
- Data back-up processes
- Analysis of legal requirements
for reporting compromises
- Coverage and responses of all
critical system components
- Reference or inclusion of
incident response procedures
from the payment brands','12.9.1 Verify that the Incident Response Plan includes:
- Roles, responsibilities, and communication
strategies in the event of a compromise including
notification of the payment brands, at a minimum
- Specific incident response procedures,
- Business recovery and continuity procedures,
- Data back-up processes
- Analysis of legal requirements for reporting
compromises (for example, California Bill 1386
which requires notification of affected consumers
in the event of an actual or suspected
compromise for any business with California
residents in their database)
- Coverage and responses for all critical system
components
- Reference or inclusion of incident response
procedures from the payment brands');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6683,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6683,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6685,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6685,6616,1,'12.9.2','Test the plan at least
annually.','12.9.2 Verify that the plan is tested at least annually.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6685,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6685,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6687,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6687,6616,1,'12.9.3','Designate specific
personnel to be available on a 24/7
basis to respond to alerts.','12.9.3 Verify through observation and review of policies,
that there is 24/7 incident response and monitoring
coverage for any evidence of unauthorized activity,
detection of unauthorized wireless access points, critical
IDS alerts, and/or reports of unauthorized critical system
or content file changes.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6687,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6687,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6689,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6689,6616,1,'12.9.4','Provide appropriate training
to staff with security breach
response responsibilities.','12.9.4 Verify through observation and review of policies
that staff with security breach responsibilities are
periodically trained.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6689,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6689,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6691,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6691,6616,1,'12.9.5','Include alerts from intrusiondetection,
intrusion-prevention, and
file-integrity monitoring systems.','12.9.5 Verify through observation and review of
processes that monitoring and responding to alerts from
security systems including detection of unauthorized
wireless access points are covered in the Incident
Response Plan.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6691,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6691,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6693,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6693,6616,1,'12.9.6','Develop process to modify
and evolve the incident response
plan according to lessons learned
and to incorporate industry
developments.','12.9.6 Verify through observation and review of policies
that there is a process to modify and evolve the incident
response plan according to lessons learned and to
incorporate industry developments.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6693,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6693,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6696,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6696,6695,1,'A.1','Protect each entityâs (that is
merchant, service provider, or other
entity) hosted environment and data,
per A.1.1 through A.1.4:
A hosting provider must fulfill these
requirements as well as all other
relevant sections of the PCI DSS.
Note: Even though a hosting provider
may meet these requirements, the
compliance of the entity that uses the
hosting provider is not guaranteed.
Each entity must comply with the PCI
DSS and validate compliance as
applicable.','A.1 Specifically for a PCI DSS assessment of a shared
hosting provider, to verify that shared hosting providers
protect entitiesâ (merchants and service providers) hosted
environment and data, select a sample of servers
(Microsoft Windows and Unix/Linux) across a
representative sample of hosted merchants and service
providers, and perform A.1.1 through A.1.4 below.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6696,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6696,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6698,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6698,6695,1,'A.1.1','Ensure that each entity only
runs processes that have access to
that entityâs cardholder data
environment.','A.1.1 If a shared hosting provider allows entities (for
example, merchants or service providers) to run their
own applications, verify these application processes run
using the unique ID of the entity. For example:
- No entity on the system can use a shared web
server user ID.
- All CGI scripts used by an entity must be created
and run as the entityâs unique user ID.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6698,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6698,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6700,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6700,6695,1,'A.1.2','Restrict each entityâs access
and privileges to own cardholder
data environment only.','A.1.2.a Verify the user ID of any application process
is not a privileged user (root/admin).
A.1.2.b Verify each entity (merchant, service
provider) has read, write, or execute permissions only
for files and directories it owns or for necessary system
files (restricted via file system permissions, access
control lists, chroot, jailshell, etc.). IMPORTANT: An
entityâs files may not be shared by group.
A.1.2.c Verify an entityâs users do not have write
access to shared system binaries.
A.1.2.d Verify that viewing of log entries is restricted
to the owning entity.
A.1.2.e To ensure each entity cannot monopolize
server resources to exploit vulnerabilities (for example,
error, race, and restart conditions, resulting in, for
example, buffer overflows), verify restrictions are in
place for the use of these system resources:
- Disk space
- Bandwidth
- Memory
- CPU');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6700,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6700,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6702,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6702,6695,1,'A.1.3','Ensure logging and audit
trails are enabled and unique to
each entityâs cardholder data
environment and consistent with
PCI DSS Requirement 10.','A.1.3.a Verify the shared hosting provider has
enabled logging as follows, for each merchant and
service provider environment:
- Logs are enabled for common third-party
applications.
- Logs are active by default.
- Logs are available for review by the owning
entity.
- Log locations are clearly communicated to the
owning entity.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6702,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6702,2902,NOW());

INSERT INTO isms_context (pkContext,nType,nState) VALUES (6704,2808,2702);
INSERT INTO rm_best_practice (fkContext,fkSectionBestPractice,nControlType,sName,tDescription,tImplementationGuide) VALUES (6704,6695,1,'A.1.4','Enable processes to provide
for timely forensic investigation in
the event of a compromise to any
hosted merchant or service
provider.','A.1.4 Verify the shared hosting provider has written
policies that provide for a timely forensics investigation
of related servers in the event of a compromise.');
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6704,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,6704,2902,NOW());

