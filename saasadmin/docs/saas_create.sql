/*==============================================================*/
/* Table: saas_activation                                       */
/*==============================================================*/
create table saas_activation (
pkid                 serial               not null,
fkinstance           int4                 null,
scode                varchar(256)         not null,
ntype                int4                 not null,
ddatecreated         timestamp            not null,
ddateused            timestamp            null,
constraint pk_saas_activation primary key (pkid)
);

/*==============================================================*/
/* Index: fkinstance_fk                                         */
/*==============================================================*/
create  index fkinstance_fk on saas_activation (
fkinstance
);

/*==============================================================*/
/* Table: saas_agenda                                           */
/*==============================================================*/
create table saas_agenda (
pkid                 serial               not null,
salias               varchar(256)         not null,
semail               varchar(256)         not null,
spassword            varchar(256)         not null,
sactivationcode      varchar(256)         null,
ddate                timestamp            not null default NOW(),
constraint pk_saas_agenda primary key (pkid)
);

/*==============================================================*/
/* Table: saas_config                                           */
/*==============================================================*/
create table saas_config (
pkconfig             serial               not null,
svalue               varchar(256)         not null,
constraint pk_saas_config primary key (pkconfig)
);

/*==============================================================*/
/* Table: saas_instance                                         */
/*==============================================================*/
create table saas_instance (
pkinstance           serial               not null,
salias               varchar(256)         not null,
sdatabasename        varchar(256)         not null,
sdatabaseuser        varchar(256)         not null,
sdatabaseuserpass    varchar(256)         not null,
ddatecreated         timestamp            not null,
constraint pk_saas_instance primary key (pkinstance)
);

alter table saas_activation
   add constraint fk_saas_act_fkinstanc_saas_ins foreign key (fkinstance)
      references saas_instance (pkinstance)
      on delete set null on update restrict;

/*==============================================================*/
/* Configs                                                      */
/*==============================================================*/
INSERT INTO saas_config (pkconfig, svalue) VALUES (3301, '');
INSERT INTO saas_config (pkconfig, svalue) VALUES (3302, '');
INSERT INTO saas_config (pkconfig, svalue) VALUES (3303, '');
INSERT INTO saas_config (pkconfig, svalue) VALUES (3304, '');
INSERT INTO saas_config (pkconfig, svalue) VALUES (3305, '');
INSERT INTO saas_config (pkconfig, svalue) VALUES (3306, '');
INSERT INTO saas_config (pkconfig, svalue) VALUES (3307, 'support@realiso.com');
INSERT INTO saas_config (pkconfig, svalue) VALUES (3308, '');
INSERT INTO saas_config (pkconfig, svalue) VALUES (3309, '1.0.0');
INSERT INTO saas_config (pkconfig, svalue) VALUES (3310, '');
INSERT INTO saas_config (pkconfig, svalue) VALUES (3311, '0');
INSERT INTO saas_config (pkconfig, svalue) VALUES (3312, '0');
INSERT INTO saas_config (pkconfig, svalue) VALUES (3313, '');
INSERT INTO saas_config (pkconfig, svalue) VALUES (3314, 'noreply@realiso.com');
INSERT INTO saas_config (pkconfig, svalue) VALUES (3315, '');
INSERT INTO saas_config (pkconfig, svalue) VALUES (3316, '');
INSERT INTO saas_config (pkconfig, svalue) VALUES (3317, '');
INSERT INTO saas_config (pkconfig, svalue) VALUES (3318, '');
INSERT INTO saas_config (pkconfig, svalue) VALUES (3319, 'Realiso Team');

INSERT INTO saas_config (pkconfig, svalue) VALUES (3401, '');
INSERT INTO saas_config (pkconfig, svalue) VALUES (3402, '');
INSERT INTO saas_config (pkconfig, svalue) VALUES (3403, '');
INSERT INTO saas_config (pkconfig, svalue) VALUES (3404, '');