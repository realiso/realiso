<?php
include_once "include.php";

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $msBuffer="";
    $moFP = fopen('terms_of_service.txt','rb');
    if ($moFP) {
      while(!feof($moFP)) {
        $msBuffer .= fgets($moFP);
      }
      fclose($moFP);
    }
    FWDWebLib::getObject('terms_of_service')->setValue($msBuffer);
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
  }
}
FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_terms_of_service.xml");
?>