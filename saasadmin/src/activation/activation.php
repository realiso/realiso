<?php
include_once "include.php";

class CreateBaseEvent extends FWDRunnable {
  public function run() {
    $msAlias = trim(strtolower(FWDWebLib::getObject('alias')->getValue()));
    $msEmail = trim(FWDWebLib::getObject('email')->getValue());
    $msPassword = trim(FWDWebLib::getObject('password')->getValue());
    $msConfirm = trim(FWDWebLib::getObject('password_confirmation')->getValue());
    $msSysId = trim(FWDWebLib::getObject('sys_id')->getValue());
    $msSubscriptionActivationCode = trim(FWDWebLib::getObject('activation_code')->getValue());
    $msTrialActivationCode = trim(FWDWebLib::getObject('trial_code')->getValue());
    $mbTrial = $msTrialActivationCode ? 1 : 0;
    if ($msPassword != $msConfirm){
      echo "hide_warnings(); js_show('wn_passwords_dont_match'); gebi('password_confirmation').value=''; gebi('password_confirmation').focus();";exit;
    }
    if (!SAASInstance::isValidAlias($msAlias)) {
      echo "hide_warnings(); js_show('wn_invalid_alias'); gebi('alias').value=''; gebi('alias').focus();";exit;
    }    
    if (SAASInstance::isAliasUsed($msAlias)){
      echo "hide_warnings(); js_show('wn_alias_in_use'); gebi('alias').value=''; gebi('alias').focus();";exit;
    }
    
    /*
     * Valida o c�digo de ativa��o.
     */
    $mbOk = true;
    $msActivationCode = "";
    $miType = 0;
    if ($mbTrial) {
      $msActivationCode = $msTrialActivationCode;
      $maData = SAASKeyGen::extractData($msActivationCode);
      $miType = $maData['license_type'];
      if (!in_array($miType, SAASLib::getTrialLicenseTypes())) $mbOk = false;
      else {
        if (SAASActivation::isValidActivationCode($msActivationCode, $miType) && SAASLib::isSystemIdAndLicenseTypeCompatible($msSysId, $miType)) $mbOk = true;
        else $mbOk = false;
      }
    }
    else {
      $msActivationCode = $msSubscriptionActivationCode;
      $maData = SAASKeyGen::extractData($msActivationCode);
      $miType = $maData['license_type'];
      if (!in_array($miType, SAASLib::getSubscriptionLicenseTypes())) $mbOk = false;
      else {
        if (SAASActivation::isValidActivationCode($msActivationCode, $miType) && SAASLib::isSystemIdAndLicenseTypeCompatible($msSysId, $miType)) $mbOk = true;
        else $mbOk = false;
      }
    }
    
    if ($mbOk) {
      $msSysId = "";
      if (SAASLib::isISMSLicense($miType)) $msSysId = SAASLib::SYSTEM_ISMS;
      else if (SAASLib::isBCMSLicense($miType)) $msSysId = SAASLib::SYSTEM_BCMS;
      else if (SAASLib::isEMSLicense($miType)) $msSysId = SAASLib::SYSTEM_EMS;
      else if (SAASLib::isOHSLicense($miType)) $msSysId = SAASLib::SYSTEM_OHS;
      else if (SAASLib::isSOXLicense($miType)) $msSysId = SAASLib::SYSTEM_SOX;
      else if (SAASLib::isPCILicense($miType)) $msSysId = SAASLib::SYSTEM_PCI;
      else if (SAASLib::isMinimaRiskLicense($miType)) $msSysId = SAASLib::SYSTEM_MINIMA;

      $moAgenda = new SAASAgenda();
      $moAgenda->setFieldValue('agenda_alias',$msAlias);
      $moAgenda->setFieldValue('agenda_email',$msEmail);
      $moAgenda->setFieldValue('agenda_password',$msPassword);
      $moAgenda->setFieldValue('agenda_activation_code',$msActivationCode);
      $moAgenda->insert();
      if (!$mbTrial) echo "goto_url('post_activation.php?t=$mbTrial&alias=$msAlias&id=$msSysId');";
      else echo "goto_url('post_activation.php?t=$mbTrial&alias=$msAlias&id=$msSysId');";
    }
    else {
      if (!$mbTrial) echo "gobi('wn_invalid_activation_code').show();";
      else echo "gobi('wn_invalid_trial_activation_code').show();";
    }
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new CreateBaseEvent("create_base_event"));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    FWDWebLib::getObject('email')->setValue(urldecode(FWDWebLib::getObject('trial_email')->getValue()));
    
    // armazena os par�metros passados por get
    $msTrialCode = FWDWebLib::getObject('trial_code')->getValue();
    $msSysId = FWDWebLib::getObject('system_id')->getValue();
    
    if ($msTrialCode && $msSysId) {
      // mensagem de erro -> essa situa��o n�o deve ocorrer
      echo "<script language='javascript'>document.location.href = 'invalid_parameters.php';</script>";
      exit;
    }
    
    // descobre o tipo da licen�a
    $maData = SAASKeyGen::extractData($msTrialCode);
    $miLicenseType = $maData['license_type'];
    
    // descobre qual o sistema
    $mbIsISMS = SAASLib::isISMSCode($msSysId) || SAASLib::isISMSLicense($miLicenseType);
    $mbIsBCMS = SAASLib::isBCMSCode($msSysId) || SAASLib::isBCMSLicense($miLicenseType);
    $mbIsEMS = SAASLib::isEMSCode($msSysId) || SAASLib::isEMSLicense($miLicenseType);
    $mbIsOHS = SAASLib::isOHSCode($msSysId) || SAASLib::isOHSLicense($miLicenseType);
    $mbIsSOX = SAASLib::isSOXCode($msSysId) || SAASLib::isSOXLicense($miLicenseType);
    $mbIsPCI = SAASLib::isPCICode($msSysId) || SAASLib::isPCILicense($miLicenseType);
    $msIsMinima = SAASLib::isMinimaRiskCode($msSysId) || SAASLib::isMinimaRiskLicense($miLicenseType);
    
    // � subscription, criar chave.
    $msCode = '';
    if ($msSysId) {
      if ($mbIsISMS) $msCode = SAASActivation::insertActivation(SAASActivation::SUBSCRIPTION_ISMS);
      else if ($mbIsBCMS) $msCode = SAASActivation::insertActivation(SAASActivation::SUBSCRIPTION_BCMS);
      else if ($mbIsEMS) $msCode = SAASActivation::insertActivation(SAASActivation::SUBSCRIPTION_EMS);
      else if ($mbIsOHS) $msCode = SAASActivation::insertActivation(SAASActivation::SUBSCRIPTION_OHS);
      else if ($mbIsSOX) $msCode = SAASActivation::insertActivation(SAASActivation::SUBSCRIPTION_SOX);
      else if ($mbIsPCI) $msCode = SAASActivation::insertActivation(SAASActivation::SUBSCRIPTION_PCI);
      else if ($mbIsMinima) $msCode = SAASActivation::insertActivation(SAASActivation::SUBSCRIPTION_MINIMARISK);
      else {
        // mensagem de erro -> par�metros inv�lidos (n�o tem como saber qual o sistema)
        echo "<script language='javascript'>document.location.href = 'invalid_parameters.php';</script>";
        exit;
      }
    }
    else $msCode = $msTrialCode;
    
    $msSys = "";
    $msImg = FWDWebLib::getInstance()->getGfxRef();
    if ($msSysId || $msTrialCode) {
      // seta vari�veis para adequa��o da tela
      if ($mbIsISMS) {
        $msSys = "Real ISMS";
        $msImg .= "realISMS.png";
        $msSysId = SAASLib::SYSTEM_ISMS;
      }
      else if ($mbIsBCMS) {
	$msSys = "Real BCMS";
	$msImg .= "realBCMS.png";
	$msSysId = SAASLib::SYSTEM_BCMS;
      }
      else if ($mbIsEMS) {
        $msSys = "Real EMS";
        $msImg .= "realEMS.png";
        $msSysId = SAASLib::SYSTEM_EMS;
      }
      else if ($mbIsOHS) {
        $msSys = "Real OHS";
        $msImg .= "realOHS.png";
        $msSysId = SAASLib::SYSTEM_OHS;
      }
      else if ($mbIsSOX) {
        $msSys = "Real SOX";
        $msImg .= "realSOX.png";
        $msSysId = SAASLib::SYSTEM_SOX;
      }
      else if ($mbIsPCI) {
        $msSys = "Real PCI";
        $msImg .= "realPCI.png";
        $msSysId = SAASLib::SYSTEM_PCI;
      }
      else if ($mbIsMinima) {
      	$msSys = "Minima Risk";
      	$msImg .= "minimaSOS.png";
      	$msSysId = SAASLib::SYSTEM_MINIMA;
      }
      else {
        // mensagem de erro -> par�metros inv�lidos (n�o tem como saber qual o sistema)
        echo "<script language='javascript'>document.location.href = 'invalid_parameters.php';</script>";
        exit;
      }
    }
    else {
      // mensagem de erro -> par�metros inv�lidos (n�o tem como saber qual o sistema)
      echo "<script language='javascript'>document.location.href = 'invalid_parameters.php';</script>";
      exit;
    }
    
    // seta as propriedades da tela que variam de acordo com o sistema
    FWDWebLib::getObject('sys_id')->setValue($msSysId);
    FWDWebLib::getObject('logo')->setAttrSrc($msImg);
    FWDWebLib::getObject('st_welcome_to_axur_isms')->replace('%system%', $msSys);
    FWDWebLib::getObject('st_ready_to_start')->replace('%system%', $msSys);
    FWDWebLib::getObject('st_trial_user_notice')->replace('%system%', $msSys);
    FWDWebLib::getInstance()->setWindowTitle("$msSys Activation");
    FWDWebLib::getObject('activation_code')->setValue($msCode);
    
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        js_show('dialog');
      
        document.body.className = 'BODYBackground';
        window.onresize = center_dialog_div;
        center_dialog_div();
        
        function hide_warnings() {
          js_hide('wn_alias_in_use');
          js_hide('wn_passwords_dont_match');
          js_hide('wn_invalid_alias');
        }
      </script>
    <?
  }
}
FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("activation.xml");
?>
