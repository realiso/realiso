<?php
include_once "include.php";

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moConfig = new SAASConfig();
    $moConfig->loadConfig();
    $msAlias = FWDWebLib::getObject('alias')->getValue() ? FWDWebLib::getObject('alias')->getValue() : "your-choosed-alias";    
    $msId = FWDWebLib::getObject('id')->getValue();
    $mbTrial = FWDWebLib::getObject('trial')->getValue() ? true : false;
        
    $msSystemName = "";
    $msSystemFullName = "";
    $msImg = FWDWebLib::getInstance()->getGfxRef();
    if (SAASLib::isISMSCode($msId)) {
      $msSystemName = "Real ISMS";
      $msSystemFullName = "Information Security Management System";
      $msImg .= "realISMS.png";
    }
    else if (SAASLib::isEMSCode($msId)) {
      $msSystemName = "Real EMS";
      $msSystemFullName = "Environmental Management System";
      $msImg .= "realEMS.png";
    }
    else if (SAASLib::isOHSCode($msId)) {
      $msSystemName = "Real OHS";
      $msSystemFullName = "Occupational Health and Safety Management System";
      $msImg .= "realOHS.png";
    }
    else if (SAASLib::isSOXCode($msId)) {
      $msSystemName = "Real SOX";
      $msSystemFullName = "Sarbanes Oxley Management System";
      $msImg .= "realSOX.png";
    }
    else if (SAASLib::isPCICode($msId)) {
      $msSystemName = "Real PCI";
      $msSystemFullName = "Payment Card Industry Management System";
      $msImg .= "realPCI.png";
    }
    else {
      // mensagem de erro -> par�metros inv�lidos
      echo "<script language='javascript'>document.location.href = 'invalid_parameters.php';</script>";
      exit;
    }
    
    $msLink = '';
    if ($mbTrial) {
      $msLink = $moConfig->getConfig(CFG_TRIAL_URL).$msAlias; 
      $miExpiracyPeriod = $moConfig->getConfig(CFG_DEFAULT_EXPIRACY_PERIOD);
      FWDWebLib::getObject('commercial_title')->setShouldDraw(false);
      FWDWebLib::getObject('commercial_info')->setShouldDraw(false);
      $msTrialInfo = FWDLanguage::getPHPStringValue('post_activation_trial_info',
        "<b>Congratulations! Your Trial Account is being processed!</b><br/><br/>".
        "Check your inbox for an email guiding you through your free %expiracy%-day trial to ensure that you experience everything that <b>%system%</b> offers.<br/><br/>".
        "<b>Step-by-Step:</b><br/><br/>".
        "1- Check your Inbox<br/>".
        "2- Login to %system%<br/>".
        "3- Experience %system%"
      );
      $msTrialInfo = str_replace('%expiracy%',$miExpiracyPeriod,$msTrialInfo);
      $msTrialInfo = str_replace('%system%',$msSystemName,$msTrialInfo);
      FWDWebLib::getObject('trial_info')->setValue($msTrialInfo);
    } else {
      $msLink = $moConfig->getConfig(CFG_COMMERCIAL_URL).$msAlias; 
      FWDWebLib::getObject('trial_title')->setShouldDraw(false);
      FWDWebLib::getObject('trial_info')->setShouldDraw(false);
      $msCommInfo = FWDLanguage::getPHPStringValue('post_activation_commercial_info',
        "You are just minutes away from signing in to <b>%system% - %full_system_name%</b>.<br/><br/>".
        "At this moment, we are working on building your base. You will receive an email with your login, password and start link.<br/><br/>".
        "<b>Step-by-Step:</b><br/><br/>".
        "1- Check your Inbox<br/>".
        "2- Login to %system%<br/>".
        "3- Setup & Use your %system%"
      );      
      $msCommInfo = str_replace('%system%',$msSystemName,$msCommInfo);
      $msCommInfo = str_replace('%full_system_name%',$msSystemFullName,$msCommInfo);
      FWDWebLib::getObject('commercial_info')->setValue($msCommInfo);
    }
    
    FWDWebLib::getObject('logo')->setAttrSrc($msImg);
    
    $msMessage = FWDLanguage::getPHPStringValue('post_activation_email_warning',
      "<b>Our \"Welcome Email\" is sometimes filtered by webmail providers like Yahoo and Hotmail.</b><br/><br/>".
      "Please, don't worry if you don't receive our \"Welcome Email\", in <b>one minute</b> your access will be ready at <a href=%link% class='FWDStaticEmailWarningLink'><font color='lightblue'><u>%link%</u></font></a>"
    );    
    FWDWebLib::getObject('email_warning')->setValue(str_replace('%link%', $msLink, $msMessage));
    
    $msUri = $moConfig->getConfig(CFG_BUY_NOW_URL);
    $msUri = substr($msUri,0,strrpos($msUri,'/'));
    if (SAASLib::isISMSCode($msId)) $msUri = str_replace('%system%', 'realisms', $msUri);
    else if (SAASLib::isEMSCode($msId)) $msUri = str_replace('%system%', 'realems', $msUri);
    else if (SAASLib::isOHSCode($msId)) $msUri = str_replace('%system%', 'realohs', $msUri);
    else if (SAASLib::isSOXCode($msId)) $msUri = str_replace('%system%', 'realsox', $msUri);
    else if (SAASLib::isPCICode($msId)) $msUri = str_replace('%system%', 'realpci', $msUri);
    else $msUri = str_replace('%system%', '', $msUri);
    $moString = new FWDString();
    $moString->setValue(FWDLanguage::getPHPStringValue('st_go_to_hotsite',"Go to Hotsite"));
    $moLink = new FWDLink();
    $moLink->setAttrURI($msUri);
    $moLink->addObjFWDString($moString);
    FWDWebLib::getObject('link_hotsite')->setObjFWDLink($moLink);
    
    FWDWebLib::getInstance()->setWindowTitle("$msSystemName Activation");
    
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        js_show('dialog');
      
        document.body.className = 'BODYBackground';
        window.onresize = center_dialog_div;
        center_dialog_div();
      </script>
    <?
  }
}
FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("post_activation.xml");
?>