<?php
/**
 * ISMS SAAS - INTERNET SECURITY MANAGEMENT SYSTEM SAAS
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGetNextInAgenda.
 *
 * <p>Consulta para retornar a pr�xima inst�ncia que deve ser criada.</p>
 * @package SAAS
 * @subpackage handlers
 */
class QueryGetNextInAgenda extends FWDDBQueryHandler { 
    
  public function __construct($poDB) {
    parent::__construct($poDB); 

    $this->coDataSet->addFWDDBField(new FWDDBField("",    "agenda_id",                   DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("",    "agenda_alias",                DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("",    "agenda_email",                DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("",    "agenda_password",             DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("",    "agenda_activation_code",      DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("",    "agenda_activation_type",      DB_NUMBER));
  }
  
  public function makeQuery() {
    $this->csSQL = "SELECT
                      a.pkId as agenda_id,
                      a.sAlias as agenda_alias,
                      a.sEmail as agenda_email,
                      a.sPassword as agenda_password,
                      a.sActivationCode as agenda_activation_code,
                      ac.nType as agenda_activation_type
                    FROM
                      saas_agenda a
                      JOIN saas_activation ac ON (a.sActivationCode = ac.sCode) 
                    ORDER BY
                      ac.nType ASC, a.dDate ASC";
  }
  
  public function executeQuery() {
    parent::executeQuery(1);
    if ($this->coDataSet->fetch()) {
      $maRes = array();
      $maRes['agenda_id'] =                   $this->getFIeldValue('agenda_id');
      $maRes['agenda_alias'] =                $this->getFIeldValue('agenda_alias');
      $maRes['agenda_email'] =                $this->getFIeldValue('agenda_email');
      $maRes['agenda_password'] =             $this->getFIeldValue('agenda_password');
      $maRes['agenda_activation_code'] =      $this->getFIeldValue('agenda_activation_code');
      $maRes['agenda_activation_type'] =      $this->getFIeldValue('agenda_activation_type');
      return $maRes;
    }
    else return array(); 
  }
}
?>