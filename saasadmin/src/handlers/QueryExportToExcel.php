<?php
/**
 * ISMS SAAS - INTERNET SECURITY MANAGEMENT SYSTEM SAAS
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryExportToExcel.
 *
 * <p>Consulta para pegar informações das instâncias para gerar um excel.</p>
 * @package SAAS
 * @subpackage handlers
 */
class QueryExportToExcel extends FWDDBQueryHandler {

  public function __construct($poDB=null){
    parent::__construct($poDB); 
    
    $this->coDataSet->addFWDDBField(new FWDDBField('', 'instance_id',             DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('', 'instance_alias',          DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('', 'instance_ups',            DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('', 'instance_type',           DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('', 'instance_code',           DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('', 'instance_creation_date',  DB_DATETIME));
    $this->coDataSet->addFWDDBField(new FWDDBField('', 'instance_db_name',        DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('', 'instance_db_user',        DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('', 'instance_db_user_pass',   DB_STRING));
  }  

  public function makeQuery(){
    $this->csSQL = "SELECT
                    i.pkInstance as instance_id,
                    i.sAlias as instance_alias,                    
                    (SELECT count(*) FROM saas_activation WHERE nType = 2 AND fkInstance = i.pkInstance) as instance_ups,
                    a.nType as instance_type,
                    a.sCode as instance_code,
                    i.dDateCreated as instance_creation_date,
                    i.sDatabaseName as instance_db_name,
                    i.sDatabaseUser as instance_db_user,
                    i.sDatabaseUserPass as instance_db_user_pass      
                  FROM
                    saas_instance i
                    JOIN saas_activation a ON (i.pkInstance = a.fkInstance)
                  WHERE
                    a.nType != " . SAASActivation::USER_PACK . "
                  ORDER BY i.dDateCreated DESC";
  }
  
  public function executeQuery() {
    parent::executeQuery();
    $maArray = array();
    while($this->coDataSet->fetch()) {      
      $miId = $this->coDataSet->getFieldByAlias('instance_id')->getValue();
      $maArray[$miId]['instance_alias'] = $this->coDataSet->getFieldByAlias('instance_alias')->getValue();
      $maArray[$miId]['instance_ups'] = $this->coDataSet->getFieldByAlias('instance_ups')->getValue();
      $maArray[$miId]['instance_type'] = $this->coDataSet->getFieldByAlias('instance_type')->getValue();
      $maArray[$miId]['instance_code'] = $this->coDataSet->getFieldByAlias('instance_code')->getValue();
      $maArray[$miId]['instance_creation_date'] = $this->coDataSet->getFieldByAlias('instance_creation_date')->getValue();
      $maArray[$miId]['instance_db_name'] = $this->coDataSet->getFieldByAlias('instance_db_name')->getValue();      
      $maArray[$miId]['instance_db_user'] = $this->coDataSet->getFieldByAlias('instance_db_user')->getValue();
      $maArray[$miId]['instance_db_user_pass'] = $this->coDataSet->getFieldByAlias('instance_db_user_pass')->getValue();
    }
    return $maArray;
  }

}

?>