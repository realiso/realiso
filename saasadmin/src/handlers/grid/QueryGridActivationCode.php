<?php
/**
 * ISMS SAAS - INTERNET SECURITY MANAGEMENT SYSTEM SAAS
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGridActivationCode.
 *
 * <p>Consulta para popular a grid de c�digos de ativa��o.</p>
 * @package SAAS
 * @subpackage handlers
 */
class QueryGridActivationCode extends FWDDBQueryHandler { 
    
  public function __construct($poDB) {
    parent::__construct($poDB); 

    $this->coDataSet->addFWDDBField(new FWDDBField("",    "activation_id",                DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("",    "activation_code",              DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("",    "activation_type",              DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("",    "activation_date_created",      DB_DATETIME));
    $this->coDataSet->addFWDDBField(new FWDDBField("",    "activation_date_used",         DB_DATETIME));
    $this->coDataSet->addFWDDBField(new FWDDBField("",    "activation_alias_name",        DB_STRING));
  }
  
  public function makeQuery() {
    $this->csSQL = "SELECT
                      a.pkId as activation_id,
                      a.sCode as activation_code,                      
                      a.nType as activation_type,
                      a.dDateCreated as activation_date_created,
                      a.dDateUsed as activation_date_used,
                      i.sAlias as activation_alias_name
                    FROM
                      saas_activation a
                      LEFT JOIN saas_instance i ON (a.fkInstance = i.pkInstance)";
  } 
}
?>