<?php
/**
 * ISMS SAAS - INTERNET SECURITY MANAGEMENT SYSTEM SAAS
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGridAgenda.
 *
 * <p>Consulta para popular a grid de agenda.</p>
 * @package SAAS
 * @subpackage handlers
 */
class QueryGridAgenda extends FWDDBQueryHandler { 
    
  public function __construct($poDB) {
    parent::__construct($poDB); 

    $this->coDataSet->addFWDDBField(new FWDDBField("",    "agenda_id",                DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("",    "agenda_alias",             DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("",    "agenda_activation_code",   DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("",    "agenda_email",             DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("",    "agenda_date",              DB_DATETIME));
    $this->coDataSet->addFWDDBField(new FWDDBField("",    "agenda_type",              DB_NUMBER));
  }
  
  public function makeQuery() {
    $this->csSQL = "SELECT
                      a.pkId as agenda_id,
                      a.sAlias as agenda_alias,
                      a.sActivationCode as agenda_activation_code,
                      a.sEmail as agenda_email,                      
                      a.dDate as agenda_date,
                      ac.nType as agenda_type
                    FROM
                      saas_agenda a
                      JOIN saas_activation ac ON (a.sActivationCode = ac.sCode)";
  } 
}
?>