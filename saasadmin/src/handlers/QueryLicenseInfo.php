<?php
/**
 * ISMS SAAS - INTERNET SECURITY MANAGEMENT SYSTEM SAAS
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryLicenseInfo.
 *
 * <p>Consulta para buscar informa��es sobre a licen�a de uma determinada inst�ncia.</p>
 * @package SAAS
 * @subpackage handlers
 */
class QueryLicenseInfo extends FWDDBQueryHandler { 
    
  public function __construct($poDB) {
    parent::__construct($poDB); 

    $this->coDataSet->addFWDDBField(new FWDDBField("",    "config_id",                DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("",    "config_value",             DB_STRING));
  }
  
  public function makeQuery() {    
    $msIn = SAASInstance::LICENSE_MODULES.",".SAASInstance::LICENSE_MAX_USERS.",".SAASInstance::LICENSE_ACTIVATION_DATE.",".SAASInstance::LICENSE_EXPIRACY.",".SAASInstance::LICENSE_MAX_SIMULTANEOUS_USERS.",".SAASInstance::LICENSE_TYPE;
    $this->csSQL = "SELECT pkConfig as config_id, sValue as config_value FROM isms_saas WHERE pkConfig IN ($msIn)";
  }
  
  public function executeQuery() {
    parent::executeQuery();
    $maRes = array();
    while ($this->coDataSet->fetch()) {
      $maRes[$this->coDataSet->getFieldByAlias("config_id")->getValue()] = $this->coDataSet->getFieldByAlias("config_value")->getValue(); 
    }
    return $maRes; 
  }
}
?>