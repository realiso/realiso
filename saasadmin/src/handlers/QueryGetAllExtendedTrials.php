<?php
/**
 * ISMS SAAS - INTERNET SECURITY MANAGEMENT SYSTEM SAAS
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

include_once $handlers_ref."QueryGetInstanceInfo.php";
 
/**
 * Classe QueryGetAllExtendedTrials.
 *
 * <p>Consulta para buscar quais inst�ncias trials que tiverem sua data de expira��o prolongada.</p>
 * @package SAAS
 * @subpackage handlers
 */
class QueryGetAllExtendedTrials extends FWDDBQueryHandler {
  
  public function __construct($poDB) {
    parent::__construct($poDB); 

    $this->coDataSet->addFWDDBField(new FWDDBField('i.pkInstance',       'instance_id',           DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('i.sAlias',           'instance_alias',        DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('i.sDatabaseName',    'instance_db_name',      DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('i.sDatabaseUser',    'instance_db_user',      DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('i.sDatabaseUserPass','instance_db_user_pass', DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('i.dDateCreated',     'instance_creation_date',DB_DATETIME));    
    $this->coDataSet->addFWDDBField(new FWDDBField('a.nType',            'instance_type',         DB_NUMBER));
  }
  
  public function makeQuery(){    
    $msDate = SAASLib::getTimestampFormat(FWDWebLib::getTime() - (60*60*24*DAYS_TO_REMOVE));    
    $this->csSQL = "SELECT
                      	i.pkInstance as instance_id,
                        i.sDatabaseName as instance_db_name,
                        i.sDatabaseUser as instance_db_user,
                        i.sDatabaseUserPass as instance_db_user_pass
                    FROM
                        saas_instance i
                        JOIN saas_activation a ON (i.pkInstance = a.fkInstance)
                    WHERE
                        a.nType IN (".implode(", ", SAASLib::getTrialLicenseTypes()).") AND i.dDateCreated < " . $msDate;
  }
  
  public function executeQuery(){
    parent::executeQuery();
    
    $moConfig = new SAASConfig();
    $moConfig->loadConfig();
    $maIds = array();
    while ($this->coDataSet->fetch()) {
      $moDB = new FWDDB(
        DB_POSTGRES,
        $this->getFieldValue('instance_db_name'),
        $this->getFieldValue('instance_db_user'),
        $this->getFieldValue('instance_db_user_pass'),
        $moConfig->getConfig(CFG_DB_SERVER)
      );
      $moDB->connect();
      $moQuery = new QueryGetInstanceInfo($moDB);
      $moQuery->makeQuery();
      $moQuery->executeQuery();
      if ($moQuery->getExpirationDate() >= SAASLib::SAASTime()) {
        $maIds[] = $this->coDataSet->getFieldByAlias('instance_id')->getValue();
      }
    }
    return $maIds;
  }
}
?>