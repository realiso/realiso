<?php
/**
 * ISMS SAAS - INTERNET SECURITY MANAGEMENT SYSTEM SAAS
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGetInstanceInfo.
 *
 * <p>Consulta para buscar informações de uma determinada instância.</p>
 * @package SAAS
 * @subpackage handlers
 */
class QueryGetInstanceInfo extends FWDDBQueryHandler {
  
  private $cbIsBlocked;
  private $csLastLogin;
  private $cbIsActivated;
  private $ciExpirationDate;
  
  public function __construct($poDB){
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField('','is_active',DB_NUMBER));    
    $this->coDataSet->addFWDDBField(new FWDDBField('','last_login',DB_DATETIME));
    $this->coDataSet->addFWDDBField(new FWDDBField('','expiration_timestamp',DB_DATETIME));
  }
  
  public function makeQuery(){
    $this->csSQL = FWDWebLib::selectConstant("
                     (SELECT sValue FROM isms_saas WHERE pkConfig = ".SAASInstance::SYSTEM_IS_ACTIVE.") as is_active,
                     (SELECT MAX(dLastLogin) FROM isms_user) as last_login,
                     (SELECT sValue FROM isms_saas WHERE pkConfig = " . SAASInstance::LICENSE_EXPIRACY .") as expiration_timestamp"
                   );
  }
  
  public function executeQuery(){
    parent::executeQuery();
    $this->coDataSet->fetch();
    $this->cbIsBlocked = ($this->coDataSet->getFieldByAlias('is_active')->getValue()?false:true);
    $this->csLastLogin = $this->coDataSet->getFieldByAlias('last_login')->getValue();
    $this->cbIsActivated = $this->coDataSet->getFieldByAlias('expiration_timestamp')->getValue() ? false : true;
    $this->ciExpirationDate = $this->coDataSet->getFieldByAlias('expiration_timestamp')->getValue();
  }
  
  public function isBlocked(){
    return $this->cbIsBlocked;
  }
  
  public function getLastLogin(){
    return $this->csLastLogin;
  }
  
  public function isActivated(){
    return $this->cbIsActivated;
  }
  
  public function getExpirationDate() {
    return $this->ciExpirationDate;
  }
}
?>