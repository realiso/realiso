<?php

  require_once('FWDCodeCompresser.php');
  require_once('js_setup.php');

  $soCompresser = new FWDCodeCompresser();

  // Parametro que indica se os arquivos devem ser compactados
  $sbCompress = (isset($_GET['c'])?$_GET['c']:true);
  $ssPath = 'javascript';
  $ssExtension = 'js';
  $siExtLen = strlen($ssExtension);
  $soDir = dir($ssPath);
  while(false!==($ssFile=$soDir->read())){
    $ssFullPath = "$ssPath/$ssFile";
    if(is_file($ssFullPath) && substr($ssFile,strlen($ssFile)-$siExtLen-1)==".$ssExtension"){
      if($sbCompress){
        echo $soCompresser->compressFile($ssFullPath)."\n";
      }else{
        readfile($ssFullPath);
        echo "\n";
      }
    }
  }
  $soDir->close();

?>