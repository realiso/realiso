
/*******************************************************************************
 * Abre uma PopUp.
 * 
 * <p>Abre (cria) uma PopUp.</p>
 *
 * @package javascript
 * @param String psPopUpId Id da PopUp
 * @param String psSrc URL da p�gina a ser aberta na PopUp
 * @param String psTitle String ou id do elemento HTML que ser� o t�tulo da PopUp
 * @param integer pbModal Indica se a PopUp � modal ou n�o
 * @param integer piClientHeight Altura da �rea cliente da PopUp (opcional)
 * @param integer piClientWidth Largura da �rea cliente da PopUp (opcional)
 * @param integer piTop Top da PopUp (opcional)
 * @param integer piLeft Left da PopUp (opcional)
 * OBS: Se piTop e piLeft n�o forem passados, a PopUp � criada centralizada
 * OBS2: Se piClientHeight e piClientWidth n�o forem passados, a PopUp � criada
 *   com um tamanho padr�o e � necess�rio chamar o m�todo finishLoad quando ela
 *   terminar de carregar, para que o seu tamanho seja ajustado. Esse m�todo j�
 *   � chamado no dump_html caso a PopUp tenha o id igual ao nome do arquivo
 *   (sem a extens�o).
 *******************************************************************************/
function saas_open_popup(psPopUpId,psSrc,psTitle,pbModal,piClientHeight,piClientWidth,piTop,piLeft){
  var moTitle = gebi(psTitle);
  var mmTitle = (moTitle?moTitle:psTitle);
  var miHeight = (piClientHeight?piClientHeight:100) + (browser.isIE?25:27);
  var miWidth = (piClientWidth?piClientWidth:200) + (browser.isIE?2:4);
  var msLoading;
  
  js_open_popup(psPopUpId,psSrc,mmTitle,pbModal,miHeight,miWidth,piTop,piLeft);
  
  if(!piClientHeight || !piClientWidth){
    msLoading = '<span class="FWDLoading" style="top:25">'+soPopUpManager.getRootWindow().gebi('FWDLoading').innerHTML+'</span>';
    soPopUpManager.getPopUpById(psPopUpId).setHtmlContent(msLoading,true);
  }
  
  psPopUpId = psSrc = msLoading = psTitle = moTitle = mmTitle = null;
}