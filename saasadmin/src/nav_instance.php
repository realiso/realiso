<?php
include_once "include.php";
include_once $handlers_grid_ref."QueryGridInstance.php";
include_once $handlers_ref."QueryIsSystemConfigured.php";
include_once $handlers_ref."QueryGetInstanceInfo.php";
include_once $handlers_ref."QueryExportToExcel.php";

function setInstanceIsActive($piInstanceId,$pbIsActive){
  $moConfig = new SAASConfig();
  $moConfig->loadConfig();
  
  $moInstance = new SAASInstance();
  $moInstance->fetchById($piInstanceId);
  
  $moDB = new FWDDB(
    DB_POSTGRES,
    $moInstance->getFieldValue('instance_db_name'),
    $moInstance->getFieldValue('instance_db_user'),
    $moInstance->getFieldValue('instance_db_user_pass'),
    $moConfig->getConfig(CFG_DB_SERVER)
  );
  
  $piIsActive = ($pbIsActive?1:0);
  
  $moDB->connect();
  $moDataset = new FWDDBDataSet($moDB);
  $moDataset->setQuery("UPDATE isms_saas SET sValue = '{$piIsActive}' WHERE pkConfig = ".SAASInstance::SYSTEM_IS_ACTIVE);
  $moDataset->execute();
}

class BlockInstanceEvent extends FWDRunnable {
  public function run(){
    $miInstanceId = FWDWebLib::getObject('selected_instance_id')->getValue();
    setInstanceIsActive($miInstanceId,false);
    FWDWebLib::getObject('grid_instance')->execEventPopulate();
  }
}

class UnblockInstanceEvent extends FWDRunnable {
  public function run(){
    $miInstanceId = FWDWebLib::getObject('selected_instance_id')->getValue();
    setInstanceIsActive($miInstanceId,true);
    FWDWebLib::getObject('grid_instance')->execEventPopulate();
  }
}

class NewInstanceEvent extends FWDRunnable {  
  public function run(){
    $mbOk= false;    
    $moHandler = new QueryIsSystemConfigured(FWDWebLib::getConnection());
    $moHandler->makeQuery();
    $mbOk = $moHandler->executeQuery();    
    if ($mbOk && PROVIDER) {    
    	echo "saas_open_popup('popup_instance_insert_minimarisk','popup_instance_insert.php','','false');";
    }
    elseif ($mbOk){
    	echo "saas_open_popup('popup_instance_insert','popup_instance_insert.php','','false');";
    }
    else {
      $msTitle = FWDLanguage::getPHPStringValue('mx_warning_title','Aten��o');
      $msMessage = FWDLanguage::getPHPStringValue('mx_new_instance_warning_message',"N�o � poss�vel criar uma nova inst�ncia pois o sistema n�o est� configurado corretamente.");
      SAASLib::openOk($msTitle,$msMessage,"",30); 
    }          
  }
}

class RemoveDisabledInstancesEvent extends FWDRunnable {  
  public function run(){
    $mbOk= false;    
    $moHandler = new QueryIsSystemConfigured(FWDWebLib::getConnection());
    $moHandler->makeQuery();
    $mbOk = $moHandler->executeQuery();    
    if ($mbOk) {    
      echo "saas_open_popup('popup_disabled_instances','popup_disabled_instances.php','','false');";
    }
    else {
      $msTitle = FWDLanguage::getPHPStringValue('mx_warning_title','Aten��o');
      $msMessage = FWDLanguage::getPHPStringValue('mx_remove_disabled_instances_warning_message',"N�o � poss�vel verificar as inst�ncias desativadas pois o sistema n�o est� configurado corretamente.");
      SAASLib::openOk($msTitle,$msMessage,"",30); 
    }          
  }
}

class InstanceConfirmRemove extends FWDRunnable {
  public function run(){
    $msTitle = FWDLanguage::getPHPStringValue('tt_remove_instance','Remover Inst�ncia');
    $msMessage = FWDLanguage::getPHPStringValue('st_instance_remove_confirm',"Voc� tem certeza de que deseja remover a inst�ncia <b>%instance_alias%</b>?");
    
    $moInstance = new SAASInstance();
    $moInstance->fetchById(FWDWebLib::getObject('selected_instance_id')->getValue());
    $msInstanceName = $moInstance->getFieldValue('instance_alias');
    $msMessage = str_replace("%instance_alias%",$msInstanceName,$msMessage);
    
    $msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
                    "soWindow = soPopUp.getOpener();" .
                    "soWindow.remove_instance();";
    
    SAASLib::openConfirm($msTitle,$msMessage,$msEventValue,40);
  }
}

class RemoveInstanceEvent extends FWDRunnable {  
  public function run(){
    $moErrorHandler = new FWDDoNothingErrorHandler();
    FWDWebLib::setErrorHandler($moErrorHandler);
    $mbDebugMode = FWDWebLib::getInstance()->getDebugMode();
    $mbDebugType = FWDWebLib::getInstance()->getDebugType();
    FWDWebLib::getInstance()->setDebugMode(false,0);

    /*
     * Pega id da inst�ncia.
     */
    $miInstanceId = FWDWebLib::getObject('selected_instance_id')->getValue();
    $moInstance = new SAASInstance();
    $moInstance->fetchById($miInstanceId);
    $msAlias = $moInstance->getFieldValue('instance_alias');
    $msDBName = $moInstance->getFieldValue('instance_db_name');
    $msDBUser = $moInstance->getFieldValue('instance_db_user');  
        
    /*
     * Remove Inst�ncia.
     */
    $mbOk = SAASInstanceManager::removeInstance($miInstanceId);
    
    $msTitle = FWDLanguage::getPHPStringValue('mx_removal_status_title','Status');
    if($moErrorHandler->errorCaptured() || !$mbOk){
      $msMessage = FWDLanguage::getPHPStringValue('mx_problems_during_instance_removal',"Problemas durante a remo��o da inst�ncia <b>$msAlias</b>. Por favor, verifique se algum dos itens abaixo n�o foi removido e, caso afirmativo, remova-o manualmente.<br/><br/><b>Banco de Dados</b> - $msDBName<br/><b>Usu�rio</b> - $msDBUser<br/><b>Pasta</b> - $msAlias");
      SAASLib::openOk($msTitle,$msMessage,"",130); 
    }else{
      $msMessage = FWDLanguage::getPHPStringValue('mx_instance_successfully_removed',"Inst�ncia <b>$msAlias</b> removida com sucesso.");
      SAASLib::openOk($msTitle,$msMessage,"",25);
    }
    
    FWDWebLib::getInstance()->setDebugMode($mbDebugMode,$mbDebugType);    
    FWDWebLib::restoreLastErrorHandler();
  }
}

class GridInstance extends FWDDrawGrid {
  
  private $csDBServer = '';
  
  public function setDBServer($psDBServer){
    $this->csDBServer = $psDBServer;
  }
  
  public function drawItem(){ 
    /*
     * Carrega informa��es espec�ficas de cada inst�ncia. 
     */   
    $moDB = new FWDDB(
      DB_POSTGRES,
      $this->getFieldValue('instance_db_name'),
      $this->getFieldValue('instance_db_user'),
      $this->getFieldValue('instance_db_user_pass'),
      $this->csDBServer
    );
    $moDB->connect();
    $moQuery = new QueryGetInstanceInfo($moDB);
    $moQuery->makeQuery();
    $moQuery->executeQuery();    
    
    switch($this->ciColumnIndex){
      case $this->getIndexByAlias('instance_creation_date'):{
        $miCreationTimestamp = SAASLib::getTimestamp($this->coCellBox->getValue());
        if (($miCreationTimestamp < (SAASLib::SAASTime() - (60*60*24*DAYS_TO_REMOVE))) && (in_array($this->caData[3]/*instance_type*/, SAASLib::getTrialLicenseTypes()))) {
          if ($moQuery->getExpirationDate() >= SAASLib::SAASTime()) $this->csDivUnitClass = 'GridLineYellow';
          else $this->csDivUnitClass = 'GridLineRed2';
        }
        $this->coCellBox->setValue(SAASLib::getSAASDate($this->coCellBox->getValue()));
        return $this->coCellBox->draw();
      }
      case $this->getIndexByAlias('instance_last_login'):{
        $msLastLogin = $moQuery->getLastLogin();
        if ($msLastLogin) {
          $miNow = FWDWebLib::getTime();
          $miLogin = SAASLib::getTimestamp($msLastLogin);
          $msNow = date('Y',$miNow) . date('m',$miNow) . date('d',$miNow);
          $msLogin = date('Y',$miLogin) . date('m',$miLogin) . date('d',$miLogin);
          if ($msNow == $msLogin) $this->csDivUnitClass = 'GridLineBlue';
          $msDate = SAASLib::getSAASDate($msLastLogin);
        }
        else $msDate = '';
        $this->coCellBox->setValue($msDate);
        return $this->coCellBox->draw();
      }
      case $this->getIndexByAlias('user_packs'):{
        if (in_array($this->getFieldValue('instance_type'), SAASLib::getTrialLicenseTypes()))        
          $this->coCellBox->setValue('-');
        return $this->coCellBox->draw();
      }
      case $this->getIndexByAlias('instance_is_blocked'):{
        if($moQuery->isBlocked()){
          $this->csDivUnitClass = 'GridLineOrange';
          $msValue = FWDLanguage::getPHPStringValue('gs_yes',"sim");
          $maNotAllowed = array('block');
        }else{
          $msValue = FWDLanguage::getPHPStringValue('gs_no',"n�o");
          $maNotAllowed = array('unblock');
        }        
        $moACL = FWDACLSecurity::getInstance();
        $moACL->setNotAllowed($maNotAllowed);
        $moMenu = FWDWebLib::getObject('menu');
        $moGrid = FWDWebLib::getObject('grid_instance');
        $msLine = $moGrid->getAttrName().'_acl_'.$this->ciRowIndex;
        FWDGridMenuACLSecurity::getInstance()->installGridMenuACL($moMenu,$moACL,$moGrid,$msLine);
        
        $this->coCellBox->setValue($msValue);
        return $this->coCellBox->draw();
      }
      case $this->getIndexByAlias('instance_type'):{
        $mbIsSubscription = in_array($this->coCellBox->getValue(), SAASLib::getSubscriptionLicenseTypes());
        if ($mbIsSubscription && !$moQuery->isActivated()) $this->csDivUnitClass = 'GridLineGray';        
        else if ($mbIsSubscription) $this->csDivUnitClass = 'GridLineGreen3';
        $this->coCellBox->setValue(SAASLib::getLicenseTypeAsString($this->coCellBox->getValue()));
        return $this->coCellBox->draw();
      }
      default:{
        return parent::drawItem();
      }
    }
  }
}

class ExportEvent extends FWDRunnable {
  public function run(){
    header('Cache-Control: ');
    header('Pragma: ');
    header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="instances_info.csv"');
    
    echo  FWDLanguage::getPHPStringValue('mx_id',"Id").';'.
          FWDLanguage::getPHPStringValue('mx_alias',"Alias").';'.
          FWDLanguage::getPHPStringValue('mx_type',"Tipo").';'.
          utf8_decode(FWDLanguage::getPHPStringValue('mx_activation_code',"C�digo de Ativa��o")).';'.
          utf8_decode(FWDLanguage::getPHPStringValue('mx_creation_date',"Data de Cria��o")).';'.
          utf8_decode(FWDLanguage::getPHPStringValue('mx_last_login',"�ltimo Login")).';'.
          utf8_decode(FWDLanguage::getPHPStringValue('mx_user_packs',"User Packs")).';'.
          utf8_decode(FWDLanguage::getPHPStringValue('mx_blocked_instance',"Inst�ncia Bloqueada?")).';'.PHP_EOL;
              
    $moConfig = new SAASConfig();
    $moConfig->loadConfig();
    $msDBServer = $moConfig->getConfig(CFG_DB_SERVER);
    
    $moHandler = new QueryExportToExcel(FWDWebLib::getConnection());
    $moHandler->makeQuery();
    $maRes = $moHandler->executeQuery();
    
    foreach ($maRes as $miKey => $maInstanceInfo) {      
      $moDB = new FWDDB(
        DB_POSTGRES,
        $maInstanceInfo['instance_db_name'],
        $maInstanceInfo['instance_db_user'],
        $maInstanceInfo['instance_db_user_pass'],
        $msDBServer
      );
      $moDB->connect();
      $moQuery = new QueryGetInstanceInfo($moDB);
      $moQuery->makeQuery();
      $moQuery->executeQuery();      
      $msLastLogin = SAASLib::getSAASDate($moQuery->getLastLogin());      
      $mbIsBlocked = $moQuery->isBlocked() ? FWDLanguage::getPHPStringValue('mx_yes',"sim") : utf8_decode(FWDLanguage::getPHPStringValue('mx_no',"n�o"));
      $msType = SAASLib::getLicenseTypeAsString($maInstanceInfo['instance_type']);
      
      echo  $miKey.';'.
            $maInstanceInfo['instance_alias'].';'.
            $msType.';'.
            $maInstanceInfo['instance_code'].';'.
            SAASLib::getSAASDate($maInstanceInfo['instance_creation_date']).';'.
            $msLastLogin.';'.
            (in_array($maInstanceInfo['instance_type'], SAASLib::getTrialLicenseTypes()) ? '-' : $maInstanceInfo['instance_ups']).';'.
            $mbIsBlocked.';'.
            PHP_EOL; 
    }   
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new NewInstanceEvent('new_instance_event'));
    $moStartEvent->addAjaxEvent(new RemoveDisabledInstancesEvent('remove_disabled_instances_event'));
    $moStartEvent->addAjaxEvent(new InstanceConfirmRemove('instance_confirm_remove'));
    $moStartEvent->addAjaxEvent(new RemoveInstanceEvent('remove_instance_event'));
    $moStartEvent->addAjaxEvent(new BlockInstanceEvent('block_instance'));
    $moStartEvent->addAjaxEvent(new UnblockInstanceEvent('unblock_instance'));
    $moStartEvent->addSubmitEvent(new ExportEvent('export_event'));
    
    $moConfig = new SAASConfig();
    $moConfig->loadConfig();
    
    $moGrid = FWDWebLib::getObject("grid_instance");
    $moHandler = new QueryGridInstance(FWDWebLib::getConnection());
    $moGrid->setQueryHandler($moHandler);
    $moDrawGrid = new GridInstance();
    $moDrawGrid->setDBServer($moConfig->getConfig(CFG_DB_SERVER));
    $moGrid->setObjFwdDrawGrid($moDrawGrid);
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        function refresh_grid() {
          js_refresh_grid('grid_instance');    
        }
        
        function remove_instance() {
          trigger_event('remove_instance_event', 3);
          refresh_grid();        
        }     
      </script>
    <?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_instance.xml");
?>