<?php
set_time_limit(3600);
include_once "include.php";

$gsUserResponsibleEmail = "axur@axur.net";
$gsAlias = trim(strtolower("consoft"));
$gsCode = trim("AUf/yGEAAAAWaGfN3LfSR4EPQJKp8sylqA");
$gsDatabaseName = "inst_".$gsAlias;
$gsDatabaseLogin = "inst_".$gsAlias;
$gsDatabaseLoginPass = md5('pass_'.$gsAlias);

/*
 * Valida a entrada.
 */
if (!SAASInstance::isValidAlias($gsAlias)) die("Alias inv�lido!");
if (SAASInstance::isAliasUsed($gsAlias)) die("Alias duplicado!");
if (!SAASACtivation::isValidActivationCode($gsCode, SAASActivation::SUBSCRIPTION_ISMS)) die("C�dido de ativa��o inv�lido!");
  
/*
 * Carrega configura��es do admin.
 */
$goConfig = new SAASConfig();
$goConfig->loadConfig();

/*
 * Conecta no banco do postgres para criar as estruturas.
 */
$goDB = new FWDDB(
  DB_POSTGRES,
  $goConfig->getConfig(CFG_DB),
  $goConfig->getConfig(CFG_DB_USER),
  $goConfig->getConfig(CFG_DB_USER_PASS),
  $goConfig->getConfig(CFG_DB_SERVER)
);
$goDB->connect();

/*
 * Cria o usu�rio que vai ser utilizado pela inst�ncia.
 */
$gsSQL = str_replace(array('%database_login%', '%database_login_pass%'), array($gsDatabaseLogin, $gsDatabaseLoginPass), file_get_contents("user_create.sql"));
$goDB->execute($gsSQL."ALTER ROLE $gsDatabaseLogin SUPERUSER;");

/*
 * Cria o banco de dados que vai ser utilizado pela inst�ncia.
 */
$gsSQL = str_replace(array('%database_name%', '%database_login%'), array($gsDatabaseName, $gsDatabaseLogin), file_get_contents("db_create.sql"));
$goDB->execute($gsSQL);

/*
 * Cria a estrutura do banco de dados.
 */
$goDB = new FWDDB(
  DB_POSTGRES,
  $gsDatabaseName,
  $gsDatabaseLogin,
  $gsDatabaseLoginPass,
  $goConfig->getConfig(CFG_DB_SERVER)
);
$goDB->connect();
$goDB->execute(file_get_contents("script.sql"));

/*
 * Criar inst�ncia no banco do admin.
 */
$goInstance = new SAASInstance();    
$goInstance->setFieldValue('instance_alias', $gsAlias);
$goInstance->setFieldValue('instance_creation_date', SAASLib::SAASTime());    
$goInstance->setFieldValue('instance_db_name', $gsDatabaseName);
$goInstance->setFieldValue('instance_db_user', $gsDatabaseLogin);
$goInstance->setFieldValue('instance_db_user_pass', $gsDatabaseLoginPass);
$giId = $goInstance->insert(true);
$goUpActivation = new SAASActivation();
$goUpActivation->createFilter($gsCode, "activation_code");
$goUpActivation->setFieldValue("activation_date_used", SAASLib::SAASTime());
$goUpActivation->setFieldValue("activation_instance_id", $giId);
$goUpActivation->update();

/*
 * Seta configura��es.
 */
$goDB = new FWDDB(
  DB_POSTGRES,
  $gsDatabaseName,
  $gsDatabaseLogin,
  $gsDatabaseLoginPass,
  $goConfig->getConfig(CFG_DB_SERVER)
);
$goDB->connect();

$gaLicenseInfo = SAASLib::getLicenseInfo($goConfig, $gsAlias, SAASActivation::SUBSCRIPTION_ISMS);
$gsClientName = $gaLicenseInfo['client'];
$giMaxUsers = $gaLicenseInfo['max_users'];
$giMaxSimultUsers = $gaLicenseInfo['max_simult_users'];
$giExpirationDate = $gaLicenseInfo['expiration_date'];    
$gsModules = $gaLicenseInfo['modules'];
$giType = SAASLib::convertAdminLicenseType2SystemLicenseType($gaLicenseInfo['type']);
$giActivationDate = $gaLicenseInfo['activation_date'];
$gsMD5 = md5($gsClientName.$gsModules.$giMaxUsers.$giActivationDate.$giExpirationDate.$giMaxSimultUsers.$giType.'LICENSEHASH');

$gsEmail = $goConfig->getConfig(CFG_AXUR_EMAIL);
$gsDays = $goConfig->getConfig(CFG_ABANDONMENT_DAYS);

$goDB->execute(

// isms_config

"UPDATE isms_config SET sValue = '1' WHERE pkConfig = 402;". // email enabled
"UPDATE isms_config SET sValue = 'localhost' WHERE pkConfig = 403;". // smtp server
"UPDATE isms_config SET sValue = '0' WHERE pkConfig = 409;". // allow digest
"UPDATE isms_config SET sValue = '00:00' WHERE pkConfig = 410;". // digest time
"UPDATE isms_config SET sValue = '" . $goConfig->getConfig(CFG_AXUR_DEFAULT_SENDER) . "' WHERE pkConfig = 415;". // default sender
"UPDATE isms_config SET sValue = '" . $goConfig->getConfig(CFG_AXUR_DEFAULT_SENDER_NAME) . "' WHERE pkConfig = 430;". // default sender name
"UPDATE isms_config SET sValue = '1' WHERE pkConfig = 417;". // dc periodicy value
"UPDATE isms_config SET sValue = '7801' WHERE pkConfig = 418;". // dc periodicy type
"UPDATE isms_config SET sValue = '00:00' WHERE pkConfig = 419;". // dc time
"UPDATE isms_config SET sValue = '1' WHERE pkConfig = 421;". // dc enabled
"UPDATE isms_config SET sValue = '" . $goConfig->getConfig(CFG_DB) . "' WHERE pkConfig = 422;". // dc database
"UPDATE isms_config SET sValue = '" . $goConfig->getConfig(CFG_DB_USER) . "' WHERE pkConfig = 423;". // dc login
"UPDATE isms_config SET sValue = '" . $goConfig->getConfig(CFG_DB_USER_PASS) . "' WHERE pkConfig = 424;". // dc pass
"UPDATE isms_config SET sValue = '" . $goConfig->getConfig(CFG_DB_SERVER) . "' WHERE pkConfig = 425;". // dc host
"UPDATE isms_config SET sValue = '" . $goConfig->getConfig(CFG_AXUR_EMAIL) . "' WHERE pkConfig = 7209;". // error report email

// isms_saas

"UPDATE isms_saas SET sValue = '$gsUserResponsibleEmail' WHERE pkConfig = " . SAASInstance::EMAIL_ADMIN . ";". // administrative email
"UPDATE isms_saas SET sValue = '" . $goConfig->getConfig(CFG_ADMIN_PATH).'classes/config.php' . "' WHERE pkConfig = " . SAASInstance::CONFIG_PATH . ";". // config path
"UPDATE isms_saas SET sValue = '" . $goConfig->getConfig(CFG_MAX_UPLOAD_FILE_SIZE) . "' WHERE pkConfig = " . SAASInstance::MAX_UPLOAD_FILE_SIZE . ";". // max file size
"UPDATE isms_saas SET sValue = '$gsEmail' WHERE pkConfig = ".SAASInstance::SAAS_REPORT_EMAIL.";              
UPDATE isms_saas SET sValue = '$gsClientName' WHERE pkConfig = ".SAASInstance::LICENSE_CLIENT_NAME.";
UPDATE isms_saas SET sValue = '$gsModules' WHERE pkConfig = ".SAASInstance::LICENSE_MODULES.";
UPDATE isms_saas SET sValue = '$giMaxUsers' WHERE pkConfig = ".SAASInstance::LICENSE_MAX_USERS.";
UPDATE isms_saas SET sValue = '$giActivationDate' WHERE pkConfig = ".SAASInstance::LICENSE_ACTIVATION_DATE.";
UPDATE isms_saas SET sValue = '$giExpirationDate' WHERE pkConfig = ".SAASInstance::LICENSE_EXPIRACY.";
UPDATE isms_saas SET sValue = '$giMaxSimultUsers' WHERE pkConfig = ".SAASInstance::LICENSE_MAX_SIMULTANEOUS_USERS.";
UPDATE isms_saas SET sValue = '$giType' WHERE pkConfig = ".SAASInstance::LICENSE_TYPE.";
UPDATE isms_saas SET sValue = '$gsMD5' WHERE pkConfig = ".SAASInstance::LICENSE_HASH.";"
);

/*
 * Ajusta sequ�ncias para n�o ocorrer conflito de chave prim�ria. 
 */
$goDB->execute(file_get_contents("adjust_sequences.sql"));

//exit;

/*
 * Criar estrutura de diret�rios do saas.
 */
$gsDstFolder = FOLDER_SUBSCRIPTION."/" . $gsAlias;            
$gsHome = $goConfig->getConfig(CFG_WEB_INSTANCE_PATH);
$gsSrc = $goConfig->getConfig(CFG_WEB_INSTANCE_SRC_PATH);  
$gsDestDir = $gsHome . $gsDstFolder;
if (!is_dir($gsDestDir)) mkdir($gsDestDir, 0750, true);
 
if (substr(PHP_OS, 0, 3) != "WIN") {
  symlink($gsSrc.FOLDER_SOURCE_SYSTEM, $gsDestDir . "/sys");
  symlink($gsSrc.FOLDER_SOURCE_SYSTEM . "/index.php", $gsDestDir . "/index.php");
  mkdir($gsDestDir. "/files");
  mkdir($msDestDir. "/custom_gfx");
  touch($gsDestDir . "/debug.txt");
}
else {
  die("OS not supported!");
}

if (substr(PHP_OS, 0, 3) != "WIN") {
  if (!(chmod($gsDestDir.'/files', 0770) && chmod($gsDestDir.'/debug.txt', 0770))) {
    die("Falha no ajuste de permiss�es da pasta files e/ou do arquivo debug.txt.");
  }  
}
else {
  die ("OS not supported!");
}

/*
 * Cria config.php
 */
$gsDstFolder = FOLDER_SUBSCRIPTION."/" . $gsAlias;
$gsPath = $goConfig->getConfig(CFG_WEB_INSTANCE_PATH);
      
if (substr(PHP_OS, 0, 3) != "WIN") $gsFullPath = $gsPath.$gsDstFolder."/config.php";
else die("OS not supported!");

$gsConfig = '<?xml version="1.0" encoding="ISO-8859-1"?>
              <root>
                <database>
                  <type>DB_POSTGRES</type>
                  <host>%server%</host>
                  <port>5432</port>
                  <database>%database%</database>
                  <schema>%user%</schema>
                  <password>%password%</password>
                </database>
                <debug>
                  <type></type>
                </debug>
              </root>';
 
$goModule = mcrypt_module_open(MCRYPT_BLOWFISH,'',MCRYPT_MODE_ECB,'');
$gsKey = "b306737041cb40b1492e0debcfe45433";
mcrypt_generic_init($goModule, $gsKey, "12345678");    

$gsDatabaseLoginPassEncrypted = base64_encode(mcrypt_generic($goModule, $gsDatabaseLoginPass));

$gsConfig = str_replace(
  array('%database%','%user%','%password%','%server%'),
  array($gsDatabaseName, $gsDatabaseLogin, $gsDatabaseLoginPassEncrypted, $goConfig->getConfig(CFG_DB_SERVER)),
  $gsConfig
);
$grHandle = fopen($gsFullPath, 'w');
fwrite($grHandle, $gsConfig);
fclose($grHandle);

?>