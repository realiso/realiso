<?php      
      header('content-type: sql/plain');
      header('Content-disposition: attachment; filename="script.sql"');
            
      readfile("postgres/isms_postgres_tables_create.sql");
      echo "\n";
      
      readfile("postgres/isms_final_data.sql");
      echo "\n";
      
      readfile("postgres/isms_postgres_types_create.sql");
      echo "\n";
      
      readfile("postgres/isms_postgres_language_create.sql");
      echo "\n";
      
      $msContent = "";
      
      $maViews = file('postgres/views/order.txt');
      foreach ($maViews as $msViews) $msContent .= file_get_contents("postgres/views/".trim($msViews)).PHP_EOL.PHP_EOL;
      
      $maFunctions = file('postgres/functions/order.txt');      
      foreach ($maFunctions as $msFunction) $msContent .= file_get_contents("postgres/functions/".trim($msFunction)).PHP_EOL.PHP_EOL;
      
      $maSPs = file('postgres/stored_procedures/order.txt');      
      foreach ($maSPs as $msSP) $msContent .= file_get_contents("postgres/stored_procedures/".trim($msSP)).PHP_EOL.PHP_EOL;
      
      $maTriggers = file('postgres/triggers/order.txt');      
      foreach ($maTriggers as $msTriggers) $msContent .= file_get_contents("postgres/triggers/".trim($msTriggers)).PHP_EOL.PHP_EOL;
      
      echo $msContent."\n";
      
      readfile("postgres/isms_postgres_populate_14_alter_table_isms_context.sql");
      echo "\n";
      
      readfile("postgres/isms_postgres_constraints_create.sql");
      echo "\n";
?>