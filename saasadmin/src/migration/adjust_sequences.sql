CREATE OR REPLACE FUNCTION adjust_sequences()
  RETURNS integer AS
'DECLARE 
    id INTEGER;
BEGIN
   SELECT INTO id MAX(nseqnumber) FROM ci_nc;
   IF (id is not null) THEN EXECUTE ''ALTER SEQUENCE ci_nc_nseqnumber_seq RESTART WITH '' || id+1; END IF;

   SELECT INTO id MAX(pkclassification) FROM isms_context_classification;
   IF (id is not null) THEN EXECUTE ''ALTER SEQUENCE isms_context_classification_pkclassification_seq RESTART WITH '' || id+1; END IF;

   SELECT INTO id MAX(pkid) FROM isms_context_history;
   IF (id is not null) THEN EXECUTE ''ALTER SEQUENCE isms_context_history_pkid_seq RESTART WITH '' || id+1; END IF;   

   SELECT INTO id MAX(pkcontext) FROM isms_context;
   IF (id is not null) THEN EXECUTE ''ALTER SEQUENCE isms_context_pkcontext_seq RESTART WITH '' || id+1; END IF;

   SELECT INTO id MAX(pkconfig) FROM isms_saas;
   IF (id is not null) THEN EXECUTE ''ALTER SEQUENCE isms_saas_pkconfig_seq RESTART WITH '' || id+1; END IF;

   SELECT INTO id MAX(pkreference) FROM pm_doc_reference;
   IF (id is not null) THEN EXECUTE ''ALTER SEQUENCE pm_doc_reference_pkreference_seq RESTART WITH '' || id+1; END IF;

   SELECT INTO id MAX(pkcomment) FROM pm_instance_comment;
   IF (id is not null) THEN EXECUTE ''ALTER SEQUENCE pm_instance_comment_pkcomment_seq RESTART WITH '' || id+1; END IF;

   SELECT INTO id MAX(pkparametername) FROM rm_parameter_name;
   IF (id is not null) THEN EXECUTE ''ALTER SEQUENCE rm_parameter_name_pkparametername_seq RESTART WITH '' || id+1; END IF;

   SELECT INTO id MAX(pkvaluename) FROM rm_parameter_value_name;
   IF (id is not null) THEN EXECUTE ''ALTER SEQUENCE rm_parameter_value_name_pkvaluename_seq RESTART WITH '' || id+1; END IF;

   SELECT INTO id MAX(pkrcvaluename) FROM rm_rc_parameter_value_name;
   IF (id is not null) THEN EXECUTE ''ALTER SEQUENCE rm_rc_parameter_value_name_pkrcvaluename_seq RESTART WITH '' || id+1; END IF;

   SELECT INTO id MAX(pkalert) FROM wkf_alert;
   IF (id is not null) THEN EXECUTE ''ALTER SEQUENCE wkf_alert_pkalert_seq RESTART WITH '' || id+1; END IF;   

   SELECT INTO id MAX(pkschedule) FROM wkf_schedule;
   IF (id is not null) THEN EXECUTE ''ALTER SEQUENCE wkf_schedule_pkschedule_seq RESTART WITH '' || id+1; END IF;

   SELECT INTO id MAX(pktask) FROM wkf_task;
   IF (id is not null) THEN EXECUTE ''ALTER SEQUENCE wkf_task_pktask_seq RESTART WITH '' || id+1; END IF;

   RETURN 1;	
END'
  LANGUAGE 'plpgsql' VOLATILE;

SELECT adjust_sequences();

DROP FUNCTION adjust_sequences();