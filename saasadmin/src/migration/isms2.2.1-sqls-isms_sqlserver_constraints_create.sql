alter table ci_action_plan
   add constraint fk_ci_actio_fkcontext_isms_con foreign key (fkcontext)
      references isms_context (pkcontext)
go


alter table ci_action_plan
   add constraint fk_ci_actio_fkrespons_isms_use foreign key (fkresponsible)
      references isms_user (fkcontext)
go


alter table ci_category
   add constraint fk_ci_categ_fkcontext_isms_con foreign key (fkcontext)
      references isms_context (pkcontext)
go


alter table ci_incident
   add constraint fk_ci_incid_fkcategor_ci_categ foreign key (fkcategory)
      references ci_category (fkcontext)
go


alter table ci_incident
   add constraint fk_ci_incid_fkcontext_isms_con2 foreign key (fkcontext)
      references isms_context (pkcontext)
go


alter table ci_incident
   add constraint fk_ci_incid_fkrespons_isms_use foreign key (fkresponsible)
      references isms_user (fkcontext)
go


alter table ci_incident_control
   add constraint fk_ci_incid_fkcontext_isms_con7 foreign key (fkcontext)
      references isms_context (pkcontext)
go


alter table ci_incident_control
   add constraint fk_ci_incid_fkcontrol_rm_contr foreign key (fkcontrol)
      references rm_control (fkcontext)
      on delete cascade
go


alter table ci_incident_control
   add constraint fk_ci_incid_fkinciden_ci_incid5 foreign key (fkincident)
      references ci_incident (fkcontext)
      on delete cascade
go


alter table ci_incident_financial_impact
   add constraint fk_ci_incid_fkclassif_isms_con foreign key (fkclassification)
      references isms_context_classification (pkclassification)
go


alter table ci_incident_financial_impact
   add constraint fk_ci_incid_fkinciden_ci_incid2 foreign key (fkincident)
      references ci_incident (fkcontext)
      on delete cascade
go


alter table ci_incident_process
   add constraint fk_ci_incid_fkcontext_isms_con foreign key (fkcontext)
      references isms_context (pkcontext)
go


alter table ci_incident_process
   add constraint fk_ci_incid_fkinciden_ci_incid foreign key (fkincident)
      references ci_incident (fkcontext)
      on delete cascade
go


alter table ci_incident_process
   add constraint fk_ci_incid_fkprocess_rm_proce foreign key (fkprocess)
      references rm_process (fkcontext)
      on delete cascade
go


alter table ci_incident_risk
   add constraint fk_ci_incid_fkcontext_isms_con8 foreign key (fkcontext)
      references isms_context (pkcontext)
go


alter table ci_incident_risk
   add constraint fk_ci_incid_fkinciden_ci_incid3 foreign key (fkincident)
      references ci_incident (fkcontext)
      on delete cascade
go


alter table ci_incident_risk
   add constraint fk_ci_incid_fkrisk_rm_risk foreign key (fkrisk)
      references rm_risk (fkcontext)
      on delete cascade
go


alter table ci_incident_risk_value
   add constraint fk_ci_incid_fkincrisk_ci_incid foreign key (fkincrisk)
      references ci_incident_risk (fkcontext)
      on delete cascade
go


alter table ci_incident_risk_value
   add constraint fk_ci_incid_fkparamet_rm_param foreign key (fkparametername)
      references rm_parameter_name (pkparametername)
      on delete cascade
go


alter table ci_incident_risk_value
   add constraint fk_ci_incid_fkvaluena_rm_param foreign key (fkvaluename)
      references rm_parameter_value_name (pkvaluename)
      on delete cascade
go


alter table ci_incident_user
   add constraint fk_ci_incid_fkinciden_ci_incid4 foreign key (fkincident)
      references ci_incident (fkcontext)
      on delete cascade
go


alter table ci_incident_user
   add constraint fk_ci_incid_fkuser_isms_use foreign key (fkuser)
      references isms_user (fkcontext)
      on delete cascade
go


alter table ci_nc
   add constraint fk_ci_nc_fkcontext_isms_con foreign key (fkcontext)
      references isms_context (pkcontext)
go


alter table ci_nc
   add constraint fk_ci_nc_fkcontrol_rm_contr foreign key (fkcontrol)
      references rm_control (fkcontext)
      on delete cascade
go


alter table ci_nc
   add constraint fk_ci_nc_fkrespons_isms_use foreign key (fkresponsible)
      references isms_user (fkcontext)
go


alter table ci_nc
   add constraint fk_ci_nc_fksender_isms_use foreign key (fksender)
      references isms_user (fkcontext)
go


alter table ci_nc_action_plan
   add constraint fk_ci_nc_ac_fkactionp_ci_actio foreign key (fkactionplan)
      references ci_action_plan (fkcontext)
      on delete cascade
go


alter table ci_nc_action_plan
   add constraint fk_ci_nc_ac_fknc_ci_nc foreign key (fknc)
      references ci_nc (fkcontext)
      on delete cascade
go


alter table ci_nc_process
   add constraint fk_ci_nc_pr_fknc_ci_nc foreign key (fknc)
      references ci_nc (fkcontext)
      on delete cascade
go


alter table ci_nc_process
   add constraint fk_ci_nc_pr_fkprocess_rm_proce foreign key (fkprocess)
      references rm_process (fkcontext)
      on delete cascade
go


alter table ci_nc_seed
   add constraint fk_ci_nc_se_fkcontrol_rm_contr foreign key (fkcontrol)
      references rm_control (fkcontext)
      on delete cascade
go


alter table ci_occurrence
   add constraint fk_ci_occur_fkcontext_isms_con foreign key (fkcontext)
      references isms_context (pkcontext)
go


alter table ci_occurrence
   add constraint fk_ci_occur_fkinciden_ci_incid foreign key (fkincident)
      references ci_incident (fkcontext)
      on delete cascade
go


alter table ci_risk_probability
   add constraint fk_ci_risk__fkrisk_ci_risk_ foreign key (fkrisk)
      references ci_risk_schedule (fkrisk)
      on delete cascade
go


alter table ci_risk_probability
   add constraint fk_ci_risk__fkvaluena_rm_param foreign key (fkvaluename)
      references rm_parameter_value_name (pkvaluename)
      on delete cascade
go


alter table ci_risk_schedule
   add constraint fk_ci_risk__fkrisk_rm_risk foreign key (fkrisk)
      references rm_risk (fkcontext)
      on delete cascade
go


alter table ci_solution
   add constraint fk_ci_solut_fkcategor_ci_categ foreign key (fkcategory)
      references ci_category (fkcontext)
      on delete cascade
go


alter table ci_solution
   add constraint fk_ci_solut_fkcontext_isms_con foreign key (fkcontext)
      references isms_context (pkcontext)
go


alter table isms_acl
   add constraint fk_isms_acl_fkparent_isms_acl foreign key (fkparent)
      references isms_acl (pkacl)
go


alter table isms_context_date
   add constraint fk_isms_con_fkcontext_isms_con foreign key (fkcontext)
      references isms_context (pkcontext)
      on delete cascade
go


alter table isms_context_history
   add constraint fk_isms_con_fkcontext_isms_con2 foreign key (fkcontext)
      references isms_context (pkcontext)
      on delete cascade
go


alter table isms_policy
   add constraint fk_isms_pol_fkcontext_isms_con foreign key (fkcontext)
      references isms_context (pkcontext)
go


alter table isms_profile
   add constraint fk_isms_pro_fkcontext_isms_con foreign key (fkcontext)
      references isms_context (pkcontext)
go


alter table isms_profile_acl
   add constraint fk_isms_pro_fkacl_isms_acl foreign key (fkacl)
      references isms_acl (pkacl)
      on delete cascade
go


alter table isms_profile_acl
   add constraint fk_isms_pro_fkprofile_isms_pro foreign key (fkprofile)
      references isms_profile (fkcontext)
      on delete cascade
go


alter table isms_scope
   add constraint fk_isms_sco_fkcontext_isms_con foreign key (fkcontext)
      references isms_context (pkcontext)
go


alter table isms_solicitor
   add constraint fk_isms_sol_fksolicit_isms_use foreign key (fksolicitor)
      references isms_user (fkcontext)
go


alter table isms_solicitor
   add constraint fk_isms_sol_fkuser_isms_use foreign key (fkuser)
      references isms_user (fkcontext)
      on delete cascade
go


alter table isms_user
   add constraint fk_isms_use_fkprofile_isms_pro foreign key (fkprofile)
      references isms_profile (fkcontext)
go


alter table isms_user
   add constraint fk_isms_use_fkcontext_isms_con foreign key (fkcontext)
      references isms_context (pkcontext)
go


alter table isms_user_password_history
   add constraint fk_isms_use_fkuser_isms_use foreign key (fkuser)
      references isms_user (fkcontext)
      on delete cascade
go


alter table isms_user_preference
   add constraint fk_isms_use_fkuser_isms_use2 foreign key (fkuser)
      references isms_user (fkcontext)
      on delete cascade
go


alter table pm_document
   add constraint fk_pm_docum_fkauthor_isms_use foreign key (fkauthor)
      references isms_user (fkcontext)
go


alter table pm_document
   add constraint fk_pm_docum_fkclassif_isms_con foreign key (fkclassification)
      references isms_context_classification (pkclassification)
go


alter table pm_document
   add constraint fk_pm_docum_fkcontext_isms_con foreign key (fkcontext)
      references isms_context (pkcontext)
go


alter table pm_document
   add constraint fk_pm_docum_fkcurrent_pm_doc_i foreign key (fkcurrentversion)
      references pm_doc_instance (fkcontext)
go


alter table pm_document
   add constraint fk_pm_docum_fkmainapp_isms_use foreign key (fkmainapprover)
      references isms_user (fkcontext)
go


alter table pm_document
   add constraint fk_pm_docum_fkparent_pm_docum foreign key (fkparent)
      references pm_document (fkcontext)
go


alter table pm_document_revision_history
   add constraint fk_pm_docum_fkdocumen_pm_docum foreign key (fkdocument)
      references pm_document (fkcontext)
      on delete cascade
go


alter table pm_doc_approvers
   add constraint fk_pm_doc_a_fkdocumen_pm_docum foreign key (fkdocument)
      references pm_document (fkcontext)
      on delete cascade
go


alter table pm_doc_approvers
   add constraint fk_pm_doc_a_fkuser_isms_use foreign key (fkuser)
      references isms_user (fkcontext)
      on delete cascade
go


alter table pm_doc_context
   add constraint fk_pm_doc_c_fkcontext_isms_con foreign key (fkcontext)
      references isms_context (pkcontext)
      on delete cascade
go


alter table pm_doc_context
   add constraint fk_pm_doc_c_fkdocumen_pm_docum foreign key (fkdocument)
      references pm_document (fkcontext)
      on delete cascade
go


alter table pm_doc_instance
   add constraint fk_pm_doc_i_fkcontext_isms_con foreign key (fkcontext)
      references isms_context (pkcontext)
go


alter table pm_doc_instance
   add constraint fk_pm_doc_i_fkdocumen_pm_docum foreign key (fkdocument)
      references pm_document (fkcontext)
      on delete cascade
go


alter table pm_doc_readers
   add constraint fk_pm_doc_r_fkdocumen_pm_docum foreign key (fkdocument)
      references pm_document (fkcontext)
      on delete cascade
go


alter table pm_doc_readers
   add constraint fk_pm_doc_r_fkuser_isms_use foreign key (fkuser)
      references isms_user (fkcontext)
      on delete cascade
go


alter table pm_doc_read_history
   add constraint fk_pm_doc_r_fkinstanc_pm_doc_i foreign key (fkinstance)
      references pm_doc_instance (fkcontext)
      on delete cascade
go


alter table pm_doc_read_history
   add constraint fk_pm_doc_r_fkuser_isms_use2 foreign key (fkuser)
      references isms_user (fkcontext)
      on delete cascade
go


alter table pm_doc_reference
   add constraint fk_pm_doc_r_fkdocumen_pm_docum2 foreign key (fkdocument)
      references pm_document (fkcontext)
      on delete cascade
go


alter table pm_doc_registers
   add constraint fk_pm_doc_r_fkdocumen_pm_docum3 foreign key (fkdocument)
      references pm_document (fkcontext)
      on delete cascade
go


alter table pm_doc_registers
   add constraint fk_pm_doc_r_fkregiste_pm_regis foreign key (fkregister)
      references pm_register (fkcontext)
      on delete cascade
go


alter table pm_instance_comment
   add constraint fk_pm_insta_fkinstanc_pm_doc_i foreign key (fkinstance)
      references pm_doc_instance (fkcontext)
      on delete cascade
go


alter table pm_instance_comment
   add constraint fk_pm_insta_fkuser_isms_use foreign key (fkuser)
      references isms_user (fkcontext)
      on delete cascade
go


alter table pm_instance_content
   add constraint fk_pm_insta_fkinstanc_pm_doc_i2 foreign key (fkinstance)
      references pm_doc_instance (fkcontext)
      on delete cascade
go


alter table pm_process_user
   add constraint fk_pm_proce_fkprocess_rm_proce foreign key (fkprocess)
      references rm_process (fkcontext)
      on delete cascade
go


alter table pm_process_user
   add constraint fk_pm_proce_fkuser_isms_use foreign key (fkuser)
      references isms_user (fkcontext)
      on delete cascade
go


alter table pm_register
   add constraint fk_pm_regis_fkclassif_isms_con foreign key (fkclassification)
      references isms_context_classification (pkclassification)
go


alter table pm_register
   add constraint fk_pm_regis_fkcontext_isms_con foreign key (fkcontext)
      references isms_context (pkcontext)
go


alter table pm_register
   add constraint fk_pm_regis_fkdocumen_pm_docum foreign key (fkdocument)
      references pm_document (fkcontext)
go


alter table pm_register
   add constraint fk_pm_regis_fkrespons_isms_use foreign key (fkresponsible)
      references isms_user (fkcontext)
go


alter table pm_register_readers
   add constraint fk_pm_regis_fkregiste_pm_regis foreign key (fkregister)
      references pm_register (fkcontext)
      on delete cascade
go


alter table pm_register_readers
   add constraint fk_pm_regis_fkuser_isms_use foreign key (fkuser)
      references isms_user (fkcontext)
      on delete cascade
go


alter table pm_template
   add constraint fk_pm_templ_fkcontext_isms_con foreign key (fkcontext)
      references isms_context (pkcontext)
go


alter table pm_template_best_practice
   add constraint fk_pm_templ_fkbestpra_rm_best_ foreign key (fkbestpractice)
      references rm_best_practice (fkcontext)
      on delete cascade
go


alter table pm_template_best_practice
   add constraint fk_pm_templ_fktemplat_pm_templ foreign key (fktemplate)
      references pm_template (fkcontext)
      on delete cascade
go


alter table pm_template_content
   add constraint fk_pm_templ_fkcontext_pm_templ foreign key (fkcontext)
      references pm_template (fkcontext)
      on delete cascade
go


alter table rm_area
   add constraint fk_rm_area_fkcontext_isms_con foreign key (fkcontext)
      references isms_context (pkcontext)
go


alter table rm_area
   add constraint fk_rm_area_fkparent_rm_area foreign key (fkparent)
      references rm_area (fkcontext)
go


alter table rm_area
   add constraint fk_rm_area_fkrespons_isms_use foreign key (fkresponsible)
      references isms_user (fkcontext)
go


alter table rm_area
   add constraint fk_rm_area_fkpriorit_isms_con foreign key (fkpriority)
      references isms_context_classification (pkclassification)
go


alter table rm_area
   add constraint fk_rm_area_fktype_isms_con foreign key (fktype)
      references isms_context_classification (pkclassification)
go


alter table rm_asset
   add constraint fk_rm_asset_fkcategor_rm_categ foreign key (fkcategory)
      references rm_category (fkcontext)
      on delete cascade
go


alter table rm_asset
   add constraint fk_rm_asset_fkcontext_isms_con foreign key (fkcontext)
      references isms_context (pkcontext)
go


alter table rm_asset
   add constraint fk_rm_asset_fkrespons_isms_use foreign key (fkresponsible)
      references isms_user (fkcontext)
go


alter table rm_asset
   add constraint fk_rm_asset_fksecurit_isms_use foreign key (fksecurityresponsible)
      references isms_user (fkcontext)
go


alter table rm_asset_asset
   add constraint fk_rm_asset_fkasset_rm_asset foreign key (fkasset)
      references rm_asset (fkcontext)
go


alter table rm_asset_asset
   add constraint fk_rm_asset_fkdepende_rm_asset foreign key (fkdependent)
      references rm_asset (fkcontext)
go


alter table rm_asset_value
   add constraint fk_rm_asset_fkasset_rm_asset2 foreign key (fkasset)
      references rm_asset (fkcontext)
      on delete cascade
go


alter table rm_asset_value
   add constraint fk_rm_asset_fkparamet_rm_param foreign key (fkparametername)
      references rm_parameter_name (pkparametername)
      on delete cascade
go


alter table rm_asset_value
   add constraint fk_rm_asset_fkvaluena_rm_param foreign key (fkvaluename)
      references rm_parameter_value_name (pkvaluename)
      on delete cascade
go


alter table rm_best_practice
   add constraint fk_rm_best__fkcontext_isms_con foreign key (fkcontext)
      references isms_context (pkcontext)
go


alter table rm_best_practice
   add constraint fk_rm_best__fksection_rm_secti foreign key (fksectionbestpractice)
      references rm_section_best_practice (fkcontext)
      on delete cascade
go


alter table rm_best_practice_event
   add constraint fk_rm_best__fkcontext_context2 foreign key (fkcontext)
      references isms_context (pkcontext)
go


alter table rm_best_practice_event
   add constraint fk_rm_best__fkbestpra_rm_best_ foreign key (fkbestpractice)
      references rm_best_practice (fkcontext)
      on delete cascade
go


alter table rm_best_practice_event
   add constraint fk_rm_best__fkevent_rm_event foreign key (fkevent)
      references rm_event (fkcontext)
      on delete cascade
go


alter table rm_best_practice_standard
   add constraint fk_rm_best__fkbestpra_rm_best_2 foreign key (fkbestpractice)
      references rm_best_practice (fkcontext)
      on delete cascade
go


alter table rm_best_practice_standard
   add constraint fk_rm_best__fkcontext_context9 foreign key (fkcontext)
      references isms_context (pkcontext)
go


alter table rm_best_practice_standard
   add constraint fk_rm_best__fkstandar_rm_stand foreign key (fkstandard)
      references rm_standard (fkcontext)
      on delete cascade
go


alter table rm_category
   add constraint fk_rm_categ_fkcontext_isms_con foreign key (fkcontext)
      references isms_context (pkcontext)
go


alter table rm_category
   add constraint fk_rm_categ_fkparent_rm_categ foreign key (fkparent)
      references rm_category (fkcontext)
go


alter table rm_control
   add constraint fk_rm_contr_fkcontext_isms_con foreign key (fkcontext)
      references isms_context (pkcontext)
go


alter table rm_control
   add constraint fk_rm_contr_fkrespons_isms_use foreign key (fkresponsible)
      references isms_user (fkcontext)
go


alter table rm_control
   add constraint fk_rm_contr_fktype_isms_con foreign key (fktype)
      references isms_context_classification (pkclassification)
go


alter table rm_control_best_practice
   add constraint fk_rm_contr_fkbestpra_rm_best_ foreign key (fkbestpractice)
      references rm_best_practice (fkcontext)
      on delete cascade
go


alter table rm_control_best_practice
   add constraint fk_rm_contr_fkcontext_context2 foreign key (fkcontext)
      references isms_context (pkcontext)
go


alter table rm_control_best_practice
   add constraint fk_rm_contr_fkcontrol_rm_contr3 foreign key (fkcontrol)
      references rm_control (fkcontext)
      on delete cascade
go


alter table rm_control_cost
   add constraint fk_rm_contr_fkcontrol_rm_contr foreign key (fkcontrol)
      references rm_control (fkcontext)
      on delete cascade
go


alter table rm_control_efficiency_history
   add constraint fk_rm_contr_fkcontrol_rm_contr2 foreign key (fkcontrol)
      references rm_control (fkcontext)
      on delete cascade
go


alter table rm_control_test_history
   add constraint fk_rm_contr_fkcontrol_rm_contr6 foreign key (fkcontrol)
      references rm_control (fkcontext)
      on delete cascade
go


alter table rm_event
   add constraint fk_rm_event_fkcontext_isms_con foreign key (fkcontext)
      references isms_context (pkcontext)
go


alter table rm_event
   add constraint fk_rm_event_fkcategor_rm_categ foreign key (fkcategory)
      references rm_category (fkcontext)
      on delete cascade
go


alter table rm_event
   add constraint fk_rm_event_fktype_isms_con foreign key (fktype)
      references isms_context_classification (pkclassification)
go


alter table rm_process
   add constraint fk_rm_proce_fkarea_rm_area foreign key (fkarea)
      references rm_area (fkcontext)
      on delete cascade
go


alter table rm_process
   add constraint fk_rm_proce_fkcontext_isms_con foreign key (fkcontext)
      references isms_context (pkcontext)
go


alter table rm_process
   add constraint fk_rm_proce_fkrespons_isms_use foreign key (fkresponsible)
      references isms_user (fkcontext)
go


alter table rm_process
   add constraint fk_rm_proce_fkpriorit_isms_con foreign key (fkpriority)
      references isms_context_classification (pkclassification)
go


alter table rm_process
   add constraint fk_rm_proce_fktype_isms_con foreign key (fktype)
      references isms_context_classification (pkclassification)
go


alter table rm_process_asset
   add constraint fk_rm_proce_fkasset_rm_asset foreign key (fkasset)
      references rm_asset (fkcontext)
      on delete cascade
go


alter table rm_process_asset
   add constraint fk_rm_proce_fkcontext_context2 foreign key (fkcontext)
      references isms_context (pkcontext)
go


alter table rm_process_asset
   add constraint fk_rm_proce_fkprocess_rm_proce foreign key (fkprocess)
      references rm_process (fkcontext)
      on delete cascade
go


alter table rm_risk
   add constraint fk_rm_risk_fkasset_rm_asset foreign key (fkasset)
      references rm_asset (fkcontext)
      on delete cascade
go


alter table rm_risk
   add constraint fk_rm_risk_fkcontext_isms_con foreign key (fkcontext)
      references isms_context (pkcontext)
go


alter table rm_risk
   add constraint fk_rm_risk_fkprobabi_rm_param foreign key (fkprobabilityvaluename)
      references rm_parameter_value_name (pkvaluename)
go


alter table rm_risk
   add constraint fk_rm_risk_fktype_isms_con foreign key (fktype)
      references isms_context_classification (pkclassification)
go


alter table rm_risk_control
   add constraint fk_rm_risk__fkcontext_isms_con foreign key (fkcontext)
      references isms_context (pkcontext)
go


alter table rm_risk_control
   add constraint fk_rm_risk__fkcontrol_rm_contr foreign key (fkcontrol)
      references rm_control (fkcontext)
      on delete cascade
go


alter table rm_risk_control
   add constraint fk_rm_risk__fkrisk_rm_risk foreign key (fkrisk)
      references rm_risk (fkcontext)
      on delete cascade
go


alter table rm_risk_control
   add constraint fk_rm_risk__fkprobabi_rm_rc_pa foreign key (fkprobabilityvaluename)
      references rm_rc_parameter_value_name (pkrcvaluename)
go


alter table rm_risk_control_value
   add constraint fk_rm_risk__fkparamet_rm_param foreign key (fkparametername)
      references rm_parameter_name (pkparametername)
      on delete cascade
go


alter table rm_risk_control_value
   add constraint fk_rm_risk__fkriskcon_rm_risk_ foreign key (fkriskcontrol)
      references rm_risk_control (fkcontext)
      on delete cascade
go


alter table rm_risk_control_value
   add constraint fk_rm_risk__fkrcvalue_rm_rc_pa foreign key (fkrcvaluename)
      references rm_rc_parameter_value_name (pkrcvaluename)
      on delete cascade
go


alter table rm_risk_limits
   add constraint fk_rm_risk_lim__fkcontext_isms_con foreign key (fkcontext)
      references isms_context (pkcontext)
go


alter table rm_risk_value
   add constraint fk_rm_risk__fkparamet_rm_param2 foreign key (fkparametername)
      references rm_parameter_name (pkparametername)
      on delete cascade
go


alter table rm_risk_value
   add constraint fk_rm_risk__fkrisk_rm_risk2 foreign key (fkrisk)
      references rm_risk (fkcontext)
      on delete cascade
go


alter table rm_risk_value
   add constraint fk_rm_risk__fkvaluena_rm_param2 foreign key (fkvaluename)
      references rm_parameter_value_name (pkvaluename)
      on delete cascade
go


alter table rm_section_best_practice
   add constraint fk_rm_secti_fkcontext_isms_con foreign key (fkcontext)
      references isms_context (pkcontext)
go


alter table rm_section_best_practice
   add constraint fk_rm_secti_fkparent_rm_secti foreign key (fkparent)
      references rm_section_best_practice (fkcontext)
go


alter table rm_standard
   add constraint fk_rm_stand_fkcontext_isms_con foreign key (fkcontext)
      references isms_context (pkcontext)
go


alter table wkf_alert
   add constraint fk_wkf_aler_fkreceive_isms_use foreign key (fkreceiver)
      references isms_user (fkcontext)
      on delete cascade
go


alter table wkf_alert
   add constraint fk_wkf_aler_fkcontext_isms_con foreign key (fkcontext)
      references isms_context (pkcontext)
      on delete cascade
go


alter table wkf_alert
   add constraint fk_wkf_aler_fkcreator_isms_use foreign key (fkcreator)
      references isms_user (fkcontext)
go


alter table wkf_control_efficiency
   add constraint fk_rm_contr_fkcontrol_wkf_cont2 foreign key (fkcontrolefficiency)
      references rm_control (fkcontext)
      on delete cascade
go


alter table wkf_control_test
   add constraint fk_wkf_cont_fkcontrol_rm_contr foreign key (fkcontroltest)
      references rm_control (fkcontext)
      on delete cascade
go


alter table wkf_doc_revision
   add constraint fk_wkf_doc__fkdocrevi_pm_docum foreign key (fkdocrevision)
      references pm_document (fkcontext)
      on delete cascade
go


alter table wkf_task
   add constraint fk_wkf_task_fkcontext_isms_con foreign key (fkcontext)
      references isms_context (pkcontext)
      on delete cascade
go


alter table wkf_task
   add constraint fk_wkf_task_fkcreator_isms_use foreign key (fkcreator)
      references isms_user (fkcontext)
go


alter table wkf_task
   add constraint fk_wkf_task_fkreceive_isms_use foreign key (fkreceiver)
      references isms_user (fkcontext)
      on delete cascade
go