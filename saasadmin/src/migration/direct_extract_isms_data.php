<?php
set_time_limit(3600);
include_once "lib/fwd5_setup.php";

$gsFilename = "isms_final_data";
header('Cache-Control: ');
header('Pragma: ');
header('Content-type: application/octet-stream');
header('Content-Disposition: attachment; filename="'.$gsFilename.'.sql"');

/*
 * Conecta no banco.
 */
$soDB = new FWDDB(DB_MSSQL, "saas_consoft", "isms", "isms", "vs019");
if (!$soDB->connect()) die($soDB->getErrorMessage());
FWDWebLib::setConnection($soDB);

/*
 * Query que pega o nome de todas as tabelas do isms.
 */
if ($soDB->getDatabaseType() == DB_MSSQL)
  $gsGetTablesName = "SELECT table_name = so.name FROM sysobjects so WHERE so.xtype = 'U'";
else if ($soDB->getDatabaseType() == DB_POSTGRES)
  $gsGetTablesName = "SELECT tablename as table_name
                      FROM pg_tables
                      WHERE
                        tablename !~* 'pg_*'
                        AND tablename !~* 'sql_*'
                      ORDER BY table_name";
else
  die("Extraction not supported.");

/*
 * Tabelas que devem ser colocado o comando SET IDENTITY_INSERT ON/OFF.
 */
$gaIdentity = array(
  "ci_nc",
  "isms_acl",
  "isms_context",
  "isms_context_classification",
  "isms_context_history",
  "pm_doc_reference",
  "pm_instance_comment",
  "rm_parameter_name",
  "rm_parameter_value_name",
  "rm_rc_parameter_value_name",
  "wkf_alert",
  "wkf_task"  
);

/*
 * Extrai os dados.
 */
if ($soDB->execute($gsGetTablesName)) {  
  $gaTables = $soDB->fetchAll();
  echo "-- numero de tabelas = " . count($gaTables) . PHP_EOL;  
  foreach ($gaTables as $gaTable) {
    echo "-----------------------------------------------------------------------------------------" . PHP_EOL;
    $gsTable = $gaTable[0];
    echo "-- dados da tabela " . $gsTable . PHP_EOL;    
    if ($soDB->execute('SELECT * FROM ' . $gsTable)) {
      $giRowCount = $soDB->getRowCount();
      echo "-- numero de registros =  " . $giRowCount . PHP_EOL;
      //if ($giRowCount && (in_array($gsTable, $gaIdentity)) && ($soDB->getDatabaseType() == DB_MSSQL)) echo "SET IDENTITY_INSERT " . $gsTable . " ON;" . PHP_EOL;
      while ($gaRow = $soDB->fetch()) {
        $maColumns = array();
        $maValues = array();
        foreach ($gaRow as $gsColumnName => $gsColumnValue) {
          $maColumns[] = $gsColumnName;
          $giType = DB_UNKNOWN;
          switch ($gsColumnName[0]) {
            case 'n':case 'b':case 'p':
              $maValues[] = FWDDBType::translate($gsColumnValue, DB_NUMBER);
            break;
            case 'f':
              $msValue = FWDDBType::translate($gsColumnValue, DB_NUMBER);
              $maValues[] = $msValue == 0 ? 'null' : $msValue;
            break;
            case 's':case 't':
              $maValues[] = FWDDBType::translate($gsColumnValue, DB_STRING);
            break;
            case 'd':
              $maValues[] = FWDDBType::translate($gsColumnValue, DB_DATETIME);
            break;
            default:
              die("Nao foi possivel definir o tipo da coluna " . $gsColumnName);
            break;
          }
        }
        echo "INSERT INTO $gsTable (". implode(',', $maColumns) .") VALUES (". implode(',', $maValues) .");" . PHP_EOL;
      }
      //if ($giRowCount && (in_array($gsTable, $gaIdentity)) && ($soDB->getDatabaseType() == DB_MSSQL)) echo "SET IDENTITY_INSERT " . $gsTable . " OFF;" . PHP_EOL;
    }
    else die($soDB->getErrorMessage());
    echo "-----------------------------------------------------------------------------------------" . PHP_EOL;
  }  
}
else die($soDB->getErrorMessage());
?>