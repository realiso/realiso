* Passos que devem ser realizados para migrar um cliente que possui a vers�o 2.2.1 para a vers�o do SaaS

1. Enviar o arquivo extract_isms_data.php (compilado) para o cliente.
		a. O cliente de colocar esse arquivo na raiz do isms;
		b. Logar no sistema;
		c. Executar o script;
		d. Pegar o sql gerado e enviar por email para Axur;

2. Criar um banco no sqlserver e rodar o seguinte arquivo: isms2.2.1-sqls-isms_sqlserver_tables_views_functions_sps_triggers_create.sql.

3. Rodar o script enviado pelo cliente (isms_data.sql).

4. Inserir as constraints no banco a partir do seguinte arquivo: isms2.2.1-sqls-isms_sqlserver_constraints_create.sql

5. Rodar o script sql que leva a vers�o 2.2.1 para vers�o do trunk (antigo, n�o o trunk do saas): sqlserver_branch2273_to_trunk.sql

6. Rodar o script que leva a vers�o antiga do trunk para a vers�o atual do SaaS (esse script deve ser atualizado sempre que mudar o banco do saas): sqlserver_trunkXXXX_to_saasXXXX.sql

7. Rodar o php direct_extract_isms_data.php para extrair os dados j� atualizados para a nova vers�o.
		a. Ser� gerado um arquivo chamado isms_final_data.sql;
		b. O arquivo isms_final_data.sql deve ser colocado na pasta postgres dentro de docs;
		c. O arquivo isms_migration_scripts_compiler.php deve ser colocado na pasta docs e executado;
		d. Ser� gerado um sql (script.sql) que cont�m os dados finais que devem ser inseridos na nova inst�ncia criada no SaaS;
		
8. Deve ser alterado o arquivo migrate.php com os dados da nova inst�ncia.

9. Por �ltimo deve-se copiar os arquivos para dentro do admin do servidor e rodar o migrate.php.

10. Done! \o/