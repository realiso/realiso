/*
README!
=> SUBSTITUIR %database_login% pelo nome do usu�rio
*/

/* INSERE FUN��ES AUXILIARES */
CREATE PROCEDURE insert_acl (@p_new_tag VARCHAR(64), @p_parent_tag VARCHAR(64)/*, @p_name VARCHAR(256), @p_description VARCHAR(1024)*/)
AS
BEGIN
   DECLARE @parent_tag_id NUMERIC
   SET @parent_tag_id = (SELECT pkAcl FROM isms_acl WHERE sTag = @p_parent_tag)
   INSERT INTO isms_acl (fkParent, sDescription, sTag, sName) VALUES (@parent_tag_id, /*@p_description*/'', @p_parent_tag + '.' + @p_new_tag, /*@p_name*/'')   
END
GO

CREATE PROCEDURE update_acl (@p_tag VARCHAR(64), @p_new_name VARCHAR(256), @p_new_description VARCHAR(1024))
AS
BEGIN
   UPDATE isms_acl SET sName = @p_new_name, sDescription = @p_new_description WHERE sTag = @p_Tag
END
GO

CREATE PROCEDURE delete_acl (@p_tag VARCHAR(64))
AS
BEGIN
   DELETE FROM isms_acl WHERE sTag = @p_Tag
END
GO

CREATE PROCEDURE map_permissions (@p_tag VARCHAR(64), @p_target_tag VARCHAR(64))
AS
BEGIN
   DECLARE @tag_id NUMERIC
   SET @tag_id = (SELECT pkAcl FROM isms_acl WHERE sTag = @p_tag)

   DECLARE @target_tag_id NUMERIC
   SET @target_tag_id = (SELECT pkAcl FROM isms_acl WHERE sTag = @p_target_tag)   
   
   DECLARE @profile_id NUMERIC
   DECLARE profiles_cursor CURSOR FOR SELECT fkContext FROM isms_profile
   OPEN profiles_cursor
   FETCH NEXT FROM profiles_cursor INTO @profile_id
   WHILE (@@FETCH_STATUS = 0) BEGIN	
	IF (EXISTS(SELECT * FROM isms_profile_acl WHERE fkProfile = @profile_id and fkAcl = @target_tag_id)) BEGIN
		INSERT INTO isms_profile_acl (fkProfile, fkAcl) VALUES (@profile_id, @tag_id)
	END
	FETCH NEXT FROM profiles_cursor INTO @profile_id
   END
   CLOSE profiles_cursor
   DEALLOCATE profiles_cursor   
END
GO

CREATE PROCEDURE drop_column (@table_name VARCHAR(256), @column_name VARCHAR(256))
AS
BEGIN
	DECLARE @constraint_name VARCHAR(256)
	DECLARE @drop_sql VARCHAR(2048)
	SET @drop_sql = ''

	DECLARE delete_cursor CURSOR FOR
	SELECT
		c_obj.name as CONSTRAINT_NAME
	FROM
		sysobjects c_obj
		JOIN syscomments  com on c_obj.id = com.id
		JOIN sysobjects t_obj on c_obj.parent_obj = t_obj.id 
		JOIN sysconstraints con on c_obj.id = con.constid
		JOIN syscolumns col on t_obj.id = col.id and con.colid = col.colid
	WHERE
	        c_obj.uid = user_id() and t_obj.name like @table_name and col.name like @column_name and c_obj.xtype in ('D', 'C')

	OPEN delete_cursor
	FETCH NEXT FROM delete_cursor INTO @constraint_name
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		SET @drop_sql = @drop_sql + 'ALTER TABLE ' + @table_name + ' DROP CONSTRAINT ' + @constraint_name + ';'
		FETCH NEXT FROM delete_cursor INTO @constraint_name
	END
	CLOSE delete_cursor
	DEALLOCATE delete_cursor
	
	SET @drop_sql = @drop_sql + 'ALTER TABLE ' + @table_name + ' DROP COLUMN ' + @column_name + ';'

	EXEC (@drop_sql)
END
GO
/* FIM*/

/* ATUALIZA��O DAS ACLS */

--inserida (estat�stica) => M.D.6.13 - ESTAT�STICAS DO PLANO DE A��O (QUEM TEM M.D.6)
EXEC insert_acl '13', 'M.D.6'
EXEC map_permissions 'M.D.6.13', 'M.D.6'

--inserida (carregar riscos da biblioteca) => M.RM.3.10 - CARREGAR RISCOS DA BIBLIOTECA (QUEM TEM M.RM.3.4)
EXEC insert_acl '10', 'M.RM.3'
EXEC map_permissions 'M.RM.3.10', 'M.RM.3.4'

--inserida (config dos pesos) => M.RM.4.11 - CONFIGURA��O DOS PESOS (QUEM TEM M.RM.4.7)
EXEC insert_acl '11', 'M.RM.4'
EXEC map_permissions 'M.RM.4.11', 'M.RM.4.7'

--inserida (hist�rico) => M.RM.5.9 - HIST�RICO DE REVIS�O/TESTE (QUEM TEM M.RM.5)
EXEC insert_acl '9', 'M.RM.5'
EXEC map_permissions 'M.RM.5.9', 'M.RM.5'

--inserida (relat�rio) => M.RM.6.2.10 - ACOMPANHAMENTO DO TESTE DOS CONTROLES (QUEM TEM M.RM.6.2)
EXEC insert_acl '10', 'M.RM.6.2'
EXEC map_permissions 'M.RM.6.2.10', 'M.RM.6.2'

--inserida (relat�rio) => M.RM.6.2.11 - CONFORMIDADE (QUEM TEM M.RM.6.2)
EXEC insert_acl '11', 'M.RM.6.2'
EXEC map_permissions 'M.RM.6.2.11', 'M.RM.6.2'

--inserida (relat�rio) => M.RM.6.3.7 - RISCOS DUPLICADOS (QUEM TEM M.RM.6.3)
EXEC insert_acl '7', 'M.RM.6.3'
EXEC map_permissions 'M.RM.6.3.7', 'M.RM.6.3'

--inserida (relat�rio) => M.PM.17.2 - RELAT�RIOS GERAIS (QUEM TEM M.PM.17.1)
EXEC insert_acl '2', 'M.PM.17'
EXEC map_permissions 'M.PM.17.2', 'M.PM.17.1'

--inserida (relat�rio) => M.PM.17.2.1 - DOCUMENTOS (QUEM TEM M.PM.17.1)
EXEC insert_acl '1', 'M.PM.17.2'
EXEC map_permissions 'M.PM.17.2.1', 'M.PM.17.1'

--inserida (relat�rio) => M.PM.17.2.2 - DOCUMENTOS POR �REA (QUEM TEM M.PM.17.1.6)
EXEC insert_acl '2', 'M.PM.17.2'
EXEC map_permissions 'M.PM.17.2.2', 'M.PM.17.1.6'

--inserida (relat�rio) => M.PM.17.2.3 - DOCUMENTOS POR COMPONENTE DO SGSI (QUEM TEM M.PM.17.1)
EXEC insert_acl '3', 'M.PM.17.2'
EXEC map_permissions 'M.PM.17.2.3', 'M.PM.17.1'

--inserida (relat�rio) => M.PM.17.2.4 - COMENT�RIOS E JUSTIFICATIVAS DE DOCUMENTOS (QUEM TEM M.PM.17.1)
EXEC insert_acl '4', 'M.PM.17.2'
EXEC map_permissions 'M.PM.17.2.4', 'M.PM.17.1'

--inserida (relat�rio) => M.PM.17.2.5 - COMENT�RIOS DE USU�RIOS (QUEM TEM M.PM.17.1)
EXEC insert_acl '5', 'M.PM.17.2'
EXEC map_permissions 'M.PM.17.2.5', 'M.PM.17.1'

--inserida (relat�rio) => M.PM.17.3 - RELAT�RIOS DE REGISTRO (QUEM TEM M.PM.17.1)
EXEC insert_acl '3', 'M.PM.17'
EXEC map_permissions 'M.PM.17.3', 'M.PM.17.1'

--inserida (relat�rio) => M.PM.17.3.1 - REGISTRO POR DOCUMENTO (QUEM TEM M.PM.17.1.3)
EXEC insert_acl '1', 'M.PM.17.3'
EXEC map_permissions 'M.PM.17.3.1', 'M.PM.17.1.3'

--inserida (relat�rio) => M.PM.17.4 - RELAT�RIOS DE CONTROLE DE ACESSO (QUEM TEM M.PM.17.1)
EXEC insert_acl '4', 'M.PM.17'
EXEC map_permissions 'M.PM.17.4', 'M.PM.17.1'

--inserida (relat�rio) => M.PM.17.4.1 - AUDITORIA DE ACESSO DOS USU�RIOS (QUEM TEM M.PM.17.1.1)
EXEC insert_acl '1', 'M.PM.17.4'
EXEC map_permissions 'M.PM.17.4.1', 'M.PM.17.1.1'

--inserida (relat�rio) => M.PM.17.4.2 - AUDITORIA DE ACESSO AOS DOCUMENTOS (QUEM TEM M.PM.17.1.2)
EXEC insert_acl '2', 'M.PM.17.4'
EXEC map_permissions 'M.PM.17.4.2', 'M.PM.17.1.2'

--inserida (relat�rio) => M.PM.17.4.3 - DOCUMENTOS E SEUS APROVADORES (QUEM TEM M.PM.17.1.12)
EXEC insert_acl '3', 'M.PM.17.4'
EXEC map_permissions 'M.PM.17.4.3', 'M.PM.17.1.12'

--inserida (relat�rio) => M.PM.17.4.4 - DOCUMENTOS E SEUS LEITORES (QUEM TEM M.PM.17.1.13)
EXEC insert_acl '4', 'M.PM.17.4'
EXEC map_permissions 'M.PM.17.4.4', 'M.PM.17.1.13'

--inserida (relat�rio) => M.PM.17.4.5 - USU�RIOS ASSOCIADOS AOS PROCESSOS (QUEM TEM M.PM.17.1.14)
EXEC insert_acl '5', 'M.PM.17.4'
EXEC map_permissions 'M.PM.17.4.5', 'M.PM.17.1.14'

--inserida (relat�rio) => M.PM.17.5 - RELAT�RIOS DE GEST�O (QUEM TEM M.PM.17.1)
EXEC insert_acl '5', 'M.PM.17'
EXEC map_permissions 'M.PM.17.5', 'M.PM.17.1'

--inserida (relat�rio) => M.PM.17.5.1 - DOCUMENTOS COM LEITURA PENDENTE (QUEM TEM M.PM.17.1.15)
EXEC insert_acl '1', 'M.PM.17.5'
EXEC map_permissions 'M.PM.17.5.1', 'M.PM.17.1.15'

--inserida (relat�rio) => M.PM.17.5.2 - DOCUMENTOS MAIS ACESSADOS (QUEM TEM M.PM.17.1.8)
EXEC insert_acl '2', 'M.PM.17.5'
EXEC map_permissions 'M.PM.17.5.2', 'M.PM.17.1.8'

--inserida (relat�rio) => M.PM.17.5.3 - DOCUMENTOS N�O ACESSADOS (QUEM TEM M.PM.17.1.9)
EXEC insert_acl '3', 'M.PM.17.5'
EXEC map_permissions 'M.PM.17.5.3', 'M.PM.17.1.9'

--inserida (relat�rio) => M.PM.17.5.4 - DOCUMENTOS MAIS REVISADOS (QUEM TEM M.PM.17.1.10)
EXEC insert_acl '4', 'M.PM.17.5'
EXEC map_permissions 'M.PM.17.5.4', 'M.PM.17.1.10'

--inserida (relat�rio) => M.PM.17.5.5 - APROVA��ES PENDENTES (QUEM TEM M.PM.17.1)
EXEC insert_acl '5', 'M.PM.17.5'
EXEC map_permissions 'M.PM.17.5.5', 'M.PM.17.1'

--inserida (relat�rio) => M.PM.17.5.6 - DATA DE CRIA��O E REVIS�O DOS DOCUMENTOS (QUEM TEM M.PM.17.1.5)
EXEC insert_acl '6', 'M.PM.17.5'
EXEC map_permissions 'M.PM.17.5.6', 'M.PM.17.1.5'

--inserida (relat�rio) => M.PM.17.5.7 - CRONOGRAMA DE REVIS�O DE DOCUMENTOS (QUEM TEM M.PM.17.1)
EXEC insert_acl '7', 'M.PM.17.5'
EXEC map_permissions 'M.PM.17.5.7', 'M.PM.17.1'

--inserida (relat�rio) => M.PM.17.5.8 - DOCUMENTOS COM REVIS�O ATRASADA (QUEM TEM M.PM.17.1)
EXEC insert_acl '8', 'M.PM.17.5'
EXEC map_permissions 'M.PM.17.5.8', 'M.PM.17.1'

--inserida (relat�rio) => M.PM.17.6 - RELAT�RIOS DE ANORMALIDADE (QUEM TEM M.PM.17.1)
EXEC insert_acl '6', 'M.PM.17'
EXEC map_permissions 'M.PM.17.6', 'M.PM.17.1'

--inserida (relat�rio) => M.PM.17.6.1 - COMPONENTES SEM DOCUMENTOS (QUEM TEM M.PM.17.1.11)
EXEC insert_acl '1', 'M.PM.17.6'
EXEC map_permissions 'M.PM.17.6.1', 'M.PM.17.1.11'

--inserida (relat�rio) => M.PM.17.6.2 - TAREFAS PENDENTES (QUEM TEM M.PM.17.1)
EXEC insert_acl '2', 'M.PM.17.6'
EXEC map_permissions 'M.PM.17.6.2', 'M.PM.17.1'

--inserida (relat�rio) => M.PM.17.6.3 - DOCUMENTOS COM UMA ALTA FREQ��NCIA DE REVIS�O (QUEM TEM M.PM.17.1)
EXEC insert_acl '3', 'M.PM.17.6'
EXEC map_permissions 'M.PM.17.6.3', 'M.PM.17.1'

--inserida (relat�rio) => M.PM.17.6.4 - DOCUMENTOS SEM REGISTROS (QUEM TEM M.PM.17.1)
EXEC insert_acl '4', 'M.PM.17.6'
EXEC map_permissions 'M.PM.17.6.4', 'M.PM.17.1'

--inserida (relat�rio) => M.PM.17.6.5 - USU�RIOS COM LEITURA PENDENTE (QUEM TEM M.PM.17.1)
EXEC insert_acl '5', 'M.PM.17.6'
EXEC map_permissions 'M.PM.17.6.5', 'M.PM.17.1'

--inserida (processo disciplinar) => M.CI.5.1 - VER A��O (QUEM TEM M.CI.5)
EXEC insert_acl '1', 'M.CI.5'
EXEC map_permissions 'M.CI.5.1', 'M.CI.5'

--inserida (processo disciplinar) => M.CI.5.2 - REGISTRAR A��O (QUEM TEM M.CI.5)
EXEC insert_acl '2', 'M.CI.5'
EXEC map_permissions 'M.CI.5.2', 'M.CI.5'

--inserida (processo disciplinar) => M.CI.5.3 - REMOVER (QUEM TEM M.CI.5)
EXEC insert_acl '3', 'M.CI.5'
EXEC map_permissions 'M.CI.5.3', 'M.CI.5'

--inserida (relat�rio) => M.CI.6.2.4 - PLANO DE A��O POR USU�RIO (QUEM TEM M.CI.6.2)
EXEC insert_acl '4', 'M.CI.6.2'
EXEC map_permissions 'M.CI.6.2.4', 'M.CI.6.2'

--inserida (relat�rio) => M.CI.6.2.5 - PLANO DE A��O POR PROCESSO (QUEM TEM M.CI.6.2)
EXEC insert_acl '5', 'M.CI.6.2'
EXEC map_permissions 'M.CI.6.2.5', 'M.CI.6.2'

--inserida (relat�rio) => M.CI.6.3 - RELAT�RIOS DE GEST�O DE INCIDENTE (QUEM TEM M.CI.6)
EXEC insert_acl '3', 'M.CI.6'
EXEC map_permissions 'M.CI.6.3', 'M.CI.6'

--inserida (relat�rio) => M.CI.6.3.1 - TAREFAS PENDENTES DA GEST�O DE INCIDENTES (QUEM TEM M.CI.6)
EXEC insert_acl '1', 'M.CI.6.3'
EXEC map_permissions 'M.CI.6.3.1', 'M.CI.6'

--inserida (relat�rio) => M.CI.6.3.2 - INCIDENTES SEM RISCO (QUEM TEM M.CI.6)
EXEC insert_acl '2', 'M.CI.6.3'
EXEC map_permissions 'M.CI.6.3.2', 'M.CI.6'

--inserida (relat�rio) => M.CI.6.3.3 - N�O CONFORMIDADE SEM PLANO DE A��O (QUEM TEM M.CI.6)
EXEC insert_acl '3', 'M.CI.6.3'
EXEC map_permissions 'M.CI.6.3.3', 'M.CI.6'

--inserida (relat�rio) => M.CI.6.3.4 - PLANO DE A��O SEM DOCUMENTO (QUEM TEM M.CI.6)
EXEC insert_acl '4', 'M.CI.6.3'
EXEC map_permissions 'M.CI.6.3.4', 'M.CI.6'

--inserida (relat�rio) => M.CI.6.3.5 - INCIDENTES SEM OCORR�NCIA (QUEM TEM M.CI.6)
EXEC insert_acl '5', 'M.CI.6.3'
EXEC map_permissions 'M.CI.6.3.5', 'M.CI.6'

--inserida (templates) => M.L.2.9 - VER MODELOS DE DOCUMENTOS DE MELHOR PR�TICA (QUEM TEM M.L.2)
EXEC insert_acl '9', 'M.L.2'
EXEC map_permissions 'M.L.2.9', 'M.L.2'

--inserida (templates) => M.L.2.10 - CRIAR MODELO DE MELHOR PR�TICA (QUEM TEM M.L.2)
EXEC insert_acl '10', 'M.L.2'
EXEC map_permissions 'M.L.2.10', 'M.L.2'

--inserida (configura��es) => A.C.10 - SISTEMA (QUEM TEM A.C.1)
EXEC insert_acl '10', 'A.C'
EXEC map_permissions 'A.C.10', 'A.C.1'

/*
deletada (associar ativo na aba de processo) => M.RM.2.3
deletada (associar processo na aba de ativo) => M.RM.3.3
deletada (tratamento de risco) => M.RM.4.3
deletada (relat�rio) => M.PM.17.1.6
deletada (relat�rio) => M.PM.17.1.4
deletada (relat�rio) => M.PM.17.1.7
*/
EXEC delete_acl 'M.RM.2.3'
EXEC delete_acl 'M.RM.3.3'
EXEC delete_acl 'M.RM.4.3'
EXEC delete_acl 'M.PM.17.1.6'
EXEC delete_acl 'M.PM.17.1.4'
EXEC delete_acl 'M.PM.17.1.7'

/* FIM DA ATUALIZA��O DAS ACLS */

/* ATUALIZA��O DA ESTRUTURA DAS ACLS */

-- Insere a coluna stag na tabela isms_profile_acl

ALTER TABLE isms_profile_acl ADD
  stag                 varchar(256)         null
GO

-- Desinverte a l�gica da associa��o e j� insere com as tags

INSERT INTO isms_profile_acl (fkprofile,fkacl,stag)
SELECT p.fkcontext, a.pkacl, a.stag
FROM isms_acl a, isms_profile p
WHERE NOT EXISTS (
  SELECT *
  FROM isms_profile_acl pa
  WHERE
    pa.fkprofile = p.fkcontext
    AND pa.fkacl = a.pkacl
)
GO

-- Deleta as associa��es antigas (negadas), elas n�o tem tag

DELETE FROM isms_profile_acl WHERE stag IS NULL
GO

-- Torna a coluna stag n�o-nula

ALTER TABLE isms_profile_acl ALTER COLUMN stag varchar(256) not null
GO

-- Deleta a coluna de id de ACL

ALTER TABLE isms_profile_acl DROP CONSTRAINT pk_isms_profile_acl
GO

DROP INDEX isms_profile_acl.fkacl_fk
GO

ALTER TABLE isms_profile_acl DROP CONSTRAINT fk_isms_pro_fkacl_isms_acl
GO

ALTER TABLE isms_profile_acl DROP COLUMN fkacl
GO

-- Determina as chaves prim�rias

ALTER TABLE isms_profile_acl ADD
  constraint pk_isms_profile_acl primary key  (fkprofile, stag)
GO

-- Deleta a tabela de ACLs

DROP TABLE isms_acl
GO

-- Deleta a fun��o get_acl_tree

DROP FUNCTION get_acl_tree
GO


/* NOVAS COLUNAS */

ALTER TABLE ci_action_plan ADD
  sdocument            varchar(256)         null
GO

ALTER TABLE ci_incident_user ADD
  tactiontaken         text                 null
GO

ALTER TABLE rm_risk ADD
  tImpact            TEXT         null
GO

ALTER TABLE rm_event ADD
  tImpact            TEXT         null
GO

ALTER TABLE pm_doc_instance ADD
  sManualVersion         varchar(256)                 null
GO

ALTER TABLE rm_parameter_name ADD
  nWeight 		 INT 		null default 1
GO
UPDATE rm_parameter_name set nWeight = 1
GO

/* VIEWS ALTERADAS */

ALTER VIEW view_ci_action_plan_active AS 
 SELECT ac.fkcontext, ac.fkresponsible, ac.sname, ac.tactionplan, ac.nactiontype, ac.ddatedeadline, ac.ddateconclusion, ac.bisefficient, ac.ddateefficiencyrevision, ac.ddateefficiencymeasured, ac.ndaysbefore, ac.bflagrevisionalert, ac.bflagrevisionalertlate, ac.sdocument
   FROM isms_context c
   JOIN ci_action_plan ac ON c.pkcontext = ac.fkcontext AND c.nstate <> 2705
GO

ALTER VIEW view_ci_incident_user_active AS
SELECT fkUser, fkIncident, tDescription, tActionTaken FROM ci_incident_user
WHERE
fkUser NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2816 AND nState = 2705)
AND
fkIncident NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2831 AND nState = 2705)
GO

ALTER VIEW view_rm_event_active AS
SELECT e.fkContext, e.fkType, e.fkCategory, e.sDescription, e.tObservation, e.bPropagate, e.tImpact
FROM isms_context c JOIN rm_event e ON (c.pkContext = e.fkContext AND c.nState <> 2705)
GO

ALTER VIEW view_rm_risk_active AS
SELECT
	r.fkContext, r.fkProbabilityValueName, r.fkType, r.fkEvent, r.fkAsset, r.sName,
	r.tDescription, r.nValue, r.nValueResidual, r.tJustification, r.nAcceptMode, r.nAcceptState,
	r.sAcceptJustification, r.nCost, r.tImpact
FROM isms_context c JOIN rm_risk r ON (c.pkContext = r.fkContext AND c.nState <> 2705)
GO

ALTER VIEW view_pm_doc_instance_active AS
SELECT
	i.fkContext, i.fkDocument, i.nMajorVersion, i.nRevisionVersion, i.tRevisionJustification, i.sPath, i.tModifyComment,
	i.dBeginProduction, i.dEndProduction, i.sFilename, i.bIsLink, i.sLink,i.dBeginApprover,i.dBeginRevision,i.sManualVersion
FROM isms_context c 
  JOIN pm_doc_instance i ON (c.pkContext = i.fkContext AND c.nState <> 2705)
  JOIN isms_context ctx_doc ON (ctx_doc.pkContext = i.fkDocument AND ctx_doc.nState <> 2705)
GO

/* NOVAS CONFIGS */

INSERT INTO isms_config (pkconfig, svalue) VALUES (7230, '')
INSERT INTO isms_config (pkconfig, svalue) VALUES (7231, '')
INSERT INTO isms_config (pkconfig, svalue) VALUES (7232, '')
INSERT INTO isms_config (pkconfig, svalue) VALUES (426, '0')
INSERT INTO isms_config (pkconfig, svalue) VALUES (427, '')
INSERT INTO isms_config (pkconfig, svalue) VALUES (428, '')
INSERT INTO isms_config (pkconfig, svalue) VALUES (429, '0')

/* FUN��ES DELETADAS */

DROP FUNCTION get_topN_documented_areas
GO

DROP FUNCTION get_topN_documented_processes
GO

/* FUN��ES DE C�LCULO DE RISCO ALTERADAS */

ALTER FUNCTION get_risk_parameter_reduction  ( @piRisk NUMERIC, @piParameter NUMERIC )  
RETURNS INTEGER
AS 
BEGIN 
  DECLARE @fiResult INTEGER

  DECLARE @fiReductionValue INTEGER
  DECLARE @fiRiskValue INTEGER

  SET @fiResult = 0

  DECLARE query_reduction CURSOR
  FOR
    SELECT parameter_reduction_value, risk_parameter_value FROM 
    (
      SELECT SUM(rcpvn.nvalue) as parameter_reduction_value 
        FROM view_rm_risk_control_active rc
          JOIN rm_risk_control_value rcv ON (rcv.fkriskcontrol = rc.fkcontext)
          JOIN view_rm_control_active c ON (c.fkContext = rc.fkcontrol AND c.bIsActive = 1)
          JOIN rm_rc_parameter_value_name rcpvn ON (rcv.fkrcvaluename = rcpvn.pkrcvaluename)
        WHERE fkrisk = @piRisk AND fkparametername = @piParameter
    ) prv,
    (
       SELECT nvalue as risk_parameter_value 
         FROM rm_risk_value rv
           JOIN isms_context ctx ON (ctx.pkContext = rv.fkRisk AND ctx.nState <> 2705)
           JOIN rm_parameter_value_name pvn ON (rv.fkvaluename = pvn.pkvaluename)
         WHERE fkrisk =  @piRisk AND fkparametername = @piParameter
    ) rpv

  OPEN query_reduction
  FETCH NEXT FROM query_reduction INTO @fiReductionValue, @fiRiskValue
  CLOSE query_reduction
  DEALLOCATE query_reduction

  IF(@fiReductionValue IS NULL) SET @fiResult = @fiRiskValue
  ELSE 
  BEGIN
    SET @fiResult = @fiRiskValue - @fiReductionValue
    IF( @fiResult < 1) SET @fiResult = 1
  END

  RETURN @fiResult
END
GO

ALTER FUNCTION get_risk_value_residual  ( @piFkRisk NUMERIC )  
RETURNS FLOAT
AS 
BEGIN 

  DECLARE @fsName VARCHAR(256)
  DECLARE @fiAssetValue FLOAT

  DECLARE @fiRiskProb FLOAT
  DECLARE @fiProbReduction FLOAT
  DECLARE @fiProbResult FLOAT
  DECLARE @fiWeightQuery INTEGER

  DECLARE @fiResult FLOAT
  DECLARE @miParWeight INTEGER
  DECLARE @fiBlueRisk INTEGER
  DECLARE @fiParameterID INTEGER

  DECLARE @fiCountRisk INTEGER
  DECLARE @fiCountAsset INTEGER
  DECLARE @fiCountSystem INTEGER

  SET @miParWeight = 0
  SET @fiResult = 0
  SET @fiBlueRisk = 0

  DECLARE compare_cursor CURSOR
  FOR 
    SELECT count_sys, count_risk, count_asset
      FROM (SELECT count(pkparametername) as count_sys  FROM rm_parameter_name pn) buffer_sys,
           (SELECT count(fkparametername) as count_risk FROM rm_risk_value WHERE fkrisk = @piFkRisk) buffer_risk,
           (SELECT count(fkparametername) as count_asset FROM rm_asset_value av
              JOIN view_rm_risk_active r ON (r.fkasset = av.fkasset AND r.fkContext = @piFkRisk)
              JOIN view_rm_asset_active a ON (a.fkContext = av.fkAsset)
            ) buffer_asset

  DECLARE values_cursor CURSOR
  FOR 
    SELECT pn.sname as parameter_name,  pn.pkparametername, pvn.nvalue as asset_value, pn.nweight as parameter_weight 
      FROM view_rm_risk_active r
        JOIN isms_context ctx ON(ctx.pkContext = r.fkAsset AND r.fkContext = @piFkRisk)
        JOIN rm_asset_value av ON (av.fkasset = r.fkasset)
        JOIN rm_parameter_name pn ON (av.fkparametername = pn.pkparametername)
        JOIN rm_parameter_value_name pvn ON (av.fkvaluename = pvn.pkvaluename)

  DECLARE probability_cursor CURSOR
  FOR
    SELECT prob_reduction_value, risk_prob_value FROM 
    (
      SELECT sum(rcpvn.nvalue) as prob_reduction_value
        FROM rm_risk_control rc
          JOIN isms_context ctx ON(ctx.pkContext = rc.fkRisk and ctx.nState <> 2705 AND rc.fkrisk = @piFkRisk)
          JOIN view_rm_control_active c ON (c.fkContext = rc.fkControl AND c.bIsActive = 1)
          JOIN rm_rc_parameter_value_name rcpvn ON (rc.fkprobabilityvaluename = rcpvn.pkrcvaluename)
    )prv,
    (
      SELECT pvn.nvalue as risk_prob_value 
        FROM view_rm_risk_active r
          JOIN rm_parameter_value_name pvn ON (r.fkprobabilityvaluename = pvn.pkvaluename AND r.fkContext = @piFkRisk)
    )rpv

  OPEN compare_cursor
  FETCH NEXT FROM compare_cursor INTO @fiCountSystem, @fiCountRisk, @fiCountAsset
    IF ( (@fiCountSystem <> @fiCountRisk) or (@fiCountSystem <> @fiCountAsset) )
    BEGIN
      SET @fiBlueRisk = 1
    END
  CLOSE compare_cursor
  DEALLOCATE compare_cursor

  IF(@fiBlueRisk = 1)
  BEGIN
    DEALLOCATE values_cursor
    DEALLOCATE probability_cursor
    RETURN 0
  END

  OPEN values_cursor
  FETCH NEXT FROM values_cursor INTO @fsName, @fiParameterID, @fiAssetValue, @fiWeightQuery
  WHILE (@@FETCH_STATUS = 0)
    BEGIN  
      SET @miParWeight = @miParWeight + @fiWeightQuery
      SET @fiResult = @fiResult + ( %database_login%.get_risk_parameter_reduction(@piFkRisk, @fiParameterID) + @fiAssetValue ) * @fiWeightQuery
      FETCH NEXT FROM values_cursor INTO @fsName, @fiParameterID, @fiAssetValue, @fiWeightQuery
    END
  CLOSE values_cursor
  DEALLOCATE values_cursor

  OPEN probability_cursor
  FETCH NEXT FROM probability_cursor INTO @fiProbReduction, @fiRiskProb

  IF(@fiProbReduction IS NULL) 
    SET @fiProbResult = @fiRiskProb
  ELSE 
    BEGIN
      SET @fiProbResult = @fiRiskProb - @fiProbReduction
      IF( @fiProbResult < 1) SET @fiProbResult = 1
    END
  IF (@miParWeight >= 1)
    SET @fiResult = ( @fiResult * @fiProbResult ) / (@miParWeight * 2)
  ELSE
    SET @fiResult = 0;
  CLOSE probability_cursor
  DEALLOCATE probability_cursor

  RETURN @fiResult
END
GO

ALTER FUNCTION get_risk_value( @piFkRisk NUMERIC )  
RETURNS FLOAT
AS 
BEGIN 

  DECLARE @fsName VARCHAR(256)
  DECLARE @fiRiskValue FLOAT
  DECLARE @fiAssetValue FLOAT
  DECLARE @fiRiskProb FLOAT
  DECLARE @fiResult FLOAT
  DECLARE @fiI INTEGER
  DECLARE @fiBlueRisk INTEGER

  DECLARE @fiCountRisk INTEGER
  DECLARE @fiCountAsset INTEGER
  DECLARE @fiCountSystem INTEGER

  DECLARE @fiWeightQuery INTEGER
  DECLARE @miParWeight INTEGER

  SET @miParWeight = 0
  SET @fiResult = 0
  SET @fiBlueRisk = 0

  DECLARE compare_cursor CURSOR
  FOR 
    SELECT count_sys, count_risk, count_asset 
      FROM (SELECT count(pkparametername) as count_sys  FROM rm_parameter_name pn) buffer_sys,
           (SELECT count(fkparametername) as count_risk FROM rm_risk_value WHERE fkrisk =   @piFkRisk) buffer_risk,
           (SELECT count(fkparametername) as count_asset 
              FROM rm_asset_value av
                JOIN view_rm_risk_active r ON (r.fkasset = av.fkasset AND r.fkcontext =   @piFkRisk)
                JOIN isms_context ctx ON(ctx.pkContext = r.fkAsset AND ctx.nState <> 2705)
            ) buffer_asset

  DECLARE values_cursor CURSOR
  FOR
    SELECT pn.sname as parameter_name, pvn.nvalue as risk_value, pvn2.nvalue as asset_value, pn.nweight as parameter_weight 
      FROM view_rm_risk_active r
        JOIN isms_context ctx ON (ctx.pkContext = r.fkAsset AND ctx.nState <> 2705 AND r.fkContext = @piFkRisk)
        JOIN rm_risk_value rv ON (rv.fkrisk = r.fkcontext)
        JOIN rm_parameter_value_name pvn ON (rv.fkvaluename = pvn.pkvaluename)
        JOIN rm_parameter_name pn ON (rv.fkparametername = pn.pkparametername)
        JOIN rm_asset_value av ON (pn.pkparametername = av.fkparametername AND av.fkasset = r.fkasset)
        JOIN rm_parameter_value_name pvn2 ON (av.fkvaluename = pvn2.pkvaluename)

  DECLARE probability_cursor CURSOR
  FOR
    SELECT pvn.nvalue as risk_prob_value 
      FROM view_rm_risk_active r
        JOIN rm_parameter_value_name pvn ON (r.fkprobabilityvaluename = pvn.pkvaluename)
      WHERE r.fkcontext =  @piFkRisk

  OPEN compare_cursor
  FETCH NEXT FROM compare_cursor INTO @fiCountSystem, @fiCountRisk, @fiCountAsset
    IF ( (@fiCountSystem <> @fiCountRisk) or (@fiCountSystem <> @fiCountAsset) )
    BEGIN
      SET @fiBlueRisk = 1
    END
  CLOSE compare_cursor
  DEALLOCATE compare_cursor

  IF(@fiBlueRisk = 1)
  BEGIN
    DEALLOCATE values_cursor
    DEALLOCATE probability_cursor
    RETURN 0
  END

  OPEN values_cursor
  FETCH NEXT FROM values_cursor INTO @fsName, @fiRiskValue, @fiAssetValue, @fiWeightQuery
  WHILE (@@FETCH_STATUS = 0)
    BEGIN  
      SET @miParWeight = @miParWeight + @fiWeightQuery
      SET @fiResult = @fiResult + (@fiAssetValue + @fiRiskValue) * @fiWeightQuery
      FETCH NEXT FROM values_cursor INTO @fsName, @fiRiskValue, @fiAssetValue, @fiWeightQuery
    END
  CLOSE values_cursor
  DEALLOCATE values_cursor

  OPEN probability_cursor
  FETCH NEXT FROM probability_cursor INTO @fiRiskProb
  IF(@miParWeight >= 1)
    BEGIN
      SET @fiResult = (@fiResult * @fiRiskProb) / (@miParWeight * 2)
    END
  ELSE
    BEGIN
      SET @fiResult = (@fiResult * @fiRiskProb) / (@miParWeight * 2)
    END
  CLOSE probability_cursor
  DEALLOCATE probability_cursor

RETURN @fiResult
END
GO

/* ATUALIZA��O DA ESTRUTURA DOS SCHEDULES */

------ migra��o oficial do schedule

------------------------------------------------------------ Cria��o da tabela wkf_schedule

create table wkf_schedule (
   pkschedule           numeric              identity,
   dstart               datetime             not null,
   dend                 datetime             null,
   ntype                int                  not null default 0,
   nperiodicity         int                  null default 0,
   nbitmap              int                  null default 0,
   nweek                int                  null default 0,
   nday                 int                  null default 0,
   dnextoccurrence      datetime             null,
   ndaystofinish        int                  null default 0,
   ddatelimit           datetime             null,
   constraint pk_wkf_schedule primary key  (pkschedule)
)
go

------------------------------------------------------------ Cria��o da tabela wkf_task_schedule

create table wkf_task_schedule (
   fkcontext            numeric              not null,
   nactivity            int                  not null default 0,
   fkschedule           numeric              null,
   balertsent           int                  not null default 0
      constraint ckc_balertsent_wkf_task check (balertsent between 0 and 1),
   nalerttype           int                  null default 0,
   constraint pk_wkf_task_schedule primary key  (fkcontext, nactivity)
)
go

create   index fkcontext_fk on wkf_task_schedule (
fkcontext
)
go

create   index fkschedule_fk on wkf_task_schedule (
fkschedule
)
go

alter table wkf_task_schedule
   add constraint fk_wkf_task_fkcontext_isms_co2 foreign key (fkcontext)
      references isms_context (pkcontext)
      on delete cascade
go

alter table wkf_task_schedule
   add constraint fk_wkf_task_fkschedul_wkf_sche foreign key (fkschedule)
      references wkf_schedule (pkschedule)
      on delete cascade
go

----------------------------------------------- Nova coluna fkSchedule em pm_document

ALTER TABLE pm_document ADD fkschedule numeric null
GO

create   index fkschedule_fk on pm_document (
fkschedule
)
go

alter table pm_document
   add constraint fk_pm_docum_fkschedul_wkf_sche foreign key (fkschedule)
      references wkf_schedule (pkschedule)
go

----------------------------------------------- Nova coluna fkSchedule em wkf_control_efficiency

ALTER TABLE wkf_control_efficiency ADD
  fkschedule numeric null  
GO

create   index fkschedule_fk on wkf_control_efficiency (
fkschedule
)
go

alter table wkf_control_efficiency
   add constraint fk_wkf_cont_fkschedul_wkf_sche foreign key (fkschedule)
      references wkf_schedule (pkschedule)
      on delete cascade
go

----------------------------------------------- Nova coluna fkSchedule em wkf_control_test

ALTER TABLE wkf_control_test ADD
  fkschedule numeric null  
GO

create   index fkschedule_fk on wkf_control_test (
fkschedule
)
go

alter table wkf_control_test
   add constraint fk_wkf_cont_fkschedul_wkf_sch2 foreign key (fkschedule)
      references wkf_schedule (pkschedule)
      on delete cascade
go

-------------------------------------------------------- Migra��o dos dados de schedule

CREATE PROCEDURE migrate_schedules
AS
BEGIN
  
  DECLARE @miContextId INT
  DECLARE @miDaysBefore INT
  DECLARE @mdDateNext DATETIME
  DECLARE @miPeriod INT
  DECLARE @miValue INT
  DECLARE @mbFlagAlert INT
  DECLARE @miState INT
  
  DECLARE @mdNextOccurrence DATETIME
  DECLARE @mdStart DATETIME
  
  -- Converte os schedules de revisao de documentos
  DECLARE document_revisions CURSOR LOCAL FOR
    SELECT
      d.fkContext,
      d.nDaysBefore,
      dr.dDateNext,
      dr.nPeriod,
      dr.nValue,
      dr.bFlagAlert,
      c.nState
    FROM
      wkf_doc_revision dr
      JOIN pm_document d ON (d.fkContext = dr.fkDocRevision)
      JOIN isms_context c ON (c.pkContext = d.fkContext)
  OPEN document_revisions
  FETCH NEXT FROM document_revisions INTO @miContextId,@miDaysBefore,@mdDateNext,@miPeriod,@miValue,@mbFlagAlert,@miState
  WHILE(@@FETCH_STATUS = 0) BEGIN
    -- Data de inicio do schedule
    SELECT @mdStart = MIN(dDateCreated) FROM wkf_task WHERE fkContext = @miContextId
    IF @mdStart IS NULL BEGIN
      SET @mdStart = getdate()
    END
    -- Proxima ocorrencia
    SET @mdNextOccurrence = DATEADD(day,-@miDaysBefore,@mdDateNext)
    -- Insere o schedule
    INSERT INTO wkf_schedule(dStart  , nType    , nPeriodicity, nDay                  , dNextOccurrence  , nDaysToFinish, dDateLimit )
                     VALUES (@mdStart, @miPeriod, @miValue    , day(@mdNextOccurrence), @mdNextOccurrence, @miDaysBefore, @mdDateNext);
    -- Associa o schedule com o documento
    UPDATE pm_document SET fkSchedule = SCOPE_IDENTITY() WHERE fkContext = @miContextId
    -- Cria um TaskSchedule se o documento estiver publicado (ACT_DOCUMENT_REVISION = 2221 e WKF_ALERT_DOCUMENT_REVISION_LATE = 9236)
    IF @miState = 2752 BEGIN
      INSERT INTO wkf_task_schedule(fkcontext   , fkschedule      , nactivity, nalerttype, balertsent  )
                            VALUES (@miContextId, SCOPE_IDENTITY(),      2221,       9236, @mbFlagAlert)
    END
    FETCH NEXT FROM document_revisions INTO @miContextId,@miDaysBefore,@mdDateNext,@miPeriod,@miValue,@mbFlagAlert,@miState
  END
  
  -- Converte os schedules de revisao de controles
  DECLARE control_revision CURSOR LOCAL FOR
    SELECT
      c.fkContext,
      c.nDaysBefore,
      ce.dDateNext,
      ce.nPeriod,
      ce.nValue,
      ce.bFlagAlert,
      ctxt.nState
    FROM
      rm_control c
      JOIN wkf_control_efficiency ce ON (ce.fkControlEfficiency = c.fkContext)
      JOIN isms_context ctxt ON (ctxt.pkContext = c.fkContext)
  OPEN control_revision
  FETCH NEXT FROM control_revision INTO @miContextId,@miDaysBefore,@mdDateNext,@miPeriod,@miValue,@mbFlagAlert,@miState
  WHILE(@@FETCH_STATUS = 0) BEGIN
    -- Data de inicio do schedule
    SELECT @mdStart = MIN(dDateCreated) FROM wkf_task WHERE fkContext = @miContextId
    IF @mdStart IS NULL BEGIN
      SET @mdStart = getdate()
    END
    -- Proxima ocorrencia
    SET @mdNextOccurrence = DATEADD(day,-@miDaysBefore,@mdDateNext)
    -- Insere o schedule
    INSERT INTO wkf_schedule(dStart  , nType    , nPeriodicity, nDay                  , dNextOccurrence  , nDaysToFinish, dDateLimit )
                     VALUES (@mdStart, @miPeriod, @miValue    , day(@mdNextOccurrence), @mdNextOccurrence, @miDaysBefore, @mdDateNext);
    -- Associa o schedule com a revisao de controle
    UPDATE wkf_control_efficiency SET fkSchedule = SCOPE_IDENTITY() WHERE fkControlEfficiency = @miContextId
    -- Cria um TaskSchedule (ACT_REAL_EFFICIENCY = 2208 e WKF_ALERT_REAL_EFFICIENCY = 9206)
    INSERT INTO wkf_task_schedule(fkcontext   , fkschedule      , nactivity, nalerttype, balertsent  )
                          VALUES (@miContextId, SCOPE_IDENTITY(),      2208,       9206, @mbFlagAlert)
    FETCH NEXT FROM control_revision INTO @miContextId,@miDaysBefore,@mdDateNext,@miPeriod,@miValue,@mbFlagAlert,@miState
  END
  
  -- Converte os schedules de teste de controles
  DECLARE control_test CURSOR LOCAL FOR
    SELECT
      c.fkContext,
      c.nDaysBefore,
      ct.dDateNext,
      ct.nPeriod,
      ct.nValue,
      ct.bFlagAlert,
      ctxt.nState
    FROM
      rm_control c
      JOIN wkf_control_test ct ON (ct.fkControlTest = c.fkContext)
      JOIN isms_context ctxt ON (ctxt.pkContext = c.fkContext)
  OPEN control_test
  FETCH NEXT FROM control_test INTO @miContextId,@miDaysBefore,@mdDateNext,@miPeriod,@miValue,@mbFlagAlert,@miState
  WHILE(@@FETCH_STATUS = 0) BEGIN
    -- Data de inicio do schedule
    SELECT @mdStart = MIN(dDateCreated) FROM wkf_task WHERE fkContext = @miContextId
    IF @mdStart IS NULL BEGIN
      SET @mdStart = getdate()
    END
    -- Proxima ocorrencia
    SET @mdNextOccurrence = DATEADD(day,-@miDaysBefore,@mdDateNext)
    -- Insere o schedule
    INSERT INTO wkf_schedule(dStart  , nType    , nPeriodicity, nDay                  , dNextOccurrence  , nDaysToFinish, dDateLimit )
                     VALUES (@mdStart, @miPeriod, @miValue    , day(@mdNextOccurrence), @mdNextOccurrence, @miDaysBefore, @mdDateNext);
    -- Associa o schedule com o teste de controle
    UPDATE wkf_control_test SET fkSchedule = SCOPE_IDENTITY() WHERE fkControlTest = @miContextId
    -- Cria um TaskSchedule (ACT_CONTROL_TEST =  2216 e WKF_ALERT_TEST_CONTROL = 9209)
    INSERT INTO wkf_task_schedule(fkcontext   , fkschedule      , nactivity, nalerttype, balertsent  )
                          VALUES (@miContextId, SCOPE_IDENTITY(),      2216,       9209, @mbFlagAlert)
    FETCH NEXT FROM control_test INTO @miContextId,@miDaysBefore,@mdDateNext,@miPeriod,@miValue,@mbFlagAlert,@miState
  END
  
END
GO

EXEC migrate_schedules
GO

DROP PROCEDURE migrate_schedules
GO

-------------------------------------------------- Modifica��es na tabela wkf_control_efficiency

ALTER TABLE wkf_control_efficiency ADD  
  tmetric text null
GO

EXEC sp_rename 'wkf_control_efficiency.[smetric1]', 'svalue1', 'COLUMN'
GO
EXEC sp_rename 'wkf_control_efficiency.[smetric2]', 'svalue2', 'COLUMN'
GO
EXEC sp_rename 'wkf_control_efficiency.[smetric3]', 'svalue3', 'COLUMN'
GO
EXEC sp_rename 'wkf_control_efficiency.[smetric4]', 'svalue4', 'COLUMN'
GO
EXEC sp_rename 'wkf_control_efficiency.[smetric5]', 'svalue5', 'COLUMN'
GO

EXEC drop_column 'wkf_control_efficiency' , 'ddatenext'
EXEC drop_column 'wkf_control_efficiency' , 'ddatelimit'
EXEC drop_column 'wkf_control_efficiency' , 'nvalue'
EXEC drop_column 'wkf_control_efficiency' , 'nperiod'
EXEC drop_column 'wkf_control_efficiency' , 'bflagalert'

---------------------------------------------------------- Altera��es na tabela wkf_control_test

EXEC drop_column 'wkf_control_test' , 'ddatenext'
EXEC drop_column 'wkf_control_test' , 'ddatelimit'
EXEC drop_column 'wkf_control_test' , 'nvalue'
EXEC drop_column 'wkf_control_test' , 'nperiod'
EXEC drop_column 'wkf_control_test' , 'bflagalert'

----------------------------------------------------------- Dele��o da tabela wkf_doc_revision

DROP TABLE wkf_doc_revision
GO

/* FIM DA ATUALIZA��O DA ESTRUTURA DO SCHEDULE */

/* VIEWS ALTERADAS */

ALTER VIEW view_pm_document_active AS
SELECT 
  d.fkContext,
  d.fkClassification,
  d.fkCurrentVersion,
  d.fkParent,
  d.fkAuthor,
  d.fkMainApprover,
  d.sName,
  d.tDescription,
  d.nType,
  d.sKeywords,
  d.dDateProduction,
  d.dDeadline,
  d.bFlagDeadlineAlert,
  d.bFlagDeadlineExpired,
  d.nDaysBefore,
  d.bHasApproved,
  d.fkSchedule
FROM
  isms_context c
  JOIN pm_document d ON (c.pkContext = d.fkContext AND c.nState <> 2705)
GO

ALTER VIEW view_pm_published_docs AS
SELECT
  d.fkContext,
  d.fkClassification,
  d.fkCurrentVersion,
  d.fkParent,
  d.fkAuthor,
  d.fkMainApprover,
  d.sName,
  d.tDescription,
  d.nType,
  d.sKeywords,
  d.dDateProduction,
  d.dDeadline,
  d.bFlagDeadlineAlert,
  d.bFlagDeadlineExpired,
  d.nDaysBefore,
  d.bHasApproved,
  d.fkSchedule
FROM
  view_pm_document_active d
  JOIN pm_doc_instance di ON (di.fkContext = d.fkCurrentVersion AND di.nMajorVersion > 0)
GO

ALTER VIEW view_pm_read_docs AS
SELECT
  d.fkContext,
  d.fkClassification,
  d.fkCurrentVersion,
  d.fkParent,
  d.fkAuthor,
  d.fkMainApprover,
  d.sName,
  d.tDescription,
  d.nType,
  d.sKeywords,
  d.dDateProduction,
  d.dDeadline,
  d.bFlagDeadlineAlert,
  d.bFlagDeadlineExpired,
  d.nDaysBefore,
  d.bHasApproved,
  d.fkSchedule
FROM
  view_pm_document_active d
  JOIN pm_doc_read_history rh ON (rh.fkInstance = d.fkCurrentVersion)
GO

/* STORED PROCEDURE ALTERADA */

ALTER PROCEDURE create_stats
AS
BEGIN

/*======================================TESTE PARA VER SE NECESSITA EXECUTAR A FUN��O=========================================*/

DECLARE @periodicy_value INT
SET @periodicy_value = (SELECT sValue FROM isms_config WHERE pkConfig = 417)

DECLARE @periodicy_type INT
SET @periodicy_type = (SELECT sValue FROM isms_config WHERE pkConfig = 418)

DECLARE @date_last_run DATETIME
SET @date_last_run = (SELECT sValue FROM isms_config WHERE pkConfig = 420)

IF %database_login%.get_next_date(@date_last_run, @periodicy_type, @periodicy_value) <= getdate()
BEGIN

/*===================================================VARI�VEIS GLOBAIS===========================================================*/

DECLARE @risk_low INT
DECLARE @risk_high INT

SELECT
  @risk_low = rl.nLow,
  @risk_high = rl.nHigh
FROM
  rm_risk_limits rl
  JOIN isms_context c ON (rl.fkContext = c.pkContext)
WHERE c.nState = 2702

/*===============================================INFORMA��ES SOBRE �REA========================================================*/

-- Valor da �rea

DECLARE @area_id INT
DECLARE @area_value FLOAT

DECLARE area_value_cursor CURSOR FOR
	SELECT fkContext, nValue FROM view_rm_area_active

OPEN area_value_cursor
FETCH NEXT FROM area_value_cursor INTO @area_id, @area_value
WHILE (@@FETCH_STATUS = 0)
BEGIN
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (@area_id, getdate(), 'area_value', @area_value)
	FETCH NEXT FROM area_value_cursor INTO @area_id, @area_value
END
CLOSE area_value_cursor
DEALLOCATE area_value_cursor

-- N� total de �reas

DECLARE  @areas_total INT
SET @areas_total = (SELECT count(*) FROM view_rm_area_active)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'areas_total', @areas_total)

-- N� �reas com risco alto

DECLARE  @areas_with_high_risk INT
SET @areas_with_high_risk = (SELECT count(*) FROM view_rm_area_active WHERE nValue >= @risk_high)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'areas_with_high_risk', @areas_with_high_risk)

-- N� �reas com risco m�dio

DECLARE  @areas_with_medium_risk INT
SET @areas_with_medium_risk = (SELECT count(*) FROM view_rm_area_active WHERE nValue > @risk_low AND nValue < @risk_high)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'areas_with_medium_risk', @areas_with_medium_risk)

-- N� �reas com risco baixo

DECLARE  @areas_with_low_risk INT
SET @areas_with_low_risk = (SELECT count(*) FROM view_rm_area_active WHERE nValue <= @risk_low AND nValue <> 0)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'areas_with_low_risk', @areas_with_low_risk)

-- N� �reas com risco n�o parametrizado

DECLARE  @areas_with_no_value_risk INT
SET @areas_with_no_value_risk = (SELECT count(*) FROM view_rm_area_active WHERE nValue = 0)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'areas_with_no_value_risk', @areas_with_no_value_risk)

-- N� �reas completas

DECLARE  @complete_areas INT
SET @complete_areas = (
	SELECT count(*) FROM view_rm_area_active a
	WHERE a.nValue > 0 AND NOT EXISTS (SELECT * FROM view_rm_process_active p WHERE p.fkArea = a.fkContext AND p.nValue=0)
)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'complete_areas', @complete_areas)

-- N� �reas parcias

DECLARE  @partial_areas INT
SET @partial_areas = (
	SELECT count(*) FROM view_rm_area_active a
	WHERE a.nValue > 0 AND EXISTS (SELECT * FROM view_rm_process_active p WHERE p.fkArea = a.fkContext AND p.nValue = 0)
)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'partial_areas', @partial_areas)

-- N� �reas n�o gerenciadas

DECLARE  @not_managed_areas INT
SET @not_managed_areas = (SELECT count(*) FROM view_rm_area_active a WHERE a.nValue = 0)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'not_managed_areas', @not_managed_areas)

-- N� �reas sem processos

DECLARE  @areas_without_processes INT
SET @areas_without_processes = (
	SELECT count(*)
	FROM view_rm_area_active a
	WHERE a.fkContext NOT IN (SELECT fkArea FROM view_rm_process_active)
)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'areas_without_processes', @areas_without_processes)

-- Total de impacto das �reas e qtd. de riscos por �rea

DECLARE @total_area_impact FLOAT
DECLARE @risk_amount_by_area INT

DECLARE total_area_impact_and_risk_amount_by_area CURSOR FOR
	SELECT area_id, count(risk_id) as risk_count, CASE WHEN sum(r.nCost) IS NULL THEN 0 ELSE sum(r.nCost) END as total_impact
	FROM
	(
		SELECT ar.fkContext as area_id, r.fkContext as risk_id
		FROM view_rm_area_active ar
		LEFT JOIN view_rm_process_active p ON (ar.fkContext = p.fkArea)
		LEFT JOIN view_rm_process_asset_active pa ON (p.fkContext = pa.fkProcess)
		LEFT JOIN view_rm_asset_active a ON (pa.fkAsset = a.fkContext)
		LEFT JOIN view_rm_risk_active r ON (a.fkContext = r.fkAsset)
		GROUP BY ar.fkContext, r.fkContext
	) res
	LEFT JOIN view_rm_risk_active r ON (res.risk_id = r.fkContext)
	GROUP BY area_id

OPEN total_area_impact_and_risk_amount_by_area
FETCH NEXT FROM total_area_impact_and_risk_amount_by_area INTO @area_id, @risk_amount_by_area, @total_area_impact
WHILE (@@FETCH_STATUS = 0)
BEGIN
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (@area_id, getdate(), 'risk_amount_by_area', @risk_amount_by_area)
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (@area_id, getdate(), 'total_area_impact', @total_area_impact)
	FETCH NEXT FROM total_area_impact_and_risk_amount_by_area INTO @area_id, @risk_amount_by_area, @total_area_impact
END
CLOSE total_area_impact_and_risk_amount_by_area
DEALLOCATE total_area_impact_and_risk_amount_by_area

-- Custo e quantidade dos controles por �rea

DECLARE @control_cost_by_area FLOAT
DECLARE @control_amount_by_area INT

DECLARE control_amount_cost_by_area CURSOR FOR
	/*Calcula o custo total dos controles de cada �rea e a quantidade de controles de cada �rea*/
	SELECT res.area_id, count(control_id) as control_count, CASE WHEN sum(cc.nCost1+cc.nCost2+cc.nCost3+cc.nCost4+cc.nCost5) IS NULL THEN 0 ELSE sum(cc.nCost1+cc.nCost2+cc.nCost3+cc.nCost4+cc.nCost5) END as control_cost
	FROM
	(
		/*Garante que um controle s� ir� ser contabilizado uma vez para uma determinada �rea*/
		SELECT ar.fkContext as area_id, rc.fkControl as control_id
		FROM view_rm_area_active ar
		LEFT JOIN view_rm_process_active p ON (ar.fkContext = p.fkArea)
		LEFT JOIN view_rm_process_asset_active pa ON (p.fkContext = pa.fkProcess)
		LEFT JOIN view_rm_asset_active a ON (pa.fkAsset = a.fkContext)
		LEFT JOIN view_rm_risk_active r ON (a.fkContext = r.fkAsset)
		LEFT JOIN view_rm_risk_control_active rc ON (r.fkContext = rc.fkRisk)
		GROUP BY ar.fkContext, rc.fkControl
	) res
	LEFT JOIN rm_control_cost cc ON (res.control_id = cc.fkControl)
	GROUP BY area_id

OPEN control_amount_cost_by_area
FETCH NEXT FROM control_amount_cost_by_area INTO @area_id, @control_amount_by_area, @control_cost_by_area
WHILE (@@FETCH_STATUS = 0)
BEGIN
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (@area_id, getdate(), 'control_amount_by_area', @control_amount_by_area)
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (@area_id, getdate(), 'control_cost_by_area', @control_cost_by_area)	
	FETCH NEXT FROM control_amount_cost_by_area INTO @area_id, @control_amount_by_area, @control_cost_by_area
END
CLOSE control_amount_cost_by_area
DEALLOCATE control_amount_cost_by_area

-- Qtd. de processos de uma �rea

DECLARE @process_amount_by_area INT

DECLARE process_amount_by_area CURSOR FOR
	SELECT a.fkContext as area_id, count(p.fkContext) as process_count
	FROM view_rm_area_active a
	LEFT JOIN view_rm_process_active p ON (a.fkContext = p.fkArea)
	GROUP BY a.fkContext

OPEN process_amount_by_area
FETCH NEXT FROM process_amount_by_area INTO @area_id, @process_amount_by_area
WHILE (@@FETCH_STATUS = 0)
BEGIN
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (@area_id, getdate(), 'process_amount_by_area', @process_amount_by_area)
	FETCH NEXT FROM process_amount_by_area INTO @area_id, @process_amount_by_area
END
CLOSE process_amount_by_area
DEALLOCATE process_amount_by_area

-- Qtd. e custo dos ativos de uma �rea

DECLARE @asset_cost_by_area FLOAT
DECLARE @asset_amount_by_area INT

DECLARE asset_amount_cost_by_area CURSOR FOR
	SELECT area_id, count(asset_id) as asset_count, CASE WHEN sum(a.nCost) IS NULL THEN 0 ELSE sum(a.nCost) END as asset_cost
	FROM
	(
		SELECT a.fkContext as area_id, pa.fkAsset as asset_id
		FROM view_rm_area_active a
		LEFT JOIN view_rm_process_active p ON (a.fkContext = p.fkArea)
		LEFT JOIN view_rm_process_asset_active pa ON (p.fkContext = pa.fkProcess)
		GROUP BY a.fkContext, pa.fkAsset
	) res
	LEFT JOIN view_rm_asset_active a ON (res.asset_id = a.fkContext)
	GROUP BY area_id

OPEN asset_amount_cost_by_area
FETCH NEXT FROM asset_amount_cost_by_area INTO @area_id, @asset_amount_by_area, @asset_cost_by_area
WHILE (@@FETCH_STATUS = 0)
BEGIN
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (@area_id, getdate(), 'asset_amount_by_area', @asset_amount_by_area)
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (@area_id, getdate(), 'asset_cost_by_area', @asset_cost_by_area)
	FETCH NEXT FROM asset_amount_cost_by_area INTO @area_id, @asset_amount_by_area, @asset_cost_by_area
END
CLOSE asset_amount_cost_by_area
DEALLOCATE asset_amount_cost_by_area

/*===============================================INFORMA��ES SOBRE PROCESSO========================================================*/

-- Valor do processo

DECLARE @process_id INT
DECLARE @process_value FLOAT

DECLARE process_value_cursor CURSOR FOR
	SELECT fkContext, nValue FROM view_rm_process_active

OPEN process_value_cursor
FETCH NEXT FROM process_value_cursor INTO @process_id, @process_value
WHILE (@@FETCH_STATUS = 0)
BEGIN
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (@process_id, getdate(), 'process_value', @process_value)
	FETCH NEXT FROM process_value_cursor INTO @process_id, @process_value
END
CLOSE process_value_cursor
DEALLOCATE process_value_cursor

-- Quantidade e custo dos ativos por processo

DECLARE @asset_amount_by_process INT
DECLARE @asset_cost_by_process FLOAT

DECLARE asset_amount_cost_by_process CURSOR FOR
	SELECT p.fkContext, count(pa.fkAsset) as asset_count, CASE WHEN sum(a.nCost) IS NULL THEN 0 ELSE sum(a.nCost) END
	FROM view_rm_process_active p
	LEFT JOIN view_rm_process_asset_active pa ON (p.fkContext = pa.fkProcess)
	LEFT JOIN view_rm_asset_active a ON (pa.fkAsset = a.fkContext)
	GROUP BY p.fkContext

OPEN asset_amount_cost_by_process
FETCH NEXT FROM asset_amount_cost_by_process INTO @process_id, @asset_amount_by_process, @asset_cost_by_process
WHILE (@@FETCH_STATUS = 0)
BEGIN
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (@process_id, getdate(), 'asset_amount_by_process', @asset_amount_by_process)
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (@process_id, getdate(), 'asset_cost_by_process', @asset_cost_by_process)
	FETCH NEXT FROM asset_amount_cost_by_process INTO @process_id, @asset_amount_by_process, @asset_cost_by_process
END
CLOSE asset_amount_cost_by_process
DEALLOCATE asset_amount_cost_by_process

-- Quantidade de riscos por processo

DECLARE @risk_count INT

DECLARE risk_amount_by_process CURSOR FOR
	SELECT p.fkContext, count(r.fkContext) as risk_count
	FROM view_rm_process_active p
	LEFT JOIN view_rm_process_asset_active pa ON (p.fkContext = pa.fkProcess)
	LEFT JOIN view_rm_asset_active a ON (pa.fkAsset = a.fkContext)
	LEFT JOIN view_rm_risk_active r ON (a.fkContext = r.fkAsset)
	GROUP BY p.fkContext

OPEN risk_amount_by_process
FETCH NEXT FROM risk_amount_by_process INTO @process_id, @risk_count
WHILE (@@FETCH_STATUS = 0)
BEGIN
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (@process_id, getdate(), 'risk_amount_by_process', @risk_count)
	FETCH NEXT FROM risk_amount_by_process INTO @process_id, @risk_count
END
CLOSE risk_amount_by_process
DEALLOCATE risk_amount_by_process

-- N� total de processos

DECLARE  @process_total INT
SET @process_total = (SELECT count(*) FROM view_rm_process_active)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'process_total', @process_total)

-- N� processos com risco alto

DECLARE  @processes_with_high_risk INT
SET @processes_with_high_risk = (SELECT count(*) FROM view_rm_process_active WHERE nValue >= @risk_high)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'processes_with_high_risk', @processes_with_high_risk)

-- N� processos com risco m�dio

DECLARE  @processes_with_medium_risk INT
SET @processes_with_medium_risk = (SELECT count(*) FROM view_rm_process_active WHERE nValue > @risk_low AND nValue < @risk_high)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'processes_with_medium_risk', @processes_with_medium_risk)

-- N� processos com risco baixo

DECLARE  @processes_with_low_risk INT
SET @processes_with_low_risk = (SELECT count(*) FROM view_rm_process_active WHERE nValue <= @risk_low AND nValue <> 0)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'processes_with_low_risk', @processes_with_low_risk)

-- N� processos com risco n�o parametrizado

DECLARE  @processes_with_no_value_risk INT
SET @processes_with_no_value_risk = (SELECT count(*) FROM view_rm_process_active WHERE nValue = 0)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'processes_with_no_value_risk', @processes_with_no_value_risk)

-- N� processos completos

DECLARE  @complete_processes INT
SET @complete_processes = (
	SELECT count(*)
	FROM view_rm_process_active p
	WHERE p.nValue > 0 AND NOT EXISTS (
		SELECT * 
		FROM view_rm_process_asset_active pa 
		JOIN view_rm_asset_active a ON (pa.fkAsset = a.fkContext) 
		WHERE pa.fkProcess = p.fkContext AND a.nValue = 0
	)
)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'complete_processes', @complete_processes)

-- N� processos parcias

DECLARE  @partial_processes INT
SET @partial_processes = (
	SELECT count(*)
	FROM view_rm_process_active p
	WHERE p.nValue > 0 AND EXISTS (
		SELECT * 
		FROM view_rm_process_asset_active pa 
		JOIN view_rm_asset_active a ON (pa.fkAsset = a.fkContext) 
		WHERE pa.fkProcess = p.fkContext AND a.nValue = 0
	)
)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'partial_processes', @partial_processes)

-- N� processos n�o gerenciados

DECLARE  @not_managed_processes INT
SET @not_managed_processes = (SELECT count(*) FROM view_rm_process_active p WHERE p.nValue = 0)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'not_managed_processes', @not_managed_processes)

-- N� processos sem ativos

DECLARE  @processes_without_assets INT
SET @processes_without_assets = (
	SELECT count(*)
	FROM view_rm_process_active p
	WHERE p.fkContext NOT IN (SELECT fkProcess FROM view_rm_process_asset_active)
)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'processes_without_assets', @processes_without_assets)

-- Total de impacto por processo

DECLARE @total_process_impact FLOAT

DECLARE total_process_impact CURSOR FOR
	SELECT p.fkContext as process_id, CASE WHEN sum(r.nCost) IS NULL THEN 0 ELSE sum(r.nCost) END as total_impact
	FROM view_rm_process_active p
	LEFT JOIN view_rm_process_asset_active pa ON (p.fkContext = pa.fkProcess)
	LEFT JOIN view_rm_asset_active a ON (pa.fkAsset = a.fkContext)
	LEFT JOIN view_rm_risk_active r ON (a.fkContext = r.fkAsset)
	GROUP BY p.fkContext

OPEN total_process_impact
FETCH NEXT FROM total_process_impact INTO @process_id, @total_process_impact

WHILE (@@FETCH_STATUS = 0)
BEGIN
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (@process_id, getdate(), 'total_process_impact', @total_process_impact)
	FETCH NEXT FROM total_process_impact INTO @process_id, @total_process_impact
END
CLOSE total_process_impact
DEALLOCATE total_process_impact

-- Custo e quantidade dos controles por processo

DECLARE @control_cost_by_process FLOAT
DECLARE @control_amount_by_process INT

DECLARE control_amount_cost_by_process CURSOR FOR
	/*Calcula o custo total dos controles de cada processo e a quantidade de controles de cada processo*/
	SELECT process_id, count(control_id) as control_count, CASE WHEN sum(cc.nCost1+cc.nCost2+cc.nCost3+cc.nCost4+cc.nCost5) IS NULL THEN 0 ELSE sum(cc.nCost1+cc.nCost2+cc.nCost3+cc.nCost4+cc.nCost5) END as control_cost
	FROM
	(
		/*Garante que um controle s� ir� ser contabilizado uma vez para um determinado processo*/
		SELECT p.fkContext as process_id, rc.fkControl as control_id
		FROM view_rm_process_active p
		LEFT JOIN view_rm_process_asset_active pa ON (p.fkContext = pa.fkProcess)
		LEFT JOIN view_rm_asset_active a ON (pa.fkAsset = a.fkContext)
		LEFT JOIN view_rm_risk_active r ON (a.fkContext = r.fkAsset)
		LEFT JOIN view_rm_risk_control_active rc ON (r.fkContext = rc.fkRisk)
		GROUP BY p.fkContext, rc.fkControl
	) res
	LEFT JOIN rm_control_cost cc ON (res.control_id = cc.fkControl)
	GROUP BY process_id

OPEN control_amount_cost_by_process
FETCH NEXT FROM control_amount_cost_by_process INTO @process_id, @control_amount_by_process, @control_cost_by_process
WHILE (@@FETCH_STATUS = 0)
BEGIN
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (@process_id, getdate(), 'control_amount_by_process', @control_amount_by_process)
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (@process_id, getdate(), 'control_cost_by_process', @control_cost_by_process)	
	FETCH NEXT FROM control_amount_cost_by_process INTO @process_id, @control_amount_by_process, @control_cost_by_process
END
CLOSE control_amount_cost_by_process
DEALLOCATE control_amount_cost_by_process

/*===============================================INFORMA��ES SOBRE ATIVO========================================================*/

-- Valor do ativo

DECLARE @asset_id INT
DECLARE @asset_value FLOAT

DECLARE asset_value_cursor CURSOR FOR
	SELECT fkContext, nValue FROM view_rm_asset_active

OPEN asset_value_cursor
FETCH NEXT FROM asset_value_cursor INTO @asset_id, @asset_value
WHILE (@@FETCH_STATUS = 0)
BEGIN
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (@asset_id, getdate(), 'asset_value', @asset_value)
	FETCH NEXT FROM asset_value_cursor INTO @asset_id, @asset_value
END
CLOSE asset_value_cursor
DEALLOCATE asset_value_cursor

-- Quantidade de processos por ativo

DECLARE @process_count INT

DECLARE process_amount_by_asset CURSOR FOR
	SELECT a.fkContext, count(pa.fkProcess) as process_count
	FROM view_rm_asset_active a
	LEFT JOIN view_rm_process_asset_active pa ON (a.fkContext = pa.fkAsset)
	GROUP BY a.fkContext

OPEN process_amount_by_asset
FETCH NEXT FROM process_amount_by_asset INTO @asset_id, @process_count
WHILE (@@FETCH_STATUS = 0)
BEGIN
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (@asset_id, getdate(), 'process_amount_by_asset', @process_count)
	FETCH NEXT FROM process_amount_by_asset INTO @asset_id, @process_count
END
CLOSE process_amount_by_asset
DEALLOCATE process_amount_by_asset

-- Quantidade de riscos por ativo

DECLARE risk_amount_by_asset CURSOR FOR
	SELECT a.fkContext, count(r.fkContext) as risk_count
	FROM view_rm_asset_active a
	LEFT JOIN view_rm_risk_active r ON (a.fkContext = r.fkAsset)
	GROUP BY a.fkContext

OPEN risk_amount_by_asset
FETCH NEXT FROM risk_amount_by_asset INTO @asset_id, @risk_count
WHILE (@@FETCH_STATUS = 0)
BEGIN
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (@asset_id, getdate(), 'risk_amount_by_asset', @risk_count)
	FETCH NEXT FROM risk_amount_by_asset INTO @asset_id, @risk_count
END
CLOSE risk_amount_by_asset
DEALLOCATE risk_amount_by_asset

-- N� total de ativos

DECLARE  @asset_total INT
SET @asset_total = (SELECT count(*) FROM view_rm_asset_active)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'asset_total', @asset_total)

-- N� ativos com risco alto

DECLARE  @assets_with_high_risk INT
SET @assets_with_high_risk = (SELECT count(*) FROM view_rm_asset_active WHERE nValue >= @risk_high)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'assets_with_high_risk', @assets_with_high_risk)

-- N� ativos com risco m�dio

DECLARE  @assets_with_medium_risk INT

SET @assets_with_medium_risk = (SELECT count(*) FROM view_rm_asset_active WHERE nValue > @risk_low AND nValue < @risk_high)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'assets_with_medium_risk', @assets_with_medium_risk)

-- N� ativos com risco baixo

DECLARE  @assets_with_low_risk INT
SET @assets_with_low_risk = (SELECT count(*) FROM view_rm_asset_active WHERE nValue <= @risk_low AND nValue <> 0)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'assets_with_low_risk', @assets_with_low_risk)

-- N� ativos com risco n�o parametrizado

DECLARE  @assets_with_no_value_risk INT
SET @assets_with_no_value_risk = (SELECT count(*) FROM view_rm_asset_active WHERE nValue = 0)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'assets_with_no_value_risk', @assets_with_no_value_risk)

-- N� ativos completos

DECLARE  @complete_assets INT
SET @complete_assets = (
	SELECT count(*) FROM view_rm_asset_active a
	WHERE a.nValue > 0 AND NOT EXISTS (
		SELECT * FROM view_rm_risk_active r WHERE r.fkAsset = a.fkContext AND r.nValue = 0
	)
)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'complete_assets', @complete_assets)

-- N� ativos parcias

DECLARE  @partial_assets INT
SET @partial_assets = (
	SELECT count(*) FROM view_rm_asset_active a
	WHERE a.nValue > 0 AND EXISTS (
		SELECT * FROM view_rm_risk_active r WHERE r.fkAsset = a.fkContext AND r.nValue = 0
	)
)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'partial_assets', @partial_assets)

-- N� ativos n�o gerenciados

DECLARE  @not_managed_assets INT
SET @not_managed_assets = (SELECT count(*) FROM view_rm_asset_active a WHERE a.nValue = 0)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'not_managed_assets', @not_managed_assets)

-- N� ativos sem riscos

DECLARE  @assets_without_risks INT
SET @assets_without_risks = (
	SELECT count(*)
	FROM view_rm_asset_active a
	WHERE a.fkContext NOT IN (SELECT fkAsset FROM view_rm_risk_active)
)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'assets_without_risks', @assets_without_risks)

-- Total de impacto dos ativos

DECLARE @total_asset_impact FLOAT

DECLARE total_asset_impact CURSOR FOR
	SELECT a.fkContext, CASE WHEN sum(r.nCost) IS NULL THEN 0 ELSE sum(r.nCost) END
	FROM view_rm_asset_active a 
	LEFT JOIN view_rm_risk_active r ON (a.fkContext = r.fkAsset)
	GROUP BY a.fkContext

OPEN total_asset_impact
FETCH NEXT FROM total_asset_impact INTO @asset_id, @total_asset_impact
WHILE (@@FETCH_STATUS = 0)
BEGIN
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (@asset_id, getdate(), 'total_asset_impact', @total_asset_impact)
	FETCH NEXT FROM total_asset_impact INTO @asset_id, @total_asset_impact
END
CLOSE total_asset_impact
DEALLOCATE total_asset_impact

-- Custo e quantidade dos controles por ativo

DECLARE @control_cost_by_asset FLOAT
DECLARE @control_amount_by_asset INT

DECLARE control_amount_cost_by_asset CURSOR FOR
	/*Calcula o custo total dos controles de cada ativo e a quantidade de controles de cada ativo*/
	SELECT res.asset_id, count(control_id) as control_count, CASE WHEN sum(cc.nCost1+cc.nCost2+cc.nCost3+cc.nCost4+cc.nCost5) IS NULL THEN 0 ELSE sum(cc.nCost1+cc.nCost2+cc.nCost3+cc.nCost4+cc.nCost5) END as control_cost
	FROM
	(
		/*Garante que um controle s� ir� ser contabilizado uma vez para um determinado ativo*/
		SELECT a.fkContext as asset_id, rc.fkControl as control_id
		FROM view_rm_asset_active a
		LEFT JOIN view_rm_risk_active r ON (a.fkContext = r.fkAsset)
		LEFT JOIN view_rm_risk_control_active rc ON (r.fkContext = rc.fkRisk)
		GROUP BY a.fkContext, rc.fkControl
	) res 
	LEFT JOIN rm_control_cost cc ON (res.control_id = cc.fkControl)
	GROUP BY asset_id

OPEN control_amount_cost_by_asset
FETCH NEXT FROM control_amount_cost_by_asset INTO @asset_id, @control_amount_by_asset, @control_cost_by_asset
WHILE (@@FETCH_STATUS = 0)
BEGIN
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (@asset_id, getdate(), 'control_amount_by_asset', @control_amount_by_asset)
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (@asset_id, getdate(), 'control_cost_by_asset', @control_cost_by_asset)	
	FETCH NEXT FROM control_amount_cost_by_asset INTO @asset_id, @control_amount_by_asset, @control_cost_by_asset
END
CLOSE control_amount_cost_by_asset
DEALLOCATE control_amount_cost_by_asset

/*===============================================INFORMA��ES SOBRE RISCO========================================================*/

-- Valor real e residual do risco

DECLARE @risk_id INT
DECLARE @risk_value FLOAT
DECLARE @risk_residual_value FLOAT

DECLARE risk_value_cursor CURSOR FOR
	SELECT fkContext, nValue, nValueResidual FROM view_rm_risk_active

OPEN risk_value_cursor
FETCH NEXT FROM risk_value_cursor INTO @risk_id, @risk_value, @risk_residual_value
WHILE (@@FETCH_STATUS = 0)
BEGIN
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (@risk_id, getdate(), 'risk_value', @risk_value)
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (@risk_id, getdate(), 'risk_residual_value', @risk_residual_value)
	FETCH NEXT FROM risk_value_cursor INTO @risk_id, @risk_value, @risk_residual_value
END
CLOSE risk_value_cursor
DEALLOCATE risk_value_cursor

-- Quantidade de controles dos riscos

DECLARE @control_count INT

DECLARE control_amount_by_risk CURSOR FOR
	SELECT r.fkContext, count(rc.fkControl) as control_count
	FROM view_rm_risk_active r
	LEFT JOIN view_rm_risk_control_active rc ON (r.fkContext = rc.fkRisk)
	GROUP BY r.fkContext

OPEN control_amount_by_risk
FETCH NEXT FROM control_amount_by_risk INTO @risk_id, @control_count
WHILE (@@FETCH_STATUS = 0)
BEGIN
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (@risk_id, getdate(), 'control_amount_by_risk', @control_count)
	FETCH NEXT FROM control_amount_by_risk INTO @risk_id, @control_count
END
CLOSE control_amount_by_risk
DEALLOCATE control_amount_by_risk

-- N� total de riscos

DECLARE  @risk_total INT
SET @risk_total = (SELECT count(*) FROM view_rm_risk_active)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'risk_total', @risk_total)

-- N� riscos com risco real alto

DECLARE  @risks_with_high_real_risk INT
SET @risks_with_high_real_risk = (SELECT count(*) FROM view_rm_risk_active WHERE nValue >= @risk_high)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'risks_with_high_real_risk', @risks_with_high_real_risk)

-- N� riscos com risco residual alto

DECLARE  @risks_with_high_residual_risk INT
SET @risks_with_high_residual_risk = (SELECT count(*) FROM view_rm_risk_active WHERE nValueResidual >= @risk_high)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'risks_with_high_residual_risk', @risks_with_high_residual_risk)

-- N� riscos com risco real m�dio

DECLARE  @risks_with_medium_real_risk INT
SET @risks_with_medium_real_risk = (SELECT count(*) FROM view_rm_risk_active WHERE nValue > @risk_low AND nValue < @risk_high)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'risks_with_medium_real_risk', @risks_with_medium_real_risk)

-- N� riscos com risco residual m�dio

DECLARE  @risks_with_medium_residual_risk INT
SET @risks_with_medium_residual_risk = (SELECT count(*) FROM view_rm_risk_active WHERE nValueResidual > @risk_low AND nValueResidual < @risk_high)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'risks_with_medium_residual_risk', @risks_with_medium_residual_risk)

-- N� riscos com risco real baixo

DECLARE  @risks_with_low_real_risk INT
SET @risks_with_low_real_risk = (SELECT count(*) FROM view_rm_risk_active WHERE nValue <= @risk_low AND nValue <> 0)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'risks_with_low_real_risk', @risks_with_low_real_risk)

-- N� riscos com risco residual baixo

DECLARE  @risks_with_low_residual_risk INT
SET @risks_with_low_residual_risk = (SELECT count(*) FROM view_rm_risk_active WHERE nValueResidual <= @risk_low AND nValueResidual <> 0)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'risks_with_low_residual_risk', @risks_with_low_residual_risk)

-- N� riscos com risco n�o parametrizado

DECLARE  @risks_with_no_value_risk INT
SET @risks_with_no_value_risk = (SELECT count(*) FROM view_rm_risk_active WHERE nValue = 0)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'risks_with_no_value_risk', @risks_with_no_value_risk)

-- N� riscos tratados

DECLARE  @risks_treated INT
SET @risks_treated = (SELECT count(*) FROM view_rm_risk_active r WHERE r.nAcceptMode<>0 OR r.nValue != r.nValueResidual)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'risks_treated', @risks_treated)

-- N� riscos n�o tratados

DECLARE  @risks_not_treated INT
SET @risks_not_treated = (SELECT count(*) FROM view_rm_risk_active r WHERE r.nAcceptMode=0 AND r.nValue = r.nValueResidual AND r.nValue > 0)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'risks_not_treated', @risks_not_treated)

-- N� riscos aceitos

DECLARE  @risks_accepted INT
SET @risks_accepted = (SELECT count(*) FROM view_rm_risk_active r WHERE r.nAcceptMode = 72601)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'risks_accepted', @risks_accepted)

-- N� riscos transferidos

DECLARE  @risks_transfered INT
SET @risks_transfered = (SELECT count(*) FROM view_rm_risk_active r WHERE r.nAcceptMode = 72602)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'risks_transfered', @risks_transfered)

-- N� riscos evitados

DECLARE  @risks_avoided INT
SET @risks_avoided = (SELECT count(*) FROM view_rm_risk_active r WHERE r.nAcceptMode = 72603)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'risks_avoided', @risks_avoided)

/*===============================================INFORMA��ES SOBRE CONTROLE========================================================*/

-- Quantidade de riscos por controle

DECLARE @control_id INT

DECLARE risk_amount_by_control CURSOR FOR
	SELECT c.fkContext, count(rc.fkRisk) as risk_count
	FROM view_rm_control_active c
	LEFT JOIN view_rm_risk_control_active rc ON (c.fkContext = rc.fkControl)
	GROUP BY c.fkContext

OPEN risk_amount_by_control
FETCH NEXT FROM risk_amount_by_control INTO @control_id, @risk_count
WHILE (@@FETCH_STATUS = 0)
BEGIN
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (@control_id, getdate(), 'risk_amount_by_control', @risk_count)
	FETCH NEXT FROM risk_amount_by_control INTO @control_id, @risk_count
END
CLOSE risk_amount_by_control
DEALLOCATE risk_amount_by_control

-- N� total de controles

DECLARE  @control_total INT
SET @control_total = (SELECT count(*) FROM view_rm_control_active)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'control_total', @control_total)


-- N� de controles com implanta��o planejada

DECLARE  @planned_implantation_controls INT
SET @planned_implantation_controls = (SELECT count(*) FROM view_rm_control_active c WHERE c.dDateDeadline >= getdate() AND c.dDateImplemented IS NULL)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'planned_implantation_controls', @planned_implantation_controls)

-- N� de controles com implanta��o atrasada

DECLARE  @late_implantation_controls INT
SET @late_implantation_controls = (SELECT count(*) FROM view_rm_control_active c WHERE c.dDateDeadline < getdate() AND c.dDateImplemented IS NULL)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'late_implantation_controls', @late_implantation_controls)

-- N� de controles implementados adequadamente

DECLARE  @adequate_implemented_controls INT
SET @adequate_implemented_controls = (
	SELECT count(*)
	FROM view_rm_control_active c
	LEFT JOIN rm_control_test_history ct ON (c.fkContext = ct.fkControl)
	LEFT JOIN wkf_control_efficiency ce ON (c.fkContext = ce.fkControlEfficiency)
	WHERE c.dDateImplemented IS NOT NULL AND ((ct.bTestedValue = 1 AND ce.nRealEfficiency >= ce.nExpectedEfficiency) OR (ct.bTestedValue = 1 AND ce.nRealEfficiency IS NULL) OR (ct.bTestedValue IS NULL AND ce.nRealEfficiency >= ce.nExpectedEfficiency))
)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'adequate_implemented_controls', @adequate_implemented_controls)

-- N� de controles implementados inadequadamente

DECLARE  @inadequate_implemented_controls INT
SET @inadequate_implemented_controls = (
	SELECT count(*)
	FROM view_rm_control_active c
	LEFT JOIN rm_control_test_history ct ON (c.fkContext = ct.fkControl)
	LEFT JOIN wkf_control_efficiency ce ON (c.fkContext = ce.fkControlEfficiency)
	WHERE 
	(
		(ct.bTestedValue=0 AND ce.nRealEfficiency>=ce.nExpectedEfficiency) OR
		(ct.bTestedValue=0 AND ce.nRealEfficiency IS NULL) OR
		(ct.bTestedValue IS NULL AND ce.nRealEfficiency<ce.nExpectedEfficiency) OR
		(ct.bTestedValue=1 AND ce.nRealEfficiency<ce.nExpectedEfficiency) OR
		(ct.bTestedValue=0 AND ce.nRealEfficiency<ce.nExpectedEfficiency)
	)
)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'inadequate_implemented_controls', @inadequate_implemented_controls)

-- N� de controles implementados mas n�o medidos

DECLARE  @not_measured_implemented_controls INT
SET @not_measured_implemented_controls = (
	SELECT count(*)
	FROM view_rm_control_active c
	LEFT JOIN rm_control_test_history ct ON (c.fkContext = ct.fkControl)
	LEFT JOIN wkf_control_efficiency ce ON (c.fkContext = ce.fkControlEfficiency)
	WHERE
		c.fkContext NOT IN (SELECT fkControlEfficiency FROM wkf_control_efficiency) AND
		c.fkContext NOT IN (SELECT fkControlTest FROM wkf_control_test)
)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'not_measured_implemented_controls', @not_measured_implemented_controls)

-- N� de controles testados com sucesso

DECLARE  @tested_ok_controls INT
SET @tested_ok_controls = (
	SELECT count(*)
	FROM view_rm_control_active c
	JOIN rm_control_test_history ct ON (c.fkContext = ct.fkControl AND ct.bTestedValue = 1 AND c.dDateImplemented IS NOT NULL)
)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'tested_ok_controls', @tested_ok_controls)

-- N� de controles testados sem sucesso

DECLARE  @tested_not_ok_controls INT
SET @tested_not_ok_controls = (
	SELECT count(*)
	FROM view_rm_control_active c
	JOIN rm_control_test_history ct ON (c.fkContext = ct.fkControl AND ct.bTestedValue = 0 AND c.dDateImplemented IS NOT NULL)
)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'tested_not_ok_controls', @tested_not_ok_controls)

-- N� de controles revisados com sucesso

DECLARE  @revision_ok_controls INT
SET @revision_ok_controls = (
	SELECT count(*)
	FROM view_rm_control_active c
	JOIN wkf_control_efficiency ce ON (c.fkContext = ce.fkControlEfficiency AND ce.nRealEfficiency >= ce.nExpectedEfficiency AND c.dDateImplemented IS NOT NULL)
)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'revision_ok_controls', @revision_ok_controls)

-- N� de controles revisados sem sucesso

DECLARE  @revision_not_ok_controls INT
SET @revision_not_ok_controls = (
	SELECT count(*)
	FROM view_rm_control_active c

	JOIN wkf_control_efficiency ce ON (c.fkContext = ce.fkControlEfficiency AND ce.nRealEfficiency < ce.nExpectedEfficiency AND c.dDateImplemented IS NOT NULL)
)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'revision_not_ok_controls', @revision_not_ok_controls)

-- N� de controles sem riscos

DECLARE  @controls_without_risks INT
SET @controls_without_risks = (
	SELECT count(*)
	FROM view_rm_control_active c
	WHERE c.fkContext NOT IN (SELECT fkControl FROM view_rm_risk_control_active)
)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'controls_without_risks', @controls_without_risks)

/*===============================================INFORMA��ES SOBRE NORMA========================================================*/

-- Quantidade de melhores pr�ticas por norma

DECLARE @standard_id INT
DECLARE @best_practice_count INT

DECLARE best_practice_amount_by_standard CURSOR FOR
	SELECT s.fkContext, count(bps.fkBestPractice)
	FROM view_rm_standard_active s
	LEFT JOIN view_rm_bp_standard_active bps ON (s.fkContext = bps.fkStandard)
	GROUP BY s.fkContext

OPEN best_practice_amount_by_standard
FETCH NEXT FROM best_practice_amount_by_standard INTO @standard_id, @best_practice_count
WHILE (@@FETCH_STATUS = 0)
BEGIN
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (@standard_id, getdate(), 'best_practice_amount_by_standard', @best_practice_count)
	FETCH NEXT FROM best_practice_amount_by_standard INTO @standard_id, @best_practice_count
END
CLOSE best_practice_amount_by_standard
DEALLOCATE best_practice_amount_by_standard

-- Quantidade de melhores pr�ticas aplicadas por norma

DECLARE applied_best_practice_amount_by_standard CURSOR FOR
	SELECT s.fkContext, count(bps.fkBestPractice)
	FROM view_rm_standard_active s
	LEFT JOIN rm_best_practice_standard bps ON (s.fkContext = bps.fkStandard)
	AND bps.fkBestPractice IN (SELECT cbp.fkBestPractice FROM view_rm_control_bp_active cbp)
	GROUP BY s.fkContext

OPEN applied_best_practice_amount_by_standard
FETCH NEXT FROM applied_best_practice_amount_by_standard INTO @standard_id, @best_practice_count
WHILE (@@FETCH_STATUS = 0)
BEGIN
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (@standard_id, getdate(), 'applied_best_practice_amount_by_standard', @best_practice_count)
	FETCH NEXT FROM applied_best_practice_amount_by_standard INTO @standard_id, @best_practice_count
END
CLOSE applied_best_practice_amount_by_standard
DEALLOCATE applied_best_practice_amount_by_standard

/*===============================================INFORMA��ES FINANCEIRAS========================================================*/

-- Custo total dos ativos

DECLARE  @asset_total_cost FLOAT
SET @asset_total_cost = (SELECT CASE WHEN sum(nCost) IS NULL THEN 0 ELSE sum(nCost) END FROM view_rm_asset_active)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'asset_total_cost', @asset_total_cost)

-- Custo total dos controles

DECLARE @control_total_cost1 FLOAT
DECLARE @control_total_cost2 FLOAT
DECLARE @control_total_cost3 FLOAT
DECLARE @control_total_cost4 FLOAT
DECLARE @control_total_cost5 FLOAT

DECLARE  control_total_cost CURSOR FOR
	SELECT 
	CASE WHEN sum(cc.nCost1) IS NULL THEN 0 ELSE sum(cc.nCost1) END,
	CASE WHEN sum(cc.nCost2) IS NULL THEN 0 ELSE sum(cc.nCost2) END,
	CASE WHEN sum(cc.nCost3) IS NULL THEN 0 ELSE sum(cc.nCost3) END,
	CASE WHEN sum(cc.nCost4) IS NULL THEN 0 ELSE sum(cc.nCost4) END,
	CASE WHEN sum(cc.nCost5) IS NULL THEN 0 ELSE sum(cc.nCost5) END
	FROM view_rm_control_active c
	JOIN rm_control_cost cc ON (c.fkContext = cc.fkControl)

OPEN control_total_cost
FETCH NEXT FROM control_total_cost INTO @control_total_cost1, @control_total_cost2, @control_total_cost3, @control_total_cost4, @control_total_cost5

INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (NULL, getdate(), 'control_total_cost1', @control_total_cost1)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (NULL, getdate(), 'control_total_cost2', @control_total_cost2)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (NULL, getdate(), 'control_total_cost3', @control_total_cost3)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (NULL, getdate(), 'control_total_cost4', @control_total_cost4)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (NULL, getdate(), 'control_total_cost5', @control_total_cost5)

CLOSE control_total_cost
DEALLOCATE control_total_cost

-- Custo dos riscos potencialmente baixos

DECLARE  @potentially_low_risk INT
SET @potentially_low_risk = (SELECT CASE WHEN sum(nCost) IS NULL THEN 0 ELSE sum(nCost) END FROM view_rm_risk_active r WHERE r.nValue <= @risk_low)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'potentially_low_risk_cost', @potentially_low_risk)

-- Custo dos riscos mitigados

DECLARE  @mitigated_risk INT
SET @mitigated_risk = (SELECT CASE WHEN sum(nCost) IS NULL THEN 0 ELSE sum(nCost) END FROM view_rm_risk_active r WHERE r.fkContext IN (SELECT fkRisk FROM view_rm_risk_control_active))
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'mitigated_risk_cost', @mitigated_risk)

-- Custo dos riscos evitados

DECLARE  @avoided_risk INT
SET @avoided_risk = (SELECT CASE WHEN sum(nCost) IS NULL THEN 0 ELSE sum(nCost) END FROM view_rm_risk_active r WHERE r.nAcceptMode = 72603)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'avoided_risk_cost', @avoided_risk)

-- Custo dos riscos transferidos

DECLARE  @transfered_risk INT
SET @transfered_risk = (SELECT CASE WHEN sum(nCost) IS NULL THEN 0 ELSE sum(nCost) END FROM view_rm_risk_active r WHERE r.nAcceptMode = 72602)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'transfered_risk_cost', @transfered_risk)

-- Custo dos riscos aceitos

DECLARE  @accepted_risk INT
SET @accepted_risk = (SELECT CASE WHEN sum(nCost) IS NULL THEN 0 ELSE sum(nCost) END FROM view_rm_risk_active r WHERE r.nAcceptMode = 72601)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'accepted_risk_cost', @accepted_risk)

-- Custo dos riscos m�dio / alto / n�o tratados

DECLARE  @not_treated_and_medium_high_risk INT
SET @not_treated_and_medium_high_risk = (
	SELECT CASE WHEN sum(nCost) IS NULL THEN 0 ELSE sum(nCost) END FROM view_rm_risk_active r
	WHERE r.nAcceptMode = 0 AND r.nValue > @risk_low AND r.fkContext NOT IN (SELECT fkRisk FROM view_rm_risk_control_active)
)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'not_treated_and_medium_high_risk', @not_treated_and_medium_high_risk)

/*================================================= POLICY MANAGEMENT ==========================================================*/

-- Quantidade de Documentos Publicados
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT NULL, getdate(), 'document_total', count(*) FROM view_pm_published_docs

-- Quantidade de Documentos por Estado
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT NULL, getdate(), 'documents_by_state_developing', count(d.fkContext)
FROM view_pm_document_active d JOIN isms_context c ON (c.pkContext = d.fkContext)
WHERE c.nState = 2751
UNION
SELECT NULL, getdate(), 'documents_by_state_approved', count(d.fkContext)
FROM view_pm_document_active d JOIN isms_context c ON (c.pkContext = d.fkContext)
WHERE c.nState = 2752
UNION
SELECT NULL, getdate(), 'documents_by_state_pendant', count(d.fkContext)
FROM view_pm_document_active d JOIN isms_context c ON (c.pkContext = d.fkContext)
WHERE c.nState = 2753

-- Quantidade de Documentos por Tipo
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT NULL, getdate(), 'documents_by_type_none', count(d.fkContext)
FROM view_pm_document_active d
WHERE d.nType = 2800
UNION
SELECT NULL, getdate(), 'documents_by_type_area', count(d.fkContext)
FROM view_pm_document_active d
WHERE d.nType = 2801
UNION
SELECT NULL, getdate(), 'documents_by_type_process', count(d.fkContext)
FROM view_pm_document_active d
WHERE d.nType = 2802
UNION
SELECT NULL, getdate(), 'documents_by_type_asset', count(d.fkContext)
FROM view_pm_document_active d
WHERE d.nType = 2803
UNION
SELECT NULL, getdate(), 'documents_by_type_control', count(d.fkContext)
FROM view_pm_document_active d
WHERE d.nType = 2805
UNION
SELECT NULL, getdate(), 'documents_by_type_scope', count(d.fkContext)
FROM view_pm_document_active d
WHERE d.nType = 2820
UNION
SELECT NULL, getdate(), 'documents_by_type_policy', count(d.fkContext)
FROM view_pm_document_active d
WHERE d.nType = 2821
UNION
SELECT NULL, getdate(), 'documents_by_type_management', count(d.fkContext)
FROM view_pm_document_active d
WHERE d.nType = 3801
UNION
SELECT NULL, getdate(), 'documents_by_type_others', count(d.fkContext)
FROM view_pm_document_active d
WHERE d.nType = 3802

-- Quantidade de Registros
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT NULL, getdate(), 'register_total', count(*) FROM view_pm_register_active

-- Quantidade de Documentos J� Lidos
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT NULL, getdate(), 'documents_read', count(*) FROM view_pm_read_docs

-- Quantidade de Documentos N�o Lidos
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT NULL, getdate(), 'documents_not_read', count(fkContext)
FROM view_pm_published_docs
WHERE fkContext NOT IN (SELECT fkContext FROM view_pm_read_docs)

-- Quantidade de Documentos com Revis�o Agendada
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT NULL, getdate(), 'documents_scheduled', count(*)
FROM view_pm_published_docs WHERE fkSchedule IS NOT NULL

-- Ocupa��o dos arquivos de documentos
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT NULL, getdate(), 'occupation_documents', CASE WHEN SUM(di.nFileSize) > 0 THEN ROUND(CAST(SUM(di.nFileSize) AS FLOAT)/1048576.0,0) ELSE 0 END
FROM pm_doc_instance di
WHERE NOT EXISTS (
  SELECT * FROM pm_document d JOIN pm_register r ON (r.fkDocument = d.fkContext)
  WHERE d.fkContext = di.fkDocument
)

-- Ocupa��o dos arquivos de documentos
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT NULL, getdate(), 'occupation_documents', CASE WHEN SUM(di.nFileSize) > 0 THEN ROUND(CAST(SUM(di.nFileSize) AS FLOAT)/1048576.0,0) ELSE 0 END
FROM pm_doc_instance di
WHERE NOT EXISTS (
  SELECT * FROM pm_document d JOIN pm_register r ON (r.fkDocument = d.fkContext)
  WHERE d.fkContext = di.fkDocument
)

-- Ocupa��o dos arquivos de template
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT NULL, getdate(), 'occupation_template', CASE WHEN SUM(t.nFileSize) > 0 THEN ROUND(CAST(SUM(t.nFileSize) AS FLOAT)/1048576.0,0) ELSE 0 END
FROM pm_template t

-- Ocupa��o dos arquivos de registro
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT NULL, getdate(), 'occupation_registers', CASE WHEN SUM(di.nFileSize) > 0 THEN ROUND(CAST(SUM(di.nFileSize) AS FLOAT)/1048576.0,0) ELSE 0 END
FROM
  pm_doc_instance di
  JOIN pm_document d ON (d.fkContext = di.fkDocument)
  JOIN pm_register r ON (r.fkDocument = d.fkContext)

-- Ocupa��o total
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT NULL, getdate(), 'occupation_total', CASE WHEN SUM(f.nFileSize) > 0 THEN ROUND(CAST(SUM(f.nFileSize) AS FLOAT)/1048576.0,0) ELSE 0 END
FROM (
  SELECT nFileSize FROM pm_doc_instance
  UNION ALL
  SELECT nFileSize FROM pm_template
) f

/*=============================================== CONTINUAL IMPROVEMENT ========================================================*/

-- Quantidade de Incidentes
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT NULL, getdate(), 'incident_total', count(*) FROM view_ci_incident_active

-- Quantidade de N�o-Conformidades
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT NULL, getdate(), 'non_conformity_total', count(*) FROM view_ci_nc_active

-- Quantidade de Incidentes por Estado
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT NULL, getdate(), 'incidents_by_state_open', count(i.fkContext)
FROM view_ci_incident_active i JOIN isms_context c ON (c.pkContext = i.fkContext)
WHERE c.nState = 2779
UNION
SELECT NULL, getdate(), 'incidents_by_state_directed', count(i.fkContext)
FROM view_ci_incident_active i JOIN isms_context c ON (c.pkContext = i.fkContext)
WHERE c.nState = 2780
UNION
SELECT NULL, getdate(), 'incidents_by_state_pendant_disposal', count(i.fkContext)
FROM view_ci_incident_active i JOIN isms_context c ON (c.pkContext = i.fkContext)
WHERE c.nState = 2781
UNION
SELECT NULL, getdate(), 'incidents_by_state_waiting_solution', count(i.fkContext)
FROM view_ci_incident_active i JOIN isms_context c ON (c.pkContext = i.fkContext)
WHERE c.nState = 2782
UNION
SELECT NULL, getdate(), 'incidents_by_state_pendant_solution', count(i.fkContext)
FROM view_ci_incident_active i JOIN isms_context c ON (c.pkContext = i.fkContext)
WHERE c.nState = 2783
UNION
SELECT NULL, getdate(), 'incidents_by_state_solved', count(i.fkContext)
FROM view_ci_incident_active i JOIN isms_context c ON (c.pkContext = i.fkContext)
WHERE c.nState = 2784

-- Quantidade de N�o-Conformidades por Estado
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT NULL, getdate(), 'nc_by_state_ci_sent', count(nc.fkContext)
FROM view_ci_nc_active nc JOIN isms_context c ON (c.pkContext = nc.fkContext)
WHERE c.nState = 2770
UNION
SELECT NULL, getdate(), 'nc_by_state_ci_open', count(nc.fkContext)
FROM view_ci_nc_active nc JOIN isms_context c ON (c.pkContext = nc.fkContext)
WHERE c.nState = 2771
UNION
SELECT NULL, getdate(), 'nc_by_state_ci_directed', count(nc.fkContext)
FROM view_ci_nc_active nc JOIN isms_context c ON (c.pkContext = nc.fkContext)
WHERE c.nState = 2772
UNION
SELECT NULL, getdate(), 'nc_by_state_ci_nc_pendant', count(nc.fkContext)
FROM view_ci_nc_active nc JOIN isms_context c ON (c.pkContext = nc.fkContext)
WHERE c.nState = 2773
UNION
SELECT NULL, getdate(), 'nc_by_state_ci_ap_pendant', count(nc.fkContext)
FROM view_ci_nc_active nc JOIN isms_context c ON (c.pkContext = nc.fkContext)
WHERE c.nState = 2774
UNION
SELECT NULL, getdate(), 'nc_by_state_ci_waiting_conclusion', count(nc.fkContext)
FROM view_ci_nc_active nc JOIN isms_context c ON (c.pkContext = nc.fkContext)
WHERE c.nState = 2775
UNION
SELECT NULL, getdate(), 'nc_by_state_ci_finished', count(nc.fkContext)
FROM view_ci_nc_active nc JOIN isms_context c ON (c.pkContext = nc.fkContext)
WHERE c.nState = 2776
UNION
SELECT NULL, getdate(), 'nc_by_state_ci_closed', count(nc.fkContext)
FROM view_ci_nc_active nc JOIN isms_context c ON (c.pkContext = nc.fkContext)
WHERE c.nState = 2777
UNION
SELECT NULL, getdate(), 'nc_by_state_denied', count(nc.fkContext)
FROM view_ci_nc_active nc JOIN isms_context c ON (c.pkContext = nc.fkContext)
WHERE c.nState = 2703

/*=============================================== ACTION PLAN STATISTICS ========================================================*/

-- Quantidade de Planos de A��o
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT NULL, getdate(), 'ap_total', count(*) FROM view_ci_action_plan_active

-- Quantidade de Planos de A��o finalizandos antes do deadline
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT NULL, getdate(), 'ap_finished', count(*) 
   FROM view_ci_action_plan_active
   WHERE dDateConclusion IS NOT NULL
         AND dDateConclusion <= dDateDeadLine;

-- Quantidade de Planos de A��o finalizandos em atrazo
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT NULL, getdate(), 'ap_finished_late', count(*) 
   FROM view_ci_action_plan_active
   WHERE dDateConclusion IS NOT NULL
         AND dDateConclusion > dDateDeadLine;

-- Quantidade de Planos de A��o eficientes
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT NULL, getdate(), 'ap_efficient', count(*) 
   FROM view_ci_action_plan_active
   WHERE dDateConclusion IS NOT NULL
         AND bIsEfficient = 1;

-- Quantidade de Planos de A��o n�o eficientes
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT NULL, getdate(), 'ap_non_efficient', count(*) 
   FROM view_ci_action_plan_active
   WHERE dDateConclusion IS NOT NULL
         AND bIsEfficient = 0;

-- m�dia de n�o conformidades por plano de a��o
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT NULL, getdate(), 'nc_per_ap_average', 
	CASE WHEN (SELECT COUNT(*) FROM (SELECT COUNT(*) as count FROM ci_nc_action_plan r JOIN isms_context ctx2 ON (r.fknc = ctx2.pkContext AND ctx2.pkContext <> 2705) GROUP BY fkActionPlan)aps) > 0
	            THEN CAST ((SELECT COUNT(*) FROM (SELECT COUNT(*) as count FROM ci_nc_action_plan r JOIN isms_context ctx1 ON (r.fknc = ctx1.pkContext AND ctx1.pkContext <> 2705) GROUP BY r.fknc)ncs) AS FLOAT)
		          /
		         CAST ((SELECT COUNT(*) FROM (SELECT COUNT(*) as count FROM ci_nc_action_plan r JOIN isms_context ctx2 ON (r.fknc = ctx2.pkContext AND ctx2.pkContext <> 2705) GROUP BY fkActionPlan)aps) AS FLOAT)
	            ELSE 0 END

/*==============================================================================================================================*/

-- Grava a data da coleta no banco de dados

UPDATE isms_config SET sValue = cast(year(getdate()) as varchar) + '-' + cast(month(getdate()) as varchar) + '-' + cast(day(getdate()) as varchar) WHERE pkConfig = 420

END

END
GO

/* TRIGGER ALTERADA */

ALTER TRIGGER update_control_is_active ON rm_control
FOR UPDATE
AS
BEGIN
  IF EXISTS(SELECT * FROM DELETED) BEGIN
    
    DECLARE @miControlId INT
    DECLARE @mbHasRevision INT
    DECLARE @mbHasTest INT
    DECLARE @mbWasActive INT
    DECLARE @mbIsActive INT
    DECLARE @mbShouldBeActive INT
    
    DECLARE @mdToday DATETIME
    
    DECLARE @mbImplementationIsLate INT
    DECLARE @mbEfficiencyNotOk INT
    DECLARE @mbRevisionIsLate INT
    DECLARE @mbTestNotOk INT
    DECLARE @mbTestIsLate INT
    
    DECLARE @mbOldImplementationIsLate INT
    DECLARE @mbOldEfficiencyNotOk INT
    DECLARE @mbOldRevisionIsLate INT
    DECLARE @mbOldTestNotOk INT
    DECLARE @mbOldTestIsLate INT
    
    DECLARE @mbIsWrong INT
    DECLARE @mbExists INT
    
    DECLARE updated_controls CURSOR LOCAL FOR SELECT fkContext, bIsActive FROM INSERTED
    
    SET @mdToday = CAST(FLOOR(CAST(GETDATE() AS FLOAT)) AS DATETIME)
    
    OPEN updated_controls
    FETCH NEXT FROM updated_controls INTO @miControlId, @mbIsActive
    WHILE(@@FETCH_STATUS = 0) BEGIN
      
      SELECT
        @mbWasActive               = bIsActive,
        @mbOldImplementationIsLate = bImplementationIsLate,
        @mbOldEfficiencyNotOk      = bEfficiencyNotOk,
        @mbOldRevisionIsLate       = bRevisionIsLate,
        @mbOldTestNotOk            = bTestNotOk,
        @mbOldTestIsLate           = bTestIsLate
      FROM DELETED
      WHERE fkContext = @miControlId
      
      SET @mbImplementationIsLate = 0
      SET @mbEfficiencyNotOk = 0
      SET @mbRevisionIsLate = 0
      SET @mbTestNotOk = 0
      SET @mbTestIsLate = 0
      
      -- Testa a data de implementa��o
      SELECT @mbImplementationIsLate = CASE WHEN dDateImplemented IS NULL AND dDateDeadline <= @mdToday THEN 1 ELSE 0 END
      FROM INSERTED
      WHERE fkContext = @miControlId
      
      -- Testa se tem revis�o
      SELECT
        @mbHasRevision = COUNT(*)
      FROM
        isms_config cfg,
        wkf_control_efficiency ce
      WHERE
        cfg.pkConfig = 404
        AND cfg.sValue = '1'
        AND ce.fkControlEfficiency = @miControlId
      
      -- Se tem revis�o, testa se ela t� ok e em dia.
      IF @mbHasRevision = 1 BEGIN
        SELECT @mbEfficiencyNotOk = CASE WHEN COUNT(*) = 0 THEN 1 ELSE 0 END
        FROM wkf_control_efficiency ce
        WHERE
          ce.fkControlEfficiency = @miControlId
          AND (
            ce.nRealEfficiency >= ce.nExpectedEfficiency
            OR ce.nRealEfficiency = 0
          )
        
        SELECT
          @mbRevisionIsLate = COUNT(*)
        FROM
          wkf_control_efficiency ce
          JOIN wkf_schedule s ON (s.pkschedule = ce.fkschedule)
          JOIN wkf_task_schedule ts ON (ts.fkschedule = s.pkschedule)
          JOIN wkf_task t ON (t.nactivity = ts.nactivity AND t.fkcontext = ts.fkcontext AND t.bvisible = 1)
          JOIN (
            SELECT
              s2.pkschedule AS schedule_id,
              COUNT(*) AS pendant_tasks
            FROM
              wkf_schedule s2
              JOIN wkf_task_schedule ts2 ON (ts2.fkschedule = s2.pkschedule)
              JOIN wkf_task t2 ON (t2.nactivity = ts2.nactivity AND t2.fkcontext = ts2.fkcontext AND t2.bvisible = 1)
            GROUP BY s2.pkschedule
          ) p ON (p.schedule_id = s.pkschedule)
        WHERE
          ce.fkControlEfficiency = @miControlId
          AND (
            p.pendant_tasks > 1
            OR (
              p.pendant_tasks = 1
              AND @mdToday > s.dDateLimit
            )
          )
        IF @mbRevisionIsLate > 1 BEGIN SET @mbRevisionIsLate = 1 END
      END
      
      -- Testa se tem teste
      SELECT
        @mbHasTest = COUNT(*)
      FROM
        isms_config cfg,
        wkf_control_test ct
      WHERE
        cfg.pkConfig = 405
        AND cfg.sValue = '1'
        AND ct.fkControlTest = @miControlId
      
      -- Se tem teste, testa se ele t� ok e em dia.
      IF @mbHasTest = 1 BEGIN
        SELECT
          @mbTestNotOk = CASE WHEN COUNT(*) = 1 THEN 1 ELSE 0 END
        FROM
          wkf_control_test ct
          LEFT JOIN rm_control_test_history th ON (
            th.fkControl = ct.fkControlTest
            AND NOT EXISTS (
              SELECT *
              FROM rm_control_test_history th2
              WHERE
                th2.fkControl = th.fkControl
                AND th2.dDateAccomplishment > th.dDateAccomplishment
            )
          )
        WHERE
          ct.fkControlTest = @miControlId
          AND th.bTestedValue = 0
        
        SELECT
          @mbTestIsLate = COUNT(*)
        FROM
          wkf_control_test ct
          JOIN wkf_schedule s ON (s.pkschedule = ct.fkschedule)
          JOIN wkf_task_schedule ts ON (ts.fkschedule = s.pkschedule)
          JOIN wkf_task t ON (t.nactivity = ts.nactivity AND t.fkcontext = ts.fkcontext AND t.bvisible = 1)
          JOIN (
            SELECT
              s2.pkschedule AS schedule_id,
              COUNT(*) AS pendant_tasks
            FROM
              wkf_schedule s2
              JOIN wkf_task_schedule ts2 ON (ts2.fkschedule = s2.pkschedule)
              JOIN wkf_task t2 ON (t2.nactivity = ts2.nactivity AND t2.fkcontext = ts2.fkcontext AND t2.bvisible = 1)
            GROUP BY s2.pkschedule
          ) p ON (p.schedule_id = s.pkschedule)
        WHERE
          ct.fkControlTest = @miControlId
          AND (
            p.pendant_tasks > 1
            OR (
              p.pendant_tasks = 1
              AND @mdToday > s.dDateLimit
            )
          );
        IF @mbTestIsLate > 1 BEGIN SET @mbTestIsLate = 1 END
      END
      
      -- Atualiza as flags e cria as seeds de N�o-Conformidade que forem necess�rias
      IF @mbImplementationIsLate + @mbEfficiencyNotOk + @mbRevisionIsLate + @mbTestNotOk + @mbTestIsLate > 0 BEGIN
        SET @mbShouldBeActive = 0
      END ELSE BEGIN
        SET @mbShouldBeActive = 1
      END
      
      IF @mbIsActive != @mbShouldBeActive BEGIN
        UPDATE rm_control SET bIsActive = @mbShouldBeActive WHERE fkContext = @miControlId
      END
      
      SELECT @mbIsWrong = COUNT(*)
      FROM rm_control
      WHERE
        fkContext = @miControlId
        AND (
             bIsActive             != @mbShouldBeActive
          OR bImplementationIsLate != @mbImplementationIsLate
          OR bEfficiencyNotOk      != @mbEfficiencyNotOk
          OR bRevisionIsLate       != @mbRevisionIsLate
          OR bTestNotOk            != @mbTestNotOk
          OR bTestIsLate           != @mbTestIsLate
        )
      
      IF @mbIsWrong = 1 BEGIN
        UPDATE rm_control
        SET
          bIsActive             = @mbShouldBeActive,
          bImplementationIsLate = @mbImplementationIsLate,
          bEfficiencyNotOk      = @mbEfficiencyNotOk,
          bRevisionIsLate       = @mbRevisionIsLate,
          bTestNotOk            = @mbTestNotOk,
          bTestIsLate           = @mbTestIsLate
        WHERE fkContext = @miControlId
        
        IF @mbShouldBeActive = 0 BEGIN
          
          IF @mbOldImplementationIsLate = 0 AND @mbImplementationIsLate = 1 BEGIN
            SELECT @mbExists = COUNT(*) FROM ci_nc_seed WHERE fkControl = @miControlId AND nDeactivationReason = 1
            IF @mbExists = 0 BEGIN
              INSERT INTO ci_nc_seed (fkControl,nDeactivationReason,dDateSent) VALUES (@miControlId,1,getdate())
            END
          END
          
          IF @mbOldEfficiencyNotOk = 0 AND @mbEfficiencyNotOk = 1 BEGIN
            SELECT @mbExists = COUNT(*) FROM ci_nc_seed WHERE fkControl = @miControlId AND nDeactivationReason = 2
            IF @mbExists = 0 BEGIN
              INSERT INTO ci_nc_seed (fkControl,nDeactivationReason,dDateSent) VALUES (@miControlId,2,getdate())
            END
          END
          
          IF @mbOldRevisionIsLate = 0 AND @mbRevisionIsLate = 1 BEGIN
            SELECT @mbExists = COUNT(*) FROM ci_nc_seed WHERE fkControl = @miControlId AND nDeactivationReason = 4
            IF @mbExists = 0 BEGIN
              INSERT INTO ci_nc_seed (fkControl,nDeactivationReason,dDateSent) VALUES (@miControlId,4,getdate())
            END
          END
          
          IF @mbOldTestNotOk = 0 AND @mbTestNotOk = 1 BEGIN
            SELECT @mbExists = COUNT(*) FROM ci_nc_seed WHERE fkControl = @miControlId AND nDeactivationReason = 8
            IF @mbExists = 0 BEGIN
              INSERT INTO ci_nc_seed (fkControl,nDeactivationReason,dDateSent) VALUES (@miControlId,8,getdate())
            END
          END
          
          IF @mbOldTestIsLate = 0 AND @mbTestIsLate = 1 BEGIN
            SELECT @mbExists = COUNT(*) FROM ci_nc_seed WHERE fkControl = @miControlId AND nDeactivationReason = 16
            IF @mbExists = 0 BEGIN
              INSERT INTO ci_nc_seed (fkControl,nDeactivationReason,dDateSent) VALUES (@miControlId,16,getdate())
            END
          END
          
        END
        
      END ELSE BEGIN
        IF @mbShouldBeActive != @mbWasActive BEGIN
          UPDATE rm_risk 
          SET
            nValue = %database_login%.get_risk_value(fkContext),
            nValueResidual = %database_login%.get_risk_value_residual(fkContext)
          WHERE fkContext IN (
            SELECT fkRisk
            FROM rm_risk_control
            WHERE fkControl = @miControlId
          )
        END
      END
      
      FETCH NEXT FROM updated_controls INTO @miControlId, @mbIsActive
    END
    
  END
END
GO

/* NOVA TRIGGER */

CREATE TRIGGER update_alert_sent ON wkf_task
FOR UPDATE
AS
BEGIN
  IF (@@rowcount=0) RETURN
  
  DECLARE @miActivity INT
  DECLARE @miContextId INT
  DECLARE @miCount INT
  DECLARE @mdToday DATETIME
  
  SET @mdToday = CAST(FLOOR(CAST(GETDATE() AS FLOAT)) AS DATETIME)
  
  DECLARE query_tasks CURSOR FOR
    SELECT i.fkcontext, i.nactivity
    FROM inserted i JOIN deleted d ON (i.pktask = d.pktask)
    WHERE d.ddateaccomplished IS NULL
      AND i.ddateaccomplished IS NOT NULL
  
  OPEN query_tasks
  FETCH NEXT FROM query_tasks INTO @miContextId, @miActivity
  WHILE @@FETCH_STATUS = 0 BEGIN
    
    SELECT
      @miCount = COUNT(*)
    FROM
      wkf_schedule s
      JOIN wkf_task_schedule ts ON (ts.fkschedule = s.pkschedule)
      JOIN wkf_task t ON (t.nactivity = ts.nactivity AND t.fkcontext = ts.fkcontext AND t.bvisible = 1)
      JOIN (
        SELECT
          s.pkschedule AS schedule_id,
          COUNT(*) AS pendant_tasks
        FROM
          wkf_schedule s
          JOIN wkf_task_schedule ts ON (ts.fkschedule = s.pkschedule)
          JOIN wkf_task t ON (t.nactivity = ts.nactivity AND t.fkcontext = ts.fkcontext AND t.bvisible = 1)
        WHERE
          ts.fkcontext = @miContextId
          AND ts.nactivity = @miActivity
        GROUP BY s.pkschedule
      ) p ON (p.schedule_id = s.pkschedule)
    WHERE
      ts.fkcontext = @miContextId
      AND ts.nactivity = @miActivity
      AND p.pendant_tasks > 1
      OR (
        p.pendant_tasks = 1
        AND @mdToday > s.dDateLimit
      )
    
    IF @miCount = 0 BEGIN
      UPDATE wkf_task_schedule
      SET bAlertSent = 0
      WHERE
        nactivity = @miActivity
        AND fkcontext = @miContextId
    END
    
    FETCH NEXT FROM query_tasks INTO @miContextId, @miActivity
  END
  CLOSE query_tasks
  DEALLOCATE query_tasks
END
GO

/* NOVA FUNCTION */

CREATE FUNCTION get_areas_and_subareas_by_user (@userId INT)
RETURNS  @result TABLE (
    area_id INT
)
AS

BEGIN   
    DECLARE @area_id INT
    DECLARE @area_id_sub INT

    DECLARE areas_user CURSOR FOR
        SELECT  a.fkContext
  FROM isms_context cont_a
  JOIN rm_area a ON (cont_a.pkContext = a.fkContext AND cont_a.nState <> 2705)    
  WHERE a.fkResponsible = @userId

    OPEN areas_user
    FETCH NEXT FROM areas_user INTO @area_id
    WHILE (@@FETCH_STATUS = 0)
    BEGIN
  INSERT INTO @result values(@area_id) 

  DECLARE areas_user_rec CURSOR FOR
  SELECT pkArea FROM %database_login%.get_sub_areas(@area_id)
  JOIN isms_context c ON (c.pkContext = pkArea AND c.nState <> 2705)
        WHERE pkArea NOT IN(select area_id as pkArea from @result)
  
  OPEN areas_user_rec
  FETCH NEXT FROM areas_user_rec INTO @area_id_sub
  WHILE (@@FETCH_STATUS = 0)
  BEGIN
      INSERT INTO @result values(@area_id_sub)
            FETCH NEXT FROM areas_user_rec INTO @area_id_sub
  END
  CLOSE areas_user_rec
        DEALLOCATE areas_user_rec
  FETCH NEXT FROM areas_user INTO @area_id
    END  


    CLOSE areas_user
    DEALLOCATE areas_user
    
    RETURN
END
GO

/* VIEW ALTERADA */

ALTER VIEW view_isms_user_active AS
SELECT
	u.fkContext, u.fkProfile, u.sName, u.sLogin, u.sPassword, u.sEmail, u.nIp,
	u.nLanguage, u.dLastLogin, u.nWrongLogonAttempts, u.bIsBlocked, u.bMustChangePassword
FROM isms_context c JOIN isms_user u ON (c.pkContext = u.fkContext AND c.nState <> 2705)
GO

/* FUNCTION ALTERADA */

ALTER FUNCTION get_asset_value  ( @piAsset NUMERIC )  
RETURNS FLOAT
AS 
BEGIN 
DECLARE @fiAssetValue FLOAT

SET @fiAssetValue = 0

DECLARE query_asset CURSOR
FOR SELECT MAX(value) FROM (
		SELECT MAX(nvalueresidual) as value
			FROM rm_risk as r
			JOIN isms_context c ON(c.pkContext = r.fkContext and c.nState <> 2705)
			WHERE r.fkasset = @piAsset
			AND nAcceptMode = 0
		UNION
		SELECT MAX(0.1) as value
			FROM rm_risk as r
			JOIN isms_context c ON(c.pkContext = r.fkContext and c.nState <> 2705)
			WHERE r.fkasset = @piAsset
			AND nAcceptMode <> 0
		UNION
		SELECT MAX(nvalue) as value
			FROM rm_asset a
			JOIN isms_context c1 ON(c1.pkContext = a.fkContext and c1.nState <> 2705)
			JOIN rm_asset_asset aa ON (a.fkContext = aa.fkAsset)
			JOIN isms_context c2 ON(c2.pkContext = aa.fkAsset and c2.nState <> 2705)
			WHERE aa.fkdependent = @piAsset 
		) as buffer


OPEN query_asset
FETCH NEXT FROM query_asset INTO @fiAssetValue
CLOSE query_asset
DEALLOCATE query_asset

IF(@fiAssetValue IS NULL) return 0

RETURN @fiAssetValue
END
GO

/* DELETA FUN��ES AUXILIARES */

DROP PROCEDURE insert_acl
GO

DROP PROCEDURE update_acl
GO

DROP PROCEDURE delete_acl
GO

DROP PROCEDURE map_permissions
GO

DROP PROCEDURE drop_column
GO