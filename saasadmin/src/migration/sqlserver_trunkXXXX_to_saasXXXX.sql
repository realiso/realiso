/* NOVA TABELA */

create table isms_saas (
   pkConfig             int                  not null,
   sValue               varchar(256)         null,
   constraint pk_isms_saas primary key  (pkconfig)
)
go

/* ALTERA��O NA TABELA ISMS_USER */

ALTER TABLE isms_user ADD
  sRequestPassword         varchar(256)                 null,
  sSession                 varchar(256)                 null,
  bAnsweredSurvey          int                          null default 0,
  bShowHelp                int                          null default 1
GO
UPDATE isms_user set bAnsweredSurvey = 0, bShowHelp = 1
GO


/* CONFIGS INSERIDAS EM ISMS_CONFIG */

INSERT INTO isms_config (pkconfig,svalue) VALUES (430,'Axur ISMS Team');
INSERT INTO isms_config (pkconfig,svalue) VALUES (7209,'support@axur.net');

/* CONFIGS DELETADAS DE ISMS_CONFIG */

DELETE FROM isms_config WHERE pkConfig IN (7230, 7231, 7232, 7901, 7902, 7903, 7904, 7905, 7906)
GO

/* CONFIGS DA TABELA ISMS_SAAS */

INSERT INTO isms_saas (pkconfig, svalue) VALUES (711701, 'support@axur.net');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711703, '%client_alias%');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711704, ''); -- modulos
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711705, ''); -- max users
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711706, ''); -- timestamp de expira��o (tem que ser vazio mesmo)
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711707, ''); -- max simult. users
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711708, ''); -- hash
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711709, '1');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711710, ''); -- tipo da licen�a
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711712, ''); -- timestamp da data de ativa��o
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711713, '%user_email%');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711714, '/home/isms/skel/admin/current/classes/config.php');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711715, '5242880');

/* ACLS */

INSERT INTO isms_profile_acl (fkprofile,stag) SELECT fkprofile,'A.S' FROM isms_profile_acl pa JOIN isms_profile p ON (p.fkcontext = pa.fkprofile AND p.nid = 0) WHERE pa.stag = 'A.C.1';
INSERT INTO isms_profile_acl (fkprofile,stag) SELECT fkprofile,'A.S.1' FROM isms_profile_acl pa JOIN isms_profile p ON (p.fkcontext = pa.fkprofile AND p.nid = 0) WHERE pa.stag = 'A.C.1';
INSERT INTO isms_profile_acl (fkprofile,stag) SELECT fkprofile,'A.S.2' FROM isms_profile_acl pa JOIN isms_profile p ON (p.fkcontext = pa.fkprofile AND p.nid = 0) WHERE pa.stag = 'A.C.1';
INSERT INTO isms_profile_acl (fkprofile,stag) SELECT fkprofile,'A.C.7.1' FROM isms_profile_acl pa JOIN isms_profile p ON (p.fkcontext = pa.fkprofile AND p.nid = 0) WHERE pa.stag = 'A.C.8.1';
INSERT INTO isms_profile_acl (fkprofile,stag) SELECT fkprofile,'A.C.7.2' FROM isms_profile_acl pa JOIN isms_profile p ON (p.fkcontext = pa.fkprofile AND p.nid = 0) WHERE pa.stag = 'A.C.8.2';
INSERT INTO isms_profile_acl (fkprofile,stag) SELECT fkprofile,'A.C.7.3' FROM isms_profile_acl pa JOIN isms_profile p ON (p.fkcontext = pa.fkprofile AND p.nid = 0) WHERE pa.stag = 'A.C.8.3';
INSERT INTO isms_profile_acl (fkprofile,stag) SELECT fkprofile,'A.C.7.4' FROM isms_profile_acl pa JOIN isms_profile p ON (p.fkcontext = pa.fkprofile AND p.nid = 0) WHERE pa.stag = 'A.C.8.4';
INSERT INTO isms_profile_acl (fkprofile,stag) SELECT fkprofile,'A.C.7.5' FROM isms_profile_acl pa JOIN isms_profile p ON (p.fkcontext = pa.fkprofile AND p.nid = 0) WHERE pa.stag = 'A.C.8.5';
INSERT INTO isms_profile_acl (fkprofile,stag) SELECT fkprofile,'A.C.7.6' FROM isms_profile_acl pa JOIN isms_profile p ON (p.fkcontext = pa.fkprofile AND p.nid = 0) WHERE pa.stag = 'A.C.8.6';
INSERT INTO isms_profile_acl (fkprofile,stag) SELECT fkprofile,'A.C.7.7' FROM isms_profile_acl pa JOIN isms_profile p ON (p.fkcontext = pa.fkprofile AND p.nid = 0) WHERE pa.stag = 'A.C.8.7';
INSERT INTO isms_profile_acl (fkprofile,stag) SELECT fkprofile,'A.C.11' FROM isms_profile_acl pa JOIN isms_profile p ON (p.fkcontext = pa.fkprofile AND p.nid = 0) WHERE pa.stag = 'A.C';
INSERT INTO isms_profile_acl (fkprofile,stag) SELECT fkprofile,'A.MA' FROM isms_profile_acl pa JOIN isms_profile p ON (p.fkcontext = pa.fkprofile AND p.nid = 0) WHERE pa.stag = 'A.C';
DELETE FROM isms_profile_acl WHERE stag = 'M.D.6.10';
DELETE FROM isms_profile_acl WHERE stag = 'M.D.5';
DELETE FROM isms_profile_acl WHERE stag = 'M.D.5.1';
DELETE FROM isms_profile_acl WHERE stag = 'M.D.5.2';
DELETE FROM isms_profile_acl WHERE stag = 'M.RM.4.7';
DELETE FROM isms_profile_acl WHERE stag = 'M.RM.4.11';
DELETE FROM isms_profile_acl WHERE stag = 'M.RM.6.5.3';
DELETE FROM isms_profile_acl WHERE stag = 'M.RM.6.5.4';
DELETE FROM isms_profile_acl WHERE stag = 'A.C.8.1';
DELETE FROM isms_profile_acl WHERE stag = 'A.C.8.2';
DELETE FROM isms_profile_acl WHERE stag = 'A.C.8.3';
DELETE FROM isms_profile_acl WHERE stag = 'A.C.8.4';
DELETE FROM isms_profile_acl WHERE stag = 'A.C.8.5';
DELETE FROM isms_profile_acl WHERE stag = 'A.C.8.6';
DELETE FROM isms_profile_acl WHERE stag = 'A.C.8.7';
INSERT INTO isms_profile_acl (fkprofile,stag) SELECT fkcontext, 'A.S' FROM isms_profile WHERE nid = 8801;
INSERT INTO isms_profile_acl (fkprofile,stag) SELECT fkcontext, 'A.S' FROM isms_profile WHERE nid = 8803;
INSERT INTO isms_profile_acl (fkprofile,stag) SELECT fkcontext, 'A.S.1' FROM isms_profile WHERE nid = 8801;
INSERT INTO isms_profile_acl (fkprofile,stag) SELECT fkcontext, 'A.S.1' FROM isms_profile WHERE nid = 8803;
INSERT INTO isms_profile_acl (fkprofile,stag) SELECT fkcontext, 'A.S.2' FROM isms_profile WHERE nid = 8801;
INSERT INTO isms_profile_acl (fkprofile,stag) SELECT fkcontext, 'A.S.2' FROM isms_profile WHERE nid = 8803;
INSERT INTO isms_profile_acl (fkprofile,stag) SELECT fkcontext, 'A.C.7.1' FROM isms_profile WHERE nid = 8801;
INSERT INTO isms_profile_acl (fkprofile,stag) SELECT fkcontext, 'A.C.7.1' FROM isms_profile WHERE nid = 8803;
INSERT INTO isms_profile_acl (fkprofile,stag) SELECT fkcontext, 'A.C.7.2' FROM isms_profile WHERE nid = 8801;
INSERT INTO isms_profile_acl (fkprofile,stag) SELECT fkcontext, 'A.C.7.2' FROM isms_profile WHERE nid = 8803;
INSERT INTO isms_profile_acl (fkprofile,stag) SELECT fkcontext, 'A.C.7.3' FROM isms_profile WHERE nid = 8801;
INSERT INTO isms_profile_acl (fkprofile,stag) SELECT fkcontext, 'A.C.7.3' FROM isms_profile WHERE nid = 8803;
INSERT INTO isms_profile_acl (fkprofile,stag) SELECT fkcontext, 'A.C.7.4' FROM isms_profile WHERE nid = 8801;
INSERT INTO isms_profile_acl (fkprofile,stag) SELECT fkcontext, 'A.C.7.4' FROM isms_profile WHERE nid = 8803;
INSERT INTO isms_profile_acl (fkprofile,stag) SELECT fkcontext, 'A.C.7.5' FROM isms_profile WHERE nid = 8801;
INSERT INTO isms_profile_acl (fkprofile,stag) SELECT fkcontext, 'A.C.7.5' FROM isms_profile WHERE nid = 8803;
INSERT INTO isms_profile_acl (fkprofile,stag) SELECT fkcontext, 'A.C.7.6' FROM isms_profile WHERE nid = 8801;
INSERT INTO isms_profile_acl (fkprofile,stag) SELECT fkcontext, 'A.C.7.6' FROM isms_profile WHERE nid = 8803;
INSERT INTO isms_profile_acl (fkprofile,stag) SELECT fkcontext, 'A.C.7.7' FROM isms_profile WHERE nid = 8801;
INSERT INTO isms_profile_acl (fkprofile,stag) SELECT fkcontext, 'A.C.7.7' FROM isms_profile WHERE nid = 8803;
INSERT INTO isms_profile_acl (fkprofile,stag) SELECT fkcontext, 'A.C.11' FROM isms_profile WHERE nid = 8801;
INSERT INTO isms_profile_acl (fkprofile,stag) SELECT fkcontext, 'A.C.11' FROM isms_profile WHERE nid = 8803;
INSERT INTO isms_profile_acl (fkprofile,stag) SELECT fkcontext, 'A.MA' FROM isms_profile WHERE nid = 8801;
INSERT INTO isms_profile_acl (fkprofile,stag) SELECT fkcontext, 'A.MA' FROM isms_profile WHERE nid = 8803;

/* VIEW ALTERADA MAS N�O PRECISA ATUALIZAR POIS J� VAI SER CRIADA */

/*
ALTER VIEW view_isms_user_active AS
SELECT
	u.fkContext, u.fkProfile, u.sName, u.sLogin, u.sPassword, u.sEmail, u.nIp,
	u.nLanguage, u.dLastLogin, u.nWrongLogonAttempts, u.bIsBlocked, u.bMustChangePassword, u.sRequestPassword, u.sSession, u.bAnsweredSurvey, u.bShowHelp
FROM isms_context c JOIN isms_user u ON (c.pkContext = u.fkContext AND c.nState <> 2705)
GO
*/

/* FUN��O ALTERADA MAS N�O PRECISA ATUALIZAR POIS J� VAI SER CRIADA  */

/*
CREATE OR REPLACE FUNCTION get_supercategories_events(piCategoryId integer) RETURNS SETOF event_id AS
'DECLARE
  mrEvent record;
  miCategoryId integer;
BEGIN
  miCategoryId = piCategoryId;
  WHILE miCategoryId IS NOT NULL LOOP
    FOR mrEvent IN SELECT fkContext FROM view_rm_event_active WHERE fkCategory = miCategoryId LOOP
      RETURN NEXT mrEvent;
    END LOOP;
    SELECT fkParent INTO miCategoryId FROM view_rm_category_active WHERE fkContext = miCategoryId;
  END LOOP;
  RETURN;
END'
LANGUAGE 'plpgsql' VOLATILE;
*/

/*
CREATE OR REPLACE FUNCTION get_asset_value(integer)
  RETURNS double precision AS
'
 SELECT MAX(value) FROM (
		SELECT MAX(nvalueresidual) as value
			FROM rm_risk as r
	    JOIN isms_context c ON(c.pkContext = r.fkContext and c.nState <> 2705)
			WHERE r.fkasset = $1
			AND (nAcceptMode = 0)
		UNION
		SELECT MAX(0.1) as value
			FROM rm_risk as r
	    JOIN isms_context c ON(c.pkContext = r.fkContext and c.nState <> 2705)
			WHERE r.fkasset = $1
			AND (nAcceptMode <> 0)
		UNION
		SELECT MAX(nvalue) as value
			FROM rm_asset_asset aa
			JOIN rm_asset a ON(aa.fkAsset = a.fkContext AND aa.fkdependent = $1)
			JOIN isms_context c1 ON(c1.pkContext = a.fkContext and c1.nState <> 2705)
			JOIN isms_context c2 ON(c2.pkContext = aa.fkAsset and c2.nState <> 2705)
    UNION 
		SELECT 0 as value
		) as buffer
'
  LANGUAGE 'sql' VOLATILE;
*/

/*
DROP FUNCTION get_document_tree(integer, integer);
CREATE OR REPLACE FUNCTION get_document_tree(integer, integer, integer)
RETURNS SETOF tree_document AS
'DECLARE
piParent ALIAS FOR $1;
pnLevel ALIAS FOR $2;
pbPublished ALIAS FOR $3;
nAuxLevel integer;
queryDoc varchar(255);
queryDocAux varchar(255);
queryDocRecursion varchar(255);
documentRoot tree_document%rowtype;
subDoc tree_document%rowtype;

BEGIN
  nAuxLevel := pnLevel + 1;
  queryDocAux := '' SELECT fkContext, sName, '' || nAuxLevel::text || '' as nLevel FROM view_pm_document_active WHERE fkParent '';
  IF piParent > 0 THEN
    queryDoc := queryDocAux || '' = '' || piParent;
  ELSE 
    queryDoc := queryDocAux || '' is NULL'';
  END IF;
  IF (pbPublished=1) THEN
    FOR documentRoot IN EXECUTE queryDoc LOOP
      IF EXISTS (SELECT fkContext FROM view_pm_published_docs WHERE fkContext = documentRoot.fkContext) THEN
        RETURN NEXT documentRoot;
        queryDocRecursion := ''SELECT fkContext, sName, nLevel FROM get_document_tree('' || documentRoot.fkContext::text || '' , '' || nAuxLevel::text || '' ) '';
        FOR subDoc IN EXECUTE queryDocRecursion LOOP
          RETURN NEXT subDoc;
        END LOOP;
      END IF;
    END LOOP;
  ELSE
    FOR documentRoot IN EXECUTE queryDoc LOOP
      RETURN NEXT documentRoot;
      queryDocRecursion := ''SELECT fkContext, sName, nLevel FROM get_document_tree('' || documentRoot.fkContext::text || '' , '' || nAuxLevel::text || '' ) '';
      FOR subDoc IN EXECUTE queryDocRecursion LOOP
        RETURN NEXT subDoc;
      END LOOP;
    END LOOP;
  END IF;
  RETURN ;
END'
LANGUAGE 'plpgsql' VOLATILE;
*/