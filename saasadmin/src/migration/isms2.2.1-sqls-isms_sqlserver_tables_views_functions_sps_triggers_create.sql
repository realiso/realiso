/*
Substituir %database_login% pelo usu�rio correto!
*/

/*==============================================================*/
/* Table: ci_action_plan                                        */
/*==============================================================*/
create table ci_action_plan (
   fkcontext            numeric              not null,
   fkresponsible        numeric              null,
   sname                varchar(256)         not null,
   tactionplan          text                 null,
   nactiontype          int                  null default 0,
   ddatedeadline        datetime             null,
   ddateconclusion      datetime             null,
   bisefficient         int                  null default 0
      constraint ckc_bisefficient_ci_actio check (bisefficient is null or (bisefficient between 0 and 1 )),
   ddateefficiencyrevision datetime             null,
   ddateefficiencymeasured datetime             null,
   ndaysbefore          int                  null default 0,
   bflagrevisionalert   int                  null default 0
      constraint ckc_bflagrevisionaler_ci_actio2 check (bflagrevisionalert is null or (bflagrevisionalert between 0 and 1 )),
   bflagrevisionalertlate int                  null default 0
      constraint ckc_bflagrevisionaler_ci_actio check (bflagrevisionalertlate is null or (bflagrevisionalertlate between 0 and 1 )),
   constraint pk_ci_action_plan primary key  (fkcontext)
)
go


/*==============================================================*/
/* Index: fkresponsible_fk                                      */
/*==============================================================*/
create   index fkresponsible_fk on ci_action_plan (
fkresponsible
)
go


/*==============================================================*/
/* Table: ci_category                                           */
/*==============================================================*/
create table ci_category (
   fkcontext            numeric              not null,
   sname                varchar(256)         not null,
   constraint pk_ci_category primary key  (fkcontext)
)
go


/*==============================================================*/
/* Table: ci_incident                                           */
/*==============================================================*/
create table ci_incident (
   fkcontext            numeric              not null,
   fkcategory           numeric              null,
   fkresponsible        numeric              null,
   sname                varchar(256)         not null,
   taccountsplan        text                 null,
   tevidences           text                 null,
   nlosstype            int                  null default 0,
   tevidencerequirementcomment text                 null,
   ddatelimit           datetime             null,
   ddatefinish          datetime             null,
   tdisposaldescription text                 null,
   tsolutiondescription text                 null,
   tproductservice      text                 null,
   bnotemaildp          int                  null default 0
      constraint ckc_bnotemaildp_ci_incid check (bnotemaildp is null or (bnotemaildp between 0 and 1 )),
   bemaildpsent         int                  null default 0
      constraint ckc_bemaildpsent_ci_incid check (bemaildpsent is null or (bemaildpsent between 0 and 1 )),
   ddate                datetime             not null,
   constraint pk_ci_incident primary key  (fkcontext)
)
go


/*==============================================================*/
/* Index: fkcategory_fk                                         */
/*==============================================================*/
create   index fkcategory_fk on ci_incident (
fkcategory
)
go


/*==============================================================*/
/* Index: fkresponsible_fk                                      */
/*==============================================================*/
create   index fkresponsible_fk on ci_incident (
fkresponsible
)
go


/*==============================================================*/
/* Table: ci_incident_control                                   */
/*==============================================================*/
create table ci_incident_control (
   fkincident           numeric              not null,
   fkcontrol            numeric              not null,
   fkcontext            numeric              not null,
   constraint pk_ci_incident_control primary key  (fkincident, fkcontrol, fkcontext)
)
go


/*==============================================================*/
/* Index: fkincident_fk                                         */
/*==============================================================*/
create   index fkincident_fk on ci_incident_control (
fkincident
)
go


/*==============================================================*/
/* Index: fkcontrol_fk                                          */
/*==============================================================*/
create   index fkcontrol_fk on ci_incident_control (
fkcontrol
)
go


/*==============================================================*/
/* Index: fkcontext_fk                                          */
/*==============================================================*/
create   index fkcontext_fk on ci_incident_control (
fkcontext
)
go


/*==============================================================*/
/* Table: ci_incident_financial_impact                          */
/*==============================================================*/
create table ci_incident_financial_impact (
   fkincident           numeric              not null,
   fkclassification     numeric              not null,
   nvalue               float                null default 0,
   constraint pk_ci_incident_financial_impac primary key  (fkincident, fkclassification)
)
go


/*==============================================================*/
/* Index: fkincident_fk                                         */
/*==============================================================*/
create   index fkincident_fk on ci_incident_financial_impact (
fkincident
)
go


/*==============================================================*/
/* Index: fkclassification_fk                                   */
/*==============================================================*/
create   index fkclassification_fk on ci_incident_financial_impact (
fkclassification
)
go


/*==============================================================*/
/* Table: ci_incident_process                                   */
/*==============================================================*/
create table ci_incident_process (
   fkcontext            numeric              not null,
   fkprocess            numeric              not null,
   fkincident           numeric              not null,
   constraint pk_ci_incident_process primary key  (fkprocess, fkincident, fkcontext)
)
go


/*==============================================================*/
/* Index: fkprocess_fk                                          */
/*==============================================================*/
create   index fkprocess_fk on ci_incident_process (
fkprocess
)
go


/*==============================================================*/
/* Index: fkincident_fk                                         */
/*==============================================================*/
create   index fkincident_fk on ci_incident_process (
fkincident
)
go


/*==============================================================*/
/* Index: fkcontext_fk                                          */
/*==============================================================*/
create   index fkcontext_fk on ci_incident_process (
fkcontext
)
go


/*==============================================================*/
/* Table: ci_incident_risk                                      */
/*==============================================================*/
create table ci_incident_risk (
   fkcontext            numeric              not null,
   fkrisk               numeric              not null,
   fkincident           numeric              not null,
   constraint pk_ci_incident_risk primary key  (fkcontext)
)
go


/*==============================================================*/
/* Index: fkrisk_fk                                             */
/*==============================================================*/
create   index fkrisk_fk on ci_incident_risk (
fkrisk
)
go


/*==============================================================*/
/* Index: fkincident_fk                                         */
/*==============================================================*/
create   index fkincident_fk on ci_incident_risk (
fkincident
)
go


/*==============================================================*/
/* Table: ci_incident_risk_value                                */
/*==============================================================*/
create table ci_incident_risk_value (
   fkparametername      numeric              not null,
   fkvaluename          numeric              not null,
   fkincrisk            numeric              not null,
   constraint pk_ci_incident_risk_value primary key  (fkparametername, fkvaluename, fkincrisk)
)
go


/*==============================================================*/
/* Index: fkparametername_fk                                    */
/*==============================================================*/
create   index fkparametername_fk on ci_incident_risk_value (
fkparametername
)
go


/*==============================================================*/
/* Index: fkvaluename_fk                                        */
/*==============================================================*/
create   index fkvaluename_fk on ci_incident_risk_value (
fkvaluename
)
go


/*==============================================================*/
/* Index: fkincrisk_fk                                          */
/*==============================================================*/
create   index fkincrisk_fk on ci_incident_risk_value (
fkincrisk
)
go


/*==============================================================*/
/* Table: ci_incident_user                                      */
/*==============================================================*/
create table ci_incident_user (
   fkuser               numeric              not null,
   fkincident           numeric              not null,
   tdescription         text                 not null,
   constraint pk_ci_incident_user primary key  (fkuser, fkincident)
)
go


/*==============================================================*/
/* Index: fkincident_fk                                         */
/*==============================================================*/
create   index fkincident_fk on ci_incident_user (
fkincident
)
go


/*==============================================================*/
/* Index: fkuser_fk                                             */
/*==============================================================*/
create   index fkuser_fk on ci_incident_user (
fkuser
)
go


/*==============================================================*/
/* Table: ci_nc                                                 */
/*==============================================================*/
create table ci_nc (
   fkcontext            numeric              not null,
   fkcontrol            numeric              null,
   fkresponsible        numeric              null,
   fksender             numeric              not null,
   sname                varchar(256)         not null,
   nseqnumber           numeric              identity,
   tdescription         text                 null,
   tcause               text                 null,
   tdenialjustification text                 null,
   nclassification      int                  null default 0,
   ddatesent            datetime             null,
   ncapability          int                  null default 0,
   constraint pk_ci_nc primary key  (fkcontext)
)
go


/*==============================================================*/
/* Index: fkcontrol_fk                                          */
/*==============================================================*/
create   index fkcontrol_fk on ci_nc (
fkcontrol
)
go


/*==============================================================*/
/* Index: fkresponsible_fk                                      */
/*==============================================================*/
create   index fkresponsible_fk on ci_nc (
fkresponsible
)
go


/*==============================================================*/
/* Index: fksender_fk                                           */
/*==============================================================*/
create   index fksender_fk on ci_nc (
fksender
)
go


/*==============================================================*/
/* Table: ci_nc_action_plan                                     */
/*==============================================================*/
create table ci_nc_action_plan (
   fknc                 numeric              not null,
   fkactionplan         numeric              not null,
   constraint pk_ci_nc_action_plan primary key  (fknc, fkactionplan)
)
go


/*==============================================================*/
/* Table: ci_nc_process                                         */
/*==============================================================*/
create table ci_nc_process (
   fknc                 numeric              not null,
   fkprocess            numeric              not null,
   constraint pk_ci_nc_process primary key  (fknc, fkprocess)
)
go


/*==============================================================*/
/* Index: fknc_fk                                               */
/*==============================================================*/
create   index fknc_fk on ci_nc_process (
fknc
)
go


/*==============================================================*/
/* Index: fkprocess_fk                                          */
/*==============================================================*/
create   index fkprocess_fk on ci_nc_process (
fkprocess
)
go


/*==============================================================*/
/* Table: ci_nc_seed                                            */
/*==============================================================*/
create table ci_nc_seed (
   fkcontrol            numeric              not null,
   ndeactivationreason  int                  not null default 0,
   ddatesent            datetime             null,
   constraint pk_ci_nc_seed primary key  (fkcontrol, ndeactivationreason)
)
go


/*==============================================================*/
/* Index: fkcontrol_fk                                          */
/*==============================================================*/
create   index fkcontrol_fk on ci_nc_seed (
fkcontrol
)
go


/*==============================================================*/
/* Table: ci_occurrence                                         */
/*==============================================================*/
create table ci_occurrence (
   fkcontext            numeric              not null,
   fkincident           numeric              null,
   tdescription         text                 not null,
   tdenialjustification text                 null,
   ddate                datetime             not null,
   constraint pk_ci_occurrence primary key  (fkcontext)
)
go


/*==============================================================*/
/* Index: fkincident_fk                                         */
/*==============================================================*/
create   index fkincident_fk on ci_occurrence (
fkincident
)
go


/*==============================================================*/
/* Table: ci_risk_probability                                   */
/*==============================================================*/
create table ci_risk_probability (
   fkrisk               numeric              not null,
   fkvaluename          numeric              not null,
   nincidentamount      int                  not null default 0,
   constraint pk_ci_risk_probability primary key  (fkrisk, fkvaluename)
)
go


/*==============================================================*/
/* Index: fkparamter_fk                                         */
/*==============================================================*/
create   index fkparamter_fk on ci_risk_probability (
fkvaluename
)
go


/*==============================================================*/
/* Index: fkrisk_fk                                             */
/*==============================================================*/
create   index fkrisk_fk on ci_risk_probability (
fkrisk
)
go


/*==============================================================*/
/* Table: ci_risk_schedule                                      */
/*==============================================================*/
create table ci_risk_schedule (
   fkrisk               numeric              not null,
   nperiod              int                  not null default 0,
   nvalue               int                  not null default 0,
   ddatelastcheck       datetime             null,
   constraint pk_ci_risk_schedule primary key  (fkrisk)
)
go


/*==============================================================*/
/* Table: ci_solution                                           */
/*==============================================================*/
create table ci_solution (
   fkcontext            numeric              not null,
   fkcategory           numeric              null,
   tproblem             text                 null,
   tsolution            text                 null,
   tkeywords            text                 null,
   constraint pk_ci_solution primary key  (fkcontext)
)
go


/*==============================================================*/
/* Table: isms_acl                                              */
/*==============================================================*/
create table isms_acl (
   pkacl                numeric              identity,
   fkparent             numeric              null,
   sdescription         varchar(256)         null,
   stag                 varchar(256)         not null,
   sname                varchar(256)         null,
   constraint pk_isms_acl primary key  (pkacl)
)
go


/*==============================================================*/
/* Index: fkparent_fk                                           */
/*==============================================================*/
create   index fkparent_fk on isms_acl (
fkparent
)
go


/*==============================================================*/
/* Table: isms_audit_log                                        */
/*==============================================================*/
create table isms_audit_log (
   ddate                datetime             not null,
   suser                varchar(256)         null,
   naction              int                  null default 0,
   starget              varchar(256)         null,
   tdescription         text                 null
)
go


/*==============================================================*/
/* Table: isms_config                                           */
/*==============================================================*/
create table isms_config (
   pkconfig             int                  not null,
   svalue               varchar(256)         null,
   constraint pk_isms_config primary key  (pkconfig)
)
go


/*==============================================================*/
/* Table: isms_context                                          */
/*==============================================================*/
create table isms_context (
   pkcontext            numeric              identity,
   ntype                int                  not null default 0,
   nstate               int                  null default 0,
   constraint pk_isms_context primary key  (pkcontext)
)
go


/*==============================================================*/
/* Table: isms_context_classification                           */
/*==============================================================*/
create table isms_context_classification (
   pkclassification     numeric              identity,
   ncontexttype         int                  not null default 0,
   sname                varchar(256)         not null,
   nclassificationtype  int                  not null default 0,
   constraint pk_isms_context_classification primary key  (pkclassification)
)
go


/*==============================================================*/
/* Table: isms_context_date                                     */
/*==============================================================*/
create table isms_context_date (
   fkcontext            numeric              not null,
   naction              int                  not null default 0,
   ddate                datetime             not null,
   nuserid              int                  not null,
   susername            varchar(256)         not null,
   constraint pk_isms_context_date primary key  (fkcontext, naction)
)
go


/*==============================================================*/
/* Index: fkcontext_fk                                          */
/*==============================================================*/
create   index fkcontext_fk on isms_context_date (
fkcontext
)
go


/*==============================================================*/
/* Table: isms_context_history                                  */
/*==============================================================*/
create table isms_context_history (
   pkid                 numeric              identity,
   fkcontext            numeric              null,
   stype                varchar(256)         not null,
   ddate                datetime             not null,
   nvalue               float                not null default 0,
   constraint pk_isms_context_history primary key  (pkid)
)
go


/*==============================================================*/
/* Index: fkcontext_fk                                          */
/*==============================================================*/
create   index fkcontext_fk on isms_context_history (
fkcontext
)
go


/*==============================================================*/
/* Table: isms_policy                                           */
/*==============================================================*/
create table isms_policy (
   fkcontext            numeric              not null,
   tdescription         text                 not null,
   constraint pk_isms_policy primary key  (fkcontext)
)
go


/*==============================================================*/
/* Table: isms_profile                                          */
/*==============================================================*/
create table isms_profile (
   fkcontext            numeric              not null,
   sname                varchar(256)         not null,
   nid                  int                  null default 0,
   constraint pk_isms_profile primary key  (fkcontext)
)
go


/*==============================================================*/
/* Table: isms_profile_acl                                      */
/*==============================================================*/
create table isms_profile_acl (
   fkprofile            numeric              not null,
   fkacl                numeric              not null,
   constraint pk_isms_profile_acl primary key  (fkprofile, fkacl)
)
go


/*==============================================================*/
/* Index: fkacl_fk                                              */
/*==============================================================*/
create   index fkacl_fk on isms_profile_acl (
fkacl
)
go


/*==============================================================*/
/* Index: fkprofile_fk                                          */
/*==============================================================*/
create   index fkprofile_fk on isms_profile_acl (
fkprofile
)
go


/*==============================================================*/
/* Table: isms_scope                                            */
/*==============================================================*/
create table isms_scope (
   fkcontext            numeric              not null,
   tdescription         text                 not null,
   constraint pk_isms_scope primary key  (fkcontext)
)
go


/*==============================================================*/
/* Table: isms_solicitor                                        */
/*==============================================================*/
create table isms_solicitor (
   fkuser               numeric              not null,
   fksolicitor          numeric              not null,
   ddatebegin           datetime             null,
   ddatefinish          datetime             null,
   constraint pk_isms_solicitor primary key  (fkuser, fksolicitor)
)
go


/*==============================================================*/
/* Index: fksolicitor_fk                                        */
/*==============================================================*/
create   index fksolicitor_fk on isms_solicitor (
fksolicitor
)
go


/*==============================================================*/
/* Index: fkuser_fk                                             */
/*==============================================================*/
create   index fkuser_fk on isms_solicitor (
fkuser
)
go


/*==============================================================*/
/* Table: isms_user                                             */
/*==============================================================*/
create table isms_user (
   fkcontext            numeric              not null,
   fkprofile            numeric              not null,
   sname                varchar(256)         not null,
   slogin               varchar(256)         not null,
   spassword            varchar(256)         null,
   semail               varchar(256)         null,
   nip                  int                  null default 0,
   nlanguage            int                  null default 0,
   dlastlogin           datetime             null,
   nwronglogonattempts  int                  null default 0,
   bisblocked           int                  null default 0
      constraint ckc_bisblocked_isms_use check (bisblocked is null or (bisblocked between 0 and 1 )),
   bmustchangepassword  int                  null default 0
      constraint ckc_bmustchangepasswo_isms_use check (bmustchangepassword is null or (bmustchangepassword between 0 and 1 )),
   constraint pk_isms_user primary key  (fkcontext)
)
go


/*==============================================================*/
/* Index: fkprofile_fk                                          */
/*==============================================================*/
create   index fkprofile_fk on isms_user (
fkprofile
)
go


/*==============================================================*/
/* Table: isms_user_password_history                            */
/*==============================================================*/
create table isms_user_password_history (
   fkuser               numeric              null,
   spassword            varchar(256)         not null,
   ddatepasswordchanged datetime             not null
)
go


/*==============================================================*/
/* Table: isms_user_preference                                  */
/*==============================================================*/
create table isms_user_preference (
   fkuser               numeric              not null,
   spreference          varchar(256)         not null,
   tvalue               text                 null,
   constraint pk_isms_user_preference primary key  (fkuser, spreference)
)
go


/*==============================================================*/
/* Index: fkuser_fk                                             */
/*==============================================================*/
create   index fkuser_fk on isms_user_preference (
fkuser
)
go


/*==============================================================*/
/* Table: pm_document                                           */
/*==============================================================*/
create table pm_document (
   fkcontext            numeric              not null,
   fkclassification     numeric              null,
   fkcurrentversion     numeric              null,
   fkparent             numeric              null,
   fkauthor             numeric              null,
   fkmainapprover       numeric              null,
   sname                varchar(256)         not null,
   tdescription         text                 null,
   ntype                int                  not null default 0,
   skeywords            varchar(256)         null,
   ddateproduction      datetime             null,
   ddeadline            datetime             null,
   bflagdeadlinealert   int                  null default 0
      constraint ckc_bflagdeadlinealer_pm_docum check (bflagdeadlinealert is null or (bflagdeadlinealert between 0 and 1 )),
   bflagdeadlineexpired int                  null default 0
      constraint ckc_bflagdeadlineexpi_pm_docum check (bflagdeadlineexpired is null or (bflagdeadlineexpired between 0 and 1 )),
   ndaysbefore          int                  null default 0,
   bhasapproved         int                  null default 0
      constraint ckc_bhasapproved_pm_docum check (bhasapproved is null or (bhasapproved between 0 and 1 )),
   constraint pk_pm_document primary key  (fkcontext)
)
go


/*==============================================================*/
/* Index: fkclassification_fk                                   */
/*==============================================================*/
create   index fkclassification_fk on pm_document (
fkclassification
)
go


/*==============================================================*/
/* Index: fkauthor_fk                                           */
/*==============================================================*/
create   index fkauthor_fk on pm_document (
fkauthor
)
go


/*==============================================================*/
/* Index: fkparent_fk                                           */
/*==============================================================*/
create   index fkparent_fk on pm_document (
fkparent
)
go


/*==============================================================*/
/* Index: fkmainappover_fk                                      */
/*==============================================================*/
create   index fkmainappover_fk on pm_document (
fkmainapprover
)
go


/*==============================================================*/
/* Table: pm_document_revision_history                          */
/*==============================================================*/
create table pm_document_revision_history (
   fkdocument           numeric              not null,
   ddaterevision        datetime             not null,
   sjustification       text                 null,
   bnewversion          int                  null default 0
      constraint ckc_bnewversion_pm_docum check (bnewversion is null or (bnewversion between 0 and 1 )),
   constraint pk_pm_document_revision_histor primary key  (fkdocument, ddaterevision)
)
go


/*==============================================================*/
/* Index: fkdocument_fk                                         */
/*==============================================================*/
create   index fkdocument_fk on pm_document_revision_history (
fkdocument
)
go


/*==============================================================*/
/* Table: pm_doc_approvers                                      */
/*==============================================================*/
create table pm_doc_approvers (
   fkdocument           numeric              not null,
   fkuser               numeric              not null,
   bhasapproved         int                  null default 0
      constraint ckc_bhasapproved_pm_doc_a check (bhasapproved is null or (bhasapproved between 0 and 1 )),
   bmanual              int                  null default 0
      constraint ckc_bmanual_pm_doc_a check (bmanual is null or (bmanual between 0 and 1 )),
   bcontextapprover     int                  null default 0
      constraint ckc_bcontextapprover_pm_doc_a check (bcontextapprover is null or (bcontextapprover between 0 and 1 )),
   constraint pk_pm_doc_approvers primary key  (fkdocument, fkuser)
)
go


/*==============================================================*/
/* Index: fkdocument_fk                                         */
/*==============================================================*/
create   index fkdocument_fk on pm_doc_approvers (
fkdocument
)
go


/*==============================================================*/
/* Index: fkuser_fk                                             */
/*==============================================================*/
create   index fkuser_fk on pm_doc_approvers (
fkuser
)
go


/*==============================================================*/
/* Table: pm_doc_context                                        */
/*==============================================================*/
create table pm_doc_context (
   fkcontext            numeric              not null,
   fkdocument           numeric              not null,
   constraint pk_pm_doc_context primary key  (fkcontext, fkdocument)
)
go


/*==============================================================*/
/* Index: fkdocument_fk                                         */
/*==============================================================*/
create   index fkdocument_fk on pm_doc_context (
fkdocument
)
go


/*==============================================================*/
/* Index: fkcontext_fk                                          */
/*==============================================================*/
create   index fkcontext_fk on pm_doc_context (
fkcontext
)
go


/*==============================================================*/
/* Table: pm_doc_instance                                       */
/*==============================================================*/
create table pm_doc_instance (
   fkcontext            numeric              not null,
   fkdocument           numeric              null,
   nmajorversion        int                  null default 0,
   nrevisionversion     int                  null default 0,
   trevisionjustification text                 null,
   spath                varchar(256)         null,
   tmodifycomment       text                 null,
   dbeginproduction     datetime             null,
   dendproduction       datetime             null,
   sfilename            varchar(256)         null,
   bislink              int                  null default 0
      constraint ckc_bislink_pm_doc_i check (bislink is null or (bislink between 0 and 1 )),
   slink                varchar(256)         null,
   dbeginapprover       datetime             null,
   dbeginrevision       datetime             null,
   nfilesize            int                  null default 0,
   constraint pk_pm_doc_instance primary key  (fkcontext)
)
go


/*==============================================================*/
/* Index: fkdocument_fk                                         */
/*==============================================================*/
create   index fkdocument_fk on pm_doc_instance (
fkdocument
)
go


/*==============================================================*/
/* Table: pm_doc_readers                                        */
/*==============================================================*/
create table pm_doc_readers (
   fkuser               numeric              not null,
   fkdocument           numeric              not null,
   bmanual              int                  null default 0
      constraint ckc_bmanual_pm_doc_r check (bmanual is null or (bmanual between 0 and 1 )),
   bdenied              int                  null default 0
      constraint ckc_bdenied_pm_doc_r check (bdenied is null or (bdenied between 0 and 1 )),
   bhasread             int                  not null default 0
      constraint ckc_bhasread_pm_doc_r check (bhasread between 0 and 1),
   constraint pk_pm_doc_readers primary key  (fkuser, fkdocument)
)
go


/*==============================================================*/
/* Index: fkdocument_fk                                         */
/*==============================================================*/
create   index fkdocument_fk on pm_doc_readers (
fkdocument
)
go


/*==============================================================*/
/* Index: fkuser_fk                                             */
/*==============================================================*/
create   index fkuser_fk on pm_doc_readers (
fkuser
)
go


/*==============================================================*/
/* Table: pm_doc_read_history                                   */
/*==============================================================*/
create table pm_doc_read_history (
   ddate                datetime             not null,
   fkinstance           numeric              not null,
   fkuser               numeric              not null,
   constraint pk_pm_doc_read_history primary key  (ddate, fkinstance, fkuser)
)
go


/*==============================================================*/
/* Index: fkuser_fk                                             */
/*==============================================================*/
create   index fkuser_fk on pm_doc_read_history (
fkuser
)
go


/*==============================================================*/
/* Table: pm_doc_reference                                      */
/*==============================================================*/
create table pm_doc_reference (
   pkreference          numeric              identity,
   fkdocument           numeric              null,
   sname                varchar(256)         not null,
   slink                varchar(256)         null,
   dcreationdate        datetime             null,
   constraint pk_pm_doc_reference primary key  (pkreference)
)
go


/*==============================================================*/
/* Index: fkdocument_fk                                         */
/*==============================================================*/
create   index fkdocument_fk on pm_doc_reference (
fkdocument
)
go


/*==============================================================*/
/* Table: pm_doc_registers                                      */
/*==============================================================*/
create table pm_doc_registers (
   fkregister           numeric              not null,
   fkdocument           numeric              not null,
   constraint pk_pm_doc_registers primary key  (fkregister, fkdocument)
)
go


/*==============================================================*/
/* Index: fkregister_fk                                         */
/*==============================================================*/
create   index fkregister_fk on pm_doc_registers (
fkregister
)
go


/*==============================================================*/
/* Index: fkdocument_fk                                         */
/*==============================================================*/
create   index fkdocument_fk on pm_doc_registers (
fkdocument
)
go


/*==============================================================*/
/* Table: pm_instance_comment                                   */
/*==============================================================*/
create table pm_instance_comment (
   pkcomment            numeric              identity,
   fkinstance           numeric              null,
   fkuser               numeric              null,
   ddate                datetime             not null,
   tcomment             text                 not null,
   constraint pk_pm_instance_comment primary key  (pkcomment)
)
go


/*==============================================================*/
/* Index: fkinstance_fk                                         */
/*==============================================================*/
create   index fkinstance_fk on pm_instance_comment (
fkinstance
)
go


/*==============================================================*/
/* Index: fkuser_fk                                             */
/*==============================================================*/
create   index fkuser_fk on pm_instance_comment (
fkuser
)
go


/*==============================================================*/
/* Table: pm_instance_content                                   */
/*==============================================================*/
create table pm_instance_content (
   fkinstance           numeric              not null,
   tcontent             text                 null,
   constraint pk_pm_instance_content primary key  (fkinstance)
)
go


/*==============================================================*/
/* Table: pm_process_user                                       */
/*==============================================================*/
create table pm_process_user (
   fkprocess            numeric              not null,
   fkuser               numeric              not null,
   constraint pk_pm_process_user primary key  (fkprocess, fkuser)
)
go


/*==============================================================*/
/* Index: fkuser_fk                                             */
/*==============================================================*/
create   index fkuser_fk on pm_process_user (
fkuser
)
go


/*==============================================================*/
/* Index: fkprocess_fk                                          */
/*==============================================================*/
create   index fkprocess_fk on pm_process_user (
fkprocess
)
go


/*==============================================================*/
/* Table: pm_register                                           */
/*==============================================================*/
create table pm_register (
   fkcontext            numeric              not null,
   fkclassification     numeric              null,
   fkdocument           numeric              null,
   fkresponsible        numeric              null,
   sname                varchar(256)         not null,
   nperiod              int                  not null default 0,
   nvalue               int                  not null default 0,
   sstorageplace        varchar(256)         null,
   sstoragetype         varchar(256)         null,
   sindexingtype        varchar(256)         null,
   sdisposition         varchar(256)         null,
   sprotectionrequirements varchar(256)         null,
   sorigin              varchar(256)         null,
   constraint pk_pm_register primary key  (fkcontext)
)
go


/*==============================================================*/
/* Index: fkclassification_fk                                   */
/*==============================================================*/
create   index fkclassification_fk on pm_register (
fkclassification
)
go


/*==============================================================*/
/* Index: fkdocument_fk                                         */
/*==============================================================*/
create   index fkdocument_fk on pm_register (
fkdocument
)
go


/*==============================================================*/
/* Index: fkresponsible_fk                                      */
/*==============================================================*/
create   index fkresponsible_fk on pm_register (
fkresponsible
)
go


/*==============================================================*/
/* Table: pm_register_readers                                   */
/*==============================================================*/
create table pm_register_readers (
   fkuser               numeric              not null,
   fkregister           numeric              not null,
   bmanual              int                  null default 0
      constraint ckc_bmanual_pm_regis check (bmanual is null or (bmanual between 0 and 1 )),
   bdenied              int                  null default 0
      constraint ckc_bdenied_pm_regis check (bdenied is null or (bdenied between 0 and 1 )),
   constraint pk_pm_register_readers primary key  (fkuser, fkregister)
)
go


/*==============================================================*/
/* Index: fkregister_fk                                         */
/*==============================================================*/
create   index fkregister_fk on pm_register_readers (
fkregister
)
go


/*==============================================================*/
/* Index: fkuser_fk                                             */
/*==============================================================*/
create   index fkuser_fk on pm_register_readers (
fkuser
)
go


/*==============================================================*/
/* Table: pm_template                                           */
/*==============================================================*/
create table pm_template (
   fkcontext            numeric              not null,
   sname                varchar(256)         not null,
   ncontexttype         int                  null default 0,
   spath                varchar(256)         null,
   sfilename            varchar(256)         null,
   tdescription         text                 null,
   skeywords            varchar(256)         null,
   nfilesize            int                  null default 0,
   constraint pk_pm_template primary key  (fkcontext)
)
go


/*==============================================================*/
/* Table: pm_template_best_practice                             */
/*==============================================================*/
create table pm_template_best_practice (
   fktemplate           numeric              not null,
   fkbestpractice       numeric              not null,
   constraint pk_pm_template_best_practice primary key  (fktemplate, fkbestpractice)
)
go


/*==============================================================*/
/* Index: fktemplate_fk                                         */
/*==============================================================*/
create   index fktemplate_fk on pm_template_best_practice (
fktemplate
)
go


/*==============================================================*/
/* Index: fkbestpractice_fk                                     */
/*==============================================================*/
create   index fkbestpractice_fk on pm_template_best_practice (
fkbestpractice
)
go


/*==============================================================*/
/* Table: pm_template_content                                   */
/*==============================================================*/
create table pm_template_content (
   fkcontext            numeric              not null,
   tcontent             text                 null,
   constraint pk_pm_template_content primary key  (fkcontext)
)
go


/*==============================================================*/
/* Table: rm_area                                               */
/*==============================================================*/
create table rm_area (
   fkcontext            numeric              not null,
   fkpriority           numeric              null,
   fktype               numeric              null,
   fkparent             numeric              null,
   fkresponsible        numeric              not null,
   sname                varchar(256)         not null,
   tdescription         text                 null,
   sdocument            varchar(256)         null,
   nvalue               float                null default 0,
   constraint pk_rm_area primary key  (fkcontext)
)
go


/*==============================================================*/
/* Index: fkparent_fk                                           */
/*==============================================================*/
create   index fkparent_fk on rm_area (
fkparent
)
go


/*==============================================================*/
/* Index: fkresponsible_fk                                      */
/*==============================================================*/
create   index fkresponsible_fk on rm_area (
fkresponsible
)
go


/*==============================================================*/
/* Index: fktype_fk                                             */
/*==============================================================*/
create   index fktype_fk on rm_area (
fktype
)
go


/*==============================================================*/
/* Index: fkpriority_fk                                         */
/*==============================================================*/
create   index fkpriority_fk on rm_area (
fkpriority
)
go


/*==============================================================*/
/* Table: rm_asset                                              */
/*==============================================================*/
create table rm_asset (
   fkcontext            numeric              not null,
   fkcategory           numeric              not null,
   fkresponsible        numeric              not null,
   fksecurityresponsible numeric              not null,
   sname                varchar(256)         not null,
   tdescription         text                 null,
   sdocument            varchar(256)         null,
   ncost                float                null default 0,
   nvalue               float                null default 0,
   blegality            int                  null default 0
      constraint ckc_blegality_rm_asset check (blegality is null or (blegality between 0 and 1 )),
   tjustification       text                 null,
   constraint pk_rm_asset primary key  (fkcontext)
)
go


/*==============================================================*/
/* Index: fkresponsible_fk                                      */
/*==============================================================*/
create   index fkresponsible_fk on rm_asset (
fkresponsible
)
go


/*==============================================================*/
/* Index: fksecurityresponsible_fk                              */
/*==============================================================*/
create   index fksecurityresponsible_fk on rm_asset (
fksecurityresponsible
)
go


/*==============================================================*/
/* Index: fkcategory_fk                                         */
/*==============================================================*/
create   index fkcategory_fk on rm_asset (
fkcategory
)
go


/*==============================================================*/
/* Table: rm_asset_asset                                        */
/*==============================================================*/
create table rm_asset_asset (
   fkasset              numeric              not null,
   fkdependent          numeric              not null,
   constraint pk_rm_asset_asset primary key  (fkasset, fkdependent)
)
go


/*==============================================================*/
/* Index: fkasset_fk                                            */
/*==============================================================*/
create   index fkasset_fk on rm_asset_asset (
fkasset
)
go


/*==============================================================*/
/* Index: fkdependent_fk                                        */
/*==============================================================*/
create   index fkdependent_fk on rm_asset_asset (
fkdependent
)
go


/*==============================================================*/
/* Table: rm_asset_value                                        */
/*==============================================================*/
create table rm_asset_value (
   fkasset              numeric              not null,
   fkvaluename          numeric              not null,
   fkparametername      numeric              not null,
   tjustification       text                 null,
   constraint pk_rm_asset_value primary key  (fkasset, fkvaluename, fkparametername)
)
go


/*==============================================================*/
/* Index: fkasset_fk                                            */
/*==============================================================*/
create   index fkasset_fk on rm_asset_value (
fkasset
)
go


/*==============================================================*/
/* Index: fkvaluename_fk                                        */
/*==============================================================*/
create   index fkvaluename_fk on rm_asset_value (
fkvaluename
)
go


/*==============================================================*/
/* Index: fkparametername_fk                                    */
/*==============================================================*/
create   index fkparametername_fk on rm_asset_value (
fkparametername
)
go


/*==============================================================*/
/* Table: rm_best_practice                                      */
/*==============================================================*/
create table rm_best_practice (
   fkcontext            numeric              not null,
   fksectionbestpractice numeric              not null,
   sname                varchar(256)         not null,
   tdescription         text                 null,
   ncontroltype         int                  null default 0,
   sclassification      varchar(256)         null,
   timplementationguide text                 null,
   tmetric              text                 null,
   sdocument            varchar(256)         null,
   tjustification       text                 null,
   constraint pk_rm_best_practice primary key  (fkcontext)
)
go


/*==============================================================*/
/* Index: fksectionbestpractice_fk                              */
/*==============================================================*/
create   index fksectionbestpractice_fk on rm_best_practice (
fksectionbestpractice
)
go


/*==============================================================*/
/* Table: rm_best_practice_event                                */
/*==============================================================*/
create table rm_best_practice_event (
   fkcontext            numeric              not null,
   fkbestpractice       numeric              null,
   fkevent              numeric              null,
   constraint pk_rm_best_practice_event primary key  (fkcontext)
)
go


/*==============================================================*/
/* Index: fkbestpractice_fk                                     */
/*==============================================================*/
create   index fkbestpractice_fk on rm_best_practice_event (
fkbestpractice
)
go


/*==============================================================*/
/* Index: fkevent_fk                                            */
/*==============================================================*/
create   index fkevent_fk on rm_best_practice_event (
fkevent
)
go


/*==============================================================*/
/* Table: rm_best_practice_standard                             */
/*==============================================================*/
create table rm_best_practice_standard (
   fkcontext            numeric              not null,
   fkstandard           numeric              null,
   fkbestpractice       numeric              null,
   constraint pk_rm_best_practice_standard primary key  (fkcontext)
)
go


/*==============================================================*/
/* Index: fkstandard_fk                                         */
/*==============================================================*/
create   index fkstandard_fk on rm_best_practice_standard (
fkstandard
)
go


/*==============================================================*/
/* Index: fkbestpractice_fk                                     */
/*==============================================================*/
create   index fkbestpractice_fk on rm_best_practice_standard (
fkbestpractice
)
go


/*==============================================================*/
/* Table: rm_category                                           */
/*==============================================================*/
create table rm_category (
   fkcontext            numeric              not null,
   fkparent             numeric              null,
   sname                varchar(256)         not null,
   tdescription         text                 null,
   constraint pk_rm_category primary key  (fkcontext)
)
go


/*==============================================================*/
/* Index: fkparent_fk                                           */
/*==============================================================*/
create   index fkparent_fk on rm_category (
fkparent
)
go


/*==============================================================*/
/* Table: rm_control                                            */
/*==============================================================*/
create table rm_control (
   fkcontext            numeric              not null,
   fktype               numeric              null,
   fkresponsible        numeric              not null,
   sname                varchar(256)         not null,
   tdescription         text                 null,
   sdocument            varchar(256)         null,
   sevidence            varchar(256)         null,
   ndaysbefore          int                  null default 0,
   bisactive            int                  null default 0
      constraint ckc_bisactive_rm_contr check (bisactive is null or (bisactive between 0 and 1 )),
   bflagimplalert       int                  null default 0
      constraint ckc_bflagimplalert_rm_contr check (bflagimplalert is null or (bflagimplalert between 0 and 1 )),
   bflagimplexpired     int                  null default 0
      constraint ckc_bflagimplexpired_rm_contr check (bflagimplexpired is null or (bflagimplexpired between 0 and 1 )),
   ddatedeadline        datetime             null,
   ddateimplemented     datetime             null,
   nimplementationstate int                  null default 0,
   bimplementationislate int                  null default 0
      constraint ckc_bimplementationis_rm_contr check (bimplementationislate is null or (bimplementationislate between 0 and 1 )),
   brevisionislate      int                  null default 0
      constraint ckc_brevisionislate_rm_contr check (brevisionislate is null or (brevisionislate between 0 and 1 )),
   btestislate          int                  null default 0
      constraint ckc_btestislate_rm_contr check (btestislate is null or (btestislate between 0 and 1 )),
   befficiencynotok     int                  null default 0
      constraint ckc_befficiencynotok_rm_contr check (befficiencynotok is null or (befficiencynotok between 0 and 1 )),
   btestnotok           int                  null default 0
      constraint ckc_btestnotok_rm_contr check (btestnotok is null or (btestnotok between 0 and 1 )),
   constraint pk_rm_control primary key  (fkcontext)
)
go


/*==============================================================*/
/* Index: fkresponsible_fk                                      */
/*==============================================================*/
create   index fkresponsible_fk on rm_control (
fkresponsible
)
go


/*==============================================================*/
/* Index: fktype_fk                                             */
/*==============================================================*/
create   index fktype_fk on rm_control (
fktype
)
go


/*==============================================================*/
/* Table: rm_control_best_practice                              */
/*==============================================================*/
create table rm_control_best_practice (
   fkcontext            numeric              not null,
   fkbestpractice       numeric              null,
   fkcontrol            numeric              null,
   constraint pk_rm_control_best_practice primary key  (fkcontext)
)
go


/*==============================================================*/
/* Index: fkbestpractice_fk                                     */
/*==============================================================*/
create   index fkbestpractice_fk on rm_control_best_practice (
fkbestpractice
)
go


/*==============================================================*/
/* Index: fkcontrol_fk                                          */
/*==============================================================*/
create   index fkcontrol_fk on rm_control_best_practice (
fkcontrol
)
go


/*==============================================================*/
/* Table: rm_control_cost                                       */
/*==============================================================*/
create table rm_control_cost (
   fkcontrol            numeric              not null,
   ncost1               float                null default 0,
   ncost2               float                null default 0,
   ncost3               float                null default 0,
   ncost4               float                null default 0,
   ncost5               float                null default 0,
   constraint pk_rm_control_cost primary key  (fkcontrol)
)
go


/*==============================================================*/
/* Table: rm_control_efficiency_history                         */
/*==============================================================*/
create table rm_control_efficiency_history (
   fkcontrol            numeric              not null,
   ddatetodo            datetime             not null,
   ddateaccomplishment  datetime             null,
   nrealefficiency      int                  null default 0,
   nexpectedefficiency  int                  null default 0,
   bincident            int                  null default 0
      constraint ckc_bincident_rm_contr check (bincident is null or (bincident between 0 and 1 )),
   constraint pk_rm_control_efficiency_histo primary key  (fkcontrol, ddatetodo)
)
go


/*==============================================================*/
/* Index: fkcontrol_fk                                          */
/*==============================================================*/
create   index fkcontrol_fk on rm_control_efficiency_history (
fkcontrol
)
go


/*==============================================================*/
/* Table: rm_control_test_history                               */
/*==============================================================*/
create table rm_control_test_history (
   fkcontrol            numeric              not null,
   ddatetodo            datetime             not null,
   ddateaccomplishment  datetime             null,
   btestedvalue         int                  null default 0
      constraint ckc_btestedvalue_rm_contr check (btestedvalue is null or (btestedvalue between 0 and 1 )),
   tdescription         text                 null,
   constraint pk_rm_control_test_history primary key  (fkcontrol, ddatetodo)
)
go


/*==============================================================*/
/* Index: fkcontrol_fk                                          */
/*==============================================================*/
create   index fkcontrol_fk on rm_control_test_history (
fkcontrol
)
go


/*==============================================================*/
/* Table: rm_event                                              */
/*==============================================================*/
create table rm_event (
   fkcontext            numeric              not null,
   fktype               numeric              null,
   fkcategory           numeric              not null,
   sdescription         varchar(256)         not null,
   tobservation         text                 null,
   bpropagate           int                  null default 0
      constraint ckc_bpropagate_rm_event check (bpropagate is null or (bpropagate between 0 and 1 )),
   constraint pk_rm_event primary key  (fkcontext)
)
go


/*==============================================================*/
/* Index: fkcategory_fk                                         */
/*==============================================================*/
create   index fkcategory_fk on rm_event (
fkcategory
)
go


/*==============================================================*/
/* Index: fktype_fk                                             */
/*==============================================================*/
create   index fktype_fk on rm_event (
fktype
)
go


/*==============================================================*/
/* Table: rm_parameter_name                                     */
/*==============================================================*/
create table rm_parameter_name (
   pkparametername      numeric              identity,
   sname                varchar(256)         not null,
   constraint pk_rm_parameter_name primary key  (pkparametername)
)
go


/*==============================================================*/
/* Table: rm_parameter_value_name                               */
/*==============================================================*/
create table rm_parameter_value_name (
   pkvaluename          numeric              identity,
   nvalue               int                  null default 1
      constraint ckc_nvalue_rm_param check (nvalue is null or (nvalue between 1 and 5 )),
   simportance          varchar(256)         null,
   simpact              varchar(256)         null,
   sriskprobability     varchar(256)         null,
   constraint pk_rm_parameter_value_name primary key  (pkvaluename)
)
go


/*==============================================================*/
/* Table: rm_process                                            */
/*==============================================================*/
create table rm_process (
   fkcontext            numeric              not null,
   fkpriority           numeric              null,
   fktype               numeric              null,
   fkarea               numeric              not null,
   fkresponsible        numeric              not null,
   sname                varchar(256)         not null,
   tdescription         text                 null,
   sdocument            varchar(256)         null,
   nvalue               float                null default 0,
   tjustification       text                 null,
   constraint pk_rm_process primary key  (fkcontext)
)
go


/*==============================================================*/
/* Index: fkarea_fk                                             */
/*==============================================================*/
create   index fkarea_fk on rm_process (
fkarea
)
go


/*==============================================================*/
/* Index: fkresponsible_fk                                      */
/*==============================================================*/
create   index fkresponsible_fk on rm_process (
fkresponsible
)
go


/*==============================================================*/
/* Index: fktype_fk                                             */
/*==============================================================*/
create   index fktype_fk on rm_process (
fktype
)
go


/*==============================================================*/
/* Index: fkpriority_fk                                         */
/*==============================================================*/
create   index fkpriority_fk on rm_process (
fkpriority
)
go


/*==============================================================*/
/* Table: rm_process_asset                                      */
/*==============================================================*/
create table rm_process_asset (
   fkcontext            numeric              not null,
   fkprocess            numeric              null,
   fkasset              numeric              null,
   constraint pk_rm_process_asset primary key  (fkcontext)
)
go


/*==============================================================*/
/* Index: fkasset_fk                                            */
/*==============================================================*/
create   index fkasset_fk on rm_process_asset (
fkasset
)
go


/*==============================================================*/
/* Index: fkprocess_fk                                          */
/*==============================================================*/
create   index fkprocess_fk on rm_process_asset (
fkprocess
)
go


/*==============================================================*/
/* Table: rm_rc_parameter_value_name                            */
/*==============================================================*/
create table rm_rc_parameter_value_name (
   pkrcvaluename        numeric              identity,
   nvalue               int                  null default 0,
   srcprobability       varchar(256)         null,
   scontrolimpact       varchar(256)         null,
   constraint pk_rm_rc_parameter_value_name primary key  (pkrcvaluename)
)
go


/*==============================================================*/
/* Table: rm_risk                                               */
/*==============================================================*/
create table rm_risk (
   fkcontext            numeric              not null,
   fkprobabilityvaluename numeric              null,
   fktype               numeric              null,
   fkevent              numeric              null,
   fkasset              numeric              not null,
   sname                varchar(256)         not null,
   tdescription         text                 null,
   nvalue               float                null default 0,
   nvalueresidual       float                null default 0,
   tjustification       text                 null,
   nacceptmode          int                  null default 0,
   nacceptstate         int                  null default 0,
   sacceptjustification varchar(256)         null,
   ncost                float                null default 0,
   constraint pk_rm_risk primary key  (fkcontext)
)
go


/*==============================================================*/
/* Index: fkasset_fk                                            */
/*==============================================================*/
create   index fkasset_fk on rm_risk (
fkasset
)
go


/*==============================================================*/
/* Index: fkevent_fk                                            */
/*==============================================================*/
create   index fkevent_fk on rm_risk (
fkevent
)
go


/*==============================================================*/
/* Index: fkprobabilityvaluename_fk                             */
/*==============================================================*/
create   index fkprobabilityvaluename_fk on rm_risk (
fkprobabilityvaluename
)
go


/*==============================================================*/
/* Index: fktype_fk                                             */
/*==============================================================*/
create   index fktype_fk on rm_risk (
fktype
)
go


/*==============================================================*/
/* Table: rm_risk_control                                       */
/*==============================================================*/
create table rm_risk_control (
   fkcontext            numeric              not null,
   fkprobabilityvaluename numeric              null,
   fkrisk               numeric              null,
   fkcontrol            numeric              null,
   tjustification       text                 null,
   constraint pk_rm_risk_control primary key  (fkcontext)
)
go


/*==============================================================*/
/* Index: fkrisk_fk                                             */
/*==============================================================*/
create   index fkrisk_fk on rm_risk_control (
fkrisk
)
go


/*==============================================================*/
/* Index: fkcontrol_fk                                          */
/*==============================================================*/
create   index fkcontrol_fk on rm_risk_control (
fkcontrol
)
go


/*==============================================================*/
/* Index: fkprobabilityvaluename_fk                             */
/*==============================================================*/
create   index fkprobabilityvaluename_fk on rm_risk_control (
fkprobabilityvaluename
)
go


/*==============================================================*/
/* Table: rm_risk_control_value                                 */
/*==============================================================*/
create table rm_risk_control_value (
   fkriskcontrol        numeric              not null,
   fkparametername      numeric              not null,
   fkrcvaluename        numeric              not null,
   tjustification       text                 null,
   constraint pk_rm_risk_control_value primary key  (fkriskcontrol, fkparametername, fkrcvaluename)
)
go


/*==============================================================*/
/* Index: fkriskcontrol_fk                                      */
/*==============================================================*/
create   index fkriskcontrol_fk on rm_risk_control_value (
fkriskcontrol
)
go


/*==============================================================*/
/* Index: fkparametername_fk                                    */
/*==============================================================*/
create   index fkparametername_fk on rm_risk_control_value (
fkparametername
)
go


/*==============================================================*/
/* Index: fkrcvaluename_fk                                      */
/*==============================================================*/
create   index fkrcvaluename_fk on rm_risk_control_value (
fkrcvaluename
)
go


/*==============================================================*/
/* Table: rm_risk_limits                                        */
/*==============================================================*/
create table rm_risk_limits (
   fkcontext            numeric              not null,
   nlow                 float                not null,
   nhigh                float                not null,
   constraint pk_rm_risk_limits primary key  (fkcontext)
)
go


/*==============================================================*/
/* Table: rm_risk_value                                         */
/*==============================================================*/
create table rm_risk_value (
   fkrisk               numeric              not null,
   fkvaluename          numeric              not null,
   fkparametername      numeric              not null,
   tjustification       text                 null,
   constraint pk_rm_risk_value primary key  (fkrisk, fkvaluename, fkparametername)
)
go


/*==============================================================*/
/* Index: fkrisk_fk                                             */
/*==============================================================*/
create   index fkrisk_fk on rm_risk_value (
fkrisk
)
go


/*==============================================================*/
/* Index: fkvaluename_fk                                        */
/*==============================================================*/
create   index fkvaluename_fk on rm_risk_value (
fkvaluename
)
go


/*==============================================================*/
/* Index: fkparametername_fk                                    */
/*==============================================================*/
create   index fkparametername_fk on rm_risk_value (
fkparametername
)
go


/*==============================================================*/
/* Table: rm_section_best_practice                              */
/*==============================================================*/
create table rm_section_best_practice (
   fkcontext            numeric              not null,
   fkparent             numeric              null,
   sname                varchar(256)         not null,
   constraint pk_rm_section_best_practice primary key  (fkcontext)
)
go


/*==============================================================*/
/* Index: fkparent_fk                                           */
/*==============================================================*/
create   index fkparent_fk on rm_section_best_practice (
fkparent
)
go


/*==============================================================*/
/* Table: rm_standard                                           */
/*==============================================================*/
create table rm_standard (
   fkcontext            numeric              not null,
   sname                varchar(256)         not null,
   tdescription         text                 null,
   tapplication         text                 null,
   tobjective           text                 null,
   constraint pk_rm_standard primary key  (fkcontext)
)
go


/*==============================================================*/
/* Table: wkf_alert                                             */
/*==============================================================*/
create table wkf_alert (
   pkalert              numeric              identity,
   fkcontext            numeric              null,
   fkreceiver           numeric              null,
   fkcreator            numeric              null,
   tjustification       text                 null,
   ntype                int                  null default 0,
   bemailsent           int                  null default 0
      constraint ckc_bemailsent_wkf_aler check (bemailsent is null or (bemailsent between 0 and 1 )),
   ddate                datetime             not null,
   constraint pk_wkf_alert primary key  (pkalert)
)
go


/*==============================================================*/
/* Index: fkreceiver_fk                                         */
/*==============================================================*/
create   index fkreceiver_fk on wkf_alert (
fkreceiver
)
go


/*==============================================================*/
/* Index: fkcontext_fk                                          */
/*==============================================================*/
create   index fkcontext_fk on wkf_alert (
fkcontext
)
go


/*==============================================================*/
/* Index: fkcreator_fk                                          */
/*==============================================================*/
create   index fkcreator_fk on wkf_alert (
fkcreator
)
go


/*==============================================================*/
/* Table: wkf_control_efficiency                                */
/*==============================================================*/
create table wkf_control_efficiency (
   fkcontrolefficiency  numeric              not null,
   nrealefficiency      int                  null default 0,
   nexpectedefficiency  int                  null default 0,
   smetric1             varchar(256)         null,
   smetric2             varchar(256)         null,
   smetric3             varchar(256)         null,
   smetric4             varchar(256)         null,
   smetric5             varchar(256)         null,
   ddatenext            datetime             null,
   ddatelimit           datetime             null,
   nvalue               int                  null default 0,
   nperiod              int                  null default 0,
   bflagalert           int                  null default 0
      constraint ckc_bflagalert_wkf_cont2 check (bflagalert is null or (bflagalert between 0 and 1 )),
   constraint pk_wkf_control_efficiency primary key  (fkcontrolefficiency)
)
go


/*==============================================================*/
/* Table: wkf_control_test                                      */
/*==============================================================*/
create table wkf_control_test (
   fkcontroltest        numeric              not null,
   tdescription         text                 null,
   ddatenext            datetime             null,
   ddatelimit           datetime             null,
   nvalue               int                  null default 0,
   nperiod              int                  null default 0,
   bflagalert           int                  null default 0
      constraint ckc_bflagalert_wkf_cont check (bflagalert is null or (bflagalert between 0 and 1 )),
   constraint pk_wkf_control_test primary key  (fkcontroltest)
)
go


/*==============================================================*/
/* Table: wkf_doc_revision                                      */
/*==============================================================*/
create table wkf_doc_revision (
   fkdocrevision        numeric              not null,
   ndaysbefore          int                  null,
   ddatenext            datetime             null,
   ddatelimit           datetime             null,
   nvalue               int                  null default 0,
   nperiod              int                  null default 0,
   bflagalert           int                  null default 0
      constraint ckc_bflagalert_wkf_doc_ check (bflagalert is null or (bflagalert between 0 and 1 )),
   constraint pk_wkf_doc_revision primary key  (fkdocrevision)
)
go


/*==============================================================*/
/* Table: wkf_task                                              */
/*==============================================================*/
create table wkf_task (
   pktask               numeric              identity,
   fkcontext            numeric              null,
   fkcreator            numeric              not null,
   fkreceiver           numeric              not null,
   nactivity            int                  null default 0,
   bvisible             int                  null,
   bemailsent           int                  null default 0
      constraint ckc_bemailsent_wkf_task check (bemailsent is null or (bemailsent between 0 and 1 )),
   ddateaccomplished    datetime             null,
   ddatecreated         datetime             null,
   constraint pk_wkf_task primary key  (pktask)
)
go


/*==============================================================*/
/* Index: fkcreator_fk                                          */
/*==============================================================*/
create   index fkcreator_fk on wkf_task (
fkcreator
)
go


/*==============================================================*/
/* Index: fkreceiver_fk                                         */
/*==============================================================*/
create   index fkreceiver_fk on wkf_task (
fkreceiver
)
go


/*==============================================================*/
/* Index: fkcontext_fk                                          */
/*==============================================================*/
create   index fkcontext_fk on wkf_task (
fkcontext
)
go

CREATE VIEW context_history AS
SELECT cc.fkContext as context_id, cc.dDate as context_date_created, cc.sUserName as context_creator_name, cm.dDate as context_date_modified, cm.sUserName as context_modifier_name
FROM isms_context_date cc JOIN isms_context_date cm ON (cc.fkContext = cm.fkContext AND cc.nAction < cm.nAction)
GO

CREATE VIEW context_names AS
/*Usu�rio*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, t.sName as context_name FROM isms_context c JOIN isms_user t ON (c.pkContext = t.fkContext) UNION
/*Perfil*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, t.sName as context_name FROM isms_context c JOIN isms_profile t ON (c.pkContext = t.fkContext) UNION
/*�rea*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, t.sName as context_name FROM isms_context c JOIN rm_area t ON (c.pkContext = t.fkContext) UNION
/*Processo*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, t.sName as context_name FROM isms_context c JOIN rm_process t ON (c.pkContext = t.fkContext) UNION
/*Ativo*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, t.sName as context_name FROM isms_context c JOIN rm_asset t ON (c.pkContext = t.fkContext) UNION
/*Risco*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, t.sName as context_name FROM isms_context c JOIN rm_risk t ON (c.pkContext = t.fkContext) UNION
/*Controle*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, t.sName as context_name FROM isms_context c JOIN rm_control t ON (c.pkContext = t.fkContext) UNION
/*Categoria*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, t.sName as context_name FROM isms_context c JOIN rm_category t ON (c.pkContext = t.fkContext) UNION
/*Evento*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, t.sDescription as context_name FROM isms_context c JOIN rm_event t ON (c.pkContext = t.fkContext) UNION
/*Melhor Pr�tica*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, t.sName as context_name FROM isms_context c JOIN rm_best_practice t ON (c.pkContext = t.fkContext) UNION
/*Norma*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, t.sName as context_name FROM isms_context c JOIN rm_standard t ON (c.pkContext = t.fkContext) UNION
/*Rela��o Processo X Ativo*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, (p.sName + ' X ' + a.sName) as context_name FROM isms_context c JOIN rm_process_asset pa ON (c.pkContext = pa.fkContext)
JOIN rm_process p ON (pa.fkProcess = p.fkContext) JOIN rm_asset a ON (pa.fkAsset = a.fkContext) UNION
/*Se��o de Melhor Pr�tica*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, t.sName as context_name FROM isms_context c JOIN rm_section_best_practice t ON (c.pkContext = t.fkContext) UNION
/*Rela��o Risco X Controle*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, (r.sName + ' X ' + co.sName) as context_name FROM isms_context c JOIN rm_risk_control rc ON (c.pkContext = rc.fkContext)
JOIN rm_risk r ON (rc.fkRisk = r.fkContext) JOIN rm_control co ON (rc.fkControl = co.fkContext) UNION
/*Documento*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, t.sName as context_name FROM isms_context c JOIN pm_document t ON (c.pkContext = t.fkContext) UNION
/*Inst�ncia do Documento*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, (d.sName + ' (' + cast(t.nMajorVersion as varchar) + '.' + cast(t.nRevisionVersion as varchar) + ')') as context_name 
FROM isms_context c JOIN pm_doc_instance t ON (c.pkContext = t.fkContext) JOIN pm_document d ON (t.fkDocument = d.fkContext) UNION
/*Categoria de Incidente*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, t.sName as context_name FROM isms_context c JOIN ci_category t ON(c.pkContext = t.fkContext) UNION
/*Ocorr�ncia*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, CAST(t.tDescription as varchar) as context_name FROM isms_context c JOIN ci_occurrence t ON(c.pkContext = t.fkContext) UNION
/*Incidente*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, t.sName as context_name FROM isms_context c JOIN ci_incident t ON(c.pkContext = t.fkContext) UNION
/*N�o Conformidade*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, t.sName as context_name FROM isms_context c JOIN ci_nc t ON(c.pkContext = t.fkContext) UNION
/*Template de Documento*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, t.sName as context_name FROM isms_context c JOIN pm_template t ON (c.pkContext = t.fkContext) UNION
/*Rela��o entre Incidente e Risco*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, '' as context_name FROM isms_context c JOIN ci_incident_risk ir ON (ir.fkContext = c.pkContext) UNION
/*Rela��o entre Incidente e Controle*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, '' as context_name FROM isms_context c JOIN ci_incident_control ic ON (ic.fkContext = c.pkContext) UNION
/*Toler�ncia ao Risco*/
SELECT c.pkcontext AS context_id, c.ntype AS context_type, c.nstate AS context_state, '' AS context_name FROM isms_context c JOIN rm_risk_limits rl ON (c.pkcontext = rl.fkcontext) UNION
/*Solu��o da Categoria de Incidente*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, CAST(s.tProblem as varchar) as context_name FROM isms_context c JOIN ci_solution s ON(c.pkContext = s.fkContext) UNION
/*Plano de A��o*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, ap.sName as context_name FROM isms_context c JOIN ci_action_plan ap ON(c.pkContext = ap.fkContext)
GO

CREATE VIEW view_pm_document_active AS
SELECT 
	d.fkContext, d.fkClassification, d.fkCurrentVersion, d.fkParent, d.fkAuthor, d.fkMainApprover,
	d.sName, d.tDescription, d.nType, d.sKeywords, d.dDateProduction, d.dDeadline, d.bFlagDeadlineAlert, d.bFlagDeadlineExpired,
	d.bHasApproved, d.nDaysBefore
FROM isms_context c JOIN pm_document d ON (c.pkContext = d.fkContext AND c.nState <> 2705)
GO

CREATE VIEW view_pm_doc_instance_active AS
SELECT
	i.fkContext, i.fkDocument, i.nMajorVersion, i.nRevisionVersion, i.tRevisionJustification, i.sPath, i.tModifyComment,
	i.dBeginProduction, i.dEndProduction, i.sFilename, i.bIsLink, i.sLink,i.dBeginApprover,i.dBeginRevision
FROM isms_context c 
  JOIN pm_doc_instance i ON (c.pkContext = i.fkContext AND c.nState <> 2705)
  JOIN isms_context ctx_doc ON (ctx_doc.pkContext = i.fkDocument AND ctx_doc.nState <> 2705)
GO

CREATE VIEW view_pm_di_active_with_content AS
SELECT
	i.fkContext, i.fkDocument, i.nMajorVersion, i.nRevisionVersion, i.tRevisionJustification, i.sPath, i.tModifyComment,
	i.dBeginProduction, i.dEndProduction, i.sFilename, i.bIsLink, i.sLink, ic.tContent,i.dBeginApprover,i.dBeginRevision
FROM isms_context c 
JOIN pm_doc_instance i ON (c.pkContext = i.fkContext AND c.nState <> 2705)
JOIN isms_context ctx_doc ON (ctx_doc.pkContext = i.fkDocument AND ctx_doc.nState <> 2705)
LEFT JOIN pm_instance_content ic ON (i.fkContext = ic.fkInstance)
GO

CREATE VIEW view_pm_di_with_content AS
SELECT
	i.fkContext, i.fkDocument, i.nMajorVersion, i.nRevisionVersion, i.tRevisionJustification, i.sPath, i.tModifyComment,
	i.dBeginProduction, i.dEndProduction, i.sFilename, i.bIsLink, i.sLink, ic.tContent
FROM isms_context c 
JOIN pm_doc_instance i ON (c.pkContext = i.fkContext)
LEFT JOIN pm_instance_content ic ON (i.fkContext = ic.fkInstance)
GO

CREATE VIEW view_isms_user_active AS
SELECT
	u.fkContext, u.fkProfile, u.sName, u.sLogin, u.sPassword, u.sEmail, u.nIp,
	u.nLanguage, u.dLastLogin
FROM isms_context c JOIN isms_user u ON (c.pkContext = u.fkContext AND c.nState <> 2705)
GO

CREATE VIEW view_rm_area_active AS
SELECT
	a.fkContext, a.fkPriority, a.fkType, a.fkParent, a.fkResponsible, a.sName,
	a.tDescription, a.sDocument, a.nValue
FROM isms_context c JOIN rm_area a ON (c.pkContext = a.fkContext AND c.nState <> 2705)
GO

CREATE VIEW view_rm_process_active AS
SELECT
	p.fkContext, p.fkPriority, p.fkType, p.fkArea, p.fkResponsible, p.sName,
	p.tDescription, p.sDocument, p.nValue, p.tJustification
FROM isms_context c JOIN rm_process p ON (c.pkContext = p.fkContext AND c.nState <> 2705)
GO

CREATE VIEW view_rm_process_asset_active AS
SELECT fkContext, fkProcess, fkAsset FROM rm_process_asset
WHERE
fkProcess NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2802 AND nState = 2705)
AND
fkAsset NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2803 AND nState = 2705)
GO

CREATE VIEW view_rm_risk_control_active AS
SELECT fkContext, fkProbabilityValueName, fkRisk, fkControl, tJustification FROM rm_risk_control
WHERE
fkRisk NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2804 AND nState = 2705)
AND
fkControl NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2805 AND nState = 2705)
GO

CREATE VIEW view_rm_risk_active AS
SELECT
	r.fkContext, r.fkProbabilityValueName, r.fkType, r.fkEvent, r.fkAsset, r.sName,
	r.tDescription, r.nValue, r.nValueResidual, r.tJustification, r.nAcceptMode, r.nAcceptState,
	r.sAcceptJustification, r.nCost
FROM isms_context c JOIN rm_risk r ON (c.pkContext = r.fkContext AND c.nState <> 2705)
GO

CREATE VIEW view_rm_asset_active AS
SELECT
	a.fkContext, a.fkCategory, a.fkResponsible, a.fkSecurityResponsible, a.sName,
	a.tDescription, a.sDocument, a.nCost, a.nValue, a.bLegality, a.tJustification
FROM isms_context c JOIN rm_asset a ON (c.pkContext = a.fkContext AND c.nState <> 2705)
GO

CREATE VIEW view_pm_process_user_active AS
SELECT fkProcess, fkUser FROM pm_process_user
WHERE
fkProcess NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2802 AND nState = 2705)
AND
fkUser NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2816 AND nState = 2705)
GO

CREATE VIEW view_rm_control_bp_active AS
SELECT fkContext, fkBestPractice, fkControl FROM rm_control_best_practice
WHERE
fkBestPractice NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2808 AND nState = 2705)
AND
fkControl NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2805 AND nState = 2705)
GO

CREATE VIEW view_rm_control_active AS
SELECT
	ctr.fkContext, ctr.fkType, ctr.fkResponsible, ctr.sName, ctr.tDescription,
	ctr.sDocument, ctr.sEvidence, ctr.nDaysBefore, ctr.bIsActive, ctr.bFlagImplAlert, ctr.bFlagImplExpired,
	ctr.dDateDeadline, ctr.dDateImplemented, ctr.nImplementationState
FROM isms_context c JOIN rm_control ctr ON (c.pkContext = ctr.fkContext AND c.nState <> 2705)
GO

CREATE VIEW view_rm_best_practice_active AS
SELECT
	bp.fkContext, bp.fkSectionBestPractice, bp.sName, bp.tDescription,
	bp.nControlType, bp.sClassification, bp.tImplementationGuide, bp.tMetric, bp.sDocument, bp.tJustification
FROM isms_context c JOIN rm_best_practice bp ON (c.pkContext = bp.fkContext AND c.nState <> 2705)
GO

CREATE VIEW view_isms_context_active AS
SELECT c.pkContext, c.nType, c.nState
FROM isms_context c WHERE c.nState <> 2705
GO

CREATE VIEW view_rm_asset_asset_active AS
SELECT fkAsset, fkDependent
FROM rm_asset_asset
WHERE
fkAsset NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2803 AND nState = 2705)
AND
fkDependent NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2803 AND nState = 2705)
GO

CREATE VIEW view_rm_bp_standard_active AS
SELECT fkContext, fkStandard, fkBestPractice FROM rm_best_practice_standard
WHERE
fkStandard NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2809 AND nState = 2705)
AND
fkBestPractice NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2808 AND nState = 2705)
GO

CREATE VIEW view_rm_bp_event_active AS
SELECT fkContext, fkBestPractice, fkEvent FROM rm_best_practice_event
WHERE
fkBestPractice NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2808 AND nState = 2705)
AND
fkEvent NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2807 AND nState = 2705)
GO

CREATE VIEW view_rm_standard_active AS
SELECT
	s.fkContext, s.sName, s.tDescription, s.tApplication, s.tObjective
FROM isms_context c JOIN rm_standard s ON (c.pkContext = s.fkContext AND c.nState <> 2705)
GO

CREATE VIEW view_pm_register_active AS
SELECT
	r.fkContext, r.fkResponsible, r.fkClassification, r.fkDocument, r.sName, r.nPeriod, r.nValue,
	r.sStoragePlace, r.sStorageType, r.sIndexingType, r.sDisposition, r.sProtectionRequirements, r.sOrigin
FROM isms_context c JOIN pm_register r ON (c.pkContext = r.fkContext AND c.nState <> 2705)
GO

CREATE VIEW view_isms_context_hist_active AS
SELECT h.pkId, h.fkContext, h.sType, h.dDate, h.nValue
FROM isms_context c RIGHT JOIN isms_context_history h ON (c.pkContext = h.fkContext AND c.nState <> 2705)
GO

CREATE VIEW view_isms_profile_active AS
SELECT p.fkContext, p.sName, p.nId
FROM isms_context c JOIN isms_profile p ON (c.pkContext = p.fkContext AND c.nState <> 2705)
GO

CREATE VIEW view_pm_doc_registers_active AS
SELECT fkRegister, fkDocument FROM pm_doc_registers
WHERE
fkDocument NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2823 AND nState = 2705)
AND
fkRegister NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2826 AND nState = 2705)
GO

CREATE VIEW view_pm_doc_context_active AS
SELECT fkContext, fkDocument FROM pm_doc_context
WHERE
fkContext NOT IN (SELECT pkContext FROM isms_context WHERE nState = 2705)
AND
fkDocument NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2823 AND nState = 2705)
GO

CREATE VIEW view_pm_doc_approvers_active AS
SELECT fkDocument, fkUser, bHasApproved FROM pm_doc_approvers
WHERE
fkDocument NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2823 AND nState = 2705)
AND
fkUser NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2816 AND nState = 2705)
GO

CREATE VIEW view_pm_doc_readers_active AS
SELECT fkUser, fkDocument, bManual, bDenied, bHasRead FROM pm_doc_readers
WHERE
fkDocument NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2823 AND nState = 2705)
AND
fkUser NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2816 AND nState = 2705)
GO

CREATE VIEW view_isms_scope_active AS
SELECT s.fkContext, s.tDescription
FROM isms_context c JOIN isms_scope s ON (c.pkContext = s.fkContext AND c.nState <> 2705)
GO

CREATE VIEW view_isms_policy_active AS
SELECT p.fkContext, p.tDescription
FROM isms_context c JOIN isms_policy p ON (c.pkContext = p.fkContext AND c.nState <> 2705)
GO

CREATE VIEW view_rm_category_active AS
SELECT cat.fkContext, cat.fkParent, cat.sName, cat.tDescription
FROM isms_context c JOIN rm_category cat ON (c.pkContext = cat.fkContext AND c.nState <> 2705)
GO

CREATE VIEW view_rm_event_active AS
SELECT e.fkContext, e.fkType, e.fkCategory, e.sDescription, e.tObservation, e.bPropagate
FROM isms_context c JOIN rm_event e ON (c.pkContext = e.fkContext AND c.nState <> 2705)
GO

CREATE VIEW view_rm_sec_bp_active AS
SELECT s.fkContext, s.fkParent, s.sName
FROM isms_context c JOIN rm_section_best_practice s ON (c.pkContext = s.fkContext AND c.nState <> 2705)
GO

CREATE VIEW view_pm_template_active AS
SELECT t.fkContext, t.sName, t.nContextType, t.sPath, t.sFileName, t.tDescription, t.sKeywords, t.nfilesize
FROM isms_context c JOIN pm_template t ON (c.pkContext = t.fkContext AND c.nState <> 2705)
GO

CREATE VIEW view_pm_tp_active_with_content AS
SELECT
  t.fkContext, t.sName, t.nContextType, t.sPath, t.sFileName, t.tDescription, t.sKeywords, tc.tContent, t.nfilesize
FROM isms_context c 
JOIN pm_template t ON (c.pkContext = t.fkContext AND c.nState <> 2705)
LEFT JOIN pm_template_content tc ON (t.fkContext = tc.fkContext)
GO

CREATE VIEW view_pm_template_bp_active AS
SELECT tbp.fkTemplate, 
  tbp.fkBestPractice
FROM pm_template_best_practice tbp
JOIN isms_context cbp ON (cbp.pkContext = tbp.fkBestPractice AND cbp.nState <> 2705)
JOIN isms_context ct ON (ct.pkContext = tbp.fkTemplate AND ct.nState <> 2705)
GO

CREATE VIEW view_ci_solution_active AS
SELECT s.fkContext, s.fkCategory, s.tProblem, s.tSolution, s.tKeywords
FROM isms_context c JOIN ci_solution s ON (c.pkContext = s.fkContext AND c.nState <> 2705)
GO

CREATE VIEW view_ci_nc_active AS 
 SELECT nc.fkcontext, nc.fkcontrol, nc.fkresponsible, nc.sname, nc.nseqnumber, nc.tdescription, nc.tcause, nc.tdenialjustification, nc.nclassification, nc.fksender, nc.ncapability, nc.ddatesent
   FROM isms_context c
   JOIN ci_nc nc ON c.pkcontext = nc.fkcontext AND c.nstate <> 2705
GO


CREATE VIEW view_ci_incident_active AS
SELECT i.fkContext, i.fkCategory, i.fkResponsible, i.sName, i.tAccountsPlan, i.tEvidences, i.nLossType,
	i.tEvidenceRequirementComment, i.dDateLimit, i.dDateFinish, i.tDisposalDescription, i.tSolutionDescription,
	i.tProductService, i.bNotEmailDP, i.bEmailDPSent, i.dDate
FROM isms_context c JOIN ci_incident i ON (c.pkContext = i.fkContext AND c.nState <> 2705)
GO

CREATE VIEW view_ci_category_active AS
SELECT cat.fkContext, cat.sName
FROM isms_context c JOIN ci_category cat ON (c.pkContext = cat.fkContext AND c.nState <> 2705)
GO

CREATE VIEW view_ci_incident_risk_active AS
SELECT ir.fkContext, ir.fkRisk,ir.fkIncident
FROM ci_incident_risk ir
JOIN isms_context ctx_r ON (ctx_r.pkContext = ir.fkRisk AND ctx_r.nState <> 2705)
JOIN isms_context ctx_i ON (ctx_i.pkContext = ir.fkIncident AND ctx_i.nState <> 2705)
GO

CREATE VIEW view_ci_inc_control_active AS
SELECT ic.fkContext, ic.fkControl,ic.fkIncident
FROM ci_incident_control ic
JOIN isms_context ctx_c ON (ctx_c.pkContext = ic.fkControl AND ctx_c.nState <> 2705)
JOIN isms_context ctx_i ON (ctx_i.pkContext = ic.fkIncident AND ctx_i.nState <> 2705)
GO

CREATE VIEW view_ci_inc_process_active AS
SELECT ip.fkContext, ip.fkProcess,ip.fkIncident
FROM ci_incident_process ip
JOIN isms_context ctx_p ON (ctx_p.pkContext = ip.fkProcess AND ctx_p.nState <> 2705)
JOIN isms_context ctx_i ON (ctx_i.pkContext = ip.fkIncident AND ctx_i.nState <> 2705)
GO

CREATE VIEW view_ci_nc_process_active AS
SELECT np.fkProcess,np.fkNC
FROM ci_nc_process np
JOIN isms_context ctx_p ON (ctx_p.pkContext = np.fkProcess AND ctx_p.nState <> 2705)
JOIN isms_context ctx_n ON (ctx_n.pkContext = np.fkNC AND ctx_n.nState <> 2705)
GO

CREATE VIEW view_ci_occurrence_active AS
SELECT o.fkContext, o.fkIncident, o.tDescription, o.tDenialJustification, o.dDate
FROM isms_context c JOIN ci_occurrence o ON (c.pkContext = o.fkContext AND c.nState <> 2705)
GO

CREATE VIEW view_ci_incident_user_active AS
SELECT fkUser, fkIncident, tDescription FROM ci_incident_user
WHERE
fkUser NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2816 AND nState = 2705)
AND
fkIncident NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2831 AND nState = 2705)
GO

CREATE VIEW view_pm_published_docs AS
SELECT
  d.fkContext,
  d.fkClassification,
  d.fkCurrentVersion,
  d.fkParent,
  d.fkAuthor,
  d.fkMainApprover,
  d.sName,
  d.tDescription,
  d.nType,
  d.sKeywords,
  d.dDateProduction,
  d.dDeadline,
  d.bFlagDeadlineAlert,
  d.bFlagDeadlineExpired,
  d.nDaysBefore,
  d.bHasApproved
FROM
  view_pm_document_active d
  JOIN pm_doc_instance di ON (di.fkContext = d.fkCurrentVersion AND di.nMajorVersion > 0)
GO

CREATE VIEW view_pm_read_docs AS
SELECT
  d.fkContext,
  d.fkClassification,
  d.fkCurrentVersion,
  d.fkParent,
  d.fkAuthor,
  d.fkMainApprover,
  d.sName,
  d.tDescription,
  d.nType,
  d.sKeywords,
  d.dDateProduction,
  d.dDeadline,
  d.bFlagDeadlineAlert,
  d.bFlagDeadlineExpired,
  d.nDaysBefore,
  d.bHasApproved
FROM
  view_pm_document_active d
  JOIN pm_doc_read_history rh ON (rh.fkInstance = d.fkCurrentVersion)
GO

CREATE VIEW view_ci_action_plan_active AS 
 SELECT ac.fkcontext, ac.fkresponsible, ac.sname, ac.tactionplan, ac.nactiontype, ac.ddatedeadline, ac.ddateconclusion, ac.bisefficient, ac.ddateefficiencyrevision, ac.ddateefficiencymeasured, ac.ndaysbefore, ac.bflagrevisionalert, ac.bflagrevisionalertlate
   FROM isms_context c
   JOIN ci_action_plan ac ON c.pkcontext = ac.fkcontext AND c.nstate <> 2705
GO


CREATE function get_acl_tree (@pnParent INTEGER, @pnLevel INTEGER)
RETURNS @sbp_tree TABLE
    (
    pkAcl  int,
    nLevel int,
    sName    VARCHAR(256) COLLATE SQL_Latin1_General_CP1_CI_AS,
    sTag    VARCHAR(256) COLLATE SQL_Latin1_General_CP1_CI_AS,
    sDescription    VARCHAR(256) COLLATE SQL_Latin1_General_CP1_CI_AS
   )
AS
BEGIN
DECLARE sbp_tree_cursor CURSOR    
   FOR SELECT pkAcl, fkParent, sName, sTag, sDescription FROM isms_acl  WHERE  fkParent = @pnParent

DECLARE root_tree_cursor CURSOR
   FOR SELECT  pkAcl, fkParent, sName, sTag, sDescription FROM isms_acl  WHERE  fkParent is NULL

DECLARE @query CURSOR

DECLARE @sbp_id NUMERIC(18,0), @level INT, @parent_id NUMERIC(18,0), @sbp_name VARCHAR(128), @sbp_description VARCHAR(128), @sbp_tag VARCHAR(128)

SET @level = @pnLevel + 1
if (@pnParent > 0)
   BEGIN SET @query = sbp_tree_cursor END
else
   BEGIN SET @query = root_tree_cursor END

OPEN @query
FETCH NEXT FROM @query INTO @sbp_id, @parent_id, @sbp_name,  @sbp_description, @sbp_tag
WHILE (@@FETCH_STATUS = 0 )
    BEGIN  
      INSERT INTO @sbp_tree  values( @sbp_id,  @level, @sbp_name,   @sbp_description, @sbp_tag)
      INSERT @sbp_tree
	  select pkAcl, nLevel, sName, sTag, sDescription
	  from  %database_login%.get_acl_tree( @sbp_id,  @level)
     FETCH NEXT FROM @query INTO @sbp_id, @parent_id, @sbp_name,  @sbp_description, @sbp_tag
   END

CLOSE @query
DEALLOCATE @query
DEALLOCATE root_tree_cursor
DEALLOCATE sbp_tree_cursor

RETURN
END
GO

CREATE FUNCTION get_area_parents (@area_id INT)
RETURNS @area_parents TABLE (
    pkArea INT
)
AS

BEGIN
    DECLARE @parent_area_id INT
    
    DECLARE area_parents_cursor CURSOR FOR
        SELECT fkParent FROM rm_area 
		JOIN isms_context c ON(c.pkContext = fkParent and c.nState <> 2705)
        WHERE fkContext = @area_id
    
    OPEN area_parents_cursor
    FETCH NEXT FROM area_parents_cursor INTO @parent_area_id
    IF (@parent_area_id IS NOT NULL)
    BEGIN
        INSERT INTO @area_parents values(@parent_area_id)
        INSERT @area_parents SELECT pkArea FROM %database_login%.get_area_parents(@parent_area_id)
    END
    
    CLOSE area_parents_cursor
    DEALLOCATE area_parents_cursor
    
    RETURN
END
GO

CREATE function get_area_tree (@pnParent INTEGER, @pnLevel INTEGER)
RETURNS @area_tree TABLE
    (    
    pkId numeric identity,
    fkContext  int,
    fkUser int,
    sName    VARCHAR(256) COLLATE SQL_Latin1_General_CP1_CI_AS,
    nLevel int    
   )
AS
BEGIN

DECLARE area_tree_cursor CURSOR
   FOR SELECT fkContext, fkResponsible, fkParent, sName FROM rm_area 
   	   JOIN isms_context c ON(c.pkContext = fkContext and c.nState <> 2705)
       WHERE  fkParent = @pnParent

DECLARE root_tree_cursor CURSOR
   FOR SELECT fkContext, fkResponsible, fkParent, sName FROM rm_area 
   JOIN isms_context c ON(c.pkContext = fkContext and c.nState <> 2705)
   WHERE  fkParent is NULL

DECLARE @query CURSOR

DECLARE @area_id NUMERIC(18,0), @level INT, @parent_id NUMERIC(18,0), @area_name VARCHAR(128), @creator_id NUMERIC(18,0)

SET @level = @pnLevel + 1
if (@pnParent > 0)
   BEGIN SET @query = area_tree_cursor END
else
   BEGIN SET @query = root_tree_cursor END

OPEN @query
FETCH NEXT FROM @query INTO @area_id, @creator_id, @parent_id, @area_name
WHILE (@@FETCH_STATUS = 0) 
    BEGIN
      INSERT INTO @area_tree  values( @area_id, @creator_id, @area_name, @level)
      INSERT @area_tree
	  select fkContext, fkUser, sName, nLevel
	  from %database_login%.get_area_tree ( @area_id,  @level)
     FETCH NEXT FROM @query INTO @area_id, @creator_id, @parent_id, @area_name
   END

CLOSE @query
DEALLOCATE @query
DEALLOCATE root_tree_cursor
DEALLOCATE area_tree_cursor

RETURN
END
GO

CREATE FUNCTION get_area_value  ( @piArea NUMERIC )  
RETURNS FLOAT
AS 
BEGIN 
DECLARE @fiAreaValue FLOAT

SET @fiAreaValue = 0

DECLARE query_area CURSOR
FOR SELECT MAX(nValue) FROM (
      SELECT nValue 
          FROM rm_process p
          JOIN isms_context c ON(c.pkContext = p.fkContext and c.nState <> 2705 and p.fkArea = @piArea)
      UNION
      SELECT nValue
          FROM rm_area a
		  JOIN isms_context c ON(c.pkContext = a.fkContext and c.nState <> 2705 and a.fkParent = @piArea)
		) as buffer


OPEN query_area
FETCH NEXT FROM query_area INTO @fiAreaValue
CLOSE query_area
DEALLOCATE query_area

IF(@fiAreaValue IS NULL) return 0

RETURN @fiAreaValue
END
GO

CREATE FUNCTION get_areas_by_user (@userId INT)
RETURNS  @result TABLE (
    area_id INT
)
AS

BEGIN   
    DECLARE @area_id INT
    DECLARE @area_id_sub INT

    DECLARE areas_user CURSOR FOR
      	SELECT  a.fkContext
	FROM isms_context cont_a
	JOIN rm_area a ON (cont_a.pkContext = a.fkContext AND cont_a.nState <> 2705)    
	WHERE a.fkResponsible = @userId

    OPEN areas_user
    FETCH NEXT FROM areas_user INTO @area_id
    WHILE (@@FETCH_STATUS = 0)
    BEGIN
	INSERT INTO @result values(@area_id) 
	INSERT INTO @result SELECT pkArea FROM %database_login%.get_area_parents(@area_id)

        DECLARE areas_user_rec CURSOR FOR
	SELECT pkArea FROM %database_login%.get_sub_areas(@area_id)
	JOIN isms_context c ON (c.pkContext = pkArea AND c.nState <> 2705)
        WHERE pkArea NOT IN(select area_id as pkArea from @result)
	
	OPEN areas_user_rec
	FETCH NEXT FROM areas_user_rec INTO @area_id_sub
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
	    INSERT INTO @result values(@area_id_sub)
            FETCH NEXT FROM areas_user_rec INTO @area_id_sub
	END
	CLOSE areas_user_rec
        DEALLOCATE areas_user_rec
	FETCH NEXT FROM areas_user INTO @area_id
    END  


    CLOSE areas_user
    DEALLOCATE areas_user
    
    RETURN
END
GO

CREATE FUNCTION get_superareas_by_user(@userId INT)
RETURNS  @result TABLE (
    area_id INT
)
AS

BEGIN   

    DECLARE @area_id INT

    DECLARE areas_user CURSOR FOR
      	SELECT  a.fkContext
	FROM isms_context cont_a
	JOIN rm_area a ON (cont_a.pkContext = a.fkContext AND cont_a.nState <> 2705)    
	where a.fkResponsible = @userId

    OPEN areas_user
    FETCH NEXT FROM areas_user INTO @area_id
    WHILE (@@FETCH_STATUS = 0)
    BEGIN
	INSERT INTO @result values(@area_id) 
	INSERT INTO @result SELECT pkArea FROM %database_login%.get_area_parents(@area_id)

	FETCH NEXT FROM areas_user INTO @area_id
    END  

    CLOSE areas_user
    DEALLOCATE areas_user
    
    RETURN
END
GO

CREATE FUNCTION get_asset_value  ( @piAsset NUMERIC )  
RETURNS FLOAT
AS 
BEGIN 
DECLARE @fiAssetValue FLOAT

SET @fiAssetValue = 0

DECLARE query_asset CURSOR
FOR SELECT MAX(value) FROM (
		SELECT MAX(nvalueresidual) as value
			FROM rm_risk as r
			JOIN isms_context c ON(c.pkContext = r.fkContext and c.nState <> 2705)
			WHERE r.fkasset = @piAsset
			AND nAcceptMode = 0
		UNION
		SELECT MAX(nvalue) as value
			FROM rm_asset a
			JOIN isms_context c1 ON(c1.pkContext = a.fkContext and c1.nState <> 2705)
			JOIN rm_asset_asset aa ON (a.fkContext = aa.fkAsset)
			JOIN isms_context c2 ON(c2.pkContext = aa.fkAsset and c2.nState <> 2705)
			WHERE aa.fkdependent = @piAsset 
		) as buffer


OPEN query_asset
FETCH NEXT FROM query_asset INTO @fiAssetValue
CLOSE query_asset
DEALLOCATE query_asset

IF(@fiAssetValue IS NULL) return 0

RETURN @fiAssetValue
END
GO

CREATE FUNCTION get_bp_from_section (@section_id INT)
RETURNS @best_practices TABLE (
    pkBestPractice INT
)
AS

BEGIN
    DECLARE @next_section_id INT
    DECLARE @next_best_practice_id INT
    
    DECLARE sub_sections_cursor CURSOR FOR
        SELECT fkContext
        FROM rm_section_best_practice
        JOIN isms_context c ON(c.pkContext = fkContext and c.nState <> 2705)
        WHERE fkParent = @section_id
    
    DECLARE best_practices_from_section_cursor CURSOR FOR
        SELECT fkContext
        FROM rm_best_practice
        JOIN isms_context c ON(c.pkContext = fkContext and c.nState <> 2705)
        WHERE fkSectionBestPractice = @section_id
    
    OPEN best_practices_from_section_cursor
    FETCH NEXT FROM best_practices_from_section_cursor INTO @next_best_practice_id
    WHILE (@@FETCH_STATUS = 0)
    BEGIN
        IF (@next_best_practice_id IS NOT NULL)
        BEGIN
            INSERT INTO @best_practices values(@next_best_practice_id)
            FETCH NEXT FROM best_practices_from_section_cursor INTO @next_best_practice_id
        END
    END
    CLOSE best_practices_from_section_cursor
    DEALLOCATE best_practices_from_section_cursor
    
    OPEN sub_sections_cursor
    FETCH NEXT FROM sub_sections_cursor INTO @next_section_id
    WHILE (@@FETCH_STATUS = 0)
    BEGIN
        IF (@next_section_id IS NOT NULL)
        BEGIN
            INSERT @best_practices SELECT pkBestPractice FROM %database_login%.get_bp_from_section(@next_section_id)
        END
        FETCH NEXT FROM sub_sections_cursor INTO @next_section_id
    END
    CLOSE sub_sections_cursor
    DEALLOCATE sub_sections_cursor
    
    RETURN
END
GO

CREATE FUNCTION get_category_parents (@category_id INT)
RETURNS @category_parents TABLE (
    pkCategory INT
)
AS

BEGIN
    DECLARE @parent_category_id INT
    
    DECLARE category_parents_cursor CURSOR FOR
        SELECT fkParent FROM rm_category 
        JOIN isms_context c ON(c.pkContext = fkParent and c.nState <> 2705)
        WHERE fkContext = @category_id
    
    OPEN category_parents_cursor
    FETCH NEXT FROM category_parents_cursor INTO @parent_category_id
    IF (@parent_category_id IS NOT NULL)
    BEGIN
        INSERT INTO @category_parents values(@parent_category_id)
        INSERT @category_parents SELECT pkCategory FROM %database_login%.get_category_parents(@parent_category_id)
    END
    
    CLOSE category_parents_cursor
    DEALLOCATE category_parents_cursor
    
    RETURN
END
GO

CREATE function get_category_tree (@pnParent INTEGER, @pnLevel INTEGER)
RETURNS @category_tree TABLE
    (
    fkContext  int,
    sName    VARCHAR(256) COLLATE SQL_Latin1_General_CP1_CI_AS,
     nLevel int
   )
AS
BEGIN
DECLARE category_tree_cursor CURSOR
   FOR SELECT fkContext, fkParent, sName FROM rm_category 
   JOIN isms_context c ON(c.pkContext = fkContext and c.nState <> 2705)
   WHERE  fkParent = @pnParent

DECLARE root_tree_cursor CURSOR
   FOR SELECT fkContext, fkParent, sName FROM rm_category 
   JOIN isms_context c ON(c.pkContext = fkContext and c.nState <> 2705)   
   WHERE  fkParent is NULL

DECLARE @query CURSOR

DECLARE @category_id NUMERIC(18,0), @level INT, @parent_id NUMERIC(18,0), @category_name VARCHAR(128)

SET @level = @pnLevel + 1
if (@pnParent > 0)
   BEGIN SET @query = category_tree_cursor END
else
   BEGIN SET @query = root_tree_cursor END

OPEN @query
FETCH NEXT FROM @query INTO @category_id, @parent_id, @category_name
WHILE (@@FETCH_STATUS = 0 )
    BEGIN  
      INSERT INTO @category_tree  values( @category_id,  @category_name, @level)
      INSERT @category_tree
	  select fkContext, sName, nLevel
	  from %database_login%.get_category_tree( @category_id,  @level)
     FETCH NEXT FROM @query INTO @category_id, @parent_id, @category_name
   END

CLOSE @query
DEALLOCATE @query
DEALLOCATE root_tree_cursor
DEALLOCATE category_tree_cursor

RETURN
END
GO

CREATE FUNCTION get_events_from_category (@category_id INT)
RETURNS @events TABLE (
    pkEvent INT
)
AS

BEGIN
    DECLARE @next_category_id INT
    DECLARE @next_event_id INT
    
    DECLARE sub_categories_cursor CURSOR FOR
        SELECT fkContext
        FROM rm_category
        JOIN isms_context c ON(c.pkContext = fkContext and c.nState <> 2705)
        WHERE fkParent = @category_id
    
    DECLARE events_from_category_cursor CURSOR FOR
        SELECT fkContext
        FROM rm_event
        JOIN isms_context c ON(c.pkContext = fkContext and c.nState <> 2705)
        WHERE fkCategory = @category_id
    
    OPEN events_from_category_cursor
    FETCH NEXT FROM events_from_category_cursor INTO @next_event_id
    WHILE (@@FETCH_STATUS = 0)
    BEGIN
        IF (@next_event_id IS NOT NULL)
        BEGIN
            INSERT INTO @events values(@next_event_id)
            FETCH NEXT FROM events_from_category_cursor INTO @next_event_id
        END
    END
    CLOSE events_from_category_cursor
    DEALLOCATE events_from_category_cursor
    
    OPEN sub_categories_cursor
    FETCH NEXT FROM sub_categories_cursor INTO @next_category_id
    WHILE (@@FETCH_STATUS = 0)
    BEGIN
        IF (@next_category_id IS NOT NULL)
        BEGIN
            INSERT @events SELECT pkEvent FROM %database_login%.get_events_from_category(@next_category_id)
        END
        FETCH NEXT FROM sub_categories_cursor INTO @next_category_id
    END
    CLOSE sub_categories_cursor
    DEALLOCATE sub_categories_cursor
    
    RETURN
END
GO

CREATE FUNCTION get_supercategories_events (@category_id INT)
RETURNS @events TABLE (
    pkEvent INT
)
AS

BEGIN
    DECLARE @next_category_id INT
    DECLARE @next_event_id INT
    
    DECLARE parent_categories_cursor CURSOR FOR
        SELECT fkParent
        FROM rm_category
        JOIN isms_context c ON(c.pkContext = fkContext and c.nState <> 2705)
        WHERE fkContext = @category_id
    
    DECLARE events_from_category_cursor CURSOR FOR
        SELECT fkContext
        FROM rm_event
        JOIN isms_context c ON(c.pkContext = fkContext and c.nState <> 2705)
        WHERE fkCategory = @category_id
    
    OPEN events_from_category_cursor
    FETCH NEXT FROM events_from_category_cursor INTO @next_event_id
    WHILE (@@FETCH_STATUS = 0)
    BEGIN
        IF (@next_event_id IS NOT NULL)
        BEGIN
            INSERT INTO @events values(@next_event_id)
            FETCH NEXT FROM events_from_category_cursor INTO @next_event_id
        END
    END
    CLOSE events_from_category_cursor
    DEALLOCATE events_from_category_cursor
    
    OPEN parent_categories_cursor
    FETCH NEXT FROM parent_categories_cursor INTO @next_category_id
    WHILE (@@FETCH_STATUS = 0)
    BEGIN
        IF (@next_category_id IS NOT NULL)
        BEGIN
            INSERT @events SELECT pkEvent FROM %database_login%.get_supercategories_events(@next_category_id)
        END
        FETCH NEXT FROM parent_categories_cursor INTO @next_category_id
    END
    CLOSE parent_categories_cursor
    DEALLOCATE parent_categories_cursor
    
    RETURN
END
GO

CREATE FUNCTION get_process_value  ( @piProcess NUMERIC )  
RETURNS FLOAT
AS 
BEGIN 
DECLARE @fiProcessValue FLOAT

SET @fiProcessValue = 0

DECLARE query_process CURSOR
FOR SELECT MAX(a.nValue) 
    FROM rm_asset as a
    JOIN isms_context c1 ON(c1.pkContext = a.fkContext and c1.nState <> 2705)
    JOIN rm_process_asset pa ON( a.fkContext = pa.fkAsset)
    JOIN isms_context c2 ON(c2.pkContext = pa.fkContext and c2.nState <> 2705)
    WHERE pa.fkProcess = @piProcess


OPEN query_process
FETCH NEXT FROM query_process INTO @fiProcessValue
CLOSE query_process
DEALLOCATE query_process

IF(@fiProcessValue IS NULL) return 0

RETURN @fiProcessValue
END
GO

CREATE FUNCTION get_processes_from_area (@area_id INT)
RETURNS @processes TABLE (
    pkProcess INT
)
AS

BEGIN
    DECLARE @next_area_id INT
    DECLARE @next_process_id INT
    
    DECLARE sub_areas_cursor CURSOR FOR
        SELECT fkContext
        FROM rm_area a
        JOIN isms_context c ON(c.pkContext = a.fkContext and c.nState <> 2705)
        WHERE fkParent = @area_id
    
    DECLARE processes_from_area_cursor CURSOR FOR
        SELECT fkContext
        FROM rm_process p
        JOIN isms_context c ON(c.pkContext = p.fkContext and c.nState <> 2705)
        WHERE fkArea = @area_id
    
    OPEN processes_from_area_cursor
    FETCH NEXT FROM processes_from_area_cursor INTO @next_process_id
    WHILE (@@FETCH_STATUS = 0)
    BEGIN
        IF (@next_process_id IS NOT NULL)
        BEGIN
            INSERT INTO @processes values(@next_process_id)
            FETCH NEXT FROM processes_from_area_cursor INTO @next_process_id
        END
    END
    CLOSE processes_from_area_cursor
    DEALLOCATE processes_from_area_cursor
    
    OPEN sub_areas_cursor
    FETCH NEXT FROM sub_areas_cursor INTO @next_area_id
    WHILE (@@FETCH_STATUS = 0)
    BEGIN
        IF (@next_area_id IS NOT NULL)
        BEGIN
            INSERT @processes SELECT pkProcess FROM %database_login%.get_processes_from_area(@next_area_id)
        END
        FETCH NEXT FROM sub_areas_cursor INTO @next_area_id
    END
    CLOSE sub_areas_cursor
    DEALLOCATE sub_areas_cursor
    
    RETURN
END
GO

CREATE FUNCTION get_risk_parameter_reduction  ( @piRisk NUMERIC, @piParameter NUMERIC )  
RETURNS INTEGER
AS 
BEGIN 
DECLARE @fiResult INTEGER

DECLARE @fiReductionValue INTEGER
DECLARE @fiRiskValue INTEGER

SET @fiResult = 0

DECLARE query_reduction CURSOR
FOR 
SELECT parameter_reduction_value, risk_parameter_value FROM 
(
  SELECT SUM(rcpvn.nvalue) as parameter_reduction_value from rm_risk_control rc
  JOIN rm_risk_control_value rcv ON (rcv.fkriskcontrol = rc.fkcontext)
  JOIN isms_context ctx1 ON(ctx1.pkContext = rc.fkrisk and ctx1.nState <> 2705)
  JOIN isms_context ctx2 ON(ctx2.pkContext = rc.fkcontrol and ctx2.nState <> 2705)
  JOIN rm_control c ON (c.fkContext = ctx2.pkContext AND c.bIsActive = 1)
  JOIN rm_rc_parameter_value_name rcpvn ON (rcv.fkrcvaluename = rcpvn.pkrcvaluename)
  WHERE fkrisk = @piRisk and fkparametername = @piParameter
) prv,
(
  SELECT nvalue as risk_parameter_value from rm_risk_value rv
  JOIN rm_parameter_value_name pvn ON (rv.fkvaluename = pvn.pkvaluename)
  WHERE fkrisk = @piRisk AND fkparametername = @piParameter
) rpv

OPEN query_reduction
FETCH NEXT FROM query_reduction INTO @fiReductionValue, @fiRiskValue
CLOSE query_reduction
DEALLOCATE query_reduction

IF(@fiReductionValue IS NULL) SET @fiResult = @fiRiskValue
ELSE 
BEGIN
	SET @fiResult = @fiRiskValue - @fiReductionValue
	IF( @fiResult < 1) SET @fiResult = 1
END

RETURN @fiResult
END
GO

CREATE FUNCTION get_risk_value( @piFkRisk NUMERIC )  
RETURNS FLOAT
AS 
BEGIN 

DECLARE @fsName VARCHAR(256)
DECLARE @fiRiskValue FLOAT
DECLARE @fiAssetValue FLOAT
DECLARE @fiRiskProb FLOAT
DECLARE @fiResult FLOAT
DECLARE @fiI INTEGER
DECLARE @fiBlueRisk INTEGER

DECLARE @fiCountRisk INTEGER
DECLARE @fiCountAsset INTEGER
DECLARE @fiCountSystem INTEGER

SET @fiI = 0
SET @fiResult = 0
SET @fiBlueRisk = 0

DECLARE compare_cursor CURSOR
FOR 
  SELECT count_sys, count_risk, count_asset FROM 
  (select count(pkparametername) as count_sys  FROM rm_parameter_name pn) buffer_sys,
  (select count(fkparametername) as count_risk from rm_risk_value where fkrisk = @piFkRisk) buffer_risk,
  (select count(fkparametername) as count_asset from rm_asset_value av
  JOIN rm_risk r ON (r.fkasset = av.fkasset)
  JOIN isms_context ctx1 ON(ctx1.pkContext = r.fkContext AND ctx1.nState <> 2705)
  JOIN isms_context ctx2 ON(ctx2.pkContext = r.fkAsset AND ctx2.nState <> 2705)
  WHERE r.fkcontext = @piFkRisk) buffer_asset

DECLARE values_cursor CURSOR
FOR 
  SELECT pn.sname as parameter_name, pvn.nvalue as risk_value, pvn2.nvalue as asset_value FROM rm_risk r
  JOIN isms_context ctx1 ON(ctx1.pkContext = r.fkContext and ctx1.nState <> 2705 and r.fkcontext = @piFkRisk)
  JOIN isms_context ctx2 ON(ctx2.pkContext = r.fkAsset and ctx2.nState <> 2705)	
  JOIN rm_risk_value rv ON (rv.fkrisk = r.fkcontext)
  JOIN rm_parameter_value_name pvn ON (rv.fkvaluename = pvn.pkvaluename)
  JOIN rm_parameter_name pn ON (rv.fkparametername = pn.pkparametername)
  JOIN rm_asset_value av ON (pn.pkparametername = av.fkparametername and av.fkasset = r.fkasset)
  JOIN rm_parameter_value_name pvn2 ON (av.fkvaluename = pvn2.pkvaluename)


DECLARE probability_cursor CURSOR
FOR 
  SELECT pvn.nvalue as risk_prob_value FROM rm_risk r
  JOIN isms_context c ON(c.pkContext = r.fkContext and c.nState <> 2705 and r.fkcontext = @piFkRisk)
  JOIN rm_parameter_value_name pvn ON (r.fkprobabilityvaluename = pvn.pkvaluename)

OPEN compare_cursor
FETCH NEXT FROM compare_cursor INTO @fiCountSystem, @fiCountRisk, @fiCountAsset
  IF ( (@fiCountSystem <> @fiCountRisk) or (@fiCountSystem <> @fiCountAsset) )
  BEGIN
  SET @fiBlueRisk = 1
  END
CLOSE compare_cursor
DEALLOCATE compare_cursor

IF(@fiBlueRisk = 1)
BEGIN
  DEALLOCATE values_cursor
  DEALLOCATE probability_cursor
  RETURN 0
END

OPEN values_cursor
FETCH NEXT FROM values_cursor INTO @fsName, @fiRiskValue, @fiAssetValue
WHILE (@@FETCH_STATUS = 0)
   BEGIN  
        SET @fiI = @fiI +1
        SET @fiResult = @fiResult + @fiAssetValue + @fiRiskValue
        FETCH NEXT FROM values_cursor INTO @fsName, @fiRiskValue, @fiAssetValue
   END
CLOSE values_cursor
DEALLOCATE values_cursor


OPEN probability_cursor
FETCH NEXT FROM probability_cursor INTO @fiRiskProb
SET @fiResult = (@fiResult * @fiRiskProb) / (@fiI * 2) 
CLOSE probability_cursor
DEALLOCATE probability_cursor

RETURN @fiResult
END
GO

CREATE FUNCTION get_risk_value_residual  ( @piFkRisk NUMERIC )  
RETURNS FLOAT
AS 
BEGIN 

DECLARE @fsName VARCHAR(256)
DECLARE @fiAssetValue FLOAT

DECLARE @fiRiskProb FLOAT
DECLARE @fiProbReduction FLOAT
DECLARE @fiProbResult FLOAT

DECLARE @fiResult FLOAT
DECLARE @fiI INTEGER
DECLARE @fiBlueRisk INTEGER
DECLARE @fiParameterID INTEGER

DECLARE @fiCountRisk INTEGER
DECLARE @fiCountAsset INTEGER
DECLARE @fiCountSystem INTEGER

SET @fiI = 0
SET @fiResult = 0
SET @fiBlueRisk = 0

DECLARE compare_cursor CURSOR
FOR 
  SELECT count_sys, count_risk, count_asset FROM 
  (SELECT count(pkparametername) as count_sys  FROM rm_parameter_name pn) buffer_sys,
  (SELECT count(fkparametername) as count_risk FROM rm_risk_value WHERE fkrisk = @piFkRisk) buffer_risk,
  (SELECT count(fkparametername) as count_asset FROM rm_asset_value av
  JOIN rm_risk r ON (r.fkasset = av.fkasset)
  JOIN isms_context ctx1 ON(ctx1.pkContext = r.fkContext and ctx1.nState <> 2705 and r.fkcontext = @piFkRisk)
  JOIN isms_context ctx2 ON(ctx2.pkContext = r.fkAsset and ctx2.nState <> 2705)
  WHERE r.fkcontext = @piFkRisk) buffer_asset

DECLARE values_cursor CURSOR
FOR 
  SELECT pn.sname as parameter_name,  pn.pkparametername, pvn.nvalue as asset_value FROM rm_risk r
  JOIN isms_context ctx1 ON(ctx1.pkContext = r.fkContext and ctx1.nState <> 2705 and r.fkcontext = @piFkRisk)
  JOIN isms_context ctx2 ON(ctx2.pkContext = r.fkAsset and ctx2.nState <> 2705)
  JOIN rm_asset_value av ON (av.fkasset = r.fkasset)
  JOIN rm_parameter_name pn ON (av.fkparametername = pn.pkparametername)
  JOIN rm_parameter_value_name pvn ON (av.fkvaluename = pvn.pkvaluename)


DECLARE probability_cursor CURSOR
FOR

  SELECT prob_reduction_value, risk_prob_value FROM 
  (
       SELECT sum(rcpvn.nvalue) as prob_reduction_value from rm_risk_control rc
       JOIN isms_context ctx1 ON(ctx1.pkContext = rc.fkControl and ctx1.nState <> 2705)
       JOIN rm_control c ON(ctx1.pkContext = c.fkContext AND c.bIsActive = 1)
       JOIN isms_context ctx2 ON(ctx2.pkContext = rc.fkRisk and ctx2.nState <> 2705)
       JOIN rm_rc_parameter_value_name rcpvn ON (rc.fkprobabilityvaluename = rcpvn.pkrcvaluename)
       WHERE rc.fkrisk = @piFkRisk
  ) prv,
  (
       SELECT pvn.nvalue as risk_prob_value from rm_risk r
       JOIN isms_context ctx1 ON(ctx1.pkContext = r.fkContext and ctx1.nState <> 2705)
       JOIN rm_parameter_value_name pvn ON (r.fkprobabilityvaluename = pvn.pkvaluename)
       WHERE r.fkcontext = @piFkRisk
  ) rpv


OPEN compare_cursor
FETCH NEXT FROM compare_cursor INTO @fiCountSystem, @fiCountRisk, @fiCountAsset
	IF ( (@fiCountSystem <> @fiCountRisk) or (@fiCountSystem <> @fiCountAsset) )
	BEGIN
		SET @fiBlueRisk = 1
	END
CLOSE compare_cursor
DEALLOCATE compare_cursor

IF(@fiBlueRisk = 1)
BEGIN
	DEALLOCATE values_cursor
	DEALLOCATE probability_cursor
	RETURN 0
END

OPEN values_cursor
FETCH NEXT FROM values_cursor INTO @fsName, @fiParameterID, @fiAssetValue
WHILE (@@FETCH_STATUS = 0)
   BEGIN  
        SET @fiI = @fiI +1
        SET @fiResult = @fiResult + %database_login%.get_risk_parameter_reduction(@piFkRisk, @fiParameterID) + @fiAssetValue
        FETCH NEXT FROM values_cursor INTO @fsName, @fiParameterID, @fiAssetValue
   END
CLOSE values_cursor
DEALLOCATE values_cursor


OPEN probability_cursor
FETCH NEXT FROM probability_cursor INTO @fiProbReduction, @fiRiskProb

IF(@fiProbReduction IS NULL) SET @fiProbResult = @fiRiskProb
ELSE 
BEGIN
	SET @fiProbResult = @fiRiskProb - @fiProbReduction
	IF( @fiProbResult < 1) SET @fiProbResult = 1
END

SET @fiResult = ( @fiResult * @fiProbResult ) / (@fiI * 2)
CLOSE probability_cursor
DEALLOCATE probability_cursor

RETURN @fiResult
END
GO

CREATE FUNCTION get_risks_from_asset (@asset_id INT)
RETURNS @risks TABLE (
    pkRisk INT
)
AS

BEGIN
    DECLARE @next_risk_id INT
    
    DECLARE risks_from_asset_cursor CURSOR FOR
        SELECT fkContext FROM rm_risk r
        JOIN isms_context ctx1 ON(ctx1.pkContext = r.fkContext and ctx1.nState <> 2705)
        JOIN isms_context ctx2 ON(ctx2.pkContext = r.fkAsset and ctx2.nState <> 2705 and r.fkAsset = @asset_id )

  OPEN risks_from_asset_cursor
    FETCH NEXT FROM risks_from_asset_cursor INTO @next_risk_id
    WHILE (@@FETCH_STATUS = 0)
    BEGIN
        IF (@next_risk_id IS NOT NULL)
        BEGIN
            INSERT INTO @risks values(@next_risk_id)
            FETCH NEXT FROM risks_from_asset_cursor INTO @next_risk_id
        END
    END
    CLOSE risks_from_asset_cursor
    DEALLOCATE risks_from_asset_cursor
    
    RETURN
END
GO

CREATE  function get_section_tree (@pnParent INTEGER, @pnLevel INTEGER)
RETURNS @section_tree TABLE
    (
    fkContext  int,
    sName VARCHAR(256) COLLATE SQL_Latin1_General_CP1_CI_AS,
    nLevel int
   )
AS
BEGIN
DECLARE section_tree_cursor CURSOR
   FOR SELECT fkContext, fkParent, sName FROM rm_section_best_practice 
   JOIN isms_context c ON(c.pkContext = fkContext and c.nState <> 2705)
   WHERE  fkParent = @pnParent

DECLARE root_tree_cursor CURSOR
   FOR SELECT fkContext, fkParent, sName FROM rm_section_best_practice 
   JOIN isms_context c ON(c.pkContext = fkContext and c.nState <> 2705)
   WHERE  fkParent is NULL

DECLARE @query CURSOR

DECLARE @section_id NUMERIC(18,0), @level INT, @parent_id NUMERIC(18,0), @section_name VARCHAR(128)

SET @level = @pnLevel + 1
if (@pnParent > 0)
   BEGIN SET @query = section_tree_cursor END
else
   BEGIN SET @query = root_tree_cursor END

OPEN @query
FETCH NEXT FROM @query INTO @section_id, @parent_id, @section_name
WHILE (@@FETCH_STATUS = 0 )
    BEGIN  
      INSERT INTO @section_tree  values( @section_id,  @section_name, @level)
      INSERT @section_tree
      select fkContext, sName, nLevel
      from %database_login%.get_section_tree( @section_id,  @level)
     FETCH NEXT FROM @query INTO @section_id, @parent_id, @section_name
   END

CLOSE @query
DEALLOCATE @query
DEALLOCATE root_tree_cursor
DEALLOCATE section_tree_cursor

RETURN
END
GO

CREATE FUNCTION get_section_parents (@section_id INT)
RETURNS @section_parents TABLE (
    pkSection INT
)
AS

BEGIN
    DECLARE @parent_section_id INT
    
    DECLARE section_parents_cursor CURSOR FOR
        SELECT fkParent FROM rm_section_best_practice 
        JOIN isms_context c ON(c.pkContext = fkContext and c.nState <> 2705)
        WHERE fkContext = @section_id
    
    OPEN section_parents_cursor
    FETCH NEXT FROM section_parents_cursor INTO @parent_section_id
    IF (@parent_section_id IS NOT NULL)
    BEGIN
        INSERT INTO @section_parents values(@parent_section_id)
        INSERT @section_parents SELECT pkSection FROM %database_login%.get_section_parents(@parent_section_id)
    END
    
    CLOSE section_parents_cursor
    DEALLOCATE section_parents_cursor
    
    RETURN
END
GO

CREATE FUNCTION get_sub_areas (@area_id INT)
RETURNS @sub_areas TABLE (
    pkArea INT
)
AS

BEGIN
    DECLARE @next_area_id INT
    
    DECLARE sub_areas_cursor CURSOR FOR
        SELECT fkContext FROM rm_area WHERE fkParent = @area_id
    
    OPEN sub_areas_cursor
    FETCH NEXT FROM sub_areas_cursor INTO @next_area_id
    WHILE (@@FETCH_STATUS = 0)
    BEGIN
        IF (@next_area_id IS NOT NULL)
        BEGIN
            INSERT INTO @sub_areas values(@next_area_id)
            INSERT @sub_areas SELECT pkArea FROM %database_login%.get_sub_areas(@next_area_id)
        END
        FETCH NEXT FROM sub_areas_cursor INTO @next_area_id
    END
    
    CLOSE sub_areas_cursor
    DEALLOCATE sub_areas_cursor
    
    RETURN
END
GO

CREATE FUNCTION get_sub_categories (@category_id INT)
RETURNS @sub_categories TABLE (
    pkCategory INT
)
AS

BEGIN
    DECLARE @next_category_id INT
    
    DECLARE sub_categoriess_cursor CURSOR FOR
        SELECT fkContext FROM rm_category WHERE fkParent = @category_id
    
    OPEN sub_categoriess_cursor
    FETCH NEXT FROM sub_categoriess_cursor INTO @next_category_id
    WHILE (@@FETCH_STATUS = 0)
    BEGIN
        IF (@next_category_id IS NOT NULL)
        BEGIN
            INSERT INTO @sub_categories values(@next_category_id)
            INSERT @sub_categories SELECT pkCategory FROM %database_login%.get_sub_categories(@next_category_id)
        END
        FETCH NEXT FROM sub_categoriess_cursor INTO @next_category_id
    END
    
    CLOSE sub_categoriess_cursor
    DEALLOCATE sub_categoriess_cursor
    
    RETURN
END
GO

CREATE FUNCTION get_sub_sections (@section_id INT)
RETURNS @sub_sections TABLE (
    pkSection INT
)
AS

BEGIN
    DECLARE @next_section_id INT
    
    DECLARE sub_sections_cursor CURSOR FOR
        SELECT fkContext FROM rm_section_best_practice WHERE fkParent = @section_id
    
    OPEN sub_sections_cursor
    FETCH NEXT FROM sub_sections_cursor INTO @next_section_id
    WHILE (@@FETCH_STATUS = 0)
    BEGIN
        IF (@next_section_id IS NOT NULL)
        BEGIN
            INSERT INTO @sub_sections values(@next_section_id)
            INSERT @sub_sections SELECT pkSection FROM %database_login%.get_sub_sections(@next_section_id)
        END
        FETCH NEXT FROM sub_sections_cursor INTO @next_section_id
    END
    
    CLOSE sub_sections_cursor
    DEALLOCATE sub_sections_cursor
    
    RETURN
END
GO

CREATE FUNCTION get_top10_area_risks ()
RETURNS @result TABLE (
    area_id INT,
    risk_id INT
)
AS

BEGIN   

    DECLARE @area_id INT
    DECLARE @risk_id INT    

    DECLARE valid_area CURSOR FOR
      	SELECT  ar.fkContext
	FROM isms_context cont_ar
	JOIN rm_area ar ON (cont_ar.pkContext = ar.fkContext AND cont_ar.nState <> 2705)    

    OPEN valid_area
    FETCH NEXT FROM valid_area INTO @area_id
    WHILE (@@FETCH_STATUS = 0)
    BEGIN
	DECLARE area_risks CURSOR FOR
		SELECT TOP 10 r.fkContext
		FROM isms_context cont_ar
		JOIN rm_area ar ON (cont_ar.pkContext = ar.fkContext AND cont_ar.nState <> 2705)   
		JOIN rm_process p ON (ar.fkContext = p.fkArea)
		JOIN isms_context cont_p ON (cont_p.pkContext = p.fkContext AND cont_p.nState <> 2705)
		JOIN rm_process_asset pa ON (p.fkContext = pa.fkProcess)
		JOIN rm_asset a ON (pa.fkAsset = a.fkContext)
		JOIN isms_context cont_a ON (cont_a.pkContext = a.fkContext AND cont_a.nState <> 2705)
		JOIN rm_risk r ON (a.fkContext = r.fkAsset)
		JOIN isms_context cont_r ON (r.fkContext = cont_r.pkContext AND cont_r.nState <> 2705)
		WHERE ar.fkContext = @area_id
		GROUP BY r.fkContext, r.nValueResidual
		ORDER BY r.nValueResidual DESC

	OPEN area_risks
	FETCH NEXT FROM area_risks INTO @risk_id
	 WHILE (@@FETCH_STATUS = 0)
	BEGIN
		 INSERT INTO @result values(@area_id,@risk_id)
		 FETCH NEXT FROM area_risks INTO @risk_id
	END			

	CLOSE area_risks
	DEALLOCATE area_risks

	FETCH NEXT FROM valid_area INTO @area_id	
    END  

    CLOSE valid_area
    DEALLOCATE valid_area
    
    RETURN
END
GO

CREATE FUNCTION get_top10_process_risks ()
RETURNS @result TABLE (
    process_id INT,
    risk_id INT
)
AS

BEGIN   

    DECLARE @process_id INT
    DECLARE @risk_id INT    

    DECLARE valid_processes CURSOR FOR
      	SELECT  p.fkContext
	FROM isms_context cont_p
	JOIN rm_process p ON (cont_p.pkContext = p.fkContext AND cont_p.nState <> 2705)    

    OPEN valid_processes
    FETCH NEXT FROM valid_processes INTO @process_id
    WHILE (@@FETCH_STATUS = 0)
    BEGIN
	DECLARE process_risks CURSOR FOR
		SELECT TOP 10 r.fkContext
		FROM isms_context cont_p
		JOIN rm_process p ON (cont_p.pkContext = p.fkContext AND cont_p.nState <> 2705)
		JOIN rm_process_asset pa ON (p.fkContext = pa.fkProcess)
		JOIN rm_asset a ON (pa.fkAsset = a.fkContext)
		JOIN isms_context cont_a ON (cont_a.pkContext = a.fkContext AND cont_a.nState <> 2705)
		JOIN rm_risk r ON (a.fkContext = r.fkAsset)
		JOIN isms_context cont_r ON (r.fkContext = cont_r.pkContext AND cont_r.nState <> 2705)
		WHERE p.fkContext = @process_id		
		ORDER BY r.nValueResidual DESC

	OPEN process_risks
	FETCH NEXT FROM process_risks INTO @risk_id
	 WHILE (@@FETCH_STATUS = 0)
	BEGIN
		 INSERT INTO @result values(@process_id,@risk_id)
		 FETCH NEXT FROM process_risks INTO @risk_id
	END			

	CLOSE process_risks
	DEALLOCATE process_risks

	FETCH NEXT FROM valid_processes INTO @process_id	
    END  

    CLOSE valid_processes
    DEALLOCATE valid_processes
    
    RETURN
END
GO

CREATE FUNCTION get_asset_dependencies (@asset_id INT)
RETURNS @asset_dependencies TABLE (
    pkAsset INT
)
AS
BEGIN
    DECLARE @dependency_asset_id INT
   
    DECLARE asset_direct_dependencies_cursor CURSOR FOR
	SELECT fkAsset FROM view_rm_asset_asset_active  WHERE fkDependent = @asset_id
    
    OPEN asset_direct_dependencies_cursor
    FETCH NEXT FROM asset_direct_dependencies_cursor INTO @dependency_asset_id
    WHILE (@@FETCH_STATUS = 0)
    BEGIN	
	INSERT INTO @asset_dependencies VALUES(@dependency_asset_id) 
	INSERT @asset_dependencies SELECT pkAsset FROM %database_login%.get_asset_dependencies(@dependency_asset_id)
	FETCH NEXT FROM asset_direct_dependencies_cursor INTO @dependency_asset_id
    END

    CLOSE asset_direct_dependencies_cursor
    DEALLOCATE asset_direct_dependencies_cursor
    
    RETURN
END
GO

CREATE FUNCTION get_asset_dependents (@asset_id INT)
RETURNS @asset_dependents TABLE (
    pkAsset INT
)
AS
BEGIN
    DECLARE @dependent_asset_id INT
   
    DECLARE asset_direct_dependents_cursor CURSOR FOR
	SELECT fkDependent FROM view_rm_asset_asset_active  WHERE fkAsset = @asset_id
    
    OPEN asset_direct_dependents_cursor
    FETCH NEXT FROM asset_direct_dependents_cursor INTO @dependent_asset_id
    WHILE (@@FETCH_STATUS = 0)
    BEGIN	
	INSERT INTO @asset_dependents VALUES(@dependent_asset_id) 
	INSERT @asset_dependents SELECT pkAsset FROM %database_login%.get_asset_dependents(@dependent_asset_id)
	FETCH NEXT FROM asset_direct_dependents_cursor INTO @dependent_asset_id
    END

    CLOSE asset_direct_dependents_cursor
    DEALLOCATE asset_direct_dependents_cursor
    
    RETURN
END
GO

CREATE FUNCTION get_asset_dependent_by_index (@asset_id INT, @index INT)
RETURNS integer
AS
BEGIN

   DECLARE @ID INTEGER
   DECLARE @count INTEGER
   DECLARE @asset_id2 INTEGER
   SET @count = 0
   SET @ID = 0

   DECLARE dependents CURSOR FOR
	SELECT fkDependent FROM view_rm_asset_asset_active  WHERE fkAsset = @asset_id
    
    OPEN dependents
    FETCH NEXT FROM dependents INTO @asset_id2
    WHILE (@@FETCH_STATUS = 0)
    BEGIN	
	IF (@count = @index)
	    SET @ID = @asset_id2
	SET @count = @count +1
             FETCH NEXT FROM dependents INTO @asset_id2
    END

    CLOSE dependents
    DEALLOCATE dependents
    
    RETURN @ID
END
GO

CREATE FUNCTION get_area_parents_trash (@area_id INT)
RETURNS @area_parents TABLE (
    pkArea INT
)
AS

BEGIN
    DECLARE @parent_area_id INT
    
    DECLARE area_parents_cursor CURSOR FOR
        SELECT fkParent FROM rm_area 
        WHERE fkContext = @area_id
    
    OPEN area_parents_cursor
    FETCH NEXT FROM area_parents_cursor INTO @parent_area_id
    IF (@parent_area_id IS NOT NULL)
    BEGIN
        INSERT INTO @area_parents values(@parent_area_id)
        INSERT @area_parents SELECT pkArea FROM %database_login%.get_area_parents_trash(@parent_area_id)
    END
    
    CLOSE area_parents_cursor
    DEALLOCATE area_parents_cursor
    
    RETURN
END
GO

CREATE FUNCTION get_category_parents_trash (@category_id INT)
RETURNS @category_parents TABLE (
    pkCategory INT
)
AS

BEGIN
    DECLARE @parent_category_id INT
    
    DECLARE category_parents_cursor CURSOR FOR
        SELECT fkParent FROM rm_category 
        WHERE fkContext = @category_id
    
    OPEN category_parents_cursor
    FETCH NEXT FROM category_parents_cursor INTO @parent_category_id
    IF (@parent_category_id IS NOT NULL)
    BEGIN
        INSERT INTO @category_parents values(@parent_category_id)
        INSERT @category_parents SELECT pkCategory FROM %database_login%.get_category_parents_trash(@parent_category_id)
    END
    
    CLOSE category_parents_cursor
    DEALLOCATE category_parents_cursor
    
    RETURN
END
GO

CREATE FUNCTION get_section_parents_trash (@section_id INT)
RETURNS @section_parents TABLE (
    pkSection INT
)
AS

BEGIN
    DECLARE @parent_section_id INT
    
    DECLARE section_parents_cursor CURSOR FOR
        SELECT fkParent FROM rm_section_best_practice 
        WHERE fkContext = @section_id
    
    OPEN section_parents_cursor
    FETCH NEXT FROM section_parents_cursor INTO @parent_section_id
    IF (@parent_section_id IS NOT NULL)
    BEGIN
        INSERT INTO @section_parents values(@parent_section_id)
        INSERT @section_parents SELECT pkSection FROM %database_login%.get_section_parents_trash(@parent_section_id)
    END
    
    CLOSE section_parents_cursor
    DEALLOCATE section_parents_cursor
    
    RETURN
END
GO

CREATE function get_document_tree (@pnParent INTEGER, @pnLevel INTEGER)
RETURNS @document_tree TABLE
    (
    fkContext  int,
    sName    VARCHAR(256) COLLATE SQL_Latin1_General_CP1_CI_AS,
    nLevel int
   )
AS
BEGIN
DECLARE document_tree_cursor CURSOR
   FOR SELECT fkContext, fkParent, sName FROM view_pm_document_active  WHERE  fkParent = @pnParent

DECLARE root_tree_cursor CURSOR
   FOR SELECT fkContext, fkParent, sName FROM view_pm_document_active  WHERE  fkParent IS NULL

DECLARE @query CURSOR

DECLARE @document_id NUMERIC(18,0), @level INT, @parent_id NUMERIC(18,0), @document_name VARCHAR(128)

SET @level = @pnLevel + 1
if (@pnParent > 0)
   BEGIN SET @query = document_tree_cursor END
else
   BEGIN SET @query = root_tree_cursor END

OPEN @query
FETCH NEXT FROM @query INTO @document_id, @parent_id, @document_name
WHILE (@@FETCH_STATUS = 0 )
    BEGIN  
      INSERT INTO @document_tree  VALUES (@document_id,  @document_name, @level)
      INSERT @document_tree
	  SELECT fkContext, sName, nLevel
	  FROM %database_login%.get_document_tree(@document_id,  @level)
     FETCH NEXT FROM @query INTO @document_id, @parent_id, @document_name
   END

CLOSE @query
DEALLOCATE @query
DEALLOCATE root_tree_cursor
DEALLOCATE document_tree_cursor

RETURN
END
GO

CREATE   FUNCTION get_document_parents (@document_id INT)
RETURNS @document_parents TABLE (
    document_id INT
)
AS

BEGIN
    DECLARE @parent_document_id INT
    
    DECLARE document_parents_cursor CURSOR FOR
        SELECT fkParent FROM pm_document 
        JOIN isms_context c ON(c.pkContext = fkParent and c.nState <> 2705)
        WHERE fkContext = @document_id
    
    OPEN document_parents_cursor
    FETCH NEXT FROM document_parents_cursor INTO @parent_document_id
    IF (@parent_document_id IS NOT NULL)
    BEGIN
        INSERT INTO @document_parents values(@parent_document_id)
        INSERT @document_parents SELECT document_id FROM %database_login%.get_document_parents(@parent_document_id)
    END
    
    CLOSE document_parents_cursor
    DEALLOCATE document_parents_cursor
    
    RETURN
END
GO

CREATE  FUNCTION get_document_parents_trash (@document_id INT)
RETURNS @document_parents TABLE (
    document_id INT
)
AS

BEGIN
    DECLARE @parent_document_id INT
    
    DECLARE document_parents_cursor CURSOR FOR
        SELECT fkParent FROM pm_document 
        WHERE fkContext = @document_id
    
    OPEN document_parents_cursor
    FETCH NEXT FROM document_parents_cursor INTO @parent_document_id
    IF (@parent_document_id IS NOT NULL)
    BEGIN
        INSERT INTO @document_parents values(@parent_document_id)
        INSERT @document_parents SELECT document_id FROM %database_login%.get_document_parents_trash(@parent_document_id)
    END
    
    CLOSE document_parents_cursor
    DEALLOCATE document_parents_cursor
    
    RETURN
END
GO

CREATE FUNCTION get_sub_documents (@document_id INT)
RETURNS @sub_documents TABLE (
    document_id INT
)
AS

BEGIN
    DECLARE @next_document_id INT
    
    DECLARE sub_documents_cursor CURSOR FOR
        SELECT fkContext FROM pm_document WHERE fkParent = @document_id
    
    OPEN sub_documents_cursor
    FETCH NEXT FROM sub_documents_cursor INTO @next_document_id
    WHILE (@@FETCH_STATUS = 0)
    BEGIN
        IF (@next_document_id IS NOT NULL)
        BEGIN
            INSERT INTO @sub_documents values(@next_document_id)
            INSERT @sub_documents SELECT document_id FROM %database_login%.get_sub_documents(@next_document_id)
        END
        FETCH NEXT FROM sub_documents_cursor INTO @next_document_id
    END
    
    CLOSE sub_documents_cursor
    DEALLOCATE sub_documents_cursor
    
    RETURN
END
GO

CREATE FUNCTION get_topN_documented_areas(@piTruncateNumber INT)
RETURNS @result TABLE (
  fkContext INT,
  sName VARCHAR(256),
  nValue FLOAT,
  doc_count INT
)
AS

BEGIN
  DECLARE @miArea INT
  DECLARE @msName VARCHAR(256)
  DECLARE @mfValue FLOAT
  DECLARE @miDocCount INT
  DECLARE @miCounter INT

  DECLARE topN CURSOR FOR
    SELECT
      a.fkContext,
      a.sName,
      a.nValue,
      count(dc.fkDocument) as doc_count
    FROM
      view_rm_area_active a
      LEFT JOIN view_pm_doc_context_active dc ON (a.fkContext = dc.fkContext)
    GROUP BY a.fkContext, a.sName, a.nValue
    ORDER BY doc_count DESC, a.sName
    
  OPEN topN
  SET @miCounter = 0
  FETCH NEXT FROM topN INTO @miArea, @msName, @mfValue, @miDocCount
  WHILE(@@FETCH_STATUS = 0 AND (@piTruncateNumber = 0 OR @miCounter < @piTruncateNumber)) BEGIN
    INSERT INTO @result VALUES(@miArea, @msName, @mfValue, @miDocCount)
    FETCH NEXT FROM topN INTO @miArea, @msName, @mfValue, @miDocCount
    SET @miCounter = @miCounter + 1
  END

  CLOSE topN
  DEALLOCATE topN

  RETURN
END

GO

CREATE FUNCTION get_topN_documented_processes(@piTruncateNumber INT)
RETURNS @result TABLE (
  fkContext INT,
  sName VARCHAR(256),
  nValue FLOAT,
  doc_count INT
)
AS

BEGIN
  DECLARE @miProcess INT
  DECLARE @msName VARCHAR(256)
  DECLARE @mfValue FLOAT
  DECLARE @miDocCount INT
  DECLARE @miCounter INT

  DECLARE topN CURSOR FOR
    SELECT
      p.fkContext,
      p.sName,
      p.nValue,
      count(dc.fkDocument) as doc_count
    FROM
      view_rm_process_active p
      LEFT JOIN view_pm_doc_context_active dc ON (p.fkContext = dc.fkContext)
    GROUP BY p.fkContext, p.sName, p.nValue
    ORDER BY doc_count DESC, p.sName
    
  OPEN topN
  SET @miCounter = 0
  FETCH NEXT FROM topN INTO @miProcess, @msName, @mfValue, @miDocCount
  WHILE(@@FETCH_STATUS = 0 AND (@piTruncateNumber = 0 OR @miCounter < @piTruncateNumber)) BEGIN
    INSERT INTO @result VALUES(@miProcess, @msName, @mfValue, @miDocCount)
    FETCH NEXT FROM topN INTO @miProcess, @msName, @mfValue, @miDocCount
    SET @miCounter = @miCounter + 1
  END

  CLOSE topN
  DEALLOCATE topN

  RETURN
END

GO

CREATE FUNCTION get_ctx_names_by_doc_id (@ctx_doc_id INT)
RETURNS VARCHAR(256)
AS
BEGIN
  DECLARE @ctx_names VARCHAR(256)
  DECLARE @ctx_aux_name VARCHAR(256)
  SET @ctx_names = ''

  DECLARE ctx_name_cursor CURSOR FOR
    SELECT context_name 
      FROM pm_doc_context d_c 
      JOIN context_names c_n ON (c_n.context_id = d_c.fkContext) 
      JOIN isms_context c ON (c.pkContext = d_c.fkContext AND c.nState <> 2705 ) 
      WHERE d_c.fkDocument = @ctx_doc_id
    UNION
    SELECT '%context_scope%' as context_name
      FROM pm_doc_context d_c
      JOIN view_isms_scope_active c_n ON (c_n.fkContext = d_c.fkContext) 
      JOIN isms_context c ON (c.pkContext = d_c.fkContext AND c.nState <> 2705 ) 
      WHERE d_c.fkDocument = @ctx_doc_id
    UNION
    SELECT '%context_policy%' as context_name
      FROM pm_doc_context d_c 
      JOIN view_isms_policy_active c_n ON (c_n.fkContext = d_c.fkContext) 
      JOIN isms_context c ON (c.pkContext = d_c.fkContext AND c.nState <> 2705 ) 
      WHERE d_c.fkDocument = @ctx_doc_id

  OPEN ctx_name_cursor
    FETCH NEXT FROM ctx_name_cursor INTO @ctx_aux_name
    WHILE (@@FETCH_STATUS = 0)
      BEGIN
        IF ((@ctx_aux_name IS NOT NULL) AND (@ctx_aux_name <> ''))
            IF(@ctx_names <> '')
              SET @ctx_names = @ctx_names + ', ' + @ctx_aux_name
            ELSE
              SET @ctx_names = @ctx_aux_name
        FETCH NEXT FROM ctx_name_cursor INTO @ctx_aux_name
      END
  CLOSE ctx_name_cursor
  DEALLOCATE ctx_name_cursor

  RETURN @ctx_names
END

GO

CREATE FUNCTION get_next_date(@pdLastTime DATETIME, @piPeriodUnitId INTEGER, @piPeriodsCount INTEGER) RETURNS DATETIME AS
BEGIN
  IF @piPeriodUnitId = 7801 BEGIN
    -- SCHEDULE_BYDAY
    RETURN DATEADD(day,@piPeriodsCount,@pdLastTime)
  END ELSE IF @piPeriodUnitId = 7802 BEGIN
    -- SCHEDULE_BYWEEK
    RETURN DATEADD(day,@piPeriodsCount*7,@pdLastTime)
  END ELSE IF @piPeriodUnitId = 7803 BEGIN
    RETURN DATEADD(month,@piPeriodsCount,@pdLastTime)
  END ELSE BEGIN
    RETURN NULL
  END
  RETURN NULL
END
GO

CREATE FUNCTION get_topN_revised_documents(@piTruncateNumber INT)
RETURNS @result TABLE (
  fkContext INT,
  sName VARCHAR(256),
  fkCurrentVersion INT,
  rev_count INT
)
AS

BEGIN
  DECLARE @miDocument INT
  DECLARE @msName VARCHAR(256)
  DECLARE @miCurrentVersion INT
  DECLARE @miRevCount INT
  DECLARE @miCounter INT

  DECLARE topN CURSOR FOR
    SELECT
      d.fkContext,
      d.sName,
      d.fkCurrentVersion,
      count(di.fkContext) as rev_count
    FROM
      view_pm_document_active d
      JOIN view_pm_doc_instance_active di ON (di.fkDocument = d.fkContext 
                AND di.nMajorVersion>1 
                AND di.dBeginProduction IS NOT NULL)
    GROUP BY d.fkContext, d.sName, d.fkCurrentVersion
    ORDER BY rev_count DESC, d.sName
    
  OPEN topN
  SET @miCounter = 0
  FETCH NEXT FROM topN INTO @miDocument, @msName, @miCurrentVersion, @miRevCount
  WHILE(@@FETCH_STATUS = 0 AND (@piTruncateNumber = 0 OR @miCounter < @piTruncateNumber)) BEGIN
    INSERT INTO @result VALUES(@miDocument, @msName, @miCurrentVersion, @miRevCount)
    FETCH NEXT FROM topN INTO @miDocument, @msName, @miCurrentVersion, @miRevCount
    SET @miCounter = @miCounter + 1
  END

  CLOSE topN
  DEALLOCATE topN

  RETURN
END

GO

CREATE FUNCTION get_assets_from_category (@category_id INT)
RETURNS @assets TABLE (
    pkAsset INT
)
AS

BEGIN
    DECLARE @next_category_id INT
    DECLARE @next_asset_id INT
    
    DECLARE sub_categories_cursor CURSOR FOR
        SELECT fkContext
        FROM rm_category
        JOIN isms_context c ON(c.pkContext = fkContext and c.nState <> 2705)
        WHERE fkParent = @category_id
    
    DECLARE assets_from_category_cursor CURSOR FOR
        SELECT fkContext
        FROM rm_asset
        JOIN isms_context c ON(c.pkContext = fkContext and c.nState <> 2705)
        WHERE fkCategory = @category_id
    
    OPEN assets_from_category_cursor
    FETCH NEXT FROM assets_from_category_cursor INTO @next_asset_id
    WHILE (@@FETCH_STATUS = 0)
    BEGIN
        IF (@next_asset_id IS NOT NULL)
        BEGIN
            INSERT INTO @assets values(@next_asset_id)
            FETCH NEXT FROM assets_from_category_cursor INTO @next_asset_id
        END
    END
    CLOSE assets_from_category_cursor
    DEALLOCATE assets_from_category_cursor
    
    OPEN sub_categories_cursor
    FETCH NEXT FROM sub_categories_cursor INTO @next_category_id
    WHILE (@@FETCH_STATUS = 0)
    BEGIN
        IF (@next_category_id IS NOT NULL)
        BEGIN
            INSERT @assets SELECT pkAsset FROM %database_login%.get_assets_from_category(@next_category_id)
        END
        FETCH NEXT FROM sub_categories_cursor INTO @next_category_id
    END
    CLOSE sub_categories_cursor
    DEALLOCATE sub_categories_cursor
    
    RETURN
END
GO

CREATE PROCEDURE delete_area (@area_id INT)
AS
BEGIN
    DECLARE @next_area_id INT
    DECLARE @area_cursor CURSOR
    
    SET @area_cursor = CURSOR FORWARD_ONLY STATIC FOR
        SELECT fkContext
        FROM rm_area
        WHERE fkParent = @area_id
    
    OPEN @area_cursor
    FETCH NEXT FROM @area_cursor INTO @next_area_id
    WHILE (@@FETCH_STATUS = 0)
    BEGIN
       exec %database_login%.delete_area @next_area_id
       FETCH NEXT FROM @area_cursor INTO @next_area_id
    END
    CLOSE @area_cursor
    DEALLOCATE @area_cursor
    
    DELETE from rm_area WHERE fkContext = @area_id
    DELETE from isms_context WHERE pkContext = @area_id
END
GO

CREATE PROCEDURE delete_category (@category_id INT)
AS
BEGIN
    DECLARE @next_category_id INT
    DECLARE @category_cursor CURSOR
    
    SET @category_cursor = CURSOR FORWARD_ONLY STATIC FOR
        SELECT fkContext
        FROM rm_category
        WHERE fkParent = @category_id
    
    OPEN @category_cursor
    FETCH NEXT FROM @category_cursor INTO @next_category_id
    WHILE (@@FETCH_STATUS = 0)
    BEGIN
       exec %database_login%.delete_category @next_category_id
       FETCH NEXT FROM @category_cursor INTO @next_category_id
    END
    CLOSE @category_cursor
    DEALLOCATE @category_cursor
    
    DELETE from rm_category WHERE fkContext = @category_id
    DELETE from isms_context WHERE pkContext = @category_id
END
GO

CREATE PROCEDURE delete_section (@section_id INT)
AS
BEGIN
    DECLARE @next_section_id INT
    DECLARE @section_cursor CURSOR
    
    SET @section_cursor = CURSOR FORWARD_ONLY STATIC FOR
        SELECT fkContext
        FROM rm_section_best_practice
        WHERE fkParent = @section_id
    
    OPEN @section_cursor
    FETCH NEXT FROM @section_cursor INTO @next_section_id
    WHILE (@@FETCH_STATUS = 0)
    BEGIN
       exec %database_login%.delete_section @next_section_id
       FETCH NEXT FROM @section_cursor INTO @next_section_id
    END
    CLOSE @section_cursor
    DEALLOCATE @section_cursor
    
    DELETE from rm_section_best_practice WHERE fkContext = @section_id
    DELETE from isms_context WHERE pkContext = @section_id
END
GO

CREATE PROCEDURE delete_document (@document_id INT)
AS
BEGIN
    DECLARE @next_document_id INT
    DECLARE @document_cursor CURSOR
    
    SET @document_cursor = CURSOR FORWARD_ONLY STATIC FOR
        SELECT fkContext
        FROM pm_document
        WHERE fkParent = @document_id
    
    OPEN @document_cursor
    FETCH NEXT FROM @document_cursor INTO @next_document_id
    WHILE (@@FETCH_STATUS = 0)
    BEGIN
       exec %database_login%.delete_document @next_document_id
       FETCH NEXT FROM @document_cursor INTO @next_document_id
    END
    CLOSE @document_cursor
    DEALLOCATE @document_cursor
    
    DELETE from pm_document WHERE fkContext = @document_id
    DELETE from isms_context WHERE pkContext = @document_id
END
GO

CREATE PROCEDURE create_stats
AS
BEGIN

/*======================================TESTE PARA VER SE NECESSITA EXECUTAR A FUN��O=========================================*/

DECLARE @periodicy_value INT
SET @periodicy_value = (SELECT sValue FROM isms_config WHERE pkConfig = 417)

DECLARE @periodicy_type INT
SET @periodicy_type = (SELECT sValue FROM isms_config WHERE pkConfig = 418)

DECLARE @date_last_run DATETIME
SET @date_last_run = (SELECT sValue FROM isms_config WHERE pkConfig = 420)

IF %database_login%.get_next_date(@date_last_run, @periodicy_type, @periodicy_value) <= getdate()
BEGIN

/*===================================================VARI�VEIS GLOBAIS===========================================================*/

DECLARE @risk_low INT
DECLARE @risk_high INT

SELECT
  @risk_low = rl.nLow,
  @risk_high = rl.nHigh
FROM
  rm_risk_limits rl
  JOIN isms_context c ON (rl.fkContext = c.pkContext)
WHERE c.nState = 2702

/*===============================================INFORMA��ES SOBRE �REA========================================================*/

-- Valor da �rea

DECLARE @area_id INT
DECLARE @area_value FLOAT

DECLARE area_value_cursor CURSOR FOR
	SELECT fkContext, nValue FROM view_rm_area_active

OPEN area_value_cursor
FETCH NEXT FROM area_value_cursor INTO @area_id, @area_value
WHILE (@@FETCH_STATUS = 0)
BEGIN
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (@area_id, getdate(), 'area_value', @area_value)
	FETCH NEXT FROM area_value_cursor INTO @area_id, @area_value
END
CLOSE area_value_cursor
DEALLOCATE area_value_cursor

-- N� total de �reas

DECLARE  @areas_total INT
SET @areas_total = (SELECT count(*) FROM view_rm_area_active)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'areas_total', @areas_total)

-- N� �reas com risco alto

DECLARE  @areas_with_high_risk INT
SET @areas_with_high_risk = (SELECT count(*) FROM view_rm_area_active WHERE nValue >= @risk_high)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'areas_with_high_risk', @areas_with_high_risk)

-- N� �reas com risco m�dio

DECLARE  @areas_with_medium_risk INT
SET @areas_with_medium_risk = (SELECT count(*) FROM view_rm_area_active WHERE nValue > @risk_low AND nValue < @risk_high)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'areas_with_medium_risk', @areas_with_medium_risk)

-- N� �reas com risco baixo

DECLARE  @areas_with_low_risk INT
SET @areas_with_low_risk = (SELECT count(*) FROM view_rm_area_active WHERE nValue <= @risk_low AND nValue <> 0)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'areas_with_low_risk', @areas_with_low_risk)

-- N� �reas com risco n�o parametrizado

DECLARE  @areas_with_no_value_risk INT
SET @areas_with_no_value_risk = (SELECT count(*) FROM view_rm_area_active WHERE nValue = 0)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'areas_with_no_value_risk', @areas_with_no_value_risk)

-- N� �reas completas

DECLARE  @complete_areas INT
SET @complete_areas = (
	SELECT count(*) FROM view_rm_area_active a
	WHERE a.nValue > 0 AND NOT EXISTS (SELECT * FROM view_rm_process_active p WHERE p.fkArea = a.fkContext AND p.nValue=0)
)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'complete_areas', @complete_areas)

-- N� �reas parcias

DECLARE  @partial_areas INT
SET @partial_areas = (
	SELECT count(*) FROM view_rm_area_active a
	WHERE a.nValue > 0 AND EXISTS (SELECT * FROM view_rm_process_active p WHERE p.fkArea = a.fkContext AND p.nValue = 0)
)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'partial_areas', @partial_areas)

-- N� �reas n�o gerenciadas

DECLARE  @not_managed_areas INT
SET @not_managed_areas = (SELECT count(*) FROM view_rm_area_active a WHERE a.nValue = 0)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'not_managed_areas', @not_managed_areas)

-- N� �reas sem processos

DECLARE  @areas_without_processes INT
SET @areas_without_processes = (
	SELECT count(*)
	FROM view_rm_area_active a
	WHERE a.fkContext NOT IN (SELECT fkArea FROM view_rm_process_active)
)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'areas_without_processes', @areas_without_processes)

-- Total de impacto das �reas e qtd. de riscos por �rea

DECLARE @total_area_impact FLOAT
DECLARE @risk_amount_by_area INT

DECLARE total_area_impact_and_risk_amount_by_area CURSOR FOR
	SELECT area_id, count(risk_id) as risk_count, CASE WHEN sum(r.nCost) IS NULL THEN 0 ELSE sum(r.nCost) END as total_impact
	FROM
	(
		SELECT ar.fkContext as area_id, r.fkContext as risk_id
		FROM view_rm_area_active ar
		LEFT JOIN view_rm_process_active p ON (ar.fkContext = p.fkArea)
		LEFT JOIN view_rm_process_asset_active pa ON (p.fkContext = pa.fkProcess)
		LEFT JOIN view_rm_asset_active a ON (pa.fkAsset = a.fkContext)
		LEFT JOIN view_rm_risk_active r ON (a.fkContext = r.fkAsset)
		GROUP BY ar.fkContext, r.fkContext
	) res
	LEFT JOIN view_rm_risk_active r ON (res.risk_id = r.fkContext)
	GROUP BY area_id

OPEN total_area_impact_and_risk_amount_by_area
FETCH NEXT FROM total_area_impact_and_risk_amount_by_area INTO @area_id, @risk_amount_by_area, @total_area_impact
WHILE (@@FETCH_STATUS = 0)
BEGIN
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (@area_id, getdate(), 'risk_amount_by_area', @risk_amount_by_area)
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (@area_id, getdate(), 'total_area_impact', @total_area_impact)
	FETCH NEXT FROM total_area_impact_and_risk_amount_by_area INTO @area_id, @risk_amount_by_area, @total_area_impact
END
CLOSE total_area_impact_and_risk_amount_by_area
DEALLOCATE total_area_impact_and_risk_amount_by_area

-- Custo e quantidade dos controles por �rea

DECLARE @control_cost_by_area FLOAT
DECLARE @control_amount_by_area INT

DECLARE control_amount_cost_by_area CURSOR FOR
	/*Calcula o custo total dos controles de cada �rea e a quantidade de controles de cada �rea*/
	SELECT res.area_id, count(control_id) as control_count, CASE WHEN sum(cc.nCost1+cc.nCost2+cc.nCost3+cc.nCost4+cc.nCost5) IS NULL THEN 0 ELSE sum(cc.nCost1+cc.nCost2+cc.nCost3+cc.nCost4+cc.nCost5) END as control_cost
	FROM
	(
		/*Garante que um controle s� ir� ser contabilizado uma vez para uma determinada �rea*/
		SELECT ar.fkContext as area_id, rc.fkControl as control_id
		FROM view_rm_area_active ar
		LEFT JOIN view_rm_process_active p ON (ar.fkContext = p.fkArea)
		LEFT JOIN view_rm_process_asset_active pa ON (p.fkContext = pa.fkProcess)
		LEFT JOIN view_rm_asset_active a ON (pa.fkAsset = a.fkContext)
		LEFT JOIN view_rm_risk_active r ON (a.fkContext = r.fkAsset)
		LEFT JOIN view_rm_risk_control_active rc ON (r.fkContext = rc.fkRisk)
		GROUP BY ar.fkContext, rc.fkControl
	) res
	LEFT JOIN rm_control_cost cc ON (res.control_id = cc.fkControl)
	GROUP BY area_id

OPEN control_amount_cost_by_area
FETCH NEXT FROM control_amount_cost_by_area INTO @area_id, @control_amount_by_area, @control_cost_by_area
WHILE (@@FETCH_STATUS = 0)
BEGIN
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (@area_id, getdate(), 'control_amount_by_area', @control_amount_by_area)
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (@area_id, getdate(), 'control_cost_by_area', @control_cost_by_area)	
	FETCH NEXT FROM control_amount_cost_by_area INTO @area_id, @control_amount_by_area, @control_cost_by_area
END
CLOSE control_amount_cost_by_area
DEALLOCATE control_amount_cost_by_area

-- Qtd. de processos de uma �rea

DECLARE @process_amount_by_area INT

DECLARE process_amount_by_area CURSOR FOR
	SELECT a.fkContext as area_id, count(p.fkContext) as process_count
	FROM view_rm_area_active a
	LEFT JOIN view_rm_process_active p ON (a.fkContext = p.fkArea)
	GROUP BY a.fkContext

OPEN process_amount_by_area
FETCH NEXT FROM process_amount_by_area INTO @area_id, @process_amount_by_area
WHILE (@@FETCH_STATUS = 0)
BEGIN
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (@area_id, getdate(), 'process_amount_by_area', @process_amount_by_area)
	FETCH NEXT FROM process_amount_by_area INTO @area_id, @process_amount_by_area
END
CLOSE process_amount_by_area
DEALLOCATE process_amount_by_area

-- Qtd. e custo dos ativos de uma �rea

DECLARE @asset_cost_by_area FLOAT
DECLARE @asset_amount_by_area INT

DECLARE asset_amount_cost_by_area CURSOR FOR
	SELECT area_id, count(asset_id) as asset_count, CASE WHEN sum(a.nCost) IS NULL THEN 0 ELSE sum(a.nCost) END as asset_cost
	FROM
	(
		SELECT a.fkContext as area_id, pa.fkAsset as asset_id
		FROM view_rm_area_active a
		LEFT JOIN view_rm_process_active p ON (a.fkContext = p.fkArea)
		LEFT JOIN view_rm_process_asset_active pa ON (p.fkContext = pa.fkProcess)
		GROUP BY a.fkContext, pa.fkAsset
	) res
	LEFT JOIN view_rm_asset_active a ON (res.asset_id = a.fkContext)
	GROUP BY area_id

OPEN asset_amount_cost_by_area
FETCH NEXT FROM asset_amount_cost_by_area INTO @area_id, @asset_amount_by_area, @asset_cost_by_area
WHILE (@@FETCH_STATUS = 0)
BEGIN
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (@area_id, getdate(), 'asset_amount_by_area', @asset_amount_by_area)
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (@area_id, getdate(), 'asset_cost_by_area', @asset_cost_by_area)
	FETCH NEXT FROM asset_amount_cost_by_area INTO @area_id, @asset_amount_by_area, @asset_cost_by_area
END
CLOSE asset_amount_cost_by_area
DEALLOCATE asset_amount_cost_by_area

/*===============================================INFORMA��ES SOBRE PROCESSO========================================================*/

-- Valor do processo

DECLARE @process_id INT
DECLARE @process_value FLOAT

DECLARE process_value_cursor CURSOR FOR
	SELECT fkContext, nValue FROM view_rm_process_active

OPEN process_value_cursor
FETCH NEXT FROM process_value_cursor INTO @process_id, @process_value
WHILE (@@FETCH_STATUS = 0)
BEGIN
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (@process_id, getdate(), 'process_value', @process_value)
	FETCH NEXT FROM process_value_cursor INTO @process_id, @process_value
END
CLOSE process_value_cursor
DEALLOCATE process_value_cursor

-- Quantidade e custo dos ativos por processo

DECLARE @asset_amount_by_process INT
DECLARE @asset_cost_by_process FLOAT

DECLARE asset_amount_cost_by_process CURSOR FOR
	SELECT p.fkContext, count(pa.fkAsset) as asset_count, CASE WHEN sum(a.nCost) IS NULL THEN 0 ELSE sum(a.nCost) END
	FROM view_rm_process_active p
	LEFT JOIN view_rm_process_asset_active pa ON (p.fkContext = pa.fkProcess)
	LEFT JOIN view_rm_asset_active a ON (pa.fkAsset = a.fkContext)
	GROUP BY p.fkContext

OPEN asset_amount_cost_by_process
FETCH NEXT FROM asset_amount_cost_by_process INTO @process_id, @asset_amount_by_process, @asset_cost_by_process
WHILE (@@FETCH_STATUS = 0)
BEGIN
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (@process_id, getdate(), 'asset_amount_by_process', @asset_amount_by_process)
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (@process_id, getdate(), 'asset_cost_by_process', @asset_cost_by_process)
	FETCH NEXT FROM asset_amount_cost_by_process INTO @process_id, @asset_amount_by_process, @asset_cost_by_process
END
CLOSE asset_amount_cost_by_process
DEALLOCATE asset_amount_cost_by_process

-- Quantidade de riscos por processo

DECLARE @risk_count INT

DECLARE risk_amount_by_process CURSOR FOR
	SELECT p.fkContext, count(r.fkContext) as risk_count
	FROM view_rm_process_active p
	LEFT JOIN view_rm_process_asset_active pa ON (p.fkContext = pa.fkProcess)
	LEFT JOIN view_rm_asset_active a ON (pa.fkAsset = a.fkContext)
	LEFT JOIN view_rm_risk_active r ON (a.fkContext = r.fkAsset)
	GROUP BY p.fkContext

OPEN risk_amount_by_process
FETCH NEXT FROM risk_amount_by_process INTO @process_id, @risk_count
WHILE (@@FETCH_STATUS = 0)
BEGIN
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (@process_id, getdate(), 'risk_amount_by_process', @risk_count)
	FETCH NEXT FROM risk_amount_by_process INTO @process_id, @risk_count
END
CLOSE risk_amount_by_process
DEALLOCATE risk_amount_by_process

-- N� total de processos

DECLARE  @process_total INT
SET @process_total = (SELECT count(*) FROM view_rm_process_active)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'process_total', @process_total)

-- N� processos com risco alto

DECLARE  @processes_with_high_risk INT
SET @processes_with_high_risk = (SELECT count(*) FROM view_rm_process_active WHERE nValue >= @risk_high)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'processes_with_high_risk', @processes_with_high_risk)

-- N� processos com risco m�dio

DECLARE  @processes_with_medium_risk INT
SET @processes_with_medium_risk = (SELECT count(*) FROM view_rm_process_active WHERE nValue > @risk_low AND nValue < @risk_high)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'processes_with_medium_risk', @processes_with_medium_risk)

-- N� processos com risco baixo

DECLARE  @processes_with_low_risk INT
SET @processes_with_low_risk = (SELECT count(*) FROM view_rm_process_active WHERE nValue <= @risk_low AND nValue <> 0)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'processes_with_low_risk', @processes_with_low_risk)

-- N� processos com risco n�o parametrizado

DECLARE  @processes_with_no_value_risk INT
SET @processes_with_no_value_risk = (SELECT count(*) FROM view_rm_process_active WHERE nValue = 0)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'processes_with_no_value_risk', @processes_with_no_value_risk)

-- N� processos completos

DECLARE  @complete_processes INT
SET @complete_processes = (
	SELECT count(*)
	FROM view_rm_process_active p
	WHERE p.nValue > 0 AND NOT EXISTS (
		SELECT * 
		FROM view_rm_process_asset_active pa 
		JOIN view_rm_asset_active a ON (pa.fkAsset = a.fkContext) 
		WHERE pa.fkProcess = p.fkContext AND a.nValue = 0
	)
)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'complete_processes', @complete_processes)

-- N� processos parcias

DECLARE  @partial_processes INT
SET @partial_processes = (
	SELECT count(*)
	FROM view_rm_process_active p
	WHERE p.nValue > 0 AND EXISTS (
		SELECT * 
		FROM view_rm_process_asset_active pa 
		JOIN view_rm_asset_active a ON (pa.fkAsset = a.fkContext) 
		WHERE pa.fkProcess = p.fkContext AND a.nValue = 0
	)
)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'partial_processes', @partial_processes)

-- N� processos n�o gerenciados

DECLARE  @not_managed_processes INT
SET @not_managed_processes = (SELECT count(*) FROM view_rm_process_active p WHERE p.nValue = 0)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'not_managed_processes', @not_managed_processes)

-- N� processos sem ativos

DECLARE  @processes_without_assets INT
SET @processes_without_assets = (
	SELECT count(*)
	FROM view_rm_process_active p
	WHERE p.fkContext NOT IN (SELECT fkProcess FROM view_rm_process_asset_active)
)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'processes_without_assets', @processes_without_assets)

-- Total de impacto por processo

DECLARE @total_process_impact FLOAT

DECLARE total_process_impact CURSOR FOR
	SELECT p.fkContext as process_id, CASE WHEN sum(r.nCost) IS NULL THEN 0 ELSE sum(r.nCost) END as total_impact
	FROM view_rm_process_active p
	LEFT JOIN view_rm_process_asset_active pa ON (p.fkContext = pa.fkProcess)
	LEFT JOIN view_rm_asset_active a ON (pa.fkAsset = a.fkContext)
	LEFT JOIN view_rm_risk_active r ON (a.fkContext = r.fkAsset)
	GROUP BY p.fkContext

OPEN total_process_impact
FETCH NEXT FROM total_process_impact INTO @process_id, @total_process_impact

WHILE (@@FETCH_STATUS = 0)
BEGIN
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (@process_id, getdate(), 'total_process_impact', @total_process_impact)
	FETCH NEXT FROM total_process_impact INTO @process_id, @total_process_impact
END
CLOSE total_process_impact
DEALLOCATE total_process_impact

-- Custo e quantidade dos controles por processo

DECLARE @control_cost_by_process FLOAT
DECLARE @control_amount_by_process INT

DECLARE control_amount_cost_by_process CURSOR FOR
	/*Calcula o custo total dos controles de cada processo e a quantidade de controles de cada processo*/
	SELECT process_id, count(control_id) as control_count, CASE WHEN sum(cc.nCost1+cc.nCost2+cc.nCost3+cc.nCost4+cc.nCost5) IS NULL THEN 0 ELSE sum(cc.nCost1+cc.nCost2+cc.nCost3+cc.nCost4+cc.nCost5) END as control_cost
	FROM
	(
		/*Garante que um controle s� ir� ser contabilizado uma vez para um determinado processo*/
		SELECT p.fkContext as process_id, rc.fkControl as control_id
		FROM view_rm_process_active p
		LEFT JOIN view_rm_process_asset_active pa ON (p.fkContext = pa.fkProcess)
		LEFT JOIN view_rm_asset_active a ON (pa.fkAsset = a.fkContext)
		LEFT JOIN view_rm_risk_active r ON (a.fkContext = r.fkAsset)
		LEFT JOIN view_rm_risk_control_active rc ON (r.fkContext = rc.fkRisk)
		GROUP BY p.fkContext, rc.fkControl
	) res
	LEFT JOIN rm_control_cost cc ON (res.control_id = cc.fkControl)
	GROUP BY process_id

OPEN control_amount_cost_by_process
FETCH NEXT FROM control_amount_cost_by_process INTO @process_id, @control_amount_by_process, @control_cost_by_process
WHILE (@@FETCH_STATUS = 0)
BEGIN
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (@process_id, getdate(), 'control_amount_by_process', @control_amount_by_process)
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (@process_id, getdate(), 'control_cost_by_process', @control_cost_by_process)	
	FETCH NEXT FROM control_amount_cost_by_process INTO @process_id, @control_amount_by_process, @control_cost_by_process
END
CLOSE control_amount_cost_by_process
DEALLOCATE control_amount_cost_by_process

/*===============================================INFORMA��ES SOBRE ATIVO========================================================*/

-- Valor do ativo

DECLARE @asset_id INT
DECLARE @asset_value FLOAT

DECLARE asset_value_cursor CURSOR FOR
	SELECT fkContext, nValue FROM view_rm_asset_active

OPEN asset_value_cursor
FETCH NEXT FROM asset_value_cursor INTO @asset_id, @asset_value
WHILE (@@FETCH_STATUS = 0)
BEGIN
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (@asset_id, getdate(), 'asset_value', @asset_value)
	FETCH NEXT FROM asset_value_cursor INTO @asset_id, @asset_value
END
CLOSE asset_value_cursor
DEALLOCATE asset_value_cursor

-- Quantidade de processos por ativo

DECLARE @process_count INT

DECLARE process_amount_by_asset CURSOR FOR
	SELECT a.fkContext, count(pa.fkProcess) as process_count
	FROM view_rm_asset_active a
	LEFT JOIN view_rm_process_asset_active pa ON (a.fkContext = pa.fkAsset)
	GROUP BY a.fkContext

OPEN process_amount_by_asset
FETCH NEXT FROM process_amount_by_asset INTO @asset_id, @process_count
WHILE (@@FETCH_STATUS = 0)
BEGIN
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (@asset_id, getdate(), 'process_amount_by_asset', @process_count)
	FETCH NEXT FROM process_amount_by_asset INTO @asset_id, @process_count
END
CLOSE process_amount_by_asset
DEALLOCATE process_amount_by_asset

-- Quantidade de riscos por ativo

DECLARE risk_amount_by_asset CURSOR FOR
	SELECT a.fkContext, count(r.fkContext) as risk_count
	FROM view_rm_asset_active a
	LEFT JOIN view_rm_risk_active r ON (a.fkContext = r.fkAsset)
	GROUP BY a.fkContext

OPEN risk_amount_by_asset
FETCH NEXT FROM risk_amount_by_asset INTO @asset_id, @risk_count
WHILE (@@FETCH_STATUS = 0)
BEGIN
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (@asset_id, getdate(), 'risk_amount_by_asset', @risk_count)
	FETCH NEXT FROM risk_amount_by_asset INTO @asset_id, @risk_count
END
CLOSE risk_amount_by_asset
DEALLOCATE risk_amount_by_asset

-- N� total de ativos

DECLARE  @asset_total INT
SET @asset_total = (SELECT count(*) FROM view_rm_asset_active)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'asset_total', @asset_total)

-- N� ativos com risco alto

DECLARE  @assets_with_high_risk INT
SET @assets_with_high_risk = (SELECT count(*) FROM view_rm_asset_active WHERE nValue >= @risk_high)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'assets_with_high_risk', @assets_with_high_risk)

-- N� ativos com risco m�dio

DECLARE  @assets_with_medium_risk INT

SET @assets_with_medium_risk = (SELECT count(*) FROM view_rm_asset_active WHERE nValue > @risk_low AND nValue < @risk_high)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'assets_with_medium_risk', @assets_with_medium_risk)

-- N� ativos com risco baixo

DECLARE  @assets_with_low_risk INT
SET @assets_with_low_risk = (SELECT count(*) FROM view_rm_asset_active WHERE nValue <= @risk_low AND nValue <> 0)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'assets_with_low_risk', @assets_with_low_risk)

-- N� ativos com risco n�o parametrizado

DECLARE  @assets_with_no_value_risk INT
SET @assets_with_no_value_risk = (SELECT count(*) FROM view_rm_asset_active WHERE nValue = 0)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'assets_with_no_value_risk', @assets_with_no_value_risk)

-- N� ativos completos

DECLARE  @complete_assets INT
SET @complete_assets = (
	SELECT count(*) FROM view_rm_asset_active a
	WHERE a.nValue > 0 AND NOT EXISTS (
		SELECT * FROM view_rm_risk_active r WHERE r.fkAsset = a.fkContext AND r.nValue = 0
	)
)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'complete_assets', @complete_assets)

-- N� ativos parcias

DECLARE  @partial_assets INT
SET @partial_assets = (
	SELECT count(*) FROM view_rm_asset_active a
	WHERE a.nValue > 0 AND EXISTS (
		SELECT * FROM view_rm_risk_active r WHERE r.fkAsset = a.fkContext AND r.nValue = 0
	)
)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'partial_assets', @partial_assets)

-- N� ativos n�o gerenciados

DECLARE  @not_managed_assets INT
SET @not_managed_assets = (SELECT count(*) FROM view_rm_asset_active a WHERE a.nValue = 0)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'not_managed_assets', @not_managed_assets)

-- N� ativos sem riscos

DECLARE  @assets_without_risks INT
SET @assets_without_risks = (
	SELECT count(*)
	FROM view_rm_asset_active a
	WHERE a.fkContext NOT IN (SELECT fkAsset FROM view_rm_risk_active)
)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'assets_without_risks', @assets_without_risks)

-- Total de impacto dos ativos

DECLARE @total_asset_impact FLOAT

DECLARE total_asset_impact CURSOR FOR
	SELECT a.fkContext, CASE WHEN sum(r.nCost) IS NULL THEN 0 ELSE sum(r.nCost) END
	FROM view_rm_asset_active a 
	LEFT JOIN view_rm_risk_active r ON (a.fkContext = r.fkAsset)
	GROUP BY a.fkContext

OPEN total_asset_impact
FETCH NEXT FROM total_asset_impact INTO @asset_id, @total_asset_impact
WHILE (@@FETCH_STATUS = 0)
BEGIN
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (@asset_id, getdate(), 'total_asset_impact', @total_asset_impact)
	FETCH NEXT FROM total_asset_impact INTO @asset_id, @total_asset_impact
END
CLOSE total_asset_impact
DEALLOCATE total_asset_impact

-- Custo e quantidade dos controles por ativo

DECLARE @control_cost_by_asset FLOAT
DECLARE @control_amount_by_asset INT

DECLARE control_amount_cost_by_asset CURSOR FOR
	/*Calcula o custo total dos controles de cada ativo e a quantidade de controles de cada ativo*/
	SELECT res.asset_id, count(control_id) as control_count, CASE WHEN sum(cc.nCost1+cc.nCost2+cc.nCost3+cc.nCost4+cc.nCost5) IS NULL THEN 0 ELSE sum(cc.nCost1+cc.nCost2+cc.nCost3+cc.nCost4+cc.nCost5) END as control_cost
	FROM
	(
		/*Garante que um controle s� ir� ser contabilizado uma vez para um determinado ativo*/
		SELECT a.fkContext as asset_id, rc.fkControl as control_id
		FROM view_rm_asset_active a
		LEFT JOIN view_rm_risk_active r ON (a.fkContext = r.fkAsset)
		LEFT JOIN view_rm_risk_control_active rc ON (r.fkContext = rc.fkRisk)
		GROUP BY a.fkContext, rc.fkControl
	) res 
	LEFT JOIN rm_control_cost cc ON (res.control_id = cc.fkControl)
	GROUP BY asset_id

OPEN control_amount_cost_by_asset
FETCH NEXT FROM control_amount_cost_by_asset INTO @asset_id, @control_amount_by_asset, @control_cost_by_asset
WHILE (@@FETCH_STATUS = 0)
BEGIN
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (@asset_id, getdate(), 'control_amount_by_asset', @control_amount_by_asset)
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (@asset_id, getdate(), 'control_cost_by_asset', @control_cost_by_asset)	
	FETCH NEXT FROM control_amount_cost_by_asset INTO @asset_id, @control_amount_by_asset, @control_cost_by_asset
END
CLOSE control_amount_cost_by_asset
DEALLOCATE control_amount_cost_by_asset

/*===============================================INFORMA��ES SOBRE RISCO========================================================*/

-- Valor real e residual do risco

DECLARE @risk_id INT
DECLARE @risk_value FLOAT
DECLARE @risk_residual_value FLOAT

DECLARE risk_value_cursor CURSOR FOR
	SELECT fkContext, nValue, nValueResidual FROM view_rm_risk_active

OPEN risk_value_cursor
FETCH NEXT FROM risk_value_cursor INTO @risk_id, @risk_value, @risk_residual_value
WHILE (@@FETCH_STATUS = 0)
BEGIN
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (@risk_id, getdate(), 'risk_value', @risk_value)
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (@risk_id, getdate(), 'risk_residual_value', @risk_residual_value)
	FETCH NEXT FROM risk_value_cursor INTO @risk_id, @risk_value, @risk_residual_value
END
CLOSE risk_value_cursor
DEALLOCATE risk_value_cursor

-- Quantidade de controles dos riscos

DECLARE @control_count INT

DECLARE control_amount_by_risk CURSOR FOR
	SELECT r.fkContext, count(rc.fkControl) as control_count
	FROM view_rm_risk_active r
	LEFT JOIN view_rm_risk_control_active rc ON (r.fkContext = rc.fkRisk)
	GROUP BY r.fkContext

OPEN control_amount_by_risk
FETCH NEXT FROM control_amount_by_risk INTO @risk_id, @control_count
WHILE (@@FETCH_STATUS = 0)
BEGIN
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (@risk_id, getdate(), 'control_amount_by_risk', @control_count)
	FETCH NEXT FROM control_amount_by_risk INTO @risk_id, @control_count
END
CLOSE control_amount_by_risk
DEALLOCATE control_amount_by_risk

-- N� total de riscos

DECLARE  @risk_total INT
SET @risk_total = (SELECT count(*) FROM view_rm_risk_active)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'risk_total', @risk_total)

-- N� riscos com risco real alto

DECLARE  @risks_with_high_real_risk INT
SET @risks_with_high_real_risk = (SELECT count(*) FROM view_rm_risk_active WHERE nValue >= @risk_high)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'risks_with_high_real_risk', @risks_with_high_real_risk)

-- N� riscos com risco residual alto

DECLARE  @risks_with_high_residual_risk INT
SET @risks_with_high_residual_risk = (SELECT count(*) FROM view_rm_risk_active WHERE nValueResidual >= @risk_high)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'risks_with_high_residual_risk', @risks_with_high_residual_risk)

-- N� riscos com risco real m�dio

DECLARE  @risks_with_medium_real_risk INT
SET @risks_with_medium_real_risk = (SELECT count(*) FROM view_rm_risk_active WHERE nValue > @risk_low AND nValue < @risk_high)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'risks_with_medium_real_risk', @risks_with_medium_real_risk)

-- N� riscos com risco residual m�dio

DECLARE  @risks_with_medium_residual_risk INT
SET @risks_with_medium_residual_risk = (SELECT count(*) FROM view_rm_risk_active WHERE nValueResidual > @risk_low AND nValueResidual < @risk_high)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'risks_with_medium_residual_risk', @risks_with_medium_residual_risk)

-- N� riscos com risco real baixo

DECLARE  @risks_with_low_real_risk INT
SET @risks_with_low_real_risk = (SELECT count(*) FROM view_rm_risk_active WHERE nValue <= @risk_low AND nValue <> 0)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'risks_with_low_real_risk', @risks_with_low_real_risk)

-- N� riscos com risco residual baixo

DECLARE  @risks_with_low_residual_risk INT
SET @risks_with_low_residual_risk = (SELECT count(*) FROM view_rm_risk_active WHERE nValueResidual <= @risk_low AND nValueResidual <> 0)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'risks_with_low_residual_risk', @risks_with_low_residual_risk)

-- N� riscos com risco n�o parametrizado

DECLARE  @risks_with_no_value_risk INT
SET @risks_with_no_value_risk = (SELECT count(*) FROM view_rm_risk_active WHERE nValue = 0)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'risks_with_no_value_risk', @risks_with_no_value_risk)

-- N� riscos tratados

DECLARE  @risks_treated INT
SET @risks_treated = (SELECT count(*) FROM view_rm_risk_active r WHERE r.nAcceptMode<>0 OR r.nValue != r.nValueResidual)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'risks_treated', @risks_treated)

-- N� riscos n�o tratados

DECLARE  @risks_not_treated INT
SET @risks_not_treated = (SELECT count(*) FROM view_rm_risk_active r WHERE r.nAcceptMode=0 AND r.nValue = r.nValueResidual AND r.nValue > 0)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'risks_not_treated', @risks_not_treated)

-- N� riscos aceitos

DECLARE  @risks_accepted INT
SET @risks_accepted = (SELECT count(*) FROM view_rm_risk_active r WHERE r.nAcceptMode = 72601)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'risks_accepted', @risks_accepted)

-- N� riscos transferidos

DECLARE  @risks_transfered INT
SET @risks_transfered = (SELECT count(*) FROM view_rm_risk_active r WHERE r.nAcceptMode = 72602)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'risks_transfered', @risks_transfered)

-- N� riscos evitados

DECLARE  @risks_avoided INT
SET @risks_avoided = (SELECT count(*) FROM view_rm_risk_active r WHERE r.nAcceptMode = 72603)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'risks_avoided', @risks_avoided)

/*===============================================INFORMA��ES SOBRE CONTROLE========================================================*/

-- Quantidade de riscos por controle

DECLARE @control_id INT

DECLARE risk_amount_by_control CURSOR FOR
	SELECT c.fkContext, count(rc.fkRisk) as risk_count
	FROM view_rm_control_active c
	LEFT JOIN view_rm_risk_control_active rc ON (c.fkContext = rc.fkControl)
	GROUP BY c.fkContext

OPEN risk_amount_by_control
FETCH NEXT FROM risk_amount_by_control INTO @control_id, @risk_count
WHILE (@@FETCH_STATUS = 0)
BEGIN
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (@control_id, getdate(), 'risk_amount_by_control', @risk_count)
	FETCH NEXT FROM risk_amount_by_control INTO @control_id, @risk_count
END
CLOSE risk_amount_by_control
DEALLOCATE risk_amount_by_control

-- N� total de controles

DECLARE  @control_total INT
SET @control_total = (SELECT count(*) FROM view_rm_control_active)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'control_total', @control_total)


-- N� de controles com implanta��o planejada

DECLARE  @planned_implantation_controls INT
SET @planned_implantation_controls = (SELECT count(*) FROM view_rm_control_active c WHERE c.dDateDeadline >= getdate() AND c.dDateImplemented IS NULL)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'planned_implantation_controls', @planned_implantation_controls)

-- N� de controles com implanta��o atrasada

DECLARE  @late_implantation_controls INT
SET @late_implantation_controls = (SELECT count(*) FROM view_rm_control_active c WHERE c.dDateDeadline < getdate() AND c.dDateImplemented IS NULL)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'late_implantation_controls', @late_implantation_controls)

-- N� de controles implementados adequadamente

DECLARE  @adequate_implemented_controls INT
SET @adequate_implemented_controls = (
	SELECT count(*)
	FROM view_rm_control_active c
	LEFT JOIN rm_control_test_history ct ON (c.fkContext = ct.fkControl)
	LEFT JOIN wkf_control_efficiency ce ON (c.fkContext = ce.fkControlEfficiency)
	WHERE c.dDateImplemented IS NOT NULL AND ((ct.bTestedValue = 1 AND ce.nRealEfficiency >= ce.nExpectedEfficiency) OR (ct.bTestedValue = 1 AND ce.nRealEfficiency IS NULL) OR (ct.bTestedValue IS NULL AND ce.nRealEfficiency >= ce.nExpectedEfficiency))
)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'adequate_implemented_controls', @adequate_implemented_controls)

-- N� de controles implementados inadequadamente

DECLARE  @inadequate_implemented_controls INT
SET @inadequate_implemented_controls = (
	SELECT count(*)
	FROM view_rm_control_active c
	LEFT JOIN rm_control_test_history ct ON (c.fkContext = ct.fkControl)
	LEFT JOIN wkf_control_efficiency ce ON (c.fkContext = ce.fkControlEfficiency)
	WHERE 
	(
		(ct.bTestedValue=0 AND ce.nRealEfficiency>=ce.nExpectedEfficiency) OR
		(ct.bTestedValue=0 AND ce.nRealEfficiency IS NULL) OR
		(ct.bTestedValue IS NULL AND ce.nRealEfficiency<ce.nExpectedEfficiency) OR
		(ct.bTestedValue=1 AND ce.nRealEfficiency<ce.nExpectedEfficiency) OR
		(ct.bTestedValue=0 AND ce.nRealEfficiency<ce.nExpectedEfficiency)
	)
)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'inadequate_implemented_controls', @inadequate_implemented_controls)

-- N� de controles implementados mas n�o medidos

DECLARE  @not_measured_implemented_controls INT
SET @not_measured_implemented_controls = (
	SELECT count(*)
	FROM view_rm_control_active c
	LEFT JOIN rm_control_test_history ct ON (c.fkContext = ct.fkControl)
	LEFT JOIN wkf_control_efficiency ce ON (c.fkContext = ce.fkControlEfficiency)
	WHERE
		c.fkContext NOT IN (SELECT fkControlEfficiency FROM wkf_control_efficiency) AND
		c.fkContext NOT IN (SELECT fkControlTest FROM wkf_control_test)
)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'not_measured_implemented_controls', @not_measured_implemented_controls)

-- N� de controles testados com sucesso

DECLARE  @tested_ok_controls INT
SET @tested_ok_controls = (
	SELECT count(*)
	FROM view_rm_control_active c
	JOIN rm_control_test_history ct ON (c.fkContext = ct.fkControl AND ct.bTestedValue = 1 AND c.dDateImplemented IS NOT NULL)
)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'tested_ok_controls', @tested_ok_controls)

-- N� de controles testados sem sucesso

DECLARE  @tested_not_ok_controls INT
SET @tested_not_ok_controls = (
	SELECT count(*)
	FROM view_rm_control_active c
	JOIN rm_control_test_history ct ON (c.fkContext = ct.fkControl AND ct.bTestedValue = 0 AND c.dDateImplemented IS NOT NULL)
)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'tested_not_ok_controls', @tested_not_ok_controls)

-- N� de controles revisados com sucesso

DECLARE  @revision_ok_controls INT
SET @revision_ok_controls = (
	SELECT count(*)
	FROM view_rm_control_active c
	JOIN wkf_control_efficiency ce ON (c.fkContext = ce.fkControlEfficiency AND ce.nRealEfficiency >= ce.nExpectedEfficiency AND c.dDateImplemented IS NOT NULL)
)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'revision_ok_controls', @revision_ok_controls)

-- N� de controles revisados sem sucesso

DECLARE  @revision_not_ok_controls INT
SET @revision_not_ok_controls = (
	SELECT count(*)
	FROM view_rm_control_active c

	JOIN wkf_control_efficiency ce ON (c.fkContext = ce.fkControlEfficiency AND ce.nRealEfficiency < ce.nExpectedEfficiency AND c.dDateImplemented IS NOT NULL)
)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'revision_not_ok_controls', @revision_not_ok_controls)

-- N� de controles sem riscos

DECLARE  @controls_without_risks INT
SET @controls_without_risks = (
	SELECT count(*)
	FROM view_rm_control_active c
	WHERE c.fkContext NOT IN (SELECT fkControl FROM view_rm_risk_control_active)
)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'controls_without_risks', @controls_without_risks)

/*===============================================INFORMA��ES SOBRE NORMA========================================================*/

-- Quantidade de melhores pr�ticas por norma

DECLARE @standard_id INT
DECLARE @best_practice_count INT

DECLARE best_practice_amount_by_standard CURSOR FOR
	SELECT s.fkContext, count(bps.fkBestPractice)
	FROM view_rm_standard_active s
	LEFT JOIN view_rm_bp_standard_active bps ON (s.fkContext = bps.fkStandard)
	GROUP BY s.fkContext

OPEN best_practice_amount_by_standard
FETCH NEXT FROM best_practice_amount_by_standard INTO @standard_id, @best_practice_count
WHILE (@@FETCH_STATUS = 0)
BEGIN
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (@standard_id, getdate(), 'best_practice_amount_by_standard', @best_practice_count)
	FETCH NEXT FROM best_practice_amount_by_standard INTO @standard_id, @best_practice_count
END
CLOSE best_practice_amount_by_standard
DEALLOCATE best_practice_amount_by_standard

-- Quantidade de melhores pr�ticas aplicadas por norma

DECLARE applied_best_practice_amount_by_standard CURSOR FOR
	SELECT s.fkContext, count(bps.fkBestPractice)
	FROM view_rm_standard_active s
	LEFT JOIN rm_best_practice_standard bps ON (s.fkContext = bps.fkStandard)
	AND bps.fkBestPractice IN (SELECT cbp.fkBestPractice FROM view_rm_control_bp_active cbp)
	GROUP BY s.fkContext

OPEN applied_best_practice_amount_by_standard
FETCH NEXT FROM applied_best_practice_amount_by_standard INTO @standard_id, @best_practice_count
WHILE (@@FETCH_STATUS = 0)
BEGIN
	INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (@standard_id, getdate(), 'applied_best_practice_amount_by_standard', @best_practice_count)
	FETCH NEXT FROM applied_best_practice_amount_by_standard INTO @standard_id, @best_practice_count
END
CLOSE applied_best_practice_amount_by_standard
DEALLOCATE applied_best_practice_amount_by_standard

/*===============================================INFORMA��ES FINANCEIRAS========================================================*/

-- Custo total dos ativos

DECLARE  @asset_total_cost FLOAT
SET @asset_total_cost = (SELECT CASE WHEN sum(nCost) IS NULL THEN 0 ELSE sum(nCost) END FROM view_rm_asset_active)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'asset_total_cost', @asset_total_cost)

-- Custo total dos controles

DECLARE @control_total_cost1 FLOAT
DECLARE @control_total_cost2 FLOAT
DECLARE @control_total_cost3 FLOAT
DECLARE @control_total_cost4 FLOAT
DECLARE @control_total_cost5 FLOAT

DECLARE  control_total_cost CURSOR FOR
	SELECT 
	CASE WHEN sum(cc.nCost1) IS NULL THEN 0 ELSE sum(cc.nCost1) END,
	CASE WHEN sum(cc.nCost2) IS NULL THEN 0 ELSE sum(cc.nCost2) END,
	CASE WHEN sum(cc.nCost3) IS NULL THEN 0 ELSE sum(cc.nCost3) END,
	CASE WHEN sum(cc.nCost4) IS NULL THEN 0 ELSE sum(cc.nCost4) END,
	CASE WHEN sum(cc.nCost5) IS NULL THEN 0 ELSE sum(cc.nCost5) END
	FROM view_rm_control_active c
	JOIN rm_control_cost cc ON (c.fkContext = cc.fkControl)

OPEN control_total_cost
FETCH NEXT FROM control_total_cost INTO @control_total_cost1, @control_total_cost2, @control_total_cost3, @control_total_cost4, @control_total_cost5

INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (NULL, getdate(), 'control_total_cost1', @control_total_cost1)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (NULL, getdate(), 'control_total_cost2', @control_total_cost2)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (NULL, getdate(), 'control_total_cost3', @control_total_cost3)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (NULL, getdate(), 'control_total_cost4', @control_total_cost4)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue) VALUES (NULL, getdate(), 'control_total_cost5', @control_total_cost5)

CLOSE control_total_cost
DEALLOCATE control_total_cost

-- Custo dos riscos potencialmente baixos

DECLARE  @potentially_low_risk INT
SET @potentially_low_risk = (SELECT CASE WHEN sum(nCost) IS NULL THEN 0 ELSE sum(nCost) END FROM view_rm_risk_active r WHERE r.nValue <= @risk_low)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'potentially_low_risk_cost', @potentially_low_risk)

-- Custo dos riscos mitigados

DECLARE  @mitigated_risk INT
SET @mitigated_risk = (SELECT CASE WHEN sum(nCost) IS NULL THEN 0 ELSE sum(nCost) END FROM view_rm_risk_active r WHERE r.fkContext IN (SELECT fkRisk FROM view_rm_risk_control_active))
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'mitigated_risk_cost', @mitigated_risk)

-- Custo dos riscos evitados

DECLARE  @avoided_risk INT
SET @avoided_risk = (SELECT CASE WHEN sum(nCost) IS NULL THEN 0 ELSE sum(nCost) END FROM view_rm_risk_active r WHERE r.nAcceptMode = 72603)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'avoided_risk_cost', @avoided_risk)

-- Custo dos riscos transferidos

DECLARE  @transfered_risk INT
SET @transfered_risk = (SELECT CASE WHEN sum(nCost) IS NULL THEN 0 ELSE sum(nCost) END FROM view_rm_risk_active r WHERE r.nAcceptMode = 72602)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'transfered_risk_cost', @transfered_risk)

-- Custo dos riscos aceitos

DECLARE  @accepted_risk INT
SET @accepted_risk = (SELECT CASE WHEN sum(nCost) IS NULL THEN 0 ELSE sum(nCost) END FROM view_rm_risk_active r WHERE r.nAcceptMode = 72601)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'accepted_risk_cost', @accepted_risk)

-- Custo dos riscos m�dio / alto / n�o tratados

DECLARE  @not_treated_and_medium_high_risk INT
SET @not_treated_and_medium_high_risk = (
	SELECT CASE WHEN sum(nCost) IS NULL THEN 0 ELSE sum(nCost) END FROM view_rm_risk_active r
	WHERE r.nAcceptMode = 0 AND r.nValue > @risk_low AND r.fkContext NOT IN (SELECT fkRisk FROM view_rm_risk_control_active)
)
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)  VALUES (NULL, getdate(), 'not_treated_and_medium_high_risk', @not_treated_and_medium_high_risk)

/*================================================= POLICY MANAGEMENT ==========================================================*/

-- Quantidade de Documentos Publicados
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT NULL, getdate(), 'document_total', count(*) FROM view_pm_published_docs

-- Quantidade de Documentos por Estado
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT NULL, getdate(), 'documents_by_state_developing', count(d.fkContext)
FROM view_pm_document_active d JOIN isms_context c ON (c.pkContext = d.fkContext)
WHERE c.nState = 2751
UNION
SELECT NULL, getdate(), 'documents_by_state_approved', count(d.fkContext)
FROM view_pm_document_active d JOIN isms_context c ON (c.pkContext = d.fkContext)
WHERE c.nState = 2752
UNION
SELECT NULL, getdate(), 'documents_by_state_pendant', count(d.fkContext)
FROM view_pm_document_active d JOIN isms_context c ON (c.pkContext = d.fkContext)
WHERE c.nState = 2753

-- Quantidade de Documentos por Tipo
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT NULL, getdate(), 'documents_by_type_none', count(d.fkContext)
FROM view_pm_document_active d
WHERE d.nType = 2800
UNION
SELECT NULL, getdate(), 'documents_by_type_area', count(d.fkContext)
FROM view_pm_document_active d
WHERE d.nType = 2801
UNION
SELECT NULL, getdate(), 'documents_by_type_process', count(d.fkContext)
FROM view_pm_document_active d
WHERE d.nType = 2802
UNION
SELECT NULL, getdate(), 'documents_by_type_asset', count(d.fkContext)
FROM view_pm_document_active d
WHERE d.nType = 2803
UNION
SELECT NULL, getdate(), 'documents_by_type_control', count(d.fkContext)
FROM view_pm_document_active d
WHERE d.nType = 2805
UNION
SELECT NULL, getdate(), 'documents_by_type_scope', count(d.fkContext)
FROM view_pm_document_active d
WHERE d.nType = 2820
UNION
SELECT NULL, getdate(), 'documents_by_type_policy', count(d.fkContext)
FROM view_pm_document_active d
WHERE d.nType = 2821
UNION
SELECT NULL, getdate(), 'documents_by_type_management', count(d.fkContext)
FROM view_pm_document_active d
WHERE d.nType = 3801
UNION
SELECT NULL, getdate(), 'documents_by_type_others', count(d.fkContext)
FROM view_pm_document_active d
WHERE d.nType = 3802

-- Quantidade de Registros
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT NULL, getdate(), 'register_total', count(*) FROM view_pm_register_active

-- Quantidade de Documentos J� Lidos
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT NULL, getdate(), 'documents_read', count(*) FROM view_pm_read_docs

-- Quantidade de Documentos N�o Lidos
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT NULL, getdate(), 'documents_not_read', count(fkContext)
FROM view_pm_published_docs
WHERE fkContext NOT IN (SELECT fkContext FROM view_pm_read_docs)

-- Quantidade de Documentos com Revis�o Agendada
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT NULL, getdate(), 'documents_scheduled', count(*)
FROM view_pm_published_docs d JOIN wkf_doc_revision dr ON (dr.fkDocRevision = d.fkContext)

-- Ocupa��o dos arquivos de documentos
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT NULL, getdate(), 'occupation_documents', CASE WHEN SUM(di.nFileSize) > 0 THEN ROUND(CAST(SUM(di.nFileSize) AS FLOAT)/1048576.0,0) ELSE 0 END
FROM pm_doc_instance di
WHERE NOT EXISTS (
  SELECT * FROM pm_document d JOIN pm_register r ON (r.fkDocument = d.fkContext)
  WHERE d.fkContext = di.fkDocument
)

-- Ocupa��o dos arquivos de documentos
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT NULL, getdate(), 'occupation_documents', CASE WHEN SUM(di.nFileSize) > 0 THEN ROUND(CAST(SUM(di.nFileSize) AS FLOAT)/1048576.0,0) ELSE 0 END
FROM pm_doc_instance di
WHERE NOT EXISTS (
  SELECT * FROM pm_document d JOIN pm_register r ON (r.fkDocument = d.fkContext)
  WHERE d.fkContext = di.fkDocument
)

-- Ocupa��o dos arquivos de template
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT NULL, getdate(), 'occupation_template', CASE WHEN SUM(t.nFileSize) > 0 THEN ROUND(CAST(SUM(t.nFileSize) AS FLOAT)/1048576.0,0) ELSE 0 END
FROM pm_template t

-- Ocupa��o dos arquivos de registro
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT NULL, getdate(), 'occupation_registers', CASE WHEN SUM(di.nFileSize) > 0 THEN ROUND(CAST(SUM(di.nFileSize) AS FLOAT)/1048576.0,0) ELSE 0 END
FROM
  pm_doc_instance di
  JOIN pm_document d ON (d.fkContext = di.fkDocument)
  JOIN pm_register r ON (r.fkDocument = d.fkContext)

-- Ocupa��o total
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT NULL, getdate(), 'occupation_total', CASE WHEN SUM(f.nFileSize) > 0 THEN ROUND(CAST(SUM(f.nFileSize) AS FLOAT)/1048576.0,0) ELSE 0 END
FROM (
  SELECT nFileSize FROM pm_doc_instance
  UNION ALL
  SELECT nFileSize FROM pm_template
) f

/*=============================================== CONTINUAL IMPROVEMENT ========================================================*/

-- Quantidade de Incidentes
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT NULL, getdate(), 'incident_total', count(*) FROM view_ci_incident_active

-- Quantidade de N�o-Conformidades
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT NULL, getdate(), 'non_conformity_total', count(*) FROM view_ci_nc_active

-- Quantidade de Incidentes por Estado
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT NULL, getdate(), 'incidents_by_state_open', count(i.fkContext)
FROM view_ci_incident_active i JOIN isms_context c ON (c.pkContext = i.fkContext)
WHERE c.nState = 2779
UNION
SELECT NULL, getdate(), 'incidents_by_state_directed', count(i.fkContext)
FROM view_ci_incident_active i JOIN isms_context c ON (c.pkContext = i.fkContext)
WHERE c.nState = 2780
UNION
SELECT NULL, getdate(), 'incidents_by_state_pendant_disposal', count(i.fkContext)
FROM view_ci_incident_active i JOIN isms_context c ON (c.pkContext = i.fkContext)
WHERE c.nState = 2781
UNION
SELECT NULL, getdate(), 'incidents_by_state_waiting_solution', count(i.fkContext)
FROM view_ci_incident_active i JOIN isms_context c ON (c.pkContext = i.fkContext)
WHERE c.nState = 2782
UNION
SELECT NULL, getdate(), 'incidents_by_state_pendant_solution', count(i.fkContext)
FROM view_ci_incident_active i JOIN isms_context c ON (c.pkContext = i.fkContext)
WHERE c.nState = 2783
UNION
SELECT NULL, getdate(), 'incidents_by_state_solved', count(i.fkContext)
FROM view_ci_incident_active i JOIN isms_context c ON (c.pkContext = i.fkContext)
WHERE c.nState = 2784

-- Quantidade de N�o-Conformidades por Estado
INSERT INTO isms_context_history (fkContext, dDate, sType, nValue)
SELECT NULL, getdate(), 'nc_by_state_ci_sent', count(nc.fkContext)
FROM view_ci_nc_active nc JOIN isms_context c ON (c.pkContext = nc.fkContext)
WHERE c.nState = 2770
UNION
SELECT NULL, getdate(), 'nc_by_state_ci_open', count(nc.fkContext)
FROM view_ci_nc_active nc JOIN isms_context c ON (c.pkContext = nc.fkContext)
WHERE c.nState = 2771
UNION
SELECT NULL, getdate(), 'nc_by_state_ci_directed', count(nc.fkContext)
FROM view_ci_nc_active nc JOIN isms_context c ON (c.pkContext = nc.fkContext)
WHERE c.nState = 2772
UNION
SELECT NULL, getdate(), 'nc_by_state_ci_nc_pendant', count(nc.fkContext)
FROM view_ci_nc_active nc JOIN isms_context c ON (c.pkContext = nc.fkContext)
WHERE c.nState = 2773
UNION
SELECT NULL, getdate(), 'nc_by_state_ci_ap_pendant', count(nc.fkContext)
FROM view_ci_nc_active nc JOIN isms_context c ON (c.pkContext = nc.fkContext)
WHERE c.nState = 2774
UNION
SELECT NULL, getdate(), 'nc_by_state_ci_waiting_conclusion', count(nc.fkContext)
FROM view_ci_nc_active nc JOIN isms_context c ON (c.pkContext = nc.fkContext)
WHERE c.nState = 2775
UNION
SELECT NULL, getdate(), 'nc_by_state_ci_finished', count(nc.fkContext)
FROM view_ci_nc_active nc JOIN isms_context c ON (c.pkContext = nc.fkContext)
WHERE c.nState = 2776
UNION
SELECT NULL, getdate(), 'nc_by_state_ci_closed', count(nc.fkContext)
FROM view_ci_nc_active nc JOIN isms_context c ON (c.pkContext = nc.fkContext)
WHERE c.nState = 2777
UNION
SELECT NULL, getdate(), 'nc_by_state_denied', count(nc.fkContext)
FROM view_ci_nc_active nc JOIN isms_context c ON (c.pkContext = nc.fkContext)
WHERE c.nState = 2703

/*==============================================================================================================================*/

-- Grava a data da coleta no banco de dados

UPDATE isms_config SET sValue = cast(year(getdate()) as varchar) + '-' + cast(month(getdate()) as varchar) + '-' + cast(day(getdate()) as varchar) WHERE pkConfig = 420

END

END
GO

CREATE TRIGGER recalculation_of_context_value ON isms_context
FOR UPDATE
AS
BEGIN
  if ( @@rowcount=0 ) return
  
  DECLARE @fiContextId int
  DECLARE @fiNewState int
  DECLARE @fiType int
  DECLARE @fiOldState int
  
  SELECT
    @fiContextId = i.pkContext,
    @fiNewState = i.nState
    FROM inserted i
  
  SELECT
    @fiType = d.nType,
    @fiOldState = d.nState
    FROM deleted d
  -- context logical delete 
  IF( (@fiNewState = 2705) AND (@fiOldState <> 2705))
    BEGIN
        IF (@fiType = 2801) -- area
          BEGIN
            UPDATE rm_area
              SET nValue = %database_login%.get_area_value(fkContext)
                WHERE fkParent = @fiContextId
          END
        IF (@fiType =  2802) -- process
          BEGIN
            UPDATE rm_area
              SET nValue = %database_login%.get_area_value(fkContext)
                WHERE fkContext IN
                  (SELECT fkArea FROM rm_process WHERE fkContext = @fiContextId)
          END
        IF (@fiType =  2803) -- asset
          BEGIN
            UPDATE rm_process
              SET nValue = %database_login%.get_process_value(fkContext)
                WHERE fkContext IN
                  (SELECT fkProcess FROM rm_process_asset WHERE fkAsset = @fiContextId)
          END
        IF (@fiType =  2804) -- risk
          BEGIN
            UPDATE rm_asset
              SET nValue = %database_login%.get_asset_value(fkContext)
                 WHERE fkContext IN 
                  (SELECT fkAsset FROM rm_risk WHERE fkContext = @fiContextId)
          END
        IF (@fiType =  2805) -- control
          BEGIN
            UPDATE rm_risk 
              SET nValue = %database_login%.get_risk_value(fkContext),
              nValueResidual = %database_login%.get_risk_value_residual(fkContext)
                WHERE fkContext IN 
                  (SELECT fkRisk FROM rm_risk_control WHERE fkControl = @fiContextId)
          END
    END
    -- context restore from the trash
    IF ((@fiNewState <> 2705) AND (@fiOldState = 2705))
    BEGIN
      IF (@fiType =  2801 ) --area
        BEGIN
          UPDATE rm_area
            SET nValue = %database_login%.get_area_value(fkContext)
              WHERE fkContext = @fiContextId
        END
      IF (@fiType = 2802 ) --process
        BEGIN
          UPDATE rm_process
            SET nValue = %database_login%.get_process_value(fkContext)
              WHERE fkContext = @fiContextId
        END
      IF (@fiType = 2803 ) --asset
        BEGIN
          UPDATE rm_asset
            SET nValue = %database_login%.get_asset_value(fkContext)
              WHERE fkContext = @fiContextId
        END
      IF (@fiType =  2804 ) --risk
        BEGIN
          UPDATE rm_risk 
            SET nValue = %database_login%.get_risk_value(fkContext),
            nValueResidual = %database_login%.get_risk_value_residual(fkContext)
              WHERE fkContext = @fiContextId
          UPDATE rm_asset
            SET nValue = %database_login%.get_asset_value(fkContext)
              WHERE fkContext IN 
                (SELECT fkAsset FROM rm_risk WHERE fkContext = @fiContextId)
        END
      IF (@fiType = 2805 ) --control
        BEGIN
          UPDATE rm_risk 
            SET nValue = %database_login%.get_risk_value(fkContext),
            nValueResidual = %database_login%.get_risk_value_residual(fkContext)
              WHERE fkContext IN 
                (SELECT fkRisk FROM rm_risk_control rc WHERE fkControl = @fiContextId)
        END
    END

END
GO

CREATE TRIGGER recalculation_of_area_by_area ON rm_area
FOR UPDATE
AS
BEGIN
if ( @@rowcount=0 ) return
DECLARE @Vchg float
DECLARE @fiParentArea numeric
SELECT  @Vchg = (i.nValue - d.nValue),
	  @fiParentArea = d.fkParent
	  FROM deleted d, inserted i
IF (  @Vchg <> 0  ) 
	BEGIN				
		UPDATE rm_area				
		SET nValue = %database_login%.get_area_value(fkContext)
		WHERE fkContext = @fiParentArea
	END
END
GO

CREATE TRIGGER recalculation_of_dependent_asset ON rm_asset 
FOR UPDATE
AS
BEGIN

if ( @@rowcount=0 ) return
DECLARE @Vchg float
DECLARE @fiAsset numeric

SELECT  @Vchg = (i.nValue - d.nValue), 
	  @fiAsset = d.fkContext
	  FROM deleted d, inserted i


IF ( @Vchg <> 0 ) 
BEGIN

DECLARE @count INTEGER
DECLARE @OK INTEGER
DECLARE @dependent_asset INTEGER
SET @dependent_asset = 1
SET @count = 0

WHILE @dependent_asset <> 0
	BEGIN	
		SET @dependent_asset = %database_login%.get_asset_dependent_by_index(@fiAsset,@count)
		UPDATE rm_asset
		SET nValue = %database_login%.get_asset_value(@dependent_asset)
		WHERE fkContext = @dependent_asset
		SET @count = @count +1
	END
END

END
GO

CREATE TRIGGER recalculation_of_process ON rm_asset 
FOR UPDATE
AS
BEGIN
if ( @@rowcount=0 ) return
DECLARE @Vchg float
DECLARE @fiAsset numeric
SELECT  @Vchg = (i.nValue - d.nValue), 
	  @fiAsset = d.fkContext
	  FROM deleted d, inserted i
	IF ( @Vchg <> 0 ) 
			BEGIN		
				UPDATE rm_process				 
				SET nValue = %database_login%.get_process_value(p.fkContext)
				FROM rm_process p, rm_process_asset pa				
				WHERE p.fkContext = pa.fkProcess 
				AND pa.fkAsset = @fiAsset
			END
END
GO

CREATE TRIGGER recalculation_of_asset_association ON rm_asset_asset
FOR INSERT, UPDATE, DELETE
AS
BEGIN
IF ( @@rowcount=0 ) RETURN

DECLARE @miAsset numeric

SELECT  @miAsset = i.fkDependent FROM inserted i

IF @miAsset IS NULL
BEGIN
	SELECT @miAsset = d.fkDependent
	  FROM deleted d 
END

UPDATE rm_asset		 
SET nValue = %database_login%.get_asset_value(fkContext)		
WHERE fkContext = @miAsset

END
GO

CREATE TRIGGER recalculation_of_area_by_process_delete ON rm_process
FOR DELETE
AS
BEGIN
if ( @@rowcount=0 ) return

DECLARE @fiParentArea numeric
SELECT  @fiParentArea = d.fkArea
	  FROM deleted d
	BEGIN				
		UPDATE rm_area				
		SET nValue = %database_login%.get_area_value(fkContext)
		WHERE fkContext = @fiParentArea
	END
END
GO

CREATE TRIGGER recalculation_of_area_by_process_update ON rm_process
FOR UPDATE
AS
BEGIN
if ( @@rowcount=0 ) return
DECLARE @fiAarea numeric

DECLARE query_process CURSOR
FOR SELECT DISTINCT(i.fkArea) from inserted i, deleted d
	WHERE i.nValue <> d.nValue
	OR i.fkArea <> d.fkArea

OPEN query_process
FETCH NEXT FROM query_process INTO @fiAarea
WHILE @@FETCH_STATUS = 0
	BEGIN	
		UPDATE rm_area
		SET nValue = %database_login%.get_area_value(fkContext)
		WHERE fkContext = @fiAarea
		FETCH NEXT FROM query_process INTO @fiAarea
	END
CLOSE query_process
DEALLOCATE query_process
END
GO

CREATE TRIGGER recalculation_of_process_association ON rm_process_asset
FOR INSERT, UPDATE, DELETE
AS
BEGIN
if ( @@rowcount=0 ) return
DECLARE @fiPprocess numeric
SELECT  @fiPprocess = i.fkProcess
	  FROM inserted i
IF @fiPprocess IS NULL
	BEGIN
	SELECT  @fiPprocess = d.fkProcess
		  FROM deleted d 
	END
	BEGIN				
		UPDATE rm_process		 
		SET nValue = %database_login%.get_process_value(fkContext)
		FROM rm_process p
		WHERE p.fkContext = @fiPprocess
	END
END
GO

CREATE TRIGGER recalculation_of_asset_for_risk_delete ON rm_risk
FOR DELETE
AS

BEGIN

if ( @@rowcount=0 ) return

DECLARE @fiRisk int
DECLARE @fiAsset numeric

SELECT
	@fiAsset = d.fkAsset,
	@fiRisk = d.fkContext 
	  FROM inserted d

UPDATE rm_asset
	SET     nValue = %database_login%.get_asset_value(fkContext)
	WHERE fkContext = @fiAsset

END
GO

CREATE TRIGGER recalculation_of_risk_for_insert ON rm_risk
FOR INSERT
AS

BEGIN

if ( @@rowcount=0 ) return

DECLARE @fiRisk int
DECLARE @fiAsset numeric

SELECT
  @fiAsset = i.fkAsset,
  @fiRisk = i.fkContext
  FROM inserted i

  UPDATE rm_asset
    SET  nValue = %database_login%.get_asset_value(fkContext)
  WHERE fkContext = @fiAsset

END
GO

CREATE TRIGGER recalculation_of_risk_for_update ON rm_risk
FOR UPDATE
AS

BEGIN

if ( @@rowcount=0 ) return

DECLARE @VRchg float
DECLARE @Achg float
DECLARE @fiRisk int
DECLARE @fiAasset numeric

DECLARE updated_risks CURSOR
FOR 
	SELECT  (i.nValueResidual - d.nValueResidual),
		  (i.nAcceptMode - d.nAcceptMode),	
		   d.fkContext,
		  d.fkAsset
		  FROM deleted d JOIN inserted i ON (i.fkcontext=d.fkcontext)

OPEN updated_risks

FETCH NEXT FROM updated_risks INTO @VRchg, @Achg, @fiRisk, @fiAasset
WHILE (@@FETCH_STATUS = 0)
      BEGIN  
	IF (
	( (@VRchg <> 0  )  AND (@VRchg IS NOT NULL)	)  OR
	( (@Achg <> 0  )     AND (@Achg IS NOT NULL)	)
	   )
		BEGIN		

      UPDATE rm_asset
      SET     nValue = %database_login%.get_asset_value(fkContext)
      WHERE fkContext = @fiAasset
    END
   FETCH NEXT FROM updated_risks INTO @VRchg, @Achg, @fiRisk, @fiAasset
   END
CLOSE updated_risks 
DEALLOCATE updated_risks 
END
GO

CREATE TRIGGER delete_user ON isms_user
FOR DELETE
AS

BEGIN

if ( @@rowcount=0 ) return

DECLARE @fiUserId NUMERIC

SELECT @fiUserId = d.fkContext
FROM deleted d

UPDATE isms_context_date
SET nUserId = 0
WHERE nUserId = @fiUserId

END
GO

CREATE TRIGGER recauculate_risk_for_risk_control_association ON RM_RISK_CONTROL
FOR DELETE
AS
BEGIN
  DECLARE @fiRiskId numeric
  if ( @@rowcount=0 ) return
  
  SELECT  @fiRiskId = fkRisk
  	  FROM deleted d
  
  		UPDATE rm_risk				
  		SET nValue = %database_login%.get_risk_value(@fiRiskId ),
          nValueResidual = %database_login%.get_risk_value_residual(@fiRiskId )
          WHERE fkContext = @fiRiskId 

END
GO

CREATE TRIGGER update_control_is_active ON rm_control
FOR UPDATE
AS
BEGIN
  IF EXISTS(SELECT * FROM DELETED) BEGIN
    
    DECLARE @miControlId INT
    DECLARE @mbHasRevision INT
    DECLARE @mbHasTest INT
    DECLARE @mbWasActive INT
    DECLARE @mbIsActive INT
    DECLARE @mbShouldBeActive INT
    DECLARE @mdDateLimit DATETIME
    DECLARE @mdDateTodo DATETIME
    DECLARE @mdDateNext DATETIME
    
    DECLARE @mbImplementationIsLate INT
    DECLARE @mbEfficiencyNotOk INT
    DECLARE @mbRevisionIsLate INT
    DECLARE @mbTestNotOk INT
    DECLARE @mbTestIsLate INT
    
    DECLARE @mbOldImplementationIsLate INT
    DECLARE @mbOldEfficiencyNotOk INT
    DECLARE @mbOldRevisionIsLate INT
    DECLARE @mbOldTestNotOk INT
    DECLARE @mbOldTestIsLate INT
    
    DECLARE @mbIsWrong INT
    DECLARE @mbExists INT
    
    DECLARE updated_controls CURSOR LOCAL FOR SELECT fkContext, bIsActive FROM INSERTED
    OPEN updated_controls
    FETCH NEXT FROM updated_controls INTO @miControlId, @mbIsActive
    WHILE(@@FETCH_STATUS = 0) BEGIN
      
      SELECT
        @mbWasActive               = bIsActive,
        @mbOldImplementationIsLate = bImplementationIsLate,
        @mbOldEfficiencyNotOk      = bEfficiencyNotOk,
        @mbOldRevisionIsLate       = bRevisionIsLate,
        @mbOldTestNotOk            = bTestNotOk,
        @mbOldTestIsLate           = bTestIsLate
      FROM DELETED
      WHERE fkContext = @miControlId
      
      SET @mbImplementationIsLate = 0
      SET @mbEfficiencyNotOk = 0
      SET @mbRevisionIsLate = 0
      SET @mbTestNotOk = 0
      SET @mbTestIsLate = 0
      
      -- Testa a data de implementa��o
      SELECT @mbImplementationIsLate = CASE WHEN dDateImplemented IS NULL AND dDateDeadline <= getdate() THEN 1 ELSE 0 END
      FROM INSERTED
      WHERE fkContext = @miControlId
      
      -- Testa se tem revis�o
      SELECT
        @mbHasRevision = COUNT(*)
      FROM
        isms_config cfg,
        wkf_control_efficiency ce
      WHERE
        cfg.pkConfig = 404
        AND cfg.sValue = '1'
        AND ce.fkControlEfficiency = @miControlId
      
      -- Se tem revis�o, testa se ela t� ok e em dia.
      IF @mbHasRevision = 1 BEGIN
        SELECT @mbEfficiencyNotOk = CASE WHEN COUNT(*) = 0 THEN 1 ELSE 0 END
        FROM wkf_control_efficiency ce
        WHERE
          ce.fkControlEfficiency = @miControlId
          AND (
            ce.nRealEfficiency >= ce.nExpectedEfficiency
            OR ce.nRealEfficiency = 0
          )
        
        SELECT
          @mdDateLimit = ce.dDateLimit,
          @mdDateTodo = eh.dDateTodo,
          @mdDateNext = ce.dDateNext
        FROM
          wkf_control_efficiency ce
          LEFT JOIN rm_control_efficiency_history eh ON (ce.fkControlEfficiency = eh.fkControl)
        WHERE ce.fkControlEfficiency = @miControlId
        
        IF (
          @mdDateTodo IS NOT NULL AND (
            (
              @mdDateLimit IS NOT NULL
              AND dateadd(day, 1, @mdDateLimit) < getdate()
            ) OR (
              @mdDateLimit IS NULL
              AND dateadd(day, 1, @mdDateNext) < getdate()
            )
          )
        )
        BEGIN
          SET @mbRevisionIsLate = 0
        END
      END
      
      -- Testa se tem teste
      SELECT
        @mbHasTest = COUNT(*)
      FROM
        isms_config cfg,
        wkf_control_test ct
      WHERE
        cfg.pkConfig = 405
        AND cfg.sValue = '1'
        AND ct.fkControlTest = @miControlId
      
      -- Se tem teste, testa se ele t� ok e em dia.
      IF @mbHasTest = 1 BEGIN
        SELECT
          @mbTestNotOk = CASE WHEN COUNT(*) = 1 THEN 1 ELSE 0 END
        FROM
          wkf_control_test ct
          LEFT JOIN rm_control_test_history th ON (
            th.fkControl = ct.fkControlTest
            AND NOT EXISTS (
              SELECT *
              FROM rm_control_test_history th2
              WHERE
                th2.fkControl = th.fkControl
                AND th2.dDateAccomplishment > th.dDateAccomplishment
            )
          )
        WHERE
          ct.fkControlTest = @miControlId
          AND th.bTestedValue = 0
        
        SELECT
          @mdDateLimit = ct.dDateLimit,
          @mdDateTodo  = th.dDateTodo,
          @mdDateNext  = ct.dDateNext
        FROM
          wkf_control_test ct
          LEFT JOIN rm_control_test_history th ON (ct.fkControlTest = th.fkControl)
        WHERE ct.fkControlTest = @miControlId
        
        IF (
          @mdDateTodo IS NOT NULL AND (
            (
              @mdDateLimit IS NOT NULL
              AND dateadd(day, 1, @mdDateLimit) < getdate()
            ) OR (
              @mdDateLimit IS NULL
              AND dateadd(day, 1, @mdDateNext) < getdate()
            )
          )
        )
        BEGIN
          SET @mbTestIsLate = 0
        END
      END
      
      -- Atualiza as flags e cria as seeds de N�o-Conformidade que forem necess�rias
      IF @mbImplementationIsLate + @mbEfficiencyNotOk + @mbRevisionIsLate + @mbTestNotOk + @mbTestIsLate > 0 BEGIN
        SET @mbShouldBeActive = 0
      END ELSE BEGIN
        SET @mbShouldBeActive = 1
      END
      
      IF @mbIsActive != @mbShouldBeActive BEGIN
        UPDATE rm_control SET bIsActive = @mbShouldBeActive WHERE fkContext = @miControlId
      END
      
      SELECT @mbIsWrong = COUNT(*)
      FROM rm_control
      WHERE
        fkContext = @miControlId
        AND (
             bIsActive             != @mbShouldBeActive
          OR bImplementationIsLate != @mbImplementationIsLate
          OR bEfficiencyNotOk      != @mbEfficiencyNotOk
          OR bRevisionIsLate       != @mbRevisionIsLate
          OR bTestNotOk            != @mbTestNotOk
          OR bTestIsLate           != @mbTestIsLate
        )
      
      IF @mbIsWrong = 1 BEGIN
        UPDATE rm_control
        SET
          bIsActive             = @mbShouldBeActive,
          bImplementationIsLate = @mbImplementationIsLate,
          bEfficiencyNotOk      = @mbEfficiencyNotOk,
          bRevisionIsLate       = @mbRevisionIsLate,
          bTestNotOk            = @mbTestNotOk,
          bTestIsLate           = @mbTestIsLate
        WHERE fkContext = @miControlId
        
        IF @mbShouldBeActive = 0 BEGIN
          
          IF @mbOldImplementationIsLate = 0 AND @mbImplementationIsLate = 1 BEGIN
            SELECT @mbExists = COUNT(*) FROM ci_nc_seed WHERE fkControl = @miControlId AND nDeactivationReason = 1
            IF @mbExists = 0 BEGIN
              INSERT INTO ci_nc_seed (fkControl,nDeactivationReason,dDateSent) VALUES (@miControlId,1,getdate())
            END
          END
          
          IF @mbOldEfficiencyNotOk = 0 AND @mbEfficiencyNotOk = 1 BEGIN
            SELECT @mbExists = COUNT(*) FROM ci_nc_seed WHERE fkControl = @miControlId AND nDeactivationReason = 2
            IF @mbExists = 0 BEGIN
              INSERT INTO ci_nc_seed (fkControl,nDeactivationReason,dDateSent) VALUES (@miControlId,2,getdate())
            END
          END
          
          IF @mbOldRevisionIsLate = 0 AND @mbRevisionIsLate = 1 BEGIN
            SELECT @mbExists = COUNT(*) FROM ci_nc_seed WHERE fkControl = @miControlId AND nDeactivationReason = 4
            IF @mbExists = 0 BEGIN
              INSERT INTO ci_nc_seed (fkControl,nDeactivationReason,dDateSent) VALUES (@miControlId,4,getdate())
            END
          END
          
          IF @mbOldTestNotOk = 0 AND @mbTestNotOk = 1 BEGIN
            SELECT @mbExists = COUNT(*) FROM ci_nc_seed WHERE fkControl = @miControlId AND nDeactivationReason = 8
            IF @mbExists = 0 BEGIN
              INSERT INTO ci_nc_seed (fkControl,nDeactivationReason,dDateSent) VALUES (@miControlId,8,getdate())
            END
          END
          
          IF @mbOldTestIsLate = 0 AND @mbTestIsLate = 1 BEGIN
            SELECT @mbExists = COUNT(*) FROM ci_nc_seed WHERE fkControl = @miControlId AND nDeactivationReason = 16
            IF @mbExists = 0 BEGIN
              INSERT INTO ci_nc_seed (fkControl,nDeactivationReason,dDateSent) VALUES (@miControlId,16,getdate())
            END
          END
          
        END
        
      END ELSE BEGIN
        IF @mbShouldBeActive != @mbWasActive BEGIN
          UPDATE rm_risk 
          SET
            nValue = %database_login%.get_risk_value(fkContext),
            nValueResidual = %database_login%.get_risk_value_residual(fkContext)
          WHERE fkContext IN (
            SELECT fkRisk
            FROM rm_risk_control
            WHERE fkControl = @miControlId
          )
        END
      END
      
      FETCH NEXT FROM updated_controls INTO @miControlId, @mbIsActive
    END
    
  END
END
GO

CREATE TRIGGER delete_event ON rm_event
FOR DELETE
AS
BEGIN

DECLARE @event_id NUMERIC
DECLARE events CURSOR FOR SELECT fkContext FROM deleted d

OPEN events
FETCH NEXT FROM events INTO @event_id
WHILE @@FETCH_STATUS = 0
	BEGIN	
		UPDATE rm_risk
		SET fkEvent = NULL
		WHERE fkEvent = @event_id
		FETCH NEXT FROM events INTO @event_id
	END
CLOSE events
DEALLOCATE events

END
GO