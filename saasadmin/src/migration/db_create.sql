CREATE DATABASE %database_name%
WITH OWNER = %database_login%
ENCODING = 'LATIN1'
TABLESPACE = pg_default;