<?php

function phpCompile2($paDirectories, $psCompiledFileName, $psSrcRef) {
  $mrHandle = fopen($psCompiledFileName, "w+");  
  if($mrHandle){
    foreach($paDirectories as $msDirectory => $maFiles){
    	if ($msDirectory == "classes2") $msDirectory = "classes";     
      $msDirectory = $psSrcRef.$msDirectory;
      if(is_dir($msDirectory)){
        foreach ($maFiles as $msFile) {
          $msFileFullPath = $msDirectory . "/" . $msFile;
          echo $msFileFullPath . "<br/>";
          $msExt = strtolower(substr($msFile,strrpos($msFile,'.')+1));
          $msFileContent = file_get_contents($msFileFullPath);          
          fwrite($mrHandle,$msFileContent);          
        }
      }else{
        echo "$msDirectory is not a directory!<br/>";
      }
    }
    return true;
  }
  else {
    echo "Could not create file = $psCompiledFileName!</br>";
    return false;
  }
}

function check_extension($file, $extensions) {
	$aux = explode(".",strtolower(trim($file)));
	$file_extension = $aux[count($aux)-1];	
	foreach($extensions as $extension) if ($file_extension == $extension) return false;	
	return true;
}

function dircopy($srcdir, $dstdir, $directories_to_ignore = array(), $extensions_to_ignore = array(), $extensions_to_allow = array(), $files_to_ignore = array()) {
  $num = 0;
  if(!is_dir($dstdir)) mkdir($dstdir, 0777, true);
  if($curdir = opendir($srcdir)) {
   while($file = readdir($curdir)) {
     if($file != '.' && $file != '..' && !in_array($file, $directories_to_ignore)) {
       $srcfile = $srcdir . '/' . $file;
       $dstfile = $dstdir . '/' . $file;
       if(is_file($srcfile) && !in_array($file, $files_to_ignore)) {
       	 if (count($extensions_to_ignore) && !check_extension($file, $extensions_to_ignore)) continue; 
       	 if (count($extensions_to_allow) && check_extension($file, $extensions_to_allow)) continue;       	 
         echo "<br>Copying '$srcfile' to '$dstfile'...";
         if(copy($srcfile, $dstfile)) {
           touch($dstfile, filemtime($srcfile)); $num++;
           echo "OK\n";
         }
         else echo "<br>Error: File '$srcfile' could not be copied!\n";
       }
       else if(is_dir($srcfile)) {
         $num += dircopy($srcfile, $dstfile, $directories_to_ignore, $extensions_to_ignore, $extensions_to_allow, $files_to_ignore);
       }
     }
   }
   closedir($curdir);
  }
  return $num;
}

$ssDir = "/home/real/Projects/realiso/realiso/new_deploy_saas_admin";
$ssClasses = $ssDir."/classes/";
$ssBaseDir = '/home/real/Projects/realiso/realiso';

if (!file_exists($ssDir)) mkdir($ssDir);
if (!file_exists($ssClasses)) mkdir($ssClasses);

echo "<h3>>>>>>  INICIO DO ARQUIVO <<<<<< </h3>";
echo "<h3>COMPILING OS PHPS</h3>";

$saFiles = array(
"classes" => array("SAASInstanceManager.php",
										"SAASHotFixManager.php",
										"SAASLib.php",
										"SAASConfig.php",
										"SAASInstance.php",
					"SAASUsers.php",
                    "SAASAgenda.php",
                    "SAASActivation.php",
                    "SAASKeyGen.php",
                    "SAASLogger.php",
                    "SAASEmailTemplate.php")
);
phpCompile2($saFiles, $ssClasses. "saas_classes.php", "../");

echo "<h3>COPYING ACTIVATION</h3>";
dircopy("../activation", $ssDir."/activation", array('.svn'), array(), array('php', 'xmc', 'txt'));

echo "<h3>COPYING HANDLERS</h3>";
dircopy("../handlers", $ssDir."/handlers", array('.svn'), array(), array('php'));

echo "<h3>COPYING SRC</h3>";
dircopy("..", $ssDir, array('.svn', 'migration', 'activation', 'compile', 'tools', 'handlers', 'classes', 'lib', 'gfx', 'sqls'), array(), array('php', 'xmc'), array('saas_xml_compile.php'));

echo "<h3>COPYING GFX</h3>";
dircopy("../gfx", $ssDir."/gfx", array('.svn'), array('db'), array(), array());

echo "<h3>COPYING NONAUTO CSS</h3>";
dircopy("../lib/css/nonauto", $ssDir."/lib/css/nonauto", array('.svn'));

echo "<h3>COPYING JAVASCRIPT</h3>";
dircopy($ssBaseDir."/FWD/src/base/javascript/nonauto", $ssDir."/lib/javascript", array('.svn'));

echo "<h3>>>>>>  FIM DO ARQUIVO <<<<<< </h3>";

?>
