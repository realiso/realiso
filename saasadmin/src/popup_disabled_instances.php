<?php
include_once "include.php";
include_once $handlers_grid_ref."QueryGridDisabledInstances.php";

class GridDisabledInstances extends FWDDrawGrid {
  public function drawItem(){
    switch($this->ciColumnIndex){
      case $this->getIndexByAlias('instance_creation_date'):{
        $this->coCellBox->setValue(SAASLib::getSAASDate($this->coCellBox->getValue()));
        return $this->coCellBox->draw();
      }
      case $this->getIndexByAlias('instance_type'):{
        if (!in_array($this->coCellBox->getValue(), SAASLib::getTrialLicenseTypes())) { // A princ�pio, s� inst�ncias trial devem ser deletadas.
          $this->csDivUnitClass = 'GridLineYellow';
          $this->coCellBox->setValue("WARNING!!!");
        }
        else {
          $this->coCellBox->setValue(SAASLib::getLicenseTypeAsString($this->coCellBox->getValue()));
        }
        return $this->coCellBox->draw();
      }
      default:{
        return parent::drawItem();
      }
    }
  }
}

class RemoveInstancesEvent extends FWDRunnable {
  public function run(){
    $moGrid = FWDWebLib::getObject('grid_disabled_instances');
    $maInstances =  $moGrid->getValue();

    if (count($maInstances)) {    
      $moErrorHandler = new FWDDoNothingErrorHandler();
      FWDWebLib::setErrorHandler($moErrorHandler);
      $mbDebugMode = FWDWebLib::getInstance()->getDebugMode();
      $mbDebugType = FWDWebLib::getInstance()->getDebugType();
      FWDWebLib::getInstance()->setDebugMode(false,0);    

      $msTitle = FWDLanguage::getPHPStringValue('mx_removal_status_title','Status');

      foreach ($maInstances as $miId) {
        // busca informa��es sobre a inst�ncia
        $moInstance = new SAASInstance();
        $moInstance->fetchById($miId);
        $msAlias = $moInstance->getFieldValue('instance_alias');
        $msDBName = $moInstance->getFieldValue('instance_db_name');
        $msDBUser = $moInstance->getFieldValue('instance_db_user');

        // remove a inst�ncia
        $mbOk = SAASInstanceManager::removeInstance($miId);

        // se ocorrer erro
        if($moErrorHandler->errorCaptured() || !$mbOk){
          // atualiza grid
          $moGrid->execEventPopulate();
          // atualiza grid de inst�ncias
          echo "soWindow = soPopUpManager.getPopUpById('popup_disabled_instances').getOpener();"
          ."if(soWindow.refresh_grid) soWindow.refresh_grid();";
          // mostra msg de erro
          $msMessage = FWDLanguage::getPHPStringValue('mx_problems_during_instance_removal',"Problemas durante a remo��o da inst�ncia <b>$msAlias</b>. Por favor, verifique se algum dos itens abaixo n�o foi removido e, caso afirmativo, remova-o manualmente.<br/><br/><b>Banco de Dados</b> - $msDBName<br/><b>Usu�rio</b> - $msDBUser<br/><b>Pasta</b> - $msAlias<br/><br/>Abortando opera��o...");
          SAASLib::openOk($msTitle,$msMessage,"",145);
          // aborta opera��o        
          exit;
        }
      }

      // atualiza a grid
      $moGrid->execEventPopulate();

      // mostra msg de sucesso
      $msMessage = FWDLanguage::getPHPStringValue('mx_all_instances_successfully_removed',"Todas as inst�ncias foram removidas com sucesso.");
      SAASLib::openOk($msTitle,$msMessage,"",25);

      // restaura error handler
      FWDWebLib::getInstance()->setDebugMode($mbDebugMode,$mbDebugType);    
      FWDWebLib::restoreLastErrorHandler();

      // atualiza grid de inst�ncias
      echo "soWindow = soPopUpManager.getPopUpById('popup_disabled_instances').getOpener();"
      ."if(soWindow.refresh_grid) soWindow.refresh_grid();";
    }
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
    $moStartEvent->addAjaxEvent(new RemoveInstancesEvent('remove_instances_event'));

    $moGrid = FWDWebLib::getObject("grid_disabled_instances");
    $moHandler = new QueryGridDisabledInstances(FWDWebLib::getConnection());
    $moGrid->setQueryHandler($moHandler);        
    $moGrid->setObjFwdDrawGrid(new GridDisabledInstances());
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_disabled_instances.xml');
?>