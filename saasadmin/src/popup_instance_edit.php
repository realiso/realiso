<?php
include_once "include.php";
include_once $handlers_ref."QueryLicenseInfo.php";

/*
 * Constantes que definem cada passo executado
 * pelo processo de atualiza��o da inst�ncia.
 */
class SaveEvent extends FWDRunnable implements FWDErrorHandler {
  
  protected $ciStep = STEP_NONE;
  
 /**
  * M�todo que manipula erros.
  *
  * <p>M�todo que manipula erros.</p>
  * @access public
  * @param integer $piErrNo N�mero do erro
  * @param string $psErrStr Mensagem de erro
  * @param string $psErrFile Nome do arquivo que gerou o erro
  * @param integer $piErrLine N�mero da linha em que ocorreu o erro
  */
  public function handleError($piErrNo, $psErrStr, $psErrFile, $piErrLine){
    switch ($this->ciStep) {
      case STEP_UPDATE_LICENSE_INFO:
        echo "gobi('status').setValue('".FWDLanguage::getPHPStringValue('st_could_not_update_license_info',"Falha na atualiza��o das informa��es da licen�a.")."');";        
      break;
      case STEP_NONE:
        echo "gobi('status').setValue('".FWDLanguage::getPHPStringValue('st_unknown_error',"Falha n�o identificada.")."');";        
      break;
    }
    exit;
  }
  
 /**
  * M�todo que manipula exce��es.
  *
  * <p>M�todo que manipula exce��es.</p>
  * @access public
  * @param FWDException $poFWDException Exce��o
  * @static
  */
  public static function handleException($poFWDException){
  }
 
  public function run(){
    FWDWebLib::setErrorHandler($this);
    
    /*
     * Carrega as configura��es.
     */
    $moConfig = new SAASConfig();
    $moConfig->loadConfig();
    
    /*
     * Insere informa��es da licen�a.
     */
    $this->ciStep = STEP_UPDATE_LICENSE_INFO;    
    $msClientName = FWDWebLib::getObject('alias')->getValue();    
    $miMaxUsers = FWDWebLib::getObject('max_users')->getValue();
    $miMaxSimultUsers = FWDWebLib::getObject('max_simultaneous_users')->getValue();
    
	$tmpTimezone = date_default_timezone_get();
	date_default_timezone_set('America/New_York');
    $miExpirationDate = FWDWebLib::getObject('expiration_date')->getTimestamp();
	date_default_timezone_set($tmpTimezone);
            
    $msModulesController = FWDWebLib::getObject('module_controller')->getValue();
    $maModules = array();    
    if (strpos($msModulesController, "RM")) $maModules[] = "RM";
    if (strpos($msModulesController, "PM")) $maModules[] = "PM";
    if (strpos($msModulesController, "CI")) $maModules[] = "CI";
    $msModules = implode("|", $maModules);
    $miType = SAASLib::convertAdminLicenseType2SystemLicenseType(FWDWebLib::getObject('license_type')->getValue());
        
    $msDatabaseLogin = FWDWebLib::getObject('db_user')->getValue();
    $msDatabaseLoginPass = FWDWebLib::getObject('db_user_password')->getValue();
    $msDatabaseName = FWDWebLib::getObject('db_name')->getValue();    
    $moDB = new FWDDB(
      DB_POSTGRES,
      $msDatabaseName,
      $msDatabaseLogin,
      $msDatabaseLoginPass,
      $moConfig->getConfig(CFG_DB_SERVER)
    );    
    $moDB->connect();    
    
    $moHandler = new QueryLicenseInfo($moDB);
    $moHandler->makeQuery();
    $maRes = $moHandler->executeQuery();
    $miActivationDate = $maRes[SAASInstance::LICENSE_ACTIVATION_DATE];
    
    $msMD5 = md5($msClientName.$msModules.$miMaxUsers.$miActivationDate.$miExpirationDate.$miMaxSimultUsers.$miType.'LICENSEHASH');
    $msSQL = "UPDATE isms_saas SET sValue = '$msClientName' WHERE pkConfig = ".SAASInstance::LICENSE_CLIENT_NAME.";
              UPDATE isms_saas SET sValue = '$msModules' WHERE pkConfig = ".SAASInstance::LICENSE_MODULES.";
              UPDATE isms_saas SET sValue = '$miMaxUsers' WHERE pkConfig = ".SAASInstance::LICENSE_MAX_USERS.";
              UPDATE isms_saas SET sValue = '$miExpirationDate' WHERE pkConfig = ".SAASInstance::LICENSE_EXPIRACY.";
              UPDATE isms_saas SET sValue = '$miMaxSimultUsers' WHERE pkConfig = ".SAASInstance::LICENSE_MAX_SIMULTANEOUS_USERS.";
              UPDATE isms_saas SET sValue = '$miType' WHERE pkConfig = ".SAASInstance::LICENSE_TYPE.";
              UPDATE isms_saas SET sValue = '$msMD5' WHERE pkConfig = ".SAASInstance::LICENSE_HASH.";";
    $moDB->execute($msSQL, 0, 0, true);       
    
    FWDWebLib::restoreLastErrorHandler();
    
    echo "gobi('status').setValue('".FWDLanguage::getPHPStringValue('st_instance_successfully_updated',"Inst�ncia atualizada com sucesso.")."');";     
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
    $moStartEvent->addAjaxEvent(new SaveEvent('save_event'));    
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $miInstanceId = FWDWebLib::getObject('instance_id')->getValue();
        
    /*
     * Busca informa��es da inst�ncia armazenada no banco do sistema. 
     */
    $moInstance = new SAASInstance();
    $moInstance->fetchById($miInstanceId);
    FWDWebLib::getObject('alias')->setValue($moInstance->getFieldValue('instance_alias'));
    FWDWebLib::getObject('db_name')->setValue($moInstance->getFieldValue('instance_db_name'));
    FWDWebLib::getObject('db_user')->setValue($moInstance->getFieldValue('instance_db_user'));
    FWDWebLib::getObject('db_user_password')->setValue($moInstance->getFieldValue('instance_db_user_pass'));
    
    /*
     * Busca informa��es da inst�ncia armazenada no banco da inst�ncia.
     */
    $moConfig = new SAASConfig();
    $moConfig->loadConfig();
    
    $moDB = new FWDDB(
      DB_POSTGRES,
      $moInstance->getFieldValue('instance_db_name'),
      $moInstance->getFieldValue('instance_db_user'),
      $moInstance->getFieldValue('instance_db_user_pass'),
      $moConfig->getConfig(CFG_DB_SERVER)
    );    
    $moHandler = new QueryLicenseInfo($moDB);
    $moHandler->makeQuery();
    $maRes = $moHandler->executeQuery();
    
    foreach($maRes as $miId => $msValue) {
      switch($miId) {
        case SAASInstance::LICENSE_MODULES:
          $msValue = str_replace('|', ':', $msValue);
          FWDWebLib::getObject('module_controller')->setValue($msValue);
        break;
        case SAASInstance::LICENSE_MAX_USERS:
          FWDWebLib::getObject('max_users')->setValue($msValue);
        break;
        case SAASInstance::LICENSE_MAX_SIMULTANEOUS_USERS:
          FWDWebLib::getObject('max_simultaneous_users')->setValue($msValue);
        break;
        case SAASInstance::LICENSE_EXPIRACY:
          if($msValue){
            FWDWebLib::getObject('expiration_date')->setTimestamp($msValue);
          }
        break;
        case SAASInstance::LICENSE_TYPE:
          FWDWebLib::getObject('license_type')->setValue(SAASLib::convertSystemLicenseType2AdminLicenseType($msValue));
        break;
      }
    }
    
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_instance_edit.xml');
?>