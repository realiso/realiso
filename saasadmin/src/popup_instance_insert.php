<?php
include_once "include.php";

/*
 * Primeiro evento executado ao se clicar no bot�o para iniciar
 * o processo de cria��o da nova inst�ncia. Respons�vel por
 * verificar a validade dos dados fornecidos pelo usu�rio.
 */
class SaveEvent extends FWDRunnable {
  public function run(){
    $msAlias = trim(strtolower(FWDWebLib::getObject('alias')->getValue()));
    $msActivationCode = trim(FWDWebLib::getObject('activation_code')->getValue());
    
    if (!SAASInstance::isValidAlias($msAlias)) {
      echo "gobi('invalid_alias_warning').show();";
      exit;
    }
    if (SAASInstance::isAliasUsed($msAlias)) {
      echo "gobi('duplicate_alias_warning').show();";
      exit;
    }
    $miLicenseType = FWDWebLib::getObject('license_type')->getValue();    
    if (!SAASACtivation::isValidActivationCode($msActivationCode, $miLicenseType)) {
      echo "gobi('invalid_code_warning').show();";
      exit; 
    }
    
    echo "trigger_event('create_user_db_event', 3);";
  }
}

/*
 * Evento respons�vel por criar o usu�rio e o banco de dados
 * que ser� utilizado pela nova inst�ncia.
 */
class CreateUserDBEvent extends FWDRunnable {
  public function run(){
    $msAlias = trim(strtolower(FWDWebLib::getObject('alias')->getValue()));
    $moConfig = new SAASConfig();
    $moConfig->loadConfig();
    
    $moManager = new SAASInstanceManager($moConfig, $msAlias);
    
    FWDWebLib::setErrorHandler(new FWDDoNothingErrorHandler());
    $mbDebugMode = FWDWebLib::getInstance()->getDebugMode();
    $mbDebugType = FWDWebLib::getInstance()->getDebugType();
    FWDWebLib::getInstance()->setDebugMode(false,0);
    
    if ($moManager->createUserAndDB()) {    
      echo "gebi('user_db_creation_right').style.display='block';";
      echo "trigger_event('execute_scripts_event', 3);";
    }
    else {
      echo "gebi('user_db_creation_wrong').style.display='block';";
      echo "gobi('status').setValue('".$moManager->getLastError()."');";
    }
    
    FWDWebLib::getInstance()->setDebugMode($mbDebugMode,$mbDebugType);
    FWDWebLib::restoreLastErrorHandler();
  }
}

/*
 * Evento respons�vel por criar a estrutura do banco de dados, ou seja,
 * respons�vel p�or criar tabelas, fun��es, views, triggers, erc.
 */
class ExecuteScriptsEvent extends FWDRunnable {  
  public function run(){    
    $msAlias = trim(strtolower(FWDWebLib::getObject('alias')->getValue()));
    $msEmail = FWDWebLib::getObject('email')->getValue();
    $msPass = FWDWebLib::getObject('password')->getValue();
    $moConfig = new SAASConfig();
    $moConfig->loadConfig();
    
    if (PROVIDER){
    	$msLanguage = FWDWebLib::getObject('db_language')->getValue();
    } else {
    	$msLanguage = NULL;
    }
    
    $moManager = new SAASInstanceManager($moConfig, $msAlias);
    
    FWDWebLib::setErrorHandler(new FWDDoNothingErrorHandler());
    $mbDebugMode = FWDWebLib::getInstance()->getDebugMode();
    $mbDebugType = FWDWebLib::getInstance()->getDebugType();
    FWDWebLib::getInstance()->setDebugMode(false,0);
    
    $miLicenseType = FWDWebLib::getObject('license_type')->getValue();
    $mbTrial = in_array($miLicenseType, SAASLib::getTrialLicenseTypes()) ? true : false;
    
    if ($moManager->executeScripts($msEmail, $msPass, $mbTrial, $miLicenseType, $msLanguage)) {    
      echo "gebi('scripts_execution_right').style.display='block';";
      echo "trigger_event('copy_phps_event', 3);";
    }
    else {
      echo "gebi('scripts_execution_wrong').style.display='block';";
      echo "gobi('status').setValue('".$moManager->getLastError()."');";
    }
    
    FWDWebLib::getInstance()->setDebugMode($mbDebugMode,$mbDebugType);
    FWDWebLib::restoreLastErrorHandler();
  }
}

/*
 * Evento respons�vel por copiar os phps da pasta fonte para
 * a pasta destino. Essa pasta que ir� armazenar a nova inst�ncia. 
 */
class CopyPHPsEvent extends FWDRunnable {  
  public function run(){
    $msAlias = trim(strtolower(FWDWebLib::getObject('alias')->getValue()));
    $moConfig = new SAASConfig();
    $moConfig->loadConfig();
    
    $moManager = new SAASInstanceManager($moConfig, $msAlias);
    
    FWDWebLib::setErrorHandler(new FWDDoNothingErrorHandler());
    $mbDebugMode = FWDWebLib::getInstance()->getDebugMode();
    $mbDebugType = FWDWebLib::getInstance()->getDebugType();
    FWDWebLib::getInstance()->setDebugMode(false,0);
    
    $miLicenseType = FWDWebLib::getObject('license_type')->getValue();
    $mbTrial = in_array($miLicenseType, SAASLib::getTrialLicenseTypes()) ? true : false;
    
    if ($moManager->copyPHPs($mbTrial)) {    
      echo "gebi('system_creation_right').style.display='block';";
      echo "trigger_event('adjusting_db_and_system_event', 3);";
    }
    else {
      echo "gebi('system_creation_wrong').style.display='block';";
      echo "gobi('status').setValue('".$moManager->getLastError()."');";
    }
    
    FWDWebLib::getInstance()->setDebugMode($mbDebugMode,$mbDebugType);
    FWDWebLib::restoreLastErrorHandler();
  }
}

/*
 * Evento respons�vel por inserir as informa��es da licen�a
 * no banco de dados criado para nova inst�ncia, criar o
 * arquivo config.php com os dados do banco de dados, e
 * inserir as informa��es da nova inst�ncia no banco de dados
 * da administra��o das inst�ncias.
 */
class AdjustingDBAndSystemEvent extends FWDRunnable {  
  public function run(){
    $msAlias = trim(strtolower(FWDWebLib::getObject('alias')->getValue()));
    $msCode = FWDWebLib::getObject('activation_code')->getValue();    
    $moConfig = new SAASConfig();
    $moConfig->loadConfig();
    
    $moManager = new SAASInstanceManager($moConfig, $msAlias);
    
    FWDWebLib::setErrorHandler(new FWDDoNothingErrorHandler());
    $mbDebugMode = FWDWebLib::getInstance()->getDebugMode();
    $mbDebugType = FWDWebLib::getInstance()->getDebugType();
    FWDWebLib::getInstance()->setDebugMode(false,0);   
    
    $miLicenseType = FWDWebLib::getObject('license_type')->getValue();
    $maLicenseInfo = SAASLib::getLicenseInfo($moConfig, $msAlias, $miLicenseType);    
    
    if ($moManager->adjustDBAndSystem($maLicenseInfo)) {      
      if ($moManager->insertInstance($msAlias, $msCode)) {
        echo "gebi('adjusting_db_system_right').style.display='block';";      
        echo "gobi('status').setValue('".FWDLanguage::getPHPStringValue('st_creation_successfull',"A cria��o do sistema foi feita com sucesso.")."');";      
        if (PROVIDER) echo "soWindow = soPopUpManager.getPopUpById('popup_instance_insert_minimarisk').getOpener();";
        else echo "soWindow = soPopUpManager.getPopUpById('popup_instance_insert').getOpener();";
        echo "if(soWindow.refresh_grid) soWindow.refresh_grid();";
      }
      else {
        echo "gebi('adjusting_db_system_wrong').style.display='block';";
        echo "gobi('status').setValue('".$moManager->getLastError()."');";  
      }
    }
    else {
      echo "gebi('adjusting_db_system_wrong').style.display='block';";
      echo "gobi('status').setValue('".$moManager->getLastError()."');";
    }
    
    FWDWebLib::getInstance()->setDebugMode($mbDebugMode,$mbDebugType);
    FWDWebLib::restoreLastErrorHandler();
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
    $moStartEvent->addAjaxEvent(new SaveEvent('save_event'));
    $moStartEvent->addAjaxEvent(new CreateUserDBEvent('create_user_db_event'));
    $moStartEvent->addAjaxEvent(new ExecuteScriptsEvent('execute_scripts_event'));
    $moStartEvent->addAjaxEvent(new CopyPHPsEvent('copy_phps_event'));
    $moStartEvent->addAjaxEvent(new AdjustingDBAndSystemEvent('adjusting_db_and_system_event'));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
if (PROVIDER){
	FWDWebLib::getInstance()->xml_load('popup_instance_insert_minimarisk.xml');
} else {
	FWDWebLib::getInstance()->xml_load('popup_instance_insert.xml');
}
?>
