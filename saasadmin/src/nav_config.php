<?php
include_once "include.php";

class SaveEvent extends FWDRunnable implements FWDErrorHandler {
  
 /**
  * M�todo que manipula erros.
  *
  * <p>M�todo que manipula erros.</p>
  * @access public
  * @param integer $piErrNo N�mero do erro
  * @param string $psErrStr Mensagem de erro
  * @param string $psErrFile Nome do arquivo que gerou o erro
  * @param integer $piErrLine N�mero da linha em que ocorreu o erro
  */
  public function handleError($piErrNo, $psErrStr, $psErrFile, $piErrLine){
    echo "gobi('status').setValue('".FWDLanguage::getPHPStringValue('st_could_not_connect_to_database',"N�o foi poss�vel estabelecer conex�o com o servidor de banco de dados.")."');";
    exit;
  }
  
 /**
  * M�todo que manipula exce��es.
  *
  * <p>M�todo que manipula exce��es.</p>
  * @access public
  * @param FWDException $poFWDException Exce��o
  * @static
  */
  public static function handleException($poFWDException){
  }
 
  public function run(){
    $mbOk = true;
    
    /*
     * Testa se web_folder � um diret�rio.
     */
    if (!is_dir(FWDWebLib::getObject('web_folder')->getValue())) {
      $mbOk = false;
      echo "gobi('invalid_web_folder').show();";
    }
    
    /*
     * Testa se web_src_folder � um diret�rio.
     */
    if (!is_dir(FWDWebLib::getObject('web_src_folder')->getValue())) {
      $mbOk = false;
      echo "gobi('invalid_web_src_folder').show();";
    }
    
    /*
     * Testa se admin_folder � um diret�rio.
     */
    if (!is_dir(FWDWebLib::getObject('admin_folder')->getValue())) {
      $mbOk = false;
      echo "gobi('invalid_admin_folder').show();";
    }
    
    /*
     * Testa conex�o com o banco de dados.
     */
    $moDB = new FWDDB(
      DB_POSTGRES,
      FWDWebLib::getObject('db_name')->getValue(),
      FWDWebLib::getObject('db_user')->getValue(),
      FWDWebLib::getObject('db_pass')->getValue(),
      FWDWebLib::getObject('db_server')->getValue()
    );
    
    FWDWebLib::setErrorHandler($this);
    $moDB->connect();
    FWDWebLib::restoreLastErrorHandler();
    
    if ($mbOk) {
      $moConfig = new SAASConfig();
      $moConfig->setConfig(CFG_DB, FWDWebLib::getObject('db_name')->getValue());
      $moConfig->setConfig(CFG_DB_USER, FWDWebLib::getObject('db_user')->getValue());
      $moConfig->setConfig(CFG_DB_USER_PASS, FWDWebLib::getObject('db_pass')->getValue());
      $moConfig->setConfig(CFG_DB_SERVER, FWDWebLib::getObject('db_server')->getValue());
      $msWebFolder = trim(FWDWebLib::getObject('web_folder')->getValue());
      $msWebSrcFolder = trim(FWDWebLib::getObject('web_src_folder')->getValue());
      $msAdminFolder = trim(FWDWebLib::getObject('admin_folder')->getValue());
      if (($msWebFolder[strlen($msWebFolder)-1] != "\\") && ($msWebFolder[strlen($msWebFolder)-1] != "/")) $msWebFolder .= "/";
      if (($msWebSrcFolder[strlen($msWebSrcFolder)-1] != "\\") && ($msWebSrcFolder[strlen($msWebSrcFolder)-1] != "/")) $msWebSrcFolder .= "/";      
      if (($msAdminFolder[strlen($msAdminFolder)-1] != "\\") && ($msAdminFolder[strlen($msAdminFolder)-1] != "/")) $msAdminFolder .= "/";
      $moConfig->setConfig(CFG_WEB_INSTANCE_PATH, $msWebFolder);
      $moConfig->setConfig(CFG_WEB_INSTANCE_SRC_PATH, $msWebSrcFolder);
      $moConfig->setConfig(CFG_ADMIN_PATH, $msAdminFolder);
      $moConfig->setConfig(CFG_AXUR_EMAIL, FWDWebLib::getObject('email')->getValue());
      $moConfig->setConfig(CFG_AXUR_DEFAULT_SENDER, FWDWebLib::getObject('sender')->getValue());
      $moConfig->setConfig(CFG_AXUR_DEFAULT_SENDER_NAME, FWDWebLib::getObject('sender_name')->getValue());
      $moConfig->setConfig(CFG_ABANDONMENT_DAYS, FWDWebLib::getObject('abandonment_days')->getValue());
      $moConfig->setConfig(CFG_CURRENT_ID, FWDWebLib::getObject('current_id')->getValue());
      $moConfig->setConfig(CFG_MAX_UPLOAD_FILE_SIZE, FWDWebLib::getObject('max_file_size')->getValue());
      echo "gebi('current_id').disabled = true;";
      
      $msTrialUrl = FWDWebLib::getObject('trial_url')->getValue();
      if(($msTrialUrl[strlen($msTrialUrl)-1] != "\\") && ($msTrialUrl[strlen($msTrialUrl)-1] != '/')) $msTrialUrl.= '/';
      $moConfig->setConfig(CFG_TRIAL_URL, $msTrialUrl);
      
      $msCommercialUrl = FWDWebLib::getObject('commercial_url')->getValue();
      if(($msCommercialUrl[strlen($msCommercialUrl)-1] != "\\") && ($msCommercialUrl[strlen($msCommercialUrl)-1] != '/')) $msCommercialUrl.= '/';
      $moConfig->setConfig(CFG_COMMERCIAL_URL, $msCommercialUrl);
      
      $moConfig->setConfig(CFG_BUY_NOW_URL, FWDWebLib::getObject('buy_now_url')->getValue());
      
      $maEmailUpdateErrorInstances = array();
      if (FWDWebLib::getObject('email')->getValue() <> FWDWebLib::getObject('last_email')->getValue()) {
        $maEmailUpdateErrorInstances = SAASInstanceManager::updateAllInstances(SAASInstance::SAAS_REPORT_EMAIL, FWDWebLib::getObject('email')->getValue(), TABLE_SAAS_CONFIG);
        $maEmailUpdateErrorInstances = array_merge($maEmailUpdateErrorInstances, SAASInstanceManager::updateAllInstances(SAASInstance::ERROR_REPORT_EMAIL, FWDWebLib::getObject('email')->getValue(), TABLE_ISMS_CONFIG));
      }
      $maSenderUpdateErrorInstances = array();
      if (FWDWebLib::getObject('sender')->getValue() <> FWDWebLib::getObject('last_sender')->getValue()) {
        $maSenderUpdateErrorInstances = SAASInstanceManager::updateAllInstances(SAASInstance::DEFAULT_EMAIL_SENDER, FWDWebLib::getObject('sender')->getValue(), TABLE_ISMS_CONFIG);
      }            
      $maSenderNameUpdateErrorInstances = array();
      if (FWDWebLib::getObject('sender_name')->getValue() <> FWDWebLib::getObject('last_sender_name')->getValue()) {
        $maSenderNameUpdateErrorInstances = SAASInstanceManager::updateAllInstances(SAASInstance::DEFAULT_EMAIL_SENDER_NAME, FWDWebLib::getObject('sender_name')->getValue(), TABLE_ISMS_CONFIG);
      }
      $maMaxFileSizeUpdateErrorInstances = array();
      if(FWDWebLib::getObject('max_file_size')->getValue() != FWDWebLib::getObject('last_max_file_size')->getValue()){
        $maMaxFileSizeUpdateErrorInstances = SAASInstanceManager::updateAllInstances(SAASInstance::MAX_UPLOAD_FILE_SIZE, FWDWebLib::getObject('max_file_size')->getValue(), TABLE_SAAS_CONFIG);
      }
      
      $maErrorMsg = array();
      if (count($maEmailUpdateErrorInstances)) {
        foreach ($maEmailUpdateErrorInstances as $miKey => $maValue) {
          $maErrorMsg[$miKey] = $maValue[0] . " (" . $maValue[1] . ")";
        }
      }      
      if (count($maSenderUpdateErrorInstances)) {
        foreach ($maSenderUpdateErrorInstances as $miKey => $maValue) {
          if (!array_key_exists($miKey, $maErrorMsg)) $maErrorMsg[$miKey] = $maValue[0] . " (" . $maValue[1] . ")";
        }
      }
      if (count($maSenderNameUpdateErrorInstances)) {
        foreach ($maSenderNameUpdateErrorInstances as $miKey => $maValue) {
          if (!array_key_exists($miKey, $maErrorMsg)) $maErrorMsg[$miKey] = $maValue[0] . " (" . $maValue[1] . ")";
        }
      }
      if(count($maMaxFileSizeUpdateErrorInstances)){
        foreach($maMaxFileSizeUpdateErrorInstances as $miKey => $maValue){
          if(!array_key_exists($miKey, $maErrorMsg)) $maErrorMsg[$miKey] = $maValue[0] . " (" . $maValue[1] . ")";
        }
      }
      $msErrorMsg = "";
      if (count($maErrorMsg))          
        $msErrorMsg = FWDLanguage::getPHPStringValue('st_error_msg',"No entanto, o email de contato da AXUR e/ou sender default das seguintes inst�ncias n�o puderam ser modificados: ") . implode(', ', $maErrorMsg);      
      echo "gobi('status').setValue('".FWDLanguage::getPHPStringValue('st_update_config_ok',"Atualiza��o feita com sucesso.")." ".$msErrorMsg."');";
      
      /*
       * Atualiza os valores.
       */
      echo "gebi('last_email').value = '".FWDWebLib::getObject('email')->getValue()."';";
      echo "gebi('last_sender').value = '".FWDWebLib::getObject('sender')->getValue()."';";
      echo "gebi('last_sender_name').value = '".FWDWebLib::getObject('sender_name')->getValue()."';";
      echo "gebi('last_max_file_size').value = '".FWDWebLib::getObject('max_file_size')->getValue()."';";
    }
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new SaveEvent('save_event'));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moConfig = new SAASConfig();
    $moConfig->loadConfig();    
    FWDWebLib::getObject('db_name')->setValue($moConfig->getConfig(CFG_DB));
    FWDWebLib::getObject('db_user')->setValue($moConfig->getConfig(CFG_DB_USER));
    FWDWebLib::getObject('db_pass')->setValue($moConfig->getConfig(CFG_DB_USER_PASS));
    FWDWebLib::getObject('db_server')->setValue($moConfig->getConfig(CFG_DB_SERVER));
    FWDWebLib::getObject('web_folder')->setValue($moConfig->getConfig(CFG_WEB_INSTANCE_PATH));
    FWDWebLib::getObject('web_src_folder')->setValue($moConfig->getConfig(CFG_WEB_INSTANCE_SRC_PATH));
    FWDWebLib::getObject('admin_folder')->setValue($moConfig->getConfig(CFG_ADMIN_PATH));
    FWDWebLib::getObject('email')->setValue($moConfig->getConfig(CFG_AXUR_EMAIL));
    FWDWebLib::getObject('sender')->setValue($moConfig->getConfig(CFG_AXUR_DEFAULT_SENDER));
    FWDWebLib::getObject('sender_name')->setValue($moConfig->getConfig(CFG_AXUR_DEFAULT_SENDER_NAME));
    FWDWebLib::getObject('abandonment_days')->setValue($moConfig->getConfig(CFG_ABANDONMENT_DAYS));
    FWDWebLib::getObject('trial_url')->setValue($moConfig->getConfig(CFG_TRIAL_URL));
    FWDWebLib::getObject('commercial_url')->setValue($moConfig->getConfig(CFG_COMMERCIAL_URL));
    FWDWebLib::getObject('buy_now_url')->setValue($moConfig->getConfig(CFG_BUY_NOW_URL));
    FWDWebLib::getObject('max_file_size')->setValue($moConfig->getConfig(CFG_MAX_UPLOAD_FILE_SIZE));
    $miInitialId = $moConfig->getConfig(CFG_CURRENT_ID);
    FWDWebLib::getObject('current_id')->setValue($miInitialId);
    if ($miInitialId) FWDWebLib::getObject('current_id')->setAttrDisabled("true");
    
    /*
     * Aramazena os valores antigos para ver se precisa atualizar
     * todas as inst�ncias caso o usu�rio clique em salvar.
     */
    FWDWebLib::getObject('last_email')->setValue($moConfig->getConfig(CFG_AXUR_EMAIL));
    FWDWebLib::getObject('last_sender')->setValue($moConfig->getConfig(CFG_AXUR_DEFAULT_SENDER));
    FWDWebLib::getObject('last_sender_name')->setValue($moConfig->getConfig(CFG_AXUR_DEFAULT_SENDER_NAME));
    FWDWebLib::getObject('last_max_file_size')->setValue($moConfig->getConfig(CFG_MAX_UPLOAD_FILE_SIZE));
    
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_config.xml");
?>