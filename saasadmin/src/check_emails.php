<?php

include_once 'include.php';
include_once $handlers_ref.'QueryEmailsTrial.php';
include_once $handlers_ref.'QueryLicenseInfo.php';

set_time_limit(7200);

class Logger {

  const INFO = 1;
  const WARNING = 2;
  const ERROR = 3;

  private $csFileName = '';
  private $crHandler = null;
  
  public function __construct($psFileName){
    $this->csFileName = $psFileName;
  }

  public function openLog(){
    $this->crHandler = fopen($this->csFileName,'a');
  }
  
  public function write($piType,$psMessage){
    switch($piType){
      case self::INFO:    $msType = 'INFO';    break;
      case self::WARNING: $msType = 'WARNING'; break;
      case self::ERROR:   $msType = 'ERROR';   break;
      default:            $msType = 'UNKNOWN';
    }
    echo "[".SAASLib::getSAASDate(SAASLib::SAASTime(), true)."][{$msType}] {$psMessage}<br>\n";
    fwrite($this->crHandler, "[".SAASLib::getSAASDate(SAASLib::SAASTime(), true)."][{$msType}] {$psMessage}\n");
  }

  public function closeLog(){
    fclose($this->crHandler);
  }
  
}

class Mailer {

  private $csFrom = '';
  private $csSenderName = '';
  protected $caSubjects = array();
  protected $caMessages = array();

  public function __construct($psFrom, $psSenderName){
    $this->csFrom = $psFrom;
    $this->csSenderName = $psSenderName;
  }

  public function sendEmail($piType,$psTo,$psSubject='',$psMessage=''){
    $moEmail = new FWDEmail();
    $moEmail->setSenderName($this->csSenderName);
    $moEmail->setFrom($this->csFrom);
    $moEmail->setTo($psTo);
    if($piType){
      $moEmail->setSubject($this->caSubjects[$piType]);
      $moEmail->setMessage($this->caMessages[$piType]);
    }else{
      $moEmail->setSubject($psSubject);
      $moEmail->setMessage($psMessage);
    }
    return $moEmail->send();
  }
}

class ISMSMailer extends Mailer {
  public function __construct($psFrom, $psSenderName){
    parent::__construct($psFrom, $psSenderName);

    $this->caSubjects[1] = FWDLanguage::getPHPStringValue('em_isms_email_type1_subject', "What did you think about Real ISMS?");
    $this->caSubjects[2] = FWDLanguage::getPHPStringValue('em_isms_email_type2_subject', "You have 5 days left of your Real ISMS Trial");
    $this->caSubjects[3] = FWDLanguage::getPHPStringValue('em_isms_email_type3_subject', "Thank you for using Real ISMS Trial");

    $this->caMessages[1] = FWDLanguage::getPHPStringValue('em_isms_email_type1_message', "We're conducting customer research on how we can improve Real ISMS and we would really value your input. It shouldn't take more than 2 minutes to complete.<br/><br/>Take the Survey <a href='http://www.surveymonkey.com/s.aspx?sm=F9BFTdst8Ginn7PdgNGxFA_3d_3d'>here</a>!<br/><br/>We truly value our customers inputs, and we routinely implement requested features or changes. Your opinion matters to us!<br/><br/>Our kindly regards,<br/> Realiso Team<br/><br/><br/><b>Realiso Corp</b><br/>626 RexCorp Plaza<br/>Uniondale, New York 11556<br/>United States<br/><br/>http://www.realiso.com/realisms<br/>http://www.realiso.com");
    $this->caMessages[2] = FWDLanguage::getPHPStringValue('em_isms_email_type2_message', "You only have 5 days left in your trial. If you have any questions, please contact us!<br/><br/>To buy Real ISMS, simply log into your trial account and click the 'Buy Now' link in above left-hand corner or access our hotiste at http://www.realiso.com/realisms<br/><br/>Once again, thank you for your interest in Real ISMS.<br/><br/>Our kindly regards,<br/>Realiso Team<br/><br/><br/><b>Realiso Corp</b><br/>626 RexCorp Plaza<br/>Uniondale, New York 11556<br/>United States<br/><br/>http://www.realiso.com/realisms<br/>http://www.realiso.com");
    $this->caMessages[3] = FWDLanguage::getPHPStringValue('em_isms_email_type3_message', "If you haven't subscribed to Real ISMS yet, please tell us why. Your answer will drive us to fully deliver what you need. This survey will take only 2 minutes. <a href='http://www.surveymonkey.com/s.aspx?sm=QSLS7DxIv5VgVxVCWPsv_2bw_3d_3d'>Click here</a>!<br/><br/>Thank you for helping us deliver the best solution in ISO 27001 implementation &amp; management.<br/><br/>Our kindly regards,<br/>Realiso Team<br/><br/><br/><b>Realiso Corp</b><br/>626 RexCorp Plaza<br/>Uniondale, New York 11556<br/>United States<br/><br/>http://www.realiso.com/realisms<br/>http://www.realiso.com");
  }
}

class EMSMailer extends Mailer {
  public function __construct($psFrom, $psSenderName){
    parent::__construct($psFrom, $psSenderName);

    $this->caSubjects[1] = FWDLanguage::getPHPStringValue('em_ems_email_type1_subject', "O que voc� achou do Real EMS?");
    $this->caSubjects[2] = FWDLanguage::getPHPStringValue('em_ems_email_type2_subject', "Voc� tem mais 5 dias no seu Real EMS Trial");
    $this->caSubjects[3] = FWDLanguage::getPHPStringValue('em_ems_email_type3_subject', "Obrigado por utilizar o Real EMS Trial");

    $this->caMessages[1] = FWDLanguage::getPHPStringValue('em_ems_email_type1_message', "N�s estamos realizando uma pesquisa de satisfa��o sobre como podemos melhorar o Real EMS e gostar�amos muito de sua opini�o. N�o demora mais do que 2 minutos.<br/><br/>Fa�a <a href='http://www.surveymonkey.com/s.aspx?sm=Iv5Lu2xahyHtsNrd2ShHBA_3d_3d'>aqui</a>!<br/><br/>N�s apreciamos a opini�o de nossos clientes e rotineiramente implementamos melhorias baseadas nestas opini�es. Sua participa��o � muito importante para n�s!<br/><br/>Atenciosamente,<br/> Realiso Team<br/><br/><br/><b>Realiso Corp</b><br/>626 RexCorp Plaza<br/>Uniondale, New York 11556<br/>United States<br/><br/>http://www.realiso.com/realems<br/>http://www.realiso.com");
    $this->caMessages[2] = FWDLanguage::getPHPStringValue('em_ems_email_type2_message', "Voc� tem apenas 5 dias restando no seu trial. Se voc� possui alguma quest�o, por favor, entre em contato conosco.<br/><br/>Para comprar o Real EMS, fa�a o log in na sua conta de trial e clique no link Compre Agora no canto esquerdo superior ou em nosso hotsite http://www.realiso.com/realems<br/><br/>Agradecemos mais uma vez pelo seu interesse no Real EMS.<br/><br/>Atenciosamente,<br/>Realiso Team<br/><br/><br/><b>Realiso Corp</b><br/>626 RexCorp Plaza<br/>Uniondale, New York 11556<br/>United States<br/><br/>http://www.realiso.com/realems<br/>http://www.realiso.com");
    $this->caMessages[3] = FWDLanguage::getPHPStringValue('em_ems_email_type3_message', "Se voc� ainda n�o se inscreveu no Real EMS, por favor, nos informe o motivo. Sua resposta nos auxiliar� na melhoria cont�nua de nossas solu��es e ofertas. Esta pesquisa demora somente 2 minutos. <a href='http://www.surveymonkey.com/s.aspx?sm=UuXDkpRUnjSpehyhBeeJBQ_3d_3d'>Clique aqui</a>!<br/><br/>Obrigado por nos auxiliar em entregar a melhor solu��o para EMS do mercado.<br/><br/>Atenciosamente,<br/>Realiso Team<br/><br/><br/><b>Realiso Corp</b><br/>626 RexCorp Plaza<br/>Uniondale, New York 11556<br/>United States<br/><br/> http://www.realiso.com/realems<br/>http://www.realiso.com");
  }
}

class OHSMailer extends Mailer {
  public function __construct($psFrom, $psSenderName){
    parent::__construct($psFrom, $psSenderName);

    $this->caSubjects[1] = FWDLanguage::getPHPStringValue('em_ohs_email_type1_subject', "O que voc� achou do Real OHS?");
    $this->caSubjects[2] = FWDLanguage::getPHPStringValue('em_ohs_email_type2_subject', "Voc� tem mais 5 dias no seu Real OHS Trial");
    $this->caSubjects[3] = FWDLanguage::getPHPStringValue('em_ohs_email_type3_subject', "Obrigado por utilizar o Real OHS Trial");

    $this->caMessages[1] = FWDLanguage::getPHPStringValue('em_ohs_email_type1_message', "N�s estamos realizando uma pesquisa de satisfa��o sobre como podemos melhorar o Real OHS e gostar�amos muito de sua opini�o. N�o demora mais do que 2 minutos.<br/><br/>Fa�a <a href='http://www.surveymonkey.com/s.aspx?sm=Iv5Lu2xahyHtsNrd2ShHBA_3d_3d'>aqui</a>!<br/><br/>N�s apreciamos a opini�o de nossos clientes e rotineiramente implementamos melhorias baseadas nestas opini�es. Sua participa��o � muito importante para n�s!<br/><br/>Atenciosamente,<br/>Realiso Team<br/><br/><br/><b>Realiso Corp</b><br/>626 RexCorp Plaza<br/>Uniondale, New York 11556<br/>United States<br/><br/>http://www.realiso.com/realohs<br/>http://www.realiso.com");
    $this->caMessages[2] = FWDLanguage::getPHPStringValue('em_ohs_email_type2_message', "Voc� tem apenas 5 dias restando no seu trial. Se voc� possui alguma quest�o, por favor, entre em contato conosco.<br/><br/>Para comprar o Real OHS, fa�a o log in na sua conta de trial e clique no link Compre Agora no canto esquerdo superior ou em nosso hotsite http://www.realiso.com/realohs<br/><br/>Agradecemos mais uma vez pelo seu interesse no Real OHS.<br/><br/>Atenciosamente,<br/>Realiso Team<br/><br/><br/><b>Realiso Corp</b><br/>626 RexCorp Plaza<br/>Uniondale, New York 11556<br/>United States<br/><br/> http://www.realiso.com/realohs<br/>http://www.realiso.com");
    $this->caMessages[3] = FWDLanguage::getPHPStringValue('em_ohs_email_type3_message', "Se voc� ainda n�o se inscreveu no Real OHS, por favor, nos informe o motivo. Sua resposta nos auxiliar� na melhoria cont�nua de nossas solu��es e ofertas. Esta pesquisa demora somente 2 minutos. <a href='http://www.surveymonkey.com/s.aspx?sm=UuXDkpRUnjSpehyhBeeJBQ_3d_3d'>Clique aqui</a>!<br/><br/>Obrigado por nos auxiliar em entregar a melhor solu��o para OHS do mercado.<br/><br/>Atenciosamente,<br/>Realiso Team<br/><br/><br/><b>Realiso Corp</b><br/>626 RexCorp Plaza<br/>Uniondale, New York 11556<br/>United States<br/><br/> http://www.realiso.com/realohs<br/>http://www.realiso.com");
  }
}

class SOXMailer extends Mailer {
  public function __construct($psFrom, $psSenderName){
    parent::__construct($psFrom, $psSenderName);

    $this->caSubjects[1] = FWDLanguage::getPHPStringValue('em_sox_email_type1_subject', "What did you think about Real SOX?");
    $this->caSubjects[2] = FWDLanguage::getPHPStringValue('em_sox_email_type2_subject', "You have 5 days left of your Real SOX Trial");
    $this->caSubjects[3] = FWDLanguage::getPHPStringValue('em_sox_email_type3_subject', "Thank you for using Real SOX Trial");

    $this->caMessages[1] = FWDLanguage::getPHPStringValue('em_sox_email_type1_message', "We're conducting customer research on how we can improve Real SOX and we would really value your input. It shouldn't take more than 2 minutes to complete.<br/><br/>Take the Survey <a href='http://www.surveymonkey.com/s.aspx?sm=F9BFTdst8Ginn7PdgNGxFA_3d_3d'>here</a>!<br/><br/>We truly value our customers inputs, and we routinely implement requested features or changes. Your opinion matters to us!<br/><br/>Our kindly regards,<br/> Realiso Team<br/><br/><br/><b>Realiso Corp</b><br/>626 RexCorp Plaza<br/>Uniondale, New York 11556<br/>United States<br/><br/>http://www.realiso.com/realsox<br/>http://www.realiso.com");
    $this->caMessages[2] = FWDLanguage::getPHPStringValue('em_sox_email_type2_message', "You only have 5 days left in your trial. If you have any questions, please contact us!<br/><br/>To buy Real SOX, simply log into your trial account and click the 'Buy Now' link in above left-hand corner or access our hotiste at http://www.realiso.com/realsox<br/><br/>Once again, thank you for your interest in Real SOX.<br/><br/>Our kindly regards,<br/>Realiso Team<br/><br/><br/><b>Realiso Corp</b><br/>626 RexCorp Plaza<br/>Uniondale, New York 11556<br/>United States<br/><br/>http://www.realiso.com/realsox<br/>http://www.realiso.com");
    $this->caMessages[3] = FWDLanguage::getPHPStringValue('em_sox_email_type3_message', "If you haven't subscribed to Real SOX yet, please tell us why. Your answer will drive us to fully deliver what you need. This survey will take only 2 minutes. <a href='http://www.surveymonkey.com/s.aspx?sm=QSLS7DxIv5VgVxVCWPsv_2bw_3d_3d'>Click here</a>!<br/><br/>Thank you for helping us deliver the best solution in ISO 27001 implementation &amp; management.<br/><br/>Our kindly regards,<br/>Realiso Team<br/><br/><br/><b>Realiso Corp</b><br/>626 RexCorp Plaza<br/>Uniondale, New York 11556<br/>United States<br/><br/>http://www.realiso.com/realsox<br/>http://www.realiso.com");
  }
}

class PCIMailer extends Mailer {
  public function __construct($psFrom, $psSenderName){
    parent::__construct($psFrom, $psSenderName);

    $this->caSubjects[1] = FWDLanguage::getPHPStringValue('em_pci_email_type1_subject', "What did you think about Real PCI?");
    $this->caSubjects[2] = FWDLanguage::getPHPStringValue('em_pci_email_type2_subject', "You have 5 days left of your Real PCI Trial");
    $this->caSubjects[3] = FWDLanguage::getPHPStringValue('em_pci_email_type3_subject', "Thank you for using Real PCI Trial");

    $this->caMessages[1] = FWDLanguage::getPHPStringValue('em_pci_email_type1_message', "We're conducting customer research on how we can improve Real PCI and we would really value your input. It shouldn't take more than 2 minutes to complete.<br/><br/>Take the Survey <a href='http://www.surveymonkey.com/s.aspx?sm=F9BFTdst8Ginn7PdgNGxFA_3d_3d'>here</a>!<br/><br/>We truly value our customers inputs, and we routinely implement requested features or changes. Your opinion matters to us!<br/><br/>Our kindly regards,<br/> Realiso Team<br/><br/><br/><b>Realiso Corp</b><br/>626 RexCorp Plaza<br/>Uniondale, New York 11556<br/>United States<br/><br/>http://www.realiso.com/realpci<br/>http://www.realiso.com");
    $this->caMessages[2] = FWDLanguage::getPHPStringValue('em_pci_email_type2_message', "You only have 5 days left in your trial. If you have any questions, please contact us!<br/><br/>To buy Real PCI, simply log into your trial account and click the 'Buy Now' link in above left-hand corner or access our hotiste at http://www.realiso.com/realpci<br/><br/>Once again, thank you for your interest in Real PCI.<br/><br/>Our kindly regards,<br/>Realiso Team<br/><br/><br/><b>Realiso Corp</b><br/>626 RexCorp Plaza<br/>Uniondale, New York 11556<br/>United States<br/><br/>http://www.realiso.com/realpci<br/>http://www.realiso.com");
    $this->caMessages[3] = FWDLanguage::getPHPStringValue('em_pci_email_type3_message', "If you haven't subscribed to Real PCI yet, please tell us why. Your answer will drive us to fully deliver what you need. This survey will take only 2 minutes. <a href='http://www.surveymonkey.com/s.aspx?sm=QSLS7DxIv5VgVxVCWPsv_2bw_3d_3d'>Click here</a>!<br/><br/>Thank you for helping us deliver the best solution in ISO 27001 implementation &amp; management.<br/><br/>Our kindly regards,<br/>Realiso Team<br/><br/><br/><b>Realiso Corp</b><br/>626 RexCorp Plaza<br/>Uniondale, New York 11556<br/>United States<br/><br/>http://www.realiso.com/realpci<br/>http://www.realiso.com");
  }
}

// Instancia o logger
$goLogger = new Logger('emails_log.txt');
$goLogger->openLog();
$goLogger->write(Logger::INFO,"Checking emails");

// Carrega as configura��es e armazena os dados necess�rios
$goConfig = new SAASConfig();
$goConfig->loadConfig();
$gsSender = $goConfig->getConfig(CFG_AXUR_DEFAULT_SENDER);
$gsSenderName = $goConfig->getConfig(CFG_AXUR_DEFAULT_SENDER_NAME);
$gsAxurEmail = $goConfig->getConfig(CFG_AXUR_EMAIL);
$gsDBHost = $goConfig->getConfig(CFG_DB_SERVER);

// Calcula as datas de interesse
$giToday = FWDWebLib::getTime();
$giYear = date('Y',$giToday);
$giMonth = date('m',$giToday);
$giDay = date('d',$giToday);

$giCreationDateType1   = mktime(0,0,0,$giMonth,$giDay - 5,$giYear);
$giExpirationDateType2 = mktime(0,0,0,$giMonth,$giDay + 5,$giYear);
$giExpirationDateType3 = mktime(0,0,0,$giMonth,$giDay - 3,$giYear);

// Faz a consulta pra pegar as inst�ncias de trial
$goQuery = new QueryEmailsTrial();
$goQuery->setLastDate($giCreationDateType1 + 86399);
$goQuery->makeQuery();
$goQuery->executeQuery();

$gaInstancesToRemove = array();
$gaInstancesWithError = array();

while($goQuery->fetch()){
  $gsAlias = $goQuery->getFieldValue('instance_alias');
  $goDB = new FWDDB(
    DB_POSTGRES,
    $goQuery->getFieldValue('instance_db_name'),
    $goQuery->getFieldValue('instance_db_user'),
    $goQuery->getFieldValue('instance_db_user_pass'),
    $gsDBHost
  );
  FWDWebLib::setErrorHandler(new FWDDoNothingErrorHandler());
  if($goDB->connect()){
    // descobre a data de expira��o
    $goQueryLicense = new QueryLicenseInfo($goDB);
    $goQueryLicense->makeQuery();
    $gaLicenseInfo = $goQueryLicense->executeQuery();
    
    // Instancia o mailer correto de acordo com o tipo do sistema
    $giLicenseType = SAASLib::convertSystemLicenseType2AdminLicenseType($gaLicenseInfo[SAASInstance::LICENSE_TYPE]);
    if (SAASLib::isISMSLicense($giLicenseType)) $goMailer = new ISMSMailer($gsSender, $gsSenderName);
    else if (SAASLib::isEMSLicense($giLicenseType)) $goMailer = new EMSMailer($gsSender, $gsSenderName);
    else if (SAASLib::isOHSLicense($giLicenseType)) $goMailer = new OHSMailer($gsSender, $gsSenderName);
    else if (SAASLib::isSOXLicense($giLicenseType)) $goMailer = new SOXMailer($gsSender, $gsSenderName);
    else if (SAASLib::isPCILicense($giLicenseType)) $goMailer = new PCIMailer($gsSender, $gsSenderName);
    else {
      $gaInstancesWithError[] = $gsAlias;
      $goLogger->write(Logger::ERROR,"Could not define license type of alias {$gsAlias}");
    }
    
    $giExpirationDate = mktime(0,0,0,date('m',$gaLicenseInfo[SAASInstance::LICENSE_EXPIRACY]),date('d',$gaLicenseInfo[SAASInstance::LICENSE_EXPIRACY]),date('Y',$gaLicenseInfo[SAASInstance::LICENSE_EXPIRACY]));
    $giInstanceCreationDate = strtotime(substr($goQuery->getFieldValue('instance_creation_date'),0,10));
    // descobre o email do admin da instancia
    $goDataset = new FWDDBDataSet($goDB,'isms_saas');
    $goField = new FWDDBField('pkConfig','config_id',DB_NUMBER);
    $goField->addFilter(new FWDDBFilter('=',SAASInstance::EMAIL_ADMIN));
    $goDataset->addFWDDBField($goField);
    $goDataset->addFWDDBField(new FWDDBField('sValue','config_value',DB_STRING));
    $goDataset->select();
    // envia os emails que forem necessarios
    if($goDataset->fetch()){
      $gsReceiver = $goDataset->getFieldByAlias('config_value')->getValue();
      if($giInstanceCreationDate==$giCreationDateType1){
        // envia email do tipo 1
        if($goMailer->sendEmail(1,$gsReceiver)){
          $goLogger->write(Logger::INFO,"Email type 1 sent to {$gsAlias}");
        }else{
          $gaInstancesWithError[] = $gsAlias;
          $goLogger->write(Logger::ERROR,"Could not send email type 1 to {$gsAlias}");
        }
      }
      if($giExpirationDate==$giExpirationDateType2){
        // envia email do tipo 2
        if($goMailer->sendEmail(2,$gsReceiver)){
          $goLogger->write(Logger::INFO,"Email type 2 sent to {$gsAlias}");
        }else{
          $gaInstancesWithError[] = $gsAlias;
          $goLogger->write(Logger::ERROR,"Could not send email type 2 to {$gsAlias}");
        }
      }elseif($giExpirationDate==$giExpirationDateType3){
        // envia email do tipo 3
        if($goMailer->sendEmail(3,$gsReceiver)){
          $goLogger->write(Logger::INFO,"Email type 3 sent to {$gsAlias}");
        }else{
          $gaInstancesWithError[] = $gsAlias;
          $goLogger->write(Logger::ERROR,"Could not send email type 3 to {$gsAlias}");
        }
        $gaInstancesToRemove[] = $gsAlias;
      }
    }else{
      $gaInstancesWithError[] = $gsAlias;
      $goLogger->write(Logger::ERROR,"Could not obtain instance admin mail address.");
    }
  }else{
    $gaInstancesWithError[] = $gsAlias;
    $goLogger->write("Could not connect to {$gsAlias} database",Logger::ERROR);
  }
  FWDWebLib::restoreLastErrorHandler();
}

$gsMessage = '';
if(count($gaInstancesToRemove)){
  sort($gaInstancesToRemove);
  $gsMessage.= FWDLanguage::getPHPStringValue('em_instances_to_remove_bl_cl',"<b>Inst�ncias a Remover:</b><br/>");
  $gsMessage.= implode('<br/>',$gaInstancesToRemove);
  $gsMessage.= '<br/><br/>';
}
if(count($gaInstancesWithError)){
  $gaInstancesWithError = array_unique($gaInstancesWithError);
  sort($gaInstancesWithError);
  $gsMessage.= FWDLanguage::getPHPStringValue('em_instances_with_error_bl_cl',"<b>Inst�ncias com Erro:</b><br/>");
  $gsMessage.= implode('<br/>',$gaInstancesWithError);
  $gsMessage.= '<br/><br/>';
}

if($gsMessage){
  $gsSubject = FWDLanguage::getPHPStringValue('em_check_emails_report',"Realiso Check Emails Report");
  if($goMailer->sendEmail(0,$gsAxurEmail,$gsSubject,$gsMessage)){
    $goLogger->write(Logger::INFO,"Report email sent to {$gsAxurEmail}");
  }else{
    $goLogger->write(Logger::ERROR,"Could not send report email to {$gsAxurEmail}");
  }
}

$goLogger->write(Logger::INFO,"emails checked");
$goLogger->closeLog();

/*
======================================================================

pre-calcula:
  H - 5: dia da criacao das instancias q devem receber email do tipo 1
  H + 5: dia da expiracao das instancias q devem receber email do tipo 2
  H - 3: dia da expiracao das instancias q devem receber email do tipo 3

faz uma consulta pra pegar tds instancias d trial q foram criadas AT� H - 5
pra cada instancia retornada
  conecta no banco da instancia
  pega a data de expiracao (E)
  se D = H - 5
    envia email do tipo 1
  se E = H + 5
    envia email do tipo 2
  se E = H - 3
    envia email do tipo 3

======================================================================

- 5 dias ap�s a cria��o do trial enviar um email que ter� um link para o cliente responder uma pesquisa.

- 5 dias antes de expirar a licen�a, enviar um email para o cliente avisando.

- 3 dias ap�s expirar a licen�a, enviar um email que ter� um link para o usu�rio responder o que achou do produto.
    Tamb�m deve enviar um email para a axur avisando que a inst�ncia j� pode ser deletada.

======================================================================
*/

?>