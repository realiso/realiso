<?php
include_once "include.php";
set_time_limit(7200);

/*
 * Inicializa o logger.
 */
$goLogger = new SAASLogger('data_collection_log.txt');
$goLogger->openLog();

/*
 * Carrega as configurações e armazena os dados necessários. 
 */
$goConfig = new SAASConfig();
$goConfig->loadConfig();

/*
 * Percorre todas as instâncias para coletar os dados.
 */
$goInstance = new SAASInstance();
$goInstance->createFilter(0, 'instance_id', '<>');
$goInstance->select();
while ($goInstance->fetch()) {
  $gsAlias = $goInstance->getFieldValue('instance_alias');
  $goDB = new FWDDB(
    DB_POSTGRES,
    $goInstance->getFieldValue('instance_db_name'),
    $goInstance->getFieldValue('instance_db_user'),
    $goInstance->getFieldValue('instance_db_user_pass'),
    $goConfig->getConfig(CFG_DB_SERVER)
  );
  FWDWebLib::setErrorHandler(new FWDDoNothingErrorHandler());
  if ($goDB->connect()) {
    if ($goDB->execute("SELECT sValue as enabled FROM isms_config WHERE pkConfig = " . SAASInstance::DATA_COLLECTION_ENABLED)) {
      $gaRes = $goDB->fetch();
      if ($gaRes['enabled']) {
        if ($goDB->execute("SELECT create_stats()")) {
          $goLogger->write(SAASLogger::INFO, "Data from instance $gsAlias successfully collected.");
        }
        else {
          $goLogger->write(SAASLogger::ERROR, "Problems while collecting data from instance $gsAlias.");
        }
      }
      else {
        $goLogger->write(SAASLogger::INFO, "Data collection disabled on instance $gsAlias.");
      }
    }
    else {
      $goLogger->write(SAASLogger::ERROR, "Problems while retrieving config " . SAASInstance::DATA_COLLECTION_ENABLED . " from instance $gsAlias.");
    }
  }
  else {
    $goLogger->write(SAASLogger::ERROR, "Could not collect data from instance $gsAlias (database connection failed).");
  }
  FWDWebLib::restoreLastErrorHandler();
}

/*
 * Fecha o log.
 */
$goLogger->closeLog();
?>