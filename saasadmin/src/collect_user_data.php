<?php
include_once "include.php";

//$sys_ref = "";

//$classes_ref = $sys_ref  . "classes/";
///include_once $sys_ref . "lib/fwd5_setup.php";

//include_once $classes_ref . "SAASConfig.php";  
//include_once $classes_ref . "SAASInstance.php";
//include_once $classes_ref . "SAASLogger.php";

//$moConfig = new FWDConfig($classes_ref . 'config.php');
//if (!$moConfig->load()) trigger_error('error_config_corrupt',E_USER_ERROR);


set_time_limit(7200);

/*
 * Inicializa o logger.
 */
$goLogger = new SAASLogger('user_data_collection_log.txt');
$goLogger->openLog();

/*
 * Carrega as configurações e armazena os dados necessários.
 */
$goConfig = new SAASConfig();
$goConfig->loadConfig();

/*
 * Percorre todas as instâncias para coletar os dados.
 */
$goInstance = new SAASInstance();
$goInstance->createFilter(0, 'instance_id', '<>');
$goInstance->select();

while ($goInstance->fetch()) {

  $gsAlias = $goInstance->getFieldValue('instance_alias');
  $goDB = new FWDDB(
    DB_POSTGRES,
    $goInstance->getFieldValue('instance_db_name'),
    $goInstance->getFieldValue('instance_db_user'),
    $goInstance->getFieldValue('instance_db_user_pass'),
    $goConfig->getConfig(CFG_DB_SERVER)
  );
  FWDWebLib::setErrorHandler(new FWDDoNothingErrorHandler());
  
  if ($goDB->connect()) {
    /* Retorna a licença da Instância */
    if ($goDB->execute("SELECT sValue as fkproduct FROM isms_saas WHERE pkConfig = " . SAASInstance::LICENSE_TYPE)){
      $produto = $goDB->fetch();

      /* Cria um arquivo sql com os dados que serão inseridos na base do site da realiso.com*/
      if ($goDB->execute("SELECT fkcontext as id, sname as name, slogin as login, semail as email from isms_user")){
        $dados = $goDB->fetchAll();
        foreach ($dados as $d){
          file_put_contents("/tmp/isms_data_to_realiso_com.sql", "INSERT INTO realiso_users (id, instance, email, login, fkproduct) VALUES (".$d['id'].",'".$gsAlias."','".$d['email']."','".$d['login']."',".$produto['fkproduct'].");\n", FILE_APPEND);
        }
      } else {
        $goLogger->write(SAASLogger::ERROR, "Problems while collecting data from instance $gsAlias.");
      }
    }
  } else {
    $goLogger->write(SAASLogger::ERROR, "Could not collect data from instance $gsAlias (database connection failed).");
  }
  FWDWebLib::restoreLastErrorHandler();
}

/*
 * Fecha o log.
 */
$goLogger->closeLog();
?>