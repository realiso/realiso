<?php
include_once "include.php";

class LogoutEvent extends FWDRunnable {
	public function run() {  		  		
  		session_unset();
		header("Location: login.php");
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new LogoutEvent(""));
FWDStartEvent::getInstance()->start();

?>