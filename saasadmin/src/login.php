<?php

//session_start();
include_once "include.php";
set_time_limit(600);

// verifica se o usuario j� est� logado
$moUser = new SaasUser();
if($moUser->isActive()){
	echo '<script>document.location.href = "default.php";</script>';
}
//termina verifica��o

class LoginEvent extends FWDRunnable {
  public function run() {
    $msLogin = FWDWebLib::getObject('user_login')->getValue();
    $msPassword = FWDWebLib::getObject('user_password')->getValue();
    $moUsers = new SAASUser();
    if ($moUsers->fetchByLoginPass($msLogin, md5($msPassword))){
    	$moUsers->setActive(true);
    	$_SESSION['user_name'] = $msLogin;
    	echo 'document.location.href = "default.php";';
    } else {
    	echo "js_show('warning');";
    }
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new LoginEvent("login_event"));
  }
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$moWebLib = FWDWebLib::getInstance();
		$msFocus = "gebi('user_login').focus();";
		$moSaasLib = SAASLib::getInstance();
		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
		<script language="javascript">        
        window.onresize = center_dialog_div;
        center_dialog_div();  
      </script>
      <?php 
	}
}
FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("login.xml");
?>
<!--[if lt IE 7]>
<script defer type='text/javascript' language='javascript'>
  correct_png();
</script>
<![endif]-->
