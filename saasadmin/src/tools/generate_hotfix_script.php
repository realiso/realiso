<?php
include_once "include.php";

$moHFManager = new SAASHotFixManager("e:/users/saas/isms_saas_source/", "source-pre", "e:/users/saas/isms_saas_source/", "new_source-pre");
$moHFManager->generate();
$maAddedDirectories = array_reverse($moHFManager->getAddedDirectories());
$maAddedFiles = $moHFManager->getAddedFiles();
$maRemovedFiles = $moHFManager->getRemovedFiles();
$maRemovedDirectories = $moHFManager->getRemovedDirectories();    
$maModifiedFiles = $moHFManager->getModifiedFiles();

/*
 * Variável que armazena o script do hotfix.
 */
$msScript = "";

/*
 * Novos diretórios.
 */
foreach ($maAddedDirectories as $msDir => $msIgnore) {
  $msScript .= SCRIPT_ACTION_CREATE_DIR . ":" . $msDir . PHP_EOL;
}

/*
 * Novos arquivos.
 */
foreach ($maAddedFiles as $msFile => $msIgnore) {
  $msScript .= SCRIPT_ACTION_CREATE_FILE . ":" . $msFile . PHP_EOL;
}

/*
 * Arquivos deletados.
 */
foreach ($maRemovedFiles as $msFile => $msIgnore) {
  $msScript .= SCRIPT_ACTION_REMOVE_FILE . ":" . $msFile . PHP_EOL;
}

/*
 * Diretórios deletados.
 */
foreach ($maRemovedDirectories as $msDir => $msIgnore) {
  $msScript .= SCRIPT_ACTION_REMOVE_DIR . ":" . $msDir . PHP_EOL;
}

/*
 * Arquivos modificados.
 */
foreach ($maModifiedFiles as $msFile => $msIgnore) {
  $msScript .= SCRIPT_ACTION_MODIFY_FILE . ":" . $msFile . PHP_EOL;
}


header('Cache-Control: ');
header('Pragma: ');
header('Content-type: application/octet-stream');
header('Content-Disposition: attachment; filename="hotfix_'.SAASLib::SAASTime().'.txt"');

echo $msScript;

?>