<?php
include_once "include.php";

if (count($argv) != 2) {
  echo "Wrong parameters! Usage: php create_instance_user_and_db.php [user]".PHP_EOL;
  exit;
}

$gsAlias = $argv[1];

/*
 * Carrega as configura��es do admin.
 */
$moConfig = new SAASConfig();
$moConfig->loadConfig();

/*
 * Conecta no banco principal.
 */ 
$moDB = new FWDDB(
  DB_POSTGRES,
  $moConfig->getConfig(CFG_DB),
  $moConfig->getConfig(CFG_DB_USER),
  $moConfig->getConfig(CFG_DB_USER_PASS),
  $moConfig->getConfig(CFG_DB_SERVER)
);
$moDB->connect();

/*
 * Cria o usu�rio que vai ser utilizado pela inst�ncia.
 */
$md5 = md5("pass_$gsAlias");
$msSQL = "CREATE ROLE inst_$gsAlias LOGIN ENCRYPTED PASSWORD '$md5' /*IN GROUP saasinstances*/
          NOINHERIT
          VALID UNTIL 'infinity';";
$moDB->execute($msSQL."ALTER ROLE inst_$gsAlias SUPERUSER;", 0, 0, true);

/*
 * Cria o banco de dados que vai ser utilizado pela inst�ncia.
 */
$msSQL = "CREATE DATABASE inst_$gsAlias
          WITH OWNER = inst_$gsAlias
          ENCODING = 'UTF8'
          TABLESPACE = pg_default;";
$moDB->execute($msSQL);

?>