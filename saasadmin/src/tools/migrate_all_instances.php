<?php
include_once "include.php";

set_time_limit(1500);

$goDB = FWDWebLib::getConnection();
$goDB->connect();
$goDB->execute("SELECT * FROM saas_instance");
$gaInst = $goDB->fetchAll();

foreach ($gaInst as $gaRow) {
  $gsInst =  $gaRow['sdatabasename'];
  
  // deleta o banco
  $gsCMD = "DROP DATABASE $gsInst;";
  system("psql -d postgres -c \"$gsCMD\"");
  
  // cria o banco de dados
  $gsCMD = "CREATE DATABASE $gsInst WITH OWNER = $gsInst ENCODING = 'UTF8' TABLESPACE = pg_default";
  system("psql -d postgres -c \"$gsCMD\"");

  // popula o banco
  system("psql -d $gsInst < $gsInst.dmp;");
}
?>