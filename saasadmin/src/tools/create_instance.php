<?php
set_time_limit(3600);
include_once "include.php";

if ($argc != 3) {
  die('Wrong parameters! Usage: create_instance.php [alias] [activation_code]');
}

$gsAlias = trim(strtolower($argv[1]));
$gsCode = trim($argv[2]);
$gsDatabaseName = "inst_".$gsAlias;
$gsDatabaseLogin = "inst_".$gsAlias;
$gsDatabaseLoginPass = md5('pass_'.$gsAlias);

/*
 * Valida a entrada.
 */
if (!SAASInstance::isValidAlias($gsAlias)) die("Alias inv�lido!");
if (SAASInstance::isAliasUsed($gsAlias)) die("Alias j� existente!");
if (!SAASACtivation::isValidActivationCode($gsCode, SAASActivation::SUBSCRIPTION_ISMS)) die("C�dido de ativa��o inv�lido!");

/*
 * Criar inst�ncia no banco do admin.
 */
$goInstance = new SAASInstance();    
$goInstance->setFieldValue('instance_alias', $gsAlias);
$goInstance->setFieldValue('instance_creation_date', SAASLib::SAASTime());    
$goInstance->setFieldValue('instance_db_name', $gsDatabaseName);
$goInstance->setFieldValue('instance_db_user', $gsDatabaseLogin);
$goInstance->setFieldValue('instance_db_user_pass', $gsDatabaseLoginPass);
$giId = $goInstance->insert(true);

/*
 * Marca o c�digo de subscription como usado.
 */
$goUpActivation = new SAASActivation();
$goUpActivation->setFieldValue("activation_date_used", SAASLib::SAASTime());
$goUpActivation->setFieldValue("activation_instance_id", $giId);
$goUpActivation->createFilter($gsCode, "activation_code");
$goUpActivation->update();

?>