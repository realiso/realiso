<?php
set_time_limit(3600);
include_once "include.php";

/*
 * Verifica entrada.
 */
if ($argc != 3) {
  die('Wrong parameters! Usage: add_user_pack.php [alias] [user_pack_code]');
}

/*
 * Dados de entrada.
 */
$gsAlias = trim(strtolower($argv[1]));
$gsCode = trim($argv[2]);

/*
 * Valida a entrada.
 */
if (!SAASInstance::isValidAlias($gsAlias)) die("Alias inv�lido!");
if (!SAASACtivation::isValidActivationCode($gsCode, SAASActivation::USER_PACK)) die("USER_PACK inv�lido!");

/*
 * Criar inst�ncia no banco do admin.
 */
$goInstance = new SAASInstance();
$goInstance->createFilter($gsAlias, 'instance_alias');
$goInstance->select();
if ($goInstance->fetch()) {
  $giId = $goInstance->getFieldValue('instance_id');
  
  $goUpActivation = new SAASActivation();
  $goUpActivation->setFieldValue("activation_date_used", SAASLib::SAASTime());
  $goUpActivation->setFieldValue("activation_instance_id", $giId);  
  $goUpActivation->createFilter($gsCode, "activation_code");
  $goUpActivation->update();
}
else {
  die('No instance with the specified alias: $gsAlias!');
}
?>