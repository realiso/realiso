<?php
set_time_limit(3600);
include_once "include.php";

if ($argc != 2) {
  die('Wrong parameters! Usage: create_config_file.php [alias]'); 
}

$gsAlias = trim(strtolower($argv[1]));
$gsDatabaseName = "inst_".$gsAlias;
$gsDatabaseLogin = "inst_".$gsAlias;
$gsDatabaseLoginPass = md5('pass_'.$gsAlias);

/*
 * Valida a entrada.
 */
if (!SAASInstance::isValidAlias($gsAlias)) die("Alias inv�lido!");
if (!SAASInstance::isAliasUsed($gsAlias)) die("Alias n�o existente!");
  
/*
 * Carrega configura��es do admin.
 */
$goConfig = new SAASConfig();
$goConfig->loadConfig();

/*
 * Cria config.php
 */
$gsDstFolder = FOLDER_SUBSCRIPTION."/" . $gsAlias;
$gsPath = $goConfig->getConfig(CFG_WEB_INSTANCE_PATH);
      
if (substr(PHP_OS, 0, 3) != "WIN") $gsFullPath = $gsPath.$gsDstFolder."/config.php";
else die("OS not supported!");

$gsConfig = '<?xml version="1.0" encoding="ISO-8859-1"?>
              <root>
                <database>
                  <type>DB_POSTGRES</type>
                  <host>%server%</host>
                  <port>5432</port>
                  <database>%database%</database>
                  <schema>%user%</schema>
                  <password>%password%</password>
                </database>
                <debug>
                  <type></type>
                </debug>
              </root>';
 
$goModule = mcrypt_module_open(MCRYPT_BLOWFISH,'',MCRYPT_MODE_ECB,'');
$gsKey = "b306737041cb40b1492e0debcfe45433";
mcrypt_generic_init($goModule, $gsKey, "12345678");    

$gsDatabaseLoginPassEncrypted = base64_encode(mcrypt_generic($goModule, $gsDatabaseLoginPass));

$gsConfig = str_replace(
  array('%database%','%user%','%password%','%server%'),
  array($gsDatabaseName, $gsDatabaseLogin, $gsDatabaseLoginPassEncrypted, $goConfig->getConfig(CFG_DB_SERVER)),
  $gsConfig
);
$grHandle = fopen($gsFullPath, 'w');
fwrite($grHandle, $gsConfig);
fclose($grHandle);

?>
