<?php
include_once "include.php";

if (count($argv) != 2) {
  echo "Wrong parameters! Usage: php drop_db.php [database]".PHP_EOL;
  exit;
}
$gsDB = $argv[1];

// deleta o banco
$gsCMD = "DROP DATABASE $gsDB;";
system("psql -d postgres -c \"$gsCMD\"");
?>