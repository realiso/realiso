<?php
include_once "include.php";

if (count($argv) != 3) {
  echo "Wrong parameters! Usage: php create_db.php [database] [user]".PHP_EOL;
  exit;
}

$gsDB = $argv[1];
$gsUser = $argv[2];

// cria o banco de dados
$gsCMD = "CREATE DATABASE $gsDB WITH OWNER = $gsUser ENCODING = 'UTF8' TABLESPACE = pg_default";
system("psql -d postgres -c \"$gsCMD\"");
?>