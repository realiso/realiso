<?php
include_once "include.php";

if (count($argv) != 4) {
  echo "Wrong parameters! Usage: php migrate_db.php [database] [login] [file]".PHP_EOL;
  exit;
}

$gsDB = $argv[1];
$gsLogin = $argv[2];
$gsFile = $argv[3];

// deleta o banco
$gsCMD = "DROP DATABASE $gsDB;";
system("psql -d postgres -c \"$gsCMD\"");

// recria o banco
$gsCMD = "CREATE DATABASE $gsDB WITH OWNER = $gsLogin ENCODING = 'UTF8' TABLESPACE = pg_default;";
system("psql -d postgres -c \"$gsCMD\"");

// popula o banco
system("psql -d $gsDB < $gsFile");
?>