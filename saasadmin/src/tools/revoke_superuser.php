<?php
include_once "include.php";

if (count($argv) != 2) {
  echo "Wrong parameters! Usage: php revoke_superuser.php [alias]".PHP_EOL;
  exit;
}

$gsAlias = $argv[1];

/*
 * Carrega as configurações do admin.
 */
$moConfig = new SAASConfig();
$moConfig->loadConfig();

/*
 * Conecta no banco principal.
 */ 
$moDB = new FWDDB(
  DB_POSTGRES,
  $moConfig->getConfig(CFG_DB),
  $moConfig->getConfig(CFG_DB_USER),
  $moConfig->getConfig(CFG_DB_USER_PASS),
  $moConfig->getConfig(CFG_DB_SERVER)
);
$moDB->connect();

$moDB->execute("ALTER ROLE inst_$gsAlias NOSUPERUSER;");

?>