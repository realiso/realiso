<?php
set_time_limit(3600);
include_once "include.php";

/*
 * Verifica entrada.
 */
if ($argc != 2) {
  die('Wrong parameters! Usage: drop_instance.php [alias]');
}

/*
 * Dados de entrada.
 */
$gsAlias = trim(strtolower($argv[1]));

/*
 * Valida a entrada.
 */
if (!SAASInstance::isValidAlias($gsAlias)) die("Alias inv�lido!");

/*
 * Deleta inst�ncia do banco do admin.
 */
$goInstance = new SAASInstance();
$goInstance->createFilter($gsAlias, 'instance_alias');
$goInstance->delete();
?>