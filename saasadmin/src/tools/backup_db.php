<?php
include_once "include.php";

set_time_limit(1500);

if (count($argv) != 3) {
  echo "Wrong parameters! Usage: php backup_db.php [database] [login]".PHP_EOL;
  exit;
}
$gsDB = $argv[1];
$gsLogin = $argv[2];

// faz o dump do banco de dados para um arquivo
system("pg_dump -E UTF8 $gsDB > $gsDB.dmp;");

// cria um banco de backup (just in case)
$gsCMD = "CREATE DATABASE bk_$gsDB WITH OWNER = $gsLogin ENCODING = 'UTF8' TABLESPACE = pg_default;";
system("psql -d postgres -c \"$gsCMD\"");
system("psql -d bk_$gsDB < $gsDB.dmp");
?>