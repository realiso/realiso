<?php
include_once "include.php";
include_once $handlers_grid_ref."QueryGridAgenda.php";

class UnblockTableEvent extends FWDRunnable {  
  public function run(){
    $moConfig = new SAASConfig();
    $moConfig->setConfig(CFG_AGENDA_LOCKED, '0');
    echo "gobi('table_locked').setValue('".FWDLanguage::getPHPStringValue('st_table_unlocked',"Tabela: Desbloqueada")."')";
  }
}

class EnableEvent extends FWDRunnable {  
  public function run(){
    $moConfig = new SAASConfig();
    $moConfig->setConfig(CFG_AGENDA_ENABLED, '1');
    echo "gobi('status').setValue('".FWDLanguage::getPHPStringValue('st_status_enabled',"Status: Habilitada")."')";
  }
}

class DisableEvent extends FWDRunnable {  
  public function run(){
    $moConfig = new SAASConfig();
    $moConfig->setConfig(CFG_AGENDA_ENABLED, '0');
    echo "gobi('status').setValue('".FWDLanguage::getPHPStringValue('st_status_disabled',"Status: Desabilitada")."')";
  }
}

class GridAgenda extends FWDDrawGrid {
  public function drawItem(){
    switch($this->ciColumnIndex){
      case $this->getIndexByAlias('agenda_type'):{
        $this->coCellBox->setValue(SAASLib::getLicenseTypeAsString($this->coCellBox->getValue()));
        return $this->coCellBox->draw();
      }
      case 6:
        $this->coCellBox->setValue(SAASLib::getSAASDate($this->coCellBox->getValue()));        
        return $this->coCellBox->draw();
      break;
      default:
        return parent::drawItem();
      break;
    }
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new EnableEvent('enable_event'));
    $moStartEvent->addAjaxEvent(new DisableEvent('disable_event'));
    $moStartEvent->addAjaxEvent(new UnblockTableEvent('unblock_table_event'));
    
    $moGrid = FWDWebLib::getObject("grid_agenda");
    $moHandler = new QueryGridAgenda(FWDWebLib::getConnection());    
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new GridAgenda());
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moConfig = new SAASConfig();
    $moConfig->loadCOnfig();
    
    if (!$moConfig->getConfig(CFG_AGENDA_ENABLED)) FWDWebLib::getObject('status')->setValue(FWDLanguage::getPHPStringValue('st_status_disabled',"Status: Desabilitada"));
    else FWDWebLib::getObject('status')->setValue(FWDLanguage::getPHPStringValue('st_status_enabled',"Status: Habilitada"));
    
    if ($moConfig->getConfig(CFG_AGENDA_LOCKED)) FWDWebLib::getObject('table_locked')->setValue(FWDLanguage::getPHPStringValue('st_table_locked',"Tabela: Bloqueada"));
    else FWDWebLib::getObject('table_locked')->setValue(FWDLanguage::getPHPStringValue('st_table_unlocked',"Tabela: Desbloqueada"));
    
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        function refresh_grid() {
          js_refresh_grid('grid_agenda');    
        }
      </script>
    <?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_agenda.xml");
?>