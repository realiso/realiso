<?php
include_once "include.php";

define('HF_LOG_INFO', 1);
define('HF_LOG_WARNING', 2);
define('HF_LOG_ERROR', 3);

define('HF_LOG_FILE', 'hf_log.txt');

/*
 * Fun��o para escrever no log.
 */
function writeLog($prHandle, $psMsg, $piType) {
  $msType = "";
  switch($piType) {    
    case HF_LOG_INFO:
      $msType = "INFO";
    break; 
    case HF_LOG_WARNING:
      $msType = "WARNING";
    break;
    case HF_LOG_ERROR:
      $msType = "ERROR";
    break;
  }
  fwrite($prHandle, "[".SAASLib::getSAASDate(SAASLib::SAASTime(), true)."][$msType] ".$psMsg.PHP_EOL); 
}

class UploadEvent extends FWDRunnable {
  
  private function showMsg($psMsg) {
    ?>
      <script language="javascript">        
        <?echo "parent.gobi('status').setValue('$psMsg');";?>
      </script>
    <?  
  }
      
  public function run(){
    set_time_limit(7200);       
    $moWebLib = FWDWebLib::getInstance();
    $moConfig = new SAASConfig();
    $moConfig->loadConfig();
    $msVersion = FWDWebLib::getObject('version')->getValue();
    
    /*
     * Upload do sistema compilado.
     */
    $moFile = $moWebLib->getObject('compiled');
    $miFileError = $moFile->getErrorCode();
    $msCompiledFilename = "";    
    if($moFile->getTempFileName() && $miFileError==FWDFile::E_NONE){
      $msCompiledFilename = $moFile->getTempFileName();      
      if (substr(PHP_OS, 0, 3) == "WIN") {
        $moZip = new ZipArchive;
        $mrRes = $moZip->open($msCompiledFilename);
        if ($mrRes === TRUE) {
            $moZip->extractTo($moConfig->getConfig(CFG_WEB_INSTANCE_SRC_PATH).FOLDER_HOTFIX_SYSTEM."-".$msVersion);
            $moZip->close();         
        } else {
          $this->showMsg(FWDLanguage::getPHPStringValue('st_could_not_extract_compiled_system',"Falha na extra��o do sistema compilado."));
          exit; 
        }
      } else {
         $msExtractDir = $moConfig->getConfig(CFG_WEB_INSTANCE_SRC_PATH).FOLDER_HOTFIX_SYSTEM."-".$msVersion;
         system("unzip $msCompiledFilename -d $msExtractDir");
      }
    } else {
      $this->showMsg(FWDLanguage::getPHPStringValue('st_could_not_upload_compiled_system',"Falha no upload do sistema compilado."));
      exit;  
    }
    
    /*
     * Upload dos sqls do sistema.
     */
    $moFile = $moWebLib->getObject('sql_system');
    $miFileError = $moFile->getErrorCode();
    $msSystemSQLFilename = "";    
    if($moFile->getTempFileName() && $miFileError==FWDFile::E_NONE){
      $msSystemSQLFilename = $moFile->getTempFileName();      
      if (substr(PHP_OS, 0, 3) == "WIN") {
        $moZip = new ZipArchive;
        $mrRes = $moZip->open($msSystemSQLFilename);
        if ($mrRes === TRUE) {
            $moZip->extractTo($moConfig->getConfig(CFG_WEB_INSTANCE_SRC_PATH).FOLDER_HOTFIX_SYSTEM."-".$msVersion);
            $moZip->close();         
        } else {
          $this->showMsg(FWDLanguage::getPHPStringValue('st_could_not_extract_system_sql',"Falha na extra��o dos sqls do sistema."));
          exit; 
        }
      } else {
         $msExtractDir = $moConfig->getConfig(CFG_WEB_INSTANCE_SRC_PATH).FOLDER_HOTFIX_SYSTEM."-".$msVersion;
         system("unzip $msSystemSQLFilename -d $msExtractDir");
      }
    } else {
      $this->showMsg(FWDLanguage::getPHPStringValue('st_could_not_upload_system_sql',"Falha no upload dos sqls do sistema."));
      exit;  
    }
         
    /*
     * Upload do script do hotfix do php.
     */
    $moFile = $moWebLib->getObject('php_hotfix');
    $msHFPHPFilename = "";
    if ($moFile->getFilename()) {
      $miFileError = $moFile->getErrorCode();    
      if($moFile->getTempFileName() && $miFileError==FWDFile::E_NONE){
        $msHFPHPFilename = $moFile->getFileName();
        if (!$moFile->copyTo($moConfig->getConfig(CFG_WEB_INSTANCE_SRC_PATH).FOLDER_HOTFIX_SYSTEM."-".$msVersion, $msHFPHPFilename)) {
          $this->showMsg(FWDLanguage::getPHPStringValue('st_could_not_copy_php_hotfix_file',"Falha na c�pia do arquivo txt do hotfix do php."));
          exit;
        }     
      } else {
        $this->showMsg(FWDLanguage::getPHPStringValue('st_could_not_upload_php_hotfix_file',"Falha no upload do arquivo txt do hotfix do php."));
        exit;
      }
    }
    
    /*
     * Upload do sql do hotfix.
     */
    $moFile = $moWebLib->getObject('sql_hotfix');
    $msHFSQLFilename = "";
    if ($moFile->getFilename()) {
      $miFileError = $moFile->getErrorCode();    
      if($moFile->getTempFileName() && $miFileError==FWDFile::E_NONE){
        $msHFSQLFilename = $moFile->getFileName();
        if (!$moFile->copyTo($moConfig->getConfig(CFG_WEB_INSTANCE_SRC_PATH).FOLDER_HOTFIX_SYSTEM."-".$msVersion, $msHFSQLFilename)) {
          $this->showMsg(FWDLanguage::getPHPStringValue('st_could_not_copy_sql_hotfix_file',"Falha na c�pia do arquivo sql do hotfix."));
          exit;
        }     
      } else {
        $this->showMsg(FWDLanguage::getPHPStringValue('st_could_not_upload_sql_hotfix_file',"Falha no upload do arquivo sql do hotfix."));
        exit;
      }
    }
    ?>
      <script language="javascript">
        parent.trigger_event('upload_complete_event', 3);       
      </script>
    <?
  }
}

class UploadCompleteEvent extends FWDRunnable {
  
  public function applyHotFix(SAASConfig $poConfig, $paPathArray, $paAddedDirectories, $paAddedFiles, $paRemovedFiles, $paRemovedDirectories, $paModifiedFiles) {
    FWDWebLib::setErrorHandler(new FWDDoNothingErrorHandler());
    $mbDebugMode = FWDWebLib::getInstance()->getDebugMode();
    $mbDebugType = FWDWebLib::getInstance()->getDebugType();
    FWDWebLib::getInstance()->setDebugMode(false,0);
    
    /*
     * Vari�vel para controle de erro. 
     */
    $mbOk = true;
    
    /*
     * Monta a vers�o sem a build.
     */    
    $msVersion = FWDWebLib::getObject('version')->getValue();    
    $maVersion = explode('.', $msVersion);    
    $msVersionWithoutBuild = $maVersion[0].'.'.$maVersion[1].'.'.$maVersion[2];
    
    /*
     * Monta os paths.
     */
    $msSrcPath = $poConfig->getConfig(CFG_WEB_INSTANCE_SRC_PATH);
    $msHFPath = $msSrcPath.FOLDER_HOTFIX_SYSTEM.'-'.$msVersion.'/';
    $msHFFolderComp = $msHFPath.'isms'.$msVersionWithoutBuild.'/';
    $msHFNEWSQLs = $msHFPath.'isms'.$msVersionWithoutBuild.'-sqls/';
    $msSQLFile = $msHFPath.'isms'.$msVersionWithoutBuild.'-hf.sql';
    $msSystemSQLs = $msHFPath.'isms'.$msVersionWithoutBuild.'-sqls/';
    $msHFSQLFileContents = (file_exists($msSQLFile) ? file_get_contents($msSQLFile) : "");
            
    /*
     * Cria um arquivo de log.
     */
    $mrHandle = fopen($msHFPath.HF_LOG_FILE, 'a');
    
    /*
     * Faz as modifica��es nos phps de cada
     * inst�ncia e tamb�m do sistema fonte. 
     */
    foreach ($paPathArray as $msPath) {      
      $msCopySrc = $msHFFolderComp;
      $msCopyDst = $msPath.'/';
      
      writeLog($mrHandle, "------------------------------------- Updating PHPs of $msCopyDst", HF_LOG_INFO);
      
      /*
       * Adiciona os novos diret�rios.
       */
      foreach ($paAddedDirectories as $msDir) {
        if (!mkdir($msCopyDst.$msDir, 0750, true)) {
          writeLog($mrHandle, "mkdir failed: ".$msCopyDst.$msDir, HF_LOG_ERROR);
          $mbOk = false;
        }
        else {
          writeLog($mrHandle, "mkdir ok: ".$msCopyDst.$msDir, HF_LOG_INFO);
        }
      }
      
      /*
       * Adiciona os novos arquivos.
       */
      foreach ($paAddedFiles as $msFile) {
        if (!copy($msCopySrc.$msFile, $msCopyDst.$msFile)) {
          writeLog($mrHandle, "copy failed: ".$msCopySrc.$msFile." -> ".$msCopyDst.$msFile, HF_LOG_ERROR);
          $mbOk = false;
        }
        else {
          writeLog($mrHandle, "copy ok: ".$msCopySrc.$msFile." -> ".$msCopyDst.$msFile, HF_LOG_INFO);
        }
      }
      
      /*
       * Remove os arquivos deletados.
       */
      foreach ($paRemovedFiles as $msFile) {
        if (!unlink($msCopyDst.$msFile)) {
          writeLog($mrHandle, "unlink failed: ".$msCopyDst.$msFile, HF_LOG_ERROR);
          $mbOk = false;
        }
        else {
          writeLog($mrHandle, "unlink ok: ".$msCopyDst.$msFile, HF_LOG_INFO);
        }
      }
      
      /*
       * Remove os diret�rios deletados.
       */
      foreach ($paRemovedDirectories as $msDir) {
        if(!rmdir($msCopyDst.$msDir)) {
          writeLog($mrHandle, "rmdir failed: ".$msCopyDst.$msDir, HF_LOG_ERROR);
          $mbOk = false;
        }
        else {
          writeLog($mrHandle, "rmdir ok: ".$msCopyDst.$msDir, HF_LOG_INFO);
        }
      }      
      
      /*
       * Substitui os arquivos modificados.
       */
      foreach ($paModifiedFiles as $msFile) {
        if(!copy($msCopySrc.$msFile, $msCopyDst.$msFile)) {
          writeLog($mrHandle, "copy (modify) failed: ".$msCopySrc.$msFile." -> ".$msCopyDst.$msFile, HF_LOG_ERROR);
          $mbOk = false;
        }
        else {
          writeLog($mrHandle, "copy (modify) ok: ".$msCopySrc.$msFile." -> ".$msCopyDst.$msFile, HF_LOG_INFO);
        }
      }
            
      writeLog($mrHandle, "------------------------------------- PHPs updated", HF_LOG_INFO);
    }
    
    /*
     * Aplica o hotfix de sql em todas as inst�ncias (se existir).
     */
    if ($msHFSQLFileContents) {
      $moInstance = new SAASInstance();
      $moInstance->createFilter(0, 'instance_id', '<>');
      $moInstance->select();
      while ($moInstance->fetch()) {      
        $msInstanceName = $moInstance->getFieldValue('instance_alias');
        $msDBName = $moInstance->getFieldValue('instance_db_name');
        $msDBUser = $moInstance->getFieldValue('instance_db_user');
        $msDBPass = $moInstance->getFieldValue('instance_db_user_pass');
        
        writeLog($mrHandle, "------------------------------------- Updating database of $msInstanceName ($msDBName)", HF_LOG_INFO);
        
        $moDB = new FWDDB(
          DB_POSTGRES,
          $msDBName,
          $msDBUser,
          $msDBPass,
          $poConfig->getConfig(CFG_DB_SERVER)
        );
        
        if ($moDB->connect()) {
          writeLog($mrHandle, "connection to database ok", HF_LOG_INFO);
          $moDB->startTransaction();
          if ($moDB->execute($msHFSQLFileContents, 0, 0, true)) writeLog($mrHandle, "database hotfix ok", HF_LOG_INFO);
          else {
            $msError = $moDB->getErrorMessage();
            writeLog($mrHandle, "database hotfix failed -> " . $msError, HF_LOG_ERROR);
            $mbOk = false;
          }
          $moDB->completeTransaction();
        }
        else {        
          writeLog($mrHandle, "connection to database failed", HF_LOG_ERROR);
          $mbOk = false;
        }      
        writeLog($mrHandle, "------------------------------------- Database updated", HF_LOG_INFO);      
      }
    }
    
    /*
     * Atualiza os sqls para as novas inst�ncias que ser�o criadas.
     */
    writeLog($mrHandle, "------------------------------------- Updating system SQLs", HF_LOG_INFO);
    
    /*
     * Deleta os sqls atuais.
     */
    $msSrcSQLs = $msSrcPath."sqls/";        
    $maFiles = scandir($msSrcSQLs);    
    foreach ($maFiles as $msFile) {
      $msDeleteFile = $msSrcSQLs.$msFile;
    	if ($msFile != '.' && $msFile != '..') {
        if (!unlink($msDeleteFile)) {
          writeLog($mrHandle, "delete sql file failed: $msDeleteFile", HF_LOG_ERROR);
          $mbOk = false;
        }
      }
    }
    
    /*
     * Copia os novos arquivos sql do sistema.
     */
    $maFiles = scandir($msHFNEWSQLs);
    foreach ($maFiles as $msFile) {
      $msCopyFile = $msHFNEWSQLs.$msFile;
      if ($msFile != '.' && $msFile != '..') {        
        if (!copy($msCopyFile, $msSrcSQLs.$msFile)){
          writeLog($mrHandle, "copy sql file failed: $msCopyFile -> $msSrcSQLs/$msFile", HF_LOG_ERROR);
          $mbOk = false;
        }
      }      
    }
    
    writeLog($mrHandle, "------------------------------------- System SQLs updated", HF_LOG_INFO);
    
    /*
     * Atualiza vers�o das inst�ncias. 
     */
    $moConfig = new SAASConfig();
    $moConfig->setConfig(CFG_INSTANCE_VERSION, $msVersion);
    
    if ($mbOk)
      echo "gobi('status').setValue('".FWDLanguage::getPHPStringValue('st_hot_fix_successfully_done', "HotFix realizado com sucesso.")."');";
    else
      echo "gobi('status').setValue('".FWDLanguage::getPHPStringValue('st_hot_fix_successfully_done', "Ocorreram problemas durante o HotFix, consulte o log para obter detalhes.")."');";
    
    FWDWebLib::getInstance()->setDebugMode($mbDebugMode,$mbDebugType);
    FWDWebLib::restoreLastErrorHandler();
  }
  
  public function run(){
    set_time_limit(7200);
    
    /*
     * Carrega as configura��es.
     */
    $moConfig = new SAASConfig();
    $moConfig->loadConfig();
    
    /*
     * Armazena a vers�o e a vers�o sem a build.
     */
    $msVersion = FWDWebLib::getObject('version')->getValue();
    $maVersion = explode('.', $msVersion);    
    $msVersionWithoutBuild = $maVersion[0].'.'.$maVersion[1].'.'.$maVersion[2];
    
    /*
     * Monta os paths que ser�o usados.
     */
    $msSrcPath = $moConfig->getConfig(CFG_WEB_INSTANCE_SRC_PATH);
    $msSrcFolderComp = FOLDER_SOURCE_SYSTEM;
        
    $maVersion = explode('.', $msVersion);    
    $msVersionWithoutBuild = $maVersion[0].'.'.$maVersion[1].'.'.$maVersion[2];
    
    $msHFPath = $msSrcPath.FOLDER_HOTFIX_SYSTEM.'-'.$msVersion.'/';
    $msHFFolderComp = 'isms'.$msVersionWithoutBuild;
    $msHFFolderPre = 'isms'.$msVersionWithoutBuild.'-pre';
    $msHFPHPPath = $msHFPath.'isms'.$msVersionWithoutBuild.'-hf.txt';
        
    /*
     * Monta os arrays baseado nas a��es especificadas no script do hotfix do php.
     */
    $maAddedDirectories = array();
    $maAddedFiles = array();
    $maRemovedFiles = array();
    $maRemovedDirectories = array();    
    $maModifiedFiles = array();
    $maAction = file($msHFPHPPath);
    foreach ($maAction as $msAction) {
      $msAction = trim($msAction);
      if ($msAction) {
        $maActionFile = explode(':', $msAction);
        switch ($maActionFile[0]) {
          case SCRIPT_ACTION_CREATE_DIR:
            $maAddedDirectories[] = $maActionFile[1];
          break;
          case SCRIPT_ACTION_CREATE_FILE:
            $maAddedFiles[] = $maActionFile[1];
          break;
          case SCRIPT_ACTION_REMOVE_DIR:
            $maRemovedDirectories[] = $maActionFile[1];
          break;
          case SCRIPT_ACTION_REMOVE_FILE:
            $maRemovedFiles[] = $maActionFile[1];
          break;
          case SCRIPT_ACTION_MODIFY_FILE:
            $maModifiedFiles[] = $maActionFile[1];
          break;
        }    
      }  
    }
        
    /*
     * Monta um array com o path do sistema fonte e de todas
     * as int�ncias. Esse array vai ser usado pela rotina que
     * ir� fazer o hotfix dos phps.
     */
    $maPathArray = array();    
    $maPathArray[] = $msSrcPath.$msSrcFolderComp;      // path do sistema fonte compilado
    $moInstance = new SAASInstance();
    $moInstance->createFilter(0, 'instance_id', '<>');
    $moInstance->select();
    while ($moInstance->fetch())      
      $maPathArray[] = $moConfig->getConfig(CFG_WEB_INSTANCE_PATH).$moInstance->getFieldValue('instance_alias');    // path de cada inst�ncia do sistema
            
    /*
     * Aplica o hotfix.
     */    
    $this->applyHotFix($moConfig, $maPathArray, $maAddedDirectories, $maAddedFiles, $maRemovedFiles, $maRemovedDirectories, $maModifiedFiles);
  }
}

class ApplyEvent extends FWDRunnable {  
  public function run(){
    $moConfig = new SAASConfig();
    $moConfig->loadConfig();
    $msVersion = FWDWebLib::getObject('version')->getValue();
    
    /*
     * Fazer verifica��o do formato da vers�o.
     */
    if (!preg_match('/^\d+\.\d+\.\d+\.\d+$/', $msVersion)) {
      echo "gobi('status').setValue('".FWDLanguage::getPHPStringValue('st_invalid_version_format', "A vers�o est� com o formato inv�lido. Exemplo de um formato correto: 2.2.0.1987")."');";
      exit;
    }
    
    /*
     * Verificar se a vers�o j� n�o existe. 
     */
    if (file_exists($moConfig->getConfig(CFG_WEB_INSTANCE_SRC_PATH).FOLDER_HOTFIX_SYSTEM."-".$msVersion)) {
      echo "gobi('status').setValue('".FWDLanguage::getPHPStringValue('st_version_already_exists', "O HotFix para essa vers�o j� foi aplicado.")."');";
      exit;
    }
        
    echo "js_submit('upload_event','ajax');";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
    $moStartEvent->addAjaxEvent(new ApplyEvent('apply_event'));
    $moStartEvent->addSubmitEvent(new UploadEvent('upload_event'));
    $moStartEvent->addAjaxEvent(new UploadCompleteEvent('upload_complete_event'));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_instance_hotfix.xml');
?>