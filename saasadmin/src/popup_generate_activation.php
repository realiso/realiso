<?php
include_once "include.php";

class SaveEvent extends FWDRunnable {
  public function run(){
    if (PROVIDER){
    	$miMinimariskSubsCount = FWDWebLib::getObject('minimarisk_subscription_count')->getValue();
    	$miMinimariskTrialCount = FWDWebLib::getObject('minimarisk_trial_count')->getValue();
    } else {
    	$miISMSSubsCount = FWDWebLib::getObject('isms_subscription_count')->getValue();
	$miBCMSSubsCount = FWDWebLib::getObject('bcms_subscription_count')->getValue();
	$miEMSSubsCount = FWDWebLib::getObject('ems_subscription_count')->getValue();
    	$miOHSSubsCount = FWDWebLib::getObject('ohs_subscription_count')->getValue();
    	$miSOXSubsCount = FWDWebLib::getObject('sox_subscription_count')->getValue();
    	$miPCISubsCount = FWDWebLib::getObject('pci_subscription_count')->getValue();
    	$miISMSTrialCount = FWDWebLib::getObject('isms_trial_count')->getValue();
	$miBCMSTrialCount = FWDWebLib::getObject('bcms_trial_count')->getValue();
	$miEMSTrialCount = FWDWebLib::getObject('ems_trial_count')->getValue();
    	$miOHSTrialCount = FWDWebLib::getObject('ohs_trial_count')->getValue();
    	$miSOXTrialCount = FWDWebLib::getObject('sox_trial_count')->getValue();
    	$miPCITrialCount = FWDWebLib::getObject('pci_trial_count')->getValue();
    }
  	$miUserPackCount = FWDWebLib::getObject('user_pack_count')->getValue();
    
    /*
     * Gera os c�digos de ativa��o do tipo SUBSCRIPTION_ISMS.
     */
    if (isset($miISMSSubsCount)){
    	$maISMSSubs = array();
    	$maISMSSubs[] = "===ISMS SUBSCRIPTION ACTIVATION CODES===";
    	for($miI = 0; $miI < $miISMSSubsCount; $miI++) $maISMSSubs[] = SAASActivation::insertActivation(SAASActivation::SUBSCRIPTION_ISMS);
    	$maISMSSubs[] = "========================================";
    }
    
    /*
     * Gera os c�digos de ativa��o do tipo TRIAL_ISMS.
     */
    if (isset($miISMSTrialCount)){
    	$maISMSTRIALs = array();
    	$maISMSTRIALs[] = "======ISMS TRIAL ACTIVATION CODES=======";
    	for($miI = 0; $miI < $miISMSTrialCount; $miI++) $maISMSTRIALs[] = SAASActivation::insertActivation(SAASActivation::TRIAL_ISMS);
    	$maISMSTRIALs[] = "========================================";
    }

    if (isset($miBCMSSubsCount)){
	    $maBCMSSubs = array();
	    $maBCMSSubs[] = "===BCMS SUBSCRIPTION ACTIVATION CODES===";
	    for($miI = 0; $miI < $miBCMSSubsCount; $miI++) $maBCMSSubs[] = SAASActivation::insertActivation(SAASActivation::SUBSCRIPTION_BCMS);
	    $maBCMSSubs[] = "========================================";
    }

    /*
     * Gera os c�digos de ativa��o do tipo TRIAL_ISMS.
     */
    if (isset($miBCMSTrialCount)){
	    $maBCMSTRIALs = array();
	    $maBCMSTRIALs[] = "======BCMS TRIAL ACTIVATION CODES=======";
	    for($miI = 0; $miI < $miBCMSTrialCount; $miI++) $maBCMSTRIALs[] = SAASActivation::insertActivation(SAASActivation::TRIAL_BCMS);
	    $maBCMSTRIALs[] = "========================================";
    }
    /*
     * Gera os c�digos de ativa��o do tipo SUBSCRIPTION_EMS.
     */
    if (isset($miEMSSubsCount)){
    	$maEMSSubs = array();
    	$maEMSSubs[] = "===EMS SUBSCRIPTION ACTIVATION CODES====";
    	for($miI = 0; $miI < $miEMSSubsCount; $miI++) $maEMSSubs[] = SAASActivation::insertActivation(SAASActivation::SUBSCRIPTION_EMS);
    	$maEMSSubs[] = "========================================";
    }

    /*
     * Gera os c�digos de ativa��o do tipo TRIAL_EMS.
     */
    if (isset($miEMSTrialCount)){
    	$maEMSTRIALs = array();
    	$maEMSTRIALs[] = "======EMS TRIAL ACTIVATION CODES========";
    	for($miI = 0; $miI < $miEMSTrialCount; $miI++) $maEMSTRIALs[] = SAASActivation::insertActivation(SAASActivation::TRIAL_EMS);
    	$maEMSTRIALs[] = "========================================";
    }

    /*
     * Gera os c�digos de ativa��o do tipo SUBSCRIPTION_OHS.
     */
    if (isset($miOHSSubsCount)){
    	$maOHSSubs = array();
    	$maOHSSubs[] = "===OHS SUBSCRIPTION ACTIVATION CODES====";
    	for($miI = 0; $miI < $miOHSSubsCount; $miI++) $maOHSSubs[] = SAASActivation::insertActivation(SAASActivation::SUBSCRIPTION_OHS);
    	$maOHSSubs[] = "========================================";
    }

    /*
     * Gera os c�digos de ativa��o do tipo TRIAL_OHS.
     */
    if (isset($miOHSTrialCount)){
    	$maOHSTRIALs = array();
    	$maOHSTRIALs[] = "======OHS TRIAL ACTIVATION CODES========";
    	for($miI = 0; $miI < $miOHSTrialCount; $miI++) $maOHSTRIALs[] = SAASActivation::insertActivation(SAASActivation::TRIAL_OHS);
    	$maOHSTRIALs[] = "========================================";
    }
    /*
     * Gera os c�digos de ativa��o do tipo SUBSCRIPTION_SOX.
     */
    if (isset($miSOXSubsCount)){
    	$maSOXSubs = array();
    	$maSOXSubs[] = "===SOX SUBSCRIPTION ACTIVATION CODES====";
    	for($miI = 0; $miI < $miSOXSubsCount; $miI++) $maSOXSubs[] = SAASActivation::insertActivation(SAASActivation::SUBSCRIPTION_SOX);
    	$maSOXSubs[] = "========================================";
    }

    /*
     * Gera os c�digos de ativa��o do tipo TRIAL_SOX.
     */
    if (isset($miSOXTrialCount)){
    	$maSOXTRIALs = array();
    	$maSOXTRIALs[] = "======SOX TRIAL ACTIVATION CODES========";
    	for($miI = 0; $miI < $miSOXTrialCount; $miI++) $maSOXTRIALs[] = SAASActivation::insertActivation(SAASActivation::TRIAL_SOX);
    	$maSOXTRIALs[] = "========================================";
    }

    /*
     * Gera os c�digos de ativa��o do tipo SUBSCRIPTION_PCI.
     */
    if (isset($miPCISubsCount)){
    	$maPCISubs = array();
    	$maPCISubs[] = "===PCI SUBSCRIPTION ACTIVATION CODES====";
    	for($miI = 0; $miI < $miPCISubsCount; $miI++) $maPCISubs[] = SAASActivation::insertActivation(SAASActivation::SUBSCRIPTION_PCI);
    	$maPCISubs[] = "========================================";
    }

    /*
     * Gera os c�digos de ativa��o do tipo TRIAL_PCI.
     */
    if (isset($miPCITrialCount)){
    	$maPCITRIALs = array();
    	$maPCITRIALs[] = "======PCI TRIAL ACTIVATION CODES========";
    	for($miI = 0; $miI < $miPCITrialCount; $miI++) $maPCITRIALs[] = SAASActivation::insertActivation(SAASActivation::TRIAL_PCI);
    	$maPCITRIALs[] = "========================================";
    }
    /*
     * Gera os c�digos de ativa��o do tipo USER_PACK.
     */
    if (isset($miUserPackCount)){
    	$maUPs = array();
    	$maUPs[] = "=======USER_PACK ACTIVATION CODES=======";
    	for($miI = 0; $miI < $miUserPackCount; $miI++) $maUPs[] = SAASActivation::insertActivation(SAASActivation::USER_PACK);
    	$maUPs[] = "========================================";
    }
    /*
     * Gera os c�digos de ativa��o do tipo SUBSCRIPTION_MINIMARISK.
     */
    if (isset($miMinimariskSubsCount)){
    	$maMinimariskSubs = array();
    	$maMinimariskSubs[] = "===Minimarisk SUBSCRIPTION ACTIVATION CODES====";
    	for($miI = 0; $miI < $miMinimariskSubsCount; $miI++) $maMinimariskSubs[] = SAASActivation::insertActivation(SAASActivation::SUBSCRIPTION_MINIMARISK);
    	$maMinimariskSubs[] = "===============================================";
    }

    /*
     * Gera os c�digos de ativa��o do tipo TRIAL_MINIMARISK.
     */
    if (isset($miMinimariskTrialCount)){
    	$maMinimariskTRIALs = array();
    	$maMinimariskTRIALs[] = "======Minimarisk TRIAL ACTIVATION CODES========";
    	for($miI = 0; $miI < $miMinimariskTrialCount; $miI++) $maMinimariskTRIALs[] = SAASActivation::insertActivation(SAASActivation::TRIAL_MINIMARISK);
    	$maMinimariskTRIALs[] = "===============================================";
    }

    /*
     * Gera o arquivo para download.
     */
    $msFilename = "activation_codes".date("YmdHis");
    header('Cache-Control: ');
    header('Pragma: ');
    header('Content-type: application/octet-stream');
    header('Content-Disposition: attachment; filename="'.$msFilename.'.txt"');
    $msRet = "";
    if (isset($maISMSSubs)) $msRet .=  implode(PHP_EOL, $maISMSSubs) . PHP_EOL . PHP_EOL;
    if (isset($maISMSTRIALs)) $msRet .= implode(PHP_EOL, $maISMSTRIALs) . PHP_EOL . PHP_EOL ;
    if (isset($maBCMSSubs)) $msRet .=  implode(PHP_EOL, $maBCMSSubs) . PHP_EOL . PHP_EOL;
    if (isset($maBCMSTRIALs)) $msRet .= implode(PHP_EOL, $maBCMSTRIALs) . PHP_EOL . PHP_EOL ;
    if (isset($maEMSSubs)) $msRet .= implode(PHP_EOL, $maEMSSubs) . PHP_EOL . PHP_EOL ;
    if (isset($maEMSSubs)) $msRet .= implode(PHP_EOL, $maEMSTRIALs) . PHP_EOL . PHP_EOL ;
    if (isset($maOHSSubs)) $msRet .= implode(PHP_EOL, $maOHSSubs) . PHP_EOL . PHP_EOL ;
    if (isset($maOHSTRIALs)) $msRet .= implode(PHP_EOL, $maOHSTRIALs) . PHP_EOL . PHP_EOL ;
    if (isset($maSOXSubs)) $msRet .= implode(PHP_EOL, $maSOXSubs) . PHP_EOL . PHP_EOL ;
    if (isset($maSOXTRIALs)) $msRet .= implode(PHP_EOL, $maSOXTRIALs) . PHP_EOL . PHP_EOL ;
    if (isset($maPCISubs)) $msRet .= implode(PHP_EOL, $maPCISubs) . PHP_EOL . PHP_EOL ;
    if (isset($maPCITRIALs)) $msRet .= implode(PHP_EOL, $maPCITRIALs) . PHP_EOL . PHP_EOL ;
    if (isset($maUPs)) $msRet .= implode(PHP_EOL, $maUPs) . PHP_EOL . PHP_EOL ;
    if (isset($maMinimariskSubs)) $msRet .= implode(PHP_EOL, $maMinimariskSubs) . PHP_EOL . PHP_EOL ;
    if (isset($maMinimariskTRIALs)) $msRet .= implode(PHP_EOL, $maMinimariskTRIALs);
    echo $msRet;
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
    $moStartEvent->addSubmitEvent(new SaveEvent('save_event'));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
if (PROVIDER){
	FWDWebLib::getInstance()->xml_load('popup_generate_activation_minimarisk.xml');
} else {
	FWDWebLib::getInstance()->xml_load('popup_generate_activation.xml');
}
?>
