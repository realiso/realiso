<?php
include_once "include.php";

define("VALIDATION_OK", 1);
define("LICENSE_INVALID", 7);
define("ALIAS_ALREADY_IN_USE", 8);

function createInstanceValdation($msAlias) {
	if (SAASInstance::isAliasUsed($msAlias))
		return ALIAS_ALREADY_IN_USE;
    else
    	return VALIDATION_OK;
}

$msAlias = $_GET['alias'];

echo createInstanceValdation($msAlias);
?>
