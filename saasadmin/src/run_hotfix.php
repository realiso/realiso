<?php
include_once "include.php";

$msSQLFile = "update.sql";
$msVersion = "1.0.0.0000";

define('HF_LOG_INFO', 1);
define('HF_LOG_WARNING', 2);
define('HF_LOG_ERROR', 3);

define('HF_LOG_FILE', 'hf_log.txt');

/*
 * Fun��o para escrever no log.
 */
function writeLog($prHandle, $psMsg, $piType) {
  $msType = "";
  switch($piType) {    
    case HF_LOG_INFO:
      $msType = "INFO";
    break; 
    case HF_LOG_WARNING:
      $msType = "WARNING";
    break;
    case HF_LOG_ERROR:
      $msType = "ERROR";
    break;
  }
  fwrite($prHandle, "[".SAASLib::getSAASDate(SAASLib::SAASTime(), true)."][$msType] ".$psMsg.PHP_EOL); 
}

/*
 * Cria um arquivo de log.
 */
$mrHandle = fopen(HF_LOG_FILE, 'a');

/*
 * L� arquivo com sql do hotfix.
 */
$msHFSQLFileContents = (file_exists($msSQLFile) ? file_get_contents($msSQLFile) : "");

/*
 * Carrega as configura��es. 
 */
$moConfig = new SAASConfig();
$moConfig->loadConfig();

FWDWebLib::setErrorHandler(new FWDDoNothingErrorHandler());
$mbDebugMode = FWDWebLib::getInstance()->getDebugMode();
$mbDebugType = FWDWebLib::getInstance()->getDebugType();
FWDWebLib::getInstance()->setDebugMode(false,0);

/*
 * Aplica o hotfix de sql em todas as inst�ncias.
 */
$mbOk = true;
if ($msHFSQLFileContents) {  
  $moInstance = new SAASInstance();
  $moInstance->createFilter(0, 'instance_id', '<>');
  $moInstance->select();
  while ($moInstance->fetch()) {      
    $msInstanceName = $moInstance->getFieldValue('instance_alias');
    $msDBName = $moInstance->getFieldValue('instance_db_name');
    $msDBUser = $moInstance->getFieldValue('instance_db_user');
    $msDBPass = $moInstance->getFieldValue('instance_db_user_pass');
    
    writeLog($mrHandle, "------------------------------------- Updating database of $msInstanceName ($msDBName)", HF_LOG_INFO);
    
    $moDB = new FWDDB(
      DB_POSTGRES,
      $msDBName,
      $msDBUser,
      $msDBPass,
      $moConfig->getConfig(CFG_DB_SERVER)
    );
    
    if ($moDB->connect()) {
      writeLog($mrHandle, "connection to database ok", HF_LOG_INFO);
      $moDB->startTransaction();
      if ($moDB->execute($msHFSQLFileContents, 0, 0, true)) writeLog($mrHandle, "database hotfix ok", HF_LOG_INFO);
      else {
        $msError = $moDB->getErrorMessage();
        writeLog($mrHandle, "database hotfix failed -> " . $msError, HF_LOG_ERROR);
        $mbOk = false;
      }
      $moDB->completeTransaction();
    }
    else {        
      writeLog($mrHandle, "connection to database failed", HF_LOG_ERROR);
      $mbOk = false;
    }      
    writeLog($mrHandle, "------------------------------------- Database updated", HF_LOG_INFO);      
  }
}

FWDWebLib::getInstance()->setDebugMode($mbDebugMode,$mbDebugType);
FWDWebLib::restoreLastErrorHandler();

/*
 * Atualiza vers�o das inst�ncias. 
 */
$moConfig = new SAASConfig();
$moConfig->setConfig(CFG_INSTANCE_VERSION, $msVersion);

if ($mbOk) echo "HotFix successfully done.";
else echo "Problems detected during the hoxfix process. Please, see the log file for details.";
?>