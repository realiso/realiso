<?php
include_once "include.php";
include_once $handlers_ref.'QueryLicenseInfo.php';

define('CHANGE_OK', 1);

function changeInstance($msAlias, $pbIsActive, $maxUsers, $miLicenseType){
	$moConfig = new SAASConfig();
    $moConfig->loadConfig();
    
      
    $msClientName = $msAlias; 
    $miMaxUsers = $maxUsers;
	$miMaxSimultUsers = $maxUsers;
    
	$tmpTimezone = date_default_timezone_get();
	date_default_timezone_set('America/New_York');
    $miExpirationDate = null;
	date_default_timezone_set($tmpTimezone);
            
    $msModules = "RM|PM|CI";
    $miType = SAASLib::convertAdminLicenseType2SystemLicenseType($miLicenseType);
        
    $msDatabaseLogin = 'inst_'.$msAlias;
	$msDatabaseLoginPass = md5('pass_'.$msAlias);
	$msDatabaseName = 'inst_'.$msAlias;
	$piIsActive = (int)($pbIsActive);
    $moDB = new FWDDB(
      DB_POSTGRES,
      $msDatabaseName,
      $msDatabaseLogin,
      $msDatabaseLoginPass,
      $moConfig->getConfig(CFG_DB_SERVER)
    );    
    $moDB->connect();    
    
    $moHandler = new QueryLicenseInfo($moDB);
    $moHandler->makeQuery();
    $maRes = $moHandler->executeQuery();
    $miActivationDate = $maRes[SAASInstance::LICENSE_ACTIVATION_DATE];
    
    $msMD5 = md5($msClientName.$msModules.$miMaxUsers.$miActivationDate.$miExpirationDate.$miMaxSimultUsers.$miType.'LICENSEHASH');
    $msSQL = "UPDATE isms_saas SET sValue = '$msClientName' WHERE pkConfig = ".SAASInstance::LICENSE_CLIENT_NAME.";
              UPDATE isms_saas SET sValue = '$msModules' WHERE pkConfig = ".SAASInstance::LICENSE_MODULES.";
              UPDATE isms_saas SET sValue = '$miMaxUsers' WHERE pkConfig = ".SAASInstance::LICENSE_MAX_USERS.";
              UPDATE isms_saas SET sValue = '$miExpirationDate' WHERE pkConfig = ".SAASInstance::LICENSE_EXPIRACY.";
              UPDATE isms_saas SET sValue = '$miMaxSimultUsers' WHERE pkConfig = ".SAASInstance::LICENSE_MAX_SIMULTANEOUS_USERS.";
              UPDATE isms_saas SET sValue = '$miType' WHERE pkConfig = ".SAASInstance::LICENSE_TYPE.";
              UPDATE isms_saas SET sValue = '$msMD5' WHERE pkConfig = ".SAASInstance::LICENSE_HASH.";
              UPDATE isms_saas SET sValue = '$piIsActive' WHERE pkConfig = ".SAASInstance::SYSTEM_IS_ACTIVE.";";
    $moDB->execute($msSQL, 0, 0, true); 
  	return CHANGE_OK;
}

$msAlias = $_GET['alias'];
$pbIsActive = $_GET['active'];
$maxUsers = $_GET['users'];
$miLicenseType = $_GET['license'];

echo changeInstance($msAlias, $pbIsActive, $maxUsers, $miLicenseType);

?>