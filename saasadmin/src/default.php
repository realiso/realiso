<?php
include_once "include.php";

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));  
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moConfig = new SAASConfig();
    $moConfig->loadConfig();
    $msVersion = $moConfig->getConfig(CFG_INSTANCE_VERSION) ? $moConfig->getConfig(CFG_INSTANCE_VERSION) : "-/-";
    FWDWebLib::getObject('version')->concatValue($msVersion);
    FWDWebLib::getObject('admin_build')->concatValue(SAASLIB::getBuild());
    FWDWebLib::getObject('user')->concatValue($_SESSION['user_name']);
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">        
        window.onresize = center_dialog_div;
        center_dialog_div();  
      </script>
    <?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
if (GENERATE_KEYS)
	FWDWebLib::getInstance()->xml_load("default.xml");
else 
	FWDWebLib::getInstance()->xml_load("default_without_keys.xml");
?>