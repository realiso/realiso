<?php
include_once "include.php";
set_time_limit(7200);

define('ABANDONMENT_LOG_INFO', 1);
define('ABANDONMENT_LOG_WARNING', 2);
define('ABANDONMENT_LOG_ERROR', 3);

/*
 * Função para escrever no log.
 */
function writeLog($prHandle, $psMsg, $piType) {
  $msType = "";
  switch($piType) {    
    case ABANDONMENT_LOG_INFO:
      $msType = "INFO";
    break; 
    case ABANDONMENT_LOG_WARNING:
      $msType = "WARNING";
    break;
    case ABANDONMENT_LOG_ERROR:
      $msType = "ERROR";
    break;
  }
  fwrite($prHandle, "[".SAASLib::getSAASDate(SAASLib::SAASTime(), true)."][$msType] ".$psMsg.PHP_EOL); 
}

/*
 * Cria arquivo para log.
 */
$grHandle = fopen('abandonment_log.txt', 'a');

/*
 * Carrega as configurações e armazena os dados necessários. 
 */
$goConfig = new SAASConfig();
$goConfig->loadConfig();
$gsAxurEmail = $goConfig->getConfig(CFG_AXUR_EMAIL);
$giAbandonmentDays = $goConfig->getConfig(CFG_ABANDONMENT_DAYS);
$giAbandonDate = strtotime("-{$giAbandonmentDays} days");

/*
 * Percorre todas as instâncias para verificar se nenhuma
 * delas poder ser considerada 'abandonada'.
 */
$goInstance = new SAASInstance();
$goInstance->createFilter(0, 'instance_id', '<>');
$goInstance->select();
$gaAbandonmentInstances = array();
$gaConnectionErrorInstances = array();
while ($goInstance->fetch()) {
  $gsAlias = $goInstance->getFieldValue('instance_alias');
  $goDB = new FWDDB(
    DB_POSTGRES,
    $goInstance->getFieldValue('instance_db_name'),
    $goInstance->getFieldValue('instance_db_user'),
    $goInstance->getFieldValue('instance_db_user_pass'),
    $goConfig->getConfig(CFG_DB_SERVER)
  );
  FWDWebLib::setErrorHandler(new FWDDoNothingErrorHandler());
  if ($goDB->connect()) {    
    $goDataset = new FWDDBDataSet($goDB, 'isms_user');
    $goField = new FWDDBField('dLastLogin', 'user_last_login', DB_DATETIME);
    $goFilter = new FWDDBFilter('>', $giAbandonDate);
    $goField->addFilter($goFilter);
    $goDataset->addFWDDBField($goField);
    $goDataset->select();
    if (!$goDataset->fetch() && (SAASLib::getTimestamp($goInstance->getFieldValue('instance_creation_date')) < $giAbandonDate)) {
      $gaAbandonmentInstances[] = $gsAlias;
      writeLog($grHandle, "Instance $gsAlias seems to have abandoned the system", ABANDONMENT_LOG_WARNING);
    }
  }
  else {
    $gaConnectionErrorInstances[] = $gsAlias;
    writeLog($grHandle, "Could not connect to $gsAlias database", ABANDONMENT_LOG_ERROR);
  }
  FWDWebLib::restoreLastErrorHandler();
}

/*
 * Monta as mensagens.
 */
$gsAbandonmentMessage = "";
if (count($gaAbandonmentInstances)) {
  $gsAbandonmentMessage = FWDLanguage::getPHPStringValue('em_instance_abandonment_detection_part_1', "The following instances seem to have abandoned the system: %abandonment_instanes%");
  $gsAbandonmentMessage = str_replace('%abandonment_instanes%', implode(', ', $gaAbandonmentInstances) , $gsAbandonmentMessage);
}
$gsConnectionMessage = "";
if (count($gaConnectionErrorInstances)) {
  $gsConnectionMessage = FWDLanguage::getPHPStringValue('em_instance_abandonment_detection_part_2', "The following instances could not be verified due to database connection problems: %connection_instanes%");
  $gsConnectionMessage = str_replace('%connection_instanes%', implode(', ', $gaConnectionErrorInstances), $gsConnectionMessage);
}

/*
 * Envia email para a AXUR.
 */      
if ($gsAbandonmentMessage || $gsConnectionMessage) {
  $goEmail = new FWDEmail();
  $goEmail->setSenderName($goConfig->getConfig(CFG_AXUR_DEFAULT_SENDER_NAME));
  $goEmail->setFrom($goConfig->getConfig(CFG_AXUR_DEFAULT_SENDER));
  $goEmail->setTo($gsAxurEmail);
  $goEmail->setSubject(FWDLanguage::getPHPStringValue('em_instance_abandonment_detection', "Realiso Abandonment Detection"));
  if ($gsAbandonmentMessage && $gsConnectionMessage) $gsMessage = $gsAbandonmentMessage . "<br/><br/>" . $gsConnectionMessage;
  else if ($gsAbandonmentMessage) $gsMessage = $gsAbandonmentMessage;
  else $gsMessage = $gsConnectionMessage;
  $goEmail->setMessage($gsMessage);
  if (!$goEmail->send())
    writeLog($grHandle, "Could not send abandonment detection summary email", ABANDONMENT_LOG_ERROR);
}
?>