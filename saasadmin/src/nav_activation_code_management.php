<?php
include_once "include.php";
include_once $handlers_grid_ref."QueryGridActivationCode.php";

class GridActivationCode extends FWDDrawGrid {
  public function drawItem(){
    switch($this->ciColumnIndex){
      case 3:
        if (!$this->coCellBox->getValue() && $this->caData[6]) $this->coCellBox->setValue("xxxxxxxxxxxxxxxxxxxxxx");        
        return $this->coCellBox->draw();
      break;      
      case 4:
        $this->coCellBox->setValue(SAASLib::getLicenseTypeAsString($this->coCellBox->getValue()));
        return $this->coCellBox->draw();
      break;
      
      case 5:
        if ($this->coCellBox->getValue()) $this->coCellBox->setValue(SAASLib::getSAASDate($this->coCellBox->getValue()));
        else $this->coCellBox->setValue("");        
        return $this->coCellBox->draw();
      break;
      
      case 6:
        if ($this->coCellBox->getValue()) $this->coCellBox->setValue(SAASLib::getSAASDate($this->coCellBox->getValue()));
        else $this->coCellBox->setValue("");
        return $this->coCellBox->draw();
      break;
      
      default:
        return parent::drawItem();
      break;
    }
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    
    $moGrid = FWDWebLib::getObject("grid_activation_code");
    $moHandler = new QueryGridActivationCode(FWDWebLib::getConnection());    
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new GridActivationCode());
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
  	// seta o link do bot�o de gera��o, um para o popup da realiso, outro para o da minimarisk
    $moGenerateEvent = new FWDClientEvent();
    $moGenerateEvent->setAttrEvent('onClick');
    $msID = "popup_generate_activation";
    if (PROVIDER) $msID .= "_minimarisk";
    $moGenerateEvent->setAttrValue("saas_open_popup('$msID','popup_generate_activation.php','','false');");
    FWDWebLib::getObject('code_generate')->addObjFWDEvent($moGenerateEvent);
  	
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        function refresh_grid() {
          js_refresh_grid('grid_activation_codes');    
        }
      </script>
    <?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_activation_code_management.xml");
?>