<?php

//set_time_limit(1200);

include_once "include.php";
include_once $handlers_ref.'QueryLicenseInfo.php';

define("CREATION_OK", 1);
define("CREATE_DATABASE_ERROR", 2);
define("EXECUTE_SCRIPTS_ERROR",3);
define("COPY_PHPS_ERROR", 4);
define("ADJUST_DB_ERROR", 5);
define("INSERT_INSTANCE_ERROR", 6);
define("LICENSE_INVALID", 7);

function createInstance($msAlias, $msEmail, $msPass, $miLicenseType, $client_id) {
	$msLanguage = 'en'; //en,ge,fr

	if (
		($miLicenseType != SAASActivation::SUBSCRIPTION_ISMS) &&
		($miLicenseType != SAASActivation::SUBSCRIPTION_BCMS) &&
		($miLicenseType != SAASActivation::SUBSCRIPTION_SOX)
		)
	{
		return LICENSE_INVALID;
	}

	$msCode = SAASActivation::insertActivation( $miLicenseType );

	$moConfig = new SAASConfig();
	$moConfig->loadConfig();

	$mbTrial = in_array($miLicenseType, SAASLib::getTrialLicenseTypes()) ? true : false;
	$populateWithTrialData = true;
	$maLicenseInfo = SAASLib::getLicenseInfo($moConfig, $msAlias, $miLicenseType);

	$moManager = new SAASInstanceManager($moConfig, $msAlias);

	FWDWebLib::setErrorHandler(new FWDDoNothingErrorHandler());
	$mbDebugMode = FWDWebLib::getInstance()->getDebugMode();
	$mbDebugType = FWDWebLib::getInstance()->getDebugType();
	FWDWebLib::getInstance()->setDebugMode(false,0);



	file_put_contents("/tmp/agoravai.txt", "Criando usuario e base de dados... - ".$miLicenseType."\n", FILE_APPEND);
	if (!$moManager->createUserAndDB()) {
		return CREATE_DATABASE_ERROR;
	}

	file_put_contents("/tmp/agoravai.txt", "Data base criada... - ".$miLicenseType."\n", FILE_APPEND);
	if (!$moManager->executeScripts($msEmail, $msPass , $mbTrial, $miLicenseType, $msLanguage, $populateWithTrialData)) {
		return EXECUTE_SCRIPTS_ERROR;
	}
	file_put_contents("/tmp/agoravai.txt", "Scripts executados com sucesso = ". $mbTrial. " ... \n", FILE_APPEND);
	if (!$moManager->copyPHPs($mbTrial)) {
		return COPY_PHPS_ERROR;
	}
	file_put_contents("/tmp/agoravai.txt", "Copia dos phps ...\n", FILE_APPEND);
	if (!$moManager->adjustDBAndSystem($maLicenseInfo)) {
		return ADJUST_DB_ERROR;
	}
	file_put_contents("/tmp/agoravai.txt", "Adjuste de DB e Sistema ...\n", FILE_APPEND);
	if (!$moManager->insertInstance($msAlias, $msCode)) {
		return INSERT_INSTANCE_ERROR;
	}
	file_put_contents("/tmp/agoravai.txt", "Instancia inserida com suceso ...\n", FILE_APPEND);

	setInstanceParameters($msAlias, $miLicenseType, $client_id);
	file_put_contents("/tmp/agoravai.txt", "Mudar data ... concluida \n", FILE_APPEND);

	return CREATION_OK;
}

function setInstanceParameters($msAlias, $miLicenseType, $client_id){
	$moConfig = new SAASConfig();
	$moConfig->loadConfig();

	$msClientName = $msAlias;
	$miMaxUsers = 1;
	$miMaxSimultUsers = 1;

	$tmpTimezone = date_default_timezone_get();
	date_default_timezone_set('America/New_York');
	$miExpirationDate = null;
	date_default_timezone_set($tmpTimezone);

	$msModules = "RM|PM|CI";

	$miType = SAASLib::convertAdminLicenseType2SystemLicenseType($miLicenseType);

	$msDatabaseLogin = 'inst_'.$msAlias;
	$msDatabaseLoginPass = md5('pass_'.$msAlias);
	$msDatabaseName = 'inst_'.$msAlias;

	$moDB = new FWDDB(
		DB_POSTGRES,
		$msDatabaseName,
		$msDatabaseLogin,
		$msDatabaseLoginPass,
		$moConfig->getConfig(CFG_DB_SERVER)
		);

	if (!$moDB->connect())
		die($moDB->getErrorMessage());

	$moHandler = new QueryLicenseInfo($moDB);
	$moHandler->makeQuery();
	$maRes = $moHandler->executeQuery();

	$miActivationDate = $maRes[SAASInstance::LICENSE_ACTIVATION_DATE];

	$msMD5 = md5($msClientName.$msModules.$miMaxUsers.$miActivationDate.$miExpirationDate.$miMaxSimultUsers.$miType.'LICENSEHASH');
	$msSQL = "UPDATE isms_saas SET sValue = '$msClientName' WHERE pkConfig = ".SAASInstance::LICENSE_CLIENT_NAME.";
	UPDATE isms_saas SET sValue = '$msModules' WHERE pkConfig = ".SAASInstance::LICENSE_MODULES.";
	UPDATE isms_saas SET sValue = '$miMaxUsers' WHERE pkConfig = ".SAASInstance::LICENSE_MAX_USERS.";
	UPDATE isms_saas SET sValue = '$miExpirationDate' WHERE pkConfig = ".SAASInstance::LICENSE_EXPIRACY.";
	UPDATE isms_saas SET sValue = '$miMaxSimultUsers' WHERE pkConfig = ".SAASInstance::LICENSE_MAX_SIMULTANEOUS_USERS.";
	UPDATE isms_saas SET sValue = '$miType' WHERE pkConfig = ".SAASInstance::LICENSE_TYPE.";
	UPDATE isms_saas SET sValue = '$msMD5' WHERE pkConfig = ".SAASInstance::LICENSE_HASH.";
	UPDATE isms_config SET sValue = '$client_id' WHERE pkConfig = 7703;";
	$moDB->execute($msSQL, 0, 0, true);
}

$msAlias = $_GET['alias'];
$msEmail = $_GET['email'];
$msPass  = $_GET['pass'];
$miLicenseType = $_GET['license'];
$client_id = $_GET['client_id'];

echo createInstance($msAlias, $msEmail, $msPass, $miLicenseType, $client_id);

?>
