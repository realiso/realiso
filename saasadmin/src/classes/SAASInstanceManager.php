<?php
/**
 * ISMS SAAS - INTERNET SECURITY MANAGEMENT SYSTEM SAAS
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

define("STEP_NONE", 0);
define("STEP_CONNECT_DB", 1);
define("STEP_CREATE_USER", 2);
define("STEP_CREATE_DB", 3);
define("STEP_CREATE_LANGUAGE", 4);
define("STEP_CREATE_TABLES", 5);
define("STEP_CREATE_TYPES", 6);
define("STEP_CREATE_VIEWS", 7);
define("STEP_CREATE_FUNCTIONS", 8);
define("STEP_CREATE_SPS", 9);
define("STEP_CREATE_TRIGGERS", 10);
define("STEP_POPULATE_CONFIG", 11);
define("STEP_POPULATE_ACLS", 12);
define("STEP_POPULATE_LIBRARIES", 13);
define("STEP_ALTER_SEQUENCE", 14);
define("STEP_CREATE_JOBS", 15);
define("STEP_COPY_PHPS", 16);
define("STEP_INSERT_LICENSE_INFO", 17);
define("STEP_ADJUST_CONFIG_PHP", 18);
define("STEP_INSERT_INSTANCE", 19);
define("STEP_UPDATE_LICENSE_INFO", 20);
define("STEP_UPDATE_INSTANCE", 21);
define("STEP_REVOKE_SUPERUSER", 22);
define("STEP_CREATE_CONSTRAINTS", 23);
define("STEP_CREATE_TRIAL", 24);
define("STEP_SET_PERMISSION", 25);

define("TABLE_SAAS_CONFIG", "isms_saas");
define("TABLE_ISMS_CONFIG", "isms_config");

/**
 * Classe SAASInstanceManager.
 *
 * <p>Classe que manipula as instâncias.</p>
 * @package SAAS
 * @subpackage classes
 */
class SAASInstanceManager implements FWDErrorHandler {

	protected $ciStep = STEP_NONE;

	protected $csLastError = "";

	protected $coConfig = null;
	protected $csAlias = "";
	protected $csDBName = "";
	protected $csDBUser = "";
	protected $csDBPass = "";
	protected $coDB;

	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe SAASConfig.</p>
	 * @access public
	 */
	public function __construct(SAASConfig $poConfig, $psAlias) {
		$this->coConfig = $poConfig;
		$this->csAlias = $psAlias;
		$this->csDBName = 'inst_'.$psAlias;
		$this->csDBUser = 'inst_'.$psAlias;
		$this->csDBPass = md5('pass_'.$psAlias);
	}

	/**
	 * Método que manipula erros.
	 *
	 * <p>Método que manipula erros.</p>
	 * @access public
	 * @param integer $piErrNo Número do erro
	 * @param string $psErrStr Mensagem de erro
	 * @param string $psErrFile Nome do arquivo que gerou o erro
	 * @param integer $piErrLine Número da linha em que ocorreu o erro
	 */
	public function handleError($piErrNo, $psErrStr, $psErrFile, $piErrLine){
		switch($this->ciStep){
			case STEP_CONNECT_DB:
				$msError = FWDLanguage::getPHPStringValue('st_could_not_connect_to_master_database',"Falha na conexão com o banco de dados principal.");
				break;
			case STEP_CREATE_USER:
				$msError = FWDLanguage::getPHPStringValue('st_could_not_create_user',"Falha na criação do usuário.");
				break;
			case STEP_CREATE_DB:
				$msError = FWDLanguage::getPHPStringValue('st_could_not_create_db',"Falha na criação do banco de dados.");
				break;
			case STEP_CREATE_LANGUAGE:
				$msError = FWDLanguage::getPHPStringValue('st_could_not_create_language',"Falha na criação da linguagem.");
				break;
			case STEP_CREATE_TABLES:
				$msError = FWDLanguage::getPHPStringValue('st_could_not_create_tables',"Falha na criação das tabelas.");
				break;
			case STEP_CREATE_CONSTRAINTS:
				$msError = FWDLanguage::getPHPStringValue('st_could_not_create_constraints',"Falha na criação das constraints.");
				break;
			case STEP_CREATE_TRIAL:
				$msError = FWDLanguage::getPHPStringValue('st_could_not_create_trial_data',"Falha na criação dos dados do trial.");
				break;
			case STEP_CREATE_TYPES:
				$msError = FWDLanguage::getPHPStringValue('st_could_not_create_types',"Falha na criação dos tipos.");
				break;
			case STEP_CREATE_VIEWS:
				$msError = FWDLanguage::getPHPStringValue('st_could_not_create_views',"Falha na criação das views.");
				break;
			case STEP_CREATE_FUNCTIONS:
				$msError = FWDLanguage::getPHPStringValue('st_could_not_create_functions',"Falha na criação das funções.");
				break;
			case STEP_CREATE_SPS:
				$msError = FWDLanguage::getPHPStringValue('st_could_not_create_sps',"Falha na criação das stored procedures.");
				break;
			case STEP_CREATE_TRIGGERS:
				$msError = FWDLanguage::getPHPStringValue('st_could_not_create_triggers',"Falha na criação das triggers.");
				break;
			case STEP_POPULATE_CONFIG:
				$msError = FWDLanguage::getPHPStringValue('st_could_not_populate_config',"Falha na inserção das configurações padrão.");
				break;
			case STEP_POPULATE_ACLS:
				$msError = FWDLanguage::getPHPStringValue('st_could_not_populate_acls',"Falha na inserção das ACLs.");
				break;
			case STEP_POPULATE_LIBRARIES:
				$msError = FWDLanguage::getPHPStringValue('st_could_not_populate_libraries',"Falha na inserção das bibliotecas.");
				break;
			case STEP_ALTER_SEQUENCE:
				$msError = FWDLanguage::getPHPStringValue('st_could_not_alter_sequence',"Falha na alteração das seqüências.");
				break;
			case STEP_REVOKE_SUPERUSER:
				$msError = FWDLanguage::getPHPStringValue('st_could_not_revoke_superuser',"Falha na alteração da permissão do usuário do banco.");
				break;
			case STEP_COPY_PHPS:
				$msError = FWDLanguage::getPHPStringValue('st_could_not_copy_phps',"Falha na cópia dos phps.");
				break;
			case STEP_SET_PERMISSION:
				$msError = FWDLanguage::getPHPStringValue('st_could_not_set_permission',"Falha no ajuste de permissões da pasta files e/ou do arquivo debug.txt.");
				break;
			case STEP_INSERT_LICENSE_INFO:
				$msError = FWDLanguage::getPHPStringValue('st_could_not_insert_license_info',"Falha na inserção das informações da licença.");
				break;
			case STEP_ADJUST_CONFIG_PHP:
				$msError = FWDLanguage::getPHPStringValue('st_could_not_adjust_config_php',"Falha no ajuste do arquivo config.php.");
				break;
			case STEP_INSERT_INSTANCE:
				$msError = FWDLanguage::getPHPStringValue('st_could_not_insert_instance',"Falha na inserção da instância no banco de dados.");
				break;
			case STEP_NONE:
				$msError = FWDLanguage::getPHPStringValue('st_unknown_error',"Falha não identificada.");
				break;
			default:
				$msError = '';
		}
		$this->csLastError = $msError . " - " . ($this->coDB ? $this->coDB->getErrorMessage(): "");
		try{
			$exception = new FWDException($msError, 0);
			$exception->setFile(0);
			$exception->setLine(0);
			throw $exception;
		}catch(FWDException $moException){
			self::handleException($moException);
		}
	}

	/**
	 * Método que manipula exceções.
	 *
	 * <p>Método que manipula exceções.</p>
	 * @access public
	 * @param FWDException $poFWDException Exceção
	 * @static
	 */
	public static function handleException($poFWDException){
		echo "alert('" . $poFWDException->getTraceAsString() . "');";
	}

	/**
	 * Atualiza uma determinada configuração de todas as instâncias do sistema.
	 *
	 * <p>Método para atualizar uma determinada configuração de todas as instâncias do sistema.</p>
	 * @access public
	 * @param integer $piConfigId Id da configuração
	 * @param string $psValue Valor da configuração
	 * @param string $psTable Tabela que se encontra a config
	 */
	public static function updateAllInstances($piConfigId, $psValue, $psTable) {
		/*
		 * Carrega configurações.
		 */
		$moConfig = new SAASConfig();
		$moConfig->loadConfig();

		/*
		 * Carrega as informações de cada instância.
		 */
		$moInstance = new SAASInstance();
		$moInstance->createFilter(0, 'instance_id', '<>');
		$moInstance->select();
		$maErrorInstances = array();
		while ($moInstance->fetch()) {
			$this->coDB = new FWDDB(
			DB_POSTGRES,
			$moInstance->getFieldValue('instance_db_name'),
			$moInstance->getFieldValue('instance_db_user'),
			$moInstance->getFieldValue('instance_db_user_pass'),
			$moConfig->getConfig(CFG_DB_SERVER)
			);
			FWDWebLib::setErrorHandler(new FWDDoNothingErrorHandler());
			if ($this->coDB->connect()) {
				$moDataset = new FWDDBDataSet($this->coDB);
				$moDataset->setQuery("UPDATE $psTable SET sValue = '$psValue' WHERE pkConfig = $piConfigId");
				if (!$moDataset->execute())
				$maErrorInstances[$moInstance->getFieldValue('instance_id')] = array($moInstance->getFieldValue('instance_alias'), $moInstance->getFieldValue('instance_db_name'));
			}
			else $maErrorInstances[$moInstance->getFieldValue('instance_id')] = array($moInstance->getFieldValue('instance_alias'), $moInstance->getFieldValue('instance_db_name'));
			FWDWebLib::restoreLastErrorHandler();
		}
		return $maErrorInstances;
	}

	public function createUserAndDB() {
		set_time_limit(7200);

		FWDWebLib::setErrorHandler($this);

		$msDatabaseLogin = $this->csDBUser;
		$msDatabaseLoginPass = $this->csDBPass;
		$msDatabaseName = $this->csDBName;

		/*
		 * Conecta no banco principal.
		 */
		$this->ciStep = STEP_CONNECT_DB;
		$this->coDB = new FWDDB(
		DB_POSTGRES,
		$this->coConfig->getConfig(CFG_DB),
		$this->coConfig->getConfig(CFG_DB_USER),
		$this->coConfig->getConfig(CFG_DB_USER_PASS),
		$this->coConfig->getConfig(CFG_DB_SERVER)
		);
		$this->coDB->connect();

		/*
		 * Cria o usuário que vai ser utilizado pela instância.
		 */
		if (!$this->csLastError) {
			$this->ciStep = STEP_CREATE_USER;
			$msSQL = str_replace(array('%database_login%', '%database_login_pass%'), array($msDatabaseLogin, $msDatabaseLoginPass), file_get_contents("../sqls/isms_postgres_create_db_user.sql"));
			$this->coDB->execute($msSQL." ALTER ROLE $msDatabaseLogin SUPERUSER;", 0, 0, true);
			echo $this->coDB->getErrorMessage();
		}
		else return false;

		/*
		 * Cria o banco de dados que vai ser utilizado pela instância.
		 */
		if (!$this->csLastError) {
			$this->ciStep = STEP_CREATE_DB;
			$msSQL = str_replace(array('%database_name%', '%database_login%'), array($msDatabaseName, $msDatabaseLogin), file_get_contents("../sqls/isms_postgres_create_db.sql"));
			$this->coDB->execute($msSQL, 0, 0, true);
		}
		else return false;

		$this->ciStep = STEP_NONE;

		FWDWebLib::restoreLastErrorHandler();

		return true;
	}

	public function executeScripts($psUser, $psPass, $pbTrial, $piLicenseType, $psLang = null, $psPopulateWithTrialData = false){
		set_time_limit(7200);

		FWDWebLib::setErrorHandler($this);

		$msPath = $this->coConfig->getConfig(CFG_ADMIN_PATH).'classes/config.php';

		$mbIsISMS = SAASLib::isISMSLicense($piLicenseType);
		$mbIsEMS = SAASLib::isEMSLicense($piLicenseType);
		$mbIsBCMS = SAASLib::isBCMSLicense($piLicenseType);
		$mbIsOHS = SAASLib::isOHSLicense($piLicenseType);
		$mbIsSOX = SAASLib::isSOXLicense($piLicenseType);
		$mbIsPCI = SAASLib::isPCILicense($piLicenseType);
		$mbIsMinima = SAASLib::isMinimaRiskLicense($piLicenseType);

		if($psPopulateWithTrialData == true && $mbIsBCMS){
			$pbTrial = false;
		}elseif($psPopulateWithTrialData == true){
			$pbTrial = true;
		}

		// linguagem da base default - cobit, iso, riscos, etc
		if ($mbIsEMS) $msLang = "br_ems";
		else if ($mbIsOHS) $msLang = "br_ohs";
		else if ($mbIsSOX) $msLang = "en_sox";
		else if ($mbIsPCI) $msLang = "en_pci";
		else if ($mbIsMinima){
			if ($psLang == 'en' || $psLang == 'ge' || $psLang == 'fr'){
				$msLang= $psLang."_sox";
			} else {
				$msLang = "en_sox";
			}
		}
		else $msLang = "en";

		// essa linguagem é só o default dos usu�rios
		if ($mbIsISMS) $miLanguage = '3302';
		else if ($mbIsEMS) $miLanguage = '3304';
		else if ($mbIsOHS) $miLanguage = '3305';
		else if ($mbIsSOX) $miLanguage = '3308';
		else if ($mbIsMinima) $miLanguage = "3308";
		else if ($mbIsPCI) $miLanguage = '3309';
		else if ($mbIsBCMS) $miLanguage = '3302';

		$msDatabaseLogin = $this->csDBUser;
		$msDatabaseLoginPass = $this->csDBPass;
		$msDatabaseName = $this->csDBName;

		$maConfig = array (
      '%user_fullname%' => $psUser,
      '%user_login%' => $psUser,
      '%user_passwordmd5%' => md5($psPass),
      '%user_email%' => $psUser,
      '%user_language%' => $miLanguage,
      '%default_sender%' => $this->coConfig->getConfig(CFG_AXUR_DEFAULT_SENDER),
      '%default_sender_name%' => $this->coConfig->getConfig(CFG_AXUR_DEFAULT_SENDER_NAME),
      '%error_report_email%' => $this->coConfig->getConfig(CFG_AXUR_EMAIL),
      '%saas_report_email%' => $this->coConfig->getConfig(CFG_AXUR_EMAIL),
      '%config_path%' => $msPath,
      '%max_file_size%' => $this->coConfig->getConfig(CFG_MAX_UPLOAD_FILE_SIZE),
		);

		$this->coDB = new FWDDB(
		DB_POSTGRES,
		$msDatabaseName,
		$msDatabaseLogin,
		$msDatabaseLoginPass,
		$this->coConfig->getConfig(CFG_DB_SERVER)
		);
		$this->coDB->connect();

		if ($mbIsBCMS) {
			if (!$this->csLastError && $pbTrial ){
				$this->ciStep = STEP_CREATE_TRIAL;
				/* ajustar trial do bcms */
				$msSQL = str_replace(array_keys($maConfig),$maConfig,file_get_contents("../sqls/bcms_postgres_trial_create.sql"));
				if($msSQL)
					$this->coDB->execute($msSQL, 0, 0, true);
			} else if (!$this->csLastError){
				$this->ciStep = STEP_CREATE_TABLES;
				$msSQL = (file_get_contents("../sqls/bcms_postgres_tables_create.sql"));
				$this->coDB->execute($msSQL, 0, 0, true);
			}else return false;

			/*
			 * Cria as constraints.
			 */
			if (!$this->csLastError) {
				$this->ciStep = STEP_CREATE_CONSTRAINTS;
				$msSQL = file_get_contents("../sqls/bcms_postgres_constraints_create.sql");
				$this->coDB->execute($msSQL, 0, 0, true);
			}
			else return false;

			/*
			 * Cria os tipos.
			 */
			if (!$this->csLastError) {
				$this->ciStep = STEP_CREATE_TYPES;
				$msSQL = file_get_contents("../sqls/bcms_postgres_types_create.sql");
				$this->coDB->execute($msSQL, 0, 0, true);
			}
			else return false;

			/*
			 * Cria as funções.
			 */
			if (!$this->csLastError) {
				$this->ciStep = STEP_CREATE_FUNCTIONS;
				$msSQL = file_get_contents("../sqls/bcms_postgres_functions_create.sql");
				$this->coDB->execute($msSQL, 0, 0, true);
			}
			else return false;

			/*
			 * Cria as views.
			 */
			if (!$this->csLastError) {
				$this->ciStep = STEP_CREATE_VIEWS;
				$msSQL = file_get_contents("../sqls/bcms_postgres_views_create.sql");
				$this->coDB->execute($msSQL, 0, 0, true);
			}
			else return false;

			/*
			 * Cria as triggers.
			 */
			if (!$this->csLastError) {
				$this->ciStep = STEP_CREATE_TRIGGERS;
				$msSQL = file_get_contents("../sqls/bcms_postgres_triggers_create.sql");
				$this->coDB->execute($msSQL, 0, 0, true);
			}
			else return false;

		}else{

			/*
			 * Cria a linguagem.
			 */
			if (!$this->csLastError) {
				$this->ciStep = STEP_CREATE_LANGUAGE;
				$msSQL = file_get_contents("../sqls/isms_postgres_language_create.sql");
				$this->coDB->execute($msSQL, 0, 0, true);
			}
			else return false;

			/*
			 * Cria as tabelas.
			 */
			if (!$this->csLastError) {
				$this->ciStep = STEP_CREATE_TABLES;
				$msSQL = file_get_contents("../sqls/isms_postgres_tables_create.sql");
				$this->coDB->execute($msSQL, 0, 0, true);
			}
			else return false;

			/*
			 * Se for trial e isms, cria os dados default.
			 */
			if ($pbTrial && $mbIsISMS) {
				if (!$this->csLastError) {
					$this->ciStep = STEP_CREATE_TRIAL;
					$msSQL = str_replace(array_keys($maConfig),$maConfig,file_get_contents("../sqls/isms_postgres_trial_create_copy.sql"));
					$this->coDB->execute($msSQL, 0, 0, true);
				}
				else return false;
			}

			/*
			 * Se for trial e sox, cria os dados default.
			 */
			if ($pbTrial && $mbIsSOX) {
				if (!$this->csLastError) {
					$this->ciStep = STEP_CREATE_TRIAL;
					$msSQL = str_replace(array_keys($maConfig),$maConfig,file_get_contents("../sqls/sox_postgres_trial_create_copy.sql"));
					$this->coDB->execute($msSQL, 0, 0, true);
				}
				else return false;
			}

			/*
			 * Se for trial e pci, cria os dados default.
			 */
			if ($pbTrial && $mbIsPCI) {
				if (!$this->csLastError) {
					$this->ciStep = STEP_CREATE_TRIAL;
					$msSQL = str_replace(array_keys($maConfig),$maConfig,file_get_contents("../sqls/pci_postgres_trial_create.sql"));
					$this->coDB->execute($msSQL, 0, 0, true);
				}
				else return false;
			}

			/*
			 * Se for trial e pci, cria os dados default.
			 */
			if ($pbTrial && $mbIsOHS) {
				if (!$this->csLastError) {
					$this->ciStep = STEP_CREATE_TRIAL;
					$msSQL = str_replace(array_keys($maConfig),$maConfig,file_get_contents("../sqls/ohs_postgres_trial_create.sql"));
					$this->coDB->execute($msSQL, 0, 0, true);
				}
				else return false;
			}

			/*
			 * Cria as constraints.
			 */
			if (!$this->csLastError) {
				$this->ciStep = STEP_CREATE_CONSTRAINTS;
				$msSQL = file_get_contents("../sqls/isms_postgres_constraints_create.sql");
				$this->coDB->execute($msSQL, 0, 0, true);
			}
			else return false;

			/*
			 * Cria os tipos.
			 */
			if (!$this->csLastError) {
				$this->ciStep = STEP_CREATE_TYPES;
				$msSQL = file_get_contents("../sqls/isms_postgres_types_create.sql");
				$this->coDB->execute($msSQL, 0, 0, true);
			}
			else return false;

			/*
			 * Cria as views.
			 */
			if (!$this->csLastError) {
				$this->ciStep = STEP_CREATE_VIEWS;
				$msSQL = file_get_contents("../sqls/isms_postgres_views_create.sql");
				$this->coDB->execute($msSQL, 0, 0, true);
			}
			else return false;

			/*
			 * Cria as funções.
			 */
			if (!$this->csLastError) {
				$this->ciStep = STEP_CREATE_FUNCTIONS;
				$msSQL = file_get_contents("../sqls/isms_postgres_functions_create.sql");
				$this->coDB->execute($msSQL, 0, 0, true);
			}
			else return false;

			/*
			 * Cria as stored_procedures.
			 */
			if (!$this->csLastError) {
				$this->ciStep = STEP_CREATE_SPS;
				$msSQL = file_get_contents("../sqls/isms_postgres_stored_procedures_create.sql");
				$this->coDB->execute($msSQL, 0, 0, true);
			}
			else return false;

			/*
			 * Cria as triggers.
			 */
			if (!$this->csLastError) {
				$this->ciStep = STEP_CREATE_TRIGGERS;
				$msSQL = file_get_contents("../sqls/isms_postgres_triggers_create.sql");
				$this->coDB->execute($msSQL, 0, 0, true);
			}
			else return false;
		}
		/*
		 * Insere as configurações default.
		 */
		if (!($pbTrial && ($mbIsISMS || $mbIsBCMS || $mbIsSOX || $mbIsPCI || $mbIsOHS || $mbIsBCMS))) {
			if (!$this->csLastError) {
				$this->ciStep = STEP_POPULATE_CONFIG;

				if($mbIsBCMS){
				  $msSQL = str_replace(array_keys($maConfig),$maConfig,file_get_contents("../sqls/bcms_postgres_install_populate_config_pt.sql"));
				} else {
				  $msSQL = str_replace(array_keys($maConfig),$maConfig,file_get_contents("../sqls/isms_postgres_install_populate_config_$msLang.sql"));
				}

				$this->coDB->execute($msSQL, 0, 0, true);
			}
			else return false;
		}

		/*
		 * Insere as ACLs.
		 */
		if (!($pbTrial && ($mbIsISMS || $mbIsBCMS || $mbIsSOX || $mbIsPCI || $mbIsOHS))) {
			if (!$this->csLastError) {
				$this->ciStep = STEP_POPULATE_ACLS;
				$msSQL = file_get_contents("../sqls/isms_postgres_profile_acl.sql");
				$this->coDB->execute($msSQL, 0, 0, true);
			}
			else return false;
		}

		/*
		 * Insere as informações das biliotecas do sistema.
		 */
		if (!$pbTrial && $mbIsISMS) {
			if (!$this->csLastError) {
				$this->ciStep = STEP_POPULATE_LIBRARIES;

				$msSQL = file_get_contents("../sqls/isms_postgres_populate_05_rm_category_$msLang.sql");
				$this->coDB->execute($msSQL, 0, 0, true);
				$msSQL = file_get_contents("../sqls/isms_postgres_populate_06_rm_event_$msLang.sql");
				$this->coDB->execute($msSQL, 0, 0, true);
				$msSQL = file_get_contents("../sqls/isms_postgres_populate_07_rm_section_best_practice_$msLang.sql");
				$this->coDB->execute($msSQL, 0, 0, true);
				$msSQL = file_get_contents("../sqls/isms_postgres_populate_08_rm_best_practice_$msLang.sql");
				$this->coDB->execute($msSQL, 0, 0, true);
				$msSQL = file_get_contents("../sqls/isms_postgres_populate_09_rm_standard_$msLang.sql");
				$this->coDB->execute($msSQL, 0, 0, true);
				$msSQL = file_get_contents("../sqls/isms_postgres_populate_10_rm_best_practice_standard_$msLang.sql");
				$this->coDB->execute($msSQL, 0, 0, true);
				$msSQL = file_get_contents("../sqls/isms_postgres_populate_11_rm_best_practice_event_$msLang.sql");
				$this->coDB->execute($msSQL, 0, 0, true);
			}
			else return false;
		}
		else if (!$pbTrial && $mbIsBCMS) {
			if (!$this->csLastError) {
				$this->ciStep = STEP_POPULATE_LIBRARIES;

//				$msSQL = file_get_contents("../sqls/bcms_postgres_tables_create.sql");
//				$this->coDB->execute($msSQL, 0, 0, true);
			}
			else return false;
		}
		else if ($mbIsEMS) {
			if (!$this->csLastError) {
				$this->ciStep = STEP_POPULATE_LIBRARIES;

				$msSQL = file_get_contents("../sqls/isms_postgres_populate_05_rm_category_$msLang.sql");
				$this->coDB->execute($msSQL, 0, 0, true);
				$msSQL = file_get_contents("../sqls/isms_postgres_populate_06_rm_event_$msLang.sql");
				$this->coDB->execute($msSQL, 0, 0, true);
			}
			else return false;
		}
		else if ($mbIsOHS) {
			if (!$this->csLastError) {
				$this->ciStep = STEP_POPULATE_LIBRARIES;

				// ...
			}
			else return false;
		}
		else if (!$pbTrial && $mbIsSOX) {
			if (!$this->csLastError) {
				$this->ciStep = STEP_POPULATE_LIBRARIES;

				$msSQL = file_get_contents("../sqls/isms_postgres_populate_05_rm_category_$msLang.sql");
				$this->coDB->execute($msSQL, 0, 0, true);
				$msSQL = file_get_contents("../sqls/isms_postgres_populate_06_rm_event_$msLang.sql");
				$this->coDB->execute($msSQL, 0, 0, true);
				$msSQL = file_get_contents("../sqls/isms_postgres_populate_07_rm_section_best_practice_$msLang.sql");
				$this->coDB->execute($msSQL, 0, 0, true);
				$msSQL = file_get_contents("../sqls/isms_postgres_populate_08_rm_best_practice_$msLang.sql");
				$this->coDB->execute($msSQL, 0, 0, true);
				$msSQL = file_get_contents("../sqls/isms_postgres_populate_09_rm_standard_$msLang.sql");
				$this->coDB->execute($msSQL, 0, 0, true);
				$msSQL = file_get_contents("../sqls/isms_postgres_populate_10_rm_best_practice_standard_$msLang.sql");
				$this->coDB->execute($msSQL, 0, 0, true);
			}
			else return false;
		}
		else if (!$pbTrial && $mbIsPCI) {
			if (!$this->csLastError) {
				$this->ciStep = STEP_POPULATE_LIBRARIES;

				$msSQL = file_get_contents("../sqls/isms_postgres_populate_05_rm_category_$msLang.sql");
				$this->coDB->execute($msSQL, 0, 0, true);
				$msSQL = file_get_contents("../sqls/isms_postgres_populate_06_rm_event_$msLang.sql");
				$this->coDB->execute($msSQL, 0, 0, true);
				$msSQL = file_get_contents("../sqls/isms_postgres_populate_07_rm_section_best_practice_$msLang.sql");
				$this->coDB->execute($msSQL, 0, 0, true);
				$msSQL = file_get_contents("../sqls/isms_postgres_populate_08_rm_best_practice_$msLang.sql");
				$this->coDB->execute($msSQL, 0, 0, true);
				$msSQL = file_get_contents("../sqls/isms_postgres_populate_09_rm_standard_$msLang.sql");
				$this->coDB->execute($msSQL, 0, 0, true);
				$msSQL = file_get_contents("../sqls/isms_postgres_populate_10_rm_best_practice_standard_$msLang.sql");
				$this->coDB->execute($msSQL, 0, 0, true);
			}
			else return false;
		}

		/*
		 * Ajusta as sequências de algumas tabelas.
		 */
		if (!$this->csLastError) {
			if(!($pbTrial && ($mbIsISMS || $mbIsSOX || $mbIsPCI || $mbIsOHS || $mbIsBCMS))){
				$this->ciStep = STEP_ALTER_SEQUENCE;

				if($mbIsBCMS){
				  $msSQL = file_get_contents("../sqls/bcms_postgres_populate_14_alter_table_isms_context.sql");
				}else{
				  $msSQL = file_get_contents("../sqls/isms_postgres_populate_14_alter_table_isms_context.sql");
				}

				$this->coDB->execute($msSQL, 0, 0, true);
			}
		}
		else return false;

		/*
		 * Retira privilégio de superusuário.
		 */
		if (!$this->csLastError) {
			$this->ciStep = STEP_REVOKE_SUPERUSER;
			$this->coDB->execute("ALTER ROLE $msDatabaseLogin NOSUPERUSER;", 0, 0, true);
		}
		else return false;

		$this->ciStep = STEP_NONE;

		FWDWebLib::restoreLastErrorHandler();

		return true;
	}

	public function copy_dir($srcdir, $dstdir) {
		$num = 0;
		if(!is_dir($dstdir)) mkdir($dstdir, 0750, true);
		if($curdir = opendir($srcdir)) {
			while($file = readdir($curdir)) {
				if($file != '.' && $file != '..') {
					$srcfile = $srcdir . '/' . $file;
					$dstfile = $dstdir . '/' . $file;
					if(is_file($srcfile)) {
						if(copy($srcfile, $dstfile)) {
							touch($dstfile, filemtime($srcfile)); $num++;
						}
						else {
							return -1;
						}
					}
					else if(is_dir($srcfile)) {
						$num += $this->copy_dir($srcfile, $dstfile);
					}
				}
			}
			closedir($curdir);
		}
		return $num;
	}

	public function copyPHPs($pbTrial){
		set_time_limit(7200);

		FWDWebLib::setErrorHandler($this);

		$this->ciStep = STEP_COPY_PHPS;

		if ($pbTrial) $msDstFolder = FOLDER_TRIAL."/" . $this->csAlias;
		else $msDstFolder = FOLDER_SUBSCRIPTION."/" . $this->csAlias;
		$msHome = $this->coConfig->getConfig(CFG_WEB_INSTANCE_PATH);
		$msSrc = $this->coConfig->getConfig(CFG_WEB_INSTANCE_SRC_PATH);
		$msDestDir = $msHome . $msDstFolder;
		if (!is_dir($msDestDir)) mkdir($msDestDir, 0750, true);

		if (substr(PHP_OS, 0, 3) == "WIN") {
			if ($this->copy_dir($msSrc.FOLDER_SOURCE_SYSTEM, $msHome.$msDstFolder) == -1) {
				$this->csLastError = FWDLanguage::getPHPStringValue('st_could_not_copy_phps',"Falha na cópia dos phps.");
				return false;
			}
		}
		else {
			symlink($msSrc.FOLDER_SOURCE_SYSTEM, $msDestDir . "/sys");
			symlink($msSrc.FOLDER_SOURCE_SYSTEM . "/index.php", $msDestDir . "/index.php");
			mkdir($msDestDir. "/files");
			mkdir($msDestDir. "/custom_gfx");
			touch($msDestDir . "/debug.txt");
		}

		/*
		 * Permissão para a pasta files e para o arquivo debug.txt.
		 */
		$this->ciStep = STEP_SET_PERMISSION;

		if (substr(PHP_OS, 0, 3) == "WIN") {
			if (!(chmod($msHome.$msDstFolder.'/packages/policy/files', 0770) && chmod($msHome.$msDstFolder.'/lib/debug.txt', 0770))) {
				$this->csLastError = FWDLanguage::getPHPStringValue('st_could_not_set_permission',"Falha no ajuste de permissões da pasta files e/ou do arquivo debug.txt.");
				return false;
			}
		}
		else {
			/* Arrumar isso.
			 if (!(chmod($msDestDir.'/files', 0770) && chmod($msDestDir.'/debug.txt', 0770))) {
			 $this->csLastError = FWDLanguage::getPHPStringValue('st_could_not_set_permission',"Falha no ajuste de permissões da pasta files e/ou do arquivo debug.txt.");
			 return false;
			 }
			 */
			if (!(chmod($msDestDir.'/files', 0770) && chmod($msDestDir.'/debug.txt', 0770) && chmod($msDestDir.'/custom_gfx', 0770) )) {
				$this->csLastError = FWDLanguage::getPHPStringValue('st_could_not_set_permission',"Falha no ajuste de permissões da pasta files, custom_gfx ou do arquivo debug.txt.");
				return false;
			}

		}

		$this->ciStep = STEP_NONE;

		FWDWebLib::restoreLastErrorHandler();

		return true;
	}

	public function adjustDBAndSystem($paLicenseInfo){
		set_time_limit(7200);

		FWDWebLib::setErrorHandler($this);

		/*
		 * Insere informações da licença.
		 */
		$this->ciStep = STEP_INSERT_LICENSE_INFO;
		$msClientName = $paLicenseInfo['client'];
		$miMaxUsers = $paLicenseInfo['max_users'];
		$miMaxSimultUsers = $paLicenseInfo['max_simult_users'];
		$miExpirationDate = $paLicenseInfo['expiration_date'];
		$msModules = $paLicenseInfo['modules'];
		$miType = $paLicenseInfo['type'];
		$miSystemLicenseType = SAASLib::convertAdminLicenseType2SystemLicenseType($miType);
		$miActivationDate = $paLicenseInfo['activation_date'];
		$msMD5 = md5($msClientName.$msModules.$miMaxUsers.$miActivationDate.$miExpirationDate.$miMaxSimultUsers.$miSystemLicenseType.'LICENSEHASH');

		$msDatabaseLogin = $this->csDBUser;
		$msDatabaseLoginPass = $this->csDBPass;
		$msDatabaseName = $this->csDBName;
		$this->coDB = new FWDDB(
		DB_POSTGRES,
		$msDatabaseName,
		$msDatabaseLogin,
		$msDatabaseLoginPass,
		$this->coConfig->getConfig(CFG_DB_SERVER)
		);
		$this->coDB->connect();

		if (in_array($miType, SAASLib::getTrialLicenseTypes())) $msDstFolder = FOLDER_TRIAL."/" . $this->csAlias;
		else $msDstFolder = FOLDER_SUBSCRIPTION."/" . $this->csAlias;

		$msEmail = $this->coConfig->getConfig(CFG_AXUR_EMAIL);
		$msDays = $this->coConfig->getConfig(CFG_ABANDONMENT_DAYS);
		$msSQL = "UPDATE isms_saas SET sValue = '$msEmail' WHERE pkConfig = ".SAASInstance::SAAS_REPORT_EMAIL.";
              UPDATE isms_saas SET sValue = '$msClientName' WHERE pkConfig = ".SAASInstance::LICENSE_CLIENT_NAME.";
              UPDATE isms_saas SET sValue = '$msModules' WHERE pkConfig = ".SAASInstance::LICENSE_MODULES.";
              UPDATE isms_saas SET sValue = '$miMaxUsers' WHERE pkConfig = ".SAASInstance::LICENSE_MAX_USERS.";
              UPDATE isms_saas SET sValue = '$miActivationDate' WHERE pkConfig = ".SAASInstance::LICENSE_ACTIVATION_DATE.";
              UPDATE isms_saas SET sValue = '$miExpirationDate' WHERE pkConfig = ".SAASInstance::LICENSE_EXPIRACY.";
              UPDATE isms_saas SET sValue = '$miMaxSimultUsers' WHERE pkConfig = ".SAASInstance::LICENSE_MAX_SIMULTANEOUS_USERS.";
              UPDATE isms_saas SET sValue = '$miSystemLicenseType' WHERE pkConfig = ".SAASInstance::LICENSE_TYPE.";
              UPDATE isms_saas SET sValue = '$msMD5' WHERE pkConfig = ".SAASInstance::LICENSE_HASH.";";
		$this->coDB->execute($msSQL, 0, 0, true);

		/*
		 * Cria o arquivo config.php
		 */
		if (!$this->csLastError) {
			$this->ciStep = STEP_ADJUST_CONFIG_PHP;
			$msPath = $this->coConfig->getConfig(CFG_WEB_INSTANCE_PATH);

			if (substr(PHP_OS, 0, 3) == "WIN") $msFullPath = $msPath.$msDstFolder."/classes/config.php";
			else $msFullPath = $msPath.$msDstFolder."/config.php";

			$msConfig = '<?xml version="1.0" encoding="ISO-8859-1"?>
                    <root>
                      <database>
                        <type>DB_POSTGRES</type>
                        <host>%server%</host>
                        <port>5432</port>
                        <database>%database%</database>
                        <schema>%user%</schema>
                        <password>%password%</password>
                      </database>
                      <debug>
                        <type></type>
                      </debug>
                    </root>';

			$moModule = mcrypt_module_open(MCRYPT_BLOWFISH,'',MCRYPT_MODE_ECB,'');
			$msKey = "b306737041cb40b1492e0debcfe45433";
			mcrypt_generic_init($moModule, $msKey, "12345678");

			$msDatabaseLoginPassEncrypted = base64_encode(mcrypt_generic($moModule, $msDatabaseLoginPass));

			$msConfig = str_replace(
			array('%database%','%user%','%password%','%server%'),
			array($msDatabaseName, $msDatabaseLogin, $msDatabaseLoginPassEncrypted, $this->coConfig->getConfig(CFG_DB_SERVER)),
			$msConfig
			);
			$mrHandle = fopen($msFullPath, 'w');
			fwrite($mrHandle, $msConfig);
			fclose($mrHandle);
		}
		else return false;

		$this->ciStep = STEP_NONE;

		FWDWebLib::restoreLastErrorHandler();

		return true;
	}

	public function insertInstance($psAlias, $psCode) {
		set_time_limit(7200);

		FWDWebLib::setErrorHandler($this);

		$this->ciStep = STEP_INSERT_INSTANCE;

		/*
		 * Insere a instância e pega seu id.
		 */
		$moInstance = new SAASInstance();
		$moInstance->setFieldValue('instance_alias', $psAlias);
		$moInstance->setFieldValue('instance_creation_date', SAASLib::SAASTime());
		$moInstance->setFieldValue('instance_db_name', $this->csDBName);
		$moInstance->setFieldValue('instance_db_user', $this->csDBUser);
		$moInstance->setFieldValue('instance_db_user_pass', $this->csDBPass);
		$miId = $moInstance->insert(true);

		if ($psCode) {
			$moUpActivation = new SAASActivation();
			$moUpActivation->createFilter($psCode, "activation_code");
			$moUpActivation->setFieldValue("activation_date_used", SAASLib::SAASTime());
			$moUpActivation->setFieldValue("activation_instance_id", $miId);
			$moUpActivation->update();
		}

		$this->ciStep = STEP_NONE;

		FWDWebLib::restoreLastErrorHandler();

		return true;
	}

	/*
	 * Função retirada de www.php.net
	 */
	private static function deltree($f){
		foreach(glob($f.'/*') as $sf){
			if (is_dir($sf) && !is_link($sf)){
				self::deltree($sf);
				rmdir($sf);
			}else{
				unlink($sf);
			}
		}
	}

	public static function removeInstance($piInstanceId) {
		/*
		 * Busca tipo da licença da instância.
		 */
		$moActivation = new SAASActivation();
		foreach (SAASLib::getLicenseTypes() as $miType) $moActivation->createFilter($miType, 'activation_type', 'in');
		$moActivation->createFilter($piInstanceId, 'activation_instance_id');
		$moActivation->select();
		$moActivation->fetch();
		$miType = $moActivation->getFieldValue('activation_type');

		/*
		 * Carrega configurações.
		 */
		$moConfig = new SAASConfig();
		$moConfig->loadConfig();

		/*
		 * Carrega informações da instância.
		 */
		$moInstance = new SAASInstance();
		$moInstance->fetchById($piInstanceId);
		$msInstanceAlias = $moInstance->getFieldValue('instance_alias');
		$msDBName = $moInstance->getFieldValue('instance_db_name');
		$msDBUser = $moInstance->getFieldValue('instance_db_user');
		$msFolder = $moInstance->getFieldValue('instance_alias');

		/*
		 * Remove diretório da instância.
		 */
		if ($moInstance->getFieldValue('instance_alias')) {
			$msInstanceDir = $moConfig->getConfig(CFG_WEB_INSTANCE_PATH);
			if (in_array($miType, SAASLib::getSubscriptionLicenseTypes())) $msInstanceDir .= FOLDER_SUBSCRIPTION . '/' . $moInstance->getFieldValue('instance_alias');
			else $msInstanceDir .= FOLDER_TRIAL . '/' . $moInstance->getFieldValue('instance_alias');
			self::deltree($msInstanceDir);
			rmdir($msInstanceDir);

			/*
			 * Remove banco de dados e usuário da instância.
			 */
			$coDB = new FWDDB(
			DB_POSTGRES,
			$moConfig->getConfig(CFG_DB),
			$moConfig->getConfig(CFG_DB_USER),
			$moConfig->getConfig(CFG_DB_USER_PASS),
			$moConfig->getConfig(CFG_DB_SERVER)
			);
			$coDB->connect();
			$msSQL = "DROP DATABASE $msDBName;";
			$coDB->execute($msSQL, 0, 0, true);
			$msSQL = "DROP ROLE $msDBUser;";
			$coDB->execute($msSQL, 0, 0, true);

			/*
			 * Deleta instância do banco de dados.
			 */
			$moInstance = new SAASInstance();
			$moInstance->createFilter($piInstanceId, 'instance_id');
			$moInstance->delete();

			return true;
		}
		else return false;
	}

	public function getLastError() {
		return $this->csLastError;
	}
}
?>
