<?php
class SAASLogger {

 /**
  * Constante para indicar mensagem de informação.
  * @constant
  */
  const INFO = 1;
  
 /**
  * Constante para indicar mensagem de warning.
  * @constant
  */
  const WARNING = 2;
  
 /**
  * Constante para indicar mensagem de erro.
  * @constant
  */
  const ERROR = 3;

 /**
  * Nome do arquivo de log.
  * @var string
  * @access private
  */
  private $csFileName = '';
 
 /**
  * Handler do arquivo de log.
  * @var resource
  * @access private
  */
  private $crHandler = null;
  
 /**
  * Construtor da classe.
  * 
  * <p>Construtor da classe de log.</p>
  * @access public
  * @param string $psFileName Nome do arquivo 
  */
  public function __construct($psFileName){
    $this->csFileName = $psFileName;
  }
 
 /**
  * Método que abre o arquivo de log para escrita.
  * 
  * <p>Método que abre o arquivo de log para escrita. Deve ser
  * chamado antes de qualquer operação de escrita.</p>
  * @access public
  */
  public function openLog(){
    $this->crHandler = fopen($this->csFileName,'a');
  }
 
 /**
  * Método para escrever no arquivo de log.
  * 
  * <p>Método para escrever no arquivo de log.</p>
  * @access public
  * @param integer $piType Tipo de mensagem
  * @param string $psMessage Mensagem 
  */ 
  public function write($piType,$psMessage){
    switch($piType){
      case self::INFO:    $msType = 'INFO';    break;
      case self::WARNING: $msType = 'WARNING'; break;
      case self::ERROR:   $msType = 'ERROR';   break;
      default:            $msType = 'UNKNOWN'; break;
    }
    //echo "[".SAASLib::getSAASDate(SAASLib::SAASTime(), true)."][{$msType}] {$psMessage}<br>\n";
    fwrite($this->crHandler, "[".SAASLib::getSAASDate(SAASLib::SAASTime(), true)."][{$msType}] {$psMessage}\n");
  }

 /**
  * Método que fecha o arquivo de log.
  * 
  * <p>Método que fecha o arquivo de log para escrita. Deve ser
  * chamado após todas as operações de escrita.</p>
  * @access public
  */
  public function closeLog(){
    fclose($this->crHandler);
  }
}
?>