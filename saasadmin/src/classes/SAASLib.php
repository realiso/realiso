<?php
/**
 * ISMS SAAS - INTERNET SECURITY MANAGEMENT SYSTEM SAAS
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/*
 * N�mero de dias que indica quando uma inst�ncia trial pode ser removida.
 */
define('DAYS_TO_REMOVE', 30);

/**
 * Classe SAASLib. Singleton. Implementa vari�veis e m�todos globais do SAAS.
 *
 * <p>Classe singleton que implementa vari�veis e m�todos globais do SAAS.
 * Garante a exist�ncia de uma e apenas uma inst�ncia da classe SAASLib.</p>
 * @package SAAS
 * @subpackage classes
 */
class SAASLib {

  /* 
      * Constantes para identificar os sitemas.
      */
  const SYSTEM_ISMS = '6ef935';  // echo substr(md5('isms'), 0, 6);
  const SYSTEM_BCMS = '538a47';  // echo substr(md5('bcms'), 0, 6);
  const SYSTEM_EMS  = '52a19f';  // echo substr(md5('ems'), 0, 6);
  const SYSTEM_OHS  = '7fe6fc';  // echo substr(md5('ohs'), 0, 6);
  const SYSTEM_SOX  = '792e5d';  // echo substr(md5('sox'), 0, 6);
  const SYSTEM_PCI  = '8d829c';  // echo substr(md5('pci'), 0, 6);
  const SYSTEM_MINIMA = '4d41a8'; //echo substr(md5('minima'), 0, 6);

  /** 
   * Inst�ncia �nica da classe SAASLib
   * @staticvar SAASLib
   * @access private
   */
  private static $coSAASLib = NULL;
  
  /** 
   * Build do sistema
   * @staticvar SAASLib
   * @access private
   */
  private static $csBuild = '223';
  
  /**
   * Construtor da classe.
   *
   * <p>Construtor da classe SAASLib.</p>
   * @access public 
   * @return SAASLib Inst�ncia �nica da classe SAASLib
   * @static
   */ 
  public function __construct() {}

  /**
   * Retorna a inst�ncia �nica da classe SAASLib (singleton).
   *
   * <p>Retorna a inst�ncia �nica da classe SAASLib. Se a inst�ncia
   * ainda n�o existir, cria a �nica inst�ncia. Se a inst�ncia j� existir,
   * retorna a �nica inst�ncia (singleton).</p>
   * @access public 
   * @return SAASLib Inst�ncia �nica da classe SAASLib
   * @static
   */
  public static function getInstance() {
    if (self::$coSAASLib == NULL) {
      self::$coSAASLib = new SAASLib();
    }
    else {} // inst�ncia �nica j� existe, retorne-a
    
    return self::$coSAASLib;
  }
  
  /**
   * Retorna a build do sistema.
   *
   * <p>Retorna a build do sistema.</p>
   * @access public 
   * @return string Build do sistema
   * @static
   */
  public static function getBuild() {
    return self::$csBuild;
  }
    
  /**
   * Retorna a hora local corrigindo erro de GMT.
   *
   * <p>O PHP est� considerando o timezone do sistema operacional para calcular a hora
   * Esta fun��o retorna a hora local corrigindo este erro.</p>
   * @access public 
   * @return timestamp 
   */
  public static function SAASTime() {
    return time();
  } 

  /**
   * Inicializa o sistema.
   *
   * <p>M�todo para inicializar o sistema.</p>
   * @access public
   */
  public function SAAS_INIT() {
  	session_start();
  	$moUser = new SAASUser();
  	$maDontTest = array('login.php', 'activation.php', 'post_activation.php', 'popup_terms_of_service.php', 'cron_check_schedules_instace.php', 'create_instance.php', 'change_instance.php', 'popup_reset_database.php', 'create_instance_validation.php', 'user_collect_data.php');
    $msFileName = substr($_SERVER['PHP_SELF'],strrpos($_SERVER['PHP_SELF'], "/")+1);
   
    if (!in_array($msFileName, $maDontTest) && !$moUser->isActive()){
    	echo '<script>document.location.href = "login.php";</script>';
    	return;
    }
    $moWebLib = FWDWebLib::getInstance();
    $moWebLib->setWindowTitle("ISMS SaaS Admin");
  }

 /**
  * Abre uma PopUp de confirma��o.
  *
  * <p>Abre uma PopUp de confirma��o.</p>
  * @access public
  * @param string $psTitle T�tulo da PopUp
  * @param string $psMessage Mensagem a ser exibida
  * @param string $psConfirmEvent C�digo JavaScript do evento a ser executado quando o usu�rio confirma
  * @param integer $piMessageHeight Altura do static com a mensagem. � opcional e se omitido, o valor no XML � mantido.
  */
  public static function openConfirm($psTitle,$psMessage,$psConfirmEvent,$piMessageHeight=0,$psDeniedEvent = ''){
    $moWebLib = FWDWebLib::getInstance();
    $msPopUpId = 'popup_confirm';
    $msXmlFile = $moWebLib->getSysRef().'confirm.xml';
    $msIconSrc = $moWebLib->getSysRefBasedOnTabMain().'gfx/'.'icon-exclamation.gif';
    $moWebLib->xml_load($msXmlFile,false);
    
    $moPanel = $moWebLib->getObject('confirm_panel');
    $moTitle = $moWebLib->getObject('confirm_title');
    $moMessage = $moWebLib->getObject('confirm_message');
    $moButtons = $moWebLib->getObject('confirm_buttons');
    
    $moTitle->setValue($psTitle);
    $moMessage->setValue($psMessage);
    $moWebLib->getObject('confirm_icon')->setAttrSrc($msIconSrc);
    // Ajusta as alturas, caso o par�metro piMessageHeight tenha sido passado
    if($piMessageHeight>0){
      $moPanelBox = $moPanel->getObjFWDBox();
      $moMessageBox = $moMessage->getObjFWDBox();
      $moButtonsBox = $moButtons->getObjFWDBox();
      
      $miMessageTop = $moMessageBox->getAttrTop();
      $miMessageHeight = $piMessageHeight;
      $miButtonsTop = $miMessageTop + $miMessageHeight + 10;
      $miPanelHeight = $miButtonsTop + $moButtonsBox->getAttrHeight() + 10;
      $miPanelWidth = $moPanelBox->getAttrWidth();
      
      $moButtonsBox->setAttrTop($miButtonsTop);
      $moPanelBox->setAttrHeight($miPanelHeight);
      $moTitle->getObjFWDBox()->setAttrWidth($miPanelWidth);
    }else{
      $moPanelBox = $moPanel->getObjFWDBox();
      $miPanelHeight = $moPanelBox->getAttrHeight();
      $miPanelWidth = $moPanelBox->getAttrWidth();
    }
    
    // centraliza a viewgroup dos botoes 'sim' e 'n�o'
    $miConfirmPanelWidth = $moPanel->getObjFWDBox()->getAttrWidth();
    $miConfirmButtonsWidth = $moButtons->getObjFWDBox()->getAttrWidth();
    $miDelta = floor( ($miConfirmPanelWidth - $miConfirmButtonsWidth) / 2 );
    $moButtons->getObjFWDBox()->setAttrLeft($miDelta);
    
    // Evento do n�o
    $moEvent = new FWDClientEvent();
    $moEvent->setAttrEvent('onClick');
    $moEvent->setAttrValue($psDeniedEvent."soPopUpManager.closePopUp('$msPopUpId');");
    $moWebLib->getObject('confirm_viewbutton_no')->addObjFWDEvent($moEvent);
    // Evento do sim
    $moEvent = new FWDClientEvent();
    $moEvent->setAttrEvent('onClick');
    $moEvent->setAttrValue($psConfirmEvent."soPopUpManager.closePopUp('$msPopUpId');");
    $moWebLib->getObject('confirm_viewbutton_yes')->addObjFWDEvent($moEvent);
    
    $msXml = str_replace(array("\n",'"'),array(' ','\"'),$moPanel->draw());
    echo "saas_open_popup('$msPopUpId','','','true',$miPanelHeight,$miPanelWidth);"
        ."soPopUpManager.getPopUpById('$msPopUpId').setHtmlContent(\"$msXml\");";
  }
  
 /**
  * Abre uma PopUp de Ok.
  *
  * <p>Abre uma PopUp de Ok.</p>
  * @access public
  * @param string $psTitle T�tulo da PopUp
  * @param string $psMessage Mensagem a ser exibida
  * @param string $psConfirmEvent C�digo JavaScript do evento a ser executado quando o usu�rio pressiona o bot�o 'OK'
  * @param integer $piMessageHeight Altura do static com a mensagem. � opcional e se omitido, o valor no XML � mantido.
  */
  public static function openOk($psTitle,$psMessage,$psConfirmEvent="",$piMessageHeight=0){
    $moWebLib = FWDWebLib::getInstance();
    $msPopUpId = 'popup_ok';
    $msXmlFile = $moWebLib->getSysRef().'ok.xml';
    
    $msIconSrc = $moWebLib->getSysRefBasedOnTabMain().'gfx/'.'icon-exclamation.gif';
    
    $moWebLib->xml_load($msXmlFile,false);
    $moPanel = $moWebLib->getObject('ok_panel');
    $moMessage = $moWebLib->getObject('ok_message');
    $moButton = $moWebLib->getObject('viewbutton_ok');
    $moWebLib->getObject('ok_title')->setValue($psTitle);
    $moMessage->setValue($psMessage);
    $moWebLib->getObject('ok_icon')->setAttrSrc($msIconSrc);
    // Ajusta as alturas, caso o par�metro piMessageHeight tenha sido passado
    if($piMessageHeight>0){
      $moPanelBox = $moPanel->getObjFWDBox();
      $moMessageBox = $moMessage->getObjFWDBox();
      $moButtonBox = $moButton->getObjFWDBox();
      
      $miMessageTop = $moMessageBox->getAttrTop();
      $miMessageHeight = $piMessageHeight;
      $miButtonTop = $miMessageTop + $miMessageHeight + 10;
      $miPanelHeight = $miButtonTop + $moButtonBox->getAttrHeight() + 10;
      $miPanelWidth = $moPanelBox->getAttrWidth();
      
      $moButtonBox->setAttrTop($miButtonTop);
      $moPanelBox->setAttrHeight($miPanelHeight);
    }else{
      $moPanelBox = $moPanel->getObjFWDBox();
      $miPanelHeight = $moPanelBox->getAttrHeight();
      $miPanelWidth = $moPanelBox->getAttrWidth();
    }
    
    // centraliza o bot�o 'ok'
    $miOkPanelWidth = $moPanel->getObjFWDBox()->getAttrWidth();
    $miOkButtonWidth = $moButton->getObjFWDBox()->getAttrWidth();
    $miDelta = floor( ($miOkPanelWidth - $miOkButtonWidth) / 2 );
    $moButton->getObjFWDBox()->setAttrLeft($miDelta);
    
    // Evento do ok
    $moEvent = new FWDClientEvent();
    $moEvent->setAttrEvent('onClick');
    $moEvent->setAttrValue($psConfirmEvent."soPopUpManager.closePopUp('$msPopUpId');");
    $moButton->addObjFWDEvent($moEvent);
    
    $msXml = str_replace(array("\n",'"'),array(' ','\"'),$moPanel->draw());
    echo "saas_open_popup('$msPopUpId','','','true',$miPanelHeight,$miPanelWidth);"
        ."soPopUpManager.getPopUpById('$msPopUpId').setHtmlContent(\"$msXml\");";
  }
       
  /**
   * Retorna uma data no formato do SAAS.
   * 
   * <p>M�todo para retornar uma data no formato do SAAS.</p>
   * @access public
   * @param string $psDate Data
   * @param boolean $pbIsTimestamp � um timestamp ou uma data em string
   * @return string Data
   */
  public static function getSAASDate($psDate = "", $pbIsTimestamp = false) {
    if (!$pbIsTimestamp) {
      if ($psDate) $piTimestamp = self::getTimestamp($psDate);
      else $piTimestamp = SAASLib::SAASTime();
    }
    else $piTimestamp = $psDate;
    
    $msDateFormat = FWDLanguage::getPHPStringValue('mx_shortdate_format', "");
    if ($msDateFormat == "%m/%d/%Y")
      return date("m/d/Y h:i a", $piTimestamp);
    elseif ($msDateFormat == "%d/%m/%Y")
      return date("d/m/Y H:i", $piTimestamp);
    else
      trigger_error("Invalid date format!", E_USER_WARNING);
  }
  
  /**
   * Retorna uma data "curta" (XX/XX/AAAA) no formato do SAAS.
   * 
   * <p>M�todo para retornar uma data "curta" (XX/XX/AAAA) no formato do SAAS.</p>
   * @access public
   * @param string $psDate Data
   * @param boolean $pbIsTimestamp � um timestamp ou uma data em string
   * @return string Data
   */
  public static function getSAASShortDate($psDate = "", $pbIsTimestamp = false) {
    if (!$pbIsTimestamp) {
      if ($psDate) $piTimestamp = self::getTimestamp($psDate);
      else $piTimestamp = SAASLib::SAASTime();
    }
    else $piTimestamp = $psDate;
    
    $msDateFormat = FWDLanguage::getPHPStringValue('mx_shortdate_format', "%m/%d/%Y");
    if ($msDateFormat == "%m/%d/%Y")
      return date("m/d/Y", $piTimestamp);
    elseif ($msDateFormat == "%d/%m/%Y")
      return date("d/m/Y", $piTimestamp);
    else
      trigger_error("Invalid date format!", E_USER_WARNING);
  }
  
  /**
   * Retorna um timestamp baseado em uma data no formato YYYY-MM-DD HH-NN-SS.
   * 
   * <p>M�todo para retornar um timestamp baseado em uma data no formato YYYY-MM-DD HH-NN-SS.</p>
   * @access public
   * @param string $psDate Data
   * @return integer Timestamp
   */
  public static function getTimestamp($psDate) {
    return FWDWebLib::getInstance()->getConnection()->getTimestamp($psDate);
  }
  
  /**
   * Retorna uma data no formato do banco.
   * 
   * <p>M�todo para retornar uma data no formato do banco.</p>
   * @access public
   * @param string $piTimestamp Timestamp
   * @return string Data
   */
  public static function getTimestampFormat($piTimestamp) {
    return FWDWebLib::getInstance()->getConnection()->timestampFormat($piTimestamp);
  }
  
  /**
   * Retorna as informações da licença.
   * 
   * <p>Método para retornar as informações da licença.</p>
   * @access public
   * @param object $poConfig Configuração
   * @param integer $psAlias Alias
   * @param integer $piType Tipo da licença
   * @return array Informações da licença
   */
  public static function getLicenseInfo(SAASConfig $poConfig, $psAlias, $piType) {
    $maLicense = array();
    $miTime = FWDWebLib::getTime();
    switch ($piType) {
      case SAASActivation::SUBSCRIPTION_ISMS:
        $maLicense = array(
          'client'=> $psAlias,
          'max_users'=> 1,
          'max_simult_users'=> 1,
          'activation_date'=> $miTime,
          'expiration_date'=> $miTime+(60*60*24), // 24 horas
          'modules'=> 'RM|PM|CI',
          'type' => $piType
        );
      break;
      case SAASActivation::TRIAL_ISMS:
        $maLicense = array(
          'client'=> $psAlias,
          'max_users'=> $poConfig->getConfig(CFG_DEFAULT_MAX_USERS),
          'max_simult_users'=> $poConfig->getConfig(CFG_DEFAULT_MAX_SIM_USERS),
          'activation_date'=> $miTime,
          'expiration_date'=> $miTime+(60*60*24*$poConfig->getConfig(CFG_DEFAULT_EXPIRACY_PERIOD)),
          'modules'=> $poConfig->getConfig(CFG_DEFAULT_MODULES),
          'type' => $piType
        );
	break;

        case SAASActivation::SUBSCRIPTION_BCMS:
        $maLicense = array(
		'client'=> $psAlias,
		'max_users'=> 1,
		'max_simult_users'=> 1,
		'activation_date'=> $miTime,
		'expiration_date'=> $miTime+(60*60*24), // 24 horas
		'modules'=> 'RM|PM|CI',
		'type' => $piType
	);
	break;
        case SAASActivation::TRIAL_BCMS:
	$maLicense = array(
		'client'=> $psAlias,
		'max_users'=> $poConfig->getConfig(CFG_DEFAULT_MAX_USERS),
		'max_simult_users'=> $poConfig->getConfig(CFG_DEFAULT_MAX_SIM_USERS),
		'activation_date'=> $miTime,
		'expiration_date'=> $miTime+(60*60*24*$poConfig->getConfig(CFG_DEFAULT_EXPIRACY_PERIOD)),
		'modules'=> $poConfig->getConfig(CFG_DEFAULT_MODULES),
		'type' => $piType
	);
	break;
      case SAASActivation::SUBSCRIPTION_EMS:
        $maLicense = array(
          'client'=> $psAlias,
          'max_users'=> 1,
          'max_simult_users'=> 1,
          'activation_date'=> $miTime,
          'expiration_date'=> $miTime+(60*60*24), // 24 horas
          'modules'=> 'RM|PM|CI',
          'type' => $piType
        );
      break;
      case SAASActivation::TRIAL_EMS:
        $maLicense = array(
          'client'=> $psAlias,
          'max_users'=> $poConfig->getConfig(CFG_DEFAULT_MAX_USERS),
          'max_simult_users'=> $poConfig->getConfig(CFG_DEFAULT_MAX_SIM_USERS),
          'activation_date'=> $miTime,
          'expiration_date'=> $miTime+(60*60*24*$poConfig->getConfig(CFG_DEFAULT_EXPIRACY_PERIOD)),
          'modules'=> $poConfig->getConfig(CFG_DEFAULT_MODULES),
          'type' => $piType
        );
      break;
      case SAASActivation::SUBSCRIPTION_OHS:
        $maLicense = array(
          'client'=> $psAlias,
          'max_users'=> 1,
          'max_simult_users'=> 1,
          'activation_date'=> $miTime,
          'expiration_date'=> $miTime+(60*60*24), // 24 horas
          'modules'=> 'RM|PM|CI',
          'type' => $piType
        );
      break;
      case SAASActivation::TRIAL_OHS:
        $maLicense = array(
          'client'=> $psAlias,
          'max_users'=> $poConfig->getConfig(CFG_DEFAULT_MAX_USERS),
          'max_simult_users'=> $poConfig->getConfig(CFG_DEFAULT_MAX_SIM_USERS),
          'activation_date'=> $miTime,
          'expiration_date'=> $miTime+(60*60*24*$poConfig->getConfig(CFG_DEFAULT_EXPIRACY_PERIOD)),
          'modules'=> $poConfig->getConfig(CFG_DEFAULT_MODULES),
          'type' => $piType
        );
      break;
      case SAASActivation::SUBSCRIPTION_SOX:
        $maLicense = array(
          'client'=> $psAlias,
          'max_users'=> 1,
          'max_simult_users'=> 1,
          'activation_date'=> $miTime,
          'expiration_date'=> $miTime+(60*60*24), // 24 horas
          'modules'=> 'RM|PM|CI',
          'type' => $piType
        );
      break;
      case SAASActivation::TRIAL_SOX:
        $maLicense = array(
          'client'=> $psAlias,
          'max_users'=> $poConfig->getConfig(CFG_DEFAULT_MAX_USERS),
          'max_simult_users'=> $poConfig->getConfig(CFG_DEFAULT_MAX_SIM_USERS),
          'activation_date'=> $miTime,
          'expiration_date'=> $miTime+(60*60*24*$poConfig->getConfig(CFG_DEFAULT_EXPIRACY_PERIOD)),
          'modules'=> $poConfig->getConfig(CFG_DEFAULT_MODULES),
          'type' => $piType
        );
      break;
      case SAASActivation::SUBSCRIPTION_PCI:
        $maLicense = array(
          'client'=> $psAlias,
          'max_users'=> 1,
          'max_simult_users'=> 1,
          'activation_date'=> $miTime,
          'expiration_date'=> $miTime+(60*60*24), // 24 horas
          'modules'=> 'RM|PM|CI',
          'type' => $piType
        );
      break;
      case SAASActivation::TRIAL_PCI:
        $maLicense = array(
          'client'=> $psAlias,
          'max_users'=> $poConfig->getConfig(CFG_DEFAULT_MAX_USERS),
          'max_simult_users'=> $poConfig->getConfig(CFG_DEFAULT_MAX_SIM_USERS),
          'activation_date'=> $miTime,
          'expiration_date'=> $miTime+(60*60*24*$poConfig->getConfig(CFG_DEFAULT_EXPIRACY_PERIOD)),
          'modules'=> $poConfig->getConfig(CFG_DEFAULT_MODULES),
          'type' => $piType
        );
      break;
      case SAASActivation::SUBSCRIPTION_MINIMARISK:
        $maLicense = array(
          'client'=> $psAlias,
          'max_users'=> 1,
          'max_simult_users'=> 1,
          'activation_date'=> $miTime,
          'expiration_date'=> $miTime+(60*60*24), // 24 horas
          'modules'=> 'RM|PM|CI',
          'type' => $piType
        );
      break;
      case SAASActivation::TRIAL_MINIMARISK:
        $maLicense = array(
          'client'=> $psAlias,
          'max_users'=> $poConfig->getConfig(CFG_DEFAULT_MAX_USERS),
          'max_simult_users'=> $poConfig->getConfig(CFG_DEFAULT_MAX_SIM_USERS),
          'activation_date'=> $miTime,
          'expiration_date'=> $miTime+(60*60*24*$poConfig->getConfig(CFG_DEFAULT_EXPIRACY_PERIOD)),
          'modules'=> $poConfig->getConfig(CFG_DEFAULT_MODULES),
          'type' => $piType
        );
      break;
      default:
        trigger_error("Invalid license type: $piType!", E_USER_ERROR);
      break;
    }    
    return $maLicense;  
  }
  
 /**
   * Retorna todos os tipos de licenças trial ou subscription.
   * 
   * <p>Método para retornar todos os tipos de licenças trial ou subscription.
   * Importante: Não retorna o tipo USER_PACK.</p>
   * @access public
   * @return array Array com os tipos de licença
   */
  public static function getLicenseTypes() {
    return array_merge(self::getTrialLicenseTypes(), self::getSubscriptionLicenseTypes());
  }
  
  /**
   * Retorna todos os tipos de licenças trial.
   * 
   * <p>Método para retornar todos os tipos de licenças trial.   
   * @access public
   * @return array Array com os tipos de licença
   */
  public static function getTrialLicenseTypes() {
    return array(
      SAASActivation::TRIAL_ISMS,
      SAASActivation::TRIAL_BCMS,
      SAASActivation::TRIAL_EMS,
      SAASActivation::TRIAL_OHS,
      SAASActivation::TRIAL_SOX,
      SAASActivation::TRIAL_PCI,
      SAASActivation::TRIAL_MINIMARISK
    );
  }
  
  /**
   * Retorna todos os tipos de licenças subscription.
   * 
   * <p>Método para retornar todos os tipos de licenças subscription.
   * @access public
   * @return array Array com os tipos de licença
   */
  public static function getSubscriptionLicenseTypes() {
    return array(      
      SAASActivation::SUBSCRIPTION_ISMS,
      SAASActivation::SUBSCRIPTION_BCMS,
      SAASActivation::SUBSCRIPTION_EMS,
      SAASActivation::SUBSCRIPTION_OHS,
      SAASActivation::SUBSCRIPTION_SOX,
      SAASActivation::SUBSCRIPTION_PCI,
      SAASActivation::SUBSCRIPTION_MINIMARISK
    );
  }
  
  /**
   * Retorna todos o tipo da licença como string.
   * 
   * <p>Método para retornar o tipo da licença como string.
   * @access public
   * @param integer Tipo da licença (inteiro)
   * @return string Tipo da licença (string)
   */
  public static function getLicenseTypeAsString($piType) {
    $msType = "";
    switch ($piType) {
      case SAASActivation::SUBSCRIPTION_ISMS:
        $msType = "SUBSCRIPTION_ISMS";
      break;
      case SAASActivation::TRIAL_ISMS:
        $msType = "TRIAL_ISMS";
      break;
      case SAASActivation::SUBSCRIPTION_BCMS:
	$msType = "SUBSCRIPTION_BCMS";
      break;
      case SAASActivation::TRIAL_BCMS:
	$msType = "TRIAL_BCMS";
      break;
      case SAASActivation::SUBSCRIPTION_EMS:
        $msType = "SUBSCRIPTION_EMS";
      break;
      case SAASActivation::TRIAL_EMS:
        $msType = "TRIAL_EMS";
      break;
      case SAASActivation::SUBSCRIPTION_OHS:
        $msType = "SUBSCRIPTION_OHS";
      break;
      case SAASActivation::TRIAL_OHS:
        $msType = "TRIAL_OHS";
      break;
      case SAASActivation::SUBSCRIPTION_SOX:
        $msType = "SUBSCRIPTION_SOX";
      break;
      case SAASActivation::TRIAL_SOX:
        $msType = "TRIAL_SOX";
      break;
      case SAASActivation::SUBSCRIPTION_PCI:
        $msType = "SUBSCRIPTION_PCI";
      break;
      case SAASActivation::TRIAL_PCI:
        $msType = "TRIAL_PCI";
      break;
      case SAASActivation::TRIAL_MINIMARISK:
      	$msType = "TRIAL_MINIMARISK";
      break;
      case SAASActivation::SUBSCRIPTION_MINIMARISK:
      	$msType = "SUBSCRIPTION_MINIMARISK";
      break;
      case SAASActivation::USER_PACK:
        $msType = "USER_PACK";
      break;
      default:
        $msType = "";
        trigger_error("Invalid license type: $piType!", E_USER_WARNING);
      break;
    }
    return $msType;
  }
  
  /**
   * Converte o tipo de licença do admin para o tipo do sistema.
   * 
   * <p>Método para converter o tipo de licença do admin para o tipo do sistema.
   * @access public
   * @param integer $piType Tipo da licença do admin
   * @return integer Tipo da licença do sistema
   */
  public static function convertAdminLicenseType2SystemLicenseType($piType) {
    $miType = 0;
    switch ($piType) {
      case SAASActivation::SUBSCRIPTION_ISMS:
        $miType = SAASInstance::LICENSE_TYPE_SUBSCRIPTION_ISMS;
      break;
      case SAASActivation::TRIAL_ISMS:
        $miType = SAASInstance::LICENSE_TYPE_TRIAL_ISMS;
      break;
      case SAASActivation::SUBSCRIPTION_BCMS:
	$miType = SAASInstance::LICENSE_TYPE_SUBSCRIPTION_BCMS;
      break;
      case SAASActivation::TRIAL_BCMS:
	$miType = SAASInstance::LICENSE_TYPE_TRIAL_BCMS;
      break;
      case SAASActivation::SUBSCRIPTION_EMS:
        $miType = SAASInstance::LICENSE_TYPE_SUBSCRIPTION_EMS;
      break;
      case SAASActivation::TRIAL_EMS:
        $miType = SAASInstance::LICENSE_TYPE_TRIAL_EMS;
      break;
      case SAASActivation::SUBSCRIPTION_OHS:
        $miType = SAASInstance::LICENSE_TYPE_SUBSCRIPTION_OHS;
      break;
      case SAASActivation::TRIAL_OHS:
        $miType = SAASInstance::LICENSE_TYPE_TRIAL_OHS;
      break;
      case SAASActivation::SUBSCRIPTION_SOX:
        $miType = SAASInstance::LICENSE_TYPE_SUBSCRIPTION_SOX;
      break;
      case SAASActivation::TRIAL_SOX:
        $miType = SAASInstance::LICENSE_TYPE_TRIAL_SOX;
      break;
      case SAASActivation::SUBSCRIPTION_PCI:
        $miType = SAASInstance::LICENSE_TYPE_SUBSCRIPTION_PCI;
      break;
      case SAASActivation::TRIAL_PCI:
        $miType = SAASInstance::LICENSE_TYPE_TRIAL_PCI;
      break;
      case SAASActivation::TRIAL_MINIMARISK:
      	$miType = SAASInstance::LICENSE_TYPE_TRIAL_SOX_MINIMA;
      break;
      case SAASActivation::SUBSCRIPTION_MINIMARISK:
      	$miType = SAASInstance::LICENSE_TYPE_SUBSCRIPTION_SOX_MINIMA;
      break;
      default:
        trigger_error("Invalid license type: $piType!", E_USER_ERROR);
      break;
    }
    return $miType;
  }
  
  /**
   * Converte o tipo de licença do sistema para o tipo do admin.
   * 
   * <p>Método para converter o tipo de licença do sistema para o tipo do admin.
   * @access public
   * @param integer $piType Tipo da licença do sistema
   * @return integer Tipo da licença do admin
   */
  public static function convertSystemLicenseType2AdminLicenseType($piType) {
    $miType = 0;
    switch ($piType) {
      case SAASInstance::LICENSE_TYPE_SUBSCRIPTION_ISMS:
        $miType = SAASActivation::SUBSCRIPTION_ISMS;
      break;
      case SAASInstance::LICENSE_TYPE_TRIAL_ISMS:
        $miType = SAASActivation::TRIAL_ISMS;
      break;
      case SAASInstance::LICENSE_TYPE_SUBSCRIPTION_BCMS:
	$miType = SAASActivation::SUBSCRIPTION_BCMS;
      break;
      case SAASInstance::LICENSE_TYPE_TRIAL_BCMS:
	$miType = SAASActivation::TRIAL_BCMS;
      break;
      case SAASInstance::LICENSE_TYPE_SUBSCRIPTION_EMS:
        $miType = SAASActivation::SUBSCRIPTION_EMS;
      break;
      case SAASInstance::LICENSE_TYPE_TRIAL_EMS:
        $miType = SAASActivation::TRIAL_EMS;
      break;
      case SAASInstance::LICENSE_TYPE_SUBSCRIPTION_OHS:
        $miType = SAASActivation::SUBSCRIPTION_OHS;
      break;
      case SAASInstance::LICENSE_TYPE_TRIAL_OHS:
        $miType = SAASActivation::TRIAL_OHS;
      break;
      case SAASInstance::LICENSE_TYPE_SUBSCRIPTION_SOX:
        $miType = SAASActivation::SUBSCRIPTION_SOX;
      break;
      case SAASInstance::LICENSE_TYPE_TRIAL_SOX:
        $miType = SAASActivation::TRIAL_SOX;
      break;
      case SAASInstance::LICENSE_TYPE_SUBSCRIPTION_PCI:
        $miType = SAASActivation::SUBSCRIPTION_PCI;
      break;
      case SAASInstance::LICENSE_TYPE_TRIAL_PCI:
        $miType = SAASActivation::TRIAL_PCI;
      break;
      case SAASInstance::LICENSE_TYPE_SUBSCRIPTION_SOX_MINIMA:
        $miType = SAASActivation::SUBSCRIPTION_MINIMARISK;
      break;
      case SAASInstance::LICENSE_TYPE_TRIAL_SOX_MINIMA:
        $miType = SAASActivation::TRIAL_MINIMARISK;
      break;
      default:
        trigger_error("Invalid license type: $piType!", E_USER_ERROR);
      break;
    }
    return $miType;
  }
  
  /**
   * Verifica se o código da licença é do EMS.
   * 
   * <p>Método para verificar se o código da licença é do EMS.
   * @access public
   * @param integer $piType Tipo da licença
   * @return boolean True se for do tipo EMS
   */
  public static function isEMSLicense($piType) {
    return in_array($piType, array(SAASActivation::SUBSCRIPTION_EMS, SAASActivation::TRIAL_EMS)) ? true : false;
  }
  
  /**
   * Verifica se o código da licença é do ISMS.
   * 
   * <p>Método para verificar se o código da licença é do ISMS.
   * @access public
   * @param integer $piType Tipo da licença
   * @return boolean True se for do tipo ISMS
   */
  public static function isISMSLicense($piType) {
    return in_array($piType, array(SAASActivation::SUBSCRIPTION_ISMS, SAASActivation::TRIAL_ISMS)) ? true : false;
  }

  public static function isBCMSLicense($piType) {
	  return in_array($piType, array(SAASActivation::SUBSCRIPTION_BCMS, SAASActivation::TRIAL_BCMS)) ? true : false;
  }

  /**
   * Verifica se o código da licença é do OHS.
   * 
   * <p>Método para verificar se o código da licença é do OHS.
   * @access public
   * @param integer $piType Tipo da licença
   * @return boolean True se for do tipo OHS
   */
  public static function isOHSLicense($piType) {
    return in_array($piType, array(SAASActivation::SUBSCRIPTION_OHS, SAASActivation::TRIAL_OHS)) ? true : false;
  }
  
  /**
   * Verifica se o código da licença é do SOX.
   * 
   * <p>Método para verificar se o código da licença é do SOX.
   * @access public
   * @param integer $piType Tipo da licença
   * @return boolean True se for do tipo SOX
   */
  public static function isSOXLicense($piType) {
    return in_array($piType, array(SAASActivation::SUBSCRIPTION_SOX, SAASActivation::TRIAL_SOX, SAASActivation::SUBSCRIPTION_MINIMARISK, SAASActivation::TRIAL_MINIMARISK)) ? true : false;
  }
  
  /**
   * Verifica se o código da licença é do PCI.
   * 
   * <p>Método para verificar se o código da licença é do PCI.
   * @access public
   * @param integer $piType Tipo da licença
   * @return boolean True se for do tipo PCI
   */
  public static function isPCILicense($piType) {
    return in_array($piType, array(SAASActivation::SUBSCRIPTION_PCI, SAASActivation::TRIAL_PCI)) ? true : false;
  }
  
  /**
   * verifica se o c�digo da licensa � da minimarisk 
   * @access public
   * @param integer $piType tipo da licensa
   * @return boolean true se for do tipo Minimarisk
   */
   
  public static function isMinimaRiskLicense($piType) {
  	return in_array($piType, array(SAASActivation::SUBSCRIPTION_MINIMARISK, SAASActivation::TRIAL_MINIMARISK)) ? true : false;
  }
  
  /**
   * Verifica se o id do sistema é do EMS.
   * 
   * <p>Método para verificar se o id do sistema é do EMS.
   * @access public
   * @param integer $psSysId Id de sistema
   * @return boolean True se for EMS
   */
  public static function isEMSCode($psSysId) {
    return $psSysId == self::SYSTEM_EMS ? true : false;
  }
  
  /**
   * Verifica se o id do sistema é do ISMS.
   * 
   * <p>Método para verificar se o id do sistema é do ISMS.
   * @access public
   * @param integer $psSysId Id de sistema
   * @return boolean True se for ISMS
   */
  public static function isISMSCode($psSysId) {
    return $psSysId == self::SYSTEM_ISMS ? true : false;
  }

  public static function isBCMSCode($psSysId) {
	  return $psSysId == self::SYSTEM_BCMS ? true : false;
  }

  /**
   * Verifica se o id do sistema é do OHS.
   * 
   * <p>Método para verificar se o id do sistema é do OHS.
   * @access public
   * @param integer $psSysId Id de sistema
   * @return boolean True se for OHS
   */
  public static function isOHSCode($psSysId) {
    return $psSysId == self::SYSTEM_OHS ? true : false;
  }
  
  /**
   * Verifica se o id do sistema é do SOX.
   * 
   * <p>Método para verificar se o id do sistema é do SOX.
   * @access public
   * @param integer $psSysId Id de sistema
   * @return boolean True se for SOX
   */
  public static function isSOXCode($psSysId) {
    return $psSysId == self::SYSTEM_SOX ? true : false;
  }
  
  /**
   * Verifica se o id do sistema é do PCI.
   * 
   * <p>Método para verificar se o id do sistema é do PCI.
   * @access public
   * @param integer $psSysId Id de sistema
   * @return boolean True se for PCI
   */
  public static function isPCICode($psSysId) {
    return $psSysId == self::SYSTEM_PCI ? true : false;
  }
  
  /**
   * verifica se o id do sistema � da Minimarisk
   * <p> m�todo para verificar se � da minimarisk
   *@access public
   *@param integer id do sistema
   *@return boolean True se for Minimarisk
   */
  public static function isMinimaRiskCode($psSysId) {
  	return $psSysId == self::SYSTEM_MINIMA ? true : false;
  }
  
  /**
   * Verifica se o id do sistema e o tipo de licença são compatíveis.
   * 
   * <p>Método para verificar se o id do sistema e o tipo de licença são compatíveis.
   * @access public
   * @param string $psSysId Id de sistema
   * @param integer $piLicenseType Tipo da licença
   * @return boolean True se for compatível
   */
  public static function isSystemIdAndLicenseTypeCompatible($psSysId, $piLicenseType) {
    $mbIsOk = false;
    switch ($psSysId) {
      case self::SYSTEM_ISMS:
        $mbIsOk = self::isISMSLicense($piLicenseType);
      break;
      case self::SYSTEM_BCMS:
	$mbIsOk = self::isBCMSLicense($piLicenseType);
      break;
      case self::SYSTEM_EMS:
        $mbIsOk = self::isEMSLicense($piLicenseType);
      break;
      case self::SYSTEM_OHS:
        $mbIsOk = self::isOHSLicense($piLicenseType);
      break;
      case self::SYSTEM_SOX:
        $mbIsOk = self::isSOXLicense($piLicenseType);
      break;
      case self::SYSTEM_PCI:
        $mbIsOk = self::isPCILicense($piLicenseType);
      break;
      case self::SYSTEM_MINIMA:
      	$mbIsOk = self::isMinimaRiskLicense($piLicenseType);
      break;
    }
    return $mbIsOk;
  }
}
?>
