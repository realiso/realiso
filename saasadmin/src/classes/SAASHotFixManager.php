<?php
/**
 * ISMS SAAS - INTERNET SECURITY MANAGEMENT SYSTEM SAAS
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

define("SCRIPT_ACTION_CREATE_DIR",    "CREATE_DIR");
define("SCRIPT_ACTION_CREATE_FILE",   "CREATE_FILE");
define("SCRIPT_ACTION_REMOVE_DIR",    "REMOVE_DIR");
define("SCRIPT_ACTION_REMOVE_FILE",   "REMOVE_FILE");
define("SCRIPT_ACTION_MODIFY_FILE",   "MODIFY_FILE");

/**
 * Classe SAASHotFixManager.
 *
 * <p>Classe que monta o hotfix.</p>
 * @package SAAS
 * @subpackage classes
 */
class SAASHotFixManager {
  
  /**
   * Nome da pasta raiz da vers�o antiga pr�-compilada
   * @var string
   * @access private
   */
  private $csOLDRoot = "";
  
  /**
   * Caminho para a pasta raiz da vers�o nova pr�-compilada
   * @var string
   * @access private
   */
  private $csNEWPath = "";
  
  /**
   * Array associativo dos diret�rio que devem ser criados
   * @var array
   * @access private
   */
  private $caDirectoriesAdded = array();
  
  /**
   * Array associativo dos diret�rio que devem ser removidos
   * @var array
   * @access private
   */
  private $caDirectoriesRemoved = array();
  
  /**
   * Array associativo dos arquivos que devem ser criados
   * @var array
   * @access private
   */
  private $caFilesAdded = array();
  
  /**
   * Array associativo dos arquivos que devem ser removidos
   * @var array
   * @access private
   */
  private $caFilesRemoved = array();
  
  /**
   * Array associativo dos arquivos que devem ser modificados
   * @var array
   * @access private
   */
  private $caFilesModified = array();
      
  /**
   * Diret�rios da vers�o antiga (utilizado no diff de diret�rios)
   * @var array
   * @access private
   */
  private $caOLDAllDirectories = array();
  
  /**
   * Arquivos da vers�o antiga (utilizado no diff de arquivos)
   * @var array
   * @access private
   */
  private $caOLDAllFiles = array();
  
  /**
   * Diret�rios da vers�o nova (utilizado no diff de diret�rios)
   * @var array
   * @access private
   */
  private $caNEWAllDirectories = array();
  
  /**
   * Arquivos da vers�o nova (utilizado no diff de arquivos)
   * @var array
   * @access private
   */
  private $caNEWAllFiles = array();  
  
  /**
   * Construtor.
   * 
   * <p>Construtor da classe SAASHotFixManager.</p>
   * @access public
   */
  public function __construct($psOldPath, $psOldRoot, $psNewPath, $psNewRoot) {
    $this->csOLDPath = $psOldPath;
    $this->csOLDRoot = $psOldRoot;
    
    $this->csNEWPath = $psNewPath;
    $this->csNEWRoot = $psNewRoot;
  }
  
  /**
   * Percorre diret�rios recursivamente, coletando os arquivos.
   * 
   * <p>Percorre diret�rios recursivamente, coletando os arquivos.</p>
   * @access private
   * @param string $psDir Diret�rio sobre o qual ser� realizado o scan
   * @param boolean $pbNEW Indica se o scan est� sendo realizado sobre a vers�o nova
   */
  private function recursiveScan($psDir,$pbNEW=false) {
    if (is_dir($psDir)) {
      if ($dirHandler = opendir($psDir)) {
        while ((($file = readdir($dirHandler)) !== false)) {
          if( ($file[0] == '.') || (is_file($file)) )
            continue;
          $this->recursiveScan($psDir."/".$file,$pbNEW);
        }
        if ($pbNEW)
          $this->caNEWAllFiles = array_filter(array_merge($this->caNEWAllFiles,$this->getFilesFromDir($psDir)));
        else
          $this->caOLDAllFiles = array_filter(array_merge($this->caOLDAllFiles,$this->getFilesFromDir($psDir)));
        closedir($dirHandler);
      }
      if ($pbNEW)
        $this->caNEWAllDirectories[] = $psDir;
      else
        $this->caOLDAllDirectories[] = $psDir;
    }
  }
  
  /**
   * Retorna os arquivos de um diret�rio.
   * 
   * <p>Retorna os arquivos de um diret�rio.</p>
   * @access private
   * @param string $psDir Diret�rio
   * @return array Arquivos do diret�rio
   */
  private function getFilesFromDir($psDir) {
    $maOut = array();
    $maFiles = scandir($psDir);
    foreach($maFiles as $msFileName) {
      if ( ($msFileName!=".") && ($msFileName!="..") && ($msFileName!="Thumbs.db") && (!is_dir($psDir."/".$msFileName)) ) {
        $maOut[] = $psDir."/".$msFileName;
      }
    }
    return $maOut;
  }
  
  /**
   * Popula os arrays associativos do Hot Fix.
   * 
   * <p>Percorre diret�rios recursivamente, coletando todos os arquivos
   * pr�-compilados das vers�es antiga e nova. Descobre as diferen�as entre
   * as vers�es antiga e nova atrav�s da fun��o array_diff_key aplicada aos
   * array associativos referentes � estrura de arquivos e diret�rios das duas
   * ves�es. Popula os arrays associativos referentes �s tarefas que devem ser
   * executadas pelo Hot Fix com informa��es da vers�o nova compilada.</p>
   * @access private
   */
  public function generate() {
    $this->recursiveScan($this->csOLDPath.$this->csOLDRoot,false);
    $this->recursiveScan($this->csNEWPath.$this->csNEWRoot,true);
    
    // create associative arrays (file => path+file)
    $maOLDAllDirectoriesAssoc = array();
    foreach ($this->caOLDAllDirectories as $msOldDir)      
      $maOLDAllDirectoriesAssoc[substr($msOldDir,strlen($this->csOLDPath.$this->csOLDRoot)+1)] = $msOldDir;    
    $this->caOLDAllDirectories = $maOLDAllDirectoriesAssoc;
    
    $maOLDAllFilesAssoc = array();
    foreach ($this->caOLDAllFiles as $msOldFile)
      $maOLDAllFilesAssoc[substr($msOldFile,strlen($this->csOLDPath.$this->csOLDRoot)+1)] = $msOldFile;
    $this->caOLDAllFiles = $maOLDAllFilesAssoc;
    
    $maNEWAllDirectoriesAssoc = array();
    foreach ($this->caNEWAllDirectories as $msNewDir)
      $maNEWAllDirectoriesAssoc[substr($msNewDir,strlen($this->csNEWPath.$this->csNEWRoot)+1)] = $msNewDir;
    $this->caNEWAllDirectories = $maNEWAllDirectoriesAssoc;
    
    $maNEWAllFilesAssoc = array();
    foreach ($this->caNEWAllFiles as $msNewFile)
      $maNEWAllFilesAssoc[substr($msNewFile,strlen($this->csNEWPath.$this->csNEWRoot)+1)] = $msNewFile;
    $this->caNEWAllFiles = $maNEWAllFilesAssoc;       
    
    // create hot fix arrays
    $this->caDirectoriesAdded = array_diff_key($this->caNEWAllDirectories,$this->caOLDAllDirectories);
    $this->caFilesAdded = array_diff_key($this->caNEWAllFiles,$this->caOLDAllFiles);
    $this->caDirectoriesRemoved = array_diff_key($this->caOLDAllDirectories,$this->caNEWAllDirectories);
    $this->caFilesRemoved = array_diff_key($this->caOLDAllFiles,$this->caNEWAllFiles);    
    $maDebugModFiles = array();
    
    foreach ($this->caNEWAllFiles as $msFile => $msFullPathAndFile) {
      if (isset($this->caOLDAllFiles[$msFile])) {    
        $msOldMD5 = md5($a = implode("",file($this->caOLDAllFiles[$msFile])));
        $msNewMD5 = md5($b = implode("",file($this->caNEWAllFiles[$msFile])));
        if ($msOldMD5 != $msNewMD5) {
          $this->caFilesModified[$msFile] = $msFullPathAndFile;
          $maDebugModFiles[$msFile] = array($this->caOLDAllFiles[$msFile]." : ".$msOldMD5,$this->caNEWAllFiles[$msFile]." : ".$msNewMD5);
        }
      }
    }
    
    
    //*/ debug
    //header('Content-Type: html; charset=ISO-8859-1');
    
    /*echo "<h1>OLD STRUCTURE</h1>";
    echo "<h4>Directories</h4>";
    foreach ($this->caOLDAllDirectories as $msKey => $msValue)
      echo "[$msKey] => $msValue<br/>";
    echo "<h4>Files</h4>";
    foreach ($this->caOLDAllFiles as $msKey => $msValue)
      echo "[$msKey] => $msValue<br/>";
    echo "<hr/>";
    
    echo "<h1>NEW STRUCTURE</h1>";
    echo "<h4>Directories</h4>";
    foreach ($this->caNEWAllDirectories as $msKey => $msValue)
      echo "[$msKey] => $msValue<br/>";
    echo "<h4>Files</h4>";
    foreach ($this->caNEWAllFiles as $msKey => $msValue)
      echo "[$msKey] => $msValue<br/>";
    echo "<hr/>";
    */
    
    /*
    echo "<h1>ADDED STRUCTURE</h1>";
    echo "<h4>Directories</h4>";
    foreach ($this->caDirectoriesAdded as $msKey => $msValue)
      echo "[<font color='green'>$msKey</font>] => $msValue<br/>";
    echo "<h4>Files</h4>";
    foreach ($this->caFilesAdded as $msKey => $msValue)
      echo "[<font color='green'>$msKey</font>] => $msValue<br/>";
    echo "<hr/>";
    
    echo "<h1>REMOVED STRUCTURE</h1>";
    echo "<h4>Directories</h4>";
    foreach ($this->caDirectoriesRemoved as $msKey => $msValue)
      echo "[<font color='red'>$msKey</font>] => $msValue<br/>";
    echo "<h4>Files</h4>";
    foreach ($this->caFilesRemoved as $msKey => $msValue)
      echo "[<font color='red'>$msKey</font>] => $msValue<br/>";
    echo "<hr/>";
    
    echo "<h1>MODIFIED FILES</h1>";
    foreach ($this->caFilesModified as $msKey => $msValue) {
      echo "[<font color='blue'>$msKey</font>] => $msValue (newer)<br/>";
      list($msOld,$msNew) = $maDebugModFiles[$msKey];
      echo "<font color='red'>$msOld</font><br/>";
      echo "<font color='green'>$msNew</font><br/><br/>";
    }
    die(1);
    */
    
    //*/
  }
  
  public function getAddedDirectories() {
    return $this->caDirectoriesAdded;    
  }
  
  public function getAddedFiles() {
    return $this->caFilesAdded;    
  }
  
  public function getRemovedDirectories() {
    return $this->caDirectoriesRemoved;    
  }
  
  public function getRemovedFiles() {
    return $this->caFilesRemoved;    
  }
  
  public function getModifiedFiles() {
    return $this->caFilesModified;
  }
}
?>