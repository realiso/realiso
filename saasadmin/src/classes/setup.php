<?php
// REALISO = 0
// MINIMARISK = 1
  define("PROVIDER", 0);
  define("GENERATE_KEYS", 1);

  $classes_ref = $sys_ref  . "classes/";
  $handlers_ref = $sys_ref . "handlers/";
  $handlers_grid_ref = $handlers_ref."grid/";
  
  include_once $sys_ref . "lib/fwd5_setup.php";
  
  include_once $classes_ref . "SAASInstanceManager.php";
  include_once $classes_ref . "SAASHotFixManager.php";
  include_once $classes_ref . "SAASLib.php";
  include_once $classes_ref . "SAASConfig.php";  
  include_once $classes_ref . "SAASInstance.php";
  include_once $classes_ref . "SAASAgenda.php";
  include_once $classes_ref . "SAASActivation.php";
  include_once $classes_ref . "SAASKeyGen.php";
  include_once $classes_ref . "SAASLogger.php";
  include_once $classes_ref . "SAASEmailTemplate.php";
  include_once $classes_ref . "SAASUsers.php";
  
  include_once $handlers_ref . "QueryCurrentActiveInstances.php";
    
  $soWebLib = FWDWebLib::getInstance();
  $soWebLib->setWindowTitle("ISMS SaaS Admin");
  $soWebLib->setSysRef($sys_ref);
  $soWebLib->setCSSRef($sys_ref . "lib/");
  $soWebLib->setGfxRef($sys_ref . "gfx/");
  $soWebLib->setSysJsRef($sys_ref. "lib/");
  
  $moConfig = new FWDConfig($classes_ref . 'config.php');
  if (!$moConfig->load()) trigger_error('error_config_corrupt',E_USER_ERROR);
  
  SAASLib::getInstance()->SAAS_INIT();
  FWDLanguage::getPHPStringValue('mx_shortdate_format', "%d/%m/%Y");
  date_default_timezone_set('America/Sao_Paulo');

?>