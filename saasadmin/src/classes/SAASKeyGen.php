<?php
/**
 * ISMS SAAS - INTERNET SECURITY MANAGEMENT SYSTEM SAAS
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe SAASKeyGen.
 *
 * <p>Classe que trata dos c�digos de ativa��o dos SaaS.</p>
 * @package SAAS
 * @subpackage classes
 */
class SAASKeyGen {

  protected static $csPrivateKey = "-----BEGIN RSA PRIVATE KEY-----
MIICXAIBAAKBgQCzKZ+FEPy6DbvFIOQ6hp6pCnth0kr5FFSh39pMsq0ytv4RRAxn
WxjPvyfx0w7vpnKbARF8zDaStN5KJIxocA7SYmYM2ItYZdAu+6JU1m9/EYT8eb7w
110cYdEIJJr+CPrx2iivXVYBbl9JVkKlf/1xFnkwhIlZ6CYAwFfnxdIswwIDAQAB
AoGAZmVTlmmvB2bx+ihiSFDIPTSQb8gQsWq9NAcMbOlxs5qCeru5mxilhZZq7fsC
flVTCuQBaqIMTqywnw9kcAwba42j8v1rTfM95tXqLSUigQO5QvEQ6YqKOG9wDIz9
4C52w9mbHzwBUQbKrvZ8tL/4rtVuFsVhJJQ0s8FAqm1j+wECQQDWklvP5BHLLkEW
cPAbXw/8ZYJD1GqQ9YsRIBl+EqWsgYy5aPZ4RCrh0c3CaZ/sHwFOE9CKNlJb6I36
yWRPerBZAkEA1cEYzWUJ0AaviW/sceh6MbQnk7diHA74Qva6ZikdxaBPQSAA6Tit
7ESA7YxlgS/OD0dwB4CP/Hvx6S0blpzCewJBALuZnQInAlOlbizDs3oK5DwlN/47
8qwoslzXttIeVZF8duAIYC2IVAG54G8Q0EyuUwLDmjmtAtbufWv9tmEzAIkCQCmv
r9OWqdQ9CYzHHBiW2wXIeZNwRxzrunTswytbR2gcPHiZ8jOJjzmtnms1XzJTjV8j
cnC0HOCDl4j++AtmZPMCQBZswfRiZYlHLrgzm+SRfwDfNzeLR9qmMPgsdPO2dx/y
pSQ9YXv6lnY2yTQ1Qtt+ePB4IYOFM9N6XCXDvxdZio0=
-----END RSA PRIVATE KEY----- ";

 /**
  * Construtor.
  *
  * <p>Construtor da classe SAASKeyGen.</p>
  * @access public
  */
  private function __construct(){}

  protected static function encodeInt($piInt){
    return chr(($piInt >> 24) & 0xFF)
          .chr(($piInt >> 16) & 0xFF)
          .chr(($piInt >>  8) & 0xFF)
          .chr( $piInt        & 0xFF);
  }

  protected static function decodeInt($psInt){
    return  (ord($psInt[0]) << 24)
          | (ord($psInt[1]) << 16)
          | (ord($psInt[2]) <<  8)
          |  ord($psInt[3]);
  }

  protected static function encodeData($piLicenseType,$piTimestamp,$piId){
    return chr($piLicenseType).self::encodeInt($piTimestamp).self::encodeInt($piId);
  }

  protected static function generateKeyFromEncodedData($psData){
    $mrKeyId = openssl_get_privatekey(self::$csPrivateKey);
    $msSignature = '';
    openssl_sign($psData,$msSignature,$mrKeyId);
    openssl_free_key($mrKeyId);
    $msHash = md5($msSignature,true);
    return substr(base64_encode($psData.$msHash),0,34);
  }

  public static function generateKey($piLicenseType,$piTimestamp,$piId){
    $msData = self::encodeData($piLicenseType,$piTimestamp,$piId);
    return self::generateKeyFromEncodedData($msData);
  }

  public static function keyIsValid($psKey){
    if(strlen($psKey)!=34) return false;
    $msKey = base64_decode("{$psKey}==");
    $msData = substr($msKey,0,9);
    return ($psKey==self::generateKeyFromEncodedData($msData));
  }

  public static function extractData($psKey){
    if(strlen($psKey)!=34) return false;
    $msKey = base64_decode("{$psKey}==");
    $msData = substr($msKey,0,9);
    if($psKey==self::generateKeyFromEncodedData($msData)){
      return array(
        'license_type' => ord($msData[0]),
        'timestamp' => self::decodeInt(substr($msData,1,4)),
        'id' => self::decodeInt(substr($msData,5,4)),
      );
    }else{
      return false;
    }
  }
}

?>