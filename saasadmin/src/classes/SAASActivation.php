<?php
/**
 * ISMS SAAS - INTERNET SECURITY MANAGEMENT SYSTEM SAAS
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe SAASActivation.
 *
 * <p>Classe que representa uma ativa��o.</p>
 * @package SAAS
 * @subpackage classes
 */
class SAASActivation extends FWDDBTable {

  const USER_PACK = 2;
  
  const SUBSCRIPTION_ISMS = 1;
  const TRIAL_ISMS = 3;
  
  const SUBSCRIPTION_EMS = 4;
  const TRIAL_EMS = 5;
  
  const SUBSCRIPTION_OHS = 6;
  const TRIAL_OHS = 7;
  
  const SUBSCRIPTION_SOX = 8;
  const TRIAL_SOX = 9;
  
  const SUBSCRIPTION_PCI = 10;
  const TRIAL_PCI = 11;
  
  const SUBSCRIPTION_MINIMARISK = 12;
  const TRIAL_MINIMARISK = 13;
  
  const SUBSCRIPTION_BCMS = 14;
  const TRIAL_BCMS = 15;
  
 /**
  * Construtor.
  *
  * <p>Construtor da classe SAASInstance.</p>
  * @access public
  */
  public function __construct(){
    parent::__construct('saas_activation');
        
    $this->csAliasId = 'activation_id';    
    $this->coDataset->addFWDDBField(new FWDDBField("pkId",          "activation_id",                DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("fkInstance",    "activation_instance_id",       DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("sCode",         "activation_code",              DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField("nType",         "activation_type",              DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("dDateCreated",  "activation_date_created",      DB_DATETIME));
    $this->coDataset->addFWDDBField(new FWDDBField("dDateUsed",     "activation_date_used",         DB_DATETIME));
  }

 /**
  * M�todo que busca uma inst�ncia baseado em seu id.
  * 
  * <p>M�todo que busca uma inst�ncia baseado em seu id.</p>
  * @access public 
  * @param integer $piInstanceId Id da inst�ncia
  * @return boolean True ou False.
  */ 
  public function fetchById($piId) {
    $this->createFilter($piId,$this->csAliasId);
    $this->select();
    return $this->fetch();
  }
 
 /**
  * M�todo para inserir um c�digo de ativa��o.
  * 
  * <p>M�todo para inserir um c�digo de ativa��o.</p>
  * @access public 
  * @param integer $piActivationType Tipo do c�digo de ativa��o
  * @return string C�digo de ativa��o
  */  
  public static function insertActivation($piActivationType) {
    /*
     * Carrega id atual.
     */
    $moConfig = new SAASConfig();
    $moConfig->fetchById(CFG_CURRENT_ID);
    $miNewId = $moConfig->getFieldValue('config_value') + 1;
    
    /*
     * Insere ativa��o e pega o id da chave inserida.
     */
    $moInsActivation = new SAASActivation();
    $msCode = SAASKeyGen::generateKey($piActivationType,SAASLib::SAASTime(),$miNewId);
    $moInsActivation->setFieldValue('activation_code', $msCode);
    $moInsActivation->setFieldValue('activation_type', $piActivationType);
    $moInsActivation->setFieldValue('activation_date_created', SAASLib::SAASTime());
    $moInsActivation->insert();
    
    /*
     * Atualiza id atual.
     */
    $moUpdateConfig = new SAASConfig();
    $moUpdateConfig->setConfig(CFG_CURRENT_ID, $miNewId);
    
    return $msCode;
  }
  
 /**
  * M�todo para validar um c�digo de ativa��o.
  * 
  * <p>M�todo para validar um c�digo de ativa��o.</p>
  * @access public 
  * @param integer $psCode C�digo de ativa��o
  * @return boolean True ou False.
  */
  public static function isValidActivationCode($psCode, $piType) {
    /*
     * Validar o c�digo se:
     *  - for v�lido
     *  - existir na tabela saas_activation
     *  - n�o tiver sido utilizado
     *  - n�o estiver na fila de espera para cria��o (saas_agenda)
     */
    $maData = SAASKeyGen::extractData($psCode);
    $mbValidActivationCode = $maData!==false;
    $mbUsed = false;
    $moActivation = new SAASActivation();
    $moActivation->createFilter($psCode, 'activation_code');
    $moActivation->select();
    $mbExists = $moActivation->fetch() ? true : false;
    $mbUsed = $moActivation->getFieldValue('activation_date_used') ? true : false;
    if ($piType != SAASActivation::USER_PACK && !$mbUsed) {    
      $moAgenda = new SAASAgenda();
      $moAgenda->createFilter($psCode, 'agenda_activation_code');
      $moAgenda->select();
      $mbUsed = $mbUsed || $moAgenda->fetch();
    }
    $msType = $maData['license_type'];
    
    if ($mbValidActivationCode && ($msType == $piType) && !$mbUsed && $mbExists) return true;
    else return false;
  }
}
?>
