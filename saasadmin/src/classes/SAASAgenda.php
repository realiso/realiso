<?php
/**
 * ISMS SAAS - INTERNET SECURITY MANAGEMENT SYSTEM SAAS
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe SAASAgenda.
 *
 * <p>Classe que representa uma agenda.</p>
 * @package SAAS
 * @subpackage classes
 */
class SAASAgenda extends FWDDBTable {

 /**
  * Construtor.
  *
  * <p>Construtor da classe SAASInstance.</p>
  * @access public
  */
  public function __construct(){
    parent::__construct('saas_agenda');
    
    $this->csAliasId = 'agenda_id';
    
    $this->coDataset->addFWDDBField(new FWDDBField('pkId',              'agenda_id',              DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('sAlias',            'agenda_alias',           DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField('sEmail',            'agenda_email',           DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField('sPassword',         'agenda_password',        DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField('sActivationCode',   'agenda_activation_code', DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField('dDate',             'agenda_date',            DB_DATETIME));
  }

 /**
  * M�todo que busca uma inst�ncia baseado em seu id.
  * 
  * <p>M�todo que busca uma inst�ncia baseado em seu id.</p>
  * @access public 
  * @param integer $piInstanceId Id da inst�ncia
  * @return boolean True ou False.
  */ 
  public function fetchById($piInstanceId) {
    $this->createFilter($piInstanceId,$this->csAliasId);
    $this->select();
    return $this->fetch();
  }
  
 /**
  * M�todo que busca uma inst�ncia baseado em seu alias.
  * 
  * <p>M�todo que busca uma inst�ncia baseado em seu alias.</p>
  * @access public 
  * @param string $psInstanceAlias Alias da inst�ncia
  * @return boolean True ou False.
  */ 
  public function fetchByAlias($psInstanceAlias) {
    $this->createFilter($psInstanceAlias,'agenda_alias');
    $this->select();
    return $this->fetch();
  }
  
  /**
  * M�todo que busca uma inst�ncia baseado em seu email.
  * 
  * <p>M�todo que busca uma inst�ncia baseado em seu email.</p>
  * @access public 
  * @param string $psInstanceEmail Email da inst�ncia
  * @return boolean True ou False.
  */ 
  public function fetchByEmail($psInstanceEmail) {
    $this->createFilter($psInstanceEmail,'agenda_email');
    $this->select();
    return $this->fetch();
  }
}
?>