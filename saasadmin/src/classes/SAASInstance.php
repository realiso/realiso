<?php
/**
 * ISMS SAAS - INTERNET SECURITY MANAGEMENT SYSTEM SAAS
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe SAASInstance.
 *
 * <p>Classe que representa uma inst�ncia.</p>
 * @package SAAS
 * @subpackage classes
 */
class SAASInstance extends FWDDBTable {

  const SAAS_REPORT_EMAIL = 711701;
  const LICENSE_CLIENT_NAME = 711703;
  const LICENSE_MODULES = 711704;
  const LICENSE_MAX_USERS = 711705;
  const LICENSE_EXPIRACY = 711706;
  const LICENSE_MAX_SIMULTANEOUS_USERS = 711707;
  const LICENSE_HASH = 711708;
  const SYSTEM_IS_ACTIVE = 711709;
  const LICENSE_TYPE = 711710;
  const LICENSE_ACTIVATION_DATE = 711712;
  const EMAIL_ADMIN = 711713;
  const CONFIG_PATH = 711714;
  const MAX_UPLOAD_FILE_SIZE = 711715;
  
  const DATA_COLLECTION_ENABLED = 421;
  const DEFAULT_EMAIL_SENDER = 415;
  const DEFAULT_EMAIL_SENDER_NAME = 430;
  
  const ERROR_REPORT_EMAIL = 7209;
  
  const LICENSE_TYPE_TRIAL_ISMS = 5801;
  const LICENSE_TYPE_SUBSCRIPTION_ISMS = 5802;
  const LICENSE_TYPE_TRIAL_EMS = 5803;
  const LICENSE_TYPE_SUBSCRIPTION_EMS = 5804;
  const LICENSE_TYPE_TRIAL_OHS = 5805;
  const LICENSE_TYPE_SUBSCRIPTION_OHS = 5806;
  const LICENSE_TYPE_TRIAL_SOX = 5807;
  const LICENSE_TYPE_SUBSCRIPTION_SOX = 5808;
  const LICENSE_TYPE_TRIAL_PCI = 5809;
  const LICENSE_TYPE_SUBSCRIPTION_PCI = 5810;
  const LICENSE_TYPE_TRIAL_SOX_MINIMA = 5811;
  const LICENSE_TYPE_SUBSCRIPTION_SOX_MINIMA = 5812;
  const LICENSE_TYPE_TRIAL_BCMS = 5815;
  const LICENSE_TYPE_SUBSCRIPTION_BCMS = 5816;
 /**
  * Construtor.
  *
  * <p>Construtor da classe SAASInstance.</p>
  * @access public
  */
  public function __construct(){
    parent::__construct('saas_instance');
    
    $this->csAliasId = 'instance_id';
    
    $this->coDataset->addFWDDBField(new FWDDBField('pkInstance',        'instance_id',              DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('sAlias',            'instance_alias',           DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField('dDateCreated',      'instance_creation_date',   DB_DATETIME));
    $this->coDataset->addFWDDBField(new FWDDBField('sDatabaseName',     'instance_db_name',         DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField('sDatabaseUser',     'instance_db_user',         DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField('sDatabaseUserPass', 'instance_db_user_pass',    DB_STRING));
  }

 /**
  * M�todo que busca uma inst�ncia baseado em seu id.
  * 
  * <p>M�todo que busca uma inst�ncia baseado em seu id.</p>
  * @access public 
  * @param integer $piInstanceId Id da inst�ncia
  * @return boolean True ou False.
  */ 
  public function fetchById($piInstanceId) {
    $this->createFilter($piInstanceId,$this->csAliasId);
    $this->select();
    return $this->fetch();
  }
  
 /**
  * M�todo que busca uma inst�ncia baseado em seu alias.
  * 
  * <p>M�todo que busca uma inst�ncia baseado em seu alias.</p>
  * @access public 
  * @param string $psAlias Alias
  * @return boolean True ou False.
  */ 
  public function fetchByAlias($psAlias) {
    $this->createFilter($psAlias,'instance_alias');
    $this->select();
    return $this->fetch();
  }
  
 /**
  * M�todo que atualiza uma inst�ncia baseado em seu id.
  * 
  * <p>M�todo que atualiza uma inst�ncia baseado em seu id.</p>
  * @access public 
  * @param integer $piInstanceId Id da inst�ncia
  * @return boolean True ou False.
  */
  public function updateById($piInstanceId) {
    $this->createFilter($piInstanceId,$this->csAliasId);    
    return parent::update();
  }
 
 /**
  * M�todo que valida um alias.
  * 
  * <p>M�todo que valida um alias.</p>
  * @access public 
  * @param integer $psAlias Alias
  * @return boolean True ou False.
  */ 
  public static function isValidAlias($psAlias) {
    if (!preg_match('/^[a-z][0-9a-z]*$/', $psAlias)) return false;
    else return true;
  }
  
 /**
  * M�todo que verifica se um alias j� foi utilizado.
  * 
  * <p>M�todo que verifica se um alias j� foi utilizado.</p>
  * @access public 
  * @param integer $psAlias Alias
  * @return boolean True ou False.
  */ 
  public static function isAliasUsed($psAlias) {
    $moInstance = new SAASInstance();
    $moAgenda = new SAASAgenda();

    $moConfig = new SAASConfig();
    $moConfig->loadConfig();

    $moDB = new FWDDB(
      DB_POSTGRES,
      $moConfig->getConfig(CFG_DB),
      $moConfig->getConfig(CFG_DB_USER),
      $moConfig->getConfig(CFG_DB_USER_PASS),
      $moConfig->getConfig(CFG_DB_SERVER)
    );
    $moDB->connect();
    $moDB->execute("SELECT datname FROM pg_database WHERE datname = 'inst_$psAlias'");

    if ($moDB->fetch() || $moAgenda->fetchByAlias($psAlias) || $moInstance->fetchByAlias($psAlias)) return true;
    else return false;
  }
}
?>
