<?php
/**
 * ISMS SAAS - INTERNET SECURITY MANAGEMENT SYSTEM SAAS
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe SAASEmailTemplate.
 *
 * <p>Classe que gerencia as mensagens de email.</p>
 * @package SAAS
 * @subpackage classes
 */
class SAASEmailTemplate {

 /**
  * Retorna template do email de welcome.
  * 
  * <p>Retorna template do email de welcome.</p>
  * @access public
  * @param integer $piType Tipo da licen�a
  * @return array Subject/Message
  */
  public static function getWelcomeEmail($piType) {
    $maEmail = array();
    if (in_array($piType, SAASLib::getSubscriptionLicenseTypes()) && SAASLib::isISMSLicense($piType)) { // ISMS SUBSCRIPTION
      $maEmail['subject'] = FWDLanguage::getPHPStringValue('em_isms_login_information_subject', 'Real ISMS Login Information');
      $maEmail['message'] = FWDLanguage::getPHPStringValue('em_isms_login_information_message_subscription', 
          "Dear %user%,<br/><br/>".
          "The power of Information Security Management is now in your hands!<br/><br/>".
          "Below is some important user information. We recommend that you print this email for your records.<br/><br/>".
          "Your user name and temporary password are below. Note that passwords are case sensitive, and your user name is in the form of your email address. Please, change your password when you first log in.<br/><br/>".
          "<b>User Name:</b> %user%<br/>".
          "<b>Password:</b> %pass%<br/><br/>".
          "Go to %link% to log in and to read our \"Get Started\" introduction.<br/><br/>".
          "Just remember, we are counting on feedback from our users to help us make Real ISMS a truly superior ISO 27001 solution, so please, dont't hold back.<br/><br/>".
          "Best regards,<br/>".
          "The Real ISMS team");
    }
    else if (in_array($piType, SAASLib::getTrialLicenseTypes()) && SAASLib::isISMSLicense($piType)) { // ISMS TRIAL
      $maEmail['subject'] = FWDLanguage::getPHPStringValue('em_isms_login_information_subject', 'Real ISMS Login Information');
      $maEmail['message'] = FWDLanguage::getPHPStringValue('em_isms_login_information_message_trial', 
          "Dear %user%,<br/><br/>".
          "Your Real ISMS base is ready!<br/><br/>".
          "We're pleased you've decided to test drive Real ISMS - Information Security Management System.<br/><br/>".
          "Your user name and temporary password are below. Note that passwords are case sensitive, and your user name is in the form of your email address. Please, change your password when you first log in.<br/><br/>".
          "<b>User Name:</b> %user%<br/>".
          "<b>Password:</b> %pass%<br/><br/>".
          "Go to %link% to log in now.<br/><br/>".
          "Just remember, we are counting on feedback from our users to help us make Real ISMS a truly superior ISO 27001 solution, so please, don't hold back.<br/><br/>".
          "Best regards,<br/>".
          "The Real ISMS team");
    }
    else if (in_array($piType, SAASLib::getSubscriptionLicenseTypes()) && SAASLib::isEMSLicense($piType)) { // EMS SUBSCRIPTION
      $maEmail['subject'] = FWDLanguage::getPHPStringValue('em_ems_login_information_subject', 'Informa��es de Login do Real EMS');
      $maEmail['message'] = FWDLanguage::getPHPStringValue('em_ems_login_information_message_subscription', 
          "Prezado %user%,<br/><br/>".
          "A for�a da verdadeira Gest�o Ambiental est� agora em suas m�os!<br/><br/>".
          "Abaixo est�o descritos seu nome de usu�rio e senha de acesso. Tenha aten��o ao digitar sua senha, pois o sistema diferencia letras ma�sculas de letras min�sculas. Seu nome de usu�rio � seu endere�o de e-mail. Por favor, altere sua senha logo no primeiro acesso.<br/><br/>".
          "<b>Nome de Usu�rio:</b> %user%<br/>".
          "<b>Senha de Acesso:</b> %pass%<br/><br/>".
          "V� para o link %link% para acessar o sistema agora.<br/><br/>".
          "Lembre-se, n�s contamos com o feedback de nossos usu�rios para ajudarnos a tornar o Real EMS uma solu��o verdadeiramente superior para ISO 14001, ent�o por favor, n�o deixe de contribuir.<br/><br/>".
          "Atenciosamente,<br/>".
          "Equipe Real EMS");
    }
    else if (in_array($piType, SAASLib::getTrialLicenseTypes()) && SAASLib::isEMSLicense($piType)) { // EMS TRIAL
      $maEmail['subject'] = FWDLanguage::getPHPStringValue('em_ems_login_information_subject', 'Informa��es de Login do Real EMS');
      $maEmail['message'] = FWDLanguage::getPHPStringValue('em_ems_login_information_message_trial', 
        "Prezado %user%,<br/><br/>".
        "Sua base do Real EMS est� pronta!<br/><br/>".
        "N�s gostar�amos de agradec�-lo por ter decido avaliar o Real EMS - Environmental Management System.<br/><br/>".
        "Abaixo est�o descritos seu nome de usu�rio e senha de acesso. Tenha aten��o ao digitar sua senha, pois o sistema diferencia letras ma�sculas de letras min�sculas. Seu nome de usu�rio � seu endere�o de e-mail. Por favor, altere sua senha logo no primeiro acesso.<br/><br/>".
        "<b>Nome de Usu�rio:</b> %user%<br/>".
        "<b>Senha de Acesso:</b> %pass%<br/><br/>".
        "V� para o link %link% para acessar o sistema agora.<br/><br/>".
        "Lembre-se, n�s contamos com o feedback de nossos usu�rios para ajudarnos a tornar o Real EMS uma solu��o verdadeiramente superior para ISO 14001, ent�o por favor, n�o deixe de contribuir.<br/><br/>".
        "Atenciosamente,<br/>".
        "Equipe Real EMS");
    }
    else if (in_array($piType, SAASLib::getSubscriptionLicenseTypes()) && SAASLib::isOHSLicense($piType)) { // OHS SUBSCRIPTION
      $maEmail['subject'] = FWDLanguage::getPHPStringValue('em_ohs_login_information_subject', 'Informa��es de Login do Real OHS');
      $maEmail['message'] = FWDLanguage::getPHPStringValue('em_ohs_login_information_message_subscription', 
        "Prezado %user%,<br/><br/>".
        "A for�a da verdadeira Gest�o da Sa�de e Seguran�a est� agora em suas m�os!<br/><br/>".
        "Abaixo est�o descritos seu nome de usu�rio e senha de acesso. Tenha aten��o ao digitar sua senha, pois o sistema diferencia letras ma�sculas de letras min�sculas. Seu nome de usu�rio � seu endere�o de e-mail. Por favor, altere sua senha logo no primeiro acesso.<br/><br/>".
        "<b>Nome de Usu�rio:</b> %user%<br/>".
        "<b>Senha de Acesso:</b> %pass%<br/><br/>".
        "V� para o link %link% para acessar o sistema agora.<br/><br/>".
        "Lembre-se, n�s contamos com o feedback de nossos usu�rios para ajudarnos a tornar o Real OHS uma solu��o verdadeiramente superior para OHSAS 18001, ent�o por favor, n�o deixe de contribuir.<br/><br/>".
        "Atenciosamente,<br/>".
        "Equipe Real OHS");
    }
    else if (in_array($piType, SAASLib::getTrialLicenseTypes()) && SAASLib::isOHSLicense($piType)) { // OHS TRIAL
      $maEmail['subject'] = FWDLanguage::getPHPStringValue('em_ohs_login_information_subject', 'Informa��es de Login do Real OHS');
      $maEmail['message'] = FWDLanguage::getPHPStringValue('em_ohs_login_information_message_trial', 
        "Prezado %user%,<br/><br/>".
        "Sua base do Real OHS est� pronta!<br/><br/>".
        "N�s gostar�amos de agradec�-lo por ter decido avaliar o Real OHS - Occupational Health and Safety Management System.<br/><br/>".
        "Abaixo est�o descritos seu nome de usu�rio e senha de acesso. Tenha aten��o ao digitar sua senha, pois o sistema diferencia letras ma�sculas de letras min�sculas. Seu nome de usu�rio � seu endere�o de e-mail. Por favor, altere sua senha logo no primeiro acesso.<br/><br/>".
        "<b>Nome de Usu�rio:</b> %user%<br/>".
        "<b>Senha de Acesso:</b> %pass%<br/><br/>".
        "V� para o link %link% para acessar o sistema agora.<br/><br/>".
        "Lembre-se, n�s contamos com o feedback de nossos usu�rios para ajudarnos a tornar o Real OHS uma solu��o verdadeiramente superior para OHSAS 18001, ent�o por favor, n�o deixe de contribuir.<br/><br/>".
        "Atenciosamente,<br/>".
        "Equipe Real OHS");
    }
    else if (in_array($piType, SAASLib::getSubscriptionLicenseTypes()) && SAASLib::isSOXLicense($piType)) { // SOX SUBSCRIPTION
      $maEmail['subject'] = FWDLanguage::getPHPStringValue('em_sox_login_information_subject', 'Informa��es de Login do Real SOX');
      $maEmail['message'] = FWDLanguage::getPHPStringValue('em_sox_login_information_message_subscription', 
        "Dear %user%,<br/><br/>".
        "The power of SOX Management is now in your hands!<br/><br/>".
        "Below is some important user information. We recommend that you print this email for your records.<br/><br/>".
        "Your user name and temporary password are below. Note that passwords are case sensitive, and your user name is in the form of your email address. Please, change your password when you first log in.<br/><br/>".
        "<b>User Name:</b> %user%<br/>".
        "<b>Password:</b> %pass%<br/><br/>".
        "Go to %link% to log in and to read our \"Get Started\" introduction.<br/><br/>".
        "Just remember, we are counting on feedback from our users to help us make Real SOX a truly superior SOX solution, so please, dont't hold back.<br/><br/>".
        "Best regards,<br/>".
        "The Real SOX team");
    }
    else if (in_array($piType, SAASLib::getTrialLicenseTypes()) && SAASLib::isSOXLicense($piType)) { // SOX TRIAL
      $maEmail['subject'] = FWDLanguage::getPHPStringValue('em_sox_login_information_subject', 'Informa��es de Login do Real SOX');
      $maEmail['message'] = FWDLanguage::getPHPStringValue('em_sox_login_information_message_trial', 
        "Dear %user%,<br/><br/>".
        "Your Real SOX base is ready!<br/><br/>".
        "We're pleased you've decided to test drive Real SOX - Sarbanes Oxley Management System.<br/><br/>".
        "Your user name and temporary password are below. Note that passwords are case sensitive, and your user name is in the form of your email address. Please, change your password when you first log in.<br/><br/>".
        "<b>User Name:</b> %user%<br/>".
        "<b>Password:</b> %pass%<br/><br/>".
        "Go to %link% to log in now.<br/><br/>".
        "Just remember, we are counting on feedback from our users to help us make Real SOX a truly superior SOX solution, so please, don't hold back.<br/><br/>".
        "Best regards,<br/>".
        "The Realiso team");
    }
    else if (in_array($piType, SAASLib::getSubscriptionLicenseTypes()) && SAASLib::isPCILicense($piType)) { // PCI SUBSCRIPTION
      $maEmail['subject'] = FWDLanguage::getPHPStringValue('em_pci_login_information_subject', 'Informa��es de Login do Real PCI');
      $maEmail['message'] = FWDLanguage::getPHPStringValue('em_pci_login_information_message_subscription', 
        "Dear %user%,<br/><br/>".
        "The power of PCI Management is now in your hands!<br/><br/>".
        "Below is some important user information. We recommend that you print this email for your records.<br/><br/>".
        "Your user name and temporary password are below. Note that passwords are case sensitive, and your user name is in the form of your email address. Please, change your password when you first log in.<br/><br/>".
        "<b>User Name:</b> %user%<br/>".
        "<b>Password:</b> %pass%<br/><br/>".
        "Go to %link% to log in and to read our \"Get Started\" introduction.<br/><br/>".
        "Just remember, we are counting on feedback from our users to help us make Real PCI a truly superior PCI solution, so please, dont't hold back.<br/><br/>".
        "Best regards,<br/>".
        "The Real PCI team");
    }
    else if (in_array($piType, SAASLib::getTrialLicenseTypes()) && SAASLib::isPCILicense($piType)) { // PCI TRIAL
      $maEmail['subject'] = FWDLanguage::getPHPStringValue('em_pci_login_information_subject', 'Informa��es de Login do Real PCI');
      $maEmail['message'] = FWDLanguage::getPHPStringValue('em_pci_login_information_message_trial', 
        "Dear %user%,<br/><br/>".
        "Your Real PCI base is ready!<br/><br/>".
        "We're pleased you've decided to test drive Real PCI - Payment Card Industry Management System.<br/><br/>".
        "Your user name and temporary password are below. Note that passwords are case sensitive, and your user name is in the form of your email address. Please, change your password when you first log in.<br/><br/>".
        "<b>User Name:</b> %user%<br/>".
        "<b>Password:</b> %pass%<br/><br/>".
        "Go to %link% to log in now.<br/><br/>".
        "Just remember, we are counting on feedback from our users to help us make Real PCI a truly superior PCI solution, so please, don't hold back.<br/><br/>".
        "Best regards,<br/>".
        "The Realiso team");
    }
    return $maEmail;
  }

  /**
  * Retorna template do email de getting started.
  * 
  * <p>Retorna template do email de getting started.</p>
  * @access public
  * @param integer $piType Tipo da licen�a
  * @return array Subject/Message
  */
  public static function getGettingStartedEmail($piType) {
    $maEmail = array();
    if (SAASLib::isISMSLicense($piType)) { // ISMS
      $maEmail['subject'] = FWDLanguage::getPHPStringValue('em_isms_instructions_subject', 'Getting Started with Real ISMS');
      $maEmail['message'] = FWDLanguage::getPHPStringValue('em_isms_instructions_message', 
        "Dear %user%,<br/>
        <br/>
        Welcome to the Real ISMS world! This email provides important instructions for your first steps on using Real ISMS.<br/>
        <br/>
        <b>Getting Started</b><br/>
        <br/>
        <b>Real ISMS</b> makes it easy to implement & manage your ISO 27001 certification.<br/>
        <br/>
        As soon as you log in to Real ISMS you'll be able to analyze a sample corporation called Sample S.A. This corporation is already set up with areas, processes, assets, risks, controls, policies and incidents. In the dashboard you'll have an overview of all Sample S.A. risks and controls. Create a Statement of Applicability in a Snap! Real ISMS reports include everything you need to stay ready for ISO 27001 requirements.<br/>
        <br/>
        To add a new area, process, asset or risk, go to <b>Risk Management</b> next to the <b>Dashboard</b>. Once you have your risk list(s) created, Real ISMS offers you a library to assist you on treating them. ISO 27002, PCI-DSS and CobIT control objectives are already included in this library set.<br/>
        <br/>
        <b>The \"risk events\" available in the Trial version are limited due to evaluation limitations. The full Real ISMS version contains over one thousand risks. You will only find risks on the top most categories. It is possible to add new risks in any category or sub-category.</b><br/>
        <br/>
        <b>Resources</b><br/>
        <br/>
        If you have any trouble or need assistance with Real ISMS, be sure to send us a feedback through Real ISMS, clicking on \"feedback\" button.<br/>
        <br/>
        Thank you for choosing Real ISMS!<br/>
        <br/>
        Our kindly regards,<br/>
        Realiso Team<br/>
        <br/>
        <br/>
        http://www.realiso.com/realisms<br/>");
    }
    else if (SAASLib::isEMSLicense($piType)) { // EMS
      $maEmail['subject'] = FWDLanguage::getPHPStringValue('em_ems_instructions_subject', 'Bem vindo ao Real EMS');
      $maEmail['message'] = FWDLanguage::getPHPStringValue('em_ems_instructions_message', 
        "Prezado %user%,<br/>
        <br/>
        Bem vindo ao mundo do Real EMS! Este e-mail fornece instru��es importantes para seus primeiros passos no uso do Real EMS.<br/>
        <br/>
        <b>Come�ando</b><br/>
        <br/>
        O <b>Real EMS</b> torna simples a tarefa de implementar e gerenciar seu sistema de gest�o ambiental.<br/>
        <br/>
        Para adicionar novas �reas, processos, ambientes ou riscos, v� para <b>Gest�o de Riscos</b> pr�xima a <b>Dashboard</b>. 
        <br/>
        <br/>
        <b>Recursos</b><br/>
        <br/>
        Se voc� tiver algum problema ou precisar de assist�ncia com o Real EMS, por favor, envie-nos um feedback atrav�s do Real EMS, clicando no bot�o \"feedback\".<br/>
        <br/>
        Obrigado por escolher o Real EMS!<br/>
        <br/>
        Atenciosamente,<br/>
        Realiso Team<br/>
        <br/>
        <br/>
        http://www.realiso.com/realems<br/>");
    }
    else if (SAASLib::isOHSLicense($piType)) { // OHS
      $maEmail['subject'] = FWDLanguage::getPHPStringValue('em_ohs_instructions_subject', 'Bem vindo ao Real OHS');
      $maEmail['message'] = FWDLanguage::getPHPStringValue('em_ohs_instructions_message', 
        "Prezado %user%,<br/>
        <br/>
        Bem vindo ao mundo do Real OHS! Este e-mail fornece instru��es importantes para seus primeiros passos no uso do Real OHS.<br/>
        <br/>
        <b>Come�ando</b><br/>
        <br/>
        <b>O Real OHS</b> torna simples a tarefa de implementar e gerenciar seu sistema de gest�o da sa�de ocupacional e seguran�a.<br/>
        <br/>
        Para adicionar novas �reas, processos, ou perigos, v� para <b>Gest�o de Riscos</b> pr�xima a <b>Dashboard</b>. 
        <br/>
        <br/>
        <b>Recursos</b><br/>
        <br/>
        Se voc� tiver algum problema ou precisar de assist�ncia com o Real OHS, por favor, envie-nos um feedback atrav�s do Real OHS, clicando no bot�o \"feedback\".<br/>
        <br/>
        Obrigado por escolher o Real OHS!<br/>
        <br/>
        Atenciosamente,<br/>
        Realiso Team<br/>
        <br/>
        <br/>
        http://www.realiso.com/realohs<br/>");
    }
    else if (SAASLib::isSOXLicense($piType)) { // SOX
      $maEmail['subject'] = FWDLanguage::getPHPStringValue('em_sox_instructions_subject', 'Getting Started with Real SOX');
      $maEmail['message'] = FWDLanguage::getPHPStringValue('em_sox_instructions_message', 
        "Dear %user%,<br/>
        <br/>
        Welcome to the Real SOX world! This email provides important instructions for your first steps on using Real SOX.<br/>
        <br/>
        <b>Getting Started</b><br/>
        <br/>
        <b>Real SOX</b> makes it easy to implement & manage your SOX implementation.<br/>
        <br/>
        As soon as you log in to Real SOX you'll be able to analyze a sample corporation called Sample S.A. This corporation is already set up with business units, departments, processes, risks and internal controls. In the dashboard you'll have an overview of all Sample S.A. risks and controls. Real SOX reports include everything you need to be ready for SOX requirements.<br/>
        <br/>
        To add a new business unit, department, process or risk, go to <b>Risk Management</b> next to the <b>Dashboard</b>. Once you have your risk list(s) created, Real SOX offers you a library to assist you on treating them. All SOX control objectives are already included in this library set.<br/>
        <br/>
        <b>Resources</b><br/>
        <br/>
        If you have any trouble or need assistance with Real SOX, be sure to send us a feedback through Real SOX, clicking on \"feedback\" button.<br/>
        <br/>
        Thank you for choosing Real SOX!<br/>
        <br/>
        Our kindly regards,<br/>
        Realiso Team<br/>
        <br/>
        <br/>
        http://www.realiso.com/realsox<br/>");
    }
    else if (SAASLib::isPCILicense($piType)) { // PCI
      $maEmail['subject'] = FWDLanguage::getPHPStringValue('em_pci_instructions_subject', 'Getting Started with Real PCI');
      $maEmail['message'] = FWDLanguage::getPHPStringValue('em_pci_instructions_message', 
        "Dear %user%,<br/>
        <br/>
        Welcome to the Real PCI world! This email provides important instructions for your first steps on using Real PCI.<br/>
        <br/>
        <b>Getting Started</b><br/>
        <br/>
        <b>Real PCI</b> makes it easy to implement & manage your PCI implementation.<br/>
        <br/>
        As soon as you log in to Real PCI you'll be able to analyze a sample corporation called Sample S.A. This corporation is already set up with areas, processes, assets, risks and controls. In the dashboard you'll have an overview of all Sample S.A. risks and controls. Real PCI reports include everything you need to be ready for PCI requirements.<br/>
        <br/>
        To add a new area, process, asset or risk, go to <b>Risk Assessment</b> next to the <b>Dashboard</b>. Once you have your risk list(s) created, Real PCI offers you a library to assist you on treating them. All PCI-DSS control objectives are already included in this library set.<br/>
        <br/>
        <b>Resources</b><br/>
        <br/>
        If you have any trouble or need assistance with Real PCI, be sure to send us a feedback through Real PCI, clicking on \"feedback\" button.<br/>
        <br/>
        Thank you for choosing Real PCI!<br/>
        <br/>
        Our kindly regards,<br/>
        Realiso Team<br/>
        <br/>
        <br/>
        http://www.realiso.com/realpci<br/>");
    }
    return $maEmail;
  }
  
}
?>