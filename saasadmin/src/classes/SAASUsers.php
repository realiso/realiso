<?php
/**
 * ISMS SAAS - INTERNET SECURITY MANAGEMENT SYSTEM SAAS
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */


/**
 * Classe SAASUsers.
 *
 * <p>Classe que manipula os usu�rios do sistema SaasAdmin.</p>
 * @package SAAS
 * @subpackage classes
 */
class SAASUser extends FWDDBTable {


 /**
  * Construtor.
  *
  * <p>Construtor da classe SAASUsers. </p>
  * @access public
  */
  public function __construct(){
    parent::__construct("saas_users");
    $this->csAliasId = 'user_id';
    $this->coDataset->addFWDDBField(new FWDDBField("fkContext",  "user_id",    DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("sLogin",    "user_login", DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField("sPassword",    "user_password", DB_STRING));
    $this->caSearchableFields = array('user_login');
  }

 /**
  * M�todo que busca um usu�rio baseado em seu id.
  *
  * <p>M�todo que busca um usu�rio baseado em seu id.</p>
  * @access public
  * @param integer $piConfigeId Id da config
  * @return boolean True ou False.
  */
  public function fetchById($piConfigId) {
    $this->createFilter($piConfigId,$this->csAliasId);
    $this->select();
    return $this->fetch();
  }

/**
  * Valida login do usu�rio
  *
  * <p>M�todo para validar um usu�rio atrav�s de login/senha.</p>
  * @access public
  * @param string $psLogin Login
  * @param string $psPassword Senha
  * @return boolean true se o login � permitido, false se for incorreto.
  */
  public function fetchByLoginPass($psLogin, $psPassword) {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $this->createFilter(FWDWebLib::ConvertToIso($psLogin,true), 'user_login');
    $this->createFilter(FWDWebLib::ConvertToIso($psPassword,true), 'user_password');
    $this->select();
    if ($this->fetch()) { //Login + Senha
    	return true;
    }
    return false;
  }


  /**
   * Verifica se o usu�rio est� logado.
   *
   * <p> M�todo para verificar se o usu�rio est� logado. </p>
   *
   * @access public
   * @return boolean true se estiver logado, false se n�o.
   */
  public function isActive(){
  	if(isset($_SESSION['user_active'])){
  		return $_SESSION['user_active'];
  	}
  	return false;
  }

  /**
   * seta o usu�rio com o status
   *
   *@access public
   *@param boolean $state, true para ativar, false para desativar
   */
  public function setActive($state){
  	$_SESSION['user_active'] = $state;
  }
}
?>