<?php
/**
 * ISMS SAAS - INTERNET SECURITY MANAGEMENT SYSTEM SAAS
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/*
 * Contantes de configuração do banco de dados da parte administrativa.
 */
define("CFG_DB", 3301);
define("CFG_DB_USER", 3302);
define("CFG_DB_USER_PASS", 3303);
define("CFG_DB_SERVER", 3304);
define("CFG_WEB_INSTANCE_PATH", 3305);
define("CFG_WEB_INSTANCE_SRC_PATH", 3306);
define("CFG_AXUR_EMAIL", 3307);
define("CFG_ABANDONMENT_DAYS", 3308);
define("CFG_INSTANCE_VERSION", 3309);
define("CFG_TRIAL_URL", 3310);
define("CFG_AGENDA_LOCKED", 3311);
define("CFG_AGENDA_ENABLED", 3312);
define("CFG_CURRENT_ID", 3313);
define("CFG_AXUR_DEFAULT_SENDER", 3314);
define("CFG_ADMIN_PATH", 3315);
define("CFG_COMMERCIAL_URL", 3316);
define("CFG_BUY_NOW_URL", 3317);
define("CFG_MAX_UPLOAD_FILE_SIZE",3318);
define("CFG_AXUR_DEFAULT_SENDER_NAME", 3319);

/*
 * Constantes de valores padrão das instâncias
 */
define("CFG_DEFAULT_MODULES",3401);
define("CFG_DEFAULT_MAX_USERS",3402);
define("CFG_DEFAULT_MAX_SIM_USERS",3403);
define("CFG_DEFAULT_EXPIRACY_PERIOD",3404);

/*
 * Constantes que definem o nome da pasta do sistema fonte
 * e o nome da pasta do sistema que será usado para fazer
 * o hotfix.
 */
define("FOLDER_SOURCE_SYSTEM", "source");
define("FOLDER_HOTFIX_SYSTEM", "hotfix");
define("FOLDER_SYSTEM_SQLS", "sqls");
define("FOLDER_TRIAL", "evaluation");
define("FOLDER_SUBSCRIPTION", "saas");

/**
 * Classe SAASConfig.
 *
 * <p>Classe que manipula as configurações do sistema.</p>
 * @package SAAS
 * @subpackage classes
 */
class SAASConfig extends FWDDBTable {
  
 /**
  * Array de configurações
  * @var array
  * @access protected
  */  
  protected $caConfig = array();
  
 /**
  * Construtor.
  * 
  * <p>Construtor da classe SAASConfig.</p>
  * @access public 
  */
  public function __construct(){
    parent::__construct("saas_config");

    $this->csAliasId = 'config_id';
    $this->coDataset->addFWDDBField(new FWDDBField("pkConfig",  "config_id",    DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("sValue",    "config_value", DB_STRING));
    
    $this->caConfig = array(      
      CFG_DB => '',
      CFG_DB_USER => '',
      CFG_DB_USER_PASS => '',
      CFG_DB_SERVER => '',
      CFG_WEB_INSTANCE_PATH => '',
      CFG_WEB_INSTANCE_SRC_PATH => '',
      CFG_AXUR_EMAIL => '',
      CFG_ABANDONMENT_DAYS => '',
      CFG_INSTANCE_VERSION => '',
      CFG_TRIAL_URL => '',
      CFG_COMMERCIAL_URL => '',
      CFG_BUY_NOW_URL => '',
      CFG_MAX_UPLOAD_FILE_SIZE => '',
      CFG_AGENDA_LOCKED => '',
      CFG_AGENDA_ENABLED => '',
      CFG_CURRENT_ID => '',
      CFG_AXUR_DEFAULT_SENDER => '',
      CFG_ADMIN_PATH => '',
      CFG_AXUR_DEFAULT_SENDER_NAME => '',
      
      CFG_DEFAULT_MODULES => '',
      CFG_DEFAULT_MAX_USERS => '',
      CFG_DEFAULT_MAX_SIM_USERS => '',
      CFG_DEFAULT_EXPIRACY_PERIOD => '',
    );
  }
   
 /**
  * Carrega as configurações atuais.
  * 
  * <p>Método para carregar as configurações atuais.</p>
  * @access public 
  */
  public function loadConfig() {
    if (count($this->caConfig)) {
      foreach($this->caConfig as $miKey => $msValue) $this->createFilter($miKey, 'config_id', 'in');
      $this->select();    
      while($this->fetch()) $this->caConfig[$this->getFieldValue('config_id')] = $this->getFieldValue('config_value');
      $this->removeFilters('config_id'); 
    }
  }
 
 /**
  * Seta uma determinada configuração.
  * 
  * <p>Método para setar uma determinada configuração.</p>
  * @access public 
  * @param integer $piConfigId Id da configuração
  * @param string $psConfigValue Valor da configuração
  */ 
  public function setConfig($piConfigId, $psConfigValue){
    $this->createFilter($piConfigId, 'config_id');    
    $this->setFieldValue('config_value', $psConfigValue);
    $this->update();
    $this->removeFilters('config_id');
  }
 
 /**
  * Retorna o valor de uma determinada configuração.
  * 
  * <p>Método para retornar o valor de uma determinada configuração.</p>
  * @access public 
  * @param integer $piConfigId Id da configuração
  * @return string Valor da configuração
  */ 
  public function getConfig($piConfigId){
    if(array_key_exists($piConfigId, $this->caConfig)){
      return $this->caConfig[$piConfigId];
    }else{
      trigger_error("Config id not found: $piConfigId", E_USER_WARNING);
    }
  }
  
 /**
  * Método que busca uma config baseado em seu id.
  * 
  * <p>Método que busca uma cofig baseado em seu id.</p>
  * @access public 
  * @param integer $piConfigeId Id da config
  * @return boolean True ou False.
  */ 
  public function fetchById($piConfigId) {
    $this->createFilter($piConfigId,$this->csAliasId);
    $this->select();
    return $this->fetch();
  }
}
?>