<?php
include_once "include.php";
include_once $handlers_ref."QueryLicenseDefaultValues.php";

class SaveEvent extends FWDRunnable {
  public function run() {
    $msModulesController = FWDWebLib::getObject('module_controller')->getValue();
    $maModules = array();    
    if (strpos($msModulesController, "RM")) $maModules[] = "RM";
    if (strpos($msModulesController, "PM")) $maModules[] = "PM";
    if (strpos($msModulesController, "CI")) $maModules[] = "CI";
    $msModules = implode("|", $maModules);

    $moConfig = new SAASConfig();
    $moConfig->setConfig(CFG_DEFAULT_MODULES, $msModules);
    $moConfig->setConfig(CFG_DEFAULT_MAX_USERS, FWDWebLib::getObject('max_users')->getValue());
    $moConfig->setConfig(CFG_DEFAULT_MAX_SIM_USERS, FWDWebLib::getObject('max_simultaneous_users')->getValue());
    $moConfig->setConfig(CFG_DEFAULT_EXPIRACY_PERIOD, FWDWebLib::getObject('expiration_period')->getValue());
    echo "self.close();";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
    $moStartEvent->addAjaxEvent(new SaveEvent('save_event'));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moHandler = new QueryLicenseDefaultValues(FWDWebLib::getConnection());
    $moHandler->makeQuery();
    $maDefaultValues = $moHandler->executeQuery();
    
    foreach($maDefaultValues as $miId => $msValue) {
      switch($miId) {
        case CFG_DEFAULT_MODULES:
          $msValue = str_replace('|', ':', $msValue);
          FWDWebLib::getObject('module_controller')->setValue($msValue);
        break;
        case CFG_DEFAULT_MAX_USERS:
          FWDWebLib::getObject('max_users')->setValue($msValue);
        break;
        case CFG_DEFAULT_MAX_SIM_USERS:
          FWDWebLib::getObject('max_simultaneous_users')->setValue($msValue);
        break;
        case CFG_DEFAULT_EXPIRACY_PERIOD:
          FWDWebLib::getObject('expiration_period')->setValue($msValue);
        break;
        default:
        break;
      }
    }
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_instance_default_values.xml');
?>