<?php
include_once "include.php";
include_once $handlers_ref."QueryIsSystemConfigured.php";
include_once $handlers_ref."QueryGetNextInAgenda.php";

define('AGENDA_LOG_INFO', 1);
define('AGENDA_LOG_WARNING', 2);
define('AGENDA_LOG_ERROR', 3);

/*
 * Fun��o para escrever no log.
 */
function writeLog($prHandle, $psMsg, $piType) {
  $msType = "";
  switch($piType) {    
    case AGENDA_LOG_INFO:
      $msType = "INFO";
    break; 
    case AGENDA_LOG_WARNING:
      $msType = "WARNING";
    break;
    case AGENDA_LOG_ERROR:
      $msType = "ERROR";
    break;
  }
  fwrite($prHandle, "[".SAASLib::getSAASDate(SAASLib::SAASTime(), true)."][$msType] ".$psMsg.PHP_EOL); 
}

/*
 * Sobreescreve error handler.
 */
FWDWebLib::setErrorHandler(new FWDDoNothingErrorHandler());

$gbDebugMode = FWDWebLib::getInstance()->getDebugMode();
$gbDebugType = FWDWebLib::getInstance()->getDebugType();
FWDWebLib::getInstance()->setDebugMode(false,0);

/*
 * Cria arquivo para log.
 */
$grHandle = fopen('agenda_log.txt', 'a');

/*
 * Antes de come�ar a criar as int�ncias que est�o na fila,
 * testa para ver se as configura��es j� foram especificadas.
 */
$gbOk= false;    
$goHandler = new QueryIsSystemConfigured(FWDWebLib::getConnection());
$goHandler->makeQuery();
$gbOk = $goHandler->executeQuery();
if (!$gbOk) {
  writeLog($grHandle, "Could not process agenda because the system is not configured yet", AGENDA_LOG_WARNING);
  exit;  
}

/*
 * Objeto para fazer update nas configura��es.
 */
$goConfigUpdate = new SAASConfig();

/*
 * Carrega as configura��es.
 */
$goConfig = new SAASConfig();
$goConfig->loadConfig();

/*
 * Tenta obter o lock. Se conseguir segue em frente, sen�o encerrra.
 */
$gbEnabled = $goConfig->getConfig(CFG_AGENDA_ENABLED);
$gbLocked = $goConfig->getConfig(CFG_AGENDA_LOCKED);
if ($gbEnabled && !$gbLocked) {
  $goConfigUpdate->setConfig(CFG_AGENDA_LOCKED, '1');
  writeLog($grHandle, "Table Locked", AGENDA_LOG_INFO);
  writeLog($grHandle, "Checking Agenda", AGENDA_LOG_INFO);
  
  $gbStopped = false;
  
  $goNextHandler = new QueryGetNextInAgenda(FWDWebLib::getConnection());
  $goNextHandler->makeQuery();
  $gaNext = $goNextHandler->executeQuery();
    
  while(count($gaNext) && !$gbStopped) {
    /*
     * Recarrega as configura��es.
     */
    $goConfig = new SAASConfig();
    $goConfig->loadConfig();
    $gbEnabled = $goConfig->getConfig(CFG_AGENDA_ENABLED);    
    if ($gbEnabled) {    
      $gsAlias = $gaNext['agenda_alias'];
      
      writeLog($grHandle, "Creating instance $gsAlias", AGENDA_LOG_INFO);
      
      /*
       * Inicializa o manager.
       */
      $goManager = new SAASInstanceManager($goConfig, $gsAlias);
      
      /*
       * Controle de erro.
       */
      $gsError = "";
      
      /*
       * Fun��o respons�vel por criar o usu�rio e o banco de dados
       * que ser� utilizado pela nova inst�ncia.
       */
      if (!$gsError) {
        if (!$goManager->createUserAndDB()) {
          $gsError = $goManager->getLastError();        
        }
      }
      
      /*
       * Fun��o respons�vel por criar a estrutura do banco de dados, ou seja,
       * respons�vel por criar tabelas, fun��es, views, triggers, erc.
       */
      if (!$gsError) {
        $mbTrial = in_array($gaNext['agenda_activation_type'], SAASLib::getTrialLicenseTypes()) ? true : false;
        if (!$goManager->executeScripts($gaNext['agenda_email'], $gaNext['agenda_password'], $mbTrial, $gaNext['agenda_activation_type'])) {
          $gsError = $goManager->getLastError();
        }
      }
      
      /*
       * Fun��o respons�vel por copiar os phps da pasta fonte para
       * a pasta destino. Essa pasta que ir� armazenar a nova inst�ncia. 
       */
      if (!$gsError) {
        $mbTrial = in_array($gaNext['agenda_activation_type'], SAASLib::getTrialLicenseTypes()) ? true : false;
        if (!$goManager->copyPHPs($mbTrial)) {
          $gsError = $goManager->getLastError();
        }    
      }
      
      /*
       * Fun��o respons�vel por inserir as informa��es da licen�a
       * no banco de dados criado para nova inst�ncia e criar o
       * arquivo config.php com os dados do banco de dados.
       */
      $gsCode = $gaNext['agenda_activation_code'];
      $giType = $gaNext['agenda_activation_type'];
      if (!$gsError) {
        $gaLicenseInfo = SAASLib::getLicenseInfo($goConfig, $gsAlias, $giType);        
        if (!$goManager->adjustDBAndSystem($gaLicenseInfo)) {
          $gsError = $goManager->getLastError();
        }
      }
      
      /*
       * Fun��o respons�vel por inserir as informa��es da nova
       * inst�ncia no banco de dados da administra��o das inst�ncias.
       */
      if (!$gsError) {
        if (!$goManager->insertInstance($gsAlias, $gsCode)) {
          $gsError = $goManager->getLastError();
        }
      }
      
      if (!$gsError) {
        writeLog($grHandle, "Instance $gsAlias successfully created", AGENDA_LOG_INFO);
        
        /*
                 * Envia email com link / usu�rio / senha para o cliente.
                 */
        $goEmail = new FWDEmail();
        $goEmail->setSenderName($goConfig->getConfig(CFG_AXUR_DEFAULT_SENDER_NAME));
        $goEmail->setFrom($goConfig->getConfig(CFG_AXUR_DEFAULT_SENDER));
        $goEmail->setTo($gaNext['agenda_email']);
        if (in_array($giType, SAASLib::getSubscriptionLicenseTypes())) { // SUBSCRIPTION
          $gsLink = $goConfig->getConfig(CFG_COMMERCIAL_URL) . $gsAlias;          
        }
        else { // TRIAL
          $gsLink = $goConfig->getConfig(CFG_TRIAL_URL) . $gsAlias;
        }
        $gaMessage = SAASEmailTemplate::getWelcomeEmail($giType);
        if (count($gaMessage)) {
          $goEmail->setSubject($gaMessage['subject']);        
          $gsMessage = str_replace(array('%link%', '%user%', '%pass%'), array($gsLink, $gaNext['agenda_email'], $gaNext['agenda_password']), $gaMessage['message']);
          $goEmail->setMessage($gsMessage);
          if (!$goEmail->send())
            writeLog($grHandle, "Could not send email with login information to $gsAlias", AGENDA_LOG_ERROR);
        }
        else writeLog($grHandle, "Could not build email with login information to $gsAlias", AGENDA_LOG_ERROR);
        
        
        /*
                 * Envia email com tutorial.
                 */
        $goEmail = new FWDEmail();
        $goEmail->setSenderName($goConfig->getConfig(CFG_AXUR_DEFAULT_SENDER_NAME));
        $goEmail->setFrom($goConfig->getConfig(CFG_AXUR_DEFAULT_SENDER));
        $goEmail->setTo($gaNext['agenda_email']);        
        $gaMessage = SAASEmailTemplate::getGettingStartedEmail($giType);
        if (count($gaMessage)) {
          $goEmail->setSubject($gaMessage['subject']);
          $gsMessage = str_replace('%user%', $gaNext['agenda_email'], $gaMessage['message']);      
          $goEmail->setMessage($gsMessage);
          if (!$goEmail->send())
            writeLog($grHandle, "Could not send email with basic instructions to $gsAlias", AGENDA_LOG_ERROR);
        }
        else writeLog($grHandle, "Could not build email with basic instructions to $gsAlias", AGENDA_LOG_ERROR);
        
       /*
               * Deleta entrada da tabela. 
               */
        $goDelAgenda = new SAASAgenda();
        $goDelAgenda->createFilter($gaNext['agenda_id'], 'agenda_id');
        $goDelAgenda->delete();
      }
      else {
        writeLog($grHandle, "Instance $gsAlias could not be created ($gsError)", AGENDA_LOG_ERROR);
        
        /*
                 * Envia email para realiso com mensagem de erro.
                 */
        $goEmail = new FWDEmail();
        $goEmail->setSenderName($goConfig->getConfig(CFG_AXUR_DEFAULT_SENDER_NAME));
        $goEmail->setFrom($goConfig->getConfig(CFG_AXUR_DEFAULT_SENDER));
        $goEmail->setTo($goConfig->getConfig(CFG_AXUR_EMAIL));
        $goEmail->setSubject(FWDLanguage::getPHPStringValue('em_problem_during_instance_creation', "Problema durante a cria��o da inst�ncia $gsAlias"));
        $gsMessage = FWDLanguage::getPHPStringValue('em_new_system_creation_failed',"A inst�ncia %instance% n�o p�de ser criada. Segue abaixo mensagem de erro:<br/><br/>Erro: $gsError");
        $gsMessage = str_replace('%instance%', $gsAlias, $gsMessage);
        $goEmail->setMessage($gsMessage);
        if(!$goEmail->send()){
          writeLog($grHandle, "Could not send email with errors to Realiso", AGENDA_LOG_ERROR);
        }
        writeLog($grHandle, "Aborting...", AGENDA_LOG_INFO);
        $goConfigUpdate->setConfig(CFG_AGENDA_LOCKED, '0');
        writeLog($grHandle, "Table Unlocked", AGENDA_LOG_INFO);
        exit();
      }
    }
    else {
      writeLog($grHandle, "Agenda manually blocked", AGENDA_LOG_WARNING);
      $gbStopped = true;
    }
    
    /*
     * Busca a pr�xima na fila.
     */
    $goNextHandler = new QueryGetNextInAgenda(FWDWebLib::getConnection());
    $goNextHandler->makeQuery();
    $gaNext = $goNextHandler->executeQuery();
  }
  
  /*
   * Rotina para apagar remover inst�ncias inativas.
   */
   //...
   
   writeLog($grHandle, "Agenda checked", AGENDA_LOG_INFO);
   $goConfigUpdate->setConfig(CFG_AGENDA_LOCKED, '0');
   writeLog($grHandle, "Table Unlocked", AGENDA_LOG_INFO);
   
}
else {
   writeLog($grHandle, "Agenda disabled or blocked", AGENDA_LOG_WARNING);
}

/*
 * Restaura error handler.
 */
FWDWebLib::getInstance()->setDebugMode($gbDebugMode,$gbDebugType);
FWDWebLib::restoreLastErrorHandler();

?>