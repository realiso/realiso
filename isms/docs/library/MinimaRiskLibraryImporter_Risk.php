<?php 

$lang = "DE";
$xmlFilename = "dump_rm_best_practice_sox2_with_cobit_english_french_100409_FR.xml";
$phpFilename = "isms_08_rm_best_practice_fr_sox.php";

class Parser {
	public $xml = '';
	public $parser = null;
	public $counter = 0;
	public $file = null;
	public $column = 0;
	public $wIni = false;
	public $tag = false;
	
	public function __construct($out, $in) {
		$this->file = $out;
		$this->xml = $in;
		$this->parser = xml_parser_create('iso-8859-1');
		xml_set_object($this->parser, $this);
    	xml_set_element_handler($this->parser,'startHandler','endHandler');
    	xml_set_character_data_handler($this->parser,'dataHandler');
	}
	
	public function startHandler($parser, $psTagName, $paAttributes){
		$tag = strtoupper($psTagName);
		$this->tag = $tag;
		switch($tag){
			case 'ROW':
				fwrite($this->file, "$this->counter => array(\n");
				$this->counter++;
				$this->column = 0;
				echo "abre row<br>";
				break;
			case 'DATA':
				$val = $this->column + 1;
				echo "abre cell coluna $val<br>";				
				break;
		}
	}
	
	public function endHandler($parser, $psTagName){
		$psTagname = strtoupper($psTagName);
		switch($psTagname){
			case 'ROW':
				if ($this->column < 5){
					echo "n�o tem implementation -> inserindo vazio<br>";
					fwrite($this->file, "\t'tImplementationGuide' => '',\n");
				}
				fwrite($this->file, "),\n");
				$this->wIni = false;
				$this->column = 0;
				echo "fecha row<br>";
				break;
			case 'DATA':
				fwrite($this->file, "',\n");
				if ($this->column == 1){
					if ($this->counter >= 210) {
						fwrite($this->file, "\t'nControlType' => '1',\n");
					}
					else {
						fwrite($this->file, "\t'nControlType' => '0',\n");
					}
					
				}
				$this->wIni = false;
				$this->column++;
				echo "fecha cell coluna " . $this->column . "<br>";
				break;
		}
		$this->tag = '';
	}
	
	public function writeData($key, $value){
		if(!$this->wIni){
			echo "escrevendo dados row $this->counter, coluna $this->column<br>";
			fwrite($this->file, "\t'$key' => '$value");
			$this->wIni = true;
		} else {
			fwrite($this->file, $value);
		}
	}
	
	public function dataHandler($parser, $psData){
		$psData = addslashes($psData);
		if ($this->tag != 'DATA') return;
		//if ($this->column == 1 && $psData >= 10612) $controlType = 1;
		//else $controlType = 0;
		switch($this->column){
			case 0:
				$this->writeData('fkContext', $psData);
				break;
			case 1:
				$this->writeData('fkSectionBestPractice',$psData);
				break;
			case 2:
				$this->writeData('sName',$psData);
				break;
			case 3:
				$this->writeData('tDescription',$psData);
				break;
			case 4:
				$this->writeData('tImplementationGuide',$psData);
				break;
			default:
				break;
		}
		
	}

	public function parseFile(){
		if(!file_exists($this->xml) || !is_readable($this->xml)){
			$this->csErrorMsg = sprintf("Could not open file '$this->xml'");
			return false;
		}else{
			$msData = file_get_contents($this->xml);
			$mbParse = xml_parse($this->parser,$msData);
			if(!$mbParse){
				$this->csErrorMsg = sprintf(
          			"XML error: %s in file $this->xml at line %d",
					xml_error_string(xml_get_error_code($this->parser)),
					xml_get_current_line_number($this->parser)
				);
				$this->freeParser();
				return false;
			}
		}
		$this->freeParser();
		return true;
	}
	
	protected function freeParser(){
		xml_parser_free($this->parser);
	}

}
$phpFile = fopen($phpFilename, "w+");
$parser = new Parser($phpFile, $xmlFilename);

if ($lang == "DE"){
	$startString = '<?php
			$laBestPracticeDE_SOX = array (';
}
elseif ($lang == "FR") { 
	$startString = '<?php 
			$laBestPracticeFR_SOX = array (';
}
else {
	die();
}
fwrite($phpFile, $startString);
$parser->parseFile();
fwrite($phpFile, ")?>");
fclose($phpFile);
echo "Importadas " .  $parser->counter . "strings.";	


?>