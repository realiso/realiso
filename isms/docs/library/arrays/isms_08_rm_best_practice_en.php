<?php

$laBestPracticeEN = 
array (
  0 => 
  array (
    'fkContext' => '1800',
    'fkSectionBestPractice' => '1602',
    'nControlType' => '0',
    'sName' => '5.1.1 Information security policy document',
    'tDescription' => 'An information security policy document should be approved by management, and published and communicated to all employees and relevant external parties.              ',
    'tImplementationGuide' => 'The information security policy document should state management commitment and set out the organization\'\'s approach to managing information security. The policy document should contain statements concerning: a) a definition of information security, its overall objectives and scope and the importance of security as an enabling mechanism for information sharing (see introduction); b) a statement of management intent, supporting the goals and principles of information security in line with the business strategy and objectives; c) a framework for setting control objectives and controls, including the structure of risk assessment and risk management; d) a brief explanation of the security policies, principles, standards and compliance requirements of particular importance to the organization, including: 1) compliance with legislative, regulatory and contractual requirements; 2) security education, training, and awareness requirements; 3) business continuity management; 4) consequences of information security policy violations; e) a definition of general and specific responsibilities for information security management, including reporting information security incidents; f) references to documentation which may support the policy, e.g. more detailed security policies and procedures for specific information systems or security rules users should comply with. This information security policy should be communicated throughout the organization to users in a form that is relevant, accessible and understandable to the intended reader. Other information The information security policy might be a part of a general policy document. If the information security policy is distributed outside the organization, care should be taken not to disclose sensitive information. Further information can be found in the ISO/IEC TR 13335.',
  ),
  1 => 
  array (
    'fkContext' => '1801',
    'fkSectionBestPractice' => '1602',
    'nControlType' => '0',
    'sName' => '5.1.2 Review of the information security policy',
    'tDescription' => 'The information security policy should be reviewed at planned intervals or if significant changes occur to ensure its continuing suitability, adequacy, and effectiveness.',
    'tImplementationGuide' => 'The information security policy should have an owner who has approved management responsibility for the development, review and evaluation of the security policy. The review should include assessing opportunities for improvement of the organization\'\'s information security policy and approach to managing information security in response to changes to the organizational environment, business circumstances, legal conditions, or technical environment. The review of the information security policy should take account of the results of management reviews. There should be defined management review procedures, including a schedule or period of the review. The input to the management review should include information on: a) feedback from interested parties; b) results of independent reviews (see 6.1.8); c) status of preventive and corrective actions (see 6.1.8 and 15.2.1); d) results of previous management reviews; e) process performance and information security policy compliance; f) changes that could affect the organization\'\'s approach to managing information security, including changes to the organizational environment, business circumstances, resource availability, contractual, regulatory and legal conditions, or to the technical environment; g) trends related to threats and vulnerabilities; h) reported information security incidents (see 13.1); i) recommendations provided by relevant authorities (see 6.1.6). The output from the management review should include any decisions and actions related to: i) improvement of the organization\'\'s approach to managing information security and its processes; ii) improvement of control objectives and controls; iii) improvement in the allocation of resources and/or responsibilities. A record of the management review should be maintained. Management approval for the revised policy should be obtained.',
  ),
  2 => 
  array (
    'fkContext' => '1802',
    'fkSectionBestPractice' => '1604',
    'nControlType' => '0',
    'sName' => '6.1.1 Management commitment to information security',
    'tDescription' => 'Management should actively support security within the organization through clear direction, demonstrated commitment, explicit assignment, and acknowledgment of information security responsibilities.',
    'tImplementationGuide' => 'Management should: a) ensure that information security goals are identified, meet the organizational requirements, are appropriately resourced, and integrated in relevant processes; b) formulate, review and approve information security policy; c) review the effectiveness of the implementation of the information security policy; d) provide clear direction and visible management support for security initiatives; e) approve assignment of specific roles and responsibilities for information security across the organization; f) initiate plans and programs to maintain information security awareness; g) ensure that the implementation of information security controls is co-ordinated across the organization (see 6.1.2). Management should identify the needs for internal or external specialist information security advice, and review and coordinate results of the advice throughout the organization. Depending on the size of the organization, such responsibilities could be handled by a dedicated management forum or by an existing management body, such as the board of directors. Other information Further information is contained in ISO/IEC TR 13335.',
  ),
  3 => 
  array (
    'fkContext' => '1803',
    'fkSectionBestPractice' => '1604',
    'nControlType' => '0',
    'sName' => '6.1.2 Information security co-ordination',
    'tDescription' => 'Information security activities should be co-ordinated by representatives from different parts of the organization with relevant roles and job functions.',
    'tImplementationGuide' => 'Typically, information security co-ordination should involve the co-operation and collaboration of managers, users, administrators, application designers, auditors and security personnel, and specialist skills in areas such as insurance, legal issues, human resources, IT or risk management. This activity should: a) ensure that security activities are executed in compliance with the information security policy; b) identify how to handle non-compliances; c) approve methodologies and processes for information security, e.g. risk assessment, information classification; d) identify significant threat changes and exposure of assets associated with information systems to threats; e) assess the adequacy and co-ordinate the implementation of information security controls; f) effectively promote information security education, training and awareness throughout the organization; g) evaluate information received from the monitoring and reviewing of information security incidents, and recommend appropriate actions in response to identified information security incidents. If the organization does not use a separate cross-functional group, e.g. because such a group is not appropriate for the organization\'\'s size, the actions described above should be undertaken by another suitable management body or individual manager. ',
  ),
  4 => 
  array (
    'fkContext' => '1804',
    'fkSectionBestPractice' => '1604',
    'nControlType' => '0',
    'sName' => '6.1.3 Allocation of information security responsibilities',
    'tDescription' => 'All information security responsibilities should be clearly defined. ',
    'tImplementationGuide' => 'Allocation of information security responsibilities should be done in accordance with the information security policy (see clause 4). Responsibilities for the protection of individual assets and for carrying out specific security processes should be clearly identified. This responsibility should be supplemented, where necessary, with more detailed guidance for specific sites and information processing facilities. Local responsibilities for the protection of assets and for carrying out specific security processes, such as business continuity planning, should be clearly defined. Individuals with allocated security responsibilities may delegate security tasks to others. Nevertheless they remain responsible and should determine that any delegated tasks have been correctly performed. Areas for which individuals are responsible should be clearly stated; in particular the following should take place: a) the assets and security processes associated with each particular system should be identified and clearly defined; b) the entity responsible for each asset or security process should be assigned and the details of this responsibility should be documented (see also 7.1.2); c) authorization levels should be clearly defined and documented. Other information In many organizations an information security manager will be appointed to take overall responsibility for the development and implementation of security and to support the identification of controls. However, responsibility for resourcing and implementing the controls will often remain with individual managers. One common practice is to appoint an owner for each asset who then becomes responsible for its day-to-day protection.',
  ),
  5 => 
  array (
    'fkContext' => '1805',
    'fkSectionBestPractice' => '1604',
    'nControlType' => '0',
    'sName' => '6.1.4 Approval process for information processing facilities',
    'tDescription' => 'A management approval process for new information processing facilities should be defined and implemented. ',
    'tImplementationGuide' => 'The following guidelines should be considered for the approval process: a) new facilities should have appropriate user management approval, authorizing their purpose and use; approval should also be obtained from the manager responsible for maintaining the local information system security environment to ensure that all relevant security policies and requirements are met; b) where necessary, hardware and software should be checked to ensure that they are compatible with other system components; c) the use of personal or privately owned information processing facilities, e.g. laptops, home-computers or hand-held devices, for processing business information, may introduce new vulnerabilities and necessary controls should be identified and implemented. ',
  ),
  6 => 
  array (
    'fkContext' => '1806',
    'fkSectionBestPractice' => '1604',
    'nControlType' => '0',
    'sName' => '6.1.5 Confidentiality agreements',
    'tDescription' => 'A confidentiality agreement reflecting the organization\'\'s requirements for the handling of information should be in place and reviewed regularly. ',
    'tImplementationGuide' => 'Confidentiality agreements should be reviewed periodically and when there are changes to the terms of employment or contract, particularly when employees are due to leave the organization, or other contractual arrangements change. Confidentiality agreements should continue for a defined period of time, in accordance with the relevant security requirements. This should also address the case that confidentiality might need to be maintained longer than a contract might do. Other Information Confidentiality or non-disclosure agreements are used to give notice to the signatories that the information they may have access to is sensitive and that they have certain obligations with regard to its use and disclosure. ',
  ),
  7 => 
  array (
    'fkContext' => '1807',
    'fkSectionBestPractice' => '1604',
    'nControlType' => '0',
    'sName' => '6.1.6 Contact with authorities',
    'tDescription' => 'The organization should maintain appropriate contacts with relevant authorities. ',
    'tImplementationGuide' => 'Organizations should have procedures in place that specify when and by whom authorities (e.g. law enforcement, fire department, supervisory authorities) should be contacted, and how identified information security incidents should be reported in a timely manner if it is suspected that laws may have been broken. Organizations under attack from the Internet may need external third parties (e.g. an Internet service provider or telecommunications operator) to take action against the attack source. Other information Maintaining such contacts may be a requirement to support information security incident management (Section 13.2) or the business continuity and contingency planning process (Section 14). Contacts with regulatory bodies are also useful to anticipate and prepare for upcoming changes in law or regulations, which have to be followed by the organization. Contacts with other authorities include utilities, emergency services and health and safety, e.g. fire departments (in connection with business continuity, see also section 14), telecommunication providers (in connection with line routing and availability), water suppliers (in connection with cooling facilities for equipment). ',
  ),
  8 => 
  array (
    'fkContext' => '1808',
    'fkSectionBestPractice' => '1604',
    'nControlType' => '0',
    'sName' => '6.1.7 Contact with special interest groups',
    'tDescription' => 'The organization should maintain appropriate contacts with special interest groups or other specialist security forums and professional associations. ',
    'tImplementationGuide' => 'Membership in special interest groups or forums should be considered as a means to: a) improve knowledge about best practices and staying up to date with relevant security information; b) ensure the understanding of the information security environment is current and complete; c) receive early warnings of alerts, advisories and patches pertaining to attacks and vulnerabilities; d) gain access to specialist information security advice; e) share and exchange information about new technologies, products, threats or vulnerabilities; f) provide suitable liaison points when dealing with information security incidents (see also 13.2.1). Other information Information sharing agreements can be established to improve cooperation and coordination of security issues. Such agreements should identify requirements for the protection of sensitive information. ',
  ),
  9 => 
  array (
    'fkContext' => '1809',
    'fkSectionBestPractice' => '1604',
    'nControlType' => '0',
    'sName' => '6.1.8 Independent review of information security',
    'tDescription' => 'The organization\'\'s approach to managing information security and its implementation (i.e. control objectives controls, policies, rule processes and procedures for information security) should be reviewed independently at planed interval, or when significant changes to the security implementation occur. ',
    'tImplementationGuide' => 'The independent review should be initiated by management. Such an independent review is necessary to ensure the continuing suitability, adequacy and effectiveness of the organization\'\'s approach to managing information security. The review should include assessing opportunities for improvement and the need for changes to the approach to security, including the policy and control objectives. Such a review should be carried out by individuals independent of the area under review, e.g. the internal audit function, an independent manager or a third party organization specializing in such reviews. Individuals carrying out these reviews should have the appropriate skills and experience. The results of the independent review should be recorded and reported to the management who initiated the review. These records should be maintained. If the independent review identifies that the organization\'\'s approach and implementation to managing information security is inadequate or not compliant with the direction for information security stated in the information security policy document (see 5.1.1), management should consider corrective actions. Other information The area, which managers should regularly review (see 15.2.1) may also be reviewed independently. Review techniques may include interviews to management, checking records or review of security policy documents. ISO 19011:2002, Guidelines for quality and /or environmental management systems auditing, may also provide helpful guidance for carrying out the independent review, including establishment and implementation of review programme. Section 15.3 specifies controls relevant to the independent review on operational information system and the use of audit tools. ',
  ),
  10 => 
  array (
    'fkContext' => '1810',
    'fkSectionBestPractice' => '1605',
    'nControlType' => '0',
    'sName' => '6.2.1 Identification of risks related to external parties',
    'tDescription' => 'The risks to organizational information assets and information processing facilities from business processes involving external parties should be identified and appropriate controls implemented before granting access. ',
    'tImplementationGuide' => 'Where there is a need to allow an external party access to the information processing facilities or information of an organization, a risk assessment (see also Section 4) should be carried out to identify any requirements for specific controls. The identification of risks related to external party access should take into account the following issues: a) the information processing facilities an external party is required to access; b) the type of access the external party will have to the information and information processing facilities, e.g.: a. physical access, e.g. to offices, computer rooms, filing cabinets; b. logical access, e.g. to an organization\'\'s databases, information systems; c. whether the access is taking place on-site or off-site; c) the value and sensitivity of the information involved, and its criticality for business operations; d) the controls necessary to protect information that is not intended to be accessible by external parties; e) the external party personnel involved in handling the organization\'\'s information; f) how organization or personnel authorized to have access can be identified and the authorization verified, and how often this need to be reconfirmed; g) the different means and controls employed by the external party when storing, processing, communicating, sharing and exchanging information; h) the impact of access not being available to the external party when required, and the external party entering or receiving inaccurate or misleading information; i) practices and procedures to deal with information security incidents and potential damages, and the terms and conditions for the continuation of external party access in the case of an information security incident; j) legal and regulatory requirements and other contractual obligations relevant to the external party that should be taken into account; k) how the interests of any other stakeholders may be affected by the arrangements. Access by external parties to the organization\'\'s information should not be provided until the appropriate controls have been implemented and, where feasible, a contract has been signed defining the terms and conditions for the connection or access and the working arrangement. Generally, all security requirements resulting from work with external parties or internal controls should be reflected by the agreement with the external party (see also 6.2.2 and 6.2.3). It should be ensured that the external party is aware of their obligations, and accepts the responsibilities and liabilities involved in accessing, processing, communicating or managing the organization\'\'s information and information processing facilities. Other information Information might be put at risk by external parties with inadequate security management. Controls should be identified and applied to administer external party access to information processing facilities. For example, if there is a special need for confidentiality of the information, non-disclosure agreements might be used. Organizations may face risks associated with inter-organizational processes, management and communication if a high degree of outsourcing is applied, or where there are several external parties involved. The controls 6.2.2 and 6.2.3 cover different external party arrangements, e.g. including: 1) service providers, such as ISPs, network providers, telephone services, maintenance and support services; 2) managed security services; 3) customers; 4) outsourcing of facilities and/or operations, e.g. IT systems, data collection services, call centre operations; 5) management and business consultants, and auditors; 6) developers and suppliers, e.g. of software products and IT systems; 7) cleaning, catering, and other outsourced support services; 8) temporary personnel, student placement and other casual short-term appointments. Such agreements can help to reduce the risks associated with external parties.',
  ),
  11 => 
  array (
    'fkContext' => '1811',
    'fkSectionBestPractice' => '1605',
    'nControlType' => '0',
    'sName' => '6.2.2 Addressing security when dealing with customers',
    'tDescription' => 'All identified security requirements should be addressed before giving customers access to organizational information or assets. ',
    'tImplementationGuide' => 'The following terms should be considered to address security prior to giving customers access to any of the organization\'\'s assets (depending on the type and extent of access given, not all of them might apply): a) asset protection, including: a. procedures to protect organizational assets, including information and software, including management of known vulnerabilities; b. procedures to determine whether any compromise of the assets, e.g. loss or modification of data, has occurred; c. integrity; d. restrictions on copying and disclosing information; b) description of the product or service to be provided; c) the different reasons, requirements and benefits for customer access; d) access control policy, covering: a. permitted access methods, and the control and use of unique identifiers such as user IDs and passwords; b. an authorization process for user access and privileges; c. a statement that all access that is not explicitly authorized is forbidden; d. a process for revoking access rights or interrupting the connection between systems; e) arrangements for reporting, notification and investigation of information inaccuracies (e.g. of personal details), information security incidents and security breaches; f) a description of each service to be made available; g) the target level of service and unacceptable levels of service; h) the right to monitor, and revoke, any activity related to the organization\'\'s assets; i) the respective liabilities of the organization and the customer; j) responsibilities with respect to legal matters and how it is ensured that the legal requirements are met, e.g. data protection legislation, especially taking into account different national legal systems if the agreement involves co-operation with organizations in other countries (see also 15.1); k) intellectual property rights (IPRs) and copyright assignment (see 15.1.2) and protection of any collaborative work (see also 6.1.5). Other information The security requirements related to customers accessing organizational assets can vary considerably depending on the information processing facilities and information being accessed. These security requirements can be addressed using customer agreements, which contains all identified risks and security requirements (see 6.2.1). Agreements with external parties may also involve other parties. Agreements granting external party access should include allowance for designation of other eligible parties and conditions for their access and involvement.',
  ),
  12 => 
  array (
    'fkContext' => '1812',
    'fkSectionBestPractice' => '1605',
    'nControlType' => '0',
    'sName' => '6.2.3 Addressing security in third party agreements',
    'tDescription' => 'Agreements with third parties involving accessing, processing, communicating or managing organizational information assets or information processing facilities, or adding products or services to information processing facilities, should contain or refer to all identified security requirements. ',
    'tImplementationGuide' => 'The agreement should ensure that there is no misunderstanding between the organization and the third party. Organizations should satisfy themselves as to the indemnity of the third party. The following terms should be considered for inclusion in the agreement in order to satisfy the identified security requirements (see 6.2.1): a) the information security policy; b) controls to ensure asset protection, including: 1) procedures to protect organizational assets, including information, software and hardware; 2) procedures to determine whether any compromise of the assets, e.g. loss or modification of information, software and hardware, has occurred; 3) controls to ensure the return or destruction of information and assets at the end of, or at an agreed point in time during, the agreement; 4) confidentiality, integrity, availability and any other relevant property (see 2.1.5) of the assets; 5) restrictions on copying and disclosing information, and using confidentiality agreements (see 6.1.5); c) description of the product or service to be provided; d) user and administrator training in methods, procedures and security; e) ensuring user awareness for information security responsibilities and issues; f) provision for the transfer of personnel, where appropriate; g) responsibilities regarding hardware and software installation and maintenance; h) a clear reporting structure and agreed reporting formats; i) a clear and specified process of change management; j) any required physical protection controls and mechanisms; k) controls to ensure protection against malicious software (see 10.4.1); l) access control policy, covering: 1) the different reasons, requirements and benefits that make the access by the third party necessary; 2) permitted access methods, and the control and use of unique identifiers such as user IDs and passwords; 3) an authorization process for user access and privileges; 4) a requirement to maintain a list of individuals authorized to use the services being made available and what their rights and privileges are with respect to such use; 5) a statement that all access that is not explicitly authorized is forbidden; 6) a process for revoking access rights or interrupting the connection between systems; m) arrangements for reporting, notification and investigation of information security incidents and security breaches, as well as violations of the requirements stated in the agreement; n) a description of each service to be made available, and a description of the information to be made available along with its security classification (see 7.2.1); o) the target level of service and unacceptable levels of service; p) the definition of verifiable performance criteria, their monitoring and reporting; q) the right to monitor, and revoke, any activity related to the organization\'\'s assets; r) the right to audit responsibilities defined in the agreement, to have those audits carried out by a third party, and to enumerate the statutory rights of auditors; s) the establishment of an escalation process for problem resolution; t) service continuity requirements, including measures for availability and reliability, in accordance with an organization\'\'s business priorities; u) the respective liabilities of the parties to the agreement; v) responsibilities with respect to legal matters and how it is ensured that the legal requirements are met, e.g. data protection legislation, especially taking into account different national legal systems if the agreement involves co-operation with organizations in other countries (see also 15.1); w) intellectual property rights (IPRs) and copyright assignment (see 15.1.2) and protection of any collaborative work (see also 6.1.5); x) involvement of the third party with subcontractors, and the security controls these subcontractors need to implement; y) conditions for renegotiation/termination of agreements: 1) a contingency plan should be in place in case either party wishes to terminate the relation before the end of the agreements; 2) renegotiation of agreements if the security requirements of the organization change; 3) current documentation of asset lists, licenses, agreements or rights relating to them. Other information The agreements can vary considerably for different organizations and among the different types of third parties. Therefore, care should be taken to include all identified risks and security requirements (see also 6.2.1) in the agreements. Where necessary, the required controls and procedures can be expanded in a security management plan. If information security management is outsourced, the agreements should address how the third party will guarantee that adequate security, as defined by the risk assessment, will be maintained, and how security will be adapted to identify and deal with changes to risks. Some of the differences between outsourcing and the other forms of third party service provision include the question of liability, planning the transition period and potential disruption of operations during this period, contingency planning arrangements and due diligence reviews, collection and management of information on security incidents. Therefore, it is important that the organization plans and manages the transition to an outsourced arrangement and has suitable processes in place to manage changes and the renegotiation/termination of agreements. The procedures for continuing processing in the event that the third party becomes unable to supply its services need to be considered in the agreement to avoid any delay in arranging replacement services. Agreements with third parties may also involve other parties. Agreements granting third party access should include allowance for designation of other eligible parties and conditions for their access and involvement. Generally agreements are primarily developed by the organization. There may be occasions in some circumstances where an agreement may be developed and imposed upon an organization by a third party. The organization needs to ensure that its own security is not unnecessarily impacted by third party requirements stipulated in imposed agreements. ',
  ),
  13 => 
  array (
    'fkContext' => '1813',
    'fkSectionBestPractice' => '1607',
    'nControlType' => '0',
    'sName' => '7.1.1 Inventory of assets',
    'tDescription' => 'All assets should be clearly identified, and an inventory of all important assets drawn up and maintained.',
    'tImplementationGuide' => 'An organization should identify all assets and document the importance of these assets. The asset inventory should include all information necessary in order to recover from a disaster, including type of asset, format, location, backup information and location, license information, and a business value for insurance purposes. The inventory should not duplicate other inventories unnecessarily, but it should be ensured that the content is aligned. In addition, ownership (see 7.1.2) and information classification (see 7.2) should be agreed and documented for each of the assets. Based on the importance of the asset, its business value and its security classification, levels of protection commensurate with the importance of the assets should be identified (more information on how to value assets to represent their importance can be found in ISO/IEC TR 13335-3). Other information There are many types of assets associated with information systems including: a) Information: databases and data files, contracts and agreements, system documentation, research information, user manuals, training material, operational or support procedures, continuity plans, fallback arrangements, archived information; b) software assets: application software, system software, development tools and utilities; c) physical assets: computer equipment, communications equipment, removable media, and other equipment; d) services: computing and communications services, general utilities, e.g. heating, lighting, power, air-conditioning; e) people, and their qualifications, skills and experience; f) intangibles, such as reputation and image of the organization. Inventories of assets help to ensure that effective asset protection takes place, and may also be required for other business purposes, such as health and safety, insurance or financial (asset management) reasons. The process of compiling an inventory of assets is an important prerequisite of risk management (see also Section 4). ',
  ),
  14 => 
  array (
    'fkContext' => '1814',
    'fkSectionBestPractice' => '1607',
    'nControlType' => '0',
    'sName' => '7.1.2 Ownership of assets',
    'tDescription' => 'All assets associated with information systems or services should be \'\'owned\'\' by a designated part of the organization. ',
    'tImplementationGuide' => 'The term \'\'owner\'\' identifies an individual or entity that has approved management responsibility for controlling the production, development, maintenance, use and security of the assets. The term owner does not mean that the person actually has any property rights to the asset. Ownership may be allocated to: a) a business process; b) a defined set of activities; c) an application; or d) a defined set of data. The asset owner should be responsible for: i) ensuring that assets associated with information systems or services are appropriately classified; ii) defining and periodically reviewing access restrictions and classifications, taking into account applicable access control policies. Other information Routine tasks may be delegated, e.g. to a custodian looking after the asset on a daily basis, but the responsibility remains with the owner. In complex information systems it may be useful to designate groups of assets, which act together to provide a particular function as \'\'services\'\'. In this case the service owner is responsible for the delivery of the service, including the functioning of the assets, which provide it. ',
  ),
  15 => 
  array (
    'fkContext' => '1815',
    'fkSectionBestPractice' => '1607',
    'nControlType' => '0',
    'sName' => '7.1.3 Acceptable use of assets',
    'tDescription' => 'Rules for the acceptable use of assets associated with information systems or services should be identified, documented and implemented.',
    'tImplementationGuide' => 'All employees, contractors and third party users should follow rules for the acceptable use of assets associated with information systems or services, including: a) rules for electronic mail and Internet usages (see 10.8); b) guidelines for the use of mobile devices, especially for the use outside the premises of the organization (see 11.7.1); Specific rules or guidance should be provided by the relevant management. Employees, contractors and third party users using or having access to the organization\'\'s assets should be aware of the limits existing for their use of organization\'\'s assets associated with information systems or services, and resources. They should be responsible for their use of any information processing resources, and of any such use carried out under their responsibility.',
  ),
  16 => 
  array (
    'fkContext' => '1816',
    'fkSectionBestPractice' => '1608',
    'nControlType' => '0',
    'sName' => '7.2.1 Classification guidelines',
    'tDescription' => 'Information and outputs from systems handling sensitive data should be classified in terms of its value, legal requirements, sensitivity and criticality to the organization. ',
    'tImplementationGuide' => 'Classifications and associated protective controls for information should take account of business needs for sharing or restricting information and the business impacts associated with such needs. Classification guidelines should include conventions for initial classification and reclassification over time, in accordance with some predetermined access control policy (see 11.1.1). It should be the responsibility of the asset owner (see 7.1.2) to define the classification of an asset, periodically review it and ensure it is kept up to date and at the appropriate level. The classification should take account of the aggregation effect mentioned in 10.7.2. Consideration should be given to the number of classification categories and the benefits to be gained from their use. Overly complex schemes may become cumbersome and uneconomic to use or prove impractical. Care should be taken in interpreting classification labels on documents from other organizations, which may have different definitions for the same or similarly named labels. Other Information The level of protection can be assessed by analyzing confidentiality, integrity and availability and any other requirements for the information considered. Information often ceases to be sensitive or critical after a certain period of time, for example, when the information has been made public. These aspects should be taken into account, as over-classification can lead to the implementation of unnecessary controls resulting in additional expense. Considering documents with similar security requirements together when assigning classification levels might help to simplify the classification task. In general, the classification given to information is a shorthand way of determining how this information is to be handled and protected. ',
  ),
  17 => 
  array (
    'fkContext' => '1817',
    'fkSectionBestPractice' => '1608',
    'nControlType' => '0',
    'sName' => '7.2.2 Information labeling and handling',
    'tDescription' => 'An appropriate set of procedures for information labeling and handling should be developed in accordance with the classification scheme adopted by the organization. ',
    'tImplementationGuide' => 'Procedures for information labeling need to cover information assets in physical and electronic formats.  Output from systems containing information that is classified as being sensitive or critical should carry an appropriate classification label (in the output). The labeling should reflect the classification according to the rules established in 7.2.1. Items for consideration include printed reports, screen displays, recorded media (e.g. tapes, disks, CDs), electronic messages and file transfers. For each classification level, handling procedures including the secure processing, storage, transmission, declassification and destruction should be defined. This should also include the procedures for chain of custody and logging of any security relevant event. Agreements with other organizations that include information sharing should include procedures to identify the classification of that information and to interpret the classification labels from other organizations. Other Information Labeling and secure handling of classified information is a key requirement for information sharing arrangements. Physical labels are a common form of labeling. However, some information assets, such as documents in electronic form, cannot be physically labeled and electronic means of labeling need to be used. For example, notification labeling may appear on the screen or display. Where labeling is not feasible, other means of designating the classification of information may be applied, e.g. via procedures or meta-data.',
  ),
  18 => 
  array (
    'fkContext' => '1818',
    'fkSectionBestPractice' => '1610',
    'nControlType' => '0',
    'sName' => '8.1.1 Roles and responsibilities',
    'tDescription' => 'Security roles and responsibilities of employees, contractors and third party users should be defined and documented in accordance with the organization\'\'s information security policy.',
    'tImplementationGuide' => 'Security roles and responsibilities should include the requirement to: a) implement and act in accordance with organizational information security policies (see 5.1); b) protect assets from unauthorized access, disclosure, modification, destruction or interference; c) execute particular security processes or activities; d) ensure responsibility is assigned to the individual for actions taken; e) report security events or potential events or other security risks to the organization. Security roles and responsibilities should be defined and clearly communicated to job candidates during the pre-employment process. Other Information Job descriptions can be used to document security roles and responsibilities. Security roles and responsibilities for individuals not engaged via the organization\'\'s employment process, e.g. engaged via a third party organization, should also be clearly defined and communicated. ',
  ),
  19 => 
  array (
    'fkContext' => '1819',
    'fkSectionBestPractice' => '1610',
    'nControlType' => '0',
    'sName' => '8.1.2 Screening',
    'tDescription' => 'Background verification checks on all candidates for employment, contractors and third party users should be carried out in accordance with relevant laws, regulations and ethics, and proportional to the business requirements, the classification of the information to be accessed, and the perceived risks.',
    'tImplementationGuide' => 'Verification checks should take into account all relevant privacy, personal data protection and/or other employment based legislation, and should, where permitted, include the following: a) availability of satisfactory character references, e.g. one business and one personal; b) a check (for completeness and accuracy) of the applicant\'\'s curriculum vitae; c) confirmation of claimed academic and professional qualifications; d) independent identity check (passport or similar document); e) more detailed checks, such as credit checks or checks of criminal records. Where a job, either on initial appointment or on promotion, involves the person having access to information processing facilities, and in particular if these are handling sensitive information, e.g. financial information or highly confidential information, the organization should also consider further, more detailed checks. Procedures should define criteria for verification checks, e.g. who is eligible to screen people, and how, when and why verification checks are carried out. A screening process should also be carried out for contractors and third party users. Where contractors are provided through an agency the contract with the agency should clearly specify the agency\'\'s responsibilities for screening and the notification procedures they need to follow if screening has not been completed or if the results give cause for doubt or concern. In the same way, the agreement with the third party (see also 6.2.3) should clearly specify all responsibilities and notification procedures for screening. Information on all candidates being considered for positions within the organization should be collected and handled in accordance with any appropriate legislation existing in the relevant jurisdiction. Depending on applicable legislation, the candidates should be informed beforehand about the screening activities.',
  ),
  20 => 
  array (
    'fkContext' => '1820',
    'fkSectionBestPractice' => '1610',
    'nControlType' => '0',
    'sName' => '8.1.3 Terms and conditions of employment',
    'tDescription' => 'As part of their contractual obligation, employees, contractors and third party users should agree and sign the terms and conditions of their employment contract, which should state their and the organization\'\'s responsibilities for information security.',
    'tImplementationGuide' => 'The terms and conditions of employment should refer to the organization\'\'s security policy in addition to clarifying and stating: a) that all employees, contractors and third party users who are given access to sensitive information should sign a confidentiality or non-disclosure agreement prior to being given access to information processing facilities; b) the employee\'\'s, contractor\'\'s and any other user\'\'s legal responsibilities and rights, e.g. regarding copyright laws, data protection legislation (see also 15.1.1 and 15.1.2); c) responsibilities for the classification of information and management of organizational assets associated with information systems and services handled by the employee, contractor or third party user (see also 7.2.1 and 10.7.3); d) responsibilities of the employee, contractor or third party user for the handling of information received from other companies or external parties; e) responsibilities of the organization for the handling of personal information, including personal information created as a result of, or in the course of, employment with the organization (see also 15.1.4); f) responsibilities that are extended outside the organization\'\'s premises and outside normal working hours, e.g. in the case of home-working (see also 9.2.5 and 11.7.1); g) actions to be taken if the employee, contractor or third party user disregards the organization\'\'s security requirements (see also 8.2.3). The organization should ensure that employees, contractors and third party users agree to terms and conditions concerning information security appropriate to the nature and extent of access they will have to the organization\'\'s assets associated with information systems and services. Where appropriate, responsibilities contained within the terms and conditions of employment should continue for a defined period after the end of the employment (see also 8.3). Other Information A code of conduct may be used to cover the employee\'\'s, contractor\'\'s or third party user\'\'s responsibilities regarding confidentiality, data protection, ethics, appropriate use of the organization\'\'s equipment and facilities, as well as reputable practices expected by the organization. The contractor or third party users may be associated with an external organization that may in turn be required to enter in contractual arrangements on behalf of the contracted individual.',
  ),
  21 => 
  array (
    'fkContext' => '1821',
    'fkSectionBestPractice' => '1611',
    'nControlType' => '0',
    'sName' => '8.2.1 Management responsibilities',
    'tDescription' => 'Management should require employees, contractors and third party users to apply security in accordance with established policies and procedures of the organization.',
    'tImplementationGuide' => 'Management responsibilities should include employees, contractors and third party users: a) being properly briefed on their information security roles and responsibilities prior to being granted access to sensitive information or information systems; b) being provided with guidelines to state security expectations of their role within the organization; c) being motivated to fulfill the security policies of the organization; d) achieving a level of awareness on security relevant to their roles and responsibilities within the organization (see also 8.2.2); e) conforming to the terms and conditions of employment, which includes the organization\'\'s information security policy and appropriate methods of working. Other Information If employees, contractors and third party users are not made aware of their security responsibilities, they can cause considerable damage to an organization. Motivated personnel are likely to be more reliable and cause less information security incidents. Poor management may cause personnel to feel undervalued resulting in a negative security impact to the organization. For example, poor management may lead to security being neglected or potential misuse of the organization\'\'s assets.',
  ),
  22 => 
  array (
    'fkContext' => '1822',
    'fkSectionBestPractice' => '1611',
    'nControlType' => '0',
    'sName' => '8.2.2 Information security awareness, education and training',
    'tDescription' => 'All employees of the organization and, where relevant, contractors and third party users should receive appropriate awareness training and regular updates in organizational policies and procedures, as relevant for their job function.',
    'tImplementationGuide' => 'Awareness training should commence with a formal induction process designed to introduce the organization\'\'s security policies and expectations before access to information or services is granted. Ongoing training should include security requirements, legal responsibilities and business controls, as well as training in the correct use of information processing facilities e.g. log-on procedure, use of software packages and information on the disciplinary process (see 8.2.3). Other Information The security awareness, education and training activities should be suitable and relevant to the person\'\'s role, responsibilities and skills, and should include information on known threats, who to contact for further security advice and the proper channels for reporting information security incidents (see also 13.1). Training to enhance awareness is intended to allow individuals to recognize information security problems and incidents, and respond according to the needs of their work role.',
  ),
  23 => 
  array (
    'fkContext' => '1823',
    'fkSectionBestPractice' => '1611',
    'nControlType' => '0',
    'sName' => '8.2.3 Disciplinary process',
    'tDescription' => 'There should be a formal disciplinary process for employees, contractors and third party users who have committed a security breach.',
    'tImplementationGuide' => 'The disciplinary process should not be commenced without prior verification that a security breach has occurred (see also 13.2.3 for collection of evidence). The formal disciplinary process should ensure correct and fair treatment for employees, contractors and third party users who are suspected of committing breaches of security. The formal disciplinary process should provide for a graduated response that takes into consideration factors such as the nature and gravity of the breach and its impact on business, whether or not this is a first or repeat offence, whether or not the violator was properly trained, relevant legislation, business contracts and other factors as required. In serious cases of misconduct the process should allow for instant removal. Other Information Such a process should also be used as a deterrent to prevent employees, contractors and third party users in violating organizational security policies and procedures, and any other security breaches. ',
  ),
  24 => 
  array (
    'fkContext' => '1824',
    'fkSectionBestPractice' => '1612',
    'nControlType' => '0',
    'sName' => '8.3.1 Termination responsibilities',
    'tDescription' => 'Responsibilities for performing employment termination should be clearly defined and assigned.',
    'tImplementationGuide' => 'The communication of termination responsibilities should include ongoing security requirements and legal responsibilities and, where appropriate, responsibilities contained within any confidentiality agreement (see 6.1.5) and the terms and conditions of employment (see 8.1.3) continuing for a defined period after the end of the employee\'\'s, contractor\'\'s or third party user\'\'s employment. Responsibilities and duties still valid after termination of employment should be contained in employee\'\'s, contractor\'\'s or third party user\'\'s contracts. Other Information The Human Resources function is generally responsible for the overall termination process and works together with the supervising manager of the person leaving to manage the security aspects of the relevant procedures. In the case of a contractor, this termination responsibility process may be undertaken by an agency responsible for the contractor, and in case of an other user this might be handled by their organization. It may be necessary to inform employees, customers, contractors or third party users of changes to personnel and operating arrangements.',
  ),
  25 => 
  array (
    'fkContext' => '1825',
    'fkSectionBestPractice' => '1612',
    'nControlType' => '0',
    'sName' => '8.3.2 Return of assets',
    'tDescription' => 'All employees, contractors and third party users should return all organizational assets in their possession upon termination of their employment, contract or agreement.',
    'tImplementationGuide' => 'The termination process should be formalized to include the return of all previously issued software, corporate documents and equipment. Other organizational assets such as mobile computing devices, credit cards, access cards, software, manuals and information stored on electronic media also need to be returned. In cases where an employee, contractor or third party user purchases the organization\'\'s equipment or uses their own personal equipment, procedures should be followed to ensure that all relevant information is transferred to the organization and securely erased from the equipment (see also 10.7.1). In cases where an employee, contractor or third party user has knowledge that is important to ongoing operations, that information should be documented and transferred to the organization.',
  ),
  26 => 
  array (
    'fkContext' => '1826',
    'fkSectionBestPractice' => '1612',
    'nControlType' => '0',
    'sName' => '8.3.3 Removal of access rights',
    'tDescription' => 'The access rights of all employees, contractors and third party users to information and information processing facilities should be removed upon termination of their employment, contract or agreement, or adjusted upon change.',
    'tImplementationGuide' => 'Upon termination, the access rights of an individual to assets associated with information systems and services should be reconsidered. This will determine whether it is necessary to remove access rights. Changes of an employment should be reflected in removal of all access rights that were not approved for the new employment. The access rights that should be removed or adapted include physical and logical access, keys, identification cards, information processing facilities (see also 11.2.4), subscriptions, and removal from any documentation that identifies them as a current member of the organization. If a departing employee, contractor or third party user has known passwords for accounts remaining active, these should be changed upon termination or change of employment, contract or agreement. Access rights for information assets and information processing facilities should be reduced or removed before the employment terminates or changes, depending on the evaluation of risk factors such as: a) whether the termination or change is initiated by the employee, contractor or third party user, or by management and the reason of termination; b) the current responsibilities of the employee, contractor or any other user; c) the value of the assets currently accessible. Other Information In certain circumstances access rights may be allocated on the basis of being available to more people than the departing employee, contractor or third party user, e.g. group IDs. In such circumstances, departing individuals should be removed from any group access lists and arrangements should be made to advise all other employees, contractors and third party users involved to no longer share this information with the person departing. In cases of management-initiated termination, disgruntled employees, contractors or third party users may deliberately corrupt information or sabotage information processing facilities. In cases of persons resigning, they may be tempted to collect information for future use. ',
  ),
  27 => 
  array (
    'fkContext' => '1827',
    'fkSectionBestPractice' => '1614',
    'nControlType' => '0',
    'sName' => '9.1.1 Physical security perimeter',
    'tDescription' => 'Organizations should use security perimeters (barriers such as walls, card controlled entry gates or manned reception desks) to protect areas that contain information processing facilities.',
    'tImplementationGuide' => 'The following guidelines should be considered and implemented where appropriate for physical security perimeters: a) security perimeters should be clearly defined, and the siting and strength of each of the perimeters should depend on the security requirements of the assets within the perimeter and the results of a risk assessment; b) perimeters of a building or site containing information processing facilities should be physically sound (i.e. there should be no gaps in the perimeter or areas where a break-in could easily occur); the external walls of the site should be of solid construction and all external doors should be suitably protected against unauthorized access with control mechanisms, e.g. bars, alarms, locks etc; doors and windows should be locked when unattended and external protection should be considered for windows, particularly at ground level; c) a manned reception area or other means to control physical access to the site or building should be in place; access to sites and buildings should be restricted to authorized personnel only; d) physical barriers should, where applicable, be built to prevent unauthorized physical access and environmental contamination; e) all fire doors on a security perimeter should be alarmed, monitored and tested in conjunction with the walls to establish the required level of resistance in accordance to suitable regional, national and international standards; they should operate in accordance with local fire code in a failsafe manner; f) suitable intruder detection systems should be installed to national, regional or international standards and regularly tested to cover all external doors and accessible windows; unoccupied areas should be alarmed at all times; cover should also be provided for other areas, e.g. computer room or communications rooms; g) information processing facilities managed by the organization should be physically separated from those managed by third parties. Other information Physical protection can be achieved by creating one or more physical barriers around the organization\'\'s premises and information processing facilities. The use of multiple barriers gives additional protection, where the failure of a single barrier does not mean that security is immediately compromised. A secure area may be a lockable office, or several rooms surrounded by a continuous internal physical security barrier. Additional barriers and perimeters to control physical access may be needed between areas with different security requirements inside the security perimeter. Special consideration should be given to buildings were multiple organizations are housed. They require special considerations of physical access security.',
  ),
  28 => 
  array (
    'fkContext' => '1828',
    'fkSectionBestPractice' => '1614',
    'nControlType' => '0',
    'sName' => '9.1.2 Physical entry controls',
    'tDescription' => 'Secure areas should be protected by appropriate entry controls to ensure that only authorized personnel are allowed access.',
    'tImplementationGuide' => 'The following guidelines should be considered: a) the date and time of entry and departure of visitors should be recorded, and all visitors should be supervised unless their access has been previously approved; they should only be granted access for specific, authorized purposes and should be issued with instructions on the security requirements of the area and on emergency procedures. b) access to areas where sensitive information is processed or stored should be controlled and restricted to authorized persons only; authentication controls, e.g. access control card plus PIN, should be used to authorize and validate all access; an audit trail of all access should be securely maintained; c) all employees, contractors and third party users and all visitors should be required to wear some form of visible identification and should immediately notify security personnel if they encounter unescorted visitors and anyone not wearing visible identification; d) third party support service personnel should be granted restricted access to secure areas or sensitive information processing facilities only when required; this access should be authorized and monitored; e) access rights to secure areas should be regularly reviewed and updated, and revoked when necessary (see 8.3.3).',
  ),
  29 => 
  array (
    'fkContext' => '1829',
    'fkSectionBestPractice' => '1614',
    'nControlType' => '0',
    'sName' => '9.1.3 Securing offices, rooms and facilities',
    'tDescription' => 'An organization should design and apply physical security for offices, rooms and facilities.',
    'tImplementationGuide' => 'The following guidelines should be considered to secure offices, rooms and facilities: a) account should be taken of relevant health and safety regulations and standards; b) key facilities should be sited to avoid access by the public; c) where applicable, buildings should be unobtrusive and give minimum indication of their purpose, with no obvious signs, outside or inside the building identifying the presence of information processing activities; d) directories and internal telephone books identifying locations of ',
  ),
  30 => 
  array (
    'fkContext' => '1830',
    'fkSectionBestPractice' => '1614',
    'nControlType' => '0',
    'sName' => '9.1.4 Protecting against external and environmental threats',
    'tDescription' => 'An organization should design and apply physical security controls against damage from fire, flood, earthquake, explosion, civil unrest, and other forms of natural or man-made disaster.',
    'tImplementationGuide' => 'Consideration should be given to any security threats presented by neighboring premises, e.g. a fire in a neighbouring building, water leaking from the roof or in floors below ground level or an explosion in the street. The following guidelines should be considered to avoid damage from fire, flood, earthquake, explosion, civil unrest, and other forms of natural or man-made disaster: a) hazardous or combustible materials should be stored at a safe distance from a secure area. Bulk supplies such as stationery should not be stored within a secure area; b) fallback equipment and back-up media should be sited at a safe distance to avoid damage from a disaster affecting the main site; c) appropriate fire fighting equipment should be provided and suitably placed.',
  ),
  31 => 
  array (
    'fkContext' => '1831',
    'fkSectionBestPractice' => '1614',
    'nControlType' => '0',
    'sName' => '9.1.5 Working in secure areas',
    'tDescription' => 'An organization should design and apply controls and guidelines for working in secure areas.',
    'tImplementationGuide' => 'The following guidelines should be considered: a) personnel should only be aware of the existence of, or activities within, a secure area on a need to know basis; b) unsupervised working in secure areas should be avoided both for safety reasons and to prevent opportunities for malicious activities; c) vacant secure areas should be physically locked and periodically checked; d) photographic, video, audio or other recording equipment, such as cameras in mobile devices, should not be allowed, unless authorized; The arrangements for working in secure areas include controls for the employees, contractors and third party users working in the secure area, as well as other third party activities taking place there.',
  ),
  32 => 
  array (
    'fkContext' => '1832',
    'fkSectionBestPractice' => '1614',
    'nControlType' => '0',
    'sName' => '9.1.6 Public access, delivery and loading areas',
    'tDescription' => 'Access points such as delivery and loading areas and other points where unauthorized persons may enter the premises should be controlled and, if possible, isolated from information processing facilities to avoid unauthorized access.',
    'tImplementationGuide' => 'The following guidelines should be considered: a) access to a delivery and loading area from outside of the building should be restricted to identified and authorized personnel; b) the delivery and loading area should be designed so that supplies can be unloaded without delivery personnel gaining access to other parts of the building; c) the external doors of a delivery and loading area should be secured when the internal doors are opened; d) incoming material should be inspected for potential threats (see 9.2.1d)) before this material is moved from the delivery and loading area to the point of use; e) incoming material should be registered in accordance with asset management procedures (see also 7.1.1) on entry to the site; f) incoming and outgoing shipments should be physically segregated, where possible.',
  ),
  33 => 
  array (
    'fkContext' => '1833',
    'fkSectionBestPractice' => '1615',
    'nControlType' => '0',
    'sName' => '9.2.1 Equipment siting and protection',
    'tDescription' => 'Equipment should be sited or protected to reduce the risks from environmental threats and hazards, and opportunities for unauthorized access.',
    'tImplementationGuide' => 'The following guidelines should be considered to protect equipment: a) equipment should be sited to minimize unnecessary access into work areas; b) information processing and storage facilities handling sensitive data should be positioned to reduce the risk of information being viewed by unauthorized persons during their use; c) items requiring special protection should be isolated to reduce the general level of protection required; d) controls should be adopted to minimize the risk of potential physical threats, e.g. theft, fire, explosives, smoke, water (or water supply failure), dust, vibration, chemical effects, electrical supply interference, communications interference, electromagnetic radiation and vandalism; e) guidelines for eating, drinking and smoking in proximity to information processing facilities should be established; f) environmental conditions, such as temperature and humidity, should be monitored for conditions, which could adversely affect the operation of information processing facilities; g) lightning protection should be applied to all buildings and lightning protection filters should be fitted to all incoming power and communications lines; h) the use of special protection methods, such as keyboard membranes, should be considered for equipment in industrial environments; i) equipment processing sensitive information should be protected to minimize the risk of information leakage due to emanation;',
  ),
  34 => 
  array (
    'fkContext' => '1834',
    'fkSectionBestPractice' => '1615',
    'nControlType' => '0',
    'sName' => '9.2.2 Supporting utilities',
    'tDescription' => 'Equipment should be protected from power failures and other disruptions caused by failures in supporting utilities.',
    'tImplementationGuide' => 'All supporting utilities, such as electricity, water supply, sewage, heating/ventilation and air conditioning should be adequate for the systems they are supporting. Support utilities should be regularly inspected and as appropriate tested to ensure their proper functioning and to reduce any risk from their malfunction or failure. A suitable electrical supply should be provided that conforms to the equipment manufacturer\'\'s specifications. An uninterruptible power supply (UPS) to support orderly close down or continuous running is recommended for equipment supporting critical business operations. Power contingency plans should cover the action to be taken on failure of the UPS. A back-up generator should be considered if processing is required to continue in case of a prolonged power failure. An adequate supply of fuel should be available to ensure that the generator can perform for a prolonged period. UPS equipment and generators should be regularly checked to ensure it has adequate capacity and tested in accordance with the manufacturer\'\'s recommendations. In addition, consideration could be given to using multiple power sources or, if the site is large a separate power substation. Emergency power off switches should be located near emergency exits in equipment rooms to facilitate rapid power down in case of an emergency. Emergency lighting should be provided in case of main power failure. The water supply should be stable and adequate to supply air conditioning, humidification equipment and fire suppression systems (where used), and there should be an alarm to indicate loss of water pressure, which may damage equipment or prevent fire suppression from acting effectively. Telecommunications equipment should be connected to the utility provider by at least two diverse routes to prevent failure in one connection path removing voice services. Voice services need to be adequate to meet local legal requirements for emergency communications. Other information Options to achieve continuity of power supplies include multiple feeds to avoid a single point of failure in the power supply.',
  ),
  35 => 
  array (
    'fkContext' => '1835',
    'fkSectionBestPractice' => '1615',
    'nControlType' => '0',
    'sName' => '9.2.3 Cabling security',
    'tDescription' => 'Power and telecommunications cabling carrying data or supporting information services should be protected from interception or damage.',
    'tImplementationGuide' => 'The following guidelines for cabling security should be considered: a) power and telecommunications lines into information processing facilities should be underground, where possible, or subject to adequate alternative protection; b) network cabling should be protected from unauthorized interception or damage, for example by using a conduit or by avoiding routes through public areas; c) power cables should be segregated from communications cables to prevent interference; d) for sensitive or critical systems further controls to consider include: 1) installation of armoured conduit and locked rooms or boxes at inspection and termination points; 2) use of alternative routings and/or transmission media providing appropriate security; 3) use of fibre optic cabling; 4) use of electromagnetic shielding to protect the cables; 5) initiation of technical sweeps and physical inspections for unauthorized devices being attached to the cables; 6) controlled access to patch panels and cable rooms; 7) use of clearly identifiable cable and equipment markings to avoid accidentally patching of wrong network cables; 8) use of a documented patch list to reduce the possibility of errors.',
  ),
  36 => 
  array (
    'fkContext' => '1836',
    'fkSectionBestPractice' => '1615',
    'nControlType' => '0',
    'sName' => '9.2.4 Equipment maintenance',
    'tDescription' => 'Equipment should be correctly maintained to ensure its continued availability and integrity.',
    'tImplementationGuide' => 'The following guidelines for equipment maintenance should be considered: a) equipment should be maintained in accordance with the supplier\'\'s recommended service intervals and specifications; b) only authorized maintenance personnel should carry out repairs and service equipment; c) records should be kept of all suspected or actual faults and all preventive and corrective maintenance; d) appropriate controls should be implemented when equipment is scheduled for maintenance, taking into account whether this maintenance is performed by personnel on site or external to the organization; where necessary, sensitive information should be cleared from the equipment, or the maintenance personnel should be sufficiently cleared; e) all requirements imposed by insurance policies should be complied with.',
  ),
  37 => 
  array (
    'fkContext' => '1837',
    'fkSectionBestPractice' => '1615',
    'nControlType' => '0',
    'sName' => '9.2.5 Security of equipment off-premises',
    'tDescription' => 'Security should be applied to off-site equipment taking into account the different risks working outside the organization\'\'s premises.',
    'tImplementationGuide' => 'Regardless of ownership, the use of any information processing equipment outside the organization\'\'s premises should be authorized by management. The following guidelines should be considered for the protection of off-site equipment: a) equipment and media taken off the premises should not be left unattended in public places; portable computers should be carried as hand luggage and disguised where possible when travelling; b) manufacturers\'\' instructions for protecting equipment should be observed at all times, e.g. protection against exposure to strong electromagnetic fields; c) home-working controls should be determined by a risk assessment and suitable controls applied as appropriate, e.g. lockable filing cabinets, clear desk policy, access controls for computers and secure communication with the office (see also ISO/IEC 18028 Network Security); d) adequate insurance cover should be in place to protect equipment off-site. Security risks, e.g. of damage, theft or eavesdropping, may vary considerably between locations and should be taken into account in determining the most appropriate controls. Other information Information storing and processing equipment includes all forms of personal computers, organizers, mobile phones, smart cards, paper or other form, which is held for home working or being transported away from the normal work location. More information about other aspects of protecting mobile equipment can be found in 11.7.1. ',
  ),
  38 => 
  array (
    'fkContext' => '1838',
    'fkSectionBestPractice' => '1615',
    'nControlType' => '0',
    'sName' => '9.2.6 Secure disposal or re-use of equipment',
    'tDescription' => 'All items of equipment containing storage media, e.g. fixed hard disks, should be checked to ensure that any sensitive data and licensed software has been removed or securely overwritten prior to disposal.',
    'tImplementationGuide' => 'Devices containing sensitive information should be physically destroyed or the information should be destroyed, deleted or overwritten using techniques to make the original information non-retrievable rather than using the standard delete or format function. Other information Damaged devices containing sensitive data may require a risk assessment to determine whether the items should be physically destroyed rather than sent for repair or discarded. Information can be compromised through careless disposal or re-use of equipment (see also 10.7.2).',
  ),
  39 => 
  array (
    'fkContext' => '1839',
    'fkSectionBestPractice' => '1615',
    'nControlType' => '0',
    'sName' => '9.2.7 Removal of property',
    'tDescription' => 'Equipment, information or software should not be taken off-site without prior authorization.',
    'tImplementationGuide' => 'The following guidelines should be considered: a) assets should not be taken off-site without prior authorization; b) employees, contractors and third party users who have authority to permit off-site removal of assets should be clearly identified; c) time limits for equipment removal should be set and returns checked for compliance; d) where necessary and appropriate, equipment should be recorded as being removed off-site and recorded when returned. Other information Spot checks, undertaken to detect unauthorized removal of property, may also be performed to detect unauthorized recording devices, weapons, etc. and prevent their entry into the site. Such spot checks should be carried out in accordance with relevant legislation and regulations. Individuals should be made aware is spot checks are carried out, and the checks should only be performed with authorization appropriate for the legal and regulatory requirements.',
  ),
  40 => 
  array (
    'fkContext' => '1840',
    'fkSectionBestPractice' => '1617',
    'nControlType' => '0',
    'sName' => '10.1.1 Documented operating procedures',
    'tDescription' => 'Operating procedures should be documented, maintained and made available to all users who need them.',
    'tImplementationGuide' => 'Documented procedures should be prepared for system activities associated with information processing and communication facilities, such as computer start-up and close-down procedures, back-up, equipment maintenance, media handling, computer room and mail handling management and safety. The operating procedures should specify the instructions for the detailed execution of each job including: a) processing and handling of information; b) backup (see 10.5); c) scheduling requirements, including interdependencies with other systems, earliest job start and latest job completion times; d) instructions for handling errors or other exceptional conditions, which might arise during job execution, including restrictions on the use of system utilities (see 11.5.4); e) support contacts in the event of unexpected operational or technical difficulties; f) special output and media handling instructions, such as the use of special stationery or the management of confidential output including procedures for secure disposal of output from failed jobs (see 10.7.2 and 10.7.3); g) system restart and recovery procedures for use in the event of system failure; h) the management of audit-trail and system log information (see 10.10). Operating procedures, and the documented procedures for system activities, should be treated as formal documents and changes authorized by management. Where technically feasible, information systems should be managed in a consistent, using the same procedures, tools and utilities.',
  ),
  41 => 
  array (
    'fkContext' => '1841',
    'fkSectionBestPractice' => '1617',
    'nControlType' => '0',
    'sName' => '10.1.2 Change management',
    'tDescription' => 'Changes to information processing facilities and systems should be controlled.',
    'tImplementationGuide' => 'Operational systems and application software should be subject to strict change management control. In particular, the following items should be considered: a) identification and recording of significant changes; b) planning and testing of changes; c) assessment of the potential impacts, including security impacts, of such changes; d) formal approval procedure for proposed changes; e) communication of change details to all relevant persons; f) fallback procedures, including procedures and responsibilities for aborting and recovering from unsuccessful changes and unforeseen events. Formal management responsibilities and procedures should be in place to ensure satisfactory control of all changes to equipment, software or procedures. When changes are made, an audit log containing all relevant information should be retained. Other information Inadequate control of changes to information processing facilities and systems is a common cause of system or security failures. Changes to the operational environment, especially when transferring a system from development to operational stage, can impact on reliability of applications (see also 12.5.1). Changes to operational systems should only be made when there is a valid business reason to do so, such as an increase in the risk to the system. Updating systems with the latest versions of operating system or application is not always in the business interest as this could introduce more vulnerabilities and instability than the current version. There may also be a need for additional training, license costs, support and maintenance and administration overhead, and new hardware especially during migration.',
  ),
  42 => 
  array (
    'fkContext' => '1842',
    'fkSectionBestPractice' => '1617',
    'nControlType' => '0',
    'sName' => '10.1.3 Segregation of duties',
    'tDescription' => 'Duties and areas of responsibility should be segregated to reduce opportunities for unauthorized or unintentional modification or misuse of organizational assets.',
    'tImplementationGuide' => 'Segregation of duties is a method for reducing the risk of accidental or deliberate system misuse. Care should be taken that no single person can access, modify or use assets without authorization or detection. The initiation of an event should be separated from its authorization. The possibility of collusion should be considered in designing the controls. Small organizations may find segregation of duties difficult to achieve, but the principle should be applied as far as is possible and practicable. Whenever it is difficult to segregate, other controls such as monitoring of activities, audit trails and management supervision should be considered. It is important that security audit remains independent. ',
  ),
  43 => 
  array (
    'fkContext' => '1843',
    'fkSectionBestPractice' => '1617',
    'nControlType' => '0',
    'sName' => '10.1.4 Separation of development, test and operational facilities',
    'tDescription' => 'Development, test and operational facilities should be separated to reduce the risks of unauthorized access or changes to the operational system.',
    'tImplementationGuide' => 'The level of separation between operational, test and development environments that is necessary to prevent operational problems should be identified and appropriate controls implemented. The following items should be considered: a) rules for the transfer of software from development to operational status should be defined and documented; b) development and operational software should run on different systems or computer processors and in different domains or directories; c) compilers, editors and other development tools or system utilities should not be accessible from operational systems when not required; d) the test system environment should emulate the operational system environment as closely as possible; e) users should use different user profiles for operational and test systems, and menus should display appropriate identification messages to reduce the risk of error; f) sensitive data should not be copied into the test system environment (see 12.4.2). Other information Development and test activities can cause serious problems, e.g. unwanted modification of files or system environment, or system failure. In this case, there is a need to maintain a known and stable environment in which to perform meaningful testing and to prevent inappropriate developer access. Where development and test personnel have access to the operational system and its information, they may be able to introduce unauthorized and untested code or alter operational data. On some systems this capability could be misused to commit fraud, or introduce untested or malicious code, which can cause serious operational problems. Developers and testers also pose a threat to the confidentiality of operational information. Development and testing activities may cause unintended changes to software or information if they share the same computing environment. Separating development, test and operational facilities is therefore desirable to reduce the risk of accidental change or unauthorized access to operational software and business data (see also 12.4.2 for the protection of test data).',
  ),
  44 => 
  array (
    'fkContext' => '1844',
    'fkSectionBestPractice' => '1618',
    'nControlType' => '0',
    'sName' => '10.2.1 Service delivery',
    'tDescription' => 'The organization should ensure that the security controls, service definitions and delivery levels included in the third party service delivery agreement are implemented, operated and maintained by the third party.',
    'tImplementationGuide' => 'Service delivery by a third party should include the agreed security arrangements, service definitions and aspects of service management. In case of outsourcing arrangements, the organization should plan the necessary transitions (of information, information processing facilities, and anything else that needs to be moved), and should ensure that security is maintained throughout the transition period. The organization should ensure that the third party maintains sufficient service capability together with workable plans designed to ensure that agreed service continuity levels are maintained following major service failures or disaster (see 14.1).',
  ),
  45 => 
  array (
    'fkContext' => '1845',
    'fkSectionBestPractice' => '1618',
    'nControlType' => '0',
    'sName' => '10.2.2 Monitoring and review of third party services',
    'tDescription' => 'The organization should regularly monitor and review the services, reports and records provided by the third party and carry out regular audits.',
    'tImplementationGuide' => 'Monitoring and review of third party services should ensure that the information security terms and conditions of the agreements are being adhered to, and that information security incidents and problems are managed properly. This should involve a service management relationship and process between the organization and the third party to: a) monitor service performance levels to check adherence to the agreements; b) review service reports produced by the third party and regular progress meetings as required by the agreements; c) provide information about information security incidents and review of this information by the third party and the organization as required by the agreements and any supporting guidelines and procedures; d) review third party audit trails and records of security events, operational problems, failures, tracing of faults and disruptions related to the service delivered; e) resolve and manage any identified problems. The responsibility for managing the relationship with a third party should be assigned to a designated individual or service management team. In addition, the organization should ensure that the third party assigns responsibilities for checking for compliance and enforcing the requirements of the agreements. Sufficient technical skills and resources should be made available to monitor that requirements of the agreement (see 6.2.3), in particular the information security requirements, are being met. Appropriate action should be taken when deficiencies in the service delivery are observed. Organizations should maintain sufficient overall control and visibility into all security aspects for sensitive or critical information or information processing facilities accessed, processed or managed by a third party. The organization should ensure they retain visibility into security activities such as change management, identification of vulnerabilities, and information security incident reporting/response through a clearly defined reporting process, format and structure. Other information In case of outsourcing, the organization needs to be aware that the ultimate responsibility for information processed by an outsourcing party remains with the organization.',
  ),
  46 => 
  array (
    'fkContext' => '1846',
    'fkSectionBestPractice' => '1618',
    'nControlType' => '0',
    'sName' => '10.2.3 Managing changes to third party services',
    'tDescription' => 'The organization should manage the changes to the provision of services, including maintaining and improving existing information security policies, procedures and controls, taking account of the criticality of business systems and processes involved and re-assessment of risks.',
    'tImplementationGuide' => 'The process of managing changes to a third party service needs to take account of: a) changes made by the organization to implement: 1) enhancements to the current services offered; 2) development of any new applications and systems; 3) modifications or updates of the organization\'\'s policies and procedures; 4) new controls to resolve information security incidents and to improve security; b) changes in third party services to implement: 1) changes and enhancement to networks; 2) use of new technologies; 3) adoption of new products or newer versions/releases; 4) new development tools and environments; 5) changes to physical location of service facilities; 6) change of vendors.',
  ),
  47 => 
  array (
    'fkContext' => '1847',
    'fkSectionBestPractice' => '1619',
    'nControlType' => '0',
    'sName' => '10.3.1 Capacity management',
    'tDescription' => 'The use of resources should be monitored, tuned, and projections made of future capacity requirements to ensure the required system performance.',
    'tImplementationGuide' => 'For each new and ongoing activity, capacity requirements should be identified. System tuning and monitoring should be applied to ensure and, where necessary, improve the availability and efficiency of systems. Detective controls should be put in place to indicate problems in due time. Projections of future capacity requirements should take account of new business and system requirements and current and projected trends in the organization\'\'s information processing capabilities. Particular attention needs to be paid to any resources with long procurement lead times or high costs, therefore managers should monitor the utilization of key system resources. They should identify trends in usage, particularly in relation to business applications or management information system tools. Managers should use this information to identify and avoid potential bottlenecks that might present a threat to system security or services, and plan appropriate action.',
  ),
  48 => 
  array (
    'fkContext' => '1848',
    'fkSectionBestPractice' => '1619',
    'nControlType' => '0',
    'sName' => '10.3.2 System acceptance',
    'tDescription' => 'Acceptance criteria for new information systems, upgrades and new versions should be established and suitable tests of the system(s) carried out during development and prior to acceptance.',
    'tImplementationGuide' => 'Managers should ensure that the requirements and criteria for acceptance of new systems are clearly defined, agreed, documented and tested. New information systems, upgrades and new versions should only be migrated into production after obtaining formal acceptance. The following items should be considered prior to formal acceptance being provided: a) performance and computer capacity requirements; b) error recovery and restart procedures, and contingency plans; c) preparation and testing of routine operating procedures to defined standards; d) agreed set of security controls in place; e) effective manual procedures; f) business continuity arrangements (see 14.1); g) evidence that installation of the new system will not adversely affect existing systems, particularly at peak processing times, such as month end; h) evidence that consideration has been given to the effect the new system has on the overall security of the organization; i) training in the operation or use of new systems; j) ease of use, as this affects user performance and avoids human error. For major new developments, the operations function and users should be consulted at all stages in the development process to ensure the operational efficiency of the proposed system design. Appropriate tests should be carried out to confirm that all acceptance criteria have been fully satisfied. Other information Acceptance may include a formal certification and accreditation process to verify that the security requirements have been properly addressed.',
  ),
  49 => 
  array (
    'fkContext' => '1849',
    'fkSectionBestPractice' => '1620',
    'nControlType' => '0',
    'sName' => '10.4.1 Controls against malicious code',
    'tDescription' => 'Detection, prevention and recovery controls to protect against malicious code and appropriate user awareness procedures should be implemented.',
    'tImplementationGuide' => 'Protection against malicious code should be based on malicious code detection and repair software, security awareness, and appropriate system access and change management controls. The following guidance should be considered: a) establishing a formal policy prohibiting the use of unauthorized software (see 15.1.2); b) establishing a formal policy to protect against risks associated with obtaining files and software either from or via external networks, or on any other medium, indicating what protective measures should be taken (see also 11.5, especially 11.5.4 and 11.5.5); c) conducting regular reviews of the software and data content of systems supporting critical business processes; the presence of any unapproved files or unauthorized amendments should be formally investigated; d) installation and regular update of malicious code detection and repair software to scan computers and media as a precautionary control, or on a routine basis; the checks carried out should include: 1) checking any files on electronic or optical media, and files received over networks, for malicious code before use; 2) checking electronic mail attachments and downloads for malicious code before use; this check should be carried out at different places, e.g. at electronic mail servers, desk top computers and when entering the network of the organization; 3) checking web pages for malicious code; e) defining management procedures and responsibilities to deal with malicious code protection on systems, training in their use, reporting and recovering from malicious code attacks (see 13.1 and 13.2); f) preparing appropriate business continuity plans for recovering from malicious code attacks, including all necessary data and software back-up and recovery arrangements (see clause 14); g) implementing procedures to regularly collect information, such as subscribing to mailing lists and/or checking web sites giving information about new malicious code; h) implementing procedures to verify all information relating to malicious code, and ensure that warning bulletins are accurate and informative; managers should ensure that qualified sources, e.g. reputable journals, reliable Internet sites or suppliers producing software protecting against malicious code, are used to differentiate between hoaxes and real malicious code; all users should be made aware of the problem of hoaxes and what to do on receipt of them. Other information The use of two or more software products protecting against malicious code across the IT environment from different vendors can improve the effectiveness of malicious code protection. Software to protect against malicious code can be installed to provide automatic updates of definition files and scanning engines to ensure the protection is up to date. In addition, this software can be installed on every desktop to carry out automatic checks. Care should be taken to protect against the introduction of malicious code during maintenance and emergency procedures, which may bypass normal malicious code protection controls.',
  ),
  50 => 
  array (
    'fkContext' => '1850',
    'fkSectionBestPractice' => '1620',
    'nControlType' => '0',
    'sName' => '10.4.2 Controls against mobile code',
    'tDescription' => 'The execution of mobile code should restrict the mobility of the code to an intended environment avoiding such code violating the organization\'\'s information security policies.',
    'tImplementationGuide' => 'The following actions should be considered to protect against mobile code: a) executing mobile code in a logically isolated environment; b) blocking any use of mobile code; c) blocking receipt of mobile code; and d) other technical measures as available on a specific system to instantiate the security policy and guidelines and procedures; e) control the resources available to mobile code access; f) cryptographic controls to uniquely authenticate mobile code. Other information Mobile code is predominantly used within mobile agent systems. Within these systems the piece of software representing the process of the mobile agent moves on its own behalf between systems supporting the mobile agent platform. Inappropriate configuration might lead to wrong or uncontrollable use of system, network or application resources as well as to a loss of information security.',
  ),
  51 => 
  array (
    'fkContext' => '1851',
    'fkSectionBestPractice' => '1621',
    'nControlType' => '0',
    'sName' => '10.5.1 Information back-up',
    'tDescription' => 'Back-up copies of information and software should be taken and tested regularly in accordance with an agreed backup policy.',
    'tImplementationGuide' => 'Adequate back-up facilities should be provided to ensure that all essential information and software can be recovered following a disaster or media failure. The following items for information back up should be considered: a) the necessary level of back-up information should be defined; b) accurate and complete records of the back-up copies and documented restoration procedures should be produced; c) the extent (e.g. full or differential backup) and frequency of backups should reflect the business requirements of the organization, the security requirements of the information involved, and the criticality of the information to the continued operation of the organization; d) the back-ups should be stored in a remote location, at a sufficient distance to escape any damage from a disaster at the main site; e) back-up information should be given an appropriate level of physical and environmental protection (see clause 9) consistent with the standards applied at the main site; the controls applied to media at the main site should be extended to cover the back-up site; f) back-up media should be regularly tested to ensure that they can be relied upon for emergency use when necessary; g) restoration procedures should be regularly checked and tested to ensure that they are effective and that they can be completed within the time allotted in the operational procedures for recovery; h) in situations where confidentiality is of importance, back-ups should be protected by means of encryption. Back-up arrangements for individual systems should be regularly tested to ensure that they meet the requirements of business continuity plans (see clause 14). For critical systems, the backup arrangements should cover all systems information, applications and data necessary to recover the complete system in the event of a disaster. The retention period for essential business information, and also any requirement for archive copies to be permanently retained should be determined (see 15.1.3). Other information Back up arrangements can be automated to ease the back-up and restore process. Such automated solutions should be sufficiently tested prior to implementation and at regular intervals.',
  ),
  52 => 
  array (
    'fkContext' => '1852',
    'fkSectionBestPractice' => '1622',
    'nControlType' => '0',
    'sName' => '10.6.1 Network controls',
    'tDescription' => 'Networks should be adequately managed and controlled, in order to be protected from threats, and to maintain security for the systems and applications using the network, including information in transit.',
    'tImplementationGuide' => 'Network managers should implement controls to ensure the security of information in networks, and the protection of connected services from unauthorized access. In particular, the following items should be considered: a) operational responsibility for networks should be separated from computer operations where appropriate (see 10.1.3); b) responsibilities and procedures for the management of remote equipment, including equipment in user areas, should be established; c) special controls should be established to safeguard the confidentiality and integrity of data passing over public networks or over wireless networks, and to protect the connected systems and applications (see 11.4 and 12.3); special controls may also be required to maintain the availability of the network services and computers connected; d) appropriate logging and monitoring should be applied to enable recording of security relevant actions; e) management activities should be closely co-ordinated both to optimize the service to the organization and to ensure that controls are consistently applied across the information processing infrastructure. Other information Additional information on network security can be found in ISO/IEC 18028 Network Security.',
  ),
  53 => 
  array (
    'fkContext' => '1853',
    'fkSectionBestPractice' => '1622',
    'nControlType' => '0',
    'sName' => '10.6.2 Security of network services',
    'tDescription' => 'Security features, service levels and management requirements of all network services should be identified and included in any network services agreement, whether these services are provided in-house or outsourced.',
    'tImplementationGuide' => 'The ability of the network service provider to manage agreed services in a secure way should be determined and regularly monitored, and the right to audit should be agreed. The security arrangements necessary for particular services, such as security features, service levels and management requirements, should be identified. The organization should ensure that network service providers implement these measures. The security features of the network services should include the provision of: a) technology applied for security of network services such as authentication, encryption, and network connection controls; b) technical parameters required for secured connection with the network services in accordance with the security and network connection rules; c) procedures for the network service usage to restrict access to network services or applications, where necessary. Other information Network services include the provision of connections, private network services, and value added networks and managed network security solutions such as firewalls and intrusion detection systems. These services can range from simple unmanaged bandwidth to complex value-added offerings.',
  ),
  54 => 
  array (
    'fkContext' => '1854',
    'fkSectionBestPractice' => '1623',
    'nControlType' => '0',
    'sName' => '10.7.1 Management of removable computer media',
    'tDescription' => 'There should be procedures for the management of removable computer media.',
    'tImplementationGuide' => 'The following guidelines for the management of removable media should be considered: a) if no longer required, the contents of any re-usable media that are to be removed from the organization should be securely erased; b) where necessary and practical, authorization should be required for media removed from the organization and a record of such removals should be kept in order to maintain an audit trail; c) all media should be stored in a safe, secure environment, in accordance with manufacturers\'\' specifications; d) information stored on media that needs to be available longer than the media lifetime (in accordance with manufacturers\'\' specifications) should be also stored elsewhere to avoid information loss due to media degradation; e) registration of removable media should be considered to limit the opportunity for data loss; f) removable media drives should only be enabled if there is a business reason for doing so. All procedures and authorization levels should be clearly documented. Other information For example, removable media include tapes, disks, flash disks, removable hard drives, CDs, DVDs, memory sticks and printed media.',
  ),
  55 => 
  array (
    'fkContext' => '1855',
    'fkSectionBestPractice' => '1623',
    'nControlType' => '0',
    'sName' => '10.7.2 Disposal of media',
    'tDescription' => 'Media should be disposed of securely and safely when no longer required, using formal procedures.',
    'tImplementationGuide' => 'Formal procedures for the secure disposal of media should minimize the risk of leakage of sensitive information to unauthorised persons. The procedures for secure disposal of media containing sensitive information should be commensurate with the sensitivity of that information. The following items should be considered: a) media containing sensitive information should be stored and disposed of securely and safely, e.g. by incineration or shredding, or erased of data for use by another application within the organization; b) procedures should be in place to identify the items that might require secure disposal; c) it may be easier to arrange for all media items to be collected and disposed of securely, rather than attempting to separate out the sensitive items; d) many organizations offer collection and disposal services for papers, equipment and media; care should be taken in selecting a suitable contractor with adequate controls and experience; e) disposal of sensitive items should be logged where possible in order to maintain an audit trail. When accumulating media for disposal, consideration should be given to the aggregation effect, which may cause a large quantity of non-sensitive information to become sensitive. Other information Sensitive information could be disclosed through careless disposal of media (see also 9.2.6 for information about disposal of equipment).',
  ),
  56 => 
  array (
    'fkContext' => '1856',
    'fkSectionBestPractice' => '1623',
    'nControlType' => '0',
    'sName' => '10.7.3 Information handling procedures',
    'tDescription' => 'Procedures for the handling and storage of information should be established to protect this information from unauthorized disclosure or misuse.',
    'tImplementationGuide' => 'Procedures should be drawn up for handling, processing, storing and communicating information consistent with its classification (see 7.2). The following items should be considered: a) handling and labelling of all media to its indicated classification level; b) access restrictions to prevent access from unauthorized personnel; c) maintenance of a formal record of the authorized recipients of data; d) ensuring that input data is complete, that processing is properly completed and that output validation is applied; e) protection of spooled data awaiting output to a level consistent with its sensitivity; f) storage of media in accordance with manufacturers\'\' specifications; g) keeping the distribution of data to a minimum; h) clear marking of all copies of media for the attention of the authorized recipient; i) review of distribution lists and lists of authorized recipients at regular intervals. Other information These procedures apply to information in documents, computing systems, networks, mobile computing, mobile communications, mail, voice mail, voice communications in general, multimedia, postal services/facilities, use of fax machines and any other sensitive items, e.g. blank cheques, invoices.',
  ),
  57 => 
  array (
    'fkContext' => '1857',
    'fkSectionBestPractice' => '1623',
    'nControlType' => '0',
    'sName' => '10.7.4 Security of system documentation',
    'tDescription' => 'System documentation should be protected against unauthorized access.',
    'tImplementationGuide' => 'To secure system documentation, the following items should be considered: a) system documentation should be stored securely; b) the access list for system documentation should be kept to a minimum and authorized by the application owner; c) system documentation held on a public network, or supplied via a public network, should be appropriately protected. Other information System documentation may contain a range of sensitive information, e.g. descriptions of applications processes, procedures, data structures, authorization processes.',
  ),
  58 => 
  array (
    'fkContext' => '1858',
    'fkSectionBestPractice' => '1624',
    'nControlType' => '0',
    'sName' => '10.8.1 Information exchange policies and procedures',
    'tDescription' => 'Formal exchange policies, procedures and controls should be in place to protect the exchange of information through the use of all types of communication facilities.',
    'tImplementationGuide' => 'The procedures and controls to be followed when using electronic communication facilities for information exchange should consider the following items: a) procedures designed to protect exchanged information from interception, copying, modification, mis-routing and destruction; b) procedures for the detection of and protection against malicious code that may be transmitted through the use of electronic communications (see Clause 10.4.1); c) procedures for protecting communicated sensitive electronic information that is in the form of an attachment; d) policy or guidelines outlining acceptable use of electronic communication facilities (see 7.1.3); e) procedures for the use of wireless communications, taking into account the particular risks involved; f) employee, contractor and any other user\'\'s responsibilities not to compromise the organization, e.g. through defamation, harassment, impersonation, forwarding of chain letters, unauthorized purchasing, etc.; g) use of cryptographic techniques e.g. to protect the confidentiality, integrity and authenticity of information (see Clause 12.3); h) retention and disposal guidelines for all business correspondence, including messages, in accordance with relevant national or local legislation or regulations;
 i) not leaving sensitive or critical information on printing facilities, e.g. copiers, printers and fax, as these may be accessed by unauthorized personnel; j) controls and restrictions associated with the forwarding of communication facilities, e.g. automatic forwarding of electronic mail to external mail addresses; k) reminding personnel that they should take appropriate precautions, e.g. not to reveal sensitive information to avoid being overheard or intercepted when making a phone call by: 1) people in their immediate vicinity particularly when using mobile phones; 2) wiretapping, and other forms of eavesdropping through physical access to the phone handset or the phone line, or using scanning receivers; 3) people at the recipient\'\'s end; l) not leaving messages containing sensitive information on answering machines since these may be replayed by unauthorized persons, stored on communal systems or stored incorrectly as a result of misdialling; m) reminding personnel about the problems of using facsimile machines, namely: 1) unauthorized access to built-in message stores to retrieve messages; 2) deliberate or accidental programming of machines to send messages to specific numbers; 3) sending documents and messages to the wrong number either by misdialling or using the wrong stored number; n) reminding personnel not to register demographic data, such as the e-mail address or other personal information, in any software to avoid collection for unauthorized use; o) reminding personnel that modern fax machines and photocopiers have page caches and store pages in case of a paper or transmission fault, which will be printed once the fault is cleared. In addition, personnel should be reminded that they should not have confidential conversations in public places or open offices and meeting places with non-sound proofed-walls. Information exchange facilities should comply with any relevant legal requirements (see clause 15). Other Information Information exchange may occur through the use of a number of different types of communication facilities, including electronic mail, voice, facsimile and video. Software exchange may occur through a number of different mediums, including downloading from the Internet and acquired from vendors selling off-the-shelf products. The business, legal and security implications associated with electronic data interchange, electronic commerce and electronic communications and the requirements for controls should be considered. Information could be compromised due to lack of awareness, policy or procedures on the use of information exchange facilities, e.g. being overheard on a mobile phone in a public place, misdirection of an electronic mail message, answering machines being overheard, unauthorised access to dial-in voice-mail systems or accidentally sending facsimiles to the wrong facsimile equipment. Business operations could be disrupted and information could be compromised if communications facilities fail, are overloaded or interrupted (see 10.3 and clause 14). Information could be compromised if accessed by unauthorized users (see clause 11).',
  ),
  59 => 
  array (
    'fkContext' => '1859',
    'fkSectionBestPractice' => '1624',
    'nControlType' => '0',
    'sName' => '10.8.2 Exchange agreements',
    'tDescription' => 'Agreements should be established for the exchange of information and software between the organization and external parties.',
    'tImplementationGuide' => 'Exchange agreements should consider the following security conditions: a) management responsibilities for controlling and notifying transmission, dispatch and receipt; b) procedures for notifying sender of transmission, dispatch and receipt; c) procedures to ensure traceability and non-repudiation; d) minimum technical standards for packaging and transmission; e) escrow agreements; f) courier identification standards; g) responsibilities and liabilities in the event of information security incidents, such as loss of data; h) use of an agreed labelling system for sensitive or critical information, ensuring that the meaning of the labels is immediately understood and that the information is appropriately protected; i) ownership and responsibilities for data protection, copyright, software license compliance and similar considerations (see 15.1.2 and 15.1.4); j) technical standards for recording and reading information and software; k) any special controls that may be required to protect sensitive items, such as cryptographic keys (see 12.3). Policies, procedures and standards should be established and maintained to protect information and physical media in transit (see also 10.8.3) and be referenced in such exchange agreements. The security content of any agreement should reflect the sensitivity of the business information involved. Other Information Agreements may be electronic or manual, and may take the form of formal contracts or conditions of employment. For sensitive information, the specific mechanisms used for the exchange of such information should be consistent for all organizations and types of agreements.',
  ),
  60 => 
  array (
    'fkContext' => '1860',
    'fkSectionBestPractice' => '1624',
    'nControlType' => '0',
    'sName' => '10.8.3 Physical media in transit',
    'tDescription' => 'Media should be protected against unauthorized access, misuse or corruption during transportation beyond an organization\'\'s physical boundaries.',
    'tImplementationGuide' => 'The following guidelines should be considered to protect information media being transported between sites: a) reliable transport or couriers should be used; b) a list of authorized couriers should be agreed with management; c) procedures to check the identification of couriers should be developed; d) packaging should be sufficient to protect the contents from any physical damage likely to arise during transit and in accordance with any manufacturers\'\' specifications (e.g. for software), for example protecting against any environmental factors that may reduce the media\'\'s restoration effectiveness such as exposure to heat, moisture or electromagnetic fields; e) controls should be adopted, where necessary, to protect sensitive information from unauthorized disclosure or modification; examples include: 1) use of locked containers; 2) delivery by hand; 3) tamper-evident packaging (which reveals any attempt to gain access); 4) in exceptional cases, splitting of the consignment into more than one delivery and dispatch by different routes. Other Information Information can be vulnerable to unauthorized access, misuse or corruption during physical transport, for instance when sending media via the postal service or via courier.',
  ),
  61 => 
  array (
    'fkContext' => '1861',
    'fkSectionBestPractice' => '1624',
    'nControlType' => '0',
    'sName' => '10.8.4 Electronic messaging',
    'tDescription' => 'Information involved in electronic messaging should be appropriately protected.',
    'tImplementationGuide' => 'Security considerations for electronic messaging should include the following: a) protecting messages from unauthorized access, modification or denial of service; b) ensuring correct addressing and transportation of the message; c) general reliability and availability of the service; d) legal considerations, for example requirements for electronic signatures; e) obtaining approval prior to using external public services such as instant messaging or file sharing; f) stronger levels of authentication controlling access from publicly accessible networks. Other Information Electronic messaging such as email, Electronic Data Interchange (EDI), and instant messaging play an increasingly important role in business communications. Electronic messaging has different risks than paper based communications.',
  ),
  62 => 
  array (
    'fkContext' => '1862',
    'fkSectionBestPractice' => '1624',
    'nControlType' => '0',
    'sName' => '10.8.5 Business information systems',
    'tDescription' => 'Policies and guidelines should be developed and implemented to protect information associated with the interconnection of business information systems.',
    'tImplementationGuide' => 'Consideration given to the security and business implications of interconnecting such facilities should include: a) known vulnerabilities in the administrative and accounting systems where information is shared between different parts of the organization; b) vulnerabilities of information in business communication systems, e.g. recording phone calls or conference calls, confidentiality of calls, storage of faxes, opening mail, distribution of mail; c) policy and appropriate controls to manage information sharing; d) excluding categories of sensitive business information and classified documents if the system does not provide an appropriate level of protection (see 7.2); e) restricting access to diary information relating to selected individuals, e.g. personnel working on sensitive projects; f) categories of personnel, contractors or business partners allowed to use the system and the locations from which it may be accessed (see 6.2 and 6.3); g) restricting selected facilities to specific categories of user; h) identifying the status of users, e.g. employees of the organization or contractors in directories for the benefit of other users; i) retention and back-up of information held on the system (see 10.5.1); j) fallback requirements and arrangements (see 14). Other Information Office information systems are opportunities for faster dissemination and sharing of business information using a combination of: documents, computers, mobile computing, mobile communications, mail, voice mail, voice communications in general, multimedia, postal services/facilities and fax machines.',
  ),
  63 => 
  array (
    'fkContext' => '1863',
    'fkSectionBestPractice' => '1625',
    'nControlType' => '0',
    'sName' => '10.9.1 Electronic commerce',
    'tDescription' => 'Electronic information passing over public networks should be protected from fraudulent activity, contract dispute, and unauthorized disclosure and modification.',
    'tImplementationGuide' => 'Security considerations for electronic commerce should include the following: a) the level of confidence each party requires in each others claimed identity, e.g. through authentication; b) authorization processes associated with who may set prices, issue or sign key trading documents; c) ensuring that trading partners are fully informed of their authorisations; d) determining and meeting requirements for confidentiality, integrity and proof of dispatch and receipt of key documents and the non-repudiation of contracts, e.g. associated with tendering and contract processes; e) the level of trust required in the integrity of advertised price lists; f) the confidentiality of any sensitive data or information; g) the confidentiality and integrity of any order transactions, payment information, delivery address details, and confirmation of receipts; h) the degree of verification appropriate to check payment information supplied by a customer; i) selecting the most appropriate settlement form of payment to guard against fraud; j) the level of protection required to maintain the confidentiality and integrity of order information; k) avoidance of loss or duplication of transaction information; l) liability associated with any fraudulent transactions; m) insurance requirements. Many of the above considerations can be addressed by the application of cryptographic controls (see 12.3), taking into account compliance with legal requirements (see 15.1, especially 15.1.6 for cryptography legislation). Electronic commerce arrangements between trading partners should be supported by a documented agreement which commits both parties to the agreed terms of trading, including details of authorization (see b) above). Other agreements with information service and value added network providers may be necessary. Public trading systems should publicize their terms of business to customers. Consideration should be given to the resilience to attack of the host(s) used for electronic commerce, and the security implications of any network interconnection required for the implementation of electronic commerce services (see 11.4.6). Other Information Electronic commerce is vulnerable to a number of network threats that may result in fraudulent activity, contract dispute and disclosure or modification of information. Electronic commerce can make use of secure authentication methods, e.g. using public key cryptography and digital signatures (see also 12.3) to reduce the risks. Also, trusted third parties can be used, where such services are needed.',
  ),
  64 => 
  array (
    'fkContext' => '1864',
    'fkSectionBestPractice' => '1625',
    'nControlType' => '0',
    'sName' => '10.9.2 On-Line Transactions',
    'tDescription' => 'Information involved in on-line transactions should be protected to prevent incomplete transmission, mis-routing, unauthorized message alteration, unauthorized disclosure, unauthorized message duplication or replay.',
    'tImplementationGuide' => 'Security considerations for on-line transactions should include the following: a) the use of electronic signatures by each of the parties involved in the transaction; b) all aspects of the transaction, i.e. ensuring that: 1) user credentials of all parties are valid and verified; 2) the transaction remains confidential; and 3) privacy associated with all parties involved is retained; c) communications path between all involved parties is encrypted; d) protocol used to communicate between all involved parties is secured; e) ensuring that the storage of the transaction details are located outside of any public accessible environment, e.g. on a storage platform existing on the organizational Intranet, and not retained and exposed on a storage medium directly accessible from the Internet; f) where a trusted authority is used (e.g. for the purposes of issuing and maintaining digital signatures and/or digital certificates) security is integrated and embedded throughout the entire end-to-end certificate/signature management process. Other Information The extent of the controls adopted will need to be commensurate with the level of the risk associated with each form of on-line transaction. Transactions may need to comply with laws, rules and regulations in the jurisdiction in which the transaction is either generated from, processed via, completed at, and/or stored. There exist many forms of transactions that can be performed in an on-line manner e.g. contractual, financial etc.',
  ),
  65 => 
  array (
    'fkContext' => '1865',
    'fkSectionBestPractice' => '1625',
    'nControlType' => '0',
    'sName' => '10.9.3 Publicly available information',
    'tDescription' => 'The integrity of information being made available on a publicly available system should be protected to prevent unauthorized modification.',
    'tImplementationGuide' => 'Software, data and other information requiring a high level of integrity, being made available on a publicly available system, should be protected by appropriate mechanisms, e.g. digital signatures (see 12.3). The publicly accessible system should be tested against weaknesses and failures prior to information made available. There should be a formal approval process before information is made publicly available. In addition, all input provided from the outside to the system should be verified and approved. Electronic publishing systems, especially those that permit feedback and direct entering of information, should be carefully controlled so that: a) information is obtained in compliance with any data protection legislation (see 15.1.4); b) information input to, and processed by, the publishing system will be processed completely and accurately in a timely manner; c) sensitive information will be protected during collection, processing and storage; d) access to the publishing system does not allow unintended access to networks to which the system is connected. Other Information Information on a publicly available system, e.g. information on a Web server accessible via the Internet, may need to comply with laws, rules and regulations in the jurisdiction in which the system is located, where trade is taking place or where the owner(s) reside. Unauthorized modification of published information may harm the reputation of the publishing organization.',
  ),
  66 => 
  array (
    'fkContext' => '1866',
    'fkSectionBestPractice' => '1626',
    'nControlType' => '0',
    'sName' => '10.10.1 Audit logging',
    'tDescription' => 'Audit logs recording user activities, exceptions and information security events should be produced and kept for an agreed period to assist in future investigations and access control monitoring.',
    'tImplementationGuide' => 'Audit logs should include, when relevant: a) user IDs; b) dates, times and details of key events, e.g. log-on and log-off; c) terminal identity or location if possible; d) records of successful and rejected system access attempts; e) records of successful and rejected data and other resource access attempts; f) changes to system configuration; g) use of privileges; h) use of system utilities and applications; i) files accessed and the kind of access; j) network addresses and protocols; k) alarms raised by the access control system; l) activation and de-activation of protection systems, such as anti-virus systems and intrusion detection systems. Other information The audit logs may contain intrusive and confidential personal data. Appropriate privacy protection measures should be taken (see also 15.1.4). Where possible, system administrators should not have permission to erase or de-activate logs of their own activities (see 10.1.3).',
  ),
  67 => 
  array (
    'fkContext' => '1867',
    'fkSectionBestPractice' => '1626',
    'nControlType' => '0',
    'sName' => '10.10.2 Monitoring system use',
    'tDescription' => 'Procedures for monitoring use of information processing facilities should be established and the results of the monitoring activities reviewed regularly.',
    'tImplementationGuide' => 'The level of monitoring required for individual facilities should be determined by a risk assessment. An organisation should comply with all relevant legal requirements applicable to its monitoring activities. Areas that should be considered include: a) authorized access, including detail such as: 1) the user ID; 2) the date and time of key events; 3) the types of events; 4) the files accessed; 5) the program/utilities used; b) all privileged operations, such as: 1) use of privileged accounts, e.g. supervisor, root, administrator; 2) system start-up and stop; 3) I/O device attachment/detachment; c) unauthorized access attempts, such as: 1) failed or rejected user actions; 2) failed or rejected actions involving data and other resources; 3) access policy violations and notifications for network gateways and firewalls; 4) alerts from proprietary intrusion detection systems; d) system alerts or failures such as: 1) console alerts or messages; 2) system log exceptions; 3) network management alarms; 4) alarms raised by the access control system; e) changes to, or attempts to change, system security settings and controls. How often the results of monitoring activities are reviewed should depend on the risks involved. Risk factors that should be considered include the: i) criticality of the application processes; ii) value, sensitivity and criticality of the information involved; iii) past experience of system infiltration and misuse, and the frequency of vulnerabilities occurring; iv) extent of system interconnection (particularly public networks); v) logging facility being de-activated. Other information Usage monitoring procedures are necessary to ensure that users are only performing activities that have been explicitly authorized. A log review involves understanding the threats faced by the system and the manner in which these may arise. Examples of events that might require further investigation in case of information security incidents are given in 13.1.1.',
  ),
  68 => 
  array (
    'fkContext' => '1868',
    'fkSectionBestPractice' => '1626',
    'nControlType' => '0',
    'sName' => '10.10.3 Protection of log information',
    'tDescription' => 'Logging facilities and log information should be protected against tampering and unauthorized access.',
    'tImplementationGuide' => 'Controls should aim to protect against unauthorized changes and operational problems with the logging facility including: a) alterations to the message types that are recorded; b) log files being edited or deleted; c) storage capacity of the log file media being exceeded, resulting in either the failure to record events or over-writing of past recorded events. Some audit logs may be required to be archived as part of the record retention policy or because of requirements to collect and retain evidence (see also 13.2.3). Other information System logs often contain a large volume of information, much of which is extraneous to security monitoring. To help identify significant events for security monitoring purposes, the copying of appropriate message types automatically to a second log, and/or the use of suitable system utilities or audit tools to perform file interrogation and rationalization should be considered. System logs need to be protected, because if the data can be modified or data in them deleted, their existence may create a false sense of security.',
  ),
  69 => 
  array (
    'fkContext' => '1869',
    'fkSectionBestPractice' => '1626',
    'nControlType' => '0',
    'sName' => '10.10.4 Administrator and operator logs',
    'tDescription' => 'System administrator and system operator activities should be logged.',
    'tImplementationGuide' => 'Logs should include: a) the time at which an event (success or failure) occurred; b) information about the event (e.g. files handled) or failure (e.g. error occurred and corrective action taken); c) which account and which administrator or operator was involved; and d) which processes were involved. System administrator and operator logs should be reviewed on a regular basis. Other information An intrusion detection system managed outside of the control of system and network administrators can be used to monitor system and network administration activities for compliance.',
  ),
  70 => 
  array (
    'fkContext' => '1870',
    'fkSectionBestPractice' => '1626',
    'nControlType' => '0',
    'sName' => '10.10.5 Fault logging',
    'tDescription' => 'Faults should be logged, analysed and appropriate action taken.',
    'tImplementationGuide' => 'Faults reported by users or by system programs related to problems with information processing or communications systems should be logged. There should be clear rules for handling reported faults including: a) review of fault logs to ensure that faults have been satisfactorily resolved; b) review of corrective measures to ensure that controls have not been compromised, and that the action taken is fully authorized. It should be ensured that error logging is enabled, if this system function is available. Other information Logging of errors and faults can impact the performance of a system. Such logging should be enabled by competent personnel, and the level of logging required for individual systems should be determined by a risk assessment, taking performance degradation into account.',
  ),
  71 => 
  array (
    'fkContext' => '1871',
    'fkSectionBestPractice' => '1626',
    'nControlType' => '0',
    'sName' => '10.10.6 Clock synchronization',
    'tDescription' => 'The clocks of all security relevant information processing systems within an organization or security domain should be synchronized with an agreed accurate time source.',
    'tImplementationGuide' => 'Where a computer or communications device has the capability to operate a real-time clock, this clock should be set to an agreed standard, e.g. Universal Co-ordinated Time (UCT) or local standard time. As some clocks are known to drift with time, there should be a procedure that checks for and corrects any significant variation. The correct interpretation of the date/time format is important to ensure that the timestamp reflects the real date/time. Local specifics (e.g. daylight savings) should be taken into account. Other information The correct setting of computer clocks is important to ensure the accuracy of audit logs, which may be required for investigations or as evidence in legal or disciplinary cases. Inaccurate audit logs may hinder such investigations and damage the credibility of such evidence. A clock linked to a radio time broadcast from a national atomic clock can be used as the master clock for logging systems. A network time protocol can be used to keep all of the servers in synchronisation with the master clock.',
  ),
  72 => 
  array (
    'fkContext' => '1872',
    'fkSectionBestPractice' => '1628',
    'nControlType' => '0',
    'sName' => '11.1.1 Access control policy',
    'tDescription' => 'An access control policy should be established, documented and reviewed based on business and security requirements for access.',
    'tImplementationGuide' => 'Access control rules and rights for each user or group of users should be clearly stated in an access control policy. Access controls are both logical and physical (see also section 9) and these should be considered together. Users and service providers should be given a clear statement of the business requirements to be met by access controls. The policy should take account of the following: a) security requirements of individual business applications; b) identification of all information related to the business applications and the risks the information is facing; c) policies for information dissemination and authorization, e.g. the need to know principle and security levels and classification of information (see 7.2); d) consistency between the access control and information classification policies of different systems and networks; e) relevant legislation and any contractual obligations regarding protection of access to data or services (see 15.1); f) standard user access profiles for common job roles in the organization; g) management of access rights in a distributed and networked environment which recognizes all types of connections available; h) segregation of access control roles, e.g. access request, access authorization, access administration; i) requirements for formal authorization of access requests (see 11.2.1); j) requirements for periodic review of access controls (see 11.2.4); k) removal of access rights (see 8.3.3). Other information Care should be taken when specifying access control rules to consider: i) differentiating between rules that must always be enforced and guidelines that are optional or conditional; ii) establishing rules based on the premise \'\'Everything is generally forbidden unless expressly permitted\'\' rather than the weaker rule \'\'Everything is generally permitted unless expressly forbidden\'\'; iii) changes in information labels (see 7.2) that are initiated automatically by information processing facilities and those initiated at the discretion of a user; iv) changes in user permissions that are initiated automatically by the information system and those initiated by an administrator; v) rules, which require specific approval before enactment and those, which do not. Access control rules should be supported by formal procedures and clearly defined responsibilities (see, for example, 6.1.3, 11.3, 10.4.1, 11.6).',
  ),
  73 => 
  array (
    'fkContext' => '1873',
    'fkSectionBestPractice' => '1629',
    'nControlType' => '0',
    'sName' => '11.2.1 User registration',
    'tDescription' => 'There should be a formal user registration and de-registration procedure for granting and revoking access to all information systems and services.',
    'tImplementationGuide' => 'The access control procedure for user registration and de-registration should include: a) using unique user IDs to enable users to be linked to and held responsible for their actions; the use of group IDs should only be permitted where they are necessary for business or operational reasons, and should be approved and documented; b) checking that the user has authorization from the system owner for the use of the information system or service; separate approval for access rights from management may also be appropriate; c) checking that the level of access granted is appropriate to the business purpose (see 11.1) and is consistent with organizational security policy, e.g. it does not compromise segregation of duties (see 10.1.3); d) giving users a written statement of their access rights; e) requiring users to sign statements indicating that they understand the conditions of access; f) ensuring service providers do not provide access until authorization procedures have been completed; g) maintaining a formal record of all persons registered to use the service; h) immediately removing or blocking access rights of users who have changed roles or jobs or left the organization; i) periodically checking for, and removing or blocking, redundant user IDs and accounts (see 11.2.4); Other information Consideration should be given to establish user access roles based on business requirements that summarize a number of access rights into typical user access profiles. Access requests and reviews (see 11.2.4) are easier managed at the level of such roles than at the level of particular rights. Consideration should be given to including clauses in personnel contracts and service contracts that specify sanctions if unauthorized access is attempted by personnel or service agents (see also 6.1.5, 8.1.3 and 8.2.3).',
  ),
  74 => 
  array (
    'fkContext' => '1874',
    'fkSectionBestPractice' => '1629',
    'nControlType' => '0',
    'sName' => '11.2.2 Privilege management',
    'tDescription' => 'The allocation and use of privileges should be restricted and controlled.',
    'tImplementationGuide' => 'Multi-user systems that require protection against unauthorized access should have the allocation of privileges controlled through a formal authorization process. The following steps should be considered: a) the access privileges associated with each system product, e.g. operating system, database management system and each application, and the users to which they need to be allocated should be identified; b) privileges should be allocated to users on a need-to-use basis and on an event-by-event basis in line with the access control policy (11.1.1), i.e. the minimum requirement for their functional role only when needed; c) an authorization process and a record of all privileges allocated should be maintained. Privileges should not be granted until the authorization process is complete; d) the development and use of system routines should be promoted to avoid the need to grant privileges to users; e) the development and use of programs, which avoid the need to run with privileges should be promoted; f) privileges should be assigned to a different user ID from those used for normal business use. Other information Inappropriate use of system administration privileges (any feature or facility of an information system that enables the user to override system or application controls) is often found to be a major contributory factor to the failure of systems that have been breached. ',
  ),
  75 => 
  array (
    'fkContext' => '1875',
    'fkSectionBestPractice' => '1629',
    'nControlType' => '0',
    'sName' => '11.2.3 User password management',
    'tDescription' => 'The allocation of passwords should be controlled through a formal management process.',
    'tImplementationGuide' => 'The process should include the following requirements: a) users should be required to sign a statement to keep personal passwords confidential and work group passwords solely within the members of the group; this signed statement could be included in the terms and conditions of employment (see 8.1.3); b) when users are required to maintain their own passwords they should be provided initially with a secure temporary password (see 11.3.1), which they are forced to change immediately; c) establish procedures to verify the identity of a user prior to providing a new, replacement or temporary password; d) temporary passwords should be given to users in a secure manner; the use of third parties or unprotected (clear text) electronic mail messages should be avoided; e) temporary passwords should be unique to an individual and should not be guessable; f) users should acknowledge receipt of passwords; g) passwords should never be stored on computer systems in an unprotected form; h) default vendor passwords should be altered following installation of systems or software. Other information Passwords are a common means of verifying a user\'\'s identity before access is given to an information system or service according to the user\'\'s authorization. Other technologies for user identification and authentication, such as biometrics, e.g. finger-print verification, signature verification and use of hardware tokens, e.g. smart cards, are available, and should be considered if appropriate.',
  ),
  76 => 
  array (
    'fkContext' => '1876',
    'fkSectionBestPractice' => '1629',
    'nControlType' => '0',
    'sName' => '11.2.4 Review of user access rights',
    'tDescription' => 'Management should review users\'\' access rights at regular intervals using a formal process.',
    'tImplementationGuide' => 'The review of access rights should consider the following guidelines: a) users\'\' access rights should be reviewed at regular intervals, e.g. a period of 6 months, and after any changes, such as promotion, demotion, termination of employment (see 11.2.1); b) user access rights should also be reviewed and re-allocated when moving from one employment to another within the same organization; c) authorizations for special privileged access rights (see 11.2.2) should be reviewed at more frequent intervals, e.g. at a period of 3 months; d) privilege allocations should be checked at regular intervals to ensure that unauthorized privileges have not been obtained; e) changes to privileged accounts should be logged for periodic review. Other information It is necessary to regularly review users\'\' access rights to maintain effective control over access to data and information services.',
  ),
  77 => 
  array (
    'fkContext' => '1877',
    'fkSectionBestPractice' => '1630',
    'nControlType' => '0',
    'sName' => '11.3.1 Password use',
    'tDescription' => 'Users should be required to follow good security practices in the selection and use of passwords.',
    'tImplementationGuide' => 'All users should be advised to: a) keep passwords confidential; b) avoid keeping a record (e.g. paper, software file or hand-held device) of passwords, unless this can be stored securely and the method of storing has been approved; c) change passwords whenever there is any indication of possible system or password compromise; d) select quality passwords with sufficient minimum length which are: 1) easy to remember; 2) not based on anything somebody else could easily guess or obtain using person related information, e.g. names, telephone numbers, and dates of birth etc.; 3) not vulnerable to dictionary attacks (i.e. do not consist of words included in dictionaries); 4) free of consecutive identical, all-numeric or all-alphabetic characters. e) change passwords at regular intervals or based on the number of accesses (passwords for privileged accounts should be changed more frequently than normal passwords), and avoid re-using or cycling old passwords; f) change temporary passwords at the first log-on; g) not include passwords in any automated log-on process, e.g. stored in a macro or function key; h) not share individual user passwords; i) not use the same password for business and non-business purposes. If users need to access multiple services, systems or platforms and are required to maintain multiple separate passwords, they should be advised that they may use a single, quality password (see d) above) for all services where the user is assured that a reasonable level of protection has been established for the storage of the password within each service, system or platform. Other information Management of the help desk system dealing with lost or forgotten passwords needs special care as this may also be a means of attack to the password system. ',
  ),
  78 => 
  array (
    'fkContext' => '1878',
    'fkSectionBestPractice' => '1630',
    'nControlType' => '0',
    'sName' => '11.3.2 Unattended user equipment',
    'tDescription' => 'Users should ensure that unattended equipment has appropriate protection.',
    'tImplementationGuide' => 'All users should be made aware of the security requirements and procedures for protecting unattended equipment, as well as their responsibilities for implementing such protection. Users should be advised to: a) terminate active sessions when finished, unless they can be secured by an appropriate locking mechanism, e.g. a password protected screen saver; b) log-off mainframe computers, servers and office PCs when the session is finished (i.e. not just switch off the PC screen or terminal); c) secure PCs or terminals from unauthorized use by a key lock or an equivalent control, e.g. password access, when not in use (see also 11.3.3). Other information Equipment installed in user areas, e.g. workstations or file servers, may require specific protection from unauthorized access when left unattended for an extended period.',
  ),
  79 => 
  array (
    'fkContext' => '1879',
    'fkSectionBestPractice' => '1630',
    'nControlType' => '0',
    'sName' => '11.3.3 Clear desk and clear screen policy',
    'tDescription' => 'Organizations should adopt a clear desk policy for papers and removable storage media and a clear screen policy for information processing facilities.',
    'tImplementationGuide' => 'The clear desk and clear screen policy should take into account the information classifications (see 7.2), legal and contractual requirements (see 15.1), and the corresponding risks and cultural aspects of the organization. The following guidelines should be considered: a) sensitive or critical business information, e.g. on paper or on electronic stoarge media, should be locked away (ideally in a safe or cabinet or other forms of security furniture) when not required, especially when the office is vacated; b) computers and terminals should be left logged off or protected with a screen and keyboard locking mechanism controlled by a password, token or similar user authentication mechanism when unattended and should be protected by key locks, passwords or other controls when not in use; c) incoming and outgoing mail points and unattended fax machines should be protected; d) unauthorised use of photocopiers and other reproduction technology (e.g., scanners, digital cameras) should be prevented; e) documents containing sensitive or classified information should be removed from printers immediately. Other information A clear desk/clear screen policy reduces the risks of unauthorized access, loss of, and damage to information during and outside normal working hours. Safes or other forms of secure storage facilities might also protect information stored therein against disasters such as a fire, earthquake, flood or explosion. Consider the use of printers with pin code function, so the originators are the only ones who can get their print outs, and only when standing next to the printer.',
  ),
  80 => 
  array (
    'fkContext' => '1880',
    'fkSectionBestPractice' => '1631',
    'nControlType' => '0',
    'sName' => '11.4.1 Policy on use of network services',
    'tDescription' => 'Users should only be provided with access to the services that they have been specifically authorized to use.',
    'tImplementationGuide' => 'A policy should be formulated concerning the use of networks and network services. This policy should cover: a) the networks and network services which are allowed to be accessed; b) authorization procedures for determining who is allowed to access which networks and networked services; c) management controls and procedures to protect access to network connections and network services; d) the means used to access networks and network services (e.g. the conditions for allowing dial-up access to an Internet service provider or remote system). The policy on he use of network services should be consistent with the business access control policy (see 11.1). Other information Unauthorized and insecure connections to network services can affect the whole organization. This control is particularly important for network connections to sensitive or critical business applications or to users in high-risk locations, e.g. public or external areas that are outside the organization\'\'s security management and control.',
  ),
  81 => 
  array (
    'fkContext' => '1881',
    'fkSectionBestPractice' => '1631',
    'nControlType' => '0',
    'sName' => '11.4.2 User authentication for external connections',
    'tDescription' => 'Appropriate authentication methods should be used to control access by remote users.',
    'tImplementationGuide' => 'Authentication of remote users can be achieved using, for example, a cryptographic based technique, hardware tokens, or a challenge/response protocol. Possible implementations of such techniques can be found in various virtual private network (VPN) solutions. Dedicated private lines can also be used to provide assurance of the source of connections. Dial-back procedures and controls, e.g. using dial-back modems, can provide protection against unauthorized and unwanted connections to an organization\'\'s information processing facilities. This type of control authenticates users trying to establish a connection to an organization\'\'s network from remote locations. When using this control, an organization should not use network services, which include call forwarding, or, if they do, they should disable the use of such features to avoid weaknesses associated with call forwarding. The call back process should ensure that an actual disconnection on the organization\'\'s side occurs. Otherwise, the remote user could hold the line open pretending that the call back verification has occurred. Call back procedures and controls should be thoroughly tested for this possibility. Node authentication can serve as an alternative means of authenticating groups of remote users where they are connected to a secure, shared computer facility. Cryptographic techniques, e.g. based on machine certificates, can be used for node authentication. This is part of several VPN based solutions. Additional authentication controls should be implemented to control access to wireless networks. In particular, special care is needed in the selection of controls for wireless networks due to the greater opportunities for undetected interception and insertion of network traffic. Other information External connections provide a potential for unauthorized access to business information, e.g. access by dial-up methods. There are different types of authentication method, some of these provide a greater level of protection than others, e.g. methods based on the use of cryptographic techniques can provide strong authentication. It is important to determine from a risk assessment the level of protection required. This is needed for the appropriate selection of an authentication method. A facility for automatic connection to a remote computer could provide a way of gaining unauthorized access to a business application. This is especially important if the connection uses a network that is outside the control of the organization\'\'s security management.',
  ),
  82 => 
  array (
    'fkContext' => '1882',
    'fkSectionBestPractice' => '1631',
    'nControlType' => '0',
    'sName' => '11.4.3 Equipment identification in the network',
    'tDescription' => 'Automatic equipment identification should be considered as a means to authenticate connections from specific locations and equipment.',
    'tImplementationGuide' => 'Equipment identification can be used if it is important that the communication can only be initiated from a specific location or equipment. An identifier in, or attached to, the equipment can be used to indicate whether this equipment is permitted to connect to the network. These identifiers should clearly indicate to which network the equipment is permitted to connect, if more than one network exists and particularly if these networks are of differing sensitivity. It may be necessary to consider physical protection of the equipment to maintain the security of the equipment identifier. Other information This control can be complemented with other techniques to authenticate the equipment\'\'s user (see 11.4.2). Equipment identification can be applied additionally to user authentication.',
  ),
  83 => 
  array (
    'fkContext' => '1883',
    'fkSectionBestPractice' => '1631',
    'nControlType' => '0',
    'sName' => '11.4.4 Remote diagnostic and configuration port protection',
    'tDescription' => 'Physical and logical access to diagnostic and configuration ports should be controlled.',
    'tImplementationGuide' => 'Potential controls for the access to diagnostic and configuration ports include the use of a key lock and supporting procedures to control physical access to the port. An example for such a supporting procedure is to ensure that diagnostic and configuration ports are only accessible by arrangement between the manager of the computer service and the hardware/software support personnel requiring access. Ports, services and similar facilities installed on a computer or network facility, which are not specifically required for business functionality, should be disabled or removed. Other information Many computer systems, network systems and communication systems are installed with a remote diagnostic or configuration facility for use by maintenance engineers. If unprotected, these diagnostic ports provide a means of unauthorized access.',
  ),
  84 => 
  array (
    'fkContext' => '1884',
    'fkSectionBestPractice' => '1631',
    'nControlType' => '0',
    'sName' => '11.4.5 Segregation in networks',
    'tDescription' => 'Groups of information services, users and information systems should be segregated on networks.',
    'tImplementationGuide' => 'One method of controlling the security of large networks is to divide them into separate logical network domains, e.g. an organization\'\'s internal network domains and external network domains, each protected by a defined security perimeter. A graduated set of controls can be applied in different logical network domains to further segregate the network security environments, e.g. publicly accessible systems, internal networks and critical assets. The domains should be defined based on a risk assessment and the different security requirements within each of the domains. Such a network perimeter can be implemented by installing a secure gateway between the two networks to be interconnected to control access and information flow between the two domains. This gateway should be configured to filter traffic between these domains (see 11.4.6 and 11.4.7) and to block unauthorized access in accordance with the organization\'\'s access control policy (see 11.1). An example of this type of gateway is what is commonly referred to as a firewall. Another method of segregating separate logical domains is to restrict network access by using virtual private networks for user groups within the organization. Networks can also be segregated using the network device functionality, e.g. IP switching. Separate domains can then be implemented by controlling the network data flows using the routing/switching capabilities, such as access control lists. The criteria for segregation of networks into domains should be based on the access control policy and access requirements (see 10.1), and also take account of the relative cost and performance impact of incorporating suitable network routing or gateway technology (see 11.4.6 and 11.4.7). In addition, segregation of networks should be based on the value of assets and classification of information stored or processed in the network, levels of trust, or lines of business, in order to reduce the total impact of a service disruption. Consideration should be given to the segregation of wireless networks from internal and private networks. As the perimeters of wireless networks are not well defined, a risk assessment should be carried out in such cases to identify controls (e.g. strong authentication, cryptographic methods, frequency selection) to maintain network segregation. Other information Networks are increasingly being extended beyond traditional organizational boundaries, as business partnerships are formed that may require the interconnection or sharing of information processing and networking facilities. Such extensions might increase the risk of unauthorized access to existing information systems that use the network, some of which may require protection from other network users because of their sensitivity or criticality.',
  ),
  85 => 
  array (
    'fkContext' => '1885',
    'fkSectionBestPractice' => '1631',
    'nControlType' => '0',
    'sName' => '11.4.6 Network connection control',
    'tDescription' => 'For shared networks, especially those extending across organizational boundaries, the capability of users to connect to the network should be restricted, in line with the access control policy and requirements of the business applications (see 11.1).',
    'tImplementationGuide' => 'The network access rights of users should be maintained and updated as required by the access control policy (see 11.1.1). means of pre-defined tables or rules. Examples of applications to which restrictions should be applied are: a) messaging, e.g. electronic mail; b) file transfer; c) interactive access; d) application access. Linking network access rights to certain times of day or dates should be considered. Other information The incorporation of controls to restrict the connection capability of the users may be required by the access control policy for shared networks, especially those extending across organizational boundaries.',
  ),
  86 => 
  array (
    'fkContext' => '1886',
    'fkSectionBestPractice' => '1631',
    'nControlType' => '0',
    'sName' => '11.4.7 Network routing control',
    'tDescription' => 'Routing controls should be implemented for networks to ensure that computer connections and information flows do not breach the access control policy of the business applications.',
    'tImplementationGuide' => 'Routing controls should be based on positive source and destination address checking mechanisms. Security gateways can be used to validate source and destination addresses at internal and external network control points if proxy and/or network address translation technologies are employed. Implementers should be aware of the strength and shortcomings of any mechanisms deployed. The requirements for network routing control should be based on the access control policy (see 11.1). Other information Shared networks, especially those extending across organizational boundaries, may require additional routing controls. This particularly applies where networks are shared with third party (non-organization) users.',
  ),
  87 => 
  array (
    'fkContext' => '1887',
    'fkSectionBestPractice' => '1632',
    'nControlType' => '0',
    'sName' => '11.5.1 Secure log-on procedures',
    'tDescription' => 'Access to information systems should be controlled by a secure log-on procedure. ',
    'tImplementationGuide' => 'The procedure for logging into an information system should be designed to minimize the opportunity for unauthorized access. The log-on procedure should therefore disclose the minimum of information about the system, in order to avoid providing an unauthorized user with any unnecessary assistance. A good log-on procedure should: a) not display system or application identifiers until the log-on process has been successfully completed; b) display a general notice warning that the computer should only be accessed by authorized users; c) not provide help messages during the log-on procedure that would aid an unauthorized user; d) validate the log-on information only on completion of all input data. If an error condition arises, the system should not indicate which part of the data is correct or incorrect; e) limit the number of unsuccessful log-on attempts allowed, e.g. to three attempts, and consider: 1) recording unsuccessful and successful attempts; 2) forcing a time delay before further log-on attempts are allowed or rejecting any further attempts without specific authorization; 3) disconnecting data link connections; 4) sending an alarm message to the system console if the maximum number of log-on attempts is reached; 5) setting the number of password retries in conjunction with the minimum length of the password and the value of the system being protected; f) limit the maximum and minimum time allowed for the log-on procedure. If exceeded, the system should terminate the log-on; g) display the following information on completion of a successful log-on: 1) date and time of the previous successful log-on; 2) details of any unsuccessful log-on attempts since the last successful log-on; h) not display the password being entered or consider hiding the password characters by symbols; i) not transmit passwords in clear text over a network. Other information If passwords are transmitted in clear during the log-on session over a network, they may be captured by a network \'\'sniffer\'\' program on the network.',
  ),
  88 => 
  array (
    'fkContext' => '1888',
    'fkSectionBestPractice' => '1632',
    'nControlType' => '0',
    'sName' => '11.5.2 User identification and authentication',
    'tDescription' => 'All users should have a unique identifier (user ID) for their personal and sole use, and a suitable authentication technique should be chosen to substantiate the claimed identity of a user.',
    'tImplementationGuide' => 'This control should be applied for all types of users (including technical support personnel, operators, network administrators, system programmers and database administrators). User IDs should be used to trace activities to the responsible individual. Regular user activities should not be performed from privileged accounts. In exceptional circumstances, where there is a clear business benefit, the use of a shared user ID for a group of users or a specific job can be used. Approval by management should be documented for such cases. Additional controls may be required to maintain accountability. Generic IDs for use by an individual should only be allowed either where the functions accessible or actions carried out by the ID do not need to be traced (e.g. read only access), or where there are other controls in place (e.g. password for a generic ID only issued to one staff at a time and logging such instance). Where strong authentication and identity verification is required, authentication methods alternative to passwords, such as cryptographic means, smart cards, tokens or biometric means, should be used. Other information Passwords (see also 11.3.1 and 11.5.3) are a very common way to provide identification and authentication (I and A) based on a secret that only the user knows. The same can also be achieved with cryptographic means and authentication protocols. The strength of user identification and authentication should be suitable to the sensitivity of the information to be accessed. Objects such as memory tokens or smart cards that users possess can also be used for I and A. Biometric authentication technologies that use the unique characteristics or attributes of an individual can also be used to authenticate the person\'\'s identity. A combination of technologies and mechanisms securely linked will result in stronger authentication.',
  ),
  89 => 
  array (
    'fkContext' => '1889',
    'fkSectionBestPractice' => '1632',
    'nControlType' => '0',
    'sName' => '11.5.3 Password management system',
    'tDescription' => 'Password management systems should provide an effective, interactive facility, which ensures quality passwords.',
    'tImplementationGuide' => 'A password management system should: a) enforce the use of individual users IDs and passwords to maintain accountability; b) allow users to select and change their own passwords and include a confirmation procedure to allow for input errors; c) enforce a choice of quality passwords (see 11.3.1); d) enforce password changes (see 11.3.1); e) force users to change temporary passwords at the first log-on (see 11.2.3); f) maintain a record of previous user passwords, e.g. for the previous 12 months, and prevent re-use; g) not display passwords on the screen when being entered; h) store password files separately from application system data; i) store and transmit passwords in protected (e.g. encrypted or hashed) form. Other information Passwords are one of the principal means of validating a user\'\'s authority to access a computer service. Some applications require user passwords to be assigned by an independent authority; in such cases, points b), d) and e) of the above guidance do not apply. In most cases the passwords are selected and maintained by users. See section 11.3.1 for guidance on the use of passwords.',
  ),
  90 => 
  array (
    'fkContext' => '1890',
    'fkSectionBestPractice' => '1632',
    'nControlType' => '0',
    'sName' => '11.5.4 Use of system utilities',
    'tDescription' => 'The use of utility programs that might be capable of overriding system and application controls should be restricted and tightly controlled.',
    'tImplementationGuide' => 'The following guidelines for the use of system utilities should be considered: a) use of identification, authentication and authorization procedures for system utilities; b) segregation of system utilities from applications software; c) limitation of the use of system utilities to the minimum practical number of trusted, authorized users (see also 11.2.2); d) authorization for ad hoc use of systems utilities; e) limitation of the availability of system utilities, e.g. for the duration of an authorized change; f) logging of all use of system utilities; g) defining and documenting of authorization levels for system utilities; h) removal or disabling of all unnecessary software based utilities and system software; i) not making system utilities available to users who have access to applications on systems where segregation of duties is required. Other information Most computer installations have one or more system utility programs that might be capable of overriding system and application controls.',
  ),
  91 => 
  array (
    'fkContext' => '1891',
    'fkSectionBestPractice' => '1632',
    'nControlType' => '0',
    'sName' => '11.5.5 Session time-out',
    'tDescription' => 'Inactive sessions should shut down after a defined period of inactivity.',
    'tImplementationGuide' => 'A time-out facility should clear the session screen and also, possibly later, close both application and network sessions after a defined period of inactivity. The time-out delay should reflect the security risks of the area, the classification of the information being handled and the applications being used, and the risks related to the users of the equipment. A limited form of time-out facility can be provided for some PCs, which clears the screen and prevents unauthorized access but does not close down the application or network sessions. Other information This control is particularly important in high risk locations, which include public or external areas outside the organization\'\'s security management. The sessions should be shut down to prevent access by unauthorized persons and denial of service attacks.',
  ),
  92 => 
  array (
    'fkContext' => '1892',
    'fkSectionBestPractice' => '1632',
    'nControlType' => '0',
    'sName' => '11.5.6 Limitation of connection time',
    'tDescription' => 'Restrictions on connection times should be used to provide additional security for high-risk applications.',
    'tImplementationGuide' => 'Connection time controls should be considered for sensitive computer applications, especially from high risk locations, e.g. public or external areas that are outside the organization\'\'s security management. Examples of such restrictions include: a) using predetermined time slots, e.g. for batch file transmissions, or regular interactive sessions of short duration; b) restricting connection times to normal office hours if there is no requirement for overtime or extended-hours operation; c) re-authentication at timed intervals should be considered. Other information Limiting the period during which connections to computer services are allowed reduces the window of opportunity for unauthorized access. Limiting the duration of active sessions prevents users from holding sessions open to prevent re-authenticating.',
  ),
  93 => 
  array (
    'fkContext' => '1893',
    'fkSectionBestPractice' => '1633',
    'nControlType' => '0',
    'sName' => '11.6.1 Information access restriction',
    'tDescription' => 'Access to information and application system functions by users and support personnel should be restricted in accordance with the defined access control policy.',
    'tImplementationGuide' => 'Restrictions to access should be based on individual business application requirements. The access control policy should also be consistent with the organizational access policy (see section 11.1). Applying the following guidelines should be considered in order to support access restriction requirements: a) providing menus to control access to application system functions; b) controlling the access rights of users, e.g. read, write, delete and execute; c) controlling access rights of other applications; d) ensuring that outputs from application systems handling sensitive information contain only the information relevant to the use of the output and are sent only to authorized terminals and locations; this should include periodic reviews of such outputs to ensure that redundant information is removed.',
  ),
  94 => 
  array (
    'fkContext' => '1894',
    'fkSectionBestPractice' => '1633',
    'nControlType' => '0',
    'sName' => '11.6.2 Sensitive system isolation',
    'tDescription' => 'Sensitive systems should have a dedicated (isolated) computing environment.',
    'tImplementationGuide' => 'The following points should be considered for sensitive system isolation: a) the sensitivity of an application system should be explicitly identified and documented by the application owner (see 7.1.2); b) when a sensitive application is to run in a shared environment, the application systems with which it will share resources and the corresponding risks should be identified and accepted by the owner of the sensitive application. Other information Some application systems are sufficiently sensitive to potential loss that they require special handling. The sensitivity may indicate that the application system: i) should run on a dedicated computer; or ii) should only share resources with trusted applications systems. Isolation could be achieved using physical or logical methods (see also 11.4.5).',
  ),
  95 => 
  array (
    'fkContext' => '1895',
    'fkSectionBestPractice' => '1634',
    'nControlType' => '0',
    'sName' => '11.7.1 Mobile computing and communications',
    'tDescription' => 'A formal policy should be in place and appropriate security measures should be adopted to protect against the risks of using mobile computing and communication facilities.',
    'tImplementationGuide' => 'When using mobile computing and communicating facilities, e.g. notebooks, palmtops, laptops, smart cards and mobile phones, special care should be taken to ensure that business information is not compromised. The mobile computing policy should take into account the risks of working with mobile computing equipment in unprotected environments. The mobile computing policy should include the requirements for physical protection, access controls, cryptographic techniques, back-ups, and virus protection. This policy should also include rules and advice on connecting mobile facilities to networks and guidance on the use of these facilities in public places. Care should be taken when using mobile computing facilities in public places, meeting rooms and other unprotected areas outside of the organization\'\'s premises. Protection should be in place to avoid the unauthorized access to or disclosure of the information stored and processed by these facilities, e.g. using cryptographic techniques (see 12.3). Users of mobile computing facilities in public places should take care to avoid the risk of overlooking by unauthorized persons. Procedures against malicious software should be in place and be kept up to date (see 10.4). Back-ups of critical business information should be taken regularly. Equipment should be available to enable the quick and easy back-up of information. These back-ups should be given adequate protection against, e.g., theft or loss of information. Suitable protection should be given to the use of mobile facilities connected to networks. Remote access to business information across public network using mobile computing facilities should only take place after successful identification and authentication, and with suitable access control mechanisms in place (see 11.4). Mobile computing facilities should also be physically protected against theft especially when left, for example, in cars and other forms of transport, hotel rooms, conference centres and meeting places. A specific procedure taking into account legal, insurance and other security requirements of the organization should be established for cases of theft or loss of the mobile computing facilities. Equipment carrying important, sensitive and/or critical business information should not be left unattended and, where possible, should be physically locked away, or special locks should be used to secure the equipment (see 9.2.5). Training should be arranged for personnel using mobile computing to raise their awareness on the additional risks resulting from this way of working and the controls that should be implemented. Other information Mobile network wireless connections are similar to other types of network connection, but have important differences that should be considered when identifying controls. Typical differences are 1) some wireless security protocols are immature and have known weaknesses and 2) information stored on mobile computers may not be backed-up because of limited network bandwidth and/or because mobile equipment may not be connected at the times when back-ups are scheduled.',
  ),
  96 => 
  array (
    'fkContext' => '1896',
    'fkSectionBestPractice' => '1634',
    'nControlType' => '0',
    'sName' => '11.7.2 Teleworking',
    'tDescription' => 'A policy, operational plans and procedures should be developed for teleworking activities.',
    'tImplementationGuide' => 'Organizations should only authorize teleworking activities if they are satisfied that appropriate security arrangements and controls are in place and that these comply with the organization\'\'s security policy. Suitable protection of the teleworking site should be in place against, e.g., the theft of equipment and information, the unauthorized disclosure of information, unauthorized remote access to the organization\'\'s internal systems or misuse of facilities. Teleworking activities should both be authorized and controlled by management, and it should be ensured that suitable arrangements are in place for this way of working. The following matters should be considered: a) the existing physical security of the teleworking site, taking into account the physical security of the building and the local environment; b) the proposed physical teleworking environment; c) the communications security requirements, taking into account the need for remote access to the organization\'\'s internal systems, the sensitivity of the information that will be accessed and pass over the communication link and the sensitivity of the internal system; d) the threat of unauthorized access to information or resources from other persons using the accommodation, e.g. family and friends; e) the use of home networks and requirements or restrictions on the configuration of wireless network services; f) policies and procedures to prevent disputes concerning rights to intellectual property developed on privately owned equipment; g) access to privately owned equipment (to check the security of the machine or during an investigation), which may be prevented by legislation; h) software licensing agreements that are such that organizations may become liable for licensing for client software on workstations owned privately by employees, contractors or third party users; i) anti-virus protection and firewall requirements. The guidelines and arrangements to be considered should include: i) the provision of suitable equipment and storage furniture for the teleworking activities, where the use of privately owned equipment that is not under the control of the organization is not allowed; ii) a definition of the work permitted, the hours of work, the classification of information that may be held and the internal systems and services that the teleworker is authorized to access; iii) the provision of suitable communication equipment, including methods for securing remote access; iv) physical security; v) rules and guidance on family and visitor access to equipment and information; vi) the provision of hardware and software support and maintenance; vii) the provision of insurance; viii) the procedures for back-up and business continuity; ix) audit and security monitoring; x) revocation of authority and access rights, and the return of equipment when the teleworking activities are terminated. Other information Teleworking uses communications technology to enable personnel to work remotely from a fixed location outside of their organization.',
  ),
  97 => 
  array (
    'fkContext' => '1897',
    'fkSectionBestPractice' => '1636',
    'nControlType' => '0',
    'sName' => '12.1.1 Security requirements analysis and specification',
    'tDescription' => 'Statements of business requirements for new information systems, or enhancements to existing information systems, should specify the requirements for security controls.',
    'tImplementationGuide' => 'Specifications for the requirements for controls should consider the automated controls to be incorporated in the information system, and the need for supporting manual controls. Similar considerations should be applied when evaluating software packages for business applications and when evaluating software packages, developed or purchased, for business applications. Security requirements and controls should reflect the business value of the information assets involved (see also 7.2), and the potential business damage, which might result from a failure or absence of security. System requirements for information security and processes for implementing security should be integrated in the early stages of information system projects. Controls introduced at the design stage are significantly cheaper to implement and maintain than those included during or after implementation. If products are purchased, a formal testing and acquisition process should be followed. Contracts with the supplier should address the identified security requirements. Where the security functionality in a proposed product does not satisfy the specified requirement then the risk introduced and associated controls should be reconsidered prior to purchasing the product. Where additional functionality is supplied and causes a security risk, this should be disabled or the proposed control structure should be reviewed to determine if advantage can be taken of the enhanced functionality available. Other information If considered appropriate, for example for cost reasons, management may wish to make use of independently evaluated and certified products. Further information about evaluation criteria for IT security products can be found in ISO/IEC 15408 or other evaluation or certification standards, as appropriate. ISO/IEC TR 13335-3 provides guidance on the use of risk management processes to identify requirements for security controls. ',
  ),
  98 => 
  array (
    'fkContext' => '1898',
    'fkSectionBestPractice' => '1637',
    'nControlType' => '0',
    'sName' => '12.2.1 Input data validation',
    'tDescription' => 'Data input to applications should be validated to ensure that this data is correct and appropriate.',
    'tImplementationGuide' => 'Checks should be applied to the input of business transactions, standing data (names and addresses, credit limits, customer reference numbers) and parameter tables (sales prices, currency conversion rates, tax rates). The following guidelines should be considered: a) dual input or other input checks, such as boundary checking or limiting fields to specific ranges of input data, to detect the following errors: 1) out-of-range values; 2) invalid characters in data fields; 3) missing or incomplete data; 4) exceeding upper and lower data volume limits; 5) unauthorized or inconsistent control data; b) periodic review of the content of key fields or data files to confirm their validity and integrity; c) inspecting hard-copy input documents for any unauthorized changes (all changes to input documents should be authorized); d) procedures for responding to validation errors; e) procedures for testing the plausibility of the input data; f) defining the responsibilities of all personnel involved in the data input process; g) creating a log of the activities involved in the data input process (see 10.10.1). Other information Automatic examination and validation of input data can be considered, where applicable, to reduce the risk of errors and to prevent standard attacks including buffer overflow and code injection.',
  ),
  99 => 
  array (
    'fkContext' => '1899',
    'fkSectionBestPractice' => '1637',
    'nControlType' => '0',
    'sName' => '12.2.2 Control of internal processing',
    'tDescription' => 'Validation checks should be incorporated into applications to detect any corruption of information through processing errors or deliberate acts.',
    'tImplementationGuide' => 'The design and implementation of applications should ensure that the risks of processing failures leading to a loss of integrity are minimized. Specific areas to consider include: a) the use of add, modify and delete functions to implement changes to data; b) the procedures to prevent programs running in the wrong order or running after failure of prior processing (see also 10.1.1); c) the use of appropriate programs to recover from failures to ensure the correct processing of data; d) protection against attacks using buffer overruns/overflows. An appropriate checklist should be prepared, activities documented, and the results should be kept secure. Examples of checks that can be incorporated include the following: i) session or batch controls, to reconcile data file balances after transaction updates; ii) balancing controls, to check opening balances against previous closing balances, namely: 1) run-to-run controls; 2) file update totals; 3) program-to-program controls; iii) validation of system-generated input data (see 12.2.1); iv) checks on the integrity, authenticity or any other security feature of data or software downloaded, or uploaded, between central and remote computers; v) hash totals of records and files; vi) checks to ensure that application programs are run at the correct time; vii) checks to ensure that programs are run in the correct order and terminate in case of a failure, and that further processing is halted until the problem is resolved; viii) creating a log of the activities involved in the processing (see 10.10.1). Other information Data that has been correctly entered can be corrupted by hardware errors, processing errors or through deliberate acts. The validation checks required will depend on the nature of the application and the business impact of any corruption of data.',
  ),
  100 => 
  array (
    'fkContext' => '1900',
    'fkSectionBestPractice' => '1637',
    'nControlType' => '0',
    'sName' => '12.2.3 Message integrity',
    'tDescription' => 'Requirements for ensuring authenticity and protecting message integrity in applications should be identified, and appropriate controls identified and implemented.',
    'tImplementationGuide' => 'An assessment of security risks should be carried out to determine if message integrity is required and to identify the most appropriate method of implementation. Other information Cryptographic techniques (see 12.3) can be used as an appropriate means of implementing message authentication.',
  ),
  101 => 
  array (
    'fkContext' => '1901',
    'fkSectionBestPractice' => '1637',
    'nControlType' => '0',
    'sName' => '12.2.4 Output data validation',
    'tDescription' => 'Data output from an application should be validated to ensure that the processing of stored information is correct and appropriate to the circumstances.',
    'tImplementationGuide' => 'Output validation may include: a) plausibility checks to test whether the output data is reasonable; b) reconciliation control counts to ensure processing of all data; c) providing sufficient information for a reader or subsequent processing system to determine the accuracy, completeness, precision and classification of the information; d) procedures for responding to output validation tests; e) defining the responsibilities of all personnel involved in the data output process; f) creating a log of activities in the data output validation process. Other information Typically, systems and applications are constructed on the assumption that having undertaken appropriate validation, verification and testing, the output will always be correct. However, this assumption is not always valid; i.e. systems that have been tested may still produce incorrect output under some circumstances.',
  ),
  102 => 
  array (
    'fkContext' => '1902',
    'fkSectionBestPractice' => '1638',
    'nControlType' => '0',
    'sName' => '12.3.1 Policy on the use of cryptographic controls',
    'tDescription' => 'An organization should develop a policy on its use of cryptographic controls for protection of its information.',
    'tImplementationGuide' => 'When developing a cryptographic policy the following should be considered: a) the management approach towards the use of cryptographic controls across the organization, including the general principles under which business information should be protected (see also 5.1.1); b) based on a risk assessment, the required level of protection should be identified taking into account the type, strength and quality of the encryption algorithm required; c) the use of encryption for protection of sensitive information transported by mobile or removable media, devices or across communication lines; d) the approach to key management, including methods to deal with the protection of cryptographic keys and the recovery of encrypted information in the case of lost, compromised or damaged keys; e) roles and responsibilities, e.g. who is responsible for: 1) the implementation of the policy; 2) the key management, including key generation (see also 12.3.2); f) how the appropriate level of cryptographic protection is to be determined; g) the standards to be adopted for the effective implementation throughout the organization (which solution is used for which business processes); h) the impact of using encrypted information on controls that rely upon content inspection (e.g. virus detection). When implementing the organization\'\'s cryptographic policy, consideration should be given to the regulations and national restrictions that might apply to the use of cryptographic techniques in different parts of the world and to the issues of trans-border flow of encrypted information (see also 15.1.6). Cryptographic controls can be used to achieve different security objectives, e.g. i) confidentiality: using encryption of information to protect sensitive or critical information, either stored or transmitted; ii) integrity/authenticity: using digital signatures or message authentication codes to protect the authenticity and integrity of stored or transmitted sensitive or critical information; iii) non-repudiation: using cryptographic techniques to obtain proof of the occurrence or non-occurrence of an event or action. Other information Making a decision as to whether a cryptographic solution is appropriate should be seen as part of the wider process of risk assessment and selection of controls. This assessment can then be used to determine whether a cryptographic control is appropriate, what type of control should be applied and for what purpose and business processes. A policy on the use of cryptographic controls is necessary to maximize benefits and minimize the risks of using cryptographic techniques, and to avoid inappropriate or incorrect use. When using digital signatures, consideration should be given to any relevant legislation, in particular legislation describing the conditions under which a digital signature is legally binding (see 15.1). Specialist advice should be sought to identify the appropriate level of protection and to define suitable specifications that will provide the required protection and support the implementation of a secure key management system (see also 12.3.2). ISO/IEC JTC1 SC27 has developed several standards related to cryptographic controls. Further information can also be found in IEEE P1363 and the OECD Guidelines on Cryptography.',
  ),
  103 => 
  array (
    'fkContext' => '1903',
    'fkSectionBestPractice' => '1638',
    'nControlType' => '0',
    'sName' => '12.3.2 Key management',
    'tDescription' => 'Key management should be in place to support the organization\'\'s use of cryptographic techniques.',
    'tImplementationGuide' => 'All cryptographic keys should be protected against modification, loss and destruction. In addition, secret and private keys need protection against unauthorized disclosure. Equipment used to generate, store and archive keys should be physically protected. A key management system should be based on an agreed set of standards, procedures and secure methods for: a) generating keys for different cryptographic systems and different applications; b) generating and obtaining public key certificates; c) distributing keys to intended users, including how keys should be activated when received; d) storing keys, including how authorized users obtain access to keys; e) changing or updating keys including rules on when keys should be changed and how this will be done; f) dealing with compromised keys; g) revoking keys including how keys should be withdrawn or deactivated, e.g. when keys have been compromised or when a user leaves an organization (in which case keys should also be archived); h) recovering keys that are lost or corrupted as part of business continuity management, e.g. for recovery of encrypted information; i) archiving keys, e.g. for information archived or backed up; j) destroying keys; k) logging and auditing of key management related activities. In order to reduce the likelihood of compromise, activation and deactivation dates for keys should be defined so that the keys can only be used for a limited period of time. This period of time should be dependent on the circumstances under which the cryptographic control is being used, and the perceived risk. In addition to of securely managing secret and private keys, the authenticity of public keys should also be considered. This authentication process can be done using public key certificates which are normally issued by a certification authority, which should be a recognized organization with suitable controls and procedures in place to provide the required degree of trust. The contents of service level agreements or contracts with external suppliers of cryptographic services, e.g. with a certification authority, should cover issues of liability, reliability of services and response times for the provision of services (see 6.2.3). Other information The management of cryptographic keys is essential to the effective use of cryptographic techniques. ISO/IEC 11770 provides further information on key management. The two types of cryptographic techniques are: i) secret key techniques, where two or more parties share the same key and this key is used both to encrypt and decrypt information; this key has to be kept secret since anyone having access to the key is able to decrypt all information being encrypted with that key, or to introduce unauthorized information using the key; ii) public key techniques, where each user has a key pair, a public key (which can be revealed to anyone) and a private key (which has to be kept secret); public key techniques can be used for encryption and to produce digital signatures (see also ISO/IEC 9796 and ISO/IEC 14888). There is a threat of someone forging a digital signature by replacing a user\'\'s public key with their own. This problem is addressed by the use of a public key certificate. Cryptographic techniques can also be used to protect cryptographic keys. Procedures may need to be considered for handling legal requests for access to cryptographic keys, e.g. encrypted information may need to be made available in an unencrypted form as evidence in a court case.',
  ),
  104 => 
  array (
    'fkContext' => '1904',
    'fkSectionBestPractice' => '1639',
    'nControlType' => '0',
    'sName' => '12.4.1 Control of operational software',
    'tDescription' => 'There should be procedures in place to control the installation of software on operational systems.',
    'tImplementationGuide' => 'To minimize the risk of corruption to operational systems, the following guidelines should be considered to control changes: a) the updating of the operational software, applications and program libraries should only be performed by trained administrators upon appropriate management authorization (see 12.4.3); b) operational systems should only hold approved executable code, and not development code or compilers; c) applications and operating system software should only be implemented after extensive and successful testing; the tests should include tests on usability, security, effects on other systems and user-friendliness, and should be carried out on separate systems (see also 10.1.4); it should be ensured that all corresponding program source libraries have been updated; d) a configuration control system should be used to keep control of all implemented software as well as the system documentation; e) a rollback strategy should be in place before any change is implemented; f) an audit log should be maintained of all updates to operational program libraries; g) previous versions of application software should be retained as a contingency measure; h) old versions of software should be archived, together with all required information and parameters, procedures, configuration details and supporting software for as long as the data is retained in archive. Vendor supplied software used in operational systems should be maintained at a level supported by the supplier. Any decision to upgrade to a new release should take into account the business requirements for the change, and the security of the release, i.e. the introduction of new security functionality or the number and severity of security problems affecting this version. Software patches should be applied when they can help to remove or reduce security weaknesses (see also 12.6.1). Physical or logical access should only be given to suppliers for support purposes when necessary, and with management approval. The supplier\'\'s activities should be monitored. Computer software may rely on externally supplied software and modules, which should be monitored and controlled to avoid unauthorized changes, which could introduce security weaknesses. Other information Operating systems should only be upgraded when there is a requirement to do so, for example, if the current operating system no longer supports the business requirements. Upgrades should not take place just because a new operating system is available. New versions of operating systems may be less secure, less stable and less well understood than current systems.',
  ),
  105 => 
  array (
    'fkContext' => '1905',
    'fkSectionBestPractice' => '1639',
    'nControlType' => '0',
    'sName' => '12.4.2 Protection of system test data',
    'tDescription' => 'Test data should be selected carefully, and protected and controlled.',
    'tImplementationGuide' => 'The use of operational databases containing personal information or any other sensitive information for testing purposes should be avoided. If personal or otherwise sensitive information is used for testing purposes, all sensitive details and content should be removed or modified beyond recognition before use. The following guidelines should be applied to protect operational data, when used for testing purposes: a) the access control procedures, which apply to operational application systems, should also apply to test application systems; b) there should be separate authorization each time operational information is copied to a test application system; c) operational information should be erased from a test application system immediately after the testing is complete; d) the copying and use of operational information should be logged to provide an audit trail. Other information System and acceptance testing usually requires substantial volumes of test data that are as close as possible to operational data.',
  ),
  106 => 
  array (
    'fkContext' => '1906',
    'fkSectionBestPractice' => '1639',
    'nControlType' => '0',
    'sName' => '12.4.3 Access control to program source code',
    'tDescription' => 'Access to program source code should be restricted.',
    'tImplementationGuide' => 'Access to program source code and associated items (such as designs, specifications, verification plans and validation plans) should be strictly controlled, in order to prevent the introduction of unauthorized functionality and to avoid unintentional changes. For program source code, this can be achieved by controlled central storage of such code, preferably in program source libraries. The following guidelines should then be considered (see also 11) to control access to such program source libraries in order to reduce the potential for corruption of computer programs: a) where possible, program source libraries should not be held in operational systems; b) the program source code and the program source libraries should be managed according to established procedures; c) support personnel should not have unrestricted access to program source libraries; d) the updating of program source libraries and associated items, and the issuing of program sources to programmers should only be performed after appropriate authorization has been received; e) program listings should be held in a secure environment (see 10.7.4); f) an audit log should be maintained of all accesses to program source libraries; g) maintenance and copying of program source libraries should be subject to strict change control procedures (see 12.5.1). Other information Program source code is code written by programmers, which is compiled (and linked) to create executables. Certain programming languages do not formally distinguish between source code and executables as the executables are created at the time they are activated. The standards ISO 10007 and ISO/IEC 12207 provide further information about configuration management and the software lifecycle process.',
  ),
  107 => 
  array (
    'fkContext' => '1907',
    'fkSectionBestPractice' => '1640',
    'nControlType' => '0',
    'sName' => '12.5.1 Change control procedures',
    'tDescription' => 'The implementation of changes should be controlled by the use of formal change control procedures.',
    'tImplementationGuide' => 'Formal change control procedures should be documented and enforced in order to minimize the corruption of information systems. Introduction of new systems and major changes to existing systems should follow a formal process of documentation, specification, testing, quality control, and managed implementation. This process should include a risk assessment, analysis of the impacts of changes, and specification of security controls needed. This process should also ensure that existing security and control procedures are not compromised, that support programmers are given access only to those parts of the system necessary for their work, and that formal agreement and approval for any change is obtained. Wherever practicable, application and operational change control procedures should be integrated (see also 10.1.2). The change procedures should include: a) maintaining a record of agreed authorization levels; b) ensuring changes are submitted by authorized users; c) reviewing controls and integrity procedures to ensure that they will not be compromised by the changes; d) identifying all software, information, database entities and hardware that require amendment; e) obtaining formal approval for detailed proposals before work commences; f) ensuring authorized users accept changes prior to implementation; g) ensuring that the system documentation set is updated on the completion of each change and that old documentation is archived or disposed of; h) maintaining a version control for all software updates; i) maintaining an audit trail of all change requests; j) ensuring that operating documentation (see 10.1.1) and user procedures are changed as necessary to remain appropriate; k) ensuring that the implementation of changes takes place at the right time and does not disturbe the business processes involved. Other information Changing software can impact the operational environment. Many organizations maintain an environment in which users test new software and which is segregated from development and production environments (see also 10.1.4). This provides a means of having control over new software and allowing additional protection of operational information that is used for testing purposes. This should include patches, service packs and other updates. Automated updates should',
  ),
  108 => 
  array (
    'fkContext' => '1908',
    'fkSectionBestPractice' => '1640',
    'nControlType' => '0',
    'sName' => '12.5.2 Technical review of applications after operating system changes',
    'tDescription' => 'When operating systems are changed, business critical applications should be reviewed and tested to ensure there is no adverse impact on organizational operations or security.',
    'tImplementationGuide' => 'This process should cover: a) review of application control and integrity procedures to ensure that they have not been compromised by the operating system changes; b) ensuring that the annual support plan and budget will cover reviews and system testing resulting from operating system changes; c) ensuring that notification of operating system changes is provided in time to allow appropriate tests and reviews to take place before implementation; d) ensuring that appropriate changes are made to the business continuity plans (see clause 14). A specific group or individual should be given responsibility for monitoring vulnerabilities and vendors\'\' releases of patches and fixes (see 12.6).',
  ),
  109 => 
  array (
    'fkContext' => '1909',
    'fkSectionBestPractice' => '1640',
    'nControlType' => '0',
    'sName' => '12.5.3 Restrictions on changes to software packages',
    'tDescription' => 'Modifications to software packages should be discouraged, limited to necessary changes and all changes should be strictly controlled.',
    'tImplementationGuide' => 'As far as possible, and practicable, vendor-supplied software packages should be used without modification. Where a software package needs to be modified the following points should be considered: a) the risk of built-in controls and integrity processes being compromised; b) whether the consent of the vendor should be obtained; c) the possibility of obtaining the required changes from the vendor as standard program updates; d) the impact if the organization becomes responsible for the future maintenance of the software as a result of changes. If changes are necessary the original software should be retained and the changes applied to a clearly identified copy. A software update management process should be implemented to ensure the most up-to-date approved patches and application updates are installed for all authorized software (see 12.6). All changes should be fully tested and documented, so that they can be reapplied if necessary to future software upgrades. If required, the modifications should be tested and validated by an independent evaluation body.',
  ),
  110 => 
  array (
    'fkContext' => '1910',
    'fkSectionBestPractice' => '1640',
    'nControlType' => '0',
    'sName' => '12.5.4 Information leakage',
    'tDescription' => 'Controls should be applied to limit the opportunities for information leakage, e.g. through covert channels.',
    'tImplementationGuide' => 'The following should be considered to limit the risk of information leakage, e.g. through the use and exploitation of covert channels: a) scanning of outbound media and communications for hidden information; b) masking and modulating system and communications behaviour to reduce the likelihood of a third party being able to deduce information from such behaviour; c) making use of systems and software that are considered to be of high integrity, e.g. using evaluated products (see ISO/IEC 15408); d) regular monitoring of personnel and system activities, where permitted under existing legislation or regulation; e) monitoring resource usage in computer systems. Other information Covert Channels are paths which are not intended to conduct information flows, but which may nevertheless exist in a system or network. For example, manipulating bits in communications protocol packets could be used as a hidden method of signaling. By their nature, preventing the existence of all possible covert channels would be difficult, if not impossible. However, the exploitation of such channels is often carried out by Trojan code (see also 10.4.1). Taking measures to protect against Trojan code therefore reduces the risk of covert channel exploitation. Prevention of unauthorized network access (11.4), as well as policies and procedures to discourage misuse of information services by personnel, (15.1.5) will help to protect against covert channels. ',
  ),
  111 => 
  array (
    'fkContext' => '1911',
    'fkSectionBestPractice' => '1640',
    'nControlType' => '0',
    'sName' => '12.5.5 Outsourced software development',
    'tDescription' => 'Controls should be applied to secure outsourced software development.',
    'tImplementationGuide' => 'Where software development is outsourced, the following points should be considered: a) licensing arrangements, code ownership and intellectual property rights (see 15.1.2); b) certification of the quality and accuracy of the work carried out; c) escrow arrangements in the event of failure of the third party; d) rights of access for audit of the quality and accuracy of work done; e) contractual requirements for quality and security functionality of code; f) testing before installation to detect malicious and Trojan code.',
  ),
  112 => 
  array (
    'fkContext' => '1912',
    'fkSectionBestPractice' => '1641',
    'nControlType' => '0',
    'sName' => '12.6.1 Control of vulnerabilities',
    'tDescription' => 'Timely information about vulnerabilities of information systems being used should be obtained, the organization\'\'s exposure to such vulnerabilities evaluated, and appropriate measures taken to address the associated risk.',
    'tImplementationGuide' => 'A current and complete inventory of assets (see 7.1) is a prerequisite for effective vulnerability management. Specific information needed to support vulnerability management includes the software vendor, version numbers, current state of deployment (e.g. what software is installed on what systems), and the person(s) within the organization responsible for the software. Appropriate, timely action should be taken in response to the identification of potential vulnerabilities. The following guidance should be followed to establish an effective vulnerability management process: a) the organization should define and establish the roles and responsibilities associated with vulnerability management, including vulnerability monitoring, vulnerability risk assessment, patching, asset tracking, and any coordination responsibilities required; b) information resources that will be used to identify relevant vulnerabilities and to maintain awareness about them should be identified for software and other technology (based on the asset inventory list, see 7.1.1); these information resources should be updated based on changes in the inventory, or when other new or useful resources are found; c) a timeline should be defined to react to notifications of potentially relevant vulnerabilities; d) once a potential vulnerability has been identified, the organization should identify the associated risks and the actions to be taken; such action could involve patching of vulnerable systems and/or applying other controls; e) depending on how urgently a vulnerability needs to be addressed, the action taken should be carried out according to the controls related to change management (see 12.5.1) or by following information security incident response procedures (see 12.2); f) if a patch is available, the risks associated with installing the patch should be assessed (the risks posed by the vulnerability should be compared with the risk of installing the patch); g) patches should be tested and evaluated before they are installed to ensure they are effective and do not result in side effects that cannot be tolerated; if no patch is available, other controls should be considered, such as 1) turning off services or capabilities related to the vulnerability; 2) adapting or adding access controls, e.g. firewalls, at network borders (see 11.4.5); 3) increased monitoring to detect or prevent actual attacks; and 4) raising awareness of the vulnerability; h) an audit log should be kept for all procedures undertaken; i) the vulnerability management process should be regularly monitored and evaluated in order to ensure its effectiveness and efficiency; j) systems at high risk should be addressed first. Other information The correct functioning of an organization\'\'s vulnerability management process is critical to many organizations and should therefore be regularly monitored. An accurate inventory is essential to ensure that potentially relevant vulnerabilities are identified. Vulnerability management can be viewed as a sub-function of change management and as such can take advantage of the change management processes and procedures (see 10.1.2 and 12.5.1). Vendors are often under significant pressure to release patches as soon as possible. Therefore, a patch may not address the problem adequately and may have negative side effects. Also, in some cases, uninstalling a patch may not be easily achieved once the patch once has been applied. If adequate testing of the patches is not possible, e.g. because of costs or lack of resources, a delay in patching can be considered to evaluate the associated risks, based on the experience reported by other users.',
  ),
  113 => 
  array (
    'fkContext' => '1913',
    'fkSectionBestPractice' => '1643',
    'nControlType' => '0',
    'sName' => '13.1.1 Reporting information security events',
    'tDescription' => 'Information security events should be reported through appropriate management channels as quickly as possible.',
    'tImplementationGuide' => 'A formal information security event reporting procedure should be established, together with an incident response and escalation procedure, setting out the action to be taken on receipt of a report of an information security event. A point of contact should be established for the reporting of information security events. It should be ensured that this point of contact is known throughout the organization, is always available and is able to provide adequate and timely response. All employees, contractors and third party users should be made aware of their responsibility to report any information security events as quickly as possible. They should also be aware of the procedure for reporting information security events and the point of contact. The reporting procedures should include: a) suitable feedback processes to ensure that those reporting information security events are notified of results after the issue has been dealt with and closed; b) information security event reporting forms to support the reporting action, and to help the person reporting to remember all necessary actions in case of an information security event; c) the correct behaviour to be undertaken in case of an information security event, i.e. 1) noting all important details (e.g. type of non-compliance or breach, occurring malfunction, messages on the screen, strange behaviour) immediately; 2) not carrying out any own action, but immediately reporting to the point of contact; d) reference to an established formal disciplinary process for dealing with employees, contractors or third party users who commit security breaches. In high-risk environments, a duress alarm3 may be provided whereby a person under duress can indicate such problems. The procedures for responding to duress alarms should reflect the high risk situation such alarms are indicating. Other Information Examples of information security events and incidents are: a) loss of service, equipment or facilities, b) system malfunctions or overloads, c) human errors, d) non-compliances with policies or guidelines, e) breaches of physical security arrangements, f) uncontrolled system changes, g) malfunctions of software or hardware, h) access violations. With due care of confidentiality aspects, information security incidents can be used in user awareness training (see 8.2.2) as examples of what could happen, how to respond to such incidents, and how to avoid them in the future. To be able to address information security events and incidents properly it might be necessary to collect evidence as soon as possible after the occurrence (see 13.2.3). Malfunctions or other anomalous system behavior may be an indicator of a security attack or actual security breach and should therefore always be reported as information security event. More information about reporting of information security events and management of information security incidents can be found in ISO/IEC TR 18044.',
  ),
  114 => 
  array (
    'fkContext' => '1914',
    'fkSectionBestPractice' => '1643',
    'nControlType' => '0',
    'sName' => '13.1.2 Reporting security weaknesses',
    'tDescription' => 'All employees, contractors and third party users of information systems and services should be required to note and report any observed or suspected security weaknesses in systems or services.',
    'tImplementationGuide' => 'All employees, contractors and third party users should report these matters either to their management or directly to their service provider as quickly as possible in order to prevent information security incidents. The reporting mechanism should be as easy, accessible and available as possible. They should be informed that they should not, in any circumstances, attempt to prove a suspected weakness. Other Information Not proving suspected weaknesses is for the own protection of employees, contractors and third party users. Testing weaknesses might be interpreted as a potential misuse of the system and could also cause damage to the information system or service and result in legal liability for the individual performing the testing.',
  ),
  115 => 
  array (
    'fkContext' => '1915',
    'fkSectionBestPractice' => '1644',
    'nControlType' => '0',
    'sName' => '13.2.1Responsibilities and procedures',
    'tDescription' => 'Management responsibilities and procedures should be established to ensure a quick, effective and orderly response to information security incidents.',
    'tImplementationGuide' => 'In addition to reporting of information security events and weaknesses (see also 13.1), the monitoring of systems, alerts and vulnerabilities (10.10.2) should be used to detect information security incidents. The following guidelines for information security incident management procedures should be considered: a) procedures should be established to handle different types of information security incident, including: 1) information system failures and loss of service; 2) malicious code, including viruses; 3) denial of service; 4) errors resulting from incomplete or inaccurate business data; 5) breaches of confidentiality and integrity; 6) misuse of information systems. b) in addition to normal contingency plans (see 14.1.3), the procedures should also cover (see also 13.2.2): 1) analysis and identification of the cause of the incident; 2) containment; 3) planning and implementation of corrective action to prevent recurrence, if necessary; 4) communication with those affected by or involved with recovery from the incident; 5) reporting the action to the appropriate authority; c) audit trails and similar evidence should be collected (see 13.2.3) and secured, as appropriate, for: 1) internal problem analysis; 2) use as forensic evidence in relation to a potential breach of contract breach or regulatory requirement or in the event of civil or criminal proceedings, e.g. under computer misuse or data protection legislation; 3) negotiating for compensation from software and service suppliers; d) action to recover from security breaches and correct system failures should be carefully and formally controlled; the procedures should ensure that: 1) only clearly identified and authorized personnel are allowed access to live systems and data (see also 6.2 for external access); 2) all emergency actions taken are documented in detail; 3) emergency action is reported to management and reviewed in an orderly manner; 4) the integrity of business systems and controls is confirmed with minimal delay. The objectives for information security incident management should be agreed with management, and it should be ensured that those responsible for information security incident management understand the organization\'\'s priorities for handling information security incidents. Other information Information security incidents might transcend organizational and national boundaries. To respond to such incidents there is an increasing need to coordinate response and share information about these incidents with external organizations as appropriate.',
  ),
  116 => 
  array (
    'fkContext' => '1916',
    'fkSectionBestPractice' => '1644',
    'nControlType' => '0',
    'sName' => '13.2.2 Learning from information security incidents',
    'tDescription' => 'There should be mechanisms in place to enable the types, volumes and costs of information security incidents to be quantified and monitored.',
    'tImplementationGuide' => 'The information gained from the evaluation of information security incidents should be used to identify recurring or high impact incidents. Other Information The evaluation of information security incidents may indicate the need for enhanced or additional controls to limit the frequency, damage and cost of future occurrences, or to be taken into account in the security policy review process (see 5.1.2).',
  ),
  117 => 
  array (
    'fkContext' => '1917',
    'fkSectionBestPractice' => '1644',
    'nControlType' => '0',
    'sName' => '13.2.3 Collection of evidence',
    'tDescription' => 'Where a follow-up action against a person or organization after an information security incident involves legal action (either civil or criminal) evidence should be collected, retained and presented to conform to the rules for evidence laid down in the relevant jurisdiction(s).',
    'tImplementationGuide' => 'Internal procedures should be developed and followed when collecting and presenting evidence for the purposes of disciplinary action handled within an organization. In general, the rules for evidence cover: a) admissibility of evidence: whether or not the evidence can be used in court; b) weight of evidence: the quality and completeness of the evidence. To achieve admissibility of the evidence, organizations should ensure that their information systems comply with any published standard or code of practice for the production of admissible evidence. To achieve weight of evidence, the quality and completeness of the controls used to correctly and consistently protect the evidence (i.e. process control evidence), throughout the period that the evidence to be recovered was stored and processed must be demonstrated by a strong evidence trail. In general, such a strong trail can be established under the following conditions: i) for paper documents: the original is kept securely with a record of the individual who found the document, where the document was found, when the document was found and who witnessed the discovery; any investigation should ensure that originals are not tampered with; ii) for information on computer media: copies of any removable media, information on hard disks or in memory should be taken to ensure availability; the log of all actions during the copying process should be kept and the process should be witnessed; one copy of the media and the log should be kept securely. Other information When an information security event is first detected, it may not be obvious whether or not the event will result in court action. Therefore, the danger exists that necessary evidence is destroyed intentionally or accidentally before the seriousness of the incident is realized. It is advisable to involve a lawyer or the police early in any contemplated legal action and take advice on the evidence required. Evidence may transcend organizational and/or jurisdictional boundaries. In such cases, it should be ensured that the organization is entitled to collect the required information as evidence. The requirements of different jurisdictions should also be considered to maximize chances of admission across the relevant jurisdictions.',
  ),
  118 => 
  array (
    'fkContext' => '1918',
    'fkSectionBestPractice' => '1646',
    'nControlType' => '0',
    'sName' => '14.1.1 Including information security in the business continuity management process',
    'tDescription' => 'A managed process should be developed and maintained for business continuity throughout the organization that addresses the information security requirements needed for the organization\'\'s business continuity.',
    'tImplementationGuide' => 'The process should bring together the following key elements of business continuity management: a) understanding the risks the organization is facing in terms of likelihood and impact in time, including an identification and prioritisation of critical business processes (see 14.1.2); b) identifying all the assets involved in critical business processes (see 7.1.1); c) understanding the impact which interruptions caused by information security incidents are likely to have on the business (it is important that solutions are found that will handle incidents causing smaller impact, as well as serious incidents that could threaten the viability of the organization), and establishing the business objectives of information processing facilities; d) considering the purchase of suitable insurance which may form part of the overall business continuity process, as well as being part of operational risk management; e) identifying and considering the implementation of additional preventive and mitigating controls; f) formulating and documenting a business continuity strategy for information security compliant with the overall business continuity strategy; g) ensuring that the business continuity programme identifies sufficient financial, organizational, technical, and environmental resources to address the identified information security requirements; h) ensuring the safety of staff and the protection of information processing facilities and organizational property; i) formulating and documenting business continuity plans addressing information security requirements in line with the agreed strategy (see 14.1.3); j) regular testing and updating of the plans and processes put in place (see 14.1.5); k) ensuring that the management of business continuity is incorporated in the organization\'\'s processes and structure; responsibility for the business continuity management process should be assigned at an appropriate level within the organization (see 6.1.1).',
  ),
  119 => 
  array (
    'fkContext' => '1919',
    'fkSectionBestPractice' => '1646',
    'nControlType' => '0',
    'sName' => '14.1.2 Business continuity and risk assessment',
    'tDescription' => 'Information security events that can cause interruptions to business processes should be identified, along with the probability and impact of such interruptions.',
    'tImplementationGuide' => 'Information security aspects of business continuity should be based on identifying information security events (or sequence of events) that can cause interruptions to the organizations business processes, e.g. equipment failure, human errors, theft, fire, natural disasters and acts of terrorism. This should be followed by a risk assessment to determine the probability and impact of such interruptions, in terms of time, damage scale and recovery period. Business continuity risk assessments should be carried out with full involvement from owners of business resources and processes. This assessment should consider all business processes and should not be limited to the information processing facilities, but should include the results specific to information security. It is important to link the different risk aspects together, to obtain a complete picture of the business continuity requirements of the organization. The assessment should identify, quantify and prioritise risks against criteria and objectives relevant to the organization, including critical resources, impacts of disruptions allowable outage times and recovery priorities. Depending on the results of the risk assessment, a business continuity strategy should be developed to determine the overall approach to business continuity. Once this strategy has been created, endorsement should be provided by management, and a plan created and endorsed to implement this strategy.',
  ),
  120 => 
  array (
    'fkContext' => '1920',
    'fkSectionBestPractice' => '1646',
    'nControlType' => '0',
    'sName' => '14.1.3 Developing and implementing continuity plans including information security',
    'tDescription' => 'Plans should be developed to maintain or restore operations and ensure availability of information at the required level and in the required time scales following interruption to, or failure of, critical business processes.',
    'tImplementationGuide' => 'The business continuity planning process should consider the following: a) identification and agreement of all responsibilities and business continuity procedures; b) identification of the acceptable loss of information and services; c) implementation of the procedures to allow recovery and restoration of business operations and availability of information in required time-scales; particular attention needs to be given to the assessment of internal and external business dependencies and the contracts in place; d) operational procedures to follow pending completion of recovery and restoration; e) documentation of agreed procedures and processes; f) appropriate education of staff in the agreed procedures and processes, including crisis management; g) testing and updating of the plans. The planning process should focus on the required business objectives, e.g. restoring of specific communication services to customers in an acceptable amount of time. The services and resources facilitating this should be identified, including staffing, non-information processing resources, as well as fallback arrangements for information processing facilities. Such fallback arrangements may include arrangements with third parties in the form of reciprocal agreements, or commercial subscription services. Business continuity plans should address organizational vulnerabilities and therefore may contain sensitive information that needs to be appropriately protected. Copies of business continuity plans should be stored in a remote location, at a sufficient distance to escape any damage from a disaster at the main site. Management should ensure copies of the business continuity plans are up-to-date and protected with the same level of security as applied at the main site. Other material necessary to execute the continuity plans should also be stored at the remote location. If alternative temporary locations are used, the level of implemented security controls at these locations should be equivalent to the main site. Other information It should be noted that crisis management plans and activities (see 12.1.3 f)) may be different from business continuity management; i.e. a crisis may occur that can be accommodated by normal management procedures.',
  ),
  121 => 
  array (
    'fkContext' => '1921',
    'fkSectionBestPractice' => '1646',
    'nControlType' => '0',
    'sName' => '14.1.4 Business continuity planning framework',
    'tDescription' => 'A single framework of business continuity plans should be maintained to ensure all plans are consistent, to consistently address information security requirements, and to identify priorities for testing and maintenance.',
    'tImplementationGuide' => 'Each business continuity plan should describe the strategy for continuity, for example the strategy to ensure information or information system availability and security. Each plan should also specify the escalation plan and the conditions for its activation, as well as the individuals responsible for executing each component of the plan. When new requirements are identified, any existing emergency procedures, e.g. evacuation plans or fallback arrangements, should be amended as appropriate. Procedures should be included within the organization\'\'s change management programme to ensure that business continuity matters are always addressed appropriately. Each plan should have a specific owner. Emergency procedures, manual fallback plans and resumption plans should be within the responsibility of the owners of the appropriate business resources or processes involved. Fallback arrangements for alternative technical services, such as information processing and communications facilities, should usually be the responsibility of the service providers. A business continuity planning framework should address the identified information security requirements and consider the following: a) the conditions for activating the plans and the process to be followed (how to assess the situation, who is to be involved, etc.); b) the conditions for activating the plans which describe the process to be followed (how to assess the situation, who is to be involved, etc.) before each plan is activated; c) emergency procedures, which describe the actions to be taken following an incident, which jeopardizes business operations; d) fallback procedures which describe the actions to be taken to move essential business activities or support services to alternative temporary locations, and to bring business processes back into operation in the required time-scales; e) temporary operational procedures to follow pending completion of recovery and restoration; f) resumption procedures which describe the actions to be taken to return to normal business operations; g) a maintenance schedule which specifies how and when the plan will be tested, and the process for maintaining the plan; h) awareness, education and training activities which are designed to create understanding of the business continuity processes and ensure that the processes continue to be effective. i) the responsibilities of the individuals, describing who is responsible for executing which component of the plan. Alternatives should be nominated as required; j) the critical assets and resources needed to be able to perform the emergency, fallback and resumption procedures. ',
  ),
  122 => 
  array (
    'fkContext' => '1922',
    'fkSectionBestPractice' => '1646',
    'nControlType' => '0',
    'sName' => '14.1.5 Testing, maintaining and re-assessing business continuity plans',
    'tDescription' => 'Business continuity plans should be tested and updated regularly to ensure that they are up to date and effective and address the requirements for information security.',
    'tImplementationGuide' => 'Business continuity plan tests should ensure that all members of the recovery team and other relevant staff are aware of the plans and their responsibility for business continuity and information security. The test schedule for business continuity plan(s) should indicate how and when each element of the plan should be tested. Each elements of the plan(s) should be tested frequently. A variety of techniques should be used in order to provide assurance that the plan(s) will operate in real life. These should include: a) table-top testing of various scenarios (discussing the business recovery arrangements using example interruptions); b) simulations (particularly for training people in their post-incident/crisis management roles); c) technical recovery testing (ensuring information systems can be restored effectively); d) testing recovery at an alternate site (running business processes in parallel with recovery operations away from the main site); e) tests of supplier facilities and services (ensuring externally provided services and products will meet the contracted commitment); f) complete rehearsals (testing that the organization, personnel, equipment, facilities and processes can cope with interruptions). These techniques can be used by any organization. They should be applied in a way that is relevant to the specific recovery plan. The results of tests should be recorded and actions taken to improve the plans, where necessary. Responsibility should be assigned for regular reviews of each business continuity plan; the identification of changes in business arrangements not yet reflected in the business continuity plans should be followed by an appropriate update of the plan. This formal change control process should ensure that the updated plans are distributed and reinforced by regular reviews of the complete plan. Examples of changes where updating of business continuity plans should be considered are acquisition of new equipment, upgrading of systems and changes in: i) personnel; ii) addresses or telephone numbers; iii) business strategy; iv) location, facilities and resources; v) legislation; vi) contractors, suppliers and key customers; vii) processes, or new/withdrawn ones; viii) risk (operational and financial).',
  ),
  123 => 
  array (
    'fkContext' => '1923',
    'fkSectionBestPractice' => '1648',
    'nControlType' => '0',
    'sName' => '15.1.1 Identification of applicable legislation',
    'tDescription' => 'All relevant statutory, regulatory and contractual requirements and the organization\'\'s approach to meet these requirements should be explicitly defined, documented and kept up to date for each information system and the organization.',
    'tImplementationGuide' => 'The specific controls and individual responsibilities to meet these requirements should be similarly defined and documented.',
  ),
  124 => 
  array (
    'fkContext' => '1924',
    'fkSectionBestPractice' => '1648',
    'nControlType' => '0',
    'sName' => '15.1.2 Intellectual property rights (IPR)',
    'tDescription' => 'Appropriate procedures should be implemented to ensure compliance with legislative, regulatory and contractual requirements on the use of material in respect of which there may be intellectual property rights and on the use of proprietary software products.',
    'tImplementationGuide' => 'The following guidelines should be considered to protect any material that may be considered intellectual property: a) publishing an intellectual property rights compliance policy which defines the legal use of software and information products; b) acquiring software only through known and reputable sources, to ensure that copyright is not violated; c) maintaining awareness of policies to protect intellectual property rights, and giving notice of the intent to take disciplinary action against personnel breaching them; d) maintaining appropriate asset registers, and identifying all assets with requirements to protect intellectual property rights; e) maintaining proof and evidence of ownership of licenses, master disks, manuals, etc; f) implementing controls to ensure that any maximum number of users permitted is not exceeded; g) carrying out checks that only authorized software and licensed products are installed; h) providing a policy for maintaining appropriate licence conditions; i) providing a policy for disposing or transferring software to others; j) using appropriate audit tools; k) complying with terms and conditions for software and information obtained from public networks; l) not duplicating, converting to another format or extracting from commercial recordings (film, audio) other than permitted by copyright law; m) not copying in full or in part, books, articles, reports or other documents, other than permitted by copyright law. Other information Intellectual property rights include software copyright, design rights, trademarks, patents, source code, licenses, and documentation. Proprietary software products are usually supplied under a license agreement that limits the use of the products to specified machines and may limit copying to the creation of back-up copies only. The IPR situation of software developed by the organization requires to be clarified with the staff. Legislative, regulatory and contractual requirements may place restrictions on the copying of proprietary material. In particular, they may require that only material that is developed by the organization, or that is licensed or provided by the developer to the organization, can be used. Copyright infringement can lead to legal action, which may involve criminal proceedings. ',
  ),
  125 => 
  array (
    'fkContext' => '1925',
    'fkSectionBestPractice' => '1648',
    'nControlType' => '0',
    'sName' => '15.1.3 Safeguarding of organizational records',
    'tDescription' => 'Organization should protected important records from loss, destruction and falsification, in accordance with statutory, regulatory, contractual and business requirements.',
    'tImplementationGuide' => 'Records should be categorized into record types, e.g. accounting records, database records, transaction logs, audit logs and operational procedures, each with details of retention periods and type of storage media, e.g. paper, microfiche, magnetic, optical. Any related cryptographic keying material and programs associated with encrypted archives or digital signatures (see 12.3), should also be stored to enable decryption of the records for the length of time the records are retained. Consideration should be given to the possibility of degradation of media used for storage of records. Storage and handling procedures should be implemented in accordance with manufacturer\'\'s recommendations. For long term storage, the use of paper and microfiche should be considered. Where electronic storage media are chosen, procedures to ensure the ability to access data (both media and format readability) throughout the retention period should be included, to safeguard against loss due to future technology change. Data storage systems should be chosen such that required data can be retrieved in an acceptable timeframe and format, depending on the requirements to be fulfilled. The system of storage and handling should ensure clear identification of records and of their retention period as defined by national or regional legislation or regulations, if applicable. This system should permit appropriate destruction of records after that period if they are not needed by the organization. To meet these record safeguarding objectives, the following steps should be taken within an organization:  a) guidelines should be issued on the retention, storage, handling and disposal of records and information; b) a retention schedule should be drawn up identifying records and the period of time for which they should be retained; c) an inventory of sources of key information should be maintained; d) appropriate controls should be implemented to protect records and information from loss, destruction and falsification; Other information Some records may need to be securely retained to meet statutory, regulatory or contractual requirements, as well as to support essential business activities. Examples of this are records that may be required as evidence that an organization operates within statutory or regulatory rules, or to ensure adequate defense against potential civil or criminal action, or to confirm the financial status of an organization with respect to shareholders, external parties and auditors. The time period and data content for information retention may be set by national law or regulation. Further information about managing organizational records can be found in ISO 15489-1.',
  ),
  126 => 
  array (
    'fkContext' => '1926',
    'fkSectionBestPractice' => '1648',
    'nControlType' => '0',
    'sName' => '15.1.4 Data protection and privacy of personal information',
    'tDescription' => 'Data protection and privacy should be ensured as required in relevant legislation and regulations.',
    'tImplementationGuide' => 'An organizational data protection and privacy policy should be developed and implemented. Compliance with this policy and all relevant data protection legislation and regulations requires appropriate management structure and control. Often this is best achieved by the appointment of a person responsible, such as a data protection officer, who should provide guidance to managers, users and service providers on their individual responsibilities and the specific procedures that should be followed. Responsibility for handling privacy information and ensuring awareness of the data protection principles should be dealt with in accordance with relevant legislation and regulations. Other information A number of countries have introduced legislation placing controls on the collection, processing and transmission of personal data (generally information on living individuals who can be identified from that information). Depending on the respective national legislation, such controls may impose duties on those collecting, processing and disseminating personal information, and may restrict the ability to transfer that data to other countries.',
  ),
  127 => 
  array (
    'fkContext' => '1927',
    'fkSectionBestPractice' => '1648',
    'nControlType' => '0',
    'sName' => '15.1.5 Prevention of misuse of information processing facilities',
    'tDescription' => 'Users should be deterred from using information processing facilities for unauthorized purposes.',
    'tImplementationGuide' => 'Management should approve the use of information processing facilities. Any use of these facilities for non-business purposes without management approval (see 6.1.4), or for any unauthorized purposes, should be regarded as improper use of the facilities. If any unauthorized activity is identified by monitoring or other means, this activity should be brought to the attention of the individual manager concerned for consideration of appropriate disciplinary and/or legal action. Legal advice should be taken before implementing monitoring procedures. All users should be aware of the precise scope of their permitted access and of the monitoring in place to detect unauthorized use. This can, for example, be achieved by giving users written authorization, a copy of which should be signed by the user and securely retained by the organization. Employees of an organization, contractors and third party users should be advised that no access will be permitted except that which is authorized. A warning message should be presented to indicate that the information processing facility being entered is owned by the organization and that unauthorized access is not permitted. The user has to acknowledge and react appropriately to the message on the screen to continue with the log-on process. Other information The information processing facilities of an organization are intended primarily or exclusively for business purposes. Intrusion detection, content inspection and other monitoring tools may help prevent and detect misuse of information processing facilities. Many countries have legislation to protect against computer misuse. It may be a criminal offence to use a computer for unauthorized purposes. The legality of monitoring the usage varies from country to country and may require management to advise all users of such monitoring and/or to obtain their agreement. Where the system being entered is used for public access (e.g., a public web server) and is subject to security monitoring, a message should be displayed saying so.',
  ),
  128 => 
  array (
    'fkContext' => '1928',
    'fkSectionBestPractice' => '1648',
    'nControlType' => '0',
    'sName' => '15.1.6 Regulation of cryptographic controls',
    'tDescription' => 'Cryptographic controls should be used in compliance with all relevant agreements, laws and regulations.',
    'tImplementationGuide' => 'The following items should be considered for compliance with the relevant agreements, laws and regulations: a) restrictions on import and/or export of computer hardware and software for performing cryptographic functions; b) restrictions on import and/or export of computer hardware and software which is designed to have cryptographic functions added to it; c) restrictions on the usage of encryption; d) mandatory or discretionary methods of access by the countries\'\' authorities to information encrypted by hardware or software to provide confidentiality of content. Legal advice should be sought to ensure compliance with national laws and regulations. Before encrypted information or cryptographic controls are moved to another country, legal advice should also be taken. ',
  ),
  129 => 
  array (
    'fkContext' => '1929',
    'fkSectionBestPractice' => '1649',
    'nControlType' => '0',
    'sName' => '15.2.1 Compliance with security policy and standards',
    'tDescription' => 'Managers should ensure that all security procedures within their area of responsibility are carried out correctly to achieve compliance with security policies and standards.',
    'tImplementationGuide' => 'Managers should regularly review the compliance of information processing within their area of responsibility with the appropriate security policies, standards and any other security requirements. If any non-compliance is found as a result of the review, managers should: a) determine the causes of the non-compliance; b) evaluate the need for actions to ensure that non-compliance do not recur; c) determine and implement appropriate corrective action; d) review the corrective action taken. Results of reviews and corrective actions carried out by managers should be recorded and these records should be maintained. Managers should report the results to the persons carrying out the independent reviews (see 6.1.8), when the independent review takes place in the area of their responsibility. Other information Operational monitoring of system use is covered in 10.10.',
  ),
  130 => 
  array (
    'fkContext' => '1930',
    'fkSectionBestPractice' => '1649',
    'nControlType' => '0',
    'sName' => '15.2.2 Technical compliance checking',
    'tDescription' => 'Information systems should be regularly checked for compliance with security implementation standards.',
    'tImplementationGuide' => 'Technical compliance checking should be performed either manually (supported by appropriate software tools, if necessary) by an experienced system engineer, and/or with the assistance of automated tools, which generate a technical report for subsequent interpretation by a technical specialist. If penetration tests or vulnerability assessments are used, caution should be exercised as such activities could lead to a compromise of the security of the system. Such tests should be planned, documented and repeatable. Any technical compliance check should only be carried out by competent, authorized persons, or under the supervision of such persons. Other information Technical compliance checking involves the examination of operational systems to ensure that hardware and software controls have been correctly implemented. This type of compliance checking requires specialist technical expertise.   Compliance checking also covers, for example, penetration testing and vulnerability assessments, which might be carried out by independent experts specifically contracted for this purpose. This can be useful in detecting vulnerabilities in the system and for checking how effective the controls are in preventing unauthorized access due to these vulnerabilities. Penetration testing and vulnerability assessments provide a snapshot of a system in a specific state at a specific time. The snapshot is limited to those portions of the system actually tested during the penetration attempt(s). Penetration testing and vulnerability assessments are not a substitute for risk assessment.',
  ),
  131 => 
  array (
    'fkContext' => '1931',
    'fkSectionBestPractice' => '1650',
    'nControlType' => '0',
    'sName' => '15.3.1 Information systems audit controls',
    'tDescription' => 'Audit requirements and activities involving checks on operational systems should be carefully planned and agreed to minimize the risk of disruptions to business processes.',
    'tImplementationGuide' => 'The following guidelines should be observed: a) audit requirements should be agreed with appropriate management; b) the scope of the checks should be agreed and controlled; c) the checks should be limited to read-only access to software and data; d) access other than read-only should only be allowed for isolated copies of system files, which should be erased when the audit is completed, or given appropriate protection if there is an obligation to keep such files under audit documentation requirements; e) resources for performing the checks should be explicitly identified and made available; f) requirements for special or additional processing should be identified and agreed; g) all access should be monitored and logged to produce a reference trail; the use of time-stamped reference trails should be considered for critical data or systems; h) all procedures, requirements and responsibilities should be documented; i) the person(s) carrying out the audit should be independent of the activities audited.',
  ),
  132 => 
  array (
    'fkContext' => '1932',
    'fkSectionBestPractice' => '1650',
    'nControlType' => '0',
    'sName' => '15.3.2 Protection of information systems audit tools',
    'tDescription' => 'Access to information systems audit tools, e.g. software or data files, should be protected to prevent any possible misuse or compromise.',
    'tImplementationGuide' => 'Information systems audit tools should be separated from development and operational systems and not held in tape libraries or user areas, unless given an appropriate level of additional protection. Other information If third parties are involved in an audit, there might be a risk of misuse of audit tools by these third parties, and information being accessed by this third party organization. Controls such as 6.2.1 (to assess the risks) and 9.1.2 (to restrict physical access) can be considered to address this risk, and any consequences, such as immediately changing passwords disclosed to the auditors, should be taken.',
  ),
  133 => 
  array (
    'fkContext' => '1934',
    'fkSectionBestPractice' => '1653',
    'nControlType' => '0',
    'sName' => 'PO1.1 IT Value Management',
    'tDescription' => 'Work with the business to ensure that the enterprise portfolio of IT-enabled investments contains programmes that have solid business cases. Recognise that there are mandatory, sustaining and discretionary investments that differ in complexity and degree of freedom in allocating funds. IT processes should provide effective and efficient delivery of the IT components of programmes and early warning of any deviations from plan, including cost, schedule or functionality, that might impact the expected outcomes of the programmes. IT services should be executed against equitable and enforceable service level agreements (SLAs). Accountability for achieving the benefits and controlling the costs should be clearly assigned and monitored. Establish fair, transparent, repeatable and comparable evaluation of business cases, including financial worth, the risk of not delivering a capability and the risk of not realising the expected benefits.',
    'tImplementationGuide' => '',
  ),
  134 => 
  array (
    'fkContext' => '1935',
    'fkSectionBestPractice' => '1653',
    'nControlType' => '0',
    'sName' => 'PO1.2 Business-IT Alignment',
    'tDescription' => 'Establish processes of bi-directional education and reciprocal involvement in strategic planning to achieve business and IT alignment and integration. Mediate between business and IT imperatives so priorities can be mutually agreed.',
    'tImplementationGuide' => '',
  ),
  135 => 
  array (
    'fkContext' => '1936',
    'fkSectionBestPractice' => '1653',
    'nControlType' => '0',
    'sName' => 'PO1.3 Assessment of Current Capability and Performance',
    'tDescription' => 'Assess the current capability and performance of solution and service delivery to establish a baseline against which future requirements can be compared. Define performance in terms of IT\'\'s contribution to business objectives, functionality, stability, complexity, costs, strengths and weaknesses.',
    'tImplementationGuide' => '',
  ),
  136 => 
  array (
    'fkContext' => '1937',
    'fkSectionBestPractice' => '1653',
    'nControlType' => '0',
    'sName' => 'PO1.4 IT Strategic Plan',
    'tDescription' => 'Create a strategic plan that defines, in co-operation with relevant stakeholders, how IT goals will contribute to the enterprise\'\'s strategic objectives and related costs and risks. It should include how IT will support IT-enabled investment programmes, IT services and IT assets. IT should define how the objectives will be met, the measurements to be used and the procedures to obtain formal sign-off from the stakeholders. The IT strategic plan should cover investment/operational budget, funding sources, sourcing strategy, acquisition strategy, and legal and regulatory requirements. The strategic plan should be sufficiently detailed to allow for the definition of tactical IT plans.',
    'tImplementationGuide' => '',
  ),
  137 => 
  array (
    'fkContext' => '1938',
    'fkSectionBestPractice' => '1653',
    'nControlType' => '0',
    'sName' => 'PO1.5 IT Tactical Plans',
    'tDescription' => 'Create a portfolio of tactical IT plans that are derived from the IT strategic plan. The tactical plans should address IT-enabled programme investments, IT services and IT assets. The tactical plans should describe required IT initiatives, resource requirements, and how the use of resources and achievement of benefits will be monitored and managed. The tactical plans should be sufficiently detailed to allow the definition of project plans. Actively manage the set of tactical IT plans and initiatives through analysis of project and service portfolios.',
    'tImplementationGuide' => '',
  ),
  138 => 
  array (
    'fkContext' => '1939',
    'fkSectionBestPractice' => '1653',
    'nControlType' => '0',
    'sName' => 'PO1.6 IT Portfolio Management',
    'tDescription' => 'Actively manage with the business the portfolio of IT-enabled investment programmes required to achieve specific strategic business objectives by identifying, defining, evaluating, prioritising, selecting, initiating, managing and controlling programmes. This should include clarifying desired business outcomes, ensuring that programme objectives support achievement of the outcomes, understanding the full scope of effort required to achieve the outcomes, assigning clear accountability with supporting measures, defining projects within the programme, allocating resources and funding, delegating authority, and commissioning required projects at programme launch.',
    'tImplementationGuide' => '',
  ),
  139 => 
  array (
    'fkContext' => '1940',
    'fkSectionBestPractice' => '1654',
    'nControlType' => '0',
    'sName' => 'PO2.1 Enterprise Information Architecture Model',
    'tDescription' => 'Establish and maintain an enterprise information model to enable applications development and decision-supporting activities, consistent with IT plans as described in PO1. The model should facilitate the optimal creation, use and sharing of information by the business in a way that maintains integrity and is flexible, functional, cost-effective, timely, secure and resilient to failure.',
    'tImplementationGuide' => '',
  ),
  140 => 
  array (
    'fkContext' => '1941',
    'fkSectionBestPractice' => '1654',
    'nControlType' => '0',
    'sName' => 'PO2.2 Enterprise Data Dictionary and Data Syntax Rules',
    'tDescription' => 'Maintain an enterprise data dictionary that incorporates the organisation\'\'s data syntax rules. This dictionary should enable the sharing of data elements amongst applications and systems, promote a common understanding of data amongst IT and business users, and prevent incompatible data elements from being created.',
    'tImplementationGuide' => '',
  ),
  141 => 
  array (
    'fkContext' => '1942',
    'fkSectionBestPractice' => '1654',
    'nControlType' => '0',
    'sName' => 'PO2.3 Data Classification Scheme',
    'tDescription' => 'Establish a classification scheme that applies throughout the enterprise, based on the criticality and sensitivity (e.g., public, confidential, top secret) of enterprise data. This scheme should include details about data ownership; definition of appropriate security levels and protection controls; and a brief description of data retention and destruction requirements, criticality and sensitivity. It should be used as the basis for applying controls such as access controls, archiving or encryption.',
    'tImplementationGuide' => '',
  ),
  142 => 
  array (
    'fkContext' => '1943',
    'fkSectionBestPractice' => '1654',
    'nControlType' => '0',
    'sName' => 'PO2.4 Integrity Management',
    'tDescription' => 'Define and implement procedures to ensure the integrity and consistency of all data stored in electronic form, such as databases, data warehouses and data archives.',
    'tImplementationGuide' => '',
  ),
  143 => 
  array (
    'fkContext' => '1944',
    'fkSectionBestPractice' => '1655',
    'nControlType' => '0',
    'sName' => 'PO3.1 Technological Direction Planning',
    'tDescription' => 'Analyse existing and emerging technologies, and plan which technological direction is appropriate to realise the IT strategy and the business systems architecture. Also identify in the plan which technologies have the potential to create business opportunities. The plan should address systems architecture, technological direction, migration strategies and contingency aspects of infrastructure components.',
    'tImplementationGuide' => '',
  ),
  144 => 
  array (
    'fkContext' => '1945',
    'fkSectionBestPractice' => '1655',
    'nControlType' => '0',
    'sName' => 'PO3.2 Technology Infrastructure Plan',
    'tDescription' => 'Create and maintain a technology infrastructure plan that is in accordance with the IT strategic and tactical plans. The plan should be based on the technological direction and include contingency arrangements and direction for acquisition of technology resources. It should consider changes in the competitive environment, economies of scale for information systems staffing and investments, and improved interoperability of platforms and applications.',
    'tImplementationGuide' => '',
  ),
  145 => 
  array (
    'fkContext' => '1946',
    'fkSectionBestPractice' => '1655',
    'nControlType' => '0',
    'sName' => 'PO3.3 Monitor Future Trends and Regulations',
    'tDescription' => 'Establish a process to monitor the business sector, industry, technology, infrastructure, legal and regulatory environment trends. Incorporate the consequences of these trends into the development of the IT technology infrastructure plan.',
    'tImplementationGuide' => '',
  ),
  146 => 
  array (
    'fkContext' => '1947',
    'fkSectionBestPractice' => '1655',
    'nControlType' => '0',
    'sName' => 'PO3.4 Technology Standards',
    'tDescription' => 'To provide consistent, effective and secure technological solutions enterprisewide, establish a technology forum to provide technology guidelines, advice on infrastructure products and guidance on the selection of technology, and measure compliance with these standards and guidelines. This forum should direct technology standards and practices based on their business relevance, risks and compliance with external requirements.',
    'tImplementationGuide' => '',
  ),
  147 => 
  array (
    'fkContext' => '1948',
    'fkSectionBestPractice' => '1655',
    'nControlType' => '0',
    'sName' => 'PO3.5 IT Architecture Board',
    'tDescription' => 'Establish an IT architecture board to provide architecture guidelines and advice on their application, and to verify compliance. This entity should direct IT architecture design, ensuring that it enables the business strategy and considers regulatory compliance and continuity requirements. This is related/linked to PO2 Define the information architecture.',
    'tImplementationGuide' => '',
  ),
  148 => 
  array (
    'fkContext' => '1949',
    'fkSectionBestPractice' => '1656',
    'nControlType' => '0',
    'sName' => 'PO4.1 IT Process Framework',
    'tDescription' => 'Define an IT process framework to execute the IT strategic plan. This framework should include an IT process structure and relationships (e.g., to manage process gaps and overlaps), ownership, maturity, performance measurement, improvement, compliance, quality targets and plans to achieve them. It should provide integration amongst the processes that are specific to IT, enterprise portfolio management, business processes and business change processes. The IT process framework should be integrated into a quality management system (QMS) and the internal control framework.',
    'tImplementationGuide' => '',
  ),
  149 => 
  array (
    'fkContext' => '1950',
    'fkSectionBestPractice' => '1656',
    'nControlType' => '0',
    'sName' => 'PO4.2 IT Strategy Committee',
    'tDescription' => 'Establish an IT strategy committee at the board level. This committee should ensure that IT governance, as part of enterprise governance, is adequately addressed; advise on strategic direction; and review major investments on behalf of the full board.',
    'tImplementationGuide' => '',
  ),
  150 => 
  array (
    'fkContext' => '1951',
    'fkSectionBestPractice' => '1656',
    'nControlType' => '0',
    'sName' => 'PO4.3 IT Steering Committee',
    'tDescription' => 'Establish an IT steering committee (or equivalent) composed of executive, business and IT management to: - Determine prioritisation of IT-enabled investment programmes in line with the enterprise\'\'s business strategy and priorities - Track status of projects and resolve resource conflict - Monitor service levels and service improvements',
    'tImplementationGuide' => '',
  ),
  151 => 
  array (
    'fkContext' => '1952',
    'fkSectionBestPractice' => '1656',
    'nControlType' => '0',
    'sName' => 'PO4.4 Organisational Placement of the IT Function',
    'tDescription' => 'Place the IT function in the overall organisational structure with a business model contingent on the importance of IT within the enterprise, specifically its criticality to business strategy and the level of operational dependence on IT. The reporting line of the CIO should be commensurate with the importance of IT within the enterprise.',
    'tImplementationGuide' => '',
  ),
  152 => 
  array (
    'fkContext' => '1953',
    'fkSectionBestPractice' => '1656',
    'nControlType' => '0',
    'sName' => 'PO4.5 IT Organisational Structure',
    'tDescription' => 'Establish an internal and external IT organisational structure that reflects business needs. In addition, put a process in place for periodically reviewing the IT organisational structure to adjust staffing requirements and sourcing strategies to meet expected business objectives and changing circumstances.',
    'tImplementationGuide' => '',
  ),
  153 => 
  array (
    'fkContext' => '1954',
    'fkSectionBestPractice' => '1656',
    'nControlType' => '0',
    'sName' => 'PO4.6 Establishment of Roles and Responsibilities',
    'tDescription' => 'Establish and communicate roles and responsibilities for IT personnel and end users that delineate between IT personnel and end-user authority, responsibilities and accountability for meeting the organisation\'\'s needs.',
    'tImplementationGuide' => '',
  ),
  154 => 
  array (
    'fkContext' => '1955',
    'fkSectionBestPractice' => '1656',
    'nControlType' => '0',
    'sName' => 'PO4.7 Responsibility for IT Quality Assurance',
    'tDescription' => 'Assign responsibility for the performance of the quality assurance (QA) function and provide the QA group with appropriate QA systems, controls and communications expertise. Ensure that the organisational placement and the responsibilities and size of the QA group satisfy the requirements of the organisation.',
    'tImplementationGuide' => '',
  ),
  155 => 
  array (
    'fkContext' => '1956',
    'fkSectionBestPractice' => '1656',
    'nControlType' => '0',
    'sName' => 'PO4.8 Responsibility for Risk, Security and Compliance',
    'tDescription' => 'Embed ownership and responsibility for IT-related risks within the business at an appropriate senior level. Define and assign roles critical for managing IT risks, including the specific responsibility for information security, physical security and compliance. Establish risk and security management responsibility at the enterprise level to deal with organisationwide issues. Additional security management responsibilities may need to be assigned at a system-specific level to deal with related security issues. Obtain direction from senior management on the appetite for IT risk and approval of any residual IT risks.',
    'tImplementationGuide' => '',
  ),
  156 => 
  array (
    'fkContext' => '1957',
    'fkSectionBestPractice' => '1656',
    'nControlType' => '0',
    'sName' => 'PO4.9 Data and System Ownership',
    'tDescription' => 'Provide the business with procedures and tools, enabling it to address its responsibilities for ownership of data and information systems. Owners should make decisions about classifying information and systems and protecting them in line with this classification.',
    'tImplementationGuide' => '',
  ),
  157 => 
  array (
    'fkContext' => '1958',
    'fkSectionBestPractice' => '1656',
    'nControlType' => '0',
    'sName' => 'PO4.10 Supervision',
    'tDescription' => 'Implement adequate supervisory practices in the IT function to ensure that roles and responsibilities are properly exercised, to assess whether all personnel have sufficient authority and resources to execute their roles and responsibilities, and to generally review KPIs.',
    'tImplementationGuide' => '',
  ),
  158 => 
  array (
    'fkContext' => '1959',
    'fkSectionBestPractice' => '1656',
    'nControlType' => '0',
    'sName' => 'PO4.11 Segregation of Duties',
    'tDescription' => 'Implement a division of roles and responsibilities that reduces the possibility for a single individual to compromise a critical process. Make sure that personnel are performing only authorised duties relevant to their respective jobs and positions.',
    'tImplementationGuide' => '',
  ),
  159 => 
  array (
    'fkContext' => '1960',
    'fkSectionBestPractice' => '1656',
    'nControlType' => '0',
    'sName' => 'PO4.12 IT Staffing',
    'tDescription' => 'Evaluate staffing requirements on a regular basis or upon major changes to the business, operational or IT environments to ensure that the IT function has sufficient resources to adequately and appropriately support the business goals and objectives.',
    'tImplementationGuide' => '',
  ),
  160 => 
  array (
    'fkContext' => '1961',
    'fkSectionBestPractice' => '1656',
    'nControlType' => '0',
    'sName' => 'PO4.13 Key IT Personnel',
    'tDescription' => 'Define and identify key IT personnel (e.g., replacements/backup personnel), and minimise reliance on a single individual performing a critical job function.',
    'tImplementationGuide' => '',
  ),
  161 => 
  array (
    'fkContext' => '1962',
    'fkSectionBestPractice' => '1656',
    'nControlType' => '0',
    'sName' => 'PO4.14 Contracted Staff Policies and Procedures',
    'tDescription' => 'Ensure that consultants and contract personnel who support the IT function know and comply with the organisation\'\'s policies for the protection of the organisation\'\'s information assets such that they meet agreed-upon contractual requirements.',
    'tImplementationGuide' => '',
  ),
  162 => 
  array (
    'fkContext' => '1963',
    'fkSectionBestPractice' => '1656',
    'nControlType' => '0',
    'sName' => 'PO4.15 Relationships',
    'tDescription' => 'Establish and maintain an optimal co-ordination, communication and liaison structure between the IT function and various other interests inside and outside the IT function, such as the board, executives, business units, individual users, suppliers, security officers, risk managers, the corporate compliance group, outsourcers and offsite management.',
    'tImplementationGuide' => '',
  ),
  163 => 
  array (
    'fkContext' => '1964',
    'fkSectionBestPractice' => '1657',
    'nControlType' => '0',
    'sName' => 'PO5.1 Financial Management Framework',
    'tDescription' => 'Establish and maintain a financial framework to manage the investment and cost of IT assets and services through portfolios of Itenabled investments, business cases and IT budgets.',
    'tImplementationGuide' => '',
  ),
  164 => 
  array (
    'fkContext' => '1965',
    'fkSectionBestPractice' => '1657',
    'nControlType' => '0',
    'sName' => 'PO5.2 Prioritisation Within IT Budget',
    'tDescription' => 'Implement a decision-making process to prioritise the allocation of IT resources for operations, projects and maintenance to maximise IT\'\'s contribution to optimising the return on the enterprise\'\'s portfolio of IT-enabled investment programmes and other IT services and assets.',
    'tImplementationGuide' => '',
  ),
  165 => 
  array (
    'fkContext' => '1966',
    'fkSectionBestPractice' => '1657',
    'nControlType' => '0',
    'sName' => 'PO5.3 IT Budgeting',
    'tDescription' => 'Establish and implement practices to prepare a budget reflecting the priorities established by the enterprise\'\'s portfolio of IT-enabled investment programmes, and including the ongoing costs of operating and maintaining the current infrastructure. The practices should support development of an overall IT budget as well as development of budgets for individual programmes, with specific emphasis on the IT components of those programmes. The practices should allow for ongoing review, refinement and approval of the overall budget and the budgets for individual programmes.',
    'tImplementationGuide' => '',
  ),
  166 => 
  array (
    'fkContext' => '1967',
    'fkSectionBestPractice' => '1657',
    'nControlType' => '0',
    'sName' => 'PO5.4 Cost Management',
    'tDescription' => 'Implement a cost management process comparing actual costs to budgets. Costs should be monitored and reported. Where there are deviations, these should be identified in a timely manner and the impact of those deviations on programmes should be assessed. Together with the business sponsor of those programmes, appropriate remedial action should be taken and, if necessary, the programme business case should be updated.',
    'tImplementationGuide' => '',
  ),
  167 => 
  array (
    'fkContext' => '1968',
    'fkSectionBestPractice' => '1657',
    'nControlType' => '0',
    'sName' => 'PO5.5 Benefit Management',
    'tDescription' => 'Implement a process to monitor the benefits from providing and maintaining appropriate IT capabilities. IT\'\'s contribution to the business, either as a component of IT-enabled investment programmes or as part of regular operational support, should be identified and documented in a business case, agreed to, monitored and reported. Reports should be reviewed and, where there are opportunities to improve IT\'\'s contribution, appropriate actions should be defined and taken. Where changes in IT\'\'s contribution impact the programme, or where changes to other related projects impact the programme, the programme business case should be updated.',
    'tImplementationGuide' => '',
  ),
  168 => 
  array (
    'fkContext' => '1969',
    'fkSectionBestPractice' => '1658',
    'nControlType' => '0',
    'sName' => 'PO6.1 IT Policy and Control Environment',
    'tDescription' => 'Define the elements of a control environment for IT, aligned with the enterprise\'\'s management philosophy and operating style. These elements should include expectations/requirements regarding delivery of value from IT investments, appetite for risk, integrity, ethical values, staff competence, accountability and responsibility. The control environment should be based on a culture that supports value delivery whilst managing significant risks, encourages cross-divisional co-operation and teamwork, promotes compliance and continuous process improvement, and handles process deviations (including failure) well.',
    'tImplementationGuide' => '',
  ),
  169 => 
  array (
    'fkContext' => '1970',
    'fkSectionBestPractice' => '1658',
    'nControlType' => '0',
    'sName' => 'PO6.2 Enterprise IT Risk and Control Framework',
    'tDescription' => 'Develop and maintain a framework that defines the enterprise\'\'s overall approach to IT risk and control and that aligns with the IT policy and control environment and the enterprise risk and control framework.',
    'tImplementationGuide' => '',
  ),
  170 => 
  array (
    'fkContext' => '1971',
    'fkSectionBestPractice' => '1658',
    'nControlType' => '0',
    'sName' => 'PO6.3 IT Policies Management',
    'tDescription' => 'Develop and maintain a set of policies to support IT strategy. These policies should include policy intent; roles and responsibilities; exception process; compliance approach; and references to procedures, standards and guidelines. Their relevance should be confirmed and approved regularly.',
    'tImplementationGuide' => '',
  ),
  171 => 
  array (
    'fkContext' => '1972',
    'fkSectionBestPractice' => '1658',
    'nControlType' => '0',
    'sName' => 'PO6.4 Policy, Standard and Procedures Rollout',
    'tDescription' => 'Roll out and enforce IT policies to all relevant staff, so they are built into and are an integral part of enterprise operations.',
    'tImplementationGuide' => '',
  ),
  172 => 
  array (
    'fkContext' => '1973',
    'fkSectionBestPractice' => '1658',
    'nControlType' => '0',
    'sName' => 'PO6.5 Communication of IT Objectives and Direction',
    'tDescription' => 'Communicate awareness and understanding of business and IT objectives and direction to appropriate stakeholders and users throughout the enterprise.',
    'tImplementationGuide' => '',
  ),
  173 => 
  array (
    'fkContext' => '1974',
    'fkSectionBestPractice' => '1659',
    'nControlType' => '0',
    'sName' => 'PO7.1 Personnel Recruitment and Retention',
    'tDescription' => 'Maintain IT personnel recruitment processes in line with the overall organisation\'\'s personnel policies and procedures (e.g., hiring, positive work environment, orienting). Implement processes to ensure that the organisation has an appropriately deployed IT workforce with the skills necessary to achieve organisational goals.',
    'tImplementationGuide' => '',
  ),
  174 => 
  array (
    'fkContext' => '1975',
    'fkSectionBestPractice' => '1659',
    'nControlType' => '0',
    'sName' => 'PO7.2 Personnel Competencies',
    'tDescription' => 'Regularly verify that personnel have the competencies to fulfil their roles on the basis of their education, training and/or experience. Define core IT competency requirements and verify that they are being maintained, using qualification and certification programmes where appropriate.',
    'tImplementationGuide' => '',
  ),
  175 => 
  array (
    'fkContext' => '1976',
    'fkSectionBestPractice' => '1659',
    'nControlType' => '0',
    'sName' => 'PO7.3 Staffing of Roles',
    'tDescription' => 'Define, monitor and supervise roles, responsibilities and compensation frameworks for personnel, including the requirement to adhere to management policies and procedures, the code of ethics, and professional practices. The level of supervision should be in line with the sensitivity of the position and extent of responsibilities assigned.',
    'tImplementationGuide' => '',
  ),
  176 => 
  array (
    'fkContext' => '1977',
    'fkSectionBestPractice' => '1659',
    'nControlType' => '0',
    'sName' => 'PO7.4 Personnel Training',
    'tDescription' => 'Provide IT employees with appropriate orientation when hired and ongoing training to maintain their knowledge, skills, abilities, internal controls and security awareness at the level required to achieve organisational goals.',
    'tImplementationGuide' => '',
  ),
  177 => 
  array (
    'fkContext' => '1978',
    'fkSectionBestPractice' => '1659',
    'nControlType' => '0',
    'sName' => 'PO7.5 Dependence Upon Individuals',
    'tDescription' => 'Minimise the exposure to critical dependency on key individuals through knowledge capture (documentation), knowledge sharing, succession planning and staff backup.',
    'tImplementationGuide' => '',
  ),
  178 => 
  array (
    'fkContext' => '1979',
    'fkSectionBestPractice' => '1659',
    'nControlType' => '0',
    'sName' => 'PO7.6 Personnel Clearance Procedures',
    'tDescription' => 'Include background checks in the IT recruitment process. The extent and frequency of periodic reviews of these checks should depend on the sensitivity and/or criticality of the function and should be applied for employees, contractors and vendors.',
    'tImplementationGuide' => '',
  ),
  179 => 
  array (
    'fkContext' => '1980',
    'fkSectionBestPractice' => '1659',
    'nControlType' => '0',
    'sName' => 'PO7.7 Employee Job Performance Evaluation',
    'tDescription' => 'Require a timely evaluation to be performed on a regular basis against individual objectives derived from the organisation\'\'s goals, established standards and specific job responsibilities. Employees should receive coaching on performance and conduct whenever appropriate.',
    'tImplementationGuide' => '',
  ),
  180 => 
  array (
    'fkContext' => '1981',
    'fkSectionBestPractice' => '1659',
    'nControlType' => '0',
    'sName' => 'PO7.8 Job Change and Termination',
    'tDescription' => 'Take expedient actions regarding job changes, especially job terminations. Knowledge transfer should be arranged, responsibilities reassigned and access rights removed such that risks are minimised and continuity of the function is guaranteed.',
    'tImplementationGuide' => '',
  ),
  181 => 
  array (
    'fkContext' => '1982',
    'fkSectionBestPractice' => '1660',
    'nControlType' => '0',
    'sName' => 'PO8.1 Quality Management System',
    'tDescription' => 'Establish and maintain a QMS that provides a standard, formal and continuous approach regarding quality management that is aligned with business requirements. The QMS should identify quality requirements and criteria; key IT processes and their sequence and interaction; and the policies, criteria and methods for defining, detecting, correcting and preventing non-conformity. The QMS should define the organisational structure for quality management, covering the roles, tasks and responsibilities. All key areas should develop their quality plans in line with criteria and policies and record quality data. Monitor and measure the effectiveness and acceptance of the QMS, and improve it when needed.',
    'tImplementationGuide' => '',
  ),
  182 => 
  array (
    'fkContext' => '1983',
    'fkSectionBestPractice' => '1660',
    'nControlType' => '0',
    'sName' => 'PO8.2 IT Standards and Quality Practices',
    'tDescription' => 'Identify and maintain standards, procedures and practices for key IT processes to guide the organisation in meeting the intent of the QMS. Use industry good practices for reference when improving and tailoring the organisation\'\'s quality practices.',
    'tImplementationGuide' => '',
  ),
  183 => 
  array (
    'fkContext' => '1984',
    'fkSectionBestPractice' => '1660',
    'nControlType' => '0',
    'sName' => 'PO8.3 Development and Acquisition Standards',
    'tDescription' => 'Adopt and maintain standards for all development and acquisition that follow the life cycle of the ultimate deliverable, and include sign-off at key milestones based on agreed-upon sign-off criteria. Consider software coding standards; naming conventions; file formats; schema and data dictionary design standards; user interface standards; interoperability; system performance efficiency; scalability; standards for development and testing; validation against requirements; test plans; and unit, regression and integration testing.',
    'tImplementationGuide' => '',
  ),
  184 => 
  array (
    'fkContext' => '1985',
    'fkSectionBestPractice' => '1660',
    'nControlType' => '0',
    'sName' => 'PO8.4 Customer Focus',
    'tDescription' => 'Focus quality management on customers by determining their requirements and aligning them to the IT standards and practices. Define roles and responsibilities concerning conflict resolution between the user/customer and the IT organisation.',
    'tImplementationGuide' => '',
  ),
  185 => 
  array (
    'fkContext' => '1986',
    'fkSectionBestPractice' => '1660',
    'nControlType' => '0',
    'sName' => 'PO8.5 Continuous Improvement',
    'tDescription' => 'Maintain and regularly communicate an overall quality plan that promotes continuous improvement.',
    'tImplementationGuide' => '',
  ),
  186 => 
  array (
    'fkContext' => '1987',
    'fkSectionBestPractice' => '1660',
    'nControlType' => '0',
    'sName' => 'PO8.6 Quality Measurement, Monitoring and Review',
    'tDescription' => 'Define, plan and implement measurements to monitor continuing compliance to the QMS, as well as the value the QMS provides. Measurement, monitoring and recording of information should be used by the process owner to take appropriate corrective and preventive actions.',
    'tImplementationGuide' => '',
  ),
  187 => 
  array (
    'fkContext' => '1988',
    'fkSectionBestPractice' => '1661',
    'nControlType' => '0',
    'sName' => 'PO9.1 IT Risk Management Framework',
    'tDescription' => 'Establish an IT risk management framework that is aligned to the organisation\'\'s (enterprise\'\'s) risk management framework.',
    'tImplementationGuide' => '',
  ),
  188 => 
  array (
    'fkContext' => '1989',
    'fkSectionBestPractice' => '1661',
    'nControlType' => '0',
    'sName' => 'PO9.2 Establishment of Risk Context',
    'tDescription' => 'Establish the context in which the risk assessment framework is applied to ensure appropriate outcomes. This should include determining the internal and external context of each risk assessment, the goal of the assessment, and the criteria against which risks are evaluated.',
    'tImplementationGuide' => '',
  ),
  189 => 
  array (
    'fkContext' => '1990',
    'fkSectionBestPractice' => '1661',
    'nControlType' => '0',
    'sName' => 'PO9.3 Event Identification',
    'tDescription' => 'Identify events (an important realistic threat that exploits a significant applicable vulnerability) with a potential negative impact on the goals or operations of the enterprise, including business, regulatory, legal, technology, trading partner, human resources and operational aspects. Determine the nature of the impact and maintain this information. Record and maintain relevant risks in a risk registry.',
    'tImplementationGuide' => '',
  ),
  190 => 
  array (
    'fkContext' => '1991',
    'fkSectionBestPractice' => '1661',
    'nControlType' => '0',
    'sName' => 'PO9.4 Risk Assessment',
    'tDescription' => 'Assess on a recurrent basis the likelihood and impact of all identified risks, using qualitative and quantitative methods. The likelihood and impact associated with inherent and residual risk should be determined individually, by category and on a portfolio basis.',
    'tImplementationGuide' => '',
  ),
  191 => 
  array (
    'fkContext' => '1992',
    'fkSectionBestPractice' => '1661',
    'nControlType' => '0',
    'sName' => 'PO9.5 Risk Response',
    'tDescription' => 'Develop and maintain a risk response process designed to ensure that cost-effective controls mitigate exposure to risks on a continuing basis. The risk response process should identify risk strategies such as avoidance, reduction, sharing or acceptance; determine associated responsibilities; and consider risk tolerance levels.',
    'tImplementationGuide' => '',
  ),
  192 => 
  array (
    'fkContext' => '1993',
    'fkSectionBestPractice' => '1661',
    'nControlType' => '0',
    'sName' => 'PO9.6 Maintenance and Monitoring of a Risk Action Plan',
    'tDescription' => 'Prioritise and plan the control activities at all levels to implement the risk responses identified as necessary, including identification of costs, benefits and responsibility for execution. Obtain approval for recommended actions and acceptance of any residual risks, and ensure that committed actions are owned by the affected process owner(s). Monitor execution of the plans, and report on any deviations to senior management.',
    'tImplementationGuide' => '',
  ),
  193 => 
  array (
    'fkContext' => '1994',
    'fkSectionBestPractice' => '1662',
    'nControlType' => '0',
    'sName' => 'PO10.1 Programme Management Framework',
    'tDescription' => 'Maintain the programme of projects, related to the portfolio of IT-enabled investment programmes, by identifying, defining, evaluating, prioritising, selecting, initiating, managing and controlling projects. Ensure that the projects support the programme\'\'s objectives. Co-ordinate the activities and interdependencies of multiple projects, manage the contribution of all the projects within the programme to expected outcomes, and resolve resource requirements and conflicts.',
    'tImplementationGuide' => '',
  ),
  194 => 
  array (
    'fkContext' => '1995',
    'fkSectionBestPractice' => '1662',
    'nControlType' => '0',
    'sName' => 'PO10.2 Project Management Framework',
    'tDescription' => 'Establish and maintain a project management framework that defines the scope and boundaries of managing projects, as well as the method to be adopted and applied to each project undertaken. The framework and supporting method should be integrated with the programme management processes.',
    'tImplementationGuide' => '',
  ),
  195 => 
  array (
    'fkContext' => '1996',
    'fkSectionBestPractice' => '1662',
    'nControlType' => '0',
    'sName' => 'PO10.3 Project Management Approach',
    'tDescription' => 'Establish a project management approach commensurate with the size, complexity and regulatory requirements of each project. The project governance structure can include the roles, responsibilities and accountabilities of the programme sponsor, project sponsors, steering committee, project office and project manager, and the mechanisms through which they can meet those responsibilities (such as reporting and stage reviews). Make sure all IT projects have sponsors with sufficient authority to own the execution of the project within the overall strategic programme.',
    'tImplementationGuide' => '',
  ),
  196 => 
  array (
    'fkContext' => '1997',
    'fkSectionBestPractice' => '1662',
    'nControlType' => '0',
    'sName' => 'PO10.4 Stakeholder Commitment',
    'tDescription' => 'Obtain commitment and participation from the affected stakeholders in the definition and execution of the project within the context of the overall IT-enabled investment programme.',
    'tImplementationGuide' => '',
  ),
  197 => 
  array (
    'fkContext' => '1998',
    'fkSectionBestPractice' => '1662',
    'nControlType' => '0',
    'sName' => 'PO10.5 Project Scope Statement',
    'tDescription' => 'Define and document the nature and scope of the project to confirm and develop amongst stakeholders a common understanding of project scope and how it relates to other projects within the overall IT-enabled investment programme. The definition should be formally approved by the programme and project sponsors before project initiation.',
    'tImplementationGuide' => '',
  ),
  198 => 
  array (
    'fkContext' => '1999',
    'fkSectionBestPractice' => '1662',
    'nControlType' => '0',
    'sName' => 'PO10.6 Project Phase Initiation',
    'tDescription' => 'Approve the initiation of each major project phase and communicate it to all stakeholders. Base the approval of the initial phase on programme governance decisions. Approval of subsequent phases should be based on review and acceptance of the deliverables of the previous phase, and approval of an updated business case at the next major review of the programme. In the event of overlapping project phases, an approval point should be established by programme and project sponsors to authorise project progression.',
    'tImplementationGuide' => '',
  ),
  199 => 
  array (
    'fkContext' => '2000',
    'fkSectionBestPractice' => '1662',
    'nControlType' => '0',
    'sName' => 'PO10.7 Integrated Project Plan',
    'tDescription' => 'Establish a formal, approved integrated project plan (covering business and information systems resources) to guide project execution and control throughout the life of the project. The activities and interdependencies of multiple projects within a programme should be understood and documented. The project plan should be maintained throughout the life of the project. The project plan, and changes to it, should be approved in line with the programme and project governance framework.',
    'tImplementationGuide' => '',
  ),
  200 => 
  array (
    'fkContext' => '2001',
    'fkSectionBestPractice' => '1662',
    'nControlType' => '0',
    'sName' => 'PO10.8 Project Resources',
    'tDescription' => 'Define the responsibilities, relationships, authorities and performance criteria of project team members, and specify the basis for acquiring and assigning competent staff members and/or contractors to the project. The procurement of products and services required for each project should be planned and managed to achieve project objectives using the organisation\'\'s procurement practices.',
    'tImplementationGuide' => '',
  ),
  201 => 
  array (
    'fkContext' => '2002',
    'fkSectionBestPractice' => '1662',
    'nControlType' => '0',
    'sName' => 'PO10.9 Project Risk Management',
    'tDescription' => 'Eliminate or minimise specific risks associated with individual projects through a systematic process of planning, identifying, analysing, responding to, monitoring and controlling the areas or events that have the potential to cause unwanted change. Risks faced by the project management process and the project deliverable should be established and centrally recorded.',
    'tImplementationGuide' => '',
  ),
  202 => 
  array (
    'fkContext' => '2003',
    'fkSectionBestPractice' => '1662',
    'nControlType' => '0',
    'sName' => 'PO10.10 Project Quality Plan',
    'tDescription' => 'Prepare a quality management plan that describes the project quality system and how it will be implemented. The plan should be formally reviewed and agreed to by all parties concerned and then incorporated into the integrated project plan.',
    'tImplementationGuide' => '',
  ),
  203 => 
  array (
    'fkContext' => '2004',
    'fkSectionBestPractice' => '1662',
    'nControlType' => '0',
    'sName' => 'PO10.11 Project Change Control',
    'tDescription' => 'Establish a change control system for each project, so all changes to the project baseline (e.g., cost, schedule, scope, quality) are appropriately reviewed, approved and incorporated into the integrated project plan in line with the programme and project governance framework.',
    'tImplementationGuide' => '',
  ),
  204 => 
  array (
    'fkContext' => '2005',
    'fkSectionBestPractice' => '1662',
    'nControlType' => '0',
    'sName' => 'PO10.12 Project Planning of Assurance Methods',
    'tDescription' => 'Identify assurance tasks required to support the accreditation of new or modified systems during project planning, and include them in the integrated project plan. The tasks should provide assurance that internal controls and security features meet the defined requirements.',
    'tImplementationGuide' => '',
  ),
  205 => 
  array (
    'fkContext' => '2006',
    'fkSectionBestPractice' => '1662',
    'nControlType' => '0',
    'sName' => 'PO10.13 Project Performance Measurement, Reporting and Monitoring',
    'tDescription' => 'Measure project performance against key project performance scope, schedule, quality, cost and risk criteria. Identify any deviations from the plan. Assess the impact of deviations on the project and overall programme, and report results to key stakeholders. Recommend, implement and monitor remedial action, when required, in line with the programme and project governance framework.',
    'tImplementationGuide' => '',
  ),
  206 => 
  array (
    'fkContext' => '2007',
    'fkSectionBestPractice' => '1662',
    'nControlType' => '0',
    'sName' => 'PO10.14 Project Closure',
    'tDescription' => 'Require that, at the end of each project, the project stakeholders ascertain whether the project delivered the planned results and benefits. Identify and communicate any outstanding activities required to achieve the planned results of the project and the benefits of the programme, and identify and document lessons learned for use on future projects and programmes.',
    'tImplementationGuide' => '',
  ),
  207 => 
  array (
    'fkContext' => '2008',
    'fkSectionBestPractice' => '1664',
    'nControlType' => '0',
    'sName' => 'AI1.1 Definition and Maintenance of Business Functional and Technical Requirements',
    'tDescription' => 'Identify, prioritise, specify and agree on business functional and technical requirements covering the full scope of all initiatives required to achieve the expected outcomes of the IT-enabled investment programme.',
    'tImplementationGuide' => '',
  ),
  208 => 
  array (
    'fkContext' => '2009',
    'fkSectionBestPractice' => '1664',
    'nControlType' => '0',
    'sName' => 'AI1.2 Risk Analysis Report',
    'tDescription' => 'Identify, document and analyse risks associated with the business requirements and solution design as part of the organisation\'\'s process for the development of requirements.',
    'tImplementationGuide' => '',
  ),
  209 => 
  array (
    'fkContext' => '2010',
    'fkSectionBestPractice' => '1664',
    'nControlType' => '0',
    'sName' => 'AI1.3 Feasibility Study and Formulation of Alternative Courses of Action',
    'tDescription' => 'Develop a feasibility study that examines the possibility of implementing the requirements. Business management, supported by the IT function, should assess the feasibility and alternative courses of action and make a recommendation to the business sponsor.',
    'tImplementationGuide' => '',
  ),
  210 => 
  array (
    'fkContext' => '2011',
    'fkSectionBestPractice' => '1664',
    'nControlType' => '0',
    'sName' => 'AI1.4 Requirements and Feasibility Decision and Approval',
    'tDescription' => 'Verify that the process requires the business sponsor to approve and sign off on business functional and technical requirements and feasibility study reports at predetermined key stages. The business sponsor should make the final decision with respect to the choice of solution and acquisition approach.',
    'tImplementationGuide' => '',
  ),
  211 => 
  array (
    'fkContext' => '2012',
    'fkSectionBestPractice' => '1665',
    'nControlType' => '0',
    'sName' => 'AI2.1 High-level Design',
    'tDescription' => 'Translate business requirements into a high-level design specification for software acquisition, taking into account the organisation\'\'s technological direction and information architecture. Have the design specifications approved by management to ensure that the high-level design responds to the requirements. Reassess when significant technical or logical discrepancies occur during development or maintenance.',
    'tImplementationGuide' => '',
  ),
  212 => 
  array (
    'fkContext' => '2013',
    'fkSectionBestPractice' => '1665',
    'nControlType' => '0',
    'sName' => 'AI2.2 Detailed Design',
    'tDescription' => 'Prepare detailed design and technical software application requirements. Define the criteria for acceptance of the requirements. Have the requirements approved to ensure that they correspond to the high-level design. Perform reassessment when significant technical or logical discrepancies occur during development or maintenance.',
    'tImplementationGuide' => '',
  ),
  213 => 
  array (
    'fkContext' => '2014',
    'fkSectionBestPractice' => '1665',
    'nControlType' => '0',
    'sName' => 'AI2.3 Application Control and Auditability',
    'tDescription' => 'Implement business controls, where appropriate, into automated application controls such that processing is accurate, complete, timely, authorised and auditable.',
    'tImplementationGuide' => '',
  ),
  214 => 
  array (
    'fkContext' => '2015',
    'fkSectionBestPractice' => '1665',
    'nControlType' => '0',
    'sName' => 'AI2.4 Application Security and Availability',
    'tDescription' => 'Address application security and availability requirements in response to identified risks and in line with the organisation\'\'s data classification, information architecture, information security architecture and risk tolerance.',
    'tImplementationGuide' => '',
  ),
  215 => 
  array (
    'fkContext' => '2016',
    'fkSectionBestPractice' => '1665',
    'nControlType' => '0',
    'sName' => 'AI2.5 Configuration and Implementation of Acquired Application Software',
    'tDescription' => 'Configure and implement acquired application software to meet business objectives.',
    'tImplementationGuide' => '',
  ),
  216 => 
  array (
    'fkContext' => '2017',
    'fkSectionBestPractice' => '1665',
    'nControlType' => '0',
    'sName' => 'AI2.6 Major Upgrades to Existing Systems',
    'tDescription' => 'In the event of major changes to existing systems that result in significant change in current designs and/or functionality, follow a similar development process as that used for the development of new systems.',
    'tImplementationGuide' => '',
  ),
  217 => 
  array (
    'fkContext' => '2018',
    'fkSectionBestPractice' => '1665',
    'nControlType' => '0',
    'sName' => 'AI2.7 Development of Application Software',
    'tDescription' => 'Ensure that automated functionality is developed in accordance with design specifications, development and documentation standards, QA requirements, and approval standards. Ensure that all legal and contractual aspects are identified and addressed for application software developed by third parties.',
    'tImplementationGuide' => '',
  ),
  218 => 
  array (
    'fkContext' => '2019',
    'fkSectionBestPractice' => '1665',
    'nControlType' => '0',
    'sName' => 'AI2.8 Software Quality Assurance',
    'tDescription' => 'Develop, resource and execute a software QA plan to obtain the quality specified in the requirements definition and the organisation\'\'s quality policies and procedures.',
    'tImplementationGuide' => '',
  ),
  219 => 
  array (
    'fkContext' => '2020',
    'fkSectionBestPractice' => '1665',
    'nControlType' => '0',
    'sName' => 'AI2.9 Applications Requirements Management',
    'tDescription' => 'Track the status of individual requirements (including all rejected requirements) during the design, development and implementation, and approve changes to requirements through an established change management process.',
    'tImplementationGuide' => '',
  ),
  220 => 
  array (
    'fkContext' => '2021',
    'fkSectionBestPractice' => '1665',
    'nControlType' => '0',
    'sName' => 'AI2.10 Application Software Maintenance',
    'tDescription' => 'Develop a strategy and plan for the maintenance of software applications.',
    'tImplementationGuide' => '',
  ),
  221 => 
  array (
    'fkContext' => '2022',
    'fkSectionBestPractice' => '1666',
    'nControlType' => '0',
    'sName' => 'AI3.1 Technological Infrastructure Acquisition Plan',
    'tDescription' => 'Produce a plan for the acquisition, implementation and maintenance of the technological infrastructure that meets established business functional and technical requirements and is in accord with the organisation\'\'s technology direction.',
    'tImplementationGuide' => '',
  ),
  222 => 
  array (
    'fkContext' => '2023',
    'fkSectionBestPractice' => '1666',
    'nControlType' => '0',
    'sName' => 'AI3.2 Infrastructure Resource Protection and Availability',
    'tDescription' => 'rastructural software to protect resources and ensure availability and integrity. Responsibilities for using sensitive infrastructure components should be clearly defined and understood by those who develop and integrate infrastructure components. Their use should be monitored and evaluated.',
    'tImplementationGuide' => '',
  ),
  223 => 
  array (
    'fkContext' => '2024',
    'fkSectionBestPractice' => '1666',
    'nControlType' => '0',
    'sName' => 'AI3.3 Infrastructure Maintenance',
    'tDescription' => 'Develop a strategy and plan for infrastructure maintenance, and ensure that changes are controlled in line with the organisation\'\'s change management procedure. Include periodic reviews against business needs, patch management, upgrade strategies, risks, vulnerabilities assessment and security requirements.',
    'tImplementationGuide' => '',
  ),
  224 => 
  array (
    'fkContext' => '2025',
    'fkSectionBestPractice' => '1666',
    'nControlType' => '0',
    'sName' => 'AI3.4 Feasibility Test Environment',
    'tDescription' => 'Establish development and test environments to support effective and efficient feasibility and integration testing of infrastructure components.',
    'tImplementationGuide' => '',
  ),
  225 => 
  array (
    'fkContext' => '2026',
    'fkSectionBestPractice' => '1667',
    'nControlType' => '0',
    'sName' => 'AI4.1 Planning for Operational Solutions',
    'tDescription' => 'Develop a plan to identify and document all technical, operational and usage aspects such that all those who will operate, use and maintain the automated solutions can exercise their responsibility.',
    'tImplementationGuide' => '',
  ),
  226 => 
  array (
    'fkContext' => '2027',
    'fkSectionBestPractice' => '1667',
    'nControlType' => '0',
    'sName' => 'AI4.2 Knowledge Transfer to Business Management',
    'tDescription' => 'Transfer knowledge to business management to allow those individuals to take ownership of the system and data, and exercise responsibility for service delivery and quality, internal control, and application administration.',
    'tImplementationGuide' => '',
  ),
  227 => 
  array (
    'fkContext' => '2028',
    'fkSectionBestPractice' => '1667',
    'nControlType' => '0',
    'sName' => 'AI4.3 Knowledge Transfer to End Users',
    'tDescription' => 'Transfer knowledge and skills to allow end users to effectively and efficiently use the system in support of business processes.',
    'tImplementationGuide' => '',
  ),
  228 => 
  array (
    'fkContext' => '2029',
    'fkSectionBestPractice' => '1667',
    'nControlType' => '0',
    'sName' => 'AI4.4 Knowledge Transfer to Operations and Support Staff',
    'tDescription' => 'Transfer knowledge and skills to enable operations and technical support staff to effectively and efficiently deliver, support and maintain the system and associated infrastructure.',
    'tImplementationGuide' => '',
  ),
  229 => 
  array (
    'fkContext' => '2030',
    'fkSectionBestPractice' => '1668',
    'nControlType' => '0',
    'sName' => 'AI5.1 Procurement Control',
    'tDescription' => 'Develop and follow a set of procedures and standards that is consistent with the business organisation\'\'s overall procurement process and acquisition strategy to acquire IT-related infrastructure, facilities, hardware, software and services needed by the business.',
    'tImplementationGuide' => '',
  ),
  230 => 
  array (
    'fkContext' => '2031',
    'fkSectionBestPractice' => '1668',
    'nControlType' => '0',
    'sName' => 'AI5.2 Supplier Contract Management',
    'tDescription' => 'Set up a procedure for establishing, modifying and terminating contracts for all suppliers. The procedure should cover, at a minimum, legal, financial, organisational, documentary, performance, security, intellectual property, and termination responsibilities and liabilities (including penalty clauses). All contracts and contract changes should be reviewed by legal advisors.',
    'tImplementationGuide' => '',
  ),
  231 => 
  array (
    'fkContext' => '2032',
    'fkSectionBestPractice' => '1668',
    'nControlType' => '0',
    'sName' => 'AI5.3 Supplier Selection',
    'tDescription' => 'Select suppliers according to a fair and formal practice to ensure a viable best fit based on specified requirements. Requirements should be optimised with input from potential suppliers.',
    'tImplementationGuide' => '',
  ),
  232 => 
  array (
    'fkContext' => '2033',
    'fkSectionBestPractice' => '1668',
    'nControlType' => '0',
    'sName' => 'AI5.4 IT Resources Acquisition',
    'tDescription' => 'Protect and enforce the organisation\'\'s interests in all acquisition contractual agreements, including the rights and obligations of all parties in the contractual terms for the acquisition of software, development resources, infrastructure and services.',
    'tImplementationGuide' => '',
  ),
  233 => 
  array (
    'fkContext' => '2034',
    'fkSectionBestPractice' => '1669',
    'nControlType' => '0',
    'sName' => 'AI6.1 Change Standards and Procedures',
    'tDescription' => 'Set up formal change management procedures to handle in a standardised manner all requests (including maintenance and patches) for changes to applications, procedures, processes, system and service parameters, and the underlying platforms.',
    'tImplementationGuide' => '',
  ),
  234 => 
  array (
    'fkContext' => '2035',
    'fkSectionBestPractice' => '1669',
    'nControlType' => '0',
    'sName' => 'AI6.2 Impact Assessment, Prioritisation and Authorisation',
    'tDescription' => 'Assess all requests for change in a structured way to determine the impact on the operational system and its functionality. Ensure that changes are categorised, prioritised and authorised.',
    'tImplementationGuide' => '',
  ),
  235 => 
  array (
    'fkContext' => '2036',
    'fkSectionBestPractice' => '1669',
    'nControlType' => '0',
    'sName' => 'AI6.3 Emergency Changes',
    'tDescription' => 'Establish a process for defining, raising, testing, documenting, assessing and authorising emergency changes that do not follow the established change process.',
    'tImplementationGuide' => '',
  ),
  236 => 
  array (
    'fkContext' => '2037',
    'fkSectionBestPractice' => '1669',
    'nControlType' => '0',
    'sName' => 'AI6.4 Change Status Tracking and Reporting',
    'tDescription' => 'Establish a tracking and reporting system to document rejected changes, communicate the status of approved and in-process changes, and complete changes. Make certain that approved changes are implemented as planned.',
    'tImplementationGuide' => '',
  ),
  237 => 
  array (
    'fkContext' => '2038',
    'fkSectionBestPractice' => '1669',
    'nControlType' => '0',
    'sName' => 'AI6.5 Change Closure and Documentation',
    'tDescription' => 'Whenever changes are implemented, update the associated system and user documentation and procedures accordingly.',
    'tImplementationGuide' => '',
  ),
  238 => 
  array (
    'fkContext' => '2039',
    'fkSectionBestPractice' => '1670',
    'nControlType' => '0',
    'sName' => 'AI7.1 Training',
    'tDescription' => 'Train the staff members of the affected user departments and the operations group of the IT function in accordance with the defined training and implementation plan and associated materials, as part of every information systems development, implementation or modification project.',
    'tImplementationGuide' => '',
  ),
  239 => 
  array (
    'fkContext' => '2040',
    'fkSectionBestPractice' => '1670',
    'nControlType' => '0',
    'sName' => 'AI7.2 Test Plan',
    'tDescription' => 'Establish a test plan based on organisationwide standards that defines roles, responsibilities, and entry and exit criteria. Ensure that the plan is approved by relevant parties.',
    'tImplementationGuide' => '',
  ),
  240 => 
  array (
    'fkContext' => '2041',
    'fkSectionBestPractice' => '1670',
    'nControlType' => '0',
    'sName' => 'AI7.3 Implementation Plan',
    'tDescription' => 'Establish an implementation and fallback/backout plan. Obtain approval from relevant parties.',
    'tImplementationGuide' => '',
  ),
  241 => 
  array (
    'fkContext' => '2042',
    'fkSectionBestPractice' => '1670',
    'nControlType' => '0',
    'sName' => 'AI7.4 Test Environment',
    'tDescription' => 'Define and establish a secure test environment representative of the planned operations environment relative to security, internal controls, operational practices, data quality and privacy requirements, and workloads.',
    'tImplementationGuide' => '',
  ),
  242 => 
  array (
    'fkContext' => '2043',
    'fkSectionBestPractice' => '1670',
    'nControlType' => '0',
    'sName' => 'AI7.5 System and Data Conversion',
    'tDescription' => 'Plan data conversion and infrastructure migration as part of the organisation\'\'s development methods, including audit trails, rollbacks and fallbacks.',
    'tImplementationGuide' => '',
  ),
  243 => 
  array (
    'fkContext' => '2044',
    'fkSectionBestPractice' => '1670',
    'nControlType' => '0',
    'sName' => 'AI7.6 Testing of Changes',
    'tDescription' => 'Test changes independently in accordance with the defined test plan prior to migration to the operational environment. Ensure that the plan considers security and performance.',
    'tImplementationGuide' => '',
  ),
  244 => 
  array (
    'fkContext' => '2045',
    'fkSectionBestPractice' => '1670',
    'nControlType' => '0',
    'sName' => 'AI7.7 Final Acceptance Test',
    'tDescription' => 'Ensure that business process owners and IT stakeholders evaluate the outcome of the testing process as determined by the test plan. Remediate significant errors identified in the testing process, having completed the suite of tests identified in the test plan and any necessary regression tests. Following evaluation, approve promotion to production.',
    'tImplementationGuide' => '',
  ),
  245 => 
  array (
    'fkContext' => '2046',
    'fkSectionBestPractice' => '1670',
    'nControlType' => '0',
    'sName' => 'AI7.8 Promotion to Production',
    'tDescription' => 'Following testing, control the handover of the changed system to operations, keeping it in line with the implementation plan. Obtain approval of the key stakeholders, such as users, system owner and operational management. Where appropriate, run the system in parallel with the old system for a while, and compare behaviour and results.',
    'tImplementationGuide' => '',
  ),
  246 => 
  array (
    'fkContext' => '2047',
    'fkSectionBestPractice' => '1670',
    'nControlType' => '0',
    'sName' => 'AI7.9 Post-implementation Review',
    'tDescription' => 'Establish procedures in line with the organisational change management standards to require a post-implementation review as set out in the implementation plan.',
    'tImplementationGuide' => '',
  ),
  247 => 
  array (
    'fkContext' => '2048',
    'fkSectionBestPractice' => '1672',
    'nControlType' => '0',
    'sName' => 'DS1.1 Service Level Management Framework',
    'tDescription' => 'Define a framework that provides a formalised service level management process between the customer and service provider. The framework should maintain continuous alignment with business requirements and priorities and facilitate common understanding between the customer and provider(s). The framework should include processes for creating service requirements, service definitions, SLAs, OLAs and funding sources. These attributes should be organised in a service catalogue. The framework should define the organisational structure for service level management, covering the roles, tasks and responsibilities of internal and external service providers and customers.',
    'tImplementationGuide' => '',
  ),
  248 => 
  array (
    'fkContext' => '2049',
    'fkSectionBestPractice' => '1672',
    'nControlType' => '0',
    'sName' => 'DS1.2 Definition of Services',
    'tDescription' => 'Base definitions of IT services on service characteristics and business requirements. Ensure that they are organised and stored centrally via the implementation of a service catalogue portfolio approach.',
    'tImplementationGuide' => '',
  ),
  249 => 
  array (
    'fkContext' => '2050',
    'fkSectionBestPractice' => '1672',
    'nControlType' => '0',
    'sName' => 'DS1.3 Service Level Agreements',
    'tDescription' => 'Define and agree to SLAs for all critical IT services based on customer requirements and IT capabilities. This should cover customer commitments; service support requirements; quantitative and qualitative metrics for measuring the service signed off on by the stakeholders; funding and commercial arrangements, if applicable; and roles and responsibilities, including oversight of the SLA. Consider items such as availability, reliability, performance, capacity for growth, levels of support, continuity planning, security and demand constraints.',
    'tImplementationGuide' => '',
  ),
  250 => 
  array (
    'fkContext' => '2051',
    'fkSectionBestPractice' => '1672',
    'nControlType' => '0',
    'sName' => 'DS1.4 Operating Level Agreements',
    'tDescription' => 'Define OLAs that explain how the services will be technically delivered to support the SLA(s) in an optimal manner. The OLAs should specify the technical processes in terms meaningful to the provider and may support several SLAs.',
    'tImplementationGuide' => '',
  ),
  251 => 
  array (
    'fkContext' => '2052',
    'fkSectionBestPractice' => '1672',
    'nControlType' => '0',
    'sName' => 'DS1.5 Monitoring and Reporting of Service Level Achievements',
    'tDescription' => 'Continuously monitor specified service level performance criteria. Reports on achievement of service levels should be provided in a format that is meaningful to the stakeholders. The monitoring statistics should be analysed and acted upon to identify negative and positive trends for individual services as well as for services overall.',
    'tImplementationGuide' => '',
  ),
  252 => 
  array (
    'fkContext' => '2053',
    'fkSectionBestPractice' => '1672',
    'nControlType' => '0',
    'sName' => 'DS1.6 Review of Service Level Agreements and Contracts',
    'tDescription' => 'Regularly review SLAs and underpinning contracts (UCs) with internal and external service providers to ensure that they are effective and up to date and that changes in requirements have been taken into account.',
    'tImplementationGuide' => '',
  ),
  253 => 
  array (
    'fkContext' => '2054',
    'fkSectionBestPractice' => '1673',
    'nControlType' => '0',
    'sName' => 'DS2.1 Identification of All Supplier Relationships',
    'tDescription' => 'Identify all supplier services, and categorise them according to supplier type, significance and criticality. Maintain formal documentation of technical and organisational relationships covering the roles and responsibilities, goals, expected deliverables, and credentials of representatives of these suppliers.',
    'tImplementationGuide' => '',
  ),
  254 => 
  array (
    'fkContext' => '2055',
    'fkSectionBestPractice' => '1673',
    'nControlType' => '0',
    'sName' => 'DS2.2 Supplier Relationship Management',
    'tDescription' => 'Formalise the supplier relationship management process for each supplier. The relationship owners should liaise on customer and supplier issues and ensure the quality of the relationship based on trust and transparency (e.g., through SLAs).',
    'tImplementationGuide' => '',
  ),
  255 => 
  array (
    'fkContext' => '2056',
    'fkSectionBestPractice' => '1673',
    'nControlType' => '0',
    'sName' => 'DS2.3 Supplier Risk Management',
    'tDescription' => 'Identify and mitigate risks relating to suppliers\'\' ability to continue effective service delivery in a secure and efficient manner on a continual basis. Ensure that contracts conform to universal business standards in accordance with legal and regulatory requirements. Risk management should further consider non-disclosure agreements (NDAs), escrow contracts, continued supplier viability, conformance with security requirements, alternative suppliers, penalties and rewards, etc.',
    'tImplementationGuide' => '',
  ),
  256 => 
  array (
    'fkContext' => '2057',
    'fkSectionBestPractice' => '1673',
    'nControlType' => '0',
    'sName' => 'DS2.4 Supplier Performance Monitoring',
    'tDescription' => 'Establish a process to monitor service delivery to ensure that the supplier is meeting current business requirements and continuing to adhere to the contract agreements and SLAs, and that performance is competitive with alternative suppliers and market conditions.',
    'tImplementationGuide' => '',
  ),
  257 => 
  array (
    'fkContext' => '2058',
    'fkSectionBestPractice' => '1674',
    'nControlType' => '0',
    'sName' => 'DS3.1 Performance and Capacity Planning',
    'tDescription' => 'Establish a planning process for the review of performance and capacity of IT resources to ensure that cost-justifiable capacity and performance are available to process the agreed-upon workloads as determined by the SLAs. Capacity and performance plans should leverage appropriate modelling techniques to produce a model of the current and forecasted performance, capacity and throughput of the IT resources.',
    'tImplementationGuide' => '',
  ),
  258 => 
  array (
    'fkContext' => '2059',
    'fkSectionBestPractice' => '1674',
    'nControlType' => '0',
    'sName' => 'DS3.2 Current Performance and Capacity',
    'tDescription' => 'Assess current performance and capacity of IT resources to determine if sufficient capacity and performance exist to deliver against agreed-upon service levels.',
    'tImplementationGuide' => '',
  ),
  259 => 
  array (
    'fkContext' => '2060',
    'fkSectionBestPractice' => '1674',
    'nControlType' => '0',
    'sName' => 'DS3.3 Future Performance and Capacity',
    'tDescription' => 'Conduct performance and capacity forecasting of IT resources at regular intervals to minimise the risk of service disruptions due to insufficient capacity or performance degradation, and identify excess capacity for possible redeployment. Identify workload trends and determine forecasts to be input to performance and capacity plans.',
    'tImplementationGuide' => '',
  ),
  260 => 
  array (
    'fkContext' => '2061',
    'fkSectionBestPractice' => '1674',
    'nControlType' => '0',
    'sName' => 'DS3.4 IT Resources Availability',
    'tDescription' => 'Provide the required capacity and performance, taking into account aspects such as normal workloads, contingencies, storage requirements and IT resource life cycles. Provisions such as prioritising tasks, fault-tolerance mechanisms and resource allocation practices should be made. Management should ensure that contingency plans properly address availability, capacity and performance of individual IT resources.',
    'tImplementationGuide' => '',
  ),
  261 => 
  array (
    'fkContext' => '2062',
    'fkSectionBestPractice' => '1674',
    'nControlType' => '0',
    'sName' => 'DS3.5 Monitoring and Reporting',
    'tDescription' => 'Continuously monitor the performance and capacity of IT resources. Data gathered should serve two purposes: - To maintain and tune current performance within IT and address such issues as resilience, contingency, current and  projected workloads, storage plans, and resource acquisition - To report delivered service availability to the business, as required by the SLAs Accompany all exception reports with recommendations for corrective action.',
    'tImplementationGuide' => '',
  ),
  262 => 
  array (
    'fkContext' => '2063',
    'fkSectionBestPractice' => '1675',
    'nControlType' => '0',
    'sName' => 'DS4.1 IT Continuity Framework',
    'tDescription' => 'Develop a framework for IT continuity to support enterprisewide business continuity management using a consistent process. The objective of the framework should be to assist in determining the required resilience of the infrastructure and to drive the development of disaster recovery and IT contingency plans. The framework should address the organisational structure for continuity management, covering the roles, tasks and responsibilities of internal and external service providers, their management and their customers, and the planning processes that create the rules and structures to document, test and execute the disaster recovery and IT contingency plans. The plan should also address items such as the identification of critical resources, noting key dependencies, the monitoring and reporting of the availability of critical resources, alternative processing, and the principles of backup and recovery.',
    'tImplementationGuide' => '',
  ),
  263 => 
  array (
    'fkContext' => '2064',
    'fkSectionBestPractice' => '1675',
    'nControlType' => '0',
    'sName' => 'DS4.2 IT Continuity Plans',
    'tDescription' => 'Develop IT continuity plans based on the framework and designed to reduce the impact of a major disruption on key business functions and processes. The plans should be based on risk understanding of potential business impacts and address requirements for resilience, alternative processing and recovery capability of all critical IT services. They should also cover usage guidelines, roles and responsibilities, procedures, communication processes, and the testing approach.',
    'tImplementationGuide' => '',
  ),
  264 => 
  array (
    'fkContext' => '2065',
    'fkSectionBestPractice' => '1675',
    'nControlType' => '0',
    'sName' => 'DS4.3 Critical IT Resources',
    'tDescription' => 'Focus attention on items specified as most critical in the IT continuity plan to build in resilience and establish priorities in recovery situations. Avoid the distraction of recovering less-critical items and ensure response and recovery in line with prioritised business needs, while ensuring that costs are kept at an acceptable level and complying with regulatory and contractual requirements. Consider resilience, response and recovery requirements for different tiers, e.g., one to four hours, four to 24 hours, more than 24 hours and critical business operational periods.',
    'tImplementationGuide' => '',
  ),
  265 => 
  array (
    'fkContext' => '2066',
    'fkSectionBestPractice' => '1675',
    'nControlType' => '0',
    'sName' => 'DS4.4 Maintenance of the IT Continuity Plan',
    'tDescription' => 'Encourage IT management to define and execute change control procedures to ensure that the IT continuity plan is kept up to date and continually reflects actual business requirements. Communicate changes in procedures and responsibilities clearly and in a timely manner.',
    'tImplementationGuide' => '',
  ),
  266 => 
  array (
    'fkContext' => '2067',
    'fkSectionBestPractice' => '1675',
    'nControlType' => '0',
    'sName' => 'DS4.5 Testing of the IT Continuity Plan',
    'tDescription' => 'Test the IT continuity plan on a regular basis to ensure that IT systems can be effectively recovered, shortcomings are addressed and the plan remains relevant. This requires careful preparation, documentation, reporting of test results and, according to the results, implementation of an action plan. Consider the extent of testing recovery of single applications to integrated testing scenarios to end to-end testing and integrated vendor testing.',
    'tImplementationGuide' => '',
  ),
  267 => 
  array (
    'fkContext' => '2068',
    'fkSectionBestPractice' => '1675',
    'nControlType' => '0',
    'sName' => 'DS4.6 IT Continuity Plan Training',
    'tDescription' => 'Provide all concerned parties with regular training sessions regarding the procedures and their roles and responsibilities in case of na incident or disaster. Verify and enhance training according to the results of the contingency tests.',
    'tImplementationGuide' => '',
  ),
  268 => 
  array (
    'fkContext' => '2069',
    'fkSectionBestPractice' => '1675',
    'nControlType' => '0',
    'sName' => 'DS4.7 Distribution of the IT Continuity Plan',
    'tDescription' => 'Determine that a defined and managed distribution strategy exists to ensure that plans are properly and securely distributed and available to appropriately authorised interested parties when and where needed. Attention should be paid to making the plans accessible under all disaster scenarios.',
    'tImplementationGuide' => '',
  ),
  269 => 
  array (
    'fkContext' => '2070',
    'fkSectionBestPractice' => '1675',
    'nControlType' => '0',
    'sName' => 'DS4.8 IT Services Recovery and Resumption',
    'tDescription' => 'Plan the actions to be taken for the period when IT is recovering and resuming services. This may include activation of backup sites, initiation of alternative processing, customer and stakeholder communication, and resumption procedures. Ensure that the business understands IT recovery times and the necessary technology investments to support business recovery and resumption needs.',
    'tImplementationGuide' => '',
  ),
  270 => 
  array (
    'fkContext' => '2071',
    'fkSectionBestPractice' => '1675',
    'nControlType' => '0',
    'sName' => 'DS4.9 Offsite Backup Storage',
    'tDescription' => 'Store offsite all critical backup media, documentation and other IT resources necessary for IT recovery and business continuity plans. Determine the content of backup storage in collaboration between business process owners and IT personnel. Management of the offsite storage facility should respond to the data classification policy and the enterprise\'\'s media storage practices. IT management should ensure that offsite arrangements are periodically assessed, at least annually, for content, environmental protection and security. Ensure compatibility of hardware and software to restore archived data, and periodically test and refresh archived data.',
    'tImplementationGuide' => '',
  ),
  271 => 
  array (
    'fkContext' => '2072',
    'fkSectionBestPractice' => '1675',
    'nControlType' => '0',
    'sName' => 'DS4.10 Post-resumption Review',
    'tDescription' => 'Determine whether IT management has established procedures for assessing the adequacy of the plan in regard to the successful resumption of the IT function after a disaster, and update the plan accordingly.',
    'tImplementationGuide' => '',
  ),
  272 => 
  array (
    'fkContext' => '2073',
    'fkSectionBestPractice' => '1676',
    'nControlType' => '0',
    'sName' => 'DS5.1 Management of IT Security',
    'tDescription' => 'Manage IT security at the highest appropriate organisational level, so the management of security actions is in line with business requirements.',
    'tImplementationGuide' => '',
  ),
  273 => 
  array (
    'fkContext' => '2074',
    'fkSectionBestPractice' => '1676',
    'nControlType' => '0',
    'sName' => 'DS5.2 IT Security Plan',
    'tDescription' => 'Translate business, risk and compliance requirements into an overall IT security plan, taking into consideration the IT infrastructure and the security culture. Ensure that the plan is implemented in security policies and procedures together with appropriate investments in services, personnel, software and hardware. Communicate security policies and procedures to stakeholders and users.',
    'tImplementationGuide' => '',
  ),
  274 => 
  array (
    'fkContext' => '2075',
    'fkSectionBestPractice' => '1676',
    'nControlType' => '0',
    'sName' => 'DS5.3 Identity Management',
    'tDescription' => 'Ensure that all users (internal, external and temporary) and their activity on IT systems (business application, IT environment, system operations, development and maintenance) are uniquely identifiable. Enable user identities via authentication mechanisms. Confirm that user access rights to systems and data are in line with defined and documented business needs and that job requirements are attached to user identities. Ensure that user access rights are requested by user management, approved by system owners and implemented by the security-responsible person. Maintain user identities and access rights in a central repository. Deploy cost-effective technical and procedural measures, and keep them current to establish user identification, implement authentication and enforce access rights.',
    'tImplementationGuide' => '',
  ),
  275 => 
  array (
    'fkContext' => '2076',
    'fkSectionBestPractice' => '1676',
    'nControlType' => '0',
    'sName' => 'DS5.4 User Account Management',
    'tDescription' => 'Address requesting, establishing, issuing, suspending, modifying and closing user accounts and related user privileges with a set of user account management procedures. Include an approval procedure outlining the data or system owner granting the access privileges. These procedures should apply for all users, including administrators (privileged users) and internal and external users, for normal and emergency cases. Rights and obligations relative to access to enterprise systems and information should be contractually arranged for all types of users. Perform regular management review of all accounts and related privileges.',
    'tImplementationGuide' => '',
  ),
  276 => 
  array (
    'fkContext' => '2077',
    'fkSectionBestPractice' => '1676',
    'nControlType' => '0',
    'sName' => 'DS5.5 Security Testing, Surveillance and Monitoring',
    'tDescription' => 'Test and monitor the IT security implementation in a proactive way. IT security should be reaccredited in a timely manner to ensure that the approved enterprise\'\'s information security baseline is maintained. A logging and monitoring function will enable the early prevention and/or detection and subsequent timely reporting of unusual and/or abnormal activities that may need to be addressed.',
    'tImplementationGuide' => '',
  ),
  277 => 
  array (
    'fkContext' => '2078',
    'fkSectionBestPractice' => '1676',
    'nControlType' => '0',
    'sName' => 'DS5.6 Security Incident Definition',
    'tDescription' => 'Clearly define and communicate the characteristics of potential security incidents so they can be properly classified and treated by the incident and problem management process.',
    'tImplementationGuide' => '',
  ),
  278 => 
  array (
    'fkContext' => '2079',
    'fkSectionBestPractice' => '1676',
    'nControlType' => '0',
    'sName' => 'DS5.7 Protection of Security Technology',
    'tDescription' => 'Make security-related technology resistant to tampering, and do not disclose security documentation unnecessarily.',
    'tImplementationGuide' => '',
  ),
  279 => 
  array (
    'fkContext' => '2080',
    'fkSectionBestPractice' => '1676',
    'nControlType' => '0',
    'sName' => 'DS5.8 Cryptographic Key Management',
    'tDescription' => 'Determine that policies and procedures are in place to organise the generation, change, revocation, destruction, distribution, certification, storage, entry, use and archiving of cryptographic keys to ensure the protection of keys against modification and unauthorised disclosure.',
    'tImplementationGuide' => '',
  ),
  280 => 
  array (
    'fkContext' => '2081',
    'fkSectionBestPractice' => '1676',
    'nControlType' => '0',
    'sName' => 'DS5.9 Malicious Software Prevention, Detection and Correction',
    'tDescription' => 'Put preventive, detective and corrective measures in place (especially up-to-date security patches and virus control) across the organisation to protect information systems and technology from malware (e.g., viruses, worms, spyware, spam).',
    'tImplementationGuide' => '',
  ),
  281 => 
  array (
    'fkContext' => '2082',
    'fkSectionBestPractice' => '1676',
    'nControlType' => '0',
    'sName' => 'DS5.10 Network Security',
    'tDescription' => 'Use security techniques and related management procedures (e.g., firewalls, security appliances, network segmentation, intrusion detection) to authorise access and control information flows from and to networks.',
    'tImplementationGuide' => '',
  ),
  282 => 
  array (
    'fkContext' => '2083',
    'fkSectionBestPractice' => '1676',
    'nControlType' => '0',
    'sName' => 'DS5.11 Exchange of Sensitive Data',
    'tDescription' => 'Exchange sensitive transaction data only over a trusted path or medium with controls to provide authenticity of content, proof of submission, proof of receipt and non-repudiation of origin.',
    'tImplementationGuide' => '',
  ),
  283 => 
  array (
    'fkContext' => '2084',
    'fkSectionBestPractice' => '1677',
    'nControlType' => '0',
    'sName' => 'DS6.1 Definition of Services',
    'tDescription' => 'Identify all IT costs, and map them to IT services to support a transparent cost model. IT services should be linked to business processes such that the business can identify associated service billing levels.',
    'tImplementationGuide' => '',
  ),
  284 => 
  array (
    'fkContext' => '2085',
    'fkSectionBestPractice' => '1677',
    'nControlType' => '0',
    'sName' => 'DS6.2 IT Accounting',
    'tDescription' => 'Capture and allocate actual costs according to the enterprise cost model. Variances between forecasts and actual costs should be analysed and reported on, in compliance with the enterprise\'\'s financial measurement systems.',
    'tImplementationGuide' => '',
  ),
  285 => 
  array (
    'fkContext' => '2086',
    'fkSectionBestPractice' => '1677',
    'nControlType' => '0',
    'sName' => 'DS6.3 Cost Modelling and Charging',
    'tDescription' => 'Establish and use an IT costing model based on the service definitions that support the calculation of chargeback rates per service. The IT cost model should ensure that charging for services is identifiable, measurable and predictable by users to encourage proper use of resources.',
    'tImplementationGuide' => '',
  ),
  286 => 
  array (
    'fkContext' => '2087',
    'fkSectionBestPractice' => '1677',
    'nControlType' => '0',
    'sName' => 'DS6.4 Cost Model Maintenance',
    'tDescription' => 'Regularly review and benchmark the appropriateness of the cost/recharge model to maintain its relevance and appropriateness to the evolving business and IT activities.',
    'tImplementationGuide' => '',
  ),
  287 => 
  array (
    'fkContext' => '2088',
    'fkSectionBestPractice' => '1678',
    'nControlType' => '0',
    'sName' => 'DS7.1 Identification of Education and Training Needs',
    'tDescription' => 'Establish and regularly update a curriculum for each target group of employees considering:  - Current and future business needs and strategy  - Value of information as an asset  - Corporate values (ethical values, control and security culture, etc.)  - Implementation of new IT infrastructure and software (i.e., packages, applications)  - Current and future skills, competence profiles, and certification and/or credentialing needs as well as required reaccreditation  - Delivery methods (e.g., classroom, web-based), target group size, accessibility and timing',
    'tImplementationGuide' => '',
  ),
  288 => 
  array (
    'fkContext' => '2089',
    'fkSectionBestPractice' => '1678',
    'nControlType' => '0',
    'sName' => 'DS7.2 Delivery of Training and Education',
    'tDescription' => 'Based on the identified education and training needs, identify target groups and their members, efficient delivery mechanisms, teachers, trainers, and mentors. Appoint trainers and organise timely training sessions. Record registration (including prerequisites), attendance and training session performance evaluations.',
    'tImplementationGuide' => '',
  ),
  289 => 
  array (
    'fkContext' => '2090',
    'fkSectionBestPractice' => '1678',
    'nControlType' => '0',
    'sName' => 'DS7.3 Evaluation of Training Received',
    'tDescription' => 'Evaluate education and training content delivery upon completion for relevance, quality, effectiveness, the retention of knowledge, cost and value. The results of this evaluation should serve as input for future curriculum definition and the delivery of training sessions.',
    'tImplementationGuide' => '',
  ),
  290 => 
  array (
    'fkContext' => '2091',
    'fkSectionBestPractice' => '1679',
    'nControlType' => '0',
    'sName' => 'DS8.1 Service Desk',
    'tDescription' => 'Establish a service desk function, which is the user interface with IT, to register, communicate, dispatch and analyse all calls, reported incidents, service requests and information demands. There should be monitoring and escalation procedures based on agreed-upon service levels relative to the appropriate SLA that allow classification and prioritisation of any reported issue as an incident, service request or information request. Measure end users\'\' satisfaction with the quality of the service desk and IT services.',
    'tImplementationGuide' => '',
  ),
  291 => 
  array (
    'fkContext' => '2092',
    'fkSectionBestPractice' => '1679',
    'nControlType' => '0',
    'sName' => 'DS8.2 Registration of Customer Queries',
    'tDescription' => 'Establish a function and system to allow logging and tracking of calls, incidents, service requests and information needs. It should work closely with such processes as incident management, problem management, change management, capacity management and availability management. Incidents should be classified according to a business and service priority and routed to the appropriate problem management team, where necessary. Customers should be kept informed of the status of their queries.',
    'tImplementationGuide' => '',
  ),
  292 => 
  array (
    'fkContext' => '2093',
    'fkSectionBestPractice' => '1679',
    'nControlType' => '0',
    'sName' => 'DS8.3 Incident Escalation',
    'tDescription' => 'Establish service desk procedures, so incidents that cannot be resolved immediately are appropriately escalated according to limits defined in the SLA and, if appropriate, workarounds are provided. Ensure that incident ownership and life cycle monitoring remain with the service desk for user-based incidents, regardless which IT group is working on resolution activities.',
    'tImplementationGuide' => '',
  ),
  293 => 
  array (
    'fkContext' => '2094',
    'fkSectionBestPractice' => '1679',
    'nControlType' => '0',
    'sName' => 'DS8.4 Incident Closure',
    'tDescription' => 'Establish procedures for the timely monitoring of clearance of customer queries. When the incident has been resolved, ensure that the service desk records the resolution steps, and confirm that the action taken has been agreed to by the customer. Also record and report unresolved incidents (known errors and workarounds) to provide information for proper problem management.',
    'tImplementationGuide' => '',
  ),
  294 => 
  array (
    'fkContext' => '2095',
    'fkSectionBestPractice' => '1679',
    'nControlType' => '0',
    'sName' => 'DS8.5 Reporting and Trend Analysis',
    'tDescription' => 'Produce reports of service desk activity to enable management to measure service performance and service response times and to identify trends or recurring problems, so service can be continually improved.',
    'tImplementationGuide' => '',
  ),
  295 => 
  array (
    'fkContext' => '2096',
    'fkSectionBestPractice' => '1680',
    'nControlType' => '0',
    'sName' => 'DS9.1 Configuration Repository and Baseline',
    'tDescription' => 'Establish a supporting tool and a central repository to contain all relevant information on configuration items. Monitor and record all assets and changes to assets. Maintain a baseline of configuration items for every system and service as a checkpoint to which to return after changes.',
    'tImplementationGuide' => '',
  ),
  296 => 
  array (
    'fkContext' => '2097',
    'fkSectionBestPractice' => '1680',
    'nControlType' => '0',
    'sName' => 'DS9.2 Identification and Maintenance of Configuration Items',
    'tDescription' => 'Establish configuration procedures to support management and logging of all changes to the configuration repository. Integrate these procedures with change management, incident management and problem management procedures.',
    'tImplementationGuide' => '',
  ),
  297 => 
  array (
    'fkContext' => '2098',
    'fkSectionBestPractice' => '1680',
    'nControlType' => '0',
    'sName' => 'DS9.3 Configuration Integrity Review',
    'tDescription' => 'Periodically review the configuration data to verify and confirm the integrity of the current and historical configuration. Periodically review installed software against the policy for software usage to identify personal or unlicensed software or any software instances in excess of current license agreements. Report, act on and correct errors and deviations.',
    'tImplementationGuide' => '',
  ),
  298 => 
  array (
    'fkContext' => '2099',
    'fkSectionBestPractice' => '1681',
    'nControlType' => '0',
    'sName' => 'DS10.1 Identification and Classification of Problems',
    'tDescription' => 'Implement processes to report and classify problems that have been identified as part of incident management. The steps involved in problem classification are similar to the steps in classifying incidents; they are to determine category, impact, urgency and priority. Categorise problems as appropriate into related groups or domains (e.g., hardware, software, support software). These groups may match the organisational responsibilities of the user and customer base, and should be the basis for allocating problems to support staff.',
    'tImplementationGuide' => '',
  ),
  299 => 
  array (
    'fkContext' => '2100',
    'fkSectionBestPractice' => '1681',
    'nControlType' => '0',
    'sName' => 'DS10.2 Problem Tracking and Resolution',
    'tDescription' => 'Ensure that the problem management system provides for adequate audit trail facilities that allow tracking, analysing and determining the root cause of all reported problems considering:  - All associated configuration items  - Outstanding problems and incidents  - Known and suspected errors  - Tracking of problem trends  Identify and initiate sustainable solutions addressing the root cause, raising change requests via the established change management process. Throughout the resolution process, problem management should obtain regular reports from change management on progress in resolving problems and errors. Problem management should monitor the continuing impact of problems and known errors on user services. In the event that this impact becomes severe, problem management should escalate the problem, perhaps referring it to an appropriate board to increase the priority of the (RFC or to implement an urgent change as appropriate. Monitor the progress of problem resolution against SLAs.',
    'tImplementationGuide' => '',
  ),
  300 => 
  array (
    'fkContext' => '2101',
    'fkSectionBestPractice' => '1681',
    'nControlType' => '0',
    'sName' => 'DS10.3 Problem Closure',
    'tDescription' => 'Put in place a procedure to close problem records either after confirmation of successful elimination of the known error or after agreement with the business on how to alternatively handle the problem.',
    'tImplementationGuide' => '',
  ),
  301 => 
  array (
    'fkContext' => '2102',
    'fkSectionBestPractice' => '1681',
    'nControlType' => '0',
    'sName' => 'DS10.4 Integration of Configuration, Incident and Problem Management',
    'tDescription' => 'Integrate the related processes of configuration, incident and problem management to ensure effective management of problems and enable improvements.',
    'tImplementationGuide' => '',
  ),
  302 => 
  array (
    'fkContext' => '2103',
    'fkSectionBestPractice' => '1682',
    'nControlType' => '0',
    'sName' => 'DS11.1 Business Requirements for Data Management',
    'tDescription' => 'Verify that all data expected for processing are received and processed completely, accurately and in a timely manner, and all output is delivered in accordance with business requirements. Support restart and reprocessing needs.',
    'tImplementationGuide' => '',
  ),
  303 => 
  array (
    'fkContext' => '2104',
    'fkSectionBestPractice' => '1682',
    'nControlType' => '0',
    'sName' => 'DS11.2 Storage and Retention Arrangements',
    'tDescription' => 'Define and implement procedures for effective and efficient data storage, retention and archiving to meet business objectives, the organisation\'\'s security policy and regulatory requirements.',
    'tImplementationGuide' => '',
  ),
  304 => 
  array (
    'fkContext' => '2105',
    'fkSectionBestPractice' => '1682',
    'nControlType' => '0',
    'sName' => 'DS11.3 Media Library Management System',
    'tDescription' => 'Define and implement procedures to maintain na inventory of stored and archived media to ensure their usability and integrity.',
    'tImplementationGuide' => '',
  ),
  305 => 
  array (
    'fkContext' => '2106',
    'fkSectionBestPractice' => '1682',
    'nControlType' => '0',
    'sName' => 'DS11.4 Disposal',
    'tDescription' => 'Define and implement procedures to ensure that business requirements for protection of sensitive data and software are met when data and hardware are disposed or transferred.',
    'tImplementationGuide' => '',
  ),
  306 => 
  array (
    'fkContext' => '2107',
    'fkSectionBestPractice' => '1682',
    'nControlType' => '0',
    'sName' => 'DS11.5 Backup and Restoration',
    'tDescription' => 'Define and implement procedures for backup and restoration of systems, applications, data and documentation in line with business requirements and the continuity plan.',
    'tImplementationGuide' => '',
  ),
  307 => 
  array (
    'fkContext' => '2108',
    'fkSectionBestPractice' => '1682',
    'nControlType' => '0',
    'sName' => 'DS11.6 Security Requirements for Data Management',
    'tDescription' => 'Define and implement policies and procedures to identify and apply security requirements applicable to the receipt, processing, storage and output of data to meet business objectives, the organisation\'\'s security policy and regulatory requirements.',
    'tImplementationGuide' => '',
  ),
  308 => 
  array (
    'fkContext' => '2109',
    'fkSectionBestPractice' => '1683',
    'nControlType' => '0',
    'sName' => 'DS12.1 Site Selection and Layout',
    'tDescription' => 'Define and select the physical sites for IT equipment to support the technology strategy linked to the business strategy. The selection and design of the layout of a site should take into account the risk associated with natural and man-made disasters, whilst considering relevant laws and regulations, such as occupational health and safety regulations.',
    'tImplementationGuide' => '',
  ),
  309 => 
  array (
    'fkContext' => '2110',
    'fkSectionBestPractice' => '1683',
    'nControlType' => '0',
    'sName' => 'DS12.2 Physical Security Measures',
    'tDescription' => 'Define and implement physical security measures in line with business requirements to secure the location and the physical assets. Physical security measures must be capable of effectively preventing, detecting and mitigating risks relating to theft, temperature, fire, smoke, water, vibration, terror, vandalism, power outages, chemicals or explosives.',
    'tImplementationGuide' => '',
  ),
  310 => 
  array (
    'fkContext' => '2111',
    'fkSectionBestPractice' => '1683',
    'nControlType' => '0',
    'sName' => 'DS12.3 Physical Access',
    'tDescription' => 'Define and implement procedures to grant, limit and revoke access to premises, buildings and areas according to business needs, including emergencies. Access to premises, buildings and areas should be justified, authorised, logged and monitored. This should apply to all persons entering the premises, including staff, temporary staff, clients, vendors, visitors or any other third party.',
    'tImplementationGuide' => '',
  ),
  311 => 
  array (
    'fkContext' => '2112',
    'fkSectionBestPractice' => '1683',
    'nControlType' => '0',
    'sName' => 'DS12.4 Protection Against Environmental Factors',
    'tDescription' => 'Design and implement measures for protection against environmental factors. Install specialised equipment and devices to monitor and control the environment.',
    'tImplementationGuide' => '',
  ),
  312 => 
  array (
    'fkContext' => '2113',
    'fkSectionBestPractice' => '1683',
    'nControlType' => '0',
    'sName' => 'DS12.5 Physical Facilities Management',
    'tDescription' => 'Manage facilities, including power and communications equipment, in line with laws and regulations, technical and business requirements, vendor specifications, and health and safety guidelines.',
    'tImplementationGuide' => '',
  ),
  313 => 
  array (
    'fkContext' => '2114',
    'fkSectionBestPractice' => '1684',
    'nControlType' => '0',
    'sName' => 'DS13.1 Operations Procedures and Instructions',
    'tDescription' => 'Define, implement and maintain procedures for IT operations, ensuring that the operations staff members are familiar with all operations tasks relevant to them. Operational procedures should cover shift handover (formal handover of activity, status updates, operational problems, escalation procedures and reports on current responsibilities) to support agreed-upon service levels and ensure continuous operations.',
    'tImplementationGuide' => '',
  ),
  314 => 
  array (
    'fkContext' => '2115',
    'fkSectionBestPractice' => '1684',
    'nControlType' => '0',
    'sName' => 'DS13.2 Job Scheduling',
    'tDescription' => 'Organise the scheduling of jobs, processes and tasks into the most efficient sequence, maximising throughput and utilisation to meet business requirements.',
    'tImplementationGuide' => '',
  ),
  315 => 
  array (
    'fkContext' => '2116',
    'fkSectionBestPractice' => '1684',
    'nControlType' => '0',
    'sName' => 'DS13.3 IT Infrastructure Monitoring',
    'tDescription' => 'Define and implement procedures to monitor the IT infrastructure and related events. Ensure that sufficient chronological information is being stored in operations logs to enable the reconstruction, review and examination of the time sequences of operations and the other activities surrounding or supporting operations.',
    'tImplementationGuide' => '',
  ),
  316 => 
  array (
    'fkContext' => '2117',
    'fkSectionBestPractice' => '1684',
    'nControlType' => '0',
    'sName' => 'DS13.4 Sensitive Documents and Output Devices',
    'tDescription' => 'Establish appropriate physical safeguards, accounting practices and inventory management over sensitive IT assets, such as special forms, negotiable instruments, special purpose printers or security tokens.',
    'tImplementationGuide' => '',
  ),
  317 => 
  array (
    'fkContext' => '2118',
    'fkSectionBestPractice' => '1684',
    'nControlType' => '0',
    'sName' => 'DS13.5 Preventive Maintenance for Hardware',
    'tDescription' => 'Define and implement procedures to ensure timely maintenance of infrastructure to reduce the frequency and impact of failures or performance degradation.',
    'tImplementationGuide' => '',
  ),
  318 => 
  array (
    'fkContext' => '2119',
    'fkSectionBestPractice' => '1686',
    'nControlType' => '0',
    'sName' => 'ME1.1 Monitoring Approach',
    'tDescription' => 'Establish a general monitoring framework and approach to define the scope, methodology and process to be followed for measuring IT\'\'s solution and service delivery, and monitor IT\'\'s contribution to the business. Integrate the framework with the corporate performance management system.',
    'tImplementationGuide' => '',
  ),
  319 => 
  array (
    'fkContext' => '2120',
    'fkSectionBestPractice' => '1686',
    'nControlType' => '0',
    'sName' => 'ME1.2 Definition and Collection of Monitoring Data',
    'tDescription' => 'Work with the business to define a balanced set of performance targets and have them approved by the business and other relevant stakeholders. Define benchmarks with which to compare the targets, and identify available data to be collected to measure the targets. Establish processes to collect timely and accurate data to report on progress against targets.',
    'tImplementationGuide' => '',
  ),
  320 => 
  array (
    'fkContext' => '2121',
    'fkSectionBestPractice' => '1686',
    'nControlType' => '0',
    'sName' => 'ME1.3 Monitoring Method',
    'tDescription' => 'Deploy a performance monitoring method (e.g., balanced scorecard) that records targets; captures measurements; provides a succinct, all-around view of IT performance; and fits within the enterprise monitoring system.',
    'tImplementationGuide' => '',
  ),
  321 => 
  array (
    'fkContext' => '2122',
    'fkSectionBestPractice' => '1686',
    'nControlType' => '0',
    'sName' => 'ME1.4 Performance Assessment',
    'tDescription' => 'Periodically review performance against targets, analyse the cause of any deviations, and initiate remedial action to address the underlying causes. At appropriate times, perform root cause analysis across deviations.',
    'tImplementationGuide' => '',
  ),
  322 => 
  array (
    'fkContext' => '2123',
    'fkSectionBestPractice' => '1686',
    'nControlType' => '0',
    'sName' => 'ME1.5 Board and Executive Reporting',
    'tDescription' => 'Develop senior management reports on IT\'\'s contribution to the business, specifically in terms of the performance of the enterprise\'\'s portfolio, IT-enabled investment programmes, and the solution and service deliverable performance of individual programmes. Include in status reports the extent to which planned objectives have been achieved, budgeted resources used,  set performance targets met and identified risks mitigated. Anticipate senior management\'\'s review by suggesting remedial actions for major deviations. Provide the report to senior management, and solicit feedback from management\'\'s review.',
    'tImplementationGuide' => '',
  ),
  323 => 
  array (
    'fkContext' => '2124',
    'fkSectionBestPractice' => '1686',
    'nControlType' => '0',
    'sName' => 'ME1.6 Remedial Actions',
    'tDescription' => 'Identify and initiate remedial actions based on performance monitoring, assessment and reporting. This includes follow-up of all monitoring, reporting and assessments through: - Review, negotiation and establishment of management responses - Assignment of responsibility for remediation - Tracking of the results of actions committed',
    'tImplementationGuide' => '',
  ),
  324 => 
  array (
    'fkContext' => '2125',
    'fkSectionBestPractice' => '1687',
    'nControlType' => '0',
    'sName' => 'ME2.1 Monitoring of Internal Control Framework',
    'tDescription' => 'Continuously monitor, benchmark and improve the IT control environment and control framework to meet organisational objectives.',
    'tImplementationGuide' => '',
  ),
  325 => 
  array (
    'fkContext' => '2126',
    'fkSectionBestPractice' => '1687',
    'nControlType' => '0',
    'sName' => 'ME2.2 Supervisory Review',
    'tDescription' => 'Monitor and evaluate the efficiency and effectiveness of internal IT managerial review controls.',
    'tImplementationGuide' => '',
  ),
  326 => 
  array (
    'fkContext' => '2127',
    'fkSectionBestPractice' => '1687',
    'nControlType' => '0',
    'sName' => 'ME2.3 Control Exceptions',
    'tDescription' => 'Identify control exceptions, and analyse and identify their underlying root causes. Escalate control exceptions and report to stakeholders appropriately. Institute necessary corrective action.',
    'tImplementationGuide' => '',
  ),
  327 => 
  array (
    'fkContext' => '2128',
    'fkSectionBestPractice' => '1687',
    'nControlType' => '0',
    'sName' => 'ME2.4 Control Self-assessment',
    'tDescription' => 'Evaluate the completeness and effectiveness of management\'\'s control over IT processes, policies and contracts through a continuing programme of self-assessment.',
    'tImplementationGuide' => '',
  ),
  328 => 
  array (
    'fkContext' => '2129',
    'fkSectionBestPractice' => '1687',
    'nControlType' => '0',
    'sName' => 'ME2.5 Assurance of Internal Control',
    'tDescription' => 'Obtain, as needed, further assurance of the completeness and effectiveness of internal controls through third-party reviews.',
    'tImplementationGuide' => '',
  ),
  329 => 
  array (
    'fkContext' => '2130',
    'fkSectionBestPractice' => '1687',
    'nControlType' => '0',
    'sName' => 'ME2.6 Internal Control at Third Parties',
    'tDescription' => 'Assess the status of external service providers\'\' internal controls. Confirm that external service providers comply with legal and regulatory requirements and contractual obligations.',
    'tImplementationGuide' => '',
  ),
  330 => 
  array (
    'fkContext' => '2131',
    'fkSectionBestPractice' => '1687',
    'nControlType' => '0',
    'sName' => 'ME2.7 Remedial Actions',
    'tDescription' => 'Identify, initiate, track and implement remedial actions arising from control assessments and reporting.',
    'tImplementationGuide' => '',
  ),
  331 => 
  array (
    'fkContext' => '2132',
    'fkSectionBestPractice' => '1688',
    'nControlType' => '0',
    'sName' => 'ME3.1 Identification of External Legal, Regulatory and Contractual Compliance Requirements',
    'tDescription' => 'Identify, on a continuous basis, local and international laws, regulations, and other external requirements that must be complied with for incorporation into the organisation\'\'s IT policies, standards, procedures and methodologies.',
    'tImplementationGuide' => '',
  ),
  332 => 
  array (
    'fkContext' => '2133',
    'fkSectionBestPractice' => '1688',
    'nControlType' => '0',
    'sName' => 'ME3.2 Optimisation of Response to External Requirements',
    'tDescription' => 'Review and adjust IT policies, standards, procedures and methodologies to ensure that legal, regulatory and contractual requirements are addressed and communicated.',
    'tImplementationGuide' => '',
  ),
  333 => 
  array (
    'fkContext' => '2134',
    'fkSectionBestPractice' => '1688',
    'nControlType' => '0',
    'sName' => 'ME3.3 Evaluation of Compliance With External Requirements',
    'tDescription' => 'Confirm compliance of IT policies, standards, procedures and methodologies with legal and regulatory requirements.',
    'tImplementationGuide' => '',
  ),
  334 => 
  array (
    'fkContext' => '2135',
    'fkSectionBestPractice' => '1688',
    'nControlType' => '0',
    'sName' => 'ME3.4 Positive Assurance of Compliance',
    'tDescription' => 'Obtain and report assurance of compliance and adherence to all internal policies derived from internal directives or external legal, regulatory or contractual requirements, confirming that any corrective actions to address any compliance gaps have been taken by the responsible process owner in a timely manner.',
    'tImplementationGuide' => '',
  ),
  335 => 
  array (
    'fkContext' => '2136',
    'fkSectionBestPractice' => '1688',
    'nControlType' => '0',
    'sName' => 'ME3.5 Integrated Reporting',
    'tDescription' => 'Integrate IT reporting on legal, regulatory and contractual requirements with similar output from other business functions.',
    'tImplementationGuide' => '',
  ),
  336 => 
  array (
    'fkContext' => '2137',
    'fkSectionBestPractice' => '1689',
    'nControlType' => '0',
    'sName' => 'ME4.1 Establishment of an IT Governance Framework',
    'tDescription' => 'Define, establish and align the IT governance framework with the overall enterprise governance and control environment. Base the framework on a suitable IT process and control model and provide for unambiguous accountability and practices to avoid a breakdown in internal control and oversight. Confirm that the IT governance framework ensures compliance with laws and regulations and is aligned with, and confirms delivery of, the enterprise\'\'s strategies and objectives. Report IT governance status and issues.',
    'tImplementationGuide' => '',
  ),
  337 => 
  array (
    'fkContext' => '2138',
    'fkSectionBestPractice' => '1689',
    'nControlType' => '0',
    'sName' => 'ME4.2 Strategic Alignment',
    'tDescription' => 'Enable board and executive understanding of strategic IT issues, such as the role of IT, technology insights and capabilities. Ensure that there is a shared understanding between the business and IT regarding the potential contribution of IT to the business strategy. Work with the board and the established governance bodies, such as an IT strategy committee, to provide strategic direction to management relative to IT, ensuring that the strategy and objectives are cascaded into business units and IT functions, and that confidence and trust are developed between the business and IT. Enable the alignment of IT to the business in strategy and operations, encouraging co-responsibility between the business and IT for making strategic decisions and obtaining benefits from IT-enabled investments.',
    'tImplementationGuide' => '',
  ),
  338 => 
  array (
    'fkContext' => '2139',
    'fkSectionBestPractice' => '1689',
    'nControlType' => '0',
    'sName' => 'ME4.3 Value Delivery',
    'tDescription' => 'Manage IT-enabled investment programmes and other IT assets and services to ensure that they deliver the greatest possible value in supporting the enterprise\'\'s strategy and objectives. Ensure that the expected business outcomes of IT-enabled investments and the full scope of effort required to achieve those outcomes are understood; that comprehensive and consistent business cases are created and approved by stakeholders; that assets and investments are managed throughout their economic life cycle; and that there is active management of the realisation of benefits, such as contribution to new services, efficiency gains and improved responsiveness to customer demands. Enforce a disciplined approach to portfolio, programme and project management, insisting that the business takes ownership of all IT-enabled investments and IT ensures optimisation of the costs of delivering IT capabilities and services.',
    'tImplementationGuide' => '',
  ),
  339 => 
  array (
    'fkContext' => '2140',
    'fkSectionBestPractice' => '1689',
    'nControlType' => '0',
    'sName' => 'ME4.4 Resource Management',
    'tDescription' => 'Oversee the investment, use and allocation of IT resources through regular assessments of IT initiatives and operations to ensure appropriate resourcing and alignment with current and future strategic objectives and business imperatives.',
    'tImplementationGuide' => '',
  ),
  340 => 
  array (
    'fkContext' => '2141',
    'fkSectionBestPractice' => '1689',
    'nControlType' => '0',
    'sName' => 'ME4.5 Risk Management',
    'tDescription' => 'Work with the board to define the enterprise\'\'s appetite for IT risk, and obtain reasonable assurance that IT risk management practices are appropriate to ensure that the actual IT risk does not exceed the board\'\'s risk appetite. Embed risk management responsibilities into the organisation, ensuring that the business and IT regularly assess and report IT-related risks and their impact and that the enterprise\'\'s IT risk position is transparent to all stakeholders.',
    'tImplementationGuide' => '',
  ),
  341 => 
  array (
    'fkContext' => '2142',
    'fkSectionBestPractice' => '1689',
    'nControlType' => '0',
    'sName' => 'ME4.6 Performance Measurement',
    'tDescription' => 'Confirm that agreed-upon IT objectives have been met or exceeded, or that progress toward IT goals meets expectations. Where agreed-upon objectives have been missed or progress is not as expected, review management\'\'s remedial action. Report to the board relevant portfolios, programme and IT performance, supported by reports to enable senior management to review the enterprise\'\'s progress toward identified goals.',
    'tImplementationGuide' => '',
  ),
  342 => 
  array (
    'fkContext' => '2143',
    'fkSectionBestPractice' => '1689',
    'nControlType' => '0',
    'sName' => 'ME4.7 Independent Assurance',
    'tDescription' => 'Obtain independent assurance (internal or external) about the conformance of IT with relevant laws and regulations; the organisation\'\'s policies, standards and procedures;generally accepted practices; and the effective and efficient performance of IT.',
    'tImplementationGuide' => '',
  ),
  343 => 
  array (
    'fkContext' => '2145',
    'fkSectionBestPractice' => '1692',
    'nControlType' => '0',
    'sName' => '1.1 - PCI',
    'tDescription' => '1.1 Establish firewall configuration standards that include the following:',
    'tImplementationGuide' => '',
  ),
  344 => 
  array (
    'fkContext' => '2146',
    'fkSectionBestPractice' => '1692',
    'nControlType' => '0',
    'sName' => '1.1.1 - PCI',
    'tDescription' => '1.1.1 A formal process for approving and testing all external network connections and changes to the firewall configuration.',
    'tImplementationGuide' => '',
  ),
  345 => 
  array (
    'fkContext' => '2147',
    'fkSectionBestPractice' => '1692',
    'nControlType' => '0',
    'sName' => '1.1.2 - PCI',
    'tDescription' => '1.1.2 A current network diagram with all connections to cardholder data, including any wireless networks',
    'tImplementationGuide' => '',
  ),
  346 => 
  array (
    'fkContext' => '2148',
    'fkSectionBestPractice' => '1692',
    'nControlType' => '0',
    'sName' => '1.1.3 - PCI',
    'tDescription' => '1.1.3 Requirements for a firewall at each Internet connection and between any demilitarized zone (DMZ) and the internal network zone',
    'tImplementationGuide' => '',
  ),
  347 => 
  array (
    'fkContext' => '2149',
    'fkSectionBestPractice' => '1692',
    'nControlType' => '0',
    'sName' => '1.1.4 - PCI',
    'tDescription' => '1.1.4 Description of groups, roles, and responsibilities for logical management of network components',
    'tImplementationGuide' => '',
  ),
  348 => 
  array (
    'fkContext' => '2150',
    'fkSectionBestPractice' => '1692',
    'nControlType' => '0',
    'sName' => '1.1.5 - PCI',
    'tDescription' => '1.1.5 Documented list of services and ports necessary for business',
    'tImplementationGuide' => '',
  ),
  349 => 
  array (
    'fkContext' => '2151',
    'fkSectionBestPractice' => '1692',
    'nControlType' => '0',
    'sName' => '1.1.6 - PCI',
    'tDescription' => '1.1.6 Justification and documentation for any available protocols besides hypertext transfer protocol (HTTP), and secure sockets layer (SSL), secure shell (SSH), and virtual private network (VPN)',
    'tImplementationGuide' => '',
  ),
  350 => 
  array (
    'fkContext' => '2152',
    'fkSectionBestPractice' => '1692',
    'nControlType' => '0',
    'sName' => '1.1.7 - PCI',
    'tDescription' => '1.1.7 Justification and documentation for any risky protocols allowed (for example, file transfer protocol (FTP)), which includes reason for use of protocol and security features implemented',
    'tImplementationGuide' => '',
  ),
  351 => 
  array (
    'fkContext' => '2153',
    'fkSectionBestPractice' => '1692',
    'nControlType' => '0',
    'sName' => '1.1.8 - PCI',
    'tDescription' => '1.1.8 Quarterly review of firewall and router rule sets',
    'tImplementationGuide' => '',
  ),
  352 => 
  array (
    'fkContext' => '2154',
    'fkSectionBestPractice' => '1692',
    'nControlType' => '0',
    'sName' => '1.1.9 - PCI',
    'tDescription' => '1.1.9 Configuration of standards for routers.',
    'tImplementationGuide' => '',
  ),
  353 => 
  array (
    'fkContext' => '2155',
    'fkSectionBestPractice' => '1692',
    'nControlType' => '0',
    'sName' => '1.2 - PCI',
    'tDescription' => '1.2 Build a firewall configuration that denies all traffic from \'\'untrusted\'\' networks and hosts, except for protocols necessary for the cardholder data environment.',
    'tImplementationGuide' => '',
  ),
  354 => 
  array (
    'fkContext' => '2156',
    'fkSectionBestPractice' => '1692',
    'nControlType' => '0',
    'sName' => '1.3 - PCI',
    'tDescription' => '1.3 Build a firewall configuration that restricts connections between publicly accessible servers and any system component storing cardholder data, including any connections from wireless networks. This firewall configuration should include the following:',
    'tImplementationGuide' => '',
  ),
  355 => 
  array (
    'fkContext' => '2157',
    'fkSectionBestPractice' => '1692',
    'nControlType' => '0',
    'sName' => '1.3.1 - PCI',
    'tDescription' => '1.3.1 Restricting inbound Internet traffic to Internet protocol (IP) addresses within the DMZ (ingress filters)',
    'tImplementationGuide' => '',
  ),
  356 => 
  array (
    'fkContext' => '2158',
    'fkSectionBestPractice' => '1692',
    'nControlType' => '0',
    'sName' => '1.3.2 - PCI',
    'tDescription' => '1.3.2 Not allowing internal addresses to pass from the Internet into the DMZ',
    'tImplementationGuide' => '',
  ),
  357 => 
  array (
    'fkContext' => '2159',
    'fkSectionBestPractice' => '1692',
    'nControlType' => '0',
    'sName' => '1.3.3 - PCI',
    'tDescription' => '1.3.3 Implementing stateful inspection, also known as dynamic packet filtering (that is, only "established" connections are allowed into the network)',
    'tImplementationGuide' => '',
  ),
  358 => 
  array (
    'fkContext' => '2160',
    'fkSectionBestPractice' => '1692',
    'nControlType' => '0',
    'sName' => '1.3.4 - PCI',
    'tDescription' => '1.3.4 Placing the database in an internal network zone, segregated from the DMZ',
    'tImplementationGuide' => '',
  ),
  359 => 
  array (
    'fkContext' => '2161',
    'fkSectionBestPractice' => '1692',
    'nControlType' => '0',
    'sName' => '1.3.5 - PCI',
    'tDescription' => '1.3.5 Restricting inbound and outbound traffic to that which is necessary for the cardholder data environment',
    'tImplementationGuide' => '',
  ),
  360 => 
  array (
    'fkContext' => '2162',
    'fkSectionBestPractice' => '1692',
    'nControlType' => '0',
    'sName' => '1.3.6 - PCI',
    'tDescription' => '1.3.6 Securing and synchronizing router configuration files. For example, running configuration files (for normal functioning of the routers), and start-up configuration files (when machines are re-booted) should have the same secure configuration',
    'tImplementationGuide' => '',
  ),
  361 => 
  array (
    'fkContext' => '2163',
    'fkSectionBestPractice' => '1692',
    'nControlType' => '0',
    'sName' => '1.3.7 - PCI',
    'tDescription' => '1.3.7 Denying all other inbound and outbound traffic not specifically allowed',
    'tImplementationGuide' => '',
  ),
  362 => 
  array (
    'fkContext' => '2164',
    'fkSectionBestPractice' => '1692',
    'nControlType' => '0',
    'sName' => '1.3.8 - PCI',
    'tDescription' => '1.3.8 Installing perimeter firewalls between any wireless networks and the cardholder data environment, and configuring these firewalls to deny any traffic from the wireless environment or from controlling any traffic (if such traffic is necessary for business purposes)',
    'tImplementationGuide' => '',
  ),
  363 => 
  array (
    'fkContext' => '2165',
    'fkSectionBestPractice' => '1692',
    'nControlType' => '0',
    'sName' => '1.3.9 - PCI',
    'tDescription' => '1.3.9 Installing personal firewall software on any mobile and employee-owned computers with direct connectivity to the Internet (for example, laptops used by employees), which are used to access the organization\'\'s network.',
    'tImplementationGuide' => '',
  ),
  364 => 
  array (
    'fkContext' => '2166',
    'fkSectionBestPractice' => '1692',
    'nControlType' => '0',
    'sName' => '1.4 - PCI',
    'tDescription' => '1.4 Prohibit direct public access between external networks and any system component that stores cardholder data (for example, databases, logs, trace files)',
    'tImplementationGuide' => '',
  ),
  365 => 
  array (
    'fkContext' => '2167',
    'fkSectionBestPractice' => '1692',
    'nControlType' => '0',
    'sName' => '1.4.1 - PCI',
    'tDescription' => '1.4.1 Implement a DMZ to filter and screen all traffic and to prohibit direct routes for inbound and outbound Internet traffic',
    'tImplementationGuide' => '',
  ),
  366 => 
  array (
    'fkContext' => '2168',
    'fkSectionBestPractice' => '1692',
    'nControlType' => '0',
    'sName' => '1.4.2 - PCI',
    'tDescription' => '1.4.2 Restrict outbound traffic from payment card applications to IP addresses within the DMZ.',
    'tImplementationGuide' => '',
  ),
  367 => 
  array (
    'fkContext' => '2169',
    'fkSectionBestPractice' => '1692',
    'nControlType' => '0',
    'sName' => '1.5 - PCI',
    'tDescription' => '1.5 Implement IP masquerading to prevent internal addresses from being translated and revealed on the Internet. Use technologies that implement RFC 1918 address space, such as port address translation (PAT) or network address translation (NAT).',
    'tImplementationGuide' => '',
  ),
  368 => 
  array (
    'fkContext' => '2170',
    'fkSectionBestPractice' => '1693',
    'nControlType' => '0',
    'sName' => '2.1 - PCI',
    'tDescription' => '2.1 Always change vendor-supplied defaults before installing a system on the network (for example, include passwords, simple network management protocol (SNMP) community strings, and elimination of unnecessary accounts).',
    'tImplementationGuide' => '',
  ),
  369 => 
  array (
    'fkContext' => '2171',
    'fkSectionBestPractice' => '1693',
    'nControlType' => '0',
    'sName' => '2.1.1 - PCI',
    'tDescription' => '2.1.1 For wireless environments, change wireless vendor defaults, including but not limited to, wired equivalent privacy (WEP) keys, default service set identifier (SSID), passwords, and SNMP community strings. Disable SSID broadcasts. Enable WiFi protected access (WPA and WPA2) technology for encryption and authentication when WPA-capable.',
    'tImplementationGuide' => '',
  ),
  370 => 
  array (
    'fkContext' => '2172',
    'fkSectionBestPractice' => '1693',
    'nControlType' => '0',
    'sName' => '2.2 - PCI',
    'tDescription' => '2.2 Develop configuration standards for all system components. Assure that these standards address all known security vulnerabilities and are consistent with industry-accepted system hardening standards as defined, for example, by SysAdmin Audit Network Security (SANS), National Institute of Standards Technology (NIST), and Center for Internet Security (CIS).',
    'tImplementationGuide' => '',
  ),
  371 => 
  array (
    'fkContext' => '2173',
    'fkSectionBestPractice' => '1693',
    'nControlType' => '0',
    'sName' => '2.2.1 - PCI',
    'tDescription' => '2.2.1 Implement only one primary function per server (for example, web servers, database servers, and DNS should be implemented on separate servers)',
    'tImplementationGuide' => '',
  ),
  372 => 
  array (
    'fkContext' => '2174',
    'fkSectionBestPractice' => '1693',
    'nControlType' => '0',
    'sName' => '2.2.2 - PCI',
    'tDescription' => '2.2.2 Disable all unnecessary and insecure services and protocols (services and protocols not directly needed to perform the devices\'\' specified function)',
    'tImplementationGuide' => '',
  ),
  373 => 
  array (
    'fkContext' => '2175',
    'fkSectionBestPractice' => '1693',
    'nControlType' => '0',
    'sName' => '2.2.3 - PCI',
    'tDescription' => '2.2.3 Configure system security parameters to prevent misuse',
    'tImplementationGuide' => '',
  ),
  374 => 
  array (
    'fkContext' => '2176',
    'fkSectionBestPractice' => '1693',
    'nControlType' => '0',
    'sName' => '2.2.4 - PCI',
    'tDescription' => '2.2.4 Remove all unnecessary functionality, such as scripts, drivers, features, subsystems, file systems, and unnecessary web servers.',
    'tImplementationGuide' => '',
  ),
  375 => 
  array (
    'fkContext' => '2177',
    'fkSectionBestPractice' => '1693',
    'nControlType' => '0',
    'sName' => '2.3 - PCI',
    'tDescription' => '2.3 Encrypt all non-console administrative access. Use technologies such as SSH, VPN, or SSL/TLS (transport layer security) for web-based management and other non-console administrative access.',
    'tImplementationGuide' => '',
  ),
  376 => 
  array (
    'fkContext' => '2178',
    'fkSectionBestPractice' => '1693',
    'nControlType' => '0',
    'sName' => '2.4 - PCI',
    'tDescription' => '2.4 Hosting providers must protect each entity\'\'s hosted environment and data. These providers must meet specific requirements as detailed in Appendix A: "PCI DSS Applicability for Hosting Providers."',
    'tImplementationGuide' => '',
  ),
  377 => 
  array (
    'fkContext' => '2179',
    'fkSectionBestPractice' => '1695',
    'nControlType' => '0',
    'sName' => '3.1 - PCI',
    'tDescription' => '3.1 Keep cardholder data storage to a minimum. Develop a data retention and disposal policy. Limit storage amount and retention time to that which is required for business, legal, and/or regulatory purposes, as documented in the data retention policy.',
    'tImplementationGuide' => '',
  ),
  378 => 
  array (
    'fkContext' => '2180',
    'fkSectionBestPractice' => '1695',
    'nControlType' => '0',
    'sName' => '3.2 - PCI',
    'tDescription' => '3.2 Do not store sensitive authentication data subsequent to authorization (even if encrypted). Sensitive authentication data includes the data as cited in the following Requirements 3.2.1 through 3.2.3:',
    'tImplementationGuide' => '',
  ),
  379 => 
  array (
    'fkContext' => '2181',
    'fkSectionBestPractice' => '1695',
    'nControlType' => '0',
    'sName' => '3.2.1 - PCI',
    'tDescription' => '3.2.1 Do not store the full contents of any track from the magnetic stripe (that is on the back of a card, in a chip or elsewhere). This data is alternatively called full track, track, track 1, track 2, and magnetic stripe data. In the normal course of business, the following data elements from the magnetic stripe may need to be retained: the accountholder\'\'s name, primary account number (PAN), expiration date, and service code. To minimize risk, store only those data elements needed for business. NEVER store the card verification code or value or PIN verification value data elements. Note: See \'\'Glossary\'\' for additional information.',
    'tImplementationGuide' => '',
  ),
  380 => 
  array (
    'fkContext' => '2182',
    'fkSectionBestPractice' => '1695',
    'nControlType' => '0',
    'sName' => '3.2.2 - PCI',
    'tDescription' => '3.2.2 Do not store the card-validation code or value (three-digit or four-digit number printed on the front or back of a payment card) used to verify card-not-present transactions Note: See "Glossary" for additional information.',
    'tImplementationGuide' => '',
  ),
  381 => 
  array (
    'fkContext' => '2183',
    'fkSectionBestPractice' => '1695',
    'nControlType' => '0',
    'sName' => '3.2.3 - PCI',
    'tDescription' => '3.2.3 Do not store the personal identification number (PIN) or the encrypted PIN block.',
    'tImplementationGuide' => '',
  ),
  382 => 
  array (
    'fkContext' => '2184',
    'fkSectionBestPractice' => '1695',
    'nControlType' => '0',
    'sName' => '3.3 - PCI',
    'tDescription' => '3.3 Mask PAN when displayed (the first six and last four digits are the maximum number of digits to be displayed). Note: This requirement does not apply to employees and other parties with a specific need to see the full PAN;
nor does the requirement supersede stricter requirements in place for displays of cardholder data (for example, for point of sale [POS] receipts).',
    'tImplementationGuide' => '',
  ),
  383 => 
  array (
    'fkContext' => '2185',
    'fkSectionBestPractice' => '1695',
    'nControlType' => '0',
    'sName' => '3.4 - PCI',
    'tDescription' => '3.4 Render PAN, at minimum, unreadable anywhere it is stored (including data on portable digital media, backup media, in logs, and data received from or stored by wireless networks) by using any of the following approaches: * Strong one-way hash functions (hashed indexes) * Truncation * Index tokens and pads (pads must be securely stored) * Strong cryptography with associated key management processes and procedures. The MINIMUM account information that must be rendered unreadable is the PAN. If for some reason, a company is unable to encrypt cardholder data, refer to Appendix B: "Compensating Controls for Encryption of Stored Data."',
    'tImplementationGuide' => '',
  ),
  384 => 
  array (
    'fkContext' => '2186',
    'fkSectionBestPractice' => '1695',
    'nControlType' => '0',
    'sName' => '3.4.1 - PCI',
    'tDescription' => '3.4.1 If disk encryption is used (rather than file- or column-level database encryption), logical access must be managed independently of native operating system access control mechanisms (for example, by not using local system or Active Directory accounts). Decryption keys must not be tied to user accounts.',
    'tImplementationGuide' => '',
  ),
  385 => 
  array (
    'fkContext' => '2187',
    'fkSectionBestPractice' => '1695',
    'nControlType' => '0',
    'sName' => '3.5 - PCI',
    'tDescription' => '3.5 Protect encryption keys used for encryption of cardholder data against both disclosure and misuse.',
    'tImplementationGuide' => '',
  ),
  386 => 
  array (
    'fkContext' => '2188',
    'fkSectionBestPractice' => '1695',
    'nControlType' => '0',
    'sName' => '3.5.1 - PCI',
    'tDescription' => '3.5.1 Restrict access to keys to the fewest number of custodians necessary',
    'tImplementationGuide' => '',
  ),
  387 => 
  array (
    'fkContext' => '2189',
    'fkSectionBestPractice' => '1695',
    'nControlType' => '0',
    'sName' => '3.5.2 - PCI',
    'tDescription' => '3.5.2 Store keys securely in the fewest possible locations and forms.',
    'tImplementationGuide' => '',
  ),
  388 => 
  array (
    'fkContext' => '2190',
    'fkSectionBestPractice' => '1695',
    'nControlType' => '0',
    'sName' => '3.6 - PCI',
    'tDescription' => '3.6 Fully document and implement all key management processes and procedures for keys used for encryption of cardholder data, including the following:',
    'tImplementationGuide' => '',
  ),
  389 => 
  array (
    'fkContext' => '2191',
    'fkSectionBestPractice' => '1695',
    'nControlType' => '0',
    'sName' => '3.6.1 - PCI',
    'tDescription' => '3.6.1 Generation of strong keys',
    'tImplementationGuide' => '',
  ),
  390 => 
  array (
    'fkContext' => '2192',
    'fkSectionBestPractice' => '1695',
    'nControlType' => '0',
    'sName' => '3.6.2 - PCI',
    'tDescription' => '3.6.2 Secure key distribution',
    'tImplementationGuide' => '',
  ),
  391 => 
  array (
    'fkContext' => '2193',
    'fkSectionBestPractice' => '1695',
    'nControlType' => '0',
    'sName' => '3.6.3 - PCI',
    'tDescription' => '3.6.3 Secure key storage',
    'tImplementationGuide' => '',
  ),
  392 => 
  array (
    'fkContext' => '2194',
    'fkSectionBestPractice' => '1695',
    'nControlType' => '0',
    'sName' => '3.6.4 - PCI',
    'tDescription' => '3.6.4 Periodic changing of keys * As deemed necessary and recommended by the associated application (for example, re-keying);
preferably automatically * At least annually.',
    'tImplementationGuide' => '',
  ),
  393 => 
  array (
    'fkContext' => '2195',
    'fkSectionBestPractice' => '1695',
    'nControlType' => '0',
    'sName' => '3.6.5 - PCI',
    'tDescription' => '3.6.5 Destruction of old keys',
    'tImplementationGuide' => '',
  ),
  394 => 
  array (
    'fkContext' => '2196',
    'fkSectionBestPractice' => '1695',
    'nControlType' => '0',
    'sName' => '3.6.6 - PCI',
    'tDescription' => '3.6.6 Split knowledge and establishment of dual control of keys (so that it requires two or three people, each knowing only their part of the key, to reconstruct the whole key)',
    'tImplementationGuide' => '',
  ),
  395 => 
  array (
    'fkContext' => '2197',
    'fkSectionBestPractice' => '1695',
    'nControlType' => '0',
    'sName' => '3.6.7 - PCI',
    'tDescription' => '3.6.7 Prevention of unauthorized substitution of keys',
    'tImplementationGuide' => '',
  ),
  396 => 
  array (
    'fkContext' => '2198',
    'fkSectionBestPractice' => '1695',
    'nControlType' => '0',
    'sName' => '3.6.8 - PCI',
    'tDescription' => '3.6.8 Replacement of known or suspected compromised keys',
    'tImplementationGuide' => '',
  ),
  397 => 
  array (
    'fkContext' => '2199',
    'fkSectionBestPractice' => '1695',
    'nControlType' => '0',
    'sName' => '3.6.9 - PCI',
    'tDescription' => '3.6.9 Revocation of old or invalid keys',
    'tImplementationGuide' => '',
  ),
  398 => 
  array (
    'fkContext' => '2200',
    'fkSectionBestPractice' => '1695',
    'nControlType' => '0',
    'sName' => '3.6.10 - PCI',
    'tDescription' => '3.6.10 Requirement for key custodians to sign a form stating that they understand and accept their key-custodian responsibilities.',
    'tImplementationGuide' => '',
  ),
  399 => 
  array (
    'fkContext' => '2201',
    'fkSectionBestPractice' => '1696',
    'nControlType' => '0',
    'sName' => '4.1 - PCI',
    'tDescription' => '4.1 Use strong cryptography and security protocols such as secure sockets layer (SSL) / transport layer security (TLS) and Internet protocol security (IPSEC) to safeguard sensitive cardholder data during transmission over open, public networks. Examples of open, public networks that are in scope of the PCI DSS are the Internet, WiFi (IEEE 802.11x), global system for mobile communications (GSM), and general packet radio service (GPRS).',
    'tImplementationGuide' => '',
  ),
  400 => 
  array (
    'fkContext' => '2202',
    'fkSectionBestPractice' => '1696',
    'nControlType' => '0',
    'sName' => '4.1.1 - PCI',
    'tDescription' => '4.1.1 For wireless networks transmitting cardholder data, encrypt the transmissions by using WiFi protected access (WPA or WPA2) technology, IPSEC VPN, or SSL/TLS. Never rely exclusively on wired equivalent privacy (WEP) to protect confidentiality and access to a wireless LAN. If WEP is used, do the following: * Use with a minimum 104-bit encryption key and 24 bit-initialization value * Use ONLY in conjunction with WiFi protected access (WPA or WPA2) technology, VPN, or SSL/TLS * Rotate shared WEP keys quarterly (or automatically if the technology permits) * Rotate shared WEP keys whenever there are changes in personnel with access to keys * Restrict access based on media access code (MAC) address.',
    'tImplementationGuide' => '',
  ),
  401 => 
  array (
    'fkContext' => '2203',
    'fkSectionBestPractice' => '1696',
    'nControlType' => '0',
    'sName' => '4.2 - PCI',
    'tDescription' => '4.2 Never send unencrypted PANs by e-mail.',
    'tImplementationGuide' => '',
  ),
  402 => 
  array (
    'fkContext' => '2204',
    'fkSectionBestPractice' => '1698',
    'nControlType' => '0',
    'sName' => '5.1 - PCI',
    'tDescription' => '5.1 Deploy anti-virus software on all systems commonly affected by viruses (particularly personal computers and servers) Note: Systems commonly affected by viruses typically do not include UNIX-based operating systems or mainframes.',
    'tImplementationGuide' => '',
  ),
  403 => 
  array (
    'fkContext' => '2205',
    'fkSectionBestPractice' => '1698',
    'nControlType' => '0',
    'sName' => '5.1.1 - PCI',
    'tDescription' => '5.1.1 Ensure that anti-virus programs are capable of detecting, removing, and protecting against other forms of malicious software, including spyware and adware.',
    'tImplementationGuide' => '',
  ),
  404 => 
  array (
    'fkContext' => '2206',
    'fkSectionBestPractice' => '1698',
    'nControlType' => '0',
    'sName' => '5.2 - PCI',
    'tDescription' => '5.2 Ensure that all anti-virus mechanisms are current, actively running, and capable of generating audit logs.',
    'tImplementationGuide' => '',
  ),
  405 => 
  array (
    'fkContext' => '2207',
    'fkSectionBestPractice' => '1699',
    'nControlType' => '0',
    'sName' => '6.1 - PCI',
    'tDescription' => '6.1 Ensure that all system components and software have the latest vendor-supplied security patches installed. Install relevant security patches within one month of release.',
    'tImplementationGuide' => '',
  ),
  406 => 
  array (
    'fkContext' => '2208',
    'fkSectionBestPractice' => '1699',
    'nControlType' => '0',
    'sName' => '6.2 - PCI',
    'tDescription' => '6.2 Establish a process to identify newly discovered security vulnerabilities (for example, subscribe to alert services freely available on the Internet). Update standards to address new vulnerability issues.',
    'tImplementationGuide' => '',
  ),
  407 => 
  array (
    'fkContext' => '2209',
    'fkSectionBestPractice' => '1699',
    'nControlType' => '0',
    'sName' => '6.3 - PCI',
    'tDescription' => '6.3 Develop software applications based on industry best practices and incorporate information security throughout the software development life cycle.',
    'tImplementationGuide' => '',
  ),
  408 => 
  array (
    'fkContext' => '2210',
    'fkSectionBestPractice' => '1699',
    'nControlType' => '0',
    'sName' => '6.3.1 - PCI',
    'tDescription' => '6.3.1 Testing of all security patches and system and software configuration changes before deployment',
    'tImplementationGuide' => '',
  ),
  409 => 
  array (
    'fkContext' => '2211',
    'fkSectionBestPractice' => '1699',
    'nControlType' => '0',
    'sName' => '6.3.2 - PCI',
    'tDescription' => '6.3.2 Separate development, test, and production environments',
    'tImplementationGuide' => '',
  ),
  410 => 
  array (
    'fkContext' => '2212',
    'fkSectionBestPractice' => '1699',
    'nControlType' => '0',
    'sName' => '6.3.3 - PCI',
    'tDescription' => '6.3.3 Separation of duties between development, test, and production environments',
    'tImplementationGuide' => '',
  ),
  411 => 
  array (
    'fkContext' => '2213',
    'fkSectionBestPractice' => '1699',
    'nControlType' => '0',
    'sName' => '6.3.4 - PCI',
    'tDescription' => '6.3.4 Production data (live PANs) are not used for testing or development',
    'tImplementationGuide' => '',
  ),
  412 => 
  array (
    'fkContext' => '2214',
    'fkSectionBestPractice' => '1699',
    'nControlType' => '0',
    'sName' => '6.3.5 - PCI',
    'tDescription' => '6.3.5 Removal of test data and accounts before production systems become active',
    'tImplementationGuide' => '',
  ),
  413 => 
  array (
    'fkContext' => '2215',
    'fkSectionBestPractice' => '1699',
    'nControlType' => '0',
    'sName' => '6.3.6 - PCI',
    'tDescription' => '6.3.6 Removal of custom application accounts, usernames, and passwords before applications become active or are released to customers',
    'tImplementationGuide' => '',
  ),
  414 => 
  array (
    'fkContext' => '2216',
    'fkSectionBestPractice' => '1699',
    'nControlType' => '0',
    'sName' => '6.3.7 - PCI',
    'tDescription' => '6.3.7 Review of custom code prior to release to production or customers in order to identify any potential coding vulnerability.',
    'tImplementationGuide' => '',
  ),
  415 => 
  array (
    'fkContext' => '2217',
    'fkSectionBestPractice' => '1699',
    'nControlType' => '0',
    'sName' => '6.4 - PCI',
    'tDescription' => '6.4 Follow change control procedures for all system and software configuration changes. The procedures must include the following:',
    'tImplementationGuide' => '',
  ),
  416 => 
  array (
    'fkContext' => '2218',
    'fkSectionBestPractice' => '1699',
    'nControlType' => '0',
    'sName' => '6.4.1 - PCI',
    'tDescription' => '6.4.1 Documentation of impact',
    'tImplementationGuide' => '',
  ),
  417 => 
  array (
    'fkContext' => '2219',
    'fkSectionBestPractice' => '1699',
    'nControlType' => '0',
    'sName' => '6.4.2 - PCI',
    'tDescription' => '6.4.2 Management sign-off by appropriate parties',
    'tImplementationGuide' => '',
  ),
  418 => 
  array (
    'fkContext' => '2220',
    'fkSectionBestPractice' => '1699',
    'nControlType' => '0',
    'sName' => '6.4.3 - PCI',
    'tDescription' => '6.4.3 Testing of operational functionality',
    'tImplementationGuide' => '',
  ),
  419 => 
  array (
    'fkContext' => '2221',
    'fkSectionBestPractice' => '1699',
    'nControlType' => '0',
    'sName' => '6.4.4 - PCI',
    'tDescription' => '6.4.4 Back-out procedures',
    'tImplementationGuide' => '',
  ),
  420 => 
  array (
    'fkContext' => '2222',
    'fkSectionBestPractice' => '1699',
    'nControlType' => '0',
    'sName' => '6.5 - PCI',
    'tDescription' => '6.5 Develop all web applications based on secure coding guidelines such as the Open Web Application Security Project guidelines. Review custom application code to identify coding vulnerabilities. Cover prevention of common coding vulnerabilities in software development processes, to include the following:',
    'tImplementationGuide' => '',
  ),
  421 => 
  array (
    'fkContext' => '2223',
    'fkSectionBestPractice' => '1699',
    'nControlType' => '0',
    'sName' => '6.5.1 - PCI',
    'tDescription' => '6.5.1 Unvalidated input',
    'tImplementationGuide' => '',
  ),
  422 => 
  array (
    'fkContext' => '2224',
    'fkSectionBestPractice' => '1699',
    'nControlType' => '0',
    'sName' => '6.5.2 - PCI',
    'tDescription' => '6.5.2 Broken access control (for example, malicious use of user IDs)',
    'tImplementationGuide' => '',
  ),
  423 => 
  array (
    'fkContext' => '2225',
    'fkSectionBestPractice' => '1699',
    'nControlType' => '0',
    'sName' => '6.5.3 - PCI',
    'tDescription' => '6.5.3 Broken authentication and session management (use of account credentials and session cookies)',
    'tImplementationGuide' => '',
  ),
  424 => 
  array (
    'fkContext' => '2226',
    'fkSectionBestPractice' => '1699',
    'nControlType' => '0',
    'sName' => '6.5.4 - PCI',
    'tDescription' => '6.5.4 Cross-site scripting (XSS) attacks',
    'tImplementationGuide' => '',
  ),
  425 => 
  array (
    'fkContext' => '2227',
    'fkSectionBestPractice' => '1699',
    'nControlType' => '0',
    'sName' => '6.5.5 - PCI',
    'tDescription' => '6.5.5 Buffer overflows',
    'tImplementationGuide' => '',
  ),
  426 => 
  array (
    'fkContext' => '2228',
    'fkSectionBestPractice' => '1699',
    'nControlType' => '0',
    'sName' => '6.5.6 - PCI',
    'tDescription' => '6.5.6 Injection flaws (for example, structured query language (SQL) injection)',
    'tImplementationGuide' => '',
  ),
  427 => 
  array (
    'fkContext' => '2229',
    'fkSectionBestPractice' => '1699',
    'nControlType' => '0',
    'sName' => '6.5.7 - PCI',
    'tDescription' => '6.5.7 Improper error handling',
    'tImplementationGuide' => '',
  ),
  428 => 
  array (
    'fkContext' => '2230',
    'fkSectionBestPractice' => '1699',
    'nControlType' => '0',
    'sName' => '6.5.8 - PCI',
    'tDescription' => '6.5.8 Insecure storage',
    'tImplementationGuide' => '',
  ),
  429 => 
  array (
    'fkContext' => '2231',
    'fkSectionBestPractice' => '1699',
    'nControlType' => '0',
    'sName' => '6.5.9 - PCI',
    'tDescription' => '6.5.9 Denial of service',
    'tImplementationGuide' => '',
  ),
  430 => 
  array (
    'fkContext' => '2232',
    'fkSectionBestPractice' => '1699',
    'nControlType' => '0',
    'sName' => '6.5.10 - PCI',
    'tDescription' => '6.5.10 Insecure configuration management',
    'tImplementationGuide' => '',
  ),
  431 => 
  array (
    'fkContext' => '2233',
    'fkSectionBestPractice' => '1699',
    'nControlType' => '0',
    'sName' => '6.6 - PCI',
    'tDescription' => '6.6 Ensure that all web-facing applications are protected against known attacks by applying either of the following methods: * Having all custom application code reviewed for common vulnerabilities by an organization that specializes in application security * Installing an application layer firewall in front of web-facing applications. Note: This method is considered a best practice until June 30, 2008, after which it becomes a requirement.',
    'tImplementationGuide' => '',
  ),
  432 => 
  array (
    'fkContext' => '2234',
    'fkSectionBestPractice' => '1701',
    'nControlType' => '0',
    'sName' => '7.1 - PCI',
    'tDescription' => '7.1 Limit access to computing resources and cardholder information only to those individuals whose job requires such access.',
    'tImplementationGuide' => '',
  ),
  433 => 
  array (
    'fkContext' => '2235',
    'fkSectionBestPractice' => '1701',
    'nControlType' => '0',
    'sName' => '7.2 - PCI',
    'tDescription' => '7.2 Establish a mechanism for systems with multiple users that restricts access based on a user\'\'s need to know and is set to \'\'deny all\'\' unless specifically allowed.',
    'tImplementationGuide' => '',
  ),
  434 => 
  array (
    'fkContext' => '2236',
    'fkSectionBestPractice' => '1702',
    'nControlType' => '0',
    'sName' => '8.1 - PCI',
    'tDescription' => '8.1 Identify all users with a unique user name before allowing them to access system components or cardholder data.',
    'tImplementationGuide' => '',
  ),
  435 => 
  array (
    'fkContext' => '2237',
    'fkSectionBestPractice' => '1702',
    'nControlType' => '0',
    'sName' => '8.2 - PCI',
    'tDescription' => '8.2 In addition to assigning a unique ID, employ at least one of the following methods to authenticate all users: * Password * Token devices (e.g., SecureID, certificates, or public key) * Biometrics.',
    'tImplementationGuide' => '',
  ),
  436 => 
  array (
    'fkContext' => '2238',
    'fkSectionBestPractice' => '1702',
    'nControlType' => '0',
    'sName' => '8.3 - PCI',
    'tDescription' => '8.3 Implement two-factor authentication for remote access to the network by employees, administrators, and third parties. Use technologies such as remote authentication and dial-in service (RADIUS) or terminal access controller access control system (TACACS) with tokens;
or VPN (based on SSL/TLS or IPSEC) with individual certificates.',
    'tImplementationGuide' => '',
  ),
  437 => 
  array (
    'fkContext' => '2239',
    'fkSectionBestPractice' => '1702',
    'nControlType' => '0',
    'sName' => '8.4 - PCI',
    'tDescription' => '8.4 Encrypt all passwords during transmission and storage on all system components.',
    'tImplementationGuide' => '',
  ),
  438 => 
  array (
    'fkContext' => '2240',
    'fkSectionBestPractice' => '1702',
    'nControlType' => '0',
    'sName' => '8.5 - PCI',
    'tDescription' => '8.5 Ensure proper user authentication and password management for non-consumer users and administrators on all system components as follows:',
    'tImplementationGuide' => '',
  ),
  439 => 
  array (
    'fkContext' => '2241',
    'fkSectionBestPractice' => '1702',
    'nControlType' => '0',
    'sName' => '8.5.1 - PCI',
    'tDescription' => '8.5.1 Control addition, deletion, and modification of user IDs, credentials, and other identifier objects',
    'tImplementationGuide' => '',
  ),
  440 => 
  array (
    'fkContext' => '2242',
    'fkSectionBestPractice' => '1702',
    'nControlType' => '0',
    'sName' => '8.5.2 - PCI',
    'tDescription' => '8.5.2 Verify user identity before performing password resets',
    'tImplementationGuide' => '',
  ),
  441 => 
  array (
    'fkContext' => '2243',
    'fkSectionBestPractice' => '1702',
    'nControlType' => '0',
    'sName' => '8.5.3 - PCI',
    'tDescription' => '8.5.3 Set first-time passwords to a unique value for each user and change immediately after the first use',
    'tImplementationGuide' => '',
  ),
  442 => 
  array (
    'fkContext' => '2244',
    'fkSectionBestPractice' => '1702',
    'nControlType' => '0',
    'sName' => '8.5.4 - PCI',
    'tDescription' => '8.5.4 Immediately revoke access for any terminated users',
    'tImplementationGuide' => '',
  ),
  443 => 
  array (
    'fkContext' => '2245',
    'fkSectionBestPractice' => '1702',
    'nControlType' => '0',
    'sName' => '8.5.5 - PCI',
    'tDescription' => '8.5.5 Remove inactive user accounts at least every 90 days',
    'tImplementationGuide' => '',
  ),
  444 => 
  array (
    'fkContext' => '2246',
    'fkSectionBestPractice' => '1702',
    'nControlType' => '0',
    'sName' => '8.5.6 - PCI',
    'tDescription' => '8.5.6 Enable accounts used by vendors for remote maintenance only during the time period needed',
    'tImplementationGuide' => '',
  ),
  445 => 
  array (
    'fkContext' => '2247',
    'fkSectionBestPractice' => '1702',
    'nControlType' => '0',
    'sName' => '8.5.7 - PCI',
    'tDescription' => '8.5.7 Communicate password procedures and policies to all users who have access to cardholder data',
    'tImplementationGuide' => '',
  ),
  446 => 
  array (
    'fkContext' => '2248',
    'fkSectionBestPractice' => '1702',
    'nControlType' => '0',
    'sName' => '8.5.8 - PCI',
    'tDescription' => '8.5.8 Do not use group, shared, or generic accounts and passwords',
    'tImplementationGuide' => '',
  ),
  447 => 
  array (
    'fkContext' => '2249',
    'fkSectionBestPractice' => '1702',
    'nControlType' => '0',
    'sName' => '8.5.9 - PCI',
    'tDescription' => '8.5.9 Change user passwords at least every 90 days',
    'tImplementationGuide' => '',
  ),
  448 => 
  array (
    'fkContext' => '2250',
    'fkSectionBestPractice' => '1702',
    'nControlType' => '0',
    'sName' => '8.5.10 - PCI',
    'tDescription' => '8.5.10 Require a minimum password length of at least seven characters',
    'tImplementationGuide' => '',
  ),
  449 => 
  array (
    'fkContext' => '2251',
    'fkSectionBestPractice' => '1702',
    'nControlType' => '0',
    'sName' => '8.5.11 - PCI',
    'tDescription' => '8.5.11 Use passwords containing both numeric and alphabetic characters',
    'tImplementationGuide' => '',
  ),
  450 => 
  array (
    'fkContext' => '2252',
    'fkSectionBestPractice' => '1702',
    'nControlType' => '0',
    'sName' => '8.5.12 - PCI',
    'tDescription' => '8.5.12 Do not allow an individual to submit a new password that is the same as any of the last four passwords he or she has used',
    'tImplementationGuide' => '',
  ),
  451 => 
  array (
    'fkContext' => '2253',
    'fkSectionBestPractice' => '1702',
    'nControlType' => '0',
    'sName' => '8.5.13 - PCI',
    'tDescription' => '8.5.13 Limit repeated access attempts by locking out the user ID after not more than six attempts',
    'tImplementationGuide' => '',
  ),
  452 => 
  array (
    'fkContext' => '2254',
    'fkSectionBestPractice' => '1702',
    'nControlType' => '0',
    'sName' => '8.5.14 - PCI',
    'tDescription' => '8.5.14 Set the lockout duration to thirty minutes or until administrator enables the user ID',
    'tImplementationGuide' => '',
  ),
  453 => 
  array (
    'fkContext' => '2255',
    'fkSectionBestPractice' => '1702',
    'nControlType' => '0',
    'sName' => '8.5.15 - PCI',
    'tDescription' => '8.5.15 If a session has been idle for more than 15 minutes, require the user to re-enter the password to re-activate the terminal',
    'tImplementationGuide' => '',
  ),
  454 => 
  array (
    'fkContext' => '2256',
    'fkSectionBestPractice' => '1702',
    'nControlType' => '0',
    'sName' => '8.5.16 - PCI',
    'tDescription' => '8.5.16 Authenticate all access to any database containing cardholder data. This includes access by applications, administrators, and all other users',
    'tImplementationGuide' => '',
  ),
  455 => 
  array (
    'fkContext' => '2257',
    'fkSectionBestPractice' => '1703',
    'nControlType' => '0',
    'sName' => '9.1 - PCI',
    'tDescription' => '9.1 Use appropriate facility entry controls to limit and monitor physical access to systems that store, process, or transmit cardholder data.',
    'tImplementationGuide' => '',
  ),
  456 => 
  array (
    'fkContext' => '2258',
    'fkSectionBestPractice' => '1703',
    'nControlType' => '0',
    'sName' => '9.1.1 - PCI',
    'tDescription' => '9.1.1 Use cameras to monitor sensitive areas. Audit collected data and correlate with other entries. Store for at least three months, unless otherwise restricted by law',
    'tImplementationGuide' => '',
  ),
  457 => 
  array (
    'fkContext' => '2259',
    'fkSectionBestPractice' => '1703',
    'nControlType' => '0',
    'sName' => '9.1.2 - PCI',
    'tDescription' => '9.1.2 Restrict physical access to publicly accessible network jacks',
    'tImplementationGuide' => '',
  ),
  458 => 
  array (
    'fkContext' => '2260',
    'fkSectionBestPractice' => '1703',
    'nControlType' => '0',
    'sName' => '9.1.3 - PCI',
    'tDescription' => '9.1.3 Restrict physical access to wireless access points, gateways, and handheld devices.',
    'tImplementationGuide' => '',
  ),
  459 => 
  array (
    'fkContext' => '2261',
    'fkSectionBestPractice' => '1703',
    'nControlType' => '0',
    'sName' => '9.2 - PCI',
    'tDescription' => '9.2 Develop procedures to help all personnel easily distinguish between employees and visitors, especially in areas where cardholder data is accessible. "Employee" refers to full-time and part-time employees, temporary employees and personnel, and consultants who are "resident" on the entity\'\'s site. A "visitor" is defined as a vendor, guest of an employee, service personnel, or anyone who needs to enter the facility for a short duration, usually not more than one day.',
    'tImplementationGuide' => '',
  ),
  460 => 
  array (
    'fkContext' => '2262',
    'fkSectionBestPractice' => '1703',
    'nControlType' => '0',
    'sName' => '9.3 - PCI',
    'tDescription' => '9.3 Make sure all visitors are handled as follows:',
    'tImplementationGuide' => '',
  ),
  461 => 
  array (
    'fkContext' => '2263',
    'fkSectionBestPractice' => '1703',
    'nControlType' => '0',
    'sName' => '9.3.1 - PCI',
    'tDescription' => '9.3.1 Authorized before entering areas where cardholder data is processed or maintained',
    'tImplementationGuide' => '',
  ),
  462 => 
  array (
    'fkContext' => '2264',
    'fkSectionBestPractice' => '1703',
    'nControlType' => '0',
    'sName' => '9.3.2 - PCI',
    'tDescription' => '9.3.2 Given a physical token (for example, a badge or access device) that expires and that identifies the visitors as non-employees',
    'tImplementationGuide' => '',
  ),
  463 => 
  array (
    'fkContext' => '2265',
    'fkSectionBestPractice' => '1703',
    'nControlType' => '0',
    'sName' => '9.3.3 - PCI',
    'tDescription' => '9.3.3 Asked to surrender the physical token before leaving the facility or at the date of expiration.',
    'tImplementationGuide' => '',
  ),
  464 => 
  array (
    'fkContext' => '2266',
    'fkSectionBestPractice' => '1703',
    'nControlType' => '0',
    'sName' => '9.4 - PCI',
    'tDescription' => '9.4 Use a visitor log to maintain a physical audit trail of visitor activity. Retain this log for a minimum of three months, unless otherwise restricted by law.',
    'tImplementationGuide' => '',
  ),
  465 => 
  array (
    'fkContext' => '2267',
    'fkSectionBestPractice' => '1703',
    'nControlType' => '0',
    'sName' => '9.5 - PCI',
    'tDescription' => '9.5 Store media back-ups in a secure location, preferably in an off-site facility, such as an alternate or backup site, or a commercial storage facility.',
    'tImplementationGuide' => '',
  ),
  466 => 
  array (
    'fkContext' => '2268',
    'fkSectionBestPractice' => '1703',
    'nControlType' => '0',
    'sName' => '9.6 - PCI',
    'tDescription' => '9.6 Physically secure all paper and electronic media (including computers, electronic media, networking and communications hardware, telecommunication lines, paper receipts, paper reports, and faxes) that contain cardholder data.',
    'tImplementationGuide' => '',
  ),
  467 => 
  array (
    'fkContext' => '2269',
    'fkSectionBestPractice' => '1703',
    'nControlType' => '0',
    'sName' => '9.7 - PCI',
    'tDescription' => '9.7 Maintain strict control over the internal or external distribution of any kind of media that contains cardholder data including the following:',
    'tImplementationGuide' => '',
  ),
  468 => 
  array (
    'fkContext' => '2270',
    'fkSectionBestPractice' => '1703',
    'nControlType' => '0',
    'sName' => '9.7.1 - PCI',
    'tDescription' => '9.7.1 Classify the media so it can be identified as confidential',
    'tImplementationGuide' => '',
  ),
  469 => 
  array (
    'fkContext' => '2271',
    'fkSectionBestPractice' => '1703',
    'nControlType' => '0',
    'sName' => '9.7.2 - PCI',
    'tDescription' => '9.7.2 Send the media by secured courier or other delivery method that can be accurately tracked.',
    'tImplementationGuide' => '',
  ),
  470 => 
  array (
    'fkContext' => '2272',
    'fkSectionBestPractice' => '1703',
    'nControlType' => '0',
    'sName' => '9.8 - PCI',
    'tDescription' => '9.8 Ensure management approves any and all media that is moved from a secured area  especially when media is distributed to individuals).',
    'tImplementationGuide' => '',
  ),
  471 => 
  array (
    'fkContext' => '2273',
    'fkSectionBestPractice' => '1703',
    'nControlType' => '0',
    'sName' => '9.9 - PCI',
    'tDescription' => '9.9 Maintain strict control over the storage and accessibility of media that contains cardholder data.',
    'tImplementationGuide' => '',
  ),
  472 => 
  array (
    'fkContext' => '2274',
    'fkSectionBestPractice' => '1703',
    'nControlType' => '0',
    'sName' => '9.9.1 - PCI',
    'tDescription' => '9.9.1 Properly inventory all media and make sure it is securely stored.',
    'tImplementationGuide' => '',
  ),
  473 => 
  array (
    'fkContext' => '2275',
    'fkSectionBestPractice' => '1703',
    'nControlType' => '0',
    'sName' => '9.10 - PCI',
    'tDescription' => '9.10 Destroy media containing cardholder data when it is no longer needed for business or  legal reasons as follows:',
    'tImplementationGuide' => '',
  ),
  474 => 
  array (
    'fkContext' => '2276',
    'fkSectionBestPractice' => '1703',
    'nControlType' => '0',
    'sName' => '9.10.1 - PCI',
    'tDescription' => '9.10.1 Cross-cut shred, incinerate, or pulp hardcopy materials',
    'tImplementationGuide' => '',
  ),
  475 => 
  array (
    'fkContext' => '2277',
    'fkSectionBestPractice' => '1703',
    'nControlType' => '0',
    'sName' => '9.10.2 - PCI',
    'tDescription' => '9.10.2 Purge, degauss, shred, or otherwise destroy electronic media so that cardholder data cannot be reconstructed.',
    'tImplementationGuide' => '',
  ),
  476 => 
  array (
    'fkContext' => '2278',
    'fkSectionBestPractice' => '1705',
    'nControlType' => '0',
    'sName' => '10.1 - PCI',
    'tDescription' => '10.1 Establish a process for linking all access to system components (especially access done with administrative privileges such as root) to each individual user.',
    'tImplementationGuide' => '',
  ),
  477 => 
  array (
    'fkContext' => '2279',
    'fkSectionBestPractice' => '1705',
    'nControlType' => '0',
    'sName' => '10.2 - PCI',
    'tDescription' => '10.2 Implement automated audit trails for all system components to reconstruct the following events:',
    'tImplementationGuide' => '',
  ),
  478 => 
  array (
    'fkContext' => '2280',
    'fkSectionBestPractice' => '1705',
    'nControlType' => '0',
    'sName' => '10.2.1 - PCI',
    'tDescription' => '10.2.1 All individual user accesses to cardholder data',
    'tImplementationGuide' => '',
  ),
  479 => 
  array (
    'fkContext' => '2281',
    'fkSectionBestPractice' => '1705',
    'nControlType' => '0',
    'sName' => '10.2.2 - PCI',
    'tDescription' => '10.2.2 All actions taken by any individual with root or administrative privileges',
    'tImplementationGuide' => '',
  ),
  480 => 
  array (
    'fkContext' => '2282',
    'fkSectionBestPractice' => '1705',
    'nControlType' => '0',
    'sName' => '10.2.3 - PCI',
    'tDescription' => '10.2.3 Access to all audit trails',
    'tImplementationGuide' => '',
  ),
  481 => 
  array (
    'fkContext' => '2283',
    'fkSectionBestPractice' => '1705',
    'nControlType' => '0',
    'sName' => '10.2.4 - PCI',
    'tDescription' => '10.2.4 Invalid logical access attempts',
    'tImplementationGuide' => '',
  ),
  482 => 
  array (
    'fkContext' => '2284',
    'fkSectionBestPractice' => '1705',
    'nControlType' => '0',
    'sName' => '10.2.5 - PCI',
    'tDescription' => '10.2.5 Use of identification and authentication mechanisms',
    'tImplementationGuide' => '',
  ),
  483 => 
  array (
    'fkContext' => '2285',
    'fkSectionBestPractice' => '1705',
    'nControlType' => '0',
    'sName' => '10.2.6 - PCI',
    'tDescription' => '10.2.6 Initialization of the audit logs',
    'tImplementationGuide' => '',
  ),
  484 => 
  array (
    'fkContext' => '2286',
    'fkSectionBestPractice' => '1705',
    'nControlType' => '0',
    'sName' => '10.2.7 - PCI',
    'tDescription' => '10.2.7 Creation and deletion of system-level objects.',
    'tImplementationGuide' => '',
  ),
  485 => 
  array (
    'fkContext' => '2287',
    'fkSectionBestPractice' => '1705',
    'nControlType' => '0',
    'sName' => '10.3 - PCI',
    'tDescription' => '10.3 Record at least the following audit trail entries for all system components for each event:',
    'tImplementationGuide' => '',
  ),
  486 => 
  array (
    'fkContext' => '2288',
    'fkSectionBestPractice' => '1705',
    'nControlType' => '0',
    'sName' => '10.3.1 - PCI',
    'tDescription' => '10.3.1 User identification',
    'tImplementationGuide' => '',
  ),
  487 => 
  array (
    'fkContext' => '2289',
    'fkSectionBestPractice' => '1705',
    'nControlType' => '0',
    'sName' => '10.5.2 - PCI',
    'tDescription' => '10.5.2 Protect audit trail files from unauthorized modifications',
    'tImplementationGuide' => '',
  ),
  488 => 
  array (
    'fkContext' => '2290',
    'fkSectionBestPractice' => '1705',
    'nControlType' => '0',
    'sName' => '10.5.3 - PCI',
    'tDescription' => '10.5.3 Promptly back-up audit trail files to a centralized log server or media that is difficult to alter',
    'tImplementationGuide' => '',
  ),
  489 => 
  array (
    'fkContext' => '2291',
    'fkSectionBestPractice' => '1705',
    'nControlType' => '0',
    'sName' => '10.5.4 - PCI',
    'tDescription' => '10.5.4 Copy logs for wireless networks onto a log server on the internal LAN.',
    'tImplementationGuide' => '',
  ),
  490 => 
  array (
    'fkContext' => '2292',
    'fkSectionBestPractice' => '1705',
    'nControlType' => '0',
    'sName' => '10.5.5 - PCI',
    'tDescription' => '10.5.5 Use file integrity monitoring and change detection software on logs to ensure that existing log data cannot be changed without generating alerts (although new data being added should not cause an alert).',
    'tImplementationGuide' => '',
  ),
  491 => 
  array (
    'fkContext' => '2293',
    'fkSectionBestPractice' => '1705',
    'nControlType' => '0',
    'sName' => '10.6 - PCI',
    'tDescription' => '10.6 Review logs for all system components at least daily. Log reviews must include those servers that perform security functions like intrusion detection system (IDS) and authentication, authorization, and accounting protocol (AAA) servers (for example, RADIUS). Note: Log harvesting, parsing, and alerting tools may be used to achieve compliance with Requirement 10.6.',
    'tImplementationGuide' => '',
  ),
  492 => 
  array (
    'fkContext' => '2294',
    'fkSectionBestPractice' => '1705',
    'nControlType' => '0',
    'sName' => '10.7 - PCI',
    'tDescription' => '10.7 Retain audit trail history for at least one year, with a minimum of three months online availability.',
    'tImplementationGuide' => '',
  ),
  493 => 
  array (
    'fkContext' => '2295',
    'fkSectionBestPractice' => '1706',
    'nControlType' => '0',
    'sName' => '11.1 - PCI',
    'tDescription' => '11.1 Test security controls, limitations, network connections, and restrictions annually to assure the ability to adequately identify and to stop any unauthorized access attempts. Use a wireless analyzer at least quarterly to identify all wireless devices in use.',
    'tImplementationGuide' => '',
  ),
  494 => 
  array (
    'fkContext' => '2296',
    'fkSectionBestPractice' => '1706',
    'nControlType' => '0',
    'sName' => '11.2 - PCI',
    'tDescription' => '11.2 Run internal and external network vulnerability scans at least quarterly and after any significant change in the network (such as new system component installations, changes in network topology, firewall rule modifications, product upgrades). Note: Quarterly external vulnerability scans must be performed by a scan vendor qualified by the payment card industry. Scans conducted after network changes may be performed by the company\'\'s internal staff.',
    'tImplementationGuide' => '',
  ),
  495 => 
  array (
    'fkContext' => '2297',
    'fkSectionBestPractice' => '1706',
    'nControlType' => '0',
    'sName' => '11.3 - PCI',
    'tDescription' => '11.3 Perform penetration testing at least once a year and after any significant infrastructure or application upgrade or modification (such as an operating system upgrade, a sub-network added to the environment, or a web server added to the environment). These penetration tests must include the following:',
    'tImplementationGuide' => '',
  ),
  496 => 
  array (
    'fkContext' => '2298',
    'fkSectionBestPractice' => '1706',
    'nControlType' => '0',
    'sName' => '11.3.1 - PCI',
    'tDescription' => '11.3.1 Network-layer penetration tests',
    'tImplementationGuide' => '',
  ),
  497 => 
  array (
    'fkContext' => '2299',
    'fkSectionBestPractice' => '1706',
    'nControlType' => '0',
    'sName' => '11.3.2 - PCI',
    'tDescription' => '11.3.2 Application-layer penetration tests.',
    'tImplementationGuide' => '',
  ),
  498 => 
  array (
    'fkContext' => '2300',
    'fkSectionBestPractice' => '1706',
    'nControlType' => '0',
    'sName' => '11.4 - PCI',
    'tDescription' => '11.4 Use network intrusion detection systems, host-based intrusion detection systems, and intrusion prevention systems to monitor all network traffic and alert personnel to suspected compromises. Keep all intrusion detection and prevention engines up-to-date.',
    'tImplementationGuide' => '',
  ),
  499 => 
  array (
    'fkContext' => '2301',
    'fkSectionBestPractice' => '1706',
    'nControlType' => '0',
    'sName' => '11.5 - PCI',
    'tDescription' => '11.5 Deploy file integrity monitoring software to alert personnel to unauthorized modification of critical system or content files;
and configure the software to perform critical file comparisons at least weekly. Critical files are not necessarily only those containing cardholder data. For file integrity monitoring purposes, critical files are usually those that do not regularly change, but the modification of which could indicate a system compromise or risk of compromise. File integrity monitoring products usually come pre-configured with critical files for the related operating system. Other critical files, such as those for custom applications, must be evaluated and defined by the entity (that is the merchant or service provider).',
    'tImplementationGuide' => '',
  ),
  500 => 
  array (
    'fkContext' => '2302',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.1 - PCI',
    'tDescription' => '12.1 Establish, publish, maintain, and disseminate a security policy that accomplishes the following:',
    'tImplementationGuide' => '',
  ),
  501 => 
  array (
    'fkContext' => '2303',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.1.1 - PCI',
    'tDescription' => '12.1.1 Addresses all requirements in this specification',
    'tImplementationGuide' => '',
  ),
  502 => 
  array (
    'fkContext' => '2304',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.1.2 - PCI',
    'tDescription' => '12.1.2 Includes an annual process that identifies threats and vulnerabilities, and results in a formal risk assessment',
    'tImplementationGuide' => '',
  ),
  503 => 
  array (
    'fkContext' => '2305',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.1.3 - PCI',
    'tDescription' => '12.1.3 Includes a review at least once a year and updates when the environment changes.',
    'tImplementationGuide' => '',
  ),
  504 => 
  array (
    'fkContext' => '2306',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.2 - PCI',
    'tDescription' => '12.2 Develop daily operational security procedures that are consistent with requirements in this specification (for example, user account maintenance procedures, and log review procedures).',
    'tImplementationGuide' => '',
  ),
  505 => 
  array (
    'fkContext' => '2307',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.3 - PCI',
    'tDescription' => '12.3 Develop usage policies for critical employee-facing technologies (such as modems and wireless) to define proper use of these technologies for all employees and contractors. Ensure these usage policies require the following:',
    'tImplementationGuide' => '',
  ),
  506 => 
  array (
    'fkContext' => '2308',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.3.1 - PCI',
    'tDescription' => '12.3.1 Explicit management approval',
    'tImplementationGuide' => '',
  ),
  507 => 
  array (
    'fkContext' => '2309',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.3.2 - PCI',
    'tDescription' => '12.3.2 Authentication for use of the technology',
    'tImplementationGuide' => '',
  ),
  508 => 
  array (
    'fkContext' => '2310',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.3.3 - PCI',
    'tDescription' => '12.3.3 List of all such devices and personnel with access',
    'tImplementationGuide' => '',
  ),
  509 => 
  array (
    'fkContext' => '2311',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.3.4 - PCI',
    'tDescription' => '12.3.4 Labeling of devices with owner, contact information, and purpose',
    'tImplementationGuide' => '',
  ),
  510 => 
  array (
    'fkContext' => '2312',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.3.5 - PCI',
    'tDescription' => '12.3.5 Acceptable uses of the technologies',
    'tImplementationGuide' => '',
  ),
  511 => 
  array (
    'fkContext' => '2313',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.3.6 - PCI',
    'tDescription' => '12.3.6 Acceptable network locations for the technologies',
    'tImplementationGuide' => '',
  ),
  512 => 
  array (
    'fkContext' => '2314',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.3.7 - PCI',
    'tDescription' => '12.3.7 List of company-approved products',
    'tImplementationGuide' => '',
  ),
  513 => 
  array (
    'fkContext' => '2315',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.3.8 - PCI',
    'tDescription' => '12.3.8 Automatic disconnect of modem sessions after a specific period of inactivity',
    'tImplementationGuide' => '',
  ),
  514 => 
  array (
    'fkContext' => '2316',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.3.9 - PCI',
    'tDescription' => '12.3.9 Activation of modems for vendors only when needed by vendors, with immediate deactivation after use',
    'tImplementationGuide' => '',
  ),
  515 => 
  array (
    'fkContext' => '2317',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.3.10 - PCI',
    'tDescription' => '12.3.10 When accessing cardholder data remotely via modem, prohibition of storage of cardholder data onto local hard drives, floppy disks, or other external media. Prohibition of cut-and-paste and print functions during remote access.',
    'tImplementationGuide' => '',
  ),
  516 => 
  array (
    'fkContext' => '2318',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.4 - PCI',
    'tDescription' => '12.4 Ensure that the security policy and procedures clearly define information security responsibilities for all employees and contractors.',
    'tImplementationGuide' => '',
  ),
  517 => 
  array (
    'fkContext' => '2319',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.5 - PCI',
    'tDescription' => '12.5 Assign to an individual or team the following information security management responsibilities:',
    'tImplementationGuide' => '',
  ),
  518 => 
  array (
    'fkContext' => '2320',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.5.1 - PCI',
    'tDescription' => '12.5.1 Establish, document, and distribute security policies and procedures',
    'tImplementationGuide' => '',
  ),
  519 => 
  array (
    'fkContext' => '2321',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.5.2 - PCI',
    'tDescription' => '12.5.2 Monitor and analyze security alerts and information, and distribute to appropriate personnel',
    'tImplementationGuide' => '',
  ),
  520 => 
  array (
    'fkContext' => '2322',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.5.3 - PCI',
    'tDescription' => '12.5.3 Establish, document, and distribute security incident response and escalation procedures to ensure timely and effective handling of all situations',
    'tImplementationGuide' => '',
  ),
  521 => 
  array (
    'fkContext' => '2323',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.5.4 - PCI',
    'tDescription' => '12.5.4 Administer user accounts, including additions, deletions, and modifications',
    'tImplementationGuide' => '',
  ),
  522 => 
  array (
    'fkContext' => '2324',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.5.5 - PCI',
    'tDescription' => '12.5.5 Monitor and control all access to data.',
    'tImplementationGuide' => '',
  ),
  523 => 
  array (
    'fkContext' => '2325',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.6 - PCI',
    'tDescription' => '12.6 Implement a formal security awareness program to make all employees aware of the importance of cardholder data security.',
    'tImplementationGuide' => '',
  ),
  524 => 
  array (
    'fkContext' => '2326',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.6.1 - PCI',
    'tDescription' => '12.6.1 Educate employees upon hire and at least annually (for example, by letters, posters, memos, meetings, and promotions)',
    'tImplementationGuide' => '',
  ),
  525 => 
  array (
    'fkContext' => '2327',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.6.2 - PCI',
    'tDescription' => '12.6.2 Require employees to acknowledge in writing that they have read and understood the company\'\'s security policy and procedures.',
    'tImplementationGuide' => '',
  ),
  526 => 
  array (
    'fkContext' => '2328',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.7 - PCI',
    'tDescription' => '12.7 Screen potential employees to minimize the risk of attacks from internal sources. For those employees such as store cashiers who only have access to one card number at a time when facilitating a transaction, this requirement is a recommendation only.',
    'tImplementationGuide' => '',
  ),
  527 => 
  array (
    'fkContext' => '2329',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.8 - PCI',
    'tDescription' => '12.8 If cardholder data is shared with service providers, then contractually the following is required:',
    'tImplementationGuide' => '',
  ),
  528 => 
  array (
    'fkContext' => '2330',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.8.1 - PCI',
    'tDescription' => '12.8.1 Service providers must adhere to the PCI DSS requirements',
    'tImplementationGuide' => '',
  ),
  529 => 
  array (
    'fkContext' => '2331',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.8.2 - PCI',
    'tDescription' => '12.8.2 Agreement that includes an acknowledgement that the service provider is responsible for the security of cardholder data the provider possesses.',
    'tImplementationGuide' => '',
  ),
  530 => 
  array (
    'fkContext' => '2332',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.9 - PCI',
    'tDescription' => '12.9 Implement an incident response plan. Be prepared to respond immediately to a system breach.',
    'tImplementationGuide' => '',
  ),
  531 => 
  array (
    'fkContext' => '2333',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.9.1 - PCI',
    'tDescription' => '12.9.1 Create the incident response plan to be implemented in the event of system compromise. Ensure the plan addresses, at a minimum, specific incident response procedures, business recovery and continuity procedures, data backup processes, roles and responsibilities, and communication and contact strategies (for example, informing the Acquirers and credit card associations)',
    'tImplementationGuide' => '',
  ),
  532 => 
  array (
    'fkContext' => '2334',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.9.2 - PCI',
    'tDescription' => '12.9.2 Test the plan at least annually',
    'tImplementationGuide' => '',
  ),
  533 => 
  array (
    'fkContext' => '2335',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.9.3 - PCI',
    'tDescription' => '12.9.3 Designate specific personnel to be available on a 24/7 basis to respond to alerts',
    'tImplementationGuide' => '',
  ),
  534 => 
  array (
    'fkContext' => '2336',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.9.4 - PCI',
    'tDescription' => '12.9.4 Provide appropriate training to staff with security breach response responsibilities',
    'tImplementationGuide' => '',
  ),
  535 => 
  array (
    'fkContext' => '2337',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.9.5 - PCI',
    'tDescription' => '12.9.5 Include alerts from intrusion detection, intrusion prevention, and file integrity monitoring systems',
    'tImplementationGuide' => '',
  ),
  536 => 
  array (
    'fkContext' => '2338',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.9.6 - PCI',
    'tDescription' => '12.9.6 Develop process to modify and evolve the incident response plan according to lessons learned and to incorporate industry developments.',
    'tImplementationGuide' => '',
  ),
  537 => 
  array (
    'fkContext' => '2339',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.10 - PCI',
    'tDescription' => '12.10 All processors and service providers must maintain and implement policies and procedures to manage connected entities, to include the following:',
    'tImplementationGuide' => '',
  ),
  538 => 
  array (
    'fkContext' => '2340',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.10.1 - PCI',
    'tDescription' => '12.10.1. Maintain a list of connected entities',
    'tImplementationGuide' => '',
  ),
  539 => 
  array (
    'fkContext' => '2341',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.10.2 - PCI',
    'tDescription' => '12.10.2. Ensure proper due diligence is conducted prior to connecting an entity',
    'tImplementationGuide' => '',
  ),
  540 => 
  array (
    'fkContext' => '2342',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.10.3 - PCI',
    'tDescription' => '12.10.3. Ensure the entity is PCI DSS compliant',
    'tImplementationGuide' => '',
  ),
  541 => 
  array (
    'fkContext' => '2343',
    'fkSectionBestPractice' => '1708',
    'nControlType' => '0',
    'sName' => '12.10.4 - PCI',
    'tDescription' => '12.10.4. Connect and disconnect entities by following an established process.',
    'tImplementationGuide' => '',
  ),
  542 => 
  array (
    'fkContext' => '2344',
    'fkSectionBestPractice' => '1705',
    'nControlType' => '1',
    'sName' => '10.3.2 - PCI',
    'tDescription' => '10.3.2 Type of event',
    'tImplementationGuide' => '',
  ),
  543 => 
  array (
    'fkContext' => '2345',
    'fkSectionBestPractice' => '1705',
    'nControlType' => '1',
    'sName' => '10.3.3 - PCI',
    'tDescription' => '10.3.3 Date and time',
    'tImplementationGuide' => '',
  ),
  544 => 
  array (
    'fkContext' => '2346',
    'fkSectionBestPractice' => '1705',
    'nControlType' => '1',
    'sName' => '10.3.4 - PCI',
    'tDescription' => '10.3.4 Success or failure indication',
    'tImplementationGuide' => '',
  ),
  545 => 
  array (
    'fkContext' => '2347',
    'fkSectionBestPractice' => '1705',
    'nControlType' => '1',
    'sName' => '10.3.5 - PCI',
    'tDescription' => '10.3.5 Origination of event',
    'tImplementationGuide' => '',
  ),
  546 => 
  array (
    'fkContext' => '2348',
    'fkSectionBestPractice' => '1705',
    'nControlType' => '1',
    'sName' => '10.3.6 - PCI',
    'tDescription' => '10.3.6 Identity or system compone name of affected data, nt, or resource',
    'tImplementationGuide' => '',
  ),
  547 => 
  array (
    'fkContext' => '2349',
    'fkSectionBestPractice' => '1705',
    'nControlType' => '1',
    'sName' => '10.4 - PCI',
    'tDescription' => '10.4 Synchronize all critical system clocks times',
    'tImplementationGuide' => '',
  ),
  548 => 
  array (
    'fkContext' => '2350',
    'fkSectionBestPractice' => '1705',
    'nControlType' => '1',
    'sName' => '10.5 - PCI',
    'tDescription' => '10.5 Secure audit trails so they cannot be altered',
    'tImplementationGuide' => '',
  ),
  549 => 
  array (
    'fkContext' => '2351',
    'fkSectionBestPractice' => '1705',
    'nControlType' => '1',
    'sName' => '10.5.1 - PCI',
    'tDescription' => '10.5.1 Limit viewing of audit trails to thoswith a job-related need',
    'tImplementationGuide' => '',
  ),
)
?>