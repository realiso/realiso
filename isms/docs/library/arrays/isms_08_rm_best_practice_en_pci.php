<?php

$laBestPracticeEN_PCI = 
array (
  0 => 
  array (
    'fkContext' => '6263',
    'fkSectionBestPractice' => '6262',
    'nControlType' => '1',
    'sName' => '1.1',
    'tDescription' => 'Establish firewall and router
configuration standards',
    'tImplementationGuide' => '1.1 Obtain and inspect the firewall and router configuration
standards and other documentation specified below to verify
that standards are complete.',
  ),
  1 => 
  array (
    'fkContext' => '6265',
    'fkSectionBestPractice' => '6262',
    'nControlType' => '1',
    'sName' => '1.1.1',
    'tDescription' => 'A formal process for approving
and testing all network connections and
changes to the firewall and router
configurations',
    'tImplementationGuide' => '1.1.1 Verify that there is a formal process for testing and
approval of all network connections and changes to
firewall and router configurations.',
  ),
  2 => 
  array (
    'fkContext' => '6267',
    'fkSectionBestPractice' => '6262',
    'nControlType' => '1',
    'sName' => '1.1.2',
    'tDescription' => 'Current network diagram with all
connections to cardholder data, including
any wireless networks',
    'tImplementationGuide' => '1.1.2.a Verify that a current network diagram (for
example, one that shows cardholder data flows over the
network) exists and that it documents all connections to
cardholder data, including any wireless networks.
1.1.2.b Verify that the diagram is kept current.',
  ),
  3 => 
  array (
    'fkContext' => '6269',
    'fkSectionBestPractice' => '6262',
    'nControlType' => '1',
    'sName' => '1.1.3',
    'tDescription' => 'Requirements for a firewall at
each Internet connection and between any
demilitarized zone (DMZ) and the internal
network zone',
    'tImplementationGuide' => '1.1.3 Verify that firewall configuration standards include
requirements for a firewall at each Internet connection and
between any DMZ and the internal network zone. Verify
that the current network diagram is consistent with the
firewall configuration standards.',
  ),
  4 => 
  array (
    'fkContext' => '6271',
    'fkSectionBestPractice' => '6262',
    'nControlType' => '1',
    'sName' => '1.1.4',
    'tDescription' => 'Description of groups, roles, and
responsibilities for logical management of
network components',
    'tImplementationGuide' => '1.1.4 Verify that firewall and router configuration
standards include a description of groups, roles, and
responsibilities for logical management of network
components.',
  ),
  5 => 
  array (
    'fkContext' => '6273',
    'fkSectionBestPractice' => '6262',
    'nControlType' => '1',
    'sName' => '1.1.5',
    'tDescription' => 'Documentation and business
justification for use of all services,
protocols, and ports allowed, including
documentation of security features
implemented for those protocols
considered to be insecure',
    'tImplementationGuide' => '1.1.5.a Verify that firewall and router configuration
standards include a documented list of services, protocols
and ports necessary for business—for example, hypertext
transfer protocol (HTTP) and Secure Sockets Layer (SSL),
Secure Shell (SSH), and Virtual Private Network (VPN)
protocols.
1.1.5.b Identify insecure services, protocols, and ports
allowed; and verify they are necessary and that security
features are documented and implemented by examining
firewall and router configuration standards and settings for
each service. An example of an insecure service, protocol,
or port is FTP, which passes user credentials in clear-text.',
  ),
  6 => 
  array (
    'fkContext' => '6275',
    'fkSectionBestPractice' => '6262',
    'nControlType' => '1',
    'sName' => '1.1.6',
    'tDescription' => 'Requirement to review firewall
and router rule sets at least every six
months',
    'tImplementationGuide' => '1.1.6.a Verify that firewall and router configuration
standards require review of firewall and router rule sets at
least every six months.
1.1.6.b Obtain and examine documentation to verify that
the rule sets are reviewed at least every six months.',
  ),
  7 => 
  array (
    'fkContext' => '6277',
    'fkSectionBestPractice' => '6262',
    'nControlType' => '1',
    'sName' => '1.2',
    'tDescription' => 'Build a firewall configuration that
restricts connections between untrusted
networks and any system components in
the cardholder data environment.',
    'tImplementationGuide' => 'Examine firewall and router configurations to verify
that connections are restricted between untrusted networks
and system components in the cardholder data
environment.

Note: An “untrusted network” is any network that is external to the networks belonging to the entity under
review, and/or which is out of the entity\'s ability to control or manage.',
  ),
  8 => 
  array (
    'fkContext' => '6279',
    'fkSectionBestPractice' => '6262',
    'nControlType' => '1',
    'sName' => '1.2.1',
    'tDescription' => 'Restrict inbound and outbound
traffic to that which is necessary for the
cardholder data environment.',
    'tImplementationGuide' => '1.2.1.a Verify that inbound and outbound traffic is limited
to that which is necessary for the cardholder data
environment, and that the restrictions are documented.
1.2.1.b Verify that all other inbound and outbound traffic is
specifically denied, for example by using an explicit “deny
all” or an implicit deny after allow statement.',
  ),
  9 => 
  array (
    'fkContext' => '6281',
    'fkSectionBestPractice' => '6262',
    'nControlType' => '1',
    'sName' => '1.2.2',
    'tDescription' => 'Secure and synchronize router
configuration files.',
    'tImplementationGuide' => '1.2.2 Verify that router configuration files are secure
and synchronized—for example, running configuration
files (used for normal running of the routers) and start-up
configuration files (used when machines are re-booted),
have the same, secure configurations.',
  ),
  10 => 
  array (
    'fkContext' => '6283',
    'fkSectionBestPractice' => '6262',
    'nControlType' => '1',
    'sName' => '1.2.3',
    'tDescription' => 'Install perimeter firewalls between
any wireless networks and the cardholder
data environment, and configure these
firewalls to deny or control (if such traffic is
necessary for business purposes) any
traffic from the wireless environment into
the cardholder data environment.',
    'tImplementationGuide' => '1.2.3 Verify that there are perimeter firewalls installed
between any wireless networks and systems that store
cardholder data, and that these firewalls deny or control (if
such traffic is necessary for business purposes) any traffic
from the wireless environment into the cardholder data
environment.',
  ),
  11 => 
  array (
    'fkContext' => '6285',
    'fkSectionBestPractice' => '6262',
    'nControlType' => '1',
    'sName' => '1.3',
    'tDescription' => 'Prohibit direct public access between
the Internet and any system component in
the cardholder data environment.',
    'tImplementationGuide' => '1.3 Examine firewall and router configurations, as detailed
below, to determine that there is no direct access between
the Internet and system components, including the choke
router at the Internet, the DMZ router and firewall, the DMZ
cardholder segment, the perimeter router, and the internal
cardholder network segment.',
  ),
  12 => 
  array (
    'fkContext' => '6287',
    'fkSectionBestPractice' => '6262',
    'nControlType' => '1',
    'sName' => '1.3.1',
    'tDescription' => 'Implement a DMZ to limit inbound
and outbound traffic to only protocols that
are necessary for the cardholder data
environment.',
    'tImplementationGuide' => '1.3.1 Verify that a DMZ is implemented to limit inbound
and outbound traffic to only protocols that are necessary
for the cardholder data environment.',
  ),
  13 => 
  array (
    'fkContext' => '6289',
    'fkSectionBestPractice' => '6262',
    'nControlType' => '1',
    'sName' => '1.3.2',
    'tDescription' => 'Limit inbound Internet traffic to IP
addresses within the DMZ.',
    'tImplementationGuide' => '1.3.2 Verify that inbound Internet traffic is limited to IP
addresses within the DMZ.',
  ),
  14 => 
  array (
    'fkContext' => '6291',
    'fkSectionBestPractice' => '6262',
    'nControlType' => '1',
    'sName' => '1.3.3',
    'tDescription' => 'Do not allow any direct routes
inbound or outbound for traffic between
the Internet and the cardholder data
environment.',
    'tImplementationGuide' => '1.3.3 Verify there is no direct route inbound or outbound
for traffic between the Internet and the cardholder data
environment.',
  ),
  15 => 
  array (
    'fkContext' => '6293',
    'fkSectionBestPractice' => '6262',
    'nControlType' => '1',
    'sName' => '1.3.4',
    'tDescription' => 'Do not allow internal addresses to
pass from the Internet into the DMZ.',
    'tImplementationGuide' => '1.3.4 Verify that internal addresses cannot pass from the
Internet into the DMZ.',
  ),
  16 => 
  array (
    'fkContext' => '6295',
    'fkSectionBestPractice' => '6262',
    'nControlType' => '1',
    'sName' => '1.3.5',
    'tDescription' => 'Restrict outbound traffic from the
cardholder data environment to the
Internet such that outbound traffic can
only access IP addresses within the DMZ.',
    'tImplementationGuide' => '1.3.5 Verify that outbound traffic from the cardholder data
environment to the Internet can only access IP addresses
within the DMZ.',
  ),
  17 => 
  array (
    'fkContext' => '6297',
    'fkSectionBestPractice' => '6262',
    'nControlType' => '1',
    'sName' => '1.3.6',
    'tDescription' => 'Implement stateful inspection,
also known as dynamic packet filtering.
(That is, only ”established” connections
are allowed into the network.)',
    'tImplementationGuide' => '1.3.6 Verify that the firewall performs stateful inspection
(dynamic packet filtering). [Only established connections
should be allowed in, and only if they are associated with
a previously established session (run a port scanner on all
TCP ports with “syn reset” or ”syn ack” bits set—a
response means packets are allowed through even if they
are not part of a previously established session).]',
  ),
  18 => 
  array (
    'fkContext' => '6299',
    'fkSectionBestPractice' => '6262',
    'nControlType' => '1',
    'sName' => '1.3.7',
    'tDescription' => 'Place the database in an internal
network zone, segregated from the DMZ.',
    'tImplementationGuide' => '1.3.7 Verify that the database is on an internal network
zone, segregated from the DMZ.',
  ),
  19 => 
  array (
    'fkContext' => '6301',
    'fkSectionBestPractice' => '6262',
    'nControlType' => '1',
    'sName' => '1.3.8',
    'tDescription' => 'Implement IP masquerading to
prevent internal addresses from being
translated and revealed on the Internet,
using RFC 1918 address space. Use
network address translation (NAT)
technologies—for example, port address
translation (PAT).',
    'tImplementationGuide' => '1.3.8 For the sample of firewall and router components,
verify that NAT or other technology using RFC 1918
address space is used to restrict broadcast of IP
addresses from the internal network to the Internet (IP
masquerading).',
  ),
  20 => 
  array (
    'fkContext' => '6303',
    'fkSectionBestPractice' => '6262',
    'nControlType' => '1',
    'sName' => '1.4',
    'tDescription' => '1.4 Install personal firewall software on
any mobile and/or employee-owned
computers with direct connectivity to the
Internet (for example, laptops used by
employees), which are used to access the
organization’s network.',
    'tImplementationGuide' => '1.4.a Verify that mobile and/or employee-owned
computers with direct connectivity to the Internet (for
example, laptops used by employees), and which are used
to access the organization’s network, have personal firewall
software installed and active.
1.4.b Verify that the personal firewall software is configured
by the organization to specific standards and is not alterable
by mobile computer users.',
  ),
  21 => 
  array (
    'fkContext' => '6306',
    'fkSectionBestPractice' => '6305',
    'nControlType' => '1',
    'sName' => '2.1',
    'tDescription' => 'Always change vendor-supplied
defaults before installing a system on the
network—for example, include passwords,
simple network management protocol
(SNMP) community strings, and elimination
of unnecessary accounts.',
    'tImplementationGuide' => '2.1 Choose a sample of system components, critical
servers, and wireless access points, and attempt to log on
(with system administrator help) to the devices using default
vendor-supplied accounts and passwords, to verify that
default accounts and passwords have been changed. (Use
vendor manuals and sources on the Internet to find vendorsupplied
accounts/passwords.)',
  ),
  22 => 
  array (
    'fkContext' => '6308',
    'fkSectionBestPractice' => '6305',
    'nControlType' => '1',
    'sName' => '2.1.1',
    'tDescription' => 'For wireless environments
connected to the cardholder data
environment or transmitting cardholder
data, change wireless vendor defaults,
including but not limited to default wireless
encryption keys, passwords, and SNMP
community strings. Ensure wireless device
security settings are enabled for strong
encryption technology for authentication
and transmission.',
    'tImplementationGuide' => '2.1.1 Verify the following regarding vendor default
settings for wireless environments and ensure that all
wireless networks implement strong encryption
mechanisms (for example, AES):

- Encryption keys were changed from default at
installation, and are changed anytime anyone with
knowledge of the keys leaves the company or
changes positions

- Default SNMP community strings on wireless
devices were changed

- Default passwords/passphrases on access points
were changed

- Firmware on wireless devices is updated to support
strong encryption for authentication and
transmission over wireless networks (for example,
WPA/WPA2)

- Other security-related wireless vendor defaults, if
applicable',
  ),
  23 => 
  array (
    'fkContext' => '6310',
    'fkSectionBestPractice' => '6305',
    'nControlType' => '1',
    'sName' => '2.2',
    'tDescription' => 'Develop configuration standards for
all system components. Assure that these
standards address all known security
vulnerabilities and are consistent with
industry-accepted system hardening
standards.',
    'tImplementationGuide' => '2.2.a Examine the organization’s system configuration
standards for all types of system components and verify the
system configuration standards are consistent with industryaccepted
hardening standards—for example, SysAdmin
Audit Network Security (SANS), National Institute of
Standards Technology (NIST), and Center for Internet
Security (CIS).
2.2.b Verify that system configuration standards include
each item below (at 2.2.1 – 2.2.4).
2.2.c Verify that system configuration standards are
applied when new systems are configured.',
  ),
  24 => 
  array (
    'fkContext' => '6312',
    'fkSectionBestPractice' => '6305',
    'nControlType' => '1',
    'sName' => '2.2.1',
    'tDescription' => 'Implement only one primary
function per server.',
    'tImplementationGuide' => '2.2.1 For a sample of system components, verify that
only one primary function is implemented per server. For
example, web servers, database servers, and DNS should
be implemented on separate servers.',
  ),
  25 => 
  array (
    'fkContext' => '6314',
    'fkSectionBestPractice' => '6305',
    'nControlType' => '1',
    'sName' => '2.2.2',
    'tDescription' => 'Disable all unnecessary and
insecure services and protocols (services
and protocols not directly needed to
perform the device’s specified function).',
    'tImplementationGuide' => '2.2.2 For a sample of system components, inspect
enabled system services, daemons, and protocols. Verify
that unnecessary or insecure services or protocols are not
enabled, or are justified and documented as to appropriate
use of the service. For example, FTP is not used, or is
encrypted via SSH or other technology.',
  ),
  26 => 
  array (
    'fkContext' => '6316',
    'fkSectionBestPractice' => '6305',
    'nControlType' => '1',
    'sName' => '2.2.3',
    'tDescription' => '2.2.3 Configure system security
parameters to prevent misuse.',
    'tImplementationGuide' => '2.2.3.a Interview system administrators and/or security
managers to verify that they have knowledge of common
security parameter settings for system components.
2.2.3.b Verify that common security parameter settings
are included in the system configuration standards.
2.2.3.c For a sample of system components, verify that
common security parameters are set appropriately.',
  ),
  27 => 
  array (
    'fkContext' => '6318',
    'fkSectionBestPractice' => '6305',
    'nControlType' => '1',
    'sName' => '2.2.4',
    'tDescription' => 'Remove all unnecessary
functionality, such as scripts, drivers,
features, subsystems, file systems, and
unnecessary web servers.',
    'tImplementationGuide' => '2.2.4 For a sample of system components, verify that all
unnecessary functionality (for example, scripts, drivers,
features, subsystems, file systems, etc.) is removed. Verify
enabled functions are documented and support secure
configuration, and that only documented functionality is
present on the sampled machines.',
  ),
  28 => 
  array (
    'fkContext' => '6320',
    'fkSectionBestPractice' => '6305',
    'nControlType' => '1',
    'sName' => '2.3',
    'tDescription' => 'Encrypt all non-console
administrative access. Use technologies
such as SSH, VPN, or SSL/TLS for webbased
management and other non-console
administrative access.',
    'tImplementationGuide' => 'For a sample of system components, verify that nonconsole
administrative access is encrypted by:

- Observing an administrator log on to each system
to verify that a strong encryption method is invoked
before the administrator’s password is requested;

- Reviewing services and parameter files on systems
to determine that Telnet and other remote log-in
commands are not available for use internally; and

- Verifying that administrator access to the webbased
management interfaces is encrypted with
strong cryptography.',
  ),
  29 => 
  array (
    'fkContext' => '6322',
    'fkSectionBestPractice' => '6305',
    'nControlType' => '1',
    'sName' => '2.4',
    'tDescription' => 'Shared hosting providers must
protect each entity’s hosted environment
and cardholder data. These providers must
meet specific requirements as detailed in
Appendix A: Additional PCI DSS
Requirements for Shared Hosting Providers.',
    'tImplementationGuide' => 'Perform testing procedures A.1.1 through A.1.4
detailed in Appendix A: Additional PCI DSS Requirements
for Shared Hosting Providers for PCI DSS assessments of
shared hosting providers, to verify that shared hosting
providers protect their entities’ (merchants and service
providers) hosted environment and data.',
  ),
  30 => 
  array (
    'fkContext' => '6325',
    'fkSectionBestPractice' => '6324',
    'nControlType' => '1',
    'sName' => '3.1',
    'tDescription' => 'Keep cardholder data storage to
a minimum. Develop a data retention and
disposal policy. Limit storage amount
and retention time to that which is
required for business, legal, and/or
regulatory purposes, as documented in
the data retention policy.',
    'tImplementationGuide' => '3.1 Obtain and examine the company policies and
procedures for data retention and disposal, and perform the
following

- Verify that policies and procedures include legal,
regulatory, and business requirements for data
retention, including specific requirements for
retention of cardholder data (for example,
cardholder data needs to be held for X period for Y
business reasons)

- Verify that policies and procedures include
provisions for disposal of data when no longer
needed for legal, regulatory, or business reasons,
including disposal of cardholder data

- Verify that policies and procedures include
coverage for all storage of cardholder data

- Verify that policies and procedures include a
programmatic (automatic) process to remove, at
least on a quarterly basis, stored cardholder data
that exceeds business retention requirements, or,
alternatively, requirements for a review, conducted
at least on a quarterly basis, to verify that stored
cardholder data does not exceed business
retention requirements',
  ),
  31 => 
  array (
    'fkContext' => '6327',
    'fkSectionBestPractice' => '6324',
    'nControlType' => '1',
    'sName' => '3.2',
    'tDescription' => 'Do not store sensitive
authentication data after authorization
(even if encrypted).
Sensitive authentication data includes the
data as cited in the Requirements
3.2.1 through 3.2.3.',
    'tImplementationGuide' => '3.2 If sensitive authentication data is received and
deleted, obtain and review the processes for deleting the
data to verify that the data is unrecoverable.
',
  ),
  32 => 
  array (
    'fkContext' => '6329',
    'fkSectionBestPractice' => '6324',
    'nControlType' => '1',
    'sName' => '3.2.1',
    'tDescription' => 'Do not store the full contents of
any track from the magnetic stripe
(located on the back of a card, contained
in a chip, or elsewhere). This data is
alternatively called full track, track, track
1, track 2, and magnetic-stripe data.
Note: In the normal course of business,
the following data elements from the
magnetic stripe may need to be retained:
- The cardholder’s name,
- Primary account number (PAN),
- Expiration date, and
- Service code
To minimize risk, store only these data
elements as needed for business.
Note: See PCI DSS Glossary of Terms,
Abbreviations, and Acronyms for
additional information.',
    'tImplementationGuide' => '3.2.1 For a sample of system components, examine the
following and verify that the full contents of any track from
the magnetic stripe on the back of card are not stored
under any circumstance:
- Incoming transaction data
- All logs (for example, transaction, history,
debugging, error)
- History files
- Trace files
- Several database schemas
- Database contents',
  ),
  33 => 
  array (
    'fkContext' => '6331',
    'fkSectionBestPractice' => '6324',
    'nControlType' => '1',
    'sName' => '3.2.2',
    'tDescription' => 'Do not store the cardverification
code or value (threedigit
or four-digit number printed on
the front or back of a payment
card) used to verify card-notpresent
transactions.
Note: See PCI DSS Glossary of
Terms, Abbreviations, and
Acronyms for additional
information.',
    'tImplementationGuide' => '3.2.2 For a sample of system components, verify that
the three-digit or four-digit card-verification code or value
printed on the front of the card or the signature panel
(CVV2, CVC2, CID, CAV2 data) is not stored under any
circumstance:
- Incoming transaction data
- All logs (for example, transaction, history,
debugging, error)
- History files
- Trace files
- Several database schemas
- Database contents',
  ),
  34 => 
  array (
    'fkContext' => '6333',
    'fkSectionBestPractice' => '6324',
    'nControlType' => '1',
    'sName' => '3.2.3',
    'tDescription' => 'Do not store the personal
identification number (PIN) or the
encrypted PIN block.',
    'tImplementationGuide' => '3.2.3 For a sample of system components, examine the
following and verify that PINs and encrypted PIN blocks
are not stored under any circumstance:
- Incoming transaction data
- All logs (for example, transaction, history,
debugging, error)
- History files
- Trace files
- Several database schemas
- Database contents',
  ),
  35 => 
  array (
    'fkContext' => '6335',
    'fkSectionBestPractice' => '6324',
    'nControlType' => '1',
    'sName' => '3.3',
    'tDescription' => 'Mask PAN when displayed
(the first six and last four digits are
the maximum number of digits to be
displayed).
Notes:
􀂃 This requirement does not apply to
employees and other parties with
a legitimate business need to see
the full PAN.
􀂃 This requirement does not
supersede stricter requirements in
place for displays of cardholder
data—for example, for point-ofsale
(POS) receipts.',
    'tImplementationGuide' => '3.3 Obtain and examine written policies and examine
displays of PAN (for example, on screen, on paper receipts)
to verify that primary account numbers (PANs) are masked
when displaying cardholder data, except for those with a
legitimate business need to see full PAN.',
  ),
  36 => 
  array (
    'fkContext' => '6337',
    'fkSectionBestPractice' => '6324',
    'nControlType' => '1',
    'sName' => '3.4',
    'tDescription' => 'Render PAN, at minimum,
unreadable anywhere it is stored
(including on portable digital media,
backup media, in logs) by using any
of the following approaches:
- One-way hashes based on
strong cryptography
- Truncation
- Index tokens and pads (pads
must be securely stored)
- Strong cryptography with
associated key-management
processes and procedures
The MINIMUM account information
that must be rendered unreadable is
the PAN.
Notes:
- If for some reason, a company is
unable render the PAN
unreadable, refer to Appendix B:
Compensating Controls.
- “Strong cryptography” is defined
in the PCI DSS Glossary of
Terms, Abbreviations, and
Acronyms.',
    'tImplementationGuide' => '3.4.a Obtain and examine documentation about the system used to protect the PAN, including the vendor, type of system/process, and the encryption algorithms (if applicable). Verify that the PAN is rendered unreadable using one of the following methods:
- One-way hashes based on strong cryptography
- Truncation
- Index tokens and pads, with the pads being
securely stored
- Strong cryptography, with associated keymanagement
processes and procedures
3.4.b Examine several tables or files from a sample of
data repositories to verify the PAN is rendered unreadable
(that is, not stored in plain-text).
3.4.c Examine a sample of removable media (for
example, back-up tapes) to confirm that the PAN is
rendered unreadable.
3.4.d Examine a sample of audit logs to confirm that the
PAN is sanitized or removed from the logs.',
  ),
  37 => 
  array (
    'fkContext' => '6339',
    'fkSectionBestPractice' => '6324',
    'nControlType' => '1',
    'sName' => '3.4.1',
    'tDescription' => 'If disk encryption is used
(rather than file- or column-level
database encryption), logical
access must be managed
independently of native operating
system access control
mechanisms (for example, by not
using local user account
databases). Decryption keys must not be tied to user accounts.',
    'tImplementationGuide' => '3.4.1.a If disk encryption is used, verify that logical
access to encrypted file systems is implemented via a
mechanism that is separate from the native operating
systems mechanism (for example, not using local user
account databases).
3.4.1.b Verify that cryptographic keys are stored
securely (for example, stored on removable media that is
adequately protected with strong access controls).
3.4.1.c Verify that cardholder data on removable media
is encrypted wherever stored.
Note: Disk encryption often cannot encrypt removable
media, so data stored on this media will need to be
encrypted separately.',
  ),
  38 => 
  array (
    'fkContext' => '6341',
    'fkSectionBestPractice' => '6324',
    'nControlType' => '1',
    'sName' => '3.5',
    'tDescription' => 'Protect cryptographic keys
used for encryption of cardholder
data against both disclosure and
misuse',
    'tImplementationGuide' => '3.5 Verify processes to protect keys used for encryption of
cardholder data against disclosure and misuse',
  ),
  39 => 
  array (
    'fkContext' => '6343',
    'fkSectionBestPractice' => '6324',
    'nControlType' => '1',
    'sName' => '3.5.1',
    'tDescription' => 'Restrict access to cryptographic keys to the fewest
number of custodians necessary.',
    'tImplementationGuide' => '3.5.1 Examine user access lists to verify that access to
keys is restricted to very few custodians.',
  ),
  40 => 
  array (
    'fkContext' => '6345',
    'fkSectionBestPractice' => '6324',
    'nControlType' => '1',
    'sName' => '3.5.2',
    'tDescription' => 'Store cryptographic keys
securely in the fewest possible
locations and forms.',
    'tImplementationGuide' => '3.5.2 Examine system configuration files to verify that
keys are stored in encrypted format and that keyencrypting
keys are stored separately from dataencrypting
keys.',
  ),
  41 => 
  array (
    'fkContext' => '6347',
    'fkSectionBestPractice' => '6324',
    'nControlType' => '1',
    'sName' => '3.6',
    'tDescription' => 'Fully document and
implement all key-management
processes and procedures for
cryptographic keys used for
encryption of cardholder data',
    'tImplementationGuide' => '3.6.a Verify the existence of key-management procedures
for keys used for encryption of cardholder data.
Note: Numerous industry standards for key management
are available from various resources including NIST, which
can be found at http://csrc.nist.gov.
3.6.b For service providers only: If the service provider
shares keys with their customers for transmission of
cardholder data, verify that the service provider provides
documentation to customers that includes guidance on how
to securely store and change customer’s keys (used to
transmit data between customer and service provider).
3.6.c Examine the key-management procedures...',
  ),
  42 => 
  array (
    'fkContext' => '6349',
    'fkSectionBestPractice' => '6324',
    'nControlType' => '1',
    'sName' => '3.6.1',
    'tDescription' => 'Generation of strong
cryptographic keys',
    'tImplementationGuide' => '3.6.1 Verify that key-management procedures are
implemented to require the generation of strong keys.',
  ),
  43 => 
  array (
    'fkContext' => '6351',
    'fkSectionBestPractice' => '6324',
    'nControlType' => '1',
    'sName' => '3.6.2',
    'tDescription' => 'Secure cryptographic key
distribution',
    'tImplementationGuide' => '3.6.2 Verify that key-management procedures are
implemented to require secure key distribution.',
  ),
  44 => 
  array (
    'fkContext' => '6353',
    'fkSectionBestPractice' => '6324',
    'nControlType' => '1',
    'sName' => '3.6.3',
    'tDescription' => 'Secure cryptographic key
storage',
    'tImplementationGuide' => '3.6.3 Verify that key-management procedures are
implemented to require secure key storage.',
  ),
  45 => 
  array (
    'fkContext' => '6355',
    'fkSectionBestPractice' => '6324',
    'nControlType' => '1',
    'sName' => '3.6.4',
    'tDescription' => 'Periodic cryptographic key
changes
- As deemed necessary and
recommended by the
associated application (for
example, re-keying);
preferably automatically
- At least annually',
    'tImplementationGuide' => '3.6.4 Verify that key-management procedures are
implemented to require periodic key changes at least
annually.',
  ),
  46 => 
  array (
    'fkContext' => '6357',
    'fkSectionBestPractice' => '6324',
    'nControlType' => '1',
    'sName' => '3.6.5',
    'tDescription' => 'Retirement or replacement
of old or suspected compromised
cryptographic keys',
    'tImplementationGuide' => '3.6.5.a Verify that key-management procedures are
implemented to require the retirement of old keys (for
example: archiving, destruction, and revocation as
applicable).
3.6.5.b Verify that the key-management procedures are
implemented to require the replacement of known or
suspected compromised keys.',
  ),
  47 => 
  array (
    'fkContext' => '6359',
    'fkSectionBestPractice' => '6324',
    'nControlType' => '1',
    'sName' => '3.6.6',
    'tDescription' => 'Split knowledge and
establishment of dual control of
cryptographic keys',
    'tImplementationGuide' => '3.6.6 Verify that key-management procedures are
implemented to require split knowledge and dual control of
keys (for example, requiring two or three people, each
knowing only their own part of the key, to reconstruct the
whole key).',
  ),
  48 => 
  array (
    'fkContext' => '6361',
    'fkSectionBestPractice' => '6324',
    'nControlType' => '1',
    'sName' => '3.6.7',
    'tDescription' => 'Prevention of unauthorized
substitution of cryptographic keys',
    'tImplementationGuide' => '3.6.7 Verify that key-management procedures are
implemented to require the prevention of unauthorized
substitution of keys.',
  ),
  49 => 
  array (
    'fkContext' => '6363',
    'fkSectionBestPractice' => '6324',
    'nControlType' => '1',
    'sName' => '3.6.8',
    'tDescription' => 'Requirement for
cryptographic key custodians to
sign a form stating that they
understand and accept their keycustodian
responsibilities',
    'tImplementationGuide' => '3.6.8 Verify that key-management procedures are
implemented to require key custodians to sign a form
specifying that they understand and accept their keycustodian
responsibilities.',
  ),
  50 => 
  array (
    'fkContext' => '6366',
    'fkSectionBestPractice' => '6365',
    'nControlType' => '1',
    'sName' => '4.1',
    'tDescription' => 'Use strong cryptography and
security protocols such as SSL/TLS or
IPSEC to safeguard sensitive cardholder
data during transmission over open,
public networks.
Examples of open, public networks that
are in scope of the PCI DSS are:
- The Internet,
- Wireless technologies,
- Global System for Mobile
communications (GSM), and
- General Packet Radio Service
(GPRS).',
    'tImplementationGuide' => '4.1.a Verify the use of encryption (for example, SSL/TLS
or IPSEC) wherever cardholder data is transmitted or
received over open, public networks
- Verify that strong encryption is used during data
transmission
- For SSL implementations:
   -- Verify that the server supports the latest
patched versions.
   -- Verify that HTTPS appears as a part of the
browser Universal Record Locator (URL).
   -- Verify that no cardholder data is required when
HTTPS does not appear in the URL.
- Select a sample of transactions as they are received
and observe transactions as they occur to verify that
cardholder data is encrypted during transit.
- Verify that only trusted SSL/TLS keys/certificates are
accepted.
- Verify that the proper encryption strength is
implemented for the encryption methodology in use.
(Check vendor recommendations/best practices.)',
  ),
  51 => 
  array (
    'fkContext' => '6368',
    'fkSectionBestPractice' => '6365',
    'nControlType' => '1',
    'sName' => '4.1.1',
    'tDescription' => 'Ensure wireless networks
transmitting cardholder data or
connected to the cardholder data
environment, use industry best
practices (for example, IEEE 802.11i)
to implement strong encryption for
authentication and transmission.
- For new wireless implementations,
it is prohibited to implement WEP
after March 31, 2009.
- For current wireless
implementations, it is prohibited to
use WEP after June 30, 2010.',
    'tImplementationGuide' => '4.1.1 For wireless networks transmitting cardholder data
or connected to the cardholder data environment, verify
that industry best practices (for example, IEEE 802.11i)
are used to implement strong encryption for
authentication and transmission.',
  ),
  52 => 
  array (
    'fkContext' => '6370',
    'fkSectionBestPractice' => '6365',
    'nControlType' => '1',
    'sName' => '4.2',
    'tDescription' => 'Never send unencrypted PANs by
end-user messaging technologies (for
example, e-mail, instant messaging,
chat).',
    'tImplementationGuide' => '4.2.a Verify that strong cryptography is used whenever
cardholder data is sent via end-user messaging
technologies.
4.2.b Verify the existence of a policy stating that
unencrypted PANs are not to be sent via end-user
messaging technologies.',
  ),
  53 => 
  array (
    'fkContext' => '6373',
    'fkSectionBestPractice' => '6372',
    'nControlType' => '1',
    'sName' => '5.1',
    'tDescription' => 'Deploy anti-virus software on all
systems commonly affected by
malicious software (particularly personal
computers and servers).',
    'tImplementationGuide' => '5.1 For a sample of system components including all
operating system types commonly affected by malicious
software, verify that anti-virus software is deployed if
applicable anti-virus technology exists.',
  ),
  54 => 
  array (
    'fkContext' => '6375',
    'fkSectionBestPractice' => '6372',
    'nControlType' => '1',
    'sName' => '5.1.1',
    'tDescription' => 'Ensure that all anti-virus
programs are capable of detecting,
removing, and protecting against all
known types of malicious software.',
    'tImplementationGuide' => '5.1.1 For a sample of system components, verify that all
anti-virus programs detect, remove, and protect against all
known types of malicious software (for example, viruses,
Trojans, worms, spyware, adware, and rootkits).',
  ),
  55 => 
  array (
    'fkContext' => '6377',
    'fkSectionBestPractice' => '6372',
    'nControlType' => '1',
    'sName' => '5.2',
    'tDescription' => '5.2 Ensure that all anti-virus
mechanisms are current, actively
running, and capable of generating audit
logs.',
    'tImplementationGuide' => '5.2 Verify that all anti-virus software is current, actively
running, and capable of generating logs by performing the
following:
5.2.a Obtain and examine the policy and verify that it
requires updating of anti-virus software and definitions.
5.2.b Verify that the master installation of the software
is enabled for automatic updates and periodic scans.
5.2.c For a sample of system components including all
operating system types commonly affected by
malicious software, verify that automatic updates and
periodic scans are enabled.
5.2d For a sample of system components, verify that
antivirus software log generation is enabled and that
such logs are retained in accordance with PCI DSS
Requirement 10.7',
  ),
  56 => 
  array (
    'fkContext' => '6380',
    'fkSectionBestPractice' => '6379',
    'nControlType' => '1',
    'sName' => '6.1',
    'tDescription' => 'Ensure that all system
components and software have the latest
vendor-supplied security patches
installed. Install critical security patches
within one month of release.
Note: An organization may consider
applying a risk-based approach to
prioritize their patch installations. For
example, by prioritizing critical
infrastructure (for example, public-facing
devices and systems, databases) higher
than less-critical internal devices, to
ensure high-priority systems and devices
are addressed within one month, and
addressing less critical devices and
systems within three months.',
    'tImplementationGuide' => '6.1.a For a sample of system components and related
software, compare the list of security patches installed on
each system to the most recent vendor security patch list,
to verify that current vendor patches are installed.
6.1.b Examine policies related to security patch
installation to verify they require installation of all critical
new security patches within one month.',
  ),
  57 => 
  array (
    'fkContext' => '6382',
    'fkSectionBestPractice' => '6379',
    'nControlType' => '1',
    'sName' => '6.2',
    'tDescription' => '6.2 Establish a process to identify
newly discovered security vulnerabilities
(for example, subscribe to alert services
freely available on the Internet). Update
configuration standards as required by
PCI DSS Requirement 2.2 to address new
vulnerability issues.',
    'tImplementationGuide' => '6.2.a Interview responsible personnel to verify that
processes are implemented to identify new security
vulnerabilities.
6.2.b Verify that processes to identify new security
vulnerabilities include using outside sources for security
vulnerability information and updating the system
configuration standards reviewed in Requirement 2.2 as
new vulnerability issues are found.',
  ),
  58 => 
  array (
    'fkContext' => '6384',
    'fkSectionBestPractice' => '6379',
    'nControlType' => '1',
    'sName' => '6.3',
    'tDescription' => 'Develop software applications in
accordance with PCI DSS (for example,
secure authentication and logging) and
based on industry best practices, and
incorporate information security
throughout the software development life
cycle.',
    'tImplementationGuide' => '6.3.a Obtain and examine written software development
processes to verify that the processes are based on
industry standards, security is included throughout the life
cycle, and software applications are developed in
accordance with PCI DSS.
6.3.b From an examination of written software
development processes, interviews of software developers,
and examination of relevant data (network configuration
documentation, production and test data, etc.)',
  ),
  59 => 
  array (
    'fkContext' => '6386',
    'fkSectionBestPractice' => '6379',
    'nControlType' => '1',
    'sName' => '6.3.1',
    'tDescription' => 'Testing of all security patches,
and system and software configuration
changes before deployment',
    'tImplementationGuide' => '6.3.1 All changes (including patches) are tested before
being deployed into production.',
  ),
  60 => 
  array (
    'fkContext' => '6388',
    'fkSectionBestPractice' => '6379',
    'nControlType' => '1',
    'sName' => '6.3.1.1',
    'tDescription' => 'Validation of all input (to
prevent cross-site scripting, injection
flaws, malicious file execution, etc.)',
    'tImplementationGuide' => '6.3.1.1 Validation of all input (to prevent cross-site
scripting, injection flaws, malicious file execution,
etc.)',
  ),
  61 => 
  array (
    'fkContext' => '6390',
    'fkSectionBestPractice' => '6379',
    'nControlType' => '1',
    'sName' => '6.3.1.2',
    'tDescription' => 'Validation of proper error
handling',
    'tImplementationGuide' => '6.3.1.2 Validation of proper error handling',
  ),
  62 => 
  array (
    'fkContext' => '6392',
    'fkSectionBestPractice' => '6379',
    'nControlType' => '1',
    'sName' => '6.3.1.3',
    'tDescription' => 'Validation of secure
cryptographic storage',
    'tImplementationGuide' => '6.3.1.3 Validation of secure cryptographic storage',
  ),
  63 => 
  array (
    'fkContext' => '6394',
    'fkSectionBestPractice' => '6379',
    'nControlType' => '1',
    'sName' => '6.3.1.4',
    'tDescription' => 'Validation of secure
communications',
    'tImplementationGuide' => '6.3.1.4 Validation of secure communications',
  ),
  64 => 
  array (
    'fkContext' => '6396',
    'fkSectionBestPractice' => '6379',
    'nControlType' => '1',
    'sName' => '6.3.1.5',
    'tDescription' => 'Validation of proper rolebased
access control (RBAC)',
    'tImplementationGuide' => '6.3.1.5 Validation of proper role-based access
control (RBAC)',
  ),
  65 => 
  array (
    'fkContext' => '6398',
    'fkSectionBestPractice' => '6379',
    'nControlType' => '1',
    'sName' => '6.3.2',
    'tDescription' => 'Separate development/test and
production environments',
    'tImplementationGuide' => '6.3.2 The development/test environments are separate
from the production environment, with access control in
place to enforce the separation.',
  ),
  66 => 
  array (
    'fkContext' => '6400',
    'fkSectionBestPractice' => '6379',
    'nControlType' => '1',
    'sName' => '6.3.3',
    'tDescription' => 'Separation of duties between
development/test and production
environments',
    'tImplementationGuide' => '6.3.3 There is a separation of duties between
personnel assigned to the development/test
environments and those assigned to the production
environment.',
  ),
  67 => 
  array (
    'fkContext' => '6402',
    'fkSectionBestPractice' => '6379',
    'nControlType' => '1',
    'sName' => '6.3.4',
    'tDescription' => 'Production data (live PANs) are
not used for testing or development',
    'tImplementationGuide' => '6.3.4 Production data (live PANs) are not used for
testing and development, or are sanitized before use.',
  ),
  68 => 
  array (
    'fkContext' => '6404',
    'fkSectionBestPractice' => '6379',
    'nControlType' => '1',
    'sName' => '6.3.5',
    'tDescription' => 'Removal of test data and
accounts before production systems
become active',
    'tImplementationGuide' => '6.3.5 Test data and accounts are removed before a
production system becomes active.',
  ),
  69 => 
  array (
    'fkContext' => '6406',
    'fkSectionBestPractice' => '6379',
    'nControlType' => '1',
    'sName' => '6.3.6',
    'tDescription' => 'Removal of custom application
accounts, user IDs, and passwords
before applications become active or are
released to customers',
    'tImplementationGuide' => '6.3.6 Custom application accounts, user IDs and/or
passwords are removed before system goes into
production or is released to customers.',
  ),
  70 => 
  array (
    'fkContext' => '6408',
    'fkSectionBestPractice' => '6379',
    'nControlType' => '1',
    'sName' => '6.3.7',
    'tDescription' => 'Review of custom code prior to
release to production or customers in
order to identify any potential coding
vulnerability
Note: This requirement for code reviews
applies to all custom code (both internal
and public-facing), as part of the system
development life cycle required by PCI
DSS Requirement 6.3. Code reviews can
be conducted by knowledgeable internal
personnel or third parties. Web
applications are also subject to additional
controls, if they are public facing, to
address ongoing threats and
vulnerabilities after implementation, as
defined at PCI DSS Requirement 6.6.',
    'tImplementationGuide' => '6.3.7.a Obtain and review policies to confirm all
custom application code changes for internal applications
must be reviewed (either using manual or automated
processes), as follows:
- Code changes are reviewed by individuals other
then the originating code author, and by
individuals who are knowledgeable in code review
techniques and secure coding practices.
- Appropriate corrections are implemented prior to
release.
- Code review results are reviewed and approved
by management prior to release.
6.3.7.b Obtain and review policies to confirm that all
custom application code changes for web applications
must be reviewed (using either manual or automated
processes) as follows:
- Code changes are reviewed by individuals other
then the originating code author, and by
individuals who are knowledgeable in code review
techniques and secure coding practices.
- Code reviews ensure code is developed according
to secure coding guidelines such as the Open
Web Security Project Guide (see PCI DSS
Requirement 6.5).
- Appropriate corrections are implemented prior to
release.
- Code review results are reviewed and approved
by management prior to release.
6.3.7.c Select a sample of recent custom application
changes and verify that custom application code is
reviewed according to 6.3.7a and 6.3.7b above.',
  ),
  71 => 
  array (
    'fkContext' => '6410',
    'fkSectionBestPractice' => '6379',
    'nControlType' => '1',
    'sName' => '6.4',
    'tDescription' => '6.4 Follow change control
procedures for all changes to system
components.',
    'tImplementationGuide' => '6.4.a Obtain and examine company change-control
procedures related to implementing security patches and
software modifications, and verify that the procedures
require items 6.4.1 – 6.4.4 below.
6.4.b For a sample of system components and recent
changes/security patches, trace those changes back to
related change control documentation.',
  ),
  72 => 
  array (
    'fkContext' => '6412',
    'fkSectionBestPractice' => '6379',
    'nControlType' => '1',
    'sName' => '6.4.1',
    'tDescription' => 'Documentation of impact',
    'tImplementationGuide' => '6.4.1 Verify that documentation of customer impact is
included in the change control documentation for each
sampled change.',
  ),
  73 => 
  array (
    'fkContext' => '6414',
    'fkSectionBestPractice' => '6379',
    'nControlType' => '1',
    'sName' => '6.4.2',
    'tDescription' => 'Management sign-off by
appropriate parties',
    'tImplementationGuide' => '6.4.2 Verify that management sign-off by appropriate
parties is present for each sampled change.',
  ),
  74 => 
  array (
    'fkContext' => '6416',
    'fkSectionBestPractice' => '6379',
    'nControlType' => '1',
    'sName' => '6.4.3',
    'tDescription' => 'Testing of operational
functionality',
    'tImplementationGuide' => '6.4.3 Verify that operational functionality testing is
performed for each sampled change.',
  ),
  75 => 
  array (
    'fkContext' => '6418',
    'fkSectionBestPractice' => '6379',
    'nControlType' => '1',
    'sName' => '6.4.4',
    'tDescription' => 'Back-out procedures',
    'tImplementationGuide' => '6.4.4 Verify that back-out procedures are prepared for
each sampled change',
  ),
  76 => 
  array (
    'fkContext' => '6420',
    'fkSectionBestPractice' => '6379',
    'nControlType' => '1',
    'sName' => '6.5',
    'tDescription' => 'Develop all web applications
(internal and external, and including web
administrative access to application)
based on secure coding guidelines such
as the Open Web Application Security
Project Guide. Cover prevention of
common coding vulnerabilities in
software development processes, to
include the following:
Note: The vulnerabilities listed at 6.5.1
through 6.5.10 were current in the
OWASP guide when PCI DSS v1.2 was
published. However, if and when the
OWASP guide is updated, the current
version must be used for these
requirements.',
    'tImplementationGuide' => '6.5.a Obtain and review software development
processes for any web-based applications. Verify that
processes require training in secure coding techniques for
developers, and are based on guidance such as the
OWASP guide (http://www.owasp.org).
6.5.b Interview a sample of developers and obtain
evidence that they are knowledgeable in secure coding
techniques.
6.5.c Verify that processes are in place to ensure that
web applications are not vulnerable to 6.5.X',
  ),
  77 => 
  array (
    'fkContext' => '6422',
    'fkSectionBestPractice' => '6379',
    'nControlType' => '1',
    'sName' => '6.5.1',
    'tDescription' => 'Cross-site scripting (XSS)',
    'tImplementationGuide' => '6.5.1 Cross-site scripting (XSS) (Validate all
parameters before inclusion.)',
  ),
  78 => 
  array (
    'fkContext' => '6424',
    'fkSectionBestPractice' => '6379',
    'nControlType' => '1',
    'sName' => '6.5.2',
    'tDescription' => 'Injection flaws, particularly
SQL injection. Also consider LDAP
and Xpath injection flaws as well as
other injection flaws.',
    'tImplementationGuide' => '6.5.2 Injection flaws, particularly SQL injection
(Validate input to verify user data cannot modify meaning
of commands and queries.)',
  ),
  79 => 
  array (
    'fkContext' => '6426',
    'fkSectionBestPractice' => '6379',
    'nControlType' => '1',
    'sName' => '6.5.3',
    'tDescription' => 'Malicious file execution',
    'tImplementationGuide' => '6.5.3 Malicious file execution (Validate input to verify
application does not accept filenames or files from
users.)',
  ),
  80 => 
  array (
    'fkContext' => '6428',
    'fkSectionBestPractice' => '6379',
    'nControlType' => '1',
    'sName' => '6.5.4',
    'tDescription' => 'Insecure direct object
references',
    'tImplementationGuide' => '6.5.4 Insecure direct object references (Do not expose
internal object references to users.)',
  ),
  81 => 
  array (
    'fkContext' => '6430',
    'fkSectionBestPractice' => '6379',
    'nControlType' => '1',
    'sName' => '6.5.5',
    'tDescription' => 'Cross-site request forgery
(CSRF)',
    'tImplementationGuide' => '6.5.5 Cross-site request forgery (CSRF) (Do not reply
on authorization credentials and tokens automatically
submitted by browsers.)',
  ),
  82 => 
  array (
    'fkContext' => '6432',
    'fkSectionBestPractice' => '6379',
    'nControlType' => '1',
    'sName' => '6.5.6',
    'tDescription' => 'Information leakage and
improper error handling',
    'tImplementationGuide' => '6.5.6 Information leakage and improper error handling
(Do not leak information via error messages or other
means.)',
  ),
  83 => 
  array (
    'fkContext' => '6434',
    'fkSectionBestPractice' => '6379',
    'nControlType' => '1',
    'sName' => '6.5.7',
    'tDescription' => 'Broken authentication and
session management',
    'tImplementationGuide' => '6.5.7 Broken authentication and session management
(Properly authenticate users and protect account
credentials and session tokens.)',
  ),
  84 => 
  array (
    'fkContext' => '6436',
    'fkSectionBestPractice' => '6379',
    'nControlType' => '1',
    'sName' => '6.5.8',
    'tDescription' => 'Insecure cryptographic
storage',
    'tImplementationGuide' => '6.5.8 Insecure cryptographic storage (Prevent
cryptographic flaws.)',
  ),
  85 => 
  array (
    'fkContext' => '6438',
    'fkSectionBestPractice' => '6379',
    'nControlType' => '1',
    'sName' => '6.5.9',
    'tDescription' => 'Insecure communications',
    'tImplementationGuide' => '6.5.9 Insecure communications (Properly encrypt all
authenticated and sensitive communications.)',
  ),
  86 => 
  array (
    'fkContext' => '6440',
    'fkSectionBestPractice' => '6379',
    'nControlType' => '1',
    'sName' => '6.5.10',
    'tDescription' => 'Failure to restrict URL access',
    'tImplementationGuide' => '6.5.10 Failure to restrict URL access (Consistently
enforce access control in presentation layer and business
logic for all URLs.)',
  ),
  87 => 
  array (
    'fkContext' => '6442',
    'fkSectionBestPractice' => '6379',
    'nControlType' => '1',
    'sName' => '6.6',
    'tDescription' => 'For public-facing web
applications, address new threats and
vulnerabilities on an ongoing basis and
ensure these applications are protected
against known attacks by either of the
following methods:
- Reviewing public-facing web
applications via manual or
automated application vulnerability
security assessment tools or
methods, at least annually and
after any changes
- Installing a web-application firewall
in front of public-facing web
applications',
    'tImplementationGuide' => '6.6 For public-facing web applications, ensure that
either one of the following methods are in place as follows:
- Verify that public-facing web applications are
reviewed (using either manual or automated
vulnerability security assessment tools or methods),
as follows:
   -- At least annually
   -- After any changes
   -- By an organization that specializes in application
security
   -- That all vulnerabilities are corrected
   -- That the application is re-evaluated after the
corrections
- Verify that a web-application firewall is in place in
front of public-facing web applications to detect and
prevent web-based attacks.
Note: “An organization that specializes in application
security” can be either a third-party company or an
internal organization, as long as the reviewers specialize
in application security and can demonstrate
independence from the development team.',
  ),
  88 => 
  array (
    'fkContext' => '6445',
    'fkSectionBestPractice' => '6444',
    'nControlType' => '1',
    'sName' => '7.1',
    'tDescription' => 'Limit access to system
components and cardholder data to only
those individuals whose job requires
such access.',
    'tImplementationGuide' => '7.1 Obtain and examine written policy for data control,
and verify 7.1.1 to 1.1.4.',
  ),
  89 => 
  array (
    'fkContext' => '6447',
    'fkSectionBestPractice' => '6444',
    'nControlType' => '1',
    'sName' => '7.1.1',
    'tDescription' => 'Restriction of access rights to
privileged user IDs to least privileges
necessary to perform job
responsibilities',
    'tImplementationGuide' => '7.1.1 Confirm that access rights for privileged user
IDs are restricted to least privileges necessary to
perform job responsibilities.',
  ),
  90 => 
  array (
    'fkContext' => '6449',
    'fkSectionBestPractice' => '6444',
    'nControlType' => '1',
    'sName' => '7.1.2',
    'tDescription' => 'Assignment of privileges is
based on individual personnel’s job
classification and function',
    'tImplementationGuide' => '7.1.2 Confirm that privileges are assigned to
individuals based on job classification and function (also
called “role-based access control” or RBAC).',
  ),
  91 => 
  array (
    'fkContext' => '6451',
    'fkSectionBestPractice' => '6444',
    'nControlType' => '1',
    'sName' => '7.1.3',
    'tDescription' => 'Requirement for an
authorization form signed by
management that specifies required
privileges',
    'tImplementationGuide' => '7.1.3 Confirm that an authorization form is required
for all access, that it must specify required privileges,
and that it must be signed by management.',
  ),
  92 => 
  array (
    'fkContext' => '6453',
    'fkSectionBestPractice' => '6444',
    'nControlType' => '1',
    'sName' => '7.1.4',
    'tDescription' => 'Implementation of an automated
access control system',
    'tImplementationGuide' => '7.1.4 Confirm that access controls are implemented
via an automated access control system.',
  ),
  93 => 
  array (
    'fkContext' => '6455',
    'fkSectionBestPractice' => '6444',
    'nControlType' => '1',
    'sName' => '7.2',
    'tDescription' => 'Establish an access control
system for systems components with
multiple users that restricts access based
on a user’s need to know, and is set to
“deny all” unless specifically allowed.',
    'tImplementationGuide' => '7.2 Examine system settings and vendor
documentation to verify that an access control system is
implemented.',
  ),
  94 => 
  array (
    'fkContext' => '6457',
    'fkSectionBestPractice' => '6444',
    'nControlType' => '1',
    'sName' => '7.2.1',
    'tDescription' => 'Coverage of all system
components',
    'tImplementationGuide' => '7.2.1 Confirm that access control systems are in
place on all system components.',
  ),
  95 => 
  array (
    'fkContext' => '6459',
    'fkSectionBestPractice' => '6444',
    'nControlType' => '1',
    'sName' => '7.2.2',
    'tDescription' => 'Assignment of privileges to
individuals based on job classification
and function',
    'tImplementationGuide' => '7.2.2 Confirm that access control systems are
configured to enforce privileges assigned to individuals
based on job classification and function.',
  ),
  96 => 
  array (
    'fkContext' => '6461',
    'fkSectionBestPractice' => '6444',
    'nControlType' => '1',
    'sName' => '7.2.3',
    'tDescription' => 'Default “deny-all” setting',
    'tImplementationGuide' => '7.2.3 Confirm that the access control systems has a
default “deny-all” setting.

Note: Some access control systems are set by default to
“allow-all,” thereby permitting access unless/until a rule is
written to specifically deny it.',
  ),
  97 => 
  array (
    'fkContext' => '6464',
    'fkSectionBestPractice' => '6463',
    'nControlType' => '1',
    'sName' => '8.1',
    'tDescription' => 'Assign all users a unique ID
before allowing them to access system
components or cardholder data.',
    'tImplementationGuide' => '8.1 Verify that all users are assigned a unique ID for
access to system components or cardholder data.',
  ),
  98 => 
  array (
    'fkContext' => '6466',
    'fkSectionBestPractice' => '6463',
    'nControlType' => '1',
    'sName' => '8.2',
    'tDescription' => 'In addition to assigning a unique
ID, employ at least one of the following
methods to authenticate all users:
- Password or passphrase
- Two-factor authentication (for
example, token devices, smart
cards, biometrics, or public keys)',
    'tImplementationGuide' => '8.2 To verify that users are authenticated using unique
ID and additional authentication (for example, a password)
for access to the cardholder data environment, perform the
following:
- Obtain and examine documentation describing the
authentication method(s) used.
- For each type of authentication method used and for
each type of system component, observe an
authentication to verify authentication is functioning
consistent with documented authentication method(s).',
  ),
  99 => 
  array (
    'fkContext' => '6468',
    'fkSectionBestPractice' => '6463',
    'nControlType' => '1',
    'sName' => '8.3',
    'tDescription' => 'Incorporate two-factor
authentication for remote access
(network-level access originating from
outside the network) to the network by
employees, administrators, and third
parties. Use technologies such as remote
authentication and dial-in service
(RADIUS); terminal access controller
access control system (TACACS) with
tokens; or VPN (based on SSL/TLS or
IPSEC) with individual certificates.',
    'tImplementationGuide' => '8.3 To verify that two-factor authentication is
implemented for all remote network access, observe an
employee (for example, an administrator) connecting
remotely to the network and verify that both a password
and an additional authentication item (for example, smart
card, token, PIN) are required.',
  ),
  100 => 
  array (
    'fkContext' => '6470',
    'fkSectionBestPractice' => '6463',
    'nControlType' => '1',
    'sName' => '8.4',
    'tDescription' => 'Render all passwords
unreadable during transmission and
storage on all system components using
strong cryptography (defined in PCI DSS
Glossary of Terms, Abbreviations, and
Acronyms).Render all passwords
unreadable during transmission and
storage on all system components using
strong cryptography (defined in PCI DSS
Glossary of Terms, Abbreviations, and
Acronyms).',
    'tImplementationGuide' => '8.4.a For a sample of system components, examine
password files to verify that passwords are unreadable
during transmission and storage.
8.4.b For service providers only, observe password files
to verify that customer passwords are encrypted.',
  ),
  101 => 
  array (
    'fkContext' => '6472',
    'fkSectionBestPractice' => '6463',
    'nControlType' => '1',
    'sName' => '8.5',
    'tDescription' => 'Ensure proper user authentication
and password management for nonconsumer
users and administrators on all
system components',
    'tImplementationGuide' => '8.5 Review procedures and interview personnel to
verify that procedures are implemented for user
authentication and password management.',
  ),
  102 => 
  array (
    'fkContext' => '6474',
    'fkSectionBestPractice' => '6463',
    'nControlType' => '1',
    'sName' => '8.5.1',
    'tDescription' => 'Control addition, deletion, and
modification of user IDs, credentials,
and other identifier objects.',
    'tImplementationGuide' => '8.5.1.a Select a sample of user IDs, including both
administrators and general users. Verify that each user is
authorized to use the system according to company
policy by performing the following:
- Obtain and examine an authorization form for
each ID.
- Verify that the sampled user IDs are implemented
in accordance with the authorization form
(including with privileges as specified and all
signatures obtained), by tracing information from
the authorization form to the system.',
  ),
  103 => 
  array (
    'fkContext' => '6476',
    'fkSectionBestPractice' => '6463',
    'nControlType' => '1',
    'sName' => '8.5.2',
    'tDescription' => 'Verify user identity before
performing password resets.',
    'tImplementationGuide' => '8.5.2 Examine password procedures and observe
security personnel to verify that, if a user requests a
password reset by phone, e-mail, web, or other non-faceto-
face method, the user’s identity is verified before the
password is reset.',
  ),
  104 => 
  array (
    'fkContext' => '6478',
    'fkSectionBestPractice' => '6463',
    'nControlType' => '1',
    'sName' => '8.5.3',
    'tDescription' => 'Set first-time passwords to a
unique value for each user and change
immediately after the first use.',
    'tImplementationGuide' => '8.5.3 Examine password procedures and observe
security personnel to verify that first-time passwords for
new users are set to a unique value for each user and
changed after first use.',
  ),
  105 => 
  array (
    'fkContext' => '6480',
    'fkSectionBestPractice' => '6463',
    'nControlType' => '1',
    'sName' => '8.5.4',
    'tDescription' => 'Immediately revoke access for
any terminated users.',
    'tImplementationGuide' => '8.5.4 Select a sample of employees terminated in
the past six months, and review current user access lists
to verify that their IDs have been deactivated or removed.',
  ),
  106 => 
  array (
    'fkContext' => '6482',
    'fkSectionBestPractice' => '6463',
    'nControlType' => '1',
    'sName' => '8.5.5',
    'tDescription' => 'Remove/disable inactive user
accounts at least every 90 days.',
    'tImplementationGuide' => '8.5.5 Verify that inactive accounts over 90 days old are
either removed or disabled.',
  ),
  107 => 
  array (
    'fkContext' => '6484',
    'fkSectionBestPractice' => '6463',
    'nControlType' => '1',
    'sName' => '8.5.6',
    'tDescription' => 'Enable accounts used by
vendors for remote maintenance only
during the time period needed.',
    'tImplementationGuide' => '8.5.6 Verify that any accounts used by vendors to
support and maintain system components are disabled,
enabled only when needed by the vendor, and monitored
while being used.',
  ),
  108 => 
  array (
    'fkContext' => '6486',
    'fkSectionBestPractice' => '6463',
    'nControlType' => '1',
    'sName' => '8.5.7',
    'tDescription' => 'Communicate password
procedures and policies to all users
who have access to cardholder data.',
    'tImplementationGuide' => '8.5.7 Interview the users from a sample of user IDs, to
verify that they are familiar with password procedures
and policies.',
  ),
  109 => 
  array (
    'fkContext' => '6488',
    'fkSectionBestPractice' => '6463',
    'nControlType' => '1',
    'sName' => '8.5.8',
    'tDescription' => '8.5.8 Do not use group, shared, or
generic accounts and passwords.',
    'tImplementationGuide' => '8.5.8.a For a sample of system components, examine
user ID lists to verify the following
- Generic user IDs and accounts are disabled or
removed.
- Shared user IDs for system administration activities
and other critical functions do not exist.
- Shared and generic user IDs are not used to
administer any system components.
8.5.8.b Examine password policies/procedures to
verify that group and shared passwords are explicitly
prohibited.
8.5.8.c Interview system administrators to verify that
group and shared passwords are not distributed, even if
requested.',
  ),
  110 => 
  array (
    'fkContext' => '6490',
    'fkSectionBestPractice' => '6463',
    'nControlType' => '1',
    'sName' => '8.5.9',
    'tDescription' => 'Change user passwords at
least every 90 days.',
    'tImplementationGuide' => '8.5.9 For a sample of system components, obtain and
inspect system configuration settings to verify that user
password parameters are set to require users to change
passwords at least every 90 days.
For service providers only, review internal processes and
customer/user documentation to verify that customer
passwords are required to change periodically and that
customers are given guidance as to when, and under
what circumstances, passwords must change.',
  ),
  111 => 
  array (
    'fkContext' => '6492',
    'fkSectionBestPractice' => '6463',
    'nControlType' => '1',
    'sName' => '8.5.10',
    'tDescription' => 'Require a minimum password
length of at least seven characters.',
    'tImplementationGuide' => '8.5.10 For a sample of system components, obtain and
inspect system configuration settings to verify that
password parameters are set to require passwords to be
at least seven characters long.
For service providers only, review internal processes and
customer/user documentation to verify that customer
passwords are required to meet minimum length
requirements.',
  ),
  112 => 
  array (
    'fkContext' => '6494',
    'fkSectionBestPractice' => '6463',
    'nControlType' => '1',
    'sName' => '8.5.11',
    'tDescription' => 'Use passwords containing both
numeric and alphabetic characters.',
    'tImplementationGuide' => '8.5.11 For a sample of system components, obtain and
inspect system configuration settings to verify that
password parameters are set to require passwords to
contain both numeric and alphabetic characters.
For service providers only, review internal processes and
customer/user documentation to verify that customer
passwords are required to contain both numeric and
alphabetic characters.',
  ),
  113 => 
  array (
    'fkContext' => '6496',
    'fkSectionBestPractice' => '6463',
    'nControlType' => '1',
    'sName' => '8.5.12',
    'tDescription' => 'Do not allow an individual to
submit a new password that is the
same as any of the last four
passwords he or she has used.',
    'tImplementationGuide' => '8.5.12 For a sample of system components, obtain and
inspect system configuration settings to verify that
password parameters are set to require that new
passwords cannot be the same as the four previously
used passwords.
For service providers only, review internal processes and
customer/user documentation to verify that new customer
passwords cannot be the same as the previous four
passwords.',
  ),
  114 => 
  array (
    'fkContext' => '6498',
    'fkSectionBestPractice' => '6463',
    'nControlType' => '1',
    'sName' => '8.5.13',
    'tDescription' => 'Limit repeated access attempts
by locking out the user ID after not
more than six attempts.',
    'tImplementationGuide' => '8.5.13 For a sample of system components, obtain and
inspect system configuration settings to verify that
password parameters are set to require that a user’s
account is locked out after not more than six invalid logon
attempts.
For service providers only, review internal processes and
customer/user documentation to verify that customer
accounts are temporarily locked-out after not more than
six invalid access attempts.',
  ),
  115 => 
  array (
    'fkContext' => '6501',
    'fkSectionBestPractice' => '6463',
    'nControlType' => '1',
    'sName' => '8.5.14',
    'tDescription' => 'Set the lockout duration to a
minimum of 30 minutes or until
administrator enables the user ID.',
    'tImplementationGuide' => '8.5.14 For a sample of system components, obtain and
inspect system configuration settings to verify that
password parameters are set to require that once a user
account is locked out, it remains locked for a minimum of
30 minutes or until a system administrator resets the
account.',
  ),
  116 => 
  array (
    'fkContext' => '6503',
    'fkSectionBestPractice' => '6463',
    'nControlType' => '1',
    'sName' => '8.5.15',
    'tDescription' => 'If a session has been idle for
more than 15 minutes, require the
user to re-enter the password to reactivate
the terminal.',
    'tImplementationGuide' => '8.5.15 For a sample of system components, obtain and
inspect system configuration settings to verify that
system/session idle time out features have been set to
15 minutes or less.',
  ),
  117 => 
  array (
    'fkContext' => '6505',
    'fkSectionBestPractice' => '6463',
    'nControlType' => '1',
    'sName' => '8.5.16',
    'tDescription' => 'Authenticate all access to any
database containing cardholder data.
This includes access by applications,
administrators, and all other users.',
    'tImplementationGuide' => '8.5.16.a Review database and application
configuration settings and verify that user authentication
and access to databases includes the following:
- All users are authenticated prior to access.
- All user access to, user queries of, and user
actions on (for example, move, copy, delete), the
database are through programmatic methods only
(for example, through stored procedures).
- Direct access or queries to databases are restricted
to database administrators.
8.5.16.b Review database applications and the
related application IDs to verify that application IDs can
only be used by the applications (and not by individual
users or other processes).',
  ),
  118 => 
  array (
    'fkContext' => '6508',
    'fkSectionBestPractice' => '6507',
    'nControlType' => '1',
    'sName' => '9.1',
    'tDescription' => 'Use appropriate facility entry
controls to limit and monitor physical
access to systems in the cardholder data
environment.',
    'tImplementationGuide' => '9.1 Verify the existence of physical security controls for
each computer room, data center, and other physical areas
with systems in the cardholder data environment.
- Verify that access is controlled with badge readers or
other devices including authorized badges and lock
and key.
- Observe a system administrator’s attempt to log into
consoles for randomly selected systems in the
cardholder environment and verify that they are
“locked” to prevent unauthorized use.',
  ),
  119 => 
  array (
    'fkContext' => '6510',
    'fkSectionBestPractice' => '6507',
    'nControlType' => '1',
    'sName' => '9.1.1',
    'tDescription' => 'Use video cameras or other
access control mechanisms to monitor
individual physical access to sensitive
areas. Review collected data and
correlate with other entries. Store for at
least three months, unless otherwise
restricted by law.
Note: “Sensitive areas” refers to any data
center, server room or any area that
houses systems that store, process, or
transmit cardholder data. This excludes
the areas where only point-of-sale
terminals are present, such as the
cashier areas in a retail store.',
    'tImplementationGuide' => '9.1.1 Verify that video cameras or other access control
mechanisms are in place to monitor the entry/exit points
to sensitive areas. Video cameras or other mechanisms
should be protected from tampering or disabling. Verify
that video cameras or other mechanisms are monitored
and that data from cameras or other mechanisms is
stored for at least three months.',
  ),
  120 => 
  array (
    'fkContext' => '6512',
    'fkSectionBestPractice' => '6507',
    'nControlType' => '1',
    'sName' => '9.1.2',
    'tDescription' => 'Restrict physical access to
publicly accessible network jacks.',
    'tImplementationGuide' => '9.1.2 Verify by interviewing network administrators and
by observation that network jacks are enabled only when
needed by authorized employees. For example,
conference rooms used to host visitors should not have
network ports enabled with DHCP. Alternatively, verify
that visitors are escorted at all times in areas with active
network jacks.',
  ),
  121 => 
  array (
    'fkContext' => '6514',
    'fkSectionBestPractice' => '6507',
    'nControlType' => '1',
    'sName' => '9.1.3',
    'tDescription' => 'Restrict physical access to
wireless access points, gateways, and
handheld devices.',
    'tImplementationGuide' => '9.1.3 Verify that physical access to wireless access
points, gateways, and handheld devices is appropriately
restricted.',
  ),
  122 => 
  array (
    'fkContext' => '6516',
    'fkSectionBestPractice' => '6507',
    'nControlType' => '1',
    'sName' => '9.2',
    'tDescription' => 'Develop procedures to help all
personnel easily distinguish between
employees and visitors, especially in
areas where cardholder data is
accessible.
For purposes of this requirement,
“employee” refers to full-time and parttime
employees, temporary employees
and personnel, and contractors and
consultants who are “resident” on the
entity’s site. A “visitor” is defined as a
vendor, guest of an employee, service
personnel, or anyone who needs to enter
the facility for a shor',
    'tImplementationGuide' => '9.2.a Review processes and procedures for assigning
badges to employees, and visitors, and verify these
processes include the following:
- Granting new badges, changing access
requirements, and revoking terminated employee
and expired visitor badges
- Limited access to badge system
9.2.b Observe people within the facility to verify that it is
easy to distinguish between employees and visitors.',
  ),
  123 => 
  array (
    'fkContext' => '6518',
    'fkSectionBestPractice' => '6507',
    'nControlType' => '1',
    'sName' => '9.3',
    'tDescription' => 'Make sure all visitors are handled
according to 9.3.1, 9.3.2 and 9.3.3.',
    'tImplementationGuide' => 'Verify that employee/visitor controls are in place according to 9.3.1, 9.3.2 and 9.3.3.',
  ),
  124 => 
  array (
    'fkContext' => '6520',
    'fkSectionBestPractice' => '6507',
    'nControlType' => '1',
    'sName' => '9.3.1',
    'tDescription' => 'Authorized before entering
areas where cardholder data is
processed or maintained',
    'tImplementationGuide' => '9.3.1 Observe visitors to verify the use of visitor ID
badges. Attempt to gain access to the data center to
verify that a visitor ID badge does not permit unescorted
access to physical areas that store cardholder data.',
  ),
  125 => 
  array (
    'fkContext' => '6522',
    'fkSectionBestPractice' => '6507',
    'nControlType' => '1',
    'sName' => '9.3.2',
    'tDescription' => 'Given a physical token (for
example, a badge or access device)
that expires and that identifies the
visitors as non-employee',
    'tImplementationGuide' => '9.3.2 Examine employee and visitor badges to verify
that ID badges clearly distinguish employees from
visitors/outsiders and that visitor badges expire.',
  ),
  126 => 
  array (
    'fkContext' => '6524',
    'fkSectionBestPractice' => '6507',
    'nControlType' => '1',
    'sName' => '9.3.3',
    'tDescription' => 'Asked to surrender the physical
token before leaving the facility or at the
date of expiration',
    'tImplementationGuide' => '9.3.3 Observe visitors leaving the facility to verify
visitors are asked to surrender their ID badge upon
departure or expiration.',
  ),
  127 => 
  array (
    'fkContext' => '6526',
    'fkSectionBestPractice' => '6507',
    'nControlType' => '1',
    'sName' => '9.4',
    'tDescription' => 'Use a visitor log to maintain a
physical audit trail of visitor activity.
Document the visitor’s name, the firm
represented, and the employee
authorizing physical access on the log.
Retain this log for a minimum of three
months, unless otherwise restricted by
law.',
    'tImplementationGuide' => '9.4.a Verify that a visitor log is in use to record physical
access to the facility as well as for computer rooms and
data centers where cardholder data is stored or
transmitted.
9.4.b Verify that the log contains the visitor’s name, the
firm represented, and the employee authorizing physical
access, and is retained for at least three months.',
  ),
  128 => 
  array (
    'fkContext' => '6528',
    'fkSectionBestPractice' => '6507',
    'nControlType' => '1',
    'sName' => '9.5',
    'tDescription' => 'Store media back-ups in a secure
location, preferably an off-site facility,
such as an alternate or back-up site, or a
commercial storage facility. Review the
location’s security at least annually.',
    'tImplementationGuide' => '9.5 Verify that the storage location is reviewed at least
annually to determine that back-up media storage is
secure.',
  ),
  129 => 
  array (
    'fkContext' => '6530',
    'fkSectionBestPractice' => '6507',
    'nControlType' => '1',
    'sName' => '9.6',
    'tDescription' => 'Physically secure all paper and
electronic media that contain cardholder
data.',
    'tImplementationGuide' => '9.6 Verify that procedures for protecting cardholder
data include controls for physically securing paper and
electronic media (including computers, removable
electronic media, networking, and communications
hardware, telecommunication lines, paper receipts, paper
reports, and faxes).',
  ),
  130 => 
  array (
    'fkContext' => '6532',
    'fkSectionBestPractice' => '6507',
    'nControlType' => '1',
    'sName' => '9.7',
    'tDescription' => 'Maintain strict control over the
internal or external distribution of any kind
of media that contains cardholder data.',
    'tImplementationGuide' => '9.7 Verify that a policy exists to control distribution of

media containing cardholder data, and that the policy

covers all distributed media including that distributed to

individuals.',
  ),
  131 => 
  array (
    'fkContext' => '6534',
    'fkSectionBestPractice' => '6507',
    'nControlType' => '1',
    'sName' => '9.7.1',
    'tDescription' => 'Classify the media so it can be
identified as confidential.',
    'tImplementationGuide' => '9.7.1 Verify that all media is classified so that it can be
identified as “confidential.”',
  ),
  132 => 
  array (
    'fkContext' => '6536',
    'fkSectionBestPractice' => '6507',
    'nControlType' => '1',
    'sName' => '9.7.2',
    'tDescription' => 'Send the media by secured
courier or other delivery method that
can be accurately tracked.',
    'tImplementationGuide' => 'Send the media by secured
courier or other delivery method that
can be accurately tracked.',
  ),
  133 => 
  array (
    'fkContext' => '6538',
    'fkSectionBestPractice' => '6507',
    'nControlType' => '1',
    'sName' => '9.8',
    'tDescription' => 'Ensure management approves
any and all media containing cardholder
data that is moved from a secured area
(especially when media is distributed to
individuals).',
    'tImplementationGuide' => '9.8 Select a recent sample of several days of offsite
tracking logs for all media containing cardholder data, and
verify the presence in the logs of tracking details and
proper management authorization.',
  ),
  134 => 
  array (
    'fkContext' => '6540',
    'fkSectionBestPractice' => '6507',
    'nControlType' => '1',
    'sName' => '9.9',
    'tDescription' => 'Maintain strict control over the
storage and accessibility of media that
contains cardholder data.',
    'tImplementationGuide' => '9.9 Obtain and examine the policy for controlling
storage and maintenance of hardcopy and electronic media
and verify that the policy requires periodic media
inventories.',
  ),
  135 => 
  array (
    'fkContext' => '6542',
    'fkSectionBestPractice' => '6507',
    'nControlType' => '1',
    'sName' => '9.9.1',
    'tDescription' => 'Properly maintain inventory logs
of all media and conduct media
inventories at least annually.',
    'tImplementationGuide' => '9.9.1 Obtain and review the media inventory log to verify
that periodic media inventories are performed at least
annually.',
  ),
  136 => 
  array (
    'fkContext' => '6544',
    'fkSectionBestPractice' => '6507',
    'nControlType' => '1',
    'sName' => '9.10',
    'tDescription' => 'Destroy media containing
cardholder data when it is no longer
needed for business or legal reasons.',
    'tImplementationGuide' => '9.10 Obtain and examine the periodic media destruction
policy and verify that it covers all media containing
cardholder data and confirm 9.10.1 and 9.10.2.',
  ),
  137 => 
  array (
    'fkContext' => '6546',
    'fkSectionBestPractice' => '6507',
    'nControlType' => '1',
    'sName' => '9.10.1',
    'tDescription' => 'Shred, incinerate, or pulp
hardcopy materials so that cardholder
data cannot be reconstructed.',
    'tImplementationGuide' => '9.10.1.a Verify that hard-copy materials are cross-cut
shredded, incinerated, or pulped such that there is
reasonable assurance the hard-copy materials cannot be
reconstructed.
9.10.1.b Examine storage containers used for
information to be destroyed to verify that the containers
are secured. For example, verify that a “to-be-shredded”
container has a lock preventing access to its contents.',
  ),
  138 => 
  array (
    'fkContext' => '6548',
    'fkSectionBestPractice' => '6507',
    'nControlType' => '1',
    'sName' => '9.10.2',
    'tDescription' => 'Render cardholder data on
electronic media unrecoverable so that
cardholder data cannot be
reconstructed.',
    'tImplementationGuide' => '9.10.2 Verify that cardholder data on electronic media is
rendered unrecoverable via a secure wipe program in
accordance with industry-accepted standards for secure
deletion, or otherwise physically destroying the media (for
example, degaussing).',
  ),
  139 => 
  array (
    'fkContext' => '6552',
    'fkSectionBestPractice' => '6550',
    'nControlType' => '1',
    'sName' => '10.1',
    'tDescription' => 'Establish a process for linking all
access to system components (especially
access done with administrative privileges
such as root) to each individual user.',
    'tImplementationGuide' => '10.1 Verify through observation and interviewing the
system administrator, that audit trails are enabled and
active for system components.',
  ),
  140 => 
  array (
    'fkContext' => '6554',
    'fkSectionBestPractice' => '6550',
    'nControlType' => '1',
    'sName' => '10.2',
    'tDescription' => 'Implement automated audit trails
for all system components to reconstruct events from 10.2.1 to 10.2.7',
    'tImplementationGuide' => '10.2 Through interviews, examination of audit logs, and
examination of audit log settings.',
  ),
  141 => 
  array (
    'fkContext' => '6556',
    'fkSectionBestPractice' => '6550',
    'nControlType' => '1',
    'sName' => '10.2.1',
    'tDescription' => 'All individual accesses to
cardholder data',
    'tImplementationGuide' => '10.2.1 Verify all individual access to cardholder data is
logged.',
  ),
  142 => 
  array (
    'fkContext' => '6558',
    'fkSectionBestPractice' => '6550',
    'nControlType' => '1',
    'sName' => '10.2.2',
    'tDescription' => 'All actions taken by any
individual with root or administrative
privileges',
    'tImplementationGuide' => '10.2.2 Verify actions taken by any individual with root or
administrative privileges is logged.',
  ),
  143 => 
  array (
    'fkContext' => '6560',
    'fkSectionBestPractice' => '6550',
    'nControlType' => '1',
    'sName' => '10.2.3',
    'tDescription' => 'Access to all audit trails',
    'tImplementationGuide' => '10.2.3 Verify access to all audit trails is logged.',
  ),
  144 => 
  array (
    'fkContext' => '6562',
    'fkSectionBestPractice' => '6550',
    'nControlType' => '1',
    'sName' => '10.2.4',
    'tDescription' => 'Invalid logical access attempts',
    'tImplementationGuide' => '10.2.4 Verify invalid logical access attempts are logged.',
  ),
  145 => 
  array (
    'fkContext' => '6564',
    'fkSectionBestPractice' => '6550',
    'nControlType' => '1',
    'sName' => '10.2.5',
    'tDescription' => 'Use of identification and
authentication mechanisms',
    'tImplementationGuide' => '10.2.5 Verify use of identification and authentication
mechanisms is logged.',
  ),
  146 => 
  array (
    'fkContext' => '6566',
    'fkSectionBestPractice' => '6550',
    'nControlType' => '1',
    'sName' => '10.2.6',
    'tDescription' => 'Initialization of the audit logs',
    'tImplementationGuide' => '10.2.6 Verify initialization of audit logs is logged.',
  ),
  147 => 
  array (
    'fkContext' => '6568',
    'fkSectionBestPractice' => '6550',
    'nControlType' => '1',
    'sName' => '10.2.6',
    'tDescription' => 'Creation and deletion of
system-level objects',
    'tImplementationGuide' => '10.2.7 Verify creation and deletion of system level
objects are logged.',
  ),
  148 => 
  array (
    'fkContext' => '6570',
    'fkSectionBestPractice' => '6550',
    'nControlType' => '1',
    'sName' => '10.3',
    'tDescription' => 'Record at least the following audit
trail entries for all system components for
each event: 10.3.1 to 10.3.6.',
    'tImplementationGuide' => '10.3 Through interviews and observation, for each
auditable event (from 10.2), perform the following: 10.3.1 to 10.3.6.',
  ),
  149 => 
  array (
    'fkContext' => '6572',
    'fkSectionBestPractice' => '6550',
    'nControlType' => '1',
    'sName' => '10.3.1',
    'tDescription' => 'User identification',
    'tImplementationGuide' => '10.3.1 Verify user identification is included in log entries.',
  ),
  150 => 
  array (
    'fkContext' => '6574',
    'fkSectionBestPractice' => '6550',
    'nControlType' => '1',
    'sName' => '10.3.2',
    'tDescription' => 'Type of event',
    'tImplementationGuide' => '10.3.2 Verify type of event is included in log entries.',
  ),
  151 => 
  array (
    'fkContext' => '6576',
    'fkSectionBestPractice' => '6550',
    'nControlType' => '1',
    'sName' => '10.3.3',
    'tDescription' => 'Date and time',
    'tImplementationGuide' => '10.3.3 Verify date and time stamp is included in log
entries.',
  ),
  152 => 
  array (
    'fkContext' => '6578',
    'fkSectionBestPractice' => '6550',
    'nControlType' => '1',
    'sName' => '10.3.4',
    'tDescription' => 'Success or failure indication',
    'tImplementationGuide' => '10.3.4 Verify success or failure indication is included in
log entries.',
  ),
  153 => 
  array (
    'fkContext' => '6580',
    'fkSectionBestPractice' => '6550',
    'nControlType' => '1',
    'sName' => '10.3.5',
    'tDescription' => 'Origination of event',
    'tImplementationGuide' => '10.3.5 Verify origination of event is included in log
entries.',
  ),
  154 => 
  array (
    'fkContext' => '6582',
    'fkSectionBestPractice' => '6550',
    'nControlType' => '1',
    'sName' => '10.3.6',
    'tDescription' => 'Identity or name of affected
data, system component, or resource',
    'tImplementationGuide' => '10.3.6 Verify identity or name of affected data, system
component, or resources is included in log entries.',
  ),
  155 => 
  array (
    'fkContext' => '6584',
    'fkSectionBestPractice' => '6550',
    'nControlType' => '1',
    'sName' => '10.4',
    'tDescription' => 'Synchronize all critical system
clocks and times.',
    'tImplementationGuide' => '10.4 Obtain and review the process for acquiring and
distributing the correct time within the organization, as well
as the time-related system-parameter settings for a sample
of system components. Verify the following is included in
the process and implemented:
10.4.a Verify that a known, stable version of NTP
(Network Time Protocol) or similar technology, kept current
per PCI DSS Requirements 6.1 and 6.2, is used for time
synchronization.
10.4.b Verify that internal servers are not all receiving time
signals from external sources. [Two or three central time
servers within the organization receive external time
signals [directly from a special radio, GPS satellites, or
other external sources based on International Atomic Time
and UTC (formerly GMT)], peer with each other to keep
accurate time, and share the time with other internal
servers.]
10.4.c Verify that specific external hosts are designated
from which the timeservers will accept NTP time updates
(to prevent a malicious individual from changing the clock).
Optionally, those updates can be encrypted with a
symmetric key, and access control lists can be created that
specify the IP addresses of client machines that will be
provided with the NTP service (to prevent unauthorized use
of internal time servers).
See www.ntp.org for more information',
  ),
  156 => 
  array (
    'fkContext' => '6586',
    'fkSectionBestPractice' => '6550',
    'nControlType' => '1',
    'sName' => '10.5',
    'tDescription' => 'Secure audit trails so they cannot
be altered.',
    'tImplementationGuide' => '10.5 Interview system administrator and examine
permissions to verify that audit trails are secured so that
they cannot be altered...',
  ),
  157 => 
  array (
    'fkContext' => '6588',
    'fkSectionBestPractice' => '6550',
    'nControlType' => '1',
    'sName' => '10.5.1',
    'tDescription' => 'Limit viewing of audit trails to
those with a job-related need.',
    'tImplementationGuide' => '10.5.1 Verify that only individuals who have a jobrelated
need can view audit trail files.',
  ),
  158 => 
  array (
    'fkContext' => '6590',
    'fkSectionBestPractice' => '6550',
    'nControlType' => '1',
    'sName' => '10.5.2',
    'tDescription' => 'Protect audit trail files from
unauthorized modifications.',
    'tImplementationGuide' => '10.5.2 Verify that current audit trail files are protected
from unauthorized modifications via access control
mechanisms, physical segregation, and/or network
segregation.',
  ),
  159 => 
  array (
    'fkContext' => '6592',
    'fkSectionBestPractice' => '6550',
    'nControlType' => '1',
    'sName' => '10.5.3',
    'tDescription' => 'Promptly back up audit trail files
to a centralized log server or media that
is difficult to alter.',
    'tImplementationGuide' => '10.5.3 Verify that current audit trail files are promptly
backed up to a centralized log server or media that is
difficult to alter.',
  ),
  160 => 
  array (
    'fkContext' => '6594',
    'fkSectionBestPractice' => '6550',
    'nControlType' => '1',
    'sName' => '10.5.4',
    'tDescription' => 'Write logs for external-facing
technologies onto a log server on the
internal LAN.',
    'tImplementationGuide' => '10.5.4 Verify that logs for external-facing technologies
(for example, wireless, firewalls, DNS, mail) are offloaded
or copied onto a secure centralized internal log server or
media.',
  ),
  161 => 
  array (
    'fkContext' => '6596',
    'fkSectionBestPractice' => '6550',
    'nControlType' => '1',
    'sName' => '10.5.5',
    'tDescription' => 'Use file-integrity monitoring or
change-detection software on logs to
ensure that existing log data cannot be
changed without generating alerts
(although new data being added should
not cause an alert).',
    'tImplementationGuide' => '10.5.5 Verify the use of file-integrity monitoring or
change-detection software for logs by examining system
settings and monitored files and results from monitoring
activities.',
  ),
  162 => 
  array (
    'fkContext' => '6598',
    'fkSectionBestPractice' => '6550',
    'nControlType' => '1',
    'sName' => '10.6',
    'tDescription' => 'Review logs for all system
components at least daily. Log reviews
must include those servers that perform
security functions like intrusion-detection
system (IDS) and authentication,
authorization, and accounting protocol
(AAA) servers (for example, RADIUS).
Note: Log harvesting, parsing, and
alerting tools may be used to meet
compliance with Requirement 10.6',
    'tImplementationGuide' => '10.6.a Obtain and examine security policies and
procedures to verify that they include procedures to review
security logs at least daily and that follow-up to exceptions
is required.
10.6.b Through observation and interviews, verify that
regular log reviews are performed for all system
components.',
  ),
  163 => 
  array (
    'fkContext' => '6600',
    'fkSectionBestPractice' => '6550',
    'nControlType' => '1',
    'sName' => '10.7',
    'tDescription' => 'Retain audit trail history for at
least one year, with a minimum of three
months immediately available for analysis
(for example, online, archived, or
restorable from back-up).',
    'tImplementationGuide' => '10.7.a Obtain and examine security policies and
procedures and verify that they include audit log retention
policies and require audit log retention for at least one year.
10.7.b Verify that audit logs are available for at least one
year and processes are in place to restore at least the last
three months’ logs for immediate analysis.',
  ),
  164 => 
  array (
    'fkContext' => '6602',
    'fkSectionBestPractice' => '6551',
    'nControlType' => '1',
    'sName' => '11.1',
    'tDescription' => 'Test for the presence of wireless
access points by using a wireless
analyzer at least quarterly or deploying a
wireless IDS/IPS to identify all wireless
devices in use.',
    'tImplementationGuide' => '11.1.a Verify that a wireless analyzer is used at least
quarterly, or that a wireless IDS/IPS is implemented and
configured to identify all wireless devices.
11.1.b If a wireless IDS/IPS is implemented, verify the
configuration will generate alerts to personnel.
11.1 c Verify the organization’s Incident Response Plan
(Requirement 12.9) includes a response in the event
unauthorized wireless devices are detected.',
  ),
  165 => 
  array (
    'fkContext' => '6604',
    'fkSectionBestPractice' => '6551',
    'nControlType' => '1',
    'sName' => '11.2',
    'tDescription' => '11.2 Run internal and external
network vulnerability scans at least
quarterly and after any significant
change in the network (such as new
system component installations, changes
in network topology, firewall rule
modifications, product upgrades).
Note: Quarterly external vulnerability
scans must be performed by an
Approved Scanning Vendor (ASV)
qualified by Payment Card Industry
Security Standards Council (PCI SSC).
Scans conducted after network changes
may be performed by the company’s
internal staff.',
    'tImplementationGuide' => '11.2.a Inspect output from the most recent four quarters
of internal network, host, and application vulnerability
scans to verify that periodic security testing of the devices
within the cardholder data environment occurs. Verify that
the scan process includes rescans until passing results are
obtained.
Note: External scans conducted after network changes,
and internal scans, may be performed by the company’s
qualified internal personnel or third parties.

11.2.b Verify that external scanning is occurring on a
quarterly basis in accordance with the PCI Security
Scanning Procedures, by inspecting output from the four
most recent quarters of external vulnerability scans to
verify that:

- Four quarterly scans occurred in the most recent 12-
month period;

- The results of each scan satisfy the PCI Security
Scanning Procedures (for example, no urgent, critical,
or high vulnerabilities);

- The scans were completed by an Approved Scanning
Vendor (ASV) qualified by PCI SSC.

Note: It is not required that four passing quarterly scans
must be completed for initial PCI DSS compliance if the assessor verifies 1) the most recent scan result was a
passing scan, 2) the entity has documented policies and
procedures requiring quarterly scanning, and 3)
vulnerabilities noted in the scan results have been
corrected as shown in a re-scan. For subsequent years
after the initial PCI DSS review, four passing quarterly
scans must have occurred.

11.2.c Verify that internal and/or external scanning is
performed after any significant change in the network, by
inspecting scan results for the last year. Verify that the
scan process includes rescans until passing results are
obtained.',
  ),
  166 => 
  array (
    'fkContext' => '6606',
    'fkSectionBestPractice' => '6551',
    'nControlType' => '1',
    'sName' => '11.3',
    'tDescription' => 'Perform external and internal
penetration testing at least once a year
and after any significant infrastructure or
application upgrade or modification (such
as an operating system upgrade, a subnetwork
added to the environment, or a
web server added to the environment).',
    'tImplementationGuide' => '11.3.a Obtain and examine the results from the most
recent penetration test to verify that penetration testing is
performed at least annually and after any significant
changes to the environment. Verify that noted
vulnerabilities were corrected and testing repeated.
11.3.b Verify that the test was performed by a qualified
internal resource or qualified external third party, and if
applicable, organizational independence of the tester exists
(not required to be a QSA or ASV).',
  ),
  167 => 
  array (
    'fkContext' => '6608',
    'fkSectionBestPractice' => '6551',
    'nControlType' => '1',
    'sName' => '11.3.1',
    'tDescription' => 'Network-layer penetration
tests',
    'tImplementationGuide' => '11.3.1 Verify that the penetration test includes networklayer
penetration tests. These tests should include
components that support network functions as well as
operating systems.',
  ),
  168 => 
  array (
    'fkContext' => '6610',
    'fkSectionBestPractice' => '6551',
    'nControlType' => '1',
    'sName' => '11.3.2',
    'tDescription' => 'Application-layer penetration
tests',
    'tImplementationGuide' => '11.3.2 Verify that the penetration test includes
application-layer penetration tests. For web applications,
the tests should include, at a minimum, the vulnerabilities
listed in Requirement 6.5.',
  ),
  169 => 
  array (
    'fkContext' => '6614',
    'fkSectionBestPractice' => '6551',
    'nControlType' => '1',
    'sName' => '11.5',
    'tDescription' => 'Deploy file-integrity monitoring
software to alert personnel to
unauthorized modification of critical
system files, configuration files, or
content files; and configure the software
to perform critical file comparisons at
least weekly.
Note: For file-integrity monitoring
purposes, critical files are usually those
that do not regularly change, but the
modification of which could indicate a
system compromise or risk of
compromise. File-integrity monitoring
products usually come pre-configured
with critical files for the related operating
system. Other critical files, such as those
for custom applications, must be
evaluated and defined by the entity (that
is, the merchant or service provider).',
    'tImplementationGuide' => '11.5 Verify the use of file-integrity monitoring products
within the cardholder data environment by observing
system settings and monitored files, as well as reviewing
results from monitoring activities.
Examples of files that should be monitored:
- System executables
- Application executables
- Configuration and parameter files
- Centrally stored, historical or archived, log and audit
files',
  ),
  170 => 
  array (
    'fkContext' => '6612',
    'fkSectionBestPractice' => '6551',
    'nControlType' => '1',
    'sName' => '11.4',
    'tDescription' => '11.4 Use intrusion-detection systems,
and/or intrusion-prevention systems to
monitor all traffic in the cardholder data
environment and alert personnel to
suspected compromises. Keep all
intrusion-detection and prevention
engines up-to-date.',
    'tImplementationGuide' => '11.4.a Verify the use of intrusion-detection systems and/or
intrusion-prevention systems and that all traffic in the
cardholder data environment is monitored.
11.4.b Confirm IDS and/or IPS are configured to alert
personnel of suspected compromises.
11.4.c Examine IDS/IPS configurations and confirm
IDS/IPS devices are configured, maintained, and updated
per vendor instructions to ensure optimal protection.',
  ),
  171 => 
  array (
    'fkContext' => '6617',
    'fkSectionBestPractice' => '6616',
    'nControlType' => '1',
    'sName' => '12.1',
    'tDescription' => 'Establish, publish, maintain,
and disseminate a security policy',
    'tImplementationGuide' => '12.1 Examine the information security policy and verify
that the policy is published and disseminated to all relevant
system users (including vendors, contractors, and business
partners).',
  ),
  172 => 
  array (
    'fkContext' => '6619',
    'fkSectionBestPractice' => '6616',
    'nControlType' => '1',
    'sName' => '12.1.1',
    'tDescription' => 'Addresses all PCI DSS
requirements.',
    'tImplementationGuide' => '12.1.1 Verify that the policy addresses all PCI DSS
requirements.',
  ),
  173 => 
  array (
    'fkContext' => '6621',
    'fkSectionBestPractice' => '6616',
    'nControlType' => '1',
    'sName' => '12.1.2',
    'tDescription' => 'Includes an annual process
that identifies threats, and
vulnerabilities, and results in a
formal risk assessment.',
    'tImplementationGuide' => '12.1.2 Verify that the information security policy includes
an annual risk assessment process that identifies threats,
vulnerabilities, and results in a formal risk assessment.',
  ),
  174 => 
  array (
    'fkContext' => '6623',
    'fkSectionBestPractice' => '6616',
    'nControlType' => '1',
    'sName' => '12.1.3',
    'tDescription' => 'Includes a review at least
once a year and updates when the
environment changes.',
    'tImplementationGuide' => '12.1.3 Verify that the information security policy is
reviewed at least annually and updated as needed to
reflect changes to business objectives or the risk
environment.',
  ),
  175 => 
  array (
    'fkContext' => '6625',
    'fkSectionBestPractice' => '6616',
    'nControlType' => '1',
    'sName' => '12.2',
    'tDescription' => 'Develop daily operational
security procedures that are consistent
with requirements in this specification
(for example, user account
maintenance procedures, and log
review procedures).',
    'tImplementationGuide' => '12.2.a Examine the daily operational security procedures.
Verify that they are consistent with this specification, and
include administrative and technical procedures for each of
the requirements.',
  ),
  176 => 
  array (
    'fkContext' => '6627',
    'fkSectionBestPractice' => '6616',
    'nControlType' => '1',
    'sName' => '12.3',
    'tDescription' => 'Develop usage policies for
critical employee-facing technologies
(for example, remote-access
technologies, wireless technologies,
removable electronic media, laptops,
personal data/digital assistants
(PDAs), e-mail usage and Internet
usage) to define proper use of these
technologies for all employees and
contractors.',
    'tImplementationGuide' => '12.3 Obtain and examine the policy for critical
employee-facing technologies and perform the following: 12.3.1 to 12.3.10.',
  ),
  177 => 
  array (
    'fkContext' => '6629',
    'fkSectionBestPractice' => '6616',
    'nControlType' => '1',
    'sName' => '12.3.1',
    'tDescription' => 'Explicit management
approval',
    'tImplementationGuide' => '12.3.1 Verify that the usage policies require explicit
management approval to use the technologies.',
  ),
  178 => 
  array (
    'fkContext' => '6631',
    'fkSectionBestPractice' => '6616',
    'nControlType' => '1',
    'sName' => '12.3.2',
    'tDescription' => 'Authentication for use of the
technology',
    'tImplementationGuide' => '12.3.2 Verify that the usage policies require that all
technology use be authenticated with user ID and
password or other authentication item (for example,
token).',
  ),
  179 => 
  array (
    'fkContext' => '6633',
    'fkSectionBestPractice' => '6616',
    'nControlType' => '1',
    'sName' => '12.3.3',
    'tDescription' => 'A list of all such devices and
personnel with access',
    'tImplementationGuide' => '12.3.3 Verify that the usage policies require a list of all
devices and personnel authorized to use the devices.',
  ),
  180 => 
  array (
    'fkContext' => '6635',
    'fkSectionBestPractice' => '6616',
    'nControlType' => '1',
    'sName' => '12.3.4',
    'tDescription' => 'Labeling of devices with
owner, contact information, and
purpose',
    'tImplementationGuide' => '12.3.4 Verify that the usage policies require labeling of
devices with owner, contact information, and purpose.',
  ),
  181 => 
  array (
    'fkContext' => '6637',
    'fkSectionBestPractice' => '6616',
    'nControlType' => '1',
    'sName' => '12.3.5',
    'tDescription' => 'Acceptable uses of the
technology',
    'tImplementationGuide' => '12.3.5 Verify that the usage policies require acceptable
uses for the technology.',
  ),
  182 => 
  array (
    'fkContext' => '6639',
    'fkSectionBestPractice' => '6616',
    'nControlType' => '1',
    'sName' => '12.3.6',
    'tDescription' => 'Acceptable network
locations for the technologies',
    'tImplementationGuide' => '12.3.6 Verify that the usage policies require acceptable
network locations for the technology.',
  ),
  183 => 
  array (
    'fkContext' => '6641',
    'fkSectionBestPractice' => '6616',
    'nControlType' => '1',
    'sName' => '12.3.7',
    'tDescription' => 'List of company-approved
products',
    'tImplementationGuide' => '12.3.7 Verify that the usage policies require a list of
company-approved products.',
  ),
  184 => 
  array (
    'fkContext' => '6643',
    'fkSectionBestPractice' => '6616',
    'nControlType' => '1',
    'sName' => '12.3.8',
    'tDescription' => 'Automatic disconnect of
sessions for remote-access
technologies after a specific period
of inactivity',
    'tImplementationGuide' => '12.3.8 Verify that the usage policies require automatic
disconnect of sessions for remote-access technologies
after a specific period of inactivity.',
  ),
  185 => 
  array (
    'fkContext' => '6645',
    'fkSectionBestPractice' => '6616',
    'nControlType' => '1',
    'sName' => '12.3.9',
    'tDescription' => 'Activation of remote-access
technologies for vendors only when
needed by vendors, with immediate
deactivation after use',
    'tImplementationGuide' => '12.3.9 Verify that the usage policies require activation of
remote-access technologies used by vendors only when
needed by vendors, with immediate deactivation after
use.',
  ),
  186 => 
  array (
    'fkContext' => '6647',
    'fkSectionBestPractice' => '6616',
    'nControlType' => '1',
    'sName' => '12.3.10',
    'tDescription' => 'When accessing
cardholder data via remote-access
technologies, prohibit copy, move,
and storage of cardholder data onto
local hard drives and removable
electronic media.',
    'tImplementationGuide' => '12.3.10 Verify that the usage policies prohibit copying,
moving, or storing of cardholder data onto local hard
drives, and removable electronic media when accessing
such data via remote-access technologies.',
  ),
  187 => 
  array (
    'fkContext' => '6649',
    'fkSectionBestPractice' => '6616',
    'nControlType' => '1',
    'sName' => '12.4',
    'tDescription' => 'Ensure that the security policy
and procedures clearly define
information security responsibilities for
all employees and contractors.',
    'tImplementationGuide' => '12.4 Verify that information security policies clearly
define information security responsibilities for both
employees and contractors.',
  ),
  188 => 
  array (
    'fkContext' => '6651',
    'fkSectionBestPractice' => '6616',
    'nControlType' => '1',
    'sName' => '12.5',
    'tDescription' => 'Assign to an individual or team
the following information security
management responsibilities:12.5.1 to 12.5.5.',
    'tImplementationGuide' => '12.5 Verify the formal assignment of information security
to a Chief Security Officer or other security-knowledgeable
member of management. Obtain and examine information
security policies and procedures to verify that the following
information security responsibilities are specifically and
formally assigned: 12.5.1 to 12.5.5.',
  ),
  189 => 
  array (
    'fkContext' => '6653',
    'fkSectionBestPractice' => '6616',
    'nControlType' => '1',
    'sName' => '12.5.1',
    'tDescription' => 'Establish, document, and
distribute security policies and
procedures.',
    'tImplementationGuide' => '12.5.1 Verify that responsibility for creating and
distributing security policies and procedures is formally
assigned.',
  ),
  190 => 
  array (
    'fkContext' => '6655',
    'fkSectionBestPractice' => '6616',
    'nControlType' => '1',
    'sName' => '12.5.2',
    'tDescription' => 'Monitor and analyze security
alerts and information, and distribute
to appropriate personnel.',
    'tImplementationGuide' => '12.5.2 Verify that responsibility for monitoring and
analyzing security alerts and distributing information to
appropriate information security and business unit
management personnel is formally assigned.',
  ),
  191 => 
  array (
    'fkContext' => '6657',
    'fkSectionBestPractice' => '6616',
    'nControlType' => '1',
    'sName' => '12.5.3',
    'tDescription' => 'Establish, document, and
distribute security incident response
and escalation procedures to ensure
timely and effective handling of all
situations.',
    'tImplementationGuide' => '12.5.3 Verify that responsibility for creating and
distributing security incident response and escalation
procedures is formally assigned.',
  ),
  192 => 
  array (
    'fkContext' => '6659',
    'fkSectionBestPractice' => '6616',
    'nControlType' => '1',
    'sName' => '12.5.4',
    'tDescription' => 'Administer user accounts,
including additions, deletions, and
modifications',
    'tImplementationGuide' => '12.5.4 Verify that responsibility for administering user
account and authentication management is formally
assigned.',
  ),
  193 => 
  array (
    'fkContext' => '6661',
    'fkSectionBestPractice' => '6616',
    'nControlType' => '1',
    'sName' => '12.5.5',
    'tDescription' => 'Monitor and control all
access to data.',
    'tImplementationGuide' => '12.5.5 Verify that responsibility for monitoring and
controlling all access to data is formally assigned.',
  ),
  194 => 
  array (
    'fkContext' => '6663',
    'fkSectionBestPractice' => '6616',
    'nControlType' => '1',
    'sName' => '12.6',
    'tDescription' => 'Implement a formal security
awareness program to make all
employees aware of the importance of
cardholder data security.',
    'tImplementationGuide' => '12.6.a Verify the existence of a formal security
awareness program for all employees.
12.6.b Obtain and examine security awareness
program procedures and documentation and perform the
following: 12.6.1 and 12.6.2.',
  ),
  195 => 
  array (
    'fkContext' => '6665',
    'fkSectionBestPractice' => '6616',
    'nControlType' => '1',
    'sName' => '12.6.1',
    'tDescription' => 'Educate employees upon
hire and at least annually.',
    'tImplementationGuide' => '12.6.1.a Verify that the security awareness program
provides multiple methods of communicating awareness
and educating employees (for example, posters, letters,
memos, web based training, meetings, and promotions).
12.6.1.b Verify that employees attend awareness
training upon hire and at least annually.',
  ),
  196 => 
  array (
    'fkContext' => '6667',
    'fkSectionBestPractice' => '6616',
    'nControlType' => '1',
    'sName' => '12.6.2',
    'tDescription' => 'Require employees to
acknowledge at least annually that
they have read and understood the
company’s security policy and
procedures.',
    'tImplementationGuide' => '12.6.2 Verify that the security awareness program
requires employees to acknowledge (for example, in
writing or electronically) at least annually that they have
read and understand the company’s information security
policy.',
  ),
  197 => 
  array (
    'fkContext' => '6669',
    'fkSectionBestPractice' => '6616',
    'nControlType' => '1',
    'sName' => '12.7',
    'tDescription' => 'Screen potential employees
(see definition of “employee” at 9.2
above) prior to hire to minimize the
risk of attacks from internal sources.
For those employees such as store
cashiers who only have access to one
card number at a time when facilitating
a transaction, this requirement is a
recommendation only.',
    'tImplementationGuide' => '12.7 Inquire with Human Resource department
management and verify that background checks are
conducted (within the constraints of local laws) on
employees prior to hire who will have access to cardholder
data or the cardholder data environment. (Examples of
background checks include previous employment history,
criminal record, credit history, and reference checks.)',
  ),
  198 => 
  array (
    'fkContext' => '6671',
    'fkSectionBestPractice' => '6616',
    'nControlType' => '1',
    'sName' => '12.8',
    'tDescription' => 'If cardholder data is shared
with service providers, maintain and
implement policies and procedures to
manage service providers.',
    'tImplementationGuide' => '12.8 If the entity being assessed shares cardholder data
with service providers (for example, back-up tape storage
facilities, managed service providers such as Web hosting
companies or security service providers, or those that
receive data for fraud modeling purposes), through
observation, review of policies and procedures, and review
of supporting documentation...',
  ),
  199 => 
  array (
    'fkContext' => '6673',
    'fkSectionBestPractice' => '6616',
    'nControlType' => '1',
    'sName' => '12.8.1',
    'tDescription' => 'Maintain a list of service
providers.',
    'tImplementationGuide' => '12.8.1 Verify that a list of service providers is
maintained.',
  ),
  200 => 
  array (
    'fkContext' => '6675',
    'fkSectionBestPractice' => '6616',
    'nControlType' => '1',
    'sName' => '12.8.2',
    'tDescription' => 'Maintain a written
agreement that includes an
acknowledgement that the service
providers are responsible for the
security of cardholder data the
service providers possess.',
    'tImplementationGuide' => '12.8.2 Verify that the written agreement includes an
acknowledgement by the service providers of their
responsibility for securing cardholder data.',
  ),
  201 => 
  array (
    'fkContext' => '6677',
    'fkSectionBestPractice' => '6616',
    'nControlType' => '1',
    'sName' => '12.8.3',
    'tDescription' => 'Ensure there is an
established process for engaging
service providers including proper
due diligence prior to engagement.',
    'tImplementationGuide' => '12.8.3 Verify that policies and procedures are
documented and were followed including proper due
diligence prior to engaging any service provider.',
  ),
  202 => 
  array (
    'fkContext' => '6679',
    'fkSectionBestPractice' => '6616',
    'nControlType' => '1',
    'sName' => '12.8.4',
    'tDescription' => 'Maintain a program to
monitor service providers’ PCI DSS
compliance status.',
    'tImplementationGuide' => '12.8.4 Verify that the entity assessed maintains a
program to monitor its service providers’ PCI DSS
compliance status.',
  ),
  203 => 
  array (
    'fkContext' => '6681',
    'fkSectionBestPractice' => '6616',
    'nControlType' => '1',
    'sName' => '12.9',
    'tDescription' => 'Implement an incident
response plan. Be prepared to
respond immediately to a system
breach.',
    'tImplementationGuide' => '12.9 Obtain and examine the Incident Response Plan
and related procedures and perform the following: 12.9.1 to 12.9.6.',
  ),
  204 => 
  array (
    'fkContext' => '6683',
    'fkSectionBestPractice' => '6616',
    'nControlType' => '1',
    'sName' => '12.9.1',
    'tDescription' => 'Create the incident
response plan to be implemented in
the event of system breach. Ensure
the plan addresses the following, at
a minimum:
- Roles, responsibilities, and
communication and contact
strategies in the event of a
compromise including
notification of the payment
brands, at a minimum
- Specific incident response
procedures
- Business recovery and
continuity procedures
- Data back-up processes
- Analysis of legal requirements
for reporting compromises
- Coverage and responses of all
critical system components
- Reference or inclusion of
incident response procedures
from the payment brands',
    'tImplementationGuide' => '12.9.1 Verify that the Incident Response Plan includes:
- Roles, responsibilities, and communication
strategies in the event of a compromise including
notification of the payment brands, at a minimum
- Specific incident response procedures,
- Business recovery and continuity procedures,
- Data back-up processes
- Analysis of legal requirements for reporting
compromises (for example, California Bill 1386
which requires notification of affected consumers
in the event of an actual or suspected
compromise for any business with California
residents in their database)
- Coverage and responses for all critical system
components
- Reference or inclusion of incident response
procedures from the payment brands',
  ),
  205 => 
  array (
    'fkContext' => '6685',
    'fkSectionBestPractice' => '6616',
    'nControlType' => '1',
    'sName' => '12.9.2',
    'tDescription' => 'Test the plan at least
annually.',
    'tImplementationGuide' => '12.9.2 Verify that the plan is tested at least annually.',
  ),
  206 => 
  array (
    'fkContext' => '6687',
    'fkSectionBestPractice' => '6616',
    'nControlType' => '1',
    'sName' => '12.9.3',
    'tDescription' => 'Designate specific
personnel to be available on a 24/7
basis to respond to alerts.',
    'tImplementationGuide' => '12.9.3 Verify through observation and review of policies,
that there is 24/7 incident response and monitoring
coverage for any evidence of unauthorized activity,
detection of unauthorized wireless access points, critical
IDS alerts, and/or reports of unauthorized critical system
or content file changes.',
  ),
  207 => 
  array (
    'fkContext' => '6689',
    'fkSectionBestPractice' => '6616',
    'nControlType' => '1',
    'sName' => '12.9.4',
    'tDescription' => 'Provide appropriate training
to staff with security breach
response responsibilities.',
    'tImplementationGuide' => '12.9.4 Verify through observation and review of policies
that staff with security breach responsibilities are
periodically trained.',
  ),
  208 => 
  array (
    'fkContext' => '6691',
    'fkSectionBestPractice' => '6616',
    'nControlType' => '1',
    'sName' => '12.9.5',
    'tDescription' => 'Include alerts from intrusiondetection,
intrusion-prevention, and
file-integrity monitoring systems.',
    'tImplementationGuide' => '12.9.5 Verify through observation and review of
processes that monitoring and responding to alerts from
security systems including detection of unauthorized
wireless access points are covered in the Incident
Response Plan.',
  ),
  209 => 
  array (
    'fkContext' => '6693',
    'fkSectionBestPractice' => '6616',
    'nControlType' => '1',
    'sName' => '12.9.6',
    'tDescription' => 'Develop process to modify
and evolve the incident response
plan according to lessons learned
and to incorporate industry
developments.',
    'tImplementationGuide' => '12.9.6 Verify through observation and review of policies
that there is a process to modify and evolve the incident
response plan according to lessons learned and to
incorporate industry developments.',
  ),
  210 => 
  array (
    'fkContext' => '6696',
    'fkSectionBestPractice' => '6695',
    'nControlType' => '1',
    'sName' => 'A.1',
    'tDescription' => 'Protect each entity’s (that is
merchant, service provider, or other
entity) hosted environment and data,
per A.1.1 through A.1.4:
A hosting provider must fulfill these
requirements as well as all other
relevant sections of the PCI DSS.
Note: Even though a hosting provider
may meet these requirements, the
compliance of the entity that uses the
hosting provider is not guaranteed.
Each entity must comply with the PCI
DSS and validate compliance as
applicable.',
    'tImplementationGuide' => 'A.1 Specifically for a PCI DSS assessment of a shared
hosting provider, to verify that shared hosting providers
protect entities’ (merchants and service providers) hosted
environment and data, select a sample of servers
(Microsoft Windows and Unix/Linux) across a
representative sample of hosted merchants and service
providers, and perform A.1.1 through A.1.4 below.',
  ),
  211 => 
  array (
    'fkContext' => '6698',
    'fkSectionBestPractice' => '6695',
    'nControlType' => '1',
    'sName' => 'A.1.1',
    'tDescription' => 'Ensure that each entity only
runs processes that have access to
that entity’s cardholder data
environment.',
    'tImplementationGuide' => 'A.1.1 If a shared hosting provider allows entities (for
example, merchants or service providers) to run their
own applications, verify these application processes run
using the unique ID of the entity. For example:
- No entity on the system can use a shared web
server user ID.
- All CGI scripts used by an entity must be created
and run as the entity’s unique user ID.',
  ),
  212 => 
  array (
    'fkContext' => '6700',
    'fkSectionBestPractice' => '6695',
    'nControlType' => '1',
    'sName' => 'A.1.2',
    'tDescription' => 'Restrict each entity’s access
and privileges to own cardholder
data environment only.',
    'tImplementationGuide' => 'A.1.2.a Verify the user ID of any application process
is not a privileged user (root/admin).
A.1.2.b Verify each entity (merchant, service
provider) has read, write, or execute permissions only
for files and directories it owns or for necessary system
files (restricted via file system permissions, access
control lists, chroot, jailshell, etc.). IMPORTANT: An
entity’s files may not be shared by group.
A.1.2.c Verify an entity’s users do not have write
access to shared system binaries.
A.1.2.d Verify that viewing of log entries is restricted
to the owning entity.
A.1.2.e To ensure each entity cannot monopolize
server resources to exploit vulnerabilities (for example,
error, race, and restart conditions, resulting in, for
example, buffer overflows), verify restrictions are in
place for the use of these system resources:
- Disk space
- Bandwidth
- Memory
- CPU',
  ),
  213 => 
  array (
    'fkContext' => '6702',
    'fkSectionBestPractice' => '6695',
    'nControlType' => '1',
    'sName' => 'A.1.3',
    'tDescription' => 'Ensure logging and audit
trails are enabled and unique to
each entity’s cardholder data
environment and consistent with
PCI DSS Requirement 10.',
    'tImplementationGuide' => 'A.1.3.a Verify the shared hosting provider has
enabled logging as follows, for each merchant and
service provider environment:
- Logs are enabled for common third-party
applications.
- Logs are active by default.
- Logs are available for review by the owning
entity.
- Log locations are clearly communicated to the
owning entity.',
  ),
  214 => 
  array (
    'fkContext' => '6704',
    'fkSectionBestPractice' => '6695',
    'nControlType' => '1',
    'sName' => 'A.1.4',
    'tDescription' => 'Enable processes to provide
for timely forensic investigation in
the event of a compromise to any
hosted merchant or service
provider.',
    'tImplementationGuide' => 'A.1.4 Verify the shared hosting provider has written
policies that provide for a timely forensics investigation
of related servers in the event of a compromise.',
  ),
)
?>