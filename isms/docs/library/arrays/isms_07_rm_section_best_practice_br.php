<?php

$laSectionBestPracticeBR = 
array (
  0 => 
  array (
    'fkContext' => '1600',
    'fkParent' => 'null',
    'sName' => 'ISO 27001',
  ),
  1 => 
  array (
    'fkContext' => '1601',
    'fkParent' => '1600',
    'sName' => '05 Pol�tica de Seguran�a da Informa��o',
  ),
  2 => 
  array (
    'fkContext' => '1602',
    'fkParent' => '1601',
    'sName' => '5.1 Pol�tica de Seguran�a da Informa��o',
  ),
  3 => 
  array (
    'fkContext' => '1603',
    'fkParent' => '1600',
    'sName' => '06 Organizando a seguran�a da informa��o',
  ),
  4 => 
  array (
    'fkContext' => '1604',
    'fkParent' => '1603',
    'sName' => '6.1 Infra-estrutura da seguran�a da informa��o',
  ),
  5 => 
  array (
    'fkContext' => '1605',
    'fkParent' => '1603',
    'sName' => '6.2 Partes externas',
  ),
  6 => 
  array (
    'fkContext' => '1606',
    'fkParent' => '1600',
    'sName' => '07 Gest�o de ativos',
  ),
  7 => 
  array (
    'fkContext' => '1607',
    'fkParent' => '1606',
    'sName' => '7.1 Responsabilidade pelos ativos',
  ),
  8 => 
  array (
    'fkContext' => '1608',
    'fkParent' => '1606',
    'sName' => '7.2 Classifica��o da Informa��o',
  ),
  9 => 
  array (
    'fkContext' => '1609',
    'fkParent' => '1600',
    'sName' => '08 Seguran�a em recursos humanos',
  ),
  10 => 
  array (
    'fkContext' => '1610',
    'fkParent' => '1609',
    'sName' => '8.1 Antes da contrata��o',
  ),
  11 => 
  array (
    'fkContext' => '1611',
    'fkParent' => '1609',
    'sName' => '8.2 Durante a contrata��o',
  ),
  12 => 
  array (
    'fkContext' => '1612',
    'fkParent' => '1609',
    'sName' => '8.3 Encerramento ou mudan�a da contrata��o',
  ),
  13 => 
  array (
    'fkContext' => '1613',
    'fkParent' => '1600',
    'sName' => '09 Seguran�a f�sica e do ambiente',
  ),
  14 => 
  array (
    'fkContext' => '1614',
    'fkParent' => '1613',
    'sName' => '9.1 �reas seguras',
  ),
  15 => 
  array (
    'fkContext' => '1615',
    'fkParent' => '1613',
    'sName' => '9.2 Seguran�a de equipamentos',
  ),
  16 => 
  array (
    'fkContext' => '1616',
    'fkParent' => '1600',
    'sName' => '10 Gerenciamento das opera��es e comunica��es',
  ),
  17 => 
  array (
    'fkContext' => '1617',
    'fkParent' => '1616',
    'sName' => '10.1 Procedimentos e responsabilidades operacionais',
  ),
  18 => 
  array (
    'fkContext' => '1618',
    'fkParent' => '1616',
    'sName' => '10.2 Gerenciamento de servi�os tercerizados',
  ),
  19 => 
  array (
    'fkContext' => '1619',
    'fkParent' => '1616',
    'sName' => '10.3 Planejamento e aceita��o dos sistemas',
  ),
  20 => 
  array (
    'fkContext' => '1620',
    'fkParent' => '1616',
    'sName' => '10.4 Prote��o contra codigos maliciosos e c�digos m�veis',
  ),
  21 => 
  array (
    'fkContext' => '1621',
    'fkParent' => '1616',
    'sName' => '10.5 C�pias de seguran�a',
  ),
  22 => 
  array (
    'fkContext' => '1622',
    'fkParent' => '1616',
    'sName' => '10.6 Gerenciamento da seguran�a em redes',
  ),
  23 => 
  array (
    'fkContext' => '1623',
    'fkParent' => '1616',
    'sName' => '10.7 Manuseio de m�dias',
  ),
  24 => 
  array (
    'fkContext' => '1624',
    'fkParent' => '1616',
    'sName' => '10.8 Troca de informa��es',
  ),
  25 => 
  array (
    'fkContext' => '1625',
    'fkParent' => '1616',
    'sName' => '10.9 Servi�os de com�cio eletr�nico',
  ),
  26 => 
  array (
    'fkContext' => '1626',
    'fkParent' => '1616',
    'sName' => '10.10 Monitoramento',
  ),
  27 => 
  array (
    'fkContext' => '1627',
    'fkParent' => '1600',
    'sName' => '11 Controle de acessos',
  ),
  28 => 
  array (
    'fkContext' => '1628',
    'fkParent' => '1627',
    'sName' => '11.1 Requisitos de neg�cio para controle de acesso',
  ),
  29 => 
  array (
    'fkContext' => '1629',
    'fkParent' => '1627',
    'sName' => '11.2 Gerenciamento de acesso do usu�rio',
  ),
  30 => 
  array (
    'fkContext' => '1630',
    'fkParent' => '1627',
    'sName' => '11.3 Responsabilidades dos usu�rios',
  ),
  31 => 
  array (
    'fkContext' => '1631',
    'fkParent' => '1627',
    'sName' => '11.4 Controle de acesso � rede',
  ),
  32 => 
  array (
    'fkContext' => '1632',
    'fkParent' => '1627',
    'sName' => '11.5 Controle de acesso aos sistema operacional',
  ),
  33 => 
  array (
    'fkContext' => '1633',
    'fkParent' => '1627',
    'sName' => '11.6 Controle de acesso � aplica��o e � informa��o',
  ),
  34 => 
  array (
    'fkContext' => '1634',
    'fkParent' => '1627',
    'sName' => '11.7 Computa��o m�vel e trabalho remoto',
  ),
  35 => 
  array (
    'fkContext' => '1635',
    'fkParent' => '1600',
    'sName' => '12 Aquisi��o, desenvolvimento e manuten��o de sistemas de informa��o',
  ),
  36 => 
  array (
    'fkContext' => '1636',
    'fkParent' => '1635',
    'sName' => '12.1 Requisitos de seguran�a de sistemas de informa��o',
  ),
  37 => 
  array (
    'fkContext' => '1637',
    'fkParent' => '1635',
    'sName' => '12.2 Processamento correto nas aplica��es',
  ),
  38 => 
  array (
    'fkContext' => '1638',
    'fkParent' => '1635',
    'sName' => '12.3 Controles criptogr�ficos',
  ),
  39 => 
  array (
    'fkContext' => '1639',
    'fkParent' => '1635',
    'sName' => '12.4 Seguran�a dos arquivos do sistema',
  ),
  40 => 
  array (
    'fkContext' => '1640',
    'fkParent' => '1635',
    'sName' => '12.5 Seguran�a em processos de desenvolvimento e de suporte',
  ),
  41 => 
  array (
    'fkContext' => '1641',
    'fkParent' => '1635',
    'sName' => '12.6 Gest�o de vulnerabilidades t�cnicas',
  ),
  42 => 
  array (
    'fkContext' => '1642',
    'fkParent' => '1600',
    'sName' => '13 Gest�o de incidentes de seguran�a da informa��o',
  ),
  43 => 
  array (
    'fkContext' => '1643',
    'fkParent' => '1642',
    'sName' => '13.1 Notifica��o de fragilidades e eventos de seguran�a da informa��o',
  ),
  44 => 
  array (
    'fkContext' => '1644',
    'fkParent' => '1642',
    'sName' => '13.2 Gest�o de incidentes de seguran�a da informa��o e melhorias',
  ),
  45 => 
  array (
    'fkContext' => '1645',
    'fkParent' => '1600',
    'sName' => '14 Gest�o de continuidade do neg�cio',
  ),
  46 => 
  array (
    'fkContext' => '1646',
    'fkParent' => '1645',
    'sName' => '14.1 Aspectos da gest�o da continuidade do neg�cio, relativos � seguran�a da informa��o',
  ),
  47 => 
  array (
    'fkContext' => '1647',
    'fkParent' => '1600',
    'sName' => '15 Conformidade',
  ),
  48 => 
  array (
    'fkContext' => '1648',
    'fkParent' => '1647',
    'sName' => '15.1 Conformidade com requisitos legais',
  ),
  49 => 
  array (
    'fkContext' => '1649',
    'fkParent' => '1647',
    'sName' => '15.2 Conformidade com normas pol�ticas de seguran�a da informa��o e conformidades t�cnicas',
  ),
  50 => 
  array (
    'fkContext' => '1650',
    'fkParent' => '1647',
    'sName' => '15.3 Considera��es quanto � auditoria de sistemas de informa��o',
  ),
  51 => 
  array (
    'fkContext' => '1651',
    'fkParent' => 'null',
    'sName' => 'CobiT',
  ),
  52 => 
  array (
    'fkContext' => '1652',
    'fkParent' => '1651',
    'sName' => 'Plan and Organise',
  ),
  53 => 
  array (
    'fkContext' => '1653',
    'fkParent' => '1652',
    'sName' => 'PO1Define a Strategic IT Plan',
  ),
  54 => 
  array (
    'fkContext' => '1654',
    'fkParent' => '1652',
    'sName' => 'PO2Define the Information Architecture',
  ),
  55 => 
  array (
    'fkContext' => '1655',
    'fkParent' => '1652',
    'sName' => 'PO3 Determine Technological Direction',
  ),
  56 => 
  array (
    'fkContext' => '1656',
    'fkParent' => '1652',
    'sName' => 'PO4 Define the IT Processes, Organisation and Relationships',
  ),
  57 => 
  array (
    'fkContext' => '1657',
    'fkParent' => '1652',
    'sName' => 'PO5 Manage the IT Investment',
  ),
  58 => 
  array (
    'fkContext' => '1658',
    'fkParent' => '1652',
    'sName' => 'PO6 Communicate Management Aims and Direction',
  ),
  59 => 
  array (
    'fkContext' => '1659',
    'fkParent' => '1652',
    'sName' => 'PO7 Manage IT Human Resources',
  ),
  60 => 
  array (
    'fkContext' => '1660',
    'fkParent' => '1652',
    'sName' => 'PO8 Manage Quality',
  ),
  61 => 
  array (
    'fkContext' => '1661',
    'fkParent' => '1652',
    'sName' => 'PO9 Assess and Manage IT Risks',
  ),
  62 => 
  array (
    'fkContext' => '1662',
    'fkParent' => '1652',
    'sName' => 'PO10 Manage Projects',
  ),
  63 => 
  array (
    'fkContext' => '1663',
    'fkParent' => '1651',
    'sName' => 'Acquire and Implement',
  ),
  64 => 
  array (
    'fkContext' => '1664',
    'fkParent' => '1663',
    'sName' => 'AI1 Identify Automated Solutions',
  ),
  65 => 
  array (
    'fkContext' => '1665',
    'fkParent' => '1663',
    'sName' => 'AI2 Acquire and Maintain Application Software',
  ),
  66 => 
  array (
    'fkContext' => '1666',
    'fkParent' => '1663',
    'sName' => 'AI3 Acquire and Maintain Technology Infrastructure',
  ),
  67 => 
  array (
    'fkContext' => '1667',
    'fkParent' => '1663',
    'sName' => 'AI4 Enable Operation and Use',
  ),
  68 => 
  array (
    'fkContext' => '1668',
    'fkParent' => '1663',
    'sName' => 'AI5 Procure IT Resources',
  ),
  69 => 
  array (
    'fkContext' => '1669',
    'fkParent' => '1663',
    'sName' => 'AI6 Manage Changes',
  ),
  70 => 
  array (
    'fkContext' => '1670',
    'fkParent' => '1663',
    'sName' => 'AI7 Install and Accredit Solutions and Changes',
  ),
  71 => 
  array (
    'fkContext' => '1671',
    'fkParent' => '1651',
    'sName' => 'Deliver and Support',
  ),
  72 => 
  array (
    'fkContext' => '1672',
    'fkParent' => '1671',
    'sName' => 'DS1 Define and Manage Service Levels',
  ),
  73 => 
  array (
    'fkContext' => '1673',
    'fkParent' => '1671',
    'sName' => 'DS2 Manage Third-party Services',
  ),
  74 => 
  array (
    'fkContext' => '1674',
    'fkParent' => '1671',
    'sName' => 'DS3 Manage Performance and Capacity',
  ),
  75 => 
  array (
    'fkContext' => '1675',
    'fkParent' => '1671',
    'sName' => 'DS4 Ensure Continuous Service',
  ),
  76 => 
  array (
    'fkContext' => '1676',
    'fkParent' => '1671',
    'sName' => 'DS5 Ensure Systems Security',
  ),
  77 => 
  array (
    'fkContext' => '1677',
    'fkParent' => '1671',
    'sName' => 'DS6 Identify and Allocate Costs',
  ),
  78 => 
  array (
    'fkContext' => '1678',
    'fkParent' => '1671',
    'sName' => 'DS7 Educate and Train Users',
  ),
  79 => 
  array (
    'fkContext' => '1679',
    'fkParent' => '1671',
    'sName' => 'DS8 Manage Service Desk and Incidents',
  ),
  80 => 
  array (
    'fkContext' => '1680',
    'fkParent' => '1671',
    'sName' => 'DS9 Manage the Configuration',
  ),
  81 => 
  array (
    'fkContext' => '1681',
    'fkParent' => '1671',
    'sName' => 'DS10 Manage Problems',
  ),
  82 => 
  array (
    'fkContext' => '1682',
    'fkParent' => '1671',
    'sName' => 'DS11 Manage Data',
  ),
  83 => 
  array (
    'fkContext' => '1683',
    'fkParent' => '1671',
    'sName' => 'DS12 Manage the Physical Environment',
  ),
  84 => 
  array (
    'fkContext' => '1684',
    'fkParent' => '1671',
    'sName' => 'DS13 Manage Operations',
  ),
  85 => 
  array (
    'fkContext' => '1685',
    'fkParent' => '1651',
    'sName' => 'Monitor and Evaluate',
  ),
  86 => 
  array (
    'fkContext' => '1686',
    'fkParent' => '1685',
    'sName' => 'ME1 Monitor and Evaluate IT Performance',
  ),
  87 => 
  array (
    'fkContext' => '1687',
    'fkParent' => '1685',
    'sName' => 'ME2 Monitor and Evaluate Internal Control',
  ),
  88 => 
  array (
    'fkContext' => '1688',
    'fkParent' => '1685',
    'sName' => 'ME3 Ensure Compliance With External Requirements',
  ),
  89 => 
  array (
    'fkContext' => '1689',
    'fkParent' => '1685',
    'sName' => 'ME4 Provide IT Governance',
  ),
  90 => 
  array (
    'fkContext' => '1690',
    'fkParent' => 'null',
    'sName' => 'PCI',
  ),
  91 => 
  array (
    'fkContext' => '1691',
    'fkParent' => '1690',
    'sName' => 'Build and Maintain a Secure Network',
  ),
  92 => 
  array (
    'fkContext' => '1692',
    'fkParent' => '1691',
    'sName' => '1 Install and maintain a firewall configuration to protect cardholder data',
  ),
  93 => 
  array (
    'fkContext' => '1693',
    'fkParent' => '1691',
    'sName' => '2 Do not use vendor-supplied defaults for system passwords and other security parameters.',
  ),
  94 => 
  array (
    'fkContext' => '1694',
    'fkParent' => '1690',
    'sName' => 'Protect Cardholder Data',
  ),
  95 => 
  array (
    'fkContext' => '1695',
    'fkParent' => '1694',
    'sName' => '3 Protect stored cardholder data',
  ),
  96 => 
  array (
    'fkContext' => '1696',
    'fkParent' => '1694',
    'sName' => '4 Encrypt transmission of cardholder data across open, public networks',
  ),
  97 => 
  array (
    'fkContext' => '1697',
    'fkParent' => '1690',
    'sName' => 'Maintain a Vulnerability Management Program',
  ),
  98 => 
  array (
    'fkContext' => '1698',
    'fkParent' => '1697',
    'sName' => '5 Use and regularly update anti-virus software or programs',
  ),
  99 => 
  array (
    'fkContext' => '1699',
    'fkParent' => '1697',
    'sName' => '6 Develop and maintain secure systems and applications',
  ),
  100 => 
  array (
    'fkContext' => '1700',
    'fkParent' => '1690',
    'sName' => 'Implement Strong Access Control Measures',
  ),
  101 => 
  array (
    'fkContext' => '1701',
    'fkParent' => '1700',
    'sName' => '7 Restrict access to cardholder data by business need-to-know',
  ),
  102 => 
  array (
    'fkContext' => '1702',
    'fkParent' => '1700',
    'sName' => '8 Assign a unique ID to each person with computer access',
  ),
  103 => 
  array (
    'fkContext' => '1703',
    'fkParent' => '1700',
    'sName' => '9 Restrict physical access to cardholder data',
  ),
  104 => 
  array (
    'fkContext' => '1704',
    'fkParent' => '1690',
    'sName' => 'Regularly Monitor and Test Networks ',
  ),
  105 => 
  array (
    'fkContext' => '1705',
    'fkParent' => '1704',
    'sName' => '10 Track and monitor all access to network resources and cardholder data',
  ),
  106 => 
  array (
    'fkContext' => '1706',
    'fkParent' => '1704',
    'sName' => '11 Regularly test security systems and processes',
  ),
  107 => 
  array (
    'fkContext' => '1707',
    'fkParent' => '1690',
    'sName' => 'Maintain an Information Security Policy',
  ),
  108 => 
  array (
    'fkContext' => '1708',
    'fkParent' => '1707',
    'sName' => '12 Maintain a policy that addresses information security for employees and contractors',
  ),
)
?>