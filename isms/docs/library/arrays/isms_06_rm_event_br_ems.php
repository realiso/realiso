<?php

$laEventBR_EMS = 
array (
0 => array (
  'fkContext' => '300',
  'fkCategory' => '100',
  'sDescription' => 'Efluentes org�nicos',
  'tImpact' => 'Altera��o da qualidade da �gua'
),
1 => array (
  'fkContext' => '301',
  'fkCategory' => '100',
  'sDescription' => 'Efluentes org�nicos',
  'tImpact' => 'Contamina��o do solo'
),
2 => array (
  'fkContext' => '302',
  'fkCategory' => '100',
  'sDescription' => 'Efluentes org�nicos',
  'tImpact' => 'Impacto visual no meio ambiente'
),
3 => array (
  'fkContext' => '303',
  'fkCategory' => '100',
  'sDescription' => 'Efluentes org�nicos',
  'tImpact' => 'Danos a vegeta��o'
),
4 => array (
  'fkContext' => '304',
  'fkCategory' => '100',
  'sDescription' => 'Efluentes org�nicos',
  'tImpact' => 'Inc�modo � vizinhan�a'
),
5 => array (
  'fkContext' => '305',
  'fkCategory' => '100',
  'sDescription' => 'Efluentes org�nicos',
  'tImpact' => 'Destino incorreto'
),
6 => array (
  'fkContext' => '306',
  'fkCategory' => '100',
  'sDescription' => 'Efluentes industriais',
  'tImpact' => 'Altera��o da qualidade da �gua'
),
7 => array (
  'fkContext' => '307',
  'fkCategory' => '100',
  'sDescription' => 'Efluentes industriais',
  'tImpact' => 'Contamina��o do solo'
),
8 => array (
  'fkContext' => '308',
  'fkCategory' => '100',
  'sDescription' => 'Efluentes industriais',
  'tImpact' => 'Impacto visual no meio ambiente'
),
9 => array (
  'fkContext' => '309',
  'fkCategory' => '100',
  'sDescription' => 'Efluentes industriais',
  'tImpact' => 'Danos a vegeta��o'
),
10 => array (
  'fkContext' => '310',
  'fkCategory' => '100',
  'sDescription' => 'Efluentes industriais',
  'tImpact' => 'Inc�modo � vizinhan�a'
),
11 => array (
  'fkContext' => '311',
  'fkCategory' => '100',
  'sDescription' => 'Efluentes industriais',
  'tImpact' => 'Destino incorreto'
),
12 => array (
  'fkContext' => '312',
  'fkCategory' => '100',
  'sDescription' => 'Emiss�es de material particulado',
  'tImpact' => 'Altera��o da qualidade do ar'
),
13 => array (
  'fkContext' => '313',
  'fkCategory' => '100',
  'sDescription' => 'Emiss�es de material particulado',
  'tImpact' => 'Altera��o da camada de oz�nio'
),
14 => array (
  'fkContext' => '314',
  'fkCategory' => '100',
  'sDescription' => 'Emiss�es de material particulado',
  'tImpact' => 'Danos � sa�de do funcion�rio'
),
15 => array (
  'fkContext' => '315',
  'fkCategory' => '100',
  'sDescription' => 'Emiss�es de material particulado',
  'tImpact' => 'Danos a vegeta��o'
),
16 => array (
  'fkContext' => '316',
  'fkCategory' => '100',
  'sDescription' => 'Emiss�es de material particulado',
  'tImpact' => 'Inc�modo � vizinhan�a'
),
17 => array (
  'fkContext' => '317',
  'fkCategory' => '100',
  'sDescription' => 'Emiss�es CFC\'s',
  'tImpact' => 'Altera��o da qualidade do ar'
),
18 => array (
  'fkContext' => '318',
  'fkCategory' => '100',
  'sDescription' => 'Emiss�es CFC\'s',
  'tImpact' => 'Aumento do efeito estufa'
),
19 => array (
  'fkContext' => '319',
  'fkCategory' => '100',
  'sDescription' => 'Emiss�es CFC\'s',
  'tImpact' => 'Altera��o da camada de oz�nio'
),
20 => array (
  'fkContext' => '320',
  'fkCategory' => '100',
  'sDescription' => 'Emiss�es CFC\'s',
  'tImpact' => 'Danos � sa�de do funcion�rio'
),
21 => array (
  'fkContext' => '321',
  'fkCategory' => '100',
  'sDescription' => 'Emiss�es CFC\'s',
  'tImpact' => 'Danos a vegeta��o'
),
22 => array (
  'fkContext' => '322',
  'fkCategory' => '100',
  'sDescription' => 'Emiss�es CFC\'s',
  'tImpact' => 'Inc�modo � vizinhan�a'
),
23 => array (
  'fkContext' => '323',
  'fkCategory' => '100',
  'sDescription' => 'Emiss�es de SOx, NOx, COx Pb, Sn, Hidrocarbonetos',
  'tImpact' => 'Altera��o da qualidade do ar'
),
24 => array (
  'fkContext' => '324',
  'fkCategory' => '100',
  'sDescription' => 'Emiss�es de SOx, NOx, COx Pb, Sn, Hidrocarbonetos',
  'tImpact' => 'Aumento do efeito estufa'
),
25 => array (
  'fkContext' => '325',
  'fkCategory' => '100',
  'sDescription' => 'Emiss�es de SOx, NOx, COx Pb, Sn, Hidrocarbonetos',
  'tImpact' => 'Altera��o da camada de oz�nio'
),
26 => array (
  'fkContext' => '326',
  'fkCategory' => '100',
  'sDescription' => 'Emiss�es de SOx, NOx, COx Pb, Sn, Hidrocarbonetos',
  'tImpact' => 'Danos � sa�de do funcion�rio'
),
27 => array (
  'fkContext' => '327',
  'fkCategory' => '100',
  'sDescription' => 'Emiss�es de SOx, NOx, COx Pb, Sn, Hidrocarbonetos',
  'tImpact' => 'Danos a vegeta��o'
),
28 => array (
  'fkContext' => '328',
  'fkCategory' => '100',
  'sDescription' => 'Emiss�es de SOx, NOx, COx Pb, Sn, Hidrocarbonetos',
  'tImpact' => 'Inc�modo � vizinhan�a'
),
29 => array (
  'fkContext' => '329',
  'fkCategory' => '100',
  'sDescription' => 'Gera��o de Ru�do',
  'tImpact' => 'Danos � sa�de do funcion�rio'
),
30 => array (
  'fkContext' => '330',
  'fkCategory' => '100',
  'sDescription' => 'Gera��o de Ru�do',
  'tImpact' => 'Inc�modo � vizinhan�a'
),
31 => array (
  'fkContext' => '331',
  'fkCategory' => '100',
  'sDescription' => 'Emiss�o de Radia��o',
  'tImpact' => 'Altera��o da qualidade do ar'
),
32 => array (
  'fkContext' => '332',
  'fkCategory' => '100',
  'sDescription' => 'Emiss�o de Radia��o',
  'tImpact' => 'Danos � sa�de do funcion�rio'
),
33 => array (
  'fkContext' => '333',
  'fkCategory' => '100',
  'sDescription' => 'Gera��o de Poeira do processo',
  'tImpact' => 'Altera��o da qualidade do ar'
),
34 => array (
  'fkContext' => '334',
  'fkCategory' => '100',
  'sDescription' => 'Gera��o de Poeira do processo',
  'tImpact' => 'Danos � sa�de do funcion�rio'
),
35 => array (
  'fkContext' => '335',
  'fkCategory' => '100',
  'sDescription' => 'Gera��o de Poeira do processo',
  'tImpact' => 'Inc�modo � vizinhan�a'
),
36 => array (
  'fkContext' => '336',
  'fkCategory' => '100',
  'sDescription' => 'Consumo de energia el�trica',
  'tImpact' => 'Esgotamento de recursos naturais'
),
37 => array (
  'fkContext' => '337',
  'fkCategory' => '100',
  'sDescription' => 'Consumo de derivados de petr�leo',
  'tImpact' => 'Altera��o da qualidade da �gua'
),
38 => array (
  'fkContext' => '338',
  'fkCategory' => '100',
  'sDescription' => 'Consumo de derivados de petr�leo',
  'tImpact' => 'Contamina��o do solo'
),
39 => array (
  'fkContext' => '339',
  'fkCategory' => '100',
  'sDescription' => 'Consumo de derivados de petr�leo',
  'tImpact' => 'Impacto visual no meio ambiente'
),
40 => array (
  'fkContext' => '340',
  'fkCategory' => '100',
  'sDescription' => 'Consumo de derivados de petr�leo',
  'tImpact' => 'Danos a vegeta��o'
),
41 => array (
  'fkContext' => '341',
  'fkCategory' => '100',
  'sDescription' => 'Consumo de derivados de petr�leo',
  'tImpact' => 'Inc�modo � vizinhan�a'
),
42 => array (
  'fkContext' => '342',
  'fkCategory' => '100',
  'sDescription' => 'Consumo de derivados de petr�leo',
  'tImpact' => 'Destino incorreto'
),
43 => array (
  'fkContext' => '343',
  'fkCategory' => '100',
  'sDescription' => 'Consumo de �gua',
  'tImpact' => 'Esgotamento de recursos naturais'
),
44 => array (
  'fkContext' => '344',
  'fkCategory' => '100',
  'sDescription' => 'Consumo de Lenha/Carv�o e derivados',
  'tImpact' => 'Esgotamento de recursos naturais'
),
45 => array (
  'fkContext' => '345',
  'fkCategory' => '100',
  'sDescription' => 'Consumo de g�s',
  'tImpact' => 'Esgotamento de recursos naturais'
),
46 => array (
  'fkContext' => '346',
  'fkCategory' => '100',
  'sDescription' => 'Consumo de g�s',
  'tImpact' => 'Inc�modo � vizinhan�a'
),
47 => array (
  'fkContext' => '347',
  'fkCategory' => '100',
  'sDescription' => 'Ligas met�licas, fios',
  'tImpact' => 'Destino incorreto'
),
48 => array (
  'fkContext' => '348',
  'fkCategory' => '100',
  'sDescription' => 'Ligas met�licas, fios',
  'tImpact' => 'Impacto visual no meio ambiente'
),
49 => array (
  'fkContext' => '349',
  'fkCategory' => '100',
  'sDescription' => 'Borrachas',
  'tImpact' => 'Destino incorreto'
),
50 => array (
  'fkContext' => '350',
  'fkCategory' => '100',
  'sDescription' => 'Borrachas',
  'tImpact' => 'Impacto visual no meio ambiente'
),
51 => array (
  'fkContext' => '351',
  'fkCategory' => '100',
  'sDescription' => 'Res�duo classe I',
  'tImpact' => 'Altera��o da qualidade da �gua'
),
52 => array (
  'fkContext' => '352',
  'fkCategory' => '100',
  'sDescription' => 'Res�duo classe I',
  'tImpact' => 'Contamina��o do solo'
),
53 => array (
  'fkContext' => '353',
  'fkCategory' => '100',
  'sDescription' => 'Res�duo classe I',
  'tImpact' => 'Impacto visual no meio ambiente'
),
54 => array (
  'fkContext' => '354',
  'fkCategory' => '100',
  'sDescription' => 'Res�duo classe I',
  'tImpact' => 'Danos a vegeta��o'
),
55 => array (
  'fkContext' => '355',
  'fkCategory' => '100',
  'sDescription' => 'Res�duo classe I',
  'tImpact' => 'Inc�modo � vizinhan�a'
),
56 => array (
  'fkContext' => '356',
  'fkCategory' => '100',
  'sDescription' => 'Res�duo classe I',
  'tImpact' => 'Destino incorreto'
),
57 => array (
  'fkContext' => '357',
  'fkCategory' => '100',
  'sDescription' => 'EPI\'s / Panos / Tecido de Forra��o / Carpete',
  'tImpact' => 'Destino incorreto'
),
58 => array (
  'fkContext' => '358',
  'fkCategory' => '100',
  'sDescription' => 'EPI\'s / Panos / Tecido de Forra��o / Carpete',
  'tImpact' => 'Impacto visual no meio ambiente'
),
59 => array (
  'fkContext' => '359',
  'fkCategory' => '100',
  'sDescription' => 'Lixo Ambulatorial',
  'tImpact' => 'Altera��o da qualidade da �gua'
),
60 => array (
  'fkContext' => '360',
  'fkCategory' => '100',
  'sDescription' => 'Lixo Ambulatorial',
  'tImpact' => 'Contamina��o do solo'
),
61 => array (
  'fkContext' => '361',
  'fkCategory' => '100',
  'sDescription' => 'Lixo Ambulatorial',
  'tImpact' => 'Impacto visual no meio ambiente'
),
62 => array (
  'fkContext' => '362',
  'fkCategory' => '100',
  'sDescription' => 'Lixo Ambulatorial',
  'tImpact' => 'Danos a vegeta��o'
),
63 => array (
  'fkContext' => '363',
  'fkCategory' => '100',
  'sDescription' => 'Lixo Ambulatorial',
  'tImpact' => 'Inc�modo � vizinhan�a'
),
64 => array (
  'fkContext' => '364',
  'fkCategory' => '100',
  'sDescription' => 'Lixo Ambulatorial',
  'tImpact' => 'Destino incorreto'
),
65 => array (
  'fkContext' => '365',
  'fkCategory' => '100',
  'sDescription' => 'L�mpadas fluorescentes, vapor de s�dio ou merc�rio',
  'tImpact' => 'Altera��o da qualidade da �gua'
),
66 => array (
  'fkContext' => '366',
  'fkCategory' => '100',
  'sDescription' => 'L�mpadas fluorescentes, vapor de s�dio ou merc�rio',
  'tImpact' => 'Contamina��o do solo'
),
67 => array (
  'fkContext' => '367',
  'fkCategory' => '100',
  'sDescription' => 'L�mpadas fluorescentes, vapor de s�dio ou merc�rio',
  'tImpact' => 'Impacto visual no meio ambiente'
),
68 => array (
  'fkContext' => '368',
  'fkCategory' => '100',
  'sDescription' => 'L�mpadas fluorescentes, vapor de s�dio ou merc�rio',
  'tImpact' => 'Danos a vegeta��o'
),
69 => array (
  'fkContext' => '369',
  'fkCategory' => '100',
  'sDescription' => 'L�mpadas fluorescentes, vapor de s�dio ou merc�rio',
  'tImpact' => 'Inc�modo � vizinhan�a'
),
70 => array (
  'fkContext' => '370',
  'fkCategory' => '100',
  'sDescription' => 'L�mpadas fluorescentes, vapor de s�dio ou merc�rio',
  'tImpact' => 'Destino incorreto'
),
71 => array (
  'fkContext' => '371',
  'fkCategory' => '100',
  'sDescription' => 'Solventes Sujos/Limpo (acetona, alcool, isoparafina, thinner)',
  'tImpact' => 'Altera��o da qualidade da �gua'
),
72 => array (
  'fkContext' => '372',
  'fkCategory' => '100',
  'sDescription' => 'Solventes Sujos/Limpo (acetona, alcool, isoparafina, thinner)',
  'tImpact' => 'Contamina��o do solo'
),
73 => array (
  'fkContext' => '373',
  'fkCategory' => '100',
  'sDescription' => 'Solventes Sujos/Limpo (acetona, alcool, isoparafina, thinner)',
  'tImpact' => 'Impacto visual no meio ambiente'
),
74 => array (
  'fkContext' => '374',
  'fkCategory' => '100',
  'sDescription' => 'Solventes Sujos/Limpo (acetona, alcool, isoparafina, thinner)',
  'tImpact' => 'Danos a vegeta��o'
),
75 => array (
  'fkContext' => '375',
  'fkCategory' => '100',
  'sDescription' => 'Solventes Sujos/Limpo (acetona, alcool, isoparafina, thinner)',
  'tImpact' => 'Inc�modo � vizinhan�a'
),
76 => array (
  'fkContext' => '376',
  'fkCategory' => '100',
  'sDescription' => 'Solventes Sujos/Limpo (acetona, alcool, isoparafina, thinner)',
  'tImpact' => 'Destino incorreto'
),
77 => array (
  'fkContext' => '377',
  'fkCategory' => '100',
  'sDescription' => 'Duratex / Madeira / F�rmica',
  'tImpact' => 'Destino incorreto'
),
78 => array (
  'fkContext' => '378',
  'fkCategory' => '100',
  'sDescription' => 'Duratex / Madeira / F�rmica',
  'tImpact' => 'Impacto visual no meio ambiente'
),
79 => array (
  'fkContext' => '379',
  'fkCategory' => '100',
  'sDescription' => 'Varredura (P� de varri��o, l� de vidro, termo ac�stico, cinzas, gesso, areia, bombril, abrasivos, lixas, cotonetes, pinceis, cera, restos de massa poli�ster, fita adesiva, filtros, couro, etc)',
  'tImpact' => 'Destino incorreto'
),
80 => array (
  'fkContext' => '380',
  'fkCategory' => '100',
  'sDescription' => 'Varredura (P� de varri��o, l� de vidro, termo ac�stico, cinzas, gesso, areia, bombril, abrasivos, lixas, cotonetes, pinceis, cera, restos de massa poli�ster, fita adesiva, filtros, couro, etc)',
  'tImpact' => 'Impacto visual no meio ambiente'
),
81 => array (
  'fkContext' => '381',
  'fkCategory' => '100',
  'sDescription' => 'Espumas / Isopor / Napa / Vulcouro / Taraflex / Poliuretano Expandido / ABS / teflon',
  'tImpact' => 'Destino incorreto'
),
82 => array (
  'fkContext' => '382',
  'fkCategory' => '100',
  'sDescription' => 'Espumas / Isopor / Napa / Vulcouro / Taraflex / Poliuretano Expandido / ABS / teflon',
  'tImpact' => 'Impacto visual no meio ambiente'
),
83 => array (
  'fkContext' => '383',
  'fkCategory' => '100',
  'sDescription' => 'Fibras',
  'tImpact' => 'Destino incorreto'
),
84 => array (
  'fkContext' => '384',
  'fkCategory' => '100',
  'sDescription' => 'Fibras',
  'tImpact' => 'Impacto visual no meio ambiente'
),
85 => array (
  'fkContext' => '385',
  'fkCategory' => '100',
  'sDescription' => 'Papel e Papel�o Limpos',
  'tImpact' => 'Destino incorreto'
),
86 => array (
  'fkContext' => '386',
  'fkCategory' => '100',
  'sDescription' => 'Papel e Papel�o Limpos',
  'tImpact' => 'Impacto visual no meio ambiente'
),
87 => array (
  'fkContext' => '387',
  'fkCategory' => '100',
  'sDescription' => 'Papel/Pl�stico contaminado (resina, cola, tinta, �leo, graxa, �cido)',
  'tImpact' => 'Altera��o da qualidade da �gua'
),
88 => array (
  'fkContext' => '388',
  'fkCategory' => '100',
  'sDescription' => 'Papel/Pl�stico contaminado (resina, cola, tinta, �leo, graxa, �cido)',
  'tImpact' => 'Contamina��o do solo'
),
89 => array (
  'fkContext' => '389',
  'fkCategory' => '100',
  'sDescription' => 'Papel/Pl�stico contaminado (resina, cola, tinta, �leo, graxa, �cido)',
  'tImpact' => 'Impacto visual no meio ambiente'
),
90 => array (
  'fkContext' => '390',
  'fkCategory' => '100',
  'sDescription' => 'Papel/Pl�stico contaminado (resina, cola, tinta, �leo, graxa, �cido)',
  'tImpact' => 'Danos a vegeta��o'
),
91 => array (
  'fkContext' => '391',
  'fkCategory' => '100',
  'sDescription' => 'Papel/Pl�stico contaminado (resina, cola, tinta, �leo, graxa, �cido)',
  'tImpact' => 'Inc�modo � vizinhan�a'
),
92 => array (
  'fkContext' => '392',
  'fkCategory' => '100',
  'sDescription' => 'Papel/Pl�stico contaminado (resina, cola, tinta, �leo, graxa, �cido)',
  'tImpact' => 'Destino incorreto'
),
93 => array (
  'fkContext' => '393',
  'fkCategory' => '100',
  'sDescription' => 'Pl�stico Duro (embalagens, bombonas, pe�as, perfis, mangueiras, cintas, etc) / Pl�stico Mole (embalagens, filmes, etc) / Copos',
  'tImpact' => 'Destino incorreto'
),
94 => array (
  'fkContext' => '394',
  'fkCategory' => '100',
  'sDescription' => 'Pl�stico Duro (embalagens, bombonas, pe�as, perfis, mangueiras, cintas, etc) / Pl�stico Mole (embalagens, filmes, etc) / Copos',
  'tImpact' => 'Impacto visual no meio ambiente'
),
95 => array (
  'fkContext' => '395',
  'fkCategory' => '100',
  'sDescription' => 'Tonner, Cartuchos de copiadoras e impressoras',
  'tImpact' => 'Destino incorreto'
),
96 => array (
  'fkContext' => '396',
  'fkCategory' => '100',
  'sDescription' => 'Tonner, Cartuchos de copiadoras e impressoras',
  'tImpact' => 'Impacto visual no meio ambiente'
),
97 => array (
  'fkContext' => '397',
  'fkCategory' => '100',
  'sDescription' => '�leo sujo, borra de �leo e Graxas, filtros de �leo',
  'tImpact' => 'Altera��o da qualidade da �gua'
),
98 => array (
  'fkContext' => '398',
  'fkCategory' => '100',
  'sDescription' => '�leo sujo, borra de �leo e Graxas, filtros de �leo',
  'tImpact' => 'Contamina��o do solo'
),
99 => array (
  'fkContext' => '399',
  'fkCategory' => '100',
  'sDescription' => '�leo sujo, borra de �leo e Graxas, filtros de �leo',
  'tImpact' => 'Impacto visual no meio ambiente'
),
100 => array (
  'fkContext' => '400',
  'fkCategory' => '100',
  'sDescription' => '�leo sujo, borra de �leo e Graxas, filtros de �leo',
  'tImpact' => 'Danos a vegeta��o'
),
101 => array (
  'fkContext' => '401',
  'fkCategory' => '100',
  'sDescription' => '�leo sujo, borra de �leo e Graxas, filtros de �leo',
  'tImpact' => 'Inc�modo � vizinhan�a'
),
102 => array (
  'fkContext' => '402',
  'fkCategory' => '100',
  'sDescription' => '�leo sujo, borra de �leo e Graxas, filtros de �leo',
  'tImpact' => 'Destino incorreto'
),
103 => array (
  'fkContext' => '403',
  'fkCategory' => '100',
  'sDescription' => 'Equipamentos e acess�rios el�tricos e eletr�nicos (Fia��es,Pilhas, Baterias, Sensores, Fus�veis, Rel�s,etc)',
  'tImpact' => 'Altera��o da qualidade da �gua'
),
104 => array (
  'fkContext' => '404',
  'fkCategory' => '100',
  'sDescription' => 'Equipamentos e acess�rios el�tricos e eletr�nicos (Fia��es,Pilhas, Baterias, Sensores, Fus�veis, Rel�s,etc)',
  'tImpact' => 'Contamina��o do solo'
),
105 => array (
  'fkContext' => '405',
  'fkCategory' => '100',
  'sDescription' => 'Equipamentos e acess�rios el�tricos e eletr�nicos (Fia��es,Pilhas, Baterias, Sensores, Fus�veis, Rel�s,etc)',
  'tImpact' => 'Impacto visual no meio ambiente'
),
106 => array (
  'fkContext' => '406',
  'fkCategory' => '100',
  'sDescription' => 'Equipamentos e acess�rios el�tricos e eletr�nicos (Fia��es,Pilhas, Baterias, Sensores, Fus�veis, Rel�s,etc)',
  'tImpact' => 'Danos a vegeta��o'
),
107 => array (
  'fkContext' => '407',
  'fkCategory' => '100',
  'sDescription' => 'Equipamentos e acess�rios el�tricos e eletr�nicos (Fia��es,Pilhas, Baterias, Sensores, Fus�veis, Rel�s,etc)',
  'tImpact' => 'Inc�modo � vizinhan�a'
),
108 => array (
  'fkContext' => '408',
  'fkCategory' => '100',
  'sDescription' => 'Equipamentos e acess�rios el�tricos e eletr�nicos (Fia��es,Pilhas, Baterias, Sensores, Fus�veis, Rel�s,etc)',
  'tImpact' => 'Destino incorreto'
),
109 => array (
  'fkContext' => '409',
  'fkCategory' => '100',
  'sDescription' => 'Vidros',
  'tImpact' => 'Destino incorreto'
),
110 => array (
  'fkContext' => '410',
  'fkCategory' => '100',
  'sDescription' => 'Vidros',
  'tImpact' => 'Impacto visual no meio ambiente'
),
111 => array (
  'fkContext' => '411',
  'fkCategory' => '100',
  'sDescription' => 'Sobra de processo',
  'tImpact' => 'Destino incorreto'
),
112 => array (
  'fkContext' => '412',
  'fkCategory' => '100',
  'sDescription' => 'Sobra de processo',
  'tImpact' => 'Impacto visual no meio ambiente'
),
113 => array (
  'fkContext' => '413',
  'fkCategory' => '100',
  'sDescription' => 'Borra de tinta',
  'tImpact' => 'Altera��o da qualidade da �gua'
),
114 => array (
  'fkContext' => '414',
  'fkCategory' => '100',
  'sDescription' => 'Borra de tinta',
  'tImpact' => 'Contamina��o do solo'
),
115 => array (
  'fkContext' => '415',
  'fkCategory' => '100',
  'sDescription' => 'Borra de tinta',
  'tImpact' => 'Impacto visual no meio ambiente'
),
116 => array (
  'fkContext' => '416',
  'fkCategory' => '100',
  'sDescription' => 'Borra de tinta',
  'tImpact' => 'Danos a vegeta��o'
),
117 => array (
  'fkContext' => '417',
  'fkCategory' => '100',
  'sDescription' => 'Borra de tinta',
  'tImpact' => 'Inc�modo � vizinhan�a'
),
118 => array (
  'fkContext' => '418',
  'fkCategory' => '100',
  'sDescription' => 'Borra de tinta',
  'tImpact' => 'Destino incorreto'
),
119 => array (
  'fkContext' => '419',
  'fkCategory' => '100',
  'sDescription' => 'Papel contaminado (res�duo de higieniza��o pessoal)',
  'tImpact' => 'Altera��o da qualidade da �gua'
),
120 => array (
  'fkContext' => '420',
  'fkCategory' => '100',
  'sDescription' => 'Papel contaminado (res�duo de higieniza��o pessoal)',
  'tImpact' => 'Contamina��o do solo'
),
121 => array (
  'fkContext' => '421',
  'fkCategory' => '100',
  'sDescription' => 'Papel contaminado (res�duo de higieniza��o pessoal)',
  'tImpact' => 'Impacto visual no meio ambiente'
),
122 => array (
  'fkContext' => '422',
  'fkCategory' => '100',
  'sDescription' => 'Papel contaminado (res�duo de higieniza��o pessoal)',
  'tImpact' => 'Danos a vegeta��o'
),
123 => array (
  'fkContext' => '423',
  'fkCategory' => '100',
  'sDescription' => 'Papel contaminado (res�duo de higieniza��o pessoal)',
  'tImpact' => 'Inc�modo � vizinhan�a'
),
124 => array (
  'fkContext' => '424',
  'fkCategory' => '100',
  'sDescription' => 'Papel contaminado (res�duo de higieniza��o pessoal)',
  'tImpact' => 'Destino incorreto'
),
125 => array (
  'fkContext' => '425',
  'fkCategory' => '100',
  'sDescription' => 'Res�duos Org�nicos',
  'tImpact' => 'Altera��o da qualidade da �gua'
),
126 => array (
  'fkContext' => '426',
  'fkCategory' => '100',
  'sDescription' => 'Res�duos Org�nicos',
  'tImpact' => 'Contamina��o do solo'
),
127 => array (
  'fkContext' => '427',
  'fkCategory' => '100',
  'sDescription' => 'Res�duos Org�nicos',
  'tImpact' => 'Impacto visual no meio ambiente'
),
128 => array (
  'fkContext' => '428',
  'fkCategory' => '100',
  'sDescription' => 'Res�duos Org�nicos',
  'tImpact' => 'Danos a vegeta��o'
),
129 => array (
  'fkContext' => '429',
  'fkCategory' => '100',
  'sDescription' => 'Res�duos Org�nicos',
  'tImpact' => 'Inc�modo � vizinhan�a'
),
130 => array (
  'fkContext' => '430',
  'fkCategory' => '100',
  'sDescription' => 'Res�duos Org�nicos',
  'tImpact' => 'Destino incorreto'
),
131 => array (
  'fkContext' => '431',
  'fkCategory' => '100',
  'sDescription' => 'Solu��o �cida',
  'tImpact' => 'Altera��o da qualidade da �gua'
),
132 => array (
  'fkContext' => '432',
  'fkCategory' => '100',
  'sDescription' => 'Solu��o �cida',
  'tImpact' => 'Contamina��o do solo'
),
133 => array (
  'fkContext' => '433',
  'fkCategory' => '100',
  'sDescription' => 'Solu��o �cida',
  'tImpact' => 'Impacto visual no meio ambiente'
),
134 => array (
  'fkContext' => '434',
  'fkCategory' => '100',
  'sDescription' => 'Solu��o �cida',
  'tImpact' => 'Danos a vegeta��o'
),
135 => array (
  'fkContext' => '435',
  'fkCategory' => '100',
  'sDescription' => 'Solu��o �cida',
  'tImpact' => 'Inc�modo � vizinhan�a'
),
136 => array (
  'fkContext' => '436',
  'fkCategory' => '100',
  'sDescription' => 'Solu��o �cida',
  'tImpact' => 'Destino incorreto'
),
137 => array (
  'fkContext' => '437',
  'fkCategory' => '100',
  'sDescription' => '�leo hidr�ulico, �gua de limpeza de pisos e paredes',
  'tImpact' => 'Altera��o da qualidade da �gua'
),
138 => array (
  'fkContext' => '438',
  'fkCategory' => '100',
  'sDescription' => '�leo hidr�ulico, �gua de limpeza de pisos e paredes',
  'tImpact' => 'Contamina��o do solo'
),
139 => array (
  'fkContext' => '439',
  'fkCategory' => '100',
  'sDescription' => '�leo hidr�ulico, �gua de limpeza de pisos e paredes',
  'tImpact' => 'Impacto visual no meio ambiente'
),
140 => array (
  'fkContext' => '440',
  'fkCategory' => '100',
  'sDescription' => '�leo hidr�ulico, �gua de limpeza de pisos e paredes',
  'tImpact' => 'Danos a vegeta��o'
),
141 => array (
  'fkContext' => '441',
  'fkCategory' => '100',
  'sDescription' => '�leo hidr�ulico, �gua de limpeza de pisos e paredes',
  'tImpact' => 'Inc�modo � vizinhan�a'
),
142 => array (
  'fkContext' => '442',
  'fkCategory' => '100',
  'sDescription' => '�leo hidr�ulico, �gua de limpeza de pisos e paredes',
  'tImpact' => 'Destino incorreto'
),
143 => array (
  'fkContext' => '443',
  'fkCategory' => '100',
  'sDescription' => '�gua condensada de vapor contaminado',
  'tImpact' => 'Altera��o da qualidade da �gua'
),
144 => array (
  'fkContext' => '444',
  'fkCategory' => '100',
  'sDescription' => '�gua condensada de vapor contaminado',
  'tImpact' => 'Contamina��o do solo'
),
145 => array (
  'fkContext' => '445',
  'fkCategory' => '100',
  'sDescription' => '�gua condensada de vapor contaminado',
  'tImpact' => 'Impacto visual no meio ambiente'
),
146 => array (
  'fkContext' => '446',
  'fkCategory' => '100',
  'sDescription' => '�gua condensada de vapor contaminado',
  'tImpact' => 'Danos a vegeta��o'
),
147 => array (
  'fkContext' => '447',
  'fkCategory' => '100',
  'sDescription' => '�gua condensada de vapor contaminado',
  'tImpact' => 'Inc�modo � vizinhan�a'
),
148 => array (
  'fkContext' => '448',
  'fkCategory' => '100',
  'sDescription' => '�gua condensada de vapor contaminado',
  'tImpact' => 'Destino incorreto'
),
149 => array (
  'fkContext' => '449',
  'fkCategory' => '100',
  'sDescription' => 'Desconforto T�rmico',
  'tImpact' => 'Danos � sa�de do funcion�rio'
),
150 => array (
  'fkContext' => '450',
  'fkCategory' => '100',
  'sDescription' => 'Desconforto Respirat�rio',
  'tImpact' => 'Danos � sa�de do funcion�rio'
),
151 => array (
  'fkContext' => '451',
  'fkCategory' => '100',
  'sDescription' => 'Desconforto Respirat�rio',
  'tImpact' => 'Inc�modo � vizinhan�a'
),
)
?>