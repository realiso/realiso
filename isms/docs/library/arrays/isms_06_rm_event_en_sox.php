<?php

$laEventEN_SOX = 
array (
  0 => 
  array (
    'fkContext' => '10008',
    'fkCategory' => '10007',
    'sDescription' => 'Deficiencies found during ongoing monitoring are not communicated to at least one level of management above the individual responsible for the function.',
  ),
  1 => 
  array (
    'fkContext' => '10009',
    'fkCategory' => '10007',
    'sDescription' => 'Deficiencies found during ongoing monitoring are not communicated to the individual responsible for the function.',
  ),
  2 => 
  array (
    'fkContext' => '10010',
    'fkCategory' => '10007',
    'sDescription' => 'Deficiencies found during separate evaluations are not communicated to at least one level of management above the individual responsible for the function.',
  ),
  3 => 
  array (
    'fkContext' => '10011',
    'fkCategory' => '10007',
    'sDescription' => 'Deficiencies found during separate evaluations are not communicated to the individual responsible for the function.',
  ),
  4 => 
  array (
    'fkContext' => '10012',
    'fkCategory' => '10007',
    'sDescription' => 'Employees do not understand the Code of Conduct.',
  ),
  5 => 
  array (
    'fkContext' => '10013',
    'fkCategory' => '10007',
    'sDescription' => 'Employees ignore the Code of Conduct.',
  ),
  6 => 
  array (
    'fkContext' => '10014',
    'fkCategory' => '10007',
    'sDescription' => 'Hostile takeover.',
  ),
  7 => 
  array (
    'fkContext' => '10015',
    'fkCategory' => '10007',
    'sDescription' => 'Inaccurate, untimely or unavailable information regarding pricing, products, actual or potential customers, advertising and promotion.',
  ),
  8 => 
  array (
    'fkContext' => '10016',
    'fkCategory' => '10007',
    'sDescription' => 'Inadequate and out-dated planning systems.',
  ),
  9 => 
  array (
    'fkContext' => '10017',
    'fkCategory' => '10007',
    'sDescription' => 'Inadequate attention to relationships with shareholders, investors or other outside parties.',
  ),
  10 => 
  array (
    'fkContext' => '10018',
    'fkCategory' => '10007',
    'sDescription' => 'Inadequate information regarding factors that may influence the entity\'s marketing strategy.',
  ),
  11 => 
  array (
    'fkContext' => '10019',
    'fkCategory' => '10007',
    'sDescription' => 'Inadequate management information systems.',
  ),
  12 => 
  array (
    'fkContext' => '10020',
    'fkCategory' => '10007',
    'sDescription' => 'Information does not generally flow down, across, and up organization.',
  ),
  13 => 
  array (
    'fkContext' => '10021',
    'fkCategory' => '10007',
    'sDescription' => 'Insufficient information regarding available opportunities.',
  ),
  14 => 
  array (
    'fkContext' => '10022',
    'fkCategory' => '10007',
    'sDescription' => 'Insufficient interaction of information technology, financial and operating management in developing strategic plans.',
  ),
  15 => 
  array (
    'fkContext' => '10023',
    'fkCategory' => '10007',
    'sDescription' => 'Integrity and ethical values are not maintained and demonstrated by management and staff.',
  ),
  16 => 
  array (
    'fkContext' => '10024',
    'fkCategory' => '10007',
    'sDescription' => 'Lack of awareness of entity-wide objectives.',
  ),
  17 => 
  array (
    'fkContext' => '10025',
    'fkCategory' => '10007',
    'sDescription' => 'Lack of awareness of sales and marketing objectives.',
  ),
  18 => 
  array (
    'fkContext' => '10026',
    'fkCategory' => '10007',
    'sDescription' => 'Lack of Code of Conduct.',
  ),
  19 => 
  array (
    'fkContext' => '10027',
    'fkCategory' => '10007',
    'sDescription' => 'Lack of product demand.',
  ),
  20 => 
  array (
    'fkContext' => '10028',
    'fkCategory' => '10007',
    'sDescription' => 'Lack of understanding of critical success factors.',
  ),
  21 => 
  array (
    'fkContext' => '10029',
    'fkCategory' => '10007',
    'sDescription' => 'Employees may not feel their efforts are noticed or appreciated.',
  ),
  22 => 
  array (
    'fkContext' => '10030',
    'fkCategory' => '10007',
    'sDescription' => 'Due dates and relative priorities of management reports are not clarified or communicated.',
  ),
  23 => 
  array (
    'fkContext' => '10031',
    'fkCategory' => '10007',
    'sDescription' => 'Lack of understanding of government policies.',
  ),
  24 => 
  array (
    'fkContext' => '10032',
    'fkCategory' => '10007',
    'sDescription' => 'Leakage of strategic information.',
  ),
  25 => 
  array (
    'fkContext' => '10033',
    'fkCategory' => '10007',
    'sDescription' => 'Limited number of appropriate distributors.',
  ),
  26 => 
  array (
    'fkContext' => '10034',
    'fkCategory' => '10007',
    'sDescription' => 'Limited number of positions.',
  ),
  27 => 
  array (
    'fkContext' => '10035',
    'fkCategory' => '10007',
    'sDescription' => 'Management does not assess risk significance.',
  ),
  28 => 
  array (
    'fkContext' => '10036',
    'fkCategory' => '10007',
    'sDescription' => 'Management does not assess the likelihood of risk occurrence.',
  ),
  29 => 
  array (
    'fkContext' => '10037',
    'fkCategory' => '10007',
    'sDescription' => 'Management does not assess what actions should be taken to mitigate risks.',
  ),
  30 => 
  array (
    'fkContext' => '10038',
    'fkCategory' => '10007',
    'sDescription' => 'Management does not compare actual performance to planned or expected results and analyze significant differences.',
  ),
  31 => 
  array (
    'fkContext' => '10039',
    'fkCategory' => '10007',
    'sDescription' => 'Management does not complete, within established timeframes, all actions that correct or otherwise resolve the matters brought to management\'s attention.',
  ),
  32 => 
  array (
    'fkContext' => '10040',
    'fkCategory' => '10007',
    'sDescription' => 'Management does not continually assess personnel skills.',
  ),
  33 => 
  array (
    'fkContext' => '10041',
    'fkCategory' => '10007',
    'sDescription' => 'Management does not determine the proper actions in response to findings and recommendations from audits and other reviews.',
  ),
  34 => 
  array (
    'fkContext' => '10042',
    'fkCategory' => '10007',
    'sDescription' => 'Management does not effectively manage its workforce.',
  ),
  35 => 
  array (
    'fkContext' => '10043',
    'fkCategory' => '10007',
    'sDescription' => 'Management does not have a good relationship with central oversight agencies such as OMB.',
  ),
  36 => 
  array (
    'fkContext' => '10044',
    'fkCategory' => '10007',
    'sDescription' => 'Management does not have a good relationship with Congress.',
  ),
  37 => 
  array (
    'fkContext' => '10045',
    'fkCategory' => '10007',
    'sDescription' => 'Management does not have a good relationship with the OIG.',
  ),
  38 => 
  array (
    'fkContext' => '10046',
    'fkCategory' => '10007',
    'sDescription' => 'Management does not have access to information relating to current technological developments.',
  ),
  39 => 
  array (
    'fkContext' => '10047',
    'fkCategory' => '10007',
    'sDescription' => 'Management does not have access to operational and/or financial data.',
  ),
  40 => 
  array (
    'fkContext' => '10048',
    'fkCategory' => '10007',
    'sDescription' => 'Management does not have mechanisms in place to identify and deal with risks that are unique to the operating environment of the Government.',
  ),
  41 => 
  array (
    'fkContext' => '10049',
    'fkCategory' => '10007',
    'sDescription' => 'Management does not hire the right personnel for the job.',
  ),
  42 => 
  array (
    'fkContext' => '10050',
    'fkCategory' => '10007',
    'sDescription' => 'Management does not maintain effective controls over information processing.',
  ),
  43 => 
  array (
    'fkContext' => '10051',
    'fkCategory' => '10007',
    'sDescription' => 'Management does not manage risk.',
  ),
  44 => 
  array (
    'fkContext' => '10052',
    'fkCategory' => '10007',
    'sDescription' => 'Organization does not have documented procedures in place to delegate authority and responsibility.',
  ),
  45 => 
  array (
    'fkContext' => '10053',
    'fkCategory' => '10007',
    'sDescription' => 'Management does not plan for personnel replacement.',
  ),
  46 => 
  array (
    'fkContext' => '10054',
    'fkCategory' => '10007',
    'sDescription' => 'Organization does not have procedures in place to delegate authority and responsibility.',
  ),
  47 => 
  array (
    'fkContext' => '10055',
    'fkCategory' => '10007',
    'sDescription' => 'Management does not play a key role in providing leadership in the area of integrity and ethical values.',
  ),
  48 => 
  array (
    'fkContext' => '10056',
    'fkCategory' => '10007',
    'sDescription' => 'Organization does not maintain physical control of, and properly secures/safeguard, vulnerable assets.',
  ),
  49 => 
  array (
    'fkContext' => '10057',
    'fkCategory' => '10007',
    'sDescription' => 'Management does not promptly evaluate findings from audits and other reviews.',
  ),
  50 => 
  array (
    'fkContext' => '10058',
    'fkCategory' => '10007',
    'sDescription' => 'Organization does not provide a proper amount of supervision.',
  ),
  51 => 
  array (
    'fkContext' => '10059',
    'fkCategory' => '10007',
    'sDescription' => 'Management does not provide adequate incentives for the personnel to ensure job success.',
  ),
  52 => 
  array (
    'fkContext' => '10060',
    'fkCategory' => '10007',
    'sDescription' => 'Organization does not provide guidance for disciplinary actions, when appropriate.',
  ),
  53 => 
  array (
    'fkContext' => '10061',
    'fkCategory' => '10007',
    'sDescription' => 'Organization does not provide guidance for proper behavior influences the quality of internal control.',
  ),
  54 => 
  array (
    'fkContext' => '10062',
    'fkCategory' => '10007',
    'sDescription' => 'Management does not provide adequate training for the personnel to ensure job success.',
  ),
  55 => 
  array (
    'fkContext' => '10063',
    'fkCategory' => '10007',
    'sDescription' => 'Organization does not provide guidance for removing temptations for unethical behavior.',
  ),
  56 => 
  array (
    'fkContext' => '10064',
    'fkCategory' => '10007',
    'sDescription' => 'Management does not provide candid and constructive counseling.',
  ),
  57 => 
  array (
    'fkContext' => '10065',
    'fkCategory' => '10007',
    'sDescription' => 'Organization has not established, and/or does not regularly review, performance measures and indicators.',
  ),
  58 => 
  array (
    'fkContext' => '10066',
    'fkCategory' => '10007',
    'sDescription' => 'Management does not provide needed training.',
  ),
  59 => 
  array (
    'fkContext' => '10067',
    'fkCategory' => '10007',
    'sDescription' => 'Organization structure does not define key areas of authority and responsibility.',
  ),
  60 => 
  array (
    'fkContext' => '10068',
    'fkCategory' => '10007',
    'sDescription' => 'Management does not provide performance appraisals.',
  ),
  61 => 
  array (
    'fkContext' => '10069',
    'fkCategory' => '10007',
    'sDescription' => 'Organization structure does not establish appropriate lines of reporting.',
  ),
  62 => 
  array (
    'fkContext' => '10070',
    'fkCategory' => '10007',
    'sDescription' => 'Organizational structure does not provide a framework for planning, directing, and controlling operations to achieve agency objectives.',
  ),
  63 => 
  array (
    'fkContext' => '10071',
    'fkCategory' => '10007',
    'sDescription' => 'Management does not provide qualified and continuous supervision of internal control objectives.',
  ),
  64 => 
  array (
    'fkContext' => '10072',
    'fkCategory' => '10007',
    'sDescription' => 'Organization does not have appropriate practices for disciplining personnel.',
  ),
  65 => 
  array (
    'fkContext' => '10073',
    'fkCategory' => '10007',
    'sDescription' => 'Plan formats are ineffective in providing necessary benchmarks against which performance can be measured.',
  ),
  66 => 
  array (
    'fkContext' => '10074',
    'fkCategory' => '10007',
    'sDescription' => 'Management does not retain valuable personnel.',
  ),
  67 => 
  array (
    'fkContext' => '10075',
    'fkCategory' => '10007',
    'sDescription' => 'Management does not track major organizational achievements.',
  ),
  68 => 
  array (
    'fkContext' => '10076',
    'fkCategory' => '10007',
    'sDescription' => 'Management has a dismissive attitude toward accounting functions.',
  ),
  69 => 
  array (
    'fkContext' => '10077',
    'fkCategory' => '10007',
    'sDescription' => 'Policies and procedures have not been established for ensuring that findings of audits and other reviews are promptly resolved.',
  ),
  70 => 
  array (
    'fkContext' => '10078',
    'fkCategory' => '10007',
    'sDescription' => 'Management has a dismissive attitude toward audits and evaluations.',
  ),
  71 => 
  array (
    'fkContext' => '10079',
    'fkCategory' => '10007',
    'sDescription' => 'Management has a dismissive attitude toward information systems.',
  ),
  72 => 
  array (
    'fkContext' => '10080',
    'fkCategory' => '10007',
    'sDescription' => 'Management has a dismissive attitude toward monitoring functions activities.',
  ),
  73 => 
  array (
    'fkContext' => '10081',
    'fkCategory' => '10007',
    'sDescription' => 'Pre-established standards are not determined.',
  ),
  74 => 
  array (
    'fkContext' => '10082',
    'fkCategory' => '10007',
    'sDescription' => 'Products become obsolete.',
  ),
  75 => 
  array (
    'fkContext' => '10083',
    'fkCategory' => '10007',
    'sDescription' => 'Management has a dismissive attitude toward personnel functions.',
  ),
  76 => 
  array (
    'fkContext' => '10084',
    'fkCategory' => '10007',
    'sDescription' => 'Relationships between oversight agencies and management do not exist.',
  ),
  77 => 
  array (
    'fkContext' => '10085',
    'fkCategory' => '10007',
    'sDescription' => 'Management has not adopted performance-based management.',
  ),
  78 => 
  array (
    'fkContext' => '10086',
    'fkCategory' => '10007',
    'sDescription' => 'Sales personnel are unaware of potential customers',
  ),
  79 => 
  array (
    'fkContext' => '10087',
    'fkCategory' => '10007',
    'sDescription' => 'Management has not comprehensively identified risks at the entity-level.',
  ),
  80 => 
  array (
    'fkContext' => '10088',
    'fkCategory' => '10007',
    'sDescription' => 'Management has not considered all significant interactions between individual activities and other parties.',
  ),
  81 => 
  array (
    'fkContext' => '10089',
    'fkCategory' => '10007',
    'sDescription' => 'Management has not considered all significant interactions between the entity and other parties.',
  ),
  82 => 
  array (
    'fkContext' => '10090',
    'fkCategory' => '10007',
    'sDescription' => 'Management has not considered all significant internal factors at the activity-level.',
  ),
  83 => 
  array (
    'fkContext' => '10091',
    'fkCategory' => '10007',
    'sDescription' => 'Organization delegation procedures do not cover authority and responsibility for operating activities.',
  ),
  84 => 
  array (
    'fkContext' => '10092',
    'fkCategory' => '10007',
    'sDescription' => 'Management has not considered all significant internal factors at the entity-level.',
  ),
  85 => 
  array (
    'fkContext' => '10093',
    'fkCategory' => '10007',
    'sDescription' => 'Organization delegation procedures do not cover authority and responsibility for reporting relationships.',
  ),
  86 => 
  array (
    'fkContext' => '10094',
    'fkCategory' => '10007',
    'sDescription' => 'Management has not established clear, consistent Agency objectives.',
  ),
  87 => 
  array (
    'fkContext' => '10095',
    'fkCategory' => '10007',
    'sDescription' => 'Organization does not have a corporate information systems architecture.',
  ),
  88 => 
  array (
    'fkContext' => '10096',
    'fkCategory' => '10007',
    'sDescription' => 'Organization does not have appropriate practices for compensating personnel.',
  ),
  89 => 
  array (
    'fkContext' => '10097',
    'fkCategory' => '10007',
    'sDescription' => 'Management has not established clear, consistent objectives at the activity-level.',
  ),
  90 => 
  array (
    'fkContext' => '10098',
    'fkCategory' => '10007',
    'sDescription' => 'Organization does not have appropriate practices for counseling personnel.',
  ),
  91 => 
  array (
    'fkContext' => '10099',
    'fkCategory' => '10007',
    'sDescription' => 'Management has not recently revisited the sufficiency of disaster recovery plans.',
  ),
  92 => 
  array (
    'fkContext' => '10100',
    'fkCategory' => '10007',
    'sDescription' => 'Organization does not have appropriate practices for evaluating personnel.',
  ),
  93 => 
  array (
    'fkContext' => '10101',
    'fkCategory' => '10007',
    'sDescription' => 'Management is unaware of legal and regulatory requirements.',
  ),
  94 => 
  array (
    'fkContext' => '10102',
    'fkCategory' => '10007',
    'sDescription' => 'Organization does not have appropriate practices for hiring personnel.',
  ),
  95 => 
  array (
    'fkContext' => '10103',
    'fkCategory' => '10007',
    'sDescription' => 'Management is unaware of valuable assets.',
  ),
  96 => 
  array (
    'fkContext' => '10104',
    'fkCategory' => '10007',
    'sDescription' => 'Organization does not have appropriate practices for orienting personnel.',
  ),
  97 => 
  array (
    'fkContext' => '10105',
    'fkCategory' => '10007',
    'sDescription' => 'Management or supervisory personnel ignore legal and regulatory requirements or company policies.',
  ),
  98 => 
  array (
    'fkContext' => '10106',
    'fkCategory' => '10007',
    'sDescription' => 'Organization does not have appropriate practices for promoting personnel.',
  ),
  99 => 
  array (
    'fkContext' => '10107',
    'fkCategory' => '10007',
    'sDescription' => 'Management takes too much risk.',
  ),
  100 => 
  array (
    'fkContext' => '10108',
    'fkCategory' => '10007',
    'sDescription' => 'Organization does not have appropriate practices for training personnel.',
  ),
  101 => 
  array (
    'fkContext' => '10109',
    'fkCategory' => '10007',
    'sDescription' => 'Mismatch in policy and knowledge about marketing and financial strategies.',
  ),
  102 => 
  array (
    'fkContext' => '10110',
    'fkCategory' => '10007',
    'sDescription' => 'Monitoring efforts do not include comparisons, reconciliations, and other actions that people take in performing their duties.',
  ),
  103 => 
  array (
    'fkContext' => '10111',
    'fkCategory' => '10007',
    'sDescription' => 'Monitoring efforts do not include regular management and supervisory activities.',
  ),
  104 => 
  array (
    'fkContext' => '10112',
    'fkCategory' => '10007',
    'sDescription' => 'No adequate strategy in place.',
  ),
  105 => 
  array (
    'fkContext' => '10113',
    'fkCategory' => '10007',
    'sDescription' => 'Non added value based acquisition.',
  ),
  106 => 
  array (
    'fkContext' => '10114',
    'fkCategory' => '10007',
    'sDescription' => 'Not meeting targets.',
  ),
  107 => 
  array (
    'fkContext' => '10115',
    'fkCategory' => '10007',
    'sDescription' => 'Ongoing monitoring does not occur in the course of normal operations.',
  ),
  108 => 
  array (
    'fkContext' => '10116',
    'fkCategory' => '10007',
    'sDescription' => 'Organization delegation procedures do not cover authority and responsibility for authorization protocols.',
  ),
  109 => 
  array (
    'fkContext' => '10117',
    'fkCategory' => '10007',
    'sDescription' => 'Separate evaluations of controls do not focus directly on their effectiveness at a specific time.',
  ),
  110 => 
  array (
    'fkContext' => '10118',
    'fkCategory' => '10007',
    'sDescription' => 'Serious matters found during ongoing monitoring are not reported to top management.',
  ),
  111 => 
  array (
    'fkContext' => '10119',
    'fkCategory' => '10007',
    'sDescription' => 'Wrong information as basis for decision making; competition risk.',
  ),
  112 => 
  array (
    'fkContext' => '10120',
    'fkCategory' => '10007',
    'sDescription' => 'Serious matters found during separate evaluations are not reported to top management.',
  ),
  113 => 
  array (
    'fkContext' => '10121',
    'fkCategory' => '10007',
    'sDescription' => 'Management has not identified appropriate knowledge and skills needed for various jobs.',
  ),
  114 => 
  array (
    'fkContext' => '10122',
    'fkCategory' => '10007',
    'sDescription' => 'Technology development projects do not support entity-wide objectives or strategies.',
  ),
  115 => 
  array (
    'fkContext' => '10123',
    'fkCategory' => '10007',
    'sDescription' => 'Management information needs with respect to payroll are not defined.',
  ),
  116 => 
  array (
    'fkContext' => '10124',
    'fkCategory' => '10007',
    'sDescription' => 'The organization does not have a positive \'ethical tone\'.',
  ),
  117 => 
  array (
    'fkContext' => '10125',
    'fkCategory' => '10007',
    'sDescription' => 'The scope and frequency of separate evaluations do not take into consideration the assessment of risks and the effectiveness of ongoing monitoring procedures.',
  ),
  118 => 
  array (
    'fkContext' => '10126',
    'fkCategory' => '10007',
    'sDescription' => 'Margin deterioration, alternative sales prices quoted.',
  ),
  119 => 
  array (
    'fkContext' => '10127',
    'fkCategory' => '10007',
    'sDescription' => 'Margin pressure risk.',
  ),
  120 => 
  array (
    'fkContext' => '10128',
    'fkCategory' => '10007',
    'sDescription' => 'There are no separate evaluations of internal control effectiveness.',
  ),
  121 => 
  array (
    'fkContext' => '10129',
    'fkCategory' => '10007',
    'sDescription' => 'There is a lack of adequate means of communicating with, and obtaining information needed throughout organization to achieve from, external stakeholders that may have a significant impact on organization achieving its goals.',
  ),
  122 => 
  array (
    'fkContext' => '10130',
    'fkCategory' => '10007',
    'sDescription' => 'Organization does not have disaster recovery plans in place.',
  ),
  123 => 
  array (
    'fkContext' => '10131',
    'fkCategory' => '10007',
    'sDescription' => 'Unsecure bad acquisition.',
  ),
  124 => 
  array (
    'fkContext' => '10132',
    'fkCategory' => '10007',
    'sDescription' => 'Treasury risk.',
  ),
  125 => 
  array (
    'fkContext' => '10133',
    'fkCategory' => '10007',
    'sDescription' => 'Waste of money as result from non economical decision making.',
  ),
  126 => 
  array (
    'fkContext' => '10134',
    'fkCategory' => '10007',
    'sDescription' => 'Wrong expectations released by investor relations.',
  ),
  127 => 
  array (
    'fkContext' => '10136',
    'fkCategory' => '10135',
    'sDescription' => 'Changing legal and regulatory requirements.',
  ),
  128 => 
  array (
    'fkContext' => '10137',
    'fkCategory' => '10135',
    'sDescription' => 'Employees may not be aware of applicable laws and regulations.',
  ),
  129 => 
  array (
    'fkContext' => '10138',
    'fkCategory' => '10135',
    'sDescription' => 'Existing patents may be disregarded.',
  ),
  130 => 
  array (
    'fkContext' => '10139',
    'fkCategory' => '10135',
    'sDescription' => 'Inadequate information about, or understanding of, filing requirements and applicable laws and regulations.',
  ),
  131 => 
  array (
    'fkContext' => '10140',
    'fkCategory' => '10135',
    'sDescription' => 'Lack of awareness of laws and regulations.',
  ),
  132 => 
  array (
    'fkContext' => '10141',
    'fkCategory' => '10135',
    'sDescription' => 'Lack of knowledge regarding OSHA laws and regulations.',
  ),
  133 => 
  array (
    'fkContext' => '10142',
    'fkCategory' => '10135',
    'sDescription' => 'Legal counsel does get all contracts where entity commits itself legally.',
  ),
  134 => 
  array (
    'fkContext' => '10143',
    'fkCategory' => '10135',
    'sDescription' => 'Legal counsel does not review contracts or agreements.',
  ),
  135 => 
  array (
    'fkContext' => '10144',
    'fkCategory' => '10135',
    'sDescription' => 'Legal counsel is unaware of all activities taking place within the entity.',
  ),
  136 => 
  array (
    'fkContext' => '10145',
    'fkCategory' => '10135',
    'sDescription' => 'Management or supervisory personnel are unaware of legal and regulatory requirements and company policies.',
  ),
  137 => 
  array (
    'fkContext' => '10146',
    'fkCategory' => '10135',
    'sDescription' => 'Nonlegal personnel are unaware that certain circumstances could potentially lead to litigation.',
  ),
  138 => 
  array (
    'fkContext' => '10147',
    'fkCategory' => '10135',
    'sDescription' => 'Personnel are unaware of applicable laws and regulations.',
  ),
  139 => 
  array (
    'fkContext' => '10148',
    'fkCategory' => '10135',
    'sDescription' => 'Personnel are unaware of applicable laws, regulations, rules or contractual agreements.',
  ),
  140 => 
  array (
    'fkContext' => '10150',
    'fkCategory' => '10149',
    'sDescription' => 'Access to resources and records is not limited to authorized individuals.',
  ),
  141 => 
  array (
    'fkContext' => '10151',
    'fkCategory' => '10149',
    'sDescription' => 'Accountability for custody and use of resources and records is not assigned and/or maintained.',
  ),
  142 => 
  array (
    'fkContext' => '10152',
    'fkCategory' => '10149',
    'sDescription' => 'Analysis cannot be conducted due to inconsistent or nonexistent performance indicators.',
  ),
  143 => 
  array (
    'fkContext' => '10153',
    'fkCategory' => '10149',
    'sDescription' => 'Documentation is not readily available for examination.',
  ),
  144 => 
  array (
    'fkContext' => '10154',
    'fkCategory' => '10149',
    'sDescription' => 'Inaccurate information or estimates regarding costs of litigation or anticipated settlements.',
  ),
  145 => 
  array (
    'fkContext' => '10155',
    'fkCategory' => '10149',
    'sDescription' => 'Inaccurate or unavailable customer information.',
  ),
  146 => 
  array (
    'fkContext' => '10156',
    'fkCategory' => '10149',
    'sDescription' => 'Inaccurate, insufficient or untimely information regarding risk-related costs or accidents or incidents that could give rise to an insurance claim.',
  ),
  147 => 
  array (
    'fkContext' => '10157',
    'fkCategory' => '10149',
    'sDescription' => 'Inaccurate, untimely or unavailable information regarding actual costs incurred.',
  ),
  148 => 
  array (
    'fkContext' => '10158',
    'fkCategory' => '10149',
    'sDescription' => 'Inaccurate, untimely or unavailable information regarding cash inflows and outflows.',
  ),
  149 => 
  array (
    'fkContext' => '10159',
    'fkCategory' => '10149',
    'sDescription' => 'Inadequate information about, or understanding of, financial reporting of tax transactions or economic events.',
  ),
  150 => 
  array (
    'fkContext' => '10160',
    'fkCategory' => '10149',
    'sDescription' => 'Inadequate information regarding tax-savings opportunities.',
  ),
  151 => 
  array (
    'fkContext' => '10161',
    'fkCategory' => '10149',
    'sDescription' => 'Inadequate or inaccurate information.',
  ),
  152 => 
  array (
    'fkContext' => '10162',
    'fkCategory' => '10149',
    'sDescription' => 'Incomplete or inaccurate information used as the basis for document preparation.',
  ),
  153 => 
  array (
    'fkContext' => '10163',
    'fkCategory' => '10149',
    'sDescription' => 'Information needs of management or others is unknown or not clearly communicated.',
  ),
  154 => 
  array (
    'fkContext' => '10164',
    'fkCategory' => '10149',
    'sDescription' => 'Information systems are incapable of providing necessary information.',
  ),
  155 => 
  array (
    'fkContext' => '10165',
    'fkCategory' => '10149',
    'sDescription' => 'Information systems cannot provide necessary information in a timely manner.',
  ),
  156 => 
  array (
    'fkContext' => '10166',
    'fkCategory' => '10149',
    'sDescription' => 'Internal control, all transactions, and other significant events are not clearly documented.',
  ),
  157 => 
  array (
    'fkContext' => '10167',
    'fkCategory' => '10149',
    'sDescription' => 'Journal entries related to tax transactions or economic events are not properly approved or posted to the general ledger.',
  ),
  158 => 
  array (
    'fkContext' => '10168',
    'fkCategory' => '10149',
    'sDescription' => 'Lack of information regarding profit margins and / or sales prices.',
  ),
  159 => 
  array (
    'fkContext' => '10169',
    'fkCategory' => '10149',
    'sDescription' => 'Lack of or inaccurate information regarding competitive products or potential new products.',
  ),
  160 => 
  array (
    'fkContext' => '10170',
    'fkCategory' => '10149',
    'sDescription' => 'Lack of understanding of reporting requirements.',
  ),
  161 => 
  array (
    'fkContext' => '10171',
    'fkCategory' => '10149',
    'sDescription' => 'Out-of-date or incomplete price information.',
  ),
  162 => 
  array (
    'fkContext' => '10172',
    'fkCategory' => '10149',
    'sDescription' => 'Pertinent operational and/or financial information is not identified, captured, and distributed in a form and time frame that permits employees to perform their duties efficiently.',
  ),
  163 => 
  array (
    'fkContext' => '10173',
    'fkCategory' => '10149',
    'sDescription' => 'Unavailable or inaccurate information.',
  ),
  164 => 
  array (
    'fkContext' => '10174',
    'fkCategory' => '10149',
    'sDescription' => 'Unavailable or inaccurate information about fraudulent acts or other improper activities of vendors.',
  ),
  165 => 
  array (
    'fkContext' => '10175',
    'fkCategory' => '10149',
    'sDescription' => 'Unavailable or inaccurate information on inventory levels or production needs.',
  ),
  166 => 
  array (
    'fkContext' => '10176',
    'fkCategory' => '10149',
    'sDescription' => 'Incomplete or inaccurate information regarding changes affecting the entity, such as competition, products, customer preferences, or legal and regulatory changes.',
  ),
  167 => 
  array (
    'fkContext' => '11410',
    'fkCategory' => '11225',
    'sDescription' => 'Employees are authorizing and executing transactions and other significant events outside the scope of their authority.',
  ),
  168 => 
  array (
    'fkContext' => '11411',
    'fkCategory' => '11225',
    'sDescription' => 'Handling and storage procedures, including storage containers, facilities and maintenance, are inappropriate for the nature of the products.',
  ),
  169 => 
  array (
    'fkContext' => '11412',
    'fkCategory' => '11225',
    'sDescription' => 'Inaccurate information.',
  ),
  170 => 
  array (
    'fkContext' => '11413',
    'fkCategory' => '11225',
    'sDescription' => 'Inaccurate input of data.',
  ),
  171 => 
  array (
    'fkContext' => '11414',
    'fkCategory' => '11225',
    'sDescription' => 'Inaccurate or incomplete information is acquired and retained.',
  ),
  172 => 
  array (
    'fkContext' => '11415',
    'fkCategory' => '11225',
    'sDescription' => 'Inaccurate or untimely information.',
  ),
  173 => 
  array (
    'fkContext' => '11416',
    'fkCategory' => '11225',
    'sDescription' => 'Inadequate health & safety considerations.',
  ),
  174 => 
  array (
    'fkContext' => '11417',
    'fkCategory' => '11225',
    'sDescription' => 'Inadequate policies and procedures to prevent unauthorized use.',
  ),
  175 => 
  array (
    'fkContext' => '11418',
    'fkCategory' => '11225',
    'sDescription' => 'Inadequate transfer or requisition procedures.',
  ),
  176 => 
  array (
    'fkContext' => '11419',
    'fkCategory' => '11225',
    'sDescription' => 'Inadequate vendor screening.',
  ),
  177 => 
  array (
    'fkContext' => '11420',
    'fkCategory' => '11225',
    'sDescription' => 'Information is too specific to be usable.',
  ),
  178 => 
  array (
    'fkContext' => '11421',
    'fkCategory' => '11225',
    'sDescription' => 'Insufficient or inappropriate resources.',
  ),
  179 => 
  array (
    'fkContext' => '11422',
    'fkCategory' => '11225',
    'sDescription' => 'Insufficient staff.',
  ),
  180 => 
  array (
    'fkContext' => '11423',
    'fkCategory' => '11225',
    'sDescription' => 'Lack of adequate tools for the personnel to ensure job success.',
  ),
  181 => 
  array (
    'fkContext' => '11424',
    'fkCategory' => '11225',
    'sDescription' => 'Lack of awareness of entity\'s current human resources.',
  ),
  182 => 
  array (
    'fkContext' => '11425',
    'fkCategory' => '11225',
    'sDescription' => 'Lack of or excess staff.',
  ),
  183 => 
  array (
    'fkContext' => '11426',
    'fkCategory' => '11225',
    'sDescription' => 'Lack or loss of information or documents.',
  ),
  184 => 
  array (
    'fkContext' => '11427',
    'fkCategory' => '11225',
    'sDescription' => 'Lost or misplaced information.',
  ),
  185 => 
  array (
    'fkContext' => '11428',
    'fkCategory' => '11225',
    'sDescription' => 'Missing documents or incorrect information.',
  ),
  186 => 
  array (
    'fkContext' => '11429',
    'fkCategory' => '11225',
    'sDescription' => 'Missing or untimely receipt of documents.',
  ),
  187 => 
  array (
    'fkContext' => '11436',
    'fkCategory' => '11225',
    'sDescription' => 'Misuse of data / information (inside and outside organization).',
  ),
  188 => 
  array (
    'fkContext' => '11437',
    'fkCategory' => '11225',
    'sDescription' => 'Personnel are unclear of levels of authority.',
  ),
  189 => 
  array (
    'fkContext' => '11438',
    'fkCategory' => '11225',
    'sDescription' => 'Personnel do not possess and maintain the level of competence that allows them to accomplish their assigned duties.',
  ),
  190 => 
  array (
    'fkContext' => '11439',
    'fkCategory' => '11225',
    'sDescription' => 'Personnel do not understand the importance of developing and implementing good internal control',
  ),
  191 => 
  array (
    'fkContext' => '11440',
    'fkCategory' => '11225',
    'sDescription' => 'Personnel enter into contracts or agreements that are beyond their scope of authority.',
  ),
  192 => 
  array (
    'fkContext' => '11441',
    'fkCategory' => '11225',
    'sDescription' => 'Personnel missing industry specific knowledge.',
  ),
  193 => 
  array (
    'fkContext' => '11442',
    'fkCategory' => '11225',
    'sDescription' => 'Poorly maintained or inadequate equipment.',
  ),
  194 => 
  array (
    'fkContext' => '11443',
    'fkCategory' => '11225',
    'sDescription' => 'Process has not properly segregated key duties and responsibilities.',
  ),
  195 => 
  array (
    'fkContext' => '11444',
    'fkCategory' => '11225',
    'sDescription' => 'Receipts are for amounts different than invoiced amounts, or are not identifiable.',
  ),
  196 => 
  array (
    'fkContext' => '11445',
    'fkCategory' => '11225',
    'sDescription' => 'Record-keeping requirements are disregarded.',
  ),
  197 => 
  array (
    'fkContext' => '11446',
    'fkCategory' => '11225',
    'sDescription' => 'Records are lost or prematurely destroyed.',
  ),
  198 => 
  array (
    'fkContext' => '11447',
    'fkCategory' => '11225',
    'sDescription' => 'Requisitions may be lost.',
  ),
  199 => 
  array (
    'fkContext' => '11448',
    'fkCategory' => '11225',
    'sDescription' => 'Reuse of supporting documents.',
  ),
  200 => 
  array (
    'fkContext' => '11449',
    'fkCategory' => '11225',
    'sDescription' => 'Technology may not be adequately defined.',
  ),
  201 => 
  array (
    'fkContext' => '11450',
    'fkCategory' => '11225',
    'sDescription' => 'Training requirements may not be adequately identified.',
  ),
  202 => 
  array (
    'fkContext' => '11451',
    'fkCategory' => '11225',
    'sDescription' => 'Transactions are not accounted for using numerical sequences.',
  ),
  203 => 
  array (
    'fkContext' => '11452',
    'fkCategory' => '11225',
    'sDescription' => 'Transactions are not entered in a timely manner.',
  ),
  204 => 
  array (
    'fkContext' => '11453',
    'fkCategory' => '11225',
    'sDescription' => 'Transfer documentation may be lost.',
  ),
  205 => 
  array (
    'fkContext' => '11454',
    'fkCategory' => '11225',
    'sDescription' => 'Transfer documents may be lost.',
  ),
  206 => 
  array (
    'fkContext' => '11455',
    'fkCategory' => '11225',
    'sDescription' => 'Transfer procedures do not require preparation of supporting documentation: intercompany goods movement.',
  ),
  207 => 
  array (
    'fkContext' => '11456',
    'fkCategory' => '11225',
    'sDescription' => 'Unauthorized access.',
  ),
  208 => 
  array (
    'fkContext' => '11457',
    'fkCategory' => '11225',
    'sDescription' => 'Unavailability of service personnel.',
  ),
  209 => 
  array (
    'fkContext' => '11458',
    'fkCategory' => '11225',
    'sDescription' => 'Untrained staff.',
  ),
  210 => 
  array (
    'fkContext' => '11459',
    'fkCategory' => '11225',
    'sDescription' => 'Variances are computed or recorded inaccurately.',
  ),
  211 => 
  array (
    'fkContext' => '11460',
    'fkCategory' => '11430',
    'sDescription' => 'Customer order information may be unclear, inaccurate or incomplete.',
  ),
  212 => 
  array (
    'fkContext' => '11461',
    'fkCategory' => '11430',
    'sDescription' => 'Customer orders may not be authorized.',
  ),
  213 => 
  array (
    'fkContext' => '11462',
    'fkCategory' => '11430',
    'sDescription' => 'Customers delay remittance.',
  ),
  214 => 
  array (
    'fkContext' => '11463',
    'fkCategory' => '11430',
    'sDescription' => 'Fictitious documentation is created.',
  ),
  215 => 
  array (
    'fkContext' => '11464',
    'fkCategory' => '11430',
    'sDescription' => 'Handling and storage procedures, including storage containers, facilities and maintenance, are inappropriate for the nature of the products.',
  ),
  216 => 
  array (
    'fkContext' => '11465',
    'fkCategory' => '11430',
    'sDescription' => 'Inaccurate or untimely pricing and inventory information.',
  ),
  217 => 
  array (
    'fkContext' => '11466',
    'fkCategory' => '11430',
    'sDescription' => 'Inappropriate handling and storage policies and procedures.',
  ),
  218 => 
  array (
    'fkContext' => '11467',
    'fkCategory' => '11430',
    'sDescription' => 'Inappropriate or unclear specifications.',
  ),
  219 => 
  array (
    'fkContext' => '11468',
    'fkCategory' => '11430',
    'sDescription' => 'Inappropriate production specifications.',
  ),
  220 => 
  array (
    'fkContext' => '11469',
    'fkCategory' => '11430',
    'sDescription' => 'Incomplete or inaccurate Information from order processing.',
  ),
  221 => 
  array (
    'fkContext' => '11470',
    'fkCategory' => '11430',
    'sDescription' => 'Incomplete or inaccurate information regarding materials transferred to / from storage.',
  ),
  222 => 
  array (
    'fkContext' => '11471',
    'fkCategory' => '11430',
    'sDescription' => 'Incomplete, untimely or inaccurate credit information.',
  ),
  223 => 
  array (
    'fkContext' => '11472',
    'fkCategory' => '11430',
    'sDescription' => 'Information on issued purchase orders is not clearly or completely communicated.',
  ),
  224 => 
  array (
    'fkContext' => '11473',
    'fkCategory' => '11430',
    'sDescription' => 'Information system does not identify available discounts and related required payment dates.',
  ),
  225 => 
  array (
    'fkContext' => '11474',
    'fkCategory' => '11430',
    'sDescription' => 'Insufficient goods movement administration.',
  ),
  226 => 
  array (
    'fkContext' => '11475',
    'fkCategory' => '11430',
    'sDescription' => 'Insufficient number of customer service representatives or service personnel.',
  ),
  227 => 
  array (
    'fkContext' => '11476',
    'fkCategory' => '11430',
    'sDescription' => 'Insufficient or excess raw materials due to poor communication with procurement, or inaccurate or untimely material requirement forecasts.',
  ),
  228 => 
  array (
    'fkContext' => '11477',
    'fkCategory' => '11430',
    'sDescription' => 'Insufficient storage capacity.',
  ),
  229 => 
  array (
    'fkContext' => '11478',
    'fkCategory' => '11430',
    'sDescription' => 'Invalid accounts payable fraudulently created for unauthorized or nonexistent purchases.',
  ),
  230 => 
  array (
    'fkContext' => '11479',
    'fkCategory' => '11430',
    'sDescription' => 'Lack of adequate systems.',
  ),
  231 => 
  array (
    'fkContext' => '11480',
    'fkCategory' => '11430',
    'sDescription' => 'Lack of customer information.',
  ),
  232 => 
  array (
    'fkContext' => '11481',
    'fkCategory' => '11430',
    'sDescription' => 'Mismatch of economical stock situation and physical stocks; interruption in production process due to stock not being there.',
  ),
  233 => 
  array (
    'fkContext' => '11482',
    'fkCategory' => '11430',
    'sDescription' => 'Order documentation is lost.',
  ),
  234 => 
  array (
    'fkContext' => '11483',
    'fkCategory' => '11430',
    'sDescription' => 'Poor communication with marketing regarding sales forecasts.',
  ),
  235 => 
  array (
    'fkContext' => '11484',
    'fkCategory' => '11430',
    'sDescription' => 'Poor performance of distributors.',
  ),
  236 => 
  array (
    'fkContext' => '11485',
    'fkCategory' => '11430',
    'sDescription' => 'Product is not produced according quality control standards.',
  ),
  237 => 
  array (
    'fkContext' => '11486',
    'fkCategory' => '11430',
    'sDescription' => 'Product is unavailable in sufficient quantity.',
  ),
  238 => 
  array (
    'fkContext' => '11487',
    'fkCategory' => '11430',
    'sDescription' => 'Product unavailability.',
  ),
  239 => 
  array (
    'fkContext' => '11488',
    'fkCategory' => '11430',
    'sDescription' => 'Purchase order specifications are unclear.',
  ),
  240 => 
  array (
    'fkContext' => '11489',
    'fkCategory' => '11430',
    'sDescription' => 'Purchase orders are lost or not forwarded to inbound activities.',
  ),
  241 => 
  array (
    'fkContext' => '11490',
    'fkCategory' => '11430',
    'sDescription' => 'Purchase orders are not entered into the system on a timely basis.',
  ),
  242 => 
  array (
    'fkContext' => '11491',
    'fkCategory' => '11430',
    'sDescription' => 'Purchase orders may be lost.',
  ),
  243 => 
  array (
    'fkContext' => '11492',
    'fkCategory' => '11430',
    'sDescription' => 'Quantities to be produced are not communicated clearly.',
  ),
  244 => 
  array (
    'fkContext' => '11493',
    'fkCategory' => '11430',
    'sDescription' => 'Sales orders are lost.',
  ),
  245 => 
  array (
    'fkContext' => '11494',
    'fkCategory' => '11430',
    'sDescription' => 'Sales personnel are unaware of marketing strategies.',
  ),
  246 => 
  array (
    'fkContext' => '11495',
    'fkCategory' => '11430',
    'sDescription' => 'Sales personnel disregard marketing strategies.',
  ),
  247 => 
  array (
    'fkContext' => '11496',
    'fkCategory' => '11430',
    'sDescription' => 'Salespeople lack knowledge about product features or benefits.',
  ),
  248 => 
  array (
    'fkContext' => '11497',
    'fkCategory' => '11430',
    'sDescription' => 'Salespeople perform poorly.',
  ),
  249 => 
  array (
    'fkContext' => '11498',
    'fkCategory' => '11430',
    'sDescription' => 'Uncommunicated changes in warranty policies.',
  ),
  250 => 
  array (
    'fkContext' => '11499',
    'fkCategory' => '11430',
    'sDescription' => 'Unordered or unauthorized products are included in customer shipment.',
  ),
  251 => 
  array (
    'fkContext' => '11500',
    'fkCategory' => '11430',
    'sDescription' => 'Untimely processing of order information.',
  ),
  252 => 
  array (
    'fkContext' => '11501',
    'fkCategory' => '11431',
    'sDescription' => 'Access to data, files, and programs are not controlled.',
  ),
  253 => 
  array (
    'fkContext' => '11502',
    'fkCategory' => '11431',
    'sDescription' => 'Acquisition documentation may be lost or otherwise not communicated to proper personnel.',
  ),
  254 => 
  array (
    'fkContext' => '11503',
    'fkCategory' => '11431',
    'sDescription' => 'Asset disposals or transfers may not be communicated to proper personnel.',
  ),
  255 => 
  array (
    'fkContext' => '11504',
    'fkCategory' => '11431',
    'sDescription' => 'Certain jobs, activities or locations are hazardous.',
  ),
  256 => 
  array (
    'fkContext' => '11505',
    'fkCategory' => '11431',
    'sDescription' => 'Data entry edit checks are not conducted.',
  ),
  257 => 
  array (
    'fkContext' => '11506',
    'fkCategory' => '11431',
    'sDescription' => 'Discontinuity of business processes.',
  ),
  258 => 
  array (
    'fkContext' => '11507',
    'fkCategory' => '11431',
    'sDescription' => 'Dishonest employees.',
  ),
  259 => 
  array (
    'fkContext' => '11508',
    'fkCategory' => '11431',
    'sDescription' => 'Disruption of normal shipping channels.',
  ),
  260 => 
  array (
    'fkContext' => '11509',
    'fkCategory' => '11431',
    'sDescription' => 'Downtime as result from inadequate skilled labor.',
  ),
  261 => 
  array (
    'fkContext' => '11510',
    'fkCategory' => '11431',
    'sDescription' => 'Downtime as result of natural or other disasters.',
  ),
  262 => 
  array (
    'fkContext' => '11511',
    'fkCategory' => '11431',
    'sDescription' => 'Downtime as result of poorly maintained, misused or obsolete equipment.',
  ),
  263 => 
  array (
    'fkContext' => '11512',
    'fkCategory' => '11431',
    'sDescription' => 'Due date information is not available.',
  ),
  264 => 
  array (
    'fkContext' => '11513',
    'fkCategory' => '11431',
    'sDescription' => 'Employees are not familiar with handling and storage requirements or procedures.',
  ),
  265 => 
  array (
    'fkContext' => '11514',
    'fkCategory' => '11431',
    'sDescription' => 'Employees ignore safety policies or procedures.',
  ),
  266 => 
  array (
    'fkContext' => '11515',
    'fkCategory' => '11431',
    'sDescription' => 'Excessive work steps/operations.',
  ),
  267 => 
  array (
    'fkContext' => '11516',
    'fkCategory' => '11431',
    'sDescription' => 'Fictitious documentation is created.',
  ),
  268 => 
  array (
    'fkContext' => '11517',
    'fkCategory' => '11431',
    'sDescription' => 'Handling and storage procedures, including storage containers, facilities and maintenance, are inappropriate for the nature of the products.',
  ),
  269 => 
  array (
    'fkContext' => '11518',
    'fkCategory' => '11431',
    'sDescription' => 'Improper cutoff of shipments at the end of a period.',
  ),
  270 => 
  array (
    'fkContext' => '11519',
    'fkCategory' => '11431',
    'sDescription' => 'Improper organization of storage facility.',
  ),
  271 => 
  array (
    'fkContext' => '11520',
    'fkCategory' => '11431',
    'sDescription' => 'Improper products or improper quantities are retrieved from storage.',
  ),
  272 => 
  array (
    'fkContext' => '11521',
    'fkCategory' => '11431',
    'sDescription' => 'Improperly trained service personnel.',
  ),
  273 => 
  array (
    'fkContext' => '11522',
    'fkCategory' => '11431',
    'sDescription' => 'Inability to identify the stage of production.',
  ),
  274 => 
  array (
    'fkContext' => '11523',
    'fkCategory' => '11431',
    'sDescription' => 'Inaccurate or incomplete shipping documents.',
  ),
  275 => 
  array (
    'fkContext' => '11524',
    'fkCategory' => '11431',
    'sDescription' => 'Inadequate physical security over fixed assets.',
  ),
  276 => 
  array (
    'fkContext' => '11525',
    'fkCategory' => '11431',
    'sDescription' => 'Inadequate physical security over goods received.',
  ),
  277 => 
  array (
    'fkContext' => '11526',
    'fkCategory' => '11431',
    'sDescription' => 'Inappropriate handling and storage policies and procedures.',
  ),
  278 => 
  array (
    'fkContext' => '11527',
    'fkCategory' => '11431',
    'sDescription' => 'Inappropriate production specifications.',
  ),
  279 => 
  array (
    'fkContext' => '11528',
    'fkCategory' => '11431',
    'sDescription' => 'Incomplete or inaccurate Information from order processing.',
  ),
  280 => 
  array (
    'fkContext' => '11529',
    'fkCategory' => '11431',
    'sDescription' => 'Incomplete or inaccurate information regarding materials transferred to / from storage.',
  ),
  281 => 
  array (
    'fkContext' => '11530',
    'fkCategory' => '11431',
    'sDescription' => 'Incorrect information is entered on shipping documentation.',
  ),
  282 => 
  array (
    'fkContext' => '11531',
    'fkCategory' => '11431',
    'sDescription' => 'Information on materials received is not entered into the information system accurately or on a timely basis by goods receiver.',
  ),
  283 => 
  array (
    'fkContext' => '11532',
    'fkCategory' => '11431',
    'sDescription' => 'Insufficient goods movement administration.',
  ),
  284 => 
  array (
    'fkContext' => '11533',
    'fkCategory' => '11431',
    'sDescription' => 'Insufficient or excess raw materials due to poor communication with procurement, or inaccurate or untimely material requirement forecasts.',
  ),
  285 => 
  array (
    'fkContext' => '11534',
    'fkCategory' => '11431',
    'sDescription' => 'Insufficient storage capacity.',
  ),
  286 => 
  array (
    'fkContext' => '11535',
    'fkCategory' => '11431',
    'sDescription' => 'Interruption of production process due to lack of materials.',
  ),
  287 => 
  array (
    'fkContext' => '11536',
    'fkCategory' => '11431',
    'sDescription' => 'Lack of adequate systems.',
  ),
  288 => 
  array (
    'fkContext' => '11537',
    'fkCategory' => '11431',
    'sDescription' => 'Lost receiving reports or lost shipping records.',
  ),
  289 => 
  array (
    'fkContext' => '11538',
    'fkCategory' => '11431',
    'sDescription' => 'Materials are not tested for specification compliance.',
  ),
  290 => 
  array (
    'fkContext' => '11539',
    'fkCategory' => '11431',
    'sDescription' => 'Materials not requisitioned are transferred.',
  ),
  291 => 
  array (
    'fkContext' => '11540',
    'fkCategory' => '11431',
    'sDescription' => 'Mismatch of economical stock situation and physical stocks; interruption in production process due to stock not being there.',
  ),
  292 => 
  array (
    'fkContext' => '11541',
    'fkCategory' => '11431',
    'sDescription' => 'Order documentation is lost.',
  ),
  293 => 
  array (
    'fkContext' => '11542',
    'fkCategory' => '11431',
    'sDescription' => 'Order or shipping documentation may be lost.',
  ),
  294 => 
  array (
    'fkContext' => '11543',
    'fkCategory' => '11431',
    'sDescription' => 'Out-of-date production facilities.',
  ),
  295 => 
  array (
    'fkContext' => '11544',
    'fkCategory' => '11431',
    'sDescription' => 'Packing materials, containers or procedures are inappropriate for the nature of the product or method of shipment.',
  ),
  296 => 
  array (
    'fkContext' => '11545',
    'fkCategory' => '11431',
    'sDescription' => 'Plans and schedules are not communicated to inbound activities, or do not clearly identify when goods can be received or where materials are needed.',
  ),
  297 => 
  array (
    'fkContext' => '11546',
    'fkCategory' => '11431',
    'sDescription' => 'Poor communication of operations\' or other activities\' needs.',
  ),
  298 => 
  array (
    'fkContext' => '11547',
    'fkCategory' => '11431',
    'sDescription' => 'Poor organization of customer service department.',
  ),
  299 => 
  array (
    'fkContext' => '11548',
    'fkCategory' => '11431',
    'sDescription' => 'Poorly organized production process.',
  ),
  300 => 
  array (
    'fkContext' => '11549',
    'fkCategory' => '11431',
    'sDescription' => 'Pressure to meet production deadlines.',
  ),
  301 => 
  array (
    'fkContext' => '11550',
    'fkCategory' => '11431',
    'sDescription' => 'Product is not produced according quality control standards.',
  ),
  302 => 
  array (
    'fkContext' => '11551',
    'fkCategory' => '11431',
    'sDescription' => 'Product is unavailable in sufficient quantity.',
  ),
  303 => 
  array (
    'fkContext' => '11552',
    'fkCategory' => '11431',
    'sDescription' => 'Product may be moved into or out of storage without proper authorization.',
  ),
  304 => 
  array (
    'fkContext' => '11553',
    'fkCategory' => '11431',
    'sDescription' => 'Product moved into or out of storage may not be documented or recorded.',
  ),
  305 => 
  array (
    'fkContext' => '11554',
    'fkCategory' => '11431',
    'sDescription' => 'Product unavailability.',
  ),
  306 => 
  array (
    'fkContext' => '11555',
    'fkCategory' => '11431',
    'sDescription' => 'Production processes do not include procedures designed to ensure quality production.',
  ),
  307 => 
  array (
    'fkContext' => '11556',
    'fkCategory' => '11431',
    'sDescription' => 'Purchase orders may be lost.',
  ),
  308 => 
  array (
    'fkContext' => '11557',
    'fkCategory' => '11431',
    'sDescription' => 'Quality problems are not discovered or appropriately reported during the production process.',
  ),
  309 => 
  array (
    'fkContext' => '11558',
    'fkCategory' => '11431',
    'sDescription' => 'Quantities to be produced are not communicated clearly.',
  ),
  310 => 
  array (
    'fkContext' => '11559',
    'fkCategory' => '11431',
    'sDescription' => 'Receiving information may be entered inaccurately in the information system, or may not be timely.',
  ),
  311 => 
  array (
    'fkContext' => '11560',
    'fkCategory' => '11431',
    'sDescription' => 'Several products compete for concurrent production.',
  ),
  312 => 
  array (
    'fkContext' => '11561',
    'fkCategory' => '11431',
    'sDescription' => 'Shipping documents are lost.',
  ),
  313 => 
  array (
    'fkContext' => '11562',
    'fkCategory' => '11431',
    'sDescription' => 'Unavailable or inaccurate information on items ordered but not received.',
  ),
  314 => 
  array (
    'fkContext' => '11563',
    'fkCategory' => '11431',
    'sDescription' => 'Uncommunicated changes in warranty policies.',
  ),
  315 => 
  array (
    'fkContext' => '11564',
    'fkCategory' => '11431',
    'sDescription' => 'Unordered or unauthorized products are included in customer shipment.',
  ),
  316 => 
  array (
    'fkContext' => '11565',
    'fkCategory' => '11431',
    'sDescription' => 'Untimely processing of order information.',
  ),
  317 => 
  array (
    'fkContext' => '11566',
    'fkCategory' => '11431',
    'sDescription' => 'Use of inefficient shipping methods.',
  ),
  318 => 
  array (
    'fkContext' => '11567',
    'fkCategory' => '11431',
    'sDescription' => 'Vendors\' inability to provide needed quantities due to other higher-priority orders or an interruption in their own supplies.',
  ),
  319 => 
  array (
    'fkContext' => '11568',
    'fkCategory' => '11430',
    'sDescription' => 'Sales orders are lost.',
  ),
  320 => 
  array (
    'fkContext' => '11569',
    'fkCategory' => '11432',
    'sDescription' => 'Acquisition documentation may be lost or otherwise not communicated to proper personnel.',
  ),
  321 => 
  array (
    'fkContext' => '11570',
    'fkCategory' => '11432',
    'sDescription' => 'Certain jobs, activities or locations are hazardous.',
  ),
  322 => 
  array (
    'fkContext' => '11571',
    'fkCategory' => '11432',
    'sDescription' => 'Downtime as result from inadequate skilled labor.',
  ),
  323 => 
  array (
    'fkContext' => '11572',
    'fkCategory' => '11432',
    'sDescription' => 'Downtime as result of poorly maintained, misused or obsolete equipment.',
  ),
  324 => 
  array (
    'fkContext' => '11573',
    'fkCategory' => '11432',
    'sDescription' => 'Due date information is not available.',
  ),
  325 => 
  array (
    'fkContext' => '11574',
    'fkCategory' => '11432',
    'sDescription' => 'Fictitious documentation is created.',
  ),
  326 => 
  array (
    'fkContext' => '11575',
    'fkCategory' => '11432',
    'sDescription' => 'Inadequate physical security over fixed assets.',
  ),
  327 => 
  array (
    'fkContext' => '11576',
    'fkCategory' => '11432',
    'sDescription' => 'Inadequate physical security over goods received.',
  ),
  328 => 
  array (
    'fkContext' => '11577',
    'fkCategory' => '11432',
    'sDescription' => 'Inadequate product testing.',
  ),
  329 => 
  array (
    'fkContext' => '11578',
    'fkCategory' => '11432',
    'sDescription' => 'Inappropriate or unclear specifications.',
  ),
  330 => 
  array (
    'fkContext' => '11579',
    'fkCategory' => '11432',
    'sDescription' => 'Inappropriate production specifications.',
  ),
  331 => 
  array (
    'fkContext' => '11580',
    'fkCategory' => '11432',
    'sDescription' => 'Interruption of production process due to lack of materials.',
  ),
  332 => 
  array (
    'fkContext' => '11581',
    'fkCategory' => '11432',
    'sDescription' => 'Materials are not tested for specification compliance.',
  ),
  333 => 
  array (
    'fkContext' => '11582',
    'fkCategory' => '11432',
    'sDescription' => 'Materials not requisitioned are transferred.',
  ),
  334 => 
  array (
    'fkContext' => '11583',
    'fkCategory' => '11432',
    'sDescription' => 'Mismatch of economical stock situation and physical stocks; interruption in production process due to stock not being there.',
  ),
  335 => 
  array (
    'fkContext' => '11584',
    'fkCategory' => '11432',
    'sDescription' => 'Out-of-date production facilities.',
  ),
  336 => 
  array (
    'fkContext' => '11585',
    'fkCategory' => '11432',
    'sDescription' => 'Patents relevant in the market place are not known.',
  ),
  337 => 
  array (
    'fkContext' => '11586',
    'fkCategory' => '11432',
    'sDescription' => 'Product is not produced according quality control standards.',
  ),
  338 => 
  array (
    'fkContext' => '11587',
    'fkCategory' => '11432',
    'sDescription' => 'Relevant patents may not be identified.',
  ),
  339 => 
  array (
    'fkContext' => '11588',
    'fkCategory' => '11432',
    'sDescription' => 'Uncommunicated changes in warranty policies.',
  ),
  340 => 
  array (
    'fkContext' => '11589',
    'fkCategory' => '11432',
    'sDescription' => 'Vendors\' inability to provide needed quantities due to other higher-priority orders or an interruption in their own supplies.',
  ),
  341 => 
  array (
    'fkContext' => '11590',
    'fkCategory' => '11433',
    'sDescription' => 'Acquisition documentation may be lost or otherwise not communicated to proper personnel.',
  ),
  342 => 
  array (
    'fkContext' => '11591',
    'fkCategory' => '11433',
    'sDescription' => 'Asset disposals or transfers may not be communicated to proper personnel.',
  ),
  343 => 
  array (
    'fkContext' => '11592',
    'fkCategory' => '11433',
    'sDescription' => 'Compensation and benefits are less than offered by other companies.',
  ),
  344 => 
  array (
    'fkContext' => '11593',
    'fkCategory' => '11433',
    'sDescription' => 'Dishonest employees.',
  ),
  345 => 
  array (
    'fkContext' => '11594',
    'fkCategory' => '11433',
    'sDescription' => 'Downtime as result from inadequate skilled labor.',
  ),
  346 => 
  array (
    'fkContext' => '11595',
    'fkCategory' => '11433',
    'sDescription' => 'Eligible employees are improperly excluded from participation.',
  ),
  347 => 
  array (
    'fkContext' => '11596',
    'fkCategory' => '11433',
    'sDescription' => 'Employee carelessness.',
  ),
  348 => 
  array (
    'fkContext' => '11597',
    'fkCategory' => '11433',
    'sDescription' => 'Errors are made in calculating benefits.',
  ),
  349 => 
  array (
    'fkContext' => '11598',
    'fkCategory' => '11433',
    'sDescription' => 'Hours are not authorized or are inaccurate.',
  ),
  350 => 
  array (
    'fkContext' => '11599',
    'fkCategory' => '11433',
    'sDescription' => 'Human resource personnel are unaware of the records that must be retained to demonstrate compliance with applicable laws and regulations.',
  ),
  351 => 
  array (
    'fkContext' => '11600',
    'fkCategory' => '11433',
    'sDescription' => 'Human resource records are not subject to proper security procedures.',
  ),
  352 => 
  array (
    'fkContext' => '11601',
    'fkCategory' => '11433',
    'sDescription' => 'Improperly trained service personnel.',
  ),
  353 => 
  array (
    'fkContext' => '11602',
    'fkCategory' => '11433',
    'sDescription' => 'Inaccurate employee information is provided to benefits personnel.',
  ),
  354 => 
  array (
    'fkContext' => '11603',
    'fkCategory' => '11433',
    'sDescription' => 'Ineffective safety and employee training programs.',
  ),
  355 => 
  array (
    'fkContext' => '11604',
    'fkCategory' => '11433',
    'sDescription' => 'Nonexistent employees are entered as program participants or beneficiaries.',
  ),
  356 => 
  array (
    'fkContext' => '11605',
    'fkCategory' => '11433',
    'sDescription' => 'Not understanding moving of personnel.',
  ),
  357 => 
  array (
    'fkContext' => '11606',
    'fkCategory' => '11433',
    'sDescription' => 'Over- or under qualified candidates may be hired.',
  ),
  358 => 
  array (
    'fkContext' => '11607',
    'fkCategory' => '11433',
    'sDescription' => 'Plan benefit provisions are unclear or complex.',
  ),
  359 => 
  array (
    'fkContext' => '11608',
    'fkCategory' => '11433',
    'sDescription' => 'Program eligibility requirements are not clearly communicated to appropriate personnel.',
  ),
  360 => 
  array (
    'fkContext' => '11609',
    'fkCategory' => '11433',
    'sDescription' => 'Staff are not evaluated on regular or timely basis.',
  ),
  361 => 
  array (
    'fkContext' => '11610',
    'fkCategory' => '11433',
    'sDescription' => 'The entity may be unaware of its future staffing needs.',
  ),
  362 => 
  array (
    'fkContext' => '11611',
    'fkCategory' => '11433',
    'sDescription' => 'Time cards do not match with payroll register time (job time is not equal to shoptime).',
  ),
  363 => 
  array (
    'fkContext' => '11612',
    'fkCategory' => '11433',
    'sDescription' => 'Time cards or other source information is submitted for nonexistent employees.',
  ),
  364 => 
  array (
    'fkContext' => '11613',
    'fkCategory' => '11433',
    'sDescription' => 'Lack of good personnel.',
  ),
  365 => 
  array (
    'fkContext' => '11614',
    'fkCategory' => '11433',
    'sDescription' => 'Lack of qualified candidates.',
  ),
  366 => 
  array (
    'fkContext' => '11615',
    'fkCategory' => '11433',
    'sDescription' => 'Unauthorized lay off of personnel.',
  ),
  367 => 
  array (
    'fkContext' => '11616',
    'fkCategory' => '11433',
    'sDescription' => 'Unauthorized personnel may gain access to payroll information.',
  ),
  368 => 
  array (
    'fkContext' => '11617',
    'fkCategory' => '11434',
    'sDescription' => 'Access to data, files, and programs are not controlled.',
  ),
  369 => 
  array (
    'fkContext' => '11618',
    'fkCategory' => '11434',
    'sDescription' => 'Acquired assets may not be adequately described.',
  ),
  370 => 
  array (
    'fkContext' => '11619',
    'fkCategory' => '11434',
    'sDescription' => 'Asset disposals or transfers may not be communicated to proper personnel.',
  ),
  371 => 
  array (
    'fkContext' => '11620',
    'fkCategory' => '11434',
    'sDescription' => 'Computer operations fail to use correct programs, files and procedures.',
  ),
  372 => 
  array (
    'fkContext' => '11621',
    'fkCategory' => '11434',
    'sDescription' => 'Data entry edit checks are not conducted.',
  ),
  373 => 
  array (
    'fkContext' => '11622',
    'fkCategory' => '11434',
    'sDescription' => 'Discontinuity of business processes.',
  ),
  374 => 
  array (
    'fkContext' => '11623',
    'fkCategory' => '11434',
    'sDescription' => 'Downtime as result of poorly maintained, misused or obsolete equipment.',
  ),
  375 => 
  array (
    'fkContext' => '11624',
    'fkCategory' => '11434',
    'sDescription' => 'Inadequate information systems.',
  ),
  376 => 
  array (
    'fkContext' => '11625',
    'fkCategory' => '11434',
    'sDescription' => 'Inadequate safeguarding of IT resources.',
  ),
  377 => 
  array (
    'fkContext' => '11626',
    'fkCategory' => '11434',
    'sDescription' => 'Information system does not identify available discounts and related required payment dates.',
  ),
  378 => 
  array (
    'fkContext' => '11627',
    'fkCategory' => '11434',
    'sDescription' => 'Information systems lack application controls.',
  ),
  379 => 
  array (
    'fkContext' => '11628',
    'fkCategory' => '11434',
    'sDescription' => 'Information systems lack general controls.',
  ),
  380 => 
  array (
    'fkContext' => '11629',
    'fkCategory' => '11434',
    'sDescription' => 'Organization has and/or is developing duplicative information system capabilities.',
  ),
  381 => 
  array (
    'fkContext' => '11630',
    'fkCategory' => '11434',
    'sDescription' => 'Organization is developing information systems outside of the corporate architecture.',
  ),
  382 => 
  array (
    'fkContext' => '11631',
    'fkCategory' => '11434',
    'sDescription' => 'Out-of-date systems.',
  ),
  383 => 
  array (
    'fkContext' => '11632',
    'fkCategory' => '11434',
    'sDescription' => 'Poor back-up and recovery procedures.',
  ),
  384 => 
  array (
    'fkContext' => '11633',
    'fkCategory' => '11434',
    'sDescription' => 'Product or processes needs are not effectively communicated to Technology Development.',
  ),
  385 => 
  array (
    'fkContext' => '11634',
    'fkCategory' => '11434',
    'sDescription' => 'Programs are subjected to unauthorized modification.',
  ),
  386 => 
  array (
    'fkContext' => '11635',
    'fkCategory' => '11434',
    'sDescription' => 'Purchase orders may be lost.',
  ),
  387 => 
  array (
    'fkContext' => '11636',
    'fkCategory' => '11434',
    'sDescription' => 'Sales orders are lost.',
  ),
  388 => 
  array (
    'fkContext' => '11637',
    'fkCategory' => '11434',
    'sDescription' => 'System and program modifications are implemented incorrectly.',
  ),
  389 => 
  array (
    'fkContext' => '11638',
    'fkCategory' => '11434',
    'sDescription' => 'Systems are not designed according to user needs or are not properly implemented.',
  ),
  390 => 
  array (
    'fkContext' => '11639',
    'fkCategory' => '11434',
    'sDescription' => 'Technology development management are unaware of project priorities.',
  ),
  391 => 
  array (
    'fkContext' => '11640',
    'fkCategory' => '11434',
    'sDescription' => 'Technology Development personnel do not have technical ability to identify or develop appropriate technology.',
  ),
  392 => 
  array (
    'fkContext' => '11641',
    'fkCategory' => '11434',
    'sDescription' => 'Technology Development personnel may acquire or have knowledge that would be useful in a development program other than that with which they are associated.',
  ),
  393 => 
  array (
    'fkContext' => '11642',
    'fkCategory' => '11435',
    'sDescription' => 'Acquired assets may not be adequately described.',
  ),
  394 => 
  array (
    'fkContext' => '11643',
    'fkCategory' => '11435',
    'sDescription' => 'Bad debts.',
  ),
  395 => 
  array (
    'fkContext' => '11644',
    'fkCategory' => '11435',
    'sDescription' => 'Bills are paid before due dates.',
  ),
  396 => 
  array (
    'fkContext' => '11645',
    'fkCategory' => '11435',
    'sDescription' => 'Cash received is diverted, lost or otherwise not reported accurately to accounts receivable.',
  ),
  397 => 
  array (
    'fkContext' => '11646',
    'fkCategory' => '11435',
    'sDescription' => 'Checks clear the bank quickly.',
  ),
  398 => 
  array (
    'fkContext' => '11647',
    'fkCategory' => '11435',
    'sDescription' => 'Equity risk.',
  ),
  399 => 
  array (
    'fkContext' => '11648',
    'fkCategory' => '11435',
    'sDescription' => 'Excessive accounts receivable collection problems.',
  ),
  400 => 
  array (
    'fkContext' => '11649',
    'fkCategory' => '11435',
    'sDescription' => 'Failure to establish or maintain appropriate relationships with financing sources.',
  ),
  401 => 
  array (
    'fkContext' => '11650',
    'fkCategory' => '11435',
    'sDescription' => 'Handling cash receipts internally can delay deposit of such receipts.',
  ),
  402 => 
  array (
    'fkContext' => '11651',
    'fkCategory' => '11435',
    'sDescription' => 'Inaccurate, untimely or unavailable information regarding amounts or due dates of payments.',
  ),
  403 => 
  array (
    'fkContext' => '11652',
    'fkCategory' => '11435',
    'sDescription' => 'Inaccurate, untimely or unavailable information regarding payment due dates.',
  ),
  404 => 
  array (
    'fkContext' => '11653',
    'fkCategory' => '11435',
    'sDescription' => 'Inadequate accounting systems for allocating costs.',
  ),
  405 => 
  array (
    'fkContext' => '11654',
    'fkCategory' => '11435',
    'sDescription' => 'Inadequate physical security over cash and documents that can be used to transfer cash.',
  ),
  406 => 
  array (
    'fkContext' => '11655',
    'fkCategory' => '11435',
    'sDescription' => 'Incomplete or inaccurate Information from order processing.',
  ),
  407 => 
  array (
    'fkContext' => '11656',
    'fkCategory' => '11435',
    'sDescription' => 'Incomplete, untimely or inaccurate credit information.',
  ),
  408 => 
  array (
    'fkContext' => '11657',
    'fkCategory' => '11435',
    'sDescription' => 'Incorrect depreciation lives or methods may be used.',
  ),
  409 => 
  array (
    'fkContext' => '11658',
    'fkCategory' => '11435',
    'sDescription' => 'Information system does not identify available discounts and related required payment dates.',
  ),
  410 => 
  array (
    'fkContext' => '11659',
    'fkCategory' => '11435',
    'sDescription' => 'Invalid accounts payable fraudulently created for unauthorized or nonexistent purchases.',
  ),
  411 => 
  array (
    'fkContext' => '11660',
    'fkCategory' => '11435',
    'sDescription' => 'Lack of awareness regarding financing alternatives.',
  ),
  412 => 
  array (
    'fkContext' => '11661',
    'fkCategory' => '11435',
    'sDescription' => 'Lack of knowledge regarding investment alternatives.',
  ),
  413 => 
  array (
    'fkContext' => '11662',
    'fkCategory' => '11435',
    'sDescription' => 'Loan risk.',
  ),
  414 => 
  array (
    'fkContext' => '11663',
    'fkCategory' => '11435',
    'sDescription' => 'Pay rates or deductions are not properly authorized or are inaccurate.',
  ),
  415 => 
  array (
    'fkContext' => '11664',
    'fkCategory' => '11435',
    'sDescription' => 'Purchase orders may be lost.',
  ),
  416 => 
  array (
    'fkContext' => '11665',
    'fkCategory' => '11435',
    'sDescription' => 'Sales orders are lost.',
  ),
  417 => 
  array (
    'fkContext' => '11666',
    'fkCategory' => '11435',
    'sDescription' => 'Subsidiary ledger does not match with general ledger.',
  ),
  418 => 
  array (
    'fkContext' => '11667',
    'fkCategory' => '11435',
    'sDescription' => 'System is not designed to reflect payment schedule included in collective bargaining agreements or individual agreements with employees.',
  ),
  419 => 
  array (
    'fkContext' => '11668',
    'fkCategory' => '11435',
    'sDescription' => 'Unauthorized access to accounts payable records and stored data.',
  ),
  420 => 
  array (
    'fkContext' => '11669',
    'fkCategory' => '11435',
    'sDescription' => 'Unauthorized access to accounts receivable records and stored data.',
  ),
  421 => 
  array (
    'fkContext' => '11670',
    'fkCategory' => '11435',
    'sDescription' => 'Unauthorized additions to accounts payable.',
  ),
  422 => 
  array (
    'fkContext' => '11671',
    'fkCategory' => '11435',
    'sDescription' => 'Unauthorized input for nonexistent returns.',
  ),
  423 => 
  array (
    'fkContext' => '11672',
    'fkCategory' => '11435',
    'sDescription' => 'Unauthorized input for nonexistent returns, allowances and write-offs.',
  ),
  424 => 
  array (
    'fkContext' => '11673',
    'fkCategory' => '11435',
    'sDescription' => 'Unauthorized personnel have access to financial information.',
  ),
  425 => 
  array (
    'fkContext' => '11674',
    'fkCategory' => '11435',
    'sDescription' => 'Untimely processing of order information.',
  ),
)
?>