<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe LibraryTools Implementa metodos de manipula��o dos dados da biblioteca dos sistemas.
 *
 * <p>Classe LibraryTools Implementa metodos de manipula��o dos dados da biblioteca dos sistemas.</p>
 * @package ISMS
 * @subpackage classes
 */
abstract class LibraryTools{

   /**
   * Atributo que que possui as informa��es de cada uma das tabelas da biblioteca do isms
   * @var Array
   * @access protected
   */
  protected $caLibraryStructure;

  /**
   * Atributo que possui a identifica��o controla a build do ISMS
   * @var Array
   * @access protected
   */
  protected $caLangs;

  /**
   * Atributo que indica qual o path onde deve-se criar os scripts sql da biblioteca
   * @var Array
   * @access protected
   */
  protected $csPathToStractSQL = 'sql_arrays/';
  /**
   * Atributo que possui o path dos sql da biblioteca que se quer importar para os arrays de dados
   * @var string
   * @access private
   */
  protected $csSqlFilesPathToCreateArray = '';

  /**
  * Seta o path em que deve ser extraido os scripts sql da biblioteca.
  *
  * <p> Seta o path em que deve ser extraido os scripts sql da biblioteca.</p>
  * @access public
  * @param integer $psPath Path em que deve ser extraido os scripts sql da biblioteca
  */
  public function setPathToStractSQL($psPath){
    $this->csPathToStractSQL = $psPath;
  }
  
  /**
  * Seta o path dos arquivos sql que se quer importar.
  *
  * <p>M�todo para setar o path dos arquivos sql que se deseja importar.</p>
  * @access public
  * @param integer $psPath Path dos arquivos sql que se deseja importar
  */
  public function setSqlFilesPathToCreateArray($psPath){
    $this->csSqlFilesPathToCreateArray = $psPath;
  }
  
  /**
   * Retorna a string de data utilizada pelo banco que se est� utilizando
   *
   * <p>Retorna a string de data utilizada pelo banco referenciado por $psDBType. Esta string de data ser� utilizada para
   * inserir a data atual nos scripts de inser��o.</p>
   * @access protected
   * @param integer $psDBType Tipo do banco utilizado para o qual se deseja criar os scripts do banco
   * @return timestamp
   */
  protected function getDateByDB($psDBType){
      switch($psDBType){
      case 'postgres':
        $msDate = "NOW()";
      break;
      case 'mssql':
        $msDate = "getdate()";
      break;
      case 'oracle':
        $msDate = "SYSDATE";
      break;
      default:
      echo "WRONG DATABASE NAME <$psDBType>";
      exit();
      break;
    }
    return $msDate;
  }

  /**
   * Verifica se a string possui como primeiro caractere uma aspa simples.
   *
   * <p>Verifica se o primeiro caractere da string $psString � uma aspas siples.</p>
   * @access protected
   * @param integer $psString String que se quer testar a existencia de aspas no primeiro caractere
   * @return boolean Se a string $psString possui uma aspas simples como primeiro caractere.
   */
  protected function hasQuoteInFisrt($psString){
    $mbReturn = false;
    if(    !(strpos($psString,"'")===false) 
         && (strpos($psString,"'")==0)
      )
    {
      $mbReturn = true;
    }
    return $mbReturn;
  }

  /**
   * Verifica se a string possui como ultimo caractere uma aspas simples n�o seguida de outra aspas simples.
   *
   * <p>Verifica se o ultimo caractere da string $psString � uma aspas simples e se esta aspas simples n�o � prescedida de outra aspas simples.</p>
   * @access protected
   * @param integer $psString String que se quer testar a existencia de aspas no ultimo caractere
   * @return boolean Se a string $psString possui uma aspas simples como ultimo caractere.
   */
  protected function hasQuoteInLast($psString, $pbDebug=false){
    $mbReturn = false;
    $msString = trim($psString);
    if($pbDebug)  echo "<br/><br/> QuoteInLast=$psString#";
    if(    !(strpos($msString,"'")===false) 
         && (strrpos($msString,"'")==strlen($msString) - 1)
         && !(strrpos( substr($msString,0,strlen($msString)-1),"'")==strlen($msString) - 2 && $msString!="''")
         && (strlen($msString)>=0)
      )
    {
      $mbReturn = true;
      if($pbDebug)    echo "Yes!";
    }else{
      if($pbDebug) echo "No!";
    }
    if($pbDebug)  echo "<br/><br/><br/>";
    return $mbReturn;
  }

  /**
   * Retorna o array de alias=>tipo referente ao arquivo passado por par�metro.
   *
   * <p>Retorna o array de alias=>tipo referente ao arquivo passado por par�metro.</p>
   * @access protected
   * @param string $psFileName String que informa qual o arquivo da biblioteca que est� sendo tratado
   * @return Array $maReturn array com os elementos alias=>DB_TYPE da tabela referente ao arquivo tratado.
   */
  protected function getAliasByFileName($psFileName){
    $maReturn = array();
    foreach($this->caLibraryStructure as $maStructure){
      if($maStructure['file_name']==$psFileName){
        $maReturn = $maStructure['alias'];
        break;
      }
    }
    return $maReturn;
  }
  
  /**
   * Retira as aspas simples de inicio e fim dos campos do tipo string
   *
   * <p>Retorna o array de alias=>valor retirando as aspas simples dos campos string do array e retorna esse array.</p>
   * @access protected
   * @param Array $paValues array do tipo alias=>value com os alias e valores de um comando sql
   * @param string $psFileName String que informa qual o arquivo da biblioteca que est� sendo tratado
   * @return Array $maReturn array com os elementos alias=>valor em que foram retiradas as aspas simples do inicio e do fim dos campos do tipo string
   */
  protected function stripQuoteString($paValues,$psFileName){
    $maAlias = $this->getAliasByFileName($psFileName);
    foreach($paValues as $msAlias=>$msValue){
      $msValue = trim($msValue);
      if($maAlias[$msAlias]==DB_STRING){
        $paValues[$msAlias] = substr($msValue,1,strlen($msValue)-2);
      }else{
        $paValues[$msAlias] = $msValue;
      }
    }
    
    return $paValues;
  }

  /**
   * Parseia a string sql $psLineContent e retorna um array do tipo (alias=>value) com os valores dos campos da tabela.
   *
   * <p>Parseia a string sql $psLineContent retornando um array do tipo (alias=>value) com os valores e alias dos campos da tabela do insert sql $psLineContent.
   * @access private
   * @param Srings String contendo um comando insert de sql da biblioteca do ISMS
   * @return Array Array do tipo (alias=>valor) referente as informa��es da strign sql $psLineContent passada por par�metro.
   */
  private function getParsedString($psLineContent,$psFile){
    /*pega a string entre () dos alias do comando sql*/
    $miI = strpos($psLineContent,'(');
    $miF = strpos($psLineContent,')');
    $msAlias = substr($psLineContent,$miI+1,$miF-$miI-1);
    $maAlias = explode(',',str_replace(' ','',$msAlias));
    /*pega a string entre () dos valores dos alias do comando sql*/
    $msLineContentAux = substr($psLineContent,strpos($psLineContent,"VALUES"));
    $miIv =strpos($msLineContentAux,'(');
    $miFv = strrpos($msLineContentAux,')');
    $msValues = substr($msLineContentAux,$miIv+1,$miFv-$miIv-1);
    $maValues = explode(',',$msValues);

    $miCountValues = count($maValues);
    $maParameters = array();
    $msParValueAux="";
    $mbIsOnQuotes = false;
    /*percorre o array dos 'peda�os' de valores*/
    for($miK=0;$miK<$miCountValues;$miK++){
      /*indica se � um n�mero*/
      $mbIsNumeric = is_numeric(trim($maValues[$miK]));
      /*indica que tem uma e somente uma aspa simples no final da string indicando final de valor do comando sql*/
      $mbHasQuoteInLast = $this->hasQuoteInLast($maValues[$miK]);
      switch($miK){
        case 0:{
          /*
            primeiro elemento, caso seja n�mero, insere no array de parametros,
             caso seja seguido de "'" insere no array de par�metros. 
            default: acumula no parametro auxiliar (v�rgula estava dentro do texto do elemento)
          */
          if( $mbIsNumeric || $mbHasQuoteInLast ){
            $maParameters[ $maAlias[count($maParameters)] ] = $maValues[$miK];
          }else{
            $msParValueAux = $maValues[$miK];
            $mbInOnQuotes = true;
          }
          break;
        }
        case ($miCountValues-1):{
          /*
            ultimo elemento, se j� possui string acumulada no parametro auxiliar, coloca esse valor no array e esvazia a varialvel
            sen�o s� coloca o ultimo par�metro no array de par�metros
          */
          if($msParValueAux){
            $maParameters [ $maAlias[count($maParameters) ] ] = $msParValueAux . ',' . $maValues[$miK];
            $msParValueAux = '';
          }else{
            $maParameters[ $maAlias[count($maParameters) ] ] = $maValues[$miK];
          }
          $mbIsOnQuotes = false;
          break;
        }
        default:{
          // teste para verificar se � um n�mero ou se � um null de sql
          $mbAuxTest = ( $mbIsNumeric || $maValues[$miK]===NULL || $maValues[$miK]===null || $maValues[$miK]==='null' || is_null($maValues[$miK]) || $maValues[$miK]==="\0" );
          /*peda�o de par�metros ou parametros entre os extremos da string*/
          if($msParValueAux){
            if( ( $mbAuxTest && !$mbIsOnQuotes ) || $mbHasQuoteInLast ){
              $maParameters[ $maAlias[count($maParameters) ] ] = $msParValueAux . ',' . $maValues[$miK];
              $msParValueAux = '';
              $mbIsOnQuotes = false;
            }else{
              $msParValueAux .= ',' . $maValues[$miK];
            }
          }else{
            if( ( $mbAuxTest && !$mbIsOnQuotes ) || $mbHasQuoteInLast ){
              $maParameters[ $maAlias[count($maParameters) ] ] = $maValues[$miK];
              $mbIsOnQuotes = false;
            }else{
              $msParValueAux .= $maValues[$miK];
              $mbIsOnQuotes = true;
            }
          }
          break;
        }
      }
    }
    
    /*DEBUG -> compara a string do sql com a parceada pelo algorito e exibe as duas quando n�o forem iguais*/
    $msValuesAux = implode(',',$maParameters);
    if($msValues != $msValuesAux){
      echo "<h2>ERROR! creating parameters value in file <".$psFile."></h2>";
      echo "<h3>Value of file:</h3><br/>" .$msValues;
      echo "<h3>Parameters Value</h3>" . $msValuesAux;
      echo "<br>msLineContents=". $psLineContent;
      echo "<br/>" . $msAlias . "<br/>";
    }
    
    return $maParameters;
  }
  
  /**
   * Verifica se a string possui como ultimo caractere uma aspas simples n�o seguida de outra aspas simples.
   *
   * <p>Verifica se o primeiro caractere da string $psString � uma aspas siples.</p>
   * @access protected
   * @param integer $psString String que se quer testar a existencia de aspas nu primeiro caractere
   * @return boolean Se a string $psString possui uma aspas simples como primeiro caractere.
   */
  public function createSQLFromLibraryArray($psLang, $psDB, $pbCreateSQLFiles=false, $pbNewLineBR = false){
    if(!in_array($psLang,$this->caLangs)){
      echo "Language not supported! Language got ($psLang), languages supported (".implode(',',$this->caLangs).")";
      exit();
    }
  
    $msReturn = '';
    $psLang = strtoupper($psLang);
    $msDate = $this->getDateByDB($psDB);
    if($pbNewLineBR){
     $msLN = "<br/>";
    }else{
      $msLN = "\n";
    }
    
    foreach($this->caLibraryStructure as $miCtxType=>$maTable){
      $maFile = $GLOBALS[$maTable['name']. $psLang];
      $msFileString = '';
      $msInsertContextIni = '';
      $msEndContextFinal = '';
      if($psDB == 'mssql'){
        $msInsertContextIni = "SET IDENTITY_INSERT isms_context ON;\n\n";
        $msEndContextFinal = "\nSET IDENTITY_INSERT isms_context OFF;\n";
      }
      foreach($maFile as $maValues){
          $maAlias = $maTable['alias'];
          $msAlias = "";
          $msValues = "";
          foreach ($maValues as $miParKey=>$msParValue){
            $msAlias .= ($msAlias)? ','.$miParKey : $miParKey;
            if(($maAlias[$miParKey]) == DB_STRING){
              $msParValueAux = "'".str_replace("'","''",$msParValue)."'";
              $msValues.= ($msValues)? ','. $msParValueAux : $msParValueAux;
            }else{
              $msValues.= ($msValues)? ','.$msParValue : $msParValue;
            }
          }
          $msValues = utf8_encode($msValues);
          $msFileString .= "INSERT INTO isms_context (pkContext,nType,nState) VALUES (".$maValues['fkContext'].",".$miCtxType.",2702);".$msLN;
          $msFileString .= "INSERT INTO ".$maTable['table']." ($msAlias) VALUES ($msValues);" . $msLN;
          $msFileString .= "INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,".$maValues['fkContext'].",2901,$msDate);" . $msLN;
          $msFileString .= "INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,".$maValues['fkContext'].",2902,$msDate);" . $msLN;
          $msFileString .= $msLN;
      }
      
      /*cria os arquivos sql da biblioteca*/
      if($pbCreateSQLFiles){
        file_put_contents($this->csPathToStractSQL .$psDB .'/' . $maTable['file_name'].strtolower($psLang).'.sql', $msInsertContextIni . $msFileString . $msEndContextFinal);
      }
      
      $msReturn .= $msFileString;
    }
    return $msInsertContextIni . $msReturn . $msEndContextFinal;
  }
  
  /**
   * Cria os Arrays e os SQLs da biblioteca do ISMS. Os arrays ser�o criados na pasta 'arrays' e os sqls na pasta 'sql'.
   *
   * <p>A partir dos SQL FULL da biblioteca (FULL -> com informa��es de contexto e de context_date) cria os arrays com as informa��es das 
   * tabelas da biblioteca e tamb�m cria os SQLParseia a string sql $psLineContent retornando um array do tipo (alias=>value) com os valores
   * e alias dos campos da tabela do insert sql $psLineContent.
   * @access public
   * @param String $psLang Linguagem que se quer gerar os arrays e sql, caso n�o seja passado por par�metro, ser�o criados os arrays e sqls referente a todas as linguagens suportadas pelo sistema
   * @param Boolean $pbPreserveLFIntoSQLLine Indica se deve-se manter as quebras de linha dentro de um valor de SQL dos scripts na hora de criar os arrays e os sql simples
   */
  public function createArrayAndSimpleSQL($psLang = '', $pbPreserveLFIntoSQLLine = true){
    if(!$this->csSqlFilesPathToCreateArray){
      $maFilesName = array();
      foreach ($this->caLibraryStructure as $maStructure){
        $maFilesNames[] = $maStructure['file_name']."(language)";
      }
      echo "You Must set the filepath from where you want to get the SQL's Library to create de ISMS Array Library.<br/>You Must use the method 'setSqlFilesPathToCreateArray' to do that!<br/><br/>";
      echo "The FileStructure to be imported must have this structure name:<br>".implode('<br/>',$maFilesNames);
      exit();
    }

    $msNewLine = '';
    $msNewLineInsertScript = "\n";
    if($pbPreserveLFIntoSQLLine){
      $msNewLine = "\n";
      $msNewLineInsertScript = "\n";
    }
    if($psLang){
      $maLang = array($psLang);
    }else{
      $maLang = $this->caLangs;
    }
    
    /*percorre todas as linguagens do sistema*/
    foreach($this->caLangs as $msLanguage){
      /*percorre todos os arquivos da biblioteca*/
      foreach ($this->caLibraryStructure as $maStructure){
        $msFileString = "";
        $maContent = file($this->csSqlFilesPathToCreateArray . $maStructure['file_name'].$msLanguage . ".sql");
        $maLibraryFileContent = array();
        $msLineContent = '';
        foreach($maContent as $msContent){
          $msContent = trim(str_replace(array("\n","\r",",NULL,"),array("","",",null,"),$msContent));
          if(strpos($msContent,'INSERT INTO isms_context')===false && strpos($msContent,'INSERT INTO isms_context_date')===false && $msContent){
            if($msContent && strrpos($msContent,');')!=(strlen($msContent)-2)){
              if($msContent) $msLineContent .= $msNewLine . $msContent;
            }else{
              if($msLineContent){
                $msLineContent .= $msNewLine  . $msContent;
              }else{
                $msLineContent .= $msContent;
              }
              $msFileString .= $msLineContent . $msNewLineInsertScript;
              
              /*transforma a linha sql em um array de atributos=>valores*/
              $maLibraryFileContent[] =  $this->stripQuoteString($this->getParsedString($msLineContent, $maStructure['file_name'].$msLanguage . ".sql"),$maStructure['file_name']);
              $msLineContent = '';
            }
          }else{
            
            if($msLineContent){
             echo "strpos=".strrpos($msContent,');')."</br>";
             echo "strLen=".strlen($msContent) ."</br>";
             echo "<br/>msContent=".$msContent."<br><br><br>";
             echo "Invalid file content =".$msLineContent."<br/><br/>";
            }
            $msLineContent = '';
          }
        }
        $msFileName = str_replace('_populate','',$maStructure['file_name']) . $msLanguage;
        
        file_put_contents('sql/' . $msFileName . '.sql', $msFileString);
        file_put_contents('arrays/'. $msFileName . '.php', "<?php\n\n$" . $maStructure['name'] . strtoupper($msLanguage) . " = \n" . var_export($maLibraryFileContent,true) . "\n?>" );
      }
    }
  }

  /**
   * Cria os Arrays da biblioteca do ISMS a partir de um banco populado. 
   *
   * <p>A partir de um banco populado do ISMS � gerado os arrays contendo os dados da biblioteca do ISMS (os arquivos ser�o criados da pasta 'arrays_dump_db')
   * e alias dos campos da tabela do insert sql $psLineContent.
   * @access public
   * @param String $psDBType Indica de qual tipo de banco que ser� importado os sql
   * @param String $psDBName Indica o nome do banco que deve-se conectar para estrair os dados
   * @param String $psUserName Indica o nome do usu�rio do banco que deve-se conectar
   * @param String $psPassword Indica a senha do usu�rio do banco
   * @param String $psServer Indica em qual host est� o bando de dados que se quer extrair os dados
   * @param String $psLang Indica a linguagem em que a biblioteca pertence
   * @param Boolean $pbPreserveLFIntoSQLLine Indica se deve-se manter as quebras de linha dentro de um valor de SQL dos scripts na hora de criar os arrays e os sql simples
   */
  public function createArrayLibraryFromBD($psDBType,$psDBName,$psUserName,$psPassword,$psServer,$psLang){
    if(!in_array($psLang,$this->caLangs)){
      echo "Language not supported! Language got ($psLang), languages supported (".implode(',',$this->caLangs).")";
      exit();
    }
    $miDBType = 0;
    switch($psDBType){
      case 'postgres':
        $miDBType = DB_POSTGRES;
      break;
      case 'mssql':
        $miDBType = DB_MSSQL;
      break;
      case 'oracle':
        $miDBType = DB_ORACLE;
      break;
      default:
      echo "WRONG DATABASE NAME <$psDBType>";
      exit();
      break;
    }
    FWDWebLib::setConnection(new FWDDB($miDBType, $psDBName, $psUserName, $psPassword, $psServer));
    
    /*percorre as tabelas pegando os dados e gerando os arrays*/
    foreach ($this->caLibraryStructure as $maStructure){
      $moDataset = new FWDDBDataSet(FWDWebLib::getConnection(), $maStructure['table']);
      $maFile = array();
      //percorre os alias da tabela criando os fields pro dataset
      foreach($maStructure['alias'] as $msAlias=>$miType){
        $moDataset->addFWDDBField( new FWDDBField($msAlias,$msAlias,$miType) );
      }
      
      //percorer todas linhas da tabela pegando os dados
      $moDataset->select();
      while($moDataset->fetch()){
        $maRow = array();
        //pra cada field do dataset, pega os valores e monta o array da linha
        foreach($maStructure['alias'] as $msFieldAlias=>$miFildType){
          $mxValue = $moDataset->getFieldByAlias($msFieldAlias)->getValue();
          if($mxValue){
            $mxValue = str_replace("''","'",$mxValue);
            $maRow[$msFieldAlias] = str_replace("'","''",$mxValue);
          }else{
            if($miFildType == DB_NUMBER)
              if($mxValue === null || $mxValue === NULL || $mxValue === 'null'){
                $maRow[$msFieldAlias] = 'null';
              }else{
                $maRow[$msFieldAlias] = '0';
              }
            else
              $maRow[$msFieldAlias] = '';
          }
        }
        $maFile[] = $maRow;
      }
      
      //grava o array de dados da tabela no arquivo correspondente
      $msFileName = str_replace('_populate','',$maStructure['file_name']) . strtolower($psLang);
      file_put_contents('arrays_dump_db/'. $msFileName . '.php', "<?php\n\n$" . $maStructure['name'] . strtoupper($psLang) . " = \n" . var_export($maFile,true) . "\n?>" );
    }
  }

};
?>
