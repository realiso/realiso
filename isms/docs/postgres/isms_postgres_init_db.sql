-- Cria usuário se necessário
CREATE ROLE isms_tst LOGIN UNENCRYPTED PASSWORD 'isms' IN GROUP saasinstances
  NOINHERIT
   VALID UNTIL 'infinity';

-- Cria o banco de dados
CREATE DATABASE isms_tst_rpereira_auto
  WITH OWNER = isms_tst
       ENCODING = 'LATIN1'
       TABLESPACE = pg_default;
       
-- Permissão para utilizar o schema pgagent (para manipulação de jobs) - Não precisa disso!
-- GRANT USAGE ON SCHEMA pgagent TO isms_tst;