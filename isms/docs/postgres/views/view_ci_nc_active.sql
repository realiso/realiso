CREATE OR REPLACE VIEW view_ci_nc_active AS 
 SELECT nc.fkcontext, nc.fkcontrol, nc.fkresponsible, nc.sname, nc.nseqnumber, nc.tdescription, nc.tcause, nc.tdenialjustification, nc.nclassification, nc.fksender, nc.ncapability, nc.ddatesent
   FROM isms_context c
   JOIN ci_nc nc ON c.pkcontext = nc.fkcontext AND c.nstate <> 2705;
