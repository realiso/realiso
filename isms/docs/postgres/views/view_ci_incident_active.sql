CREATE VIEW view_ci_incident_active AS
SELECT i.fkContext, i.fkCategory, i.fkResponsible, i.sName, i.tAccountsPlan, i.tEvidences, i.nLossType,
	i.tEvidenceRequirementComment, i.dDateLimit, i.dDateFinish, i.tDisposalDescription, i.tSolutionDescription,
	i.tProductService, i.bNotEmailDP, i.bEmailDPSent, i.dDate
FROM isms_context c JOIN ci_incident i ON (c.pkContext = i.fkContext AND c.nState <> 2705);