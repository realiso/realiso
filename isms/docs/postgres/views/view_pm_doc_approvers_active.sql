CREATE VIEW view_pm_doc_approvers_active AS
SELECT fkDocument, fkUser, bHasApproved FROM pm_doc_approvers
WHERE
fkDocument NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2823 AND nState = 2705)
AND
fkUser NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2816 AND nState = 2705);