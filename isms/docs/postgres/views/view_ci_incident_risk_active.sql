CREATE VIEW view_ci_incident_risk_active AS
SELECT ir.fkContext, ir.fkRisk,ir.fkIncident
FROM ci_incident_risk ir
JOIN isms_context ctx_r ON (ctx_r.pkContext = ir.fkRisk AND ctx_r.nState <> 2705)
JOIN isms_context ctx_i ON (ctx_i.pkContext = ir.fkIncident AND ctx_i.nState <> 2705);