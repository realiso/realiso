CREATE VIEW view_isms_policy_active AS
SELECT p.fkContext, p.tDescription
FROM isms_context c JOIN isms_policy p ON (c.pkContext = p.fkContext AND c.nState <> 2705);