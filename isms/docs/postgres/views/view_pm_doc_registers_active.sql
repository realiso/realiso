CREATE VIEW view_pm_doc_registers_active AS
SELECT fkRegister, fkDocument FROM pm_doc_registers
WHERE
fkDocument NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2823 AND nState = 2705)
AND
fkRegister NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2826 AND nState = 2705);