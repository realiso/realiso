CREATE VIEW context_names AS
/*Usuario*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, t.sName as context_name FROM isms_context c JOIN isms_user t ON (c.pkContext = t.fkContext) UNION
/*Perfil*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, t.sName as context_name FROM isms_context c JOIN isms_profile t ON (c.pkContext = t.fkContext) UNION
/*Area*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, t.sName as context_name FROM isms_context c JOIN rm_area t ON (c.pkContext = t.fkContext) UNION
/*Processo*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, t.sName as context_name FROM isms_context c JOIN rm_process t ON (c.pkContext = t.fkContext) UNION
/*Ativo*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, t.sName as context_name FROM isms_context c JOIN rm_asset t ON (c.pkContext = t.fkContext) UNION
/*Risco*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, t.sName as context_name FROM isms_context c JOIN rm_risk t ON (c.pkContext = t.fkContext) UNION
/*Controle*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, t.sName as context_name FROM isms_context c JOIN rm_control t ON (c.pkContext = t.fkContext) UNION
/*Categoria*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, t.sName as context_name FROM isms_context c JOIN rm_category t ON (c.pkContext = t.fkContext) UNION
/*Evento*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, t.sDescription as context_name FROM isms_context c JOIN rm_event t ON (c.pkContext = t.fkContext) UNION
/*Melhor Pratica*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, t.sName as context_name FROM isms_context c JOIN rm_best_practice t ON (c.pkContext = t.fkContext) UNION
/*Norma*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, t.sName as context_name FROM isms_context c JOIN rm_standard t ON (c.pkContext = t.fkContext) UNION
/*Relacao Processo X Ativo*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, (p.sName || ' X ' || a.sName) as context_name FROM isms_context c JOIN rm_process_asset pa ON (c.pkContext = pa.fkContext)
JOIN rm_process p ON (pa.fkProcess = p.fkContext) JOIN rm_asset a ON (pa.fkAsset = a.fkContext) UNION
/*Secao de Melhor Pratica*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, t.sName as context_name FROM isms_context c JOIN rm_section_best_practice t ON (c.pkContext = t.fkContext) UNION
/*Relacao Risco X Controle*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, (r.sName || ' X ' || co.sName) as context_name FROM isms_context c JOIN rm_risk_control rc ON (c.pkContext = rc.fkContext)
JOIN rm_risk r ON (rc.fkRisk = r.fkContext) JOIN rm_control co ON (rc.fkControl = co.fkContext) UNION
/*Documento*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, t.sName as context_name FROM isms_context c JOIN pm_document t ON (c.pkContext = t.fkContext) UNION
/*Instancia do Documento*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, (d.sName || ' (' || t.nMajorVersion::text || '.' || t.nRevisionVersion::text) || ')' as context_name 
FROM isms_context c JOIN pm_doc_instance t ON (c.pkContext = t.fkContext) JOIN pm_document d ON (t.fkDocument = d.fkContext) UNION
/*Categoria de Incidente*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, t.sName as context_name FROM isms_context c JOIN ci_category t ON(c.pkContext = t.fkContext) UNION
/*Ocorrencia*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, CAST(t.tDescription as varchar) as context_name FROM isms_context c JOIN ci_occurrence t ON(c.pkContext = t.fkContext) UNION
/*Incidente*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, t.sName as context_name FROM isms_context c JOIN ci_incident t ON(c.pkContext = t.fkContext) UNION
/*Nao Conformidade*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, t.sName as context_name FROM isms_context c JOIN ci_nc t ON(c.pkContext = t.fkContext) UNION
/*Template de Documento*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, t.sName as context_name FROM isms_context c JOIN pm_template t ON (c.pkContext = t.fkContext) UNION
/*Relacao entre Incidente e Risco*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, '' as context_name FROM isms_context c JOIN ci_incident_risk ir ON (ir.fkContext = c.pkContext) UNION
/*Relacao entre Incidente e Controle*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, (ctrl.sName || ' -> ' || i.sName) as context_name FROM isms_context c JOIN ci_incident_control ic ON (c.pkContext = ic.fkContext)
JOIN rm_control ctrl ON (ic.fkControl = ctrl.fkContext) JOIN ci_incident i ON (ic.fkIncident = i.fkContext) UNION
/*Tolerancia ao Risco*/
SELECT c.pkcontext AS context_id, c.ntype AS context_type, c.nstate AS context_state, '' AS context_name FROM isms_context c JOIN rm_risk_limits rl ON c.pkcontext = rl.fkcontext UNION
/*Solucao da Categoria de Incidente*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, CAST(s.tProblem as varchar) as context_name FROM isms_context c JOIN ci_solution s ON(c.pkContext = s.fkContext) UNION
/*Plano de Acao*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, ap.sName as context_name FROM isms_context c JOIN ci_action_plan ap ON(c.pkContext = ap.fkContext) UNION
/*Registro*/
SELECT c.pkContext as context_id, c.nType as context_type, c.nState as context_state, r.sName as context_name FROM isms_context c JOIN pm_register r ON(c.pkContext = r.fkContext);