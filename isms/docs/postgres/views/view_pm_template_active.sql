CREATE VIEW view_pm_template_active AS
SELECT t.fkContext, t.sName, t.nContextType, t.sPath, t.sFileName, t.tDescription, t.sKeywords,t.nfilesize
FROM isms_context c JOIN pm_template t ON (c.pkContext = t.fkContext AND c.nState <> 2705);