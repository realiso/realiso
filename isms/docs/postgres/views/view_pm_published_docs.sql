CREATE VIEW view_pm_published_docs AS
SELECT
  d.fkContext,
  d.fkClassification,
  d.fkCurrentVersion,
  d.fkParent,
  d.fkAuthor,
  d.fkMainApprover,
  d.sName,
  d.tDescription,
  d.nType,
  d.sKeywords,
  d.dDateProduction,
  d.dDeadline,
  d.bFlagDeadlineAlert,
  d.bFlagDeadlineExpired,
  d.nDaysBefore,
  d.bHasApproved,
  d.fkSchedule
FROM
  pm_document d
  JOIN isms_context c ON (c.pkContext = d.fkContext)
  JOIN pm_doc_instance di ON (di.fkContext = d.fkCurrentVersion)
WHERE
  (c.nState = 2752 AND d.dDateProduction < NOW())
  OR ( c.nState = 2751 AND di.nMajorVersion > 0 )
  OR ( c.nState = 2753 AND di.dEndProduction IS NOT NULL );
