CREATE VIEW view_ci_incident_user_active AS
SELECT fkUser, fkIncident, tDescription, tActionTaken FROM ci_incident_user
WHERE
fkUser NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2816 AND nState = 2705)
AND
fkIncident NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2831 AND nState = 2705);