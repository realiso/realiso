CREATE VIEW view_rm_asset_active AS
SELECT
	a.fkContext, a.fkCategory, a.fkResponsible, a.fkSecurityResponsible, a.sName,
	a.tDescription, a.sDocument, a.nCost, a.nValue, a.bLegality, a.tJustification
FROM isms_context c JOIN rm_asset a ON (c.pkContext = a.fkContext AND c.nState <> 2705);