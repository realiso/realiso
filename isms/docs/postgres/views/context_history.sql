CREATE VIEW context_history AS
SELECT cc.fkContext as context_id, cc.dDate as context_date_created, cc.sUserName as context_creator_name, cm.dDate as context_date_modified, cm.sUserName as context_modifier_name
FROM isms_context_date cc JOIN isms_context_date cm ON (cc.fkContext = cm.fkContext AND cc.nAction < cm.nAction);