CREATE VIEW view_ci_category_active AS
SELECT cat.fkContext, cat.sName
FROM isms_context c JOIN ci_category cat ON (c.pkContext = cat.fkContext AND c.nState <> 2705);