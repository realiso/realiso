CREATE VIEW view_rm_sec_bp_active AS
SELECT s.fkContext, s.fkParent, s.sName
FROM isms_context c JOIN rm_section_best_practice s ON (c.pkContext = s.fkContext AND c.nState <> 2705);