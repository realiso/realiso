CREATE VIEW view_ci_inc_control_active AS
SELECT ic.fkContext, ic.fkControl,ic.fkIncident
FROM ci_incident_control ic
JOIN isms_context ctx_c ON (ctx_c.pkContext = ic.fkControl AND ctx_c.nState <> 2705)
JOIN isms_context ctx_i ON (ctx_i.pkContext = ic.fkIncident AND ctx_i.nState <> 2705);