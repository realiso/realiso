CREATE VIEW view_pm_register_active AS
SELECT
	r.fkContext, r.fkResponsible, r.fkClassification, r.fkDocument, r.sName, r.nPeriod, r.nValue,
	r.sStoragePlace, r.sStorageType, r.sIndexingType, r.sDisposition, r.sProtectionRequirements, r.sOrigin
FROM isms_context c JOIN pm_register r ON (c.pkContext = r.fkContext AND c.nState <> 2705);