CREATE VIEW view_pm_template_bp_active AS
SELECT tbp.fkTemplate, 
  tbp.fkBestPractice
FROM pm_template_best_practice tbp
JOIN isms_context cbp ON (cbp.pkContext = tbp.fkBestPractice AND cbp.nState <> 2705)
JOIN isms_context ct ON (ct.pkContext = tbp.fkTemplate AND ct.nState <> 2705);