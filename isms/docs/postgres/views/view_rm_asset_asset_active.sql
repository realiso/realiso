CREATE VIEW view_rm_asset_asset_active AS
SELECT fkAsset, fkDependent
FROM rm_asset_asset
WHERE
fkAsset NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2803 AND nState = 2705)
AND
fkDependent NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2803 AND nState = 2705);