CREATE VIEW view_rm_best_practice_active AS
SELECT
	bp.fkContext, bp.fkSectionBestPractice, bp.sName, bp.tDescription,
	bp.nControlType, bp.sClassification, bp.tImplementationGuide, bp.tMetric, bp.sDocument, bp.tJustification
FROM isms_context c JOIN rm_best_practice bp ON (c.pkContext = bp.fkContext AND c.nState <> 2705);