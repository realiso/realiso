CREATE VIEW view_pm_doc_context_active AS
SELECT fkContext, fkDocument FROM pm_doc_context
WHERE
fkContext NOT IN (SELECT pkContext FROM isms_context WHERE nState = 2705)
AND
fkDocument NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2823 AND nState = 2705);