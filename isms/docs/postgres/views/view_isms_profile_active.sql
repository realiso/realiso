CREATE VIEW view_isms_profile_active AS
SELECT p.fkContext, p.sName, p.nId
FROM isms_context c JOIN isms_profile p ON (c.pkContext = p.fkContext AND c.nState <> 2705);