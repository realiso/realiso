CREATE VIEW view_rm_risk_active AS
SELECT
	r.fkContext, r.fkProbabilityValueName, r.fkType, r.fkEvent, r.fkAsset, r.sName,
	r.tDescription, r.nValue, r.nValueResidual, r.tJustification, r.nAcceptMode, r.nAcceptState,
	r.sAcceptJustification, r.nCost, r.tImpact
FROM isms_context c JOIN rm_risk r ON (c.pkContext = r.fkContext AND c.nState <> 2705);