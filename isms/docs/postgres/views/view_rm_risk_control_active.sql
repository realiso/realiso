CREATE VIEW view_rm_risk_control_active AS
SELECT fkContext, fkProbabilityValueName, fkRisk, fkControl, tJustification FROM rm_risk_control
WHERE
fkRisk NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2804 AND nState = 2705)
AND
fkControl NOT IN (SELECT pkContext FROM isms_context WHERE nType = 2805 AND nState = 2705);