CREATE VIEW view_ci_inc_process_active AS
SELECT ip.fkContext, ip.fkProcess,ip.fkIncident
FROM ci_incident_process ip
JOIN isms_context ctx_p ON (ctx_p.pkContext = ip.fkProcess AND ctx_p.nState <> 2705)
JOIN isms_context ctx_i ON (ctx_i.pkContext = ip.fkIncident AND ctx_i.nState <> 2705);