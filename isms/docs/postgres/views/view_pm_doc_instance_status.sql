CREATE VIEW view_pm_doc_instance_status AS
SELECT
  d.fkContext AS document_id,
  di.fkContext AS doc_instance_id,
  CASE
    WHEN
      d.fkCurrentVersion IS NULL
      OR ( c.nState = 2751 AND di.nMajorVersion = 0 )
      THEN 2751
    WHEN
      c.nState = 2753
      AND di.dBeginProduction IS NULL
      THEN 2753
    WHEN
      d.fkCurrentVersion = di.fkContext
      AND (
        c.nState = 2752
        OR ( c.nState = 2751 AND di.nMajorVersion > 0 )
        OR ( c.nState = 2753 AND di.dEndProduction IS NOT NULL )
      )
      THEN
        CASE
          WHEN d.dDateProduction < NOW() THEN 2752
          ELSE 2756
        END
    WHEN
      di.fkDocument = d.fkContext
      AND di.fkContext != d.fkCurrentVersion
      AND di.dEndProduction IS NOT NULL
      THEN 2755
    ELSE 2754
  END AS doc_instance_status
FROM
  pm_document d
  JOIN isms_context c ON (c.pkContext = d.fkContext)
  LEFT JOIN pm_doc_instance di ON (di.fkDocument = d.fkContext);