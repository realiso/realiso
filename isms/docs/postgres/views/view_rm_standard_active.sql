CREATE VIEW view_rm_standard_active AS
SELECT
	s.fkContext, s.sName, s.tDescription, s.tApplication, s.tObjective
FROM isms_context c JOIN rm_standard s ON (c.pkContext = s.fkContext AND c.nState <> 2705);