CREATE VIEW view_pm_read_docs AS
SELECT
  d.fkContext,
  d.fkClassification,
  d.fkCurrentVersion,
  d.fkParent,
  d.fkAuthor,
  d.fkMainApprover,
  d.sName,
  d.tDescription,
  d.nType,
  d.sKeywords,
  d.dDateProduction,
  d.dDeadline,
  d.bFlagDeadlineAlert,
  d.bFlagDeadlineExpired,
  d.nDaysBefore,
  d.bHasApproved,
  d.fkSchedule
FROM
  view_pm_document_active d
  JOIN pm_doc_read_history rh ON (rh.fkInstance = d.fkCurrentVersion);