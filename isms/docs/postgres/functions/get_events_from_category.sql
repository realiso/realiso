CREATE OR REPLACE FUNCTION get_events_from_category(integer)
  RETURNS SETOF event_id AS
'DECLARE
piCategory ALIAS FOR $1;
queryCat varchar(255);
queryEvent varchar(255);
eventCat context_id%rowtype;
category context_id%rowtype;
eventSubCat context_id%rowtype;
BEGIN
     queryCat := '' SELECT fkContext FROM rm_category cat
		JOIN isms_context c ON(c.pkContext = cat.fkContext and c.nState <> 2705)	
		WHERE  fkParent = '' || piCategory;

     queryEvent := '' SELECT fkContext FROM rm_event e
		JOIN isms_context c ON(c.pkContext = e.fkContext and c.nState <> 2705)	
		WHERE fkCategory = '' || piCategory;

     FOR eventCat IN EXECUTE queryEvent LOOP
        if(eventCat.id IS NOT NULL) THEN
		RETURN NEXT eventCat;
	END IF;
     END LOOP;

     FOR category IN EXECUTE queryCat LOOP
        if(category.id IS NOT NULL) THEN
	        FOR eventSubCat IN EXECUTE '' SELECT pkEvent FROM get_events_from_category('' || category.id || '') '' LOOP
		        RETURN NEXT eventSubCat;
	        END LOOP;
	    END IF;
     END LOOP;

     RETURN ;
END'
  LANGUAGE 'plpgsql' VOLATILE;