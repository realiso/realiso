CREATE OR REPLACE FUNCTION get_bp_from_section(integer)
  RETURNS SETOF best_practices AS
'DECLARE
   piSection ALIAS FOR $1;
   querySubSec varchar(255);
   queryBPfromSec varchar(255);
   ctxBP context_id%rowtype;
   ctxSubSec context_id%rowtype;
   ctxSubSecAux context_id%rowtype;
BEGIN
     querySubSec := '' SELECT fkContext
                       FROM rm_section_best_practice bp
		       JOIN isms_context c ON(c.pkContext = bp.fkContext and c.nState <> 2705)	
                       WHERE fkParent = '' || piSection;

     queryBPfromSec := '' SELECT fkContext
                          FROM rm_best_practice bp
                          JOIN isms_context c ON(c.pkContext = bp.fkContext and c.nState <> 2705)	
                              WHERE fkSectionBestPractice = '' || piSection;

     FOR ctxBP IN EXECUTE queryBPfromSec LOOP
        if(ctxBP.id IS NOT NULL) THEN
		RETURN NEXT ctxBP;
	END IF;
     END LOOP;

     FOR ctxSubSec IN EXECUTE querySubSec LOOP
        if(ctxSubSec.id IS NOT NULL) THEN
	    FOR ctxSubSecAux IN EXECUTE '' SELECT pkBestPractice FROM get_bp_from_section( '' || ctxSubSec.id || '') '' LOOP
		RETURN NEXT ctxSubSecAux;
	    END LOOP;
	END IF;
     END LOOP;

     RETURN ;
END'
  LANGUAGE 'plpgsql' VOLATILE;