CREATE OR REPLACE FUNCTION get_area_value(integer)
  RETURNS double precision AS
'
SELECT MAX(nValue) FROM (
        SELECT nValue 
          FROM rm_process p
	  JOIN isms_context ctx1 ON(ctx1.pkContext = p.fkContext and ctx1.nState <> 2705 and p.fkArea = $1)
      UNION
        SELECT nValue
          FROM rm_area a
          JOIN isms_context ctx2 ON(ctx2.pkContext = a.fkContext and ctx2.nState <> 2705 and a.fkParent = $1)
      UNION
	SELECT 0 as value
     ) as buffer
'
  LANGUAGE 'sql' VOLATILE;