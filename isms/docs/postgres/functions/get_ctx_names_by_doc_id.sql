CREATE OR REPLACE FUNCTION get_ctx_names_by_doc_id(integer)
  RETURNS character varying AS
'
DECLARE
   ctx_names varchar(2048);
   ctx_aux_name ctx_names%rowtype;
BEGIN
  FOR ctx_aux_name IN 
    SELECT context_name 
      FROM pm_doc_context d_c 
      JOIN context_names c_n ON (c_n.context_id = d_c.fkContext) 
      JOIN isms_context c ON (c.pkContext = d_c.fkContext AND c.nState <> 2705 ) 
      WHERE d_c.fkDocument = $1
    UNION
    SELECT ''%context_scope%'' as context_name
      FROM pm_doc_context d_c
      JOIN view_isms_scope_active c_n ON (c_n.fkContext = d_c.fkContext) 
      JOIN isms_context c ON (c.pkContext = d_c.fkContext AND c.nState <> 2705 ) 
      WHERE d_c.fkDocument = $1
    UNION
    SELECT ''%context_policy%'' as context_name
      FROM pm_doc_context d_c 
      JOIN view_isms_policy_active c_n ON (c_n.fkContext = d_c.fkContext) 
      JOIN isms_context c ON (c.pkContext = d_c.fkContext AND c.nState <> 2705 ) 
      WHERE d_c.fkDocument = $1
  LOOP
     IF ((ctx_aux_name.str IS NOT NULL) AND (ctx_aux_name.str <> '''')) THEN
         IF(ctx_names <> '''') THEN
     ctx_names := ctx_names || '', '' || ctx_aux_name.str;
         ELSE
     ctx_names := ctx_aux_name.str;
         END IF;
      END IF;
  END LOOP;
     RETURN ctx_names;
END '
  LANGUAGE 'plpgsql' VOLATILE;