CREATE OR REPLACE FUNCTION get_area_tree(integer, integer)
  RETURNS SETOF tree_area AS
'DECLARE
   piParent ALIAS FOR $1;
   piLevel ALIAS FOR $2;
   maArea tree_area%rowtype;
   maAreaAux tree_area_aux%rowtype;
   miId integer;
BEGIN
     miId:=1;
     FOR maAreaAux IN EXECUTE  '' SELECT * FROM get_area_tree_aux(''|| piParent ||'',''|| piLevel ||'') '' LOOP
        maArea.pkId := miId;
        maArea.fkContext := maAreaAux.fkContext;
        maArea.sName := maAreaAux.sName;
        maArea.nLevel := maAreaAux.nLevel;
	miId:=miId+1;	
	RETURN NEXT maArea;
     END LOOP;
     RETURN ;
END'
  LANGUAGE 'plpgsql' VOLATILE;