CREATE OR REPLACE FUNCTION get_top10_process_risks()
  RETURNS SETOF process_risk_id AS
'DECLARE
   queryValidProcess varchar(255);
   queryTop10 varchar(1024);
   validProcess context_id%rowtype;
   top10ProcessRisk process_risk_id%rowtype;
BEGIN
     queryValidProcess := '' SELECT  p.fkContext FROM isms_context cont_p
                          JOIN rm_process p ON (cont_p.pkContext = p.fkContext AND cont_p.nState <> 2705) '';    

     FOR validProcess IN EXECUTE queryValidProcess LOOP
          queryTop10 := '' SELECT  p.fkContext, r.fkContext
		           FROM isms_context cont_p
		           JOIN rm_process p ON (cont_p.pkContext = p.fkContext AND cont_p.nState <> 2705)
		           JOIN rm_process_asset pa ON (p.fkContext = pa.fkProcess)
		           JOIN rm_asset a ON (pa.fkAsset = a.fkContext)
		           JOIN isms_context cont_a ON (cont_a.pkContext = a.fkContext AND cont_a.nState <> 2705)
		           JOIN rm_risk r ON (a.fkContext = r.fkAsset)
		           JOIN isms_context cont_r ON (r.fkContext = cont_r.pkContext AND cont_r.nState <> 2705)
		           WHERE p.fkContext = '' || validProcess.id || ''		
		           ORDER BY r.nValueResidual DESC
			   LIMIT 10 '';
	
           FOR top10ProcessRisk IN EXECUTE queryTop10 LOOP
                RETURN NEXT top10ProcessRisk;
           END LOOP;
     END LOOP;

     RETURN ;
END'
  LANGUAGE 'plpgsql' VOLATILE;