CREATE OR REPLACE FUNCTION get_document_parents_trash(iddoc integer) RETURNS SETOF document_id AS
'
  DECLARE
    parentId document_id;
  BEGIN
    SELECT fkParent INTO parentId FROM pm_document WHERE fkContext = idDoc;
    WHILE parentId.document_id IS NOT NULL LOOP
      RETURN NEXT parentId;
      SELECT fkParent INTO parentId FROM pm_document WHERE fkContext = parentId.document_id;
    END LOOP;
    RETURN;
  END
'
LANGUAGE 'plpgsql' VOLATILE;