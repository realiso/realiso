CREATE OR REPLACE FUNCTION get_asset_dependencies(integer)
  RETURNS SETOF asset_id AS
'DECLARE 
asset_id ALIAS FOR $1;
query_asset_dependencies varchar(255);
asset_ids context_id%rowtype;
asset_rec context_id%rowtype;
BEGIN
   query_asset_dependencies := ''SELECT fkAsset FROM view_rm_asset_asset_active  WHERE fkDependent = '' || asset_id;
   
   FOR asset_ids IN EXECUTE query_asset_dependencies LOOP
	IF(asset_ids.id IS NOT NULL) THEN
	    RETURN NEXT asset_ids;
	    FOR asset_rec IN EXECUTE ''SELECT pkAsset FROM get_asset_dependencies(''|| asset_ids.id ||'')'' LOOP					
		RETURN NEXT asset_rec;
	    END LOOP;
	END IF;
   END LOOP;
   RETURN ;
END'
  LANGUAGE 'plpgsql' VOLATILE;