DROP FUNCTION get_area_parents(integer);

DROP FUNCTION get_area_tree(integer, integer);

DROP FUNCTION get_area_tree_aux(integer, integer);

DROP FUNCTION get_area_value(integer);

DROP FUNCTION get_areas_by_user(integer);

DROP FUNCTION get_superareas_by_user(integer);

DROP FUNCTION get_asset_value(integer);

DROP FUNCTION get_bp_from_section(integer);

DROP FUNCTION get_category_parents(integer);

DROP FUNCTION get_category_tree(integer, integer);

DROP FUNCTION get_events_from_category(integer);

DROP FUNCTION get_supercategories_events(integer);

DROP FUNCTION get_process_value(integer);

DROP FUNCTION get_processes_from_area(integer);

DROP FUNCTION get_risk_parameter_reduction(integer, integer);

DROP FUNCTION get_risk_value(integer);

DROP FUNCTION get_risk_value_residual(integer);

DROP FUNCTION get_risks_from_asset(integer);

DROP FUNCTION get_section_parents(integer);

DROP FUNCTION get_section_tree(integer, integer);

DROP FUNCTION get_sub_areas(integer);

DROP FUNCTION get_sub_categories(integer);

DROP FUNCTION get_sub_sections(integer);

DROP FUNCTION get_top10_area_risks();

DROP FUNCTION get_top10_process_risks();

DROP FUNCTION get_asset_dependencies(integer);

DROP FUNCTION get_asset_dependents(integer);

DROP FUNCTION get_area_parents_trash(integer);

DROP FUNCTION get_category_parents_trash(integer);

DROP FUNCTION get_section_parents_trash(integer);

DROP FUNCTION get_document_tree(integer, integer);

DROP FUNCTION get_sub_documents(iddoc integer);

DROP FUNCTION get_document_parents(iddoc integer);

DROP FUNCTION get_document_parents_trash(iddoc integer);

DROP FUNCTION get_ctx_names_by_doc_id(integer);

DROP FUNCTION get_next_date(pdLastTime TIMESTAMP, piPeriodUnitId INT, piPeriodsCount INT);

DROP FUNCTION get_topN_revised_documents(pitruncatenumber integer);

DROP FUNCTION get_assets_from_category(integer);

DROP FUNCTION get_areas_and_subareas_by_user(integer);

DROP FUNCTION get_all_asset_and_dependencies();

DROP FUNCTION get_all_asset_and_dependents();