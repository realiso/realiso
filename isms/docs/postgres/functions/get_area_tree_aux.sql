CREATE OR REPLACE FUNCTION get_area_tree_aux(integer, integer)
  RETURNS SETOF tree_area_aux AS
'DECLARE
   piParent ALIAS FOR $1;
   piLevel ALIAS FOR $2;
   nAuxLevel integer;
   queryArea varchar(1024);
   queryAreaAux varchar(1024);
   queryAreaRecursion varchar(255);
   areaRoot tree_area_aux%rowtype;
   subArea tree_area_aux%rowtype;

BEGIN
     nAuxLevel := piLevel + 1;
     queryAreaAux := '' SELECT fkContext, sName, '' || nAuxLevel::text || '' as nLevel FROM rm_area a
		JOIN isms_context c ON(c.pkContext = a.fkContext and c.nState <> 2705)	
		WHERE  fkParent ''  ;
     IF piParent > 0 THEN
       queryArea := queryAreaAux || '' = '' || piParent;
     ELSE 
       queryArea := queryAreaAux || '' is NULL'';
     END IF;
     FOR areaRoot IN EXECUTE queryArea LOOP
        RETURN NEXT areaRoot;
        queryAreaRecursion := ''SELECT fkContext, sName, nLevel FROM get_area_tree_aux('' || areaRoot.fkContext::text || '' , '' || nAuxLevel::text || '' ) '';
        FOR subArea IN EXECUTE queryAreaRecursion LOOP
	    RETURN NEXT subArea;
        END LOOP;
     END LOOP;
     RETURN ;
END'
  LANGUAGE 'plpgsql' VOLATILE;