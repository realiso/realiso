CREATE OR REPLACE FUNCTION get_top10_area_risks()
  RETURNS SETOF area_risk_id AS
'DECLARE
   queryValidArea varchar(255);
   queryTop10 varchar(1024);
   validArea context_id%rowtype;
   top10AreaRisk area_risk_id%rowtype;
BEGIN
     queryValidArea := ''SELECT  ar.fkContext FROM isms_context cont_ar
	                         JOIN rm_area ar ON (cont_ar.pkContext = ar.fkContext AND cont_ar.nState <> 2705)'';    

     FOR validArea IN EXECUTE queryValidArea LOOP
          queryTop10 := '' SELECT ar.fkContext, r.fkContext FROM isms_context cont_ar
		      JOIN rm_area ar ON (cont_ar.pkContext = ar.fkContext AND cont_ar.nState <> 2705)   
		      JOIN rm_process p ON (ar.fkContext = p.fkArea)
		      JOIN isms_context cont_p ON (cont_p.pkContext = p.fkContext AND cont_p.nState <> 2705)
		      JOIN rm_process_asset pa ON (p.fkContext = pa.fkProcess)
		      JOIN rm_asset a ON (pa.fkAsset = a.fkContext)
		      JOIN isms_context cont_a ON (cont_a.pkContext = a.fkContext AND cont_a.nState <> 2705)
		      JOIN rm_risk r ON (a.fkContext = r.fkAsset)
		      JOIN isms_context cont_r ON (r.fkContext = cont_r.pkContext AND cont_r.nState <> 2705)
		      WHERE ar.fkContext = '' || validArea.id || ''
		      GROUP BY r.fkContext, r.nValueResidual, ar.fkContext
		      ORDER BY r.nValueResidual DESC 
		   LIMIT 10 '';
	
           FOR top10AreaRisk IN EXECUTE queryTop10 LOOP
                RETURN NEXT top10AreaRisk;
           END LOOP;
     END LOOP;

     RETURN;
END'
  LANGUAGE 'plpgsql' VOLATILE;