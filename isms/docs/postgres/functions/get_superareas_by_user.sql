CREATE OR REPLACE FUNCTION get_superareas_by_user(integer)
  RETURNS SETOF area_ids AS
'DECLARE 
    userId ALIAS FOR $1;
    query_area varchar(255);
    area area_ids%rowtype;
    area2 area_ids%rowtype;
BEGIN
   query_area := '' SELECT fkContext FROM rm_area a
		    JOIN isms_context c ON(c.pkContext = a.fkContext and c.nState <> 2705)
		      WHERE a.fkResponsible = '' || userId;
   
    FOR area IN EXECUTE query_area LOOP
	RETURN NEXT area;
        FOR area2 IN EXECUTE '' SELECT pkArea FROM get_area_parents('' || area.area_id || '') '' LOOP
	    RETURN NEXT area2;
        END LOOP;

   END LOOP;

   RETURN ;	
END'
  LANGUAGE 'plpgsql' VOLATILE;