CREATE OR REPLACE FUNCTION get_areas_by_user(integer)
  RETURNS SETOF area_ids AS
'DECLARE 
    userId ALIAS FOR $1;
    query_area varchar(255);
    area context_id%rowtype;
    subArea context_id%rowtype;
    areaParent context_id%rowtype;
BEGIN
   query_area := '' SELECT fkContext FROM rm_area a 
		    JOIN isms_context c ON(c.pkContext = a.fkContext and c.nState <> 2705) 
		        WHERE a.fkResponsible = '' || userId;

   FOR area IN EXECUTE query_area LOOP
	RETURN NEXT area;

        FOR subArea IN EXECUTE '' SELECT pkArea FROM get_area_parents('' || area.id || '') '' LOOP
	    RETURN NEXT subArea;
        END LOOP;

        FOR subArea IN EXECUTE '' SELECT pkArea FROM get_sub_areas('' || area.id || '') '' LOOP
	    RETURN NEXT subArea;
        END LOOP;

   END LOOP;

   RETURN ;	
END'
  LANGUAGE 'plpgsql' VOLATILE;