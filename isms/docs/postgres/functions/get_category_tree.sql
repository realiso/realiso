CREATE OR REPLACE FUNCTION get_category_tree(integer, integer)
  RETURNS SETOF tree_category AS
'DECLARE
piParent ALIAS FOR $1;
pnLevel ALIAS FOR $2;
nAuxLevel integer;
queryCat varchar(255);
queryCatAux varchar(255);
queryCatRecursion varchar(255);
categoryRoot tree_category%rowtype;
subCat tree_category%rowtype;

BEGIN
     nAuxLevel := pnLevel + 1;
     queryCatAux := '' SELECT fkContext, sName, '' || nAuxLevel::text || '' as nLevel FROM rm_category cat
		JOIN isms_context c ON(c.pkContext = cat.fkContext and c.nState <> 2705)	
		WHERE  fkParent ''  ;
     IF piParent > 0 THEN
       queryCat := queryCatAux || '' = '' || piParent;
     ELSE 
       queryCat := queryCatAux || '' is NULL'';
     END IF;
     FOR categoryRoot IN EXECUTE queryCat LOOP
        RETURN NEXT categoryRoot;
        queryCatRecursion := ''SELECT fkContext, sName, nLevel FROM get_category_tree('' || categoryRoot.fkContext::text || '' , '' || nAuxLevel::text || '' ) '';
        FOR subCat IN EXECUTE queryCatRecursion LOOP
	    RETURN NEXT subCat;
        END LOOP;
     END LOOP;
     RETURN ;
END'
  LANGUAGE 'plpgsql' VOLATILE;