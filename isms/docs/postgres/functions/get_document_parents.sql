CREATE OR REPLACE FUNCTION get_document_parents(iddoc integer) RETURNS SETOF document_id AS
'
  DECLARE
    parentId document_id;
  BEGIN
    SELECT fkParent INTO parentId FROM view_pm_document_active WHERE fkContext = idDoc;
    WHILE parentId.document_id IS NOT NULL LOOP
      RETURN NEXT parentId;
      SELECT fkParent INTO parentId FROM view_pm_document_active WHERE fkContext = parentId.document_id;
    END LOOP;
    RETURN;
  END
'
LANGUAGE 'plpgsql' VOLATILE;