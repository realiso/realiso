CREATE OR REPLACE FUNCTION get_section_tree(integer, integer)
  RETURNS SETOF tree_best_practice AS
'DECLARE
   piParent ALIAS FOR $1;
   piLevel ALIAS FOR $2;
   nAuxLevel integer;
   queryBP varchar(1024);
   queryBPAux varchar(1024);
   queryBPRecursion varchar(255);
   bestPracticeRoot tree_best_practice%rowtype;
   subBestPractice tree_best_practice%rowtype;

BEGIN
     nAuxLevel := piLevel + 1;
     queryBPAux := '' SELECT fkContext, sName, '' || nAuxLevel::text || '' as nLevel FROM rm_section_best_practice sbp
		JOIN isms_context c ON(c.pkContext = sbp.fkContext and c.nState <> 2705)	
		WHERE  fkParent ''  ;
     IF piParent > 0 THEN
       queryBP := queryBPAux || '' = '' || piParent;
     ELSE 
       queryBP := queryBPAux || '' is NULL'';
     END IF;
     FOR bestPracticeRoot IN EXECUTE queryBP LOOP
        RETURN NEXT bestPracticeRoot;
        queryBPRecursion := ''SELECT fkContext, sName, nLevel FROM get_section_tree('' || bestPracticeRoot.fkContext::text || '' , '' || nAuxLevel::text || '' ) '';
        FOR subBestPractice IN EXECUTE queryBPRecursion LOOP
	    RETURN NEXT subBestPractice;
        END LOOP;
     END LOOP;
     RETURN ;
END'
  LANGUAGE 'plpgsql' VOLATILE;