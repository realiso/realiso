CREATE OR REPLACE FUNCTION get_supercategories_events(piCategoryId integer) RETURNS SETOF event_id AS
'DECLARE
  mrEvent record;
  miCategoryId integer;
BEGIN
  miCategoryId = piCategoryId;
  WHILE miCategoryId IS NOT NULL LOOP
    FOR mrEvent IN SELECT fkContext FROM view_rm_event_active WHERE fkCategory = miCategoryId LOOP
      RETURN NEXT mrEvent;
    END LOOP;
    SELECT fkParent INTO miCategoryId FROM view_rm_category_active WHERE fkContext = miCategoryId;
  END LOOP;
  RETURN;
END'
LANGUAGE 'plpgsql' VOLATILE;
