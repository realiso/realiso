CREATE OR REPLACE FUNCTION get_asset_value(integer)
RETURNS double precision AS
'
 DECLARE
  miResult double precision;
  miAssetValue row_double%rowtype;
  miBlueRisk integer;
  assetCount asset_value_cont%rowtype;
BEGIN
  miResult := 0;
  miBlueRisk := 0; 

  FOR assetCount IN 
    SELECT count_sys, count_asset 
      FROM (SELECT count(pkparametername) as count_sys  FROM rm_parameter_name pn) buffer_sys,
           (SELECT count(fkparametername) as count_asset 
              FROM rm_asset_value av
                JOIN isms_context ctx ON(ctx.pkContext = av.fkAsset AND ctx.nState <> 2705 AND av.fkAsset = $1)
            ) buffer_asset
  LOOP
    IF (assetCount.cont_system <> assetCount.cont_asset) THEN
      miBlueRisk := 1;
    END IF;
  END LOOP;

 IF (miBlueRisk = 1) THEN
    miResult = 0;
 ELSE
    FOR miAssetValue IN
	SELECT MAX(value) FROM (
	  SELECT MAX(nvalueresidual) as value
	    FROM view_rm_risk_active as r
	    WHERE r.fkasset = $1 AND (nAcceptMode = 0)
	  UNION
	  SELECT MAX(0.1) as value
	    FROM view_rm_risk_active as r
	    WHERE r.fkasset = $1 AND (nAcceptMode <> 0)
	  UNION
	  SELECT MAX(nvalue) as value
	    FROM view_rm_asset_asset_active aa
	      JOIN view_rm_asset_active a ON(aa.fkAsset = a.fkContext AND aa.fkdependent = $1)
	  UNION 
	  SELECT 0 as value
	) as buffer
    LOOP
	miResult = miResult + miAssetValue.value;
    END LOOP;
  END IF;

  RETURN miResult;
END'
  LANGUAGE 'plpgsql' VOLATILE;