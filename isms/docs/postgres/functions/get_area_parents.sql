CREATE OR REPLACE FUNCTION get_area_parents(integer)
  RETURNS SETOF area_id AS
'DECLARE 
area_id ALIAS FOR $1;
query_area_parents varchar(255);
area_ids context_id%rowtype;
area_rec context_id%rowtype;
BEGIN
   query_area_parents := '' SELECT a.fkParent FROM rm_area a
			 JOIN isms_context c ON(c.pkContext = a.fkContext and c.nState <> 2705)
			     WHERE fkContext = '' ||area_id;
   
   FOR area_ids IN EXECUTE query_area_parents LOOP
	IF(area_ids.id IS NOT NULL) THEN
	    RETURN NEXT area_ids;
	    FOR area_rec IN EXECUTE ''SELECT pkArea FROM get_area_parents(''|| area_ids.id ||'')'' LOOP
		RETURN NEXT area_rec;
	    END LOOP;
	END IF;
   END LOOP;
   RETURN ;	
END'
  LANGUAGE 'plpgsql' VOLATILE;