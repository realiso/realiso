CREATE OR REPLACE FUNCTION get_processes_from_area(integer)
  RETURNS SETOF process_id AS
'DECLARE
   piArea ALIAS FOR $1;
   querySubAreas varchar(255);
   queryProcFromArea varchar(255);
   ctxProc context_id%rowtype;
   ctxArea context_id%rowtype;
   ctxProcAux context_id%rowtype;
BEGIN

     querySubAreas := '' SELECT fkContext FROM rm_area a
	JOIN isms_context c ON(c.pkContext = a.fkContext and c.nState <> 2705)	
        WHERE fkParent = '' || piArea;

     queryProcFromArea := '' SELECT fkContext FROM rm_process p
        JOIN isms_context c ON(c.pkContext = p.fkContext and c.nState <> 2705)	
        WHERE fkArea = '' || piArea;

     FOR ctxProc IN EXECUTE queryProcFromArea LOOP
        if(ctxProc.id IS NOT NULL) THEN
		RETURN NEXT ctxProc;
	END IF;
     END LOOP;

     FOR ctxArea IN EXECUTE querySubAreas LOOP
        if(ctxArea.id IS NOT NULL) THEN
	    FOR ctxProcAux IN EXECUTE '' SELECT pkProcess FROM get_processes_from_area( '' || ctxArea.id || '') '' LOOP
		RETURN NEXT ctxProcAux;
	    END LOOP;
	END IF;
     END LOOP;

     RETURN ;
END'
  LANGUAGE 'plpgsql' VOLATILE;