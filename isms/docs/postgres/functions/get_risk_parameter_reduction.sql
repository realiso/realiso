CREATE OR REPLACE FUNCTION get_risk_parameter_reduction(integer, integer)
  RETURNS integer AS
'DECLARE
  piRisk ALIAS FOR $1;
  piParameter ALIAS FOR $2;
  queryReduction varchar(1024);
  miValue integer;
  handleReduct risk_par_reduction%rowtype;
  
BEGIN
  miValue :=0;

  queryReduction :=''
  SELECT parameter_reduction_value, risk_parameter_value FROM 
  (
    SELECT SUM(rcpvn.nvalue) as parameter_reduction_value 
      FROM view_rm_risk_control_active rc
        JOIN rm_risk_control_value rcv ON (rcv.fkriskcontrol = rc.fkcontext)
        JOIN view_rm_control_active c ON (c.fkContext = rc.fkcontrol AND c.bIsActive = 1)
        JOIN rm_rc_parameter_value_name rcpvn ON (rcv.fkrcvaluename = rcpvn.pkrcvaluename)
      WHERE fkrisk = '' || @piRisk || '' AND fkparametername = '' || @piParameter || ''
  ) prv,
  (
  SELECT nvalue as risk_parameter_value 
     FROM rm_risk_value rv
        JOIN isms_context ctx ON (ctx.pkContext = rv.fkRisk AND ctx.nState <> 2705)
        JOIN rm_parameter_value_name pvn ON (rv.fkvaluename = pvn.pkvaluename)
     WHERE fkrisk = '' || @piRisk || '' AND fkparametername = '' || @piParameter || ''
  ) rpv
      '';
  FOR handleReduct IN EXECUTE queryReduction LOOP
    if(handleReduct.par_red_value IS NOT NULL) THEN
        miValue = handleReduct.risk_par_value - handleReduct.par_red_value;
        if(miValue<1) THEN
             miValue:=1;
        END IF;
    ELSE
      miValue := handleReduct.risk_par_value;
    END IF;
  END LOOP;

  RETURN miValue;
END'
  LANGUAGE 'plpgsql' VOLATILE;
