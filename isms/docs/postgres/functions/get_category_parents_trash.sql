CREATE OR REPLACE FUNCTION get_category_parents_trash(integer)
  RETURNS SETOF category_id AS
'DECLARE
piCategory ALIAS FOR $1;
queryCat varchar(255);
queryCatRecursion varchar(255);
category context_id%rowtype;
catParent context_id%rowtype;
BEGIN
     queryCat := '' SELECT cat.fkParent FROM rm_category cat WHERE  cat.fkContext = '' || piCategory;

     FOR category IN EXECUTE queryCat LOOP
	if(category.id IS NOT NULL) THEN
	    RETURN NEXT category;
	    FOR catParent IN EXECUTE ''SELECT pkCategory FROM get_category_parents_trash(''|| category.id ||'')'' LOOP
	      RETURN NEXT catParent;
	    END LOOP;
	END IF;
     END LOOP;

     RETURN ;
END'
  LANGUAGE 'plpgsql' VOLATILE;