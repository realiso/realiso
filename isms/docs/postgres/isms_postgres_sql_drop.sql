drop index CI_ACTION_PLAN_PK;

drop index FKRESPONSIBLE_FK8;

drop index CI_CATEGORY_PK;

drop index CI_INCIDENT_PK;

drop index FKCATEGORY_FK3;

drop index FKRESPONSIBLE_FK6;

drop index CI_INCIDENT_CONTROL_PK;

drop index FKCONTEXT_FK6;

drop index FKCONTROL_FK6;

drop index FKINCIDENT_FK4;

drop index CI_INCIDENT_FINANCIAL_IMPACT_PK;

drop index FKCLASSIFICATION_FK3;

drop index FKINCIDENT_FK6;

drop index CI_INCIDENT_PROCESS_PK;

drop index FKINCIDENT_FK5;

drop index FKPROCESS_FK4;

drop index CI_INCIDENT_RISK_PK2;

drop index FKINCIDENT_FK7;

drop index FKRISK_FK5;

drop index CI_INCIDENT_RISK_VALUE_PK;

drop index FKINCRISK_FK;

drop index FKPARAMETERNAME_FK4;

drop index FKVALUENAME_FK3;

drop index CI_INCIDENT_USER_PK;

drop index FKINCIDENT_FK2;

drop index FKUSER_FK8;

drop index CI_NC_PK;

drop index FKCONTROL_FK5;

drop index FKRESPONSIBLE_FK7;

drop index FKSENDER_FK;

drop index CI_NC_ACTION_PLAN_PK;

drop index FKACTIONPLAN_FK;

drop index FKNC_FK2;

drop index CI_NC_PROCESS_PK;

drop index FKNC_FK;

drop index FKPROCESS_FK3;

drop index CI_NC_SEED_PK;

drop index FKCONTROL_FK7;

drop index CI_OCURRENCE_PK;

drop index FKINCIDENT_FK;

drop index CI_RISK_VALUE_PK;

drop index FKPARAMTER_FK;

drop index FKRISK_FK4;

drop index CI_RISK_SCHEDULE_PK;

drop index CI_SOLUTION_PK;

drop index FKCATEGORY_FK4;

drop index ISMS_CONFIG_PK;

drop index ISMS_CONTEXT_PK;

drop index ISMS_CONTEXT_CLASSIFICATION_PK;

drop index FKCONTEXT_FK3;

drop index ISMS_CONTEXT_DATE_PK;

drop index FKCONTEXT_FK5;

drop index ISMS_CONTEXT_HISTORY_PK;

drop index ISMS_POLICY_PK;

drop index ISMS_PROFILE_PK;

drop index FKPROFILE_FK2;

drop index ISMS_PROFILE_ACL_PK;

drop index ISMS_SAAS_PK;

drop index ISMS_SCOPE_PK;

drop index FKSOLICITOR_FK;

drop index FKUSER_FK2;

drop index ISMS_SOLICITOR_PK;

drop index FKPROFILE_FK;

drop index ISMS_USER_PK;

drop index FKUSER_FK;

drop index ISMS_USER_PREFERENCE_PK;

drop index FKAUTHOR_FK;

drop index FKCLASSIFICATION_FK;

drop index FKCURRENTVERSION_FK;

drop index FKMAINAPPOVER_FK;

drop index FKPARENT_FK5;

drop index FKSCHEDULE_FK4;

drop index PM_DOCUMENT_PK;

drop index FKDOCUMENT_FK5;

drop index PM_DOCUMENT_REVISION_HISTORY_PK;

drop index FKDOCUMENT_FK;

drop index FKUSER_FK3;

drop index FKCONTEXT_FK4;

drop index FKDOCUMENT_FK8;

drop index FKDOCUMENT_FK2;

drop index PM_DOC_INSTANCE_PK;

drop index FKDOCUMENT_FK4;

drop index FKUSER_FK5;

drop index FKUSER_FK7;

drop index PM_DOC_READ_HISTORY_PK;

drop index FKDOCUMENT_FK3;

drop index PM_DOC_REFERENCE_PK;

drop index FKDOCUMENT_FK6;

drop index FKREGISTER_FK;

drop index FKINSTANCE_FK;

drop index FKUSER_FK4;

drop index PM_INSTANCE_COMMENT_PK;

drop index PM_INSTANCE_CONTENT_PK;

drop index FKPROCESS_FK2;

drop index FKUSER_FK6;

drop index FKCLASSIFICATION_FK2;

drop index FKDOCUMENT_FK7;

drop index FKRESPONSIBLE_FK5;

drop index PM_REGISTER_PK;

drop index FKREGISTER_FK2;

drop index FKUSER_FK9;

drop index PM_REGISTER_READERS_PK;

drop index PM_TEMPLATE_PK;

drop index FKBESTPRACTICE_FK4;

drop index FKTEMPLATE_FK;

drop index PM_TEMPLATE_BEST_PRACTICE_PK;

drop index PM_TEMPLATE_CONTENT_PK;

drop index FKPARENT_FK;

drop index FKPRIORITY_FK;

drop index FKRESPONSIBLE_FK;

drop index FKTYPE_FK;

drop index RM_AREA_PK;

drop index FKCATEGORY_FK;

drop index FKRESPONSIBLE_FK4;

drop index FKSECURITYRESPONSIBLE_FK;

drop index RM_ASSET_PK;

drop index FKASSET_FK4;

drop index FKDEPENDENT_FK;

drop index RM_ASSET_ASSET_PK;

drop index FKASSET_FK3;

drop index FKPARAMETERNAME_FK;

drop index FKVALUENAME_FK;

drop index RM_ASSET_VALUE_PK;

drop index FKSECTIONBESTPRACTICE_FK;

drop index RM_BEST_PRACTICE_PK;

drop index FKBESTPRACTICE_FK3;

drop index FKEVENT_FK2;

drop index RM_BEST_PRACTICE_EVENT_PK;

drop index FKBESTPRACTICE_FK2;

drop index FKSTANDARD_FK;

drop index RM_BEST_PRACTICE_STANDARD_PK;

drop index FKPARENT_FK2;

drop index RM_CATEGORY_PK;

drop index FKRESPONSIBLE_FK3;

drop index FKTYPE_FK3;

drop index RM_CONTROL_PK;

drop index FKBESTPRACTICE_FK;

drop index FKCONTROL_FK2;

drop index RM_CONTROL_BEST_PRACTICE_PK;

drop index RM_CONTROL_COST_PK;

drop index FKCONTROL_FK3;

drop index RM_CONTROL_EFFICIENCY_HISTORY_P;

drop index FKCONTROL_FK4;

drop index RM_CONTROL_TEST_HISTORY_PK;

drop index FKCATEGORY_FK2;

drop index FKTYPE_FK5;

drop index RM_EVENT_PK;

drop index RM_PARAMETER_NAME_PK;

drop index RM_PARAMETER_VALUE_NAME_PK;

drop index FKAREA_FK;

drop index FKPRIORITY_FK2;

drop index FKRESPONSIBLE_FK2;

drop index FKTYPE_FK2;

drop index RM_PROCESS_PK;

drop index FKASSET_FK;

drop index FKPROCESS_FK;

drop index RM_PROCESS_ASSET_PK;

drop index RM_RC_PARAMETER_VALUE_NAME_PK;

drop index FKASSET_FK2;

drop index FKEVENT_FK;

drop index FKPROBABILITYVALUENAME_FK;

drop index FKTYPE_FK4;

drop index RM_RISK_PK;

drop index FKCONTROL_FK;

drop index FKPROBABILITYVALUENAME_FK2;

drop index FKRISK_FK;

drop index RM_RISK_CONTROL_PK;

drop index FKPARAMETERNAME_FK3;

drop index FKRCVALUENAME_FK;

drop index FKRISKCONTROL_FK;

drop index RM_RISK_CONTROL_VALUE_PK;

drop index RM_RISK_LIMITS_PK;

drop index FKPARAMETERNAME_FK2;

drop index FKRISK_FK2;

drop index FKVALUENAME_FK2;

drop index RM_RISK_VALUE_PK;

drop index FKPARENT_FK3;

drop index RM_SECTION_BEST_PRACTICE_PK;

drop index RM_STANDARD_PK;

drop index FKCONTEXT_FK2;

drop index FKCREATOR_FK2;

drop index FKRECEIVER_FK2;

drop index WKF_ALERT_PK;

drop index FKSCHEDULE_FK;

drop index WKF_CONTROL_EFFICIENCY_PK;

drop index FKSCHEDULE_FK2;

drop index WKF_CONTROL_TEST_PK;

drop index WKF_SCHEDULE_PK;

drop index FKCONTEXT_FK;

drop index FKCREATOR_FK;

drop index FKRECEIVER_FK;

drop index WKF_TASK_PK;

drop index FKCONTEXT_FK7;

drop index FKSCHEDULE_FK3;

drop index WKF_TASK_SCHEDULE_PK;

drop table CI_ACTION_PLAN CASCADE;

drop table CI_CATEGORY CASCADE;

drop table CI_INCIDENT CASCADE;

drop table CI_INCIDENT_CONTROL CASCADE;

drop table CI_INCIDENT_FINANCIAL_IMPACT CASCADE;

drop table CI_INCIDENT_PROCESS CASCADE;

drop table CI_INCIDENT_RISK CASCADE;

drop table CI_INCIDENT_RISK_VALUE CASCADE;

drop table CI_INCIDENT_USER CASCADE;

drop table CI_NC CASCADE;

drop table CI_NC_ACTION_PLAN CASCADE;

drop table CI_NC_PROCESS CASCADE;

drop table CI_NC_SEED CASCADE;

drop table CI_OCCURRENCE CASCADE;

drop table CI_RISK_PROBABILITY CASCADE;

drop table CI_RISK_SCHEDULE CASCADE;

drop table CI_SOLUTION CASCADE;

drop table ISMS_AUDIT_LOG CASCADE;

drop table ISMS_CONFIG CASCADE;

drop table ISMS_CONTEXT CASCADE;

drop table ISMS_CONTEXT_CLASSIFICATION CASCADE;

drop table ISMS_CONTEXT_DATE CASCADE;

drop table ISMS_CONTEXT_HISTORY CASCADE;

drop table ISMS_POLICY CASCADE;

drop table ISMS_PROFILE CASCADE;

drop table ISMS_PROFILE_ACL CASCADE;

drop table ISMS_SAAS CASCADE;

drop table ISMS_SCOPE CASCADE;

drop table ISMS_SOLICITOR CASCADE;

drop table ISMS_USER CASCADE;

drop table ISMS_USER_PASSWORD_HISTORY CASCADE;

drop table ISMS_USER_PREFERENCE CASCADE;

drop table PM_DOCUMENT CASCADE;

drop table PM_DOCUMENT_REVISION_HISTORY CASCADE;

drop table PM_DOC_APPROVERS CASCADE;

drop table PM_DOC_CONTEXT CASCADE;

drop table PM_DOC_INSTANCE CASCADE;

drop table PM_DOC_READERS CASCADE;

drop table PM_DOC_READ_HISTORY CASCADE;

drop table PM_DOC_REFERENCE CASCADE;

drop table PM_DOC_REGISTERS CASCADE;

drop table PM_INSTANCE_COMMENT CASCADE;

drop table PM_INSTANCE_CONTENT CASCADE;

drop table PM_PROCESS_USER CASCADE;

drop table PM_REGISTER CASCADE;

drop table PM_REGISTER_READERS CASCADE;

drop table PM_TEMPLATE CASCADE;

drop table PM_TEMPLATE_BEST_PRACTICE CASCADE;

drop table PM_TEMPLATE_CONTENT CASCADE;

drop table RM_AREA CASCADE;

drop table RM_ASSET CASCADE;

drop table RM_ASSET_ASSET CASCADE;

drop table RM_ASSET_VALUE CASCADE;

drop table RM_BEST_PRACTICE CASCADE;

drop table RM_BEST_PRACTICE_EVENT CASCADE;

drop table RM_BEST_PRACTICE_STANDARD CASCADE;

drop table RM_CATEGORY CASCADE;

drop table RM_CONTROL CASCADE;

drop table RM_CONTROL_BEST_PRACTICE CASCADE;

drop table RM_CONTROL_COST CASCADE;

drop table RM_CONTROL_EFFICIENCY_HISTORY CASCADE;

drop table RM_CONTROL_TEST_HISTORY CASCADE;

drop table RM_EVENT CASCADE;

drop table RM_PARAMETER_NAME CASCADE;

drop table RM_PARAMETER_VALUE_NAME CASCADE;

drop table RM_PROCESS CASCADE;

drop table RM_PROCESS_ASSET CASCADE;

drop table RM_RC_PARAMETER_VALUE_NAME CASCADE;

drop table RM_RISK CASCADE;

drop table RM_RISK_CONTROL CASCADE;

drop table RM_RISK_CONTROL_VALUE CASCADE;

drop table RM_RISK_LIMITS CASCADE;

drop table RM_RISK_VALUE CASCADE;

drop table RM_SECTION_BEST_PRACTICE CASCADE;

drop table RM_STANDARD CASCADE;

drop table WKF_ALERT CASCADE;

drop table WKF_CONTROL_EFFICIENCY CASCADE;

drop table WKF_CONTROL_TEST CASCADE;

drop table WKF_SCHEDULE CASCADE;

drop table WKF_TASK CASCADE;

drop table WKF_TASK_SCHEDULE CASCADE;