CREATE OR REPLACE FUNCTION recalculation_of_area_by_process_delete()
  RETURNS "trigger" AS
'BEGIN

IF (TG_OP = ''DELETE'') THEN
    UPDATE rm_area
    SET nValue = get_area_value(fkContext)
    WHERE fkContext = OLD.fkArea;
END IF;
RETURN NEW;
END'
  LANGUAGE 'plpgsql' VOLATILE;

CREATE TRIGGER recalculation_of_area_by_process_delete
  AFTER DELETE
  ON rm_process
  FOR EACH ROW
  EXECUTE PROCEDURE recalculation_of_area_by_process_delete();