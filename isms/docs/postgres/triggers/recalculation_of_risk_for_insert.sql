CREATE OR REPLACE FUNCTION recalculation_of_risk_for_insert()
  RETURNS "trigger" AS
'DECLARE
  mbAcceptRisk boolean;
BEGIN

IF (TG_OP = ''INSERT'') THEN

    UPDATE rm_risk 
      SET nValue = get_risk_value(fkContext),
      nValueResidual = get_risk_value_residual(fkContext)
      WHERE fkContext = NEW.fkContext;
    UPDATE rm_asset
	  SET nValue = get_asset_value(fkContext)
	  WHERE fkContext = NEW.fkAsset;
END IF;
RETURN NEW;
END;'
  LANGUAGE 'plpgsql' VOLATILE;

CREATE TRIGGER recalculation_of_risk_for_insert
  AFTER INSERT
  ON rm_risk
  FOR EACH ROW
  EXECUTE PROCEDURE recalculation_of_risk_for_insert();