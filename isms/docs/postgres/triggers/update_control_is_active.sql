CREATE OR REPLACE FUNCTION update_control_is_active() RETURNS "trigger"
    AS '
DECLARE
  mbHasRevision INTEGER;
  mbHasTest INTEGER;
  mdDateLimit TIMESTAMP;
  mdDateTodo TIMESTAMP;
  mdDateNext TIMESTAMP;
  mbImplementationIsLate INTEGER;
  mbEfficiencyNotOk INTEGER;
  mbRevisionIsLate INTEGER;
  mbTestNotOk INTEGER;
  mbTestIsLate INTEGER;
BEGIN
  
  mbImplementationIsLate = 0;
  mbEfficiencyNotOk = 0;
  mbRevisionIsLate = 0;
  mbTestNotOk = 0;
  mbTestIsLate = 0;
  
  -- Testa a data de implementação
  IF NEW.dDateImplemented IS NULL AND NEW.dDateDeadline <= NOW() THEN
    mbImplementationIsLate = 1;
  END IF;

  -- Testa se tem revisão
  SELECT COUNT(*)
  INTO mbHasRevision
  FROM
    isms_config cfg,
    wkf_control_efficiency ce
  WHERE
    cfg.pkConfig = 404
    AND cfg.sValue = ''1''
    AND ce.fkControlEfficiency = NEW.fkContext;

  -- Se tem revisão, testa se ela tá ok e em dia.
  IF mbHasRevision = 1 THEN
    SELECT CASE WHEN COUNT(*) = 0 THEN 1 ELSE 0 END
    INTO mbEfficiencyNotOk
    FROM wkf_control_efficiency ce
    WHERE
      ce.fkControlEfficiency = NEW.fkContext
      AND (
        ce.nRealEfficiency >= ce.nExpectedEfficiency
        OR ce.nRealEfficiency = 0
      );
    
    SELECT COUNT(*)
    INTO mbRevisionIsLate
    FROM
      wkf_control_efficiency ce
      JOIN wkf_schedule s ON (s.pkschedule = ce.fkschedule)
      JOIN wkf_task_schedule ts ON (ts.fkschedule = s.pkschedule)
      JOIN wkf_task t ON (t.nactivity = ts.nactivity AND t.fkcontext = ts.fkcontext AND t.bvisible = 1)
      JOIN (
        SELECT
          s2.pkschedule AS schedule_id,
          COUNT(*) AS pendant_tasks
        FROM
          wkf_schedule s2
          JOIN wkf_task_schedule ts2 ON (ts2.fkschedule = s2.pkschedule)
          JOIN wkf_task t2 ON (t2.nactivity = ts2.nactivity AND t2.fkcontext = ts2.fkcontext AND t2.bvisible = 1)
        GROUP BY s2.pkschedule
      ) p ON (p.schedule_id = s.pkschedule)
    WHERE
      ce.fkControlEfficiency = NEW.fkContext
      AND (
        p.pendant_tasks > 1
        OR (
          p.pendant_tasks = 1
          AND CURRENT_DATE > s.dDateLimit
        )
      );
    IF mbRevisionIsLate > 1 THEN mbRevisionIsLate = 1; END IF;
    
  END IF;

  -- Testa se tem teste
  SELECT COUNT(*)
  INTO mbHasTest
  FROM
    isms_config cfg,
    wkf_control_test ct
  WHERE
    cfg.pkConfig = 405
    AND cfg.sValue = ''1''
    AND ct.fkControlTest = NEW.fkContext;

  -- Se tem teste, testa se ele tá ok e em dia.
  IF mbHasTest = 1 THEN
    SELECT CASE WHEN COUNT(*) = 1 THEN 1 ELSE 0 END
    INTO mbTestNotOk
    FROM
      wkf_control_test ct
      LEFT JOIN rm_control_test_history th ON (
        th.fkControl = ct.fkControlTest
        AND NOT EXISTS (
          SELECT *
          FROM rm_control_test_history th2
          WHERE
            th2.fkControl = th.fkControl
            AND th2.dDateAccomplishment > th.dDateAccomplishment
        )
      )
    WHERE
      ct.fkControlTest = NEW.fkContext
      AND th.bTestedValue = 0;
    
    SELECT COUNT(*)
    INTO mbTestIsLate
    FROM
      wkf_control_test ct
      JOIN wkf_schedule s ON (s.pkschedule = ct.fkschedule)
      JOIN wkf_task_schedule ts ON (ts.fkschedule = s.pkschedule)
      JOIN wkf_task t ON (t.nactivity = ts.nactivity AND t.fkcontext = ts.fkcontext AND t.bvisible = 1)
      JOIN (
        SELECT
          s2.pkschedule AS schedule_id,
          COUNT(*) AS pendant_tasks
        FROM
          wkf_schedule s2
          JOIN wkf_task_schedule ts2 ON (ts2.fkschedule = s2.pkschedule)
          JOIN wkf_task t2 ON (t2.nactivity = ts2.nactivity AND t2.fkcontext = ts2.fkcontext AND t2.bvisible = 1)
        GROUP BY s2.pkschedule
      ) p ON (p.schedule_id = s.pkschedule)
    WHERE
      ct.fkControlTest = NEW.fkContext
      AND (
        p.pendant_tasks > 1
        OR (
          p.pendant_tasks = 1
          AND CURRENT_DATE > s.dDateLimit
        )
      );
    IF mbTestIsLate > 1 THEN mbTestIsLate = 1; END IF;
    
  END IF;

  -- Atualiza as flags e cria as seeds de Não-Conformidade que forem necessárias
  NEW.bImplementationIsLate = mbImplementationIsLate;
  NEW.bEfficiencyNotOk = mbEfficiencyNotOk;
  NEW.bRevisionIsLate = mbRevisionIsLate;
  NEW.bTestNotOk = mbTestNotOk;
  NEW.bTestIsLate = mbTestIsLate;

  IF mbImplementationIsLate + mbEfficiencyNotOk + mbRevisionIsLate + mbTestNotOk + mbTestIsLate > 0 THEN
    NEW.bIsActive = 0;
    
    IF OLD.bImplementationIsLate = 0 AND NEW.bImplementationIsLate = 1 THEN
      IF NOT EXISTS (SELECT * FROM ci_nc_seed WHERE fkControl = NEW.fkContext AND nDeactivationReason = 1) THEN
        INSERT INTO ci_nc_seed (fkControl,nDeactivationReason,dDateSent) VALUES (NEW.fkContext,1,NOW());
      END IF;
    END IF;
    
    IF OLD.bEfficiencyNotOk = 0 AND NEW.bEfficiencyNotOk = 1 THEN
      IF NOT EXISTS (SELECT * FROM ci_nc_seed WHERE fkControl = NEW.fkContext AND nDeactivationReason = 2) THEN
        INSERT INTO ci_nc_seed (fkControl,nDeactivationReason,dDateSent) VALUES (NEW.fkContext,2,NOW());
      END IF;
    END IF;
    
    IF OLD.bRevisionIsLate = 0 AND NEW.bRevisionIsLate = 1 THEN
      IF NOT EXISTS (SELECT * FROM ci_nc_seed WHERE fkControl = NEW.fkContext AND nDeactivationReason = 4) THEN
        INSERT INTO ci_nc_seed (fkControl,nDeactivationReason,dDateSent) VALUES (NEW.fkContext,4,NOW());
      END IF;
    END IF;
    
    IF OLD.bTestNotOk = 0 AND NEW.bTestNotOk = 1 THEN
      IF NOT EXISTS (SELECT * FROM ci_nc_seed WHERE fkControl = NEW.fkContext AND nDeactivationReason = 8) THEN
        INSERT INTO ci_nc_seed (fkControl,nDeactivationReason,dDateSent) VALUES (NEW.fkContext,8,NOW());
      END IF;
    END IF;
    
    IF OLD.bTestIsLate = 0 AND NEW.bTestIsLate = 1 THEN
      IF NOT EXISTS (SELECT * FROM ci_nc_seed WHERE fkControl = NEW.fkContext AND nDeactivationReason = 16) THEN
        INSERT INTO ci_nc_seed (fkControl,nDeactivationReason,dDateSent) VALUES (NEW.fkContext,16,NOW());
      END IF;
    END IF;
    
  ELSE
    NEW.bIsActive = 1;
  END IF;

  RETURN NEW;
END
'
  LANGUAGE plpgsql;

CREATE TRIGGER update_control_is_active
  BEFORE UPDATE ON rm_control
  FOR EACH ROW
  EXECUTE PROCEDURE update_control_is_active();