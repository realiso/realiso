CREATE OR REPLACE FUNCTION recalculation_of_risks_values()
  RETURNS "trigger" AS
'DECLARE
      mbAcceptRisk boolean;
      mfValueResidual double precision;
      mbCondition boolean;
  BEGIN
    IF TG_OP = ''UPDATE'' AND NEW.bIsActive != OLD.bIsActive THEN
      UPDATE rm_risk SET
        nValue = get_risk_value(NEW.fkContext),
        nValueResidual = get_risk_value_residual(NEW.fkContext)
      WHERE fkContext IN (
        SELECT fkRisk
        FROM rm_risk_control rc
        WHERE fkControl = NEW.fkContext
      );
    END IF;
    RETURN NEW;
  END'
  LANGUAGE 'plpgsql' VOLATILE;

CREATE TRIGGER recalculation_of_risks_values
  AFTER UPDATE
  ON rm_control
  FOR EACH ROW
  EXECUTE PROCEDURE recalculation_of_risks_values();