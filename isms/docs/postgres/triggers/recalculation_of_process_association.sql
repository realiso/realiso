CREATE OR REPLACE FUNCTION recalculation_of_process_association()
  RETURNS "trigger" AS
'DECLARE
 miProcess integer;
BEGIN

IF( TG_OP = ''DELETE'') THEN
  miProcess= OLD.fkProcess;
ELSE
  miProcess= NEW.fkProcess;
END IF;

  UPDATE rm_process
   -- atualiza valor do risco do processo como sendo o valor do seu maior ativo
    SET nValue = get_process_value(fkContext)
    WHERE fkContext = miProcess;
RETURN NEW;
END;'
  LANGUAGE 'plpgsql' VOLATILE;

CREATE TRIGGER recalculation_of_process_association
  AFTER INSERT OR UPDATE OR DELETE
  ON rm_process_asset
  FOR EACH ROW
  EXECUTE PROCEDURE recalculation_of_process_association();