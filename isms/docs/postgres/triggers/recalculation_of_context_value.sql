CREATE OR REPLACE FUNCTION recalculation_of_context_value()
  RETURNS "trigger" AS
'DECLARE    
BEGIN
IF (TG_OP = ''UPDATE'') THEN
       IF( (NEW.nState = 2705) AND (OLD.nState <> 2705) )  THEN
            IF (OLD.nType = 2801) THEN
			UPDATE rm_area
			   SET nValue = get_area_value(fkContext)
			   WHERE fkParent = NEW.pkContext;
            END IF;
	    IF (OLD.nType =  2802) THEN
                       UPDATE rm_area
                           SET nValue = get_area_value(fkContext)
                           WHERE fkContext IN
			      (SELECT fkArea FROM rm_process WHERE fkContext = NEW.pkContext);
	    END IF;
	    IF (OLD.nType =  2803) THEN
		       UPDATE rm_process
			   SET nValue = get_process_value(fkContext)
			   WHERE fkContext IN
			     (SELECT fkProcess FROM rm_process_asset WHERE fkAsset = NEW.pkContext);
	    END IF;
	    IF (OLD.nType =  2804) THEN
		UPDATE rm_asset
                    SET nValue = get_asset_value(fkContext)
                       WHERE fkContext IN 
                           (SELECT fkAsset FROM rm_risk WHERE fkContext = NEW.pkContext);
	    END IF;
	    IF (OLD.nType =  2805) THEN
		       UPDATE rm_risk SET
			  nValue = get_risk_value(fkContext),
			  nValueResidual = get_risk_value_residual(fkContext)
                          WHERE fkContext IN 
                                (SELECT fkRisk FROM rm_risk_control WHERE fkControl = NEW.pkContext);
	    END IF;
         END IF;
        
	 IF ((NEW.nState <> 2705) AND (OLD.nState = 2705)) THEN
		IF (OLD.nType =  2801 ) THEN --area
			UPDATE rm_area
			  SET nValue = get_area_value(fkContext)
			  WHERE fkContext = NEW.pkContext;
                END IF;
		IF (OLD.nType =  2802 )  THEN --process
		      UPDATE rm_process
			SET nValue = get_process_value(fkContext)
			WHERE fkContext = NEW.pkContext;
                END IF;
		IF (OLD.nType = 2803 ) THEN --asset
		       UPDATE rm_asset
			  SET nValue = get_asset_value(fkContext)
			  WHERE fkContext = NEW.pkContext;
	        END IF;
		IF (OLD.nType =  2804 ) THEN --risk
		       UPDATE rm_risk 
			  SET nValue = get_risk_value(fkContext),
			  nValueResidual = get_risk_value_residual(fkContext)
			  WHERE fkContext = NEW.pkContext;
		       UPDATE rm_asset
                           SET nValue = get_asset_value(fkContext)
                           WHERE fkContext IN 
                               (SELECT fkAsset FROM rm_risk WHERE fkContext = NEW.pkContext);
                END IF;
		IF (OLD.nType = 2805 ) THEN --control
		    UPDATE rm_risk SET
			nValue = get_risk_value(fkContext),
			nValueResidual = get_risk_value_residual(fkContext)
			WHERE fkContext IN 
			   (SELECT fkRisk FROM rm_risk_control rc WHERE fkControl = NEW.pkContext);
		END IF;
          END IF;

END IF;
RETURN NEW;
END;'
  LANGUAGE 'plpgsql' VOLATILE;

CREATE TRIGGER recalculation_of_context_value
  AFTER UPDATE
  ON isms_context
  FOR EACH ROW
  EXECUTE PROCEDURE recalculation_of_context_value();