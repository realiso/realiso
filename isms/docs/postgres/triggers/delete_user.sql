CREATE OR REPLACE FUNCTION delete_user()
  RETURNS "trigger" AS
'BEGIN

IF (TG_OP = ''DELETE'') THEN
    UPDATE isms_context_date
    SET nUserId = 0
    WHERE nUserId = OLD.fkContext;
END IF;
RETURN NULL;
END'
  LANGUAGE 'plpgsql' VOLATILE;

CREATE TRIGGER delete_user
  AFTER DELETE
  ON isms_user
  FOR EACH ROW
  EXECUTE PROCEDURE delete_user();