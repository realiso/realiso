CREATE OR REPLACE FUNCTION recalculation_of_asset_for_risk_delete()
  RETURNS "trigger" AS
'DECLARE
BEGIN
IF (TG_OP = ''DELETE'') THEN
    UPDATE rm_asset
    SET nValue = get_asset_value(fkContext)
    WHERE fkContext = OLD.fkAsset;
END IF;
RETURN NEW;
END'
  LANGUAGE 'plpgsql' VOLATILE;

CREATE TRIGGER recalculation_of_asset_for_risk_delete
  AFTER DELETE
  ON rm_risk
  FOR EACH ROW
  EXECUTE PROCEDURE recalculation_of_asset_for_risk_delete();