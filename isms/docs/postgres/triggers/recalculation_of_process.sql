CREATE OR REPLACE FUNCTION recalculation_of_process()
  RETURNS "trigger" AS
'DECLARE
  miVchg double precision;
BEGIN
IF (TG_OP = ''UPDATE'') THEN
    miVchg := (NEW.nValue - OLD.nValue);
    IF (miVchg <> 0) THEN
        UPDATE rm_process
        SET nValue = get_process_value(rm_process.fkContext)
        FROM rm_process_asset pa
        WHERE rm_process.fkContext = pa.fkProcess
        AND pa.fkAsset = OLD.fkContext;
    END IF;
END IF;
RETURN NEW;
END;'
  LANGUAGE 'plpgsql' VOLATILE;

CREATE TRIGGER recalculation_of_process
  AFTER UPDATE
  ON rm_asset
  FOR EACH ROW
  EXECUTE PROCEDURE recalculation_of_process();