CREATE OR REPLACE FUNCTION update_alert_sent()
  RETURNS "trigger" AS
'BEGIN
  IF OLD.ddateaccomplished IS NULL AND NEW.ddateaccomplished IS NOT NULL THEN
    PERFORM *
    FROM
      wkf_schedule s
      JOIN wkf_task_schedule ts ON (ts.fkschedule = s.pkschedule)
      JOIN wkf_task t ON (t.nactivity = ts.nactivity AND t.fkcontext = ts.fkcontext AND t.bvisible = 1)
      JOIN (
        SELECT
          s.pkschedule AS schedule_id,
          COUNT(*) AS pendant_tasks
        FROM
          wkf_schedule s
          JOIN wkf_task_schedule ts ON (ts.fkschedule = s.pkschedule)
          JOIN wkf_task t ON (t.nactivity = ts.nactivity AND t.fkcontext = ts.fkcontext AND t.bvisible = 1)
        WHERE
          ts.fkcontext = NEW.fkcontext
          AND ts.nactivity = NEW.nactivity
        GROUP BY s.pkschedule
      ) p ON (p.schedule_id = s.pkschedule)
    WHERE
      ts.fkcontext = NEW.fkcontext
      AND ts.nactivity = NEW.nactivity
      AND p.pendant_tasks > 1
      OR (
        p.pendant_tasks = 1
        AND CURRENT_DATE > s.dDateLimit
      );
      
    IF NOT FOUND THEN
      UPDATE wkf_task_schedule
      SET bAlertSent = 0
      WHERE
        nactivity = NEW.nactivity
        AND fkcontext = NEW.fkcontext;
    END IF;
  END IF;
  RETURN NEW;
END'
  LANGUAGE 'plpgsql' VOLATILE;

CREATE TRIGGER update_alert_sent
  AFTER UPDATE
  ON wkf_task
  FOR EACH ROW
  EXECUTE PROCEDURE update_alert_sent();
