CREATE OR REPLACE FUNCTION recalculation_of_area_by_process_update()
  RETURNS "trigger" AS
'DECLARE
    miArea integer;
BEGIN
   miArea := NEW.fkArea;
IF (TG_OP = ''UPDATE'')  THEN
  UPDATE rm_area SET nValue = get_area_value(fkContext) WHERE fkContext = miArea;
  
  IF (NEW.fkArea <> OLD.fkArea) THEN
      UPDATE rm_area SET nValue = get_area_value(fkContext) WHERE fkContext = OLD.fkArea;
  END IF;
END IF;
RETURN NEW;
END'
  LANGUAGE 'plpgsql' VOLATILE;

CREATE TRIGGER recalculation_of_area_by_process_update
  AFTER UPDATE
  ON rm_process
  FOR EACH ROW
  EXECUTE PROCEDURE recalculation_of_area_by_process_update();