CREATE OR REPLACE FUNCTION recalculation_of_asset_association()
  RETURNS "trigger" AS
'DECLARE
    miAsset integer;
BEGIN

IF (TG_OP = ''DELETE'') THEN
    miAsset := OLD.fkDependent;
ELSE
   IF (NEW.fkDependent IS NOT NULL) THEN
       miAsset := NEW.fkDependent;
   ELSE
       miAsset := OLD.fkDependent;
   END IF;
END IF;

UPDATE rm_asset
  SET nValue = get_asset_value(fkContext)
     WHERE fkContext = miAsset;

RETURN NEW;
END'
  LANGUAGE 'plpgsql' VOLATILE;

CREATE TRIGGER recalculation_of_asset_association
  AFTER INSERT OR UPDATE OR DELETE
  ON rm_asset_asset
  FOR EACH ROW
  EXECUTE PROCEDURE recalculation_of_asset_association();