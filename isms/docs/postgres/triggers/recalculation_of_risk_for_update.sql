CREATE OR REPLACE FUNCTION recalculation_of_risk_for_update()
  RETURNS "trigger" AS
'DECLARE
    mbAcceptRisk boolean;
    mbAssetChanged boolean;
    mfValueResidual double precision;
    mbCondition boolean;
BEGIN
IF (TG_OP = ''UPDATE'') THEN
    IF(NEW.nAcceptMode <> OLD.nAcceptMode) THEN
        mbAcceptRisk := true;
    ELSE
        mbAcceptRisk := false;
    END IF;
    IF(NEW.fkAsset <> OLD.fkAsset) THEN
        mbAssetChanged := true;
    ELSE
        mbAssetChanged := false;
    END IF;
    mfValueResidual := NEW.nValueResidual - OLD.nValueResidual;
    IF(  ( (mfValueResidual <> 0) AND 
           ( (NEW.nValueResidual IS NOT NULL) OR (OLD.nValueResidual IS NOT NULL) )  
         ) 
	 OR 
	 (mbAcceptRisk)
	 OR
	 (mbAssetChanged)
      ) 
    THEN
	mbCondition := true;
    ELSE
	mbCondition := false;
    END IF;
   IF (mbCondition) THEN
       UPDATE rm_risk 
       SET nValue = get_risk_value(fkContext),
       nValueResidual = get_risk_value_residual(fkContext)
       WHERE fkContext = NEW.fkContext;
       
       UPDATE rm_asset
          SET nValue = get_asset_value(fkContext)
          WHERE fkContext = NEW.fkAsset;

       IF (mbAssetChanged) THEN
          UPDATE rm_asset
          SET nValue = get_asset_value(fkContext)
          WHERE fkContext = OLD.fkAsset;
       END IF;
    END IF;
END IF;
RETURN NEW;
END'
  LANGUAGE 'plpgsql' VOLATILE;

CREATE TRIGGER recalculation_of_risk_for_update
  AFTER UPDATE
  ON rm_risk
  FOR EACH ROW
  EXECUTE PROCEDURE recalculation_of_risk_for_update();