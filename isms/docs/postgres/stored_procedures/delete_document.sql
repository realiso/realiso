CREATE OR REPLACE FUNCTION delete_document(iddoc integer) RETURNS integer AS
'
  DECLARE
    doc pm_document%ROWTYPE;
  BEGIN
    FOR doc IN SELECT fkContext FROM pm_document WHERE fkParent = idDoc LOOP
      PERFORM delete_document(doc.fkContext);
    END LOOP;
    DELETE FROM pm_document WHERE fkContext = idDoc;
    DELETE FROM isms_context WHERE pkContext = idDoc;
    RETURN 1;
  END
'
  LANGUAGE 'plpgsql' VOLATILE;