/* PERFIL */
INSERT INTO isms_context (pkContext,nType,nState) VALUES (1,2817,2700);          INSERT INTO isms_profile (fkContext,sName,nId) VALUES (1,'Security Officer',8801);         	INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1,2901,NOW());      INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,1,2902,NOW());      
INSERT INTO isms_context (pkContext,nType,nState) VALUES (2,2817,2700);          INSERT INTO isms_profile (fkContext,sName,nId) VALUES (2,'Gestor',8802);           					INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,2,2901,NOW());      INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,2,2902,NOW());      
INSERT INTO isms_context (pkContext,nType,nState) VALUES (3,2817,2700);          INSERT INTO isms_profile (fkContext,sName,nId) VALUES (3,'Administrador',8803);       			INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,3,2901,NOW());      INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,3,2902,NOW());      
INSERT INTO isms_context (pkContext,nType,nState) VALUES (4,2817,2700);          INSERT INTO isms_profile (fkContext,sName,nId) VALUES (4,'Sem Permissões',8804);     INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,4,2901,NOW());      INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,4,2902,NOW());      
INSERT INTO isms_context (pkContext,nType,nState) VALUES (5,2817,2700);          INSERT INTO isms_profile (fkContext,sName,nId) VALUES (5,'Leitor de Documentos',8805);     INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5,2901,NOW());      INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,5,2902,NOW());

/* USUARIO */
INSERT INTO isms_context (pkcontext, ntype, nstate) values (13, 2816, 0);
INSERT INTO isms_user values (13, 1, '%user_fullname%', '%user_login%', '%user_passwordmd5%', '%user_email%', null, '%user_language%', null, 0, 0, 0);
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,13,2901,NOW());
INSERT INTO isms_context_date (sUserName, nUserId, fkContext, nAction, ddate) VALUES ('Realiso Online',0,13,2902,NOW());

/***** NOMES DOS PARAMETROS DE RISCO *****/
INSERT INTO rm_parameter_name (pkparametername, sname, nweight) VALUES (1, 'Disponibilidade'    ,1);
INSERT INTO rm_parameter_name (pkparametername, sname, nweight) VALUES (2, 'Confidencialidade' ,1);
INSERT INTO rm_parameter_name (pkparametername, sname, nweight) VALUES (3, 'Integridade'       ,1);

/***** VALORES DOS PARAMETROS DE RISCO *****/
/* ATIVO E RISCO */
INSERT INTO rm_parameter_value_name (pkvaluename, nvalue, simportance, simpact, sriskprobability) VALUES (1, 1.0, 'Negligenciável', 'Muito Baixo'  , 'Raro (1% - 20%)');
INSERT INTO rm_parameter_value_name (pkvaluename, nvalue, simportance, simpact, sriskprobability) VALUES (2, 2.0, 'Baixo'       , 'Baixo'       , 'Improvável (21% - 40%)');
INSERT INTO rm_parameter_value_name (pkvaluename, nvalue, simportance, simpact, sriskprobability) VALUES (3, 3.0, 'Médio'    , 'Médio'    , 'Moderado (41% - 60%)');
INSERT INTO rm_parameter_value_name (pkvaluename, nvalue, simportance, simpact, sriskprobability) VALUES (4, 4.0, 'Alto'      , 'Alto'      , 'Provável (61% - 80%)');
INSERT INTO rm_parameter_value_name (pkvaluename, nvalue, simportance, simpact, sriskprobability) VALUES (5, 5.0, 'Crítico'  , 'Muito Alto' , 'Quase certo (81% - 99%)');

/* RISCO x CONTROLE */
INSERT INTO rm_rc_parameter_value_name (pkrcvaluename, nvalue, srcprobability, scontrolimpact) VALUES (1, 0.0, 'Não afeta'        , 'Não afeta');
INSERT INTO rm_rc_parameter_value_name (pkrcvaluename, nvalue, srcprobability, scontrolimpact) VALUES (2, 1.0, 'Decréscimo de um Nível'   , 'Decréscimo de um Nível');
INSERT INTO rm_rc_parameter_value_name (pkrcvaluename, nvalue, srcprobability, scontrolimpact) VALUES (3, 2.0, 'Decréscimo de dois Níveis'   , 'Decréscimo de dois Níveis');
INSERT INTO rm_rc_parameter_value_name (pkrcvaluename, nvalue, srcprobability, scontrolimpact) VALUES (4, 3.0, 'Decréscimo de três Níveis' , 'Decréscimo de três Níveis');
INSERT INTO rm_rc_parameter_value_name (pkrcvaluename, nvalue, srcprobability, scontrolimpact) VALUES (5, 4.0, 'Decréscimo de quatro Níveis'  , 'Decréscimo de quatro Níveis');

/***** NOMES DOS CUSTOS *****/
INSERT INTO isms_config (pkconfig, svalue) VALUES (201, 'Hardware');
INSERT INTO isms_config (pkconfig, svalue) VALUES (202, 'Software');
INSERT INTO isms_config (pkconfig, svalue) VALUES (203, 'Serviços');
INSERT INTO isms_config (pkconfig, svalue) VALUES (204, 'Pessoas');
INSERT INTO isms_config (pkconfig, svalue) VALUES (205, 'Educação');

/***** GERAL *****/
INSERT INTO isms_config (pkconfig, svalue) VALUES (401, '0');
INSERT INTO isms_config (pkconfig, svalue) VALUES (402, '1');
INSERT INTO isms_config (pkconfig, svalue) VALUES (403, 'localhost');
INSERT INTO isms_config (pkconfig, svalue) VALUES (404, '1');
INSERT INTO isms_config (pkconfig, svalue) VALUES (405, '1');
INSERT INTO isms_config (pkconfig, svalue) VALUES (406, '1');
INSERT INTO isms_config (pkconfig, svalue) VALUES (407, '0');
INSERT INTO isms_config (pkconfig, svalue) VALUES (408, '0');
INSERT INTO isms_config (pkconfig, svalue) VALUES (409, '0');
INSERT INTO isms_config (pkconfig, svalue) VALUES (410, '00:00');
INSERT INTO isms_config (pkconfig, svalue) VALUES (411, '1970-01-01 00:00:00');
INSERT INTO isms_config (pkConfig, svalue) VALUES (415,'%default_sender%');
INSERT INTO isms_config (pkconfig, svalue) VALUES (416, '1');
INSERT INTO isms_config (pkconfig, svalue) VALUES (417, '1');
INSERT INTO isms_config (pkconfig, svalue) VALUES (418, '7801');
INSERT INTO isms_config (pkconfig, svalue) VALUES (419, '00:00');
INSERT INTO isms_config (pkconfig, svalue) VALUES (420, date_part('year', now()) || '-' || date_part('month', now()) || '-' || date_part('day', now()) );
INSERT INTO isms_config (pkconfig, svalue) VALUES (421, '1');
INSERT INTO isms_config (pkconfig, svalue) VALUES (426, '1');
INSERT INTO isms_config (pkconfig, svalue) VALUES (427, 'Confidencial');
INSERT INTO isms_config (pkconfig, svalue) VALUES (428, '');
INSERT INTO isms_config (pkconfig, svalue) VALUES (429, '1');
INSERT INTO isms_config (pkconfig, svalue) VALUES (430, '%default_sender_name%');
INSERT INTO isms_config (pkconfig, svalue) VALUES (431, 'CURR_REAL');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7404, '0');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7500, '0');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7501, '');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7502, '');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7503, '');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7504, '');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7510, '');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7511, '');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7512, '');

/***** POLITICA DE SENHAS *****/
INSERT INTO isms_config (pkconfig, svalue) VALUES (7201, '5');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7202, '0');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7203, '0');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7204, '0');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7205, '3');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7206, '365');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7207, '5');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7208, '7');

/***** USUARIOS ESPECIAIS *****/
INSERT INTO isms_config (pkconfig, svalue) VALUES (801, '13');
INSERT INTO isms_config (pkconfig, svalue) VALUES (803, '13');
INSERT INTO isms_config (pkconfig, svalue) VALUES (804, '13');
INSERT INTO isms_config (pkconfig, svalue) VALUES (805, '13');
INSERT INTO isms_config (pkconfig, svalue) VALUES (806, '13');
INSERT INTO isms_config (pkconfig, svalue) VALUES (807, '13');
INSERT INTO isms_config (pkconfig, svalue) VALUES (808, '13');
INSERT INTO isms_config (pkconfig, svalue) VALUES (809, '13');

/***** CLASSIFICACAO *****/
/* AREA */
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (1, 2801, 'Sede', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (2, 2801, 'Subsidiário', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (3, 2801, 'Escritório Regional', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (4, 2801, 'Fábrica', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (5, 2801, '3 - Alto', 5502);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (6, 2801, '2 - Médio', 5502);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (7, 2801, '1 - Baixo', 5502);

/* PROCESSO */
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (8, 2802, 'Mercado & consumidores', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (9, 2802, 'Desenvolvimento de novo produto', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (10, 2802,'Produção & entrega', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (11, 2802,'Recursos humanos', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (12, 2802,'Tecnologia da informação', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (13, 2802,'Finanças & contabilidade', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (14, 2802,'Ambiente, saúde & segurança', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (15, 2802,'Melhoria de performance', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (16, 2802, '3 - Alto', 5502);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (17, 2802, '2 - Médio', 5502);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (18, 2802, '1 - Baixo', 5502);

/* RISCO */
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (19, 2804, 'Vulnerabilidade', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (20, 2804, 'Ameaça', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (21, 2804, 'Evento (Vulnerabilidade x Ameaça)', 5501);

/* EVENTO */
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (22, 2807, 'Físico', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (23, 2807, 'Tecnológico', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (24, 2807, 'Administrativo', 5501);

/* CONTROLE */
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (25, 2805, 'Baseado em Análise de Risco', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (26, 2805, 'Questões de Compliance', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (27, 2805, 'Outros', 5501);

/* DOCUMENTO */
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (28, 2823, 'Confidential', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (29, 2823, 'Restrito', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (30, 2823, 'Institucional', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (31, 2823, 'Público', 5501);

/* REGISTRO */
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (32, 2826, 'Confidencial', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (33, 2826, 'Restrito', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (34, 2826, 'Institucional', 5501);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (35, 2826, 'Público', 5501);

/* CUSTO DO INCIDENTE */
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (36, 2831, 'Multa', 5503);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (37, 2831, 'Recuperação do Ambiente', 5503);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (38, 2831, 'Perda Direta', 5503);
INSERT INTO isms_context_classification (pkclassification, ncontexttype, sname, nclassificationtype) VALUES (39, 2831, 'Perda Indireta', 5503);

/* RISK_LOW */
INSERT INTO isms_context (pkcontext, ntype, nstate) values (14, 2822, 2702);
INSERT INTO rm_risk_limits (fkcontext, nlow, nhigh) VALUES (14, 9, 18);

/* RISK_VALUE_COUNT */
INSERT INTO isms_config (pkconfig, svalue) VALUES (7403, '5');

/* SAAS Config */
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711701, '%saas_report_email%');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711703, '');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711704, '');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711705, '');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711706, '');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711707, '');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711708, '');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711709, '1');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711710, '');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711712, '');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711713, '%user_email%');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711714, '%config_path%');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711715, '%max_file_size%');

/* ERROR_REPORT_EMAIL */
INSERT INTO isms_config (pkconfig,svalue) VALUES (7209,'%error_report_email%');

/*RISK_FORMULA_TYPE*/
INSERT INTO isms_config (pkConfig, sValue) VALUES (5701, '8301');