/*******************************************************************************
 * Classe ISMSContextsAssociator. Classe para telas de associa��o de contextos.
 * 
 * <p>Classe para controlar as grids das telas de associa��o de contextos.</p>
 *
 * @package javascript
 * @param String psSearchGridName Nome da grid de busca
 * @param String psCurrentGridName Nome da grid de contextos atuais
 * @param String psCurrentVariableName Nome da variable de contextos atuais
 * @param String psGridPackName Nome do pacote que cont�m as grids
 *******************************************************************************/
function ISMSContextsAssociator(psSearchGridName,psCurrentGridName,psCurrentVariableName,psGridPackName){
  
  var self = this;
  var csSearchGridName = psSearchGridName;
  var csCurrentGridName = psCurrentGridName;
  var csCurrentVariableName = psCurrentVariableName;
  var csGridPackName = psGridPackName;
  
 /*****************************************************************************
  * Adiciona os contextos selecionados aos atuais.
  *
  * <p>Move os contextos selecionados da grid de busca para a grid de contextos
  * atuais.</p>
  *****************************************************************************/
  self.addSelected = function(){
    var maSelectedValues = gobi(csSearchGridName).getValue();
    if(maSelectedValues!=null){
      if(maSelectedValues.join(':').length){
        var maNewCurrents = gobi(csCurrentGridName).getAllIds().concat(maSelectedValues);
        gebi(csCurrentVariableName).value = maNewCurrents.join(':');
        gobi(csGridPackName).refresh();
        maNewCurrents = null;
      }
    }
    maSelectedValues = null;
  }
  
 /*****************************************************************************
  * Remove os contextos selecionados.
  *
  * <p>Remove os contextos selecionados da grid de contextos atuais.</p>
  *****************************************************************************/
  self.removeSelected = function(){
    var moGridCurrent = gobi(csCurrentGridName);
    var maSelectedValues = moGridCurrent.getValue();
    if(maSelectedValues.length){
      gebi(csCurrentVariableName).value = moGridCurrent.getAllIds().diff(maSelectedValues).join(':');
      gobi(csGridPackName).refresh();
    }
    maSelectedValues = moGridCurrent = null;
  }
  
}
