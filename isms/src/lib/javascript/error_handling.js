/*****************************************************************************
 * Exibe uma popup de debug
 *
 * <p>Exibe uma mensagem de erro em uma popup de debug.</p>
 *
 * @param String psMessage A mensagem a ser exibida
 * @param integer piClientHeight Altura da �rea cliente da popup (opcional)
 * @param integer piClientWidth Largura da �rea cliente da popup (opcional)
 * @param boolean pbKnownError Indica se � um erro conhecido (opcional)
 *****************************************************************************/
function showDebug(psMessage,piClientHeight,piClientWidth,pbKnownError){
  var miClientHeight = (piClientHeight?piClientHeight:400);
  var miClientWidth = (piClientWidth?piClientWidth:600);
  var miHeight = (browser.isIE ? miClientHeight + 25 : miClientHeight + 27);
  var miWidth = (browser.isIE ? miClientWidth + 2 : miClientWidth + 4);
  if(lbRelease){
    // cria um hidden
    var moHidden = document.createElement('input');
    moHidden.type = 'hidden';
    moHidden.name = 'isms_js_exception';
    moHidden.value = psMessage;
    gebi('dialog').appendChild(moHidden);
    // dispara o evento
    trigger_event('isms_handle_js_error',3);
    // abre a popup de erro, digo, comportamento inesperado
    if(soPopUpManager.getPopUpById('popup_unexpected_behavior')==null){
      isms_open_popup('popup_unexpected_behavior','popup_unexpected_behavior.php','','true');
    }
  }else{
    // mostra popup de debug
    var msDebug = '<div style="height:'+miClientHeight+'px;width:'+miWidth+'px;overflow:auto;">'
                    +psMessage
                 +'</div>';
    js_open_popup('debug','','<b>Error</b>',false,miHeight,miWidth);
    soPopUpManager.getPopUpById('debug').setHtmlContent(msDebug);
  }
}

/*****************************************************************************
 * Exibe uma popup de debug de uma exce��o
 *
 * <p>Exibe uma popup de debug de uma exce��o.</p>
 *
 * @param Exception poException Exce��o a ser exibida
 * @param integer piClientHeight Altura da �rea cliente da popup (opcional)
 * @param integer piClientWidth Largura da �rea cliente da popup (opcional)
 *****************************************************************************/
function debugException(poException,piClientHeight,piClientWidth){
  var msDebug = '<h3>[Exception]</h3>';
  for(var msProperty in poException){
	  if(msProperty != 'extraInfo'){
		  msDebug+= '<b>'+msProperty+':</b> '+poException[msProperty]+'<br>';		  
	  }
  }
  if(poException.extraInfo){
	  msDebug += "<b>EXTRA INFO</b><br>{";
	  msDebug += poException.extraInfo;
	  msDebug += "<br>}";
  }
  showDebug(msDebug,piClientHeight,piClientWidth);
}