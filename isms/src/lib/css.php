<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 * 
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
  
  require_once('FWDCodeCompresser.php');
  require_once('css_setup.php');

  $soCompresser = new FWDCodeCompresser();

  // Parametro que indica se os arquivos devem ser compactados
  $sbCompress = 0;//(isset($_GET['c'])?$_GET['c']:true);
  $ssPath = 'css';
  $ssExtension = 'css';
  $siExtLen = strlen($ssExtension);
  $soDir = dir($ssPath);
  while(false!==($ssFile=$soDir->read())){
    $ssFullPath = "$ssPath/$ssFile";
    if(is_file($ssFullPath) && substr($ssFile,strlen($ssFile)-$siExtLen-1)==".$ssExtension"){
      if($sbCompress){
        echo $soCompresser->compressFile($ssFullPath)."\n";
      }else{
        readfile($ssFullPath);
        echo "\n";
      }
    }
  }
  $soDir->close();

?>