<?

include_once "include.php";

define("LICENSE_TYPE_SUBSCRIPTION_ISMS", 5802);
define("LICENSE_TYPE_SUBSCRIPTION_SOX", 5808);
define("LICENSE_TYPE_SUBSCRIPTION_BCMS", 5816);

function setInstance($productId){
	switch($productId){
		case LICENSE_TYPE_SUBSCRIPTION_ISMS:
			return 1;
		case LICENSE_TYPE_SUBSCRIPTION_SOX:
			return 8;
		case LICENSE_TYPE_SUBSCRIPTION_BCMS:
			return 14;
	}
}

$ismsSaaS = new ISMSSaaS();
$instance = $ismsSaaS->getConfig(ISMSSaaS::LICENSE_CLIENT_NAME);
$license = $ismsSaaS->getConfig(ISMSSaaS::LICENSE_TYPE);

$miLicenseType = setInstance($license);

$planCode = $_GET['planCode'];
$keySubscription = $_GET['keySubscription'];
$token = $_GET['token'];
$payerID = $_GET['PayerID'];

$ismsASaaS = new ISMSASaaS();
$updatedClientActivePaypal = $ismsASaaS->updateClientActivePayPal($planCode, $payerID, $token);

if($updatedClientActivePaypal=="Conta Ativada Com Sucesso!"){
	$numberOfUsers = getNumberOfUsers($planCode);
	$instanceIsActive = 1;
	//$status = file_get_contents("http://172.99.0.234/saasadmin/change_instance.php?alias=".$instance."&active=".$instanceIsActive."&users=".$numberOfUsers."&license=".$miLicenseType); // Dev
	//$status = file_get_contents("http://homolog.realiso.com/current/change_instance.php?alias=".$instance."&active=".$instanceIsActive."&users=".$numberOfUsers."&license=".$miLicenseType); // Homolog
	$status = file_get_contents("http://saas.realiso.com/current/change_instance.php?alias=".$instance."&active=".$instanceIsActive."&users=".$numberOfUsers."&license=".$miLicenseType); // Production

	if($status=="1"){
		if($ismsASaaS->updatedClientConfirmStatusSubscription()){
			//header("Location: http://172.99.0.234/bcms/login.php"); // Dev
			//header("Location: https://homolog.realiso.com/".$instance."/sys/autologin.php"); // Homolog
			header("Location: https://saas.realiso.com/".$instance."/sys/autologin.php"); // Production
		}
    } else {
		//logger(SYNC_JOB, "Instancia NAO Bloqueada >> $assinatura->company");
	}
}


function getNumberOfUsers($planCode){
	$numberOfUsers = 1;
	switch ($planCode) {
		case 'BUS5':
			$numberOfUsers = 5;
			break;
		case 'BUS10':
			$numberOfUsers = 10;
			break;
		case 'BUS20':
			$numberOfUsers = 20;
			break;
	}
	return $numberOfUsers;
}

//http://172.99.0.234/saasadmin/current/change_instance.php?alias=utodoze&active=0&users=5

//payapl_success.php?planCode=BUS5&keySubscription=03390LDgxQOazVDl8Pj0AmvIPZYuLHEVt14gVGHBIfjPu3K62dRuEGNl7U92&token=EC-4UN953431J0826918&PayerID=YXLSX92KRJJFE

?>