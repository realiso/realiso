<?php
include_once "include.php";
include_once $handlers_ref . "select/QuerySelectControlEfficiency.php";
include_once $handlers_ref . "select/QuerySelectControlTest.php";

class DownloadGraphicEvent extends FWDRunnable {
  public function run(){
    $msFilename = "graph".date("YmdHis");
    header('Cache-Control: ');
    header('Pragma: ');
    header('Content-type: application/octet-stream');
    header('Content-Disposition: attachment; filename="'.$msFilename.'.png"');    
    $msGraphType = FWDWebLib::getObject('select_graphic')->getValue();
    $moGraph = FWDWebLib::getObject($msGraphType);
    echo $_SESSION[$moGraph->getChartName()];
  }
}

class ChangeControlEfficiencyEvent extends FWDRunnable {
  public function run() {
    $moConfig = new ISMSConfig();
    if($moConfig->getConfig(GENERAL_TEST_ENABLED)){
      $miControlId = FWDWebLib::getObject('select_control_efficiency')->getValue();
      $moLineChartControlEfficiency = new ISMSGraphControlEfficiency($miControlId);
      
      $moChart = FWDWebLib::getObject('control_efficiency_lineChart');
      $moChart->setChart($moLineChartControlEfficiency->makeGraph());
      $msImgId = $moChart->getChartName();
      $msSrc = FWDWebLib::getInstance()->getLibRef()."FWDGetImg.php?img=$msImgId&id=".uniqid(session_id());
      echo "gebi('control_efficiency_lineChart').src='".$msSrc."';";
    }
  }
}

class ChangeControlTestEvent extends FWDRunnable {
  public function run() {
    $moConfig = new ISMSConfig();
    if($moConfig->getConfig(GENERAL_TEST_ENABLED)){
      $miControlId = FWDWebLib::getObject('select_control_test')->getValue();
      $moLineChartControlEfficiency = new ISMSGraphControlTest($miControlId);
      
      $moChart = FWDWebLib::getObject('control_test_lineChart');
      $moChart->setChart($moLineChartControlEfficiency->makeGraph(80,35,820,210));
      $msImgId = $moChart->getChartName();
      $msSrc = FWDWebLib::getInstance()->getLibRef()."FWDGetImg.php?img=$msImgId&id=".uniqid(session_id());
      echo "gebi('control_test_lineChart').src='".$msSrc."';";
    }
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));

    $moConfig = new ISMSConfig();
    if($moConfig->getConfig(GENERAL_REVISION_ENABLED)){
      $moStartEvent->addAjaxEvent(new ChangeControlEfficiencyEvent("change_control_efficiency_event"));
    }
    if($moConfig->getConfig(GENERAL_TEST_ENABLED)){
      $moStartEvent->addAjaxEvent(new ChangeControlTestEvent("change_control_test_event"));
    }
    $moStartEvent->addSubmitEvent(new DownloadGraphicEvent("download_graphic_event"));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    //teste para ver se o custo esta abilitado no sistema
    $moConfig = new ISMSConfig();

    $moHandler = new QuerySelectControlEfficiency(FWDWebLib::getConnection());
    $moSelect = FWDWebLib::getObject('select_control_efficiency');
    $moSelect->setQueryHandler($moHandler);
    $moSelect->populate();
    
    $moHandler = new QuerySelectControlTest(FWDWebLib::getConnection());
    $moSelect = FWDWebLib::getObject('select_control_test');
    $moSelect->setQueryHandler($moHandler);
    $moSelect->populate();
    
    $mbShowVBDownload = false;
    
    if(ISMSLib::hasModule(RISK_MANAGEMENT_MODE)){
      $mbShowVBDownload = true;
      /** RM_OPTGROUP **/
      /** GR�FICO EM PIZZA DO SUM�RIO DE RISCO POTENCIAL **/
      $moPieChartPotentialRisk = new ISMSGraphPotentialRisk();
      $moPieChartPotentialRisk = $moPieChartPotentialRisk->makeGraph();
      if($moPieChartPotentialRisk){
        FWDWebLib::getObject('potential_risk_pieChart')->setChart($moPieChartPotentialRisk /*->makeGraph()*/);
      }else{
        # The data for the bar chart
        $data = array(85, 156, 179.5, 211, 123);
  
        # The labels for the bar chart
        $labels = array("Mon", "Tue", "Wed", "Thu", "Fri");
  
        # Create a XYChart object of size 250 x 250 pixels
        $c = new XYChart(250, 250);
  
        # Set the plotarea at (30, 20) and of size 200 x 200 pixels
        $c->setPlotArea(30, 20, 200, 200);
  
        # Add a bar chart layer using the given data
        $c->addBarLayer($data);
  
        # Set the labels on the x axis.
        $c->xAxis->setLabels($labels);
        
        $c->addText(100, 20, "teste");// [, font [, fontSize [, fontColor [, alignment [, angle [, vertical ]]]]]]);
        //FWDWebLib::getObject('potential_risk_pieChart')->setAttrDisplay('false');
      }
    
      /** GR�FICO EM BARRA DO SUM�RIO DE RISCO RESIDUAL **/
      $moBarChartResidualRisk = new ISMSGraphResidualRisk();
      FWDWebLib::getObject('residual_risk_barChart')->setChart($moBarChartResidualRisk->makeGraph());
      
      /** GR�FICO EM PIZZA DEMONSTRANDO OS TRATAMENTOS DOS RISCOS **/
      $moPieChartTreatedRisk = new ISMSGraphTreatedRisk();
      FWDWebLib::getObject('treated_risk_pieChart')->setChart($moPieChartTreatedRisk->makeGraph());
    
      /** GR�FICO EM BARRA COM OS 10 PROCESSOS COM MAIS ATIVOS ASSOCIADOS **/
      $moBarChartProcessAsset = new ISMSGraphProcessAsset();
      FWDWebLib::getObject('process_asset_barChart')->setChart($moBarChartProcessAsset->makeGraph());
      
      /** GR�FICO EM BARRA COM OS 10 ATIVOS QUE POSSUEM MAIS RISCOS) **/
      $moBarChartAssetRisk = new ISMSGraphAssetRisk();
      FWDWebLib::getObject('asset_risk_barChart')->setChart($moBarChartAssetRisk->makeGraph());
      
      /** GR�FICO EM BARRA COM OS 10 PROCESSOS QUE POSSUEM MAIS RISCOS) **/
      $moBarChartProcessRisk = new ISMSGraphProcessRisk();
      FWDWebLib::getObject('process_risk_barChart')->setChart($moBarChartProcessRisk->makeGraph());
      
      /** GR�FICO EM BARRA COM OS 10 ATIVOS QUE POSSUEM MAIS RISCOS RESIDUAIS EM VERMELHO) **/
      $moBarChartRedAssetRisk = new ISMSGraphRedAssetRisk();
      FWDWebLib::getObject('red_asset_risk_barChart')->setChart($moBarChartRedAssetRisk->makeGraph());
      
      /** GR�FICO EM BARRA COM OS 10 PROCESSOS QUE POSSUEM MAIS RISCOS RESIDUAIS EM VERMELHO) **/
      $moBarChartRedProcessRisk = new ISMSGraphRedProcessRisk();
      FWDWebLib::getObject('red_process_risk_barChart')->setChart($moBarChartRedProcessRisk->makeGraph());
    /** FIM RM_OPTGROUP **/
    /** TEST_OPTGROUP **/
      //Verifica se o sistema est� com teste de controles habilitado.
      if($moConfig->getConfig(GENERAL_TEST_ENABLED)){
        /** GR�FICO EM LINHA COM HIST�RICO DOS TESTES DE CONTROLES **/
        $moLineChartControlTest = new ISMSGraphControlTest();
        FWDWebLib::getObject('control_test_lineChart')->setChart($moLineChartControlTest->makeGraph(80,35,820,210));
        //FWDWebLib::getObject('control_test_viewgroup')->setAttrDisplay('true');
      } else {
        //sistema com testes de controle desabilitado, n�o renderizar este grafico
        FWDWebLib::getObject('control_test_viewgroup')->setShouldDraw(false);
        FWDWebLib::getObject('test_optgroup')->setShouldDraw(false);
      }
    /** FIM TEST_OPTGROUP **/
    /** REVISION_OPTGROUP **/
      //Verifica se o sistema est� com revis�o de controles habilitado.
      if($moConfig->getConfig(GENERAL_REVISION_ENABLED)){
        /** GR�FICO EM LINHA COM HIST�RICO DA REVIS�O DE EFICI�NCIA DE CONTROLES **/
        $moLineChartControlEfficiency = new ISMSGraphControlEfficiency();
        FWDWebLib::getObject('control_efficiency_lineChart')->setChart($moLineChartControlEfficiency->makeGraph());
      }else{
        //sistema com revis�o de controle desabilitado, n�o renderizar este grafico
        FWDWebLib::getObject('control_efficiency_viewgroup')->setShouldDraw(false);
        FWDWebLib::getObject('revision_optgroup')->setShouldDraw(false);
      }
    /** FIM REVISION_OPTGROUP **/
    /** COST_OPTGROUP **/
      //Verifica se o sistema est� com custos habilitados.
      if($moConfig->getConfig(GENERAL_COST_ENABLED)){
        /** GR�FICO EM PIZZA DA ESTIMATIVA DE CUSTO DOS ATIVOS **/
        $moPieChartAssetEstimatedCost = new ISMSGraphAssetEstimatedCost();  
        FWDWebLib::getObject('asset_estimated_cost_pieChart')->setChart($moPieChartAssetEstimatedCost->makeGraph());
        
        /** GR�FICO EM PIZZA DOS CUSTOS DOS ATIVOS **/
        $moPieChartAssetCost = new ISMSGraphAssetCost();
        FWDWebLib::getObject('asset_cost_pieChart')->setChart($moPieChartAssetCost->makeGraph());
      }else{
        //sistema com custos desabilitados, n�o renderizar estes graficos
        FWDWebLib::getObject('asset_estimated_cost_pieChart')->setShouldDraw(false);
        FWDWebLib::getObject('asset_cost_pieChart')->setShouldDraw(false);
        FWDWebLib::getObject('cost_optgroup')->setShouldDraw(false);
      }
    /** FIM COST_OPTGROUP **/
    } else {
      FWDWebLib::getObject('potential_risk_pieChart')->setShouldDraw(false);
      FWDWebLib::getObject('residual_risk_barChart')->setShouldDraw(false);
      FWDWebLib::getObject('treated_risk_pieChart')->setShouldDraw(false);
      FWDWebLib::getObject('process_asset_barChart')->setShouldDraw(false);
      FWDWebLib::getObject('asset_risk_barChart')->setShouldDraw(false);
      FWDWebLib::getObject('process_risk_barChart')->setShouldDraw(false);
      FWDWebLib::getObject('red_asset_risk_barChart')->setShouldDraw(false);
      FWDWebLib::getObject('red_process_risk_barChart')->setShouldDraw(false);
      FWDWebLib::getObject('control_test_viewgroup')->setShouldDraw(false);
      FWDWebLib::getObject('test_optgroup')->setShouldDraw(false);
      FWDWebLib::getObject('control_efficiency_viewgroup')->setShouldDraw(false);
      FWDWebLib::getObject('revision_optgroup')->setShouldDraw(false);
      FWDWebLib::getObject('asset_estimated_cost_pieChart')->setShouldDraw(false);
      FWDWebLib::getObject('asset_cost_pieChart')->setShouldDraw(false);
      FWDWebLib::getObject('cost_optgroup')->setShouldDraw(false);
      FWDWebLib::getObject('rm_optgroup')->setShouldDraw(false);
    }
  /** PM_OPTGROUP **/
    //Verifica se o sistema possui o m�dulo de gest�o das pol�ticas
    if(ISMSLib::hasModule(POLICY_MODE)){
      $mbShowVBDownload = true;
      /** GR�FICO EM BARRA DA QUANTIDADE DE DOCUMENTOS CRIADOS **/
      $moBarChartDocumentsCreated = new ISMSGraphDocumentsCreated();
      FWDWebLib::getObject('documents_created_barChart')->setChart($moBarChartDocumentsCreated->makeGraph());
      
      /** GR�FICO EM BARRA DA QUANTIDADE DE DOCUMENTOS ACESSADOS **/
      $moBarChartDocumentsAccessed = new ISMSGraphDocumentsAccessed();
      FWDWebLib::getObject('documents_accessed_barChart')->setChart($moBarChartDocumentsAccessed->makeGraph());
      
      /** GR�FICO EM PIZZA DE QUANTIDADE DE DOCUMENTOS POR TIPO **/
      $moPieChartDocumentsType = new ISMSGraphDocumentsType();
      FWDWebLib::getObject('documents_type_pieChart')->setChart($moPieChartDocumentsType->makeGraph());
            
      /** GR�FICO EM PIZZA DE QUANTIDADE DE DOCUMENTOS POR STATUS **/
      $moPieChartDocumentsState = new ISMSGraphDocumentsState();
      FWDWebLib::getObject('documents_state_pieChart')->setChart($moPieChartDocumentsState->makeGraph());
      
      /** GR�FICO EM PIZZA DE QUANTIDADE DE DOCUMENTOS POR TIPO DE CLASSIFICA��O **/
      $moPieChartDocumentsClassification = new ISMSGraphDocumentsClassification();
      FWDWebLib::getObject('documents_classification_pieChart')->setChart($moPieChartDocumentsClassification->makeGraph());
      
      /** GR�FICO EM BARRA DOS 10 DOCUMENTOS COM MAIS REGISTROS **/
      $moPieChartDocumentRegisters = new ISMSGraphDocumentRegisters();
      FWDWebLib::getObject('document_registers_barChart')->setChart($moPieChartDocumentRegisters->makeGraph());

      /** GR�FICO EM BARRA DOS 10 PROCESSOS COM MAIS DOCUMENTOS **/
      $moPieChartProcessDocuments = new ISMSGraphProcessDocuments();
      FWDWebLib::getObject('process_documents_barChart')->setChart($moPieChartProcessDocuments->makeGraph());
      
      /** GR�FICO EM BARRA DOS 10 PROCESSOS COM MAIS PESSOAS ENVOLVIDAS **/
      $moPieChartProcessUsers = new ISMSGraphProcessUsers();
      FWDWebLib::getObject('process_users_barChart')->setChart($moPieChartProcessUsers->makeGraph());
      
      $moSelect = FWDWebLib::getObject('select_graphic');
      $moSelect->setItemValue('documents_created_barChart',FWDLanguage::getPHPStringValue('mx_amount_of_created_documents','Quantidade de Documentos criados'));
      $moSelect->setItemValue('documents_accessed_barChart',FWDLanguage::getPHPStringValue('mx_amount_of_accessed_documents','Quantidade de acessos a Documentos'));
      
      /** GR�FICO EM BARRA DA QUANTIDADE DE DOCUMENTOS CRIADOS **/
      $moBarChartDocumentsCreated = new ISMSGraphDocumentsCreated();
      FWDWebLib::getObject('documents_created_barChart')->setChart($moBarChartDocumentsCreated->makeGraph());
      
      /** GR�FICO EM BARRA DA QUANTIDADE DE DOCUMENTOS ACESSADOS **/
      $moBarChartDocumentsAccessed = new ISMSGraphDocumentsAccessed();
      FWDWebLib::getObject('documents_accessed_barChart')->setChart($moBarChartDocumentsAccessed->makeGraph());      
    } else {
      FWDWebLib::getObject('documents_created_barChart')->setShouldDraw(false);
      FWDWebLib::getObject('documents_accessed_barChart')->setShouldDraw(false);
      FWDWebLib::getObject('documents_type_pieChart')->setShouldDraw(false);
      FWDWebLib::getObject('documents_state_pieChart')->setShouldDraw(false);
      FWDWebLib::getObject('documents_classification_pieChart')->setShouldDraw(false);
      FWDWebLib::getObject('process_users_barChart')->setShouldDraw(false);
      FWDWebLib::getObject('document_registers_barChart')->setShouldDraw(false);
      FWDWebLib::getObject('process_documents_barChart')->setShouldDraw(false);
      FWDWebLib::getObject('documents_created_barChart')->setShouldDraw(false);
      FWDWebLib::getObject('documents_accessed_barChart')->setShouldDraw(false);      
      FWDWebLib::getObject('pm_optgroup')->setShouldDraw(false);
    }
  /** FIM PM_OPTGROUP **/
  /** CI_OPTGROUP **/
    //Verifica se o sistema possui o m�dulo de Gest�o de Incidente
    if(ISMSLib::hasModule(INCIDENT_MODE)){
      $mbShowVBDownload = true;
      /** GR�FICO EM PIZZA DA PROPOR��O DE INCIDENTES E TIPOS DE PERDA **/
      $moPieChartIncidentLossType = new ISMSGraphIncidentLossType();
      FWDWebLib::getObject('incident_loss_type_pieChart')->setChart($moPieChartIncidentLossType->makeGraph());
      
      /** GR�FICO EM PIZZA DA PROPOR��O DE INCIDENTES E SUAS CATEGORIAS **/
      $moPieChartIncidentCategory = new ISMSGraphIncidentCategory();
      FWDWebLib::getObject('incident_category_pieChart')->setChart($moPieChartIncidentCategory->makeGraph());
      
      /** GR�FICO EM BARRA DO N�MERO DE INCIDENTES CRIADOS NOS �LTIMOS 6 MESES **/
      $moBarChartIncidentsCreated = new ISMSGraphIncidentsCreated();
      FWDWebLib::getObject('incidents_created_barChart')->setChart($moBarChartIncidentsCreated->makeGraph());
      
      /** GR�FICO EM BARRA COM OS 10 ATIVOS QUE POSSUEM MAIS INCIDENTES) **/
      $moBarChartAssetIncident = new ISMSGraphAssetIncident();
      FWDWebLib::getObject('asset_incident_barChart')->setChart($moBarChartAssetIncident->makeGraph());
      
      /** GR�FICO EM BARRA COM OS 10 PROCESSOS QUE POSSUEM MAIS INCIDENTES) **/
      $moBarChartProcessIncident = new ISMSGraphProcessIncident();
      FWDWebLib::getObject('process_incident_barChart')->setChart($moBarChartProcessIncident->makeGraph());
      
      /** GR�FICO EM BARRA COM OS 10 PROCESSOS QUE POSSUEM MAIS N�O CONFORMIDADES) **/
      $moBarChartProcessNonConformity = new ISMSGraphProcessNonConformity();
      FWDWebLib::getObject('process_nc_barChart')->setChart($moBarChartProcessNonConformity->makeGraph());
      
      /** GR�FICO EM PIZZA DA PROPOR��O PLANOS DE A��O E SEUS TIPOS **/
      $moPieChartAPActionType = new ISMSGraphAPActionType();
      FWDWebLib::getObject('ap_action_type_pieChart')->setChart($moPieChartAPActionType->makeGraph());
      
      /** GR�FICO EM BARRA COM OS 10 INCIDENTES COM MAIOR IMPACTO FINANCEIRO) **/
      $moBarChartIncidentFinancialImpact = new ISMSGraphIncidentFinancialImpact();
      FWDWebLib::getObject('incident_financial_impact_barChart')->setChart($moBarChartIncidentFinancialImpact->makeGraph());
    } else {
      FWDWebLib::getObject('incident_financial_impact_barChart')->setShouldDraw(false);
      FWDWebLib::getObject('asset_incident_barChart')->setShouldDraw(false);
      FWDWebLib::getObject('process_incident_barChart')->setShouldDraw(false);
      FWDWebLib::getObject('process_nc_barChart')->setShouldDraw(false);
      FWDWebLib::getObject('ap_action_type_pieChart')->setShouldDraw(false);
      FWDWebLib::getObject('incident_loss_type_pieChart')->setShouldDraw(false);
      FWDWebLib::getObject('incidents_created_barChart')->setShouldDraw(false);
      FWDWebLib::getObject('incident_category_pieChart')->setShouldDraw(false);
      FWDWebLib::getObject('ci_optgroup')->setShouldDraw(false);
    }
  /** FIM CI_OPTGROUP **/
    
    if (!$mbShowVBDownload) {
      FWDWebLib::getObject('vb_download')->setShouldDraw(false);
    }
    
    //instala a seguran�a de ACL na p�gina
    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
    
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    
    $msCtrlEffCode = "";
    $msCtrlTestCode = "";
    $mbCtrlEffEnabled = $moConfig->getConfig(GENERAL_REVISION_ENABLED);
    $mbCtrlTestEnabled = $moConfig->getConfig(GENERAL_TEST_ENABLED);
    if($mbCtrlEffEnabled){
      $msCtrlEffCode = "
        function change_control_efficiency() {
          if (gebi('select_control_efficiency').value)
            trigger_event('change_control_efficiency_event','3');
        }
        change_control_efficiency();
      ";
    }
    
    if($mbCtrlTestEnabled){
      $msCtrlTestCode = "
        function change_control_test() {
          if (gebi('select_control_test').value)
              trigger_event('change_control_test_event','3');
        }
        change_control_test();
      ";
    }
    
    ?>
      <script language="javascript">
        var soSelector = new FWDSelector();
        
        function change_graphic(){
          soSelector.select(gebi('select_graphic').value);
        }
        
        change_graphic();
        
        <? echo $msCtrlEffCode . $msCtrlTestCode; ?>
      </script>
    <?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_graphics.xml");

?>