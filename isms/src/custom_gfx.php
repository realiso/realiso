<?php

include_once 'include.php';

$moWebLib = FWDWebLib::getInstance();
$moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());

$msDirectory = (ISMSLib::isCompiled() ? $GLOBALS['custom_gfx_ref'] : 'custom_gfx/');

$deniedChars = array();
$deniedChars[0] = "/";
$deniedChars[1] = "\\";

$gfx_file = str_replace($deniedChars, "", $_GET["f"]);

$customGfxFile = $msDirectory . $gfx_file;

if(file_exists($customGfxFile))
{
	print file_get_contents($customGfxFile);
}

?>