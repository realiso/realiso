<?
include_once "include.php";



$miLanguage = FWDWebLib::getPOST('user_language');

if ($miLanguage) {
	$_COOKIE['ISMSLANGUAGE'] = $miLanguage;
	setcookie('ISMSLANGUAGE', $miLanguage, time()+60*60*24*365);
	FWDLanguage::selectLanguage($miLanguage);
} else {
	if(isset($_COOKIE['ISMSLANGUAGE'])){
		FWDLanguage::selectLanguage($_COOKIE['ISMSLANGUAGE']);
	} else {
		$_COOKIE['ISMSLANGUAGE'] = FWDLanguage::getSelectedLanguage();
		setcookie('ISMSLANGUAGE', FWDLanguage::getSelectedLanguage(), time()+60*60*24*365); //1-year cookie
		FWDLanguage::selectLanguage(FWDLanguage::getSelectedLanguage());
	}
}

$title = FWDLanguage::getPHPStringValue('st_title_asaas_block', "Fim do Trial");
$body_title = FWDLanguage::getPHPStringValue('st_body_title_asaas_block', "Querido usu�rio,<br><br> � hora de decidir pelo plano que melhor se encaixa na sua organiza��o.<br/>");
$choose_plan = FWDLanguage::getPHPStringValue('st_choose_plan_asaas_block', "Selecione abaixo a op��o desejada:");
$users = FWDLanguage::getPHPStringValue('st_users_asaas_block', "Usu�rios");
$month = FWDLanguage::getPHPStringValue('st_month_asaas_block', "M�s");
$buy = FWDLanguage::getPHPStringValue('st_buy_asaas_block', "Continuar");
$wait_title = FWDLanguage::getPHPStringValue('st_wait_title_asaas_block', 'Aguarde enquanto processamos a sua requisi��o');
$wait_body = FWDLanguage::getPHPStringValue('st_wait_body_asaas_block', '<p><br>Em instantes voc� ser� redirecionado para o ambiente de pagamento para finalizar a sua compra.<br/></p>');
$warning_payment_title = FWDLanguage::getPHPStringValue('st_warning_payment_title', 'Querido usu�rio, infelizmente ainda n�o recebemos a confirma��o do seu pagamento via Paypal.');
$warning_payment_body = FWDLanguage::getPHPStringValue('st_warning_payment_body', 'Por favor, entre em contato conosco atrav�s do suporte. Acesse');

$string_data = array(
	'title' => $title,
	'body_title' => $body_title,
	'choose_plan' => $choose_plan,
	'users' => $users,
	'month' => $month,
	'buy' => $buy,
	'wait_title' => $wait_title,
	'wait_body' => $wait_body,
	'warning_payment_title' => $warning_payment_title,
	'warning_payment_body' => $warning_payment_body
);

echo json_encode(array ("status" => "success", "dados" => $string_data));

// echo json_encode($string_data);

//Testar o $msMessage = FWDLanguage::getPHPStringValue('st_license_file_not_found', "N�o foi poss�vel encontrar o arquivo de licen�a <b>'%license_name%'</b>."); include_once "include.php";
// $title = "Aviso de vencimento";
?>