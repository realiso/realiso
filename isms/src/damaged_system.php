<?php
include_once "include.php";

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $msMessage = FWDLanguage::getPHPStringValue('st_system_corrupted', "Seu sistema pode estar corrompido. Por favor, repita a instalação ou contate o nosso Suporte no site da Realiso.");
    FWDWebLib::getObject("damaged_system_message")->setValue($msMessage);
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("damaged_system.xml");
?>