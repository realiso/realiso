<?php

include_once 'include.php';

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
  }
}


class ScreenEvent extends FWDRunnable {
  public function run(){
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));

    if (ISMSLib::getConfigById(GENERAL_INSTANCE_ONSITE)) {
    	$msErrorString = FWDWebLib::getObject('param_error_string')->getValue();
    	FWDWebLib::getObject('debug_msg')->setValue($msErrorString);
    }

    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
  }
}



FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));

$msXmlFile = "popup_unexpected_behavior.xml";
if (ISMSLib::getConfigById(GENERAL_INSTANCE_ONSITE)) {
	$msXmlFile = "popup_unexpected_behavior_onsite.xml";
}

FWDWebLib::getInstance()->xml_load($msXmlFile);

?>