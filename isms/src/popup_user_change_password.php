<?php
include_once "include.php";

class ChangePasswordEvent extends FWDRunnable {
	public function run() {
  	$msNewPassword = FWDWebLib::getObject('new_password')->getValue();
    $msConfirm = FWDWebLib::getObject('confirm_password')->getValue();
    if ($msNewPassword != $msConfirm) {
      echo "js_show('label_unmatching_password_warning');";
      exit;
    }
    $moPassPolicy = new ISMSPasswordPolicy(); 
    if (!$moPassPolicy->checkPassword($msNewPassword)) {
      echo "js_show('label_weak_password_warning');";
      exit;
    }
    
    $miUserId = 0;
    $miErrorId = 0;
    $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    if ($moSession->attributeExists('loginErrorId')) {
      $miErrorId = $moSession->getAttrLoginErrorId();
    }
    if($miErrorId==ISMSUser::LOGIN_ERROR_PASSWORD_EXPIRED || $miErrorId==ISMSUser::LOGIN_ERROR_CHANGE_PASSWORD) {
      if ($moSession->attributeExists('loginErrorUserId')) {
        $miUserId = $moSession->getAttrLoginErrorUserId();
      }
    } else {
      $miUserId = $moSession->getUserId();
    }
    
    $moUserPasswordHistory = new ISMSUserPasswordHistory();
    if (!$moUserPasswordHistory->checkPassword(md5($msNewPassword),$miUserId)) {
      echo "js_show('label_existing_password_warning');";
      exit;
    }
    
    $moUser = new ISMSUser();
    $moUser->setFieldValue('user_password',md5($msNewPassword));
    $moUser->setFieldValue('user_must_change_password',false);
    $moUser->update($miUserId,false);
    
    setcookie('ISMSPASSWORD',false);
    
    $moUserPasswordHistory = new ISMSUserPasswordHistory();
    $moUserPasswordHistory->addPassword(md5($msNewPassword),$miUserId);
    
    $moUser = new ISMSUser();
    $moUser->fetchById($miUserId);
    $msUserLogin = $moUser->getFieldValue('user_login');
    
    $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    if (!$moSession->attributeExists("loginWithoutPassword"))
      $moSession->addAttribute("loginWithoutPassword");
    $moSession->setAttrLoginWithoutPassword($msUserLogin);
    
    echo "gebi('var_expired').value=0;self.close();";
	}
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new ChangePasswordEvent("change_password_event"));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $miErrorId = 0;
    $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    if ($moSession->attributeExists('loginErrorId')) {
      $miErrorId = $moSession->getAttrLoginErrorId();
    }
    if ($miErrorId==ISMSUser::LOGIN_ERROR_CHANGE_PASSWORD) {
      FWDWebLib::getObject('var_expired')->setValue(1);
      FWDWebLib::getObject('label_change_password_message')->setValue(FWDLanguage::getPHPStringValue('st_user_password_change','Voc� deve trocar sua senha agora.'));
      FWDWebLib::getObject('cancel')->setShouldDraw(false);
    }elseif($miErrorId==ISMSUser::LOGIN_ERROR_PASSWORD_EXPIRED) {
      FWDWebLib::getObject('var_expired')->setValue(1);
      FWDWebLib::getObject('label_change_password_message')->setValue(FWDLanguage::getPHPStringValue('st_user_password_expired','<b>Sua senha expirou!</b><br/>Voc� dever� troc�-la agora.'));
      FWDWebLib::getObject('cancel')->setShouldDraw(false);
    } else {
      $miUserId = $moSession->getUserId();
      $moUser = new ISMSUser();
      if ($moUser->fetchById($miUserId)) {
        $msUserLogin = $moUser->getFieldValue('user_login');
        if (!$moSession->attributeExists("loginWithoutPassword"))
          $moSession->addAttribute("loginWithoutPassword");
        $moSession->setAttrLoginWithoutPassword($msUserLogin);

        $msMessage = FWDLanguage::getPHPStringValue('st_user_password_will_expire','Sua senha ir� expirar em %days% dia(s). Se voc� quiser, pode troc�-la agora.');
        $moUserPasswordHistory = new ISMSUserPasswordHistory();
        $msPasswordTime = $moUserPasswordHistory->getCurrentPasswordTime();
        $msDaysLeft = ceil(($msPasswordTime + (86400*ISMSLib::getConfigById(PASSWORD_CHANGE_FREQUENCY)) - ISMSLib::ISMSTime())/86400) + 1;
        $msMessage = str_replace('%days%',$msDaysLeft,$msMessage);
        FWDWebLib::getObject('label_change_password_message')->setValue($msMessage);
      }
    }
    
    // Preenche o ToolTip da pol�tica de senhas
    $moPassPolicy = new ISMSPasswordPolicy();
    FWDWebLib::getObject('tooltip_helper_password_policy')->setValue($moPassPolicy->getPolicyDescriptionAsString());
    
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
    	<script language="javascript">
      gebi('new_password').focus();
      
      function hide_warnings() {
        js_hide('label_weak_password_warning');
        js_hide('label_existing_password_warning');
        js_hide('label_unmatching_password_warning');
      }
      
      self.setCloseEvent(
          function(){
            soWindow = soPopUpManager.getPopUpById('popup_user_change_password').getOpener();
            if (gebi('var_expired').value==0) soWindow.trigger_event('login_event',3);
            soWindow = null;
          }
        );
	    </script>
	  <?
  }
}

// temos que pegar a linguagem setada no cookie.
$miCookieLanguage = "";
if (isset($_COOKIE['ISMSLANGUAGE'])) {
   	$miCookieLanguage = $_COOKIE['ISMSLANGUAGE'];
   	FWDLanguage::selectLanguage($miCookieLanguage);
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_user_change_password.xml");
?>