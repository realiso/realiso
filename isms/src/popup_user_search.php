<?php
include_once "include.php";
include_once $handlers_ref . "grid/QueryGridUserSearch.php";

class GridUserSearch extends FWDDrawGrid {
  public function DrawItem(){
    switch($this->ciColumnIndex){
      case 1:
        $msGfxRef = FWDWebLib::getInstance()->getGfxRef();
      $moIcon = new FWDIcon(new FWDBox(2,4));
    $moIcon->setAttrSrc("{$msGfxRef}icon-user.gif");    
    return $moIcon->draw();
      break;  
          
      default:
        return parent::drawItem();
      break;
    }
  }
}

class SearchUserEvent extends FWDRunnable {
  public function run(){
    $moGrid = FWDWebLib::getObject('grid_user_search');
    $moGrid->execEventPopulate();
  }
}

class InsertUserEvent extends FWDRunnable {
  public function run() {    
    $maGridValue = FWDWebLib::getObject('grid_user_search')->getValue();    
    if (count($maGridValue)) {        
      echo "soPopUp = soPopUpManager.getPopUpById('popup_user_search');
        soWindow = soPopUp.getOpener();            
        soWindow.set_user({$maGridValue[0][1]},'{$maGridValue[0][3]}');
        soPopUpManager.closePopUp('popup_user_search');";
    }
    else echo "js_show('warning');";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
    public function run(){
      $moStartEvent = FWDStartEvent::getInstance();
      $moStartEvent->setScreenEvent(new ScreenEvent("")); 
      $moStartEvent->addAjaxEvent(new SearchUserEvent("search_user_event"));
      $moStartEvent->addAjaxEvent(new InsertUserEvent("insert_user_event"));
      
      $moGrid = FWDWebLib::getObject("grid_user_search");
      $moHandler = new QueryGridUserSearch(FWDWebLib::getConnection());
      
      $moHandler->setLogin(FWDWebLib::getObject("var_user_login")->getValue());
      $moHandler->setName(FWDWebLib::getObject("var_user_name")->getValue());
      $msIds = FWDWebLib::getObject("exclude_users")->getValue();    
      if (is_string($msIds)) $moHandler->setExludedUsers(FWDWebLib::unserializeString($msIds));
      
      $moGrid->setQueryHandler($moHandler);
      $moGrid->setObjFwdDrawGrid(new GridUserSearch());
    }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        function search(){
          gobi('warning').hide();
          gebi('var_user_login').value = gebi('user_login').value;
          gebi('var_user_name').value = gebi('user_name').value;
          gobi('grid_user_search').setPopulate(true);
          trigger_event("search_user_event","3");
        }
      </script>
    <? 
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_user_search.xml");
?>