<?php
set_time_limit(3000);

require_once('include.php');
require_once("$base_ref/translation/include.php");

// id do sistema da tabela de 'lic_system'
$miSystemId = 53;

// Inicializa o DataSet pro driver de BD
$soDB = new FWDDB(DB_POSTGRES, "isms_tools", "postgres", "informant", "127.0.0.1");
$soDataSet = new FWDDBDataSet($soDB, 'translation_strings');
$soDataSet->addFWDDBField(new FWDDBField('skSystem','string_system_id',DB_NUMBER,$miSystemId));
$soDataSet->addFWDDBField(new FWDDBField('zId','string_id',DB_STRING));
$soDataSet->addFWDDBField(new FWDDBField('zFile','string_file',DB_STRING));
$soDataSet->addFWDDBField(new FWDDBField('zValue','string_value',DB_STRING));
$soDataSet->addFWDDBField(new FWDDBField('dDateInclusion','string_date_inclusion',DB_DATETIME));
$soDataSet->addFWDDBField(new FWDDBField('zPortuguesIsms','string_portugues_isms', DB_STRING));
$soDataSet->addFWDDBField(new FWDDBField('zSpanishIsms','string_spanish_isms', DB_STRING));
$soDataSet->addFWDDBField(new FWDDBField('zEnglishIsms','string_english_isms', DB_STRING));
$soDataSet->addFWDDBField(new FWDDBField('zChineseIsms','string_chinese_isms', DB_STRING));
$soDataSet->addFWDDBField(new FWDDBField('zPortuguesEms','string_portuguese_ems', DB_STRING));
$soDataSet->addFWDDBField(new FWDDBField('zPortuguesOhs','string_portuguese_ohs', DB_STRING));
$soDataSet->addFWDDBField(new FWDDBField('zEnglishEms','string_english_ems', DB_STRING));
$soDataSet->addFWDDBField(new FWDDBField('zEnglishOhs','string_english_ohs', DB_STRING));
$soDataSet->addFWDDBField(new FWDDBField('zEnglishSox','string_english_sox', DB_STRING));
$soDataSet->addFWDDBField(new FWDDBField('zEnglishPci','string_english_pci', DB_STRING));
$soDataSet->addFWDDBField(new FWDDBField('zGermanSox','string_german_sox', DB_STRING));
$soDataSet->addFWDDBField(new FWDDBField('zFrenchSox','string_french_sox', DB_STRING));
$soDataSet->addFWDDBField(new FWDDBField('zPortugues3380','string_portugues_3380', DB_STRING));

$soDataSet->getFieldByAlias('string_system_id')->addFilter(new FWDDBFilter("=", $miSystemId));

$soDataSet->setOrderBy('string_file', '+');
$soDataSet->setOrderBy('string_id', '+');

// obtem do sistema de tradu��o, as tradu��es e armazena elas nos arquivos de tradu��o do sistema
$miTimeStampLocal = microtime(1);
echo "<h2>Transfer�ncia do banco para o PHP";

// Cria o driver de BD usando o DataSet
$soDBDriver = new FWDDBDriver($soDataSet);
$soTranslate = new FWDTranslate($soDBDriver);

// Armazena em um arquivo .php as strings lidas com driver de BD
$soTranslate->storeToPHPFile('language/portuguese_isms.php', 'portugues_isms');
$soTranslate->storeToPHPFile('language/english_isms.php', 'english_isms');
$soTranslate->storeToPHPFile('language/chinese_isms.php', 'chinese_isms');
$soTranslate->storeToPHPFile('language/portuguese_ems.php', 'portuguese_ems');
$soTranslate->storeToPHPFile('language/portuguese_ohs.php', 'portuguese_ohs');
$soTranslate->storeToPHPFile('language/english_ems.php', 'english_ems');
$soTranslate->storeToPHPFile('language/english_ohs.php', 'english_ohs');
$soTranslate->storeToPHPFile('language/english_sox.php', 'english_sox');
$soTranslate->storeToPHPFile('language/english_pci.php', 'english_pci');
$soTranslate->storeToPHPFile('language/spanish_isms.php', 'spanish_isms');
$soTranslate->storeToPHPFile('language/german_sox.php', 'german_sox');
$soTranslate->storeToPHPFile('language/french_sox.php', 'french_sox');
$soTranslate->storeToPHPFile('language/portuguese_3380.php', 'portugues_3380');

echo ' em ' . number_format(microtime(1) - $miTimeStampLocal,2) . ' segundo(s).</h2>';

echo "ANTES DE FAZER O COMMIT, LEMBRAR DE INCLUIR AS STRINGS TRADUZIDAS DA FWD!";
echo "<BR><BR>O ARQUIVO PORTUGUESE.PHP N�O PODE POSSUIR STRINGS TRADUZIDAS EM SEU CONTE�DO!";
?>