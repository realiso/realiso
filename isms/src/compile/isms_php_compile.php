<?php
set_time_limit(0);
function phpCompile2($paDirectories, $psCompiledFileName, $psSrcRef) {
  $mrHandle = fopen($psCompiledFileName, "w+");  
  if($mrHandle){
    foreach($paDirectories as $msDirectory => $maFiles){
    	if ($msDirectory == "classes2") $msDirectory = "classes";     
      $msDirectory = $psSrcRef.$msDirectory;
      if(is_dir($msDirectory)){
        foreach ($maFiles as $msFile) {
          $msFileFullPath = $msDirectory . "/" . $msFile;
          echo $msFileFullPath . "<br/>";
          $msExt = strtolower(substr($msFile,strrpos($msFile,'.')+1));
          $msFileContent = file_get_contents($msFileFullPath);          
          fwrite($mrHandle,$msFileContent);          
        }
      }else{
        echo "$msDirectory is not a directory!<br/>";
      }
    }
    return true;
  }
  else {
    echo "Could not create file = $psCompiledFileName!</br>";
    return false;
  }
}

function check_extension($file, $extensions) {
	$aux = explode(".",strtolower(trim($file)));
	$file_extension = $aux[count($aux)-1];	
	foreach($extensions as $extension) if ($file_extension == $extension) return false;	
	return true;
}

function dircopy($srcdir, $dstdir, $directories_to_ignore = array(), $extensions_to_ignore = array(), $extensions_to_allow = array(), $files_to_ignore = array()) {
  $num = 0;
  if(!is_dir($dstdir)) mkdir($dstdir, 0777, true);
  if($curdir = opendir($srcdir)) {
   while($file = readdir($curdir)) {
     if($file != '.' && $file != '..' && !in_array($file, $directories_to_ignore)) {
       $srcfile = $srcdir . '/' . $file;
       $dstfile = $dstdir . '/' . $file;
       if(is_file($srcfile) && !in_array($file, $files_to_ignore)) {
       	 if (count($extensions_to_ignore) && !check_extension($file, $extensions_to_ignore)) continue; 
       	 if (count($extensions_to_allow) && check_extension($file, $extensions_to_allow)) continue;       	 
         echo "<br>Copying '$srcfile' to '$dstfile'...";
         if(copy($srcfile, $dstfile)) {
           touch($dstfile, filemtime($srcfile)); $num++;
           echo "OK\n";
         }
         else echo "<br>Error: File '$srcfile' could not be copied!\n";
       }
       else if(is_dir($srcfile)) {
         $num += dircopy($srcfile, $dstfile, $directories_to_ignore, $extensions_to_ignore, $extensions_to_allow, $files_to_ignore);
       }
     }
   }
   closedir($curdir);
  }
  return $num;
}

$ssBaseDir = "/home/real/Projects/realiso/realiso";
$ssFWD = "/fwd";
$ssDir = "/home/real/Projects/realiso/realiso/new_deploy_isms";
$ssClasses = $ssDir."/classes/";

if (!file_exists($ssDir)) mkdir($ssDir);
if (!file_exists($ssClasses)) mkdir($ssClasses);

echo "<h3>>>>>>  INICIO DO ARQUIVO <<<<<< </h3>";
echo "<h3>COMPILING OS PHPS</h3>";

$saFiles = array(
"classes" => array("ISMSXMLParser.php",
										"ISMSTable.php",
										"ISMSContext.php",
										"Workflow.php")
);
phpCompile2($saFiles, $ssClasses. "isms_classes0.php", "../");

$saFiles = array(
"classes" => array("ISMSLib.php",
										"ISMSLicense.php",
										"ISMSSession.php",
                    "ISMSSaasErrorHandler.php",
										"ISMSAuditLog.php",
										"ISMSActivity.php",
										"ISMSActiveDirectory.php", 
										"ISMSASaaS.php", 		
										"IAssociation.php", 
										"ISMSContextDate.php",
										"ISMSContextObject.php",
										"ISMSContextClassification.php", 
										"IWorkflow.php",
										"WKFAlert.php",
										"WKFTask.php",
										"WKFTaskMetadata.php",
                    "WKFSchedule.php", 
                    "WKFTaskSchedule.php",
                    "ISMSTaskManager.php",
                    "ISMSHandleJSError.php",
                    "ISMSSyncSite.php"),
										
"classes/risk_management" => array(		"RMWorkflow.php",
																			"WorkflowProcessAsset.php",
																			"RiskLimitsWorkflow.php",
																			"RMArea.php",
																			"RMProcess.php",
																			"RMAssetAsset.php",
																			"RMAsset.php",
																			"RMProcessAsset.php",
																			"RMRisk.php",
																			"RMAcceptRisk.php",
																			"RMRiskControl.php",
																			"RMRiskConfig.php",
																			"RMRiskLimits.php", 
																			"RMCategory.php",
																			"RMEvent.php",
																			"RMBestPractice.php",
																			"RMStandard.php", 
																			"RMSectionBestPractice.php", 
																			"RMControl.php",
																			"RMControlBestPractice.php",
																			"WKFControlEfficiency.php",
																			"RMControlCost.php",
																			"ControlSchedule.php",
																			"WKFControlTest.php",
																			"RMControlEfficiencyHistory.php",
																			"RMControlTestHistory.php",
																			"RMControlImplementationAccept.php",
                                      "WorkflowRiskControl.php", 
																			"RMBestPracticeStandard.php",
																			"RMBestPracticeEvent.php"),

"classes/policy_management" => array("DocumentSchedule.php",
																			"PMDocument.php",
																			"PMDocInstance.php",
																			"PMProcessUser.php",
																			"PMInstanceContent.php",
																			"PMDocReference.php",
																			"PMDocumentReader.php",
																			"PMInstanceComment.php",
																			"PMDocumentApprover.php",
																			"PMRegister.php",
																			"PMWorkflow.php",
																			"PMDocumentRevisionHistory.php",
																			"PMDocRegisters.php",
																			"PMDocReadHistory.php",
																			"PMDocContext.php",
																			"PMTemplate.php",
																			"PMTemplateContent.php",
																			"PMTemplateBestPractice.php",
																			"PMRegisterReader.php"
																		),
  
"classes/continual_improvement" => array("OccurrenceWorkflow.php",
																					"NCWorkflow.php",
																					"CIIncident.php",
																					"CIIncidentUser.php",
																					"CICategory.php",
																					"CISolution.php",
																					"CIOccurrence.php",
																					"CINonConformity.php",
																					"CINonConformityProcess.php",
																					"CINonConformitySeed.php",
																					"CIRiskProbability.php",
																					"CIRiskSchedule.php",
																					"CIIncidentRisk.php",
																					"CIIncidentRiskParameter.php",
																					"CIIncidentControl.php",
																					"CIIncidentProcess.php",
																					"CIIncidentFinancialImpact.php",
																					"ImprovementSchedule.php",
																					"IncidentWorkflow.php",
																					"CIActionPlan.php",
																					"CINcActionPlan.php",
																					"ActionPlanWorkflow.php"
																		),
																			
"classes2" => array("ISMSLibraryShowImport.php",
										"ISMSLibraryImport.php",
										"ISMSLibraryExport.php", 
										"ISMSConfig.php",
										"ISMSSolicitor.php",
										"ISMSUser.php",
										"ISMSUserPasswordHistory.php",
										"ISMSProfile.php",
                    					"ISMSProfileAcl.php",
										"ISMSPasswordPolicy.php",
										"TmpAdUser.php",
										"ISMSUserPreferences.php",
										"ISMSEmailPreferences.php",
										"ISMSGridUserPreferences.php", 
										"ISMSMailer.php",
										"ISMSNonConformityTypes.php",
										"ISMSEmailTemplates.php",
                    "ISMSScope.php",
                    "ISMSPolicy.php",
                    "ISMSSaaS.php",
                    "ISMSReportExcelWriter.php",
                    "ISMSSaaSAdmin.php",
                    "ISMSFile.php")
);
phpCompile2($saFiles, $ssClasses. "isms_classes1.php", "../");

$saFiles = array(
"classes/risk_management/riskparameters" => array("RMIValue.php",
																									"RiskParameters.php",
																									"RiskParametersValue.php",
																									"RMAssetValue.php",
																									"RMAssetDependency.php",
																									"RMRiskValue.php",
																									"CIIncidentRiskValue.php",
																									"RMRiskControlValue.php",
																									"RMParameterName.php",
																									"RMParameterValueName.php",
																									"RMRCParameterValueName.php")
);
phpCompile2($saFiles, $ssClasses. "isms_riskparameters.php", "../");

$saFiles = array(
"reports" => array("ISMSReport.php",										
										"ISMSReportTemplate.php",
										"ISMSReportWindow.php",
										"ISMSReportHideLoadingEvent.php",
                    "ISMSFilteredReport.php"),
										
"classes/risk_management/report" => array("ISMSReportGenericClassifTypePrioFilter.php",
																					"ISMSReportRiskTreatmentPlan.php",
																					"ISMSReportControlSummary.php",
                                          "ISMSReportControlSummaryPM.php",
																					"ISMSReportControlAccompaniment.php",
																					"ISMSReportControlEfficiencyAccompaniment.php",
																					"ISMSReportAreasWithoutProcesses.php",
																					"ISMSReportAssetsWithoutRisks.php",
																					"ISMSReportControlsNotMeasured.php",
																					"ISMSReportRiskValues.php",
																					"ISMSReportRiskValuesFilter.php",
																					"ISMSReportRisksByControl.php",
																					"ISMSReportRisksByControlFilter.php",
																					"ISMSReportStatementOfApplicabilityFilter.php",
																					"ISMSReportUserResponsabilities.php",
																					"ISMSReportUserResponsabilitiesFilter.php",
																					"ISMSReportDetailedPendantTasks.php",
																					"ISMSReportSummarizedPendantTasks.php",
																					"ISMSReportPendantContexts.php",
																					"ISMSReportAsset.php",
																					"ISMSReportAssetFilter.php",
																					"ISMSReportProcessesWithoutAssets.php",
																					"ISMSReportRisksByAsset.php",
																					"ISMSReportRisksByAssetFilter.php",
																					"ISMSReportRisksByProcess.php",
																					"ISMSReportRisksByProcessFilter.php",
																					"ISMSReportControlsByRisk.php",
																					"ISMSReportControlsByAsset.php",
																					"ISMSReportControlsByResponsible.php",
																					"ISMSReportControlEfficiency.php",
																					"ISMSReportControlEfficiencyFilter.php",
																					"ISMSReportControlImplementationDate.php",
																					"ISMSReportControlPlanning.php",
																					"ISMSReportControlPlanningFilter.php",
																					"ISMSReportControlCostByResponsible.php",
																					"ISMSReportControlCostByAsset.php",
																					"ISMSReportControlCostByProcess.php",
																					"ISMSReportControlCostByArea.php",  
																					"ISMSReportTop10AssetsWithMoreRisks.php",
																					"ISMSReportTop10AssetsWithMoreRisksFilter.php",
																					"ISMSReportControlCost.php",
																					"ISMSReportRiskAmountByProcess.php",
																					"ISMSReportRiskAmountByArea.php",
																					"ISMSReportRiskStatusByArea.php",
																					"ISMSReportRiskStatusByProcess.php",
																					"ISMSReportRisksResume.php",
																					"ISMSReportRisksResumeFilter.php",
																					"ISMSReportPendantTasksFilter.php",
																					"ISMSReportTop10RisksByProcess.php",
																					"ISMSReportTop10RisksByArea.php",																					
																					"ISMSReportStatementOfApplicability.php",
																					"ISMSReportRiskImpact.php",
																					"ISMSReportRiskImpactFilter.php",
																					"ISMSReportAssetImportance.php",
																					"ISMSReportParametersFilter.php",
																					"ISMSReportRisksByArea.php",
																					"ISMSReportRisksByAreaFilter.php",  
																					"ISMSReportEventsByCategory.php",
																					"ISMSReportEventsByCategoryFilter.php",
																					"ISMSReportControlsWithoutRisks.php",
																					"ISMSReportAreasByType.php",
																					"ISMSReportAreasByPriority.php",
																					"ISMSReportProcessesByType.php",
																					"ISMSReportProcessesByPriority.php",
																					"ISMSReportRisksByType.php",
																					"ISMSReportEventsByType.php",
																					"ISMSReportControlsByType.php",
                                          "ISMSReportControlTestAccompaniment.php",
                                          "ISMSReportConformity.php",
                                          "ISMSReportConformityFilter.php",
                                          "ISMSReportAreas.php",
                                          "ISMSReportAreasFilter.php",
                                          "ISMSReportProcesses.php",
                                          "ISMSReportProcessesFilter.php",
                                          "ISMSReportAssets.php",
                                          "ISMSReportAssetsFilter.php",
                                          "ISMSReportRisks.php",
                                          "ISMSReportRisksFilter.php",
                                          "ISMSReportControls.php",
                                          "ISMSReportControlsFilter.php",
                                          "ISMSReportBestPractices.php",
                                          "ISMSReportBestPracticesFilter.php",
                                          "ISMSReportControlCostByCostCategory.php",
                                          "ISMSReportControlCostByCostCategoryFilter.php",
                                          "ISMSReportRiskFinancialImpact.php",
                                          "ISMSReportRiskFinancialImpactFilter.php",
                                          "ISMSReportAssetDependenciesAndDependents.php",
                                          "ISMSReportAssetDependenciesAndDependentsFilter.php",
                                          "ISMSReportControlRevisionAgendaByResponsible.php",
                                          "ISMSReportControlTestAgendaByResponsible.php",
                                          "ISMSReportAssetRelevance.php",
										  "ISMSReportControlsByProcess.php",
                                          "ISMSReportControlByProcessFilter.php"
                                          ),
                                          
"classes/admin/report" => array("ISMSReportAuditTask.php",
																"ISMSReportAuditTaskFilter.php",
																"ISMSReportAuditAlert.php",
																"ISMSReportAuditAlertFilter.php",
																"ISMSReportAuditLog.php",
																"ISMSReportAuditLogFilter.php"),
																
"classes/policy_management/report" => array(
																	"ISMSReportAccessedDocuments.php",
																	"ISMSReportAccessedDocumentsFilter.php",
																	"ISMSReportDocumentAccesses.php",
																	"ISMSReportDocumentAccessesFilter.php",
																	"ISMSReportDocumentRegisters.php",
																	"ISMSReportDocumentRegistersFilter.php",																	
																	"ISMSReportDocumentApprovers.php",
																	"ISMSReportDocumentApproversFilter.php",
																	"ISMSReportDocumentReaders.php",
																	"ISMSReportDocumentReadersFilter.php",
																	"ISMSReportTopRevisedDocuments.php",
																	"ISMSReportTopRevisedDocumentsFilter.php",
																	"ISMSReportTopAccessedDocuments.php",
																	"ISMSReportTopAccessedDocumentsFilter.php",
																	"ISMSReportNotAccessedDocuments.php",
																	"ISMSReportNotAccessedDocumentsFilter.php",
																	"ISMSReportDocumentsPendentReads.php",
																	"ISMSReportDocumentsPendentReadsFilter.php",
																	"ISMSReportContextsWithoutDocuments.php",																	
																	"ISMSReportUsersByProcess.php",
																	"ISMSReportUsersByProcessFilter.php",
																	"ISMSReportDocumentDates.php",
																	"ISMSReportDocumentDatesFilter.php",                                  
                                  "ISMSReportDocumentsRevisionSchedule.php",
                                  "ISMSReportDocumentsRevisionLate.php",
                                  "ISMSReportDocInstanceCommentAndJustification.php",
                                  "ISMSReportDocInstanceCommentAndJustificationFilter.php",
                                  "ISMSReportUserComments.php",
                                  "ISMSReportUserCommentsFilter.php",
																	"ISMSReportDocumentsByArea.php",
																	"ISMSReportDocumentsByAreaFilter.php",
																	"ISMSReportDocumentSummary.php",
																	"ISMSReportDocumentSummaryFilter.php",
																	"ISMSReportRegistersByType.php",
																	"ISMSReportDocumentsByType.php",
																	"ISMSReportElementClassificationFilter.php",
                                  "ISMSReportDetailedPendantTasksPM.php",
                                  "ISMSReportSummarizedPendantTasksPM.php",
                                  "ISMSReportDocumentsWithoutRegister.php",
                                  "ISMSReportDocumentsWithoutRegisterFilter.php",
                                  "ISMSReportUsersWithPendantRead.php",
                                  "ISMSReportUsersWithPendantReadFilter.php",
                                  "ISMSReportDocsWithHighFrequencyRevision.php",
                                  "ISMSReportDocsWithHighFrequencyRevisionFilter.php",
                                  "ISMSReportPendantApprovals.php",
                                  "ISMSReportDocumentsByComponent.php",
                                  "ISMSReportDocumentsByComponentFilter.php",
                                  "ISMSReportDocumentsByState.php",
                                  "ISMSReportDocumentsByStateFilter.php"),

"classes/continual_improvement/report" => array(
																	"ISMSReportOccurrenceByIncident.php",  
																	"ISMSReportUserByIncident.php",
																	"ISMSReportIncidentByResponsible.php",
																	"ISMSReportOccurrenceAccompaniment.php",  
																	"ISMSReportIncidentAccompaniment.php",
																	"ISMSReportIncidentFilter.php",
                                          							"ISMSReportActionPlanFilter.php",
																	"ISMSReportOccurrenceFilter.php",
																	"ISMSReportNonConformityFilter.php",
																	"ISMSReportNonConformityByProcess.php",
																	"ISMSReportNonConformityAccompaniment.php",
																	"ISMSReportControlNonConformityByProcess.php",
																	"ISMSReportFinancialImpactByIncident.php",
																	"ISMSReportIncidentControlRevision.php",
																	"ISMSReportElementsAffectedByIncident.php",
																	"ISMSReportElementsFilter.php",
																	"ISMSReportRiskAutoProbabilityValue.php",
                                  "ISMSReportDetailedPendantTasksCI.php",
                                  "ISMSReportSummarizedPendantTasksCI.php",
                                  "ISMSReportIncidentWithoutRisk.php",
                                  "ISMSReportNCWithoutAP.php",
                                  "ISMSReportAPWithoutDoc.php",
                                  "ISMSReportActionPlanByUser.php",
                                  "ISMSReportActionPlanByProcess.php",
                                  "ISMSReportDuplicatedRisksByAsset.php",
                                  "ISMSReportDuplicatedRisks.php",
                                  "ISMSReportIncidentWithoutOccurrence.php",
                                  "ISMSReportIncidents.php",
                                  "ISMSReportIncidentsFilter.php",
                                  "ISMSReportNonConformities.php",
                                  "ISMSReportNonConformitiesFilter.php",
                                  "ISMSReportDisciplinaryProcess.php",
                                  "ISMSReportDisciplinaryProcessFilter.php"
                                  )
);
phpCompile2($saFiles, $ssClasses. "isms_report.php", "../");

$saFiles = array(
"graphs" => array("ISMSGraphPieChart.php",
									"ISMSGraphBarChart.php",
									"ISMSGraphLineChart.php",
									"ISMSGraphPotentialRisk.php",
									"ISMSGraphResidualRisk.php",
									"ISMSGraphAssetEstimatedCost.php",
									"ISMSGraphAssetCost.php",
									"ISMSGraphProcessAsset.php",
									"ISMSGraphAssetRisk.php",
									"ISMSGraphProcessRisk.php",
									"ISMSGraphRedAssetRisk.php",
									"ISMSGraphRedProcessRisk.php",
									"ISMSGraphTreatedRisk.php",
									"ISMSGraphControlEfficiency.php",
									"ISMSGraphControlTest.php",
									"ISMSGraphSingleStatistic.php",
									"ISMSGraphIncidentTotalStatistic.php",
									"ISMSGraphMultipleStatistics.php",
									"ISMSGraphDocumentsCreated.php",
									"ISMSGraphDocumentsAccessed.php",
									"ISMSGraphDocumentsType.php",
									"ISMSGraphDocumentsState.php",
									"ISMSGraphDocumentsClassification.php",
									"ISMSGraphProcessUsers.php",
									"ISMSGraphProcessDocuments.php",
									"ISMSGraphDocumentRegisters.php",
									"ISMSGraphIncidentLossType.php",
									"ISMSGraphIncidentCategory.php",
									"ISMSGraphIncidentsCreated.php",
									"ISMSGraphAssetIncident.php",
									"ISMSGraphProcessIncident.php",
									"ISMSGraphProcessNonConformity.php",
									"ISMSGraphAPActionType.php",
									"ISMSGraphIncidentFinancialImpact.php")
);
phpCompile2($saFiles, $ssClasses. "isms_graph.php", "../");

echo "<h3>COPYING CONTEXTS_INFO.PHP</h3>";
if (copy("../classes/contexts_info.php", $ssDir."/classes/contexts_info.php"))
	echo "Copying '../classes/contexts_info.php' to '$ssDir/classes/contexts_info.php'...OK";
else
	echo "'Copying ../classes/contexts_info.php to $ssDir/classes/contexts_info.php'...<font color='red' size='20'>FAILED</font>";

echo "<h3>COPYING ACL_TAGS.PHP</h3>";
if (copy("../classes/acl_tags.php", $ssDir."/classes/acl_tags.php"))
  echo "Copying '../classes/acl_tags.php' to '$ssDir/classes/acl_tags.php'...OK";
else
  echo "'Copying ../classes/acl_tags.php to $ssDir/classes/acl_tags.php'...<font color='red' size='20'>FAILED</font>";

echo "<h3>COPYING SYSTEM_SETUP.PHP</h3>";
if (copy("../classes/system_setup.php", $ssDir."/classes/system_setup.php"))
  echo "Copying '../classes/system_setup.php' to '$ssDir/classes/system_setup.php'...OK";
else
  echo "'Copying ../classes/system_setup.php to $ssDir/classes/system_setup.php'...<font color='red' size='20'>FAILED</font>";
  
echo "<h3>COPYING SETUP.PHP</h3>";
if (copy("../classes/setup.php", $ssDir."/classes/setup.php"))
  echo "Copying '../classes/setup.php' to '$ssDir/classes/setup.php'...OK";
else
  echo "'Copying ../classes/setup.php to $ssDir/classes/setup.php'...<font color='red' size='20'>FAILED</font>";
  
echo "<h3>COPYING LANGUAGE</h3>";
dircopy("../language", $ssDir."/language", array('.svn'), array(), array('php'));

echo "<h3>COPYING HANDLERS</h3>";
dircopy("../handlers", $ssDir."/handlers", array('.svn'), array(), array('php'));

echo "<h3>COPYING PACKAGES</h3>";
dircopy("../packages", $ssDir."/packages", array('.svn'), array(), array('php', 'xmc'));

echo "<h3>COPYING SRC</h3>";
dircopy("..", $ssDir, array('.svn', 'language', 'compile', 'graphs', 'reports', 'migration', 'handlers', 'packages', 'classes', 'lib', 'gfx'), array(), array('php', 'xmc', 'html'), array('isms_xml_compile.php', 'translation_scan_files.php', 'translation_update_files.php', 'license.php', 'license_.php'));

echo "<h3>COPYING NONAUTO CLASSES</h3>";
dircopy("../classes/nonauto", $ssDir."/classes/nonauto", array('.svn'), array(), array('php'));

echo "<h3>COPYING GFX</h3>";
dircopy("../gfx", $ssDir."/gfx", array('.svn'), array('db'), array(), array());

echo "<h3>COPYING NONAUTO CSS</h3>";
dircopy("../lib/css/nonauto", $ssDir."/lib/css/nonauto", array('.svn'));

echo "<h3>COPYING JAVASCRIPT</h3>";
dircopy($ssBaseDir.$ssFWD."/src/base/javascript/nonauto", $ssDir."/lib/javascript", array('.svn'));

echo "<h3>COPYING NONAUTO JAVASCRIPT</h3>";
dircopy("../lib/javascript/nonauto", $ssDir."/lib/javascript/nonauto", array('.svn'));

echo "<h3>>>>>>  FIM DO ARQUIVO <<<<<< </h3>";

?>
