<?php
include_once "include.php";

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    
    if (isset($_COOKIE['ISMSLANGUAGE'])) {
    	$miSelectedLanguage = $_COOKIE['ISMSLANGUAGE'];
    	FWDLanguage::selectLanguage($miSelectedLanguage);
    }
    
    $msMessage = FWDLanguage::getPHPStringValue('st_license_expired', "Sua licen�a expirou em <b>%expiracy_str%</b>.");
    $msMessage = str_replace("%expiracy_str%",ISMSLib::getISMSShortDate(FWDWebLib::getObject("param_expiracy")->getValue(),true),$msMessage);
    FWDWebLib::getObject("license_expired_message")->setValue($msMessage);
    $msEvent = "soPopUpManager.closePopUp('popup_license_expired');";
    $moEvent = new FWDClientEvent();
    $moEvent->setAttrEvent('onClick');
    $moEvent->setAttrValue($msEvent);
    FWDWebLib::getObject("vb_ok")->addObjFWDEvent($moEvent);
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("license_expired.xml");
?>