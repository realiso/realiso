<?php
include_once "include.php";
include_once $handlers_ref . "QueryTasks.php";

// Verifica se o usu�rio n�o est� tentando acessar o sistema pela URL errada
ISMSLib::checkUrl();

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    $mbHasCIModule =" var sbHasCIModule=0; ";
    if(FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(INCIDENT_MODE)){
      $mbHasCIModule =" var sbHasCIModule=1; ";
    }
    ?>
		<!--[if lt IE 7]>
		<script defer type='text/javascript' language='javascript'>
		  correct_png();
		</script>
		<![endif]-->
      <script language="javascript">
        <?echo $mbHasCIModule?>
        document.body.className = 'BODYBackground';
        window.onresize = center_dialog_div;
        center_dialog_div();
        function refresh_grids() {
          window.location.reload();
        }

        function open_task_view(type,task_id,context_id,context_type) {
          switch (type){
            case 2:
              isms_open_popup('popup_complex_task_view', 'popup_complex_task_view.php?task='+task_id, '', 'true', 272, 400);
            break;
            case 3:
              isms_open_popup('popup_simple_task_view', 'popup_simple_task_view.php?task='+task_id, '', 'true', 261, 440);
              break;
            case 4:
              isms_open_popup('popup_new_risk_parameter_approval_task', 'packages/improvement/popup_new_risk_parameter_approval_task.php?task='+task_id, '', 'true', 296, 440);  
              break;
          }
        }

        function open_task_control_revision(task_id,context_id) {
          if(sbHasCIModule)
             isms_open_popup('popup_control_revision_task', 'packages/risk/popup_control_revision_task.php?task='+task_id+'&control='+context_id, '', 'true', 380, 800);
          else
             isms_open_popup('popup_control_revision_task', 'packages/risk/popup_control_revision_task.php?task='+task_id+'&control='+context_id, '', 'true', 380, 390);
        }

        function open_task_control_test(task_id,context_id) {
          isms_open_popup('popup_control_test_task', 'packages/risk/popup_control_test_task.php?task='+task_id+'&control='+context_id, '', 'true', 422, 400);
        }
      </script>
    <?
    $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    $miTaskId = FWDWebLib::getObject("task_id")->getValue();

    if($miTaskId){
      // Utilizado para indicar ao sistema que o sistema est� sendo aberto
      //por um link de task dos emails de task gerados pelo sistema. Assim ao abrir
      //a popup de visualiza��o, ela ser� aberta corretamente
      ?>
        <script language="javascript">
           setISMSMode(TASK_EMAIL_MODE);
        </script>
      <?
    }
    //$_SERVER['REMOTE_USER'] = 'mschmidt';

    if (isset($_SERVER['REMOTE_USER'])) {
    	$moSession->login($_SERVER['REMOTE_USER']);
    }

    if ($moSession->getUserId()) {
        $moUser = new ISMSUser();
        $miUserId = $moSession->getUserId();
        $moUser->fetchById($miUserId);

        /* Se foi passada uma task, deve abrir uma popup para edi��o da mesma */

        if ($miTaskId) {
          $moQueryTasks = new QueryTasks(FWDWebLib::getConnection());
          $moQueryTasks->setUser($miUserId);
          $moQueryTasks->setTask($miTaskId);
          $moQueryTasks->makeQuery();
          $moQueryTasks->executeQuery();
          $moQueryTasksDataset = $moQueryTasks->getDataset();
          if ($moQueryTasksDataset->fetch()) {
            $miActivity = $moQueryTasksDataset->getFieldByAlias("task_activity")->getValue();
            $miContextId = $moQueryTasksDataset->getFieldByAlias("task_context_id")->getValue();
            $miContextType = $moQueryTasksDataset->getFieldByAlias("task_context_type")->getValue();
            if ($miActivity == ACT_REAL_EFFICIENCY) {
              // popup_control_revision
              echo "
                <script language='javascript'>
                  open_task_control_revision(".$miTaskId.",".$miContextId.");
				  soPopUpManager.getPopUpById('popup_control_revision_task').setCloseEvent(function (){document.location.href = 'tab_main.php';});
                </script>
                ";
            }
            elseif($miActivity == ACT_CONTROL_TEST){
                echo "
                  <script language='javascript'>
                    open_task_control_test(".$miTaskId.",".$miContextId.");
				  soPopUpManager.getPopUpById('popup_control_test_task').setCloseEvent(function (){document.location.href = 'tab_main.php';});
                  </script>
                  ";
            }
            elseif (($miContextType == CONTEXT_PROCESS_ASSET) || ($miContextType == CONTEXT_RISK_CONTROL)) {
              // popup_complex_task_view
              echo "
                <script language='javascript'>
                  open_task_view(2,".$miTaskId.");
				  soPopUpManager.getPopUpById('popup_complex_task_view').setCloseEvent(function (){document.location.href = 'tab_main.php';});
                </script>
                ";
            }
            elseif($miActivity == ACT_NEW_RISK_PARAMETER_APPROVAL) {
              echo "
                <script language='javascript'>
                  open_task_view(4,".$miTaskId.");
				  soPopUpManager.getPopUpById('popup_new_risk_parameter_approval_task').setCloseEvent(function (){document.location.href = '../../tab_main.php';});
                </script>
                ";
            	
            	
            } else {
              // popup_simple_task_view
              echo "
                <script language='javascript'>
                  open_task_view(3, ".$miTaskId.");
				  soPopUpManager.getPopUpById('popup_simple_task_view').setCloseEvent(function (){document.location.href = 'tab_main.php';});
                </script>
                ";
            }
            exit;
          }
        }

        /*
         * Se o usu�rio estiver sendo representado por algum outro usu�rio,
         * deve abrir uma popup perguntando se ele gostaria de continuar com
         * o procurador ou ent�o acabar com o v�nculo de procurador.
         */
        if ($moUser->hasSolicitor()) {
          ?>
            <script language='javascript'>
              isms_open_popup('popup_solicitor_conflict','popup_solicitor_conflict.php','','true',127,445);
            </script>
          <?
        }
        else {
          /*
           * Se for representante de algum usu�rio, deve abrir uma popup
           * para que o usu�rio escolha com qual usu�rio deseja logar no
           * sistema.
           */
          $maUsers = $moUser->getRepresentedUsersList();
          if (count($maUsers)) {
            ?>
              <script language='javascript'>
                isms_open_popup('popup_choose_role','popup_choose_role.php','','true',172,450);
              </script>
            <?
          }
          else {
            if(ISMSLib::getGestorPermission()) {
               echo "<script language='javascript'>document.location.href = 'tab_main.php';</script>";
            }
            elseif(ISMSLib::getAdminPermission()) {
               echo "<script language='javascript'>document.location.href = './packages/admin/tab_main.php';</script>";
            }
            else {
               echo "<script language='javascript'>document.location.href = 'login.php';</script>";
            }
          }
        }
      }
      else {
        if ($miTaskId) {
          if (!$moSession->attributeExists("task"))
            $moSession->addAttribute("task");
          $moSession->setAttrTask($miTaskId);
        }
        echo "<script language='javascript'>document.location.href = 'login.php';</script>";
      }
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("default.xml");
?>