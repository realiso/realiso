<?php
include_once "include.php";
include_once $handlers_ref . "QuerySummaryResidualRisk.php";
include_once $handlers_ref . "QueryMyElements.php";
include_once $handlers_ref . "grid/QueryGridPendantTasks.php";

include_once $handlers_ref . "QueryCountAllDocuments.php";
include_once $handlers_ref . "QueryCountDocumentsByType.php";
include_once $handlers_ref . "QueryCountDocumentsWithFiles.php";
include_once $handlers_ref . "QueryCountDocumentsWithLinks.php";
include_once $handlers_ref . "QueryCountDocumentsWithoutReads.php";
include_once $handlers_ref . "QueryTop10DocumentsRead.php";
include_once $handlers_ref . "QueryCountDocumentsByState.php";
include_once $handlers_ref . "QueryCountDocsRevisionPeriod.php";
include_once $handlers_ref . "QueryCountDocsTimeToApprove.php";

class GridResidualRiskSummary extends FWDDrawGrid {
  public function drawItem(){
    switch($this->ciColumnIndex){
      case 1:
        switch($this->ciRowIndex) {
          case 1  : $this->coCellBox->setIconSrc("icon-area_gray.gif"); break;
          case 2  : $this->coCellBox->setIconSrc("icon-process_gray.gif"); break;
          case 3  : $this->coCellBox->setIconSrc("icon-asset_gray.gif"); break;
          case 4  : $this->coCellBox->setIconSrc("icon-risk_gray.gif"); break;
          default : $this->coCellBox->setIconSrc(""); break;
        }
        return parent::drawItem();
        break;
      
      default:
        return parent::drawItem();
        break;
    }
  }
}

class GridMyElements extends FWDDrawGrid {
  public function drawItem(){
    switch($this->ciColumnIndex){
      case 1:
        $this->coCellBox->setIconSrc("icon-wrong.gif");
        return parent::drawItem();
        break;
      
      default:
        return parent::drawItem();
        break;
    }
  }
}

class GridPendantTasks extends FWDDrawGrid {
  public function drawItem(){
    switch($this->ciColumnIndex){
      case 1:
        $this->coCellBox->setIconSrc("icon-wrong.gif");
        return parent::drawItem();
      break;
        
      case 2:
      	$this->coCellBox->setValue(ISMSActivity::getDescription($this->coCellBox->getValue()));
      	return parent::drawItem();
      break;
      
      default:
        return parent::drawItem();
      break;
    }
  }
}
class GridDocumentSumary extends FWDDrawGrid {
  public function drawItem(){
    switch($this->ciRowIndex) {
      case 1: 
      case 7:
        $this->csDivUnitClass = 'GridLineGray'; 
      break;
    }
    return parent::drawItem();
  }
}

class GridDiferentClassRowOne extends FWDDrawGrid {
  public function drawItem() {
    switch($this->ciRowIndex) {
      case 1 : $this->csDivUnitClass = 'GridLineGray'; break;
      default: break;
    }
    return parent::drawItem();
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent("event_screen"));
  }
}

function getPermissions(){
  $maDeniedACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();    
  $maListPermission = array();
  $maListPermission['list_area'] = false;
  $maListPermission['list_process'] = false;
  $maListPermission['list_asset'] = false;
  $maListPermission['list_risk'] = false;
  $maListPermission['list_control'] = false;
  if(!in_array('M.RM.1.1', $maDeniedACLs)) $maListPermission['list_area'] = true;
  if(!in_array('M.RM.2.1', $maDeniedACLs)) $maListPermission['list_process'] = true;
  if(!in_array('M.RM.3.1', $maDeniedACLs)) $maListPermission['list_asset'] = true;
  if(!in_array('M.RM.4.1', $maDeniedACLs)) $maListPermission['list_risk'] = true;
  if(!in_array('M.RM.5.1', $maDeniedACLs)) $maListPermission['list_control'] = true;
  
  return $maListPermission;
}
function getArrayGrids(){  
  $moConfig = new ISMSConfig();
  $mbHasRMModule = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(RISK_MANAGEMENT_MODE);
  $mbHasPMModule = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(POLICY_MODE);
  
  $maGrids = array();
  if ($mbHasRMModule) {
    $maGrids['grid_my_residual_risk'  ] = array('module'=>'rm', 'height'=>FWDWebLib::getObject('grid_my_residual_risk')->getObjFWDBox()->getAttrHeight());
    $maGrids['grid_my_pendencies'     ] = array('module'=>'rm', 'height'=>FWDWebLib::getObject('grid_my_pendencies')->getObjFWDBox()->getAttrHeight());
    $maGrids['grid_my_elements'       ] = array('module'=>'rm', 'height'=>FWDWebLib::getObject('grid_my_elements')->getObjFWDBox()->getAttrHeight());
  } else {
    FWDWebLib::getObject('grid_my_residual_risk')->setShouldDraw(false);
    FWDWebLib::getObject('grid_my_pendencies')->setShouldDraw(false);
    FWDWebLib::getObject('grid_my_elements')->setShouldDraw(false);
  }
  if($mbHasPMModule){
    $maGrids['grid_doc_context_with_documents'  ] = array('module'=>'pm', 'height'=>FWDWebLib::getObject('grid_doc_context_with_documents')->getObjFWDBox()->getAttrHeight());
    $maGrids['grid_doc_sumary'                  ] = array('module'=>'pm', 'height'=>FWDWebLib::getObject('grid_doc_sumary')->getObjFWDBox()->getAttrHeight());
    $maGrids['grid_doc_approver_time'           ] = array('module'=>'pm', 'height'=>FWDWebLib::getObject('grid_doc_approver_time')->getObjFWDBox()->getAttrHeight());
    $maGrids['grid_doc_top10'                   ] = array('module'=>'pm', 'height'=>FWDWebLib::getObject('grid_doc_top10')->getObjFWDBox()->getAttrHeight());
    $maGrids['grid_doc_period_revision'         ] = array('module'=>'pm', 'height'=>FWDWebLib::getObject('grid_doc_period_revision')->getObjFWDBox()->getAttrHeight());
  }else{
    FWDWebLib::getObject('grid_doc_context_with_documents')->setShouldDraw(false);
    FWDWebLib::getObject('grid_doc_sumary')->setShouldDraw(false);
    FWDWebLib::getObject('grid_doc_approver_time')->setShouldDraw(false);
    FWDWebLib::getObject('grid_doc_top10')->setShouldDraw(false);
    FWDWebLib::getObject('grid_doc_period_revision')->setShouldDraw(false);
  }
  return $maGrids;
}
function populate_grid_my_residual_risk(){
  /*** POPULA GRID 'MEU SUM�RIO DE RISCO' ***/
  $moGridMyResidualRisk = FWDWebLib::getObject("grid_my_residual_risk");
  $moGridMyResidualRisk->setObjFWDDrawGrid(new GridResidualRiskSummary());
  

  $moGridMyResidualRisk->setItem(2,1,FWDLanguage::getPHPStringValue('gs_area','�rea'));
  $moGridMyResidualRisk->setItem(2,2,FWDLanguage::getPHPStringValue('gs_process','Processo'));
  $moGridMyResidualRisk->setItem(2,3,FWDLanguage::getPHPStringValue('gs_asset','Ativo'));
  $moGridMyResidualRisk->setItem(2,4,FWDLanguage::getPHPStringValue('gs_risk','Risco'));    
  
  $moQuery = new QuerySummaryResidualRisk(FWDWebLib::getConnection());
  $moQuery->setUserId(FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId());
  $moQuery->setUserPermissions(getPermissions());
  $moQuery->makeQuery();
  $moQuery->executeQuery();
  foreach ($moQuery->getResidualRiskSummary() as $msName => $miValue) {
    $miColumn =
      strstr($msName,"high") ? 3 :
      (strstr($msName,"mid") ? 4 :
        (strstr($msName,"low") ? 5 : 
          (strstr($msName,"np") ? 6 :
            (strstr($msName,"total") ? 7 : -1))));
    $miLine =
      strstr($msName,"area") ? 1 :
      (strstr($msName,"process") ? 2 :
       (strstr($msName,"asset") ? 3 :
        (strstr($msName,"risk") ? 4 : -1)));
    
    if ($miColumn<0 || $miLine<0)
      trigger_error("Invalid Residual Risk Summary Query Name: $msName",E_USER_ERROR);
    
    if ($miColumn>=3 && $miColumn <=7 && $miValue==0) $miValue = FWDLanguage::getPHPStringValue('gs_na','NA');
    if ($miColumn==7) $miValue = "<b>".$miValue."</b>";
    
    $moGridMyResidualRisk->setItem($miColumn,$miLine,$miValue);
  }
}

function populate_grid_my_elements(){
  /*** POPULA GRID 'MEUS ELEMENTOS' ***/
  $moGridMyElements = FWDWebLib::getObject("grid_my_elements");
  $moGridMyElements->setObjFWDDrawGrid(new GridMyElements());
  
  $moGridMyElements->setItem(2,1,FWDLanguage::getPHPStringValue('gs_areas_without_processes','�reas sem processos'));
  $moGridMyElements->setItem(2,2,FWDLanguage::getPHPStringValue('gs_processes_without_assets','Processos sem ativos'));
  $moGridMyElements->setItem(2,3,FWDLanguage::getPHPStringValue('gs_assets_without_risk_events','Ativos sem eventos de risco'));
  $moGridMyElements->setItem(2,4,FWDLanguage::getPHPStringValue('gs_controls_without_risk_events_association','Controles sem associa��o com eventos de risco'));
  
  $moQuery = new QueryMyElements(FWDWebLib::getConnection());
  $moQuery->setUserPermissions(getPermissions());
  $moQuery->makeQuery();
  $moQuery->executeQuery();
  $maMyElements = $moQuery->getMyElements();
  
  $msNA = FWDLanguage::getPHPStringValue('gs_na','NA');
  $moGridMyElements->setItem(3,1,$maMyElements["areas_without_processes"]?$maMyElements["areas_without_processes"]:$msNA);
  $moGridMyElements->setItem(3,2,$maMyElements["processes_without_assets"]?$maMyElements["processes_without_assets"]:$msNA);
  $moGridMyElements->setItem(3,3,$maMyElements["assets_without_risks"]?$maMyElements["assets_without_risks"]:$msNA);
  $moGridMyElements->setItem(3,4,$maMyElements["controls_without_risks"]?$maMyElements["controls_without_risks"]:$msNA);
}

function populate_grid_my_pendencies(){
  /*** POPULA GRID 'MINHAS PEND�NCIAS' ***/
  $moGridMyPendencies = FWDWebLib::getObject("grid_my_pendencies");
  $moGridMyPendencies->setObjFWDDrawGrid(new GridPendantTasks());
  $moGridMyPendencies->setQueryHandler(new QueryGridPendantTasks(FWDWebLib::getConnection()));
}

function populate_grid_doc_sumary(){
  $moGridDocSumary = FWDWebLib::getObject('grid_doc_sumary');
  $moGridDocSumary->setObjFWDDrawGrid(new GridDocumentSumary());

  $moHandlerDocByState = new QueryCountDocumentsByState(FWDWebLib::getConnection());
  $moHandlerAllDocs = new QueryCountAllDocuments(FWDWebLib::getConnection());
  $moHandlerDocsWithFiles = new QueryCountDocumentsWithFiles(FWDWebLib::getConnection());
  $moHandlerDocsWithLinks = new QueryCountDocumentsWithLinks(FWDWebLib::getConnection());
  $moHandlerDocsWithoutReads = new QueryCountDocumentsWithoutReads(FWDWebLib::getConnection());
  
  $moHandlerDocByState->setUserId(FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId());
  $moHandlerAllDocs->setUserId(FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId());
  $moHandlerDocsWithFiles->setUserId(FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId());
  $moHandlerDocsWithLinks->setUserId(FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId());
  $moHandlerDocsWithoutReads->setUserId(FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId());


  $miAllDocs = 0;
  $maDocsByState = $moHandlerDocByState->getValues();
  foreach ($maDocsByState as $miCount){
    $miAllDocs += $miCount;
  }
  $miDocsWithFiles = $moHandlerDocsWithFiles->getValues();
  $miDocsWithLinks = $moHandlerDocsWithLinks->getValues();
  $maDocsWithoutReads = $moHandlerDocsWithoutReads->getValues(); 
  $miDocsWithoutReads = $maDocsWithoutReads['doc_not_read'];

  if((!$miDocsWithFiles)||($miDocsWithFiles < 0))
    $miDocsWithFiles=0;
  if((!$miDocsWithLinks)||($miDocsWithLinks < 0))
    $miDocsWithLinks=0;
  if((!$miDocsWithoutReads)||($miDocsWithoutReads < 0))
   $miDocsWithoutReads=0;
  
  /*Popula a grid dos estados de um documento tt , si*/
  $miLine = 1;
  $moGridDocSumary->setItem(1,$miLine,FWDLanguage::getPHPStringValue('gc_documents_status_bl','<b>Status dos Documentos</b>'));
  $moGridDocSumary->setItem(2,$miLine,FWDLanguage::getPHPStringValue('gc_qty_bl','<b>Qtde.</b>'));   
  $moGridDocSumary->setItem(3,$miLine,FWDLanguage::getPHPStringValue('gc_percentage_bl','<b>%</b>'));
  $miLine++;
  foreach($maDocsByState as $miKey=>$msValue){
    $moContextObj = new ISMSContextObject();
    $moGridDocSumary->setItem(1,$miLine,$moContextObj->getContextStateAsString($miKey));
    $moGridDocSumary->setItem(2,$miLine,$msValue);
    if(!$msValue)
      $msValue = 0;
    if($miAllDocs)
      $moGridDocSumary->setItem(3,$miLine,number_format(($msValue/$miAllDocs)*100)."%");
    else
      $moGridDocSumary->setItem(3,$miLine,"0%");
    $miLine++;
  }
  
  $moGridDocSumary->setItem(1,$miLine,FWDLanguage::getPHPStringValue('gs_documents_total_bl','<b>Total de Documentos</b>'));
  $moGridDocSumary->setItem(2,$miLine,"<b>".$miAllDocs."</b>");
  $moGridDocSumary->setItem(3,$miLine,"<b>100%</b>");
  $miLine++;
  
  $moGridDocSumary->setItem(1,$miLine,FWDLanguage::getPHPStringValue('gs_published_documents_information_bl','<b>Informa��es sobre Documentos Publicados</b>'));
  $moGridDocSumary->setItem(2,$miLine,FWDLanguage::getPHPStringValue('gc_qty_bl','<b>Qtde.</b>'));   
  $moGridDocSumary->setItem(3,$miLine,FWDLanguage::getPHPStringValue('gc_percentage_bl','<b>%</b>'));
  $miLine++;
  $moContextObj = new ISMSContextObject();
 
  $moGridDocSumary->setItem(1,$miLine,FWDLanguage::getPHPStringValue('gs_documents_with_files','Documentos com Arquivos'));
  $moGridDocSumary->setItem(2,$miLine,$miDocsWithFiles);
  if($maDocsByState[CONTEXT_STATE_DOC_APPROVED])
    $moGridDocSumary->setItem(3,$miLine,number_format(($miDocsWithFiles/$maDocsByState[CONTEXT_STATE_DOC_APPROVED])*100)."%");
  else 
    $moGridDocSumary->setItem(3,$miLine,"0%");
  $miLine++;
  
  $moGridDocSumary->setItem(1,$miLine,FWDLanguage::getPHPStringValue('gs_documents_with_links','Documentos com Links'));
  $moGridDocSumary->setItem(2,$miLine,$miDocsWithLinks);
  if($maDocsByState[CONTEXT_STATE_DOC_APPROVED])
    $moGridDocSumary->setItem(3,$miLine,number_format(($miDocsWithLinks/$maDocsByState[CONTEXT_STATE_DOC_APPROVED])*100)."%");
  else 
    $moGridDocSumary->setItem(3,$miLine,"0%");
  $miLine++;
  
  $moGridDocSumary->setItem(1,$miLine,FWDLanguage::getPHPStringValue('gs_documents_to_be_read','Documentos com leitura pendente'));
  $moGridDocSumary->setItem(2,$miLine,$miDocsWithoutReads);
  if($maDocsByState[CONTEXT_STATE_DOC_APPROVED])
     $moGridDocSumary->setItem(3,$miLine,number_format(($miDocsWithoutReads/$maDocsByState[CONTEXT_STATE_DOC_APPROVED])*100)."%");
  else
    $moGridDocSumary->setItem(3,$miLine,"0%");

}

function populate_grid_doc_context_with_documents(){
  $moGridDocCtxDocs = FWDWebLib::getObject('grid_doc_context_with_documents');
  $moGridDocCtxDocs->setObjFWDDrawGrid(new GridDiferentClassRowOne());
  $moHandlerDocsByType = new QueryCountDocumentsByType(FWDWebLib::getConnection());
  $moHandlerDocsByType->setUserId(ISMSLib::getCurrentUserId());
  $maDocsByType = $moHandlerDocsByType->getValues();
  $moGridDocCtxDocs->setItem(2,1,FWDLanguage::getPHPStringValue('gc_documents_qty_bl','<b>Qtde. Documentos</b>'));
  $moGridDocCtxDocs->setItem(3,1,FWDLanguage::getPHPStringValue('gc_percentage_bl','<b>%</b>'));
    
  $miLine = 2;
  foreach($maDocsByType as $miKey=>$maValues){
    $moContextObj = new ISMSContextObject();
    $moCtx = $moContextObj->getContextObject($miKey);   
    $moGridDocCtxDocs->setItem(1,$miLine,$moCtx->getLabel());
    $moGridDocCtxDocs->setItem(2,$miLine,$maValues['doc']?$maValues['doc']:0);
    
    $mfPercentual = 0;
    if($maValues['all']){
      if($maValues['doc']>$maValues['all'])
        $mfPercentual = 100;
      else
        if($maValues['all'])
          $mfPercentual = ($maValues['doc'] / $maValues['all'])*100;
        else
          $mfPercentual = 100;
    } 
    $moGridDocCtxDocs->setItem(3,$miLine,number_format($mfPercentual,0)."%");
    
    $miLine++;
  }
}
function populate_grid_doc_top10(){
  $moGridDocTop10 = FWDWebLib::getObject('grid_doc_top10');
  $moGridDocTop10->setObjFWDDrawGrid(new GridDiferentClassRowOne());
  $moHandlerDocTop10 = new QueryTop10DocumentsRead(FWDWebLib::getConnection());
  $moHandlerDocTop10->setUserId(FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId());
  $maDocTop10 = $moHandlerDocTop10->getValues();

  if(count($maDocTop10)){
   $moGridDocTop10->setItem(2,1,FWDLanguage::getPHPStringValue('gc_readings_bl','<b>Leituras</b>'));
  }//else{} // nenhum documento foi lido no sistema, assim n�o deve-se popular a grid
  $miLine = 2;
  foreach($maDocTop10 as $miKey=>$maValues){
    $moGridDocTop10->setItem(1,$miLine,$maValues['name']);
    $moGridDocTop10->setItem(2,$miLine,$maValues['count']);
    $miLine++;
  }

}
function populate_grid_doc_period_revision(){
  $moGridDocPeriodRevision = FWDWebLib::getObject('grid_doc_period_revision');
  $moGridDocPeriodRevision->setObjFWDDrawGrid(new GridDiferentClassRowOne());
  $moHandlerDocRevisionPeriod = new QueryCountDocsRevisionPeriod(FWDWebLib::getConnection());
  $moHandlerDocRevisionPeriod->setUserId(FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId());
  $maRevPeriod = $moHandlerDocRevisionPeriod->getValues();

 /*Popula a grid de periodo de revis�o dos documentos*/
  $moGridDocPeriodRevision->setItem(1,1,FWDLanguage::getPHPStringValue('gc_time_period_smaller_or_equal_bl','<b>Per�odo de Tempo (menor ou igual a)</b>'));
  $moGridDocPeriodRevision->setItem(2,1,FWDLanguage::getPHPStringValue('gc_qty_bl','<b>Qtde.</b>'));
  $moGridDocPeriodRevision->setItem(1,2,FWDLanguage::getPHPStringValue('gs_one_week','Uma Semana'));
  $moGridDocPeriodRevision->setItem(2,2,$maRevPeriod[WEEK_INTERVAL]);
  $moGridDocPeriodRevision->setItem(1,3,FWDLanguage::getPHPStringValue('gs_one_month','Um M�s'));
  $moGridDocPeriodRevision->setItem(2,3,$maRevPeriod[MONTH_INTERVAL]);
  $moGridDocPeriodRevision->setItem(1,4,FWDLanguage::getPHPStringValue('gs_three_months','Tr�s Meses'));
  $moGridDocPeriodRevision->setItem(2,4,$maRevPeriod[TREE_MONTH_INTERVAL]);
  $moGridDocPeriodRevision->setItem(1,5,FWDLanguage::getPHPStringValue('gs_six_months','Seis Meses'));
  $moGridDocPeriodRevision->setItem(2,5,$maRevPeriod[SIX_MONTH_INTERVAL]);
  $moGridDocPeriodRevision->setItem(1,6,FWDLanguage::getPHPStringValue('gs_more_than_six_months','Maior que Seis Meses'));
  $moGridDocPeriodRevision->setItem(2,6,$maRevPeriod[YEAR_INTERVAL]);
}
function populate_grid_doc_approver_time(){
  $moGridDocDocApproverTime = FWDWebLib::getObject('grid_doc_approver_time');
  $moHandlerQueryCountDocsTimeToAprove = new QueryCountDocsTimeToApprove(FWDWebLib::getConnection());
  $moHandlerQueryCountDocsTimeToAprove->setUserId(FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId());
  $msTimeToApprove = $moHandlerQueryCountDocsTimeToAprove->getValues();

  $moGridDocDocApproverTime->setItem(1,1,FWDLanguage::getPHPStringValue('gs_average_time','Tempo M�dio'));
  $moGridDocDocApproverTime->setItem(2,1,$msTimeToApprove);
}

function getModules(){
  return array('RM','PM');
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();

    $moPreference = new ISMSUserPreferences();
    if($moPreference->preferenceUserExistes("dashboard_sumary_pref_basic")){
      $maPrefUser = FWDWebLib::unserializeString($moPreference->getPreference("dashboard_sumary_pref_basic"));
      $maPrefUserKeys = array();
      if(is_array($maPrefUser)){
        $maPrefUserKeys = array_flip($maPrefUser);
      } 
    }else{
      $maPrefUserKeys = getArrayGrids();
      $maValuesKeys ="";
      foreach($maPrefUserKeys as $miKey=>$maValue){
        $maValuesKeys[] = $miKey;
      }
      $moPreference->setPreference("dashboard_sumary_pref_basic",serialize($maValuesKeys));
    }
    $maGrids = getArrayGrids();
    $mbColumnOneRM = true;
    $mbColumnOnePM = true;
    $miTopCommon = 30;
    
    $miTopOneRM = $miTopCommon;
    $miTopTwoRM = $miTopCommon;
    $miTopOnePM = $miTopCommon;
    $miTopTwoPM = $miTopCommon;

    $miTopFirstPanel = 15;
    $miLeftOne = 10;
    $miLeftTwo = 485;
    $miSpaceBetweenElements = 30;
    
    if(count($maPrefUserKeys)){
      foreach($maGrids as $miKey=>$maValues){
        FWDWebLib::getObject($miKey)->setShouldDraw(false);
      }
      foreach ($maPrefUserKeys as $miKey=>$maValues){
        $moGrid = FWDWebLib::getObject($miKey);
        if(isset($maGrids[$miKey])){
          $msModule = strtoupper($maGrids[$miKey]['module']);
          if(${"mbColumnOne".$msModule}){
            $moGrid->getObjFWDBox()->setAttrLeft($miLeftOne);
            $moGrid->getObjFWDBox()->setAttrTop(${"miTopOne".$msModule});
            ${"miTopOne".$msModule} = ${"miTopOne".$msModule} + $maGrids[$miKey]['height'] + $miSpaceBetweenElements;
          }else{
            $moGrid->getObjFWDBox()->setAttrLeft($miLeftTwo);
            $moGrid->getObjFWDBox()->setAttrTop(${"miTopTwo".$msModule});
            ${"miTopTwo".$msModule} = ${"miTopTwo".$msModule} + $maGrids[$miKey]['height'] + $miSpaceBetweenElements;
          }
          ${"mbColumnOne".$msModule} = !${"mbColumnOne".$msModule}; 
          
           /*executa a fun��o que popula a grid correspondente, fun��o no formato "populate_" 
           * mais o nome da grid definido no xml */
          $msFuctionName = "populate_".$miKey;
          $msFuctionName();
          
          $moGrid->setShouldDraw(true);
        }
      }
    }else{
      foreach ($maPrefUserKeys as $miKey=>$maValues){
        $moGrid = FWDWebLib::getObject($miKey);
        $msModule = strtoupper($maGrids[$miKey]['module']);
        if(${"mbColumnOne".$msModule}){
          $moGrid->getObjFWDBox()->setAttrLeft($miLeftOne);
          $moGrid->getObjFWDBox()->setAttrTop(${"miTopOne".$msModule});
          ${"miTopOne".$msModule} = ${"miTopOne".$msModule} + $maGrids[$miKey]['height'] + $miSpaceBetweenElements;
        }else{
          $moGrid->getObjFWDBox()->setAttrLeft($miLeftTwo);
          $moGrid->getObjFWDBox()->setAttrTop(${"miTopTwo".$msModule});
          ${"miTopTwo".$msModule} = ${"miTopTwo".$msModule} + $maGrids[$miKey]['height'] + $miSpaceBetweenElements;
        }
        ${"mbColumnOne".$msModule} = !${"mbColumnOne".$msModule}; 
          
        /*executa a fun��o que popula a grid correspondente, fun��o no formato "populate_" 
        * mais o nome da grid definido no xml */
        $msFuctionName = "populate_".$miKey;
        $msFuctionName();
        $moGrid->setShouldDraw(true);
      }
    }

    /*atualiza a altura dos panels dos m�dulos*/
    $miValuePast =  $miTopFirstPanel ;
    foreach (getModules() as $strModule){
      $miValue = max(${"miTopOne".$strModule},${"miTopTwo".$strModule});
      FWDWebLib::getObject(strtolower($strModule).'_sumary')->getObjFWDBox()->setAttrTop($miValuePast);
      if($miValue > $miTopCommon){
        FWDWebLib::getObject(strtolower($strModule).'_sumary')->getObjFWDBox()->setAttrHeight( $miValue + $miSpaceBetweenElements);
        $miValuePast = $miValuePast + $miValue + 2*$miSpaceBetweenElements;
      }else{
        FWDWebLib::getObject(strtolower($strModule).'_sumary')->setShouldDraw(false);
      }
    }

    $miTopOne = $miTopOneRM + $miTopOnePM;
    $miTopTwo = $miTopTwoRM + $miTopTwoPM;
    FWDWebLib::getObject('dialog')->getObjFWDBox()->setAttrHeight(max($miTopOne,$miTopTwo,430));
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));

  ?>
    <script language="javascript">
      function refresh(){
       showLoading();
       document.forms[0].submit();
      } 
    </script>
    <?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_summary_basic.xml");
?>
