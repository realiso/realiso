<?php
$gaStrings = array(


/* './abstracts/FWDGridAbstract.php' */

'fwdgrid_pref_column_label'=>'Columna:',
'fwdgrid_pref_column_name'=>'Columna',
'fwdgrid_pref_column_order'=>'Orden:',
'fwdgrid_pref_column_order_title'=>'Orden de Columnas de Grilla:',
'fwdgrid_pref_column_properties'=>'Propiedades de la columna',
'fwdgrid_pref_column_show_title'=>'Columnas Mostradas y Orden de Visualización entre ellas',
'fwdgrid_pref_column_width'=>'Ancho*:',
'fwdgrid_pref_columns_grid'=>'Columnas de Grilla:',
'fwdgrid_pref_columns_show'=>'Columnas Mostradas:',
'fwdgrid_pref_false_value'=>'No',
'fwdgrid_pref_grid_columns_title'=>'Columnas de Grilla:',
'fwdgrid_pref_grid_properties'=>'Edición de las propiedades de la Grilla:',
'fwdgrid_pref_rows_height'=>'Altura de Filas:',
'fwdgrid_pref_rows_per_page'=>'Filas por página:',
'fwdgrid_pref_select_source'=>'Seleccionar Origen',
'fwdgrid_pref_select_target'=>'Seleccionar Destino',
'fwdgrid_pref_target'=>'Destino:',
'fwdgrid_pref_tooltip_label'=>'Mensaje Textual:',
'fwdgrid_pref_true_value'=>'Sí',
'fwdgrid_pref_width_obs'=>'* \'0\' = ancho automático',

/* './base/FWDDefaultErrorHandler.php' */

'error_config_corrupt'=>'El archivo de configuración está dañado.',
'error_configuration'=>'El archivo de configuración está dañado.',
'error_db_connection'=>'Error de conexión con la Base de Datos.',
'error_db_invalid_object_name'=>'Nombre de objeto de base de datos no válido. Probablemente usando un usuario de DB no válido.',
'error_debug_permission_denied'=>'Permiso denegado en archivo de depuración.',
'error_session_permission_denied'=>'Permiso denegado en archivo de sesión.',
'error_smtp'=>'Falló al conectarse al servidor de e-mail.',

/* './base/FWDLanguage.php' */

'default_language'=>'Portugués',

/* './base/FWDLicense.php' */

'error_corrupt_hash'=>'El resumen (hash) está dañado.',
'error_corrupt_license'=>'La licencia está dañada.',
'error_permission_denied'=>'Permiso denegado en archivo de activación.',

/* './base/FWDSyslog.php' */

'error_connection_syslog'=>'No se puede conectar a servidor de registro (syslog).',

/* './event/classEvent/FWDGridUserEdit.php' */

'fwdGridUserEditErrorWidthGrid'=>'Fueron encontradas inconsistencias: La suma de las columnas de la grilla debe ser menor o igual que:',
'fwdGridUserEditErrorWidthGridExplain'=>'de todos modos el valor obtenido es:',

/* './formats/FWDPDF.php' */

'fpdf_page'=>'Página',

/* './report/FWDReportClassificator.php' */

'report_confidencial'=>'Informe Confidencial',
'report_institutional'=>'Informe Institucional',
'report_public'=>'Informe Público',

/* './report/FWDReportGenerator.php' */

'report_confidential_report'=>'Informe Confidencial',
'report_excel'=>'Excel',
'report_html'=>'HTML',
'report_html_download'=>'Descarga HTML',
'report_institutional_report'=>'Informe Institucional',
'report_pdf'=>'PDF',
'report_public_report'=>'Informe Público',
'report_word'=>'Word',

/* './view/FWDCalendar.php' */

'locale'=>'en_us',

/* './view/FWDGrid.php' */

'fwdgrid_info_no_result_rows'=>'No se encontraron resultados',
'fwdgrid_number_page_of'=>'de',
'fwdgrid_page_label'=>'página',

/* './view/FWDTreeAjax.php' */

'FWDLoading'=>'Cargando',


/* './about_isms.xml' */

'st_about_axur_full'=>'Realiso Corp. Es una compañía lider en sistemas de gestión a demanda. Tenemos cientos de clientes presentes en varios sectores del mercado, incluidos los financieros, telecomunicaciones, industria, gobierno, reventa, energía, minería, punto-com, los servicios,  petróleo y el gas. Usamos las mejores prácticas utilizadas mundialmente y le proporcionamos soluciones de alta tecnología para reducir el riesgo organizacional, medir y demostrar la eficiencia de los controles con respecto a la información de la protección de los activos de la organización.',
'st_about_axur_title'=>'Acerca de Realiso Corp.',
'st_about_isms_full'=>'Real ISMS - Sistema de Gestión de la Seguridad de la información es una suite para la gestión de un Sistema de Gestión de Seguridad de la Información acorde a la norma ISO 27001. Real ISMS ayuda a las empresas de cualquier tamaño, donde varios miembros están vinculados con activos bajo custodia, identificación de nuevos riesgos de seguridad y mantenimiento de la seguridad.',
'st_about_isms_title'=>'Acerca de Real ISMS',
'st_axur_information_security'=>'Realiso Corp.',
'st_axur_link'=>'www.realiso.com',
'st_build'=>'Build',
'st_copyright'=>'Copyright',
'st_information_security_management_system'=>'Sistema de Gestión de la Seguridad de la Información',
'st_version'=>'Versión',

/* './access_denied.php' */

'st_license_file_not_found'=>'Archivo de licencia <b>%license_name%</b> no encontrado.',

/* './access_denied.xml' */

'tt_access_denied'=>'Acceso Denegado',

/* './classes/admin/report/ISMSReportAuditAlertFilter.php' */

'rs_alerts_created_after'=>'Alertas creadas después',
'rs_alerts_created_before'=>'Alertas creadas antes',

/* './classes/admin/report/ISMSReportAuditLogFilter.php' */

'rs_action_cl'=>'Acción:',
'rs_log_after'=>'Registrar después',
'rs_log_before'=>'Registrar antes',

/* './classes/admin/report/ISMSReportAuditTaskFilter.php' */

'rs_activity_cl'=>'Actividad:',
'rs_and_before'=>'y antes',
'rs_any_date'=>'Cualquier fecha',
'rs_creation_date_cl'=>'Fecha de creación:',
'rs_creator_cl'=>'Creador:',
'rs_receiver_cl'=>'Receptor:',
'rs_tasks_created_after'=>'Tarea creada después',
'rs_tasks_created_before'=>'Tarea creada antes',
'st_all_users'=>'Todos los usuarios',

/* './classes/continual_improvement/CIActionPlan.php' */

'mx_action_plan'=>'Plan de Acción',
'mx_ap_remove_error_message'=>'Imposible eliminar el Plan de Acción. Hay al menos una No conformidad asociada a él.',
'mx_ap_remove_error_title'=>'Error al eliminar el Plan de Acción',

/* './classes/continual_improvement/CICategory.php' */

'mx_root'=>'Raíz',

/* './classes/continual_improvement/CIIncident.php' */

'mx_incident'=>'Incidente',

/* './classes/continual_improvement/CIIncidentControl.php' */

'mx_incident_control_association'=>'Asociación de Incidente a Control',

/* './classes/continual_improvement/CIIncidentProcess.php' */

'mx_incident_process_association'=>'Asociación de Incidente a Proceso',

/* './classes/continual_improvement/CINonConformity.php' */

'mx_non_conformity'=>'No conformidad',

/* './classes/continual_improvement/CINonConformitySeed.php' */

'mx_inefficient'=>'Ineficiente',
'mx_late_efficiency_revision'=>'Revisión de Eficacia Demorada',
'mx_late_implementation'=>'Implementación Tardía',
'mx_late_test'=>'Prueba Demorada',
'mx_test_failure'=>'Prueba de Fallos 93',

/* './classes/continual_improvement/CIOccurrence.php' */

'mx_occurrence'=>'Ocurrencia',
'mx_occurrence_remove_error'=>'Error al eliminar Ocurrencia',
'mx_occurrence_remove_error_message'=>'Imposible eliminar esta Ocurrencia. Hay al menos un Incidente asociado a ella.',
'st_incident_module_name'=>'Mejora Contínua',

/* './classes/continual_improvement/CISolution.php' */

'mx_solution'=>'Solución',

/* './classes/continual_improvement/report/ISMSReportActionPlanFilter.php' */

'lb_action_type'=>'Tipo de acción:',
'mx_both'=>'Ambos',

/* './classes/continual_improvement/report/ISMSReportDisciplinaryProcessFilter.php' */

'rs_user_cl'=>'Usuario:',

/* './classes/continual_improvement/report/ISMSReportElementsFilter.php' */

'lb_elements_cl'=>'Items:',

/* './classes/continual_improvement/report/ISMSReportIncidentFilter.php' */

'lb_incident_category_cl'=>'Categoría:',
'lb_loss_type_cl'=>'Tipo de pérdida:',
'lb_solution_date_cl'=>'Fecha de solución:',
'mx_immediate_disposal'=>'Eliminación Inmediata',

/* './classes/continual_improvement/report/ISMSReportIncidentsFilter.php' */

'rs_asset_cl'=>'Activo:',

/* './classes/continual_improvement/report/ISMSReportNonConformitiesFilter.php' */

'rs_status_cl'=>'Estado:',

/* './classes/continual_improvement/report/ISMSReportNonConformityFilter.php' */

'lb_classification_cl'=>'Clasificación:',
'lb_conclusion_date_cl'=>'Fecha de finalización:',
'lb_deadline_cl'=>'Fecha límite:',
'lb_potential_cl'=>'Potencial:',
'mx_after_initialdate'=>'Después de %initialDate%',
'mx_before_finaldate'=>'Antes de %finalDate%',
'mx_between_initialdate_finaldate'=>'Entre %initialDate% y %finalDate%',
'mx_external_audit'=>'Auditoría Externa',
'mx_internal_audit'=>'Auditoría Interna',
'mx_no'=>'No',
'mx_security_control'=>'Control de Seguridad',
'mx_yes'=>'Sí',

/* './classes/continual_improvement/report/ISMSReportOccurrenceFilter.php' */

'lb_status_cl'=>'Estado:',
'mx_all'=>'Todos',

/* './classes/ISMSActivity.php' */

'wk_action_plan_approval'=>'Aprobación de Plan de Acción',
'wk_action_plan_finish_confirm'=>'Aprobar la Fecha de Finalización del Plan de Acción',
'wk_action_plan_revision'=>'Action Plan Follow Up 150',
'wk_area_approval'=>'Aprobar Área',
'wk_asset_approval'=>'Aprobar Activo',
'wk_best_practice_approval'=>'Aprobar Mejor Práctica',
'wk_best_practice_section_approval'=>'Aprobar Sección de Mejor Práctica',
'wk_category_approval'=>'Aprobar Categoría',
'wk_control_approval'=>'Aprobar Control',
'wk_control_implementation_approval'=>'Aprobar Implementación de Control',
'wk_control_test_followup'=>'Supervisar de Prueba de Control (129)',
'wk_document_approval'=>'Aprobar Documento',
'wk_document_revision_followup'=>'Supervisar la revisión de documentos 138',
'wk_document_template_approval'=>'Aprobar Plantilla de Documento',
'wk_event_approval'=>'Aprobar Evento',
'wk_incident_approval'=>'Aprobar Incidente',
'wk_incident_control_induction'=>'Inducida por el control de eficacia de medición 153',
'wk_incident_disposal_approval'=>'Aprobación de Eliminación Inmediata de Incidente',
'wk_incident_solution_approval'=>'Aprobación de Solución de Incidente',
'wk_non_conformity_approval'=>'Aprobación de No conformidad',
'wk_non_conformity_data_approval'=>'Aprobación de Datos de No conformidad',
'wk_occurrence_approval'=>'Aprobación de Ocurrencia',
'wk_policy_approval'=>'Aprobar Política',
'wk_process_approval'=>'Aprobar proceso',
'wk_process_asset_association_approval'=>'Aprobar Asociación de Activo con Proceso.',
'wk_real_efficiency_followup'=>'Monitorear Eficacia Real',
'wk_risk_acceptance_approval'=>'Aprobar la Aceptación de Riesgos debajo del valor',
'wk_risk_acceptance_criteria_approval'=>'Aprobar la definición del criterio para Aceptación de Riesgos',
'wk_risk_approval'=>'Aprobar Riesgo',
'wk_risk_control_association_approval'=>'Aprobar Asociación de Control con Riesgo.',
'wk_risk_tolerance_approval'=>'Aprobar Tolerancia a Riesgo',
'wk_scope_approval'=>'Aprobar Alcance',
'wk_standard_approval'=>'Aprobar Estándar',

/* './classes/ISMSAuditLog.php' */

'ad_action_bad_login'=>'ha intentado ingresar con una contraseña no válida',
'ad_action_blocked_login'=>'está bloqueado e intentó ingresar',
'ad_login'=>'Sistema de Ingreso',
'ad_login_error'=>'ha intentado ingresar con un usuario no válido',
'ad_login_failed'=>'ha intentado ingresar sin permiso',
'ad_logout'=>'Sistema de Salida',
'ad_removed_from_system'=>'Eliminado del sistema',
'ad_restored'=>'recuperado',

/* './classes/ISMSContext.php' */

'ad_log_message'=>'%action% %label% <b>%context_name%</b> por %user_name%.',
'ad_on_behalf_of'=>'en nombre de',
'db_default_document_name'=>'%label_context% %context_name% %label_doc%',
'st_denied_permission_to_approve'=>'No tiene permisos para aprobar la',
'st_denied_permission_to_delete'=>'No tiene permisos para eliminar la',
'st_denied_permission_to_execute'=>'No tiene permisos para ejecutar la tarea.',
'st_denied_permission_to_insert'=>'No tiene permisos para insertar una(s)',
'st_denied_permission_to_read'=>'No tiene permisos para ver la',

/* './classes/ISMSContextObject.php' */

'mx_ap_acceptance_pendant'=>'Aceptación de Plan de Acción Pendiente',
'mx_approval'=>'En Aprobación',
'mx_approved'=>'Aprobado',
'mx_closed'=>'Cerrado',
'mx_co_approved'=>'Aprobado por Ambos',
'mx_deleted'=>'Eliminado',
'mx_denied'=>'Denegado',
'mx_directed'=>'Reenviado',
'mx_finished'=>'Finalizado',
'mx_measured'=>'Medido',
'mx_nc_treatment_pendant'=>'Tratamiento de No conformidad Pendiente',
'mx_obsolete'=>'Obsoleto',
'mx_open'=>'Abierto',
'mx_pendant'=>'Pendiente',
'mx_pendant_solution'=>'Pendiente - Solución',
'mx_pendant_solved'=>'Pendiente - Resuelto',
'mx_published'=>'Publicado',
'mx_revision'=>'En Revisión',
'mx_sent'=>'Emitido',
'mx_solved'=>'Resuelto',
'mx_to_be_published'=>'Para ser publicado',
'mx_under_development'=>'En desarrollo',
'mx_waiting_conclusion'=>'Esperando Finalización',
'mx_waiting_deadline'=>'Estimado',
'mx_waiting_measurement'=>'Esperando Medición',
'mx_waiting_solution'=>'Esperando Solución',

/* './classes/ISMSEmailTemplates.php' */

'em_activity'=>'Actividad',
'em_alert_messages_sent'=>'<b>Estimado usuario de Real ISMS,<b> <br> Los siguientes mensajes de alerta fueron enviados a usted.<br>',
'em_asset'=>'Activo',
'em_at'=>'a',
'em_click_here_to_access_the_system'=>'Acceder a Real IMS',
'em_client'=>'Cliente',
'em_control'=>'Control',
'em_control_has_deadline'=>'<b>Estimado usuario de Real ISMS,<b> <br> el control a continuación tiene una fecha límite para su aplicación. 215',
'em_created_on'=>'Creado en',
'em_description'=>'Descripción',
'em_disciplinary_process'=>'<b>Estimado Admnistrador del Proceso Disciplinario, </b><br>Los siguientes usuarios del sistema fueron agregados al proceso disciplinario.',
'em_execute_task'=>'<b>Estimado usuario de Real ISMS,<b> <br>nosotros le solicitamos que ejecute la/s actividad(es) de arriba.<br> 212',
'em_feedback'=>'Retroalimentación',
'em_feedback_type'=>'Tipo de retroalimentación',
'em_incident'=>'Incidente',
'em_incident_risk_parametrization_message'=>'<b>Estimado usuario de Real ISMS,</b><br>el siguiente riesgo fue asociado a un activo bajo su responsabilidad. 216',
'em_isms_feedback_msg'=>'Un usuario escribió una retroalimenteación acerca de Real ISMS.',
'em_isms_new_user_msg'=>'Usted fue agregado a Real ISMS. A continuación puede ver los datos de acceso al sistema:',
'em_limit'=>'Límite',
'em_login'=>'Ingresar',
'em_new_parameters'=>'Nuevos Parámetros',
'em_new_password_generated'=>'<b>Estimado usuario de Real ISMS,<b> <br>una nueva contraseña ha sido creada para tí.<br> Su nueva contraseña es 213',
'em_new_password_request_ignore'=>'Si usted no ha solicitado una nueva contraseña, por favor ignore este e-mail. Sus credenciales previas todavía son válidas, y si usted las usa, esta nueva contraseña será descartada.',
'em_observation'=>'Observación',
'em_original_parameters'=>'Parámetros por Defecto',
'em_password'=>'Contraseña',
'em_risk'=>'Riesgo',
'em_saas_abandoned_system_msg'=>'<b>El Real ISMS del cliente de arriba fue abandonado.</b>',
'em_sender'=>'Remitente',
'em_sent_on'=>'Enviado en',
'em_tasks_and_alerts_transferred'=>'<b>Estimado usuario de Real ISMS,<b> <br> Un usuario del sistema fue eliminado, y sus tareas y alertas han sido transferidad a usted. 214',
'em_tasks_and_alerts_transferred_message'=>'El usuario de arriba ha sido eliminado del sistema, sus tareas y alertas han sido transferidas a su nombre.',
'em_user'=>'Usuario',
'em_users'=>'Usuarios',
'link'=>'Enlace',

/* './classes/ISMSLib.php' */

'em_license_expired'=>'La licencia del Cliente <b>%client_name%</b> expira en <b>%expiracy%</b>',
'em_subject_license_expired'=>'Licencia Expirada',
'mx_asset_manager'=>'Dueño del Activo',
'mx_chairman'=>'Gestión',
'mx_chinese'=>'Chino (Beta)',
'mx_control_manager'=>'Dueño del Control',
'mx_created_by'=>'Creado por:',
'mx_day'=>'día',
'mx_days'=>'días',
'mx_english'=>'Inglés',
'mx_evidence_manager'=>'Administrador de Evidencia',
'mx_french'=>'Français',
'mx_german'=>'Deutsch',
'mx_incident_manager'=>'Administrador de Incidentes',
'mx_library_manager'=>'Dueño de la Librería',
'mx_modified_by'=>'Modificado por:',
'mx_month'=>'mes',
'mx_months'=>'meses',
'mx_non_conformity_manager'=>'Gestión de No conformidades',
'mx_on'=>'en',
'mx_portuguese'=>'Portugués',
'mx_process_manager'=>'Gestión de Proceso Disciplinario',
'mx_shortdate_format'=>'%m/%d/%Y',
'mx_spanish'=>'Español (Beta!)',
'mx_week'=>'semana',
'mx_weeks'=>'semanas',
'st_denied_permission_to_access_incident_module'=>'Usted no tiene permiso para acceder a mejora continua de trabajo. <br><br> Esta licencia no tiene este permiso!',
'st_denied_permission_to_access_policy_module'=>'Usted no tiene permiso para acceder al módulo de Gestión de Políticas. <br><br>usted está usando una licencia que no le permite acceder a este módulo!',

/* './classes/ISMSLicense.php' */

'mx_commercial'=>'Comercial',
'mx_trial'=>'Trial',

/* './classes/ISMSMailer.php' */

'em_to_execute_task_check_activity_list'=>'<b> Para ejecutar esta tarea, acceda al sistema y verifique su lista de actividades. </b><br> Para más información sobre esta tarea, por favor, póngase en contacto con el oficial de seguridad.',

/* './classes/ISMSPasswordPolicy.php' */

'mx_minimum_chars'=>'Longitud mínima',
'mx_minimum_numeric_chars'=>'Número mínimo de caracteres numéricos',
'mx_minimum_special_chars'=>'Número mínimo de caracteres especiales',
'mx_password_policy'=>'<b>Política de Contraseña:</b><br/>',
'mx_upper_and_lower_case_chars'=>'Caracteres en mayúsculas y minúsculas',
'st_no'=>'No',
'st_yes'=>'Sí',

/* './classes/ISMSPolicy.php' */

'mx_policy'=>'Política',

/* './classes/ISMSProfile.php' */

'mx_profile'=>'Perfil',
'st_profile_remove_error'=>'Imposible eliminar perfil <b>%profile_name%</b>: Hay usuarios asociados con este perfil.',
'tt_profile_remove_error'=>'Error cuando se eliminaba el Perfil',

/* './classes/ISMSScope.php' */

'mx_scope'=>'Alcance',

/* './classes/ISMSSession.php' */

'ad_login_message'=>'%label_user% <b>%user_name%</b> %action%.',

/* './classes/ISMSUser.php' */

'mx_user'=>'Usuario',
'st_admim_module_name'=>'Administrar 276',
'st_user_remove_error'=>'Imposible eliminar perfil <b>%profile_name%</b>: Hay usuarios asociados con este perfil.',
'tt_user_remove_error'=>'Error cuando se eliminaba el Usuario',

/* './classes/nonauto/admin/ISMSDefaultConfig.php' */

'db_area_priority_high'=>'3 - Alto',
'db_area_priority_low'=>'1 - Bajo',
'db_area_priority_middle'=>'2 - Medio',
'db_area_type_factory_unit'=>'Fábrica',
'db_area_type_filial'=>'Subsidiaria',
'db_area_type_matrix'=>'Oficina Central',
'db_area_type_regional_office'=>'Oficina Regional',
'db_control_cost_hardware'=>'Hardware',
'db_control_cost_material'=>'Material',
'db_control_cost_others'=>'Educación',
'db_control_cost_pessoal'=>'Personas',
'db_control_cost_software'=>'Software',
'db_control_type_based_on_risk_analize'=>'Basado en el Análisis de Riesgos',
'db_control_type_laws_and_regulamentation'=>'Cuestión relativa al cumplimiento 311',
'db_control_type_others'=>'Otros',
'db_document_type_1'=>'Confidencial',
'db_document_type_2'=>'Restringido',
'db_document_type_3'=>'Institucional',
'db_document_type_4'=>'Público',
'db_impact_1'=>'Muy Bajo',
'db_impact_2'=>'Bajo',
'db_impact_3'=>'Medio',
'db_impact_4'=>'Alto',
'db_impact_5'=>'Muy Alto',
'db_impact3_1'=>'Bajo',
'db_impact3_2'=>'Medio',
'db_impact3_3'=>'Alto',
'db_importance_1'=>'Insignificante 343',
'db_importance_2'=>'Bajo',
'db_importance_3'=>'Medio',
'db_importance_4'=>'Alto',
'db_importance_5'=>'Crítico',
'db_importance3_1'=>'Bajo',
'db_importance3_2'=>'Medio',
'db_importance3_3'=>'Alto',
'db_incident_cost_aplicable_penalty'=>'Bien 292',
'db_incident_cost_direct_lost'=>'Pérdida directa',
'db_incident_cost_enviroment'=>'Recuperación del Medio Ambiente 293',
'db_incident_cost_indirect_lost'=>'Pérdida indirecta',
'db_process_priority_high'=>'3 - Alto',
'db_process_priority_low'=>'1 - Bajo',
'db_process_priority_middle'=>'2 - Medio',
'db_process_type_4'=>'Recursos humanos',
'db_process_type_5'=>'Tecnología de la información',
'db_process_type_6'=>'Finanzas y Contabilidad',
'db_process_type_7'=>'Ambiental, Salud y Seguridad',
'db_process_type_8'=>'Mejora de la eficiencia',
'db_process_type_administrative'=>'Mercado y clientes',
'db_process_type_operational'=>'Desarrollo de nuevos productos',
'db_process_type_support'=>'Producción y Envío',
'db_rcimpact_0'=>'No afecta',
'db_rcimpact_1'=>'Disminución de un nivel',
'db_rcimpact_2'=>'Disminución de dos niveles',
'db_rcimpact_3'=>'Disminución de tres niveles',
'db_rcimpact_4'=>'Disminución de cuatro niveles',
'db_rcimpact3_0'=>'No afecta',
'db_rcimpact3_1'=>'Disminución de un nivel',
'db_rcimpact3_2'=>'Disminución de dos niveles',
'db_rcprob_0'=>'No afecta',
'db_rcprob_1'=>'Disminución de un nivel',
'db_rcprob_2'=>'Disminución de dos niveles',
'db_rcprob_3'=>'Disminución de tres niveles',
'db_rcprob_4'=>'Disminución de cuatro niveles',
'db_rcprob3_0'=>'No afecta',
'db_rcprob3_1'=>'Disminución de un nivel',
'db_rcprob3_2'=>'Disminución de dos niveles',
'db_register_type_1'=>'Confidencial',
'db_register_type_2'=>'Restringido',
'db_register_type_3'=>'Institucional',
'db_register_type_4'=>'Público',
'db_risk_confidenciality'=>'Confidencialidad',
'db_risk_disponibility'=>'Disponibilidad',
'db_risk_intregrity'=>'Integridad',
'db_risk_severity'=>'Severidad',
'db_risk_type_administrative'=>'Administrativo',
'db_risk_type_fisic'=>'Físico',
'db_risk_type_tecnologic'=>'Tecnológico',
'db_risk_type_threat'=>'Amenaza',
'db_risk_type_vulnerability'=>'Vulnerabilidad',
'db_risk_type_vulnerability_x_threat'=>'Evento (Vulnerabilidad x Amenaza)',
'db_riskprob_1'=>'Raro (1% - 20%) 305',
'db_riskprob_2'=>'Poco probable (21% - 40%) 306',
'db_riskprob_3'=>'Moderado (41% - 60%) 307',
'db_riskprob_4'=>'Probable (61% - 80%) 308',
'db_riskprob_5'=>'Casi seguro (81% - 99%) 353',
'db_riskprob3_1'=>'Improbable 278',
'db_riskprob3_2'=>'Intermedio',
'db_riskprob3_3'=>'Probable 280',
'si_external_audit'=>'Auditoría Externa',
'si_internal_audit'=>'Auditoría Interna',
'si_security_control'=>'Control de Seguridad',

/* './classes/nonauto/FeedbackEvent.php' */

'em_isms_feedback'=>'Retroalimentación de Real ISMS',
'mx_bug'=>'Error',
'mx_content_issue'=>'Sugerencia',
'mx_feature_request'=>'Mejora',

/* './classes/nonauto/policy_management/DocumentsDrawGrid.php' */

'to_future_publication'=>'El documento estará disponible a los lectores en la fecha %date%',

/* './classes/policy_management/PMDocInstance.php' */

'mx_document_instance'=>'Instancia de Documento',

/* './classes/policy_management/PMDocument.php' */

'st_denied_permission_to_manage_document'=>'Usted no tiene permiso para gestionar este documento.',
'st_document_remove_error'=>'No es posible eliminar el documento <b>%name%</b>. El documento contiene subdocumentos o los registros asociados. Para eliminar documentos o registros que contengan subdocumentos asociados, active la opción "Eliminar en cascada" en "Admin -> Configuración."',
'st_policy_management'=>'Gestión de Política',
'tt_document_remove_error'=>'Error cuando se eliminaba el documento.',

/* './classes/policy_management/PMRegister.php' */

'mx_day_days'=>'día(s)',
'mx_month_months'=>'mes(es)',
'mx_register'=>'Registro',
'mx_week_weeks'=>'semana(s)',

/* './classes/policy_management/PMTemplate.php' */

'mx_document_template'=>'Plantilla de documento',

/* './classes/policy_management/report/ISMSReportAccessedDocumentsFilter.php' */

'rs_users'=>'Usuarios',

/* './classes/policy_management/report/ISMSReportContextsWithoutDocuments.php' */

'rs_components'=>'Componentes',

/* './classes/policy_management/report/ISMSReportDocsWithHighFrequencyRevision.php' */

'rs_justification'=>'Razón',
'rs_revision_date'=>'Fecha de Revisión',
'rs_user'=>'Usuario',

/* './classes/policy_management/report/ISMSReportDocsWithHighFrequencyRevisionFilter.php' */

'report_filter_all_userid'=>'Todos 384',
'report_filter_high_frequency_revision_definition'=>'Alta frecuencia de revisión es:',
'report_filter_high_frequency_revision_definition_explanation'=>'más de una revisión en el período de una semana.',
'report_filter_userid_cl'=>'Responsable:',

/* './classes/policy_management/report/ISMSReportDocumentAccessesFilter.php' */

'rs_accesses'=>'Accesos',
'rs_date'=>'Fecha',
'rs_only_last'=>'Sólo el último',
'rs_show_all'=>'Mostrar todo 388',

/* './classes/policy_management/report/ISMSReportDocumentDatesFilter.php' */

'rs_after_initialdate'=>'Después de %initialDate%',
'rs_before_finaldate'=>'Antes de %finalDate%',
'rs_between_initialfinaldate'=>'Entre %initialDate% y %finalDate%',
'rs_creation_date'=>'Fecha de Creación',
'rs_last_revision_date'=>'Fecha de última revisión',

/* './classes/policy_management/report/ISMSReportDocumentRegistersFilter.php' */

'rs_documents'=>'Documentos',

/* './classes/policy_management/report/ISMSReportDocumentsByComponentFilter.php' */

'report_filter_all_doctype'=>'Todos',
'report_filter_doctype_cl'=>'Tipo de Documento:',

/* './classes/policy_management/report/ISMSReportDocumentsByStateFilter.php' */

'report_filter_showregisterdocs_cl'=>'Mostrar documentos de Registro: 401',
'report_filter_status_cl'=>'Estado de los Documentos',
'rs_all_status'=>'Todos',

/* './classes/policy_management/report/ISMSReportDocumentSummary.php' */

'rs_approvers_list_section'=>'Lista de aprobadores',
'rs_author'=>'Autor',
'rs_components_list_section'=>'Lista de Componentes',
'rs_deadline'=>'Fecha límite',
'rs_description'=>'Descripción',
'rs_document_type_management'=>'Gestión',
'rs_documents_type_others'=>'Otros 424',
'rs_file'=>'Archivo',
'rs_general_info_section'=>'Información General',
'rs_keywords'=>'Palabras claves',
'rs_link'=>'Enlace',
'rs_no_approvers_message'=>'Este documento no tiene aprobadores.',
'rs_no_components_message'=>'Este documento no tiene componentes.',
'rs_no_readers_message'=>'Este documento no tiene lectores.',
'rs_no_references_message'=>'Este documento no tiene referencias.',
'rs_no_registers_message'=>'Este documento no tiene registros.',
'rs_no_subdocs_message'=>'Este documento no tiene subdocumentos.',
'rs_published_date'=>'Publicado en',
'rs_readers_list_section'=>'Lista de lectores',
'rs_references_list_section'=>'Lista de Referencias',
'rs_registers_list_section'=>'Lista de Registros',
'rs_report_not_supported'=>'REPORTE NO SOPORTADO EN ESTE FORMATO',
'rs_subdocs_list_section'=>'Lista de Subdocumentos',
'rs_type'=>'Tipo',
'rs_version'=>'Versión',

/* './classes/policy_management/report/ISMSReportDocumentsWithoutRegisterFilter.php' */

'report_filter_all_document_responsible'=>'Todos 432',
'report_filter_document_responsible_cl'=>'Responsable del Documento:',

/* './classes/policy_management/report/ISMSReportNotAccessedDocumentsFilter.php' */

'rs_expand_readers'=>'Expandir lectores',

/* './classes/policy_management/report/ISMSReportSummarizedPendantTasksPM.php' */

'rs_amount'=>'Cantidad',
'rs_responsible'=>'Responsable',
'rs_tasks'=>'Tareas',

/* './classes/policy_management/report/ISMSReportTopRevisedDocumentsFilter.php' */

'rs_expand_documents'=>'Expandir documentos',
'rs_show_only_first_cl'=>'Mostrar sólo el primero:',

/* './classes/policy_management/report/ISMSReportUserCommentsFilter.php' */

'rs_all_documents'=>'Todos 436',
'rs_documents_cl'=>'Documentos:',

/* './classes/policy_management/report/ISMSReportUsersByProcessFilter.php' */

'rs_no'=>'No',
'rs_organize_by_area'=>'Organizados por área',
'rs_yes'=>'Sí',

/* './classes/policy_management/report/ISMSReportUsersWithPendantRead.php' */

'cb_user'=>'Usuario',
'mx_document'=>'Documento',

/* './classes/policy_management/report/ISMSReportUsersWithPendantReadFilter.php' */

'report_filter_all_document_reader_cl'=>'Todos 439',
'report_filter_document_reader_cl'=>'Lectores de Documentos',

/* './classes/risk_management/report/ISMSReportBestPracticesFilter.php' */

'rs_only_applied'=>'Solo aplicado',
'rs_standard_cl'=>'Estandar:',

/* './classes/risk_management/report/ISMSReportConformityFilter.php' */

'rs_all'=>'Todos 443',
'rs_standard_filter_cl'=>'Filtro estandar:',

/* './classes/risk_management/report/ISMSReportControlCostByCostCategoryFilter.php' */

'rs_costcategory_cl'=>'Categoría:',

/* './classes/risk_management/report/ISMSReportControlPlanningFilter.php' */

'rs_control_types_cl'=>'Tipo de Control:',

/* './classes/risk_management/report/ISMSReportControlsFilter.php' */

'rs_delayed'=>'Demorado',
'rs_implementationstatus_cl'=>'Mostrar solo:',
'rs_not_trusted_controls'=>'Controles no confiables',
'rs_trusted_and_efficient_controls'=>'Controles confiables y eficientes',
'rs_under_implementation'=>'En implementación',

/* './classes/risk_management/report/ISMSReportEventsByCategoryFilter.php' */

'rs_category_cl'=>'Categoría:',

/* './classes/risk_management/report/ISMSReportGenericClassifTypePrioFilter.php' */

'lb_event_types_cl'=>'Tipo de Eventos:',

/* './classes/risk_management/report/ISMSReportParametersFilter.php' */

'rs_considered_parameters_cl'=>'Parámetros considerados:',

/* './classes/risk_management/report/ISMSReportPendantTasksFilter.php' */

'rs_event_types_cl'=>'Tipo de Eventos:',

/* './classes/risk_management/report/ISMSReportProcessesByPriority.php' */

'rs_undefined_priority'=>'Prioridad indefinida',

/* './classes/risk_management/report/ISMSReportRiskFinancialImpactFilter.php' */

'mx_accepted'=>'Aceptado',
'mx_avoided'=>'Evitado',
'mx_not_treated'=>'No Tratado',
'mx_reduced'=>'Reducido',
'mx_transferred'=>'Transferido',
'mx_treated'=>'Tratado',
'rs_not_treated_or_accepted'=>'No tratado o aceptado',
'rs_treatment_cl'=>'Tratamiento:',

/* './classes/risk_management/report/ISMSReportRiskImpactFilter.php' */

'lb_considered_parameters_cl'=>'Parámetros considerados:',
'lb_risk_types_cl'=>'Tipo de Riesgos:',

/* './classes/risk_management/report/ISMSReportRisksByAreaFilter.php' */

'rs_area_cl'=>'Área:',

/* './classes/risk_management/report/ISMSReportRisksByAssetFilter.php' */

'rs_event_category_cl'=>'Category:',

/* './classes/risk_management/report/ISMSReportRisksByProcessFilter.php' */

'rs_process_cl'=>'Proceso:',
'st_all'=>'Todos',

/* './classes/risk_management/report/ISMSReportRisksByType.php' */

'rs_undefined_type'=>'Tipo indefinido',

/* './classes/risk_management/report/ISMSReportRisksFilter.php' */

'rs_estimated_risks'=>'riesgos estimados',
'rs_not_estimated'=>'No Estimado',
'rs_not_estimated_risks'=>'riesgos no estimados',
'rs_not_treated_risks'=>'riesgos no tratados',
'rs_residual_value_cl'=>'Riesgo Residual:',
'rs_show_only_cl'=>'Mostrar solo:',
'rs_treated_risks'=>'riesgos tratados',
'rs_value_cl'=>'Riesgo Potencial:',

/* './classes/risk_management/report/ISMSReportRisksResume.php' */

'cb_area'=>'Área',
'cb_asset'=>'Activo',
'cb_process'=>'Proceso',
'cb_risk'=>'Riesgo',
'st_parameterized_risks'=>'Riesgos Estimados',

/* './classes/risk_management/report/ISMSReportRisksResumeFilter.php' */

'cb_all'=>'Todo',
'lb_area_cl'=>'Área:',
'lb_area_priorities_cl'=>'Prioridades del Área:',
'lb_area_types_cl'=>'Tipo de área:',
'lb_asset_cl'=>'Activo:',
'lb_department'=>'Proceso:',
'lb_process_priorities_cl'=>'Prioridades del proceso:',
'lb_process_types_cl'=>'Tipo de procesos:',

/* './classes/risk_management/report/ISMSReportRiskValuesFilter.php' */

'rs_high'=>'Alto',
'rs_low'=>'Bajo',
'rs_mid_high'=>'Medio Alto',
'rs_mid_low'=>'Medio Bajo',
'rs_medium'=>'Medio',
'rs_not_parameterized'=>'No Estimado',
'rs_real_risk'=>'Riesgo Potencial',
'rs_residual_risk'=>'Riesgo Residual',
'rs_risk_color_based_on_cl'=>'Color del riesgo basado en:',
'rs_risk_types_cl'=>'Tipo de Riesgos:',
'rs_risks_values_cl'=>'Valores de los Riesgos:',

/* './classes/risk_management/report/ISMSReportStatementOfApplicabilityFilter.php' */

'lb_control_types_cl'=>'Tipo de Controles:',
'lb_standard_cl'=>'Estándar:',

/* './classes/risk_management/report/ISMSReportTop10AssetsWithMoreRisksFilter.php' */

'lb_risk_types'=>'Tipo de Riesgos:',

/* './classes/risk_management/riskparameters/RiskParameters.php' */

'mx_justification_bl'=>'Motivo',
'mx_justifying'=>'Justificar',

/* './classes/risk_management/RMAcceptRisk.php' */

'mx_risk_acceptance'=>'Aceptación de Riesgo',

/* './classes/risk_management/RMArea.php' */

'mx_area'=>'Área',
'st_area_remove_error_message'=>'"Imposible borrar el Área del Negocio <b>%area_name%</b>: El Área contiene sub-áreas y/o Procesos. Para remover un Área del Negocio que contiene sub-áreas o procesos, active la opción ""Borrar en Cascada"" en ""Administrar -> Configuraciones""."',
'tt_area_remove_erorr'=>'Error cuando se eliminaba Área del Negocio',

/* './classes/risk_management/RMAsset.php' */

'mx_asset'=>'Activo',
'st_asset_remove_error_message'=>'"Imposible borrar el Activo <b>%asset_name%</b>: Hay Riesgos asociados con el Activo. Para eliminar Activos que están asociados a Riesgos, active la opción ""Borrar en Cascada"" en ""Administrar -> Configuraciones"""',
'tt_asset_remove_error'=>'Error cuando se eliminaba Activo',

/* './classes/risk_management/RMBestPractice.php' */

'mx_best_practice'=>'Mejor Práctica',
'st_best_practice_remove_error_message'=>'"Imposible borrar la Mejor Práctica <b>%best_practice_name%</b>: La Mejor práctica está asociada con uno o más Controles. Para eliminar una Mejor Práctica que está asociada con uno o más Controles, active la opción ""Borrar en Cascada"" en ""Administrar -> Configuraciones"""',
'tt_best_practice_remove_error'=>'Error cuando se eliminaba Mejor Práctica (Best Practice)',

/* './classes/risk_management/RMBestPracticeEvent.php' */

'mx_best_practice_event_association'=>'Asociación de Mejor Práctica con Evento',

/* './classes/risk_management/RMBestPracticeStandard.php' */

'mx_best_practice_standard_association'=>'Asociación de Mejor Práctica con Estándar',

/* './classes/risk_management/RMCategory.php' */

'mx_category'=>'Categoría',
'st_category_remove_error_message'=>'"Imposible borrar la Categoría <b>%category_name%</b>: La Categoría contiene Subcategorías y/o Eventos y/o Activos. Para eliminar una Categoría que contiene Subcategorías, Eventos o Assets, active la opción ""Borrar en Cascada"" en ""Administrar -> Configuraciones"""',

/* './classes/risk_management/RMControl.php' */

'mx_control'=>'Control',
'st_rm_module_name'=>'Gestión de Riesgo',

/* './classes/risk_management/RMControlBestPractice.php' */

'mx_control_best_practice_association'=>'Asociación de Mejor Práctica con Control',

/* './classes/risk_management/RMControlImplementationAccept.php' */

'mx_control_implementation_acceptance'=>'Aceptación de Implementación de Control',

/* './classes/risk_management/RMEvent.php' */

'mx_event'=>'Evento',

/* './classes/risk_management/RMProcess.php' */

'mx_process'=>'Proceso',

/* './classes/risk_management/RMProcessAsset.php' */

'mx_process_asset_association'=>'Asociación de Activo con Proceso',

/* './classes/risk_management/RMRisk.php' */

'mx_risk'=>'Riesgo',

/* './classes/risk_management/RMRiskControl.php' */

'mx_risk_control_association'=>'Asociación de Control con Riesgo',

/* './classes/risk_management/RMRiskLimits.php' */

'mx_risk_tolerance'=>'Tolerancia a Riesgo',

/* './classes/risk_management/RMSectionBestPractice.php' */

'mx_best_practice_section'=>'Sección de Mejores Prácticas',
'st_home'=>'Raíz',
'st_section_control_best_practice_remove_error_message'=>'Imposible borrar la Sección <b>%section_name%</b>: La Sección contiene Mejores Prácticas y/o Sub-secciones que contienen Mejores Prácticas vinculadas con controles.',
'st_section_remove_error_message'=>'"Imposible borrar la Sección <b>%section_name%</b>: La Sección contiene Mejores Prácticas y/o Sub-secciones que contienen Mejores Prácticas vinculadas con controles. Para eliminar una Secciones que contienen Subsecciones o Mejores Prácticas, active la opción ""Borrar en Cascada"" en ""Administrar -> Configuraciones"""',
'tt_section_remove_error'=>'Error cuando se eliminaba la Sección',

/* './classes/risk_management/RMStandard.php' */

'mx_standard'=>'Estándar',
'st_libraries_module_name'=>'Librerías',
'st_remove_standard_error'=>'Imposible borrar el estándar <b>%standard_name%</b> porque éste está vinculado con mejores prácticas.',
'tt_remove_standard_error'=>'Error cuando se eliminaba el Estándar',

/* './classes/WKFAlert.php' */

'em_alert_check_list'=>'<br>Para ver el alerta, acceda al sistema y revise su lista de actividades</b><br> Para más información acerca de esta alerta, póngase en contacto con el oficial de seguridad.',
'wk_ap_conclusion'=>'El Plan de Acción %name% ha sido finalizado, en otras palabras, ha sido medido.',
'wk_ap_efficiency_revision_late'=>'La revisión de eficiencia del Plan de Acción \'%name%\' ha expirado.',
'wk_ap_efficiency_revision_near'=>'Se aproxima la fecha de revisión de eficiencia del Plan de Acción \'%name%\'.',
'wk_approved'=>'ha sido aprobado.',
'wk_asset_np_risk_association'=>'Un riesgo no estimado fue asociado al activo \'%asset%\' del evento sugerido \'%event%\'. 545',
'wk_control_implementation'=>'El Control \'%name%\' debe ser implementado.',
'wk_control_test_late'=>'El seguimiento de la Prueba de Control \'%name%\'  es tardía. 543',
'wk_deadline_close'=>'La fecha límite para la aprobación del documento \'%name%\' se está aproximando.',
'wk_deadline_expired'=>'La fecha límite para la aprobación del documento \'%name%\' ha expirado.',
'wk_delegated'=>'ha sido delegada a usted.',
'wk_denied'=>'ha sido denegado.',
'wk_disciplinary_process'=>'Los usuario(s) %users% ha/han sido agregado(s) al proceso disciplinario de incidente %name%.',
'wk_document_comment'=>'El Documento \'%name%\' ha recibido un comentario.',
'wk_document_creation_comment'=>'El documento \'%name%\' recibió un comentario de criación.',
'wk_document_is_now_published'=>'El Documento \'%name%\' fue publicado.',
'wk_document_new_version'=>'El Documento \'%name%\' tiene una nueva versión.',
'wk_document_publication'=>'El Documento \'%name%\' se envío a publicación.',
'wk_document_revision'=>'El Documento \'%name%\' se envió a revisión.',
'wk_document_revision_late'=>'La revisión del documento \'%name%\' está demorada.',
'wk_implementation_expired'=>'La Implementación del control \'%name%\' ha expirado.',
'wk_inc_asset'=>'El incidente \'%incident%\' ha sido relacionado con el activo \'%asset%\'.',
'wk_inc_control'=>'El incidente \'%incident%\' ha sido vinculado al control \'%control%\'.',
'wk_inc_disposal_app'=>'La eliminación inmediata para el incidente %name% fue aprobada.',
'wk_inc_disposal_denied'=>'La eliminación inmediata del incidente %name% fue denegada.',
'wk_inc_evidence_required'=>'Proceso de Recolección de Evidencia requerido por el incidente \'%name%\'.',
'wk_inc_sent_to_responsible'=>'El incidente \'%name%\' fue reenviado a usted.',
'wk_inc_solution_app'=>'La solución al incidente %name% ha sido aprobada.',
'wk_inc_solution_denied'=>'La solución al incidente %name% ha sido denegada.',
'wk_incident'=>'El incidente \'%name%\' ha sido creado.',
'wk_incident_risk_parametrization'=>'El riesgo %risk_name% ha sido asociado al activo %asset_name%, del cual usted es responsable. Los parámetros del riesgo son diferentes del riesgo original.',
'wk_non_conformity_waiting_conclusion'=>'Nonconformity %name% awaiting finishing. 552',
'wk_non_conformity_waiting_deadline'=>'Nonconformity %name% awaiting deadline. 551',
'wk_pre_approved'=>'ha sido pre-aprobada.',
'wk_probability_update_down'=>'La frecuencia de los incidentes asociados al riesgo \'%name%\', en el último período, indica que la probabilidad es inferior a lo que se estimaba. 529',
'wk_probability_update_up'=>'La frecuencia de los incidentes asociados al riesgo \'%name%\', en el último período, indica que la probabilidad es superior a lo que se estimaba. 539',
'wk_real_efficiency_late'=>'El seguimiento de la eficacia real de control \'% nombre%\' es tardío. 528',
'wk_risk_created'=>'El riesgo no estimado \'%risk%\' ha sido vinculado al activo \'%asset%\'.',
'wk_warning'=>'Advertencia',

/* './classes/WKFSchedule.php' */

'mx_first_week'=>'primero',
'mx_fourth_week'=>'cuarto',
'mx_friday'=>'Viernes',
'mx_last'=>'último',
'mx_last_week'=>'último',
'mx_monday'=>'Lunes',
'mx_saturday'=>'Sábado',
'mx_second_last'=>'segundo a último',
'mx_second_wee'=>'segundo',
'mx_sunday'=>'Domingo',
'mx_third_week'=>'tercero',
'mx_thursday'=>'Jueves',
'mx_tuesday'=>'Martes',
'mx_wednesday'=>'Miércoles',
'st_schedule_and'=>'y',
'st_schedule_april'=>'Abril',
'st_schedule_august'=>'Agosto',
'st_schedule_daily'=>'cada día',
'st_schedule_day_by_month'=>'en el día %day%',
'st_schedule_day_by_month_inverse'=>'en %day% día',
'st_schedule_day_by_week'=>'en %week% %week_day%',
'st_schedule_december'=>'Diciembre',
'st_schedule_each_n_days'=>'cada %periodicity% días',
'st_schedule_each_n_months'=>'cada %periodicity% meses, %day_expression% del mes',
'st_schedule_each_n_weeks'=>'en %week_days%, cada %periodicity% semanas',
'st_schedule_endless_interval'=>'Desde %start%',
'st_schedule_february'=>'Febrero',
'st_schedule_friday_pl'=>'Viernes',
'st_schedule_interval'=>'Entre %start% y %end%',
'st_schedule_january'=>'Enero',
'st_schedule_july'=>'Julio',
'st_schedule_june'=>'Junio',
'st_schedule_march'=>'Marzo',
'st_schedule_may'=>'Mayo',
'st_schedule_monday_pl'=>'Lunes',
'st_schedule_monthly'=>'%day_expression% de cada mes',
'st_schedule_november'=>'Noviembre',
'st_schedule_october'=>'Octubre',
'st_schedule_saturday_pl'=>'Sábado',
'st_schedule_september'=>'Septiembre',
'st_schedule_specified_months'=>'%day_expression% de %months%',
'st_schedule_sunday_pl'=>'Domingo',
'st_schedule_thursday_pl'=>'Jueves',
'st_schedule_tuesday_pl'=>'Martes',
'st_schedule_wednesday_pl'=>'Miércoles',
'st_schedule_weekly'=>'cada %week_days%',

/* './classes/WKFTask.php' */

'wk_task'=>'Tarea',
'wk_task_execution_permission'=>'Usted no tiene permiso para ejecutar la tarea.',

/* './crontab.php' */

'em_saas_abandoned_system'=>'Sistema Abandonado',

/* './damaged_system.php' */

'st_system_corrupted'=>'Su sistema posiblemente esté dañado. Por favor, repita la instalación o contacte nuestro Soporte en el sitio de Realiso.',

/* './damaged_system.xml' */

'tt_damaged_system'=>'Sistema dañado.',

/* './email_sender.php' */

'em_daily_digest'=>'Resumen Semanal',

/* './graphs/ISMSGraphAPActionType.php' */

'mx_ap_action_type_proportion'=>'Proporción de Planes de Acción y sus Tipos',
'mx_corrective_action'=>'Acción Correctiva',
'mx_preventive_action'=>'Acción Preventiva',

/* './graphs/ISMSGraphAssetCost.php' */

'mx_summary_of_assets_costs'=>'Resumen de Activos y Costos',
'st_assets_cost_above_1m'=>'Activos con valores por encima de %curr_symbol% 1.000.000',
'st_assets_cost_below_100k'=>'Activos con valores por debajo de %curr_symbol% 100.000',
'st_assets_cost_between_1m_and_500k'=>'Activos con valores entre %curr_symbol% 1.000.000 y %curr_symbol% 5.000.000',
'st_assets_cost_between_500k_and_100k'=>'Activos con valores entre %curr_symbol% 5.000.000 y %curr_symbol% 100.000',

/* './graphs/ISMSGraphAssetEstimatedCost.php' */

'mx_assets_with_without_estimated_costs'=>'Activos con/sin Valor de Costo Estimado',
'st_assets_with_estimated_cost'=>'Activos con costo estimado',
'st_assets_without_estimated_cost'=>'Activos sin costo estimado',

/* './graphs/ISMSGraphAssetIncident.php' */

'mx_10_assets_with_most_incidents'=>'Top 10 de Activos con más Incidentes',

/* './graphs/ISMSGraphAssetRisk.php' */

'mx_10_assets_with_most_risks'=>'Top 10 de Activos con más riesgos',

/* './graphs/ISMSGraphControlEfficiency.php' */

'mx_expected_efficiency'=>'Eficacia Esperada',
'mx_history_of_controls_efficiency_revision'=>'Historia de Revisiones de Eficacia de Controles',
'mx_real_efficiency'=>'Eficacia Real',

/* './graphs/ISMSGraphControlTest.php' */

'mx_history_of_control_test'=>'Historia de Test de Control',
'mx_test_not_ok'=>'Prueba No Correcta',
'mx_test_ok'=>'Prueba Correcta',
'mx_test_value'=>'Valor del Prueba',

/* './graphs/ISMSGraphDocumentRegisters.php' */

'mx_10_document_with_most_registers'=>'Top 10 de Documentos con más Registros',

/* './graphs/ISMSGraphDocumentsAccessed.php' */

'mx_amount_of_accessed_documents'=>'Cantidad de Documentos Accedidos',
'st_accessed_documents'=>'Documentos Accedidos',
'st_accessed_documents_cumulative'=>'Documentos Accedidos  Acumulado',

/* './graphs/ISMSGraphDocumentsClassification.php' */

'mx_amount_of_documents_by_classification'=>'Cantidad de Documentos por Tipo de Clasificación',

/* './graphs/ISMSGraphDocumentsCreated.php' */

'mx_amount_of_created_documents'=>'Cantidad de Documentos Creados',
'st_created_documents'=>'Documentos Creados',
'st_created_documents_cumulative'=>'Documentos Creados  Acumulado',

/* './graphs/ISMSGraphDocumentsState.php' */

'mx_amount_of_documents_by_status'=>'Cantidad de Documentos por Estado',

/* './graphs/ISMSGraphDocumentsType.php' */

'mx_amount_of_documents_by_type'=>'Cantidad de Documentos por Tipo',
'mx_management'=>'Gestión',
'mx_others'=>'Otros',
'mx_without_type'=>'Sin Tipo',

/* './graphs/ISMSGraphIncidentCategory.php' */

'mx_incident_category_proportion'=>'Proporciones de Incidentes y Categorías',
'st_others'=>'Otros',

/* './graphs/ISMSGraphIncidentFinancialImpact.php' */

'mx_incident_financial_impact_summary'=>'Top 10 Incidentes de Impacto Financiero',

/* './graphs/ISMSGraphIncidentLossType.php' */

'mx_direct_losses'=>'Pérdidas Directas',
'mx_incident_loss_type_proportion'=>'Proporciones de Incidentes y Tipo de Pérdidas',
'mx_indirect_losses'=>'Pérdidas Indirectas',

/* './graphs/ISMSGraphIncidentsCreated.php' */

'mx_incidents_created_summary'=>'Resumen de Incidentes en los últimos 6 meses',

/* './graphs/ISMSGraphPotentialRisk.php' */

'mx_potential_risks_summary'=>'Resumen de Riesgos Potenciales',
'st_high_risks'=>'Riesgos Altos',
'st_low_risks'=>'Riesgos Bajos',
'st_medium_risks'=>'Riesgos Medios',
'st_medium_high_risks'=>'Riesgo medio-alto',
'st_medium_low_risks'=>'Riesgo medio-bajo',

'st_non_parameterized_risks'=>'Riesgos No Estimados',

/* './graphs/ISMSGraphProcessAsset.php' */

'mx_10_processes_with_most_associated_assets'=>'Top 10 Procesos con más activos asociados',

/* './graphs/ISMSGraphProcessDocuments.php' */

'mx_10_processes_with_most_documents'=>'Top 10 Procesos con más Documentos',

/* './graphs/ISMSGraphProcessIncident.php' */

'mx_10_processes_with_most_incidents'=>'Top 10 Procesos con más Incidentes',

/* './graphs/ISMSGraphProcessNonConformity.php' */

'mx_10_processes_with_most_nc_summary'=>'Top 10 Procesos con más No Conformidades',

/* './graphs/ISMSGraphProcessRisk.php' */

'mx_10_processes_with_most_risks'=>'Top 10 Procesos con más riesgos',

/* './graphs/ISMSGraphProcessUsers.php' */

'mx_10_processes_with_most_users'=>'Top 10 Procesos con más personas involucradas',

/* './graphs/ISMSGraphRedAssetRisk.php' */

'mx_10_assets_with_most_residual_risks_in_red'=>'Top 10 Activos con los más altos riesgos',

/* './graphs/ISMSGraphRedProcessRisk.php' */

'mx_10_processes_with_most_residual_risks_in_red'=>'Top 10 Procesos con los más altos riesgos',

/* './graphs/ISMSGraphResidualRisk.php' */

'mx_residual_risks_summary'=>'Resumen de Riesgos Residuales',
'st_high_risk'=>'Riesgo Alto',
'st_low_risk'=>'Riesgo Bajo',
'st_medium_risk'=>'Riesgo Medio',
'st_non_parameterized_risk'=>'Riesgo No Estimado',

/* './graphs/ISMSGraphSingleStatistic.php' */

'st_chart_not_enough_data'=>'No hay suficientes datos recolectados para generar ese gráfico.',

/* './graphs/ISMSGraphTreatedRisk.php' */

'mx_summary_of_risks_treatments'=>'Resumen de Tratamientos de Riesgos',
'st_avoided_risk'=>'Riesgo Evitado',
'st_risk_accepted_being_yellow_or_red'=>'Riesgo Aceptado, siendo medio o alto 678',
'st_risk_accepted_for_being_green'=>'Riesgo Aceptado por ser bajo 675',
'st_risk_treated_with_user_of_control'=>'Riesgo tratado con el uso de control',
'st_transferred_risk'=>'Riesgo Transferido',

/* './handlers/QueryCountDocsTimeToApprove.php' */

'gs_day'=>'día',
'gs_days'=>'días',
'gs_hour'=>'hora',
'gs_hours'=>'horas',
'gs_minute'=>'minuto',
'gs_minutes'=>'minutos',
'gs_not_applicable'=>'NA',
'gs_second'=>'segundo',
'gs_seconds'=>'segundos',

/* './invalid_activation_code.php' */

'st_invalid_activation_code'=>'El código de activación usado no es válido. Por favor, revise este o contacte nuestro Equipo de Soporte en el sitio web de RealIso.',

/* './invalid_activation_code.xml' */

'tt_invalid_activation_code'=>'Código de Activación No Válido',

/* './license_corrupted.xml' */

'st_license_corrupted_message'=>'Su sistema está usando una licencia no válida o una que ha sido dañada.',
'tt_license_corrupted'=>'Licencia no Válida o Dañada',

/* './license_expired.php' */

'st_license_expired'=>'Su licencia expira en <b>%expiracy_str%</b>.',

/* './license_expired.xml' */

'tt_license_expired'=>'Licencia expirada',

/* './license_isms.php' */

'st_incident_management'=>'Mejora Contínua',
'st_number_of_modules'=>'Número de módulos',
'st_risk_management'=>'Gestión de Riesgo',
'st_system_modules_bl'=>'<b>Módulos del Sistema</b>',

/* './license_limit_reached.php' */

'st_assets'=>'Activos',
'st_modules'=>'Módulos',
'st_standards'=>'Estándares',
'st_users'=>'Usuarios',

/* './license_limit_reached.xml' */

'st_license_limit_reached'=>'El número máximo de %attribute_name% ha sido alcanzado. No es posible agregar %attribute_name%.',
'tt_license_limit_reached'=>'El límite de %attribute_name% ha sido alcanzado',

/* './asaas_block_popup_language.php' */
'st_title_asaas_block'=>'Final del ensayo',
'st_body_title_asaas_block'=>"Estimado usuario, <br><br> Es el momento de elegir el plan que mejor se adapte a su organización. <br/>",
'st_choose_plan_asaas_block'=>'Seleccione la opción deseada:',
'st_users_asaas_block'=>'&nbsp Usuarios',
'st_month_asaas_block'=>'Mes',
'st_buy_asaas_block'=>'&nbsp&nbsp Continuar &nbsp&nbsp',
'st_wait_title_asaas_block'=>'Por favor, espere mientras procesamos su petición',
'st_wait_body_asaas_block'=>'<p><br>En cuestión de segundos serás redirigido al medio de pago para finalizar su compra.<br/></p>',
'st_warning_payment_title' => 'Estimado usuario, por desgracia, no hemos recibido la confirmación de su pago.',
'st_warning_payment_body' => 'Por favor, póngase en contacto con nosotros a través de apoyo. Visita',

/* './login.php' */

'ad_login_error_user_and_password_do_not_match'=>'%user_login% (%user_ip%)',
'ad_login_log_message'=>'%label_user% <b>%user_name%</b> %action%.',
'mx_session_conflict'=>'Ingreso fallido porque hay otro usuario  que inició sesión en el mismo navegador.',
'st_blocked_instance_message'=>'Instancia Bloqueada',
'tt_blocked_instance'=>'Instancia Bloqueada',
'tt_session_conflict'=>'Ingreso Fallido',

/* './login.xml' */

'cb_remember_password'=>'Recordar Contraseña',
'cb_remember_user'=>'Recordar Usuario',
'st_login_panel'=>'Panel de Ingreso',
'vb_forgot_your_password'=>'¿Olvidó su contraseña?',
'vb_login'=>'Ingreso',
'wn_denied_access'=>'¡Usuario sin permisos para ingresar al sistema!',
'wn_invalid_user_or_password'=>'¡Usuario o contraseña no válida!',
'wn_max_users'=>'Número máximo de ingresos simultáneos alcanzado!',
'wn_user_blocked'=>'¡Usuario Bloqueado!',

/* './migration/ISMS_import.php' */

'ad_created'=>'Creado',
'ad_edited'=>'Editado',
'ad_removed_from_bin'=>'Eliminado de la Papelera de Reciclaje',

/* './migration/ISMS_migration.php' */

'st_destination_db_error'=>'Error cuando se conectaba a la base de datos de destino para la migración. ¡Verifique si el dato ingresado es correcto!',
'st_source_db_error'=>'Error cuando se conectaba a la base de datos de origen para la migración. ¡Verifique si el dato ingresado es correcto!',

/* './migration/ISMS_migration.xml' */

'lb_database_name_cl'=>'Nombre de Base de Datos:',
'lb_database_password_cl'=>'Contraseña de la Base de Datos:',
'lb_database_user_name_cl'=>'Nombre de Usuario de la BD:',
'lb_hostname_cl'=>'Nombre de equipo:',
'tt_database_copy_bl'=>'<b>Copia de Base de Datos</b>',
'tt_destiny_database_bl'=>'<b>Base de Datos de destino</b>',
'tt_results_bl'=>'<b>Resultados</b> 731',
'tt_source_database_bl'=>'<b>Base de Datos de Origen</b>',

/* './nav_graphics.xml' */

'lb_graphic_cl'=>'Elija un Gráfico:',
'si_optgroup_ci'=>'Mejora Contínua',
'si_optgroup_cost'=>'Costo',
'si_optgroup_pm'=>'Administrador de Políticas',
'si_optgroup_revision'=>'Revisión',
'si_optgroup_rm'=>'Administrador de Riesgos',
'si_optgroup_test'=>'Prueba',

/* './nav_search.xml' */

'cb_document_template'=>'Plantilla de Documentos',
'cb_incident'=>'Incidente',
'cb_incident_category_library'=>'Categoría de Incidentes',
'cb_non_conformity'=>'No Conformidad',
'cb_occurrence'=>'Ocurrencia',
'lb_new_search_bl_cl'=>'<b>Nueva Búsqueda:</b>',
'rb_action_plan'=>'Plan de Acción',
'rb_advanced'=>'Avanzado',
'rb_basic'=>'Básico',
'rb_soluction'=>'Soluciones',
'st_creation_cl'=>'Creación:',
'st_dates_filter_bl'=>'Filtro de Fecha',
'st_modification_cl'=>'Modificación:',
'to_advanced_search_doc'=>'<b>Búsqueda avanzada de documentos:</b><br/>Tipo de búsqueda que incluye el contenido de un archivo en la búsqueda, además de los campos de búsqueda básica.',
'to_template_document_advanced_help'=>'<b>Búsqueda avanzada de plantillas</b><br/>Tipo de búsqueda que incluye el contenido de un archivo en la búsqueda, así como los campos de búsqueda.',
'tt_data_filter'=>'Filtro de Fecha',
'tt_elements_filter'=>'Filtro de Items:',
'vb_dates_filter'=>'Filtro de Fecha',
'vb_elements_filter'=>'Filtro de Elementos',
'wn_select_one_element_filter'=>'La investigación debe tener al menos 1 elemento de filtro seleccionado.',

/* './nav_statistics.php' */

'mx_action_plan_efficient_and_not'=>'Cantidad de Planes de Acción eficientes y no eficientes',
'mx_action_plan_finished_late'=>'Cantidad de Planes de Acción finalizados tarde',
'mx_action_plan_finished_on_time'=>'Cantidad de Planes de Acción finalizados a tiempo',
'mx_action_plan_total'=>'Cantidad de Planes de Acción',
'mx_area_amount'=>'Cantidad de Áreas',
'mx_area_assets_amount'=>'Cantidad de Activos por Área',
'mx_area_assets_cost'=>'Costo de Activos de Área',
'mx_area_controls_amount'=>'Cantidad de Controles por Área',
'mx_area_controls_cost'=>'Costo de Controles de Áreas',
'mx_area_processes_amount'=>'Cantidad de Procesos por Área',
'mx_area_risks_amount'=>'Cantidad de Riesgos por Área',
'mx_area_total_impact'=>'Total de Impacto de Área',
'mx_area_values'=>'Valores del Área',
'mx_areas_risk_summary'=>'Resumen de Riesgos por Área',
'mx_areas_without_processes'=>'Áreas sin Procesos',
'mx_asset_controls_amount'=>'Cantidad de Controles de Activos',
'mx_asset_controls_cost'=>'Costo de Controles de Activos',
'mx_asset_processes_amount'=>'Cantidad de Procesos de Activos',
'mx_asset_risks_amount'=>'Cantidad de Riesgos de Activos',
'mx_asset_total_impact'=>'Impacto Total de Activos',
'mx_asset_values'=>'Valores de Activos',
'mx_assets_amount'=>'Cantidad de Activos',
'mx_assets_risk_summary'=>'Resumen de Riesgos por Activo',
'mx_assets_total_cost'=>'Costo Total de Activos',
'mx_assets_without_risk_events'=>'Activos sin Eventos de Riesgos',
'mx_business_areas'=>'Áreas de Negocio',
'mx_business_processes'=>'Procesos de Negocio',
'mx_control_risks_amount'=>'Cantidad de Controles de Riesgos',
'mx_controls_adequacy'=>'Adecuación de Controles',
'mx_controls_amount'=>'Cantidad de Controles',
'mx_controls_implementation'=>'Implementación de Controles',
'mx_controls_revision'=>'Revisión de Controles',
'mx_controls_test'=>'Prueba de Controles',
'mx_controls_without_associated_risks'=>'Controles sin Riesgos Asociados',
'mx_document_total'=>'Cantidad de Documentos',
'mx_documents_by_state'=>'Cantidad de Documentos por Estado',
'mx_documents_by_type'=>'Cantidad de Documentos por Tipo',
'mx_implemented_controls_investment'=>'Inversión de Controles Implementados',
'mx_incident_total'=>'Cantidad de Incidentes',
'mx_incidents_by_state'=>'Cantidad de Incidentes por Estados',
'mx_nc_per_ap_average'=>'Cantidad promedio de No Conformidades por Plan de Acción',
'mx_non_conformity_by_state'=>'Cantidad de No Conformidades por Estado',
'mx_non_conformity_total'=>'Cantidad de No Conformidades',
'mx_non_parameterized_risks'=>'Riesgos No Estimados',
'mx_not_measured_implemented_controls'=>'Implementación de Controles No Medidos',
'mx_occupation_documents'=>'Ocupación de Archivos de Documentos (MB)',
'mx_occupation_registers'=>'Ocupación de Archivos de Registros (MB)',
'mx_occupation_template'=>'Ocupación de Archivos de Plantillas (MB)',
'mx_occupation_total'=>'Ocupación Total (MB)',
'mx_process_assets_amount'=>'Cantidad de Activos de Procesos',
'mx_process_assets_cost'=>'Costo de Activo de Proceso',
'mx_process_controls_amount'=>'Cantidad de Controles por Proceso',
'mx_process_controls_cost'=>'Costo de Procesos de Controles',
'mx_process_risks_amount'=>'Cantidad de Riesgos de Proceso',
'mx_process_total_impact'=>'Total de Impacto de Proceso',
'mx_process_values'=>'Valores de Proceso',
'mx_processes_amount'=>'Cantidad de Procesos',
'mx_processes_risk_summary'=>'Resumen de Riesgos por Proceso',
'mx_processes_without_assets'=>'Procesos sin Activos',
'mx_read_documents'=>'Documentos Leídos',
'mx_read_documents_proportion'=>'Proporción de Documentos Leídos',
'mx_register_total'=>'Cantidad de Registros',
'mx_risk_treatment_potential_impact'=>'Impacto Potencial de Tratamiento de Riesgo',
'mx_risks_amount'=>'Cantidad de Riesgos',
'mx_risks_potential_impact'=>'Impacto Potencial de Riesgos',
'mx_risks_treatment'=>'Tratamiento de Riesgos',
'mx_scheduled_documents'=>'Proporción de Revisión de Documentos Agendados',
'mx_scheduled_revision_documents'=>'Revisión de Documentos Agendada',
'mx_standard_best_practice_amount'=>'Cantidad de Mejores Prácticas del Estándar',
'mx_total_documents'=>'Total de Documentos',
'mx_unread_documents'=>'Documentos No Leídos',
'mx_unread_documents_proportion'=>'Proporción de Documentos No Leídos',
'si_abnormalities'=>'Anormalidades',
'si_action_plan_statistics'=>'Estadísticas del Plan de Acción',
'si_area_statistics'=>'Estadísticas por Área',
'si_asset_statistics'=>'Estadísticas de Activos',
'si_best_practice_summary'=>'Resumen de Mejor Práctica',
'si_continual_improvement'=>'Estadísticas de Mejora Contínua',
'si_control_summary'=>'Resumen de Control',
'si_financial_statistics'=>'Estadísticas Financieras',
'si_management_level_and_range'=>'Rango y Niveles de Gestión',
'si_policy_management'=>'Estadísiticas de Gestión de Políticas',
'si_process_statistics'=>'Estadísticas de Procesos',
'si_risk_summary'=>'Resumen de Riesgo',
'si_risks_summary'=>'Resumen de Riesgos',
'si_system_status'=>'Estado del Sistema',
'st_accepted_risk'=>'Riesgo Aceptado',
'st_applied_best_practices_amount'=>'Cantidad de Mejores Prácticas Aplicadas',
'st_best_practices_amount'=>'Cantidad de Mejores Prácticas',
'st_complete_areas'=>'Áreas Completas',
'st_complete_assets'=>'Activos Totales',
'st_complete_processes'=>'Procesos Completos',
'st_controls_being_implemented'=>'Controles siendo implementados',
'st_controls_correctly_implemented'=>'Controles Implementados Correctamente',
'st_controls_incorrectly_implemented'=>'Controles Implementados Incorrectamente',
'st_controls_with_delayed_implementation'=>'Controles con implementación demorada',
'st_denied_access_to_statistics'=>'Las configuraciones referidas a recolección de datos están deshabilitados, por lo tanto, no es posible ver esta pantalla.',
'st_efficient_action_plan'=>'Planes de Acción eficientes',
'st_medium_high_not_treated_risk'=>'Riesgo Medio / Alto / No Tratado',
'st_mitigated_risk'=>'Riesgos Reducido',
'st_non_efficient_action_plan'=>'Planes de Acción no eficientes',
'st_not_treated_risks'=>'Riesgos no Tratados',
'st_partial_areas'=>'Áreas Parciales',
'st_partial_assets'=>'Activos Parciales',
'st_partial_processes'=>'Procesos Parciales',
'st_potentially_low_risk'=>'Riesgo Potencialmente Bajo',
'st_successfully_revised'=>'Revisado exitosamente',
'st_successfully_tested'=>'Exitosamente probado',
'st_tranferred_risk'=>'Riesgo Transferido',
'st_treated_risks'=>'Riesgos Tratados',
'st_unmanaged_areas'=>'Áreas no Gestionadas',
'st_unmanaged_assets'=>'Activos no Gestionados',
'st_unmanaged_processes'=>'Procesos no Gestionados',
'st_unsuccessfully_revised'=>'Revisado no exitosamente',
'st_unsuccessfully_tested'=>'Prueba no exitosa',

/* './nav_statistics.xml' */

'lb_finish_cl'=>'Fin:',
'lb_statistics_cl'=>'Estadísticas:',
'rb_adequate'=>'Suitable (887)',
'rb_ap_efficient'=>'Eficiente',
'rb_ap_non_efficient'=>'No Eficiente',
'rb_in_implantation'=>'En implementación',
'rb_inadequate'=>'Unsuitable (888)',
'rb_late'=>'Demorado',
'rb_not_successfully'=>'No Exitosamente',
'rb_real_risk'=>'Riesgo Potencial:',
'rb_residual_risk'=>'Riesgo Residual:',
'rb_successfully'=>'Exitosamente',
'vb_refresh'=>'Actualizar',

/* './nav_summary_advanced.php' */

'gc_amount_bl'=>'<b>#</b>',
'gc_applied_bl'=>'<b>Aplicado</b>',
'gc_assets_bl'=>'<b>Activos</b>',
'gc_best_practices_bl'=>'<b># de Mejores Prácticas</b>',
'gc_description_bl'=>'<b>Descripción</b>',
'gc_disciplinary_process_amount_bl'=>'<b># de Procesos Disciplinarios</b>',
'gc_doc_rev_qty_bl'=>'<b>#</b>',
'gc_doc_sumary_percentage_bl'=>'<b>%</b>',
'gc_doc_sumary_qty_bl'=>'<b>#</b>',
'gc_document_bl'=>'<b>Documento</b>',
'gc_efficiency_rates_bl'=>'<b>Porcentajes de Eficacia</b> (955)',
'gc_financial_impact_bl'=>'<b>Impacto Financiero</b>',
'gc_implemented_controls_bl'=>'<b>Controles Implementados</b>',
'gc_incident_amount_bl'=>'<b># de Incidentes</b>',
'gc_investment_bl'=>'<b>Inversión</b>',
'gc_nc_amount'=>'<b># de No Conformidades</b>',
'gc_nc_amount_bl'=>'<b># de No Conformidades</b>',
'gc_planned_controls_bl'=>'<b>Controles Planteados</b>',
'gc_potential_bl'=>'<b>Potencial</b>',
'gc_potential_impact_bl'=>'<b>Impacto Potencial</b>',
'gc_processes_bl'=>'<b>Procesos</b>',
'gc_residual_bl'=>'<b>Residual</b>',
'gc_standards_bl'=>'<b>Estándares</b>',
'gc_status_bl'=>'<b>Estado</b>',
'gc_total_bl'=>'<b>Total</b>',
'gc_users_bl'=>'<b>Empleado</b>',
'gs_accepted_risk'=>'Riesgo Aceptado',
'gs_amount_bl'=>'<b>#</b>',
'gs_applicability_statement'=>'Declaración de Aplicabilidad',
'gs_avoided_risk'=>'Riesgo Evitado',
'gs_ci_ap_without_doc'=>'Planes de Acción sin Documentos',
'gs_ci_cn_without_ap'=>'No Conformidades sin Planes de Acción',
'gs_ci_incident_without_occurrence'=>'Incidentes sin Ocurrencias',
'gs_ci_incident_without_risks'=>'Incidentes sin Riesgos',
'gs_ci_pending_tasks'=>'Tareas pendientes de Mejora Contínua',
'gs_components_without_document'=>'Componentes sin Documentos',
'gs_controls_without_associated_risk'=>'Contoles sin Riesgos Asociados',
'gs_delayed'=>'Demorado',
'gs_high_risk'=>'Riesgo Alto',
'gs_medium_high_risk'=>'Riesgo medio-alto',
'gs_medium_low_risk'=>'Riesgo medio-bajo',
'gs_impact_left_bl'=>'<b>Impacto Restante</b>',
'gs_implemented_controls_mitigated_risks_bl'=>'Controles Implementados / Riesgos Reducidos',
'gs_implemented_controls_protected_assets_bl'=>'Controles Implementados / Activos Protegidos',
'gs_implemented_controls_treated_risks_bl'=>'Controles Implementados/Riesgos Tratados',
'gs_inefficient_controls'=>'Controles No eficientes',
'gs_low_risk'=>'Riesgo Bajo',
'gs_medium_risk'=>'Riesgo Medio',
'gs_mitigated_risk'=>'Riesgo Reducido',
'gs_non_parameterized_risks'=>'Riesgos No estimados',
'gs_not_measured_controls'=>'Controles No Medidos',
'gs_not_treated'=>'No Tratado',
'gs_not_treated_risks_high_and_medium'=>'Riesgos No Tratados  Alto a Medio',
'gs_not_trusted_controls'=>'Controles no confiables',
'gs_pm_docs_with_pendant_read'=>'Documentos con lectura pendiente',
'gs_pm_documents_without_register'=>'Documentos sin Registros',
'gs_pm_incident_without_risks'=>'Documentos con alta frecuencia de revisiones',
'gs_pm_never_read_documents'=>'Documentos nunca leídos',
'gs_pm_pending_tasks'=>'Tareas Pendientes de Gestión de Política',
'gs_pm_users_with_pendant_read'=>'Usuarios con lecturas pendientes',
'gs_potential_risks_low'=>'Riesgos Potenciales  Bajo',
'gs_risk_management_pending_tasks'=>'Tareas Pendientes de Gestión de Riesgos',
'gs_risk_treatment_plan'=>'Plan de Tratamiento de Riesgos',
'gs_scope_statement'=>'Declaración del Alcance',
'gs_sgsi_policy_statement'=>'Declaración de la Política del SGSI',
'gs_total_bl'=>'<b>Total</b>',
'gs_total_non_parameterized_risks_bl'=>'<b>Riesgos No Estimados</b>',
'gs_total_parameterized_risks_bl'=>'<b>Riesgos Estimados</b>',
'gs_total_risks_bl'=>'<b>Total</b>',
'gs_transferred_risk'=>'Riesgo Transferido',
'gs_treated'=>'Tratado',
'gs_trusted_and_efficient_controls'=>'Controles confiables y eficientes',
'gs_under_implementation'=>'En Implementación',
'tt_help_impact_left'=>'<b>Impacto Restante</b><br/><br/>Suma de los impactos de riesgo que se mantuvieron después del tratamiento.<br/> Este valor representa el total de impacto potencial que la compañía está expuesta luego del tratamiento de riesgo.',
'tt_help_implemented_controls_mitigated_risks'=>'<b>Controles Implementados/Riesgos Reducidos</b><br/><br/>Porcentaje entre la suma de los costos de los controles implementados y la suma de los potenciales de los Riesgos Reducidos.<br/>Este valor muestra el costo del control considerando la suma del impacto potencial del riesgo a la compañía en términos financieros. Esta proporción incluye solo el valor de los riesgos que fueron reducidos por la aplicación de controles.',
'tt_help_implemented_controls_protected_assets'=>'<b>Controles Aplicados/Activos Protegidos</b><br/><br/>Porcentaje entre la suma de los costos de los controles implementados y el valor de los activos protegidos.<br/>Esto es importante para no gastar más con los controles que el valor de los activos protegidos.',
'tt_help_implemented_controls_treated_risks'=>'<b>Controles Implementados/Riesgos Tratados</b><br/><br/>Proporción entre la suma de los costos de los controles implementados y la suma de los impactos potenciales de los Riesgos Tratados.<br/>Este valor muestra el costo del control considerando la suma potencial de impacto de riesgos en terminos financieros para la compañía. Esta proporción incluye todos los riesgos que fueron tratados por alguna de las posibles formas de tratamiento.',
'tt_help_inefficient_controls'=>'<b>Controles No Eficaz</b><br/><br/> Controles cuya revisión de eficacia no se ha retrasado, y cuya última revisión ha sido considerada como no eficaz (la eficacia real del control es menor que la eficacia esperada)',
'tt_help_not_measured_controls'=>'<b> Controles No medidos</b><br/><br/> Controles implementados que no han tenido revisión de eficiencia, ni prueba, o cuya medida, revisión de eficiencia, o prueba ha expirado.',
'tt_help_trusted'=>'<b>Controles no Confiables</b><br/><br/> Controles que su prueba no se ha retrasado y que su última prueba ha fallado.',
'tt_help_trusted_and_efficient'=>'<b>Controles Confiables y Eficazes</b><br/><br/> Controles que han sido probados y permitido la revisión de eficacia, y ambas actividades no se ha retrasado.',
'gs_risk_bl'=>'<b>Riesgo</b>',
'gs_asset_bl'=>'<b>Activo</b>',
'gs_process_bl'=>'<b>Proceso</b>',
'gs_area_bl'=>'<b>Área</b>',

/* './nav_summary_advanced.xml' */

'mi_excel_report'=>'Informe .XLS',
'mi_html_report'=>'Reporte HTML',
'mi_html_report_download'=>'DESCARGA de Reporte HTML',
'mi_pdf_report'=>'Informe PDF',
'mi_word_report'=>'Informe .DOC',
'tt_abnormalities_pm_summary_bl'=>'<b>Resumen de Anormalidades de la Gestión de Políticas</b>',
'tt_abnormalities_rm_summary_bl'=>'<b>Resumen de Anormalidades de la Gestión de Riesgo</b>',
'tt_abnormality_ci_summary_bl'=>'<b>Resumen de Anormalidades de la Mejora Contínua</b>',
'tt_best_practices_summary_bl'=>'<b>Mejores Prácticas - Índice de Cumplimiento</b>',
'tt_controls_summary_bl'=>'<b>Resumen de Controles</b>',
'tt_documents_average_approval_time_bl'=>'<b>Tiempo Promedio de Aprobación de Documentos</b>',
'tt_financial_summary_bl'=>'<b>Resumen Financiero</b>',
'tt_general_risks_summary_bl'=>'<b>Riesgos Estimados vs. No Estimados</b>',
'tt_grid_incident_summary_title_bl'=>'<b>Resumen de Incidentes</b>',
'tt_grid_incidents_per_asset_title_bl'=>'<b>Top 5 de Activos con más Accidentes</b>',
'tt_grid_incidents_per_process_title_bl'=>'<b>Top 5 de Procesos con más Incidentes</b>',
'tt_grid_incidents_per_user_title_bl'=>'<b>Top 5 de Usuarios con más Processos Disciplinarios</b>',
'tt_grid_nc_per_process_title_bl'=>'<b>Top 5 Procesos con más No Conformidades</b>',
'tt_grid_nc_summary_title_bl'=>'<b>Resumen de No Conformidades</b>',
'tt_incident_summary_bl'=>'<b>Resúmenes de Mejora Contínua</b>',
'tt_last_documents_revision_time_period_summary_bl'=>'<b>Resumen de Último Período de Revisión de Documentos</b>',
'tt_parameterized_risks_summary_bl'=>'<b>Riesgo Potencial vs. Riesgo Residual</b>',
'tt_policy_management_summary_bl'=>'<b>Resumen de Gestión de Política</b>',
'tt_risk_management_documentation_summary_bl'=>'<b>Resumen de Documentación de Gestión de Riesgo</b>',
'tt_risk_management_status_bl'=>'<b>Nivel de Riesgo</b>',
'tt_risk_management_x_policy_management_bl'=>'<b>Documentos por Elemento de Negocios</b>',
'tt_sgsi_documents_and_reports_bl'=>'<b>Documentos Obligatorios para el SGSI ISO 27001</b>',
'tt_top_10_most_read_documents_bl'=>'<b>Documentos Leídos - Top 10</b>',
'tt_grid_top10_risk_by_process'=>'<b>Top 10 Riesgos x Procesos</b>',
'tt_grid_top10_risk_by_area'=>'<b>Top 10 Riesgos x Áreas</b>',

/* './nav_summary_basic.php' */

'gc_documents_qty_bl'=>'<b># de Documentos</b>',
'gc_documents_status_bl'=>'<b>Estado de los Documentos</b>',
'gc_percentage_bl'=>'<b>%</b>',
'gc_qty_bl'=>'<b>#</b>',
'gc_readings_bl'=>'<b>Lecturas</b>',
'gc_time_period_smaller_or_equal_bl'=>'<b>Período de Tiempo (menor o igual que)</b>',
'gs_area'=>'Área',
'gs_areas_without_processes'=>'Áreas sin procesos',
'gs_asset'=>'Activo',
'gs_assets_without_risk_events'=>'Activos sin eventos de riesgo',
'gs_average_time'=>'Tiempo Promedio',
'gs_controls_without_risk_events_association'=>'Control sin asociación con eventos de riesgo',
'gs_documents_to_be_read'=>'Documentos a ser leídos',
'gs_documents_total_bl'=>'<b>Total de Documentos</b>',
'gs_documents_with_files'=>'Documentos con filtros',
'gs_documents_with_links'=>'Documentos con enlaces',
'gs_more_than_six_months'=>'Más de Seis Meses',
'gs_na'=>'NA',
'gs_one_month'=>'Un Mes',
'gs_one_week'=>'Una Semana',
'gs_process'=>'Proceso',
'gs_processes_without_assets'=>'Procesos sin activos',
'gs_published_documents_information_bl'=>'<b>Documentos Totales</b>',
'gs_risk'=>'Riesgo',
'gs_six_months'=>'Seis Meses',
'gs_three_months'=>'Tres Meses',

/* './nav_summary_basic.xml' */

'gc_period'=>'Período',
'tt_my_documents_average_approval_time_bl'=>'<b>Mi Tiempo Promedio de Aprobación de Documentos</b>',
'tt_my_documents_last_revision_average_time_summary_bl'=>'<b>Resumen de Tiempo Promedio de Última Revisión de Mis Documentos</b>',
'tt_my_documents_policy_management_summary_bl'=>'<b>Resumen de Gestión de Políticas (Mis Documentos)</b>',
'tt_my_elements_bl'=>'<b>Mis Items</b>',
'tt_my_pendencies_bl'=>'<b>Mis Pendientes de Gestión de Riesgo</b>',
'tt_my_risk_summary_bl'=>'<b>Mi Resumen de Riesgos</b>',
'tt_policy_management_x_risk_management_my_elements_bl'=>'<b>Gestión de Riesgo (Mis Items) x Gestión de Política</b>',
'tt_policy_sumary_bl'=>'<b>Resúmenes de Gestión de Políticas</b>',
'tt_risk_management_my_elements_documentation_summary_bl'=>'<b>Resumen Gestión de Riesgo Mis Items de Documentación</b> (1033)',
'tt_risk_summary_bl'=>'<b>Resúmenes de Gestión de Riesgo</b>',
'tt_top_10_most_read_my_documents_bl'=>'<b>Top 10 Mi Documentos Más Leídos</b>',

/* './nav_warnings.xml' */

'gc_inquirer'=>'Solicitante',
'gc_justif'=>'Solo.',
'mi_denied'=>'Denegar',
'mi_open_task'=>'Abrir tarea',
'tt_alerts_bl'=>'<b>Mis Alertas</b>',
'tt_tasks_bl'=>'<b>Mis Tareas</b>',
'vb_remove_alerts'=>'Eliminar Alertas',
'wn_event_already_approved_removed'=>'Evento ya aprobado o eliminado.',
'wn_removal_warning'=>'Usted debe seleccionar al menos una advertencia para ser eliminada.',

/* './packages/admin/acl_tree.xml' */

'to_acl_A'=>'Permisos para el área \'Administrar\' del sistema',
'to_acl_A.A'=>'Pestaña Auditar',
'to_acl_A.A.1'=>'Pestaña con la lista de tareas generadas por el sistema',
'to_acl_A.A.2'=>'Pestaña con la lista de alertas generadas por el sistema',
'to_acl_A.A.3'=>'Pestaña con las acciones ejecutadas en el sistema',
'to_acl_A.C'=>'Pestaña personalización del sistema',
'to_acl_A.C.1'=>'Personalización de nombres y cantidad de parámetros para la estimación de riesgo',
'to_acl_A.C.10'=>'Personalización del peso de cada parámetro de riesgo',
'to_acl_A.C.11'=>'Personalización de clasificación del sistema',
'to_acl_A.C.2'=>'Personalización de la definición de la matriz de riesgos, de la etiqueta de estimación de importancia de activos, el impacto y probabilidad, y el criterio de reducción de control (1624)',
'to_acl_A.C.3'=>'Personalización de parámetros para la estimación de costos',
'to_acl_A.C.4'=>'Personalización de parámetros de impacto financiero',
'to_acl_A.C.5'=>'Personalización de varias características del sistema',
'to_acl_A.C.6'=>'Personalización de usuarios especiales',
'to_acl_A.C.7'=>'Personalización de la cantidad y nombres de tipos y prioridades usadas en el sistema',
'to_acl_A.C.7.1'=>'Configuración de clasificación de área, en relación con tipo y prioridad',
'to_acl_A.C.7.2'=>'Configuración de clasificación de procesos, en relación al tipo y prioridad',
'to_acl_A.C.7.3'=>'Configuración de clasificación de riesgo, en relación con tipo',
'to_acl_A.C.7.4'=>'Configuración de clasificación de eventos, en relación al tipo',
'to_acl_A.C.7.5'=>'Configuración de clasificación de controles, en relación al tipo',
'to_acl_A.C.7.6'=>'Configuración de clasificación de documentos, en relación al tipo',
'to_acl_A.C.7.7'=>'Configuración de clasificación de registros, en relación al tipo',
'to_acl_A.C.8'=>'Personalización de controles de seguridad',
'to_acl_A.C.9'=>'Personalización de nivel de aceptación de riesgo',
'to_acl_A.L'=>'Pestaña',
'to_acl_A.L.1'=>'Click para restaurar los items seleccionados',
'to_acl_A.L.2'=>'Click para eliminar los items seleccionados',
'to_acl_A.L.3'=>'Click para eliminar todos los items de la papelera de reciclaje',
'to_acl_A.MA'=>'Pestaña gestión de cuenta',
'to_acl_A.P'=>'Pestaña Perfiles',
'to_acl_A.P.1'=>'Menú para editar cierto perfil',
'to_acl_A.P.2'=>'Menú para borrar cierto perfil',
'to_acl_A.P.3'=>'Click para insertar un perfil',
'to_acl_A.S'=>'Pestaña configuraciones del sistema',
'to_acl_A.S.1'=>'Configuración de política de contraseñas',
'to_acl_A.S.2'=>'Configuración del sistema \'Borrar Cascada\'',
'to_acl_A.U'=>'Pestaña usuarios',
'to_acl_A.U.1'=>'Menú para editar cierto usuario',
'to_acl_A.U.2'=>'Menú para eliminar cierto usuario',
'to_acl_A.U.3'=>'Click para insertar un usuario',
'to_acl_A.U.4'=>'Click para reasignar las contraseñas de todos los usuarios',
'to_acl_A.U.5'=>'Menú para bloquear usuarios de sistema',
'to_acl_M'=>'Permisos para el modo de sistema \'Vista de Usuario\'',
'to_acl_M.CI'=>'Pestaña de mejora contínua',
'to_acl_M.CI.1'=>'Pestaña de ocurrencias',
'to_acl_M.CI.1.1'=>'Permiso para ver todas las ocurrencias',
'to_acl_M.CI.1.2'=>'Click para insertar ocurrencias',
'to_acl_M.CI.1.3'=>'Menú para editar incluso si el usuario no es el administrador del incidente ni el creador de la ocurrencia',
'to_acl_M.CI.1.4'=>'Menú para eliminar incluso si el usuario no es el administrador del incidente ni el creador de la ocurrencia',
'to_acl_M.CI.2'=>'Pestaña de incidente',
'to_acl_M.CI.2.1'=>'Permiso para ver todos los incidentes',
'to_acl_M.CI.2.2'=>'Click para insertar un incidente',
'to_acl_M.CI.2.3'=>'Menú para editar inclusive si el usuario no es el administrador del incidente',
'to_acl_M.CI.2.4'=>'Menú para eliminar inclusive si el usuario no es el administrador del incidente',
'to_acl_M.CI.3'=>'Pestaña de no conformidades',
'to_acl_M.CI.3.1'=>'Permiso para ver todas las no conformidades',
'to_acl_M.CI.3.2'=>'Click para insertar una no conformidad',
'to_acl_M.CI.3.3'=>'Menú para editar incluso si el usuario no es el administrador de la no conformidad',
'to_acl_M.CI.3.4'=>'Menú para eliminar incluso si el usuario no es el administrador de la no conformidad',
'to_acl_M.CI.5'=>'Pestaña proceso disciplinario',
'to_acl_M.CI.5.1'=>'Permiso para ver la acción tomada en relación con el usuario involucrado en cierto proceso disciplinario',
'to_acl_M.CI.5.2'=>'Permiso para registrar la acción que debe ser tomada en relación con el usuario involucrado en cierto proceso disciplinario',
'to_acl_M.CI.5.3'=>'Permiso para eliminar un usuario de un cierto proceso proceso disciplinario',
'to_acl_M.CI.6'=>'Pestaña informes',
'to_acl_M.CI.6.1'=>'Informes de incidente',
'to_acl_M.CI.6.1.1'=>'Informe de ocurrencias por incidente',
'to_acl_M.CI.6.1.2'=>'Informe de proceso disciplinario',
'to_acl_M.CI.6.1.3'=>'Informe de recolección de evidencia',
'to_acl_M.CI.6.1.4'=>'Informe de seguimiento de ocurrencias',
'to_acl_M.CI.6.1.5'=>'Informe de seguimiento de incidentes',
'to_acl_M.CI.6.1.6'=>'Informe de impacto financiero por incidente',
'to_acl_M.CI.6.1.7'=>'Informe de revisiones de control causadas por incidentes',
'to_acl_M.CI.6.1.8'=>'Informe de items afectados por incidentes',
'to_acl_M.CI.6.1.9'=>'Informe de cálculo automático de probabilidad de riesgos',
'to_acl_M.CI.6.2'=>'Informe de no conformidades',
'to_acl_M.CI.6.2.1'=>'Informe de no conformidades por proceso',
'to_acl_M.CI.6.2.2'=>'Informe de no conformidades de control por proceso',
'to_acl_M.CI.6.2.3'=>'Informe de no conformidades',
'to_acl_M.CI.6.2.4'=>'Informe de plan de acción por usuario',
'to_acl_M.CI.6.2.5'=>'Informe de planes de acción por proceso',
'to_acl_M.CI.6.3'=>'Informes de mejora contínua',
'to_acl_M.CI.6.3.1'=>'Informe de tareas pendientes de Mejora Contínua',
'to_acl_M.CI.6.3.2'=>'Informe de incidentes sin riesgos asociados.',
'to_acl_M.CI.6.3.3'=>'Informe de no conformidades sin planes de acción asociados a ellas.',
'to_acl_M.CI.6.3.4'=>'Informe de Planes de Acción sin Documentos (1214)',
'to_acl_M.CI.6.3.5'=>'Informe de incidentes sin ocurrencias',
'to_acl_M.CI.7'=>'Pestaña plan de acción',
'to_acl_M.CI.7.1'=>'Pestaña permiso par ver todos los planes de acción',
'to_acl_M.CI.7.2'=>'Click para insertar un plan de acción',
'to_acl_M.CI.7.3'=>'Permiso para editar cualquier plan de acción',
'to_acl_M.CI.7.4'=>'Permiso para eliminar cualquier plan de acción',
'to_acl_M.D'=>'Pestaña Panel',
'to_acl_M.D.1'=>'Pestaña de Resumen',
'to_acl_M.D.2'=>'Pestaña  Resumen Avanzado',
'to_acl_M.D.3'=>'Pestaña Gráficos',
'to_acl_M.D.6'=>'Pestaña de Estadísticas del Sistema (gráficos)',
'to_acl_M.D.6.1'=>'Estadísticas acerca de las áreas de negocio gestionadas por el sistema',
'to_acl_M.D.6.10'=>'Estadísticas acerca del estado del sistema (resumen de riesgo x área, riesgo x proceso, riesgo x activo, resumen de riesgos)',
'to_acl_M.D.6.11'=>'Estadísticas acerca de la gestión de política del sistema',
'to_acl_M.D.6.12'=>'Estadísticas acerca del sistema de mejora contínua',
'to_acl_M.D.6.13'=>'Estadísticas acerca de planes de acción y acerca de la relación entre planes de acción y no conformidades',
'to_acl_M.D.6.2'=>'Estadísticas acerca de procesos de áreas de negocio gestionadas por el sistema',
'to_acl_M.D.6.3'=>'Estadísticas acerca de activos gestionados por el sistema',
'to_acl_M.D.6.4'=>'Resumen de riesgos administrados por el sistema',
'to_acl_M.D.6.5'=>'Resumen de mejores prácticas',
'to_acl_M.D.6.6'=>'Resumen de controles del sistema',
'to_acl_M.D.6.7'=>'Resumen de anormalidades detectadas por el sistema',
'to_acl_M.D.6.8'=>'Estadísticas financieras  costos, impacto financiero e inversión',
'to_acl_M.D.6.9'=>'Estadísticas acera de área, cobertura y gestión de procesos y activos',
'to_acl_M.L'=>'Pestaña gestión de riesgo',
'to_acl_M.L.1'=>'Pestaña librería de eventos',
'to_acl_M.L.1.1'=>'Menú para mostrar en subcategorías y eventos de cierta categoría',
'to_acl_M.L.1.2'=>'Menú para editar una cierta categoría',
'to_acl_M.L.1.3'=>'Menú para eliminar una cierta categoría',
'to_acl_M.L.1.4'=>'Menú para asociar mejores prácticas a cierto evento',
'to_acl_M.L.1.5'=>'Menú para editar un cierto evento',
'to_acl_M.L.1.6'=>'Menú para eliminar un cierto evento',
'to_acl_M.L.1.7'=>'Click para insertar una nueva categoría',
'to_acl_M.L.1.8'=>'Click para insertar un nuevo evento',
'to_acl_M.L.1.9'=>'Click para asociar un evento a la categoría en cual el usuario está',
'to_acl_M.L.2'=>'Pestaña de Librería de Mejores Prácticas',
'to_acl_M.L.2.1'=>'Menú para mostrar en subsecciones y mejores prácticas de una cierta sección',
'to_acl_M.L.2.10'=>'Permite al usuario crear una plantilla de documento para la mejor práctica',
'to_acl_M.L.2.2'=>'Menú para editar cierta sección',
'to_acl_M.L.2.3'=>'Menú para borrar cierta sección',
'to_acl_M.L.2.4'=>'Menú para asociar eventos a cierta mejor práctica',
'to_acl_M.L.2.5'=>'Menú para editar una cierta mejor práctica',
'to_acl_M.L.2.6'=>'Menú para eliminar una cierta mejor práctica',
'to_acl_M.L.2.7'=>'Click para insertar una nueva sección',
'to_acl_M.L.2.8'=>'Click para insertar una nueva mejor práctica',
'to_acl_M.L.2.9'=>'Mostrar la plantilla de documento de mejor práctica en la pestaña de plantillas de documentos',
'to_acl_M.L.3'=>'Pestaña Bilbioteca de estándares',
'to_acl_M.L.3.1'=>'Menú para editar cierto estándar',
'to_acl_M.L.3.2'=>'Menú para borrar cierto estándar',
'to_acl_M.L.3.3'=>'Click para insertar un nuevo estándar',
'to_acl_M.L.4'=>'Pestaña Librería de Mejores Prácticas',
'to_acl_M.L.4.1'=>'Menú para editar cierta sección',
'to_acl_M.L.4.2'=>'Menú para editar cierta sección',
'to_acl_M.L.5'=>'Pestaña Plantilla de Documentos',
'to_acl_M.L.5.1'=>'Menú para editar una plantilla de documento',
'to_acl_M.L.5.2'=>'Menú para eliminar una plantilla de documento',
'to_acl_M.L.5.3'=>'Click para insertar una nueva plantilla de documento',
'to_acl_M.L.6'=>'Pestaña biblioteca de categorías',
'to_acl_M.L.6.1'=>'Click para insertar una categoría',
'to_acl_M.L.6.2'=>'Menú para editar categoría',
'to_acl_M.L.6.3'=>'Menú para eliminar categoría',
'to_acl_M.L.6.4'=>'Click para insertar una solución',
'to_acl_M.L.6.5'=>'Menú para editar solución',
'to_acl_M.L.6.6'=>'Menú para eliminar solución',
'to_acl_M.PM'=>'Pestaña Administración de Política',
'to_acl_M.PM.10'=>'Pestaña Vista/Gestión de documentos en desarrollo',
'to_acl_M.PM.10.1'=>'Click para insertar un documento',
'to_acl_M.PM.11'=>'Pestaña de visualización/gestión de documentos en aprobación',
'to_acl_M.PM.12'=>'Pestaña Vista/Gestión de documentos publicados',
'to_acl_M.PM.13'=>'Pestaña Vista/Gestión de documentos en revisión',
'to_acl_M.PM.14'=>'Pestaña Vista/Gestión de documentos obsoletos',
'to_acl_M.PM.15'=>'Pestaña Vista/Gestión de todos los documentos',
'to_acl_M.PM.16'=>'Pestaña de Vista/Gestión de registros',
'to_acl_M.PM.17'=>'Pestaña de informes',
'to_acl_M.PM.17.1'=>'Otros informes',
'to_acl_M.PM.17.1.16'=>'Informe que lista las propiedades de cierto documento',
'to_acl_M.PM.17.1.17'=>'Informe de clasificación',
'to_acl_M.PM.17.2'=>'Informes generales',
'to_acl_M.PM.17.2.1'=>'Informe de documentos',
'to_acl_M.PM.17.2.2'=>'Informe de documentos por área',
'to_acl_M.PM.17.2.3'=>'Informe de documentos por componente  de SGSI',
'to_acl_M.PM.17.2.4'=>'Informe de comentarios y revisiones de cambio de documentos',
'to_acl_M.PM.17.2.5'=>'Informe de comentarios de usuarios',
'to_acl_M.PM.17.3'=>'Informe de registros',
'to_acl_M.PM.17.3.1'=>'Informe de registros por documentos',
'to_acl_M.PM.17.4'=>'Informes de Control de Acceso',
'to_acl_M.PM.17.4.1'=>'Informe de auditoría de acceso de usuarios',
'to_acl_M.PM.17.4.2'=>'Informe de auditoría de acceso a documentos',
'to_acl_M.PM.17.4.3'=>'Informe de documentos y sus aprobadores',
'to_acl_M.PM.17.4.4'=>'Informe de documentos y sus lectores',
'to_acl_M.PM.17.4.5'=>'Informe de usuarios asociados a procesos',
'to_acl_M.PM.17.5'=>'Informes de Gestión',
'to_acl_M.PM.17.5.1'=>'Informe de documentos con lectura pendiente',
'to_acl_M.PM.17.5.2'=>'Informe de documentos más accedidos',
'to_acl_M.PM.17.5.3'=>'Informe de documentos no accedidos',
'to_acl_M.PM.17.5.4'=>'Informe de documentos más revisados',
'to_acl_M.PM.17.5.5'=>'Informe de aprobaciones pendientes',
'to_acl_M.PM.17.5.6'=>'Informe de fecha de creación y revisión de documentos',
'to_acl_M.PM.17.5.7'=>'Informe de agenda de revisión de documentos',
'to_acl_M.PM.17.5.8'=>'Informe de documentos con revisión retrasada',
'to_acl_M.PM.17.6'=>'Informes de anormalidades',
'to_acl_M.PM.17.6.1'=>'Informe de componentes sin documentos',
'to_acl_M.PM.17.6.2'=>'Tareas pendientes de gestión de políticas',
'to_acl_M.PM.17.6.3'=>'Documentos con una alta frecuencia de revisión. Alta frecuencia se refiere a documentos que son revisados más de una vez a la semana en promedio.',
'to_acl_M.PM.17.6.4'=>'Informe de documentos sin registros',
'to_acl_M.PM.17.6.5'=>'Informe que lista los usuarios que han sido lectores de documentos que están publicados, pero que nadie ha leído esos documentos. (1489)',
'to_acl_M.PM.18'=>'Visualización/Gestión de documentos para ser publicados',
'to_acl_M.RM'=>'Pestaña de Gestión de Riesgo',
'to_acl_M.RM.1'=>'Pestaña Área',
'to_acl_M.RM.1.1'=>'Permiso para ver todas las áreas',
'to_acl_M.RM.1.2'=>'Menú para mostrar in subáreas de cierto área',
'to_acl_M.RM.1.3'=>'Menú para ver procesos de cierta área',
'to_acl_M.RM.1.4'=>'Menú para editar cierta área',
'to_acl_M.RM.1.5'=>'Menú para eliminar una cierta área',
'to_acl_M.RM.1.6'=>'Click para insertar una nueva área',
'to_acl_M.RM.1.7'=>'Click para insertar una nueva área en la raíz',
'to_acl_M.RM.1.8'=>'Permiso para editar un área, provisto porque usted es responsable de la misma',
'to_acl_M.RM.1.9'=>'Permiso para eliminar un área, siempre que usted sea responsable de ésta',
'to_acl_M.RM.2'=>'Pestaña proceso',
'to_acl_M.RM.2.1'=>'Permiso para ver todos los procesos',
'to_acl_M.RM.2.2'=>'Menú para mostrar activos de cierto proceso',
'to_acl_M.RM.2.4'=>'Menú para editar cierto proceso',
'to_acl_M.RM.2.5'=>'Menú para borrar cierto proceso',
'to_acl_M.RM.2.6'=>'Click para insertar un nuevo proceso',
'to_acl_M.RM.2.7'=>'Permiso para editar un proceso, provisto porque usted es responsable del mismo',
'to_acl_M.RM.2.8'=>'Permiso para eliminar un proceso, siempre que usted sea responsable de éste',
'to_acl_M.RM.3'=>'Pestaña Activo',
'to_acl_M.RM.3.1'=>'Permiso para ver todos los activos',
'to_acl_M.RM.3.10'=>'Menú contextual para inserción de riesgo a activos desde eventos de una categoría de la Librería de Categoría de Eventos',
'to_acl_M.RM.3.2'=>'Menú para mostrar riesgos de cierto activo',
'to_acl_M.RM.3.4'=>'Menú para crear un riesgo para un activo, basado en un evento',
'to_acl_M.RM.3.5'=>'Menú para editar un cierto activo',
'to_acl_M.RM.3.6'=>'Menú para eliminar un cierto activo',
'to_acl_M.RM.3.7'=>'Click para insertar un nuevo activo',
'to_acl_M.RM.3.8'=>'Permiso para editar un activo, provisto porque usted es responsable del mismo',
'to_acl_M.RM.3.9'=>'Permiso para eliminar un activo, siempre que usted sea responsable de éste',
'to_acl_M.RM.4'=>'Pestaña Riesgos',
'to_acl_M.RM.4.1'=>'Permiso para ver todos los riesgos',
'to_acl_M.RM.4.10'=>'Permiso para eliminar un riesgo, siempre que usted sea responsable de éste',
'to_acl_M.RM.4.2'=>'Menú para mostrar controles de cierto riesgo',
'to_acl_M.RM.4.4'=>'Menú para editar cierto riesgo',
'to_acl_M.RM.4.5'=>'Menú para eliminar cierto riesgo',
'to_acl_M.RM.4.6'=>'Click para insertar un nuevo riesgo',
'to_acl_M.RM.4.8'=>'Click para estimar el riesgo',
'to_acl_M.RM.4.9'=>'Permiso para editar un riesgo, provisto porque usted es responsable del mismo',
'to_acl_M.RM.5'=>'Pestaña Control',
'to_acl_M.RM.5.1'=>'Permisos para ver todos los controles',
'to_acl_M.RM.5.2'=>'Menú para mostrar los riesgos asociados a cierto control',
'to_acl_M.RM.5.3'=>'Menú para asociar riesgo a cierto control',
'to_acl_M.RM.5.4'=>'Menú para editar un cierto control',
'to_acl_M.RM.5.5'=>'Menú para borrar cierto control',
'to_acl_M.RM.5.6'=>'Click para insertar un nuevo control',
'to_acl_M.RM.5.7'=>'Permiso para editar un control, provisto porque usted es responsable del mismo',
'to_acl_M.RM.5.8'=>'Permiso para eliminar el Control, mientras usted sea responsable para esto',
'to_acl_M.RM.5.9'=>'Cambiar en la historia de revisión/prueba de los controles del sistema',
'to_acl_M.RM.6'=>'Pestaña Informes',
'to_acl_M.RM.6.1'=>'Informes de Riesgos',
'to_acl_M.RM.6.1.1'=>'Informe de valores de riesgo',
'to_acl_M.RM.6.1.2'=>'Informe de riesgos por proceso',
'to_acl_M.RM.6.1.3'=>'Informe de riesgos por activo',
'to_acl_M.RM.6.1.4'=>'Informe de riesgos por control',
'to_acl_M.RM.6.1.5'=>'Informe de impacto de riesgo',
'to_acl_M.RM.6.1.6'=>'Informe de importancia de activos',
'to_acl_M.RM.6.1.7'=>'Informe de Riesgos por Área',
'to_acl_M.RM.6.1.8'=>'Lista de comprobación de Informe de eventos (1126)',
'to_acl_M.RM.6.2'=>'Informes de control',
'to_acl_M.RM.6.2.1'=>'Informe de controles por responsable',
'to_acl_M.RM.6.2.10'=>'Informe de seguimiento de los controles de prueba',
'to_acl_M.RM.6.2.11'=>'Informe de conformidades',
'to_acl_M.RM.6.2.12'=>'Informe de la agenda para la revisión de los controles por el propietario',
'to_acl_M.RM.6.2.13'=>'Informe del oficial de controles de prueba del programa',
'to_acl_M.RM.6.2.2'=>'Informe de controles por riesgo',
'to_acl_M.RM.6.2.3'=>'Informe de planificación de controles',
'to_acl_M.RM.6.2.4'=>'Informe de fecha de implementación de controles',
'to_acl_M.RM.6.2.5'=>'Informe de eficacia de controles',
'to_acl_M.RM.6.2.6'=>'Informe de resumen de controles',
'to_acl_M.RM.6.2.7'=>'Informe de seguimiento de controles',
'to_acl_M.RM.6.2.8'=>'Informe de seguimiento de eficacia de controles',
'to_acl_M.RM.6.2.9'=>'Informe de controles no medidos',
'to_acl_M.RM.6.3'=>'Informes de Gestión de Riesgos',
'to_acl_M.RM.6.3.1'=>'Informe de tareas pendientes de la gestión de riesgos',
'to_acl_M.RM.6.3.2'=>'Informe de área sin procesos',
'to_acl_M.RM.6.3.3'=>'Activos sin reporte de riesgos',
'to_acl_M.RM.6.3.4'=>'Informe de procesos sin activos',
'to_acl_M.RM.6.3.5'=>'Informe de riesgos no estimados',
'to_acl_M.RM.6.3.6'=>'Informe de controles sin riesgos',
'to_acl_M.RM.6.3.7'=>'Informe de riesgos duplicados',
'to_acl_M.RM.6.4'=>'Informes financieros',
'to_acl_M.RM.6.4.4'=>'Informe de costos por control',
'to_acl_M.RM.6.4.5'=>'Informe de costos de control por responsable',
'to_acl_M.RM.6.4.6'=>'Informe de costos de control por área',
'to_acl_M.RM.6.4.7'=>'Informe de costos de control por procesos',
'to_acl_M.RM.6.4.8'=>'Informe de costos de control por activo',
'to_acl_M.RM.6.5'=>'Informes de ISO 27001',
'to_acl_M.RM.6.5.1'=>'Informe de Declaración de Aplicabilidad',
'to_acl_M.RM.6.5.2'=>'Informe de tratamiento de riesgos',
'to_acl_M.RM.6.5.3'=>'Informe de declaración de alcance de SGSI',
'to_acl_M.RM.6.5.4'=>'Informe de declaración de política de SGSI',
'to_acl_M.RM.6.6'=>'Informes resúmenes',
'to_acl_M.RM.6.6.1'=>'Informe: top 10  activos con más riesgos',
'to_acl_M.RM.6.6.2'=>'Informe: top 10  activos con los más altos riesgos',
'to_acl_M.RM.6.6.3'=>'Informe: top 10  riesgos por área',
'to_acl_M.RM.6.6.4'=>'Informe: top 10  riesgos por procesos',
'to_acl_M.RM.6.6.5'=>'Informe de cantidad de riesgos por procesos',
'to_acl_M.RM.6.6.6'=>'Informe de cantidad de riesgos por área',
'to_acl_M.RM.6.6.7'=>'Informe de estado de riesgo por área',
'to_acl_M.RM.6.6.8'=>'Informe riesgos por proceso',
'to_acl_M.RM.6.7'=>'Otros informes',
'to_acl_M.RM.6.7.1'=>'Informe de responsabilidades de ususarios',
'to_acl_M.RM.6.7.2'=>'Informes de items pendientes',
'to_acl_M.RM.6.7.3'=>'Informe de activos',
'to_acl_M.RM.6.7.4'=>'Informe de clasificación de items',
'to_acl_M.RM.6.7.5'=>'Report that indicates for each asset its dependencies and its dependents',
'to_acl_M.RM.6.7.6'=>'Report that shows the asset relevance',
'to_acl_M.S'=>'Pestaña buscar',
'tr_acl_A'=>'Administrar',
'tr_acl_A.A'=>'AUDITAR',
'tr_acl_A.A.1'=>'TAREAS',
'tr_acl_A.A.2'=>'ALERTAS',
'tr_acl_A.A.3'=>'REGISTROS (logs)',
'tr_acl_A.C'=>'PERSONALIZACIONES',
'tr_acl_A.C.1'=>'PARAMETROS DE ESTIMACIÓN DE RIESGO',
'tr_acl_A.C.10'=>'PESO DE PARÁMETROS DE RIESGOS',
'tr_acl_A.C.11'=>'CLASIFICACIÓN DEL SISTEMA',
'tr_acl_A.C.2'=>'DEFINICIÓN DE MATRIZ DE RIESGOS',
'tr_acl_A.C.3'=>'PARÁMETROS DE ESTIMACIÓN DE COSTOS',
'tr_acl_A.C.4'=>'PARÁMETROS DE IMPACTO FINANCIERO',
'tr_acl_A.C.5'=>'CARACTERÍSTICAS DEL SISTEMA',
'tr_acl_A.C.6'=>'USUARIOS ESPECIALES',
'tr_acl_A.C.7'=>'PARAMETRIZACIÓN DE TIPO Y PRIORIDAD',
'tr_acl_A.C.7.1'=>'ÁREA',
'tr_acl_A.C.7.2'=>'PROCESO',
'tr_acl_A.C.7.3'=>'RIESGO',
'tr_acl_A.C.7.4'=>'EVENTO',
'tr_acl_A.C.7.5'=>'CONTROL',
'tr_acl_A.C.7.6'=>'DOCUMENTOS',
'tr_acl_A.C.7.7'=>'REGISTROS',
'tr_acl_A.C.8'=>'REVISIÓN DE CONTROL Y PRUEBA',
'tr_acl_A.C.9'=>'NIVEL DE ACEPTACIÓN DE RIESGO',
'tr_acl_A.L'=>'PAPELERA DE RECICLAJE',
'tr_acl_A.L.1'=>'RECUPERAR',
'tr_acl_A.L.2'=>'ELIMINAR',
'tr_acl_A.L.3'=>'ELIMINAR TODO',
'tr_acl_A.MA'=>'MI CUENTA',
'tr_acl_A.P'=>'PERFILES',
'tr_acl_A.P.1'=>'EDITAR',
'tr_acl_A.P.2'=>'ELIMINAR',
'tr_acl_A.P.3'=>'INSERTAR PERFIL',
'tr_acl_A.S'=>'CONFIGURACIONES DEL SISTEMA',
'tr_acl_A.S.1'=>'POLÍTICA DE CONTRASEÑAS',
'tr_acl_A.S.2'=>'BORRAR CASCADA',
'tr_acl_A.U'=>'USUARIOS',
'tr_acl_A.U.1'=>'EDITAR',
'tr_acl_A.U.2'=>'ELIMINAR',
'tr_acl_A.U.3'=>'INSERTAR USUARIO',
'tr_acl_A.U.4'=>'REASIGNAR CONTRASEÑA',
'tr_acl_A.U.5'=>'BLOQUEAR USUARIO',
'tr_acl_M'=>'Vista de usuario',
'tr_acl_M.CI'=>'MEJORA CONTÍNUA',
'tr_acl_M.CI.1'=>'OCURRENCIAS',
'tr_acl_M.CI.1.1'=>'LISTAR',
'tr_acl_M.CI.1.2'=>'INSERTAR OCURRENCIA',
'tr_acl_M.CI.1.3'=>'EDITAR SIN RESTRICCIÓN',
'tr_acl_M.CI.1.4'=>'ELIMINAR SIN RESTRICCIÓN',
'tr_acl_M.CI.2'=>'INCIDENTE',
'tr_acl_M.CI.2.1'=>'LISTAR',
'tr_acl_M.CI.2.2'=>'INSERTAR INCIDENTE',
'tr_acl_M.CI.2.3'=>'EDITAR SIN RESTRICCIÓN',
'tr_acl_M.CI.2.4'=>'ELIMINAR SIN RESTRICCIÓN',
'tr_acl_M.CI.3'=>'NO CONFORMIDAD',
'tr_acl_M.CI.3.1'=>'LISTAR',
'tr_acl_M.CI.3.2'=>'INSERTAR NO CONFORMIDADES',
'tr_acl_M.CI.3.3'=>'EDITAR SIN RESTRICCIÓN',
'tr_acl_M.CI.3.4'=>'ELIMINAR SIN RESTRICCIÓN',
'tr_acl_M.CI.5'=>'PROCESO DISCIPLINARIO',
'tr_acl_M.CI.5.1'=>'VER ACCIÓN',
'tr_acl_M.CI.5.2'=>'REGISTRAR ACCIÓN',
'tr_acl_M.CI.5.3'=>'ELIMINAR',
'tr_acl_M.CI.6'=>'INFORMES',
'tr_acl_M.CI.6.1'=>'INFORMES DE INCIDENTES',
'tr_acl_M.CI.6.1.1'=>'OCURRENCIAS POR INCIDENTE',
'tr_acl_M.CI.6.1.2'=>'PROCESO DISCIPLINARIO',
'tr_acl_M.CI.6.1.3'=>'RECOLECCIÓN DE EVIDENCIA',
'tr_acl_M.CI.6.1.4'=>'SEGUIMIENTO DE OCURRENCIAS',
'tr_acl_M.CI.6.1.5'=>'SEGUIMIENTO DE INCIDENTE',
'tr_acl_M.CI.6.1.6'=>'IMPACTO FINANCIERO POR INCIDENTE',
'tr_acl_M.CI.6.1.7'=>'REVISIÓN DE CONTROL CAUSADO POR INCIDENTES',
'tr_acl_M.CI.6.1.8'=>'ITEMS AFECTADOS POR INCIDENTES',
'tr_acl_M.CI.6.1.9'=>'CÁLCULO AUTOMÁTICO DE PROBABILIDAD DE RIESGOS',
'tr_acl_M.CI.6.2'=>'INFORME DE NO CONFORMIDADES',
'tr_acl_M.CI.6.2.1'=>'NO CONFORMIDADES POR PROCESO',
'tr_acl_M.CI.6.2.2'=>'NO CONFORMIDADES DE CONTROL POR PROCESO',
'tr_acl_M.CI.6.2.3'=>'SEGUIMIENTO DE NO CONFORMIDADES',
'tr_acl_M.CI.6.2.4'=>'PLAN DE ACCIÓN POR USUARIO',
'tr_acl_M.CI.6.2.5'=>'PLAN DE ACCIÓN POR PROCESO',
'tr_acl_M.CI.6.3'=>'INFORMES DE MEJORA CONTÍNUA',
'tr_acl_M.CI.6.3.1'=>'TAREAS PENDIENTES DE MEJORA CONTÍNUA',
'tr_acl_M.CI.6.3.2'=>'INCIDENTES SIN RIESGOS',
'tr_acl_M.CI.6.3.3'=>'NO CONFORMIDADES SIN PLAN DE ACCIÓN',
'tr_acl_M.CI.6.3.4'=>'PLANES DE ACCIÓN SIN DOCUMENTOS',
'tr_acl_M.CI.6.3.5'=>'INCIDENTE SIN OCURRENCIAS',
'tr_acl_M.CI.7'=>'PLAN DE ACCIÓN',
'tr_acl_M.CI.7.1'=>'LISTAR',
'tr_acl_M.CI.7.2'=>'INSERTAR',
'tr_acl_M.CI.7.3'=>'EDITAR SIN RESTRICCIÓN',
'tr_acl_M.CI.7.4'=>'ELIMINAR SIN RESTRICCIÓN',
'tr_acl_M.D'=>'PANEL',
'tr_acl_M.D.1'=>'RESÚMENES',
'tr_acl_M.D.2'=>'RESUMEN AVANZADO',
'tr_acl_M.D.3'=>'GrÁFICOS',
'tr_acl_M.D.6'=>'ESTADÍSTICAS',
'tr_acl_M.D.6.1'=>'GRÁFICOS DEL ÁREA',
'tr_acl_M.D.6.10'=>'ESTADO DEL SISTEMA',
'tr_acl_M.D.6.11'=>'ESTADÍSTICAS DE GESTIÓN DE POLÍTICA',
'tr_acl_M.D.6.12'=>'ESTADÍSTICAS DE MEJORA CONTÍNUA',
'tr_acl_M.D.6.13'=>'ESTADÍSTICAS DE PLAN DE ACCIÓN',
'tr_acl_M.D.6.2'=>'GRÁFICOS DE PROCESO',
'tr_acl_M.D.6.3'=>'GRAFICOS DE ACTIVOS',
'tr_acl_M.D.6.4'=>'RESUMEN DE RIESGOS',
'tr_acl_M.D.6.5'=>'RESUMEN DE MEJOR PRÁCTICA',
'tr_acl_M.D.6.6'=>'RESUMEN DE CONTROL',
'tr_acl_M.D.6.7'=>'ANORMALIDADES',
'tr_acl_M.D.6.8'=>'ESTADÍSTICAS FINANCIERAS',
'tr_acl_M.D.6.9'=>'COBERTURA EN EL NIVEL DE GESTIÓN',
'tr_acl_M.L'=>'BIBLIOTECAS',
'tr_acl_M.L.1'=>'BIBLIOTECA DE EVENTOS',
'tr_acl_M.L.1.1'=>'ABRIR CATEGORÍA',
'tr_acl_M.L.1.2'=>'EDITAR CATEGORÍA',
'tr_acl_M.L.1.3'=>'ELIMINAR CATEGORÍA',
'tr_acl_M.L.1.4'=>'ASOCIAR MEJORES PRÁCTICAS',
'tr_acl_M.L.1.5'=>'EDITAR EVENTO',
'tr_acl_M.L.1.6'=>'ELIMINAR EVENTO',
'tr_acl_M.L.1.7'=>'INSERTAR CATEGORÍA',
'tr_acl_M.L.1.8'=>'INSERTAR EVENTO',
'tr_acl_M.L.1.9'=>'ASOCIAR EVENTO',
'tr_acl_M.L.2'=>'BIBLIOTECA DE MEJORES PRÁCTICAS',
'tr_acl_M.L.2.1'=>'ABRIR SECCIÓN',
'tr_acl_M.L.2.10'=>'CREAR UNA PLANTILLA DE MEJOR PRÁCTICA',
'tr_acl_M.L.2.2'=>'EDITAR SECCIÓN',
'tr_acl_M.L.2.3'=>'ELIMINAR SECCIÓN',
'tr_acl_M.L.2.4'=>'ASOCIAR EVENTOS',
'tr_acl_M.L.2.5'=>'EDITAR MEJOR PRÁCTICA',
'tr_acl_M.L.2.6'=>'ELIMINAR UNA MEJOR PRÁCTICA',
'tr_acl_M.L.2.7'=>'INSERTAR SECCIÓN',
'tr_acl_M.L.2.8'=>'INSERTAR MEJOR PRÁCTICA',
'tr_acl_M.L.2.9'=>'VER PLANTILLA DE DOCUMENTO DE MEJOR PRÁCTICA',
'tr_acl_M.L.3'=>'BIBLIOTECA DE ESTÁNDARES',
'tr_acl_M.L.3.1'=>'EDITAR ESTÁNDARES',
'tr_acl_M.L.3.2'=>'ELIMINAR ESTÁNDARES',
'tr_acl_M.L.3.3'=>'INSERTAR ESTÁNDAR',
'tr_acl_M.L.4'=>'IMPORTAR / EXPORTAR',
'tr_acl_M.L.4.1'=>'EXPORTAR',
'tr_acl_M.L.4.2'=>'IMPORTAR',
'tr_acl_M.L.5'=>'BIBLIOTECA DE PLANTILLAS DE DOCUMENTOS',
'tr_acl_M.L.5.1'=>'EDITAR PLANTILLA DE DOCUMENTOS',
'tr_acl_M.L.5.2'=>'ELIMINAR PLANTILLA DE DOCUMENTOS',
'tr_acl_M.L.5.3'=>'INSERTAR PLANTILLA DE DOCUMENTO',
'tr_acl_M.L.6'=>'BIBLIOTECA DE CATEGORÍAS',
'tr_acl_M.L.6.1'=>'INSERTAR CATEGORÍA',
'tr_acl_M.L.6.2'=>'EDITAR CATEGORÍA',
'tr_acl_M.L.6.3'=>'ELIMINAR CATEGORÍA',
'tr_acl_M.L.6.4'=>'INSERTAR SOLUCIÓN',
'tr_acl_M.L.6.5'=>'EDITAR SOLUCIÓN',
'tr_acl_M.L.6.6'=>'ELIMINAR SOLUCIÓN',
'tr_acl_M.PM'=>'GESTIÓN DE POLÍTICA',
'tr_acl_M.PM.10'=>'DOCUMENTOS EN DESARROLLO',
'tr_acl_M.PM.10.1'=>'INSERTAR DOCUMENTO',
'tr_acl_M.PM.11'=>'DOCUMENTOS EN APROBACIÓN',
'tr_acl_M.PM.12'=>'DOCUMENTOS PUBLICADOS',
'tr_acl_M.PM.13'=>'DOCUMENTOS EN REVISIÓN',
'tr_acl_M.PM.14'=>'DOCUMENTOS OBSOLETOS',
'tr_acl_M.PM.15'=>'TODOS LOS DOCUMENTOS',
'tr_acl_M.PM.16'=>'REGISTROS',
'tr_acl_M.PM.17'=>'INFORMES',
'tr_acl_M.PM.17.1'=>'OTROS',
'tr_acl_M.PM.17.1.16'=>'PROPIEDADES DEL DOCUMENTO',
'tr_acl_M.PM.17.1.17'=>'INFORME DE CLASIFICACIÓN',
'tr_acl_M.PM.17.2'=>'INFORMES GENERALES',
'tr_acl_M.PM.17.2.1'=>'DOCUMENTOS',
'tr_acl_M.PM.17.2.2'=>'DOCUMENTOS POR ÁREA',
'tr_acl_M.PM.17.2.3'=>'DOCUMENTOS POR COMPONENTE DE SGSI',
'tr_acl_M.PM.17.2.4'=>'COMENTARIOS DE DOCUMENTOS  Y RAZONES',
'tr_acl_M.PM.17.2.5'=>'COMENTARIOS DE USUARIO',
'tr_acl_M.PM.17.3'=>'INFORMES DE REGISTRISTROS',
'tr_acl_M.PM.17.3.1'=>'REGISTROS POR DOCUMENTOS',
'tr_acl_M.PM.17.4'=>'INFORMES DE CONTROL DE ACCESO',
'tr_acl_M.PM.17.4.1'=>'AUDITORÍA DE ACCESO DE USUARIOS',
'tr_acl_M.PM.17.4.2'=>'AUDITORÍA DE ACCESO A DOCUMENTOS',
'tr_acl_M.PM.17.4.3'=>'DOCUMENTOS Y SUS APROBADORES',
'tr_acl_M.PM.17.4.4'=>'DOCUMENTOS Y SUS LECTORES',
'tr_acl_M.PM.17.4.5'=>'USUARIOS ASOCIADOS A PROCESOS',
'tr_acl_M.PM.17.5'=>'INFORMES DE GESTIÓN',
'tr_acl_M.PM.17.5.1'=>'DOCUMENTOS CON LECTURA PENDIENTE',
'tr_acl_M.PM.17.5.2'=>'DOCUMENTOS MÁS ACCEDIDOS',
'tr_acl_M.PM.17.5.3'=>'DOCUMENTOS NO ACCEDIDOS',
'tr_acl_M.PM.17.5.4'=>'DOCUMENTOS MÁS REVISADOS',
'tr_acl_M.PM.17.5.5'=>'APROVACIONES PENDIENTES',
'tr_acl_M.PM.17.5.6'=>'FECHA DE CREACION Y REVISIÓN DE DOCUMENTOS',
'tr_acl_M.PM.17.5.7'=>'AGENDA DE REVISIÓN DE DOCUMENTOS',
'tr_acl_M.PM.17.5.8'=>'DOCUMENTOS CON REVISIÓN RETRASADA',
'tr_acl_M.PM.17.6'=>'INFORMES DE ANORMALIDADES',
'tr_acl_M.PM.17.6.1'=>'COMPONENTES SIN DOCUMENTOS',
'tr_acl_M.PM.17.6.2'=>'TAREAS PENDIENTES',
'tr_acl_M.PM.17.6.3'=>'DOCUMENTOS CON UNA ALTA FRECUENCIA DE REVISIÓN',
'tr_acl_M.PM.17.6.4'=>'DOCUMENTOS SIN REGISTROS',
'tr_acl_M.PM.17.6.5'=>'USUARIOS CON LECTURA PENDIENTE',
'tr_acl_M.PM.18'=>'DOCUMENTOS PAR SER PUBLICADOS',
'tr_acl_M.RM'=>'GESTIÓN DE RIESGO',
'tr_acl_M.RM.1'=>'ÁREA',
'tr_acl_M.RM.1.1'=>'LISTAR TODO',
'tr_acl_M.RM.1.2'=>'VER SUB-AREA',
'tr_acl_M.RM.1.3'=>'VER PROCESO',
'tr_acl_M.RM.1.4'=>'EDITAR',
'tr_acl_M.RM.1.5'=>'ELIMINAR',
'tr_acl_M.RM.1.6'=>'INSERTAR ÁREA',
'tr_acl_M.RM.1.7'=>'INSERTAR UN ÁREA DE PRIMER NIVEL',
'tr_acl_M.RM.1.8'=>'EDITAR SI ES RESPONSABLE',
'tr_acl_M.RM.1.9'=>'ELIMINAR SI ES RESPONSABLE',
'tr_acl_M.RM.2'=>'PROCESO',
'tr_acl_M.RM.2.1'=>'LISTAR TODO',
'tr_acl_M.RM.2.2'=>'VER ACTIVOS',
'tr_acl_M.RM.2.4'=>'EDITAR',
'tr_acl_M.RM.2.5'=>'ELIMINAR',
'tr_acl_M.RM.2.6'=>'INSERTAR PROCESO',
'tr_acl_M.RM.2.7'=>'EDITAR SI ES RESPONSABLE',
'tr_acl_M.RM.2.8'=>'ELIMINAR SI ES RESPONSABLE',
'tr_acl_M.RM.3'=>'ACTIVO',
'tr_acl_M.RM.3.1'=>'LISTAR TODO',
'tr_acl_M.RM.3.10'=>'CARGAR RIESGO DESDE BIBLIOTECA',
'tr_acl_M.RM.3.2'=>'VER RIESGOS',
'tr_acl_M.RM.3.4'=>'NUEVO RIESGO',
'tr_acl_M.RM.3.5'=>'EDITAR',
'tr_acl_M.RM.3.6'=>'ELIMINAR',
'tr_acl_M.RM.3.7'=>'INSERTAR ACTIVO',
'tr_acl_M.RM.3.8'=>'EDITAR SI ES RESPONSABLE',
'tr_acl_M.RM.3.9'=>'ELIMINAR SI ES RESPONSABLE',
'tr_acl_M.RM.4'=>'RIESGO',
'tr_acl_M.RM.4.1'=>'LISTAR TODO',
'tr_acl_M.RM.4.10'=>'ELIMINAR SI ES RESPONSABLE',
'tr_acl_M.RM.4.2'=>'VER CONTROLES',
'tr_acl_M.RM.4.4'=>'EDITAR',
'tr_acl_M.RM.4.5'=>'ELIMINAR',
'tr_acl_M.RM.4.6'=>'INSERTAR RIESGO',
'tr_acl_M.RM.4.8'=>'ESTIMAR',
'tr_acl_M.RM.4.9'=>'EDITAR SI ES RESPONSABLE',
'tr_acl_M.RM.5'=>'CONTROL',
'tr_acl_M.RM.5.1'=>'LISTAR TODOS',
'tr_acl_M.RM.5.2'=>'VER RIESGOS',
'tr_acl_M.RM.5.3'=>'ASOCIAR RIESGOS',
'tr_acl_M.RM.5.4'=>'EDITAR',
'tr_acl_M.RM.5.5'=>'ELIMINAR',
'tr_acl_M.RM.5.6'=>'INSERTAR CONTROL',
'tr_acl_M.RM.5.7'=>'EDITAR SI ES RESPONSABLE',
'tr_acl_M.RM.5.8'=>'ELIMINAR SI ES RESPONSABLE',
'tr_acl_M.RM.5.9'=>'HISTORIA REVISION/PRUEBA',
'tr_acl_M.RM.6'=>'INFORMES',
'tr_acl_M.RM.6.1'=>'DE RIESGOS',
'tr_acl_M.RM.6.1.1'=>'VALORES DE RIESGO',
'tr_acl_M.RM.6.1.2'=>'RIESGOS POR PROCESO',
'tr_acl_M.RM.6.1.3'=>'RIESGOS POR ACTIVO',
'tr_acl_M.RM.6.1.4'=>'RIESGOS POR CONTROL',
'tr_acl_M.RM.6.1.5'=>'IMPACTO DEL RIESGO',
'tr_acl_M.RM.6.1.6'=>'IMPORTANCIA DE ACTIVOS',
'tr_acl_M.RM.6.1.7'=>'RIESGOS POR ÁREA',
'tr_acl_M.RM.6.1.8'=>'CHECKLIST POR EVENTO',
'tr_acl_M.RM.6.2'=>'DE CONTROLES',
'tr_acl_M.RM.6.2.1'=>'CONTROLES POR RESPONSABLE',
'tr_acl_M.RM.6.2.10'=>'SEGUIMIENTO DE LOS CONTROLES DE PRUEBA',
'tr_acl_M.RM.6.2.11'=>'CONFORMIDAD',
'tr_acl_M.RM.6.2.12'=>'CONTROL\'S REVISION AGENDA BY RESPONSIBLE',
'tr_acl_M.RM.6.2.13'=>'CONTROL\'S TEST AGENDA BY RESPONSIBLE',
'tr_acl_M.RM.6.2.2'=>'CONTROLES POR RIESGO',
'tr_acl_M.RM.6.2.3'=>'PLANIFICACIÓN DE CONTROLES',
'tr_acl_M.RM.6.2.4'=>'FECHA DE IMPLEMENTACIÓN DE CONTROLES',
'tr_acl_M.RM.6.2.5'=>'EFICACIA DE CONTROLES',
'tr_acl_M.RM.6.2.6'=>'RESUMEN DE CONTROLES',
'tr_acl_M.RM.6.2.7'=>'SEGUIMIENTO DE CONTROLES',
'tr_acl_M.RM.6.2.8'=>'SEGUIMIENTO DE EFICACIA DE CONTROLES',
'tr_acl_M.RM.6.2.9'=>'CONTROLES NO MEDIDOS',
'tr_acl_M.RM.6.3'=>'DE ADMINISTRACIÓN DE RIESGOS',
'tr_acl_M.RM.6.3.1'=>'TAREAS PENDIENTES',
'tr_acl_M.RM.6.3.2'=>'ÁREAS SIN PROCESOS',
'tr_acl_M.RM.6.3.3'=>'ACTIVOS SIN RIESGOS',
'tr_acl_M.RM.6.3.4'=>'PROCESOS SIN ACTIVOS',
'tr_acl_M.RM.6.3.5'=>'RIESGOS NO ESTIMADOS',
'tr_acl_M.RM.6.3.6'=>'CONTROLES SIN RIESGOS',
'tr_acl_M.RM.6.3.7'=>'RIESGOS DUPLICADOS',
'tr_acl_M.RM.6.4'=>'FINANCIERO',
'tr_acl_M.RM.6.4.4'=>'COSTO POR CONTROL',
'tr_acl_M.RM.6.4.5'=>'COSTOS DE CONTROL POR RESPONSABLE',
'tr_acl_M.RM.6.4.6'=>'COSTOS DE CONTROL POR ÁREA',
'tr_acl_M.RM.6.4.7'=>'COSTOS DE CONTROL POR PROCESOS',
'tr_acl_M.RM.6.4.8'=>'COSTOS DE CONTROL POR ACTIVO',
'tr_acl_M.RM.6.5'=>'ISO 27001',
'tr_acl_M.RM.6.5.1'=>'DECLARACIÓN DE APLICABILIDAD',
'tr_acl_M.RM.6.5.2'=>'PLAN DE TRATAMIENTO DE RIESGOS',
'tr_acl_M.RM.6.5.3'=>'DECLARACIÓN DE ALCANCE DE SGSI',
'tr_acl_M.RM.6.5.4'=>'DECLARACIÓN DE POLÍTICA DE SGSI',
'tr_acl_M.RM.6.6'=>'RESÚMENES',
'tr_acl_M.RM.6.6.1'=>'TOP 10  ACTIVOS CON MÁS RIESGOS',
'tr_acl_M.RM.6.6.2'=>'TOP 10  ACTIVOS CON LOS MÁS ALTOS RIESGOS',
'tr_acl_M.RM.6.6.3'=>'TOP 10  RIESGOS POR ÁREA',
'tr_acl_M.RM.6.6.4'=>'TOP 10  RIESGOS POR PROCESOS',
'tr_acl_M.RM.6.6.5'=>'CANTIDAD DE RIESGOS POR PROCESO',
'tr_acl_M.RM.6.6.6'=>'CANTIDAD DE RIESGOS POR ÁREA',
'tr_acl_M.RM.6.6.7'=>'ESTADO DE RIESGO POR ÁREA',
'tr_acl_M.RM.6.6.8'=>'ESTADO DE RIESGOS POR PROCESO',
'tr_acl_M.RM.6.7'=>'OTROS',
'tr_acl_M.RM.6.7.1'=>'RESPONSABILIDADES DE USUARIOS',
'tr_acl_M.RM.6.7.2'=>'ITEMS PENDIENTES',
'tr_acl_M.RM.6.7.3'=>'ACTIVOS',
'tr_acl_M.RM.6.7.4'=>'CLASIFICACIÓN DE ACTIVOS',
'tr_acl_M.RM.6.7.5'=>'ASSETS - DEPENDENCIES AND DEPENDENTS',
'tr_acl_M.RM.6.7.6'=>'ASSET RELEVANCE',
'tr_acl_M.S'=>'BUSCAR',


/* './packages/admin/popup_reset_database.php' */
'st_database_permanent_removal' => 'Extracción permanente de la base de datos',
'st_question_database_permanent_removal' => 'Todos los datos se eliminan! ¿Estás seguro que quieres continuar?',


/* './packages/admin/popup_reset_database.xml' */
'st_reset_database' => 'Limpie todos los datos',
'st_reset_database_question' => '¿Quieres eliminar TODOS los datos?',
'st_elements_successfully_removed' => 'La base de datos se ha eliminado correctamente.',
'vb_clean' => 'Clean',
'vb_close' => 'Close',


/* './packages/admin/popup_import_ad_users.xml' */
'lb_import_users'=>'<b>Importación de usuarios</b>',
'lb_ad_info_message'=>'Por favor, cargue a los usuarios de su servidor de AD y seleccione los que serán importados.',
'gc_username'=>'Nombre de usuario',
'gc_userlogin'=>'AD Server Login',
'wn_no_users_warning'=>'Lo sentimos, no hay ningún usuario se ha encontrado.',
'wn_no_users_selected_warning'=>'Por favor, seleccione un usuario para esta operación.',
'wn_license_limit_warning'=>'El número de usuarios seleccionados excede su licencia.',
'lb_profile_define'=>'Elegir un perfil:',
'vb_import_selected_users'=>'Importar usuarios seleccionados',
'vb_load_users'=>'Cargar los usuarios',

/* './packages/admin/cfg_general.xml' */

'lb_session_expiracy'=>'Tiempo de expiración de sesión:',
'st_minutes'=>'minutos.',
'to_session_expiracy'=>'<b>Tiempo de expiración de sesión:</b><br/><br/>Define cuanto tiempo de inactividad el sistema espera, antes de que la sesión de un usuario que ha ingresado expire.',
'vb_deactivate'=>'Desactivar',

/* './packages/admin/custom_asset_values_name.xml' */

'st_asset_parametrization_cl_bl'=>'<b>Etiqueta de Estimación de Importacia de Activo:</b>',
'st_importance'=>'Importancia (Activo)',
'tt_asset_values_name_information'=>'¿Como quiere etiquetar sus parámetros de estimación de importancia de los activos?',

/* './class/risk_managent/report/ISMSReportAssetRelevance.php' */
'rs_importance_value'=>'Valor de Importancia (Activo)',

/* './packages/admin/custom_control.xml' */

'cb_revision'=>'Revisión',
'cb_test'=>'Prueba',
'st_revision_and_test'=>'<b>Revisión de Control y Prueba</b>',
'st_revision_and_test_description'=>'¿Quiere rever sus controles de seguridad?',
'to_control_revision_config_bl_cl'=>'<b>Configuración de Revisión del Control:</b><br/><br/> Determina si el sistema permitirá la revisión de la real eficacia de controles.<br/>Esta configuración le permitirá a usted evaluar cuando un control es eficaz o no. (1671)',
'to_control_test_config_bl_cl'=>'<b>Configuración de Pruebas de Control:</b> <br/><br/>Determina si el sistema permitirá las pruebas de los controles.<br/>Esta configuración le permitirá evaluar cuando un control es confiable o no.',

/* './packages/admin/custom_control_cost_name.xml' */

'lb_cost_name_1_bl'=>'<b>Nombre del Costo 1</b>',
'lb_cost_name_2_bl'=>'<b>Nombre del Costo 2</b>',
'lb_cost_name_3_bl'=>'<b>Nombre del Costo 3</b>',
'lb_cost_name_4_bl'=>'<b>Nombre del Costo 4</b>',
'lb_cost_name_5_bl'=>'<b>Nombre del Costo 5</b>',
'st_costs_estimation_parameters_information'=>'¿Cuales son las categorías de costos que usted normalmente cuenta en aplicación de controles?',
'static_st_cost_estimation_parameters_bl'=>'<b>Parametetros de Estimación de Costos</b>',

/* './packages/admin/custom_currency.xml' */

'lb_currency_cl'=>'Moneda:',
'si_australian_dollar'=>'Dólar australiano ($)- AUD',
'si_bolivar'=>'Bolívar venezolano (Bs F)- VEF',
'si_brunei_dollar'=>'Dólar de Brunei ($)- BND',
'si_canadian_dollar'=>'Dólar canadiense ($) - CAD',
'si_chile_pesos'=>'Peso chileno ($) - CLP',
'si_colombia_pesos'=>'Peso colombiano ($) - COP',
'si_czech_koruna'=>'Corona checa (Kč) - CZK',
'si_danish_krone'=>'Corona danesa (kr)- DKK',
'si_dollar'=>'Dólares de los EE.UU. ($) - USD',
'si_euro'=>'Euro (€) - EUR',
'si_india_rupee'=>'Rupia india (₨)- INR',
'si_israel_new_shekels'=>'Chelín israelí (₪) - ILS',
'si_mexican_peso'=>'Peso mexicano ($) - MXN',
'si_newzeland_dollar'=>'Dólar de Nueva Zelanda ($) - NZD',
'si_pakistan_rupee'=>'Rupia paquistaní (₨) - PKR',
'si_pesos'=>'Peso argentino ($) - ARS',
'si_pounds'=>'Libra esterlina (£) - GBP',
'si_reais'=>'Real brasileño (R$) - BRL',
'si_singapore_dollar'=>'Dólar de Singapur ($) - SGD',
'si_swedish_krona'=>'Corona sueca (kr) - SEK',
'si_switzerland'=>'Franco suizo (Fr) - CHF',
'si_yen'=>'Yen japonés (¥) - JPY',
'si_yuan'=>'Yuan chino (¥) - CNY',
'st_system_currency_bl'=>'<b>Moneda del sistema</b>',
'st_system_currency_information'=>'¿Qué moneda quiere utilizar en el sistema?',

/* './packages/admin/custom_financial_impact_parameters.xml' */

'st_custom_financial_impact_parameter_note'=>'Ingrese el nombre de las categorías en las cuales será accedido el impacto financiero de un incidente',
'st_description_financial_impact_parameter'=>'Real ISMS le permite a usted definir la fuente de posibles pérdidas financieras asociadas con un evento de riesgo. ¿Cuales son las categorías para la fuente financiera que desa usar?',
'st_financial_impact_parameters'=>'<b>Parámetros de Impacto Financiero</b>',

/* './packages/admin/custom_interface.xml' */

'si_system_interface_param_cl'=>'<b>Parámetros de la interfaz:</b>',
'st_color_picker'=>'Abrir paleta de colores',
'st_darkColor'=>'Color oscuro:',
'st_lightColor'=>'Color de luz:',
'st_logotype_login_cl'=>'Logotipo de inicio de sesión: (266x84)',
'st_logotype_top_cl'=>'Top Logo: (133x42)',
'wn_invalid_image_type'=>'Tipo de imagen no válido.',

/* './packages/admin/custom_non_conformity_types.xml' */

'si_non_conformity_types_parametrization_cl'=>'<b>Tipos de parámetros de incumplimiento:</b>',
'st_description_non_conformity_types_parametrization'=>'El nombre que los tipos de incumplimiento?',

/* './packages/admin/custom_quantity_rm_parameters.xml' */

'lb_mode_cl'=>'Modo:',
'rb_3_three'=>'3 (tres)',
'rb_5_five'=>'5 (cinco)',
'rb_optimist'=>'Optimista',
'rb_pessimist'=>'Pesimista',
'st_custom_quantity_parameters_note'=>'OBS.: Usted puede cambiar las configuraciones en el futuro, pero será necesario reevaluar algunos valores de riesgos.',
'st_risk_quantity_values_bl_cl'=>'<b>Definición de la Matriz de Riesgo:</b>',
'tt_custom_quantity_rm_parameters_information'=>'Mejores prácticas recomiendan que usted asigne importancia, impacto y probabilidad en una matriz de 3x3.<br/> Algunas organizaciones eligen usaruna matriz de 5 x 5.<br/> Esta es una decisión que usted debe tomar antes de comenzar la gestión de riesgos.<br/> Usando una matriz de 3 x 3, los riesgos tendrán valores entre 1 y 9. Si elige una matriz de 5 x 5,los riesgos tendrán valores entre 1 y 25.<br/><br/> ¿Cómo desea su escala de matriz de riesgo?',

/* './packages/admin/custom_report_classification.xml' */

'lb_to_cl'=>'A:',
'st_report_classification'=>'<b>Clasificación de Informes</b>',
'st_report_classification_information'=>'¿Cuál es la Clasificación (y para quien esta clasificación aplica) que desea para usar en las pestañas del sistema?',
'st_system_classification_bl'=>'<b>Sistema de Clasificación</b>',
'st_system_classification_information'=>'¿Qué tipo de clasificación (y al que esta clasificación se aplica) usted desea utilizar en el sistema?',
'to_general_settings_classification'=>'<b>Clasificación:</b><br/>Indica, en la parte inferior del sistema, su clasificación. Ej.: Confidencial, Secreto, etc. (1686)',
'to_general_settings_classification_dest'=>'<b>A:</b><br/>Indica para quien se aplica la clasificación. Este campo tomará efecto solo cuando la clasificación ha sido completada.',

/* './packages/admin/custom_risk_control_values_name.xml' */

'st_rc_impact'=>'Impacto',
'st_rc_impact5_'=>'Impacto',
'st_rc_probability'=>'Probabilidad',
'st_risk_control_values_name'=>'<b>Etiqueta para los Criterios de Reducción por Controles</b>',
'st_very_high'=>'<b>Muy Alto</b>',
'st_very_low'=>'<b>Muy Bajo</b>',
'tt_risk_control_value_name_information'=>'Cuando usted aplica controles para reducir el riesgo, ellos reducen la probabilidad o el impacto vinculado a ese riesgo. ¿Cómo desea etiquetar esas reducciones en impacto y probabilidad?',

/* './packages/admin/custom_risk_limits.xml' */

'tt_use_risk_level'=>'Uso:',
'rb_3_level'=>'3 niveles',
'rb_5_level'=>'5 levels',
'st_risk_limits_definition2'=>'Riesgo el valor estimado es menor o igual que \'Tolerancia al riesgo 1\' estará marcado con verde. <br/>Superior a la \'Tolerancia al riesgo 1\' y menor o igual a \'Tolerancia al riesgo 2\' se marcará con el amarillo. <br/>Superior a la \'Tolerancia al riesgo 2\' y menor o igual a \'Tolerancia al riesgo 3\' serán marcados con color naranja. <br/>Superior a la \'Tolerancia al riesgo 3\' y menor o igual a \'Tolerancia al reisgo 4\' se marcará con rojo. <br/>Superior a la \'Tolerancia al riesgo 4\' se marcará con negro.',
'lb_risk_limits_1_bl_cl'=>'<b>Tolerancia al riesgo 1:</b>',
'lb_risk_limits_2_bl_cl'=>'<b>Tolerancia al riesgo 2:</b>',
'lb_risk_limits_3_bl_cl'=>'<b>Tolerancia al riesgo 3:</b>',
'lb_risk_limits_4_bl_cl'=>'<b>Tolerancia al riesgo 4:</b>',
'wn_two_higher_than_three'=>'El valor del límite de riesgo entre dos y no puede ser mayor que el valor del umbral de riesgo de 3.',
'wn_three_higher_than_four'=>'El valor del límite de riesgo 3 no puede ser mayor que el valor umbral de riesgo de 4.',
'wn_four_higher_than_max2'=>'El valor del umbral de 4 de riesgos no puede superar el límite de la configuración del sistema.',

'lb_risk_limits_high_bl_cl'=>'<b>Tolerancia a Riesgo 2:</b>',
'lb_risk_limits_low_bl_cl'=>'<b>Tolerancia a Riesgo 1:</b>',
'st_risk_limits_definition'=>'Define los valores para manejo de riesgo.<br/><br/> Riesgos cuyo valor calculado es inferior a la Tolerancia Nivel 1 será resaltada con color verde.<br/>Riesgos cuyo valor calculado es mayor que la tolerancia Nivel 2 se destacó con color rojo.<br/>Riesgos cuyo valor calculado esté entre estos dos niveles de tolerancia se resaltará con color amarillo.',
'tt_aceptable_risk_limits'=>'<b>Niveles Aceptables de Riesgo</b>',
'tt_risk_limits'=>'Tolerancia a Riesgo',
'wn_high_higher_than_max'=>'El valor límite del riesgo 2 no puede exceder el límite de configuración del sistema.',
'wn_low_higher_than_high'=>'El valor límite del riesgo 1 no puede ser mayor que el valor límite del riesgo 2.',
'wn_low_lower_than_1'=>'El valor límite del riesgo 1 no puede ser menor que 1.',
'wn_save_risk_matrix_update_risk_limits'=>'La tarea generada por el sistema al usuario <b>%chairman_name%</b> para la aprobación del nuevo nivel de riesgo. Hasta que el usuario apruebe el nuevo límite, el sistema usará el viejo límite de riesgo.',

/* './packages/admin/custom_risk_parameters_names.xml' */

'st_risk_parameters_bl_cl'=>'<b>Parámetros de Estimación de Riesgo:</b>',
'st_risk_parameters_help'=>'Ingrese los nombres de las propiedadesque estarán disponibles en el software para la evaluacón de riesgo. Nosotros sugerimos las siguientes propiedades: confidencialidad, integridad y disponibilidad',
'tt_risk_parameters_information'=>'¿Como etiqueta sus Parámetros de Estimación de Riesgo?',
'wn_risk_parameters_minimum_requirement'=>'Al menos un Parámetro de Riesgo debe ser especificado.',

/* './packages/admin/custom_risk_parameters_weight.xml' */

'st_risk_parameters_weight'=>'<b>Peso de Parámetros de Riesgos</b>',
'st_risk_parameters_weight_definition'=>'Defina el peso por cada parámetro de riesgo.Los pesos deben ser mayores o iguales a cero, y al menos un peso debe ser positivo. Los valores de riesgos y activos serán actualizados de acuerdo a los cambios en los pesos actuales.',
'wn_at_least_one_positive'=>'and yet other properties such as authenticity, responsibility, non-repudiation and reliability. Or as you prefer.',

/* './packages/admin/custom_risk_values_name.xml' */

'st_custom_risk_matrix_definition_generic_note'=>'"OBS.: primero seleccione el tamaño de la matriz de riesgo (3x3 or 5x5) en ""Definición de Matriz de Riesgo""."',
'st_high'=>'<b>Alto</b>',
'st_high_bl'=>'<b>Alto</b>',
'st_impact'=>'Impacto (Riesgo)',
'st_impact_risk'=>'Impacto (Riesgo)',
'st_low'=>'<b>Bajo</b>',
'st_low_bl'=>'<b>Bajo</b>',
'st_medium'=>'<b>Medio</b>',
'st_medium_bl'=>'<b>Medio</b>',
'st_probability_risk'=>'Probabilidad (Riesgo)',
'st_risk_parametrization'=>'<b>Etiqueta de Estimación de  Impacto y Probabilidad:</b>',
'st_riskprob'=>'Probabilidad (Riesgo)',
'st_values'=>'Valores',
'st_very_high_bl'=>'<b>Muy Alto</b>',
'st_very_low_bl'=>'<b>Muy Bajo</b>',
'tt_risk_values_name_information'=>'¿Como desea etiquetar su criterio de estimación de Impacto y Probabilidad?',

/* './packages/admin/custom_special_users.xml' */

'lb_asset_manager_bl'=>'<b>Administrador de Activos</b>',
'lb_chairman_bl'=>'<b>Gestión</b>',
'lb_control_manager_bl'=>'<b>Administrador de Controles</b>',
'lb_library_manager_bl'=>'<b>Administrador de Librerías</b>',
'lb_user_disciplinary_process_manager_bl'=>'<b>Administrador de Procesos Disciplinarios</b>',
'lb_user_evidence_manager_bl'=>'<b>Administrador de Evidencias</b>',
'lb_user_incident_manager_bl'=>'<b>Administrador de Incidentes</b>',
'lb_user_document_auditor_bl'=>'<b>Auditor de Documentos</b>',
'lb_user_non_conformity_manager_bl'=>'<b>Administrador de No Conformidades</b>',
'st_custom_special_users_information'=>'El flujo de trabajo de Real ISMS requiere la definición de algunos usuarios responsables de tareas especiales predefinidas en el contexto de un SGSI. ¿Quién va a ocupar el papel de estos usuarios en su sistema?',
'st_help'=>'[?]',
'st_special_users_bl'=>'<b>Usuarios Especiales de Real ISMS</b>',
'tt_special_user'=>'<b>Administrador:</b><br/><br/>usuario responsable de la aprobación de la primeras áreas de negocio, aceptación de riesgo, y otros aspectos que requieren la aprobación Gerencial.',
'tt_special_user_asset_manager'=>'<b>Administrador de Activos:</b><br/><br/>usuario responsable por el control del inventario de activos',
'tt_special_user_control_manager'=>'<b>Administrador de Controles:</b><br/><br/>usuario responsable de la administración de los controles de seguridad, aprobación de la creación de nuevos controles, y organizando controles en Real ISMS.',
'tt_special_user_disciplinary_process_manager'=>'<b>Administrador del Proceso Disciplinario:</b><br/><br/>usuario que recibe alertas de Proceso Disciplinario',
'tt_special_user_evidence_manager'=>'<b>Administrador de Evidencia:</b><br/><br/>usuario que recibe altertas de recolección de evidencia',
'tt_special_user_incident_manager'=>'<b>Administrador de Incidentes:</b><br/><br/> usuario responsable por la aprobación, edición y eliminación de ocurrencias, así como para la creación, edición y eliminación de incidentes<br/><br/>Sólo este usuario podrá crear procesos disciplinarios, así como asociar riesgos o procesos al incidente.',
'tt_special_user_document_auditor'=>'<b>Auditor de Documentos:</b><br/><br/>usuario puede leer todos los documentos del sistema.',
'tt_special_user_library_manager'=>'<b>Administrador de Librería:</b><br/><br/>usuario responsable del control de las librerías de Real ISMS, verificando nuevos eventos, nuevas mejores prácticas y estándares de seguridad',
'tt_special_user_nc_manager'=>'<b>Administrador de No Conformidades:</b><br/><br/>usuario responsable de la aprobación de no conformidades creadas por otros usuarios de Real ISMS.',

/* './packages/admin/custom_system_features.xml' */

'cb_auto_document_creation'=>'¿Generar los documentos automáticamente después de la creación de los elementos en el sistema?',
'cb_cost'=>'¿Usar Características de Estimación de Costos?',
'cb_document_manual_versioning'=>'¿Desea usted usar Versionado Manual de Documentos?',
'cb_percentual_risk'=>'Mostrar Valores de Riesgo en escala de 0 a 100?',
'cb_solicitor'=>'¿Permite al Procurador compartir derechos de acceso, privilegios y responsabilidades?',
'st_data_collecting'=>'Habilitar Recolección de Datos con una periodicidad diaria para generar estadísticas de Real ISMS.',
'st_manual_data_control'=>'¿Desea usted usar Control Manual de Fechas?',
'st_risk_formula'=>'Usar Fórmula de Riesgo  Proporcional.',
'st_system_features'=>'<b>Características del Sistema</b>',
'to_data_collecting_settings'=>'<b>Configuraciones de Recolección de Datos:</b><br/><br/>Configura si el sistema recolectará información para generar un histórico del sistema mostrado en la pestaña de estadísticas.',
'to_delete_cascade'=>'<b>Control Manual de Fechas del Sistema:</b><br/><br/>Permite al usuario definir fechas en el pasado y/o en el futuro en partes del sistema en el cual, por defecto, este tipo de flexibilidad no es permitida.',
'to_document_manual_versioning'=>'<b>Versionado Manual de Documentos:</b><br/><br/>Determina si el sistema usará versionado manual o automático de documentos.',
'to_general_settings_automatic_documents_creation'=>'<b>Configuraciones de Creación Automática de Documentos:</b><br/><br/>Configura si el sistema creará automáticamente un documento por cada item (área, proceso, activo y control) creado por el usuario.',
'to_percentual_risk_configuration'=>'<b>Configuración de escala de valores de riesgo:</b><br/><br/>Determina si los valores de riesgo deberían deben exhibirse en valor absoluto (0 a 9 o 0 a 25) o en escala de 0 a 100.',
'to_procurator_configuration'=>'<b>Procurator Configuration:</b><br/><br/>Determine if the system will allow the Procurement mechanism in the temporary absence of a user. (1748)',
'to_risk_formula'=>'<b>Fórmula de Riesgo Proporcional:</b><br/><br/>Fórmula que considera los pesos de importancia y consecuencia de incrementar el valor final de cada riesgo.',
'to_system_cost_configuration'=>'<b>Configuración de Costo:</b><br/><br/>Determina si el sistema habilitará el cálculo de costos.',

/* './packages/admin/custom_type_and_priority_parametrization.xml' */

'si_document'=>'Documento',
'si_event'=>'Evento',
'si_risk'=>'Riesgo',
'st_description_type_and_priority_parametrization'=>'Como etiqueta usted sus elementos?',
'st_element'=>'<b>Elemento:</b>',
'st_priority_bl'=>'<b>Prioridad</b>',
'st_type_and_priority_parametrization'=>'<b>Parametrización de Tipo y Prioridad</b>',
'st_type_bl'=>'<b>Tipo</b>',
'wn_classification_minimum_requirement'=>'Al menos tres clasificaciones deben ser especificadas por cada item.',

/* './packages/admin/nav_audit_alert.xml' */

'gc_justification'=>'Razón',
'tt_audit_alerts_bl'=>'<b>Alertas de Auditoría</b>',

/* './packages/admin/nav_audit_log.xml' */

'lb_user_cl'=>'Usuario:',
'si_all_actions'=>'Todas las Acciones',
'si_all_users'=>'Todos los Usuarios',
'tt_audit_log_bl'=>'<b>Registro de Auditoría</b>',

/* './packages/admin/nav_audit_task.xml' */

'gc_accomplishment'=>'Realización',
'gc_creation'=>'Creación',
'gc_receiver'=>'Receptor',
'lb_activity_cl'=>'Actividad:',
'lb_creator_cl'=>'Creador:',
'lb_receiver_cl'=>'Receptor:',
'si_all_activities'=>'Todas las Actividades',
'si_all_creators'=>'Todos los Creadores:',
'si_all_receivers'=>'Todos los Receptores',
'tt_audit_task_bl'=>'<b>Tarea de Auditoría</b>',

/* './packages/admin/nav_config.php' */

'st_check_for_correct_data'=>'Controle si los datos fueron completados correctamente.',
'st_impact_parameter_remove_message'=>'La eliminación de un parámetro financiero causará la eliminación de este parámetro en los incidentes ya estimados. ¿Usted desea continuar de todas formas?',
'st_info_saved_but'=>'La información ha sido guardada. Sin embargo,',
'st_not_possible_to_update_data_collection_info'=>'no fue posible actualizar la información recolección de datos',
'st_not_possible_to_update_mail_server_info'=>'no fue posible actualizar la información del servidor de email',
'st_unavailable_information'=>'información no disponible.',
'tt_alert'=>'Alerta',
'tt_deactivate_system'=>'Desactivar Sistema',
'tt_impact_parameter_remove'=>'Eliminar Parámetro de Impacto Financiero',

/* './packages/admin/nav_config.xml' */

'si_costs_names'=>'Nombre de los Costos',
'si_parametrization'=>'Estimación',
'si_risk_parameters_values'=>'Valores de Parámetros de Riesgos',
'si_syslog'=>'Registro del Sistema',
'si_system'=>'Sistema',

/* './packages/admin/nav_customization.php' */

'st_change_amount_of_parameters_confirm'=>'Cambiar la cantidad de parámetros hará que el sistema no pueda estimar.<BR><b> ¿Está seguro que desea hacer esto?</b> (1799)',
'st_change_amount_of_risks_confirm'=>'¡Cambiar la cantidad de riesgos causará que el sistema rehaga todos sus cálculos de riesgo!<BR><b>¿Está seguro que desea hacer esto?</b>',
'st_remove_classification_error'=>'Sus cambios han sigo guardados. Sin embargo,no es posible eliminar las siguientes clasificaciones, porque están siendo usadas por uno o más items: <b>%not_deletable%</b>.',
'st_risk_parameters_weight_edit_confirm'=>'Usted está cambiando datos sensibles.Cuando usted cambia el peso de los parámetros, los valores de todos los riesgos y activos del sistema serán cambiados en consecuencia. ¿Usted desea confirmar los cambios?',
'tt_remove_classification_error'=>'Error cuando se eliminaba la Clasificación',
'tt_save_parameters_names'=>'Guardar Nombre de Parámetros',
'tt_save_parameters_values'=>'Guardar Valores de Parámetros',
'tt_sensitive_data'=>'Dato Sensible',

/* './packages/admin/nav_customization.xml' */

'lb_settings_cl'=>'Personalización:',
'si_aceptable_risk_limits'=>'Niveles Aceptable de Riesgos',
'si_control_cost_names'=>'Parámetros de Estimación de Costos',
'si_control_revision_and_test'=>'Revisión de Control y Prueba',
'si_financial_impact_parameters'=>'Parámetros de Impacto Financiero',
'si_impact_and_importance_dimensions'=>'Parámetros de estimación de riesgo',
'si_non_conformity_types_parametrization'=>'Tipos de parámetros de incumplimiento',
'si_parametrization_asset'=>'Etiqueta de Estimación de Importancia de Activo',
'si_parametrization_control'=>'Etiqueta para los Criterios de Reducción por Controles',
'si_parametrization_risk'=>'Etiqueta de Impacto y Estimación de Probabilidad',
'si_priority_and_type_parametrization'=>'Parametrización de Tipo y Prioridad',
'si_report_classification'=>'Informe de Clasificación (1813)',
'si_risk_parameters_weight'=>'Peso de Parámetros de Riesgo',
'si_special_users'=>'Usuarios Especiales',
'si_system_classification'=>'Sistema de Clasificación',
'si_system_currency'=>'Moneda',
'si_system_features'=>'Características del Sistema',
'si_system_interface_param'=>'Parámetros de la interfaz',
'si_system_rm_parameters_quantity'=>'Definición de la Matriz de Riesgo',
'wn_changes_successfully_after_login'=>'Cambios registrados y están disponibles en el siguiente sistema de inicio de sesión.',
'wn_updating_risk_values'=>'Cambios guardados exitosamente. Espere miestras los valores de riesgos son calculados.',
'wn_values_successfully_changed'=>'Valores existosamente actualizados.',

/* './packages/admin/nav_my_account.php' */

'st_activation_date'=>'Fecha de Activación',
'st_cancel_service'=>'Cancelación de Servicio',
'st_cancel_service_email_message'=>'Una solicitud de cancelación de servicio ha sido hecha por el cliente \'%client%\'.',
'st_client_name'=>'Nombre del Cliente',
'st_confirm_cancel_service_message'=>'¿Está seguro que quiere cancelar el servicio?',
'st_deactivate_account_text'=>'Para desactivar su cuenta, haga click en el botón de abajo.<br/> Su base de datos será mantenida por 30 días. Luego, todos los datos serán totalmente eliminados. Para recibir esta base de datos, contáctenos en %email%',
'st_expiracy_date'=>'Fecha de Expiración',
'st_expiracy_date_never'=>'Nunca',
'st_license_type'=>'Tipo de Licencia',
'st_max_simult_users'=>'Cantidad Máxima de Usuarios Simultáneos',
'st_number_of_users'=>'Número de Usuarios',
'tt_cancel_service'=>'Cancelación de Servicio',
'tt_confirm_cancel_service'=>'Cancelación de Servicio',
'tt_deactivate_account'=>'Desactivar Cuenta',

/* './packages/admin/nav_my_account.xml' */

'lb_email_cl'=>'E-mail:',
'st_cancel_service_bl'=>'<b>Cancelación de Servicio</b>',
'st_change_email_bl'=>'<b>Cambio de email administrativo</b>',
'st_deactivate_account_bl'=>'<b>Desactivar Cuenta</b>',
'st_license_info_bl'=>'<b>Información de Licencia:</b>',
'st_upgrade_license_bl'=>'<b>Actualización de Licencia</b>',
'st_upgrade_license_message'=>'"Por favor, active su paquete de usuarios usando los códigos de activación enviados por e-mail. Ponga los códigos encima, y presione ""Activar"". ¡Este será enviado instantáneamente!"',
'vb_activate'=>'Activar',
'vb_cancel_service'=>'Cancelar Servicio',
'vb_change'=>'Cambio',
'vb_deactivate_account'=>'Desactivar Mi Cuenta',
'vb_upgrade'=>'Actualización',
'wn_email_changed'=>'email fue cambiado exitosamente',
'wn_invalid_activation_code'=>'Algunos códigos no son válidos. Por favor, contacte %email%.',
'wn_successful_activation'=>'Licencia actualizada existosamente.',

/* './packages/admin/nav_profile.php' */

'st_profile_remove_message'=>'Está seguro que desea eliminar el Perfil <b>%profile_name%</b>?',
'tt_remove_profile'=>'Eliminar Perfil',

/* './packages/admin/nav_profile.xml' */

'mi_visualize'=>'Ver',
'tt_profiles_bl'=>'<b>Perfiles</b>',

/* './packages/admin/nav_system_configuration.php' */
'si_ad_setup'=>'Active Directory',

/* './packages/admin/nav_system_configuration_on_site.xml' */

'lb_system_settings_cl'=>'Configuraciones del Sistema',
'si_delete_cascade'=>'Borrar Cascada',
'si_password_policy'=>'Política de Contraseñas',
'si_timezone_setup'=>'Seleccionar la zona horaria',
'wn_changes_successfully_saved'=>'Los cambios fueron guardados exitosamente.',

/* './packages/admin/nav_trash.php' */

'st_all_items_remove_confirm'=>'Todos los artículos en la papelera de reciclaje se eliminará definitivamente! ¿Desea continuar?',
'st_items_restore_confirm'=>'Desea recuperar los items seleccionados?',
'st_not_excluded_users_and_profile_message'=>'Los usuarios seleccionados no pueden ser eliminados porque están relacionados con items existentes en la papelera de reciclaje. <br/> Los perfiles seleccionados no pueden ser eliminados, ya que están relacionados con los usuarios existentes en la papelera de reciclaje que no fueron seleccionados o no pudieron ser eliminados.',
'st_not_restored_documents_message'=>'¡Los documentos seleccionados no pueden ser restaurados porque están vinculados a uno o más items que todavía están eliminados!',
'st_not_restored_items_message'=>'¡Los items seleccionados no pueden ser restaurados porque están vinculados a uno o más usuarios que todavía están eliminados!',
'st_not_restored_users_message'=>'Los Usuarios seleccionados no pueden ser eliminados porque ellos están vinculados a items existentes en la papelera de reciclaje.',
'st_not_selected_items_remove_confirm'=>'Los artículos que no fueron seleccionados y son los subtemas o asociados a los elementos seleccionados se quitarán. ¿Desea continuar?',
'tt_all_items_permanent_removal'=>'Borrado permanente de todos los items',
'tt_document_not_restored'=>'Documento no recuperado',
'tt_items_not_restored'=>'Items no recuperados',
'tt_items_permanent_removal'=>'Borrado permanente de items',
'tt_items_restoral'=>'Recuperación de Items',
'tt_profile_not_deleted'=>'Los perfiles no fueron eliminados',
'tt_users_and_profile_not_deleted'=>'Usuarios y perfiles no fueron eliminados',
'tt_users_not_deleted'=>'El Usuario no fue borrado',

/* './packages/admin/nav_trash.xml' */

'tt_bin_bl'=>'<b>Papelera de Reciclaje</b>',
'vb_remove_all'=>'Eliminar Todo',
'wn_elements_removed_successfully'=>'Item(s) eliminados exitosamente.',
'wn_elements_restaured_successfully'=>'Item(s) eliminados exitosamente.',

/* './packages/admin/nav_users.php' */

'em_alerts_and_tasks_transfer'=>'Tranferencia de Tareas y Alertas',
'st_choose_user_message'=>'En la próxima pantalla, elija un usuario que recibirá las tareas y advertencias pendientes de este usuario.',
'st_reset_all_users_passwords_confirm'=>'¿Está seguro que desea reiniciar las contraseñas de <b> todos los usuarios</b> y enviar una nueva contraseña a sus emails?',
'st_user_remove_confirm'=>'¿Está seguro de que desea borrar el Usuario <b>%user_name%</b>?',
'tt_choose_user_warning'=>'Advertencia de Elección del Usuario',
'tt_remove_user'=>'Eliminar Usuario',
'tt_reset_all_users_password'=>'Reiniciar Contraseñas de Todos los Usuarios',

/* './packages/admin/nav_users.xml' */

'gc_email'=>'E-mail',
'gc_profile'=>'Perfil',
'mi_block'=>'Bloquear',
'mi_unblock'=>'Desbloquear',
'tt_users_bl'=>'<b>Usuarios</b>',
'vb_reset_passwords'=>'Reiniciar Contraseña',
'vb_import_ad_users'=>'Importación de usuarios de AD',

/* './packages/admin/popup_associate_elements.xml' */

'lb_associate_to_cl'=>'Asociar a:',
'tt_items_association'=>'Asociación de Items',
'vb_cancel_all'=>'Cancelar Todo',

/* './packages/admin/popup_configure_authentication.php' */

'st_connection_failed'=>'Conexión Fallida.',
'st_database_structure_wrong'=>'La base de datos no tiene la estructura esperada.',
'st_successful_connection'=>'Conexión Exitosa.',

/* './packages/admin/popup_configure_authentication.xml' */

'lb_databse_bl_cl'=>'<b>Base de Datos:</b>',
'lb_host_bl_cl'=>'<b>Equipo:</b>',
'lb_password_bl_cl'=>'<b>Contraseña:</b>',
'st_postgres_authentication_data_needed'=>'El dato a continuación es requerido para permitir al sistema acceder a la base de datos que contiene información sobre el calendario postgres. (1898)',
'tt_setup_authentication'=>'Configurar Autenticación',
'vb_test'=>'Prueba',

/* './packages/admin/popup_deactivate.php' */

'em_deactivate_account'=>'Desactivación de Cuenta',
'em_deactivate_account_email_message'=>'El Cliente \'%client%\' has solicitado la desactivación de cuenta.<br><b>Motivo:</b><br>%deactivation_reason%',

/* './packages/admin/popup_deactivate.xml' */

'lb_deactivation_reason_cl'=>'Por favor, diganos por que usted está desactivando:',
'st_confirm_deactivate_account_message'=>'Está seguro de que quiere desactivar su cuenta?',
'tt_confirm_deactivate_account'=>'Desactivar Cuenta',

/* './packages/admin/popup_no_access.xml' */

'st_user_wo_system_access'=>'Usted no tiene permiso para acceder al sistema. Presione <b>Ok</b> o cierre esta ventana para salir.',
'tt_user_wo_system_access'=>'Usuario sin permiso para acceder al sistema',

/* './packages/admin/popup_profile_edit.php' */

'tt_edit_profile'=>'Editar Perfil',
'tt_insert_profile'=>'Agregar Perfil',
'tt_visualizae_profile'=>'Mostrar Perfil',

/* './packages/admin/popup_profile_edit.xml' */

'st_profile_warning'=>'* Este perfil no puede ser editado',

/* './packages/admin/popup_restore_name_conflict.xml' */

'st_name_conflicts_message'=>'Ha sido detectado que uno o más items seleccionados presentaron configto de nombre, significa que hay un item activo en el sistema que tiene el mismo tipo y nombre. ¿Qué items desea restaurar?',
'tt_name_conflicts_found'=>'Configtos de Nombre Encontrados',

/* './packages/admin/popup_restore_structure_and_name_conflict.xml' */

'rb_all_except_name_conflict'=>'All except the ones with name conflict * (1917)',
'rb_all_except_structure_conflict'=>'All except the ones with structural conflict (1920)',
'st_structure_name_conflicts_message'=>'Se ha detectado que hay un confligto de estructura y nombre con uno o más items seleccionados. Un confligto de estructura ocurre cuando hay items no seleccionados necesarios para hacer la restauración de los items seleccionados. Un confligto de nombre ocurre cuando hay un item activo del sistema con el mismo tipo y el mismo nombre de un item que será restaurado. ¿Qué items desea usted restaurar?',
'tt_structure_and_name_conflicts_found'=>'Configtos de Estructura y Nombre Encontrados',
'wn_not_restoring_name_conflict'=>'* Items no restaurados con nombre en confligto posiblemente causen confligtos estructurales. (1915)',

/* './packages/admin/popup_restore_structure_conflict.xml' */

'cb_associate_structure_conflicted_items_individually'=>'Asociar individualmente items con confligtos estructurales a items existentes en el sistema.',
'rb_all'=>'Todos',
'rb_only_not_conflicting'=>'Solo las que no presentarón confligtos (1912)',
'st_structure_conflicts_message'=>'Ha sido detectado que uno o más items seleccionados presentaron confligtos de estructura, lo que significa que para restaurar estos, usted debería tener que restaurar una estructura de items no seleccionados. ¿Qué items desea ustede restaurar? (1923)',
'tt_structure_conflicts_found'=>'Configtos de Estructura Encontrados',
'vb_restore'=>'Recuperar',

/* './packages/admin/popup_upgrade_license.xml' */

'lb_activation_code_cl'=>'Código de activación:',
'tt_upgrade_license'=>'Actualización de Licencia',

/* './packages/admin/popup_user_edit.php' */

'em_new_password'=>'Nueva Contraseña',
'em_new_user'=>'Bienvenido a Real ISMS',
'em_new_user_footer_bl'=>'Para más información, contacte el oficial de seguridad.',
'lb_password_cl'=>'Contraseña:',
'tt_profile_edit_error'=>'Error while changing the profile',
'tt_reset_user_password'=>'Reiniciar Contraseña de Usuario',
'tt_user_adding'=>'Alta de Usuario',
'tt_user_editing'=>'Edición de Usuario',
'tt_user_insert_error'=>'User limit reached',
'wn_users_limit_reached'=>'The number of licensed users has been reached. From now on only users with the <b>Document Reader</b> profile (users that only have permission to read documents) can be used. Click <a target="_blank" href=%link%>here</a> in order to buy more licenses.',

/* './packages/admin/popup_user_edit.xml' */

'cb_change_password_on_login'=>'Cambiar contraseña en el próximo ingreso',
'cb_acess_old_documents'=>'Permitir la lectura de documentos ya creados',
'lb_confirmation_cl'=>'Confirmación:',
'lb_email_bl_cl'=>'<b>E-mail:</b>',
'lb_language_bl_cl'=>'<b>Lenguaje:</b>',
'lb_login_bl_cl'=>'<b>Usuario:</b>',
'lb_new_password_cl'=>'Nueva Contraseña:',
'lb_profile_bl_cl'=>'<b>Perfil:</b>',
'st_language_bl_cl'=>'<b>Lenguaje:</b>',
'vb_reset_password'=>'Reiniciar contraseña',
'wn_different_passwords'=>'Contraseñas Diferentes',
'wn_existing_email'=>'Ya existe el email',
'wn_existing_login'=>'Ya existe el usuario',
'wn_existing_trash_email'=>'Ya existe un usuario en la basura con el mismo correo electrónico. Para utilizar estos datos debe eliminar o restaurar el usuario que está en la basura.',
'wn_existing_trash_login'=>'Ya existe un usuario en la basura con el mismo nombre de usuario. Para utilizar estos datos debe eliminar o restaurar el usuario que está en la basura.',
'wn_invalid_email'=>'E-mail no válido',
'wn_password_changed'=>'Constraseña cambiada',
'wn_weak_password'=>'Su contraseña no cumple los requisitos de la política de contraseñas. Por favor, elija otra.',
'wn_wrong_current_password'=>'Contraseña equivocada',

/* './packages/admin/report/popup_report_audit_alert.php' */

'rs_alerts_audit'=>'Alertas de Auditoría',

/* './packages/admin/report/popup_report_audit_log.php' */

'rs_systems_log_audit'=>'Registro de Sistema de Auditoría',

/* './packages/admin/report/popup_report_audit_task.php' */

'rs_tasks_audit'=>'Tareas de Auditoría',

/* './packages/admin/report/popup_report_audit_task.xml' */

'rs_accomplishment'=>'Realización',
'rs_creation'=>'Creación',
'rs_creator'=>'Creador',
'rs_receiver'=>'Receptor',

/* './packages/admin/system_configuration_delete_cascade.xml' */

'cb_delete_cascade'=>'Borrar Cascada',
'st_delete_cascade'=>'<b>Borrar Cascada</b>',
'st_system_configuration_delete_cascade_information'=>'Determina si una eliminación de item causa la eliminación automática de subitems.<br/>Si \'Borrar Cascada\' está deshabilitado, será imposible eliminar los items que tengan subitems.<br/><br/>¿Desea usar \'Borrar Casada\' en el sistema?',

/* './packages/admin/system_configuration_email_setup.xml' */

'si_email_ssl'=>'SSL',
'si_email_ssl3'=>'SSLv3',
'si_email_tls'=>'TLS',

/* './packages/admin/system_configuration_password_policy.xml' */

'cb_case_characters'=>'Caracteres en Minúscula/Mayúscula',
'cb_numeric_characters_cl'=>'Caracteres Numéricos:',
'cb_special_characters_cl'=>'Caracteres Especiales:',
'lb_days_before_warning'=>'Notificar expiración de contraseñas:',
'lb_label_block_limit_cl'=>'Límite de Bloqueo:',
'lb_minimum_characters_cl'=>'Longitud mínima',
'lb_password_history_apply_cl'=>'Aplicar historial de contraseñas:',
'lb_password_max_lifetime_cl'=>'Máximo tiempo de vida de contraseña:',
'st_configuration_password_policy_information'=>'¿Cual es la política de contraseñas que desea usar en el sistema?',
'st_days_before'=>'día antes.',
'st_default_configuration'=>'Reiniciar a la configuración por defecto',
'st_invalid_logon'=>'intentos de ingreso no válidos',
'st_memorized_passwords'=>'contraseñas memorizadas.',
'st_password_policy_bl_cl'=>'<b>Política de contraseñas:</b>',
'wn_min_char_warning'=>'La longitud mínima debe ser igual o más larga que el número de caracteres especiales más 2 (al menos un carácter en minúsculas y uno en mayúscula), más los caracteres numéricos.<br/><br/>Obs: Sólo las opciones seleccionadas son tenidas en cuenta.',

/* './packages/admin/system_configuration_server_setup.xml' */


/* './packages/admin/system_configuration_timezone_setup.xml' */

'cb_timezone_international_time'=>'Formato de hora:',
'cb_timezone_international_time_12_hour'=>'12:00 PM',
'cb_timezone_international_time_24_hour'=>'23:59',
'cb_timezone_international_time_lang_default'=>'Idioma predeterminado',
'cb_timezone_setup'=>'Seleccionar uso horario',
'si_timezone_-1'=>'GMT -1',
'si_timezone_-10'=>'GMT -10',
'si_timezone_-11'=>'GMT -11',
'si_timezone_-12'=>'GMT -12',
'si_timezone_-2'=>'GMT -2',
'si_timezone_-3'=>'GMT -3',
'si_timezone_-4'=>'GMT -4',
'si_timezone_-5'=>'GMT -5',
'si_timezone_-6'=>'GMT -6',
'si_timezone_-7'=>'GMT -7',
'si_timezone_-8'=>'GMT -8',
'si_timezone_-9'=>'GMT -9',
'si_timezone_+1'=>'GMT +1',
'si_timezone_+10'=>'GMT +10',
'si_timezone_+11'=>'GMT +11',
'si_timezone_+12'=>'GMT +12',
'si_timezone_+2'=>'GMT +2',
'si_timezone_+3'=>'GMT +3',
'si_timezone_+4'=>'GMT +4',
'si_timezone_+5'=>'GMT +5',
'si_timezone_+6'=>'GMT +6',
'si_timezone_+7'=>'GMT +7',
'si_timezone_+8'=>'GMT +8',
'si_timezone_+9'=>'GMT +9',
'si_timezone_0'=>'GMT',
'st_system_configuration_timezone_setup_information'=>'Determina cual será el uso horario usado en el sistema',
'st_timezone_setup'=>'<b>Seleccionar la zona horaria</b>',

/* ./packages/admin/system_configuration_ad_setup.xml */
'st_ad_config_bl_cl'=>'<b>Configuración Active Directory:</b>',
'st_configuration_ad_config_information'=>'Establecer los parámetros que el sistema va a utilizar para la gestión de usuarios de Active Directory.',
'lb_ad_host_cl'=>'Dirección AD Server:',
'lb_ad_port_cl'=>'Porta:',
'lb_ad_ssl'=>'Usar SSL?',
'lb_no'=>'No',
'lb_yes'=>'Sí',
'lb_email_user_cl'=>'Search Base DN:',
'lb_ad_binding_dn_cl'=>'Binding DN:',
'lb_ad_user_info'=>'<b>Escriba a continuación el nombre de usuario y contraseña que se utiliza para obtener los datos en AD.</b>',
'lb_ad_search_user_cl'=>'Login:',
'lb_email_password_cl'=>'Contraseña:',
'vb_test_and_save'=>'Guardar',
'wn_ad_test_error'=>'Algunos parámetros son correctos. No hay datos se ha guardado, por favor corrija los datos.',

/* './packages/admin/tab_audit.xml' */

'ti_alerts'=>'Alertas',
'ti_log'=>'Registro',
'ti_tasks'=>'Tareas',

/* './packages/admin/tab_main.php' */

'st_survey_link'=>'http://www.surveymonkey.com/s.aspx?sm=F9BFTdst8Ginn7PdgNGxFA_3d_3d',
'to_ip_cl'=>'IP:',
'to_last_login_cl'=>'Último ingreso:',

/* './packages/admin/tab_main.xml' */

'lb_feedback'=>'Retroalimentación',
'st_about_isms_bl'=>'<b>Acerca de</b>',
'st_buy_now_bl'=>'<b>Comprar ahora</b>',
'st_pipe_bl'=>'<b>|</b>',
'st_preferences_bl'=>'<b>Preferencias</b>',
'st_satisfaction_survey_bl'=>'<b>Encuesta de Experiencia de Usuario</b>',
'st_send_us_feedback_cl'=>'Dejar una retroalimentación:',
'st_software_license_bl'=>'<b>Licencia</b>',
'ti_audit'=>'Auditoría',
'ti_bin'=>'Papelera de Reciclaje',
'ti_customization'=>'Personalización',
'ti_my_account'=>'Mi Cuenta',
'ti_profiles'=>'Perfiles',
'ti_system_settings'=>'Configuraciones del Sistema',
'ti_users'=>'Usuarios',
'vb_exit'=>'Salir',
'vb_feedback'=>'Retroalimentación',
'vb_help'=>'Ayuda',
'vb_user_view'=>'Vista de usuario',
'st_clean_database' => 'Limpiar base de datos',
'st_warning_asaas_part_1' => '<b>Su ensayo termina en </b>',
'st_warning_asaas_part_2'=> '<b>días, compra ahora</b>',
'st_asaas_buy' => 'Comprar',

/* './packages/improvement/nav_action_plan.php' */

'mx_corrective'=>'Correctivo',
'mx_preventive'=>'Preventivo',
'st_ap_remove_message'=>'Está seguro que desea eliminar el Plan de Acción <b>%name%</b>? (2003)',
'tt_ap_remove'=>'Eliminar Plan de Acción',
'tt_nc_filter_grid_action_plan'=>'(filtrado por no conformidad \'<b>%nc_name%</b>\')',

/* './packages/improvement/nav_action_plan.xml' */

'gc_actiontype'=>'Tipo de Acción',
'gc_ap_cns'=>'No Conformidades',
'gc_dateconclusion'=>'Fecha de Finalización',
'gc_datedeadline'=>'Fecha Límite',
'gc_isefficient'=>'Eficiente',
'gc_responsible_id'=>'Responsable',
'mi_associate_nonconformity'=>'Asociar la no conformidad',
'mi_data_to_meassure'=>'Definir Medida',
'mi_meassure'=>'Medida',
'tt_ap_bl'=>'<b>Plan de Acción</b>',

/* './packages/improvement/nav_disciplinary_process.xml' */

'gc_action_taken'=>'Acción Ejecutada',
'mi_register_action'=>'Registro de Acción',

/* './packages/improvement/nav_incident.php' */

'mx_direct'=>'Directo',
'mx_indirect'=>'Indirecto',
'mx_no_category'=>'Sin Categoría',
'st_incident_remove_confirm'=>'Está seguro que desea eliminar el incidente <b>%incident_name%</b>?',
'tt_incident_remove_confirm'=>'Eliminar Incidente',

/* './packages/improvement/nav_incident.xml' */

'gc_incident_date_limit'=>'Estimado',
'gc_loss_type'=>'Tipo de Pérdida',
'mi_disciplinary_process'=>'Proceso Disciplinario',
'mi_finish'=>'Finalizar',
'mi_immediate_disposal_approval'=>'Aprobación de eliminación inmediata',
'mi_solution_approval'=>'Aprobación de solución',
'mi_view_solution'=>'Mostrar Solución',
'mi_view_evidence'=>'Mostrar Evidencia',
'mi_view_immediate_disposal'=>'Mostrar Disposición inmediata',
'mi_view_loss_type'=>'Mostrar Tipo de Pérdida',
'tt_incident'=>'<b>Incidente</b>',

/* './packages/improvement/nav_non_conformity.php' */

'gc_control_security'=>'Control de Seguridad',
'gc_external_audit'=>'Auditoría Externa',
'gc_internal_audit'=>'Auditoría Interna',
'gc_no'=>'No',
'gc_yes'=>'Yes',
'st_nc_remove_message'=>'¿Está seguro que desea eliminar la no conformidad <b>%name%</b>?',
'tt_ap_filter_grid_non_conformity'=>'(filtrado por plan de acción \'<b>%ap_name%</b>\')',
'tt_nc_remove'=>'Eliminar No Conformidad',

/* './packages/improvement/nav_non_conformity.xml' */

'gc_action_plans'=>'Plan de Acción',
'gc_sender'=>'Emisor',
'gc_sender_bl'=>'<b>Emisor:</b>',
'gc_state'=>'Estado',
'mi_action_plan'=>'Asociar Plan de Acción',
'mi_send_to_ap_pendant'=>'Enviar a Aceptación de Plan de Acción',
'mi_send_to_conclusion'=>'Enviar a Finalizar',
'mi_send_to_responsible'=>'Enviar a Responsable',
'tt_nc_bl'=>'<b>No conformidad</b>',

/* './packages/improvement/nav_occurrence.php' */

'st_occurrence_remove'=>'¿Está seguro que desea eliminar la ocurrencia <b>%occurrence_text%</b>?',
'tt_occurrence_remove'=>'Remover Ocurrencia',

/* './packages/improvement/nav_occurrence.xml' */

'gc_incident'=>'Incidente',
'mi_incident'=>'Asociar Incidente',
'tt_occurrences'=>'<b>Ocurrencias</b>',

/* './packages/improvement/nav_report.php' */

'mx_report_action_plan_by_process'=>'Plan de Acción por Proceso',
'mx_report_action_plan_by_user'=>'Plan de Acción por Usuario',
'mx_report_ap_without_doc'=>'Planes de Acción sin Documentos',
'mx_report_ci_pendant_tasks'=>'Tareas pendientes en Mejora Continua',
'mx_report_control_nonconformity_by_process'=>'No Conformidades de Control por Procesos',
'mx_report_elements_affected_by_incident'=>'Items afectados por Incidentes',
'mx_report_financial_impact_by_incident'=>'Impacto Financiero por Incidente',
'mx_report_incident_accompaniment'=>'Seguimiento de Incidente',
'mx_report_incident_by_responsible'=>'Recolección de Evidencia',
'mx_report_incident_control_revision'=>'Revisión de Control causada por Incidentes',
'mx_report_incident_without_risk'=>'Incidentes sin Riesgos',
'mx_report_incidents_without_occurrence'=>'Incidentes sin Ocurrencias',
'mx_report_nc_without_ap'=>'No Conformidades sin Plan de Acción',
'mx_report_nonconformity_accompaniment'=>'Seguimiento de No Conformidad',
'mx_report_nonconformity_by_process'=>'No Conformidades por Procesos',
'mx_report_occurrence_accompaniment'=>'Seguimiento de Ocurrencia',
'mx_report_occurrence_by_incident'=>'Ocurrencias por Incidente',
'mx_report_risk_auto_probability_value'=>'Cálculo Automático de Probabilidad de Riesgo',
'mx_report_user_by_incident'=>'Proceso Disciplinario',

/* './packages/improvement/nav_report.xml' */

'lb_conclusion_cl'=>'Finalizando:',
'lb_incident_order_by'=>'Ordenación',
'lb_show_cl'=>'Mostrar:',
'si_capability_all'=>'Todas',
'si_capability_no'=>'No',
'si_capability_yes'=>'Sí',
'si_ci_manager'=>'Informes de Mejora Contínua',
'si_incident_order_by_category'=>'Categoría',
'si_incident_order_by_date'=>'Data del incidente',
'si_incident_order_by_responsible'=>'Responsable',
'si_incident_report'=>'Informe de Incidentes',
'si_nonconformity_report'=>'Informes de No Conformidades',
'st_action_plan_filter_bl'=>'<b>Filtro por Plan de Acción</b>',
'st_ap_filter'=>'<b>Filtro Plano de Acción</ b>',
'st_elements_filter_bl'=>'<b>Filtro de Items</b>',
'st_incident_filter_bl'=>'<b>Filtro de Incidente</b>',
'st_nonconformity_filter_bl'=>'<b>Filtro de No Conformidad</b>',
'st_occurrence_filter_bl'=>'<b>Filtro de Ocurrencia</b>',
'st_status'=>'Estado:',
'wn_at_least_one_element_must_be_selected'=>'Al menos un items debe ser seleccionado.',

/* './packages/improvement/popup_action_plan_associate.xml' */

'tt_action_plan_association'=>'Asocación con Plan de Acción',
'tt_current_action_plans_bl'=>'<b>Planes de Acción Actuales</b>',

/* './packages/improvement/popup_action_plan_edit.xml' */

'lb_ap_actionplan_bl_cl'=>'<b>Acción:</b>',
'lb_ap_actiontype_bl_cl'=>'<b>Tipo de Acción:</b>',
'lb_ap_dateconclusion_cl'=>'Fecha de Finalización:',
'lb_ap_datedeadline_bl_cl'=>'<b>Fecha Límite:</b>',
'lb_ap_name_bl_cl'=>'<b>Nombre:</b>',
'lb_ap_responsible_id_bl_cl'=>'<b>Responsable:</b>',
'rb_corrective'=>'Correctiva',
'rb_preventive'=>'Preventiva',
'tt_ap_edit'=>'Edición de Plan de Acción',
'vb_finish'=>'Finalizar',
'wn_deadline'=>'La fecha límite debe ser posterior o igual a la fecha actual.',

/* './packages/improvement/popup_action_plan_finish_confirm.xml' */

'lb_ap_days_before_bl'=>'<b>Notificar antes</b>',
'lb_date_efficiency_revision_bl_cl'=>'<b>Fecha de Revisión de Eficacia:</b>',
'st_action_plan_finish_confirm_message'=>'Quiere usted finalizar el Plan de Acción \'%name%\'?',
'tt_action_plan_finish'=>'Finalizar Plan de Acción',
'wn_revision_date_after_conclusion_date'=>'¡La fecha de revisión de eficacia debe ser posterior o la misma que la fecha de finalización!',

/* './packages/improvement/popup_action_plan_revision_task.xml' */

'lb_action_cl'=>'Acción:',
'st_action_plan_efficient_bl'=>'<b>¿Fue eficiente el Plan de Acción?</b>',
'st_data_revision_bl_cl'=>'<b>Fecha de Revisión:</b>',
'tt_action_plan_efficiency_revision'=>'Revisión de Eficiencia del Plan de Acción',

/* './packages/improvement/popup_disciplinary_process_action.php' */

'st_denied_permission_to_edit_action_taken'=>'Usted no tiene permiso para registrar una acción en el proceso disciplinario.',

/* './packages/improvement/popup_disciplinary_process_action.xml' */

'lb_action_bl_cl'=>'<b>Acción:</b>',
'st_incident_cl'=>'<b>Incidente:</b>',
'st_user_cl'=>'<b>Usuario:</b>',

/* './packages/improvement/popup_disciplinary_process_edit.php' */

'em_disciplinary_process_creation'=>'Creación de Proceso Disciplinario',
'em_disciplinary_process_creation_footer'=>'Para más información, contacte el oficial de seguridad.',
'st_incident_user_remove'=>'¿Está seguro que desea eliminar el usuario <b>%user_name%</b> del proceso disciplinario del incidente <b>%incident_name%</b>?',
'tt_incident_user_remove'=>'Eliminar Usuario',

/* './packages/improvement/popup_disciplinary_process_edit.xml' */

'gc_observation_involvement'=>'Observación / Mejora',
'gc_user'=>'Usuario',
'lb_incident_cl'=>'Incidente:',
'lb_observation_involvement_cl'=>'Observación / Mejora',
'lb_user_bl_cl'=>'<b>Usuario:</b>',
'tt_disciplinary_process'=>'<b>Proceso Disciplinario</b>',
'wn_email_dp_sent'=>'Un e-mail ya fue enviado al Gestor de Proceso Disciplinario.',

/* './packages/improvement/popup_evidence_requirement.xml' */

'lb_comments_cl'=>'Comentarios:',
'tt_evidence_requirement'=>'Recolección de Evidencia',
'wn_evidence_requirement_editing'=>'Obs. Al editar el comentario anterior, una descripción con el comentario contenido será enviado al Administrador de Recolección de evidencia.',

/* './packages/improvement/popup_financial_impact_edit.xml' */

'st_total'=>'Total',
'tt_financial_impact_parametrization'=>'Estimación de Impacto Financiero',

/* './packages/improvement/popup_incident_accounts_plan.xml' */

'tt_incident_accounts_plan'=>'Plan de Contabilidad',

/* './packages/improvement/popup_incident_affected_product_service.xml' */

'tt_affected_product_service'=>'Producto / Servicio Afectado',

/* './packages/improvement/popup_incident_approval_task.xml' */

'tt_assets_per_incident'=>'<b>Activos Relacionados al Incidente</b>',

/* './packages/improvement/popup_incident_disposal.xml' */

'tt_immediate_disposal'=>'Eliminación Inmediata',

/* './packages/improvement/popup_incident_edit.xml' */

'lb_accounts_plan_cl'=>'Plan de Contabilidad:',
'lb_affected_product_service_cl'=>'Producto / Servicio Afectado',
'lb_date_bl_cl'=>'<b>Fecha:</b>',
'lb_date_limit_bl_cl'=>'<b>Finalización Estimada:</b>',
'lb_evidences_cl'=>'Evidencias:',
'lb_hour_bl_cl'=>'<b>Hora:</b>',
'lb_immediate_disposal_cl'=>'Eliminación Inmediata:',
'lb_loss_type_bl_cl'=>'<b>Tipo de Pérdida</b>',
'lb_occurrences_bl_cl'=>'<b>Ocurrencias:</b>',
'lb_occurrences_cl'=>'Ocurrencias:',
'lb_solution_cl'=>'Solución:',
'si_direct'=>'Directo',
'si_indirect'=>'Indirecto',
'st_collect_evidence'=>'¿Recolectar Evidencias?',
'st_incident_date_finish'=>'Finalización Estimada:',
'st_incident_date_finish_bl'=>'<b>Finalización Estimada:</b>',
'tt_incident_edit'=>'Edición de Incidente',
'vb_financial_impact'=>'Impacto Financiero',
'vb_observations'=>'Observaciones',
'wn_future_date_not_allowed'=>'No está permitida fecha / hora futura.',
'wn_prevision_finish'=>'La fecha estimada de finalización debe ser igual o posterior que la fecha actual.',

/* './packages/improvement/popup_incident_evidence.xml' */

'tt_evidence'=>'Evidencia',
'tt_loss_type'=>'Tipo de Pérdida',

/* './packages/improvement/popup_incident_process_association_edit.php' */

'st_save_suggested_incident_processes'=>'¿Usted desea vincular los procesos sugeridos al incidente<b>%context_name% </b>?',
'tt_save_suggested_incident_processes'=>'Vincular Procesos Sugeridos',

/* './packages/improvement/popup_incident_process_association_edit.xml' */

'cb_search_only_suggested_process'=>'Buscar solo procesos sugeridos',
'tt_incident_process_association'=>'Asociación de Proceso a Incidente',

/* './packages/improvement/popup_incident_risk_asset_association_edit.xml' */

'lb_assets_cl'=>'Activos:',
'lb_risks_cl'=>'Riesgos:',
'st_risk_asset_association_message'=>'Cuando selecciona un activo, por favor seleccione el riesgo que usted desea que esté asociado a este. Es posible asociar más de un riesgo seleccionándolos con la tecla Crtl.',
'tt_risk_asset_association'=>'Asociación de Riesgos a Activos',

/* './packages/improvement/popup_incident_risk_association_edit.php' */

'em_incident_risk_parametrization'=>'Estimación de Riesgo asociado a Incidente',
'em_incident_risk_parametrization_footer'=>'Para más información, contacte el oficial de seguridad.',
'mx_non_parameterized_risk'=>'Riesgo No Estimado',
'to_dependencies_bl_cl'=>'<b>Dependencias:</b> <br><br>',
'to_dependents_bl_cl'=>'<b>Dependiente:</b> <br><br>',

/* './packages/improvement/popup_incident_risk_association_edit.xml' */

'tt_associated_controls_and_assets'=>'<b>Controles y Activos Vinculados</b>',
'tt_incident_risk_association'=>'Asociación Incidente-Riesgo',
'tt_risks_refferring_controls_and_assets'=>'<b>Riesgos Considerando Controles  y Activos</b>',
'tt_risks_related_selected'=>'<b>Vinculados / Riesgos Seleccionados</b> (2223)',
'vb_insert_risk'=>'Insertar Riesgo',
'vb_risk_incident_parametrization'=>'Estimación Incidente-Riesgo',
'vb_search_assets'=>'Buscar Activos',
'vb_search_controls'=>'Buscar Controles',
'wn_parameterize_incident_risk'=>'Debe estimar primero la asociación Incidente-Riesgo antes de guardar su asociación.',

/* './packages/improvement/popup_incident_risk_association_options.xml' */

'lb_associate_risk_to_assets'=>'¿Usted desea vincular los riesgos actuales con los activos seleccionados? Usted puede elegir cuales riesgos quiere vincular con que activos. (2229)',
'lb_send_alert_to_other_users'=>'¿Desea envíar alertas a los otros usuarios además de al responable de seguridad de los activos y controles vinculados al riesgo %new_risks%?',
'tt_incident_risk_association_options'=>'Opciones a la asociación de riesgos e incidentes',
'vb_continue'=>'Continuar',

/* './packages/improvement/popup_incident_risk_parametrization.xml' */

'tt_incident_risk_association_parametrization'=>'Estimación a la Asociación Incidente-Riesgo',

/* './packages/improvement/popup_incident_search.xml' */

'tt_incident_search'=>'Búsqueda de Incidentes',
'vb_relation'=>'Vincular (2236)',
'wn_no_incident_selected'=>'No fue seleccionado incidente.',

/* './packages/improvement/popup_incident_solution.xml' */

'tt_solution'=>'Solución',

/* './packages/improvement/popup_incident_state_forward.php' */

'st_incident_finish'=>'¿Desea usted finalizar el incidente?',
'st_incident_send_to_app_disposal'=>'¿Desea enviar el incidente a Aprobación de Eliminación Inmediata?',
'st_incident_send_to_app_solution'=>'¿Desea envíar el incidente para Aprobación de Solución?',
'st_incident_send_to_responsible'=>'¿Desea usted enviar el incidente al responsable?',
'tt_incident_finish'=>'Finalizar Incidente',
'tt_incident_send_to_app_disposal'=>'Enviar para Aprobación de Eliminación Inmediata',
'tt_incident_send_to_app_solution'=>'Envíar para Aprobación de Solución',
'tt_incident_send_to_responsible'=>'Enviar a Responsable',

/* './packages/improvement/popup_incident_task_view.php' */

'lb_incident_immediate_disposal_cl'=>'Eliminación Inmediata:',
'lb_incident_solution_cl'=>'Solución:',

/* './packages/improvement/popup_incident_task_view.xml' */

'lb_date_limit_cl'=>'Fecha Límite:',

/* './packages/improvement/popup_new_risk_parameter_approval_task.php' */


/* './packages/improvement/popup_new_risk_parameter_approval_task.xml' */


/* './packages/improvement/popup_non_conformity_approval_task.xml' */

'lb_processes_cl'=>'Procesos:',

/* './packages/improvement/popup_non_conformity_associate.xml' */

'st_name'=>'Nombre:',
'tt_current_non_conformitys_bl'=>'<b>No conformidades Actuales</b>',
'tt_non_conformity_association'=>'Asociación de no conformidades',

/* './packages/improvement/popup_non_conformity_create.xml' */

'lb_date_sent_cl'=>'Fecha de Emisión:',
'lb_sender_cl'=>'Emisor:',
'tt_non_conformity_record'=>'Registro de No Conformidad',

/* './packages/improvement/popup_non_conformity_data_approval_task.php' */

'st_denied_permission_to_execute_task'=>'Usted no tiene permiso para ejecutar la tarea.',

/* './packages/improvement/popup_non_conformity_data_approval_task.xml' */

'lb_ap_responsible_cl'=>'API Resp.: (2271)',
'lb_cause_cl'=>'Causa:',
'lb_inquirer_cl'=>'Solicitante:',
'lb_justificative_cl'=>'Razón:',
'lb_non_conformity_cl'=>'No conformidad:',
'lb_task_cl'=>'Tarea',
'tt_task_visualization'=>'Visualización de Tarea',

/* './packages/improvement/popup_non_conformity_edit.xml' */

'lb_action_plans_cl'=>'Planes de Acción:',
'lb_nc_capability_bl_cl'=>'Potencial',
'lb_nc_cause_cl'=>'Causa',
'lb_nc_classification_cl'=>'Clasificación:',
'lb_nc_datesent_cl'=>'Fecha de Emisión:',
'lb_nc_description_cl'=>'Descripción',
'lb_nc_name_bl_cl'=>'<b>Nombre:</b>',
'lb_nc_responsible_id_cl'=>'Responsable:',
'lb_nc_sender_id_cl'=>'Emisor:',
'lb_processes_bl_cl'=>'<b>Procesos:</b>',
'si_no'=>'No',
'si_yes'=>'Sí',
'tt_nc_edit'=>'Editición de No conformidad',

/* './packages/improvement/popup_non_conformity_revision_task.xml' */

'lb_action_plan_cl'=>'Plan de Acción:',
'lb_ap_efficient_bl'=>'<b>¿El Plan de Acción fue eficiente?</b>',
'rb_no'=>'No',
'rb_yes'=>'Sí',
'tt_non_conformity_efficiency_revision'=>'Revisión de Eficiencia de No conformidades',

/* './packages/improvement/popup_non_conformity_state_forward.php' */

'st_non_conformity_send_to_ap_responsible'=>'¿Desea enviar la No Conformidad para tener estos Planes de  Acción aceptados?',
'st_non_conformity_send_to_approve'=>'¿ Desea enviar la No Conformidad a la Finalización de Planes de Acción?',
'st_non_conformity_send_to_responsible'=>'¿Desea enviarle la No conformidad al responsable?',
'tt_non_conformity_send_to_ap_responsible'=>'Enviar a la Aceptación de Planes de Acción',
'tt_non_conformity_send_to_approve'=>'Enviar a la Finalización de Planes de Acción',
'tt_non_conformity_send_to_responsible'=>'Enviar a Responsable',

/* './packages/improvement/popup_non_conformity_state_forward.xml' */

'vb_no'=>'No',
'vb_yes'=>'Sí',

/* './packages/improvement/popup_occurrence_edit.xml' */

'lb_description_bl_cl'=>'<b>Descripción:</b>',
'st_date_bl'=>'<b>Fecha:</b>',
'st_date_warning'=>'No está permitida una fecha / hora futura',
'st_hour_bl'=>'<b>Hora:</b>',
'tt_register_occurrence'=>'Registrar Ocurrencia',

/* './packages/improvement/popup_occurrences_search.xml' */

'tt_current_occurrences'=>'<b>Ocurrencias Actuales</b>',
'tt_occurrences_search'=>'Búsqueda de Ocurrencias',

/* './packages/improvement/popup_probability_calculation_config.xml' */

'lb_incidents_in_period_cl'=>'Incidentes entre %initial_date% y %final_date%:',
'lb_last_check_cl'=>'Última verificación',
'lb_periodicity_bl_cl'=>'<b>Periodicidad:</b>',
'lb_starting_at'=>'Comenzando por',
'si_days'=>'Día(s)',
'si_months'=>'Mes(es)',
'si_weeks'=>'Semana(s)',
'st_total_incidents_found'=>'Total de incidentes encontrados:',
'tt_incidents_related_to_risk_in_period'=>'<b>Incidentes relacionados a riesgos en el periodo</b>',
'tt_probability_calculation_config'=>'Configuración de Cálculo Automático de Probabilidad',
'tt_probability_level_vs_incident_count'=>'<b>Nivel de Probabilidad<br/><br/><b>X</b><br/><br/><b>Cantidad de Incidentes</b>',
'vb_update'=>'Actualizar',
'wn_probability_config_not_filled'=>'Los intervalos no fueron llenados o fueron cargados con valores inconsistentes',

/* './packages/improvement/popup_process_search.xml' */

'tt_current_processes'=>'<b>Procesos Actuales<b>',
'tt_process_search'=>'Búsqueda de procesos',

/* './packages/improvement/popup_request_compare_impact_risks.xml' */

'popup_request_compare_impact_no'=>'No',
'popup_request_compare_impact_yes'=>'Si',

/* './packages/improvement/popup_risk_create.xml' */

'lb_impact_money_cl'=>'Impacto Financiero:',
'si_select_one_below'=>'Seleccione uno de los items de abajo',
'st_risk_impact'=>'Impacto:',
'tt_insert_risk'=>'Insertar Riesgo',

/* './packages/improvement/report/clock.php' */

'st_clock_wait'=>'<b>Atención:</b> Este informe posiblemente tome unos minutos para ser generado.<br>Por favor, espere.',

/* './packages/improvement/report/popup_report_action_plan_by_process.php' */

'tt_action_plan_by_process'=>'Planes de Acción por Proceso',

/* './packages/improvement/report/popup_report_action_plan_by_user.php' */

'tt_action_plan_by_user'=>'Planes de Acción por Usuario',

/* './packages/improvement/report/popup_report_ap_without_doc.php' */

'rs_ap_without_docs'=>'Planes de Acción sin Documentos',

/* './packages/improvement/report/popup_report_ap_without_doc.xml' */

'rs_ap'=>'Plan de Acción',

/* './packages/improvement/report/popup_report_control_nonconformity_by_process.php' */

'mx_control_nonconformity_by_process'=>'No conformidad de Control por Proceso',

/* './packages/improvement/report/popup_report_disciplinary_process.php' */

'mx_disciplinary_process'=>'Procesos Disciplinarios',

/* './packages/improvement/report/popup_report_disciplinary_process.xml' */

'rs_dp_name'=>'Nombre',

/* './packages/improvement/report/popup_report_elements_affected_by_incident.php' */

'mx_elements_affected_by_incident'=>'Items afectados por Incidentes',

/* './packages/improvement/report/popup_report_elements_affected_by_incident.xml' */

'rs_incident_bl_cl'=>'<b>Incidente:</b>',

/* './packages/improvement/report/popup_report_financial_impact_by_incident.php' */

'mx_financial_impact_by_incident'=>'Impacto Financiero por Incidente',

/* './packages/improvement/report/popup_report_incident_accompaniment.php' */

'mx_incident_accompaniment'=>'Seguimiento de Incidente',

/* './packages/improvement/report/popup_report_incident_by_responsible.php' */

'mx_evidence_collection'=>'Recolección de Evidencia',

/* './packages/improvement/report/popup_report_incident_control_revision.php' */

'mx_incident_control_revision'=>'Revisión de Control causada por Incidentes',

/* './packages/improvement/report/popup_report_incident_control_revision.xml' */

'rs_date_todo'=>'Fecha Estimada',
'rs_er'=>'RE (2355)',

/* './packages/improvement/report/popup_report_incident_without_occurrence.php' */

'rs_incidents_without_occurrence'=>'Incidentes sin Ocurrencias',

/* './packages/improvement/report/popup_report_incident_without_risk.php' */

'rs_incidentes_without_risks'=>'Incidentes sin Riesgos',

/* './packages/improvement/report/popup_report_incident_without_risk.xml' */

'rs_incident'=>'Incidente',

/* './packages/improvement/report/popup_report_incidents.php' */

'mx_incidents'=>'Incidentes',

/* './packages/improvement/report/popup_report_incidents.xml' */

'rs_incident_name'=>'Nombre',

/* './packages/improvement/report/popup_report_nc_without_ap.php' */

'rs_nc_without_ap'=>'No conformidades sin Planes de Acción',
'st_nc_manager_with_parentheses'=>'(Administrador de No Conformidades)',

/* './packages/improvement/report/popup_report_non_conformities.php' */

'mx_non_conformities'=>'No conformidades',

/* './packages/improvement/report/popup_report_non_conformities.xml' */

'rs_nc_name'=>'Nombre',

/* './packages/improvement/report/popup_report_nonconformity_accompaniment.php' */

'mx_nonconformity_accompaniment'=>'Seguimiento de No Conformidad',
'rs_not_defined'=>'No definido',

/* './packages/improvement/report/popup_report_nonconformity_accompaniment.xml' */

'rs_action_plan'=>'Plan de Acción',
'rs_conclusion'=>'Finalización',
'rs_non_conformity'=>'No conformidad:',

/* './packages/improvement/report/popup_report_nonconformity_by_process.php' */

'mx_nonconformity_by_process'=>'No conformidades por Procesos',

/* './packages/improvement/report/popup_report_nonconformity_by_process.xml' */

'rs_nonconformity'=>'No conformidad',

/* './packages/improvement/report/popup_report_occurrence_accompaniment.php' */

'mx_occurrence_accompaniment'=>'Seguimiento de Ocurrencia',

/* './packages/improvement/report/popup_report_occurrence_by_incident.php' */

'mx_occurrence_by_incident'=>'Ocurrencias por Incidente',

/* './packages/improvement/report/popup_report_occurrence_by_incident.xml' */

'rs_occurrence'=>'Ocurrencias',

/* './packages/improvement/report/popup_report_risk_auto_probability_value.php' */

'mx_risk_auto_probability_value'=>'Cálculo Automático de Probabilidad de Riesgo',

/* './packages/improvement/report/popup_report_risk_auto_probability_value.xml' */

'rs_incident_amount'=>'Cantidad e Incidentes',
'rs_last_verification'=>'Última Verificación',
'rs_periodicity'=>'Periodicidad',

/* './packages/improvement/report/popup_report_user_by_incident.php' */

'mx_user_by_incident'=>'Proceso Disciplinario',

/* './packages/improvement/report/popup_report_user_by_incident.xml' */

'rs_incident_cl'=>'Incidente:',

/* './packages/improvement/report/report_template.xml' */

'rs_fim'=>'FIN DEL INFORME',

/* './packages/improvement/tab_continual_improvement.xml' */

'ti_action_plan'=>'Plan de Acción',
'ti_disciplinary_process'=>'Proceso Disciplinario',
'ti_incident'=>'Incidente',
'ti_non_conformity'=>'No conformidad',
'ti_occurrences'=>'Ocurrencias',
'ti_report'=>'Informes',

/* './packages/libraries/nav_best_practices.php' */

'st_remove_best_practice_confirm'=>'¿Está seguro de que desea eliminar la Mejor Práctica <b>%best_practice_name%</b>?',
'st_remove_section_confirm'=>'¿Está seguro de que desea eliminar la sección <b>%section_name%</b>?',
'tt_remove_best_practice'=>'Eliminar Mejor Práctica',
'tt_remove_section'=>'Eliminar Sección',

/* './packages/libraries/nav_best_practices.xml' */

'mi_associate_events'=>'Asociar Eventos',
'mi_open_section'=>'Abrir Sección',
'mi_templates'=>'Plantillas de Documentos',
'tt_best_practices_bl'=>'<b>Mejores Prácticas</b>',
'vb_insert_best_practice'=>'Insertar Mejor Práctica',
'vb_insert_section'=>'Insertar Sección',

/* './packages/libraries/nav_document_template.php' */

'st_remove_document_template_confirm'=>'¿Está seguro de que desea eliminar la Plantilla de Documento <b>%context_name%</b>?',
'tt_remove_document_template'=>'Eliminar Plantilla de Documento',

/* './packages/libraries/nav_document_template.xml' */

'si_best_practice'=>'Mejor Práctica',
'si_denied'=>'Denegado',
'si_doc_template_control'=>'Control',
'si_pending'=>'Pendiente',
'tt_documents_templates_bl'=>'<b>Plantillas de Documentos</b>',

/* './packages/libraries/nav_events.php' */

'st_remove_category_confirm'=>'¿Está seguro de que desea eliminar la categoría  <b>%category_name%</b>?',
'st_remove_event_confirm'=>'¿Está seguro de que desea eliminar el evento  <b>%event_name%</b>?',
'tt_remove_category'=>'Eliminar Categoría',
'tt_remove_event'=>'Eliminar Evento',

/* './packages/libraries/nav_events.xml' */

'mi_associate_best_practices'=>'Asociar Mejores Prácticas',
'mi_open_event_category'=>'Open Category',
'tt_events_library_bl'=>'<b>Librería de Eventos</b>',
'vb_associate_event'=>'Asociar Evento',
'vb_insert_category'=>'Insertar Categoría',
'vb_insert_event'=>'Insertar Evento',

/* './packages/libraries/nav_import_export.php' */

'wn_error_invalid_file'=>'Error: archivo no válido.',
'wn_extension_not_allowed'=>'Extensión no permitida.',
'wn_maximum_size_exceeded'=>'Tamaño máximo excedido.',

/* './packages/libraries/nav_import_export.xml' */

'lb_client_cl'=>'Cliente:',
'lb_export_library_cl'=>'Exportar Librería:',
'lb_import_library_cl'=>'Importar Librería (AOL/XML):',
'lb_license_cl'=>'Licencia:',
'si_best_practices'=>'Mejores Prácticas',
'si_events_library'=>'Librería de Eventos',
'si_standards'=>'Estándares',
'si_templates'=>'Plantillas de Documentos',
'st_export_list_bl'=>'<b>Listado de Exportaciones</b>',
'st_exporting_options_bl'=>'<b>Opciones de Exportación</b>',
'st_import_list_bl'=>'<b>Listado of Importaciones</b>',
'st_or_bl'=>'<b>O</b>',
'to_export'=>'<b>Exportar:</b><br/><br/>Exporta items seleccionados.',
'to_export_aol'=>'<b>Exportación:</b><br/><br/>Exportar los elementos seleccionados en formato AOL.',
'to_export_list'=>'<b>Listar:</b><br/><br/>Lista items de librería, que serán exportados.',
'to_export_xml'=>'<b>Exportación:</b><br/><br/>Exportar los elementos seleccionados en formato XML.',
'to_import'=>'<b>Importar</b><br/><br/>Importar items seleccionados.',
'to_import_list'=>'<b>Listar:</b><br/><br/>Lista items de librería, que serán importados.',
'tr_best_practices'=>'Mejores Prácticas',
'tr_events_library'=>'Librería de Eventos',
'tr_no_items_to_import'=>'No hay items para ser importados',
'tr_standards'=>'Estándares',
'tr_template'=>'Plantillas de Documentos',
'tr_templates'=>'Plantillas de Documentos',
'vb_export'=>'Exportar',
'vb_export_as_aol'=>'Exportación como AOL',
'vb_export_as_xml'=>'Exportación como XML',
'vb_import'=>'Importar',
'vb_list'=>'Listar',
'vb_list_import'=>'Listar',
'wn_import_required_file'=>'Debe primero elegir un archivo antes de hacer click en la lista.',

/* './packages/libraries/nav_incident_category.php' */

'st_category_remove'=>'¿Está usted seguro que desea eliminar la categoría <b>%category_name%</b>?',
'st_solution_remove'=>'¿Está usted seguro que desea eliminar la solución?',
'tt_category_remove'=>'Eliminar Categoría',
'tt_category_remove_error'=>'Error eliminando la Categoría',
'tt_solution_remove'=>'Eliminar Solución',

/* './packages/libraries/nav_incident_category.xml' */

'gc_keywords'=>'Palabras claves',
'gc_problem'=>'Problem',
'gc_solution'=>'Solución',
'gc_solutions_bl'=>'<b>Soluciones</b>',
'mi_delete'=>'Eliminar',
'mi_open_category'=>'Abrir Categoría',
'tt_category_library'=>'<b>Librería de Categorías</b>',
'vb_solution_insert'=>'Insertar Solución',

/* './packages/libraries/nav_standards.php' */

'st_remove_standard_confirm'=>'¿Está seguro que desea eliminar el estandar <b>%standard_name%</b>?',
'tt_remove_standard'=>'Eliminar Estándar',

/* './packages/libraries/nav_standards.xml' */

'gc_creator'=>'Creador',
'tt_standards_bl'=>'<b>Estandardes</b>',

/* './packages/libraries/popup_best_practice_add.xml' */

'si_all_sections'=>'Todas las Secciones',
'tt_current_best_practices_bl'=>'<b>Mejores Prácticas Actuales</b>',

/* './packages/libraries/popup_best_practice_edit.php' */

'tt_best_practice_adding'=>'Alta de Mejor Práctica',
'tt_best_practice_editing'=>'Edición de Mejor Práctica',

/* './packages/libraries/popup_best_practice_edit.xml' */

'lb_control_objective_cl'=>'Objetivo de Control:',
'lb_control_type_cl'=>'Tipo de Control:',
'lb_implementation_guidelines_cl'=>'Guías de Implementación:',
'lb_metric_cl'=>'Métrica:',
'lb_name_good_practices_bl_cl'=>'<b>Nombre/ Buenas Prácticas:</b>',
'lb_standards_bl_cl'=>'<b>Estándares:</b>',
'si_administrative'=>'Administrativo',
'si_awareness'=>'Advertencia',
'si_coercion'=>'Coerción',
'si_correction'=>'Corrección',
'si_detection'=>'Detección',
'si_limitation'=>'Limitación',
'si_monitoring'=>'Monitoreo',
'si_physical'=>'Físico',
'si_prevention'=>'Prevención',
'si_recovery'=>'Recuperación',
'si_technical'=>'Técnico',
'tt_best_practice'=>'Mejor Práctica',

/* './packages/libraries/popup_category_edit.php' */

'tt_category_adding'=>'Agregar Categoría',
'tt_category_editing'=>'Edición de Categoría',

/* './packages/libraries/popup_document_template_edit.php' */

'tt_document_template_adding'=>'Adición de una Plantilla de Documento',
'tt_document_template_editing'=>'Edición de Plantilla de Documento',

/* './packages/libraries/popup_document_template_edit.xml' */

'lb_best_practice_cl'=>'Mejor Práctica:',
'lb_type_bl_cl'=>'<b>Tipo:</b>',
'si_action_plan'=>'Plan de Acción',
'si_area'=>'Área',
'si_asset'=>'Activo',
'si_control'=>'Control',
'si_policy'=>'Política',
'si_process'=>'Proceso',
'si_scope'=>'Alcance',
'si_select_type'=>'Seleccionar Tipo',

/* './packages/libraries/popup_event_add.xml' */

'tt_current_events_bl'=>'<b>Eventos Actuales</b>',

/* './packages/libraries/popup_event_edit.php' */

'tt_event_adding'=>'Alta de Eventos',
'tt_event_editing'=>'Edición de Eventos',

/* './packages/libraries/popup_event_edit.xml' */

'cb_suggested_event'=>'Evento Sugerido',
'lb_observation_cl'=>'Observación:',
'st_event_impact_cl'=>'Impacto:',

/* './packages/libraries/popup_imple_guide.xml' */

'tt_implementation_guidelines'=>'Guías de Implementación',

/* './packages/libraries/popup_incident_category_edit.php' */

'tt_category_addition'=>'Agregar Categoría',

/* './packages/libraries/popup_incident_category_edit.xml' */

'tt_category_edit'=>'Edición de Categoría',

/* './packages/libraries/popup_incident_solution_edit.xml' */

'lb_problem_bl_cl'=>'<b>Problema:</b>',
'lb_solution_bl_cl'=>'<b>Solución:</b>',
'tt_solution_edit'=>'Edición de Categoría',

/* './packages/libraries/popup_metric.xml' */

'tt_metric'=>'Métrica',

/* './packages/libraries/popup_section_edit.php' */

'tt_section_adding'=>'Alta de Sección',
'tt_section_editing'=>'Edición de Sección',

/* './packages/libraries/popup_standard_add.xml' */

'tt_current_standards_bl'=>'<b>Estandares Actuales</b>',
'tt_standards_search'=>'Búsqueda de Estándares',

/* './packages/libraries/popup_standard_edit.php' */

'tt_standard_adding'=>'Alta de Estándares',
'tt_standard_editing'=>'Edición de Estándares',

/* './packages/libraries/popup_standard_edit.xml' */

'lb_application_cl'=>'Aplicación:',
'lb_objective_cl'=>'Objetivo:',

/* './packages/libraries/tab_libraries.xml' */

'ti_best_practices'=>'Mejores Prácticas',
'ti_category_library'=>'Categorías de Incidentes',
'ti_events_library'=>'Librería de Eventos',
'ti_export_and_import'=>'Exportar e Importar',
'ti_standards'=>'Estándares',
'ti_templates'=>'Plantilla de Documentos',

/* './packages/policy/nav_all.php' */

'document_is_subdocument'=>'<b>%document_name%</b> es un subdocumento. <br>Documento Raíz: <b>%document_parent%</b>',

/* './packages/policy/nav_all.xml' */

'lb_status'=>'Estado',
'tt_all_documents_bl'=>'<b>Todos los Documentos</b>',

/* './packages/policy/nav_approving.xml' */

'tt_approving_documents_bl'=>'<b>Documentos En Aprobación</b>',

/* './packages/policy/nav_developing.php' */

'tt_developing_document_bl'=>'<b>Documentos En Desarrollo</b>',

/* './packages/policy/nav_developing.xml' */

'vb_insert_document'=>'Insertar Documento',

/* './packages/policy/nav_obsolete.xml' */

'gc_file_name'=>'Archivo / Enlace',
'tt_obsolete_documents_bl'=>'<b>Versiones Obsoletas de Documentos</b>',

/* './packages/policy/nav_publish.php' */

'tt_published_documents_bl'=>'<b>Documentos Publicados</b>',

/* './packages/policy/nav_register.php' */

'st_remove_register_confirm'=>'¿Está seguro de que desea eliminar el Registro <b>%register_name%</b>?',
'tt_remove_register'=>'Remover Registro',

/* './packages/policy/nav_register.xml' */

'gc_retention_time'=>'Tiempo de Retención',
'gc_storage_place'=>'Lugar de Almacenado',
'gc_storage_type'=>'Tipo de Almacenado',
'mi_read'=>'Leer',
'mi_read_document'=>'Leer Documento',
'mi_readers'=>'Lectores',
'vb_insert_register'=>'Insertar Registro',

/* './packages/policy/nav_report.php' */

'mx_documents_with_high_revision_frequency'=>'Documentos con una alta frecuencia de revisión',
'mx_documents_without_registers'=>'Documentos sin Registros',
'mx_pendant_task_pm'=>'Tareas pendientes',
'mx_report_documents_pendent_reads'=>'Documentos con Lectura Pendiente',
'mx_report_element_classification'=>'Informe de Clasificación',
'mx_users_with_pendant_read'=>'Usuarios con tareas pendientes',
'si_access_to_documents'=>'Auditar Accesos a Documentos',
'si_accessed_documents'=>'Auditar Acceso de Usuarios',
'si_approvers'=>'Documentos y sus Aprobadores',
'si_documents_by_component'=>'Documentos por Componente de SGSI',
'si_documents_by_state'=>'Documentos',
'si_documents_dates'=>'Fechas de Creación y Revisión de Documentos',
'si_documents_per_area'=>'Documentos por Área',
'si_items_with_without_documents'=>'Componentes sin Documentos',
'si_most_accessed_documents'=>'Documentos Más Accedidos',
'si_most_revised_documents'=>'Documentos Más Revisados',
'si_not_accessed_documents'=>'Documentos no accedidos',
'si_pendant_approvals'=>'Aprobaciones Pendientes',
'si_readers'=>'Documentos y sus Lectores',
'si_registers_per_document'=>'Registro por Documento',
'si_users_per_process'=>'Usuarios asociados a Procesos',

/* './packages/policy/nav_report.xml' */

'cb_expand_documents'=>'Expandir Documentos',
'cb_expand_readers'=>'Expandir Lectores',
'cb_organize_by_area'=>'Organizar por área',
'cb_show_all_access'=>'Mostrar todos los accesos',
'cb_show_register_documents'=>'Mostrar Registros de Documentos',
'lb_creation_date_cl'=>'Fecha de Creación',
'lb_documents_bl_cl'=>'<b>Documentos:</b>',
'lb_final_date_cl'=>'Fecha Final:',
'lb_initial_date_cl'=>'Fecha Inicial:',
'lb_last_revision_date_cl'=>'Última Fecha de Revisión',
'lb_show_only_first_cl'=>'Mostrar solo el primero:',
'lb_until'=>'hasta',
'lb_users_bl_cl'=>'<b>Usuarios:</b>',
'si_abnormalities_reports'=>'Informes de Anormalidades',
'si_access_control_reports'=>'Informes de Control de Acceso',
'si_all_status'=>'Todos',
'si_approved'=>'Publicada',
'si_developing'=>'En desarrollo',
'si_documents_by_type'=>'Documentos por Tipo',
'si_general_reports'=>'Informes Generales',
'si_management_reports'=>'Informes de Gestión',
'si_obsolete'=>'Obsoleto',
'si_pendant'=>'En aprobación',
'si_register_reports'=>'Informes de Registros',
'si_registers_by_type'=>'Registros por Tipo',
'si_revision'=>'En revisión',
'st_asset'=>'Activos',
'st_components'=>'<b>Componentes:</b>',
'st_control'=>'Control',
'st_element_type_warning'=>'Por favor, seleccione al menos un tipo.',
'st_process'=>'Procesos',
'st_status_bl'=>'<b>Estado:</b>',
'st_type_cl'=>'Tipo:',
'wn_component_filter_warning'=>'Por favor, seleccione al menos un tipo de componento.',
'wn_no_filter_available'=>'Sin filtro disponible.',
'wn_select_a_document'=>'Por favor, seleccione un documento.',

/* './packages/policy/nav_revision.xml' */

'mi_edit_author'=>'Editar Autor',
'tt_revision_documents_bl'=>'<b>Documentos en Revisión</b>',

/* './packages/policy/nav_to_be_published.php' */

'st_document_confirm_remove'=>'¿Está seguro de que desea eliminar el documento <b>%document_name%</b>?',
'tt_remove_document'=>'Eliminar Documento',

/* './packages/policy/nav_to_be_published.xml' */

'gc_elements'=>'Items',
'gc_file_version'=>'Versión',
'gc_sub_doc'=>'Subdocumentos',
'lb_classification'=>'Clasificación',
'lb_type'=>'Tipo',
'mi_approve'=>'Aprobar',
'mi_associate_components'=>'Asociar Componentes',
'mi_comments'=>'Comentarios',
'mi_copy_readers'=>'Copiar Lectores',
'mi_details'=>'Detalles',
'mi_document_templates'=>'Plantillas de Documentos',
'mi_edit_approvers'=>'Editar Aprobadores',
'mi_edit_mainapprover'=>'Editar Responsable',
'mi_edit_readers'=>'Editar Lectores',
'mi_previous_versions'=>'Versiones Previas',
'mi_publish'=>'Publicar',
'mi_references'=>'Referencias',
'mi_registers'=>'Registros',
'mi_send_to_approval'=>'Envíar para Aprobación',
'mi_send_to_revision'=>'Envíar para Revisión',
'mi_sub_documents'=>'Subdocumentos',
'mi_view_approvers'=>'Ver Aprobadores',
'mi_view_components'=>'Ver Componentes',
'mi_view_readers'=>'Ver Lectores',
'tt_documents_to_be_published_bl'=>'<b>Documentos para ser Publicados</b>',

/* './packages/policy/popup_approvers_edit.xml' */

'tt_approvers_editing'=>'Edición de Aprobadores',
'tt_curret_approvers_bl'=>'<b>Aprobadores Actuales</b>',

/* './packages/policy/popup_approvers_view.xml' */

'tt_approvers_visualization'=>'Visualización de Aprobadores',

/* './packages/policy/popup_associate_users.xml' */

'gc_login'=>'Ingreso',
'lb_login_cl'=>'Ingreso:',
'tt_current_users_bl'=>'<b>Usuarios Actuales</b>',
'tt_users_search'=>'Búsqueda de Usuarios',

/* './packages/policy/popup_create_document_confirm.xml' */

'rb_creation_document_question'=>'¿Usted desea crear un documento en el sistema y usar este como plantilla para el registro <b>%register_name%</b>?',
'rb_document_do_nothing'=>'¿No crear / asociar documento al registro <b>%register_name%</b>?',
'rb_document_relation_question'=>'¿Usted desea asociar un documento existente en el sistema y usar este como plantilla para registro <b>%register_name%</b>?',
'st_what_action_you_wish_to_do'=>'¿Qué acción desea ejecutar?',
'tt_document_creation_or_association'=>'Alta/Asociación de Documentos',
'vb_ok'=>'Ok',

/* './packages/policy/popup_document_author_edit.xml' */

'tt_author_editing'=>'Edición de Autor',

/* './packages/policy/popup_document_context_associate.php' */

'st_permission_to_associate_item_document_denied'=>'"Usted no tiene permisos para asociar un item a un documento de el tipo ""Gestión"" u ""Otros"""',

/* './packages/policy/popup_document_context_associate.xml' */

'tt_current_items_bl'=>'<b>Items Actuales</b>',
'tt_risk_management_items_association'=>'Asociación de Items de Gestión de Riesgos',

/* './packages/policy/popup_document_context_view.xml' */

'tt_associated_risk_management_items_visualization'=>'Visualización de Items de Gestión de Riesgos Asociados',

/* './packages/policy/popup_document_copy_readers.xml' */

'tt_documents_search'=>'Búsqueda de Documentos',
'vb_copy_readers'=>'Copiar Lectores',

/* './packages/policy/popup_document_create.xml' */

'lb_document_name_bl_cl'=>'<b>Nombre del Documento:</b>',
'tt_new_document'=>'Nuevo Documento',

/* './packages/policy/popup_document_edit.php' */

'si_management'=>'Gestión',
'si_no_type'=>'Sin Tipo',
'si_others'=>'Otros',
'si_register'=>'Registro',
'st_creation_date'=>'Fecha de Creación:',
'st_document_approval_confirm'=>'¿Desea enviar el documento para aprobación?',
'st_download_file'=>'Descargar archivo:',
'st_publish_document_confirm'=>'¿Desea publicar el documento?',
'st_publish_document_on_date_confirm'=>'¿Desea publicar el documento en %date%?',
'st_size'=>'Tamaño:',
'to_max_file_size'=>'Tamaño máximo de archivo: %max_file_size%',
'tt_document_adding'=>'Adición de Documento',
'tt_document_editing'=>'Edición de Documento',
'tt_publish'=>'Publicar',
'tt_send_for_approval'=>'Enviar para Aprobación',

/* './packages/policy/popup_document_edit.xml' */

'lb_author_bl_cl'=>'<b>Autor:</b>',
'lb_deadline_bl_cl'=>'<b>Fecha Límite:</b><br/><br/>Fecha límite para la aprobación de documento.',
'lb_inform_in_advance_of'=>'Informe de avance de',
'lb_publication_date_bl_cl'=>'<b>Fecha de Publicación:</b>',
'lb_revision_cl'=>'Revisión:',
'si_disable'=>'Deshabilitar',
'si_enable'=>'Habilitar',
'si_file'=>'Archivo',
'si_link'=>'Enlace',
'st_days'=>'días.',
'st_general_information'=>'Información General',
'st_management_information'=>'Información de Gestión',
'st_test'=>'prueba',
'to_doc_instance_manual_version'=>'<b>Versión Manual:</b><br/>La versión manual solamente tomará efecto cuando el documento tiene un archivo o un enlace asociado a este.',
'to_document_inform_in_advance'=>'<b>Informe en avance:</b><br/><br/>Este es el número de días antes de la fecha límite o revisión en el cual una alerta o adventencia de tarea  debe ser generada. (2697)',
'to_document_type_modification'=>'Para modificar el tipo de documento, usted debe eliminar las relaciones entre el documento y los Items de la Gestión de Riesgo.',
'wn_deadline_date_must_be_less_date_publication'=>'La fecha del plazo debe ser menor o igual que la fecha de publicación.',
'wn_doc_exists_error'=>'Ya existe un documento con el mismo nombre en el sistema.',
'wn_doc_exists_trash_error'=>'Ya existe un documento con el mismo nombre en la papelera del sistema.',
'wn_max_file_size_exceeded'=>'Tamaño máximo de archivo excedido.',
'wn_publication_date_after_today'=>'La fecha de publicación debe ser la misma o futura a hoy',
'wn_upload_error'=>'Error intentando envíar archivo.',

/* './packages/policy/popup_document_instances.php' */

'st_copy_previous_version_confirm'=>'¿Está seguro que desea copiar la versión previa?',
'tt_copy_previous_version'=>'Copiar versión previa',

/* './packages/policy/popup_document_instances.xml' */

'gc_end_date'=>'Fecha de Finalización',
'gc_modification_comment'=>'Comentario de Modificación',
'gc_revision_justification'=>'Razón de la Revisión',
'gc_start_date'=>'Fecha de Inicio',
'gc_version'=>'Versión',
'tt_previous_versions'=>'Versiones Previas',
'wn_no_version_selected'=>'No hay versión seleccionada',

/* './packages/policy/popup_document_main_approver_edit.xml' */

'lb_approver_bl_cl'=>'<b>Responsable:</b>',
'tt_approver_editing'=>'Responsable de la edición',

/* './packages/policy/popup_document_manage.xml' */

'st_approver_list_only_when_developing'=>'La lista de aprobadores solo puede ser modificada cuando el documento está en desarrollo.',
'st_items_association_not_management_or_others'=>'Solo es posible modificar las asociaciones de un documento que no es del tipo Gestión u Otros.',
'st_items_association_only_when_developing'=>'Solo es posible modificar las asociaciones de un documento cuando está en desarrollo.',
'st_readers_list_only_when_developing'=>'La lista de lectores solo puede ser editada cuando el documento está en desarrollo',
'st_revision_for_published_document_only'=>'Sólo un documento publicado puede ser enviado para revisión',
'tt_associate_items_bl'=>'<b>Asociar Items</b>',
'tt_edit_approvers_bl'=>'<b>Editar Aprobador</b>',
'tt_edit_readers_bl'=>'<b>Editar Lectores</b>',
'tt_manage_document'=>'Gestionar Documentos',
'tt_send_for_revision_bl'=>'<b>Enviar para Revisión</b>',
'tt_view_approvers_bl'=>'<b>Ver Aprobador</b>',
'tt_view_items_bl'=>'<b>Ver Items</b>',
'tt_view_readers_bl'=>'<b>Ver Lectores</b>',
'vb_copy'=>'Copiar',
'vb_send'=>'Enviar',

/* './packages/policy/popup_document_read.php' */

'st_remove_comment_confirm'=>'¿Está ustede seguro de eliminar el comentario referido?',
'tt_approve_document'=>'Aprobar Documento',
'tt_remove_comment'=>'Eliminar Comentario',

/* './packages/policy/popup_document_read.xml' */

'ti_comments'=>'Comentarios',
'ti_general'=>'General',
'ti_references'=>'Referencias',
'ti_sub_documents'=>'Sub-Documentos',
'tt_document_reading'=>'Lectura de Documento (2738)',
'vb_approve'=>'Aprobar',
'vb_deny'=>'Denegar',

/* './packages/policy/popup_document_read_comments.xml' */

'gc_author'=>'Autor',
'gc_comment'=>'Comentario',
'gc_date'=>'Fecha',
'tt_coments'=>'Comentarios',
'tt_comments_bl'=>'<b>Comentarios</b>',

/* './packages/policy/popup_document_read_details.xml' */

'lb_date_cl'=>'Fecha',
'lb_keywords_cl'=>'Palabras claves:',
'lb_size_cl'=>'Tamaño:',
'lb_version_cl'=>'Versiones:',
'tt_details'=>'Detalles',

/* './packages/policy/popup_document_read_subdocuments.xml' */

'tt_sub_documents'=>'Sub-Documentos',

/* './packages/policy/popup_document_reference.php' */

'st_remove_reference_confirm'=>'¿Está seguro de que desea eliminar la referencia <b>%doc_reference_name%</b>?',
'tt_remove_reference'=>'Eliminar Referencia',

/* './packages/policy/popup_document_reference.xml' */

'gc_creation_date'=>'Fecha de Creación',
'gc_link'=>'Enlace',
'lb_link_bl_cl'=>'<b>Enlace:</b>',
'st_http'=>'http://',
'tt_references'=>'Referencias',
'tt_references_bl'=>'<b>Referencias</b>',

/* './packages/policy/popup_document_register.xml' */

'tt_associated_registers_bl'=>'<b>Registros Asociados</b>',
'tt_registers'=>'Registros',
'vb_disassociate'=>'Desasociar',

/* './packages/policy/popup_document_register_associate.xml' */

'gc_classification'=>'Clasificación',
'gc_place'=>'Lugar',
'gc_time'=>'Tiempo',
'tt_register_association'=>'Asociación de Registro',
'tt_registers_bl'=>'<b>Registros</b>',
'vb_associate_registers'=>'Asociar Registros',
'wn_no_register_selected'=>'No hay registro selecionado.',
'wn_register_same_name_error'=>'Ya existe un registro con el nombre %register_name% asociado.',

/* './packages/policy/popup_document_register_edit.php' */

'tt_register_adding'=>'Alta de Registro',
'tt_register_editing'=>'Edición de Registro',
'vb_create'=>'Crear',
'vb_edit'=>'Editar',

/* './packages/policy/popup_document_register_edit.xml' */

'lb_disposition_cl'=>'Disposición',
'lb_indexing_type_cl'=>'Tipo de Indexado:',
'lb_origin_cl'=>'Origen:',
'lb_protection_requirements_cl'=>'Requerimientos de Protección:',
'si_day_days'=>'Día(s)',
'si_month_months'=>'Mes(es)',
'si_week_weeks'=>'Semana(s)',
'vb_lookup'=>'Búscar',
'wn_register_exists_error'=>'Ya existe un registro con el mismo nombre en el sistema.',
'wn_register_exists_trash_error'=>'Ya existe un registro con el mismo nombre en la papelera del sistema.',
'wn_retention_time'=>'El tiempo de retención debe ser mayor a cero',

/* './packages/policy/popup_document_register_read.xml' */

'lb_responsible'=>'Responsable',
'lb_retention_time_cl'=>'<b>Tiempo de Retención:</b>',
'lb_storage_place_cl'=>'Lugar de Almacenado:',
'lb_storage_type_cl'=>'Tipo de Almacenado:',
'tt_register_reading'=>'Lectura de Registro',
'vb_preferences'=>'Preferencias',

/* './packages/policy/popup_document_revise_not_alter.xml' */

'tt_revise_document_without_altering'=>'Revisar Documentos sin Alteración',

/* './packages/policy/popup_document_revision_task.php' */

'mx_scheduled_revision'=>'Revisión Agendada',

/* './packages/policy/popup_document_revision_task.xml' */

'lb_author_cl'=>'Autor:',
'lb_file_cl'=>'Archivo:',
'lb_link_cl'=>'Enlace:',
'tt_document_revision'=>'Revisón de Documento',
'vb_create_new_version'=>'Crear Nueva Versión',
'vb_download'=>'Descarga',
'vb_revise_without_altering'=>'Revisar sin Alteración',
'vb_visit_link'=>'Visitar Enlace',
'vb_visualize'=>'Ver',

/* './packages/policy/popup_document_search.xml' */

'tt_document_search'=>'Búsqueda de Documentos',
'wn_no_document_selected'=>'No hay documento seleccionado.',

/* './packages/policy/popup_documents_search.xml' */

'tt_current_documents_bl'=>'<b>Documentos Actuales</b>',

/* './packages/policy/popup_modification_comment.php' */

'tt_popup_modification_comment_creation'=>'Comentarios de Creación',
'tt_popup_modification_comment_creationfasdfawfqwerqwe'=>'Comentarios de Creación',

/* './packages/policy/popup_modification_comment.xml' */

'lb_comment_bl_cl'=>'<b>Comentario:</b>',
'tt_comment_modification'=>'Comentarios de Modificación',
'tt_comment_modification2'=>'Comentarios de Modificación',

/* './packages/policy/popup_readers_edit.xml' */

'si_all_processes'=>'Todos los Procesos',
'tt_current_readers_bl'=>'<b>Lectores Actuales</b>',
'tt_readers_editing'=>'Edición de Lectores',

/* './packages/policy/popup_readers_view.xml' */

'tt_readers_visualization'=>'Visualización de Lectores',

/* './packages/policy/popup_register_readers_edit.xml' */

'tt_current_readers'=>'<b>Lectores Actuales</b>',
'tt_readers_edit'=>'Edición de Lectores',

/* './packages/policy/popup_revision_justification.xml' */

'tt_revision_justification'=>'Razón de la Revisión',
'vb_send_for_revision'=>'Envíar a Revisión',

/* './packages/policy/popup_subdocuments_management.php' */

'st_create_subdocument'=>'Crear Subdocumento',

/* './packages/policy/popup_subdocuments_management.xml' */

'tt_manage_subdocuments'=>'Gestionar Subdocumentos',

/* './packages/policy/popup_template_search.xml' */

'rb_suggested_by_best_practice_only'=>'mostrar sólo las plantillas de mejores prácticas sugeridas',
'rb_suggested_only'=>'Solamente Sugerido',
'tt_document_templates_search'=>'Búsqueda de Plantilla de Documentos',

/* './packages/policy/report/popup_report_accessed_documents.php' */

'rs_accessed_documents'=>'Auditoría de Acceso de Usuario',
'rs_management'=>'Administración (2802)',
'rs_others'=>'Otros',

/* './packages/policy/report/popup_report_contexts_without_documents.php' */

'rs_items_with_without_documents'=>'Componentes sin Documentos',

/* './packages/policy/report/popup_report_doc_instance_comment_and_justification.php' */

'mx_doc_instance_comment_and_justification'=>'Comentarios y Razones de Documentos',

/* './packages/policy/report/popup_report_doc_instance_comment_and_justification.xml' */

'rs_header_instance_modify_comment'=>'Comentario de Modificación',
'rs_header_instance_rev_justification'=>'Razón de Revisión',

/* './packages/policy/report/popup_report_docs_with_high_frequency_revision.php' */

'tt_docs_with_high_frequency_revision'=>'Documentos con una alta frecuencia de revisión',

/* './packages/policy/report/popup_report_document_accesses.php' */

'rs_accesses_to_documents'=>'Auditoría de Acceso a Documentos',
'rs_no_type'=>'Sin Tipo',

/* './packages/policy/report/popup_report_document_accesses.xml' */

'rs_time'=>'Tiempo',

/* './packages/policy/report/popup_report_document_approvers.php' */

'rs_approvers'=>'Documentos y sus aprobadores',

/* './packages/policy/report/popup_report_document_dates.php' */

'rs_documents_dates'=>'Fechas de Creación y Revisión de Documentos',

/* './packages/policy/report/popup_report_document_dates.xml' */

'rs_created_on_cl'=>'Creado en:',

/* './packages/policy/report/popup_report_document_readers.php' */

'rs_readers'=>'Documentos y sus Lectores',

/* './packages/policy/report/popup_report_document_readers.xml' */

'rs_author_cl'=>'Autor:',

/* './packages/policy/report/popup_report_document_registers.php' */

'rs_registers_per_document'=>'Registro por Documento',

/* './packages/policy/report/popup_report_document_registers.xml' */

'rs_name'=>'Nombre',
'rs_retention_time'=>'Fecha de Retención',

/* './packages/policy/report/popup_report_document_summary.php' */

'mx_report_document_summary'=>'Propiedades del Documento',

/* './packages/policy/report/popup_report_documents_by_area.php' */

'rs_documents_per_area'=>'Documentos por Área',

/* './packages/policy/report/popup_report_documents_by_area.xml' */

'rs_document_documents'=>'documento(s)',

/* './packages/policy/report/popup_report_documents_by_component.php' */

'rs_document_of_component'=>'documentos del %component%',
'tt_documents_by_component'=>'Documentos por Componente de SGSI',

/* './packages/policy/report/popup_report_documents_by_state.php' */

'tt_documents_by_state'=>'Documentos',

/* './packages/policy/report/popup_report_documents_by_type.php' */

'rs_documents_by_type'=>'Documentos por Tipo',

/* './packages/policy/report/popup_report_documents_revision_late.php' */

'mx_documents_revision_late'=>'Documentos con Revisión Retrasada',

/* './packages/policy/report/popup_report_documents_revision_late.xml' */

'rs_document_next_revision'=>'Próxima Revisión',

/* './packages/policy/report/popup_report_documents_revision_schedule.php' */

'mx_documents_revision_schedule'=>'Agenda de Revisión de Documentos',

/* './packages/policy/report/popup_report_documents_revision_schedule.xml' */

'rs_document_name'=>'Nombre',
'rs_document_schedule'=>'Programación',

/* './packages/policy/report/popup_report_documents_without_register.php' */

'rs_document_main_approver_name'=>'Responsable por Documento',
'rs_document_name_bl'=>'Documento',
'tt_documents_without_register'=>'Documentos sin Registros',

/* './packages/policy/report/popup_report_not_accessed_documents.php' */

'rs_not_accessed_documents'=>'Documentos no Accedidos',

/* './packages/policy/report/popup_report_pendant_approvals.php' */

'tt_pendant_approvals'=>'Aprobaciones Pendientes',

/* './packages/policy/report/popup_report_registers_by_type.php' */

'rs_registers_by_type'=>'Registros por Tipo',

/* './packages/policy/report/popup_report_top_accessed_documents.php' */

'rs_most_accessed_documents'=>'Documentos Más Accedidos',

/* './packages/policy/report/popup_report_top_accessed_documents.xml' */

'rs_accesses_cl'=>'Accesos:',

/* './packages/policy/report/popup_report_top_revised_documents.php' */

'rs_most_revised_documents'=>'Documentos Más Revisados',

/* './packages/policy/report/popup_report_top_revised_documents.xml' */

'rs_revisions_cl'=>'Revisiones:',

/* './packages/policy/report/popup_report_user_comments.php' */

'mx_user_comments'=>'Comentarios de Usuarios',

/* './packages/policy/report/popup_report_user_comments.xml' */

'rs_comment_date'=>'Fecha',
'rs_comment_text'=>'Comentario',
'rs_comment_user'=>'Usuario',

/* './packages/policy/report/popup_report_users_by_process.php' */

'rs_users_per_process'=>'Usuarios asociados a Procesos',

/* './packages/policy/report/popup_report_users_by_process_and_area.xml' */

'rs_user_bl_cl'=>'<b>Usuario:</b>',

/* './packages/policy/report/popup_report_users_with_pendant_read.php' */

'mx_documents_pendent_reads'=>'Usuarios con lecturas pendientes',

/* './packages/policy/tab_policy_management.xml' */

'ti_all'=>'Todos',
'ti_developing'=>'En Desarrollo',
'ti_obsolete'=>'Obsoleto',
'ti_publish'=>'Publicado',
'ti_registers'=>'Registros',
'ti_revision'=>'En Revisión',
'ti_to_approve'=>'En Aprobación',
'ti_to_be_published'=>'Para ser Publicado',

/* './packages/risk/nav_area.php' */

'st_area_remove_confirm'=>'¿Está seguro que desea eliminar el área <b>%area_name%</b>?',
'tt_remove_area'=>'Eliminar Área',

/* './packages/risk/nav_area.xml' */

'gc_subarea'=>'Sub-Áreas',
'lb_sub_areas_count_cl'=>'Número de Sub-Áreas',
'mi_processes'=>'Procesos',
'mi_sub_areas'=>'Sub-Áreas',
'tt_business_areas_bl'=>'<b>Áreas de Negocio</b>',

/* './packages/risk/nav_asset.php' */

'grid_asset_filter_dependents'=>'(dependencias del activo \'<b>%asset_name%</b>\')',
'st_asset_copy'=>'Copiar (2885)',
'st_asset_remove_confirm'=>'¿Está seguro que desea eliminar el Activo <b>%asset_name%</b>?',
'tt_remove_asset'=>'Eliminar Activo',

/* './packages/risk/nav_asset.xml' */

'gc_asset_category'=>'Categoría de activos',
'gc_asset_dependencies'=>'Dep.(cias)',
'gc_asset_dependents'=>'Dep.(tes)',
'gc_processes'=>'Procesos',
'lb_process_count_cl'=>'Número de Procesos:',
'mi_associate_processes'=>'Asociar Procesos',
'mi_duplicate'=>'Duplicar',
'mi_edit_dependencies'=>'Ajustar Dependencias',
'mi_edit_dependents'=>'Ajustar Dependientes',
'mi_insert_risk'=>'Insertar Riesgos',
'mi_load_risks_from_library'=>'Cargar riesgos desde biblioteca',
'tt_assets_bl'=>'<b>Activos</b>',

/* './packages/risk/nav_control.php' */

'st_control_remove_confirm'=>'¿Está seguro que desea eliminar el Control <b>%control_name%</b>?',

/* './packages/risk/nav_control.xml' */

'gc_cost'=>'Costo',
'gc_icon'=>'Ícono',
'gc_risks'=>'Riesgos',
'lb_risk_count_cl'=>'Número de Riesgos:',
'mi_associate_risks'=>'Asociar Riesgos',
'mi_view_risks'=>'Ver Riesgos',
'rb_active'=>'Activado',
'rb_not_active'=>'Desactivado',
'tt_controls_directory_bl'=>'<b>Directorio de Controles</b>',
'vb_revision_and_test_history'=>'Historia de Revisiones/Pruebas',

/* './packages/risk/nav_process.php' */

'grid_process_asset_filter'=>'(filtrado por activo \'<b>%asset_name%</b>\')',
'st_process_remove_confirm'=>'¿Está seguro que desea eliminar el Proceso <b>%process_name%</b>?',
'tt_remove_process'=>'Eliminar Proceso',

/* './packages/risk/nav_process.xml' */

'cb_not_estimated'=>'No Estimado',
'gc_assets'=>'Activos',
'gc_responsible'=>'Responsable',
'gc_users'=>'Usuarios',
'lb_asset_count_cl'=>'Número de Activos',
'lb_responsible_cl'=>'Responsable:',
'lb_value_cl'=>'Riesgo:',
'mi_add_document'=>'Crear Documento',
'mi_associate_assets'=>'Asociar Activos',
'mi_documents'=>'Documentos',
'mi_view_assets'=>'Ver Activos',
'si_all_priorities'=>'Todos',
'si_all_responsibles'=>'Todos',
'si_all_types'=>'Todos',
'tt_processes_bl'=>'<b>Procesos</b>',

/* './packages/risk/nav_report.php' */

'mx_no_priority'=>'Sin Prioridad',
'mx_no_type'=>'Sin Tipo',
'si_amount_of_risks_per_area'=>'Cantidad de Riesgos por Área',
'si_amount_of_risks_per_process'=>'Cantidad de Riesgos por Proceso',
'si_areas_without_processes'=>'Áreas sin Procesos',
'si_assets'=>'Activos',
'si_assets_importance'=>'Importancia de Activos',
'si_assets_without_risks'=>'Activos sin Eventos de Riesgo',
'si_checklist_per_event'=>'Checklist por Activo',
'si_controls_efficiency'=>'Eficacia de Controles',
'si_controls_implementation_date'=>'Fecha de Implementación de Controles',
'si_controls_per_responsible'=>'Controles por Responsable',
'si_controls_per_risk'=>'Controles por Riesgo',
'si_controls_by_asset'=>'Controles por Activo',
'si_controls_without_risks'=>'Controles sin Riesgos Asociados',
'si_cost_of_controls_per_area'=>'Costo de Controles por Área',
'si_cost_of_controls_per_asset'=>'Costos de Control por Activo',
'si_cost_of_controls_per_process'=>'Costo de controles por Área',
'si_cost_of_controls_per_responsible'=>'Costos de Control por Responsable',
'si_cost_per_control'=>'Costos por Control',
'si_duplicated_risks'=>'Riesgos Duplicados',
'si_element_classification'=>'Clasificación de Items',
'si_follow_up_of_controls'=>'Seguimiento de Controles',
'si_follow_up_of_controls_efficiency'=>'Seguimiento de Controles de Eficacia',
'si_follow_up_of_controls_test'=>'Seguimiento de Controles de Prueba',
'si_non_parameterized_risks'=>'Riesgos No Estimados',
'si_not_measured_controls'=>'Controles No Medidos',
'si_pending_items'=>'Items Pendientes',
'si_pending_tasks'=>'Tareas Pendientes',
'si_plan_of_risks_treatment'=>'Plan de Tratamiento de Riesgo',
'si_planning_of_controls'=>'Planeamiento de Controles',
'si_processes_without_assets'=>'Procesos sin Activos',
'si_report_conformity'=>'Conformidad',
'si_risk_impact'=>'Impacto de Riesgo',
'si_risk_status_per_area'=>'Estado de Riesgo por Área',
'si_risk_status_per_process'=>'Estado de Riesgo por Proceso',
'si_risks_per_area'=>'Riesgos por Área',
'si_risks_per_asset'=>'Riesgos por Activo',
'si_risks_per_control'=>'Riesgos por Control',
'si_risks_per_process'=>'Riesgos por Proceso',
'si_risks_values'=>'Valores de Riesgos',
'si_sgsi_policy_statement'=>'Declaración de Política de SGSI',
'si_sgsi_scope_statement'=>'Declaraciones de Alcance de SGSI',
'si_statement_of_applicability'=>'Declaración de Aplicabilidad',
'si_summary_of_controls'=>'Resumen de Controles',
'si_top_10_assets_with_highest_risks'=>'Top 10 Activos con más Altos Riesgos',
'si_top_10_assets_with_most_risks'=>'Top 10 Activos con más Riesgos',
'si_top_10_risks_per_area'=>'Top 10 de Riesgos por Área',
'si_top_10_risks_per_process'=>'Top 10 de Riesgos por Procesos',
'si_users_responsibilities'=>'Responsabilidades de Usuarios',
'st_all_standards'=>'Todos',

/* './packages/risk/nav_report.xml' */

'cb_approved'=>'Aprobado',
'cb_asset_security'=>'Seguridad de Activo',
'cb_denied'=>'Denegado',
'cb_organize_by_asset'=>'Organizar por Activo',
'cb_organize_by_process'=>'Organizar por Proceso',
'cb_pendant'=>'Pendiente',
'lb_comment_cl'=>'Comentario:',
'lb_consider_cl'=>'Considerado: (3007)',
'lb_filter_by_cl'=>'Filtrar por:',
'lb_filter_by_standard_cl'=>'Filtrado por Estandar:',
'lb_filter_cl'=>'Filtro:',
'lb_process_cl'=>'Proceso:',
'lb_process_status_cl'=>'Estado del activo:',
'lb_reponsible_for_cl'=>'Responsable por:',
'lb_risk_status_cl'=>'Estado de riesgo:',
'lb_values_cl'=>'Valores:',
'rb_area'=>'Área',
'rb_area_process'=>'Área y Proceso',
'rb_asset'=>'Activo',
'rb_both'=>'Ambos',
'rb_detailed'=>'Detallado',
'rb_efficient'=>'Eficiente',
'rb_implemented'=>'Implementado',
'rb_inefficient'=>'Ineficiente',
'rb_planned'=>'Planeado',
'rb_process'=>'Proceso',
'rb_real'=>'Potencial',
'rb_summarized'=>'Resumido',
'si_all_reports'=>'Todos los Informes',
'si_areas_by_priority'=>'Áreas por Prioridad',
'si_areas_by_type'=>'Áreas por Tipo',
'si_control_reports'=>'Informes de Control',
'si_controls_by_type'=>'Controles por Tipo',
'si_events_by_type'=>'Eventos por Tipo',
'si_financial_reports'=>'Informes Financieros',
'si_iso_27001_reports'=>'Informes ISO 27001',
'si_processes_by_priority'=>'Procesos por Prioridad',
'si_processes_by_type'=>'Procesos por Tipo',
'si_risk_management_reports'=>'Informes de Gestión de Riesgos',
'si_risk_reports'=>'Informes de Riesgo',
'si_risks_by_type'=>'Riesgos por Tipo',
'si_summaries'=>'Resúmenes',
'st_area_priority'=>'Prioridades del Área',
'st_area_type'=>'Tipos de Área',
'st_asset_cl'=>'Activo:',
'st_config_filter_type_priority'=>'Configurar el Filtro Seleccionado:',
'st_control_type'=>'Tipos de Control',
'st_event_type'=>'Tipos de Eventos',
'st_no_filter_available'=>'Sin filtros disponibles.',
'st_none_selected'=>'Seleccionar un Filtro',
'st_organize_by_asset'=>'Organizar por Activo',
'st_process_priority'=>'Prioridades de Proceso',
'st_process_type'=>'Tipos de Proceso',
'st_risk_type'=>'Tipos de Riesgos',
'st_show_cl'=>'Mostrar:',
'st_standard_cl'=>'Estándar:',
'tt_specific_report_filters'=>'Filtros Específicos en el Informe',
'tt_type_and_priority_filter_cl'=>'Filtros de Tipo y Prioridad:',
'vb_pre_report'=>'Pre-Informe',
'vb_report'=>'Informe',
'wn_no_report_selected'=>'No hay informe seleccionado',
'wn_select_at_least_one_filter'=>'Al menos un filtro debe ser seleccionado.',
'wn_select_at_least_one_parameter'=>'Al menos un parámetro debe ser seleccionado.',
'wn_select_risk_value'=>'Por favor, seleccione un valor de riesgo para ser mostrado.',

/* './packages/risk/nav_risk.php' */

'gs_avoided'=>'Evitado',
'gs_reduced'=>'Reducido',
'gs_restrained'=>'Retenido',
'gs_transferred'=>'Transferido',
'st_denied_permission_to_edit'=>'Usted no tiene permiso para editar el',
'st_risk_remove_confirm'=>'¿Está seguro de que desea eliminar el riesgo <b>%risk_name%</b>?',
'st_risk_remove_has_incident'=>'<br><br><b>Atención: El riesgo tiene incidente(s) asociados.</b>',
'st_risk_remove_many_confirm'=>'¿Está seguro que desea eliminar los <b>%count%</b> riesgos seleccionados?',
'st_risk_remove_many_have_incident'=>'<br><br><b>Atención: Al menos un riesgo tiene incidente(s) asociados.</b>',
'tt_incident_filter'=>'filtrado por incidente \'<b>%incident_name%</b>\'',
'tt_remove_risk'=>'Eliminar Riesgo',

/* './packages/risk/nav_risk.xml' */

'cb_avoided'=>'Evitado',
'cb_not_treated'=>'No Tratado',
'cb_reduced'=>'Reducido',
'cb_restrained'=>'Retenido',
'cb_search_in_name_only'=>'buscar sólo en nombre',
'cb_transferred'=>'Transferido',
'gc_controls'=>'Controles',
'gc_residual'=>'Residual',
'gc_risk'=>'Riesgo',
'gc_risk_name'=>'Nombre',
'gc_status'=>'Estado',
'gc_treatment'=>'Tratamiento',
'lb_control_count_cl'=>'Número de Controles:',
'lb_name_description_cl'=>'Nombre/Descripción',
'lb_risk_residual_value_cl'=>'Residual:',
'lb_risk_value_cl'=>'Riesgo:',
'lb_treatment_type_cl'=>'Tipo de Tratamiento:',
'mi_avoid'=>'Riesgo Evitado',
'mi_cancel_treatment'=>'Cancelar Tratamiento',
'mi_hold_accept'=>'Retener/Aceptar Riesgo',
'mi_reduce_risk'=>'Reducir Riesgos',
'mi_transfer'=>'Transferir Riesgo',
'mi_view_controls'=>'Ver Controles',
'si_all_assets'=>'Todo',
'si_all_states'=>'Todos',
'st_from'=>'de',
'st_to'=>'a',
'vb_aply'=>'Aplicar',
'vb_parameterize'=>'Estimar',

/* './packages/risk/popup_area_edit.php' */

'tt_area_adding'=>'Adición de Área',
'tt_area_editing'=>'Edición de Área',

/* './packages/risk/popup_asset_dependencies.php' */

'st_dependencies_list'=>'Lista de Dependencias',
'st_dependents_list'=>'Lista de Dependientes',
'tt_dependencies_adjustment'=>'Ajuste de las dependencias',
'tt_dependents_adjustment'=>'Ajuste de los dependientes',

/* './packages/risk/popup_asset_dependencies.xml' */

'tt_assets_list'=>'Lista de Activos',

/* './packages/risk/popup_asset_edit.php' */

'lb_value_cl_money_symbol'=>'Costo: $',
'tt_asset_adding'=>'Alta de Activo',
'tt_asset_duplicate'=>'Duplicación de Activos',
'tt_asset_editing'=>'Edición de Activo',

/* './packages/risk/popup_asset_edit.xml' */

'lb_category_bl_cl'=>'<b>Categoría:</b>',
'lb_demands_attention_to_legal_conformity_cl'=>'Exige atención a la conformidad jurídica:',
'lb_justification_cl'=>'Motivo:',
'lb_security_responsible_bl_cl'=>'<b>Responsable de Seguridad:</b>',
'st_asset_parametrization_bl'=>'<b>Relevancia de Activo</b>',
'vb_adjust_dependencies'=>'Ajuste de las dependencias',
'vb_adjust_dependents'=>'Ajuste de los dependientes',
'wn_responsible_invalid'=>'Usuario responsable no válido.',
'wn_security_responsible_invalid'=>'Usuario responsable de la seguridad no válido.',

/* './packages/risk/popup_asset_search.xml' */

'wn_no_asset_selected'=>'No hay activo seleccionado.',

/* './packages/risk/popup_best_practice_search.xml' */

'lb_section_cl'=>'Sección:',
'si_all'=>'Todos',
'tt_best_practices_search'=>'Busqueda de Mejores Prácticas',

/* './packages/risk/popup_control_cost.php' */

'vb_save'=>'Guardar',

/* './packages/risk/popup_control_cost.xml' */

'tt_control_cost'=>'Costo de Control',

/* './packages/risk/popup_control_edit.php' */

'st_and'=>'y',
'st_of_revision'=>'de revisión',
'st_of_test'=>'de prueba',
'st_remove_control_implementation_confirm'=>'¿Está seguro de que desea eliminar la implementación del control <b>%control_name%</b>?',
'st_result_in_removing'=>'Esto resultará en eliminación',
'st_this_control'=>'este control',
'tt_control_adding'=>'Alta de Control',
'tt_remove_control_implementation'=>'Eliminar implementación de control',

/* './packages/risk/popup_control_edit.xml' */

'cb_enable_control_revision_cl'=>'Habilitar Revisión de Control:',
'cb_enable_control_tests_cl'=>'Habilitar Pruebas de Control:',
'lb_best_practices_bl_cl'=>'<b>Mejores Prácticas:</b>',
'lb_control_cost_cl'=>'Costo de Control:',
'lb_evidence_cl'=>'Evidencia:',
'lb_implementation_limit_bl_cl'=>'<b>Límite de Implementación:</b>',
'lb_implemented_on_cl'=>'Implementado en:',
'lb_send_alert_task_bl_cl'=>'<b>Envíar Alerta/Tarea:</b>',
'st_day_days_before'=>'día(s) antes',
'st_money_symbol'=>'$',
'tt_control_editing'=>'Edición de Control',
'vb_add'=>'Agregar',
'vb_control_cost'=>'Costo de Control',
'vb_implement'=>'Implementar',
'vb_remove_implementation'=>'Eliminar Implementación',
'vb_revision'=>'Revisión',
'vb_tests'=>'Pruebas',
'wn_implementation_cannot_after_today'=>'La fecha de aplicación no puede ser después de la fecha actual.',
'wn_implementation_cannot_be_null'=>'La fecha de implementación no puede ser nula, si desea eliminar la implementación, use el botón apropiado para ejecutar este evento.',
'wn_implementation_limit_after_today'=>'La fecha límite debe ser igual o posterior a la fecha actual.',

/* './packages/risk/popup_control_revision.xml' */

'lb_expected_efficiency_bl_cl'=>'<b>Eficacia Esperada:</b>',
'lb_high_value_5_cl'=>'(Alto) Valor 5:',
'lb_low_value_1_cl'=>'(Bajo) Valor 1:',
'st_helper'=>'[?]',
'to_control_revision_expected_efficiency'=>'<b>Eficacia Esperada:</b><br/><br/>Determina que nivel de servicio es esperado para el control en el período espefícido.<br/><br/>Considera la performace necesaria para reducir riesgos asociados a este control. (3163)',
'to_control_revision_metric_help'=>'<b>Métrica:</b><br/><br/>Defina como medir este control.',
'to_control_revision_value'=>'<b>Values:</b><br/><br/>Determine a scale for the possible service levels of this control in the period.<br/><br/>Consider the low values as a bad performance, and for higher values, the best possible performance for the control. (3164)',
'tt_control_efficiency_revision'=>'Revisión de Eficacia de Control',

/* './packages/risk/popup_control_revision_and_test_edit.php' */

'st_control_test_not_ok'=>'No Ok',
'st_control_test_ok'=>'Ok',
'st_revision_remove_confirm'=>'¿Está seguro de que desea eliminar la revisión con Fecha Estimada en <b>%deadline_date%</b> y Fecha de Realización en <b>%realized_date%</b> ?',
'st_revision_update_by_insert_confirm'=>'Hay una revisión de control en la Fecha Estimada en <b>%date_to_do%</b>. ¿Está seguro de que desea sustituír esta revisión de control?',
'st_test_remove_confirm'=>'¿Está seguro que desea eliminar la prueba Fecha Estimada en <b>%deadline_date%</b> Fecha de Realización en <b>%realized_date%</b> ?',
'st_test_update_by_insert_confirm'=>'Hay una prueba de control en la Fecha Estimada en <b>%date_to_do%</b>. ¿Está seguro de que desea sustituír esta prueba de control?',
'tt_control_efficiency_history_add_title'=>'Insertar Revisión de Control',
'tt_control_test_history_add_title'=>'Insertar Prueba de Control',
'tt_remove_control'=>'Eliminar Implementación de Control',
'tt_replace_control_revision'=>'Reemplazo de Revisión de Control <b>%control_name%</b>',
'tt_replace_control_test'=>'Reemplazo de Prueba de Control <b>%control_name%</b>',

/* './packages/risk/popup_control_revision_and_test_edit.xml' */

'gc_date_realized'=>'Fecha de realización',
'gc_date_to_do'=>'Fecha Estimada',
'gc_expected_efficiency'=>'EE',
'gc_real_efficiency'=>'ER',
'gc_realization_date'=>'Fecha de realización',
'gc_test_date_to_do'=>'Fecha Estimada',
'gc_test_value'=>'Valor de prueba',
'mi_edit'=>'Editar',
'mi_remove'=>'Eliminar',
'mi_revision_edit'=>'Editar Revisiones',
'mi_test_edit'=>'Editar Pruebas',
'rb_not_ok'=>'No OK',
'rb_ok'=>'Ok',
'st_control_test_result'=>'<b>Prueba:</b>',
'st_date_realized'=>'<b>Fecha de realización:</b>',
'st_date_to_do'=>'<b>Fecha Estimada:</b>',
'st_description'=>'Descripción:',
'st_er'=>'ER',
'test_date_to_do'=>'<b>Fecha Estimada</b>',
'tt_add_and_edit_revision_and_test_control'=>'Agregar o Editar Prueba de Control y Revisión',
'tt_control_efficiency_history_edit_title'=>'Editar Revisión de Control',
'tt_control_selected_revision_history'=>'Historia de Revisiones de Control',
'tt_control_selected_test_history'=>'Historia de pruebas de control',
'tt_control_test_history_edit_title'=>'Editar Prueba de Control',
'vb_insert'=>'Insertar',

/* './packages/risk/popup_control_revision_task.php' */

'tt_control_revision_task_incident_revision'=>'Medida Inducida de Control de Eficacia (por Incidente %incident_name%) (3149)',

/* './packages/risk/popup_control_revision_task.xml' */

'lb_from'=>'de',
'lb_start_cl'=>'Inicio:',
'lb_to'=>'a',
'lb_value_1_cl'=>'Valor 1:',
'lb_value_2_cl'=>'Valor 2:',
'lb_value_3_cl'=>'Valor 3:',
'lb_value_4_cl'=>'Valor 4:',
'lb_value_5_cl'=>'Valor 5:',
'st_ee'=>'EE',
'st_er_bl'=>'<b>ER</b>',
'st_high_parenthesis'=>'(Alto)',
'st_low_parenthesis'=>'(Bajo)',
'st_metric'=>'Métrica',
'st_total_incidents_found_on_period'=>'Total de incidentes encontrados en el período:',
'st_total_incidents_on_time'=>'Total de incidentes en el intérvalo de tiempo actual:',
'to_expected_efficiency'=>'Eficacia Esperada',
'to_real_efficiency'=>'Eficacia Real',
'tt_control_revision_task'=>'Revisión de Control',
'tt_incidents_occurred_from_period'=>'Incidentes ocurridos desde %date_begin% a %date_today%',
'vb_filter'=>'Filtro',

/* './packages/risk/popup_control_risk_edit.php' */

'tt_risk_control_association_editing'=>'Edición de Asociación de Riesgo de Control (3165)',

/* './packages/risk/popup_control_risk_edit.xml' */

'tt_risks_bl'=>'<b>Riesgos</b>',
'vb_associate_risk'=>'Asociar Riesgo',
'vb_disassociate_risks'=>'Desasociar Riesgos',

/* './packages/risk/popup_control_risk_search.xml' */

'gc_asset'=>'Activos',
'tt_risks_search'=>'Búsqueda de Riesgos',
'vb_associate_risks'=>'Asociar Riesgo',
'wn_no_risk_selected'=>'No hay riesgo seleccionado.',

/* './packages/risk/popup_control_search.xml' */

'vb_associate'=>'Asociar',

/* './packages/risk/popup_control_test.xml' */

'lb_schedule_bl_cl'=>'<b>Programación</b>',
'tt_control_test_revision'=>'Revisión de Prueba de Control',

/* './packages/risk/popup_control_test_task.xml' */

'cb_no'=>'No',
'cb_open_next_task'=>'Abrir próxima tarea',
'cb_yes'=>'Sí',
'lb_description_test'=>'Descripción de la prueba:',
'lb_observations_cl'=>'Observaciones:',
'lb_test_ok_bl_cl'=>'<b>Prueba ok:</b>',
'tt_control_test'=>'Prueba de Control',

/* './packages/risk/popup_create_risks_from_category.xml' */

'tt_risk_creation_from_category'=>'Creacion de Riesgos desde una categoría de evento',

/* './packages/risk/popup_create_risks_from_events.xml' */

'st_select_events_not_to_associate'=>'¡Seleccione a continuación sólo los eventos que usted <b>NO</ b>, quiere asociar a los activos!',
'tt_risk_creation_from_events'=>'Creacion de Riesgos desde Eventos',
'vb_link_events_later'=>'Link Events Later',
'vb_relate_events_to_asset'=>'Enlazar Evento a Activo',

/* './packages/risk/popup_process_asset_association.xml' */

'tt_assets_search'=>'Búsqueda de Activos',
'tt_current_assets_bl'=>'<b>Activos Actuales</b>',

/* './packages/risk/popup_process_edit.php' */

'st_no_published_documents'=>'No hay documentos publicados',
'tt_process_adding'=>'Alta de Proceso',
'tt_process_editing'=>'Edición de Proceso',

/* './packages/risk/popup_process_edit.xml' */

'lb_area_bl_cl'=>'<b>Área:</b>',
'lb_document_cl'=>'Documento:',
'lb_name_bl_cl'=>'<b>Nombre:</b>',
'lb_priority_cl'=>'Prioridad:',
'lb_responsible_bl_cl'=>'<b>Responsable:</b>',
'vb_associate_users'=>'Asociar Usuarios',
'vb_properties'=>'Propiedades',
'vb_view'=>'Ver',

/* './packages/risk/popup_process_search.xml' */

'gc_area'=>'Área',
'si_all_areas'=>'Todas las Áreas',
'tt_current_processes_bl'=>'<b>Procesos Actuales</b>',
'tt_processes_search'=>'Búsqueda de Procesos',
'vb_remove'=>'Eliminar',

/* './packages/risk/popup_risk_control_association_edit.php' */

'st_control_not_mitigating_efficiency_message'=>'¡El Control no está reduciendo Riesgo porque la Eficacia Real es más baja que la Eficacia Esperada!',
'st_control_not_mitigating_not_implemented_message'=>'¡El Control no está reduciendo Riesgos porque este no está implementado y la fecha de implementación ha expirado!',

/* './packages/risk/popup_risk_control_association_edit.xml' */

'lb_control_cl'=>'Control:',
'st_control_parametrization_bl_cl'=>'<b>Estimación de Control:</b>',
'st_values_to_reduce_risk_consequence_and_probability'=>'Los valores en el lado definirán  por cuanto de este control reducirá la consecuencia y la probabilidad  del riesgo actual. (3197)',
'tt_risk_reduction'=>'Reducción de Riesgo',

/* './packages/risk/popup_risk_control_edit.php' */

'tt_control_risk_association_editing'=>'Edición de Asociación Riesgo-Control',

/* './packages/risk/popup_risk_control_edit.xml' */

'lb_risk_cl'=>'Riesgo:',
'mi_edit_association'=>'Editar Asociación',
'st_residual'=>'Residual',
'st_risk'=>'Riesgo',
'tt_controls_bl'=>'<b>Controles</b>',
'vb_disassociate_controls'=>'Desasociar Controles',

/* './packages/risk/popup_risk_control_search.xml' */

'cb_search_only_suggested_controls'=>'Búscar sólo los Controles sugeridos',
'gc_name'=>'Nombre',
'lb_name_cl'=>'Nombre:',
'tt_control_search'=>'Búsqueda de Control',
'vb_associate_control'=>'Asociar Control',
'wn_no_control_selected'=>'No hay control seleccionado.',

/* './packages/risk/popup_risk_edit.php' */

'st_sensitive_data_reapproval'=>'Usted está cambiando datos sensibles que requieren reaprobación. ¿Desea confirmar cambios?',
'tt_reapproval'=>'Reaprovación',
'tt_risk_adding'=>'Alta de Riesgo',
'tt_risk_editing'=>'Edición de Riesgo',

/* './packages/risk/popup_risk_edit.xml' */

'lb_asset_bl_cl'=>'<b>Activo:</b>',
'lb_event_bl_cl'=>'<b>Evento:</b>',
'lb_impact_cl_money_symbol'=>'Impacto Financiero:',
'lb_real_risk_bl_cl'=>'<b>Riesgo Potencial:</b>',
'lb_residual_risk_bl_cl'=>'<b>Riesgo Residual:</b>',
'lb_type_cl'=>'Tipo:',
'si_select_one_of_the_items_below'=>'Selecione uno de los items de abajo',
'st_risk_parametrization_bl_cl'=>'<b>Estimación de Riesgo:</b>',
'static_static19'=>'Impacto:',
'vb_calculate_risk'=>'Calcular Riesgo',
'vb_config'=>'Configurar',
'vb_find'=>'Encontrar',
'vb_probability_automatic_calculation'=>'Cálculo automático de probabilidad',
'warning_wn_asset_desparametrized'=>'¡No es posible calcular el valor de riesgo, porque el activo vinculado al riesgo no está estimado!',
'warning_wn_control_desparametrized'=>'al menos un control no está estimado. Antes de calcular el riesgo residual, estimar el/los control(es) vinculados a este riesgo!',

/* './packages/risk/popup_risk_insert_search_event_asset.xml' */

'gc_description'=>'Descripción',
'gc_impact'=>'Impacto',
'lb_category_cl'=>'Categoría:',
'lb_description_cl'=>'Descripción',
'si_all_categories'=>'Todas las Categorías',
'tt_events_search'=>'Búsqueda de Eventos',
'vb_create_risk'=>'Crear Riesgo',
'vb_search'=>'Buscar',
'wn_no_event_selected'=>'No hay un evento seleccionado.',

/* './packages/risk/popup_risk_insert_type.xml' */

'rb_create_independent_risk'=>'Crear un riesgo independiente',
'st_which_action_to_take'=>'¿Qué acción desea tomar?',
'vb_cancel'=>'Cancelar',
'vb_create_risk_from_event'=>'Crear riesgo desde evento (recomendado)',

/* './packages/risk/popup_risk_parameters_weight.xml' */

'tt_risk_parameters_weight'=>'Peso de Parámetros de Riesgo',

/* './packages/risk/popup_risk_parametrization.php' */

'mx_probability'=>'Probabilidad',
'st_denied_permission_to_estimate_risks'=>'No tiene permiso para estimar múltimes riesgos',
'to_impact_cl'=>'Impacto:',
'to_risk_cl'=>'Riesgo:',

/* './packages/risk/popup_risk_parametrization.xml' */

'lb_all_bl_cl'=>'<b>Todas:</b>',
'tt_risk_parametrization'=>'Estimación de Riesgo',

/* './packages/risk/popup_risk_treatment.php' */

'tt_avoid'=>'evitar',
'tt_hold_accept'=>'retener/aceptar',
'tt_transfer'=>'transferir',

/* './packages/risk/popup_risk_treatment.xml' */

'lb_justification_bl_cl'=>'<b>Razón:</b>',
'st_accept_transfer_or_avoid_risk'=>'¿Desa <b>%treatment_type%</b> el Riesgo \'<b>%risk_name%</b>\'?',
'tt_risk_treatment'=>'Tratamiento de Riesgo (<b>%treatment_type%</b>)',

/* './packages/risk/popup_risks_control_association_edit.xml' */

'lb_all_risks_bl_cl'=>'<b>Todos los Riesgos:</b>',
'lb_control_bl_cl'=>'<b>Control:</b>',
'tt_risk_control_association_parametrization'=>'Reducción de Riesgo',

/* './packages/risk/report/clock.php' */

'st_generating_report'=>'Generando Informe...',
'st_operation_wait'=>'<b>Atención:</b> Este informe puede tardar unos minutos para ser creado.<br> Por favor, espere.',

/* './packages/risk/report/loading_report.php' */

'st_verifying_existing_reports'=>'Verificando Informes Existentes',

/* './packages/risk/report/popup_report_areas.php' */

'mx_areas'=>'Áreas',

/* './packages/risk/report/popup_report_areas.xml' */

'rs_header_area_name'=>'Nombre',
'rs_header_area_responsible'=>'Responsable',

/* './packages/risk/report/popup_report_areas_by_priority.php' */

'rs_areas_by_priority'=>'Áreas por Prioridad',

/* './packages/risk/report/popup_report_areas_by_type.php' */

'rs_areas_by_type'=>'Áreas por Tipo',

/* './packages/risk/report/popup_report_areas_without_processes.php' */

'rs_areas_without_processes'=>'Áreas sin Procesos',

/* './packages/risk/report/popup_report_asset.php' */

'rs_assets'=>'Activos',

/* './packages/risk/report/popup_report_asset_deps.php' */

'si_assets_deps'=>'Activo - Dependencias y Dependientes',
'si_dependencies'=>'Dependencies',
'si_dependents'=>'Dependents',

/* './packages/risk/report/popup_report_asset_importance.php' */

'rs_assets_importance'=>'Importancia de Activos',

/* './packages/risk/report/popup_report_asset_relevance.php' */

'rs_asset_cl_bl'=>'<b>Activo:</b>',
'si_asset_relevance'=>'Relevancia de activos',

/* './packages/risk/report/popup_report_asset_relevance.xml' */

'rs_responsible_cl'=>'Responsable:',

/* './packages/risk/report/popup_report_assets.php' */

'mx_assets'=>'Activos',

/* './packages/risk/report/popup_report_assets.xml' */

'rs_header_asset_name'=>'Nombre',
'rs_header_asset_responsible'=>'Responsable',

/* './packages/risk/report/popup_report_assets_without_risks.php' */

'rs_assets_without_risks'=>'Activos sin Eventos de Riesgo',

/* './packages/risk/report/popup_report_assets_without_risks.xml' */

'rs_security_responsible'=>'Responsable de Seguridad',

/* './packages/risk/report/popup_report_best_practices.php' */

'mx_best_practices'=>'Mejores Prácticas',

/* './packages/risk/report/popup_report_best_practices.xml' */

'rs_best_practice_name'=>'Nombre',

/* './packages/risk/report/popup_report_conformity.php' */

'rs_applied_best_practices'=>'Mejores Prácticas Aplicadas',
'rs_not_applied_best_practices'=>'Mejores Prácticas No Aplicadas',
'rs_not_used'=>'No Usado',
'rs_not_used_controls'=>'Controles No Usados',
'rs_report_conformity_footer'=>'Total de Mejores Prácticas = %total% / Total de Mejores Prácticas Aplicadas = %applied%',
'rs_used_controls'=>'Controles Usados',
'tt_conformity'=>'Conformidad',

/* './packages/risk/report/popup_report_control_accompaniment.php' */

'rs_follow_up_of_controls'=>'Seguimiento de Controles',

/* './packages/risk/report/popup_report_control_by_process.php' */

'rs_general_risk'=>'Riesgo General',

/* './packages/risk/report/popup_report_control_by_process.xml' */

'cb_best_practices'=>'Mejores Prácticas',
'cb_control'=>'Control',
'gc_category'=>'Categoría',
'gc_type'=>'Tipo',
'lb_estimation'=>'Estimación de Riesgos',
'lb_evidence'=>'Evidencia',
'lb_process'=>'Activo',
'lb_relavance'=>'Relevancia',

/* './packages/risk/report/popup_report_control_cost.php' */

'rs_cost_per_control'=>'Costo por Control',

/* './packages/risk/report/popup_report_control_cost_by_area.php' */

'rs_cost_of_controls_per_area'=>'Costo de Controles por Área',

/* './packages/risk/report/popup_report_control_cost_by_asset.php' */

'rs_cost_of_controls_per_asset'=>'Costo de Controles por Activo',

/* './packages/risk/report/popup_report_control_cost_by_cost_category.php' */

'mx_control_cost_by_cost_category'=>'Costo de Controles por Categoría de Costo',

/* './packages/risk/report/popup_report_control_cost_by_cost_category.xml' */

'rs_control_cost'=>'Costo',
'rs_control_name'=>'Nombre',
'rs_total_cost'=>'Total',

/* './packages/risk/report/popup_report_control_cost_by_process.php' */

'rs_cost_of_controls_per_process'=>'Costo de Controles por Proceso',

/* './packages/risk/report/popup_report_control_cost_by_responsible.php' */

'rs_cost_of_controls_per_responsible'=>'Costo de Controles por Responsable',

/* './packages/risk/report/popup_report_control_cost_by_responsible.xml' */

'rs_cost'=>'Costo',

/* './packages/risk/report/popup_report_control_efficiency.php' */

'rs_controls_efficiency'=>'Eficacia de Controles',
'rs_efficient_controls'=>'Controles Eficientes',
'rs_inefficient_controls'=>'Controles No Eficientes',

/* './packages/risk/report/popup_report_control_efficiency_accompaniment.php' */

'rs_follow_up_of_control_efficiency'=>'Seguimiento de los Controles de Eficacia',

/* './packages/risk/report/popup_report_control_efficiency_accompaniment.xml' */

'rs_ee'=>'EE (2354)',
'rs_re'=>'RE (3276)',

/* './packages/risk/report/popup_report_control_implementation_date.php' */

'rs_controls_implemented_outside_stated_period'=>'Controles implementados Fuera del período',
'rs_controls_implemented_within_stated_period'=>'Controles implementados en período',
'rs_date_of_implementation_of_controls'=>'Fecha de Implementación de Control',

/* './packages/risk/report/popup_report_control_implementation_date.xml' */

'rs_implementation'=>'Implementación',

/* './packages/risk/report/popup_report_control_planning.php' */

'rs_implementation_date'=>'Fecha de Implementación',
'rs_implementation_deadline'=>'Fecha límite de Implementación',
'rs_implemented_controls'=>'Controles Implementados',
'rs_planned_controls'=>'Controles Planeados',
'rs_planning_of_controls'=>'Planificación de Controles',

/* './packages/risk/report/popup_report_control_revision_agenda_by_responsible.php' */

'si_control_revision_agenda_by_responsible'=>'Orden del día para la revisión de los controles por el propietario',

/* './packages/risk/report/popup_report_control_revision_agenda_by_responsible.xml' */

'rs_next_revision'=>'Próxima Revisión',

/* './packages/risk/report/popup_report_control_summary_pm.php' */

'rs_best_practice'=>'Mejor Práctica',
'rs_document'=>'Documento',
'rs_summary_of_controls'=>'Resumen de Controles',

/* './packages/risk/report/popup_report_control_summary_pm.xml' */

'rs_accomplishment_bl_cl'=>'<b>Realización:</b>',
'rs_description_cl'=>'<b>Descripción:</b>',

/* './packages/risk/report/popup_report_control_test_accompaniment.php' */

'rs_ctrl_test_not_ok'=>'No Ok',
'rs_ctrl_test_ok'=>'Ok',
'rs_follow_up_of_control_test'=>'Seguimiento de los Controles de Prueba',

/* './packages/risk/report/popup_report_control_test_accompaniment.xml' */

'rs_comment'=>'Comentario',
'rs_control_cl'=>'Control:',
'rs_date_accomplishment'=>'Fecha de Realización',
'rs_predicted_date'=>'Fecha prevista',
'rs_test'=>'Prueba',

/* './packages/risk/report/popup_report_control_test_agenda_by_responsible.php' */

'si_control_test_agenda_by_responsible'=>'Programa de prueba a oficial de controles',

/* './packages/risk/report/popup_report_control_test_agenda_by_responsible.xml' */

'rs_next_test'=>'Next Test',
'rs_processes'=>'Processes',
'rs_responsibles'=>'Responsibles',
'rs_schedule'=>'Programación',

/* './packages/risk/report/popup_report_controls.php' */

'mx_controls'=>'Controles',

/* './packages/risk/report/popup_report_controls.xml' */

'rs_header_control_name'=>'Nombre',
'rs_header_control_responsible'=>'Responsable',

/* './packages/risk/report/popup_report_controls_by_responsible.php' */

'rs_controls_per_responsible'=>'Controles por Responsable',

/* './packages/risk/report/popup_report_controls_by_risk.php' */

'rs_controls_per_risk'=>'Controles por Riesgo',

/* './packages/risk/report/popup_report_controls_by_asset.php' */
'rs_controls_per_asset'=>'Controles por Activos',

/* './packages/risk/report/popup_report_controls_by_asset.xml' */

'rs_controls'=>' Controles:',

/* './packages/risk/report/popup_report_controls_by_risk.xml' */

'rs_affected_asset_bl_cl'=>'<b>Activo Afectado:</b>',
'rs_risk_and_impact_bl_cl'=>'<b>Riesgo / Impacto:</b>',
'rs_risk_bl_cl'=>'<b>Riesgo:</b>',

/* './packages/risk/report/popup_report_controls_by_type.php' */

'rs_controls_by_type'=>'Controles por Tipo',

/* './packages/risk/report/popup_report_controls_not_measured.php' */

'rs_not_measured_controls'=>'Controles No Medidos',

/* './packages/risk/report/popup_report_controls_not_measured.xml' */

'rs_efficiency'=>'Eficacia',

/* './packages/risk/report/popup_report_controls_without_risks.php' */

'rs_controls_without_risks'=>'Controles sin Riesgos Asociados',

/* './packages/risk/report/popup_report_detailed_pendant_tasks.php' */

'rs_detailed_pending_tasks'=>'Tareas pendientes Detalladas',

/* './packages/risk/report/popup_report_duplicated_risks.php' */

'tt_duplicated_risks'=>'Duplicar Riesgos',

/* './packages/risk/report/popup_report_duplicated_risks.xml' */

'rs_risk_cl'=>'Riesgo:',

/* './packages/risk/report/popup_report_duplicated_risks_by_asset.php' */

'tt_duplicated_risks_by_asset'=>'Duplicar Riesgos por Activo',

/* './packages/risk/report/popup_report_duplicated_risks_by_asset.xml' */

'rs_risk'=>'Riesgo',

/* './packages/risk/report/popup_report_events_by_category.php' */

'rs_checklist_by_event'=>'Checklist por Activo',

/* './packages/risk/report/popup_report_events_by_category.xml' */

'rs_brackets'=>'[ ]',
'rs_event'=>'Evento',
'rs_na'=>'NA',
'rs_nc'=>'NC',
'rs_ok'=>'Ok',

/* './packages/risk/report/popup_report_events_by_type.php' */

'rs_events_by_type'=>'Eventos por Tipo',

/* './packages/risk/report/popup_report_pendant_contexts.php' */

'rs_pending_items'=>'Items Pendientes',

/* './packages/risk/report/popup_report_pendant_contexts.xml' */

'rs_approver'=>'Aprobadores',

/* './packages/risk/report/popup_report_policy.php' */

'rs_sgsi_policy_statement'=>'Declaración de Política de SGSI',

/* './packages/risk/report/popup_report_policy.xml' */

'rs_system_policy'=>'Política de Sistema',

/* './packages/risk/report/popup_report_processes.php' */

'mx_processes'=>'Procesos',

/* './packages/risk/report/popup_report_processes.xml' */

'rs_header_process_name'=>'Nombre',
'rs_header_process_responsible'=>'Responsable',

/* './packages/risk/report/popup_report_processes_by_priority.php' */

'rs_processes_by_priority'=>'Procesos por Prioridad',

/* './packages/risk/report/popup_report_processes_by_priority.xml' */

'rs_priority_cl'=>'Priodad:',

/* './packages/risk/report/popup_report_processes_by_type.php' */

'rs_processes_by_type'=>'Procesos por Tipo',

/* './packages/risk/report/popup_report_processes_without_assets.php' */

'rs_processes_without_assets'=>'Procesos sin Activos',

/* './packages/risk/report/popup_report_risk_amount_by_area.php' */

'rs_amount_of_risks_per_area'=>'Cantidad de Riesgos por Área',

/* './packages/risk/report/popup_report_risk_amount_by_process.php' */

'rs_amount_of_risks_per_process'=>'Cantidad de Riesgos por Proceso',

/* './packages/risk/report/popup_report_risk_financial_impact.php' */

'mx_risk_financial_impact'=>'Impacto de Riesgo Financiero',

/* './packages/risk/report/popup_report_risk_financial_impact.xml' */

'rs_risk_name'=>'Nombre',
'rs_risk_residual_value'=>'Riesgo Residual',
'rs_risk_value'=>'Riesgo Potencial',
'rs_total_impact'=>'Total',

/* './packages/risk/report/popup_report_risk_impact.php' */

'rs_risk_impact'=>'Impacto del Riesgo',

/* './packages/risk/report/popup_report_risk_status_by_area.php' */

'rs_risk_status_per_area'=>'Estado de Riesgo por Área',

/* './packages/risk/report/popup_report_risk_status_by_area.xml' */

'rs_area'=>'Área',

/* './packages/risk/report/popup_report_risk_status_by_process.php' */

'rs_risk_status_per_process'=>'Estado de Riesgo por Proceso',

/* './packages/risk/report/popup_report_risk_status_by_process.xml' */

'rs_process'=>'Proceso',

/* './packages/risk/report/popup_report_risk_treatment_plan.php' */

'rs_and'=>'y',
'rs_avoided'=>'Evitado',
'rs_not_treated'=>'No Tratado',
'rs_plan_of_risks_treatment'=>'Plan de Tratamiento de Riesgo',
'rs_reduced'=>'Reducido',
'rs_restrained'=>'Retenido',
'rs_transferred'=>'Transferido',

/* './packages/risk/report/popup_report_risk_treatment_plan.xml' */

'rs_asset_bl_cl'=>'<b>Activo:</b>',
'rs_status'=>'Estado',

/* './packages/risk/report/popup_report_risk_values.php' */

'rs_risks_values'=>'Valores de Riesgos',

/* './packages/risk/report/popup_report_risk_values.xml' */

'rs_affected_asset'=>'Activo Afectado',

/* './packages/risk/report/popup_report_risks.php' */

'mx_risks'=>'Riesgos',

/* './packages/risk/report/popup_report_risks_by_area.php' */

'rs_risks_per_area'=>'Riesgos por Área',

/* './packages/risk/report/popup_report_risks_by_asset.php' */

'rs_risks_per_asset'=>'Riesgos por Activo',

/* './packages/risk/report/popup_report_risks_by_control.php' */

'rs_report_of_risk_per_control'=>'Riesgos por Control',

/* './packages/risk/report/popup_report_risks_by_control.xml' */

'rs_control_bl_cl'=>'<b>Control:</b>',

/* './packages/risk/report/popup_report_risks_by_process.php' */

'rs_risks_per_process'=>'Riesgos por Procesos',

/* './packages/risk/report/popup_report_risks_by_type.php' */

'rs_risks_by_type'=>'Riesgo por Tipo',

/* './packages/risk/report/popup_report_risks_by_type.xml' */

'rs_type_cl'=>'Tipo:',

/* './packages/risk/report/popup_report_risks_resume.php' */

'cb_very_high' => 'Muy Alto',
'cb_very_low' => 'Muy Bajo',
'rs_very_high' => 'Muy Alto',
'rs_very_low' => 'Muy Bajo',
'cb_high'=>'Alto',
'cb_low'=>'Bajo',
'cb_medium'=>'Medio',
'cb_not_parameterized'=>'No estimado',
'gc_capability'=>'Potencial',
'lb_indicators'=>'Indicadores',
'lb_total'=>'Total',
'rb_not_treated'=>'No Tratado',
'rb_residual'=>'Residual',
'rb_treated'=>'Tratado',
'tt_general_summary_of_risks'=>'Riesgo Estimado vs. No Estimado',
'tt_risk_management_status'=>'Nivel de Riesgo',
'tt_summary_of_parameterized_risks'=>'Riesgo Potencial vs. Riesgo Residual',

/* './packages/risk/report/popup_report_scope.php' */

'rs_sgsi_scope_statement'=>'Declaración de Alcance del SGSI',

/* './packages/risk/report/popup_report_scope.xml' */

'rs_system_scope'=>'Alcance del Sistema',

/* './packages/risk/report/popup_report_statement_of_applicability.php' */

'rs_applied'=>'Aplicado',
'rs_no_risks_application_control'=>'Hasta ahora no han sido identificado Riesgos que justifiquen la aplicación de Control',
'rs_not_applied'=>'No Aplicado',
'rs_reduction_of_risks'=>'Reducción de Riesgos.',
'rs_statement_of_applicability'=>'Declaración de Aplicabilidad',
'rs_statement_of_applicability_pre_report'=>'Pre-Informe de Declaración de Aplicabilidad',

/* './packages/risk/report/popup_report_statement_of_applicability.xml' */

'rs_applicability_cl'=>'Aplicabilidad:',
'rs_applicability'=>'Aplicabilidad',
'rs_best_practice_cl'=>'Mejor Práctica:',
'rs_control'=>'Control',
'rs_justification_cl'=>'Razón:',

/* './packages/risk/report/popup_report_statement_of_applicability_justificative.xml' */

'tt_justification'=>'Razón',
'vb_close'=>'Cerrar',
'vb_confirm'=>'Corfirmar',

/* './packages/risk/report/popup_report_summarized_pendant_tasks.php' */

'rs_summarized_pending_tasks'=>'Tareas Pendientes Resumidas',

/* './packages/risk/report/popup_report_top10_assets_with_more_risks.php' */

'rs_top_10_assets_with_highest_risks'=>'Top 10 Activos con los más Altos Riesgos',
'rs_top_10_assets_with_most_risks'=>'Top 10 Activos con más Riesgos',

/* './packages/risk/report/popup_report_top10_assets_with_more_risks.xml' */

'rs_amount_of_risks'=>'Cantidad de Riesgos',
'rs_asset'=>'Activos',

/* './packages/risk/report/popup_report_top10_risks_by_area.php' */

'rs_top_10_risks_per_area'=>'Top 10 Riesgos por Área',

/* './packages/risk/report/popup_report_top10_risks_by_area.xml' */

'rs_area_bl_cl'=>'<b>Área:</b>',

/* './packages/risk/report/popup_report_top10_risks_by_process.php' */

'rs_top_10_risks_per_process'=>'Top 10 Riesgos por Proceso',

/* './packages/risk/report/popup_report_top10_risks_by_process.xml' */

'rs_process_bl_cl'=>'<b>Proceso:</b>',
'rs_risk_and_impact'=>'Riesgo / Impacto',

/* './packages/risk/report/popup_report_user_responsabilities.php' */

'rs_security_resp'=>'Responsable de Seguridad',
'rs_special_user'=>'Usuario Especial',
'rs_users_responsibilities'=>'Responsabilidades de Usuarios',

/* './packages/risk/report/popup_report_user_responsabilities.xml' */

'rs_responsible_bl_cl'=>'<b>Responsable:</b>',

/* './packages/risk/report/report_template.xml' */

'rs_comment_cl'=>'Comentario:',
'rs_end_of_report'=>'Fin del Informe',

/* './packages/risk/tab_risk_management.xml' */

'ti_area'=>'Área',
'ti_asset'=>'Activo',
'ti_controls'=>'Controles',
'ti_process'=>'Proceso',
'ti_reports'=>'Informes',
'ti_risk'=>'Riesgo',

/* './popup_choose_role.php' */

'mx_user_doesnt_have_permission_to_login'=>'El usuario <b>%user_name%</b> no tiene permisos para acceder al sistema.',
'tt_login_error'=>'Falló el Ingreso al Sistema',

/* './popup_choose_role.xml' */

'lb_represent_bl_cl'=>'<b>Representar:</b>',
'st_solicitor_explanation'=>'Si desea representar alguno de esos usuarios, presione Ok.',
'tt_solicitor_mode'=>'Modo Procurador (3394)',

/* './popup_email_preferences.xml' */

'cb_action_plan'=>'Plan de Acción',
'cb_areas'=>'Áreas',
'cb_assets'=>'Activos',
'cb_controls'=>'Controles',
'cb_documents'=>'Documentos',
'cb_events'=>'Eventos',
'cb_incidents'=>'Incidentes',
'cb_non_conformities'=>'No Conformidades',
'cb_occurrences'=>'Ocurrencias',
'cb_others'=>'Otros',
'cb_processes'=>'Procesos',
'cb_risks'=>'Riesgos',
'cb_select_all'=>'Seleccionar Todo',
'cb_standards'=>'Estándares',
'lb_warning_types_to_receive_cl'=>'Selecione cuales alarmas desea recibir:',
'rb_html_email'=>'E-mail con HTML',
'rb_messages_digest_complete'=>'Completo (un e-mail diario con mensajes completos)',
'rb_messages_digest_none'=>'No (un e-mail por cada mensaje)',
'rb_messages_digest_subjects'=>'Asuntos (un e-mail diario con asuntos de los mensajes solamente)',
'rb_text_email'=>'E-mail con Texto Plano',
'st_email_preferences_bl'=>'<b>Preferencias de e-mail</b>',
'st_messages_digest_type_cl'=>'Tipo de resumen de mensajes:',

/* './popup_help.xml' */

'cb_do_not_show_again'=>'No mostrar este mensaje nuevamente',
'tt_tip_bl'=>'<b>Consejo</b>',

/* './popup_password_request.xml' */

'lb_user_login_cl'=>'Usuario:',
'st_password_request'=>'Para obtener una nueva contraseña, introduzca su usuario. Una nueva contraseña será enviada a su correo electrónico.',
'st_warning_general_email_disabled'=>'El sistema de e-mail está deshabilitado, entonces es imposible generar una nueva contraseña.',
'tt_password_reminder'=>'Nueva Solicitud de Contraseña',
'vb_new_password'=>'Nueva contraseña',
'wn_password_sent'=>'La nueva contraseña fue enviada al usuario, siempre y cuando el usuario exista.',

/* './popup_risk_limits_task_view.xml' */

'lb_risk_limits_high_cl'=>'Tolerancia a Riesgo 2:',
'lb_risk_limits_low_cl'=>'Tolerancia a Riesgo 1:',
'tt_risk_limits_approval'=>'Aprobar Limites de Riesgo',

/* './popup_saas_disclaimer.xml' */

'tt_disclaimer_popup_title'=>'Disclaimer',
'vb_agree'=>'Estoy de acuerdo',
'vb_disagree'=>'No estoy de acuerdo',

/* './popup_schedule_edit.xml' */

'lb_april'=>'Abril',
'lb_august'=>'Agosto',
'lb_december'=>'Diciembre',
'lb_february'=>'Febrero',
'lb_january'=>'Enero',
'lb_july'=>'Julio',
'lb_june'=>'Junio',
'lb_march'=>'Marzo',
'lb_may'=>'Mayo',
'lb_november'=>'Noviembre',
'lb_october'=>'Octubre',
'lb_schedule_days_to_finish'=>'Días para realizar tarea:',
'lb_schedule_end'=>'Finalizar en:',
'lb_schedule_periodicity'=>'Cada',
'lb_schedule_start'=>'Empezar en:',
'lb_schedule_type_cl'=>'Tipo:',
'lb_september'=>'Septiembre',
'lb_week_days'=>'En los siguientes días:',
'mx_second_week'=>'segundo',
'si_schedule_by_day'=>'diariamente',
'si_schedule_by_month'=>'Mensualmente',
'si_schedule_by_week'=>'Semanalmente',
'si_schedule_month_day'=>'Día del mes',
'si_schedule_week_day'=>'Día de la semana',
'st_months'=>'meses',
'st_never'=>'nunca',
'st_schedule_months'=>'En los siguientes meses:',
'st_weeks'=>'semanas',
'tt_schedule_edit'=>'Programación de tareas',
'tx_1'=>'1',
'tx_schedule_days_to_finish'=>'1',

/* './popup_solicitor_conflict.xml' */

'st_solicitor_conflict'=>'El usuario <b>%solicitor_name%</b> ha sido nominado por usted para ser su Procurador en su período de ausencia. ¿Usted quisiera finalizar este vínculo? (3462)',
'tt_solicitor_conflict'=>'Confligto de Procurador (3463)',

/* './popup_survey.xml' */

'st_survey_text'=>'"Luego de evaluar el sistema, no olvide responder nuestro cuestonario de calidad. Puede acceder a este a traves del enlace ""Cuestionario de Experiencia de Usuario"" en el pie de sistema.¿Desea responder a este ahora?"',
'tt_survey_bl'=>'<b>Encuesta</b>',

/* './popup_unexpected_behavior.xml' */

'st_unexpected_behavior_message'=>'Un comportamiento inesperado en el sistema ha sido detectado. Un email con información detallada acerda de lo que ha ocurrido has sido enviado a nuestro equipo de Soporte Técnico. Si es necesario, cierre el navegador web e ingrese al sistema de nuevo.',

/* './popup_unexpected_behavior_onsite.xml' */

'tt_unexpected_behavior'=>'Comportamiento Inesperado',

/* './popup_user_change_password.php' */

'st_user_password_change'=>'Debe cambiar la contraseña ahora.',
'st_user_password_expired'=>'<b>¡Su contraseña ha expirado!</b><br/>Debe cambiarla ahora.',
'st_user_password_will_expire'=>'Su contraseña expirará en %days% día(s). Si lo desea, usted puede cambiarla.',

/* './popup_user_change_password.xml' */

'lb_confirm_cl'=>'Confirmar:',
'tt_change_password'=>'Cambiar Contraseña',
'vb_change_password'=>'Cambiar Contraseña',
'wn_unmatching_password'=>'Las contraseñas no coinciden.',

/* './popup_user_dashboard_preference.php' */

'tt_average_time_for_documents_approval'=>'Tiempo Aprobación Promedio de Documentos',
'tt_average_time_for_my_documents_approval'=>'Tiempo de Aprobación Promedio de Mis Documentos',
'tt_financial_summary'=>'Resumen Financiero',
'tt_grid_abnormality_ci_title'=>'Resumen de Anormalidades de la Mejora Contínua',
'tt_grid_abnormality_pm_title'=>'Resumen de Anormalidades de la Gestión de Políticas',
'tt_grid_incident_summary_title'=>'Resumen de Incidentes',
'tt_grid_incidents_per_asset_title'=>'Top 5 Activos con más Incidentes',
'tt_grid_incidents_per_process_title'=>'Top 5 Procesos con más No Incidentes',
'tt_grid_incidents_per_user_title'=>'Top 5 Usuarios con más Procesos Disciplinarios',
'tt_grid_nc_per_process_title'=>'Top 5 Procesos con más No Conformidades',
'tt_grid_nc_summary_title'=>'Resumen de No Conformidades',
'tt_my_items'=>'Mis Items',
'tt_my_pendencies'=>'Mis Pendientes desde la Gestión de Riesgo',
'tt_my_risk_summary'=>'Resumen de Mis Riesgos',
'tt_risk_management_my_items_x_policy_management'=>'Gestión de Riesgos (Mis Items) x Gestión de Política',
'tt_risk_management_x_policy_management'=>'Documentos por Elemento de Negocio',
'tt_sgsi_reports_and_documents'=>'Documentos Mandatorios del SGSI ISO 27001',
'tt_summary_of_abnormalities'=>'Resumen de Anormalidades de la Gestión de Riesgo',
'tt_summary_of_best_practices'=>'Mejores Prácticas - Índice de Cumplimiento',
'tt_summary_of_controls'=>'Resumen de Controles',
'tt_summary_of_my_documents_last_revision_average_time'=>'Resumen de Último Período de Revisión de Documentos',
'tt_summary_of_policy_management'=>'Resumen de Gestión de Política',
'tt_summary_of_policy_management_my_documents'=>'Resumen de Gestión de Políticas (Mis Documentos)',
'tt_summary_of_risk_management_documentation'=>'Resumen de la Documentación de Gestión de Riesgo',
'tt_summary_of_the_risk_management_from_my_items_documentation'=>'Resumen de Gestión de Riesgos de Mis Documentación de Items (3478)',
'tt_top_10_most_read_documents'=>'Top 10 Documentos más leídos',
'tt_top_10_most_read_my_documents'=>'Top 10 Mis Documentos Más Leídos',

/* './popup_user_dashboard_preference.xml' */

'st_all_elements_not_selected'=>'Todos los Items (no seleccionados)',
'st_selected_elements_cl'=>'Items seleccionados:',
'tt_summary_preferences_editing'=>'Edición de Preferencias de Resumen',

/* './popup_user_preferences.php' */

'si_email'=>'Email',
'si_general'=>'General',

/* './popup_user_preferences.xml' */

'lb_preferences_cl'=>'Preferencias',
'tt_edit_preferences'=>'Editar Preferencias',

/* './popup_user_profile.xml' */

'cb_change_password'=>'Cambiar Contraseña',
'lb_current_password_cl'=>'Contraseña Actual:',
'lb_dashboard_preferences_cl'=>'Preferencias del Panel:',
'lb_general_config_bl'=>'<b>Configuraciones Generales</b>',
'lb_general_config_bl_cl'=>'<b>Configuración General</b>',
'lb_language_cl'=>'Lenguaje:',
'lb_password_confirm_cl'=>'Confirmación de la Contraseña:',
'lb_solicitor_cl'=>'Procurador: (3525)',
'lb_solicitor_finish_date_cl'=>'Fin de Procuración: (3529)',
'lb_solicitor_start_date_cl'=>'Comienzo de Procuración: (3526)',
'st_change_password_sub_title_bl'=>'<b>Cambio de Contraseña</b>',
'st_solicitor_sub_title_bl'=>'<b>Procurador</b> (3524)',
'to_dashboard_user_preferences_help'=>'<b>Configuración de las preferencias del panel:</b><br/><br/>Determina el orden y cual grilla de información será mostrada en su panel.',
'wn_email_incorrect'=>'E-mail no válido.',
'wn_existing_password'=>'La contraseña está en la lista de contraseña recientes',
'wn_initial_date_after_today'=>'La fecha inicial debe ser igual o posterior a la actual.',
'wn_missing_dates'=>'Los campos de fecha son obligatorios cuando un procurador es seleccionado. (3527)',
'wn_missing_solicitor'=>'Es obligatorio seleccionar un procurador cuando una de las fechas fue completada. (3516)',
'wn_past_date'=>'La fecha final debe ser igual o posterior a la actual.',
'wn_wrong_dates'=>'La fecha final de la procuración debe ser posterior que la fecha en la que comenzó. (3530)',

/* './popup_user_search.xml' */

'tt_user_search'=>'Búsqueda de Usuario',
'wn_no_user_selected'=>'No hay usuario seleccinado',

/* './reports/ISMSReportTemplate.php' */

'rs_ip_cl'=>'IP:',
'rs_manager_cl'=>'Administrador:',

/* './session_expired.xml' */

'st_session_expired'=>'Su sesión ha expirado. Presione <b>Ok</b> o cierre esta ventana para ingresar nuevamente.',
'tt_session_expired'=>'Sesión Expirada',

/* './tab_dashboard.xml' */

'ti_graphics'=>'Gráficos',
'ti_statistics'=>'Estadísticas',
'ti_summary'=>'Resúmenes',
'ti_warnings'=>'Mis Tareas y Alertas',

/* './tab_main.xml' */

'st_search'=>'Buscar',
'ti_continual_improvement'=>'Mejora Continua',
'ti_dashboard'=>'Panel',
'ti_libraries'=>'Librerías',
'ti_policy_management'=>'Gestión de Política',
'ti_risk_management'=>'Gestión de Riesgo',
'ti_search'=>'Buscar',
'vb_admin'=>'Administrar (3550)',
'vb_go'=>'Ir',

/* './view/FWDCalendar.php' */

'locale'=>'es',

/* './view/FWDColorPicker.php' */

'chooseColor'=>'Elige un color!',
'choosenColor'=>'Color seleccionado',
'colorHelper1'=>'Sitúe el puntero del ratón sobre la paleta para ver los colores.',
'colorHelper2'=>'Haga clic para seleccionar un color.',
'newColor'=>'Nuevo Color',

/* './visualize.php' */

'tt_visualize'=>'Visualizar',

'st_amount_incident_period'=>'Número total de incidentes en el período: ',
'rs_manual_version'=>'Versión manual',
);
?>
