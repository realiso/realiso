<?php
$gaStrings = array(

/* './about_isms.xml' */

'st_about_axur_full'=>'A Realiso Corp. (www.realiso.com) é uma empresa líder em soluções para gestão da segurança da informação, ambiental e saúde e segurança. Provemos soluções de alta tecnologia para reduzir o risco organizacional, medir e demonstrar a eficiência dos controles utilizados, utilizando as melhores práticas do mercado.',
'st_about_axur_title'=>'Sobre a Realiso Corp.',
'st_about_isms_full'=>'Real EMS - Environmental Management System é uma suite de ferramentas para Sistemas de Gestão Ambiental. O Real EMS ajuda organizações de qualquer tamanho, onde diversos membros estão envolvidos com a custódia dos A.P.S., identificação de novos riscos e manutenção do meio A.P.S..',
'st_about_isms_title'=>'Sobre o Real EMS',
'st_axur_information_security'=>'Realiso Corp.',
'st_axur_link'=>'www.realiso.com',
'st_build'=>'Build',
'st_copyright'=>'Copyright',
'st_information_security_management_system'=>'Environmental Management System',
'st_version'=>'Versão',

/* './access_denied.php' */

'st_license_file_not_found'=>'Não foi possível encontrar o arquivo de licença <b>\'%license_name%\'</b>.',

/* './access_denied.xml' */

'tt_access_denied'=>'Acesso Negado',

/* './classes/admin/report/ISMSReportAuditAlertFilter.php' */

'rs_alerts_created_after'=>'Alertas criados depois de',
'rs_alerts_created_before'=>'Alertas criados antes de',

/* './classes/admin/report/ISMSReportAuditLogFilter.php' */

'rs_action_cl'=>'Ação:',
'rs_log_after'=>'Log depois de',
'rs_log_before'=>'Log antes de',

/* './classes/admin/report/ISMSReportAuditTaskFilter.php' */

'rs_activity_cl'=>'Atividade:',
'rs_and_before'=>'e antes de',
'rs_any_date'=>'Qualquer data',
'rs_creation_date_cl'=>'Data de criação:',
'rs_creator_cl'=>'Criador:',
'rs_receiver_cl'=>'Receptor:',
'rs_tasks_created_after'=>'Tarefas criadas depois de',
'rs_tasks_created_before'=>'Tarefas criadas antes de',
'st_all_users'=>'Todos usuários',

/* './classes/continual_improvement/CIActionPlan.php' */

'mx_action_plan'=>'Plano de Ação',
'mx_ap_remove_error_message'=>'Não foi possível remover o Plano de Ação, pois ele está associado a pelo menos uma Não Conformidade.',
'mx_ap_remove_error_title'=>'Erro ao remover Plano de Ação',

/* './classes/continual_improvement/CICategory.php' */

'mx_root'=>'Raiz',

/* './classes/continual_improvement/CIIncident.php' */

'mx_incident'=>'Acidente',

/* './classes/continual_improvement/CIIncidentControl.php' */

'mx_incident_control_association'=>'Associação de Controle a Acidente',

/* './classes/continual_improvement/CIIncidentProcess.php' */

'mx_incident_process_association'=>'Associação de Acidente a Processo',

/* './classes/continual_improvement/CINonConformity.php' */

'mx_non_conformity'=>'Não Conformidade',

/* './classes/continual_improvement/CINonConformitySeed.php' */

'mx_inefficient'=>'Ineficiente',
'mx_late_efficiency_revision'=>'Revisão de Eficácia Atrasada',
'mx_late_implementation'=>'Implementação Atrasada',
'mx_late_test'=>'Teste Atrasado',
'mx_test_failure'=>'Falha no Teste',

/* './classes/continual_improvement/CIOccurrence.php' */

'mx_occurrence'=>'Incidente',
'mx_occurrence_remove_error'=>'Erro ao remover Incidente',
'mx_occurrence_remove_error_message'=>'Não foi possível remover o incidente, pois ele está associado a um acidente.',
'st_incident_module_name'=>'Melhoria Contínua',

/* './classes/continual_improvement/CISolution.php' */

'mx_solution'=>'Solução',

/* './classes/continual_improvement/report/ISMSReportActionPlanFilter.php' */

'lb_action_type'=>'Tipo de Ação:',
'mx_both'=>'Ambas',

/* './classes/continual_improvement/report/ISMSReportDisciplinaryProcessFilter.php' */

'rs_user_cl'=>'Usuário:',

/* './classes/continual_improvement/report/ISMSReportElementsFilter.php' */

'lb_elements_cl'=>'Elementos:',

/* './classes/continual_improvement/report/ISMSReportIncidentFilter.php' */

'lb_incident_category_cl'=>'Categoria:',
'lb_loss_type_cl'=>'Tipo de Perda:',
'lb_solution_date_cl'=>'Data da Solução:',
'mx_immediate_disposal'=>'Disposição Imediata',

/* './classes/continual_improvement/report/ISMSReportIncidentsFilter.php' */

'rs_asset_cl'=>'A.P.S.:',

/* './classes/continual_improvement/report/ISMSReportNonConformitiesFilter.php' */

'rs_status_cl'=>'Status:',

/* './classes/continual_improvement/report/ISMSReportNonConformityFilter.php' */

'lb_classification_cl'=>'Classificação:',
'lb_conclusion_date_cl'=>'Data de Conclusão:',
'lb_deadline_cl'=>'Prazo:',
'lb_potential_cl'=>'Potencial:',
'mx_after_initialdate'=>'Depois de %initialDate%',
'mx_before_finaldate'=>'Antes de %finalDate%',
'mx_between_initialdate_finaldate'=>'Entre %initialDate% e %finalDate%',
'mx_external_audit'=>'Auditoria Externa',
'mx_internal_audit'=>'Auditoria Interna',
'mx_no'=>'Não',
'mx_security_control'=>'Controle de Segurança',
'mx_yes'=>'Sim',

/* './classes/continual_improvement/report/ISMSReportOccurrenceFilter.php' */

'lb_status_cl'=>'Status:',
'mx_all'=>'Todos',

/* './classes/ISMSActivity.php' */

'wk_action_plan_approval'=>'Aprovar Plano de Ação',
'wk_action_plan_finish_confirm'=>'Aprovar Previsão de Conclusão do Plano de Ação',
'wk_action_plan_revision'=>'Acompanhar Revisão do Plano de Ação',
'wk_area_approval'=>'Aprovar Área',
'wk_asset_approval'=>'Aprovar A.P.S.',
'wk_best_practice_approval'=>'Aprovar Requisito Legal',
'wk_best_practice_section_approval'=>'Aprovar Seção de Requisito Legal',
'wk_category_approval'=>'Aprovar Categoria',
'wk_control_approval'=>'Aprovar Controle',
'wk_control_implementation_approval'=>'Aprovar Implementação do Controle',
'wk_control_test_followup'=>'Acompanhar Teste do Controle',
'wk_document_approval'=>'Aprovar Documento',
'wk_document_revision_followup'=>'Acompanhar Revisão de Documento',
'wk_document_template_approval'=>'Aprovar Modelo de Documento',
'wk_event_approval'=>'Aprovar Evento',
'wk_incident_approval'=>'Aprovar Acidente',
'wk_incident_control_induction'=>'Medição Induzida de Eficácia do Controle',
'wk_incident_disposal_approval'=>'Aprovar Disposição Imediata para Acidente',
'wk_incident_solution_approval'=>'Aprovar Solução para Acidente',
'wk_non_conformity_approval'=>'Aprovar Não Conformidade',
'wk_non_conformity_data_approval'=>'Aprovar Dados da Não Conformidade',
'wk_occurrence_approval'=>'Aprovar Incidente',
'wk_policy_approval'=>'Aprovar Documento',
'wk_process_approval'=>'Aprovar Processo',
'wk_process_asset_association_approval'=>'Aprovar Associação de A.P.S. com Processo',
'wk_real_efficiency_followup'=>'Acompanhar Eficácia Real',
'wk_risk_acceptance_approval'=>'Aprovar Aceitação de Riscos acima do valor',
'wk_risk_acceptance_criteria_approval'=>'Aprovar definição de critérios de Aceitação de Riscos',
'wk_risk_approval'=>'Aprovar Risco',
'wk_risk_control_association_approval'=>'Aprovar Associação de Controle com Risco',
'wk_risk_tolerance_approval'=>'Aprovar Tolerância ao Risco',
'wk_scope_approval'=>'Aprovar Escopo',
'wk_standard_approval'=>'Aprovar Legislação',

/* './classes/ISMSAuditLog.php' */

'ad_action_bad_login'=>'tentou efetuar login com uma senha inválida',
'ad_action_blocked_login'=>'está bloqueado e tentou efetuar login no sistema',
'ad_login'=>'entrou no sistema',
'ad_login_error'=>'tentou efetuar login com um nome de usuário inválido',
'ad_login_failed'=>'tentou entrar no sistema porém não possui permissão',
'ad_logout'=>'saiu do sistema',
'ad_removed_from_system'=>'Removido(a) do sistema',
'ad_restored'=>'Restaurado(a)',

/* './classes/ISMSContext.php' */

'ad_log_message'=>'%action% %label% <b>%context_name%</b> por %user_name%.',
'ad_on_behalf_of'=>'em nome de',
'db_default_document_name'=>'%label_doc% %label_context% %context_name%',
'st_denied_permission_to_approve'=>'Você não tem permissão para aprovar o(a)',
'st_denied_permission_to_delete'=>'Você não tem permissão para remover o(a)',
'st_denied_permission_to_execute'=>'Você não tem permissão para executar a tarefa',
'st_denied_permission_to_insert'=>'Você não tem permissão para inserir um(a)',
'st_denied_permission_to_read'=>'Você não tem permissão para ver o(a)',

/* './classes/ISMSContextObject.php' */

'mx_ap_acceptance_pendant'=>'Aceitação do Plano de Ação Pendente',
'mx_approval'=>'Em Aprovação',
'mx_approved'=>'Aprovado(a)',
'mx_closed'=>'Fechado',
'mx_co_approved'=>'Co-aprovado(a)',
'mx_deleted'=>'Deletado(a)',
'mx_denied'=>'Negado(a)',
'mx_directed'=>'Encaminhado',
'mx_finished'=>'Concluído',
'mx_measured'=>'Medido',
'mx_nc_treatment_pendant'=>'Tratamento de Não Conformidade Pendente',
'mx_obsolete'=>'Obsoleto',
'mx_open'=>'Aberto',
'mx_pendant'=>'Pendente',
'mx_pendant_solution'=>'Pendente - Solução',
'mx_pendant_solved'=>'Pendente - Solucionado',
'mx_published'=>'Publicado',
'mx_revision'=>'Em Revisão',
'mx_sent'=>'Emitido',
'mx_solved'=>'Solucionado',
'mx_to_be_published'=>'A Publicar',
'mx_under_development'=>'Em Desenvolvimento',
'mx_waiting_conclusion'=>'Aguardando Conclusão',
'mx_waiting_deadline'=>'Previsão',
'mx_waiting_measurement'=>'Aguardando Medição',
'mx_waiting_solution'=>'Aguardando Solução',

/* './classes/ISMSEmailTemplates.php' */

'em_activity'=>'Atividade',
'em_alert_messages_sent'=>'<b>Prezado usuário do Real EMS,</b><br>os seguintes alertas foram enviados para você.<br>',
'em_asset'=>'A.P.S.',
'em_at'=>'às',
'em_click_here_to_access_the_system'=>'Acesse o Real EMS',
'em_client'=>'Cliente',
'em_control'=>'Controle',
'em_control_has_deadline'=>'<b>Prezado usuário do Real EMS,</b><br>o controle abaixo possui data limite para implementação.',
'em_created_on'=>'Gerado em',
'em_description'=>'Descrição',
'em_disciplinary_process'=>'<b>Prezado Gestor de Processo Disciplinar,</b><br>O(s) seguinte(s) usuário(s) do sistema foi/foram adicionados a um processo disciplinar.',
'em_execute_task'=>'<b>Prezado usuário do Real EMS,</b><br>solicitamos que execute a(s) atividade(s) apontada(s) abaixo.<br>',
'em_feedback'=>'Feedback',
'em_feedback_type'=>'Tipo de Feedback',
'em_incident'=>'Acidente',
'em_incident_risk_parametrization_message'=>'<b>Prezado usuário do Real EMS,</b><br>o risco abaixo foi associado a um A.P.S. de sua responsabilidade.<br>Os parâmetros de risco são diferentes do risco original.',
'em_isms_feedback_msg'=>'Um usuário escreveu um feedback para o Real EMS.',
'em_isms_new_user_msg'=>'Você foi adicionado ao Real EMS. Abaixo seguem os dados de acesso:',
'em_limit'=>'Limite',
'em_login'=>'Login',
'em_new_parameters'=>'Novos Parâmetros',
'em_new_password_generated'=>'<b>Prezado usuário do Real EMS,</b><br>uma nova senha foi gerada para você.<br>Sua nova senha é',
'em_new_password_request_ignore'=>'Se você não solicitou uma nova senha, por favor ignore este e-mail. Suas credenciais anteriores ainda são válidas e ao usá-las esta nova senha será descartada.',
'em_observation'=>'Observação',
'em_original_parameters'=>'Parâmetros Originais',
'em_password'=>'Senha',
'em_risk'=>'Risco',
'em_saas_abandoned_system_msg'=>'<b>O Real EMS do cliente abaixo foi abandonado.</b>',
'em_sender'=>'Remetente',
'em_sent_on'=>'enviado em',
'em_tasks_and_alerts_transferred'=>'<b>Prezado usuário do Real EMS,</b><br>Um usuário do sistema foi removido, e suas tarefas e alertas foram transferidas a você.',
'em_tasks_and_alerts_transferred_message'=>'O usuário acima foi removido do sistema, passando suas tarefas e alertas para o seu nome.',
'em_user'=>'Usuário',
'em_users'=>'Usuários',
'link'=>'Link',

/* './classes/ISMSLib.php' */

'em_license_expired'=>'A Licença do Cliente <b>%client_name%</b> expirou em <b>%expiracy%</b>',
'em_subject_license_expired'=>'Licença Expirada',
'mx_asset_manager'=>'Gestor de A.P.S.',
'mx_chairman'=>'Direção',
'mx_chinese'=>'Chinês (Beta)',
'mx_control_manager'=>'Gestor de Controle',
'mx_created_by'=>'Criado por:',
'mx_day'=>'dia',
'mx_days'=>'dias',
'mx_english'=>'Inglês',
'mx_evidence_manager'=>'Gestor de Evidência',
'mx_french'=>'Français',
'mx_german'=>'Deutsch',
'mx_incident_manager'=>'Gestor de Acidente',
'mx_library_manager'=>'Gestor de Biblioteca',
'mx_modified_by'=>'Modificado por:',
'mx_month'=>'mês',
'mx_months'=>'meses',
'mx_non_conformity_manager'=>'Gestor de Não Conformidade',
'mx_on'=>'em',
'mx_portuguese'=>'Português',
'mx_process_manager'=>'Gestor de Processo Disciplinar',
'mx_spanish'=>'Español (Beta!)',
'mx_week'=>'semana',
'mx_weeks'=>'semanas',
'st_denied_permission_to_access_incident_module'=>'Você não tem permissão para acessar o módulo de Melhoria Contínua. <br><br>Você está utilizando uma licença que não permite acessar este módulo!',
'st_denied_permission_to_access_policy_module'=>'Você não tem permissão para acessar o módulo de Gestão de Documentos. <br><br>você está utilizando uma licença que não permite acessar este módulo!',

/* './classes/ISMSLicense.php' */

'mx_commercial'=>'Comercial',
'mx_trial'=>'Trial',

/* './classes/ISMSMailer.php' */

'em_to_execute_task_check_activity_list'=>'<b>Para executar a tarefa acesse o sistema e verifique sua lista de atividades.</b><br>Para maiores informações sobre esta tarefa entre em contato com o security officer.',

/* './classes/ISMSPasswordPolicy.php' */

'mx_minimum_chars'=>'Número mínimo de caracteres',
'mx_minimum_numeric_chars'=>'Número mínimo de caracteres numéricos',
'mx_minimum_special_chars'=>'Número mínimo de caracteres especiais',
'mx_password_policy'=>'<b>Política de Senhas:</b><br/>',
'mx_upper_and_lower_case_chars'=>'Caracteres maiúsculos e minúsculos',
'st_no'=>'Não',
'st_yes'=>'Sim',

/* './classes/ISMSPolicy.php' */

'mx_policy'=>'Documento',

/* './classes/ISMSProfile.php' */

'mx_profile'=>'Perfil',
'st_profile_remove_error'=>'Não foi possível remover o perfil <b>%profile_name%</b>: Existem usuários associados a este perfil.',
'tt_profile_remove_error'=>'Erro ao remover Perfil',

/* './classes/ISMSScope.php' */

'mx_scope'=>'Escopo',

/* './classes/ISMSSession.php' */

'ad_login_message'=>'%label_user% <b>%user_name%</b> %action%.',

/* './classes/ISMSUser.php' */

'mx_user'=>'Usuário',
'st_admim_module_name'=>'Admin',
'st_user_remove_error'=>'Não foi possível remover o usuário <b>%user_name%</b>: O usuário é responsável por um ou mais elementos do sistema ou é um usuário especial.',
'tt_user_remove_error'=>'Erro ao remover Usuário',

/* './classes/nonauto/admin/ISMSDefaultConfig.php' */

'db_area_priority_high'=>'3 - Alto',
'db_area_priority_low'=>'1 - Baixo',
'db_area_priority_middle'=>'2 - Médio',
'db_area_type_factory_unit'=>'Fábrica',
'db_area_type_filial'=>'Subsidiário',
'db_area_type_matrix'=>'Sede',
'db_area_type_regional_office'=>'Escritório Regional',
'db_control_cost_hardware'=>'Hardware',
'db_control_cost_material'=>'Serviços',
'db_control_cost_others'=>'Educação',
'db_control_cost_pessoal'=>'Pessoas',
'db_control_cost_software'=>'Software',
'db_control_type_based_on_risk_analize'=>'Baseado em Análise de Risco',
'db_control_type_laws_and_regulamentation'=>'Questões de Compliance',
'db_control_type_others'=>'Outros',
'db_document_type_1'=>'Confidential',
'db_document_type_2'=>'Restrito',
'db_document_type_3'=>'Institucional',
'db_document_type_4'=>'Público',
'db_impact_1'=>'Muito Baixo',
'db_impact_2'=>'Baixo',
'db_impact_3'=>'Médio',
'db_impact_4'=>'Alto',
'db_impact_5'=>'Muito Alto',
'db_impact3_1'=>'Baixo',
'db_impact3_2'=>'Médio',
'db_impact3_3'=>'Alto',
'db_importance_1'=>'Negligenciável',
'db_importance_2'=>'Baixo',
'db_importance_3'=>'Médio',
'db_importance_4'=>'Alto',
'db_importance_5'=>'Crítico',
'db_importance3_1'=>'Baixo',
'db_importance3_2'=>'Médio',
'db_importance3_3'=>'Alto',
'db_incident_cost_aplicable_penalty'=>'Multa',
'db_incident_cost_direct_lost'=>'Perda Direta',
'db_incident_cost_enviroment'=>'Recuperação do A.P.S.',
'db_incident_cost_indirect_lost'=>'Perda Indireta',
'db_process_priority_high'=>'3 - Alto',
'db_process_priority_low'=>'1 - Baixo',
'db_process_priority_middle'=>'2 - Médio',
'db_process_type_4'=>'Recursos humanos',
'db_process_type_5'=>'Tecnologia da informação',
'db_process_type_6'=>'Finanças & contabilidade',
'db_process_type_7'=>'A.P.S., saúde & segurança',
'db_process_type_8'=>'Melhoria de performance',
'db_process_type_administrative'=>'Mercado & consumidores',
'db_process_type_operational'=>'Desenvolvimento de novo produto',
'db_process_type_support'=>'Produção & entrega',
'db_rcimpact_0'=>'Não afeta',
'db_rcimpact_1'=>'Decréscimo de um Nível',
'db_rcimpact_2'=>'Decréscimo de dois Níveis',
'db_rcimpact_3'=>'Decréscimo de três Níveis',
'db_rcimpact_4'=>'Decréscimo de quatro Níveis',
'db_rcimpact3_0'=>'Não afeta',
'db_rcimpact3_1'=>'Decréscimo de um Nível',
'db_rcimpact3_2'=>'Decréscimo de dois Níveis',
'db_rcprob_0'=>'Não afeta',
'db_rcprob_1'=>'Decréscimo de um Nível',
'db_rcprob_2'=>'Decréscimo de dois Níveis',
'db_rcprob_3'=>'Decréscimo de três Níveis',
'db_rcprob_4'=>'Decréscimo de quatro Níveis',
'db_rcprob3_0'=>'Não afeta',
'db_rcprob3_1'=>'Decréscimo de um Nível',
'db_rcprob3_2'=>'Decréscimo de dois Níveis',
'db_register_type_1'=>'Confidencial',
'db_register_type_2'=>'Restrito',
'db_register_type_3'=>'Institucional',
'db_register_type_4'=>'Público',
'db_risk_confidenciality'=>'Confidencialidade',
'db_risk_disponibility'=>'Disponibilidade',
'db_risk_intregrity'=>'Integridade',
'db_risk_severity'=>'Severidade',
'db_risk_type_administrative'=>'Administrativo',
'db_risk_type_fisic'=>'Físico',
'db_risk_type_tecnologic'=>'Tecnológico',
'db_risk_type_threat'=>'Ameaça',
'db_risk_type_vulnerability'=>'Vulnerabilidade',
'db_risk_type_vulnerability_x_threat'=>'Evento (Vulnerabilidade x Ameaça)',
'db_riskprob_1'=>'Raro (1% - 20%)',
'db_riskprob_2'=>'Improvável (21% - 40%)',
'db_riskprob_3'=>'Moderado (41% - 60%)',
'db_riskprob_4'=>'Provável (61% - 80%)',
'db_riskprob_5'=>'Quase certo (81% - 99%)',
'db_riskprob3_1'=>'Improvável',
'db_riskprob3_2'=>'Moderado',
'db_riskprob3_3'=>'Provável',
'si_external_audit'=>'Auditoria Externa',
'si_internal_audit'=>'Auditoria Interna',
'si_security_control'=>'Controle de Segurança',

/* './classes/nonauto/FeedbackEvent.php' */

'em_isms_feedback'=>'Real EMS Feedback',
'mx_bug'=>'Bug',
'mx_content_issue'=>'Sugestão',
'mx_feature_request'=>'Melhoria',

/* './classes/nonauto/policy_management/DocumentsDrawGrid.php' */

'to_future_publication'=>'O documento somente estará disponível para os leitores no dia %date%.',

/* './classes/policy_management/PMDocInstance.php' */

'mx_document_instance'=>'Instância de Documento',

/* './classes/policy_management/PMDocument.php' */

'st_denied_permission_to_manage_document'=>'Você não tem permissão para gerenciar o documento.',
'st_document_remove_error'=>'Não foi possível remover o documento <b>%name%</b>. O documento contém subdocumentos ou registros associados. Para remover documentos que contenham subdocumentos ou registros associados, ative a opção "Delete Cascade" na aba "Admin -> Configuração".',
'st_policy_management'=>'Gestão dos Documentos',
'tt_document_remove_error'=>'Erro ao remover documento',

/* './classes/policy_management/PMRegister.php' */

'mx_day_days'=>'dia(s)',
'mx_month_months'=>'mês(es)',
'mx_register'=>'Registro',
'mx_week_weeks'=>'semana(s)',

/* './classes/policy_management/PMTemplate.php' */

'mx_document_template'=>'Modelo de Documento',

/* './classes/policy_management/report/ISMSReportAccessedDocumentsFilter.php' */

'rs_users'=>'Usuários',

/* './classes/policy_management/report/ISMSReportContextsWithoutDocuments.php' */

'rs_components'=>'Componentes',

/* './classes/policy_management/report/ISMSReportDocsWithHighFrequencyRevision.php' */

'rs_justification'=>'Justificativa',
'rs_revision_date'=>'Data de Revisão',
'rs_user'=>'Usuário',

/* './classes/policy_management/report/ISMSReportDocsWithHighFrequencyRevisionFilter.php' */

'report_filter_all_userid'=>'todos(as)',
'report_filter_high_frequency_revision_definition'=>'Alta freqüência de revisão é:',
'report_filter_high_frequency_revision_definition_explanation'=>'mais de uma revisão em um período de uma semana.',
'report_filter_userid_cl'=>'Responsável:',

/* './classes/policy_management/report/ISMSReportDocumentAccessesFilter.php' */

'rs_accesses'=>'Acessos',
'rs_date'=>'Data',
'rs_only_last'=>'Somente o último',
'rs_show_all'=>'Exibir todos',

/* './classes/policy_management/report/ISMSReportDocumentDatesFilter.php' */

'rs_after_initialdate'=>'Depois de %initialDate%',
'rs_before_finaldate'=>'Antes de %finalDate%',
'rs_between_initialfinaldate'=>'Entre %initialDate% e %finalDate%',
'rs_creation_date'=>'Data de criação',
'rs_last_revision_date'=>'Data da última revisão',

/* './classes/policy_management/report/ISMSReportDocumentRegistersFilter.php' */

'rs_documents'=>'Documentos',

/* './classes/policy_management/report/ISMSReportDocumentsByComponentFilter.php' */

'report_filter_all_doctype'=>'Todos',
'report_filter_doctype_cl'=>'Tipo de Documento:',

/* './classes/policy_management/report/ISMSReportDocumentsByStateFilter.php' */

'report_filter_showregisterdocs_cl'=>'Mostrar Documentos de Registro:',
'report_filter_status_cl'=>'Status dos Documentos:',
'rs_all_status'=>'Todos',

/* './classes/policy_management/report/ISMSReportDocumentSummary.php' */

'rs_approvers_list_section'=>'Lista de Aprovadores',
'rs_author'=>'Autor',
'rs_components_list_section'=>'Lista de Componentes',
'rs_deadline'=>'Prazo',
'rs_description'=>'Descrição',
'rs_document_type_management'=>'Gestão',
'rs_documents_type_others'=>'Outros',
'rs_file'=>'Arquivo',
'rs_general_info_section'=>'Informações Gerais',
'rs_keywords'=>'Palavras-chave',
'rs_link'=>'Link',
'rs_no_approvers_message'=>'Esse documento não possui aprovadores.',
'rs_no_components_message'=>'Esse documento não possui componentes.',
'rs_no_readers_message'=>'Esse documento não possui leitores.',
'rs_no_references_message'=>'Esse documento não possui referências.',
'rs_no_registers_message'=>'Esse documento não possui registros.',
'rs_no_subdocs_message'=>'Esse documento não possui subdocumentos.',
'rs_published_date'=>'Publicação',
'rs_readers_list_section'=>'Lista de Leitores',
'rs_references_list_section'=>'Lista de Referências',
'rs_registers_list_section'=>'Lista de Registros',
'rs_report_not_supported'=>'\'RELATÓRIO NÃO SUPORTADO NESTE FORMATO\'',
'rs_subdocs_list_section'=>'Lista de Subdocumentos',
'rs_type'=>'Tipo',
'rs_version'=>'Versão',

/* './classes/policy_management/report/ISMSReportDocumentsWithoutRegisterFilter.php' */

'report_filter_all_document_responsible'=>'todos(as)',
'report_filter_document_responsible_cl'=>'Responsável pelo Documento:',

/* './classes/policy_management/report/ISMSReportNotAccessedDocumentsFilter.php' */

'rs_expand_readers'=>'Expandir leitores',

/* './classes/policy_management/report/ISMSReportSummarizedPendantTasksPM.php' */

'rs_amount'=>'Quantidade',
'rs_responsible'=>'Responsável',
'rs_tasks'=>'Tarefas',

/* './classes/policy_management/report/ISMSReportTopRevisedDocumentsFilter.php' */

'rs_expand_documents'=>'Expandir documentos',
'rs_show_only_first_cl'=>'Exibir somente os primeiros:',

/* './classes/policy_management/report/ISMSReportUserCommentsFilter.php' */

'rs_all_documents'=>'todos(as)',
'rs_documents_cl'=>'Documentos:',

/* './classes/policy_management/report/ISMSReportUsersByProcessFilter.php' */

'rs_no'=>'Não',
'rs_organize_by_area'=>'Organizar por área',
'rs_yes'=>'Sim',

/* './classes/policy_management/report/ISMSReportUsersWithPendantRead.php' */

'cb_user'=>'Usuário',
'mx_document'=>'Documento',

/* './classes/policy_management/report/ISMSReportUsersWithPendantReadFilter.php' */

'report_filter_all_document_reader_cl'=>'todos(as)',
'report_filter_document_reader_cl'=>'Leitores dos documentos:',

/* './classes/risk_management/report/ISMSReportBestPracticesFilter.php' */

'rs_only_applied'=>'Somente Aplicadas',
'rs_standard_cl'=>'Legislação:',

/* './classes/risk_management/report/ISMSReportConformityFilter.php' */

'rs_all'=>'Todas',
'rs_standard_filter_cl'=>'Filtro de Legislação:',

/* './classes/risk_management/report/ISMSReportControlCostByCostCategoryFilter.php' */

'rs_costcategory_cl'=>'Categoria:',

/* './classes/risk_management/report/ISMSReportControlPlanningFilter.php' */

'rs_control_types_cl'=>'Tipos de Controle:',

/* './classes/risk_management/report/ISMSReportControlsFilter.php' */

'rs_delayed'=>'Atrasados',
'rs_implementationstatus_cl'=>'Exibir Apenas:',
'rs_not_trusted_controls'=>'Controles não confiáveis',
'rs_trusted_and_efficient_controls'=>'Controles confiáveis e eficientes',
'rs_under_implementation'=>'Em processo de implantação',

/* './classes/risk_management/report/ISMSReportEventsByCategoryFilter.php' */

'rs_category_cl'=>'Categoria:',

/* './classes/risk_management/report/ISMSReportGenericClassifTypePrioFilter.php' */

'lb_event_types_cl'=>'Tipos de Evento:',

/* './classes/risk_management/report/ISMSReportParametersFilter.php' */

'rs_considered_parameters_cl'=>'Parâmetros Considerados:',

/* './classes/risk_management/report/ISMSReportPendantTasksFilter.php' */

'rs_event_types_cl'=>'Tipos de Evento:',

/* './classes/risk_management/report/ISMSReportProcessesByPriority.php' */

'rs_undefined_priority'=>'Prioridade Indefinida',

/* './classes/risk_management/report/ISMSReportRiskFinancialImpactFilter.php' */

'mx_accepted'=>'Aceito',
'mx_avoided'=>'Evitado',
'mx_not_treated'=>'Não Tratado',
'mx_reduced'=>'Reduzido',
'mx_transferred'=>'Transferido',
'mx_treated'=>'Tratado',
'rs_not_treated_or_accepted'=>'Não tratados ou aceitos',
'rs_treatment_cl'=>'Tratamento:',

/* './classes/risk_management/report/ISMSReportRiskImpactFilter.php' */

'lb_considered_parameters_cl'=>'Parâmetros Considerados:',
'lb_risk_types_cl'=>'Tipos de Risco:',

/* './classes/risk_management/report/ISMSReportRisksByAreaFilter.php' */

'rs_area_cl'=>'Área:',

/* './classes/risk_management/report/ISMSReportRisksByAssetFilter.php' */

'rs_event_category_cl'=>'Categoria:',

/* './classes/risk_management/report/ISMSReportRisksByProcessFilter.php' */

'rs_process_cl'=>'Processo:',
'st_all'=>'Todas',

/* './classes/risk_management/report/ISMSReportRisksByType.php' */

'rs_undefined_type'=>'Tipo Indefinido',

/* './classes/risk_management/report/ISMSReportRisksFilter.php' */

'rs_estimated_risks'=>'riscos estimados',
'rs_not_estimated'=>'Não Estimado',
'rs_not_estimated_risks'=>'riscos não estimados',
'rs_not_treated_risks'=>'riscos não tratados',
'rs_residual_value_cl'=>'Risco Residual:',
'rs_show_only_cl'=>'Exibir Apenas:',
'rs_treated_risks'=>'riscos tratados',
'rs_value_cl'=>'Risco Potencial:',

/* './classes/risk_management/report/ISMSReportRisksResume.php' */

'cb_area'=>'Área',
'cb_asset'=>'A.P.S.',
'cb_process'=>'Processo',
'cb_risk'=>'Risco',
'st_parameterized_risks'=>'Riscos Estimados',

/* './classes/risk_management/report/ISMSReportRisksResumeFilter.php' */

'cb_all'=>'Todos',
'lb_area_cl'=>'Área:',
'lb_area_priorities_cl'=>'Prioridades de Area:',
'lb_area_types_cl'=>'Tipos de Area:',
'lb_asset_cl'=>'A.P.S.:',
'lb_department'=>'Processo:',
'lb_process_priorities_cl'=>'Prioridades de Processo:',
'lb_process_types_cl'=>'Tipos de Processo:',

/* './classes/risk_management/report/ISMSReportRiskValuesFilter.php' */

'rs_high'=>'Alto',
'rs_low'=>'Baixo',
'rs_mid_high'=>'Médio Alto',
'rs_mid_low'=>'Médio Baixo',
'rs_medium'=>'Médio',
'rs_not_parameterized'=>'Não Estimados',
'rs_real_risk'=>'Risco Potencial',
'rs_residual_risk'=>'Risco Residual',
'rs_risk_color_based_on_cl'=>'Cor do risco baseada no:',
'rs_risk_types_cl'=>'Tipos de Risco:',
'rs_risks_values_cl'=>'Valores dos Riscos:',

/* './classes/risk_management/report/ISMSReportStatementOfApplicabilityFilter.php' */

'lb_control_types_cl'=>'Tipos de Controle:',
'lb_standard_cl'=>'Legislação:',

/* './classes/risk_management/report/ISMSReportTop10AssetsWithMoreRisksFilter.php' */

'lb_risk_types'=>'Tipos de Risco',

/* './classes/risk_management/riskparameters/RiskParameters.php' */

'mx_justification_bl'=>'Justificativa',
'mx_justifying'=>'Justificando',

/* './classes/risk_management/RMAcceptRisk.php' */

'mx_risk_acceptance'=>'Aceitação de Risco',

/* './classes/risk_management/RMArea.php' */

'mx_area'=>'Área',
'st_area_remove_error_message'=>'Não foi possível remover a Área de Negócio <b>%area_name%</b>: A Área contém Sub-Áreas e/ou Processos. Para remover Áreas de Negócio que contenham Sub-Áreas ou Processos, ative a opção "Delete Cascade" na aba "Admin -> Configuração".',
'tt_area_remove_erorr'=>'Erro ao remover Área de Negócio',

/* './classes/risk_management/RMAsset.php' */

'mx_asset'=>'A.P.S.',
'st_asset_remove_error_message'=>'Não foi possível remover o A.P.S. <b>%asset_name%</b>: Existem Riscos associados ao A.P.S.. Para remover A.P.S. para o qual existem Riscos associados, ative a opção "Delete Cascade" na aba "Admin -> Configuração".',
'tt_asset_remove_error'=>'Erro ao remover A.P.S.',

/* './classes/risk_management/RMBestPractice.php' */

'mx_best_practice'=>'Requisitos Legais',
'st_best_practice_remove_error_message'=>'Não foi possível remover o Requisito Legal <b>%best_practice_name%</b>: O Requisito Legal está associado a um ou mais controles. Para remover Requisitos Legais que estejam associados a um ou mais controles, ative a opção "Delete Cascade" na aba "Admin -> Configuração".',
'tt_best_practice_remove_error'=>'Erro ao remover Requisito Legal',

/* './classes/risk_management/RMBestPracticeEvent.php' */

'mx_best_practice_event_association'=>'Associação de Requisito Legal a Evento',

/* './classes/risk_management/RMBestPracticeStandard.php' */

'mx_best_practice_standard_association'=>'Associação de Requisito Legal a Legislação',

/* './classes/risk_management/RMCategory.php' */

'mx_category'=>'Categoria',
'st_category_remove_error_message'=>'Não foi possível remover a Categoria <b>%category_name%</b>: A Categoria contém Sub-Categorias e/ou Eventos e/ou A.P.S.. Para remover Categorias que contenham Sub-Categorias, Eventos ou A.P.S., ative a opção "Delete Cascade" na aba "Admin -> Configuração".',

/* './classes/risk_management/RMControl.php' */

'mx_control'=>'Controle',
'st_rm_module_name'=>'Gestão de Aspectos e Impactos',

/* './classes/risk_management/RMControlBestPractice.php' */

'mx_control_best_practice_association'=>'Associação de Requisitos Legais a Controle',

/* './classes/risk_management/RMControlImplementationAccept.php' */

'mx_control_implementation_acceptance'=>'Aceitação da Implementação do Controle',

/* './classes/risk_management/RMEvent.php' */

'mx_event'=>'Evento',

/* './classes/risk_management/RMProcess.php' */

'mx_process'=>'Processo',

/* './classes/risk_management/RMProcessAsset.php' */

'mx_process_asset_association'=>'Associação de A.P.S. a Processo',

/* './classes/risk_management/RMRisk.php' */

'mx_risk'=>'Risco',

/* './classes/risk_management/RMRiskControl.php' */

'mx_risk_control_association'=>'Associação de Controle a Risco',

/* './classes/risk_management/RMRiskLimits.php' */

'mx_risk_tolerance'=>'Tolerância ao Risco',

/* './classes/risk_management/RMSectionBestPractice.php' */

'mx_best_practice_section'=>'Seção de Requisito Legal',
'st_home'=>'Raiz',
'st_section_control_best_practice_remove_error_message'=>'Não foi possível remover a Seção <b>%section_name%</b>: A Seção contém Requisitos Legais e/ou Sub-Seções que contém Requisitos Legais relacinados a Controles.',
'st_section_remove_error_message'=>'Não foi possível remover a Seção <b>%section_name%</b>: A Seção contém Sub-Seções e/ou Requisitos Legais. Para remover Seções que contenham Sub-Seções ou Requisitos Legais, ative a opção "Delete Cascade" na aba "Admin -> Configuração".',
'tt_section_remove_error'=>'Erro ao remover Seção',

/* './classes/risk_management/RMStandard.php' */

'mx_standard'=>'Legislação',
'st_libraries_module_name'=>'Bibliotecas',
'st_remove_standard_error'=>'Não é possível remover a legislação <b>%standard_name%</b> pois ela está associada com requisitos legais.',
'tt_remove_standard_error'=>'Erro ao Remover Legislação',

/* './classes/WKFAlert.php' */

'em_alert_check_list'=>'<b>Para visualizar o alerta acesse o sistema e verifique sua lista de atividades.</b><br>Para maiores informações sobre este alerta entre em contato com o security officer.',
'wk_ap_conclusion'=>'O plano de ação %name% foi concluido, ou seja, ele foi medido',
'wk_ap_efficiency_revision_late'=>'A data de revisão da eficiência do Plano de Ação \'%name%\' expirou.',
'wk_ap_efficiency_revision_near'=>'A data de revisão da eficiência do Plano de Ação \'%name%\' está próxima.',
'wk_approved'=>'foi aprovado(a).',
'wk_asset_np_risk_association'=>'Risco não estimado foi associado ao A.P.S. \'%asset%\' a partir do evento sugerido \'%event%\'.',
'wk_control_implementation'=>'O Controle \'%name%\' deve ser implementado.',
'wk_control_test_late'=>'Acompanhamento de Teste do Controle \'%name%\' está atrasado.',
'wk_deadline_close'=>'O deadline para aprovação do documento \'%name%\' está próximo.',
'wk_deadline_expired'=>'O deadline para aprovação do documento \'%name%\' expirou.',
'wk_delegated'=>'foi delegado(a) para você.',
'wk_denied'=>'foi negado(a).',
'wk_disciplinary_process'=>'O(s) usuário(s) %users% foi/foram adicionado(s) ao processo disciplinar do acidente %name%.',
'wk_document_comment'=>'O Documento \'%name%\' recebeu um comentário.',
'wk_document_creation_comment'=>'O documento \'%name%\' recebeu um comentário de criação.',
'wk_document_is_now_published'=>'O documento \'%name%\' foi publicado.',
'wk_document_new_version'=>'O documento \'%name%\' tem uma nova versão.',
'wk_document_publication'=>'O documento \'%name%\' foi enviado para publicação.',
'wk_document_revision'=>'O documento \'%name%\' foi enviado para revisão.',
'wk_document_revision_late'=>'A revisão do documento \'%name%\' está atrasada.',
'wk_implementation_expired'=>'A Implementação do Controle \'%name%\' expirou.',
'wk_inc_asset'=>'O acidente \'%incident%\' foi relacionado ao A.P.S. \'%asset%\'.',
'wk_inc_control'=>'O acidente \'%incident%\' foi relacionado ao controle \'%control%\'.',
'wk_inc_disposal_app'=>'A disposição imediata para o acidente \'%name%\' foi aprovada.',
'wk_inc_disposal_denied'=>'A disposição imediata para o acidente \'%name%\' foi negada.',
'wk_inc_evidence_required'=>'Processo de coleta de evidências requerido para o acidente \'%name%\'.',
'wk_inc_sent_to_responsible'=>'O acidente \'%name%\' foi encaminhado para você.',
'wk_inc_solution_app'=>'A solução para o acidente \'%name%\' foi aprovada.',
'wk_inc_solution_denied'=>'A solução para o acidente \'%name%\' foi negada.',
'wk_incident'=>'Criado o acidente \'%name%\'.',
'wk_incident_risk_parametrization'=>'O risco %risk_name% foi associado ao A.P.S. %asset_name%, pelo qual você é responsável. Os parâmetros de risco são diferentes do risco original.',
'wk_non_conformity_waiting_conclusion'=>'Não Conformidade %name% aguardando conclusão.',
'wk_non_conformity_waiting_deadline'=>'Não Conformidade %name% aguardando previsão de conclusão.',
'wk_pre_approved'=>'foi pré-aprovado(a).',
'wk_probability_update_down'=>'A freqüência de acidentes associados ao risco \'%name%\', no último período, sugere que sua probabilidade é menor do que a estimada',
'wk_probability_update_up'=>'A freqüência de acidentes associados ao risco \'%name%\', no último período, sugere que sua probabilidade é maior do que a estimada.',
'wk_real_efficiency_late'=>'Acompanhamento de Eficácia Real do Controle \'%name%\' está atrasado.',
'wk_risk_created'=>'O risco não estimado \'%risk%\' foi associado ao A.P.S. \'%asset%\'.',
'wk_warning'=>'Alerta',

/* './classes/WKFSchedule.php' */

'mx_first_week'=>'primeiro(a)',
'mx_fourth_week'=>'quarto(a)',
'mx_friday'=>'sexta-feira',
'mx_last'=>'último',
'mx_last_week'=>'último(a)',
'mx_monday'=>'segunda-feira',
'mx_saturday'=>'sábado',
'mx_second_last'=>'penúltimo',
'mx_second_wee'=>'segundo(a)',
'mx_sunday'=>'domingo',
'mx_third_week'=>'terceiro(a)',
'mx_thursday'=>'quinta-feira',
'mx_tuesday'=>'terça-feira',
'mx_wednesday'=>'quarta-feira',
'st_schedule_and'=>'e',
'st_schedule_april'=>'Abril',
'st_schedule_august'=>'Agosto',
'st_schedule_daily'=>'todos os dias',
'st_schedule_day_by_month'=>'no dia %day%',
'st_schedule_day_by_month_inverse'=>'no %day% dia',
'st_schedule_day_by_week'=>'no(a) %week% %week_day%',
'st_schedule_december'=>'Dezembro',
'st_schedule_each_n_days'=>'a cada %periodicity% dias',
'st_schedule_each_n_months'=>'a cada %periodicity% meses, %day_expression% do mês',
'st_schedule_each_n_weeks'=>'nos(as) %week_days%, a cada %periodicity% semanas',
'st_schedule_endless_interval'=>'A partir de %start%',
'st_schedule_february'=>'Fevereiro',
'st_schedule_friday_pl'=>'sextas',
'st_schedule_interval'=>'Entre %start% e %end%',
'st_schedule_january'=>'Janeiro',
'st_schedule_july'=>'Julho',
'st_schedule_june'=>'Junho',
'st_schedule_march'=>'Março',
'st_schedule_may'=>'Maio',
'st_schedule_monday_pl'=>'segundas',
'st_schedule_monthly'=>'%day_expression% de cada mês',
'st_schedule_november'=>'Novembro',
'st_schedule_october'=>'Outubro',
'st_schedule_saturday_pl'=>'sábados',
'st_schedule_september'=>'Setembro',
'st_schedule_specified_months'=>'%day_expression% de %months%',
'st_schedule_sunday_pl'=>'domingos',
'st_schedule_thursday_pl'=>'quintas',
'st_schedule_tuesday_pl'=>'terças',
'st_schedule_wednesday_pl'=>'quartas',
'st_schedule_weekly'=>'todos(as) %week_days%',

/* './classes/WKFTask.php' */

'wk_task'=>'Tarefa',
'wk_task_execution_permission'=>'Você não tem permissão para executar esta task!',

/* './crontab.php' */

'em_saas_abandoned_system'=>'Sistema Abandonado',

/* './damaged_system.php' */

'st_system_corrupted'=>'Seu sistema pode estar corrompido. Por favor, repita a instalação ou contate o nosso Suporte no site da Realiso.',

/* './damaged_system.xml' */

'tt_damaged_system'=>'Sistema Corrompido',

/* './email_sender.php' */

'em_daily_digest'=>'Digest diário',

/* './graphs/ISMSGraphAPActionType.php' */

'mx_ap_action_type_proportion'=>'Proporção de Planos de Ação e seus Tipos',
'mx_corrective_action'=>'Ação Corretiva',
'mx_preventive_action'=>'Ação Preventiva',

/* './graphs/ISMSGraphAssetCost.php' */

'mx_summary_of_assets_costs'=>'Sumário de A.P.S. e Custos',
'st_assets_cost_above_1m'=>'A.P.S. com custo acima de %curr_symbol% 1.000.000',
'st_assets_cost_below_100k'=>'A.P.S. com custo abaixo de %curr_symbol% 100.000',
'st_assets_cost_between_1m_and_500k'=>'A.P.S. com custo entre %curr_symbol% 1.000.000 e %curr_symbol% 500.000',
'st_assets_cost_between_500k_and_100k'=>'A.P.S. com custo entre %curr_symbol% 500.000 e %curr_symbol% 100.000',

/* './graphs/ISMSGraphAssetEstimatedCost.php' */

'mx_assets_with_without_estimated_costs'=>'A.P.S. com/sem Valor de Custo Estimado',
'st_assets_with_estimated_cost'=>'A.P.S. com custo estimado',
'st_assets_without_estimated_cost'=>'A.P.S. sem custo estimado',

/* './graphs/ISMSGraphAssetIncident.php' */

'mx_10_assets_with_most_incidents'=>'Os 10 A.P.S. que possuem mais Acidentes',

/* './graphs/ISMSGraphAssetRisk.php' */

'mx_10_assets_with_most_risks'=>'Os 10 A.P.S. que possuem mais riscos',

/* './graphs/ISMSGraphControlEfficiency.php' */

'mx_expected_efficiency'=>'Eficácia Esperada',
'mx_history_of_controls_efficiency_revision'=>'Histórico da Revisão de Eficácia dos Controles',
'mx_real_efficiency'=>'Eficácia Real',

/* './graphs/ISMSGraphControlTest.php' */

'mx_history_of_control_test'=>'Histórico dos Testes de Controle',
'mx_test_not_ok'=>'Teste Não OK',
'mx_test_ok'=>'Teste OK',
'mx_test_value'=>'Valor do Teste',

/* './graphs/ISMSGraphDocumentRegisters.php' */

'mx_10_document_with_most_registers'=>'Os 10 Documentos com mais Registros',

/* './graphs/ISMSGraphDocumentsAccessed.php' */

'mx_amount_of_accessed_documents'=>'Quantidade de Documentos acessados',
'st_accessed_documents'=>'Documentos Acessados',
'st_accessed_documents_cumulative'=>'Documentos Acessados - Cumulativo',

/* './graphs/ISMSGraphDocumentsClassification.php' */

'mx_amount_of_documents_by_classification'=>'Quantidade de Documentos por Tipo de Classificação',

/* './graphs/ISMSGraphDocumentsCreated.php' */

'mx_amount_of_created_documents'=>'Quantidade de Documentos criados',
'st_created_documents'=>'Documentos Criados',
'st_created_documents_cumulative'=>'Documentos Criados - Cumulativo',

/* './graphs/ISMSGraphDocumentsState.php' */

'mx_amount_of_documents_by_status'=>'Quantidade de Documentos por Status',

/* './graphs/ISMSGraphDocumentsType.php' */

'mx_amount_of_documents_by_type'=>'Quantidade de Documentos por Tipo',
'mx_management'=>'Gestão',
'mx_others'=>'Outros',
'mx_without_type'=>'Sem Tipo',

/* './graphs/ISMSGraphIncidentCategory.php' */

'mx_incident_category_proportion'=>'Proporção de Acidentes e Categorias',
'st_others'=>'Outros',

/* './graphs/ISMSGraphIncidentFinancialImpact.php' */

'mx_incident_financial_impact_summary'=>'Os 10 Acidentes com maior Impacto Financeiro',

/* './graphs/ISMSGraphIncidentLossType.php' */

'mx_direct_losses'=>'Perdas Diretas',
'mx_incident_loss_type_proportion'=>'Proporção de Acidentes e Tipos de Perda',
'mx_indirect_losses'=>'Perdas Indiretas',

/* './graphs/ISMSGraphIncidentsCreated.php' */

'mx_incidents_created_summary'=>'Sumário de Acidentes criados nos últimos 6 meses',

/* './graphs/ISMSGraphPotentialRisk.php' */

'mx_potential_risks_summary'=>'Sumário de Riscos Potenciais',
'st_high_risks'=>'Riscos Altos',
'st_low_risks'=>'Riscos Baixos',
'st_medium_risks'=>'Riscos Médios',
'st_medium_high_risks'=>'Riscos Médio Altos',
'st_medium_low_risks'=>'Riscos Médio Baixos',
'st_non_parameterized_risks'=>'Riscos Não Estimados',

/* './graphs/ISMSGraphProcessAsset.php' */

'mx_10_processes_with_most_associated_assets'=>'Os 10 processos que possuem mais A.P.S. associados',

/* './graphs/ISMSGraphProcessDocuments.php' */

'mx_10_processes_with_most_documents'=>'Os 10 Processos com mais Documentos',

/* './graphs/ISMSGraphProcessIncident.php' */

'mx_10_processes_with_most_incidents'=>'Os 10 Processos que possuem mais Acidentes',

/* './graphs/ISMSGraphProcessNonConformity.php' */

'mx_10_processes_with_most_nc_summary'=>'Os 10 Processos que possuem mais Não Conformidades',

/* './graphs/ISMSGraphProcessRisk.php' */

'mx_10_processes_with_most_risks'=>'Os 10 processos que possuem mais riscos',

/* './graphs/ISMSGraphProcessUsers.php' */

'mx_10_processes_with_most_users'=>'Os 10 Processos com mais pessoas envolvidas',

/* './graphs/ISMSGraphRedAssetRisk.php' */

'mx_10_assets_with_most_residual_risks_in_red'=>'Os 10 A.P.S. que possuem mais riscos residuais altos',

/* './graphs/ISMSGraphRedProcessRisk.php' */

'mx_10_processes_with_most_residual_risks_in_red'=>'Os 10 processos que possuem mais riscos residuais altos',

/* './graphs/ISMSGraphResidualRisk.php' */

'mx_residual_risks_summary'=>'Sumário de Riscos Residuais',
'st_high_risk'=>'Risco Alto',
'st_low_risk'=>'Risco Baixo',
'st_medium_risk'=>'Risco Médio',
'st_medium_high_risks'=>'Riscos Médio Altos',
'st_medium_low_risks'=>'Riscos Médio Baixos',
'st_non_parameterized_risk'=>'Risco Não Estimado',

/* './graphs/ISMSGraphSingleStatistic.php' */

'st_chart_not_enough_data'=>'Não há dados coletados suficientes para gerar esse gráfico.',

/* './graphs/ISMSGraphTreatedRisk.php' */

'mx_summary_of_risks_treatments'=>'Sumário dos Tratamentos de Riscos',
'st_avoided_risk'=>'Risco Evitado',
'st_risk_accepted_being_yellow_or_red'=>'Risco aceito, sendo médio ou alto',
'st_risk_accepted_for_being_green'=>'Risco aceito por ser baixo',
'st_risk_treated_with_user_of_control'=>'Risco tratado com aplicação de controle',
'st_transferred_risk'=>'Risco Transferido',

/* './handlers/QueryCountDocsTimeToApprove.php' */

'gs_day'=>'dia',
'gs_days'=>'dias',
'gs_hour'=>'hora',
'gs_hours'=>'horas',
'gs_minute'=>'minuto',
'gs_minutes'=>'minutos',
'gs_not_applicable'=>'NA',
'gs_second'=>'segundo',
'gs_seconds'=>'segundos',

/* './invalid_activation_code.php' */

'st_invalid_activation_code'=>'O código de ativação inserido é inválido. Por favor, revise-o ou contate nosso Suporte no site da Realiso.',

/* './invalid_activation_code.xml' */

'tt_invalid_activation_code'=>'Código de Ativação Inválido',

/* './license_corrupted.xml' */

'st_license_corrupted_message'=>'Seu sistema está utilizando uma licença inválida ou que foi corrompida.',
'tt_license_corrupted'=>'Licença Inválida ou Corrompida',

/* './license_expired.php' */

'st_license_expired'=>'Sua licença expirou em <b>%expiracy_str%</b>.',

/* './license_expired.xml' */

'tt_license_expired'=>'Licença expirada',

/* './license_isms.php' */

'st_incident_management'=>'Melhoria Contínua',
'st_number_of_modules'=>'Número de Módulos',
'st_risk_management'=>'Gestão de Aspectos e Impactos',
'st_system_modules_bl'=>'<b>Módulos do Sistema</b>',

/* './license_limit_reached.php' */

'st_assets'=>'A.P.S.',
'st_modules'=>'módulos',
'st_standards'=>'legislações',
'st_users'=>'usuários',

/* './license_limit_reached.xml' */

'st_license_limit_reached'=>'O número máximo de %attribute_name% foi atingido. Não é possível adicionar %attribute_name%.',
'tt_license_limit_reached'=>'Limite de %attribute_name% atingido',

/* './login.php' */

'ad_login_error_user_and_password_do_not_match'=>'%user_login% (%user_ip%)',
'ad_login_log_message'=>'%label_user% <b>%user_name%</b> %action%.',
'mx_session_conflict'=>'O login falhou pois existe outro usuário logado no sistema com esse mesmo browser.',
'st_blocked_instance_message'=>'Instância Bloqueada',
'tt_blocked_instance'=>'Instância Bloqueada',
'tt_session_conflict'=>'Falha no Login',

/* './login.xml' */

'cb_remember_password'=>'Lembrar Senha',
'cb_remember_user'=>'Lembrar Usuário',
'st_login_panel'=>'Painel de Login',
'vb_forgot_your_password'=>'Esqueceu sua senha?',
'vb_login'=>'Login',
'wn_denied_access'=>'Usuário sem permissão para acessar o sistema!',
'wn_invalid_user_or_password'=>'Usuário ou senha inválida!',
'wn_max_users'=>'Número máximo de logins simultâneos alcançado!',
'wn_user_blocked'=>'Usuário bloqueado!',

/* './migration/ISMS_import.php' */

'ad_created'=>'Criado(a)',
'ad_edited'=>'Editado(a)',
'ad_removed_from_bin'=>'Removido(a) da lixeira',

/* './migration/ISMS_migration.php' */

'st_destination_db_error'=>'Erro ao conectar ao BD destino da migração. nn Verifique se os dados preenchidos estão corretos!',
'st_source_db_error'=>'Erro ao conectar ao BD origem da migração. nn Verifique se os dados preenchidos estão corretos!',

/* './migration/ISMS_migration.xml' */

'lb_database_name_cl'=>'Database Name:',
'lb_database_password_cl'=>'Database Password:',
'lb_database_user_name_cl'=>'Database User Name:',
'lb_hostname_cl'=>'Hostname:',
'tt_database_copy_bl'=>'<b>Database Copy</b>',
'tt_destiny_database_bl'=>'<b>Destiny Database</b>',
'tt_results_bl'=>'<b>Results</b>',
'tt_source_database_bl'=>'<b>Source Database</b>',

/* './nav_graphics.xml' */

'lb_graphic_cl'=>'Gráfico:',
'si_optgroup_ci'=>'Melhoria Contínua',
'si_optgroup_cost'=>'Custo',
'si_optgroup_pm'=>'Gestão dos Documentos',
'si_optgroup_revision'=>'Revisão',
'si_optgroup_rm'=>'Gestão de Aspectos e Impactos',
'si_optgroup_test'=>'Teste',

/* './nav_search.xml' */

'cb_document_template'=>'Modelo de Documento',
'cb_incident'=>'Acidente',
'cb_incident_category_library'=>'Categorias de Acidentes',
'cb_non_conformity'=>'Não Conformidade',
'cb_occurrence'=>'Incidente',
'lb_new_search_bl_cl'=>'<b>Nova pesquisa:</b>',
'rb_action_plan'=>'Plano de Ação',
'rb_advanced'=>'Avançado',
'rb_basic'=>'Básico',
'rb_soluction'=>'Soluções',
'st_creation_cl'=>'Criação:',
'st_dates_filter_bl'=>'Filtro de Datas',
'st_modification_cl'=>'Modificação:',
'to_advanced_search_doc'=>'Pesquisa avançada de documentos:
Tipo de pesquisa que inclui o conteúdo do arquivo na pesquisa, além dos campos da pesquisa básica.',
'to_template_document_advanced_help'=>'Pesquisa avançada de modelos de documentos:
Tipo de pesquisa que inclui o conteúdo do arquivo na pesquisa, além dos campos da pesquisa básica.',
'tt_data_filter'=>'Filtro de Datas',
'tt_elements_filter'=>'Filtro de Elementos',
'vb_dates_filter'=>'Filtro de Datas',
'vb_elements_filter'=>'Filtro de Elementos',
'wn_select_one_element_filter'=>'A pesquisa deve ter pelo menos 1 filtro de elemento selecionado.',

/* './nav_statistics.php' */

'mx_action_plan_efficient_and_not'=>'Quantidade de Planos de Ação eficientes e não eficientes',
'mx_action_plan_finished_late'=>'Quantidade de Planos de Ação finalizados com atraso',
'mx_action_plan_finished_on_time'=>'Quantidade de Planos de Ação finalizados na data',
'mx_action_plan_total'=>'Quantidade de Planos de Ação',
'mx_area_amount'=>'Quantidade de Áreas',
'mx_area_assets_amount'=>'Quantidade de A.P.S. da Área',
'mx_area_assets_cost'=>'Custo dos A.P.S. da Área',
'mx_area_controls_amount'=>'Quantidade de Controles da Área',
'mx_area_controls_cost'=>'Custo dos Controles da Área',
'mx_area_processes_amount'=>'Quantidade de Processos da Área',
'mx_area_risks_amount'=>'Quantidade de Riscos da Área',
'mx_area_total_impact'=>'Total de Impacto da Área',
'mx_area_values'=>'Valores da Área',
'mx_areas_risk_summary'=>'Sumário de Risco das Áreas',
'mx_areas_without_processes'=>'Áreas sem Processos',
'mx_asset_controls_amount'=>'Quantidade de Controles do A.P.S.',
'mx_asset_controls_cost'=>'Custo dos Controles do A.P.S.',
'mx_asset_processes_amount'=>'Quantidade de Processos do A.P.S.',
'mx_asset_risks_amount'=>'Quantidade de Riscos do A.P.S.',
'mx_asset_total_impact'=>'Total de Impacto do A.P.S.',
'mx_asset_values'=>'Valores do A.P.S.',
'mx_assets_amount'=>'Quantidade de A.P.S.',
'mx_assets_risk_summary'=>'Sumário de Risco dos A.P.S.',
'mx_assets_total_cost'=>'Custo Total dos A.P.S.',
'mx_assets_without_risk_events'=>'A.P.S. sem Eventos de Risco',
'mx_business_areas'=>'Áreas de Negócios',
'mx_business_processes'=>'Processos de Negócios',
'mx_control_risks_amount'=>'Quantidade de Riscos do Controle',
'mx_controls_adequacy'=>'Adequação dos Controles',
'mx_controls_amount'=>'Quantidade de Controles',
'mx_controls_implementation'=>'Implantação dos Controles',
'mx_controls_revision'=>'Revisão dos Controles',
'mx_controls_test'=>'Teste dos Controles',
'mx_controls_without_associated_risks'=>'Controles sem Riscos Associados',
'mx_document_total'=>'Quantidade de Documentos',
'mx_documents_by_state'=>'Quantidade de Documentos por Status',
'mx_documents_by_type'=>'Quantidade de Documentos por Tipo',
'mx_implemented_controls_investment'=>'Investimento dos Controles Implementados',
'mx_incident_total'=>'Quantidade de Acidentes',
'mx_incidents_by_state'=>'Quantidade de Acidentes por Estado',
'mx_nc_per_ap_average'=>'Quantidade média de Não Conformidades por Plano de Ação',
'mx_non_conformity_by_state'=>'Quantidade de Não Conformidades por Estado',
'mx_non_conformity_total'=>'Quantidade de Não Conformidades',
'mx_non_parameterized_risks'=>'Riscos não Estimados',
'mx_not_measured_implemented_controls'=>'Controles Implementados não Medidos',
'mx_occupation_documents'=>'Ocupação dos Arquivos de Documentos (em MB)',
'mx_occupation_registers'=>'Ocupação dos Arquivos de Registro (em MB)',
'mx_occupation_template'=>'Ocupação dos Arquivos de Modelo (em MB)',
'mx_occupation_total'=>'Ocupação Total (em MB)',
'mx_process_assets_amount'=>'Quantidade de A.P.S. do Processo',
'mx_process_assets_cost'=>'Custo dos A.P.S. do Processo',
'mx_process_controls_amount'=>'Quantidade de Controles do Processo',
'mx_process_controls_cost'=>'Custo dos Controles do Processo',
'mx_process_risks_amount'=>'Quantidade de Riscos do Processo',
'mx_process_total_impact'=>'Total de Impacto do Processo',
'mx_process_values'=>'Valores do Processo',
'mx_processes_amount'=>'Quantidade de Processos',
'mx_processes_risk_summary'=>'Sumário de Risco dos Processos',
'mx_processes_without_assets'=>'Processos sem A.P.S.',
'mx_read_documents'=>'Documentos Lidos',
'mx_read_documents_proportion'=>'Proporção de Documentos Lidos',
'mx_register_total'=>'Quantidade de Registros',
'mx_risk_treatment_potential_impact'=>'Impacto Potencial do Tratamento do Risco',
'mx_risks_amount'=>'Quantidade de Riscos',
'mx_risks_potential_impact'=>'Impacto Potencial dos Riscos',
'mx_risks_treatment'=>'Tratamento dos Riscos',
'mx_scheduled_documents'=>'Proporção de Documentos que Possuem Revisão Agendada',
'mx_scheduled_revision_documents'=>'Documentos Com Revisão Agendada',
'mx_standard_best_practice_amount'=>'Quantidade de Requisitos Legais da Legislação',
'mx_total_documents'=>'Total de Documentos',
'mx_unread_documents'=>'Documentos Não Lidos',
'mx_unread_documents_proportion'=>'Proporção de Documentos Não Lidos',
'si_abnormalities'=>'Anormalidades',
'si_action_plan_statistics'=>'Estatísticas do Plano de Ação',
'si_area_statistics'=>'Estatísticas de Área',
'si_asset_statistics'=>'Estatísticas de A.P.S.',
'si_best_practice_summary'=>'Sumário de Requisitos Legais',
'si_continual_improvement'=>'Estatísticas da Melhoria Contínua',
'si_control_summary'=>'Sumário de Controle',
'si_financial_statistics'=>'Estatísticas Financeiras',
'si_management_level_and_range'=>'Abrangência e Nível de Gerenciamento',
'si_policy_management'=>'Estatísticas da Gestão de Documentos',
'si_process_statistics'=>'Estatísticas de Processo',
'si_risk_summary'=>'Sumário de Risco',
'si_risks_summary'=>'Sumário de Riscos',
'si_system_status'=>'Status do Sistema',
'st_accepted_risk'=>'Risco Aceito',
'st_applied_best_practices_amount'=>'Qtd. de Requisitos Legais Aplicados',
'st_best_practices_amount'=>'Qtd. de Requisitos Legais',
'st_complete_areas'=>'Áreas Completas',
'st_complete_assets'=>'A.P.S. Completos',
'st_complete_processes'=>'Processos Completos',
'st_controls_being_implemented'=>'Controles em Implantação',
'st_controls_correctly_implemented'=>'Controles Implementados Adequadamente',
'st_controls_incorrectly_implemented'=>'Controles Implementados Inadequadamente',
'st_controls_with_delayed_implementation'=>'Controles com Implantação Atrasada',
'st_denied_access_to_statistics'=>'A configuração referente a coleta de dados está desabilitada, assim, não é possivel visualizar esta tela.',
'st_efficient_action_plan'=>'Planos de ação eficientes',
'st_medium_high_not_treated_risk'=>'Risco Médio / Alto / Não Tratado',
'st_mitigated_risk'=>'Risco Reduzido',
'st_non_efficient_action_plan'=>'Planos de ação não eficientes',
'st_not_treated_risks'=>'Riscos não Tratados',
'st_partial_areas'=>'Áreas Parcias',
'st_partial_assets'=>'A.P.S. Parcias',
'st_partial_processes'=>'Processos Parcias',
'st_potentially_low_risk'=>'Risco Potencialmente Baixo',
'st_successfully_revised'=>'Revisados com sucesso',
'st_successfully_tested'=>'Testados com sucesso',
'st_tranferred_risk'=>'Risco Transferido',
'st_treated_risks'=>'Riscos Tratados',
'st_unmanaged_areas'=>'Áreas Não Gerenciadas',
'st_unmanaged_assets'=>'A.P.S. Não Gerenciados',
'st_unmanaged_processes'=>'Processos Não Gerenciados',
'st_unsuccessfully_revised'=>'Revisados sem sucesso',
'st_unsuccessfully_tested'=>'Testados sem sucesso',

/* './nav_statistics.xml' */

'lb_finish_cl'=>'Fim:',
'lb_statistics_cl'=>'Estatísticas:',
'rb_adequate'=>'Adequados',
'rb_ap_efficient'=>'Eficientes',
'rb_ap_non_efficient'=>'Não eficientes',
'rb_in_implantation'=>'Em implantação',
'rb_inadequate'=>'Inadequados',
'rb_late'=>'Atrasados',
'rb_not_successfully'=>'Sem sucesso',
'rb_real_risk'=>'Risco Potencial',
'rb_residual_risk'=>'Risco Residual',
'rb_successfully'=>'Com sucesso',
'vb_refresh'=>'Atualizar',

/* './nav_summary_advanced.php' */

'gc_amount_bl'=>'<b>Qtde.</b>',
'gc_applied_bl'=>'<b>Aplicadas</b>',
'gc_assets_bl'=>'<b>A.P.S.</b>',
'gc_best_practices_bl'=>'<b># de Requisitos Legais</b>',
'gc_description_bl'=>'<b>Descrição</b>',
'gc_disciplinary_process_amount_bl'=>'<b>Qtde. Processos Disciplinares</b>',
'gc_doc_rev_qty_bl'=>'<b>Qtde.</b>',
'gc_doc_sumary_percentage_bl'=>'<b>%</b>',
'gc_doc_sumary_qty_bl'=>'<b>Qtde.</b>',
'gc_document_bl'=>'<b>Documento</b>',
'gc_efficiency_rates_bl'=>'<b>Taxas de Eficácia</b>',
'gc_financial_impact_bl'=>'<b>Impacto Financeiro</b>',
'gc_implemented_controls_bl'=>'<b>Controles Implementados</b>',
'gc_incident_amount_bl'=>'<b>Qtde. Acidentes</b>',
'gc_investment_bl'=>'<b>Investimento</b>',
'gc_nc_amount'=>'<b>Qtde. Não Conformidades</b>',
'gc_nc_amount_bl'=>'<b>Qtde. Não Conformidades</b>',
'gc_planned_controls_bl'=>'<b>Controles Planejados</b>',
'gc_potential_bl'=>'<b>Potencial</b>',
'gc_potential_impact_bl'=>'<b>Impacto Potencial</b>',
'gc_processes_bl'=>'<b>Processos</b>',
'gc_residual_bl'=>'<b>Residual</b>',
'gc_standards_bl'=>'<b>Legislações</b>',
'gc_status_bl'=>'<b>Status</b>',
'gc_total_bl'=>'<b>Total</b>',
'gc_users_bl'=>'<b>Funcionário</b>',
'gs_accepted_risk'=>'Risco Aceito',
'gs_amount_bl'=>'<b>Qtde.</b>',
'gs_applicability_statement'=>'Declaração de Aplicabilidade',
'gs_avoided_risk'=>'Risco Evitado',
'gs_ci_ap_without_doc'=>'Plano de Ação sem Documento',
'gs_ci_cn_without_ap'=>'Não conformidade sem Plano de Ação',
'gs_ci_incident_without_occurrence'=>'Acidentes sem Incidentes',
'gs_ci_incident_without_risks'=>'Acidentes sem Risco',
'gs_ci_pending_tasks'=>'Tarefas pendentes da Melhoria Contínua',
'gs_components_without_document'=>'Componentes sem Documento',
'gs_controls_without_associated_risk'=>'Controles sem risco associado',
'gs_delayed'=>'Atrasados',
'gs_high_risk'=>'Risco Alto',
'gs_medium_high_risk'=>'Risco Médio Alto',
'gs_medium_low_risk'=>'Risco Médio Baixo',
'gs_impact_left_bl'=>'<b>Impacto Restante</b>',
'gs_implemented_controls_mitigated_risks_bl'=>'Controles Implementados/Riscos Reduzidos',
'gs_implemented_controls_protected_assets_bl'=>'Controles Implementados/A.P.S. Protegidos',
'gs_implemented_controls_treated_risks_bl'=>'Controles Implementados/Riscos Tratados',
'gs_inefficient_controls'=>'Controles não eficientes',
'gs_low_risk'=>'Risco Baixo',
'gs_medium_risk'=>'Risco Médio',
'gs_mitigated_risk'=>'Risco Reduzido',
'gs_non_parameterized_risks'=>'Riscos não estimados',
'gs_not_measured_controls'=>'Controles não medidos',
'gs_not_treated'=>'Não Tratados',
'gs_not_treated_risks_high_and_medium'=>'Riscos Não Tratados - Alto e Médio',
'gs_not_trusted_controls'=>'Controles não confiáveis',
'gs_pm_docs_with_pendant_read'=>'Documentos com leitura pendente',
'gs_pm_documents_without_register'=>'Documentos sem Registros',
'gs_pm_incident_without_risks'=>'Documentos com uma alta frequência de revisão',
'gs_pm_never_read_documents'=>'Documentos nunca lidos',
'gs_pm_pending_tasks'=>'Tarefas pendentes da Gestão de Documentos',
'gs_pm_users_with_pendant_read'=>'Usuários com leitura pendente',
'gs_potential_risks_low'=>'Riscos Potenciais - Baixo',
'gs_risk_management_pending_tasks'=>'Tarefas pendentes da Gestão de Aspectos e Impactos',
'gs_risk_treatment_plan'=>'Plano de Tratamento do Risco',
'gs_scope_statement'=>'Declaração de Escopo',
'gs_sgsi_policy_statement'=>'Declaração da Política do SGSI',
'gs_total_bl'=>'<b>Total</b>',
'gs_total_non_parameterized_risks_bl'=>'<b>Riscos Não Estimados</b>',
'gs_total_parameterized_risks_bl'=>'<b>Riscos Estimados</b>',
'gs_total_risks_bl'=>'<b>Total</b>',
'gs_transferred_risk'=>'Risco Transferido',
'gs_treated'=>'Tratados',
'gs_trusted_and_efficient_controls'=>'Controles confiáveis e eficientes',
'gs_under_implementation'=>'Em processo de implantação',
'tt_help_impact_left'=>'<b>Impacto Restante</b><br/><br/>Soma dos impactos dos riscos que restaram após o tratamento.<br/>Este valor representa o total de impacto potencial a que a empresa está sujeita após o tratamento dos riscos.',
'tt_help_implemented_controls_mitigated_risks'=>'<b>Controles Implementados/Riscos Reduzidos</b><br/><br/>Razão entre a soma de custos dos controles implementados e a soma dos impactos potenciais dos Riscos Reduzidos.<br/>Este valor demonstra qual o custo de controle frente a soma do potencial impacto de risco para a empresa em termos financeiros. Esta razão inclui apenas o valor dos riscos que foram reduzidos pela aplicação de controles.',
'tt_help_implemented_controls_protected_assets'=>'<b>Controles Implementados/A.P.S. Protegidos</b><br/><br/>Razão entre a soma de custos dos controles implementados e o valor dos A.P.S. protegidos.<br/>É importante não gastar mais com controles do que o valor dos A.P.S. protegidos.',
'tt_help_implemented_controls_treated_risks'=>'<b>Controles Implementados/Riscos Tratados</b><br/><br/>Razão entre a soma de custos dos controles implementados e a soma dos impactos potenciais dos Riscos Tratados.<br/>Este valor demonstra qual o custo de controle frente a soma do potencial impacto de risco para a empresa em termos financeiros. Esta razão inclui todos os riscos tratados por qualquer uma das formas de tratamento possíveis.',
'tt_help_inefficient_controls'=>'<b>Controles Não Eficazes</b><br/><br/>Controles que não possuem revisão de eficácia atrasada e cuja última revisão foi considerada como ineficiente (eficiência real do controle menor que a eficiência esperada deste controle)',
'tt_help_not_measured_controls'=>'<b>Controles Não Medidos</b><br/><br/>Controles implementados que não possuem revisão de eficácia ou teste ou cuja medição, revisão de eficiência ou teste, expirou.',
'tt_help_trusted'=>'<b>Controles não Confiáveis</b><br/><br/>Controles que não possuem teste atrazado e cujo último teste foi marcado',
'tt_help_trusted_and_efficient'=>'<b>Controles Confiáveis e Eficientes</b><br/><br/>Controles que possuem teste e revisão de eficácia, que tanto o teste quanto a revisão de eficiência não estão atrazados, que o último teste realizado foi confiável e que a última revisão realizada foi eficiênte (eficiência real do controle maior ou igual a eficiência esperada este controle)',
'gs_risk_bl'=>'<b>Risco</b>',
'gs_asset_bl'=>'<b>Ativo</b>',
'gs_process_bl'=>'<b>Processo</b>',
'gs_area_bl'=>'<b>Área</b>',

/* './nav_summary_advanced.xml' */

'mi_excel_report'=>'Relatório EXCEL',
'mi_html_report'=>'Relatório HTML',
'mi_html_report_download'=>'Relatório HTML DOWNLOAD',
'mi_pdf_report'=>'Relatório PDF',
'mi_word_report'=>'Relatório WORD',
'tt_abnormalities_pm_summary_bl'=>'<b>Sumário de Anormalidades da Gestão de Documentos</b>',
'tt_abnormalities_rm_summary_bl'=>'<b>Sumário de Anormalidades da Gestão de Aspectos e Impactos</b>',
'tt_abnormality_ci_summary_bl'=>'<b>Sumário de Anormalidades da Melhoria Contínua</b>',
'tt_best_practices_summary_bl'=>'<b>Sumário de Requisitos Legais</b>',
'tt_controls_summary_bl'=>'<b>Sumário de Controles</b>',
'tt_documents_average_approval_time_bl'=>'<b>Tempo Médio de Aprovação dos Documentos</b>',
'tt_financial_summary_bl'=>'<b>Sumário Financeiro</b>',
'tt_general_risks_summary_bl'=>'<b>Risco Estimado vs. Não-Estimado</b>',
'tt_grid_incident_summary_title_bl'=>'<b>Sumário de Incidentes</b>',
'tt_grid_incidents_per_asset_title_bl'=>'<b>Top 5 A.P.S. com mais Acidentes</b>',
'tt_grid_incidents_per_process_title_bl'=>'<b>Top 5 Processos com mais Acidentes</b>',
'tt_grid_incidents_per_user_title_bl'=>'<b>Processo Disciplinar: Top 5</b>',
'tt_grid_nc_per_process_title_bl'=>'<b>Top 5 Processos com mais Não Conformidades</b>',
'tt_grid_nc_summary_title_bl'=>'<b>Sumário de Não Conformidades</b>',
'tt_incident_summary_bl'=>'<b>Sumários da Melhoria Contínua</b>',
'tt_last_documents_revision_time_period_summary_bl'=>'<b>Sumário do Período de Tempo da Última Revisão dos Documentos</b>',
'tt_parameterized_risks_summary_bl'=>'<b>Riscos Potenciais vs. Riscos Residuais</b>',
'tt_policy_management_summary_bl'=>'<b>Sumário de Gestão de Documentos</b>',
'tt_risk_management_documentation_summary_bl'=>'<b>Sumário de Documentação da Gestão de Aspectos e Impactos</b>',
'tt_risk_management_status_bl'=>'<b>Nível de Risco</b>',
'tt_risk_management_x_policy_management_bl'=>'<b>Gestão de Aspectos e Impactos x Gestão de Documentos</b>',
'tt_sgsi_documents_and_reports_bl'=>'<b>Relatórios e Documentos do SGSI</b>',
'tt_top_10_most_read_documents_bl'=>'<b>Top 10 Documentos mais lidos</b>',

/* './nav_summary_basic.php' */

'gc_documents_qty_bl'=>'<b>Qtde. Documentos</b>',
'gc_documents_status_bl'=>'<b>Status dos Documentos</b>',
'gc_percentage_bl'=>'<b>%</b>',
'gc_qty_bl'=>'<b>Qtde.</b>',
'gc_readings_bl'=>'<b>Leituras</b>',
'gc_time_period_smaller_or_equal_bl'=>'<b>Período de Tempo (menor ou igual a)</b>',
'gs_area'=>'Área',
'gs_areas_without_processes'=>'Áreas sem processos',
'gs_asset'=>'A.P.S.',
'gs_assets_without_risk_events'=>'A.P.S. sem eventos de risco',
'gs_average_time'=>'Tempo Médio',
'gs_controls_without_risk_events_association'=>'Controles sem associação com eventos de risco',
'gs_documents_to_be_read'=>'Documentos com leitura pendente',
'gs_documents_total_bl'=>'<b>Total de Documentos</b>',
'gs_documents_with_files'=>'Documentos com Arquivos',
'gs_documents_with_links'=>'Documentos com Links',
'gs_more_than_six_months'=>'Maior que Seis Meses',
'gs_na'=>'NA',
'gs_one_month'=>'Um Mês',
'gs_one_week'=>'Uma Semana',
'gs_process'=>'Processo',
'gs_processes_without_assets'=>'Processos sem A.P.S.',
'gs_published_documents_information_bl'=>'<b>Informações sobre Documentos Publicados</b>',
'gs_risk'=>'Risco',
'gs_six_months'=>'Seis Meses',
'gs_three_months'=>'Três Meses',

/* './nav_summary_basic.xml' */

'gc_period'=>'Período',
'tt_my_documents_average_approval_time_bl'=>'<b>Tempo Médio de Aprovação dos Meus Documentos</b>',
'tt_my_documents_last_revision_average_time_summary_bl'=>'<b>Sumário do Tempo Médio da última Revisão dos Meus Documentos</b>',
'tt_my_documents_policy_management_summary_bl'=>'<b>Sumario de Gestão de Documentos (Meus Documentos)</b>',
'tt_my_elements_bl'=>'<b>Meus Elementos</b>',
'tt_my_pendencies_bl'=>'<b>Minhas Pendências da Gestão de Aspectos e Impactos</b>',
'tt_my_risk_summary_bl'=>'<b>Meu Sumário de Risco</b>',
'tt_policy_management_x_risk_management_my_elements_bl'=>'<b>Gestão de Aspectos e Impactos (Meus Elementos) x Gestão de Documentos</b>',
'tt_policy_sumary_bl'=>'<b>Sumários da Gestão de Documentos</b>',
'tt_risk_management_my_elements_documentation_summary_bl'=>'<b>Sumário de Documentação dos Meus Elementos da Gestão de Aspectos e Impactos</b>',
'tt_risk_summary_bl'=>'<b>Sumários da Gestão de Aspectos e Impactos</b>',
'tt_top_10_most_read_my_documents_bl'=>'<b>Top 10 Meus Documentos Mais Lidos</b>',

/* './nav_warnings.xml' */

'gc_inquirer'=>'Requerente',
'gc_justif'=>'Just.',
'mi_denied'=>'Negar',
'mi_open_task'=>'Abrir tarefa',
'tt_alerts_bl'=>'<b>Alertas</b>',
'tt_tasks_bl'=>'<b>Tarefas</b>',
'vb_remove_alerts'=>'Remover Alertas',
'wn_event_already_approved_removed'=>'Evento já aprovado ou removido.',
'wn_removal_warning'=>'Pelo menos um alerta deve ser selecionado para ser removido.',

/* './packages/admin/acl_tree.xml' */

'to_acl_A'=>'Permissões para a área \'Admin\' do sistema',
'to_acl_A.A'=>'Aba de Auditoria',
'to_acl_A.A.1'=>'Aba com a lista de tarefas geradas pelo sistema',
'to_acl_A.A.2'=>'Aba com a lista de alertas gerados pelo sistema',
'to_acl_A.A.3'=>'Aba com as ações executadas no sistema',
'to_acl_A.C'=>'Aba de customizações do sistema',
'to_acl_A.C.1'=>'Customização dos nomes e da quantidade dos parâmetros de estimativa de risco',
'to_acl_A.C.10'=>'Customização do peso de cada um dos parâmetros de risco',
'to_acl_A.C.11'=>'Customização da classificação do sistema',
'to_acl_A.C.2'=>'Customização da matriz de definição de risco, nomes da estimativa da importância do A.P.S., de impacto, probabilidade e dos critérios para a redução dos controles',
'to_acl_A.C.3'=>'Customização dos parâmetros da estimativa de custo',
'to_acl_A.C.4'=>'Customização dos parâmetros de impacto financeiro',
'to_acl_A.C.5'=>'Customização de varias características do sistema',
'to_acl_A.C.6'=>'Customização dos usuários especiais',
'to_acl_A.C.7'=>'Customização da quantidade e do nome de tipos e prioridades utilizados no sistema',
'to_acl_A.C.7.1'=>'Configuração da classificação das áreas relativo ao tipo e à prioridade',
'to_acl_A.C.7.2'=>'Configuração da classificação dos processos relativo ao tipo e à prioridade',
'to_acl_A.C.7.3'=>'Configuração da classificação dos riscos relativo ao tipo',
'to_acl_A.C.7.4'=>'Configuração da classificação dos eventos relativo ao tipo',
'to_acl_A.C.7.5'=>'Configuração da classificação dos controles relativo ao tipo',
'to_acl_A.C.7.6'=>'Configuração da classificação dos documentos relativo ao tipo',
'to_acl_A.C.7.7'=>'Configuração da classificação dos registros relativo ao tipo',
'to_acl_A.C.8'=>'Customização da segurança dos controles',
'to_acl_A.C.9'=>'Customização do nível de aceitação de risco',
'to_acl_A.L'=>'Aba da Lixeira',
'to_acl_A.L.1'=>'Botão para restaurar os elementos selecionados',
'to_acl_A.L.2'=>'Botão para remover os elementos selecionados',
'to_acl_A.L.3'=>'Botão para remover todos os elementos da lixeira',
'to_acl_A.MA'=>'Aba de gerenciamento de conta',
'to_acl_A.P'=>'Aba de Perfis',
'to_acl_A.P.1'=>'Menu para editar um determinado perfil',
'to_acl_A.P.2'=>'Menu para remover um determinado perfil',
'to_acl_A.P.3'=>'Botão de adicionar um perfil',
'to_acl_A.S'=>'Aba de configurações do sistema',
'to_acl_A.S.1'=>'Configuração da política de senhas',
'to_acl_A.S.2'=>'Configuração do \'Delete Cascade\' do sistema',
'to_acl_A.U'=>'Aba de Usuários',
'to_acl_A.U.1'=>'Menu para editar um determinado usuário',
'to_acl_A.U.2'=>'Menu para remover um determinado usuário',
'to_acl_A.U.3'=>'Botão de adicionar um usuário',
'to_acl_A.U.4'=>'Botão para resetar a senha de todos usuários',
'to_acl_A.U.5'=>'Menu para bloquear usuários do sistema',
'to_acl_M'=>'Permissões para a área \'Visão do usuário\' do sistema',
'to_acl_M.CI'=>'Aba de melhoria contínua',
'to_acl_M.CI.1'=>'Aba de incidentes',
'to_acl_M.CI.1.1'=>'Permissão de visualizar todos os incidentes',
'to_acl_M.CI.1.2'=>'Botão para inserir incidentes',
'to_acl_M.CI.1.3'=>'Menu para editar mesmo que o usuário não seja o gestor de acidente nem o criador do incidente',
'to_acl_M.CI.1.4'=>'Menu para remover mesmo que o usuário não seja o gestor de acidente nem o criador do incidente',
'to_acl_M.CI.2'=>'Aba de acidente',
'to_acl_M.CI.2.1'=>'Permissão de visualizar todos acidentes',
'to_acl_M.CI.2.2'=>'Botão de inserção de acidente',
'to_acl_M.CI.2.3'=>'Menu de editar mesmo que o usuário não seja o gestor de acidente',
'to_acl_M.CI.2.4'=>'Menu de remover mesmo que o usuário não seja o gestor de acidente',
'to_acl_M.CI.3'=>'Aba de não conformidade',
'to_acl_M.CI.3.1'=>'Permissão de visualizar todas as não conformidades',
'to_acl_M.CI.3.2'=>'Botão de inserção de não conformidade',
'to_acl_M.CI.3.3'=>'Menu de editar mesmo que o usuário não seja o gestor de não conformidade',
'to_acl_M.CI.3.4'=>'Menu de remover mesmo que o usuário não seja o gestor de não conformidade',
'to_acl_M.CI.5'=>'Aba de processo disciplinar',
'to_acl_M.CI.5.1'=>'Permissão para ver a ação tomada em relação a um usuário envolvido em um determinado processo disciplinar',
'to_acl_M.CI.5.2'=>'Permissão para registrar a ação que deve ser tomada em relação a um usuário envolvido em um determinado processo disciplinar',
'to_acl_M.CI.5.3'=>'Permissão para remover um usuário de um determinado processo disciplinar',
'to_acl_M.CI.6'=>'Aba de relatórios',
'to_acl_M.CI.6.1'=>'Relatórios de acidente',
'to_acl_M.CI.6.1.1'=>'Relatório de incidentes por acidente',
'to_acl_M.CI.6.1.2'=>'Relatório de processo disciplinar',
'to_acl_M.CI.6.1.3'=>'Relatório de coleta de evidências',
'to_acl_M.CI.6.1.4'=>'Relatório de acompanhamento de incidente',
'to_acl_M.CI.6.1.5'=>'Relatório de acompanhamento de acidente',
'to_acl_M.CI.6.1.6'=>'Relatório de impacto financeiro por acidente',
'to_acl_M.CI.6.1.7'=>'Relatório de revisões de controles ocasionadas por acidente',
'to_acl_M.CI.6.1.8'=>'Relatório de elementos afetados por acidentes',
'to_acl_M.CI.6.1.9'=>'Relatório de cálculo automático de probabilidade dos riscos',
'to_acl_M.CI.6.2'=>'Relatórios de não conformidade',
'to_acl_M.CI.6.2.1'=>'Relatório de não conformidade por processo',
'to_acl_M.CI.6.2.2'=>'Relatório de não conformidade de controle por processo',
'to_acl_M.CI.6.2.3'=>'Relatório de acompanhamento de não conformidade',
'to_acl_M.CI.6.2.4'=>'Relatório de plano de ação por usuário',
'to_acl_M.CI.6.2.5'=>'Relatório de plano de ação por processo',
'to_acl_M.CI.6.3'=>'Relatórios de melhoria contínua',
'to_acl_M.CI.6.3.1'=>'Relatórios das tarefas pendentes da melhoria contínua',
'to_acl_M.CI.6.3.2'=>'Relatório de acidentes sem risco associado.',
'to_acl_M.CI.6.3.3'=>'Relatório de não conformidades sem planos de ação associados.',
'to_acl_M.CI.6.3.4'=>'Relatórios de Plano de Ação sem Documento',
'to_acl_M.CI.6.3.5'=>'Relatório de acidentes sem incidentes',
'to_acl_M.CI.7'=>'Aba de plano de ação',
'to_acl_M.CI.7.1'=>'Permissão de visualizar todos os planos de ação',
'to_acl_M.CI.7.2'=>'Botão de inserção de plano de ação',
'to_acl_M.CI.7.3'=>'Permissão para editar qualquer plano de ação',
'to_acl_M.CI.7.4'=>'Permissão para remover qualquer plano de ação',
'to_acl_M.D'=>'Aba Dashboard',
'to_acl_M.D.1'=>'Aba de Sumário',
'to_acl_M.D.2'=>'Aba de Sumário Avançado',
'to_acl_M.D.3'=>'Aba de Gráficos',
'to_acl_M.D.6'=>'Aba de Estatísticas (gráficos) do sistema',
'to_acl_M.D.6.1'=>'Estatísticas sobre as áreas de negócio gerenciadas pelo sistema',
'to_acl_M.D.6.10'=>'Estatisticas sobre o status do sistema (sumário de risco x área, risco x processo, risco x A.P.S., sumário de riscos)',
'to_acl_M.D.6.11'=>'Estatísticas da gestão de documentos do sistema',
'to_acl_M.D.6.12'=>'Estatísticas da melhoria contínua do sistema',
'to_acl_M.D.6.13'=>'Estatísticas dos planos de ação e da relação entre plano de ação e não conformidade',
'to_acl_M.D.6.2'=>'Estatísticas sobre os processos das áreas de negócio gerenciadas pelo sistema',
'to_acl_M.D.6.3'=>'Estatísticas sobre os A.P.S. gerenciados pelo sistema',
'to_acl_M.D.6.4'=>'Sumário dos riscos gerenciados pelo sistema',
'to_acl_M.D.6.5'=>'Sumário de requisitos legais',
'to_acl_M.D.6.6'=>'Sumário dos controles do sistema',
'to_acl_M.D.6.7'=>'Sumário de anormalidades detectadas pelo sistema',
'to_acl_M.D.6.8'=>'Estatísticas financeiras - custos, impacto financeiro e investimento',
'to_acl_M.D.6.9'=>'Estatisticas sobre a abrangência e o gerenciamento de areas, processos e A.P.S.',
'to_acl_M.L'=>'Aba de Gerenciamento de Risco',
'to_acl_M.L.1'=>'Aba da Biblioteca de Eventos',
'to_acl_M.L.1.1'=>'Menu para navegar nas sub-categorias e eventos de uma determinada categoria',
'to_acl_M.L.1.2'=>'Menu para editar uma determinada categoria',
'to_acl_M.L.1.3'=>'Menu para remover uma determinada categoria',
'to_acl_M.L.1.4'=>'Menu para associar requisitos legais a um determinado evento',
'to_acl_M.L.1.5'=>'Menu para editar um determinado evento',
'to_acl_M.L.1.6'=>'Menu para remover um determinado evento',
'to_acl_M.L.1.7'=>'Botão de adicionar nova categoria',
'to_acl_M.L.1.8'=>'Botão de adicionar novo evento',
'to_acl_M.L.1.9'=>'Botão para associar um evento a categoria em que o usuário se encontra',
'to_acl_M.L.2'=>'Aba da Biblioteca de Requisitos Legais',
'to_acl_M.L.2.1'=>'Menu para navegar nas sub-seções e requisitos legais de uma determinada seção',
'to_acl_M.L.2.10'=>'Permite ao usuário criar um modelo de documento para um requisito legal',
'to_acl_M.L.2.2'=>'Menu para editar uma determinada seção',
'to_acl_M.L.2.3'=>'Menu para remover uma determinada seção',
'to_acl_M.L.2.4'=>'Menu para associar eventos a um determinado requisito legal',
'to_acl_M.L.2.5'=>'Menu para editar um determinado requisito legal',
'to_acl_M.L.2.6'=>'Menu para remover um determinado requisito legal',
'to_acl_M.L.2.7'=>'Botão de adicionar nova seção',
'to_acl_M.L.2.8'=>'Botão de adicionar novo requisito legal',
'to_acl_M.L.2.9'=>'Exibe os modelos de documentos de requisito legal na aba de modelos de documento',
'to_acl_M.L.3'=>'Aba da Biblioteca de Legislações',
'to_acl_M.L.3.1'=>'Menu para editar uma determinada legislação',
'to_acl_M.L.3.2'=>'Menu para remover uma determinada legislação',
'to_acl_M.L.3.3'=>'Botão de adicionar nova legislação',
'to_acl_M.L.4'=>'Aba da Biblioteca de Requisitos Legais',
'to_acl_M.L.4.1'=>'Menu para editar uma determinada seção',
'to_acl_M.L.4.2'=>'Menu para editar uma determinada seção',
'to_acl_M.L.5'=>'Aba de Modelos de Documento',
'to_acl_M.L.5.1'=>'Menu para editar um modelo de documento',
'to_acl_M.L.5.2'=>'Menu para remover um modelo de documento',
'to_acl_M.L.5.3'=>'Botão de adicionar novo modelo de documento',
'to_acl_M.L.6'=>'Aba de biblioteca de categorias',
'to_acl_M.L.6.1'=>'Botão de inserção de categoria',
'to_acl_M.L.6.2'=>'Menu de editar categoria',
'to_acl_M.L.6.3'=>'Menu de remover categoria',
'to_acl_M.L.6.4'=>'Botão de inserção de solução',
'to_acl_M.L.6.5'=>'Menu de editar solução',
'to_acl_M.L.6.6'=>'Menu de remover solução',
'to_acl_M.PM'=>'Aba de Gestão de Documentos',
'to_acl_M.PM.10'=>'Aba de visualização / gerenciamento de documentos em desenvolvimento',
'to_acl_M.PM.10.1'=>'Botão de inserir documento',
'to_acl_M.PM.11'=>'Aba de visualização / gerenciamento de documentos em aprovação',
'to_acl_M.PM.12'=>'Aba de visualização / gerenciamento de documentos publicados',
'to_acl_M.PM.13'=>'Aba de visualização / gerenciamento de documentos em revisão',
'to_acl_M.PM.14'=>'Aba de visualização / gerenciamento de documentos obsoletos',
'to_acl_M.PM.15'=>'Aba de visualização / gerenciamento de todos documentos',
'to_acl_M.PM.16'=>'Aba de visualização / gerenciamento de registros',
'to_acl_M.PM.17'=>'Aba de Relatórios',
'to_acl_M.PM.17.1'=>'Outros relatórios',
'to_acl_M.PM.17.1.16'=>'Relatório que lista as propriedades de um determinado documento',
'to_acl_M.PM.17.1.17'=>'Relatório de classificação',
'to_acl_M.PM.17.2'=>'Relatórios Gerais',
'to_acl_M.PM.17.2.1'=>'Relatório de documentos',
'to_acl_M.PM.17.2.2'=>'Relatório de documentos por área',
'to_acl_M.PM.17.2.3'=>'Relatório de documentos por componente do SGSI',
'to_acl_M.PM.17.2.4'=>'Relatório de comentários de modificação e justificativas de revisão de documentos',
'to_acl_M.PM.17.2.5'=>'Relatório de comentários de usuários',
'to_acl_M.PM.17.3'=>'Relatórios de Registro',
'to_acl_M.PM.17.3.1'=>'Relatório de registro por documento',
'to_acl_M.PM.17.4'=>'Relatórios de Controle de Acesso',
'to_acl_M.PM.17.4.1'=>'Relatório de auditoria de acesso dos usuários',
'to_acl_M.PM.17.4.2'=>'Relatório de auditoria de acesso aos documentos',
'to_acl_M.PM.17.4.3'=>'Relatório de documentos e seus aprovadores',
'to_acl_M.PM.17.4.4'=>'Relatório de documentos e seus leitores',
'to_acl_M.PM.17.4.5'=>'Relatório de usuários associados aos processos',
'to_acl_M.PM.17.5'=>'Relatórios de Gestão',
'to_acl_M.PM.17.5.1'=>'Relatório de documentos com leitura pendente',
'to_acl_M.PM.17.5.2'=>'Relatório de documentos mais acessados',
'to_acl_M.PM.17.5.3'=>'Relatório de documentos não acessados',
'to_acl_M.PM.17.5.4'=>'Relatório de documentos mais revisados',
'to_acl_M.PM.17.5.5'=>'Relatório de aprovações pendentes',
'to_acl_M.PM.17.5.6'=>'Relatório de data de criação e revisão dos documentos',
'to_acl_M.PM.17.5.7'=>'Relatório de cronograma de revisão de documentos',
'to_acl_M.PM.17.5.8'=>'Relatório de documentos com revisão atrasada',
'to_acl_M.PM.17.6'=>'Relatórios de Anormalidade',
'to_acl_M.PM.17.6.1'=>'Relatório de componentes sem documentos',
'to_acl_M.PM.17.6.2'=>'Tarefas pendentes da gestão de documentos',
'to_acl_M.PM.17.6.3'=>'Documentos que possuem uma freqüência alta de revisão. Freqüência alta significa documentos que são revisados em média mais de uma vez por semana.',
'to_acl_M.PM.17.6.4'=>'Relatório de documentos sem registros',
'to_acl_M.PM.17.6.5'=>'Relatório que lista os usuários que são leitores de documentos que são publicados mas que ainda não leram esses documentos.',
'to_acl_M.PM.18'=>'Aba de visualização / gerenciamento de documentos aguardando data de publicação',
'to_acl_M.RM'=>'Aba de Gerenciamento de Risco',
'to_acl_M.RM.1'=>'Aba de Área',
'to_acl_M.RM.1.1'=>'Permissão de visualizar todas áreas',
'to_acl_M.RM.1.2'=>'Menu para navegar nas sub-áreas de uma determinada área',
'to_acl_M.RM.1.3'=>'Menu para visualizar processos de uma determinada área',
'to_acl_M.RM.1.4'=>'Menu para editar uma determinada área',
'to_acl_M.RM.1.5'=>'Menu para remover uma determinada área',
'to_acl_M.RM.1.6'=>'Botão de adicionar uma nova área',
'to_acl_M.RM.1.7'=>'Botão de adicionar uma nova área na raiz',
'to_acl_M.RM.1.8'=>'Permissão de editar uma área, quando responsável por ela',
'to_acl_M.RM.1.9'=>'Permissão para remover uma Área somente se responsável por ela',
'to_acl_M.RM.2'=>'Aba de Processo',
'to_acl_M.RM.2.1'=>'Permissão de visualizar todos processos',
'to_acl_M.RM.2.2'=>'Menu para navegar nos A.P.S. de um determinado processo',
'to_acl_M.RM.2.4'=>'Menu para editar um determinado processo',
'to_acl_M.RM.2.5'=>'Menu para remover um determinado processo',
'to_acl_M.RM.2.6'=>'Botão de adicionar novo processo',
'to_acl_M.RM.2.7'=>'Permissão de editar um processo, quando responsável por ele',
'to_acl_M.RM.2.8'=>'Permissão para remover um Processo somente se responsável por ele',
'to_acl_M.RM.3'=>'Aba de A.P.S.',
'to_acl_M.RM.3.1'=>'Permissão de visualizar todos A.P.S.',
'to_acl_M.RM.3.10'=>'Menu de contexto para a inserção de riscos ao A.P.S. a partir dos eventos de uma categoria da biblioteca de categorias de evento.',
'to_acl_M.RM.3.2'=>'Menu para navegar nos riscos de um determinado A.P.S.',
'to_acl_M.RM.3.4'=>'Menu para criar um risco para o A.P.S., baseado em um evento',
'to_acl_M.RM.3.5'=>'Menu para editar um determinado A.P.S.',
'to_acl_M.RM.3.6'=>'Menu para remover um determinado A.P.S.',
'to_acl_M.RM.3.7'=>'Botão de adicionar novo A.P.S.',
'to_acl_M.RM.3.8'=>'Permissão de editar um A.P.S., quando responsável por ele',
'to_acl_M.RM.3.9'=>'Permissão para remover um A.P.S. somente se responsável por ele',
'to_acl_M.RM.4'=>'Aba de Riscos',
'to_acl_M.RM.4.1'=>'Permissão de visualizar todos riscos',
'to_acl_M.RM.4.10'=>'Permissão para remover um Risco somente se responsável por ele',
'to_acl_M.RM.4.2'=>'Menu para navegar nos controles de um determinado risco',
'to_acl_M.RM.4.4'=>'Menu para editar um determinado risco',
'to_acl_M.RM.4.5'=>'Menu para remover um determinado risco',
'to_acl_M.RM.4.6'=>'Botão de adicionar novo risco',
'to_acl_M.RM.4.8'=>'Botão para estimar o risco',
'to_acl_M.RM.4.9'=>'Permissão de editar um risco, quando responsável por ele',
'to_acl_M.RM.5'=>'Aba de Controle',
'to_acl_M.RM.5.1'=>'Permissão de visualizar todos controles',
'to_acl_M.RM.5.2'=>'Menu para navegar nos riscos associados a um determinado controle',
'to_acl_M.RM.5.3'=>'Menu para associar riscos a um determinado controle',
'to_acl_M.RM.5.4'=>'Menu para editar um determinado controle',
'to_acl_M.RM.5.5'=>'Menu para remover um determinado controle',
'to_acl_M.RM.5.6'=>'Botão de adicionar novo controle',
'to_acl_M.RM.5.7'=>'Permissão de editar um controle, quando responsável por ele',
'to_acl_M.RM.5.8'=>'Permissão para remover um Controle somente se responsável por ele',
'to_acl_M.RM.5.9'=>'Alteração do histórico de revisão/teste dos controles do sistema',
'to_acl_M.RM.6'=>'Aba de Relatórios',
'to_acl_M.RM.6.1'=>'Relatórios de Riscos',
'to_acl_M.RM.6.1.1'=>'Relatório de valores dos riscos',
'to_acl_M.RM.6.1.2'=>'Relatório de riscos por processo',
'to_acl_M.RM.6.1.3'=>'Relatório de riscos por A.P.S.',
'to_acl_M.RM.6.1.4'=>'Relatório de riscos por controle',
'to_acl_M.RM.6.1.5'=>'Relatório de impacto do risco',
'to_acl_M.RM.6.1.6'=>'Relatório de Importância dos A.P.S.',
'to_acl_M.RM.6.1.7'=>'Relatório de Riscos por Área',
'to_acl_M.RM.6.1.8'=>'Relatório de Checklist por Evento',
'to_acl_M.RM.6.2'=>'Relatórios de Controles',
'to_acl_M.RM.6.2.1'=>'Relatório de controles por responsável',
'to_acl_M.RM.6.2.10'=>'Relatório de acompanhamento do teste dos controles',
'to_acl_M.RM.6.2.11'=>'Relatório de conformidade',
'to_acl_M.RM.6.2.12'=>'Relatório da agenda de revisão dos controles por responsável',
'to_acl_M.RM.6.2.13'=>'Relatório da agenda de teste dos controles por responsável',
'to_acl_M.RM.6.2.2'=>'Relatório de controles por risco',
'to_acl_M.RM.6.2.3'=>'Relatório de planejamento dos controles',
'to_acl_M.RM.6.2.4'=>'Relatório de data de implementação dos controles',
'to_acl_M.RM.6.2.5'=>'Relatório de eficácia dos controles',
'to_acl_M.RM.6.2.6'=>'Relatório de resumo dos controles',
'to_acl_M.RM.6.2.7'=>'Relatório de acompanhamento de controles',
'to_acl_M.RM.6.2.8'=>'Relatório de acompanhamento da eficácia dos controles',
'to_acl_M.RM.6.2.9'=>'Relatório de controles não medidos',
'to_acl_M.RM.6.3'=>'Relatórios de Gestão de Aspectos e Impactos',
'to_acl_M.RM.6.3.1'=>'Relatório de tarefas pendentes da gestão de aspectos e impactos',
'to_acl_M.RM.6.3.2'=>'Relatório de áreas sem processos',
'to_acl_M.RM.6.3.3'=>'Relatório de A.P.S. sem risco',
'to_acl_M.RM.6.3.4'=>'Relatório de processos sem A.P.S.',
'to_acl_M.RM.6.3.5'=>'Relatório de riscos não estimados',
'to_acl_M.RM.6.3.6'=>'Relatório de controles sem risco',
'to_acl_M.RM.6.3.7'=>'Relatório de riscos duplicados',
'to_acl_M.RM.6.4'=>'Relatórios Financeiros',
'to_acl_M.RM.6.4.4'=>'Relatório de custo por controle',
'to_acl_M.RM.6.4.5'=>'Relatório de custo dos controles por responsável',
'to_acl_M.RM.6.4.6'=>'Relatório de custo dos controles por área',
'to_acl_M.RM.6.4.7'=>'Relatório de custo dos controles por processo',
'to_acl_M.RM.6.4.8'=>'Relatório de custo dos controles por A.P.S.',
'to_acl_M.RM.6.5'=>'Relatórios ISO 27001',
'to_acl_M.RM.6.5.1'=>'Relatório de declaração de aplicabilidade',
'to_acl_M.RM.6.5.2'=>'Relatório de tratamento dos riscos',
'to_acl_M.RM.6.5.3'=>'Relatório de declaração de escopo do SGSI',
'to_acl_M.RM.6.5.4'=>'Relatório de declaração de política do SGSI',
'to_acl_M.RM.6.6'=>'Relatórios sumários',
'to_acl_M.RM.6.6.1'=>'Relatório top 10 - A.P.S. com mais riscos',
'to_acl_M.RM.6.6.2'=>'Relatório top 10 - A.P.S. com mais riscos altos',
'to_acl_M.RM.6.6.3'=>'Relatório top 10 - riscos por área',
'to_acl_M.RM.6.6.4'=>'Relatório top 10 - riscos por processo',
'to_acl_M.RM.6.6.5'=>'Relatório de quantidade de riscos por processo',
'to_acl_M.RM.6.6.6'=>'Relatório de quantidade de riscos por área',
'to_acl_M.RM.6.6.7'=>'Relatório de status de risco por área',
'to_acl_M.RM.6.6.8'=>'Relatório de risco por processo',
'to_acl_M.RM.6.7'=>'Outros relatórios',
'to_acl_M.RM.6.7.1'=>'Relatório de responsabilidades dos usuários',
'to_acl_M.RM.6.7.2'=>'Relatório de elementos pendentes',
'to_acl_M.RM.6.7.3'=>'Relatório de A.P.S.',
'to_acl_M.RM.6.7.4'=>'Relatório de classificação dos elementos',
'to_acl_M.RM.6.7.5'=>'Relatório que indica para cada A.P.S. quais suas dependências e quais seus dependentes',
'to_acl_M.RM.6.7.6'=>'Relatório que indica a relevância dos A.P.S.',
'to_acl_M.S'=>'Aba de Pesquisa',
'tr_acl_A'=>'Admim',
'tr_acl_A.A'=>'AUDITORIA',
'tr_acl_A.A.1'=>'TAREFAS',
'tr_acl_A.A.2'=>'ALERTAS',
'tr_acl_A.A.3'=>'LOG',
'tr_acl_A.C'=>'CUSTOMIZAÇÕES',
'tr_acl_A.C.1'=>'PARAMETROS DE ESTIMATIVA DE RISCO',
'tr_acl_A.C.10'=>'PESO DOS PARÂMETROS DE RISCO',
'tr_acl_A.C.11'=>'CLASSIFICAÇÃO DO SISTEMA',
'tr_acl_A.C.2'=>'MATRIZ DE DEFINIÇÃO DE RISCO',
'tr_acl_A.C.3'=>'PARÂMETROS DA ESTIMATIVA DE CUSTO',
'tr_acl_A.C.4'=>'PARÂMETROS DE IMPACTO FINANCEIRO',
'tr_acl_A.C.5'=>'CARACTERÍSTICAS DO SISTEMA',
'tr_acl_A.C.6'=>'USUÁRIOS ESPECIAIS',
'tr_acl_A.C.7'=>'PARAMETRIZAÇÃO DE TIPO E PRIORIDADE',
'tr_acl_A.C.7.1'=>'ÁREA',
'tr_acl_A.C.7.2'=>'PROCESSO',
'tr_acl_A.C.7.3'=>'RISCO',
'tr_acl_A.C.7.4'=>'EVENTO',
'tr_acl_A.C.7.5'=>'CONTROLE',
'tr_acl_A.C.7.6'=>'DOCUMENTO',
'tr_acl_A.C.7.7'=>'REGISTRO',
'tr_acl_A.C.8'=>'REVISÃO E TESTE DE CONTROLE',
'tr_acl_A.C.9'=>'NÍVEL DE ACEITAÇÃO DE RISCO',
'tr_acl_A.L'=>'LIXEIRA',
'tr_acl_A.L.1'=>'RESTAURAR',
'tr_acl_A.L.2'=>'REMOVER',
'tr_acl_A.L.3'=>'REMOVER TODOS',
'tr_acl_A.MA'=>'MINHA CONTA',
'tr_acl_A.P'=>'PERFIS',
'tr_acl_A.P.1'=>'EDITAR',
'tr_acl_A.P.2'=>'REMOVER',
'tr_acl_A.P.3'=>'INSERIR PERFIL',
'tr_acl_A.S'=>'CONFIGURAÇÕES DO SISTEMA',
'tr_acl_A.S.1'=>'POLÍTICA DE SENHAS',
'tr_acl_A.S.2'=>'DELETE CASCADE',
'tr_acl_A.U'=>'USUÁRIOS',
'tr_acl_A.U.1'=>'EDITAR',
'tr_acl_A.U.2'=>'REMOVER',
'tr_acl_A.U.3'=>'INSERIR USUÁRIO',
'tr_acl_A.U.4'=>'RESETAR SENHAS',
'tr_acl_A.U.5'=>'BLOQUEAR USUÁRIO',
'tr_acl_M'=>'Visão do usuário',
'tr_acl_M.CI'=>'MELHORIA CONTÍNUA',
'tr_acl_M.CI.1'=>'INCIDENTES',
'tr_acl_M.CI.1.1'=>'LISTAR',
'tr_acl_M.CI.1.2'=>'INSERIR INCIDENTE',
'tr_acl_M.CI.1.3'=>'EDITAR SEM RESTRIÇÃO',
'tr_acl_M.CI.1.4'=>'REMOVER SEM RESTRIÇÃO',
'tr_acl_M.CI.2'=>'ACIDENTE',
'tr_acl_M.CI.2.1'=>'LISTAR',
'tr_acl_M.CI.2.2'=>'INSERIR ACIDENTE',
'tr_acl_M.CI.2.3'=>'EDITAR SEM RESTRIÇÃO',
'tr_acl_M.CI.2.4'=>'REMOVER SEM RESTRIÇÃO',
'tr_acl_M.CI.3'=>'NÃO CONFORMIDADE',
'tr_acl_M.CI.3.1'=>'LISTAR',
'tr_acl_M.CI.3.2'=>'INSERIR NÃO CONFORMIDADE',
'tr_acl_M.CI.3.3'=>'EDITAR SEM RESTRIÇÃO',
'tr_acl_M.CI.3.4'=>'REMOVER SEM RESTRIÇÃO',
'tr_acl_M.CI.5'=>'PROCESSO DISCIPLINAR',
'tr_acl_M.CI.5.1'=>'VER AÇÃO',
'tr_acl_M.CI.5.2'=>'REGISTRAR AÇÃO',
'tr_acl_M.CI.5.3'=>'REMOVER',
'tr_acl_M.CI.6'=>'RELATÓRIOS',
'tr_acl_M.CI.6.1'=>'RELATÓRIOS DE ACIDENTE',
'tr_acl_M.CI.6.1.1'=>'INCIDENTES POR ACIDENTE',
'tr_acl_M.CI.6.1.2'=>'PROCESSO DISCIPLINAR',
'tr_acl_M.CI.6.1.3'=>'COLETA DE EVIDÊNCIAS',
'tr_acl_M.CI.6.1.4'=>'ACOMPANHAMENTO DE INCIDENTES',
'tr_acl_M.CI.6.1.5'=>'ACOMPANHAMENTO DE ACIDENTE',
'tr_acl_M.CI.6.1.6'=>'IMPACTO FINANCEIRO POR ACIDENTE',
'tr_acl_M.CI.6.1.7'=>'REVISÃO DE CONTROLE OCASIONADA POR ACIDENTES',
'tr_acl_M.CI.6.1.8'=>'ELEMENTOS AFETADOS POR ACIDENTES',
'tr_acl_M.CI.6.1.9'=>'CÁLCULO AUTOMÁTICO DE PROBABILIDADE DOS RISCOS',
'tr_acl_M.CI.6.2'=>'RELATÓRIOS DE NÃO CONFORMIDADE',
'tr_acl_M.CI.6.2.1'=>'NÃO CONFORMIDADE POR PROCESSO',
'tr_acl_M.CI.6.2.2'=>'NÃO CONFORMIDADE DE CONTROLE POR PROCESSO',
'tr_acl_M.CI.6.2.3'=>'ACOMPANHAMENTO DE NÃO CONFORMIDADE',
'tr_acl_M.CI.6.2.4'=>'PLANO DE AÇÃO POR USUÁRIO',
'tr_acl_M.CI.6.2.5'=>'PLANO DE AÇÃO POR PROCESSO',
'tr_acl_M.CI.6.3'=>'RELATÓRIOS DE MELHORIA CONTÍNUA',
'tr_acl_M.CI.6.3.1'=>'TAREFAS PENDENTES DA MELHORIA CONTÍNUA',
'tr_acl_M.CI.6.3.2'=>'INCIDENTES SEM RISCO',
'tr_acl_M.CI.6.3.3'=>'NÃO CONFORMIDADE SEM PLANO DE AÇÃO',
'tr_acl_M.CI.6.3.4'=>'PLANO DE AÇÃO SEM DOCUMENTO',
'tr_acl_M.CI.6.3.5'=>'ACIDENTES SEM INCIDENTES',
'tr_acl_M.CI.7'=>'PLANO DE AÇÃO',
'tr_acl_M.CI.7.1'=>'LISTAR',
'tr_acl_M.CI.7.2'=>'INSERIR',
'tr_acl_M.CI.7.3'=>'EDITAR SEM RESTRIÇÃO',
'tr_acl_M.CI.7.4'=>'REMOVER SEM RESTRIÇÃO',
'tr_acl_M.D'=>'DASHBOARD',
'tr_acl_M.D.1'=>'SUMÁRIOS',
'tr_acl_M.D.2'=>'SUMÁRIO AVANÇADO',
'tr_acl_M.D.3'=>'GRÁFICOS',
'tr_acl_M.D.6'=>'ESTATÍSTICAS',
'tr_acl_M.D.6.1'=>'GRÁFICOS DE ÁREA',
'tr_acl_M.D.6.10'=>'STATUS DO SISTEMA',
'tr_acl_M.D.6.11'=>'ESTATÍSTICA DE GESTÃO DE DOCUMENTOS',
'tr_acl_M.D.6.12'=>'ESTATÍSTICA DE MELHORIA CONTÍNUA',
'tr_acl_M.D.6.13'=>'ESTATÍSTICAS DO PLANO DE AÇÃO',
'tr_acl_M.D.6.2'=>'GRÁFICOS DE PROCESSO',
'tr_acl_M.D.6.3'=>'GRÁFICOS DE A.P.S.',
'tr_acl_M.D.6.4'=>'SUMÁRIO DE RISCOS',
'tr_acl_M.D.6.5'=>'SUMÁRIO DE REQUISITOS LEGAIS',
'tr_acl_M.D.6.6'=>'SUMÁRIO DE CONTROLE',
'tr_acl_M.D.6.7'=>'ANORMALIDADES',
'tr_acl_M.D.6.8'=>'ESTATÍSTICAS FINANCEIRAS',
'tr_acl_M.D.6.9'=>'ABRANGÊNCIA EM NÍVEL DE GERENCIAMENTO',
'tr_acl_M.L'=>'BIBLIOTECAS',
'tr_acl_M.L.1'=>'BIBLIOTECA DE EVENTOS',
'tr_acl_M.L.1.1'=>'ABRIR CATEGORIA',
'tr_acl_M.L.1.2'=>'EDITAR CATEGORIA',
'tr_acl_M.L.1.3'=>'REMOVER CATEGORIA',
'tr_acl_M.L.1.4'=>'ASSOCIAR REQUISITOS LEGAIS',
'tr_acl_M.L.1.5'=>'EDITAR EVENTO',
'tr_acl_M.L.1.6'=>'REMOVER EVENTO',
'tr_acl_M.L.1.7'=>'INSERIR CATEGORIA',
'tr_acl_M.L.1.8'=>'INSERIR EVENTO',
'tr_acl_M.L.1.9'=>'ASSOCIAR EVENTO',
'tr_acl_M.L.2'=>'BIBLIOTECA DE REQUISITOS LEGAIS',
'tr_acl_M.L.2.1'=>'ABRIR SEÇÃO',
'tr_acl_M.L.2.10'=>'CRIAR MODELO DE REQUISITO LEGAL',
'tr_acl_M.L.2.2'=>'EDITAR SEÇÃO',
'tr_acl_M.L.2.3'=>'REMOVER SEÇÃO',
'tr_acl_M.L.2.4'=>'ASSOCIAR EVENTOS',
'tr_acl_M.L.2.5'=>'EDITAR REQUISITO LEGAL',
'tr_acl_M.L.2.6'=>'REMOVER REQUISITO LEGAL',
'tr_acl_M.L.2.7'=>'INSERIR SEÇÃO',
'tr_acl_M.L.2.8'=>'INSERIR REQUISITO LEGAL',
'tr_acl_M.L.2.9'=>'VER MODELOS DE DOCUMENTOS DE REQUISITO LEGAL',
'tr_acl_M.L.3'=>'BIBLIOTECA DE LEGISLAÇÕES',
'tr_acl_M.L.3.1'=>'EDITAR LEGISLAÇÕES',
'tr_acl_M.L.3.2'=>'REMOVER LEGISLAÇÕES',
'tr_acl_M.L.3.3'=>'INSERIR LEGISLAÇÃO',
'tr_acl_M.L.4'=>'IMPORTAR / EXPORTAR',
'tr_acl_M.L.4.1'=>'EXPORTAR',
'tr_acl_M.L.4.2'=>'IMPORTAR',
'tr_acl_M.L.5'=>'BIBLIOTECA DE MODELOS DE DOCUMENTO',
'tr_acl_M.L.5.1'=>'EDITAR MODELO DE DOCUMENTO',
'tr_acl_M.L.5.2'=>'REMOVER MODELO DE DOCUMENTO',
'tr_acl_M.L.5.3'=>'INSERIR MODELO DE DOCUMENTO',
'tr_acl_M.L.6'=>'BIBLIOTECA DE CATEGORIAS',
'tr_acl_M.L.6.1'=>'INSERIR CATEGORIA',
'tr_acl_M.L.6.2'=>'EDITAR CATEGORIA',
'tr_acl_M.L.6.3'=>'REMOVER CATEGORIA',
'tr_acl_M.L.6.4'=>'INSERIR SOLUÇÃO',
'tr_acl_M.L.6.5'=>'EDITAR SOLUÇÃO',
'tr_acl_M.L.6.6'=>'REMOVER SOLUÇÃO',
'tr_acl_M.PM'=>'GESTÃO DE DOCUMENTOS',
'tr_acl_M.PM.10'=>'DOCUMENTOS EM DESENVOLVIMENTO',
'tr_acl_M.PM.10.1'=>'INSERIR DOCUMENTO',
'tr_acl_M.PM.11'=>'DOCUMENTOS EM APROVAÇÃO',
'tr_acl_M.PM.12'=>'DOCUMENTOS PUBLICADOS',
'tr_acl_M.PM.13'=>'DOCUMENTOS EM REVISÃO',
'tr_acl_M.PM.14'=>'DOCUMENTOS OBSOLETOS',
'tr_acl_M.PM.15'=>'TODOS DOCUMENTOS',
'tr_acl_M.PM.16'=>'REGISTROS',
'tr_acl_M.PM.17'=>'RELATÓRIOS',
'tr_acl_M.PM.17.1'=>'OUTROS',
'tr_acl_M.PM.17.1.16'=>'PROPRIEDADES DO DOCUMENTO',
'tr_acl_M.PM.17.1.17'=>'RELATÓRIO DE CLASSIFICAÇÃO',
'tr_acl_M.PM.17.2'=>'RELATÓRIOS GERAIS',
'tr_acl_M.PM.17.2.1'=>'DOCUMENTOS',
'tr_acl_M.PM.17.2.2'=>'DOCUMENTOS POR ÁREA',
'tr_acl_M.PM.17.2.3'=>'DOCUMENTOS POR COMPONENTE DO SGSI',
'tr_acl_M.PM.17.2.4'=>'COMENTÁRIOS E JUSTIFICATIVAS DE DOCUMENTOS',
'tr_acl_M.PM.17.2.5'=>'COMENTÁRIOS DE USUÁRIOS',
'tr_acl_M.PM.17.3'=>'RELATÓRIOS DE REGISTRO',
'tr_acl_M.PM.17.3.1'=>'REGISTRO POR DOCUMENTO',
'tr_acl_M.PM.17.4'=>'RELATÓRIOS DE CONTROLE DE ACESSO',
'tr_acl_M.PM.17.4.1'=>'AUDITORIA DE ACESSO DOS USUÁRIOS',
'tr_acl_M.PM.17.4.2'=>'AUDITORIA DE ACESSO AOS DOCUMENTOS',
'tr_acl_M.PM.17.4.3'=>'DOCUMENTOS E SEUS APROVADORES',
'tr_acl_M.PM.17.4.4'=>'DOCUMENTOS E SEUS LEITORES',
'tr_acl_M.PM.17.4.5'=>'USUÁRIOS ASSOCIADOS AOS PROCESSOS',
'tr_acl_M.PM.17.5'=>'RELATÓRIOS DE GESTÃO',
'tr_acl_M.PM.17.5.1'=>'DOCUMENTOS COM LEITURA PENDENTE',
'tr_acl_M.PM.17.5.2'=>'DOCUMENTOS MAIS ACESSADOS',
'tr_acl_M.PM.17.5.3'=>'DOCUMENTOS NÃO ACESSADOS',
'tr_acl_M.PM.17.5.4'=>'DOCUMENTOS MAIS REVISADOS',
'tr_acl_M.PM.17.5.5'=>'APROVAÇÕES PENDENTES',
'tr_acl_M.PM.17.5.6'=>'DATA DE CRIAÇÃO E REVISÃO DOS DOCUMENTOS',
'tr_acl_M.PM.17.5.7'=>'CRONOGRAMA DE REVISÃO DE DOCUMENTOS',
'tr_acl_M.PM.17.5.8'=>'DOCUMENTOS COM REVISÃO ATRASADA',
'tr_acl_M.PM.17.6'=>'RELATÓRIOS DE ANORMALIDADE',
'tr_acl_M.PM.17.6.1'=>'COMPONENTES SEM DOCUMENTOS',
'tr_acl_M.PM.17.6.2'=>'TAREFAS PENDENTES',
'tr_acl_M.PM.17.6.3'=>'DOCUMENTOS COM UMA ALTA FREQÜÊNCIA DE REVISÃO',
'tr_acl_M.PM.17.6.4'=>'DOCUMENTOS SEM REGISTROS',
'tr_acl_M.PM.17.6.5'=>'USUÁRIOS COM LEITURA PENDENTE',
'tr_acl_M.PM.18'=>'DOCUMENTOS A PUBLICAR',
'tr_acl_M.RM'=>'GESTÃO DE ASPECTO E IMPACTO',
'tr_acl_M.RM.1'=>'ÁREA',
'tr_acl_M.RM.1.1'=>'LISTAR TODOS',
'tr_acl_M.RM.1.2'=>'VER SUB-ÁREA',
'tr_acl_M.RM.1.3'=>'VER PROCESSOS',
'tr_acl_M.RM.1.4'=>'EDITAR',
'tr_acl_M.RM.1.5'=>'REMOVER',
'tr_acl_M.RM.1.6'=>'INSERIR ÁREA',
'tr_acl_M.RM.1.7'=>'INSERIR ÁREA DE PRIMEIRO NÍVEL',
'tr_acl_M.RM.1.8'=>'EDITAR SE RESPONSÁVEL',
'tr_acl_M.RM.1.9'=>'REMOVER SE RESPONSÁVEL',
'tr_acl_M.RM.2'=>'PROCESSO',
'tr_acl_M.RM.2.1'=>'LISTAR TODOS',
'tr_acl_M.RM.2.2'=>'VER A.P.S.',
'tr_acl_M.RM.2.4'=>'EDITAR',
'tr_acl_M.RM.2.5'=>'REMOVER',
'tr_acl_M.RM.2.6'=>'INSERIR PROCESSO',
'tr_acl_M.RM.2.7'=>'EDITAR SE RESPONSÁVEL',
'tr_acl_M.RM.2.8'=>'REMOVER SE RESPONSÁVEL',
'tr_acl_M.RM.3'=>'A.P.S.',
'tr_acl_M.RM.3.1'=>'LISTAR TODOS',
'tr_acl_M.RM.3.10'=>'CARREGAR RISCOS DA BIBLIOTECA',
'tr_acl_M.RM.3.2'=>'VER RISCOS',
'tr_acl_M.RM.3.4'=>'NOVO RISCO',
'tr_acl_M.RM.3.5'=>'EDITAR',
'tr_acl_M.RM.3.6'=>'REMOVER',
'tr_acl_M.RM.3.7'=>'INSERIR A.P.S.',
'tr_acl_M.RM.3.8'=>'EDITAR SE RESPONSÁVEL',
'tr_acl_M.RM.3.9'=>'REMOVER SE RESPONSÁVEL',
'tr_acl_M.RM.4'=>'RISCO',
'tr_acl_M.RM.4.1'=>'LISTAR TODOS',
'tr_acl_M.RM.4.10'=>'REMOVER SE RESPONSÁVEL',
'tr_acl_M.RM.4.2'=>'VER CONTROLES',
'tr_acl_M.RM.4.4'=>'EDITAR',
'tr_acl_M.RM.4.5'=>'REMOVER',
'tr_acl_M.RM.4.6'=>'INSERIR RISCO',
'tr_acl_M.RM.4.8'=>'ESTIMAR',
'tr_acl_M.RM.4.9'=>'EDITAR SE RESPONSÁVEL',
'tr_acl_M.RM.5'=>'CONTROLE',
'tr_acl_M.RM.5.1'=>'LISTAR TODOS',
'tr_acl_M.RM.5.2'=>'VER RISCOS',
'tr_acl_M.RM.5.3'=>'ASSOCIAR RISCOS',
'tr_acl_M.RM.5.4'=>'EDITAR',
'tr_acl_M.RM.5.5'=>'REMOVER',
'tr_acl_M.RM.5.6'=>'INSERIR CONTROLE',
'tr_acl_M.RM.5.7'=>'EDITAR SE RESPONSÁVEL',
'tr_acl_M.RM.5.8'=>'REMOVER SE RESPONSÁVEL',
'tr_acl_M.RM.5.9'=>'HISTÓRICO DE REVISÃO/TESTE',
'tr_acl_M.RM.6'=>'RELATÓRIOS',
'tr_acl_M.RM.6.1'=>'DE RISCOS',
'tr_acl_M.RM.6.1.1'=>'VALORES DOS RISCOS',
'tr_acl_M.RM.6.1.2'=>'RISCOS POR PROCESSO',
'tr_acl_M.RM.6.1.3'=>'RISCOS POR A.P.S.',
'tr_acl_M.RM.6.1.4'=>'RISCOS POR CONTROLE',
'tr_acl_M.RM.6.1.5'=>'IMPACTO DO RISCO',
'tr_acl_M.RM.6.1.6'=>'IMPORTÂNCIA DOS A.P.S.',
'tr_acl_M.RM.6.1.7'=>'RISCOS POR ÁREA',
'tr_acl_M.RM.6.1.8'=>'CHECKLIST POR EVENTO',
'tr_acl_M.RM.6.2'=>'DE CONTROLES',
'tr_acl_M.RM.6.2.1'=>'CONTROLES POR RESPONSÁVEL',
'tr_acl_M.RM.6.2.10'=>'ACOMPANHAMENTO DO TESTE DOS CONTROLES',
'tr_acl_M.RM.6.2.11'=>'CONFORMIDADE',
'tr_acl_M.RM.6.2.12'=>'AGENDA DE REVISÃO DOS CONTROLES POR RESPONSÁVEL',
'tr_acl_M.RM.6.2.13'=>'AGENDA DE TESTE DOS CONTROLES POR RESPONSÁVEL',
'tr_acl_M.RM.6.2.2'=>'CONTROLES POR RISCO',
'tr_acl_M.RM.6.2.3'=>'PLANEJAMENTO DOS CONTROLES',
'tr_acl_M.RM.6.2.4'=>'DATA DE IMPLEMENTAÇÃO DOS CONTROLES',
'tr_acl_M.RM.6.2.5'=>'EFICÁCIA DOS CONTROLES',
'tr_acl_M.RM.6.2.6'=>'RESUMO DE CONTROLES',
'tr_acl_M.RM.6.2.7'=>'ACOMPANHAMENTO DE CONTROLES',
'tr_acl_M.RM.6.2.8'=>'ACOMPANHAMENTO DA EFICÁCIA DOS CONTROLES',
'tr_acl_M.RM.6.2.9'=>'CONTROLES NÃO MEDIDOS',
'tr_acl_M.RM.6.3'=>'DE GESTÃO DE ASPECTOS E IMPACTOS',
'tr_acl_M.RM.6.3.1'=>'TAREFAS PENDENTES',
'tr_acl_M.RM.6.3.2'=>'ÁREAS SEM PROCESSOS',
'tr_acl_M.RM.6.3.3'=>'A.P.S. SEM RISCO',
'tr_acl_M.RM.6.3.4'=>'PROCESSOS SEM A.P.S.',
'tr_acl_M.RM.6.3.5'=>'RISCOS NÃO ESTIMADOS',
'tr_acl_M.RM.6.3.6'=>'CONTROLES SEM RISCO',
'tr_acl_M.RM.6.3.7'=>'RISCOS DUPLICADOS',
'tr_acl_M.RM.6.4'=>'FINANCEIROS',
'tr_acl_M.RM.6.4.4'=>'CUSTO POR CONTROLE',
'tr_acl_M.RM.6.4.5'=>'CUSTO DOS CONTROLES POR RESPONSÁVEL',
'tr_acl_M.RM.6.4.6'=>'CUSTO DOS CONTROLES POR ÁREA',
'tr_acl_M.RM.6.4.7'=>'CUSTO DOS CONTROLES POR PROCESSO',
'tr_acl_M.RM.6.4.8'=>'CUSTO DOS CONTROLES POR A.P.S.',
'tr_acl_M.RM.6.5'=>'ISO 27001',
'tr_acl_M.RM.6.5.1'=>'DECLARAÇÃO DE APLICABILIDADE',
'tr_acl_M.RM.6.5.2'=>'PLANO DE TRATAMENTO DOS RISCOS',
'tr_acl_M.RM.6.5.3'=>'DECLARAÇÃO DE ESCOPO DO SGSI',
'tr_acl_M.RM.6.5.4'=>'DECLARAÇÃO DE POLÍTICA DO SGSI',
'tr_acl_M.RM.6.6'=>'SUMÁRIOS',
'tr_acl_M.RM.6.6.1'=>'TOP 10 - A.P.S. COM MAIS RISCOS',
'tr_acl_M.RM.6.6.2'=>'TOP 10 - A.P.S. COM MAIS RISCOS ALTOS',
'tr_acl_M.RM.6.6.3'=>'TOP 10 - RISCOS POR ÁREA',
'tr_acl_M.RM.6.6.4'=>'TOP 10 - RISCOS POR PROCESSO',
'tr_acl_M.RM.6.6.5'=>'QUANTIDADE DE RISCOS POR PROCESSO',
'tr_acl_M.RM.6.6.6'=>'QUANTIDADE DE RISCOS POR ÁREA',
'tr_acl_M.RM.6.6.7'=>'STATUS DE RISCO POR ÁREA',
'tr_acl_M.RM.6.6.8'=>'STATUS DE RISCO POR PROCESSO',
'tr_acl_M.RM.6.7'=>'OUTROS',
'tr_acl_M.RM.6.7.1'=>'RESPONSABILIDADES DOS USUÁRIOS',
'tr_acl_M.RM.6.7.2'=>'ELEMENTOS PENDENTES',
'tr_acl_M.RM.6.7.3'=>'A.P.S.',
'tr_acl_M.RM.6.7.4'=>'CLASSIFICAÇÃO DOS ELEMENTOS',
'tr_acl_M.RM.6.7.5'=>'A.P.S. - DEPENDÊNCIAS E DEPENDENTES',
'tr_acl_M.RM.6.7.6'=>'RELEVÂNCIA DOS A.P.S.',
'tr_acl_M.S'=>'PESQUISA',

/* './packages/admin/cfg_general.xml' */

'lb_session_expiracy'=>'Tempo de expiração da sessão:',
'st_minutes'=>'minutos.',
'to_session_expiracy'=>'<b>Tempo de expiração da sessão:</b><br/><br/>Determina quanto tempo de inatividade o sistema espera, antes de a sessão do usuário logado ser expirada.',
'vb_deactivate'=>'Desativar',

/* './packages/admin/custom_asset_values_name.xml' */

'st_asset_parametrization_cl_bl'=>'<b>Nomes de Estimativa da Importância do A.P.S.:</b>',
'st_importance'=>'Importancia (A.P.S.)',
'tt_asset_values_name_information'=>'Como você deseja nomear os parâmetros de estimativa da importância de seus A.P.S.?',

/* './packages/admin/custom_control.xml' */

'cb_revision'=>'Revisão',
'cb_test'=>'Teste',
'st_revision_and_test'=>'<b>Revisão e Teste de Controle</b>',
'st_revision_and_test_description'=>'Você deseja rever a segurança de seus controles?',
'to_control_revision_config_bl_cl'=>'<b>Configuração de Revisão do Controle:</b><br/><br/>Determina se o sistema permitirá a revisão da eficácia real dos controles.<br/>Essa configuração permitirá avaliar se um controle é eficiente.',
'to_control_test_config_bl_cl'=>'<b>Configuração de Teste do Controle:</b><br/><br/>Determina se o sistema permitirá a execução de testes nos controles.<br/>Essa configuração permitirá avaliar se um controle é confiável.',

/* './packages/admin/custom_control_cost_name.xml' */

'lb_cost_name_1_bl'=>'<b>Nome do Custo 1</b>',
'lb_cost_name_2_bl'=>'<b>Nome do Custo 2</b>',
'lb_cost_name_3_bl'=>'<b>Nome do Custo 3</b>',
'lb_cost_name_4_bl'=>'<b>Nome do Custo 4</b>',
'lb_cost_name_5_bl'=>'<b>Nome do Custo 5</b>',
'st_costs_estimation_parameters_information'=>'Quais são as categorias de custo que você normalmente considera na aplicação dos controles?',
'static_st_cost_estimation_parameters_bl'=>'<b> Parâmetros de Estimativa de Custos</b>',

/* './packages/admin/custom_currency.xml' */

'lb_currency_cl'=>'Moeda:',
'si_australian_dollar'=>'Australian dollar ($)- AUD',
'si_bolivar'=>'Bolívar venezuelano (Bs F)- VEF',
'si_brunei_dollar'=>'Dólar do Brunei ($)- BND',
'si_canadian_dollar'=>'Dólar canadense ($) - CAD',
'si_chile_pesos'=>'Peso chileno ($) - CLP',
'si_colombia_pesos'=>'Peso colombiano ($) - COP',
'si_czech_koruna'=>'Coroa Checa (Kč) - CZK',
'si_danish_krone'=>'Coroa dinamarquesa (kr)- DKK',
'si_dollar'=>'Dólar dos Estados Unidos ($) - USD',
'si_euro'=>'Euro (€) - EUR',
'si_india_rupee'=>'Rupia indiana (₨)- INR',
'si_israel_new_shekels'=>'Novo shekel de Israel (₪) - ILS',
'si_mexican_peso'=>'Peso Mexicano ($) - MXN',
'si_newzeland_dollar'=>'Dólar neozelandês ($) - NZD',
'si_pakistan_rupee'=>'Rúpia paquistanesa (₨) - PKR',
'si_pesos'=>'Peso argentino ($) - ARS',
'si_pounds'=>'Libra esterlina (£) - GBP',
'si_reais'=>'Real brasileiro (R$) - BRL',
'si_singapore_dollar'=>'Dólar de Singapura ($) - SGD',
'si_swedish_krona'=>'Coroa sueca (kr) - SEK',
'si_switzerland'=>'Franco suíço (Fr) - CHF',
'si_yen'=>'Iene Japonês (¥) - JPY',
'si_yuan'=>'Yuan chinês (¥) - CNY',
'st_system_currency_bl'=>'<b>Moeda do Sistema</b>',
'st_system_currency_information'=>'Qual moeda você quer utilizar no sistema?',
'to_general_settings_currency'=>'<b>Moeda:</b> <br/> <br/> Configura a moeda mostrada nas telas do sistema. Note que não é realizada conversão quando é alterada a moeda.',

/* './packages/admin/custom_financial_impact_parameters.xml' */

'st_custom_financial_impact_parameter_note'=>'Digite o nome das categorias sobre as quais será avaliado o impacto financeiro de um acidente.',
'st_description_financial_impact_parameter'=>'O Real EMS permite definir as fontes de possíveis perdas financeiras associadas a um risco. Quais são as categorias de fontes financeiras que você deseja utilizar?',
'st_financial_impact_parameters'=>'<b>Parâmetros de Impacto Financeiro</b>',

/* './packages/admin/custom_interface.xml' */

'si_system_interface_param_cl'=>'<b>Parâmetros de Interface:</b>',
'st_color_picker'=>'Abrir paleta de cores',
'st_darkColor'=>'Cor escura:',
'st_lightColor'=>'Cor clara:',
'st_logotype_login_cl'=>'Logotipo login: (266x84)',
'st_logotype_top_cl'=>'Logotipo topo: (133x42)',
'wn_invalid_image_type'=>'Tipo de imagem inválido.',

/* './packages/admin/custom_non_conformity_types.xml' */

'si_non_conformity_types_parametrization_cl'=>'<b>Parametrização de Tipos de Não Conformidade:</b>',
'st_description_non_conformity_types_parametrization'=>'Como você nomeia os tipos de não conformidade?',

/* './packages/admin/custom_quantity_rm_parameters.xml' */

'lb_mode_cl'=>'Modo:',
'rb_3_three'=>'3 (três)',
'rb_5_five'=>'5 (cinco)',
'rb_optimist'=>'Otimista',
'rb_pessimist'=>'Pessimista',
'st_custom_quantity_parameters_note'=>'OBS: Você poderá alterar essas configurações no futuro, porém será necessário reavaliar alguns valores de risco.',
'st_risk_quantity_values_bl_cl'=>'<b>Matriz de Definição de Risco:</b>',
'tt_custom_quantity_rm_parameters_information'=>'Requisitos legais recomendam que você atribua importância, impacto e probabilidade numa matriz 3x3.<br/> Algumas organizações preferem utilizar uma matriz 5x5.<br/> Essa decisão você deve tomar antes de iniciar a gestão de aspectos e impactos.<br/> Usando uma matriz 3x3, os riscos terão valores entre 1 e 9. Se você escolher uma matriz 5x5, os riscos terão valores entre 1 e 25.<br/><br/> Como você quer medir sua matriz de risco?',

/* './packages/admin/custom_report_classification.xml' */

'lb_to_cl'=>'Para:',
'st_report_classification'=>'<b>Classificação dos Relatórios</b>',
'st_report_classification_information'=>'Qual a Classificação (e para quem esta classificação se aplica) você deseja utilizar nas abas do sistema?',
'st_system_classification_bl'=>'<b>Classificação do Sistema</b>',
'st_system_classification_information'=>'Qual classificação (e para quem esta classificação se aplica) você deseja utilizar para o sistema?',
'to_general_settings_classification'=>'<b>Classificação:</b><br/><br/>Indica, no rodapé do sistema, a classificação do mesmo. Ex.: Confidencial, Secreto, etc.',
'to_general_settings_classification_dest'=>'<b>Para:</b><br/><br/>Indica para quem se aplica a classificação acima. Esse campo terá efeito somente quando a classificação tiver sido preenchida.',

/* './packages/admin/custom_risk_control_values_name.xml' */

'st_rc_impact'=>'Impacto',
'st_rc_impact5_'=>'Impacto',
'st_rc_probability'=>'Probabilidade',
'st_risk_control_values_name'=>'<b>Nomes dos critérios para redução por controles</b>',
'st_very_high'=>'<b>Muito Alto</b>',
'st_very_low'=>'<b>Muito Baixo</b>',
'tt_risk_control_value_name_information'=>'Quando você aplica controles para reduzir o risco, eles reduzem a probabilidade ou o impacto relacionado a estes riscos. Como você deseja nomear essas reduções de impacto e probabilidade?',

/* './packages/admin/custom_risk_limits.xml' */

'lb_risk_limits_high_bl_cl'=>'<b>Tolerância ao Risco 2:</b>',
'lb_risk_limits_low_bl_cl'=>'<b>Tolerância ao Risco 1:</b>',
'st_risk_limits_definition'=>'Definir os valores para enquadramento dos riscos.<br/><br/> Riscos cujo valor calculado for inferior à \'Tolerância ao Risco 1\' serão sinalizados com a cor verde.<br/>Riscos cujo valor calculado seja superior à \'Tolerância ao Risco 2\' serão sinalizados com a cor vermelha.<br/>Riscos cujo valor calculado situa-se na faixa intermediária serão sinalizados com a cor amarela.',
'tt_aceptable_risk_limits'=>'<b>Nível de aceitação de Risco</b>',
'tt_risk_limits'=>'Quais são os níveis aceitáveis de risco?',
'wn_high_higher_than_max'=>'O valor do limite de risco 2 não pode exceder o limite da configuração do sistema.',
'wn_low_higher_than_high'=>'O valor do limite de risco 1 não pode ser maior que o valor do limite de risco 2.',
'wn_low_lower_than_1'=>'O valor do limite de risco 1 não pode ser menor que 1.',
'wn_save_risk_matrix_update_risk_limits'=>'O sistema gerou uma tarefa para o usuário <b>%chairman_name%</b> aprovar os novos limites de risco. Até a aprovação dos novos limites, o sistema utilizará os limites de risco antigos.',

/* './packages/admin/custom_risk_parameters_names.xml' */

'st_risk_parameters_bl_cl'=>'<b>Parâmetros de estimativa de Risco:</b>',
'st_risk_parameters_help'=>'Digite os nomes das propriedades que serão disponibilizadas no software para a avaliação de risco. Sugerimos as seguintes propriedades: confidencialidade, integridade e disponibilidade; e ainda outras propriedades tais como autenticidade, responsabilidade, não-repúdio e confiabilidade. Ou como preferir.',
'tt_risk_parameters_information'=>'Como você nomeia sua Estimativa de parâmetros de risco?',
'wn_risk_parameters_minimum_requirement'=>'Pelo menos um Parâmetro de Risco deve ser especificado.',

/* './packages/admin/custom_risk_parameters_weight.xml' */

'st_risk_parameters_weight'=>'<b>Peso dos Parâmetros de Risco</b>',
'st_risk_parameters_weight_definition'=>'Definir os pesos para cada parâmetro de risco. Os pesos devem ser maiores ou iguais a zero, e no mínimo um peso deve ser positivo. Os valores dos riscos e A.P.S. serão atualizados de acordo com as mudanças nos pesos atuais.',
'wn_at_least_one_positive'=>'Pelo menos um peso deve ter valor maior que zero.',

/* './packages/admin/custom_risk_values_name.xml' */

'st_custom_risk_matrix_definition_generic_note'=>'OBS: primeiro selecione o tamanho da matriz de risco (3x3 ou 5x5) no tópico "Matriz de definição de risco".',
'st_high'=>'<b>Alto</b>',
'st_high_bl'=>'<b>Alto</b>',
'st_impact'=>'Impacto (Risco)',
'st_impact_risk'=>'Impacto (Risco)',
'st_low'=>'<b>Baixo</b>',
'st_low_bl'=>'<b>Baixo</b>',
'st_medium'=>'<b>Médio</b>',
'st_medium_bl'=>'<b>Médio</b>',
'st_probability_risk'=>'Probabilidade (Risco)',
'st_risk_parametrization'=>'<b>Nomes da Estimativa de Impacto e Probabilidade:</b>',
'st_riskprob'=>'Probabilidade (Risco)',
'st_values'=>'Valores',
'st_very_high_bl'=>'<b>Muito Alto</b>',
'st_very_low_bl'=>'<b>Muito Baixo</b>',
'tt_risk_values_name_information'=>'Como você deseja nomear os critérios de estimativa de Impacto e Probabilidade?',

/* './packages/admin/custom_special_users.xml' */

'lb_asset_manager_bl'=>'<b>Gestor de A.P.S.</b>',
'lb_chairman_bl'=>'<b>Direção</b>',
'lb_control_manager_bl'=>'<b>Gestor de Controle</b>',
'lb_library_manager_bl'=>'<b>Gestor de Biblioteca</b>',
'lb_user_disciplinary_process_manager_bl'=>'<b>Gestor de Processo Disciplinar</b>',
'lb_user_evidence_manager_bl'=>'<b>Gestor de Evidência</b>',
'lb_user_incident_manager_bl'=>'<b>Gestor de Acidente</b>',
'lb_user_document_auditor_bl'=>'<b>Auditor de Documentos</b>',
'lb_user_non_conformity_manager_bl'=>'<b>Gestor de Não Conformidade</b>',
'st_custom_special_users_information'=>'O Workflow do Real EMS requer a definição de alguns usuários responsáveis por tarefas especiais pré-determinadas no contexto do EMS. Quem irá realizar o papel desses usuários em seu sistema?',
'st_help'=>'[?]',
'st_special_users_bl'=>'<b>Usuários Especiais do Real EMS</b>',
'tt_special_user'=>'<b>Direção:</b><br/><br/>usuário responsável pela aprovação das primeiras áreas de negócio, da política de segurança e do escopo, pela aceitação de riscos, e pelos demais aspectos que requerem aprovação da "Direção"',
'tt_special_user_asset_manager'=>'<b>Gerenciador de A.P.S.:</b><br/><br/>usuário responsável pelo controle do inventário de A.P.S.',
'tt_special_user_control_manager'=>'<b>Gerenciador de Controle:</b><br/><br/>usuário responsável por gerenciar os controles de segurança, aprovar a criação de novos controles, e organizar os controles no Real EMS',
'tt_special_user_disciplinary_process_manager'=>'<b>Gestor de Processo Disciplinar:</b><br/><br/>usuário que recebe os alertas de processo disciplinar',
'tt_special_user_evidence_manager'=>'<b>Gestor de Evidência:</b><br/><br/>usuário que recebe os alertas de coleta de evidência',
'tt_special_user_incident_manager'=>'<b>Gestor de Acidente:</b><br/><br/>usuário responsável por aprovar, editar e remover incidentes. E responsável por criar, editar e remover acidentes.<br/><br/>Somente esse usuário pode criar processos disciplinares, assim como associar riscos ou processos ao acidente.',
'tt_special_user_document_auditor'=>'<b>Auditor de Documentos:</b><br/><br/>usuário que pode ler todos os documentos do sistema.',
'tt_special_user_library_manager'=>'<b>Gerenciador de Biblioteca:</b><br/><br/>usuário responsável por controlar as bibliotecas do Real EMS, e verificar novos eventos, novos requisitos legais e legislações de segurança',
'tt_special_user_nc_manager'=>'<b>Gestor de Não Conformidade:</b><br/><br/>usuário responsável por aprovar as não-conformidades criadas por outros usuários do Real EMS',

/* './packages/admin/custom_system_features.xml' */

'cb_auto_document_creation'=>'Gerar os documentos automaticamente após a criação de elementos no sistema?',
'cb_cost'=>'Utilizar os Recursos de Estimativa de Custos?',
'cb_document_manual_versioning'=>'Utilizar Versionamento Manual de Documentos?',
'cb_percentual_risk'=>'Mostrar os Valores de Risco em escala de 0 a 100?',
'cb_solicitor'=>'Permitir Procurador compartilhar direitos de acesso, privilégios e responsabilidades?',
'st_data_collecting'=>'Habilitar a Coleta de Dados com periodicidade diária para gerar as estatísticas do Real EMS.',
'st_manual_data_control'=>'Utilizar Controle Manual de Datas?',
'st_risk_formula'=>'Utilizar Cálculo de Risco Proporcional.',
'st_system_features'=>'<b>Características do Sistema</b>',
'to_data_collecting_settings'=>'<b>Configuração de Coleta de Dados:</b><br/><br/>Determina se o sistema capturará informações do banco para gerar o histórico exibido na aba de estatísticas.',
'to_delete_cascade'=>'<b>Controle Manual de Datas do Sistema:</b><br/><br/>Permite ao usuário definir datas no passado e / ou no futuro nos locais em que o sistema, por padrão, não permite esse tipo de flexibilidade.',
'to_document_manual_versioning'=>'<b>Versionamento Manual de Documentos:</b><br/><br/>Determina se o sistema utilizará versionamento manual ou automático dos documentos.',
'to_general_settings_automatic_documents_creation'=>'<b>Configuração de Criação Automática de Documentos:</b><br/><br/>Determina se o sistema criará automaticamente um documento para cada um dos elementos (área, processo, A.P.S. e controle) criados pelo usuário.',
'to_percentual_risk_configuration'=>'<b>Configuração da escala dos valores de Risco:</b><br/><br/>Determina se o valor dos riscos deve ser exibido em valor absoluto (0 a 9 ou 0 a 25) ou em escala de 0 a 100.',
'to_procurator_configuration'=>'<b>Configuração de Procurador:</b><br/><br/>Determina se o sistema permitirá a passagem de responsabilidades de um usuário para outro usuário no caso da ausência temporaria deste usuário.',
'to_risk_formula'=>'<b>Cálculo de Risco Proporcional:</b><br/><br/>Fórmula para o cálculo de risco que considera os pesos de relevância e conseqüência para potencializar os valores finais de cada risco.',
'to_system_cost_configuration'=>'<b>Configuração de Custo do Sistema:</b><br/><br/>Determina se o sistema permitirá o calculo de custos nos módulos do sistema.',

/* './packages/admin/custom_type_and_priority_parametrization.xml' */

'si_document'=>'Documento',
'si_event'=>'Evento',
'si_risk'=>'Risco',
'st_description_type_and_priority_parametrization'=>'Como você nomeia seus elementos?',
'st_element'=>'<b>Elemento:</b>',
'st_priority_bl'=>'<b>Prioridade</b>',
'st_type_and_priority_parametrization'=>'<b>Parametrização de Tipo e Prioridade</b>',
'st_type_bl'=>'<b>Tipo</b>',
'wn_classification_minimum_requirement'=>'Pelo menos três Classificações devem ser especificadas para cada item.',

/* './packages/admin/nav_audit_alert.xml' */

'gc_justification'=>'Justificativa',
'tt_audit_alerts_bl'=>'<b>Auditoria de Alertas</b>',

/* './packages/admin/nav_audit_log.xml' */

'lb_user_cl'=>'Usuário:',
'si_all_actions'=>'Todas Ações',
'si_all_users'=>'Todos Usuários',
'tt_audit_log_bl'=>'<b>Log de Auditoria</b>',

/* './packages/admin/nav_audit_task.xml' */

'gc_accomplishment'=>'Realização',
'gc_creation'=>'Criação',
'gc_receiver'=>'Receptor',
'lb_activity_cl'=>'Atividade:',
'lb_creator_cl'=>'Criador:',
'lb_receiver_cl'=>'Receptor:',
'si_all_activities'=>'Todas Atividades',
'si_all_creators'=>'Todos Criadores',
'si_all_receivers'=>'Todos Receptores',
'tt_audit_task_bl'=>'<b>Auditoria de Tarefas</b>',

/* './packages/admin/nav_config.php' */

'st_check_for_correct_data'=>'Verifique se os dados foram preenchidos corretamente.',
'st_impact_parameter_remove_message'=>'A remoção de um parâmetro de impacto financeiro irá ocasionar a remoção deste parâmetro nos acidentes já estimados. Deseja prosseguir assim mesmo?',
'st_info_saved_but'=>'As informações foram salvas. Porém,',
'st_not_possible_to_update_data_collection_info'=>'não foi possível atualizar as informações referentes à coleta de dados',
'st_not_possible_to_update_mail_server_info'=>'não foi possivel atualizar as informações referentes ao servidor de email',
'st_unavailable_information'=>'informação não disponível.',
'tt_alert'=>'Alerta',
'tt_deactivate_system'=>'Desativar Sistema',
'tt_impact_parameter_remove'=>'Remover Parâmetro de Impacto Financeiro',

/* './packages/admin/nav_config.xml' */

'si_costs_names'=>'Nomes dos Custos',
'si_parametrization'=>'Estimativa',
'si_risk_parameters_values'=>'Valores dos Parâmetros de Risco',
'si_syslog'=>'Syslog',
'si_system'=>'Sistema',

/* './packages/admin/nav_customization.php' */

'st_change_amount_of_parameters_confirm'=>'Alterar a quantidade de parâmetros resultará na desparametrização do sistema.<BR><b>Você tem certeza que deseja fazer isso?</b>',
'st_change_amount_of_risks_confirm'=>'Alterar a quantidade de riscos do sistema resultará na necessidade de refazer todos os cálculos de risco no sistema!<BR><b>Você tem certeza que deseja fazer isso?</b>',
'st_remove_classification_error'=>'Suas alterações foram salvas. No entanto, não foi possível remover as seguintes classificações, pois estão sendo usadas por um ou mais elementos: <b>%not_deletable%</b>.',
'st_risk_parameters_weight_edit_confirm'=>'Você está alterando dados sensíveis. Ao alterar o peso dos parâmetros os valores de todos os riscos e A.P.S. do sistema serão alterados de acordo. Deseja confirmar as alterações?',
'tt_remove_classification_error'=>'Erro ao remover Classificação',
'tt_save_parameters_names'=>'Salvar Nomes de Parâmetros',
'tt_save_parameters_values'=>'Salvar Valores de Parâmetros',
'tt_sensitive_data'=>'Dados Sensíveis',

/* './packages/admin/nav_customization.xml' */

'lb_settings_cl'=>'Customizações do sistema:',
'si_aceptable_risk_limits'=>'Nível de aceitação de Risco',
'si_control_cost_names'=>'Parâmetros de Estimativa de Custos',
'si_control_revision_and_test'=>'Revisão e Teste de Controle',
'si_financial_impact_parameters'=>'Parâmetros de impacto financeiro',
'si_impact_and_importance_dimensions'=>'Parâmetros de estimativa de Risco',
'si_non_conformity_types_parametrization'=>'Parametrização de Tipos de Não Conformidade',
'si_parametrization_asset'=>'Nomes de Estimativa da Importância do A.P.S.',
'si_parametrization_control'=>'Nomes dos critérios para redução por controles',
'si_parametrization_risk'=>'Nomes da Estimativa de Impacto e Probabilidade',
'si_priority_and_type_parametrization'=>'Parametrização de Tipo e Prioridade',
'si_report_classification'=>'Classificação dos Relatórios',
'si_risk_parameters_weight'=>'Peso dos Parâmetros de Risco',
'si_special_users'=>'Usuários especiais',
'si_system_classification'=>'Classificação do Sistema',
'si_system_currency'=>'Moeda',
'si_system_features'=>'Características do Sistema',
'si_system_interface_param'=>'Parâmetros de Interface',
'si_system_rm_parameters_quantity'=>'Matriz de Definição de Risco',
'wn_changes_successfully_after_login'=>'Alterações gravadas e disponíveis no próximo login do sistema.',
'wn_updating_risk_values'=>'Alterações gravadas com sucesso. Aguarde enquanto os valores dos riscos estão sendo recalculados.',
'wn_values_successfully_changed'=>'Valores alterados com sucesso.',

/* './packages/admin/nav_my_account.php' */

'st_activation_date'=>'Data de Ativação',
'st_cancel_service'=>'Cancelamento de Serviço',
'st_cancel_service_email_message'=>'Foi feita uma solicitação de cancelamento de serviço pelo cliente \'%client%\'.',
'st_client_name'=>'Nome do Cliente',
'st_confirm_cancel_service_message'=>'Tem certeza que deseja cancelar o serviço?',
'st_deactivate_account_text'=>'Para desativar sua conta, clique no botão abaixo.<br/>Seu banco de dados será mantido por 30 dias. Depois disso, todos os dados serão deletados.Para receber o banco, contate-nos em %email%',
'st_expiracy_date'=>'Data de Expiração',
'st_expiracy_date_never'=>'Nunca',
'st_license_type'=>'Tipo de Licença',
'st_max_simult_users'=>'Máx. Usuários Simultâneos',
'st_number_of_users'=>'Número de Usuários',
'tt_cancel_service'=>'Cancelamento de Serviço',
'tt_confirm_cancel_service'=>'Cancelamento de Serviço',
'tt_deactivate_account'=>'Desativar Conta',

/* './packages/admin/nav_my_account.xml' */

'lb_email_cl'=>'E-mail:',
'st_cancel_service_bl'=>'Cancelamento de Serviço',
'st_change_email_bl'=>'<b>Alteração do email Administrativo</b>',
'st_deactivate_account_bl'=>'<b>Desativar Conta</b>',
'st_license_info_bl'=>'<b>Informações da Licença:</b>',
'st_upgrade_license_bl'=>'<b>Upgrade de Licença</b>',
'st_upgrade_license_message'=>'Por favor, ative os seus user packs usando os códigos de ativação que foram enviados por e-mail. Cole os códigos de ativação abaixo e clique em "Ativar". A ativação terá efeito imediato.',
'st_upgrade_license_text'=>'Por favor, ative o seu User Pack (para 10 usuários) usando o código de ativação que foi enviado por e-mail. Cole o código de ativação abaixo e clique em "Ativar". A ativação terá efeito instantaneamente.',
'vb_activate'=>'Ativar',
'vb_cancel_service'=>'Cancelar Serviço',
'vb_change'=>'Alterar',
'vb_deactivate_account'=>'Desativar Minha Conta',
'vb_upgrade'=>'Upgrade',
'wn_email_changed'=>'email alterado com sucesso',
'wn_invalid_activation_code'=>'Alguns códigos são inválidos. Por favor, contate %email%.',
'wn_successful_activation'=>'Licença atualizada com sucesso.',

/* './packages/admin/nav_profile.php' */

'st_profile_remove_message'=>'Você tem certeza que deseja remover o perfil <b>%profile_name%</b>?',
'tt_remove_profile'=>'Remover Perfil',

/* './packages/admin/nav_profile.xml' */

'mi_visualize'=>'Visualizar',
'tt_profiles_bl'=>'<b>Perfis</b>',

/* './packages/admin/nav_system_configuration.php' */


/* './packages/admin/nav_system_configuration_on_site.xml' */

'lb_system_settings_cl'=>'Configurações do sistema:',
'si_delete_cascade'=>'Delete Cascade',
'si_email_setup'=>'Configuração de E-mail',
'si_password_policy'=>'Política de Senhas',
'si_server_setup'=>'Manutenção do servidor',
'si_timezone_setup'=>'Selecionar Fuso-horário',
'wn_changes_successfully_saved'=>'Alterações gravadas com sucesso.',

/* './packages/admin/nav_trash.php' */

'st_all_items_remove_confirm'=>'Todos os elementos da lixeira serão excluídos permanentemente! Deseja continuar?',
'st_items_restore_confirm'=>'Deseja restaurar os elementos selecionados?',
'st_not_excluded_users_and_profile_message'=>'Os usuários selecionados não puderam ser removidos, pois estão relacionados a itens existentes na lixeira.<br/> Os perfis selecionados não puderam ser removidos, pois estão relacionados a usuários existentes na lixeira que não foram selecionados para remoção ou que não puderam ser excluídos.',
'st_not_removed_profiles_message'=>'Os perfis selecionados não puderam ser removidos, pois estão relacionados a usuários existentes na lixeira que não foram selecionados ou que não puderam ser removidos.',
'st_not_restored_documents_message'=>'Documentos selecionados não puderam ser restaurados, pois estão relacionados a um ou mais elementos que permanecem removidos!',
'st_not_restored_items_message'=>'Elementos selecionados não puderam ser restaurados, pois estão relacionados a um ou mais usuários que permanecem removidos!',
'st_not_restored_users_message'=>'Os Usuários selecionados não puderam ser removidos, pois estão relacionados a itens existentes na lixeira.',
'st_not_selected_items_remove_confirm'=>'Elementos não selecionados que forem sub-elementos ou estiverem associados a elementos selecionados serão removidos. Deseja continuar?',
'tt_all_items_permanent_removal'=>'Remoção permanente de todos os elementos',
'tt_document_not_restored'=>'Documentos não restaurados',
'tt_items_not_restored'=>'Elementos não restaurados',
'tt_items_permanent_removal'=>'Remoção permanente de elementos',
'tt_items_restoral'=>'Restauração de elementos',
'tt_profile_not_deleted'=>'Perfis não removidos',
'tt_users_and_profile_not_deleted'=>'Usuários e perfis não removidos',
'tt_users_not_deleted'=>'Usuários não removidos',

/* './packages/admin/nav_trash.xml' */

'tt_bin_bl'=>'<b>Lixeira</b>',
'vb_remove_all'=>'Remover Todos',
'wn_elements_removed_successfully'=>'Elemento(s) removidos(s) com sucesso.',
'wn_elements_restaured_successfully'=>'Elemento(s) restaurado(s) com sucesso.',

/* './packages/admin/nav_users.php' */

'em_alerts_and_tasks_transfer'=>'Transferência de Tarefas e Alertas',
'st_choose_user_message'=>'Na tela a seguir, escolha o usuário que receberá as tarefas e os avisos pendentes deste usuário.',
'st_reset_all_users_passwords_confirm'=>'Você tem certeza que deseja resetar a senha de <b>todos os usuários</b> e enviar a nova senha para seus e-mails?',
'st_user_remove_confirm'=>'Você tem certeza de que deseja remover o usuário <b>%user_name%</b>?',
'tt_choose_user_warning'=>'Aviso de Escolha de Usuário',
'tt_remove_user'=>'Remover Usuário',
'tt_reset_all_users_password'=>'Resetar senha de todos os usuários',

/* './packages/admin/nav_users.xml' */

'gc_email'=>'Email',
'gc_profile'=>'Perfil',
'mi_block'=>'Bloquear',
'mi_unblock'=>'Desbloquear',
'tt_users_bl'=>'<b>Usuários</b>',
'vb_reset_passwords'=>'Resetar Senhas',

/* './packages/admin/popup_associate_elements.xml' */

'lb_associate_to_cl'=>'Associar a:',
'tt_items_association'=>'Associação de elementos',
'vb_cancel_all'=>'Cancelar Todos',

/* './packages/admin/popup_configure_authentication.php' */

'st_connection_failed'=>'Conexão falhou.',
'st_database_structure_wrong'=>'Base de dados não possui a estrutura esperada.',
'st_successful_connection'=>'Conexão feita com sucesso.',

/* './packages/admin/popup_configure_authentication.xml' */

'lb_databse_bl_cl'=>'Base de dados:',
'lb_host_bl_cl'=>'Host:',
'lb_password_bl_cl'=>'Senha:',
'st_postgres_authentication_data_needed'=>'Os dados abaixo são necessários para que o sistema possa acessar a base de dados onde estão as informações referentes ao schedule do postgres.',
'tt_setup_authentication'=>'Configurar Autenticação',
'vb_test'=>'Testar',

/* './packages/admin/popup_deactivate.php' */

'em_deactivate_account'=>'Desativação de Conta',
'em_deactivate_account_email_message'=>'Foi feita uma solicitação de desativação de conta pelo cliente \'%client%\'.<br><b>Motivo:</b><br>%deactivation_reason%',

/* './packages/admin/popup_deactivate.xml' */

'lb_deactivation_reason_cl'=>'Por favor, nos informe a razão pela qual você está desativando:',
'st_confirm_deactivate_account_message'=>'Tem certeza que deseja desativar sua conta?',
'tt_confirm_deactivate_account'=>'Desativar Conta',

/* './packages/admin/popup_no_access.xml' */

'st_user_wo_system_access'=>'Você não tem mais acesso ao sistema. Pressione <b>Ok</b> ou feche esta janela para sair.',
'tt_user_wo_system_access'=>'Usuário sem permissão para acessar o sistema',

/* './packages/admin/popup_profile_edit.php' */

'tt_edit_profile'=>'Editar Perfil',
'tt_insert_profile'=>'Inserir Perfil',
'tt_visualizae_profile'=>'Visualizar Perfil',

/* './packages/admin/popup_profile_edit.xml' */

'st_profile_warning'=>'* Esse perfil não pode ser editado.',

/* './packages/admin/popup_restore_name_conflict.xml' */

'st_name_conflicts_message'=>'Foi detectado que um ou mais elementos selecionados apresentaram conflito de nome, ou seja, existe um elemento ativo no sistema que possui o mesmo tipo e o mesmo nome. Quais elementos deseja restaurar?',
'tt_name_conflicts_found'=>'Conflitos de nome encontrados',

/* './packages/admin/popup_restore_structure_and_name_conflict.xml' */

'rb_all_except_name_conflict'=>'Todos exceto os que apresentaram conflito de nome *',
'rb_all_except_structure_conflict'=>'Todos exceto os que apresentaram conflito de estrutura',
'st_structure_name_conflicts_message'=>'Foi detectado que um ou mais elementos selecionados apresentaram conflito de estrutura e nome. Um conflito de estrutura ocorre quando elementos não selecionados são necessários para efetuar a restauração de elementos selecionados. Um conflito de nome ocorre quando existe um elemento A.P.S. no sistema do mesmo tipo e com o mesmo nome de um elemento que será restaurado. Quais elementos deseja restaurar?',
'tt_structure_and_name_conflicts_found'=>'Conflitos de estrutura e de nome encontrados',
'wn_not_restoring_name_conflict'=>'* Não restaurar elementos com conflito de nome pode ocasionar conflitos de estrutura',

/* './packages/admin/popup_restore_structure_conflict.xml' */

'cb_associate_structure_conflicted_items_individually'=>'Associar elementos com conflitos de estrutura a elementos existentes no sistema individualmente.',
'rb_all'=>'Todos',
'rb_only_not_conflicting'=>'Apenas os que não apresentaram conflito *',
'st_structure_conflicts_message'=>'Foi detectado que um ou mais elementos selecionados apresentaram conflito de estrutura, ou seja, para restaurá-los, é necessário restaurar uma estrutura de elementos não selecionados. Quais elementos deseja restaurar?',
'tt_structure_conflicts_found'=>'Conflitos de estrutura encontrados',
'vb_restore'=>'Restaurar',

/* './packages/admin/popup_upgrade_license.xml' */

'lb_activation_code_cl'=>'Código de ativação:',
'tt_upgrade_license'=>'Upgrade de Licença',

/* './packages/admin/popup_user_edit.php' */

'em_new_password'=>'Nova Senha',
'em_new_user'=>'Bem-vindo ao Real EMS',
'em_new_user_footer_bl'=>'<b>Para maiores informações entre em contato com o security officer.</b>',
'lb_password_cl'=>'Senha:',
'st_reset_user_password_confirm'=>'Você tem certeza que deseja resetar a senha do usuário e enviar a nova senha para seu e-mail?',
'tt_profile_edit_error'=>'Erro ao modificar o perfil',
'tt_reset_user_password'=>'Resetar senha de usuário',
'tt_user_adding'=>'Adição de Usuário',
'tt_user_editing'=>'Edição de Usuário',
'tt_user_insert_error'=>'Limite de usuários alcançado',
'wn_users_limit_reached'=>'O número de usuários licenciados foi alcançado. A partir de agora somente usuários com o perfil <b>Leitor de Documentos</b> (usuários que possuem somente permissão de leitura de documentos) poderão ser utilizados. Adquira mais licenças de usuários clicando <a target="_blank" href=%link%>aqui</a>.',

/* './packages/admin/popup_user_edit.xml' */

'cb_change_password_on_login'=>'Trocar a senha no próximo login',
'lb_confirmation_cl'=>'Confirmação:',
'lb_email_bl_cl'=>'<b>E-mail:</b>',
'lb_language_bl_cl'=>'Idioma:',
'lb_login_bl_cl'=>'<b>Login:</b>',
'lb_new_password_cl'=>'Nova Senha:',
'lb_profile_bl_cl'=>'<b>Perfil:</b>',
'st_language_bl_cl'=>'<b>Idioma:</b>',
'vb_reset_password'=>'Resetar Senha',
'wn_different_passwords'=>'Senhas diferentes',
'wn_existing_email'=>'Email já existe',
'wn_existing_login'=>'Login já existe',
'wn_existing_trash_email'=>'Já existe um usuário na lixeira com mesmo e-mail. Para utilizar estes dados você deverá excluir ou restaurar o usuário que encontra-se na lixeira.',
'wn_existing_trash_login'=>'Já existe um usuário na lixeira com mesmo login. Para utilizar estes dados você deverá excluir ou restaurar o usuário que encontra-se na lixeira.',
'wn_invalid_email'=>'Email inválido',
'wn_password_changed'=>'Senha alterada',
'wn_weak_password'=>'A sua senha não cumpre os requisitos da política de senhas. Por favor, escolha outra.',
'wn_wrong_current_password'=>'Senha incorreta',

/* './packages/admin/report/popup_report_audit_alert.php' */

'rs_alerts_audit'=>'Auditoria de Alertas',

/* './packages/admin/report/popup_report_audit_log.php' */

'rs_systems_log_audit'=>'Auditoria do Log do Sistema',

/* './packages/admin/report/popup_report_audit_task.php' */

'rs_tasks_audit'=>'Auditoria de Tarefas',

/* './packages/admin/report/popup_report_audit_task.xml' */

'rs_accomplishment'=>'Realização',
'rs_creation'=>'Criação',
'rs_creator'=>'Criador',
'rs_receiver'=>'Receptor',

/* './packages/admin/system_configuration_delete_cascade.xml' */

'cb_delete_cascade'=>'Habilitar o Delete Cascade',
'st_delete_cascade'=>'<b>Delete Cascade</b>',
'st_system_configuration_delete_cascade_information'=>'Determina se ao remover um elemento do sistema, também serão removido seus sub-elementos.<br/>Caso o \'Delete Cascade\' estiver desabilitado, não será permitida a remoção de elementos que contenham sub-elementos.<br/><br/>Você deseja usar o \'Delete Cascade\' no sistema?',

/* './packages/admin/system_configuration_email_setup.xml' */

'lb_email_encryption_cl'=>'Tipo de criptografia:',
'lb_email_host_cl'=>'Servidor SMTP:',
'lb_email_password_cl'=>'Senha:',
'lb_email_port_cl'=>'Porta:',
'lb_email_user_cl'=>'Usuário:',
'si_email_none'=>'Nenhuma',
'si_email_ssl'=>'SSL',
'si_email_ssl2'=>'SSLv2',
'si_email_ssl3'=>'SSLv3',
'si_email_tls'=>'TLS',
'st_configuration_email_config_information'=>'Configure os dados do servidor de e-mail que será usado pelo sistema.',
'st_email_config_bl_cl'=>'<b>Configurações de e-mail:</b>',

/* './packages/admin/system_configuration_password_policy.xml' */

'cb_case_characters'=>'Caracteres Maiúsc./Minúsc.',
'cb_numeric_characters_cl'=>'Caracteres Numéricos:',
'cb_special_characters_cl'=>'Caracteres Especiais:',
'lb_days_before_warning'=>'Avisar que a senha irá expirar:',
'lb_label_block_limit_cl'=>'Limite de bloqueio:',
'lb_minimum_characters_cl'=>'Número mínimo de Caracteres:',
'lb_password_history_apply_cl'=>'Aplicar histórico de senhas:',
'lb_password_max_lifetime_cl'=>'Tempo de vida máximo da senha:',
'st_configuration_password_policy_information'=>'Qual a política de senhas que você deseja usar no sistema?',
'st_days_before'=>'dias antes.',
'st_default_configuration'=>'Restaurar configuração padrão',
'st_invalid_logon'=>'tentativas de logon inválidas',
'st_memorized_passwords'=>'senhas memorizadas.',
'st_password_policy_bl_cl'=>'<b>Política de Senhas:</b>',
'wn_min_char_warning'=>'O número mínimo de caracteres deve ser maior ou igual ao número de caracteres especiais, mais 2 (pelo menos uma letra minúscula e uma letra maiúscula), mais o número de caracteres numéricos.<br/><br/>Obs: Somente são contabilizadas as opções marcadas.',

/* './packages/admin/system_configuration_server_setup.xml' */

'lb_server_dns_cl'=>'Servidor DNS:',
'lb_server_gateway_cl'=>'Gateway do servidor:',
'lb_server_ip_cl'=>'IP do servidor:',
'lb_server_update_cl'=>'Arquivo de atualização:',
'st_configuration_server_config_information'=>'Configuração dos parâmetros de rede do servidor.',
'st_server_config_bl_cl'=>'<b>Manutenção de servidor:</b>',

/* './packages/admin/system_configuration_timezone_setup.xml' */

'cb_timezone_international_time'=>'Formato de hora:',
'cb_timezone_international_time_12_hour'=>'12:00 PM',
'cb_timezone_international_time_24_hour'=>'23:59',
'cb_timezone_international_time_lang_default'=>'Padrão Língua',
'cb_timezone_setup'=>'Escolher o fuso:',
'si_timezone_-1'=>'GMT -1',
'si_timezone_-10'=>'GMT -10',
'si_timezone_-11'=>'GMT -11',
'si_timezone_-12'=>'GMT -12',
'si_timezone_-2'=>'GMT -2',
'si_timezone_-3'=>'GMT -3',
'si_timezone_-4'=>'GMT -4',
'si_timezone_-5'=>'GMT -5',
'si_timezone_-6'=>'GMT -6',
'si_timezone_-7'=>'GMT -7',
'si_timezone_-8'=>'GMT -8',
'si_timezone_-9'=>'GMT -9',
'si_timezone_+1'=>'GMT +1',
'si_timezone_+10'=>'GMT +10',
'si_timezone_+11'=>'GMT +11',
'si_timezone_+12'=>'GMT +12',
'si_timezone_+2'=>'GMT +2',
'si_timezone_+3'=>'GMT +3',
'si_timezone_+4'=>'GMT +4',
'si_timezone_+5'=>'GMT +5',
'si_timezone_+6'=>'GMT +6',
'si_timezone_+7'=>'GMT +7',
'si_timezone_+8'=>'GMT +8',
'si_timezone_+9'=>'GMT +9',
'si_timezone_0'=>'GMT',
'st_system_configuration_timezone_setup_information'=>'Determina qual o fuso-horário que será usado no sistema.',
'st_timezone_setup'=>'<b>Selecionar Fuso-horário</b>',

/* './packages/admin/tab_audit.xml' */

'ti_alerts'=>'Alertas',
'ti_log'=>'Log',
'ti_tasks'=>'Tarefas',

/* './packages/admin/tab_main.php' */

'st_survey_link'=>'http://www.surveymonkey.com/s.aspx?sm=Iv5Lu2xahyHtsNrd2ShHBA_3d_3d',
'to_ip_cl'=>'IP:',
'to_last_login_cl'=>'Último Login:',

/* './packages/admin/tab_main.xml' */

'lb_feedback'=>'Feedback',
'st_about_isms_bl'=>'<b>Sobre</b>',
'st_buy_now_bl'=>'<b>Compre Agora</b>',
'st_pipe_bl'=>'<b>|</b>',
'st_preferences_bl'=>'<b>Preferências</b>',
'st_satisfaction_survey_bl'=>'<b>Você está satisfeito?</b>',
'st_send_us_feedback_cl'=>'Envie-nos seu feedback:',
'st_software_license_bl'=>'Licença',
'ti_audit'=>'Auditoria',
'ti_bin'=>'Lixeira',
'ti_customization'=>'Customizações',
'ti_my_account'=>'Minha Conta',
'ti_profiles'=>'Perfis',
'ti_system_settings'=>'Configurações do Sistema',
'ti_users'=>'Usuários',
'vb_exit'=>'Sair',
'vb_feedback'=>'Feedback',
'vb_help'=>'Ajuda',
'vb_user_view'=>'Visão do usuário',

/* './packages/improvement/nav_action_plan.php' */

'mx_corrective'=>'Corretiva',
'mx_preventive'=>'Preventiva',
'st_ap_remove_message'=>'Você tem certeza que deseja remover o plano de ação <b>%name%</b>?',
'tt_ap_remove'=>'Remover Plano de Ação',
'tt_nc_filter_grid_action_plan'=>'(filtrado pela não conformidade \'<b>%nc_name%</b>\')',

/* './packages/improvement/nav_action_plan.xml' */

'gc_actiontype'=>'Tipo de Ação',
'gc_ap_cns'=>'Não Conformidades',
'gc_dateconclusion'=>'Data de Conclusão',
'gc_datedeadline'=>'Data Limite',
'gc_isefficient'=>'Eficiente',
'gc_responsible_id'=>'Responsável',
'mi_associate_nonconformity'=>'Associar Não Conformidade',
'mi_data_to_meassure'=>'Definir Medição',
'mi_meassure'=>'Medir',
'tt_ap_bl'=>'<b>Plano de Ação</b>',

/* './packages/improvement/nav_disciplinary_process.xml' */

'gc_action_taken'=>'Ação Realizada',
'mi_register_action'=>'Registrar Ação',

/* './packages/improvement/nav_incident.php' */

'mx_direct'=>'Direta',
'mx_indirect'=>'Indireta',
'mx_no_category'=>'Sem Categoria',
'st_incident_remove_confirm'=>'Você tem certeza que deseja remover o acidente <b>%incident_name%</b>?',
'tt_incident_remove_confirm'=>'Remover Acidente',

/* './packages/improvement/nav_incident.xml' */

'gc_incident_date_limit'=>'Previsão',
'gc_loss_type'=>'Tipo de Perda',
'mi_disciplinary_process'=>'Processo Disciplinar',
'mi_finish'=>'Finalizar',
'mi_immediate_disposal_approval'=>'Aprovação Disp. Imediata',
'mi_solution_approval'=>'Aprovação Solução',
'mi_view_solution'=>'Visualizar Solução',
'mi_view_evidence'=>'Visualizar Evidência',
'mi_view_immediate_disposal'=>'Visualizar Disposição Imediata',
'mi_view_loss_type'=>'Visualizar Tipo de Perda',
'tt_incident'=>'<b>Acidente</b>',

/* './packages/improvement/nav_non_conformity.php' */

'gc_control_security'=>'Controle de Segurança',
'gc_external_audit'=>'Auditoria Externa',
'gc_internal_audit'=>'Auditoria Interna',
'gc_no'=>'Não',
'gc_yes'=>'Sim',
'st_nc_remove_message'=>'Você tem certeza de que deseja remover a não conformidade <b>%name%</b>?',
'tt_ap_filter_grid_non_conformity'=>'(filtrada pelo plano de ação \'<b>%ap_name%</b>\')',
'tt_nc_remove'=>'Remover Não Conformidade',

/* './packages/improvement/nav_non_conformity.xml' */

'gc_action_plans'=>'Planos de Ação',
'gc_sender'=>'Emissor',
'gc_state'=>'Status',
'mi_action_plan'=>'Associar Plano de Ação',
'mi_send_to_ap_pendant'=>'Enviar para Aceitação do PA',
'mi_send_to_conclusion'=>'Enviar para Conclusão',
'mi_send_to_responsible'=>'Enviar para Responsável',
'tt_nc_bl'=>'<b>Não Conformidade</b>',

/* './packages/improvement/nav_occurrence.php' */

'st_occurrence_remove'=>'Você tem certeza de que deseja remover o incidente <b>%occurrence_text%</b>?',
'tt_occurrence_remove'=>'Remover Incidente',

/* './packages/improvement/nav_occurrence.xml' */

'gc_incident'=>'Acidente',
'mi_incident'=>'Associar Acidente',
'tt_occurrences'=>'<b>Incidentes</b>',

/* './packages/improvement/nav_report.php' */

'mx_report_action_plan_by_process'=>'Plano de Ação por Processo',
'mx_report_action_plan_by_user'=>'Plano de Ação por Usuário',
'mx_report_ap_without_doc'=>'Plano de Ação sem Documento',
'mx_report_ci_pendant_tasks'=>'Tarefas pendentes da Melhoria Contínua',
'mx_report_control_nonconformity_by_process'=>'Não Conformidade de Controle por Processo',
'mx_report_elements_affected_by_incident'=>'Elementos afetados por Acidentes',
'mx_report_financial_impact_by_incident'=>'Impacto Financeiro por Acidente',
'mx_report_incident_accompaniment'=>'Acompanhamento de Acidente',
'mx_report_incident_by_responsible'=>'Coleta de Evidências',
'mx_report_incident_control_revision'=>'Revisões de Controles ocasionadas por Acidentes',
'mx_report_incident_without_risk'=>'Acidente sem Risco',
'mx_report_incidents_without_occurrence'=>'Acidentes sem Incidentes',
'mx_report_nc_without_ap'=>'Não Conformidade Sem Plano de Ação',
'mx_report_nonconformity_accompaniment'=>'Acompanhamento de Não Conformidade',
'mx_report_nonconformity_by_process'=>'Não Conformidade por Processo',
'mx_report_occurrence_accompaniment'=>'Acompanhamento de Incidente',
'mx_report_occurrence_by_incident'=>'Incidentes por Acidente',
'mx_report_risk_auto_probability_value'=>'Cálculo Automático de Probabilidade dos Riscos',
'mx_report_user_by_incident'=>'Processo Disciplinar',

/* './packages/improvement/nav_report.xml' */

'lb_conclusion_cl'=>'Conclusão:',
'lb_incident_order_by'=>'Ordenação:Ordenação:',
'lb_show_cl'=>'Mostrar:',
'si_capability_all'=>'Todos',
'si_capability_no'=>'Não',
'si_capability_yes'=>'Sim',
'si_ci_manager'=>'Relatórios de Melhoria Contínua',
'si_incident_order_by_category'=>'Categoria',
'si_incident_order_by_date'=>'Data do Acidente',
'si_incident_order_by_name'=>'Nome',
'si_incident_order_by_occurrence_qt'=>'Quantidade de Incidentes',
'si_incident_order_by_responsible'=>'Responsável',
'si_incident_report'=>'Relatórios de Acidente',
'si_nonconformity_report'=>'Relatórios de Não Conformidade',
'st_action_plan_filter_bl'=>'<b>Filtro de Plano de Ação</b>',
'st_ap_filter'=>'<b>Filtro de Plano de Ação</b>',
'st_elements_filter_bl'=>'<b>Filtro de Elementos</b>',
'st_incident_filter_bl'=>'<b>Filtro de Acidente</b>',
'st_nonconformity_filter_bl'=>'<b>Filtro de Não Conformidade</b>',
'st_occurrence_filter_bl'=>'<b>Filtro de Incidente</b>',
'st_status'=>'Status:',
'wn_at_least_one_element_must_be_selected'=>'Pelo menos um elemento deve ser selecionado.',

/* './packages/improvement/popup_action_plan_associate.xml' */

'tt_action_plan_association'=>'Associação de Planos de Ação',
'tt_current_action_plans_bl'=>'<b>Planos de Ação Atuais</b>',

/* './packages/improvement/popup_action_plan_edit.xml' */

'lb_ap_actionplan_bl_cl'=>'<b>Ação:</b>',
'lb_ap_actiontype_bl_cl'=>'<b>Tipo de Ação:</b>',
'lb_ap_dateconclusion_cl'=>'Data de Conclusão:',
'lb_ap_datedeadline_bl_cl'=>'<b>Limite de Implementação:</b>',
'lb_ap_name_bl_cl'=>'<b>Nome:</b>',
'lb_ap_responsible_id_bl_cl'=>'<b>Responsável:</b>',
'rb_corrective'=>'Corretiva',
'rb_preventive'=>'Preventiva',
'tt_ap_edit'=>'Edição de Plano de Ação',
'vb_finish'=>'Concluir',
'wn_deadline'=>'O limite de implementação deve ser maior ou igual a data atual.',

/* './packages/improvement/popup_action_plan_finish_confirm.xml' */

'lb_ap_days_before_bl'=>'<b>Avisar com antecedência de</b>',
'lb_date_efficiency_revision_bl_cl'=>'<b>Data da Revisão da Eficácia:</b>',
'st_action_plan_finish_confirm_message'=>'Deseja finalizar o Plano de Ação \'%name%\'?',
'tt_action_plan_finish'=>'Finalizar Plano de Ação',
'wn_revision_date_after_conclusion_date'=>'A data da revisão da eficácia deve ser maior ou igual a data de conclusão!',

/* './packages/improvement/popup_action_plan_revision_task.xml' */

'lb_action_cl'=>'Ação:',
'st_action_plan_efficient_bl'=>'<b>O Plano de Ação foi eficiente?</b>',
'st_data_revision_bl_cl'=>'<b>Data da revisão:</b>',
'tt_action_plan_efficiency_revision'=>'Revisão de Eficiência do Plano de Ação',

/* './packages/improvement/popup_disciplinary_process_action.php' */

'st_denied_permission_to_edit_action_taken'=>'Você não tem permissão para registrar uma ação em um processo disciplinar.',

/* './packages/improvement/popup_disciplinary_process_action.xml' */

'lb_action_bl_cl'=>'<b>Ação:</b>',
'st_incident_cl'=>'<b>Acidente:</b>',
'st_user_cl'=>'<b>Usuário:</b>',

/* './packages/improvement/popup_disciplinary_process_edit.php' */

'em_disciplinary_process_creation'=>'Criação de Processo Disciplinar',
'em_disciplinary_process_creation_footer'=>'Para maiores informações entre em contato com o security officer.',
'st_incident_user_remove'=>'Você tem certeza que deseja remover o usuário <b>%user_name%</b> do processo disciplinar do acidente <b>%incident_name%</b>?',
'tt_incident_user_remove'=>'Remover Usuário',

/* './packages/improvement/popup_disciplinary_process_edit.xml' */

'gc_observation_involvement'=>'Observação / Envolvimento',
'gc_user'=>'Usuário',
'lb_incident_cl'=>'Acidente:',
'lb_observation_involvement_cl'=>'Observação / Envolvimento:',
'lb_user_bl_cl'=>'<b>Usuário:</b>',
'tt_disciplinary_process'=>'<b>Processo Disciplinar</b>',
'wn_email_dp_sent'=>'Um e-mail já foi enviado para o Gestor de Processo Disciplinar.',

/* './packages/improvement/popup_evidence_requirement.xml' */

'lb_comments_cl'=>'Comentários:',
'tt_evidence_requirement'=>'Coleta de Evidências',
'wn_evidence_requirement_editing'=>'Obs: Ao editar o comentário acima, um alerta com seu conteúdo será enviado ao gestor de coleta de evidências.',

/* './packages/improvement/popup_financial_impact_edit.xml' */

'st_total'=>'Total',
'tt_financial_impact_parametrization'=>'Estimativa de Impacto Financeiro',

/* './packages/improvement/popup_incident_accounts_plan.xml' */

'tt_incident_accounts_plan'=>'Plano de Contas',

/* './packages/improvement/popup_incident_affected_product_service.xml' */

'tt_affected_product_service'=>'Produto / Serviço Afetado',

/* './packages/improvement/popup_incident_approval_task.xml' */

'tt_assets_per_incident'=>'<b>A.P.S. Relacionados ao Acidente</b>',

/* './packages/improvement/popup_incident_disposal.xml' */

'tt_immediate_disposal'=>'Disposição Imediata',

/* './packages/improvement/popup_incident_edit.xml' */

'lb_accounts_plan_cl'=>'Plano de Contas:',
'lb_affected_product_service_cl'=>'Produto / Serviço Afetado:',
'lb_date_bl_cl'=>'<b>Data:</b>',
'lb_date_limit_bl_cl'=>'<b>Previsão de Término:</b>',
'lb_evidences_cl'=>'Evidências:',
'lb_hour_bl_cl'=>'<b>Hora:</b>',
'lb_immediate_disposal_cl'=>'Disposição Imediata:',
'lb_loss_type_bl_cl'=>'<b>Tipo de Perda:</b>',
'lb_occurrences_bl_cl'=>'Incidentes:',
'lb_occurrences_cl'=>'Incidentes:',
'lb_solution_cl'=>'Solução:',
'si_direct'=>'Direta',
'si_indirect'=>'Indireta',
'st_collect_evidence'=>'Coletar Evidências?',
'st_incident_date_finish'=>'Previsão de Término:',
'st_incident_date_finish_bl'=>'Previsão de Término:',
'tt_incident_edit'=>'Edição de Acidente',
'vb_financial_impact'=>'Impacto Financeiro',
'vb_observations'=>'Observações',
'wn_future_date_not_allowed'=>'Não é permitido uma data/hora no futuro.',
'wn_prevision_finish'=>'A previsão de término deve ser maior ou igual ao dia atual.',

/* './packages/improvement/popup_incident_evidence.xml' */

'tt_evidence'=>'Evidência',
'tt_loss_type'=>'Tipo de Perda',

/* './packages/improvement/popup_incident_process_association_edit.php' */

'st_save_suggested_incident_processes'=>'Você deseja relacionar os processos sugeridos ao acidente <b>%context_name%</b>?',
'tt_save_suggested_incident_processes'=>'Relacionar Processos Sugeridos',

/* './packages/improvement/popup_incident_process_association_edit.xml' */

'cb_search_only_suggested_process'=>'Pesquisar apenas processos sugeridos',
'tt_incident_process_association'=>'Associação de processo a acidente',

/* './packages/improvement/popup_incident_risk_asset_association_edit.xml' */

'lb_assets_cl'=>'A.P.S.:',
'lb_risks_cl'=>'Riscos:',
'st_risk_asset_association_message'=>'Ao selecionar um A.P.S., selecione os riscos que deseja que sejam associados a ele. Você pode associar mais de um risco, selecionando-os com a tecla Control pressionada.',
'tt_risk_asset_association'=>'Associação de Riscos a A.P.S.',

/* './packages/improvement/popup_incident_risk_association_edit.php' */

'em_incident_risk_parametrization'=>'Estimativa de Risco associado ao Acidente',
'em_incident_risk_parametrization_footer'=>'Para maiores informações entre em contato com o security officer.',
'mx_non_parameterized_risk'=>'Risco Não Estimado',
'to_dependencies_bl_cl'=>'<b>Dependências:</b> <br><br>',
'to_dependents_bl_cl'=>'<b>Dependentes:</b> <br><br>',

/* './packages/improvement/popup_incident_risk_association_edit.xml' */

'tt_associated_controls_and_assets'=>'<b>Controles e A.P.S. relacionados</b>',
'tt_incident_risk_association'=>'Associação Acidente-Risco',
'tt_risks_refferring_controls_and_assets'=>'<b>Riscos Referentes aos Controles e A.P.S.</b>',
'tt_risks_related_selected'=>'<b>Riscos relacionados / selecionados</b>',
'vb_insert_risk'=>'Inserir Risco',
'vb_risk_incident_parametrization'=>'Estimativa Acidente-Risco',
'vb_search_assets'=>'Buscar A.P.S.',
'vb_search_controls'=>'Buscar Controles',
'wn_parameterize_incident_risk'=>'Você deve estimar a associação Acidente-Risco antes de salvar sua associação.',

/* './packages/improvement/popup_incident_risk_association_options.xml' */

'lb_associate_risk_to_assets'=>'Deseja relacionar os riscos atuais a A.P.S. selecionados? Você poderá escolher quais riscos deseja relacionar a quais A.P.S..',
'lb_send_alert_to_other_users'=>'Deseja enviar alertas para outros usuários além dos responsáveis pela segurança dos A.P.S. e controles relacionados ao(s) %new_risks% novo(s) risco(s)?',
'tt_incident_risk_association_options'=>'Opções para associação de riscos e acidentes',
'vb_continue'=>'Continuar',

/* './packages/improvement/popup_incident_risk_parametrization.xml' */

'tt_incident_risk_association_parametrization'=>'Estimativa da associação Acidente-Risco',

/* './packages/improvement/popup_incident_search.xml' */

'tt_incident_search'=>'Busca de Acidentes',
'vb_relation'=>'Relacionar',
'wn_no_incident_selected'=>'Nenhum acidente selecionado.',

/* './packages/improvement/popup_incident_solution.xml' */

'tt_solution'=>'Solução',

/* './packages/improvement/popup_incident_state_forward.php' */

'st_incident_finish'=>'Deseja finalizar o acidente?',
'st_incident_send_to_app_disposal'=>'Deseja enviar o acidente para aprovação de Disposição Imediata?',
'st_incident_send_to_app_solution'=>'Deseja enviar o acidente para aprovação de Solução?',
'st_incident_send_to_responsible'=>'Deseja enviar o acidente para o responsável?',
'tt_incident_finish'=>'Finalizar o Acidente',
'tt_incident_send_to_app_disposal'=>'Enviar para Aprovação de Disposição Imediata',
'tt_incident_send_to_app_solution'=>'Enviar para Aprovação de Solução',
'tt_incident_send_to_responsible'=>'Enviar para Responsável',

/* './packages/improvement/popup_incident_task_view.php' */

'lb_incident_immediate_disposal_cl'=>'Disposição Imediata:',
'lb_incident_solution_cl'=>'Solução:',

/* './packages/improvement/popup_incident_task_view.xml' */

'lb_date_limit_cl'=>'Data Limite:',

/* './packages/improvement/popup_new_risk_parameter_approval_task.php' */


/* './packages/improvement/popup_new_risk_parameter_approval_task.xml' */


/* './packages/improvement/popup_non_conformity_approval_task.xml' */

'lb_processes_cl'=>'Processos:',

/* './packages/improvement/popup_non_conformity_associate.xml' */

'st_name'=>'Nome:',
'tt_current_non_conformitys_bl'=>'<b>Não Conformidades Atuais</b>',
'tt_non_conformity_association'=>'Associação de Não Conformidades',

/* './packages/improvement/popup_non_conformity_create.xml' */

'lb_date_sent_cl'=>'Data de Emissão:',
'lb_sender_cl'=>'Emissor:',
'tt_non_conformity_record'=>'Registro de Não Conformidade',

/* './packages/improvement/popup_non_conformity_data_approval_task.php' */

'st_denied_permission_to_execute_task'=>'Você não tem permissão para executar a tarefa',

/* './packages/improvement/popup_non_conformity_data_approval_task.xml' */

'lb_ap_responsible_cl'=>'Resp. pela IPA:',
'lb_cause_cl'=>'Causa:',
'lb_inquirer_cl'=>'Requerente:',
'lb_justificative_cl'=>'Justificativa:',
'lb_non_conformity_cl'=>'Não Conformidade:',
'lb_task_cl'=>'Tarefa:',
'tt_task_visualization'=>'Visualização da Tarefa',

/* './packages/improvement/popup_non_conformity_edit.xml' */

'lb_action_plans_cl'=>'Planos de Ação:',
'lb_nc_capability_bl_cl'=>'Potencial:',
'lb_nc_cause_cl'=>'Causa:',
'lb_nc_classification_cl'=>'Classificação:',
'lb_nc_datesent_cl'=>'Data de Emissão:',
'lb_nc_description_cl'=>'Descrição:',
'lb_nc_name_bl_cl'=>'<b>Nome:</b>',
'lb_nc_responsible_id_cl'=>'Responsável:',
'lb_nc_sender_id_cl'=>'Emissor:',
'lb_processes_bl_cl'=>'<b>Processos:</b>',
'si_no'=>'Não',
'si_yes'=>'Sim',
'tt_nc_edit'=>'Edição de Não Conformidade',

/* './packages/improvement/popup_non_conformity_revision_task.xml' */

'lb_action_plan_cl'=>'Plano de Ação:',
'lb_ap_efficient_bl'=>'<b>O Plano de Ação foi eficiente?</b>',
'rb_no'=>'Não',
'rb_yes'=>'Sim',
'tt_non_conformity_efficiency_revision'=>'Revisão de Eficiência da Não Conformidade',

/* './packages/improvement/popup_non_conformity_state_forward.php' */

'st_non_conformity_send_to_ap_responsible'=>'Deseja enviar a Não Conformidade para a Aceitação dos Planos de Ação?',
'st_non_conformity_send_to_approve'=>'Deseja enviar a Não Conformidade para a Conclusão dos Planos de Ação?',
'st_non_conformity_send_to_responsible'=>'Deseja enviar a Não Conformidade para o Responsável?',
'tt_non_conformity_send_to_ap_responsible'=>'Enviar para a Aceitação dos Planos de Ação',
'tt_non_conformity_send_to_approve'=>'Enviar para a Conclusão dos Planos de Ação',
'tt_non_conformity_send_to_responsible'=>'Enviar para Responsável',

/* './packages/improvement/popup_non_conformity_state_forward.xml' */

'vb_no'=>'Não',
'vb_yes'=>'Sim',

/* './packages/improvement/popup_occurrence_edit.xml' */

'lb_description_bl_cl'=>'<b>Descrição:</b>',
'st_date_bl'=>'<b>Data:</b>',
'st_date_warning'=>'Não é permitido uma data/hora no futuro.',
'st_hour_bl'=>'<b>Hora:</b>',
'tt_register_occurrence'=>'Registrar Ação',

/* './packages/improvement/popup_occurrences_search.xml' */

'tt_current_occurrences'=>'<b>Incidentes Atuais</b>',
'tt_occurrences_search'=>'Busca de Incidentes',

/* './packages/improvement/popup_probability_calculation_config.xml' */

'lb_incidents_in_period_cl'=>'Acidentes entre %initial_date% e %final_date%:',
'lb_last_check_cl'=>'Ultima verificação:',
'lb_periodicity_bl_cl'=>'<b>Periodicidade:</b>',
'lb_starting_at'=>'A partir de',
'si_days'=>'Dia(s)',
'si_months'=>'Mês(es)',
'si_weeks'=>'Semana(s)',
'st_total_incidents_found'=>'Total de acidentes encontrados:',
'tt_incidents_related_to_risk_in_period'=>'<b>Acidentes relacionados ao risco no período</b>',
'tt_probability_calculation_config'=>'Configuração de Cálculo Automático de Probabilidade',
'tt_probability_level_vs_incident_count'=>'<b>Nível de Probabilidade</b> <br/><br/> <b>X</b> <br/><br/> <b>Quantidade de Acidentes</b>',
'vb_update'=>'Atualizar',
'wn_probability_config_not_filled'=>'Intervalos não preenchidos ou com valores inconsistentes',

/* './packages/improvement/popup_process_search.xml' */

'tt_current_processes'=>'<b>Processos Atuais</b>',
'tt_process_search'=>'Busca de Processos',

/* './packages/improvement/popup_request_compare_impact_risks.xml' */


/* './packages/improvement/popup_risk_create.xml' */

'lb_impact_money_cl'=>'Impacto Financeiro:',
'si_select_one_below'=>'Selecione um dos itens abaixo',
'st_risk_impact'=>'Impacto:',
'tt_insert_risk'=>'Inserir Risco',

/* './packages/improvement/report/clock.php' */

'st_clock_wait'=>'<b>Atenção:</b> Este relatório pode levar alguns minutos para ser gerado.<br>Por favor, aguarde.',

/* './packages/improvement/report/popup_report_action_plan_by_process.php' */

'tt_action_plan_by_process'=>'Plano de Ação por Processo',

/* './packages/improvement/report/popup_report_action_plan_by_user.php' */

'tt_action_plan_by_user'=>'Plano de Ação por Usuário',

/* './packages/improvement/report/popup_report_ap_without_doc.php' */

'rs_ap_without_docs'=>'Planos de Ação sem Documentos',

/* './packages/improvement/report/popup_report_ap_without_doc.xml' */

'rs_ap'=>'Plano de Ação',

/* './packages/improvement/report/popup_report_control_nonconformity_by_process.php' */

'mx_control_nonconformity_by_process'=>'Não Conformidade de Controle por Processo',

/* './packages/improvement/report/popup_report_disciplinary_process.php' */

'mx_disciplinary_process'=>'Processos Disciplinares',

/* './packages/improvement/report/popup_report_disciplinary_process.xml' */

'rs_dp_name'=>'Nome',

/* './packages/improvement/report/popup_report_elements_affected_by_incident.php' */

'mx_elements_affected_by_incident'=>'Elementos afetados por Acidentes',

/* './packages/improvement/report/popup_report_elements_affected_by_incident.xml' */

'rs_incident_bl_cl'=>'<b>Acidente:</b>',

/* './packages/improvement/report/popup_report_financial_impact_by_incident.php' */

'mx_financial_impact_by_incident'=>'Impacto Financeiro por Acidente',

/* './packages/improvement/report/popup_report_incident_accompaniment.php' */

'mx_incident_accompaniment'=>'Acompanhamento de Acidente',

/* './packages/improvement/report/popup_report_incident_by_responsible.php' */

'mx_evidence_collection'=>'Coleta de Evidências',

/* './packages/improvement/report/popup_report_incident_control_revision.php' */

'mx_incident_control_revision'=>'Revisões de Controles ocasionadas por Acidentes',

/* './packages/improvement/report/popup_report_incident_control_revision.xml' */

'rs_date_todo'=>'Data Prevista',
'rs_er'=>'ER',

/* './packages/improvement/report/popup_report_incident_without_occurrence.php' */

'rs_incidents_without_occurrence'=>'Acidentes sem Incidentes',

/* './packages/improvement/report/popup_report_incident_without_risk.php' */

'rs_incidentes_without_risks'=>'Acidentes sem Riscos',

/* './packages/improvement/report/popup_report_incident_without_risk.xml' */

'rs_incident'=>'Acidente',

/* './packages/improvement/report/popup_report_incidents.php' */

'mx_incidents'=>'Acidentes',

/* './packages/improvement/report/popup_report_incidents.xml' */

'rs_incident_name'=>'Nome',

/* './packages/improvement/report/popup_report_nc_without_ap.php' */

'rs_nc_without_ap'=>'Não Conformidades sem Planos de Ação',
'st_nc_manager_with_parentheses'=>'(Gestor de Não Conformidade)',

/* './packages/improvement/report/popup_report_non_conformities.php' */

'mx_non_conformities'=>'Não Conformidades',

/* './packages/improvement/report/popup_report_non_conformities.xml' */

'rs_nc_name'=>'Nome',

/* './packages/improvement/report/popup_report_nonconformity_accompaniment.php' */

'mx_nonconformity_accompaniment'=>'Acompanhamento de Não Conformidade',
'rs_not_defined'=>'Não definido',

/* './packages/improvement/report/popup_report_nonconformity_accompaniment.xml' */

'rs_action_plan'=>'Plano de Ação',
'rs_conclusion'=>'Conclusão',
'rs_non_conformity'=>'Não Conformidade:',

/* './packages/improvement/report/popup_report_nonconformity_by_process.php' */

'mx_nonconformity_by_process'=>'Não Conformidade por Processo',

/* './packages/improvement/report/popup_report_nonconformity_by_process.xml' */

'rs_nonconformity'=>'Não Conformidade',

/* './packages/improvement/report/popup_report_occurrence_accompaniment.php' */

'mx_occurrence_accompaniment'=>'Acompanhamento de Incidente',

/* './packages/improvement/report/popup_report_occurrence_by_incident.php' */

'mx_occurrence_by_incident'=>'Incidentes por Acidente',

/* './packages/improvement/report/popup_report_occurrence_by_incident.xml' */

'rs_date_cl'=>'Data',
'rs_occurrence'=>'Incidente',

/* './packages/improvement/report/popup_report_risk_auto_probability_value.php' */

'mx_risk_auto_probability_value'=>'Cálculo Automático de Probabilidade dos Riscos',

/* './packages/improvement/report/popup_report_risk_auto_probability_value.xml' */

'rs_incident_amount'=>'Qtd. Acidentes',
'rs_last_verification'=>'Última Verificação',
'rs_periodicity'=>'Periodicidade',

/* './packages/improvement/report/popup_report_user_by_incident.php' */

'mx_user_by_incident'=>'Processo Disciplinar',

/* './packages/improvement/report/popup_report_user_by_incident.xml' */

'rs_incident_cl'=>'Acidente:',

/* './packages/improvement/report/report_template.xml' */

'rs_fim'=>'FIM DO RELATÓRIO',

/* './packages/improvement/tab_continual_improvement.xml' */

'ti_action_plan'=>'Plano de Ação',
'ti_disciplinary_process'=>'Processo Disciplinar',
'ti_incident'=>'Acidente',
'ti_non_conformity'=>'Não Conformidade',
'ti_occurrences'=>'Incidentes',
'ti_report'=>'Relatórios',

/* './packages/libraries/nav_best_practices.php' */

'st_remove_best_practice_confirm'=>'Você tem certeza de que deseja remover o requisito legal <b>%best_practice_name%</b>?',
'st_remove_section_confirm'=>'Você tem certeza de que deseja remover a seção <b>%section_name%</b>?',
'tt_remove_best_practice'=>'Remover Requisito Legal',
'tt_remove_section'=>'Remover Seção',

/* './packages/libraries/nav_best_practices.xml' */

'mi_associate_events'=>'Associar Eventos',
'mi_open_section'=>'Abrir Seção',
'mi_templates'=>'Modelos de Documentos',
'tt_best_practices_bl'=>'<b>Requisitos Legais</b>',
'vb_insert_best_practice'=>'Inserir Requisito Legal',
'vb_insert_section'=>'Inserir Seção',

/* './packages/libraries/nav_document_template.php' */

'st_remove_document_template_confirm'=>'Você tem certeza de que deseja remover o modelo de documento <b>%context_name%</b>?',
'tt_remove_document_template'=>'Remover Modelo de Documento',

/* './packages/libraries/nav_document_template.xml' */

'si_best_practice'=>'Requisito Legal',
'si_denied'=>'Negado',
'si_doc_template_control'=>'Controle',
'si_pending'=>'Pendente',
'tt_documents_templates_bl'=>'<b>Modelos de Documentos</b>',

/* './packages/libraries/nav_events.php' */

'st_remove_category_cascade_message'=>'Você estará deletando também qualquer A.P.S. relacionado à Categoria, bem como seus Riscos e associação dos mesmos com outros elementos do sistema.',
'st_remove_category_confirm'=>'Você tem certeza de que deseja remover a categoria <b>%category_name%</b>?',
'st_remove_event_confirm'=>'Você tem certeza de que deseja remover o evento <b>%event_name%</b>',
'tt_remove_category'=>'Remover Categoria',
'tt_remove_event'=>'Remover Evento',

/* './packages/libraries/nav_events.xml' */

'mi_associate_best_practices'=>'Associar Requisitos Legais',
'mi_open_event_category'=>'Abrir Categoria',
'tt_events_library_bl'=>'<b>Biblioteca de Eventos</b>',
'vb_associate_event'=>'Associar Evento',
'vb_insert_category'=>'Inserir Categoria',
'vb_insert_event'=>'Inserir Evento',

/* './packages/libraries/nav_import_export.php' */

'wn_error_invalid_file'=>'Erro: arquivo inválido.',
'wn_extension_not_allowed'=>'Extensão não permitida.',
'wn_maximum_size_exceeded'=>'Tamanho máximo excedido.',

/* './packages/libraries/nav_import_export.xml' */

'lb_client_cl'=>'Cliente:',
'lb_export_library_cl'=>'Exportar Biblioteca:',
'lb_import_library_cl'=>'Importar Biblioteca (AOL/XML):',
'lb_license_cl'=>'Licença:',
'si_best_practices'=>'Requisitos Legais',
'si_events_library'=>'Biblioteca de Eventos',
'si_standards'=>'Legislações',
'si_templates'=>'Modelos de Documentos',
'st_export_list_bl'=>'<b>Lista de Exportação</b>',
'st_exporting_options_bl'=>'<b>Opções de Exportação</b>',
'st_import_list_bl'=>'<b>Lista de Importação</b>',
'st_or_bl'=>'<b>Ou</b>',
'to_export'=>'<b>Exportar:</b><br/><br/>Exportar os elementos selecionados.',
'to_export_aol'=>'<b>Exportar:</b><br/><br/>Exportar os elementos selecionados em formato AOL.',
'to_export_list'=>'<b>Listar:</b><br/><br/>Lista os elementos da biblioteca, que serão exportados.',
'to_export_xml'=>'<b>Exportar:</b><br/><br/>Exportar os elementos selecionados em formato XML.',
'to_import'=>'<b>Importar:</b><br/><br/>Importar os elementos selecionados.',
'to_import_list'=>'<b>Listar:</b><br/><br/>Lista os elementos da biblioteca, que serão importados.',
'tr_best_practices'=>'Requisitos Legais',
'tr_events_library'=>'Biblioteca de Eventos',
'tr_no_items_to_import'=>'Não há elementos a serem importados',
'tr_standards'=>'Legislações',
'tr_template'=>'Modelos de Documentos',
'tr_templates'=>'Modelos de Documentos',
'vb_export'=>'Exportar',
'vb_export_as_aol'=>'Exportar como AOL',
'vb_export_as_xml'=>'Exportar como XML',
'vb_import'=>'Importar',
'vb_list'=>'Listar',
'vb_list_import'=>'Listar',
'wn_import_required_file'=>'Você deve escolher um arquivo antes de clicar em Listar.',

/* './packages/libraries/nav_incident_category.php' */

'st_category_remove'=>'Você tem certeza de que deseja remover a categoria <b>%category_name%</b>?',
'st_category_remove_error'=>'Não é possivel remover a categoria <b>%category_name%</b> porque ela esta relacionada a um ou mais acidentes!',
'st_solution_remove'=>'Você tem certeza de que deseja remover esta solução?',
'tt_category_remove'=>'Remover Categoria',
'tt_category_remove_error'=>'Erro ao Remover Categoria',
'tt_solution_remove'=>'Remover Solução',

/* './packages/libraries/nav_incident_category.xml' */

'gc_keywords'=>'Palavras-chave',
'gc_problem'=>'Problema',
'gc_solution'=>'Solução',
'gc_solutions_bl'=>'<b>Soluções</b>',
'mi_delete'=>'Remover',
'mi_open_category'=>'Abrir Categoria',
'tt_category_library'=>'<b>Biblioteca de Categorias</b>',
'vb_solution_insert'=>'Inserir Solução',

/* './packages/libraries/nav_standards.php' */

'st_remove_standard_confirm'=>'Você tem certeza que deseja remover a legislação <b>%standard_name%</b>?',
'tt_remove_standard'=>'Remover Legislação',

/* './packages/libraries/nav_standards.xml' */

'gc_creator'=>'Criador',
'tt_standards_bl'=>'<b>Legislações</b>',

/* './packages/libraries/popup_best_practice_add.xml' */

'si_all_sections'=>'Todas Seções',
'tt_current_best_practices_bl'=>'<b>Requisitos Legais Atuais</b>',

/* './packages/libraries/popup_best_practice_edit.php' */

'tt_best_practice_adding'=>'Adição de Requisito Legal',
'tt_best_practice_editing'=>'Edição de Requisito Legal',

/* './packages/libraries/popup_best_practice_edit.xml' */

'lb_control_objective_cl'=>'Objetivo do Controle:',
'lb_control_type_cl'=>'Tipo de Controle:',
'lb_implementation_guidelines_cl'=>'Guia de Implementação:',
'lb_metric_cl'=>'Métrica:',
'lb_name_good_practices_bl_cl'=>'<b>Nome/Requisito:</b>',
'lb_standards_bl_cl'=>'<b>Legislações:</b>',
'si_administrative'=>'Administrativo',
'si_awareness'=>'Conscientização',
'si_coercion'=>'Coerção',
'si_correction'=>'Correção',
'si_detection'=>'Detecção',
'si_limitation'=>'Limitação',
'si_monitoring'=>'Monitoração',
'si_physical'=>'Físico',
'si_prevention'=>'Prevenção',
'si_recovery'=>'Recuperação',
'si_technical'=>'Técnico',
'tt_best_practice'=>'Requisito Legal',

/* './packages/libraries/popup_category_edit.php' */

'tt_category_adding'=>'Adição de Categoria',
'tt_category_editing'=>'Edição de Categoria',

/* './packages/libraries/popup_document_template_edit.php' */

'tt_document_template_adding'=>'Adição de Modelo de Documento',
'tt_document_template_editing'=>'Edição de Modelo de Documento',

/* './packages/libraries/popup_document_template_edit.xml' */

'lb_best_practice_cl'=>'Requisito Legal:',
'lb_type_bl_cl'=>'<b>Tipo:</b>',
'si_action_plan'=>'Plano de Ação',
'si_area'=>'Área',
'si_asset'=>'A.P.S.',
'si_control'=>'Controle',
'si_policy'=>'Documento',
'si_process'=>'Processo',
'si_scope'=>'Escopo',
'si_select_type'=>'Selecione um Tipo',

/* './packages/libraries/popup_event_add.xml' */

'tt_current_events_bl'=>'<b>Eventos Atuais</b>',

/* './packages/libraries/popup_event_edit.php' */

'tt_event_adding'=>'Adição de Evento',
'tt_event_editing'=>'Edição de Evento',

/* './packages/libraries/popup_event_edit.xml' */

'cb_suggested_event'=>'Evento sugerido',
'lb_observation_cl'=>'Observação:',
'st_event_impact_cl'=>'Impacto:',

/* './packages/libraries/popup_imple_guide.xml' */

'tt_implementation_guidelines'=>'Guia de Implementação',

/* './packages/libraries/popup_incident_category_edit.php' */

'tt_category_addition'=>'Adição de Categoria',

/* './packages/libraries/popup_incident_category_edit.xml' */

'tt_category_edit'=>'Edição de Categoria',

/* './packages/libraries/popup_incident_solution_edit.xml' */

'lb_problem_bl_cl'=>'<b>Problema:</b>',
'lb_solution_bl_cl'=>'<b>Solução:</b>',
'tt_solution_edit'=>'Edição de Solução',

/* './packages/libraries/popup_metric.xml' */

'tt_metric'=>'Métrica',

/* './packages/libraries/popup_section_edit.php' */

'tt_section_adding'=>'Adição de Seção',
'tt_section_editing'=>'Edição de Seção',

/* './packages/libraries/popup_standard_add.xml' */

'tt_current_standards_bl'=>'<b>Legislações Atuais</b>',
'tt_standards_search'=>'Busca de Legislações',

/* './packages/libraries/popup_standard_edit.php' */

'tt_standard_adding'=>'Adição de Legislação',
'tt_standard_editing'=>'Edição de Legislação',

/* './packages/libraries/popup_standard_edit.xml' */

'lb_application_cl'=>'Aplicação:',
'lb_objective_cl'=>'Objetivo:',

/* './packages/libraries/tab_libraries.xml' */

'ti_best_practices'=>'Requisitos Legais',
'ti_category_library'=>'Categorias de Acidentes',
'ti_events_library'=>'Biblioteca de Eventos',
'ti_export_and_import'=>'Exportar & Importar',
'ti_standards'=>'Legislações Aplicáveis',
'ti_templates'=>'Modelos de Documentos',

/* './packages/policy/nav_all.php' */

'document_is_subdocument'=>'<b>%document_name%</b> é um subdocumento.<br>Documento Raiz: <b>%document_parent%</b>',

/* './packages/policy/nav_all.xml' */

'lb_status'=>'Status',
'tt_all_documents_bl'=>'<b>Todos Documentos</b>',

/* './packages/policy/nav_approving.xml' */

'tt_approving_documents_bl'=>'<b>Documentos Em Aprovação</b>',

/* './packages/policy/nav_developing.php' */

'tt_developing_document_bl'=>'<b>Documentos Em Desenvolvimento</b>',

/* './packages/policy/nav_developing.xml' */

'vb_insert_document'=>'Inserir Documento',

/* './packages/policy/nav_obsolete.xml' */

'gc_file_name'=>'Arquivo / Link',
'tt_obsolete_documents_bl'=>'<b>Versões Obsoletas de Documentos</b>',

/* './packages/policy/nav_publish.php' */

'tt_published_documents_bl'=>'<b>Documentos Publicados</b>',

/* './packages/policy/nav_register.php' */

'st_remove_register_confirm'=>'Você tem certeza de que deseja remover o registro <b>%register_name%</b>?',
'tt_remove_register'=>'Remover Registro',

/* './packages/policy/nav_register.xml' */

'gc_retention_time'=>'Tempo de Retenção',
'gc_storage_place'=>'Local de Armazenamento',
'gc_storage_type'=>'Tipo de Armazenamento',
'mi_read'=>'Ler',
'mi_read_document'=>'Ler Documento',
'mi_readers'=>'Leitores',
'vb_insert_register'=>'Inserir Registro',

/* './packages/policy/nav_report.php' */

'mx_documents_with_high_revision_frequency'=>'Documentos com uma alta freqüência de revisão',
'mx_documents_without_registers'=>'Documentos sem Registros',
'mx_pendant_task_pm'=>'Tarefas pendentes',
'mx_report_documents_pendent_reads'=>'Documentos com Leitura Pendente',
'mx_report_element_classification'=>'Relatório de Classificação',
'mx_users_with_pendant_read'=>'Usuários com leitura pendente',
'si_access_to_documents'=>'Auditoria de Acesso aos Documentos',
'si_accessed_documents'=>'Auditoria de Acesso dos Usuários',
'si_approvers'=>'Documentos e seus Aprovadores',
'si_documents_by_component'=>'Documentos por Componente do SGSI',
'si_documents_by_state'=>'Documentos',
'si_documents_dates'=>'Data de Criação e Revisão dos Documentos',
'si_documents_per_area'=>'Documentos por Área',
'si_items_with_without_documents'=>'Componentes sem Documentos',
'si_most_accessed_documents'=>'Documentos mais acessados',
'si_most_revised_documents'=>'Documentos mais revisados',
'si_not_accessed_documents'=>'Documentos não acessados',
'si_pendant_approvals'=>'Aprovações Pendentes',
'si_readers'=>'Documentos e seus Leitores',
'si_registers_per_document'=>'Registro por Documento',
'si_users_per_process'=>'Usuários associados aos Processos',

/* './packages/policy/nav_report.xml' */

'cb_expand_documents'=>'Expandir documentos',
'cb_expand_readers'=>'Expandir leitores',
'cb_organize_by_area'=>'Organizar por área',
'cb_show_all_access'=>'Exibir todos acessos',
'cb_show_register_documents'=>'Mostrar Documentos de Registro',
'lb_creation_date_cl'=>'Data de criação:',
'lb_documents_bl_cl'=>'<b>Documentos:</b>',
'lb_final_date_cl'=>'Data final:',
'lb_initial_date_cl'=>'Data inicial:',
'lb_last_revision_date_cl'=>'Data da última revisão:',
'lb_show_only_first_cl'=>'Exibir somente os primeiros:',
'lb_until'=>'até',
'lb_users_bl_cl'=>'<b>Usuários:</b>',
'si_abnormalities_reports'=>'Relatórios de Anormalidade',
'si_access_control_reports'=>'Relatórios de Controle de Acesso',
'si_all_status'=>'Todos',
'si_approved'=>'Aprovado',
'si_developing'=>'Em desenvolvimento',
'si_documents_by_type'=>'Documentos por Tipo',
'si_general_reports'=>'Relatórios Gerais',
'si_management_reports'=>'Relatórios de Gestão',
'si_obsolete'=>'Obsoleto',
'si_pendant'=>'Em aprovação',
'si_register_reports'=>'Relatórios de Registro',
'si_registers_by_type'=>'Registros por Tipo',
'si_revision'=>'Em revisão',
'st_asset'=>'A.P.S.',
'st_components'=>'<b>Componentes:</b>',
'st_control'=>'Controle',
'st_element_type_warning'=>'Por favor, selecione pelo menos um tipo.',
'st_process'=>'Processo',
'st_status_bl'=>'<b>Status:</b>',
'st_type_cl'=>'Tipo:',
'wn_component_filter_warning'=>'Por favor, selecione ao menos um tipo de componente.',
'wn_no_filter_available'=>'Nenhum filtro disponível.',
'wn_select_a_document'=>'Por favor, selecione um documento.',

/* './packages/policy/nav_revision.xml' */

'mi_edit_author'=>'Editar Autor',
'tt_revision_documents_bl'=>'<b>Documentos em Revisão</b>',

/* './packages/policy/nav_to_be_published.php' */

'st_document_confirm_remove'=>'Você tem certeza de que deseja remover o documento <b>%document_name%</b>?',
'tt_remove_document'=>'Remover Documento',

/* './packages/policy/nav_to_be_published.xml' */

'gc_elements'=>'Elementos',
'gc_file_version'=>'Versão',
'gc_sub_doc'=>'Subdoc',
'lb_classification'=>'Classificação',
'lb_type'=>'Tipo',
'mi_approve'=>'Aprovar',
'mi_associate_components'=>'Associar Componentes',
'mi_comments'=>'Comentários',
'mi_copy_readers'=>'Copiar Leitores',
'mi_details'=>'Detalhes',
'mi_document_templates'=>'Modelos de Documento',
'mi_edit_approvers'=>'Editar Aprovadores',
'mi_edit_mainapprover'=>'Editar Responsável',
'mi_edit_readers'=>'Editar Leitores',
'mi_previous_versions'=>'Versões Anteriores',
'mi_publish'=>'Publicar',
'mi_references'=>'Referências',
'mi_registers'=>'Registros',
'mi_send_to_approval'=>'Enviar para Aprovação',
'mi_send_to_revision'=>'Enviar para Revisão',
'mi_sub_documents'=>'Subdocumentos',
'mi_view_approvers'=>'Ver Aprovadores',
'mi_view_components'=>'Ver Componentes',
'mi_view_readers'=>'Ver Leitores',
'tt_documents_to_be_published_bl'=>'<b>Documentos a Publicar</b>',

/* './packages/policy/popup_approvers_edit.xml' */

'tt_approvers_editing'=>'Edição de Aprovadores',
'tt_curret_approvers_bl'=>'<b>Aprovadores Atuais</b>',

/* './packages/policy/popup_approvers_view.xml' */

'tt_approvers_visualization'=>'Visualização de Aprovadores',

/* './packages/policy/popup_associate_users.xml' */

'gc_login'=>'Login',
'lb_login_cl'=>'Login:',
'tt_current_users_bl'=>'<b>Usuários Atuais</b>',
'tt_users_search'=>'Busca de Usuários',

/* './packages/policy/popup_create_document_confirm.xml' */

'rb_creation_document_question'=>'Criar um documento no sistema e utilizá-lo como modelo para o registro <b>%register_name%</b> ?',
'rb_document_do_nothing'=>'Não criar / associar documento para o registro <b>%register_name%</b> ?',
'rb_document_relation_question'=>'Associar um documento existente no sistema e utilizá-lo como modelo para o registro <b>%register_name%</b>?',
'st_what_action_you_wish_to_do'=>'Que ação deseja realizar?',
'tt_document_creation_or_association'=>'Adição / Associação de Documento',
'vb_ok'=>'Ok',

/* './packages/policy/popup_document_author_edit.xml' */

'tt_author_editing'=>'Edição de Autor',

/* './packages/policy/popup_document_context_associate.php' */

'st_permission_to_associate_item_document_denied'=>'Você não tem permissão para associar um elemento a um documento do tipo "Gestão" ou "Outros"',

/* './packages/policy/popup_document_context_associate.xml' */

'tt_current_items_bl'=>'<b>Elementos Atuais</b>',
'tt_risk_management_items_association'=>'Associação de Elementos da Gestão de Aspectos e Impactos',

/* './packages/policy/popup_document_context_view.xml' */

'tt_associated_risk_management_items_visualization'=>'Visualização de Elementos Associados da Gestão de Aspectos e Impactos',

/* './packages/policy/popup_document_copy_readers.xml' */

'tt_documents_search'=>'Busca de Documento',
'vb_copy_readers'=>'Copiar Leitores',

/* './packages/policy/popup_document_create.xml' */

'lb_document_name_bl_cl'=>'<b>Nome do Documento:</b>',
'tt_new_document'=>'Novo Documento',

/* './packages/policy/popup_document_edit.php' */

'si_management'=>'Gestão',
'si_no_type'=>'Sem Tipo',
'si_others'=>'Outros',
'si_register'=>'Registro',
'st_creation_date'=>'Data de Criação:',
'st_document_approval_confirm'=>'Deseja enviar o documento para aprovação?',
'st_download_file'=>'Fazer download do arquivo:',
'st_publish_document_confirm'=>'Deseja publicar o documento?',
'st_publish_document_on_date_confirm'=>'Deseja publicar o documento no dia %date%?',
'st_size'=>'Tamanho:',
'to_max_file_size'=>'Tamanho máximo de arquivo: %max_file_size%',
'tt_document_adding'=>'Adição de Documento',
'tt_document_editing'=>'Edição de Documento',
'tt_publish'=>'Publicar',
'tt_send_for_approval'=>'Enviar para aprovação',

/* './packages/policy/popup_document_edit.xml' */

'lb_author_bl_cl'=>'<b>Autor:</b>',
'lb_deadline_bl_cl'=>'<b>Deadline:</b><br/><br/>Data limite para aprovação do documento.',
'lb_inform_in_advance_of'=>'Avisar com antecedência de',
'lb_publication_date_bl_cl'=>'<b>Data de Publicação:</b>',
'lb_revision_cl'=>'Revisão:',
'si_disable'=>'Desabilitar',
'si_enable'=>'Habilitar',
'si_file'=>'Arquivo',
'si_link'=>'Link',
'st_days'=>'dias.',
'st_general_information'=>'Informações Gerais',
'st_management_information'=>'Informações Gerenciais',
'st_test'=>'teste',
'to_doc_instance_manual_version'=>'<b>Versão Manual:</b><br/>A versão manual só terá efeito quando o documento tiver um arquivo ou um link associado.',
'to_document_inform_in_advance'=>'<b>Avisar com antecedência:</b><br/><br/>É o número de dias, antes do deadline ou da revisão, em que deve ser gerado um aviso de alerta ou tarefa.',
'to_document_type_modification'=>'Para modificar o tipo do documento, deve-se remover os relacionamentos entre o documento e os elementos da Gestão de Aspectos e Impactos.',
'wn_deadline_date_must_be_less_date_publication'=>'A data de deadline deve ser menor ou igual a data de publicação.',
'wn_doc_exists_error'=>'Já existe um documento com mesmo nome no sistema.',
'wn_doc_exists_trash_error'=>'Já existe um documento com mesmo nome na lixeira do sistema.',
'wn_max_file_size_exceeded'=>'Tamanho máximo de arquivo excedido.',
'wn_publication_date_after_today'=>'A data para publicação deve ser maior ou igual a data atual.',
'wn_upload_error'=>'Erro ao tentar enviar arquivo.',

/* './packages/policy/popup_document_instances.php' */

'st_copy_previous_version_confirm'=>'Tem certeza de que deseja copiar a versão anterior?',
'tt_copy_previous_version'=>'Copiar versão anterior',

/* './packages/policy/popup_document_instances.xml' */

'gc_end_date'=>'Data de Fim',
'gc_modification_comment'=>'Comentário de Modificação',
'gc_revision_justification'=>'Justificativa de Revisão',
'gc_start_date'=>'Data de Início',
'gc_version'=>'Versão',
'tt_previous_versions'=>'Versões Anteriores',
'wn_no_version_selected'=>'Nenhuma versão selecionada.',

/* './packages/policy/popup_document_main_approver_edit.xml' */

'lb_approver_bl_cl'=>'<b>Responsável:</b>',
'tt_approver_editing'=>'Edição de Responsável',

/* './packages/policy/popup_document_manage.xml' */

'st_approver_list_only_when_developing'=>'A lista de aprovadores só pode ser modificada quando o documento está em desenvolvimento.',
'st_items_association_not_management_or_others'=>'Só é possível modificar as associações de um documento que não seja do tipo \'Gestão\' ou \'Outros\'.',
'st_items_association_only_when_developing'=>'Só é possível modificar as associações de um documento que esteja em desenvolvimento.',
'st_readers_list_only_when_developing'=>'A lista de leitores só pode ser modificada quando o documento está em desenvolvimento.',
'st_revision_for_published_document_only'=>'Só um documento publicado pode ser enviado para revisão.',
'tt_associate_items_bl'=>'<b>Associar Elementos</b>',
'tt_edit_approvers_bl'=>'<b>Editar Aprovadores</b>',
'tt_edit_readers_bl'=>'<b>Editar Leitores</b>',
'tt_manage_document'=>'Gerenciar Documento',
'tt_send_for_revision_bl'=>'<b>Enviar para Revisão</b>',
'tt_view_approvers_bl'=>'<b>Visualizar Aprovadores</b>',
'tt_view_items_bl'=>'<b>Visualizar Elementos</b>',
'tt_view_readers_bl'=>'<b>Visualizar Leitores</b>',
'vb_copy'=>'Copiar',
'vb_send'=>'Enviar',

/* './packages/policy/popup_document_read.php' */

'st_remove_comment_confirm'=>'Você tem certeza de que deseja excluir o comentário referido?',
'tt_approve_document'=>'Aprovar Documento',
'tt_remove_comment'=>'Remover Comentário',

/* './packages/policy/popup_document_read.xml' */

'ti_comments'=>'Comentários',
'ti_general'=>'Geral',
'ti_references'=>'Referências',
'ti_sub_documents'=>'Subdocumentos',
'tt_document_reading'=>'Leitura de Documento',
'vb_approve'=>'Aprovar',
'vb_deny'=>'Negar',

/* './packages/policy/popup_document_read_comments.xml' */

'gc_author'=>'Autor',
'gc_comment'=>'Comentário',
'gc_date'=>'Data',
'tt_coments'=>'Comentários',
'tt_comments_bl'=>'<b>Comentários</b>',

/* './packages/policy/popup_document_read_details.xml' */

'lb_date_cl'=>'Data:',
'lb_keywords_cl'=>'Palavras-chave:',
'lb_size_cl'=>'Tamanho:',
'lb_version_cl'=>'Versão:',
'tt_details'=>'Detalhes',

/* './packages/policy/popup_document_read_subdocuments.xml' */

'tt_sub_documents'=>'Subdocumentos',

/* './packages/policy/popup_document_reference.php' */

'st_remove_reference_confirm'=>'Você tem certeza de que deseja remover a referência <b>%doc_reference_name%</b>?',
'tt_remove_reference'=>'Remover Referência',

/* './packages/policy/popup_document_reference.xml' */

'gc_creation_date'=>'Data de criação',
'gc_link'=>'Link',
'lb_link_bl_cl'=>'<b>Link:</b>',
'st_http'=>'http://',
'tt_references'=>'Referências',
'tt_references_bl'=>'<b>Referências</b>',

/* './packages/policy/popup_document_register.xml' */

'tt_associated_registers_bl'=>'<b>Registros Associados</b>',
'tt_registers'=>'Registros',
'vb_disassociate'=>'Desassociar',

/* './packages/policy/popup_document_register_associate.xml' */

'gc_classification'=>'Classificação',
'gc_place'=>'Local',
'gc_time'=>'Tempo',
'tt_register_association'=>'Associação de Registro',
'tt_registers_bl'=>'<b>Registros</b>',
'vb_associate_registers'=>'Associar Registros',
'wn_no_register_selected'=>'Nenhum registro selecionado.',
'wn_register_same_name_error'=>'Já existe um registro com nome %register_name% associado.',

/* './packages/policy/popup_document_register_edit.php' */

'tt_register_adding'=>'Adição de Registro',
'tt_register_editing'=>'Edição de Registro',
'vb_create'=>'Criar',
'vb_edit'=>'Editar',

/* './packages/policy/popup_document_register_edit.xml' */

'lb_disposition_cl'=>'Disposição:',
'lb_indexing_type_cl'=>'Forma de Indexação:',
'lb_origin_cl'=>'Origem:',
'lb_protection_requirements_cl'=>'Requisitos de Proteção:',
'si_day_days'=>'dia(s)',
'si_month_months'=>'mês(es)',
'si_week_weeks'=>'semana(s)',
'vb_lookup'=>'Buscar',
'wn_register_exists_error'=>'Já existe um registro com mesmo nome no sistema.',
'wn_register_exists_trash_error'=>'Já existe um registro com mesmo nome na lixeira do sistema.',
'wn_retention_time'=>'O tempo de retenção deve ser maior que zero',

/* './packages/policy/popup_document_register_read.xml' */

'lb_responsible'=>'Responsável',
'lb_retention_time_cl'=>'<b>Tempo de Retenção:</b>',
'lb_storage_place_cl'=>'Local de Armazenamento:',
'lb_storage_type_cl'=>'Forma de Armazenamento:',
'tt_register_reading'=>'Leitura de Registro',
'vb_preferences'=>'Preferências',

/* './packages/policy/popup_document_revise_not_alter.xml' */

'tt_revise_document_without_altering'=>'Revisar Documento sem Alterar',

/* './packages/policy/popup_document_revision_task.php' */

'mx_scheduled_revision'=>'Revisão agendada.',

/* './packages/policy/popup_document_revision_task.xml' */

'lb_author_cl'=>'Autor:',
'lb_file_cl'=>'Arquivo:',
'lb_link_cl'=>'Link:',
'tt_document_revision'=>'Revisão do Documento',
'vb_create_new_version'=>'Criar nova versão',
'vb_download'=>'Download',
'vb_revise_without_altering'=>'Revisar sem Alterar',
'vb_visit_link'=>'Visitar Link',
'vb_visualize'=>'Visualizar',

/* './packages/policy/popup_document_search.xml' */

'tt_document_search'=>'Busca de Documento',
'wn_no_document_selected'=>'Nenhum documento selecionado.',

/* './packages/policy/popup_documents_search.xml' */

'tt_current_documents_bl'=>'<b>Documentos Atuais</b>',

/* './packages/policy/popup_modification_comment.php' */

'tt_popup_modification_comment_creation'=>'Comentário de Criação',
'tt_popup_modification_comment_creationfasdfawfqwerqwe'=>'Comentário de Criação',

/* './packages/policy/popup_modification_comment.xml' */

'lb_comment_bl_cl'=>'<b>Comentário:</b>',
'tt_comment_modification'=>'Comentar Modificação',
'tt_comment_modification2'=>'Comentar Modificação',

/* './packages/policy/popup_readers_edit.xml' */

'si_all_processes'=>'Todos Processos',
'tt_current_readers_bl'=>'<b>Leitores Atuais</b>',
'tt_readers_editing'=>'Edição de Leitores',

/* './packages/policy/popup_readers_view.xml' */

'tt_readers_visualization'=>'Visualização de Leitores',

/* './packages/policy/popup_register_readers_edit.xml' */

'tt_current_readers'=>'<b>Leitores Atuais</b>',
'tt_readers_edit'=>'Edição de Leitores',

/* './packages/policy/popup_revision_justification.xml' */

'tt_revision_justification'=>'Justificativa de Revisão',
'vb_send_for_revision'=>'Enviar para revisão',

/* './packages/policy/popup_subdocuments_management.php' */

'st_create_subdocument'=>'Criar Subdocumento',

/* './packages/policy/popup_subdocuments_management.xml' */

'tt_manage_subdocuments'=>'Gerenciar Subdocumentos',

/* './packages/policy/popup_template_search.xml' */

'rb_suggested_by_best_practice_only'=>'mostrar somente templates de requisitos legais sugeridos',
'rb_suggested_only'=>'Somente Sugeridos',
'tt_document_templates_search'=>'Busca de Modelos de Documentos',

/* './packages/policy/report/popup_report_accessed_documents.php' */

'rs_accessed_documents'=>'Auditoria de Acesso dos Usuários',
'rs_management'=>'Gestão',
'rs_others'=>'Outros',

/* './packages/policy/report/popup_report_contexts_without_documents.php' */

'rs_items_with_without_documents'=>'Componentes sem Documentos',

/* './packages/policy/report/popup_report_doc_instance_comment_and_justification.php' */

'mx_doc_instance_comment_and_justification'=>'Comentários e Justificativas de Documentos',

/* './packages/policy/report/popup_report_doc_instance_comment_and_justification.xml' */

'rs_header_instance_modify_comment'=>'Comentário de Modificação',
'rs_header_instance_rev_justification'=>'Justificativa de Revisão',

/* './packages/policy/report/popup_report_docs_with_high_frequency_revision.php' */

'tt_docs_with_high_frequency_revision'=>'Documentos com uma alta frequência de revisão',

/* './packages/policy/report/popup_report_document_accesses.php' */

'rs_accesses_to_documents'=>'Auditoria de Acesso aos Documentos',
'rs_no_type'=>'Sem tipo',

/* './packages/policy/report/popup_report_document_accesses.xml' */

'rs_time'=>'Hora',

/* './packages/policy/report/popup_report_document_approvers.php' */

'rs_approvers'=>'Documentos e seus Aprovadores',

/* './packages/policy/report/popup_report_document_dates.php' */

'rs_documents_dates'=>'Data de Criação e Revisão dos Documentos',

/* './packages/policy/report/popup_report_document_dates.xml' */

'rs_created_on_cl'=>'Criado em:',

/* './packages/policy/report/popup_report_document_readers.php' */

'rs_readers'=>'Documentos e seus Leitores',

/* './packages/policy/report/popup_report_document_readers.xml' */

'rs_author_cl'=>'Elaborador:',

/* './packages/policy/report/popup_report_document_registers.php' */

'rs_registers_per_document'=>'Registro por Documento',

/* './packages/policy/report/popup_report_document_registers.xml' */

'rs_name'=>'Nome',
'rs_retention_time'=>'Tempo de Retenção',

/* './packages/policy/report/popup_report_document_summary.php' */

'mx_report_document_summary'=>'Propriedades do Documento',

/* './packages/policy/report/popup_report_documents_by_area.php' */

'rs_documents_per_area'=>'Documentos por Área',

/* './packages/policy/report/popup_report_documents_by_area.xml' */

'rs_document_documents'=>'documento(s)',

/* './packages/policy/report/popup_report_documents_by_component.php' */

'rs_document_of_component'=>'Documentos de %component%',
'tt_documents_by_component'=>'Documentos por Componente do SGSI',

/* './packages/policy/report/popup_report_documents_by_state.php' */

'tt_documents_by_state'=>'Documentos',

/* './packages/policy/report/popup_report_documents_by_type.php' */

'rs_documents_by_type'=>'Documentos por Tipo',

/* './packages/policy/report/popup_report_documents_revision_late.php' */

'mx_documents_revision_late'=>'Documentos com Revisão Atrasada',

/* './packages/policy/report/popup_report_documents_revision_late.xml' */

'rs_document_next_revision'=>'Próxima Revisão',

/* './packages/policy/report/popup_report_documents_revision_schedule.php' */

'mx_documents_revision_schedule'=>'Cronograma de Revisão de Documentos',

/* './packages/policy/report/popup_report_documents_revision_schedule.xml' */

'rs_document_name'=>'Nome',
'rs_document_schedule'=>'Agendamento',

/* './packages/policy/report/popup_report_documents_without_register.php' */

'rs_document_main_approver_name'=>'Responsável pelo documento',
'rs_document_name_bl'=>'Documento',
'tt_documents_without_register'=>'Documentos sem Registro',

/* './packages/policy/report/popup_report_not_accessed_documents.php' */

'rs_not_accessed_documents'=>'Documentos Não Acessados',

/* './packages/policy/report/popup_report_pendant_approvals.php' */

'tt_pendant_approvals'=>'Aprovações Pendentes',

/* './packages/policy/report/popup_report_registers_by_type.php' */

'rs_registers_by_type'=>'Registros por Tipo',

/* './packages/policy/report/popup_report_top_accessed_documents.php' */

'rs_most_accessed_documents'=>'Documentos Mais Acessados',

/* './packages/policy/report/popup_report_top_accessed_documents.xml' */

'rs_accesses_cl'=>'Acessos:',

/* './packages/policy/report/popup_report_top_revised_documents.php' */

'rs_most_revised_documents'=>'Documentos Mais Revisados',

/* './packages/policy/report/popup_report_top_revised_documents.xml' */

'rs_revisions_cl'=>'Revisões:',

/* './packages/policy/report/popup_report_user_comments.php' */

'mx_user_comments'=>'Comentários de Usuários',

/* './packages/policy/report/popup_report_user_comments.xml' */

'rs_comment_date'=>'Data',
'rs_comment_text'=>'Comentário',
'rs_comment_user'=>'Usuário',

/* './packages/policy/report/popup_report_users_by_process.php' */

'rs_users_per_process'=>'Usuários associados aos Processos',

/* './packages/policy/report/popup_report_users_by_process_and_area.xml' */

'rs_user_bl_cl'=>'<b>Usuário:</b>',

/* './packages/policy/report/popup_report_users_with_pendant_read.php' */

'mx_documents_pendent_reads'=>'Documentos com Leitura Pendente',

/* './packages/policy/tab_policy_management.xml' */

'ti_all'=>'Todos',
'ti_developing'=>'Em Desenvolvimento',
'ti_obsolete'=>'Obsoletos',
'ti_publish'=>'Publicados',
'ti_registers'=>'Registros',
'ti_revision'=>'Em Revisão',
'ti_to_approve'=>'Em Aprovação',
'ti_to_be_published'=>'A Publicar',

/* './packages/risk/nav_area.php' */

'st_area_remove_confirm'=>'Você tem certeza de que deseja remover a área <b>%area_name%</b>?',
'tt_remove_area'=>'Remover Área',

/* './packages/risk/nav_area.xml' */

'gc_subarea'=>'Sub-Áreas',
'lb_sub_areas_count_cl'=>'Número de Sub-Áreas:',
'mi_processes'=>'Processos',
'mi_sub_areas'=>'Sub-Áreas',
'tt_business_areas_bl'=>'<b>Áreas de Negócio</b>',

/* './packages/risk/nav_asset.php' */

'grid_asset_filter_dependents'=>'(dependências do A.P.S. \'<b>%asset_name%</b>\')',
'st_asset_copy'=>'Cópia',
'st_asset_remove_confirm'=>'Você tem certeza que deseja remove o A.P.S. <b>%asset_name%</b>?',
'tt_remove_asset'=>'Remover A.P.S.',

/* './packages/risk/nav_asset.xml' */

'gc_asset_category'=>'Categoria do A.P.S.',
'gc_asset_dependencies'=>'Dep.(as)',
'gc_asset_dependents'=>'Dep.(es)',
'gc_processes'=>'Processos',
'lb_process_count_cl'=>'Número de Processos:',
'mi_associate_processes'=>'Associar Processos',
'mi_duplicate'=>'Duplicar',
'mi_edit_dependencies'=>'Ajustar Dependências',
'mi_edit_dependents'=>'Ajustar Dependentes',
'mi_insert_risk'=>'Inserir Risco',
'mi_load_risks_from_library'=>'Carregar riscos da biblioteca',
'tt_assets_bl'=>'<b>A.P.S.</b>',

/* './packages/risk/nav_control.php' */

'st_control_remove_confirm'=>'Você tem certeza de que deseja remover o controle <b>%control_name%</b>?',

/* './packages/risk/nav_control.xml' */

'gc_cost'=>'Custo',
'gc_icon'=>'Ícone',
'gc_risks'=>'Riscos',
'lb_risk_count_cl'=>'Número de Riscos:',
'mi_associate_risks'=>'Associar Riscos',
'mi_view_risks'=>'Ver Riscos',
'rb_active'=>'Ativados',
'rb_not_active'=>'Desativados',
'tt_controls_directory_bl'=>'<b>Diretório de Controles</b>',
'vb_revision_and_test_history'=>'Histórico de Revisão/Teste',

/* './packages/risk/nav_process.php' */

'grid_process_asset_filter'=>'(filtrados pelo A.P.S. \'<b>%asset_name%</b>\')',
'st_process_remove_confirm'=>'Você tem certeza que deseja remover o processo <b>%process_name%</b>?',
'tt_remove_process'=>'Remover Processo',

/* './packages/risk/nav_process.xml' */

'cb_not_estimated'=>'Não Estimado',
'gc_assets'=>'A.P.S.',
'gc_responsible'=>'Responsável',
'gc_users'=>'Usuários',
'lb_asset_count_cl'=>'Número de A.P.S.:',
'lb_responsible_cl'=>'Responsável:',
'lb_value_cl'=>'Risco:',
'mi_add_document'=>'Criar Documento',
'mi_associate_assets'=>'Associar A.P.S.',
'mi_documents'=>'Documentos',
'mi_view_assets'=>'Ver A.P.S.',
'si_all_priorities'=>'Todas',
'si_all_responsibles'=>'Todos',
'si_all_types'=>'Todos',
'tt_processes_bl'=>'<b>Processos</b>',

/* './packages/risk/nav_report.php' */

'mx_no_priority'=>'Sem Prioridade',
'mx_no_type'=>'Sem Tipo',
'si_amount_of_risks_per_area'=>'Quantidade de Riscos por Área',
'si_amount_of_risks_per_process'=>'Quantidade de Riscos por Processo',
'si_areas_without_processes'=>'Áreas sem Processos',
'si_assets'=>'A.P.S.',
'si_assets_importance'=>'Importância dos A.P.S.',
'si_assets_without_risks'=>'A.P.S. sem Eventos de Risco',
'si_checklist_per_event'=>'Checklist por Evento',
'si_controls_efficiency'=>'Eficácia dos Controles',
'si_controls_implementation_date'=>'Data de Implementação dos Controles',
'si_controls_per_responsible'=>'Controles por Responsável',
'si_controls_per_risk'=>'Controles por Risco',
'si_controls_without_risks'=>'Controles sem Riscos',
'si_cost_of_controls_per_area'=>'Custo dos Controles por Área',
'si_cost_of_controls_per_asset'=>'Custo dos Controles por A.P.S.',
'si_cost_of_controls_per_process'=>'Custo dos Controles por Processo',
'si_cost_of_controls_per_responsible'=>'Custo dos Controles por Responsável',
'si_cost_per_control'=>'Custo por Controle',
'si_duplicated_risks'=>'Riscos Duplicados',
'si_element_classification'=>'Classificação dos Elementos',
'si_follow_up_of_controls'=>'Acompanhamento de Controles',
'si_follow_up_of_controls_efficiency'=>'Acompanhamento da Eficácia dos Controles',
'si_follow_up_of_controls_test'=>'Acompanhamento do Teste dos Controles',
'si_non_parameterized_risks'=>'Riscos não Estimados',
'si_not_measured_controls'=>'Controles não Medidos',
'si_pending_items'=>'Elementos Pendentes',
'si_pending_tasks'=>'Tarefas Pendentes',
'si_plan_of_risks_treatment'=>'Plano de Tratamento dos Riscos',
'si_planning_of_controls'=>'Planejamento dos Controles',
'si_processes_without_assets'=>'Processos sem A.P.S.',
'si_report_conformity'=>'Conformidade',
'si_risk_impact'=>'Impacto do Risco',
'si_risk_status_per_area'=>'Status de Risco por Área',
'si_risk_status_per_process'=>'Status de Risco por Processo',
'si_risks_per_area'=>'Riscos por Área',
'si_risks_per_asset'=>'Riscos por A.P.S.',
'si_risks_per_control'=>'Riscos por Controle',
'si_risks_per_process'=>'Riscos por Processo',
'si_risks_values'=>'Valores dos Riscos',
'si_sgsi_policy_statement'=>'Declaração de Política do SGSI',
'si_sgsi_scope_statement'=>'Declaração de Escopo do SGSI',
'si_statement_of_applicability'=>'Declaração de Aplicabilidade',
'si_summary_of_controls'=>'Resumo de Controles',
'si_top_10_assets_with_highest_risks'=>'Top 10 A.P.S. com Mais Riscos Altos',
'si_top_10_assets_with_most_risks'=>'Top 10 A.P.S. com Mais Riscos',
'si_top_10_risks_per_area'=>'Top 10 Riscos por Área',
'si_top_10_risks_per_process'=>'Top 10 Riscos por Processo',
'si_users_responsibilities'=>'Responsabilidades dos Usuários',
'st_all_standards'=>'Todas',
'st_clear_justificatives_confirm'=>'Você tem certeza que deseja limpar as justificativas?',
'tt_clear_justificatives'=>'Confirmar limpeza de justificativas',

/* './packages/risk/nav_report.xml' */

'cb_approved'=>'Aprovado',
'cb_asset_security'=>'Segurança do A.P.S.',
'cb_denied'=>'Negado',
'cb_organize_by_asset'=>'Organizar por A.P.S.',
'cb_organize_by_process'=>'Organizar por Processo',
'cb_pendant'=>'Pendente',
'lb_comment_cl'=>'Comentário:',
'lb_consider_cl'=>'Considerar:',
'lb_filter_by_cl'=>'Filtrar por:',
'lb_filter_by_standard_cl'=>'Filtrar por Legislação:',
'lb_filter_cl'=>'Filtrar:',
'lb_process_cl'=>'Processo:',
'lb_process_colon'=>'Atividade, Produto e Serviços:',
'lb_process_status_cl'=>'Status de A. P. S.:',
'lb_reponsible_for_cl'=>'Responsável por:',
'lb_risk_status_cl'=>'Status do Risco:',
'lb_values_cl'=>'Valores:',
'rb_area'=>'Área',
'rb_area_process'=>'Área e Processo',
'rb_asset'=>'A.P.S.',
'rb_both'=>'Ambos',
'rb_detailed'=>'Detalhado',
'rb_efficient'=>'Eficientes',
'rb_implemented'=>'Implementados',
'rb_inefficient'=>'Ineficientes',
'rb_planned'=>'Planejados',
'rb_process'=>'Processo',
'rb_real'=>'Real',
'rb_summarized'=>'Resumido',
'si_all_reports'=>'Todos Relatórios',
'si_areas_by_priority'=>'Áreas por Prioridade',
'si_areas_by_type'=>'Áreas por Tipo',
'si_control_reports'=>'Relatórios de Controles',
'si_controls_by_type'=>'Controles por Tipo',
'si_events_by_type'=>'Eventos por Tipo',
'si_financial_reports'=>'Relatórios Financeiros',
'si_iso_27001_reports'=>'Relatórios ISO 27001',
'si_processes_by_priority'=>'Processos por Prioridade',
'si_processes_by_type'=>'Processos por Tipo',
'si_risk_management_reports'=>'Relatórios de Gestão de Aspectos e Impactos',
'si_risk_reports'=>'Relatórios de Riscos',
'si_risks_by_type'=>'Riscos por Tipo',
'si_summaries'=>'Sumários',
'st_area_priority'=>'Prioridades de Área',
'st_area_type'=>'Tipos de Área',
'st_asset_cl'=>'A.P.S.:',
'st_config_filter_type_priority'=>'Configurar Filtro Selecionado:',
'st_control_type'=>'Tipos de Controle',
'st_event_type'=>'Tipos de Evento',
'st_no_filter_available'=>'Nenhum filtro disponível.',
'st_none_selected'=>'Selecione um Filtro',
'st_organize_by_asset'=>'Organizar por A.P.S.',
'st_process_priority'=>'Prioridades de Processo',
'st_process_type'=>'Tipos de Processo',
'st_risk_type'=>'Tipos de Risco',
'st_show_cl'=>'Exibir:',
'st_standard_cl'=>'Legislação:',
'tt_specific_report_filters'=>'Filtros específicos do relatório',
'tt_type_and_priority_filter_cl'=>'Filtros de Tipo e Prioridade:',
'vb_clear_pre_report'=>'Limpar Justificativas',
'vb_pre_report'=>'Pré-Relatório',
'vb_report'=>'Relatório',
'wn_no_report_selected'=>'Nenhum relatório selecionado.',
'wn_select_at_least_one_filter'=>'Pelo menos um filtro deve ser selecionado!',
'wn_select_at_least_one_parameter'=>'Pelo menos um parâmetro deve ser selecionado.',
'wn_select_risk_value'=>'Por favor, selecione algum valor de risco para ser exibido.',

/* './packages/risk/nav_risk.php' */

'gs_avoided'=>'Evitado',
'gs_reduced'=>'Reduzido',
'gs_restrained'=>'Retido',
'gs_transferred'=>'Transferido',
'st_denied_permission_to_edit'=>'Você não tem permissão para editar o(a)',
'st_risk_remove_confirm'=>'Você tem certeza que deseja remover o risco <b>%risk_name%</b>?',
'st_risk_remove_has_incident'=>'<br><br><b>Atenção: O risco possui acidentes(s) associados(s).</b>',
'st_risk_remove_many_confirm'=>'Você tem certeza que deseja remover os <b>%count%</b> riscos selecionados?',
'st_risk_remove_many_have_incident'=>'<br><br><b>Atenção: Pelo menos um risco possui acidentes(s) associados(s).</b>',
'tt_incident_filter'=>'filtrados pelo acidente \'<b>%incident_name%</b>\'',
'tt_remove_risk'=>'Remover Risco',

/* './packages/risk/nav_risk.xml' */

'cb_avoided'=>'Evitado',
'cb_not_treated'=>'Não Tratado',
'cb_reduced'=>'Reduzido',
'cb_restrained'=>'Retido',
'cb_search_in_name_only'=>'pesquisar somente no nome',
'cb_transferred'=>'Transferido',
'gc_controls'=>'Controles',
'gc_residual'=>'Residual',
'gc_risk'=>'Risco',
'gc_risk_name'=>'Aspecto',
'gc_status'=>'Status',
'gc_treatment'=>'Tratamento',
'lb_control_count_cl'=>'Número de Controles:',
'lb_name_description_cl'=>'Nome/Descrição:',
'lb_risk_residual_value_cl'=>'Residual:',
'lb_risk_value_cl'=>'Risco:',
'lb_treatment_type_cl'=>'Tipo de Tratamento:',
'mi_avoid'=>'Evitar Risco',
'mi_cancel_treatment'=>'Cancelar Tratamento',
'mi_hold_accept'=>'Reter/Aceitar Risco',
'mi_reduce_risk'=>'Reduzir Risco',
'mi_transfer'=>'Transferir Risco',
'mi_view_controls'=>'Ver Controles',
'si_all_assets'=>'Todos',
'si_all_states'=>'Todos',
'st_from'=>'de',
'st_to'=>'a',
'vb_aply'=>'Aplicar',
'vb_parameterize'=>'Estimar',

/* './packages/risk/popup_area_edit.php' */

'tt_area_adding'=>'Adição de Área',
'tt_area_editing'=>'Edição de Área',

/* './packages/risk/popup_asset_dependencies.php' */

'st_dependencies_list'=>'Lista de Dependências',
'st_dependents_list'=>'Lista de Dependentes',
'tt_dependencies_adjustment'=>'Ajuste de Dependências',
'tt_dependents_adjustment'=>'Ajuste de Dependentes',

/* './packages/risk/popup_asset_dependencies.xml' */

'tt_assets_list'=>'Lista de A.P.S.',

/* './packages/risk/popup_asset_edit.php' */

'lb_value_cl_money_symbol'=>'Valor',
'tt_asset_adding'=>'Adição de A.P.S.',
'tt_asset_duplicate'=>'Duplicação de A.P.S.',
'tt_asset_editing'=>'Edição de A.P.S.',

/* './packages/risk/popup_asset_edit.xml' */

'lb_category_bl_cl'=>'Categoria',
'lb_demands_attention_to_legal_conformity_cl'=>'Requer atenção quanto a Conformidade Legal:',
'lb_justification_cl'=>'Justificativa:',
'lb_security_responsible_bl_cl'=>'<b>Responsável pela Segurança:</b>',
'st_asset_parametrization_bl'=>'<b>Relevância do A.P.S.</b>',
'vb_adjust_dependencies'=>'Ajustar Dependências',
'vb_adjust_dependents'=>'Ajustar Dependentes',
'wn_responsible_invalid'=>'Usuário responsável inválido.',
'wn_security_responsible_invalid'=>'Usuário responsável pela segurança inválido.',

/* './packages/risk/popup_asset_search.xml' */

'wn_no_asset_selected'=>'Nenhum A.P.S. selecionado.',

/* './packages/risk/popup_best_practice_search.xml' */

'lb_section_cl'=>'Seção:',
'si_all'=>'Todos',
'tt_best_practices_search'=>'Busca de Requisitos Legais',

/* './packages/risk/popup_control_cost.php' */

'vb_save'=>'Salvar',

/* './packages/risk/popup_control_cost.xml' */

'tt_control_cost'=>'Custo do Controle',

/* './packages/risk/popup_control_edit.php' */

'st_and'=>'e',
'st_of_revision'=>'da revisão',
'st_of_test'=>'do teste',
'st_remove_control_implementation_confirm'=>'Você tem certeza de que deseja remover a implementação do controle <b>%control_name%</b>?',
'st_result_in_removing'=>'Isto resultará na remoção',
'st_this_control'=>'deste controle',
'tt_control_adding'=>'Adição de Controle',
'tt_remove_control_implementation'=>'Remover a impementação do controle',

/* './packages/risk/popup_control_edit.xml' */

'cb_enable_control_revision_cl'=>'Habilitar revisão do controle:',
'cb_enable_control_tests_cl'=>'Habilitar testes do controle:',
'lb_best_practices_bl_cl'=>'<b>Requisitos Legais:</b>',
'lb_control_cost_cl'=>'Custo do Controle:',
'lb_evidence_cl'=>'Evidência:',
'lb_implementation_limit_bl_cl'=>'<b>Limite de Implementação:</b>',
'lb_implemented_on_cl'=>'Implementado em:',
'lb_send_alert_task_bl_cl'=>'<b>Enviar Alerta / Tarefa:</b>',
'st_day_days_before'=>'dia(s) antes',
'st_money_symbol'=>'$',
'tt_control_editing'=>'Edição de Controle',
'vb_add'=>'Adicionar',
'vb_control_cost'=>'Custo do Controle',
'vb_implement'=>'Implementar',
'vb_remove_implementation'=>'Remover Implementação',
'vb_revision'=>'Revisão',
'vb_tests'=>'Testes',
'wn_implementation_cannot_after_today'=>'A data de implementação não pode ser maior que a data atual.',
'wn_implementation_cannot_be_null'=>'A data de implementação não pode ser nula, caso você deseja remover a implementação, utilize o botão apropriado para executar esse evento.',
'wn_implementation_limit_after_today'=>'O limite de implementação deve ser maior ou igual a data atual.',

/* './packages/risk/popup_control_revision.xml' */

'lb_expected_efficiency_bl_cl'=>'<b>Eficácia Esperada:</b>',
'lb_high_value_5_cl'=>'(Alta) Valor 5:',
'lb_low_value_1_cl'=>'(Baixa) Valor 1:',
'st_helper'=>'[?]',
'to_control_revision_expected_efficiency'=>'<b>Eficácia Esperada:</b><br/><br/>Determinar qual o nível de serviço esperado para o controle no período especificado.<br/><br/>Considerar o desempenho necessário para reduzir os riscos associados a este controle.',
'to_control_revision_metric_help'=>'<b>Métrica:</b><br/><br/>Determinar qual a métrica de medição do controle, ou seja, de que maneira o controle será medido.',
'to_control_revision_value'=>'<b>Valores:</b><br/><br/>Determinar uma escala para possíveis níveis de serviço deste controle no período.<br/><br/>Considerar como valores mais baixos um desempenho ruim, e para os valores mais altos o melhor desempenho possível para o controle.',
'tt_control_efficiency_revision'=>'Revisão da eficácia do controle',

/* './packages/risk/popup_control_revision_and_test_edit.php' */

'st_control_test_not_ok'=>'Não Ok',
'st_control_test_ok'=>'Ok',
'st_revision_remove_confirm'=>'Você tem certeza de que deseja remover a revisão com Data Prevista em <b>%deadline_date%</b> e Data de Realização em <b>%realized_date%</b> ?',
'st_revision_update_by_insert_confirm'=>'Já existe uma revisão de controle na Data Prevista em <b>%date_to_do%</b>. Você tem certeza que deseja substituir essa revisão do controle ?',
'st_test_remove_confirm'=>'Você tem certeza de que deseja remover o teste com Data Prevista em <b>%deadline_date%</b> e Data de Realização em <b>%realized_date%</b> ?',
'st_test_update_by_insert_confirm'=>'Já existe um teste de controle na Data Prevista em <b>%date_to_do%</b>. Você tem certeza que deseja substituir esse teste do controle ?',
'tt_control_efficiency_history_add_title'=>'Inserir Revisão do Controle',
'tt_control_test_history_add_title'=>'Inserir Teste do Controle',
'tt_remove_control'=>'Remover Teste do Controle',
'tt_replace_control_revision'=>'Substituição de Revisão do Controle <b>%control_name%</b>',
'tt_replace_control_test'=>'Substituição de Teste do Controle <b>%control_name%</b>',

/* './packages/risk/popup_control_revision_and_test_edit.xml' */

'gc_date_realized'=>'Data Realização',
'gc_date_to_do'=>'Data Prevista',
'gc_expected_efficiency'=>'EE',
'gc_real_efficiency'=>'ER',
'gc_realization_date'=>'Data Realização',
'gc_test_date_to_do'=>'Data Prevista',
'gc_test_value'=>'Valor do Teste',
'mi_edit'=>'Editar',
'mi_remove'=>'Remover',
'mi_revision_edit'=>'Editar Revisões',
'mi_test_edit'=>'Editar Testes',
'rb_not_ok'=>'Não Ok',
'rb_ok'=>'Ok',
'st_control_test_result'=>'<b>Teste:</b>',
'st_date_realized'=>'<b>Data Realização:</b>',
'st_date_to_do'=>'<b>Data Prevista:</b>',
'st_description'=>'Descrição:',
'st_er'=>'ER',
'test_date_to_do'=>'Data Prevista',
'tt_add_and_edit_revision_and_test_control'=>'Adição ou Edição de Revisão e Teste de Controles',
'tt_control_efficiency_history_edit_title'=>'Editar Revisão do Controle',
'tt_control_selected_revision_history'=>'Histórico de revisões do controle',
'tt_control_selected_test_history'=>'Histórico de testes do controle',
'tt_control_test_history_edit_title'=>'Editar Teste do Controle',
'vb_insert'=>'Inserir',

/* './packages/risk/popup_control_revision_task.php' */

'tt_control_revision_task_incident_revision'=>'Medição Induzida de Eficácia do Controle (pelo Acidente \'%incident_name%\')',

/* './packages/risk/popup_control_revision_task.xml' */

'lb_from'=>'De',
'lb_start_cl'=>'Início:',
'lb_to'=>'até',
'lb_value_1_cl'=>'Valor 1:',
'lb_value_2_cl'=>'Valor 2:',
'lb_value_3_cl'=>'Valor 3:',
'lb_value_4_cl'=>'Valor 4:',
'lb_value_5_cl'=>'Valor 5:',
'st_ee'=>'EE',
'st_er_bl'=>'<b>ER</b>',
'st_high_parenthesis'=>'(Alta)',
'st_low_parenthesis'=>'(Baixa)',
'st_metric'=>'Métrica:',
'st_total_incidents_found_on_period'=>'Total de acidentes encontrados no período:',
'st_total_incidents_on_time'=>'Total de acidentes da janela de tempo atual:',
'to_expected_efficiency'=>'Eficácia Esperada',
'to_real_efficiency'=>'Eficácia Real',
'tt_control_revision_task'=>'Revisão do controle',
'tt_incidents_occurred_from_period'=>'Acidentes ocorridos de %date_begin% até %date_today%',
'vb_filter'=>'Filtrar',

/* './packages/risk/popup_control_risk_edit.php' */

'tt_risk_control_association_editing'=>'Edição de Associação Controle-Risco',

/* './packages/risk/popup_control_risk_edit.xml' */

'tt_risks_bl'=>'<b>Riscos</b>',
'vb_associate_risk'=>'Associar Risco',
'vb_disassociate_risks'=>'Desassociar Riscos',

/* './packages/risk/popup_control_risk_search.xml' */

'gc_asset'=>'A.P.S.',
'tt_risks_search'=>'Busca de Riscos',
'vb_associate_risks'=>'Associar Riscos',
'wn_no_risk_selected'=>'Nenhum risco selecionado.',

/* './packages/risk/popup_control_search.xml' */

'vb_associate'=>'Associar',

/* './packages/risk/popup_control_test.xml' */

'lb_schedule_bl_cl'=>'<b>Agendamento:</b>',
'tt_control_test_revision'=>'Revisão do teste do controle',

/* './packages/risk/popup_control_test_task.xml' */

'cb_no'=>'Não',
'cb_open_next_task'=>'Abrir próxima tarefa',
'cb_yes'=>'Sim',
'lb_description_test'=>'Descrição do teste:',
'lb_observations_cl'=>'Observações:',
'lb_test_ok_bl_cl'=>'<b>Teste ok:</b>',
'tt_control_test'=>'Teste do controle',

/* './packages/risk/popup_create_risks_from_category.xml' */

'tt_risk_creation_from_category'=>'Criação de riscos a partir de uma categoria de eventos',

/* './packages/risk/popup_create_risks_from_events.xml' */

'st_select_events_not_to_associate'=>'Selecione abaixo apenas os eventos que você <b>NÃO</b> deseja relacionar ao A.P.S.!',
'tt_risk_creation_from_events'=>'Criação de riscos a partir de eventos',
'vb_link_events_later'=>'Relacionar Eventos mais Tarde',
'vb_relate_events_to_asset'=>'Relacionar Eventos ao A.P.S.',

/* './packages/risk/popup_process_asset_association.xml' */

'tt_assets_search'=>'Busca de A.P.S.',
'tt_current_assets_bl'=>'<b>A.P.S. Atuais</b>',

/* './packages/risk/popup_process_edit.php' */

'st_no_published_documents'=>'Não há documentos publicados',
'tt_process_adding'=>'Adição de Processo',
'tt_process_editing'=>'Edição de Processo',

/* './packages/risk/popup_process_edit.xml' */

'lb_area_bl_cl'=>'<b>Área:</b>',
'lb_document_cl'=>'Documento:',
'lb_name_bl_cl'=>'<b>Nome:</b>',
'lb_priority_cl'=>'Prioridade:',
'lb_responsible_bl_cl'=>'<b>Responsável:</b>',
'vb_associate_users'=>'Associar Usuários',
'vb_properties'=>'Propriedades',
'vb_view'=>'Ver',

/* './packages/risk/popup_process_search.xml' */

'gc_area'=>'Área',
'si_all_areas'=>'Todas Áreas',
'tt_current_processes_bl'=>'<b>Processos Atuais</b>',
'tt_processes_search'=>'Busca de Processos',
'vb_remove'=>'Remover',

/* './packages/risk/popup_risk_control_association_edit.php' */

'st_control_not_mitigating_efficiency_message'=>'O Controle não está reduzindo Riscos, pois a eficácia real está menor que a eficácia esperada!',
'st_control_not_mitigating_not_implemented_message'=>'O Controle não está reduzindo Riscos, pois não está implementado e a data de deadline de implementação expirou!',

/* './packages/risk/popup_risk_control_association_edit.xml' */

'lb_control_cl'=>'Controle:',
'st_control_parametrization_bl_cl'=>'<b>Estimativa do controle:</b>',
'st_values_to_reduce_risk_consequence_and_probability'=>'Os valores ao lado irão definir em quanto este controle irá reduzir a conseqüência e probabilidade do risco em questão.',
'tt_risk_reduction'=>'Redução do risco',

/* './packages/risk/popup_risk_control_edit.php' */

'tt_control_risk_association_editing'=>'Edição de Associação Risco-Controle',

/* './packages/risk/popup_risk_control_edit.xml' */

'lb_risk_cl'=>'Risco:',
'mi_edit_association'=>'Editar Associação',
'st_residual'=>'Residual',
'st_risk'=>'Risco',
'tt_controls_bl'=>'<b>Controles</b>',
'vb_disassociate_controls'=>'Desassociar Controles',

/* './packages/risk/popup_risk_control_search.xml' */

'cb_search_only_suggested_controls'=>'Pesquisar apenas controles sugeridos',
'gc_name'=>'Nome',
'lb_name_cl'=>'Nome:',
'tt_control_search'=>'Busca de Controle',
'vb_associate_control'=>'Associar Controle',
'wn_no_control_selected'=>'Nenhum controle selecionado.',

/* './packages/risk/popup_risk_edit.php' */

'st_sensitive_data_reapproval'=>'Você está alterando dados sensíveis, que necessitam passar por uma reaprovação. Deseja confirmar as alterações?',
'tt_reapproval'=>'Reaprovação',
'tt_risk_adding'=>'Adição de Risco',
'tt_risk_editing'=>'Edição de Risco',

/* './packages/risk/popup_risk_edit.xml' */

'lb_asset_bl_cl'=>'<b>A.P.S.:</b>',
'lb_event_bl_cl'=>'<b>Evento:</b>',
'lb_impact_cl_money_symbol'=>'Impacto Financeiro:',
'lb_real_risk_bl_cl'=>'<b>Risco Potencial:</b>',
'lb_residual_risk_bl_cl'=>'<b>Risco Residual:</b>',
'lb_type_cl'=>'Tipo:',
'si_select_one_of_the_items_below'=>'Selecione um dos itens abaixo',
'st_risk_parametrization_bl_cl'=>'<b>Estimativa do risco:</b>',
'static_static19'=>'Impacto:',
'vb_calculate_risk'=>'Calcular Risco',
'vb_config'=>'Configurar',
'vb_find'=>'Buscar',
'vb_probability_automatic_calculation'=>'Cálculo automático de probabilidade',
'warning_wn_asset_desparametrized'=>'Não é possivel calcular o valor do risco, pois o A.P.S. relacionado ao risco não está estimado!',
'warning_wn_control_desparametrized'=>'pelo menos um controle não está estimado. Antes de calcular o risco residual, estime o(s) controle(s) relacionados a este risco!',

/* './packages/risk/popup_risk_insert_search_event_asset.xml' */

'gc_description'=>'Descrição',
'gc_impact'=>'Impacto',
'lb_category_cl'=>'Categoria:',
'lb_description_cl'=>'Descrição:',
'si_all_categories'=>'Todas Categorias',
'tt_events_search'=>'Busca de Eventos',
'vb_create_risk'=>'Criar Risco',
'vb_search'=>'Pesquisar',
'wn_no_event_selected'=>'Nenhum evento selecionado.',

/* './packages/risk/popup_risk_insert_type.xml' */

'rb_create_independent_risk'=>'Criar um risco independente',
'st_which_action_to_take'=>'Que ação deseja realizar?',
'vb_cancel'=>'Cancelar',
'vb_create_risk_from_event'=>'Criar um risco a partir de um evento (recomendado)',

/* './packages/risk/popup_risk_parameters_weight.xml' */

'tt_risk_parameters_weight'=>'Peso dos Parâmetros de Risco',

/* './packages/risk/popup_risk_parametrization.php' */

'mx_probability'=>'Probabilidade',
'st_denied_permission_to_estimate_risks'=>'Você não tem permissão para estimar vários riscos',
'to_impact_cl'=>'Impacto:',
'to_risk_cl'=>'Risco:',

/* './packages/risk/popup_risk_parametrization.xml' */

'lb_all_bl_cl'=>'<b>Todos:</b>',
'tt_risk_parametrization'=>'Estimativa de Riscos',

/* './packages/risk/popup_risk_treatment.php' */

'tt_avoid'=>'evitar',
'tt_hold_accept'=>'reter/aceitar',
'tt_transfer'=>'transferir',

/* './packages/risk/popup_risk_treatment.xml' */

'lb_justification_bl_cl'=>'<b>Justificativa:</b>',
'st_accept_transfer_or_avoid_risk'=>'Você deseja <b>%treatment_type%</b> o Risco "<b>%risk_name%</b>" ?',
'tt_risk_treatment'=>'Tratamento de Risco (<b>%treatment_type%</b>)',

/* './packages/risk/popup_risks_control_association_edit.xml' */

'lb_all_risks_bl_cl'=>'<b>Todos Riscos:</b>',
'lb_control_bl_cl'=>'<b>Controle:</b>',
'tt_risk_control_association_parametrization'=>'Redução de Riscos',

/* './packages/risk/report/clock.php' */

'st_generating_report'=>'Gerando Relatório...',
'st_operation_wait'=>'<b>Atenção:</b> Este relatório pode levar alguns minutos para ser gerado.<br>Por favor, aguarde.',

/* './packages/risk/report/loading_report.php' */

'st_verifying_existing_reports'=>'Verificando relatórios existentes.',

/* './packages/risk/report/popup_report_areas.php' */

'mx_areas'=>'Áreas',

/* './packages/risk/report/popup_report_areas.xml' */

'rs_header_area_name'=>'Nome',
'rs_header_area_responsible'=>'Responsável',

/* './packages/risk/report/popup_report_areas_by_priority.php' */

'rs_areas_by_priority'=>'Áreas por Prioridade',

/* './packages/risk/report/popup_report_areas_by_type.php' */

'rs_areas_by_type'=>'Áreas por Tipo',

/* './packages/risk/report/popup_report_areas_without_processes.php' */

'rs_areas_without_processes'=>'Áreas sem Processos',

/* './packages/risk/report/popup_report_asset.php' */

'rs_assets'=>'A.P.S.',

/* './packages/risk/report/popup_report_asset_deps.php' */

'si_assets_deps'=>'A.P.S. - Dependências e Dependentes',
'si_dependencies'=>'Dependências',
'si_dependents'=>'Dependentes',

/* './packages/risk/report/popup_report_asset_importance.php' */

'rs_assets_importance'=>'Importância dos A.P.S.',

/* './packages/risk/report/popup_report_asset_relevance.php' */

'rs_asset_cl_bl'=>'<b>A.P.S.:</b>',
'si_asset_relevance'=>'Relevância dos A.P.S.',

/* './packages/risk/report/popup_report_asset_relevance.xml' */

'rs_responsible_cl'=>'Responsável:',

/* './packages/risk/report/popup_report_assets.php' */

'mx_assets'=>'A.P.S.',

/* './packages/risk/report/popup_report_assets.xml' */

'rs_header_asset_name'=>'Nome',
'rs_header_asset_responsible'=>'Responsável',

/* './packages/risk/report/popup_report_assets_without_risks.php' */

'rs_assets_without_risks'=>'A.P.S. sem Riscos',

/* './packages/risk/report/popup_report_assets_without_risks.xml' */

'rs_security_responsible'=>'Responsável pela Segurança',

/* './packages/risk/report/popup_report_best_practices.php' */

'mx_best_practices'=>'Requisitos Legais',

/* './packages/risk/report/popup_report_best_practices.xml' */

'rs_best_practice_name'=>'Nome',

/* './packages/risk/report/popup_report_conformity.php' */

'rs_applied_best_practices'=>'Requisitos Legais Aplicados',
'rs_not_applied_best_practices'=>'Requisitos Legais Não Atendidos',
'rs_not_used'=>'Não Utilizada',
'rs_not_used_controls'=>'Controles Não Utilizados',
'rs_report_conformity_footer'=>'Total de Requisitos Legais = %total% / Total de Requisitos Legais Aplicados = %applied%',
'rs_used_controls'=>'Controles Utilizados',
'tt_conformity'=>'Conformidade',

/* './packages/risk/report/popup_report_control_accompaniment.php' */

'rs_follow_up_of_controls'=>'Acompanhamento de Controles',

/* './packages/risk/report/popup_report_control_by_process.php' */

'rs_general_risk'=>'Geral de Aspectos e Impactos',

/* './packages/risk/report/popup_report_control_by_process.xml' */

'cb_best_practices'=>'Requisitos Legais',
'cb_control'=>'Controle',
'gc_category'=>'Categoria',
'gc_type'=>'Tipo',
'lb_estimation'=>'Estimativa de Aspectos e Impactos',
'lb_evidence'=>'Evidência',
'lb_process'=>'A. P. S.',
'lb_relavance'=>'Relevância',

/* './packages/risk/report/popup_report_control_cost.php' */

'rs_cost_per_control'=>'Custo por Controle',

/* './packages/risk/report/popup_report_control_cost_by_area.php' */

'rs_cost_of_controls_per_area'=>'Custo dos Controles por Área',

/* './packages/risk/report/popup_report_control_cost_by_asset.php' */

'rs_cost_of_controls_per_asset'=>'Custo dos Controles por A.P.S.',

/* './packages/risk/report/popup_report_control_cost_by_cost_category.php' */

'mx_control_cost_by_cost_category'=>'Custos de Controles por Categoria de Custo',

/* './packages/risk/report/popup_report_control_cost_by_cost_category.xml' */

'rs_control_cost'=>'Custo',
'rs_control_name'=>'Nome',
'rs_total_cost'=>'Total',

/* './packages/risk/report/popup_report_control_cost_by_process.php' */

'rs_cost_of_controls_per_process'=>'Custo dos Controles por Processo',

/* './packages/risk/report/popup_report_control_cost_by_responsible.php' */

'rs_cost_of_controls_per_responsible'=>'Custo dos Controles por Responsável',

/* './packages/risk/report/popup_report_control_cost_by_responsible.xml' */

'rs_cost'=>'Custo',

/* './packages/risk/report/popup_report_control_efficiency.php' */

'rs_controls_efficiency'=>'Eficácia dos Controles',
'rs_efficient_controls'=>'Controles Eficientes',
'rs_inefficient_controls'=>'Controles Ineficientes',

/* './packages/risk/report/popup_report_control_efficiency_accompaniment.php' */

'rs_follow_up_of_control_efficiency'=>'Acompanhamento da Eficácia dos Controles',

/* './packages/risk/report/popup_report_control_efficiency_accompaniment.xml' */

'rs_ee'=>'EE',
'rs_re'=>'ER',

/* './packages/risk/report/popup_report_control_implementation_date.php' */

'rs_controls_implemented_outside_stated_period'=>'Controles implementados fora do prazo',
'rs_controls_implemented_within_stated_period'=>'Controles implementados dentro do prazo',
'rs_date_of_implementation_of_controls'=>'Data de Implementação dos Controles',

/* './packages/risk/report/popup_report_control_implementation_date.xml' */

'rs_implementation'=>'Implementação',

/* './packages/risk/report/popup_report_control_planning.php' */

'rs_implementation_date'=>'Data de Implementação',
'rs_implementation_deadline'=>'Prazo de Implementação',
'rs_implemented_controls'=>'Controles Implementados',
'rs_planned_controls'=>'Controles Planejados',
'rs_planning_of_controls'=>'Planejamento dos Controles',

/* './packages/risk/report/popup_report_control_revision_agenda_by_responsible.php' */

'si_control_revision_agenda_by_responsible'=>'Agenda de Revisão dos Controles por Responsável',

/* './packages/risk/report/popup_report_control_revision_agenda_by_responsible.xml' */

'rs_next_revision'=>'Próxima Revisão',

/* './packages/risk/report/popup_report_control_summary_pm.php' */

'rs_best_practice'=>'Requisito Legal',
'rs_document'=>'Documento',
'rs_summary_of_controls'=>'Resumo de Controles',

/* './packages/risk/report/popup_report_control_summary_pm.xml' */

'rs_accomplishment_bl_cl'=>'<b>Realização:</b>',
'rs_description_cl'=>'<b>Descrição:</b>',

/* './packages/risk/report/popup_report_control_test_accompaniment.php' */

'rs_ctrl_test_not_ok'=>'Não Ok',
'rs_ctrl_test_ok'=>'Ok',
'rs_follow_up_of_control_test'=>'Acompanhamento do Teste dos Controles',

/* './packages/risk/report/popup_report_control_test_accompaniment.xml' */

'rs_comment'=>'Comentário:',
'rs_control_cl'=>'Controle:',
'rs_date_accomplishment'=>'Data Realização',
'rs_predicted_date'=>'Data Prevista',
'rs_test'=>'Teste',

/* './packages/risk/report/popup_report_control_test_agenda_by_responsible.php' */

'si_control_test_agenda_by_responsible'=>'Agenda de Teste dos Controles por Responsável',

/* './packages/risk/report/popup_report_control_test_agenda_by_responsible.xml' */

'rs_next_test'=>'Próximo Teste',
'rs_processes'=>'Processos',
'rs_responsibles'=>'Responsáveis',
'rs_schedule'=>'Agendamento',

/* './packages/risk/report/popup_report_controls.php' */

'mx_controls'=>'Controles',

/* './packages/risk/report/popup_report_controls.xml' */

'rs_header_control_name'=>'Nome',
'rs_header_control_responsible'=>'Responsável',

/* './packages/risk/report/popup_report_controls_by_responsible.php' */

'rs_controls_per_responsible'=>'Controles por Responsável',

/* './packages/risk/report/popup_report_controls_by_risk.php' */

'rs_controls_per_risk'=>'Controles por Risco',

/* './packages/risk/report/popup_report_controls_by_risk.xml' */

'rs_affected_asset_bl_cl'=>'<b>A.P.S. Afetado:</b>',
'rs_risk_and_impact_bl_cl'=>'<b>Risco / Impacto:</b>',
'rs_risk_bl_cl'=>'<b>Risco:</b>',

/* './packages/risk/report/popup_report_controls_by_type.php' */

'rs_controls_by_type'=>'Controles por Tipo',

/* './packages/risk/report/popup_report_controls_not_measured.php' */

'rs_not_measured_controls'=>'Controles não Medidos',

/* './packages/risk/report/popup_report_controls_not_measured.xml' */

'rs_efficiency'=>'Eficácia',

/* './packages/risk/report/popup_report_controls_without_risks.php' */

'rs_controls_without_risks'=>'Controles sem Riscos',

/* './packages/risk/report/popup_report_detailed_pendant_tasks.php' */

'rs_detailed_pending_tasks'=>'Tarefas Pendentes Detalhadas',

/* './packages/risk/report/popup_report_duplicated_risks.php' */

'tt_duplicated_risks'=>'Riscos Duplicados',

/* './packages/risk/report/popup_report_duplicated_risks.xml' */

'rs_risk_cl'=>'Risco:',

/* './packages/risk/report/popup_report_duplicated_risks_by_asset.php' */

'tt_duplicated_risks_by_asset'=>'Riscos Duplicados por A.P.S.',

/* './packages/risk/report/popup_report_duplicated_risks_by_asset.xml' */

'rs_risk'=>'Risco',

/* './packages/risk/report/popup_report_events_by_category.php' */

'rs_checklist_by_event'=>'Checklist por Evento',

/* './packages/risk/report/popup_report_events_by_category.xml' */

'rs_brackets'=>'[ ]',
'rs_event'=>'Evento',
'rs_na'=>'NA',
'rs_nc'=>'NC',
'rs_ok'=>'OK',

/* './packages/risk/report/popup_report_events_by_type.php' */

'rs_events_by_type'=>'Eventos por Tipo',

/* './packages/risk/report/popup_report_pendant_contexts.php' */

'rs_pending_items'=>'Elementos Pendentes',

/* './packages/risk/report/popup_report_pendant_contexts.xml' */

'rs_approver'=>'Aprovador',

/* './packages/risk/report/popup_report_policy.php' */

'rs_sgsi_policy_statement'=>'Declaração de Política do SGSI',

/* './packages/risk/report/popup_report_policy.xml' */

'rs_system_policy'=>'Política do Sistema',

/* './packages/risk/report/popup_report_processes.php' */

'mx_processes'=>'Processos',

/* './packages/risk/report/popup_report_processes.xml' */

'rs_header_process_name'=>'Nome',
'rs_header_process_responsible'=>'Responsável',

/* './packages/risk/report/popup_report_processes_by_priority.php' */

'rs_processes_by_priority'=>'Processos por Prioridade',

/* './packages/risk/report/popup_report_processes_by_priority.xml' */

'rs_priority_cl'=>'Prioridade:',

/* './packages/risk/report/popup_report_processes_by_type.php' */

'rs_processes_by_type'=>'Processos por Tipo',

/* './packages/risk/report/popup_report_processes_without_assets.php' */

'rs_processes_without_assets'=>'Processos sem A.P.S.',

/* './packages/risk/report/popup_report_risk_amount_by_area.php' */

'rs_amount_of_risks_per_area'=>'Quantidade de Riscos por Área',

/* './packages/risk/report/popup_report_risk_amount_by_process.php' */

'rs_amount_of_risks_per_process'=>'Quantidade de Riscos por Processo',

/* './packages/risk/report/popup_report_risk_financial_impact.php' */

'mx_risk_financial_impact'=>'Impacto Financeiro dos Riscos',

/* './packages/risk/report/popup_report_risk_financial_impact.xml' */

'rs_risk_name'=>'Aspecto',
'rs_risk_residual_value'=>'Risco Residual',
'rs_risk_value'=>'Risco Potencial',
'rs_total_impact'=>'Total',

/* './packages/risk/report/popup_report_risk_impact.php' */

'rs_risk_impact'=>'Impacto do Risco',

/* './packages/risk/report/popup_report_risk_status_by_area.php' */

'rs_risk_status_per_area'=>'Status de Risco por Área',

/* './packages/risk/report/popup_report_risk_status_by_area.xml' */

'rs_area'=>'Área',

/* './packages/risk/report/popup_report_risk_status_by_process.php' */

'rs_risk_status_per_process'=>'Status de Risco por Processo',

/* './packages/risk/report/popup_report_risk_status_by_process.xml' */

'rs_process'=>'Processo',

/* './packages/risk/report/popup_report_risk_treatment_plan.php' */

'rs_and'=>'e',
'rs_avoided'=>'Evitado',
'rs_not_treated'=>'Não Tratado',
'rs_plan_of_risks_treatment'=>'Plano de Tratamento dos Riscos',
'rs_reduced'=>'Reduzido',
'rs_restrained'=>'Retido',
'rs_transferred'=>'Transferido',

/* './packages/risk/report/popup_report_risk_treatment_plan.xml' */

'rs_asset_bl_cl'=>'<b>A.P.S.:</b>',
'rs_status'=>'Status',

/* './packages/risk/report/popup_report_risk_values.php' */

'rs_risks_values'=>'Valores dos Riscos',

/* './packages/risk/report/popup_report_risk_values.xml' */

'rs_affected_asset'=>'A.P.S. Afetado',

/* './packages/risk/report/popup_report_risks.php' */

'mx_risks'=>'Riscos',

/* './packages/risk/report/popup_report_risks_by_area.php' */

'rs_risks_per_area'=>'Riscos por Área',

/* './packages/risk/report/popup_report_risks_by_asset.php' */

'rs_risks_per_asset'=>'Riscos por A.P.S.',

/* './packages/risk/report/popup_report_risks_by_control.php' */

'rs_report_of_risk_per_control'=>'Relatório de Riscos por Controle',

/* './packages/risk/report/popup_report_risks_by_control.xml' */

'rs_control_bl_cl'=>'<b>Controle:</b>',

/* './packages/risk/report/popup_report_risks_by_process.php' */

'rs_risks_per_process'=>'Riscos por Processo',

/* './packages/risk/report/popup_report_risks_by_type.php' */

'rs_risks_by_type'=>'Riscos por Tipo',

/* './packages/risk/report/popup_report_risks_by_type.xml' */

'rs_type_cl'=>'Tipo:',

/* './packages/risk/report/popup_report_risks_resume.php' */

'cb_very_high' => 'Muito Alto',
'cb_very_low' => 'Muito Baixo',
'rs_very_high' => 'Muito Alto',
'rs_very_low' => 'Muito Baixo',
'cb_high'=>'Alto',
'cb_low'=>'Baixo',
'cb_medium'=>'Médio',
'cb_not_parameterized'=>'Não estimados',
'gc_capability'=>'Potencial',
'lb_indicators'=>'Indicadores',
'lb_total'=>'Total',
'rb_not_treated'=>'Não Tratados',
'rb_residual'=>'Residual',
'rb_treated'=>'Tratados',
'tt_general_summary_of_risks'=>'Risco Estimado vs. Não-Estimado',
'tt_risk_management_status'=>'Nível de Risco',
'tt_summary_of_parameterized_risks'=>'Riscos Potenciais vs. Riscos Residuais',

/* './packages/risk/report/popup_report_scope.php' */

'rs_sgsi_scope_statement'=>'Declaração de Escopo do SGSI',

/* './packages/risk/report/popup_report_scope.xml' */

'rs_system_scope'=>'Escopo do Sistema',

/* './packages/risk/report/popup_report_statement_of_applicability.php' */

'rs_applied'=>'Aplicado',
'rs_no_risks_application_control'=>'Até o momento não foram identificados riscos que justifiquem a aplicação do controle.',
'rs_not_applied'=>'Não Aplicado',
'rs_reduction_of_risks'=>'Redução de riscos.',
'rs_statement_of_applicability'=>'Declaração de Aplicabilidade',
'rs_statement_of_applicability_pre_report'=>'Pré-Relatório de Declaração de Aplicabilidade',

/* './packages/risk/report/popup_report_statement_of_applicability.xml' */

'rs_applicability_cl'=>'Aplicabilidade:',
'rs_applicability'=>'Aplicabilidade',
'rs_best_practice_cl'=>'Requisito Legal:',
'rs_control'=>'Controle',
'rs_justification_cl'=>'Justificativa:',

/* './packages/risk/report/popup_report_statement_of_applicability_justificative.xml' */

'tt_justification'=>'Justificativa',
'vb_close'=>'Fechar',
'vb_confirm'=>'Confirmar',

/* './packages/risk/report/popup_report_summarized_pendant_tasks.php' */

'rs_summarized_pending_tasks'=>'Tarefas Pendentes Resumidas',

/* './packages/risk/report/popup_report_top10_assets_with_more_risks.php' */

'rs_top_10_assets_with_highest_risks'=>'Top 10 A.P.S. com Mais Riscos Altos',
'rs_top_10_assets_with_most_risks'=>'Top 10 A.P.S. com Mais Riscos',

/* './packages/risk/report/popup_report_top10_assets_with_more_risks.xml' */

'rs_amount_of_risks'=>'Qtde. Riscos',
'rs_asset'=>'A.P.S.',

/* './packages/risk/report/popup_report_top10_risks_by_area.php' */

'rs_top_10_risks_per_area'=>'Top 10 Riscos por Área',

/* './packages/risk/report/popup_report_top10_risks_by_area.xml' */

'rs_area_bl_cl'=>'<b>Área:</b>',

/* './packages/risk/report/popup_report_top10_risks_by_process.php' */

'rs_top_10_risks_per_process'=>'Top 10 Riscos por Processo',

/* './packages/risk/report/popup_report_top10_risks_by_process.xml' */

'rs_process_bl_cl'=>'<b>Processo:</b>',
'rs_risk_and_impact'=>'Risco / Impacto',

/* './packages/risk/report/popup_report_user_responsabilities.php' */

'rs_security_resp'=>'Resp. Segurança',
'rs_special_user'=>'Usuário Especial',
'rs_users_responsibilities'=>'Responsabilidades dos Usuários',

/* './packages/risk/report/popup_report_user_responsabilities.xml' */

'rs_responsible_bl_cl'=>'<b>Responsável:</b>',

/* './packages/risk/report/report_template.xml' */

'rs_comment_cl'=>'Comentário:',
'rs_end_of_report'=>'FIM DO RELATÓRIO',

/* './packages/risk/tab_risk_management.xml' */

'ti_area'=>'Área',
'ti_asset'=>'Atividades, Produtos e Serviços',
'ti_controls'=>'Controles',
'ti_process'=>'Processo',
'ti_reports'=>'Relatórios',
'ti_risk'=>'Aspectos e Impactos',

/* './popup_choose_role.php' */

'mx_user_doesnt_have_permission_to_login'=>'O usuário <b>%user_name%</b> não possui permissão para acessar o sistema.',
'tt_login_error'=>'Erro ao Logar no Sistema',

/* './popup_choose_role.xml' */

'lb_represent_bl_cl'=>'<b>Representar:</b>',
'st_solicitor_explanation'=>'Caso você queira representar algum desses usuários, pressione OK.',
'tt_solicitor_mode'=>'Modo Procurador',

/* './popup_email_preferences.xml' */

'cb_action_plan'=>'Plano de Ação',
'cb_areas'=>'Áreas',
'cb_assets'=>'A.P.S.',
'cb_controls'=>'Controles',
'cb_documents'=>'Documentos',
'cb_events'=>'Eventos',
'cb_incidents'=>'Acidentes',
'cb_non_conformities'=>'Não Conformidades',
'cb_occurrences'=>'Incidentes',
'cb_others'=>'Outros',
'cb_processes'=>'Processos',
'cb_risks'=>'Riscos',
'cb_select_all'=>'Selecionar Tudo',
'cb_standards'=>'Legislações',
'lb_warning_types_to_receive_cl'=>'Selecione quais os tipos de avisos que você gostaria de receber:',
'rb_html_email'=>'Email em HTML',
'rb_messages_digest_complete'=>'Completo (um email diário com as mensagens completas)',
'rb_messages_digest_none'=>'Nenhum (um email para cada mensagem)',
'rb_messages_digest_subjects'=>'Assuntos (um email diário apenas com os assuntos das mensagens)',
'rb_text_email'=>'Email em Texto',
'st_email_preferences_bl'=>'<b>Preferências de e-mail</b>',
'st_messages_digest_type_cl'=>'Tipo de digest das mensagens:',

/* './popup_help.xml' */

'cb_do_not_show_again'=>'Não mostrar novamente',
'st_tip_text'=>'<b>Ao acessar as tabelas do Real EMS, use o menu de contexto.</b><br/><br/>Para editar, remover ou linkar para outros componentes do Real EMS, clique com o botão direito em um resultado, de forma a ver o menu de popup com as ações disponíveis.',
'tt_tip_bl'=>'<b>Dica</b>',

/* './popup_password_request.xml' */

'lb_user_login_cl'=>'Usuário:',
'st_password_request'=>'Para obter uma nova senha, digite seu Usuário. Uma nova senha será enviada para seu e-mail.',
'st_warning_general_email_disabled'=>'O e-mail do sistema está desabilitado, não sendo possível gerar nova senha.',
'tt_password_reminder'=>'Pedido de Nova Senha',
'vb_new_password'=>'Nova Senha',
'wn_password_sent'=>'A senha foi enviada ao usuário, desde que o mesmo exista.',

/* './popup_risk_limits_task_view.xml' */

'lb_risk_limits_high_cl'=>'Tolerância ao Risco 2:',
'lb_risk_limits_low_cl'=>'Tolerância ao Risco 1:',
'tt_risk_limits_approval'=>'Aprovar Limites de Risco',

/* './popup_saas_disclaimer.xml' */

'tt_disclaimer_popup_title'=>'Termo de Responsabilidade',
'vb_agree'=>'Concordo',
'vb_disagree'=>'Discordo',

/* './popup_schedule_edit.xml' */

'lb_april'=>'Abril',
'lb_august'=>'Agosto',
'lb_december'=>'Dezembro',
'lb_february'=>'Fevereiro',
'lb_january'=>'Janeiro',
'lb_july'=>'Julho',
'lb_june'=>'Junho',
'lb_march'=>'Março',
'lb_may'=>'Maio',
'lb_november'=>'Novembro',
'lb_october'=>'Outubro',
'lb_schedule_days_to_finish'=>'Dias para executar a tarefa:',
'lb_schedule_end'=>'Termina em:',
'lb_schedule_periodicity'=>'A cada',
'lb_schedule_start'=>'Inicia em:',
'lb_schedule_type_cl'=>'Tipo:',
'lb_september'=>'Setembro',
'lb_week_days'=>'Nos seguintes dias:',
'mx_second_week'=>'segundo(a)',
'si_schedule_by_day'=>'Diário',
'si_schedule_by_month'=>'Mensal',
'si_schedule_by_week'=>'Semanal',
'si_schedule_month_day'=>'Dia do mês',
'si_schedule_week_day'=>'Dia da semana',
'st_months'=>'meses',
'st_never'=>'nunca',
'st_schedule_months'=>'Nos seguintes meses:',
'st_weeks'=>'semanas',
'tt_schedule_edit'=>'Agendamento de Tarefa',
'tx_1'=>'1',
'tx_schedule_days_to_finish'=>'1',

/* './popup_solicitor_conflict.xml' */

'st_solicitor_conflict'=>'O usuário <b>%solicitor_name%</b> foi nomeado por você para ser seu procurador no período de sua ausência. Gostaria de acabar com esse vínculo?',
'tt_solicitor_conflict'=>'Conflito de Procurador',

/* './popup_survey.xml' */

'st_survey_text'=>'Depois de avaliar o sistema, não esqueça de preencher nossa pesquisa de qualidade. Você pode acessá-la através do link "Pesquisa sobre Experiência do Usuário" no rodapé do sistema. Você deseja responder essa pesquisa agora?',
'tt_survey_bl'=>'<b>Pesquisa</b>',

/* './popup_unexpected_behavior.xml' */

'st_unexpected_behavior_message'=>'Foi detectado um comportamento inesperado no sistema. Um e-mail com as informações detalhadas sobre o ocorrido foi enviado para a nossa equipe de Suporte Técnico. Se necessário, feche o browser e entre no sistema novamente.',

/* './popup_unexpected_behavior_onsite.xml' */

'tt_unexpected_behavior'=>'Comportamento Inesperado',

/* './popup_user_change_password.php' */

'st_user_password_change'=>'Você deve trocar sua senha agora.',
'st_user_password_expired'=>'<b>Sua senha expirou!</b><br/>Você deverá trocá-la agora.',
'st_user_password_will_expire'=>'Sua senha irá expirar em %days% dia(s). Se você quiser, pode trocá-la agora.',

/* './popup_user_change_password.xml' */

'lb_confirm_cl'=>'Confirmar:',
'tt_change_password'=>'Trocar Senha',
'vb_change_password'=>'Trocar Senha',
'wn_unmatching_password'=>'As senhas não conferem.',

/* './popup_user_dashboard_preference.php' */

'tt_average_time_for_documents_approval'=>'Tempo Médio de Aprovação dos Documentos',
'tt_average_time_for_my_documents_approval'=>'Tempo Médio de Aprovação dos Meus Documentos',
'tt_financial_summary'=>'Sumário Financeiro',
'tt_grid_abnormality_ci_title'=>'Sumário de Anormalidades da Melhoria Contínua',
'tt_grid_abnormality_pm_title'=>'Sumário de Anormalidades da Gestão dos Documentos',
'tt_grid_incident_summary_title'=>'Sumário de Acidentes',
'tt_grid_incidents_per_asset_title'=>'Top 5 A.P.S. com mais Acidentes',
'tt_grid_incidents_per_process_title'=>'Top 5 Processos com mais Acidentes',
'tt_grid_incidents_per_user_title'=>'Top 5 Usuários com mais Processos Disciplinares',
'tt_grid_nc_per_process_title'=>'Top 5 Processos com mais Não Conformidades',
'tt_grid_nc_summary_title'=>'Sumário de Não Conformidades',
'tt_my_items'=>'Meus Elementos',
'tt_my_pendencies'=>'Minhas Pendências da Gestão de Aspectos e Impactos',
'tt_my_risk_summary'=>'Meu Sumário de Risco',
'tt_risk_management_my_items_x_policy_management'=>'Gestão de Aspectos e Impactos (Meus Elementos) x Gestão dos Documentos',
'tt_risk_management_x_policy_management'=>'Documentos por Elemento de Negócio',
'tt_sgsi_reports_and_documents'=>'Documentos obrigatórios do SGSI pela ISO 27001',
'tt_summary_of_abnormalities'=>'Sumário de Anormalidades da Gestão de Aspectos e Impactos',
'tt_summary_of_best_practices'=>'Requisitos Legais - Índice de Conformidade',
'tt_summary_of_controls'=>'Sumário de Controles',
'tt_summary_of_my_documents_last_revision_average_time'=>'Sumário do período de tempo da última revisão dos documentos',
'tt_summary_of_policy_management'=>'Sumário de Gestão de Documentos',
'tt_summary_of_policy_management_my_documents'=>'Sumário de Gestão dos Documentos (Meus Documentos)',
'tt_summary_of_risk_management_documentation'=>'Sumário de Documentação da Gestão de Aspectos e Impactos',
'tt_summary_of_the_risk_management_from_my_items_documentation'=>'Sumário de Documentação dos Meus Elementos da Gestão de Aspectos e Impactos',
'tt_top_10_most_read_documents'=>'Top 10 Documentos mais lidos',
'tt_top_10_most_read_my_documents'=>'Top 10 Meus Documentos Mais Lidos',

/* './popup_user_dashboard_preference.xml' */

'st_all_elements_not_selected'=>'Todos elementos (não selecionados)',
'st_selected_elements_cl'=>'Elementos selecionados:',
'tt_summary_preferences_editing'=>'Edição de Preferências do Sumário',

/* './popup_user_preferences.php' */

'si_email'=>'Email',
'si_general'=>'Geral',

/* './popup_user_preferences.xml' */

'lb_preferences_cl'=>'Preferências:',
'tt_edit_preferences'=>'Editar Preferências',

/* './popup_user_profile.xml' */

'cb_change_password'=>'Alterar senha',
'lb_current_password_cl'=>'Senha Atual:',
'lb_dashboard_preferences_cl'=>'Preferências do Dashboard:',
'lb_general_config_bl'=>'<b>Configurações Gerais</b>',
'lb_general_config_bl_cl'=>'Configurações Gerais',
'lb_language_cl'=>'Idioma:',
'lb_password_confirm_cl'=>'Confirmação da Senha:',
'lb_solicitor_cl'=>'Procurador:',
'lb_solicitor_finish_date_cl'=>'Final Procuração:',
'lb_solicitor_start_date_cl'=>'Início Procuração:',
'st_change_password_sub_title_bl'=>'<b>Alteração de Senha</b>',
'st_solicitor_sub_title_bl'=>'<b>Procurador</b>',
'to_dashboard_user_preferences_help'=>'<b>Configuração das preferências do dashboard:</b><br/><br/>Determina a ordem e quais grids de informação do sistema serão exibidas em seu dashboard.',
'wn_email_incorrect'=>'Email inválido.',
'wn_existing_password'=>'Esta senha não pode ser usada, por estar em seu histórico de senhas recentes.',
'wn_initial_date_after_today'=>'A data inicial deve ser maior ou igual ao dia atual.',
'wn_missing_dates'=>'Os campos de datas são obrigatórios quando selecionado um procurador.',
'wn_missing_solicitor'=>'É obrigatório selecionar um procurador quando uma das datas foi preenchida.',
'wn_past_date'=>'A data final deve ser maior ou igual ao dia atual.',
'wn_wrong_dates'=>'A data do final da procuração deve ser maior que a data do início da procuração.',

/* './popup_user_search.xml' */

'tt_user_search'=>'Busca de Usuário',
'wn_no_user_selected'=>'Nenhum usuário selecionado.',

/* './reports/ISMSReportTemplate.php' */

'rs_ip_cl'=>'IP:',
'rs_manager_cl'=>'Gestor:',

/* './session_expired.xml' */

'st_session_expired'=>'Sua sessão expirou. Pressione <b>Ok</b> ou feche esta janela para efetuar um novo login.',
'tt_session_expired'=>'Sessão expirada',

/* './tab_dashboard.xml' */

'ti_graphics'=>'Gráficos',
'ti_statistics'=>'Estatísticas',
'ti_summary'=>'Sumários',
'ti_warnings'=>'Minhas Tarefas & Alertas',

/* './tab_main.xml' */

'st_search'=>'Pesquisar',
'ti_continual_improvement'=>'Melhoria Contínua',
'ti_dashboard'=>'Dashboard',
'ti_libraries'=>'Bibliotecas',
'ti_policy_management'=>'Gestão dos Documentos',
'ti_risk_management'=>'Gestão de Aspectos e Impactos',
'ti_search'=>'Pesquisa',
'vb_admin'=>'Admin',
'vb_go'=>'Ir',

/* './view/FWDCalendar.php' */

'locale'=>'pt_br',

/* './view/FWDColorPicker.php' */

'chooseColor'=>'Selecione uma cor!',
'choosenColor'=>'Cor Escolhida',
'colorHelper1'=>'Passe o mouse acima da paleta para ver as cores.',
'colorHelper2'=>'Clique para escolher uma cor.',
'newColor'=>'Nova Cor',

/* './visualize.php' */

'tt_visualize'=>'Visualizar',
'st_amount_incident_period'=>'Quantidade total de incidentes no período: ',
'rs_manual_version'=>'Versão manual',
);
?>
