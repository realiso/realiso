<?php 
set_time_limit(3000);

require_once('include.php');
require_once("$base_ref/translation/include.php");


$linguas['espanhol']['filename'] = "c:/strings_isms.csv";
$linguas['espanhol']['table'] = 'zspanishisms';
//$linguas['alemao']['filename'] = "D:/Downloads/mimarisk/-_dump_dicionarios_isms__DE_final230309.csv";
///$linguas['alemao']['table'] = 'zGermanSox';
//$linguas['frances']['filename'] = "D:/Downloads/mimarisk/-_dump_dicionarios_isms__FR_final230309.csv";
//$linguas['frances']['table'] = 'zFrenchSox';

$miSystemId = 53;
$soDB = new FWDDB(DB_POSTGRES, "isms_tools", "isms", "isms", "127.0.0.1");
$soDataSet = new FWDDBDataSet($soDB, 'translation_strings');
$soDataSet->addFWDDBField(new FWDDBField('skSystem','string_system_id',DB_NUMBER,$miSystemId));
$soDataSet->addFWDDBField(new FWDDBField('zId','string_id',DB_STRING));
$soDataSet->addFWDDBField(new FWDDBField('zFile','string_file',DB_STRING));
$soDataSet->addFWDDBField(new FWDDBField('zValue','string_value',DB_STRING));
$soDataSet->addFWDDBField(new FWDDBField('dDateInclusion','string_date_inclusion',DB_DATETIME));
$soDataSet->addFWDDBField(new FWDDBField('zPortuguesIsms','string_portugues_isms', DB_STRING));
$soDataSet->addFWDDBField(new FWDDBField('zSpanishIsms','string_spanish_isms', DB_STRING));
$soDataSet->addFWDDBField(new FWDDBField('zEnglishIsms','string_english_isms', DB_STRING));
$soDataSet->addFWDDBField(new FWDDBField('zChineseIsms','string_chinese_isms', DB_STRING));
$soDataSet->addFWDDBField(new FWDDBField('zPortuguesEms','string_portuguese_ems', DB_STRING));
$soDataSet->addFWDDBField(new FWDDBField('zPortuguesOhs','string_portuguese_ohs', DB_STRING));
$soDataSet->addFWDDBField(new FWDDBField('zEnglishEms','string_english_ems', DB_STRING));
$soDataSet->addFWDDBField(new FWDDBField('zEnglishOhs','string_english_ohs', DB_STRING));
$soDataSet->addFWDDBField(new FWDDBField('zEnglishSox','string_english_sox', DB_STRING));
$soDataSet->addFWDDBField(new FWDDBField('zEnglishPci','string_english_pci', DB_STRING));
$soDataSet->addFWDDBField(new FWDDBField('zGermanSox','string_german_sox', DB_STRING));
$soDataSet->addFWDDBField(new FWDDBField('zFrenchSox','string_french_sox', DB_STRING));
$soDataSet->addFWDDBField(new FWDDBField('zPortugues3380','string_portugues_3380', DB_STRING));

$soDataSet->getFieldByAlias('string_system_id')->addFilter(new FWDDBFilter("=", $miSystemId));

$soDataSet->setOrderBy('string_file', '+');
$soDataSet->setOrderBy('string_id', '+');

$x = 0;
$y = 0;

$miTimeStampLocal = microtime(1);

foreach($linguas as $lingua){
	$dict = file($lingua['filename'], FILE_IGNORE_NEW_LINES);
	echo "acessando arquivo: " . $lingua['filename'] . "<BR>";
	for($z = 1; $z < count($dict); $z++){
		$line = split('[;]', $dict[$z], 4);
		$query ="update translation_strings set " . $lingua['table'] ." = '" . addslashes($line[3]) . "' where skkey = $line[0] ";
		$query = utf8_encode($query);
		if (!$soDB->execute($query))
			echo "erro na query: $query<br/>";
		$x++;
	}
	$y++;
}
echo "<h1>Feito em " . number_format(microtime(1) - $miTimeStampLocal,2) .  " segundos!</h1><br>Foram atualizadas $x strings em $y linguas<br>";

?>