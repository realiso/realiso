<?php
include_once "include.php";

class SaveEvent extends FWDRunnable {
  public function run() {    
    $miDocumentId = FWDWebLib::getObject('param_document_id')->getValue();
    $moDocument = new PMDocument();
    $moDocument->testPermissionToManage($miDocumentId);
    
    $miAuthorId = FWDWebLib::getObject('author_id')->getValue();
    if ($miAuthorId) {
      $moDocument = new PMDocument();
      $moDocument->setFieldValue('document_author',$miAuthorId);
      $moDocument->update($miDocumentId);
    }
    echo "soWindow = soPopUpManager.getPopUpById('popup_document_author_edit').getOpener();
          if (soWindow.refresh_grid) soWindow.refresh_grid();
          soWindow = null;
          soPopUpManager.closePopUp('popup_document_author_edit');";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent("")); 
    $moStartEvent->addAjaxEvent(new SaveEvent("save_event"));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $miDocumentId = FWDWebLib::getObject('param_document_id')->getValue();
    $moDocument = new PMDocument();
    $moDocument->testPermissionToManage($miDocumentId);
    $moDocument->fetchById($miDocumentId);
    FWDWebLib::getObject('document_name')->setValue($moDocument->getFieldValue('document_name'));
    $miAuthorId = $moDocument->getFieldValue('document_author');
    FWDWebLib::getObject('author_id')->setValue($miAuthorId);
    $moUser = new ISMSUser();
    $moUser->fetchById($miAuthorId);
    FWDWebLib::getObject('document_author')->setValue($moUser->getFieldValue('user_name'));
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
    <script language="javascript">
      function set_user(id, name){
        gebi('author_id').value = id;
        gobi('document_author').setValue(name);
        id = name = null;
      }
    </script>
    <? 
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_document_author_edit.xml");
?>