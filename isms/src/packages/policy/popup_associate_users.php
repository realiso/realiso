<?php

include_once "include.php";
include_once $handlers_ref . "grid/QueryGridUserSearch.php";
include_once $handlers_ref . "grid/policy/QueryGridCurrentUsers.php";

function getCurrentUserIds(){
  $msValues = FWDWebLib::getObject('current_users_ids')->getValue();
  $maIds = array_filter(explode(':', $msValues));
  return $maIds;
}

class GridUsers extends FWDDrawGrid {
  public function drawItem(){
    switch($this->ciColumnIndex){
      case 1:{
        $this->coCellBox->setIconSrc("../gfx/icon-user.gif");
        return parent::drawItem();
      }
      default:{
        return parent::drawItem();
      }
    }
  }
}

class SearchUserEvent extends FWDRunnable {
  public function run(){
    $moGrid = FWDWebLib::getObject('grid_user_search');
    $moGrid->execEventPopulate();
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new SearchUserEvent('search_user_event'));

    $moGrid = FWDWebLib::getObject('grid_user_search');
    $moHandler = new QueryGridUserSearch(FWDWebLib::getConnection());
    $moHandler->setName(FWDWebLib::getObject('var_user_name')->getValue());
    $moHandler->setLogin(FWDWebLib::getObject('var_user_login')->getValue());
    $moHandler->setExludedUsers(getCurrentUserIds());
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new GridUsers());

    $moGrid = FWDWebLib::getObject('grid_current_users');
    $moHandler = new QueryGridCurrentUsers(FWDWebLib::getConnection());
    $moHandler->showUsers(getCurrentUserIds());
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new GridUsers());
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    FWDWebLib::getObject('current_users_ids')->setValue(FWDWebLib::getObject('current_users')->getValue());

    $moGrid = FWDWebLib::getObject('grid_current_users');
    $moHandler = new QueryGridCurrentUsers(FWDWebLib::getConnection());
    $moHandler->showUsers(getCurrentUserIds());
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new GridUsers());

    //testa se a licen�a utilizada permite acessar o m�dulo de documenta��o
    ISMSLib::testUserPermissionToPoliceMode();

    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        gebi('user_login').focus();
        
        function refresh_grid(){
          gobi('grid_user_search').setPopulate(true);
          js_refresh_grid('grid_user_search');
          gobi('grid_current_users').setPopulate(true);
          js_refresh_grid('grid_current_users');
        }
        function enter_user_search_event(e){
          if(!e) e = window.event;
          if(e['keyCode']==13){
            gebi('var_user_name').value = gebi('user_name').value;
            gebi('var_user_login').value = gebi('user_login').value;
            gobi('grid_user_search').setPopulate(true);
            trigger_event("search_user_event","3");
          }
        }
        FWDEventManager.addEvent(gebi('user_name'), 'keydown', enter_user_search_event);
        FWDEventManager.addEvent(gebi('user_login'), 'keydown', enter_user_search_event);
      </script>
    <?
  }
}

//testa se a licen�a utilizada permite acessar o m�dulo de documenta��o
ISMSLib::testUserPermissionToPoliceMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_associate_users.xml");

?>