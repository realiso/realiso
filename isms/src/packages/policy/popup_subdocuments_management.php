<?php
include_once "include.php";
include_once $handlers_ref . "QueryTreeDocument.php";

/*
 * Fun��o que monta os elementos utilizados em um nodo da �rvore.
 */
function getElements($psId, $psValue = "") {
	$maElements = array('icon' => null, 'value' => null, 'link' => null);

	$mbIsRootElement = $psValue ? false : true;

	/*
	 * Se n�o for passado o nome do documento, busca no banco
	 * (ocorre quando � a raiz da �rvore).
	 */
	if ($mbIsRootElement) {
		$moDocument = new PMDocument();
		$moDocument->fetchById($psId);
		$psValue = $moDocument->getFieldValue('document_name');
	}

	$moDocument = new PMDocument();
	$mbUserCanEdit = $moDocument->userCanEdit($psId);

	// Nome do documento com link para edi��o (se n�o for o nodo raiz)
	$moStatic = new FWDStatic();
	if (!$mbIsRootElement && $mbUserCanEdit) {
	  $moStatic->setAttrStringNoEscape("true");
	  $msId1 = uniqid();
	  $moStatic->setValue("<a href='javascript:edit_document($psId,\"$msId1\");'>$psValue</a>");
	}
	else {
		$moStatic->setValue($psValue);
	}

	// �cone do tipo do documento
	$msIcon = '';
	$moPMDocument = new PMDocument();
	$moPMDocument->fetchById($psId);
	$moIcon = new FWDIcon();
	$moIcon->setAttrSrc(FWDWebLib::getInstance()->getGfxRef().$moPMDocument->getIcon());

  // Link para criar subdocumento
  $moLink = new FWDStatic();
  if ($mbUserCanEdit) {
    $moLink->setAttrStringNoEscape("true");
    $msId2 = uniqid();
    $moLink->setValue("<a class='createSubDocument' href='javascript:create_subdocument($psId,\"$msId2\");'>[<font color='#0000ff'>".FWDLanguage::getPHPStringValue('st_create_subdocument','Criar Subdocumento')."</font>]</a>");
  }

  $maElements['icon'] = $moIcon;
  $maElements['value'] = $moStatic;
  $maElements['link'] = $moLink;

  return $maElements;
}

/*
 * Fun��o que constr�i/retorna o dbtree.
 */
function createDbTree() {
	$miDocumentId = FWDWebLib::getObject('document_id')->getValue();

	$moHandler = new QueryTreeDocument(FWDWebLib::getConnection());
	$moHandler->setRootDocument($miDocumentId);
	$moHandler->makeQuery();
	$moHandler->executeQuery();
	$moDbTree = FWDWebLib::getObject('subdocuments_tree');

	$maElements = getElements($miDocumentId);
	$moDbTree->addObjFWDView($maElements['icon']);
	$moDbTree->addObjFWDView($maElements['value']);
	$moDbTree->addObjFWDView($maElements['link']);

	$moDbTree->setDataSet($moHandler->getDataSet());
	$moDbTree->setNodeBuilder(new NodeBuilder());
	$moDbTree->buildTree();

	return $moDbTree;
}

class NodeBuilder extends FWDNodeBuilder {
	public function buildNode($psId, $psValue) {
		$moNode = new FWDTree();
    $moNode->setAttrName($psId);

    // Adiciona objetos no nodo
    $maElements = getElements($psId, $psValue);
    $moNode->addObjFWDView($maElements['icon']);
    $moNode->addObjFWDView($maElements['value']);
    $moNode->addObjFWDView($maElements['link']);

    return $moNode;
	}
}


class RefreshTreeEvent extends FWDRunnable {
  public function run(){
		$moDbTreeBase = createDbTree();
		$msDraw = str_replace(array("\n",'"'),array(' ','\"'),$moDbTreeBase->draw(false));
		echo "gebi('root_tree').innerHTML = \"$msDraw\";";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new RefreshTreeEvent('refresh_tree_event'));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();

    $miDocumentId = FWDWebLib::getObject('document_id')->getValue();
    $moDocument = new PMDocument();
    $moDocument->testPermissionToEdit($miDocumentId);

		/*
     * Armazena o id que serve de sufixo para o identificador da popup.
     * Necess�rio para que essa janela n�o seja sobreescrita por uma
     * outra 'inst�ncia' dessa mesma janela.
     */
    $msPopupId = $moWebLib->getObject('param_popupid')->getValue();
    $moWebLib->getObject('popupid')->setValue($msPopupId);

	createDbTree();
	
	?>
		<style>
			.FWDTreeBase a.createSubDocument:hover{
				text-decoration: underline;
			}
		</style>
	<?

    $moWebLib->dump_html($moWebLib->getObject('dialog'));
    ?>
    	<script language="javascript">
				function create_subdocument(piParentId, psPopupId) {
					isms_open_popup('popup_document_edit'+psPopupId,'packages/policy/popup_document_edit.php?parent='+piParentId+'&popupid='+psPopupId,'','true',497,560);
				}

				function edit_document(piDocId, psPopupId) {
					isms_open_popup('popup_document_edit'+psPopupId,'packages/policy/popup_document_edit.php?document='+piDocId+'&popupid='+psPopupId,'','true',497,560);
				}

				function refresh_tree() {
					trigger_event('refresh_tree_event', 3);
				}
        
        function refresh_grid() {
          soWindow = self.getOpener();
          if (soWindow.refresh_grid) soWindow.refresh_grid();
          soWindow = null;
        }
			</script>
    <?
  }
}

//testa se a licen�a utilizada permite acessar o m�dulo de documenta��o
ISMSLib::testUserPermissionToPoliceMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_subdocuments_management.xml");
?>