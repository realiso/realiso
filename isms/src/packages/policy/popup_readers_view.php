<?php
include_once "include.php";
include_once $handlers_ref."grid/policy/QueryGridCurrentUsers.php";

class GridReadersView extends FWDDrawGrid {
  public function DrawItem(){
    switch($this->ciColumnIndex){
      case 1:
        $msGfxRef = FWDWebLib::getInstance()->getGfxRef();
        $moIcon = new FWDIcon(new FWDBox(2,4));
        $moIcon->setAttrSrc("{$msGfxRef}icon-user.gif");
        return $moIcon->draw();
      default:
        return parent::drawItem();
      break;
    }
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
    
    $moDocumentUser = new PMDocumentReader();
    $maUsers = $moDocumentUser->getAllowedUsers(FWDWebLib::getObject('param_document_id')->getValue());
    
    $moGrid = FWDWebLib::getObject("grid_readers_view");
    $moHandler = new QueryGridCurrentUsers(FWDWebLib::getConnection()); 
    $moHandler->showUsers($maUsers);
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new GridReadersView());
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $miDocumentId = FWDWebLib::getObject("param_document_id")->getValue();
    $moPMDocument = new PMDocument();
    $moPMDocument->testPermissionToRead($miDocumentId);
  
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
?>
<script language="javascript">
  function refresh_grid() {
    js_refresh_grid('grid_readers_view');
  }
</script>
<?
  }
}

//testa se a licen�a utilizada permite acessar o m�dulo de documenta��o
ISMSLib::testUserPermissionToPoliceMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_readers_view.xml");
?>