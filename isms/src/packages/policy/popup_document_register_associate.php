<?php
include_once "include.php";
include_once $handlers_ref."grid/policy/QueryGridRegister.php";
include_once $classes_isms_ref . "nonauto/policy_management/RegistersDrawGrid.php";

class AssociateRegistersEvent extends FWDRunnable {
  public function run() {
    $miDocumentId = FWDWebLib::getObject('document_id')->getValue();
    
    $moDocument = new PMDocument();
    $moDocument->testPermissionToEdit($miDocumentId);
    
    $msRegisters = FWDWebLib::getObject('selected_registers')->getValue();
    $maRegisters = explode(",",$msRegisters);

    //obtem todos os registros do documento
    $moPMDocRegister = new PMDocRegisters();
    $moPMDocRegister->createFilter($miDocumentId,'document_id');
    $moPMDocRegister->select();

    while($moPMDocRegister->fetch()) {
    	$docRegisterId = $moPMDocRegister->getFieldValue('register_id');

    	$moPMRegister = new PMRegister();
		$moPMRegister->fetchById($docRegisterId);
		$docRegisterName = strtolower(trim($moPMRegister->getFieldValue('register_name')));
		
		//compara com os que foram selecionados
		foreach($maRegisters as $miRegisterId) {
			$moPMRegister = new PMRegister();

			$moPMRegister->fetchById($miRegisterId);
			
			$registerName = trim($moPMRegister->getName());
			$registerNameLower = strtolower($registerName);
			
			if($docRegisterName == $registerNameLower){
			    $msErrorTitle = FWDWebLib::getObject("register_same_name_error")->getValue();
			    $msErrorTitle = str_replace("%register_name%", $registerName, $msErrorTitle);

			    print "gobi('register_same_name_error').setValue('$msErrorTitle');";
				print  "js_show('register_same_name_error');";
				return;	
			}
		}	
    }
    
    /*
    foreach($maRegisters as $miRegisterId) {
      $moPMDocRegister = new PMDocRegisters();
      $moPMDocRegister->createFilter($miRegisterId,'register_id');
      $moPMDocRegister->createFilter($miDocumentId,'document_id');
      $moPMDocRegister->select();
      if (!$moPMDocRegister->fetch()) {
        $moPMDocRegister->setFieldValue('register_id',$miRegisterId);
        $moPMDocRegister->setFieldValue('document_id',$miDocumentId);
        $moPMDocRegister->insert();
      }
    }
    /*/
    $moDocument->setFieldValue('document_id',$miDocumentId);
    $moDocument->updateRelation(new PMDocRegisters(),'register_id',$maRegisters,false);
    //*/
    echo "soWindow = soPopUpManager.getPopUpById('popup_document_register_associate').getOpener();"
        ."if(soWindow.refresh_grid) soWindow.refresh_grid();"
        ."soPopUpManager.closePopUp('popup_document_register_associate');";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new AssociateRegistersEvent("associate_registers"));

    $miDocumentId = FWDWebLib::getObject('param_document_id')->getValue();
    
    $maExcludedRegisters = array();
    $moPMRegister = new PMDocRegisters();
    $moPMRegister->createFilter($miDocumentId,'document_id');
    $moPMRegister->select();
    while($moPMRegister->fetch()) {
      $maExcludedRegisters[] = $moPMRegister->getFieldValue('register_id');
    }
    
    $moGrid = FWDWebLib::getObject('grid_associate_register');
    $moHandler = new QueryGridRegister(FWDWebLib::getConnection());
    $moHandler->setExcludedRegisters($maExcludedRegisters);
    $moGrid->setObjFwdDrawGrid(new RegistersDrawGrid());
    $moGrid->setQueryHandler($moHandler);
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $miDocumentId = $moWebLib->getObject('param_document_id')->getValue();
    
    $moDocument = new PMDocument();
    $moDocument->testPermissionToEdit($miDocumentId);
    
    $moWebLib->getObject('document_id')->setValue($miDocumentId);
    $moPMDocument = new PMDocument();
    $moPMDocument->fetchById($miDocumentId);
    $msDocumentName = $moPMDocument->getName();
    $moWebLib->getObject("document_name")->setValue($msDocumentName);
    
    $moWebLib->dump_html($moWebLib->getObject('dialog'));
  }
}

//testa se a licen�a utilizada permite acessar o m�dulo de documenta��o
ISMSLib::testUserPermissionToPoliceMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_document_register_associate.xml");
?>