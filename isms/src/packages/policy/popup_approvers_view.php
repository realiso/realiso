<?php
include_once "include.php";
include_once $handlers_ref."grid/policy/QueryGridCurrentUsers.php";

class GridApproversView extends FWDDrawGrid {
  public function DrawItem(){
    switch($this->ciColumnIndex){
      case 1:
        $msGfxRef = FWDWebLib::getInstance()->getGfxRef();
        $moIcon = new FWDIcon(new FWDBox(2,4));
        $moIcon->setAttrSrc("{$msGfxRef}icon-user.gif");
        return $moIcon->draw();
      default:
        return parent::drawItem();
      break;
    }
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
    
    $moDocumentApprover = new PMDocumentApprover();
    $maUsers = $moDocumentApprover->getUsers(FWDWebLib::getObject('param_document_id')->getValue(),true);
    
    $moGrid = FWDWebLib::getObject("grid_approvers_view");
    $moHandler = new QueryGridCurrentUsers(FWDWebLib::getConnection()); 
    $moHandler->showUsers($maUsers);
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new GridApproversView());
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $miDocumentId = FWDWebLib::getObject("param_document_id")->getValue();
    $moPMDocument = new PMDocument();
    $moPMDocument->testPermissionToRead($miDocumentId);
  
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
?>
<script language="javascript">
  function refresh_grid() {
    js_refresh_grid('grid_approvers_view');
  }
</script>
<?
  }
}

//testa se a licen�a utilizada permite acessar o m�dulo de documenta��o
ISMSLib::testUserPermissionToPoliceMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_approvers_view.xml");
?>