<?php
include_once "include.php";

class SaveEvent extends FWDRunnable {
  public function run() {    
    $miDocumentId = FWDWebLib::getObject('param_document_id')->getValue();
    $moDocument = new PMDocument();
    $moDocument->testPermissionToManage($miDocumentId);
    
    $miApproverId = FWDWebLib::getObject('mainapprover_id')->getValue();
    if ($miApproverId) {
      $moDocument = new PMDocument();
      $moDocument->setFieldValue('document_main_approver',$miApproverId);
      $moDocument->update($miDocumentId);
    }
    echo "soWindow = soPopUpManager.getPopUpById('popup_document_main_approver_edit').getOpener();
          if (soWindow.refresh_grid) soWindow.refresh_grid();
          soWindow = null;
          soPopUpManager.closePopUp('popup_document_main_approver_edit');";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent("")); 
    $moStartEvent->addAjaxEvent(new SaveEvent("save_event"));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $miDocumentId = FWDWebLib::getObject('param_document_id')->getValue();
    $moDocument = new PMDocument();
    $moDocument->testPermissionToManage($miDocumentId);
    $moDocument->fetchById($miDocumentId);
    FWDWebLib::getObject('document_name')->setValue($moDocument->getFieldValue('document_name'));
    $miApproverId = $moDocument->getFieldValue('document_main_approver');
    FWDWebLib::getObject('mainapprover_id')->setValue($miApproverId);
    $moUser = new ISMSUser();
    $moUser->fetchById($miApproverId);
    FWDWebLib::getObject('document_approver')->setValue($moUser->getFieldValue('user_name'));
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
    <script language="javascript">
      function set_user(id, name){
        gebi('mainapprover_id').value = id;
        gobi('document_approver').setValue(name);
        id = name = null;
      }
    </script>
    <? 
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_document_main_approver_edit.xml");
?>