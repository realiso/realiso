<?php

include_once 'include.php';
include_once $handlers_ref . 'grid/policy/QueryGridDocumentSearch.php';

class SearchDrawGrid extends FWDDrawGrid {
	public function drawItem(){
		switch($this->ciColumnIndex){
			case $this->getIndexByAlias('document_id'):{
				$moDocument = new PMDocument();
				$moDocument->fetchById($this->caData[1]);
				$this->coCellBox->setIconSrc($moDocument->getIcon());
				return $this->coCellBox->draw();
				break;
			}
			default:{
				return parent::drawItem();
			}
		}
	}
}

class SearchEvent extends FWDRunnable {
	public function run(){
		$moGrid = FWDWebLib::getObject('grid_search');
		$moGrid->execEventPopulate();
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(''));
		$moStartEvent->addAjaxEvent(new SearchEvent('search_event'));

		$moGrid = FWDWebLib::getObject('grid_search');
		$moHandler = new QueryGridDocumentSearch(FWDWebLib::getConnection());

		if(FWDWebLib::getObject('register_id')->getValue()){
			$moHandler->showAllDocuments(true);
			$moHandler->setIsSearchByRegister(true);
		}

		$moHandler->setName(FWDWebLib::getObject('var_name')->getValue());

		$moGrid->setQueryHandler($moHandler);
		$moGrid->setObjFwdDrawGrid(new SearchDrawGrid());
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
			<script language='javascript'>gebi('name').focus();</script>
		<?
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_document_search.xml');
?>