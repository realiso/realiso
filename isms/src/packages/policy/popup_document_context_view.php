<?php
include_once "include.php";
include_once $handlers_ref."grid/policy/QueryGridContextNames.php";

class GridDocumentContextView extends FWDDrawGrid {
  public function DrawItem(){
    switch($this->ciColumnIndex){
      case 1:
        switch($this->caData[3]) {
          case CONTEXT_AREA:
            $this->coCellBox->setIconSrc("icon-area_gray.gif");
          break;
          case CONTEXT_PROCESS:
            $this->coCellBox->setIconSrc("icon-process_gray.gif");
          break;
          case CONTEXT_ASSET:
            $this->coCellBox->setIconSrc("icon-asset_gray.gif");
          break;
          case CONTEXT_CONTROL:
            $this->coCellBox->setIconSrc("icon-control.gif");
          break;
          case CONTEXT_BEST_PRACTICE:
            $this->coCellBox->setIconSrc("icon-best_practice.gif");
          break;
          default:
            $this->coCellBox->setIconSrc("icon-wrong.gif");
            $this->coCellBox->setValue('');
          break;
        }
        return parent::drawItem();
      case 3:
        $moContext = ISMSContextObject::getContextObject($this->caData[3]);
        if($moContext){
          $this->coCellBox->setValue($moContext->getLabel());
        } else {
          $this->coCellBox->setValue('');
        }
        return parent::drawItem();
      default:
        return parent::drawItem();
      break;
    }
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
    
    $moDocumentContext = new PMDocContext();
    $maContexts = $moDocumentContext->getCurrentContexts(FWDWebLib::getObject('param_document_id')->getValue());
    
    $moGrid = FWDWebLib::getObject("grid_document_context_view");
    $moHandler = new QueryGridContextNames(FWDWebLib::getConnection()); 
    $moHandler->setContextIds($maContexts);
    $moHandler->setCurrentOnly(true);
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new GridDocumentContextView());
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $miDocumentId = FWDWebLib::getObject("param_document_id")->getValue();
    $moPMDocument = new PMDocument();
    $moPMDocument->testPermissionToRead($miDocumentId);
  
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
?>
<script language="javascript">
  function refresh_grid() {
    js_refresh_grid('grid_document_context_view');
  }
</script>
<?
  }
}

//testa se a licen�a utilizada permite acessar o m�dulo de documenta��o
ISMSLib::testUserPermissionToPoliceMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_document_context_view.xml");
?>