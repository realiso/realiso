<?php
include_once "include.php";

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moConfig = new ISMSConfig();
    if($moConfig->getConfigFromDB(CLIENT_ID_ASAAS)) {
        $moISMSASaaS = new ISMSASaaS();
        $moISMSASaaS->setNewCheckPointASaaS(utf8_encode("Acessou M�dulo Gest�o de Pol�ticas"), utf8_encode("M�dulo Gest�o de Pol�ticas"));
    }

    $miContextType = FWDWebLib::getObject('context_type')->getValue();
    $miContextId = FWDWebLib::getObject('context_id')->getValue();
    $mbCreateContext = (bool)FWDWebLib::getObject('create_context')->getValue();

    if ($miContextType) {
      switch ($miContextType) {
        case CONTEXT_DOCUMENT:
        case CONTEXT_CI_OCCURRENCE:
        case CONTEXT_CI_INCIDENT:
        case CONTEXT_CI_NON_CONFORMITY:

          if ($mbCreateContext)
            FWDWebLib::getObject('tab_group')->setAttrLoadTabItemIndex(1);
          else
            FWDWebLib::getObject('tab_group')->setAttrLoadTabItemIndex(4);
          if ($miContextId) {
            $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
            if (!$moSession->attributeExists("tabPolicyManagementContextId"))
              $moSession->addAttribute("tabPolicyManagementContextId");
            $moSession->setAttrTabPolicyManagementContextId($miContextId);
          }
        break;

        case CONTEXT_REGISTER:
          FWDWebLib::getObject('tab_group')->setAttrLoadTabItemIndex(7);
        break;

        default:
          FWDWebLib::getObject('tab_group')->setAttrLoadTabItemIndex(3);
        break;
      }
    }

    //instala a seguran�a de ACL na p�gina
    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());

    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
  }
}
//testa se a licen�a utilizada permite acessar o m�dulo de documenta��o
ISMSLib::testUserPermissionToPoliceMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("tab_policy_management.xml");
?>