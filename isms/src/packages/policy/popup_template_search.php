<?php
include_once "include.php";
include_once $handlers_ref . "grid/policy/QueryGridTemplateSearch.php";

class GridTemplates extends FWDDrawGrid {
  public function drawItem(){
    switch($this->ciColumnIndex){
      case $this->getIndexByAlias('context_id'):
        $this->coCellBox->setIconSrc(ISMSLib::getIcon($this->getFieldValue('template_file_name')));
        return $this->coCellBox->draw();
      break;
      case $this->getIndexByAlias('context_name'):
        $msPath = $this->getFieldValue('template_path');
        if ($msPath && file_exists($msPath)) {
          $miDocumentId = $this->getFieldValue('context_id');
          $miDocInstance = $this->getFieldValue('context_id');
          $this->coCellBox->setAttrStringNoEscape("true");
          $this->coCellBox->setValue("<a href=javascript:read('".$miDocumentId."');>".$this->coCellBox->getValue()."</a>");
        }
        return parent::drawItem();
      break;
      default:
        return parent::drawItem();
      break;
    }
  }
}

class SearchTemplateEvent extends FWDRunnable {
  public function run(){
    $moGrid = FWDWebLib::getObject('grid_template_search');
    $moGrid->execEventPopulate();
  }
}

class DownloadFileEvent extends FWDRunnable {
  public function run() {
    $moWebLib = FWDWebLib::getInstance();
    $miTemplateId = $moWebLib->getObject('read_template_id')->getValue();
    
    $moTemplate = new PMTemplate();
    $moTemplate->fetchById($miTemplateId);
    $msPath = $moTemplate->getFieldValue('template_path');
    $msFileName = $moTemplate->getFieldValue('template_file_name');
    if ($msPath && file_exists($msPath)) {
      header('Cache-Control: ');// leave blank to avoid IE errors
      header('Pragma: ');// leave blank to avoid IE errors
      header('Content-type: application/octet-stream');
      header('Content-Disposition: attachment; filename="'.rawurlencode($msFileName).'"');
      $moCrypt = new FWDCrypt();
      $moFP = fopen($msPath,'rb');
      $moCrypt->setIV(fread($moFP,strlen($moCrypt->getIV())));
      while(!feof($moFP)) {
        echo $moCrypt->decryptNoBase64(fread($moFP,16384));
      }
      fclose($moFP);
    }
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new SearchTemplateEvent('search_template_event'));
    $moStartEvent->addSubmitEvent(new DownloadFileEvent('download_file'));
    
    $moGrid = FWDWebLib::getObject('grid_template_search');
    $moHandler = new QueryGridTemplateSearch(FWDWebLib::getConnection());
    $moHandler->setFilterName(FWDWebLib::getObject('var_filter_name')->getValue());
    $moHandler->setFilterIds(FWDWebLib::getObject('filter_ids')->getValue());
    
    $msTemplateType = FWDWebLib::getObject('var_filter_type')->getValue();

    $moHandler->setTemplateType($msTemplateType);
    if (str_replace(':','',FWDWebLib::getObject('search_type')->getValue()) == 'suggested') {
      $moHandler->showOnlySuggested();
    }
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new GridTemplates());
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){ 
    FWDWebLib::getObject('var_doc_id')->setValue(FWDWebLib::getObject('param_document_id')->getValue());
    
    if(FWDWebLib::getObject('param_type_filter')->getValue()==2799){
      $moCtxObj = new ISMSContextObject();
      $moObj = $moCtxObj->getContextObjectByContextId(FWDWebLib::getObject('var_doc_id')->getValue());
      $moObj->fetchById(FWDWebLib::getObject('var_doc_id')->getValue());
      FWDWebLib::getObject('var_filter_type')->setValue($moObj->getFieldValue('document_type'));
    }elseif(FWDWebLib::getObject('param_type_filter')->getValue()==CONTEXT_SCOPE){
      FWDWebLib::getObject('var_filter_type')->setValue(CONTEXT_SCOPE.",".CONTEXT_POLICY.",".DOCUMENT_TYPE_MANAGEMENT);
    }else{
      FWDWebLib::getObject('var_filter_type')->setValue(FWDWebLib::getObject('param_type_filter')->getValue());
    }
    //obtem os ids dos contextos relacionados ao documento que se quer ver os templates
    $moPMDocContext = new PMDocContext();
    $moPMDocContext->createFilter(FWDWebLib::getObject('var_doc_id')->getValue(),'document_id');
    $moPMDocContext->select();
    $maContexts = array();
    while($moPMDocContext->fetch()) {
      $maContexts[] = $moPMDocContext->getFieldValue('context_id');
    }
    FWDWebLib::getObject('filter_ids')->setValue(implode(':',$maContexts));
    if((FWDWebLib::getObject('param_type_filter')->getValue()==CONTEXT_SCOPE)
       || ( (FWDWebLib::getObject('param_type_filter')->getValue()==2799) && (FWDWebLib::getObject('var_filter_type')->getValue()!=CONTEXT_CONTROL) )
    ){
      FWDWebLib::getObject('st_suggested')->setShouldDraw(false);
      FWDWebLib::getObject('search_type_suggested')->setShouldDraw(false);
      FWDWebLib::getObject('search_type')->setShouldDraw(false);
      FWDWebLib::getObject('grid_template_search')->getObjFWDBox()->setAttrTop(60);
      FWDWebLib::getObject('grid_template_search')->getObjFWDBox()->setAttrHeight(315);
    }
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        gebi('tx_name').focus();
        function read(piTemplateId) {
          gebi('read_template_id').value=piTemplateId;
          js_submit('download_file','ajax');
        }
      </script>
    <?
  }
}
//testa se a licen�a utilizada permite acessar o m�dulo de documenta��o
ISMSLib::testUserPermissionToPoliceMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_template_search.xml");
?>