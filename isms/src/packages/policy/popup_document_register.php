<?php
include_once "include.php";
include_once $handlers_ref."grid/policy/QueryGridAssociatedRegisters.php";
include_once $classes_isms_ref . "nonauto/policy_management/RegistersDrawGrid.php";

class DesassociateRegistersEvent extends FWDRunnable {
  public function run() {
    $miDocumentId = FWDWebLib::getObject('document_id')->getValue();
    
    $moDocument = new PMDocument();
    $moDocument->testPermissionToEdit($miDocumentId);
    
    $msRegisters = FWDWebLib::getObject('selected_registers')->getValue();
    $maRegisters = explode(",",$msRegisters);
    foreach($maRegisters as $miRegisterId) {
      $moPMDocRegister = new PMDocRegisters();
      $moPMDocRegister->createFilter($miRegisterId,'register_id');
      $moPMDocRegister->createFilter($miDocumentId,'document_id');
      $moPMDocRegister->delete();
    }
    echo "refresh_grid();";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new DesassociateRegistersEvent("desassociate_registers"));

    $miDocumentId = FWDWebLib::getObject('param_document_id')->getValue();
    $moGrid = FWDWebLib::getObject('grid_register');
    $moHandler = new QueryGridAssociatedRegisters(FWDWebLib::getConnection());
    $moHandler->setDocumentId($miDocumentId);
    $moGrid->setObjFwdDrawGrid(new RegistersDrawGrid());
    $moGrid->setQueryHandler($moHandler);
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $miDocumentId = $moWebLib->getObject('param_document_id')->getValue();
    
    $moDocument = new PMDocument();
    $moDocument->testPermissionToEdit($miDocumentId);
    
    $moWebLib->getObject('document_id')->setValue($miDocumentId);
    
    $moWebLib->dump_html($moWebLib->getObject('dialog'));
    ?>
    <script language="javascript">
    function refresh_grid() {
      js_refresh_grid('grid_register');
    }
    </script>
    <?
  }
}

//testa se a licen�a utilizada permite acessar o m�dulo de documenta��o
ISMSLib::testUserPermissionToPoliceMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_document_register.xml");

?>