<?php

include_once "include.php";
include_once $handlers_ref . "grid/policy/QueryGridDocumentSearch.php";

function getCurrentIds(){
  $msValues = FWDWebLib::getObject('var_current_ids')->getValue();
  $maIds = array_filter(explode(':', $msValues));
  return $maIds;
}

class DrawGridSearch extends FWDDrawGrid {
  public function drawItem(){
    switch($this->ciColumnIndex){
      case 1:{
        $moDocument = new PMDocument();
        $moDocument->fetchById($this->caData[1]);
        $this->coCellBox->setIconSrc($moDocument->getIcon());
        return parent::drawItem();
      }
      default:{
        return parent::drawItem();
      }
    }
  }
}

class SearchEvent extends FWDRunnable {
  public function run(){
    $moGrid = FWDWebLib::getObject('grid_search');
    $moGrid->execEventPopulate();
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new SearchEvent('search_event'));

    $moGrid = FWDWebLib::getObject('grid_search');
    $moHandler = new QueryGridDocumentSearch(FWDWebLib::getConnection());
    $moHandler->setName(FWDWebLib::getObject('var_name')->getValue());
    $moHandler->setExluded(getCurrentIds());
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new DrawGridSearch());

    $moGrid = FWDWebLib::getObject('grid_current');
    $moHandler = new QueryGridDocumentSearch(FWDWebLib::getConnection());
    $moHandler->showDocuments(getCurrentIds());
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new DrawGridSearch());
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    FWDWebLib::getObject('var_current_ids')->setValue(FWDWebLib::getObject('current_ids')->getValue());

    $moGrid = FWDWebLib::getObject('grid_current');
    $moHandler = new QueryGridDocumentSearch(FWDWebLib::getConnection());
    $moHandler->showDocuments(getCurrentIds());
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new DrawGridSearch());

    //testa se a licen�a utilizada permite acessar o m�dulo de documenta��o
    ISMSLib::testUserPermissionToPoliceMode();

    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        gebi('name').focus();
        
        function refresh_grid(){
          gobi('grid_search').setPopulate(true);
          js_refresh_grid('grid_search');
          gobi('grid_current').setPopulate(true);
          js_refresh_grid('grid_current');
        }
        function enter_search_event(e){
          if(!e) e = window.event;
          if(e['keyCode']==13){
            gebi('var_name').value = gebi('name').value;
            gobi('grid_search').setPopulate(true);
            trigger_event("search_event","3");
          }
        }
        FWDEventManager.addEvent(gebi('name'), 'keydown', enter_search_event);
      </script>
    <?
  }
}

//testa se a licen�a utilizada permite acessar o m�dulo de documenta��o
ISMSLib::testUserPermissionToPoliceMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_documents_search.xml");

?>