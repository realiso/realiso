<?php
include_once "include.php";
include_once $handlers_ref."grid/QueryGridUserSearch.php";
include_once $handlers_ref."grid/policy/QueryGridCurrentUsers.php";

class GridUserSearch extends FWDDrawGrid {
  public function DrawItem(){
    switch($this->ciColumnIndex){
      case 1:
        $msGfxRef = FWDWebLib::getInstance()->getGfxRef();
        $moIcon = new FWDIcon(new FWDBox(2,4));
        $moIcon->setAttrSrc("{$msGfxRef}icon-user.gif");
        return $moIcon->draw();
      default:
        return parent::drawItem();
    }
  }
}

class AssociateApproversEvent extends FWDRunnable {
  public function run(){
    $miDocumentId = FWDWebLib::getObject('document_id')->getValue();
    
    // Testa se o usu�rio tem permiss�o para gerenciar o documento
    $moDocument = new PMDocument();
    $moDocument->testPermissionToManage($miDocumentId);
    
    $msNew = FWDWebLib::getObject('current_approvers_ids')->getValue();
    if($msNew){
      $maNew = explode(':',$msNew);
    }else{
      $maNew = array();
    }
    
    $moDocumentApprover = new PMDocumentApprover();
    $maOld = $moDocumentApprover->getUsers($miDocumentId,true);
    
    $maToInsert = array_diff($maNew,$maOld);
    $maToDelete = array_diff($maOld,$maNew);
    
    //c�digo para testar se o main approver n�o esta sendo incluido ou excluido da tabela de aprovadores
    //do documento 
    $moDocumentTest = new PMDocument();
    $moDocument->fetchById($miDocumentId);
    $miMainApprover = $moDocument->getFieldValue('document_main_approver');
    if((in_array($miMainApprover,$maToInsert)) || (in_array($miMainApprover,$maToDelete)) ){
          trigger_error("Tying to insert or delete the Document Main Approver with Id={$miMainApprover} from de document with Id={$miDocumentId} into the document_aprrover table!".strtolower($this->getLabel()).".",E_USER_ERROR);
    }
    
    $moDocumentApprover = new PMDocumentApprover();
    if(count($maToInsert)) $moDocumentApprover->insertUsers($miDocumentId,$maToInsert);
    if(count($maToDelete)) $moDocumentApprover->deleteUsers($miDocumentId,$maToDelete);
    
    echo "soWindow = soPopUpManager.getPopUpById('popup_approvers_edit').getOpener();
          if (soWindow.refresh_grid) soWindow.refresh_grid();
          soWindow = null;
          soPopUpManager.closePopUp('popup_approvers_edit');";
  }
}

class SearchUserEvent extends FWDRunnable {
  public function run(){
    $moGrid = FWDWebLib::getObject('grid_approvers_search');
    $moGrid->execEventPopulate();
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
    $moStartEvent->addAjaxEvent(new SearchUserEvent('search_user_event'));
    $moStartEvent->addAjaxEvent(new AssociateApproversEvent('associate_approvers_event'));

    $maExcludedUsers = array();    
    
    $miDocumentId = FWDWebLib::getObject('document_id')->getValue();
    if(!$miDocumentId){
      $miDocumentId = FWDWebLib::getObject('param_document_id')->getValue();
      FWDWebLib::getObject('document_id')->setValue($miDocumentId);
    }
    $moDocument = new PMDocument();
    if ($moDocument->fetchById($miDocumentId)){
      $maExcludedUsers[] = $moDocument->getFieldValue('document_main_approver');
      $maExcludedUsers[] = $moDocument->getFieldValue('document_author');
    }

    $moDocumentApprover = new PMDocumentApprover();
    $moDocumentApprover->createFilter($miDocumentId,'document_id');
    $moDocumentApprover->createFilter(true,'is_context_approver');
    $moDocumentApprover->select();
    while($moDocumentApprover->fetch()) {
      $maExcludedUsers[] = $moDocumentApprover->getFieldValue('user_id');
    }
    
    $moSearchGrid = FWDWebLib::getObject('grid_approvers_search');
    $moSearchHandler = new QueryGridUserSearch(FWDWebLib::getConnection());
    $moSearchHandler->setExludedUsers(array_unique($maExcludedUsers));
    $moSearchGrid->setObjFwdDrawGrid(new GridUserSearch());
    
    $moCurrentGrid = FWDWebLib::getObject('grid_current_approvers');
    $moCurrentHandler = new QueryGridCurrentUsers(FWDWebLib::getConnection());
    $moCurrentGrid->setObjFwdDrawGrid(new GridUserSearch());
    
    $moSearchHandler->setName(FWDWebLib::getObject('name_filter')->getValue());
    $moSearchHandler->setLogin(FWDWebLib::getObject('login_filter')->getValue());
    
    $msCurrentUsers = FWDWebLib::getObject('current_approvers_ids')->getValue();
    if($msCurrentUsers){
      $maUsers = explode(':',$msCurrentUsers);
      
      $moSearchHandler->setExludedUsers(array_unique(array_merge($maUsers,$maExcludedUsers)));
      $moCurrentHandler->showUsers($maUsers);
    }else{
      $moCurrentHandler->setEmpty(true);
    }
    
    $moSearchGrid->setQueryHandler($moSearchHandler);
    $moCurrentGrid->setQueryHandler($moCurrentHandler);
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
  
    // Testa se o usu�rio tem permiss�o para gerenciar o documento
    $miDocumentId = FWDWebLib::getObject('document_id')->getValue();
    $moDocument = new PMDocument();
    $moDocument->testPermissionToManage($miDocumentId);
  
    $moDocumentApprover = new PMDocumentApprover();
    $maUsers = $moDocumentApprover->getUsers(FWDWebLib::getObject('param_document_id')->getValue(),true);
    FWDWebLib::getObject('current_approvers_ids')->setValue(implode(':',$maUsers));
    
    $moCurrentHandler = new QueryGridCurrentUsers(FWDWebLib::getConnection());
    $moCurrentHandler->showUsers($maUsers);
    FWDWebLib::getObject('grid_current_approvers')->setQueryHandler($moCurrentHandler);
    
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        gebi('user_login').focus();
      </script>
    <?
  }
}

//testa se a licen�a utilizada permite acessar o m�dulo de documenta��o
ISMSLib::testUserPermissionToPoliceMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_approvers_edit.xml');

?>