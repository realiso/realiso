<?php

include_once "include.php";

class SendToRevisionEvent extends FWDRunnable {
  public function run(){
    $miDocumentId = FWDWebLib::getObject('document_id')->getValue();
    $moDocument = new PMDocument();
    
    $moDocument->testPermissionToManage($miDocumentId);
    
    $moDocument->fetchById($miDocumentId);
    $moDocument->stateForward(WKF_ACTION_DOC_REVISION,FWDWebLib::getObject('justification')->getValue());
    echo "try{
            soPopUpManager.getRootWindow().soTabSubManager.getSelectedWindow().soTabSubManager.getSelectedWindow().refresh_grid();
          }catch(e){e=null}"
        ."soPopUpManager.closePopUp('popup_revision_justification');";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
    $moStartEvent->addAjaxEvent(new SendToRevisionEvent('send_to_revision'));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $miDocumentId = FWDWebLib::getObject('param_document_id')->getValue();
    $miPopupId = FWDWebLib::getObject('param_popup_id')->getValue();
    FWDWebLib::getObject('document_id')->setValue($miDocumentId);
    FWDWebLib::getObject('popup_id')->setValue($miPopupId);
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        gebi('justification').focus();
      </script>
    <?
  }
}

//testa se a licen�a utilizada permite acessar o m�dulo de documenta��o
ISMSLib::testUserPermissionToPoliceMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_revision_justification.xml');

?>