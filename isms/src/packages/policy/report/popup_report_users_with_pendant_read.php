<?php

set_time_limit(3600);
include_once 'include.php';


class ReaderLevelIterator extends FWDReportLevelIterator {



  public function __construct(){
    parent::__construct('doc_reader_id');
    FWDWebLib::getObject('reader_icon')->setAttrSrc(FWDWebLib::getInstance()->getGfxRef().'icon-user.gif');
  }

  public function fetch(FWDDBDataSet $poDataSet){
    $msName = $poDataSet->getFieldByAlias('doc_reader_name')->getValue();
    FWDWebLib::getObject('reader_name')->setValue($msName);
  }
}


class DocumentLevelIterator extends FWDReportLevelIterator {

  protected $csGfxRef;

  public function __construct(){
    parent::__construct('doc_id');
    $this->csGfxRef = FWDWebLib::getInstance()->getGfxRef();
  }

  public function fetch(FWDDBDataSet $poDataSet){
    $msName = $poDataSet->getFieldByAlias('doc_name')->getValue();
    $mbIsLink = $poDataSet->getFieldByAlias('doc_instance_is_link')->getValue();
    $msFileName = $poDataSet->getFieldByAlias('doc_instance_file_name')->getValue();
    
    if($mbIsLink){
      $msIcon = 'icon-link.png';
    }else{
      $msIcon = ISMSLib::getIcon($msFileName);
    }
    FWDWebLib::getObject('document_icon')->setAttrSrc($this->csGfxRef.$msIcon);
    FWDWebLib::getObject('document_name')->setValue($msName);
  }
}



class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moFilter = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter();

    FWDWebLib::getObject('level_document')->setLevelIterator(new DocumentLevelIterator());
    FWDWebLib::getObject('level_reader')->setLevelIterator(new ReaderLevelIterator());
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport(FWDWebLib::getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    
    $moComponent->setReportFilter($moFilter);
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('mx_documents_pendent_reads','Usu�rios com leitura pendente'));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_report_users_with_pendant_read.xml');

?>