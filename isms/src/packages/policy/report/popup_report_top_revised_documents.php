<?php

set_time_limit(3600);
include_once 'include.php';

class DocumentLevelIterator extends FWDReportLevelIterator {

  protected $csGfxRef;

  public function __construct(){
    parent::__construct('doc_id');
    $this->csGfxRef = FWDWebLib::getInstance()->getGfxRef();
  }

  public function fetch(FWDDBDataSet $poDataSet){
    $msName = $poDataSet->getFieldByAlias('doc_name')->getValue();
    $mbIsLink = $poDataSet->getFieldByAlias('curr_is_link')->getValue();
    $msFileName = $poDataSet->getFieldByAlias('curr_file_name')->getValue();
    $miRevisionCount = $poDataSet->getFieldByAlias('doc_rev_count')->getValue();
    
    if($mbIsLink){
      $msIcon = 'icon-link.png';
    }else{
      $msIcon = ISMSLib::getIcon($msFileName);
    }
    FWDWebLib::getObject('document_icon')->setAttrSrc($this->csGfxRef.$msIcon);
    FWDWebLib::getObject('document_name')->setValue($msName);
    FWDWebLib::getObject('document_revision_count')->setValue($miRevisionCount);
  }
}

class RevisionLevelIterator extends FWDReportLevelIterator {

  protected $cbExpand;
  protected $csGfxRef;
  
  public function __construct($pbExpand){
    parent::__construct('rev_id');
    $this->csGfxRef = FWDWebLib::getInstance()->getGfxRef();
    $this->cbExpand = $pbExpand;
  }

  public function fetch(FWDDBDataSet $poDataSet){
    if (ISMSLib::getConfigById(GENERAL_DOCUMENTS_MANUAL_VERSIONING) && $poDataSet->getFieldByAlias('document_version')->getValue()) {
      $msVersion = $poDataSet->getFieldByAlias('document_version')->getValue();
    } else {
      $msVersion = $poDataSet->getFieldByAlias('document_major_version')->getValue().".".$poDataSet->getFieldByAlias('document_revision_version')->getValue();
    }
    $miRevDate = $poDataSet->getFieldByAlias('rev_date')->getValue();
    
    FWDWebLib::getObject('revision_version')->setValue($msVersion);
    FWDWebLib::getObject('revision_date')->setValue(ISMSLib::getISMSShortDate($miRevDate));
  }
  
  public function changedLevel(FWDDBDataSet $poDataSet){
    if($this->cbExpand){
      return parent::changedLevel($poDataSet);
    }else{
      return false;
    }
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moFilter = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter();
    $mbExpand = $moFilter->getExpand();
    
    FWDWebLib::getObject('level_document')->setLevelIterator(new DocumentLevelIterator());
    FWDWebLib::getObject('level_revision')->setLevelIterator(new RevisionLevelIterator($mbExpand));
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport(FWDWebLib::getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    
    $moComponent->setReportFilter(FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter());
    
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('rs_most_revised_documents','Documentos Mais Revisados'));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_report_top_revised_documents.xml');

?>