<?php

set_time_limit(3600);
include_once 'include.php';

class DocumentLevelIterator extends FWDReportLevelIterator {
  
  protected $csGfxRef;
  
  public function __construct(){
    parent::__construct('document_id');
    
    $this->csGfxRef = FWDWebLib::getInstance()->getGfxRef();
  }
  
  public function fetch(FWDDBDataSet $poDataSet){
    FWDWebLib::getObject('document_name')->setValue($poDataSet->getFieldByAlias('document_name')->getValue());
    FWDWebLib::getObject('user_name')->setValue($poDataSet->getFieldByAlias('user_name')->getValue());
    
    $mbIsLink = $poDataSet->getFieldByAlias('doc_instance_is_link')->getValue();
    $msFileName = $poDataSet->getFieldByAlias('doc_instance_file_name')->getValue();
    if($mbIsLink){
      $msIcon = 'icon-link.png';
    }else{
      $msIcon = ISMSLib::getIcon($msFileName);
    }
    FWDWebLib::getObject('document_id')->setAttrSrc($this->csGfxRef.$msIcon);
    FWDWebLib::getObject('user_id')->setAttrSrc($this->csGfxRef .'icon-user.gif');
  }
  
}

class RevisionLevelIterator extends FWDReportLevelIterator {

  public function __construct(){
    parent::__construct('doc_date_revision');
  }

  public function fetch(FWDDBDataSet $poDataSet){
    $msDateRevision = ISMSLib::getISMSShortDate($poDataSet->getFieldByAlias('doc_date_revision')->getValue());
    $msJustification = $poDataSet->getFieldByAlias('doc_revision_justification')->getValue();
    FWDWebLib::getObject('revision_date')->setValue($msDateRevision);
    FWDWebLib::getObject('revision_justification')->setValue($msJustification);
  }

}


class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moFilter = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter();
    
    FWDWebLib::getObject('document_level')->setLevelIterator(new DocumentLevelIterator());
    FWDWebLib::getObject('level_revision')->setLevelIterator(new RevisionLevelIterator());
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport(FWDWebLib::getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    
    $moComponent->setReportFilter($moFilter);
    
    //FWDWebLib::getObject('header_document_name')->setValue(FWDLanguage::getPHPStringValue('rs_document_name_bl','Documento'));
   // FWDWebLib::getObject('header_responsible_name')->setValue(FWDLanguage::getPHPStringValue('rs_document_main_approver_name','Respons�vel pelo documento'));
    
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('tt_docs_with_high_frequency_revision','Documentos com uma alta frequ�ncia de revis�o'));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_report_docs_with_high_frequency_revision.xml');

?>