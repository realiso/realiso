<?php

set_time_limit(3600);
include_once 'include.php';

class DocumentLevelIterator extends FWDReportLevelIterator {
  
  protected $csGfxRef;
  
  public function __construct(){
    parent::__construct('document_id');
    $this->csGfxRef = FWDWebLib::getInstance()->getGfxRef();
  }
  
  public function fetch(FWDDBDataSet $poDataSet){
    $mbIsLink = $poDataSet->getFieldByAlias('document_is_link')->getValue();
    $msFileName = $poDataSet->getFieldByAlias('document_file_name')->getValue();
    $miScheduleId = $poDataSet->getFieldByAlias('document_schedule_id')->getValue();
    
    $moSchedule = new WKFSchedule();
    $moSchedule->fetchById($miScheduleId);
    $msSchedule = $moSchedule->getTextDescription();
    
    if($mbIsLink){
      $msIcon = 'icon-link.png';
    }else{
      $msIcon = ISMSLib::getIcon($msFileName);
    }
    FWDWebLib::getObject('document_icon')->setAttrSrc($this->csGfxRef.$msIcon);
    FWDWebLib::getObject('document_name')->setValue($poDataSet->getFieldByAlias('document_name')->getValue());
    FWDWebLib::getObject('document_schedule')->setValue($msSchedule);
    FWDWebLib::getObject('document_next_revision')->setValue(FWDWebLib::getShortDate($poDataSet->getFieldByAlias('document_next_revision')->getValue()));
  }
  
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moFilter = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter();
    
    FWDWebLib::getObject('document_level')->setLevelIterator(new DocumentLevelIterator());
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport(FWDWebLib::getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    
    $moComponent->setReportFilter($moFilter);
    
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('mx_documents_revision_late','Documentos com Revis�o Atrasada'));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_report_documents_revision_late.xml');

?>