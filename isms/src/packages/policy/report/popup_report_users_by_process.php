<?php

set_time_limit(3600);
include_once 'include.php';

class AreaLevelIterator extends FWDReportLevelIterator {

  protected $csGfxRef;

  public function __construct(){
    parent::__construct('area_id');
    $this->csGfxRef = FWDWebLib::getInstance()->getGfxRef();
  }

  public function fetch(FWDDBDataSet $poDataSet){
    $msName = $poDataSet->getFieldByAlias('area_name')->getValue();
    $miValue = $poDataSet->getFieldByAlias('area_value')->getValue();
    
    $msIcon = "{$this->csGfxRef}icon-area_".RMRiskConfig::getRiskColor($miValue).'.png';
    
    FWDWebLib::getObject('area_icon')->setAttrSrc($msIcon);
    FWDWebLib::getObject('area_name')->setValue($msName);
  }

}

class ProcessLevelIterator extends FWDReportLevelIterator {

  protected $csGfxRef;

  public function __construct(){
    parent::__construct('process_id');
    $this->csGfxRef = FWDWebLib::getInstance()->getGfxRef();
  }

  public function fetch(FWDDBDataSet $poDataSet){
    $msName = $poDataSet->getFieldByAlias('process_name')->getValue();
    $miValue = $poDataSet->getFieldByAlias('process_value')->getValue();
    
    $msIcon = "{$this->csGfxRef}icon-process_".RMRiskConfig::getRiskColor($miValue).'.png';
    
    FWDWebLib::getObject('process_icon')->setAttrSrc($msIcon);
    FWDWebLib::getObject('process_name')->setValue($msName);
  }

}

class UserLevelIterator extends FWDReportLevelIterator {

  public function __construct(){
    parent::__construct('user_id');
    FWDWebLib::getObject('user_icon')->setAttrSrc(FWDWebLib::getInstance()->getGfxRef().'icon-user.gif');
  }

  public function fetch(FWDDBDataSet $poDataSet){
    $msName = $poDataSet->getFieldByAlias('user_name')->getValue();
    FWDWebLib::getObject('user_name')->setValue($msName);
  }

}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moFilter = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter();
    if($moFilter->getOrganizeByArea()){
      FWDWebLib::getInstance()->xml_load('popup_report_users_by_process_and_area.xml',false);
      FWDWebLib::getObject('level_area')->setLevelIterator(new AreaLevelIterator());
    }else{
      FWDWebLib::getInstance()->xml_load('popup_report_users_by_process.xml',false);
    }
    FWDWebLib::getObject('level_process')->setLevelIterator(new ProcessLevelIterator());
    FWDWebLib::getObject('level_user')->setLevelIterator(new UserLevelIterator());
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport(FWDWebLib::getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    $moComponent->setReportFilter($moFilter);
    
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('rs_users_per_process','Usu�rios associados aos Processos'));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
/*
 * Ao inv�s do xml_load ser chamado aqui, ele � chamado
 * baseado no filtro do relat�rio que o usu�rio escolheu.
 * Com isso, � necess�rio chamar a fun��o start() manualmente.
 */
FWDStartEvent::getInstance()->start();

?>