<?php

set_time_limit(3600);
include_once 'include.php';

class DocumentLevelIterator extends FWDReportLevelIterator {

  protected $csGfxRef;

  public function __construct(){
    parent::__construct('doc_id');
    $this->csGfxRef = FWDWebLib::getInstance()->getGfxRef();
  }

  public function fetch(FWDDBDataSet $poDataSet){
    $msName = $poDataSet->getFieldByAlias('doc_name')->getValue();
    $mbIsLink = $poDataSet->getFieldByAlias('doc_instance_is_link')->getValue();
    $msFileName = $poDataSet->getFieldByAlias('doc_instance_file_name')->getValue();
    $msMainApprover = $poDataSet->getFieldByAlias('doc_main_approver_name')->getValue();
    $msAuthor = $poDataSet->getFieldByAlias('doc_author_name')->getValue();
    
    if($mbIsLink){
      $msIcon = 'icon-link.png';
    }else{
      $msIcon = ISMSLib::getIcon($msFileName);
    }
    FWDWebLib::getObject('document_icon')->setAttrSrc($this->csGfxRef.$msIcon);
    FWDWebLib::getObject('document_name')->setValue($msName);
    FWDWebLib::getObject('document_main_approver')->setValue($msMainApprover);
    FWDWebLib::getObject('document_author')->setValue($msAuthor);
  }
}

class ReaderLevelIterator extends FWDReportLevelIterator {

  public function __construct(){
    parent::__construct('doc_reader_id');
    FWDWebLib::getObject('reader_icon')->setAttrSrc(FWDWebLib::getInstance()->getGfxRef().'icon-user.gif');
  }

  public function fetch(FWDDBDataSet $poDataSet){
    $msName = $poDataSet->getFieldByAlias('doc_reader_name')->getValue();
    FWDWebLib::getObject('reader_name')->setValue($msName);
  }

}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    FWDWebLib::getObject('level_document')->setLevelIterator(new DocumentLevelIterator());
    FWDWebLib::getObject('level_reader')->setLevelIterator(new ReaderLevelIterator());
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport(FWDWebLib::getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    
    $moComponent->setReportFilter(FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter());
    
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('rs_readers','Documentos e seus Leitores'));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_report_document_readers.xml');

?>