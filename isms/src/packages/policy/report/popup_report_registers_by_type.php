<?php
set_time_limit(3600);
include_once "include.php";

class TypeLevelIterator extends FWDReportLevelIterator {  

  public function __construct(){
    parent::__construct("class_id");
  }
	
  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();    
    $moWebLib->getObject('type_name')->setValue($poDataSet->getFieldByAlias("class_name")->getValue());
  }
}

class RegisterLevelIterator extends FWDReportLevelIterator {

  public function __construct(){
    parent::__construct("reg_id");
  }

  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $moWebLib->getObject('register_name')->setValue($poDataSet->getFieldByAlias("reg_name")->getValue());
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();    
    $moWebLib->getObject("level_type")->setLevelIterator(new TypeLevelIterator());
    $moWebLib->getObject("level_register")->setLevelIterator(new RegisterLevelIterator());
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport($moWebLib->getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    $moComponent->setReportFilter($moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter());
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('rs_registers_by_type', "Registros por Tipo"));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_report_registers_by_type.xml");
?>