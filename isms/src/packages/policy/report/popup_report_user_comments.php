<?php

set_time_limit(3600);
include_once 'include.php';

class DocumentLevelIterator extends FWDReportLevelIterator {
  
  public function __construct(){
    parent::__construct('document_id');
    FWDWebLib::getObject('document_icon')->setAttrSrc(FWDWebLib::getInstance()->getGfxRef().'icon-document.gif');
  }
  
  public function fetch(FWDDBDataSet $poDataSet){
    FWDWebLib::getObject('document_name')->setValue($poDataSet->getFieldByAlias('document_name')->getValue());
  }
  
}
class InstanceLevelIterator extends FWDReportLevelIterator {
  
  private $cbManualVersioningEnabled;
  private $csGfxRef;
  
  public function __construct(){
    parent::__construct('instance_id');
    $this->csGfxRef = FWDWebLib::getInstance()->getGfxRef();
    $this->cbManualVersioningEnabled = ISMSLib::getConfigById(GENERAL_DOCUMENTS_MANUAL_VERSIONING);
  }
  
  private function getVersion(FWDDBDataSet $poDataSet){
    $msVersion = '';
    if($this->cbManualVersioningEnabled){
      $msVersion = $poDataSet->getFieldByAlias('instance_manual_version')->getValue();
    } 
    if(!$msVersion){
      $miMainVerion = intval($poDataSet->getFieldByAlias('instance_major_version')->getValue());
      $miSubVersion = intval($poDataSet->getFieldByAlias('instance_revision_version')->getValue());
      if(!$miMainVerion) $miMainVerion = '0';
      if(!$miSubVersion) $miSubVersion = '0';
      $msVersion = $miMainVerion.'.'.$miSubVersion;
    }
    return $msVersion;
  }
  
  public function fetch(FWDDBDataSet $poDataSet){
    $msVersion = $this->getVersion($poDataSet);
    $mbIsLink = $poDataSet->getFieldByAlias('instance_is_link')->getValue();
    $msFileName = $poDataSet->getFieldByAlias('instance_file_name')->getValue();
    
    if($mbIsLink){
      $msIcon = 'icon-link.png';
    }else{
      $msIcon = ISMSLib::getIcon($msFileName);
    }
    
    FWDWebLib::getObject('instance_icon')->setAttrSrc($this->csGfxRef.$msIcon);
    FWDWebLib::getObject('instance_version')->setValue($msVersion);
  }
  
}
class CommentLevelIterator extends FWDReportLevelIterator {
  
  public function __construct(){
    parent::__construct('comment_date');
  }
  
  public function fetch(FWDDBDataSet $poDataSet){
    FWDWebLib::getObject('comment_date')->setValue(FWDWebLib::getDate($poDataSet->getFieldByAlias('comment_date')->getValue()));
    FWDWebLib::getObject('comment_user')->setValue($poDataSet->getFieldByAlias('comment_user')->getValue());
    FWDWebLib::getObject('comment_text')->setValue($poDataSet->getFieldByAlias('comment_text')->getValue());
  }
  
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moFilter = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter();
    
    FWDWebLib::getObject('document_level')->setLevelIterator(new DocumentLevelIterator());
    
    FWDWebLib::getObject('instance_level')->setLevelIterator(new InstanceLevelIterator());
    
    FWDWebLib::getObject('comment_level')->setLevelIterator(new CommentLevelIterator());
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport(FWDWebLib::getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    
    $moComponent->setReportFilter($moFilter);
    
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('mx_user_comments','Comentários de Usuários'));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_report_user_comments.xml');

?>