<?php

set_time_limit(3600);
include_once 'include.php';

class DocumentLevelIterator extends FWDReportLevelIterator {

  protected $csGfxRef;

  public function __construct(){
    parent::__construct('document_id');
    $this->csTypelessString = FWDLanguage::getPHPStringValue('rs_no_type','Sem tipo');
    $this->csGfxRef = FWDWebLib::getInstance()->getGfxRef();
  }

  public function fetch(FWDDBDataSet $poDataSet){
    $msName = $poDataSet->getFieldByAlias('document_name')->getValue();
    $mbIsLink = $poDataSet->getFieldByAlias('doc_instance_is_link')->getValue();
    $msFileName = $poDataSet->getFieldByAlias('doc_instance_file_name')->getValue();
    
    if($mbIsLink){
      $msIcon = 'icon-link.png';
    }else{
      $msIcon = ISMSLib::getIcon($msFileName);
    }
    FWDWebLib::getObject('document_icon')->setAttrSrc($this->csGfxRef.$msIcon);
    FWDWebLib::getObject('document_name')->setValue($msName);
  }
}

class UserLevelIterator extends FWDReportLevelIterator {

  public function __construct(){
    parent::__construct("user_id");
  }

 /**
  * Verifica se mudou o n�vel.
  *
  * <p>M�todo para verificar se houve mudan�a de n�vel.</p>
  * @access public
  * @return boolean Verdadeiro ou falso
  */
  public function changedLevel(FWDDBDataSet $poDataSet){
    return true;
  }

  public function fetch(FWDDBDataSet $poDataSet){
    $msGfxRef = FWDWebLib::getInstance()->getGfxRef();
    $msUserName = $poDataSet->getFieldByAlias("user_name")->getValue();
    $msIcon = $msGfxRef . 'icon-user.png';
    $msDate = $poDataSet->getFieldByAlias('access_date')->getValue();
    $msTime = date('H:i:s',strtotime($msDate));
    $msDate = ISMSLib::getISMSShortDate($msDate);
    
    FWDWebLib::getObject('user_icon')->setAttrSrc($msIcon);
    FWDWebLib::getObject('user_name')->setValue($msUserName);
    FWDWebLib::getObject('access_date')->setValue($msDate);
    FWDWebLib::getObject('access_time')->setValue($msTime);
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    FWDWebLib::getObject('level_user')->setLevelIterator(new UserLevelIterator());
    FWDWebLib::getObject('level_document')->setLevelIterator(new DocumentLevelIterator());
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport(FWDWebLib::getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    //*
    $moComponent->setReportFilter(FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter());
    /*/
    $moFilter = new ISMSReportDocumentAccessesFilter();
    $moFilter->setDocument(3508);
    $moFilter->setShowAllReads(true);
    $moFilter->setInitialDate('2007-05-01');
    $moFilter->setFinalDate('2007-05-15 16:15:00');
    $moComponent->setReportFilter($moFilter);
    //*/
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('rs_accesses_to_documents', "Auditoria de Acesso aos Documentos"));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_report_document_accesses.xml');

?>