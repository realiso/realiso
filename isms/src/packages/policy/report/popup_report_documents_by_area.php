<?php

set_time_limit(3600);
include_once 'include.php';

class DummyLevelIterator extends FWDReportLevelIterator {
  public function __construct(){}
  public function changedLevel(FWDDBDataSet $poDataSet){return false;}
  public function fetch(FWDDBDataSet $poDataSet){}
}

class AreaLevelIterator extends FWDReportLevelIterator {

  protected $csGfxRef;
  protected $ciAreaNameDefaultWidth;
  protected $ciDocumentNameDefaultWidth;

  public function __construct(){
    parent::__construct('area_id');
    $this->csGfxRef = FWDWebLib::getInstance()->getGfxRef();
    $this->ciAreaNameDefaultWidth = FWDWebLib::getObject('area_name')->getAttrWidth();
    $this->ciDocumentNameDefaultWidth = FWDWebLib::getObject('document_name')->getAttrWidth();
  }

  public function fetch(FWDDBDataSet $poDataSet){
    $msAreaName = $poDataSet->getFieldByAlias('area_name')->getValue();
    $miAreaValue = $poDataSet->getFieldByAlias('area_value')->getValue();
    $miAreaDocumentsCount = $poDataSet->getFieldByAlias('doc_count')->getValue();
    $miLevel = $poDataSet->getFieldByAlias('area_level')->getValue() - 1;
    
    $msIcon = $this->csGfxRef . 'icon-area_' . RMRiskConfig::getRiskColor($miAreaValue) . '.png';
    
    $miOffset = 20 * $miLevel;
    
    FWDWebLib::getObject('level_area')->setAttrOffset($miOffset);
    FWDWebLib::getObject('area_icon')->setAttrSrc($msIcon);
    FWDWebLib::getObject('area_name')->setValue($msAreaName);
    FWDWebLib::getObject('area_name')->setAttrWidth($this->ciAreaNameDefaultWidth - $miOffset);
    FWDWebLib::getObject('document_name')->setAttrWidth($this->ciDocumentNameDefaultWidth - $miOffset);
    FWDWebLib::getObject('area_documents_count')->setValue($miAreaDocumentsCount);
  }

}

class DocumentLevelIterator extends FWDReportLevelIterator {

  protected $cbExpand;
  protected $csGfxRef;

  public function __construct($pbExpand){
    parent::__construct('document_id');
    $this->csGfxRef = FWDWebLib::getInstance()->getGfxRef();
    $this->cbExpand = $pbExpand;
  }

  public function fetch(FWDDBDataSet $poDataSet){
    $msName = $poDataSet->getFieldByAlias('document_name')->getValue();
    $mbIsLink = $poDataSet->getFieldByAlias('doc_instance_is_link')->getValue();
    $msFileName = $poDataSet->getFieldByAlias('doc_instance_file_name')->getValue();
    
    if($mbIsLink){
      $msIcon = 'icon-link.png';
    }else{
      $msIcon = ISMSLib::getIcon($msFileName);
    }
    FWDWebLib::getObject('document_icon')->setAttrSrc($this->csGfxRef.$msIcon);
    FWDWebLib::getObject('document_name')->setValue($msName);
  }
  
  public function changedLevel(FWDDBDataSet $poDataSet){
    if($this->cbExpand){
      return parent::changedLevel($poDataSet);
    }else{
      return false;
    }
  }

}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moFilter = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter();
    $mbExpand = $moFilter->getExpand();
    
    $moAreaLevel = FWDWebLib::getObject('level_area');
    $moAreaLevel->setLevelIterator(new AreaLevelIterator());
    FWDWebLib::getObject('level_document')->setLevelIterator(new DocumentLevelIterator($mbExpand));
    FWDWebLib::getObject('level_dummy')->setLevelIterator(new DummyLevelIterator($mbExpand));
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport(FWDWebLib::getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    
    if(!$moFilter->getExpand()){
      $moAreaLevel->setAttrBottomMargin(0);
      FWDWebLib::getObject('body_area')->setAttrBgColor('E0E0E0');
      FWDWebLib::getObject('body_area')->setAttrBgColor2('F7F7F7');
    }
    
    $moComponent->setReportFilter($moFilter);
    
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('rs_documents_per_area','Documentos por �rea'));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_report_documents_by_area.xml');

?>