<?php

set_time_limit(3600);
include_once 'include.php';

class DocLevelIterator extends FWDReportLevelIterator {
  
  public function __construct(){
    parent::__construct('doc_id');
  }
  
  public function fetch(FWDDBDataSet $poDataSet){
    $msFilename = $poDataSet->getFieldByAlias('doc_filename')->getValue();
    $mbIsLink = $poDataSet->getFieldByAlias('doc_is_link')->getValue();
    
    if($mbIsLink) $msIcon = 'icon-link.png';
    else $msIcon = ISMSLib::getIcon($msFilename);
    
    FWDWebLib::getObject('doc_icon')->setAttrSrc(FWDWebLib::getInstance()->getGfxRef().$msIcon);
    FWDWebLib::getObject('doc_name')->setValue($poDataSet->getFieldByAlias('doc_name')->getValue());
  }
  
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moFilter = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter();
    
    FWDWebLib::getObject('doc_level')->setLevelIterator(new DocLevelIterator());
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport(FWDWebLib::getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    
    $moComponent->setReportFilter($moFilter);
    
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('tt_pendant_approvals','Aprovações Pendentes'));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_report_pendant_approvals.xml');

?>