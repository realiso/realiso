<?php

set_time_limit(3600);
include_once 'include.php';

class DocumentLevelIterator extends FWDReportLevelIterator {

  protected $csGfxRef;

  public function __construct(){
    parent::__construct('doc_id');
    
    $this->csGfxRef = FWDWebLib::getInstance()->getGfxRef();
  }

  public function fetch(FWDDBDataSet $poDataSet){
    $msName = $poDataSet->getFieldByAlias('doc_name')->getValue();
    $mbIsLink = $poDataSet->getFieldByAlias('doc_instance_is_link')->getValue();
    $msFileName = $poDataSet->getFieldByAlias('doc_instance_file_name')->getValue();
    
    if($mbIsLink){
      $msIcon = 'icon-link.png';
    }else{
      $msIcon = ISMSLib::getIcon($msFileName);
    }
    FWDWebLib::getObject('document_icon')->setAttrSrc($this->csGfxRef.$msIcon);
    FWDWebLib::getObject('document_name')->setValue($msName);
  }
}

class ReaderLevelIterator extends FWDReportLevelIterator {

  protected $cbExpand;

  public function __construct($pbExpand){
    parent::__construct('doc_reader_id');
    FWDWebLib::getObject('reader_icon')->setAttrSrc(FWDWebLib::getInstance()->getGfxRef().'icon-user.gif');
    $this->cbExpand = $pbExpand;
  }

  public function fetch(FWDDBDataSet $poDataSet){
    $msName = $poDataSet->getFieldByAlias('doc_reader_name')->getValue();
    FWDWebLib::getObject('reader_name')->setValue($msName);
  }

  public function changedLevel(FWDDBDataSet $poDataSet){
    if($this->cbExpand){
      return parent::changedLevel($poDataSet);
    }else{
      return false;
    }
  }

}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moFilter = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter();
    $mbExpand = $moFilter->getExpand();
  
    $moDocumentLevel = FWDWebLib::getObject('level_document');
    $moDocumentLevel->setLevelIterator(new DocumentLevelIterator());
    FWDWebLib::getObject('level_reader')->setLevelIterator(new ReaderLevelIterator($mbExpand));
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport(FWDWebLib::getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    
    if(!$moFilter->getExpand()){
      $moDocumentLevel->setAttrBottomMargin(0);
      FWDWebLib::getObject('body_document')->setAttrBgColor('E0E0E0');
      FWDWebLib::getObject('body_document')->setAttrBgColor2('F7F7F7');
    }
    $moComponent->setReportFilter($moFilter);
    
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('rs_not_accessed_documents','Documentos N�o Acessados'));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_report_not_accessed_documents.xml');

?>