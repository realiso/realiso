<?php

include_once "include.php";

class PublishEvent extends FWDRunnable {
  public function run(){
    $miDocumentId = FWDWebLib::getObject('param_document_id')->getValue();
    
    $moDocument = new PMDocument();
    $moDocument->testPermissionToEdit($miDocumentId);
    
    $msPopupId = FWDWebLib::getObject('param_popup_id')->getValue();
    
    $msComment = FWDWebLib::getObject('comment')->getValue();
    $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    $moDocument = new PMDocument();
    $moDocument->fetchById($miDocumentId);
    $moDocument->stateForward(WKF_ACTION_SEND_DOC_TO_APPROVE,$msComment);
    if($moDocument->isApprover($miUserId)){
      $moDocument->fetchById($miDocumentId);
      $moDocument->stateForward(WKF_ACTION_APPROVE);
    }
    echo "soPopUpManager.closePopUp('popup_modification_comment');";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
    $moStartEvent->addAjaxEvent(new PublishEvent('publish'));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $msPopupId = FWDWebLib::getObject('param_popup_id')->getValue();

    $moDocument = new PMDocument();
    $moDocument->fetchById(FWDWebLib::getObject('param_document_id')->getValue());

    // o usuario tem permiss�o pra salvar?
    $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
	if($moDocument->getResponsible() != $miUserId){
	    $btnSave = FWDWebLib::getObject('btn_save');
	    $btnSave->setAttrDisabled(true);
	}    
    
    $moDocInstance = new PMDocInstance();
    $moDocInstance->fetchById($moDocument->getFieldValue('document_current_version'));
    
    if( intval($moDocInstance->getFieldValue('doc_instance_major_version')) < 1){
      FWDWebLib::getObject('popup_title_comment')->setValue( FWDLanguage::getPHPStringValue('tt_popup_modification_comment_creation','Coment�rio de Cria��o') );
    }
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        gebi('comment').focus();
        
        soPopUpManager.getPopUpById('popup_modification_comment').setCloseEvent(
          function(){
            if (soPopUpManager.getPopUpById('popup_document_edit<?=$msPopupId?>')) soPopUpManager.closePopUp('popup_document_edit<?=$msPopupId?>');
            else {
              soWindow = soPopUpManager.getPopUpById('popup_modification_comment').getOpener();
              if (soWindow.refresh_grid) soWindow.refresh_grid();
              soWindow = null;
            }
          }
        );
      </script>
    <?
  }
}

//testa se a licen�a utilizada permite acessar o m�dulo de documenta��o
ISMSLib::testUserPermissionToPoliceMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_modification_comment.xml');

?>