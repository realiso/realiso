<?php
include_once "include.php";
include_once $handlers_ref . "grid/policy/QueryGridDocuments.php";
include_once $handlers_ref . "select/policy/QuerySelectDocumentType.php";
include_once $classes_isms_ref . "nonauto/policy_management/DocumentsDrawGrid.php";

class FilterEvent extends FWDRunnable {
  public function run() {
    $moGrid = FWDWebLib::getObject('grid_developing');
    $moGrid->execEventPopulate(); 
  }
}

class DocumentConfirmRemove extends FWDRunnable {
  public function run(){
    $msTitle = FWDLanguage::getPHPStringValue('tt_remove_document','Remover Documento');
    $msMessage = FWDLanguage::getPHPStringValue('st_document_confirm_remove',"Voc� tem certeza de que deseja remover o documento <b>%document_name%</b>?");
    
    $moPMDocument = new PMDocument();
    $moPMDocument->fetchById(FWDWebLib::getObject('selected_document_id')->getValue());
    $msDocumentName = ISMSLib::truncateString($moPMDocument->getFieldValue('document_name'), 70);
    $msMessage = str_replace("%document_name%",substr(str_replace('\\','\\\\',$msDocumentName),0,100),$msMessage);
    
    $msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
                    "soWindow = soPopUp.getOpener();" .
                    "soWindow.remove_document();";
    
    ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,50);
  }
}

class RemoveDocumentEvent extends FWDRunnable {
  public function run(){    
    $miDocumentId = FWDWebLib::getObject('selected_document_id')->getValue();
    $moPMDocument = new PMDocument();
    $moPMDocument->delete($miDocumentId);
  }
}

class DownloadFileEvent extends FWDRunnable {
  public function run() {
    $moWebLib = FWDWebLib::getInstance();
    $miDocumentId = $moWebLib->getObject('read_document_id')->getValue();
    $miUserId = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    $miDocInstanceId = $moWebLib->getObject('read_doc_instance_id')->getValue();
    
    $moDocument = new PMDocument();
    $moDocument->testPermissionToRead($miDocumentId);
    
    $moReader = new PMDocumentReader();
    $moReader->createFilter($miDocumentId,'document_id');
    $moReader->createFilter($miUserId,'user_id');
    $moReader->select();
    if ($moReader->fetch()) {
      $moReader->setFieldValue('user_has_read_document',1);
      $moReader->update();
    } else {
      //o usuario nao esta na lista de leitores
    }
    
    $moPMDocReadHistory = new PMDocReadHistory();
    $moPMDocReadHistory->setFieldValue('user_id',$miUserId);
    $moPMDocReadHistory->setFieldValue('document_id',$miDocInstanceId);
    $moPMDocReadHistory->setFieldValue('read_date',ISMSLib::ISMSTime());
    $moPMDocReadHistory->insert();
    
    $moPMDocument = new PMDocument();
    
    if($moPMDocument->fetchById($miDocumentId)){
      $moPMDocInstance = new PMDocInstance();
      if ($moPMDocInstance->fetchById($miDocInstanceId)) {
        $msPath = $moPMDocInstance->getFieldValue('doc_instance_path');
        $msFileName = $moPMDocInstance->getFieldValue('doc_instance_file_name');
        header('Cache-Control: ');// leave blank to avoid IE errors
        header('Pragma: ');// leave blank to avoid IE errors
        header('Content-type: application/octet-stream');
        header('Content-Disposition: attachment; filename="'.rawurlencode($msFileName).'"');
        $moCrypt = new FWDCrypt();
        $moFP = fopen($msPath,'rb');
        $moCrypt->setIV(fread($moFP,strlen($moCrypt->getIV())));
        while(!feof($moFP)) {
          echo $moCrypt->decryptNoBase64(fread($moFP,16384));
        }
        fclose($moFP);
      }
    }
  }
}

class VisitLinkEvent extends FWDRunnable {
  public function run() {
    $moWebLib = FWDWebLib::getInstance();
    $moReader = new PMDocumentReader();
    $miDocInstanceId = $moWebLib->getObject('read_doc_instance_id')->getValue();
    
    $miDocumentId = $moWebLib->getObject('read_document_id')->getValue();
    $miUserId = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    
    $moDocument = new PMDocument();
    $moDocument->testPermissionToRead($miDocumentId);
    
    $moReader->createFilter($miDocumentId,'document_id');
    $moReader->createFilter($miUserId,'user_id');
    $moReader->select();
    if ($moReader->fetch()) {
      $moReader->setFieldValue('user_has_read_document',1);
      $moReader->update();
    } else {
      //o usuario nao esta na lista de leitores
    }
    
    $moPMDocReadHistory = new PMDocReadHistory();
    $moPMDocReadHistory->setFieldValue('user_id',$miUserId);
    $moPMDocReadHistory->setFieldValue('document_id',$miDocInstanceId);
    $moPMDocReadHistory->setFieldValue('read_date',ISMSLib::ISMSTime());
    $moPMDocReadHistory->insert();
    
    $moPMDocInstance = new PMDocInstance();
    if ($moPMDocInstance->fetchById($miDocInstanceId))
      $msURL = $moPMDocInstance->getFieldValue('doc_instance_link');
    if ($msURL)
      echo "js_open('{$msURL}','document_link',740,502,'center_screen');";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new DocumentConfirmRemove('document_confirm_remove'));
    $moStartEvent->addAjaxEvent(new RemoveDocumentEvent('remove_document'));
    $moStartEvent->addAjaxEvent(new FilterEvent('filter_all'));
    $moStartEvent->addSubmitEvent(new DownloadFileEvent('download_file'));
    $moStartEvent->addAjaxEvent(new VisitLinkEvent('visit_link_event'));
    
    //instala a seguran�a de ACL na p�gina
    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
    FWDMenuACLSecurity::getInstance()->installMenuACL(FWDWebLib::getObject('menu'),FWDACLSecurity::getInstance());
    
    $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    if ($moSession->attributeExists("tabPolicyManagementContextId")) {
      $miContextId = $moSession->getAttrTabPolicyManagementContextId();
      //$moSession->deleteAttribute("tabPolicyManagementContextId");
    } else {
      $miContextId=0; 
    }
    $miContextId = FWDWebLib::getObject('context_id')->getValue();
    
    $moGrid = FWDWebLib::getObject("grid_developing");
    $moHandler = new QueryGridDocuments(FWDWebLib::getConnection());
    $miType = (int) FWDWebLib::getObject('filter_type')->getValue();
    if($miType) $moHandler->setTypesToShow($miType);
    if($miContextId) $moHandler->setContextId($miContextId);
    $moHandler->setTypesToHide(CONTEXT_REGISTER);
    $moHandler->setDocumentClassification((int) FWDWebLib::getObject('filter_classification')->getValue());
    /*filtro fixo pelo status do documento*/
    $moHandler->setDocumentStatus(CONTEXT_STATE_DOC_DEVELOPING);    
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new DocumentsDrawGrid('grid_developing'));
  }
}


class ScreenEvent extends FWDRunnable {
  public function run(){
    $msGridDevelopingTitle = FWDLanguage::getPHPStringValue('tt_developing_document_bl','<b>Documentos Em Desenvolvimento</b>');
    $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    if ($moSession->attributeExists("tabPolicyManagementContextId")) {
      $miContextId = $moSession->getAttrTabPolicyManagementContextId();
      $moSession->deleteAttribute("tabPolicyManagementContextId");
      FWDWebLib::getObject('context_id')->setValue($miContextId);
      $moContextObject = new ISMSContextObject();
      $moContext = $moContextObject->getContextObjectByContextId($miContextId);
      $moContext->fetchById($miContextId);
      $msContextType = $moContext->getLabel();
      $msContextName = $moContext->getName();
      FWDWebLib::getObject('grid_developing_title')->setValue(" - ".$msContextType." ".$msContextName,false);
    }
    
    $moHandler = new QuerySelectDocumentType(FWDWebLib::getConnection());    
    $moSelect = FWDWebLib::getObject('select_document_classification');
    $moSelect->setQueryHandler($moHandler);
    $moSelect->populate();
    
    $moContext = new ISMSContextObject();   
    $moSelect = FWDWebLib::getObject('select_document_type');
    $moSelect->setItemValue(CONTEXT_NONE             ,FWDLanguage::getPHPStringValue('si_no_type','Sem Tipo'));
    $moSelect->setItemValue(CONTEXT_AREA             ,ISMSContextObject::getContextObject(CONTEXT_AREA)->getLabel());
    $moSelect->setItemValue(CONTEXT_PROCESS          ,ISMSContextObject::getContextObject(CONTEXT_PROCESS)->getLabel());
    $moSelect->setItemValue(CONTEXT_ASSET            ,ISMSContextObject::getContextObject(CONTEXT_ASSET)->getLabel());
    $moSelect->setItemValue(CONTEXT_CONTROL          ,ISMSContextObject::getContextObject(CONTEXT_CONTROL)->getLabel());
    $moSelect->setItemValue(CONTEXT_SCOPE            ,ISMSContextObject::getContextObject(CONTEXT_SCOPE)->getLabel());
    $moSelect->setItemValue(CONTEXT_POLICY           ,ISMSContextObject::getContextObject(CONTEXT_POLICY)->getLabel());
    $moSelect->setItemValue(DOCUMENT_TYPE_MANAGEMENT ,FWDLanguage::getPHPStringValue('si_management','Gest�o'));
    $moSelect->setItemValue(DOCUMENT_TYPE_OTHERS     ,FWDLanguage::getPHPStringValue('si_others','Outros'));
    if (FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(INCIDENT_MODE)){
      $moSelect->setItemValue(CONTEXT_CI_ACTION_PLAN   ,ISMSContextObject::getContextObject(CONTEXT_CI_ACTION_PLAN)->getLabel());
      
      $moSelect->setItemValue(CONTEXT_CI_OCCURRENCE   ,ISMSContextObject::getContextObject(CONTEXT_CI_OCCURRENCE)->getLabel());
      $moSelect->setItemValue(CONTEXT_CI_INCIDENT   ,ISMSContextObject::getContextObject(CONTEXT_CI_INCIDENT)->getLabel());
      $moSelect->setItemValue(CONTEXT_CI_NON_CONFORMITY   ,ISMSContextObject::getContextObject(CONTEXT_CI_NON_CONFORMITY)->getLabel());
    }
                                                 
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        function refresh_grid(){
          js_refresh_grid('grid_developing');
        }
        function clear_context_filter() {
          gebi("context_id").value=0;
          gobi("grid_developing_title").setValue('<?=$msGridDevelopingTitle?>');
        }        
        function remove_document(){
          trigger_event('remove_document',3);
          refresh_grid();
        }
        function read(piDocumentId,piDocInstanceId,psType) {
          gebi('read_document_id').value=piDocumentId;
          gebi('read_doc_instance_id').value=piDocInstanceId;
          switch(psType) {
            case "link":
              trigger_event('visit_link_event',3);
              break;
            case "file":
              js_submit('download_file','ajax');
              break;
            default:
              gebi('read_document_id').value=0;
              break;
          }
        }
      </script>
    <?
  }
}
//testa se a licen�a utilizada permite acessar o m�dulo de documenta��o
  ISMSLib::testUserPermissionToPoliceMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_developing.xml");

?>