<?php
include_once "include.php";
include_once $handlers_ref . "grid/improvement/QueryGridIncidentControlAssetSearch.php";
include_once $handlers_ref . "grid/improvement/QueryGridRiskByControlAsset.php";
include_once $handlers_ref . "QueryRisksByIncidentId.php";
include_once $handlers_ref . "QueryAssetsByIncidentId.php";
include_once $handlers_ref . "QueryAssetAndControlByRisk.php";
include_once $handlers_ref . "QueryAssetAndControlResponsiblesByRisk.php";
include_once $handlers_ref . "QueryAssetDependents.php";
include_once $handlers_ref . "QueryAssetDependencies.php";
include_once $handlers_ref . "QueryIncidentRiskByRiskAndIncident.php";
set_time_limit(300);
class GridControlAsset extends FWDDrawGrid {
  public function DrawItem(){
    switch($this->ciColumnIndex){
      case 1:
          $msGfxRef = FWDWebLib::getInstance()->getGfxRef();
          $miContextValue = $this->caData[4];
          if($this->caData[3]=='asset'){
            $msAssetColor = RMRiskConfig::getRiskColor($miContextValue);
            $this->coCellBox->setIconSrc("icon-asset_".$msAssetColor.".gif");

          }elseif($this->caData[3]=='control'){
            if($miContextValue){
              $this->coCellBox->setIconSrc("icon-control.gif");
            }else{
              $this->coCellBox->setIconSrc("icon-control_red.gif");
            }
          }else{
            $this->coCellBox->setValue($this->caData[3]);
          }
          return parent::drawItem();
        break;

      case 2:
        $this->coCellBox->setAttrStringNoEscape("true");
        if($this->caData[3]=='asset'){
          $this->coCellBox->setValue("<a href='javascript:open_visualize(".$this->caData[1].",".CONTEXT_ASSET.",\"".uniqid()."\")'>{$this->caData[2]}</a>");
        }elseif($this->caData[3]=='control'){
          $this->coCellBox->setValue("<a href='javascript:open_visualize(".$this->caData[1].",".CONTEXT_CONTROL.",\"".uniqid()."\")'>{$this->caData[2]}</a>");
        }
        return $this->coCellBox->draw();
        break;
      case $this->getIndexByAlias('dependencies'):{
        if($this->getFieldValue('stype')=='asset'){
          $moDependents = new QueryAssetDependents(FWDWebLib::getConnection());
          $moDependents->setAsset($this->getFieldValue('context_id'));
          $moDependents->setRecursive(true);
          $moDependents->makeQuery();
          $moDependents->executeQuery();
          $maDependents = $moDependents->getAssetDependents();

          $moDependencies = new QueryAssetDependencies(FWDWebLib::getConnection());
          $moDependencies->setAsset($this->getFieldValue('context_id'));
          $moDependencies->setRecursive(true);
          $moDependencies->makeQuery();
          $moDependencies->executeQuery();
          $maDependencies = $moDependencies->getAssetDependencies();
          $msToolTip = '';
          if(count($maDependents)){
            $msToolTip .= FWDLanguage::getPHPStringValue('to_dependents_bl_cl',"<b>Dependentes:</b> <br><br>");
            foreach($maDependents as $maDependent){
              $msToolTip .= $maDependent['asset_name']."<br>";
            }

          }
          if(count($maDependencies)){
            if($msToolTip){
              $msToolTip .= "<br>";
            }
            $msToolTip .= FWDLanguage::getPHPStringValue('to_dependencies_bl_cl',"<b>Depend�ncias:</b> <br><br>");
            foreach($maDependencies as $maDependency){
              $msToolTip .= $maDependency['asset_name']."<br>";
            }
          }
          if($msToolTip){
            $moToolTip = new FWDToolTip();
            $moToolTip->setValue($msToolTip);
            $this->coCellBox->setObjFWDToolTip($moToolTip);
            $this->coCellBox->setIconSrc("icon-detail.gif");
          }else{
            $this->coCellBox->setIconSrc('');
          }
        }else{
          $this->coCellBox->setIconSrc('');
        }
        return parent::drawItem();
      }
      default:
        return parent::drawItem();
      break;
    }
  }
}

class GridAllRisks extends FWDDrawGrid {
  public function DrawItem(){
    switch($this->ciColumnIndex){
      case 1:
          $msGfxRef = FWDWebLib::getInstance()->getGfxRef();
          $miContextValue = $this->caData[3];
          $msRiskColor = RMRiskConfig::getRiskColor($miContextValue);
          $this->coCellBox->setIconSrc("icon-risk_".$msRiskColor.".gif");
          return parent::drawItem();
        break;
      case 2:
        $this->coCellBox->setAttrStringNoEscape("true");
        $this->coCellBox->setValue("<a href='javascript:open_visualize(".$this->caData[1].",".CONTEXT_RISK.",\"".uniqid()."\")'>{$this->caData[2]}</a>");
        return $this->coCellBox->draw();
        break;

      default:
        return parent::drawItem();
      break;
    }
  }
}

class GridSelectedRisks extends FWDDrawGrid {
  public function DrawItem(){
    if(FWDWebLib::getObject('original_risks_selected')->getValue()){
      if(in_array($this->caData[1],explode(':',FWDWebLib::getObject('original_risks_selected')->getValue() ) )){
//        $this->bpRowEventBlock=true;
//        $this->csDivUnitClass = 'GridLineGray';
      }
    }
    switch($this->ciColumnIndex){
      case 1:
          $msGfxRef = FWDWebLib::getInstance()->getGfxRef();
          $miContextValue = $this->caData[3];
          $msRiskColor = RMRiskConfig::getRiskColor($miContextValue);
          $this->coCellBox->setIconSrc("icon-risk_".$msRiskColor.".gif");
          return parent::drawItem();
        break;
      case 2:
        $this->coCellBox->setAttrStringNoEscape("true");
        $this->coCellBox->setValue("<a href='javascript:open_visualize(".$this->caData[1].",".CONTEXT_RISK.",\"".uniqid()."\")'>{$this->caData[2]}</a>");
        return $this->coCellBox->draw();
        break;

      default:
        return parent::drawItem();
      break;
    }
  }
}

class SaveEvent extends FWDRunnable {
  public function run() {
    $maRiskParameters = array();
    $maNewRiskParameters = array();

    $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    if ($moSession->attributeExists("incidentRiskParameters")) {
      $maNewRiskParameters = unserialize($moSession->getAttrIncidentRiskParameters());
    }
    $moSession->deleteAttribute("incidentRiskParameters");
    $miUserId = $moSession->getUserId();

    $miIncidentId = FWDWebLib::getObject('var_incident_id')->getValue();
    $moIncidentRisk = new CIIncidentRisk();
    $maNewRisks = array_unique(array_filter(explode(':',FWDWebLib::getObject('new_risks_selected')->getValue())));
    $moIncidentRisk->setFieldValue('incident_id',$miIncidentId);
    $maControlIds = array();

    /*if (count(array_diff($maNewRisks,array_keys($maNewRiskParameters)))) {
      echo "js_show('required_parametrization_warning');";
      return;
    }*/
/*	if (!count($maNewRiskParameters)) { // tem que corrigir aqui, precisa verificar somente se a quantidade foi alterada, de novos e velhos riscos -- Piero
      	echo "js_show('required_parametrization_warning');";
      	return;
	} */

    
//    $fd = fopen("C:/tmp/risk.txt","a+");
    $moQueryRisks = new QueryRisksByIncidentId(FWDWebLib::getConnection());
    $moQueryRisks->setIncidentId($miIncidentId);
    $maOldStoredValues = $moQueryRisks->getValues();
    $maOldScreenValues = explode(':',FWDWebLib::getObject('original_risks_selected')->getValue());
//    fwrite($fd,"maOldStoredValues: " . print_r($maOldStoredValues,true). "maOldScreenValues: " . count($maOldScreenValues) . "\n");
    if(count($maOldScreenValues) == 1 && !$maOldScreenValues[0]) {
//    	fwrite($fd,"passa aqui\n");
    	$maOldScreenValues = array();
    } 
    if(count($maOldStoredValues) > count($maOldScreenValues)){
//    	fwrite($fd,"ta entrando no count\n");
		$maToRemove = array_diff($maOldStoredValues,$maOldScreenValues);
//		fwrite($fd,print_r($maToRemove,true));
		foreach($maToRemove as $maToRemoveId) {
			$moQueryRiskIncidentId = new QueryIncidentRiskByRiskAndIncident(FWDWebLib::getConnection());
			$moQueryRiskIncidentId->setIncidentId($miIncidentId);
			$moQueryRiskIncidentId->setRiskId($maToRemoveId);
			$moId = $moQueryRiskIncidentId->getValue();
			$moIncidentRisk = new CIIncidentRisk();
//			fwrite($fd,"indo remover: $moId\n");
			if(!$moIncidentRisk->delete($moId)) { // TESTAR AQUI AMANHA
//				fwrite($fd,"deu algum erro\n");
			}
		}	
    }

    foreach($maNewRisks as $miRisk){
      $moIncidentRisk->setFieldValue('risk_id',$miRisk);
      //$moIncidentRisk->setFieldValue('probability_value_name',$maNewRiskParameters[$miRisk][0]);
      $miIncidentRisk = $moIncidentRisk->insert(true);

      $maRiskParameters[$miRisk] = array();

      $moValues = new QueryRiskValues(FWDWebLib::getConnection());
      $moValues->setElementId($miRisk);
      $moValues->makeQuery();
      $moValues->executeQuery();
      $mbParameterizedRisk = false;
      while($moValues->getDataset()->fetch()) {
        $mbParameterizedRisk = true;
        $moValueDataset = $moValues->getDataset();
        $miValueId = $moValueDataset->getFieldByAlias("value_id")->getValue();
        $miParameterId = $moValueDataset->getFieldByAlias("parameter_id")->getValue();
        $maRiskParameters[$miRisk][$miParameterId] = $miValueId;
      }
    
      /*if ($mbParameterizedRisk) {
        $moProbability = new QueryRiskProbability(FWDWebLib::getConnection());
        $moProbability->setElementId($miRisk);
        $moProbability->makeQuery();
        $moProbability->executeQuery();
        $moProbability->getDataset()->fetch();
        $moProbabilityDataset = $moProbability->getDataset();
        $miValueId = $moProbabilityDataset->getFieldByAlias("value_id")->getValue();
        $maRiskParameters[$miRisk]["prob"] = $miValueId;
      }*/

      //$moNewRiskParameters = $maNewRiskParameters[$miRisk][1];
      //$moNewRiskParameters = $maNewRiskParameters[$miRisk];
      $moNewRiskParameters = $maNewRiskParameters;
      
      //$maIncidentRiskParameters = $moNewRiskParameters->getValues();
      $maIncidentRiskParameters = $moNewRiskParameters;
      
      if (array_diff_assoc($maIncidentRiskParameters,$maRiskParameters[$miRisk])) {
        $moRisk = new RMRisk();
        $moRisk->fetchById($miRisk);
        $miAssetId = $moRisk->getFieldValue('risk_asset_id');
        $msRiskName = $moRisk->getFieldValue('risk_name');
        $moAsset = new RMAsset();
        $moAsset->fetchById($miAssetId);
        $miAssetSecResponsible = $moAsset->getFieldValue('asset_security_responsible_id');
        $msAssetName = $moAsset->getFieldValue('asset_name');
        if ($miAssetSecResponsible != $miUserId) { //Enviar e-mail se usu�rio logado != resp. seguran�a do ativo.
          $moUser = new ISMSUser();
          $moUser->fetchById($miAssetSecResponsible);
          $msUserEmail = $moUser->getFieldValue('user_email');
                    
          $moAlert = new WKFAlert();
          $moAlert->setFieldValue('alert_context_id', $miRisk);
          $moAlert->setFieldValue('alert_creator_id', $miUserId);
          $moAlert->setFieldValue('alert_receiver_id', $miAssetSecResponsible);
          $moAlert->setFieldValue('alert_type', WKF_ALERT_INCIDENT_RISK_PARAMETRIZATION);
          $moAlert->setFieldValue('alert_date', ISMSLib::ISMSTime());
          $moAlert->insert(false, false);
          
          if($msUserEmail && ISMSLib::getConfigById(GENERAL_EMAIL_ENABLED)) {

            $maParameters = array();
            $moHandler = new QueryParameterNames(FWDWebLib::getConnection());
            $moHandler->makeQuery();
            $moHandler->executeQuery();
            $moDataset = $moHandler->getDataset();
            while($moDataset->fetch()){
              $miId = $moDataset->getFieldByAlias('parametername_id')->getValue();
              $msName = $moDataset->getFieldByAlias('parametername_name')->getValue();
              $maParameters[$miId] = $msName;
            }
            $maValuesNames = array();
            $moHandler = new QueryParameterValueNames(FWDWebLib::getConnection());
            $moHandler->setType(RISKPARAMETER_RISK);
            $moHandler->makeQuery();
            $moHandler->executeQuery();
            $moDataset = $moHandler->getDataset();
            while($moDataset->fetch()){
              $miId = $moDataset->getFieldByAlias('value_id')->getValue();
              $msName = $moDataset->getFieldByAlias('value_name')->getValue();
              $maValuesNames[$miId] = $msName;
            }

            /*$maProbabilityValues = array();
            $moHandler = new QueryProbabilityValueNames(FWDWebLib::getConnection());
            $moHandler->setType(RISKPARAMETER_RISK);
            $moHandler->makeQuery();
            $moHandler->executeQuery();
            $moDataset = $moHandler->getDataset();
            while($moDataset->fetch()){
              $miId = $moDataset->getFieldByAlias('value_id')->getValue();
              $msName = $moDataset->getFieldByAlias('value_name')->getValue();
              $maProbabilityValues[$miId] = $msName;
            }*/

            //Originais:
            foreach($maRiskParameters as $miRiskId=>$maParametersNames) {
              $msOriginalParameters = "";
              /*var_dump($maParametersNames);
              asort($maParametersNames);
              var_dump($maParametersNames);exit;*/
              asort($maParametersNames);
              if (!count($maParametersNames)) {
                $msOriginalParameters = FWDLanguage::getPHPStringValue("mx_non_parameterized_risk","Risco N�o Estimado");
              }
              foreach($maParametersNames as $mmParameterId=>$miParameterValue) {
                /*if ($mmParameterId == "prob") {
                  $msOriginalParameters .= FWDLanguage::getPHPStringValue("mx_probability","Probabilidade").": ".$maProbabilityValues[$miParameterValue]."<BR>";
                } else {*/
                  if (isset ($maParameters[$mmParameterId]))
                    $msOriginalParameters .= $maParameters[$mmParameterId].": ".$maValuesNames[$miParameterValue]."<BR>";
                //}
              }
            }

            //Novos:
            $msNewParameters = "";
            asort($maIncidentRiskParameters);
            foreach($maIncidentRiskParameters as $mmParameterId=>$miParameterValue) {
              /*if ($mmParameterId == "prob") {
                $msNewParameters .= FWDLanguage::getPHPStringValue("mx_probability","Probabilidade").": ".$maProbabilityValues[$miParameterValue]."<BR>";
              } else {*/
                if (isset($maParameters[$mmParameterId]))
                  $msNewParameters .= $maParameters[$mmParameterId].": ".$maValuesNames[$miParameterValue]."<BR>";
              //}
            }

            $msDate = ISMSLib::getISMSDate();
            $moEmailPreferences = new ISMSEmailPreferences();
            $msFormat = $moEmailPreferences->getEmailFormat($miAssetSecResponsible);
            $msSystemName = ISMSLib::getSystemName();
            $moMailer = new ISMSMailer();
            $moMailer->setFormat($msFormat);
            $moMailer->setTo($msUserEmail);
            $moMailer->setSubject("[$msSystemName] " . date('d/m/Y') . ' - '.FWDLanguage::getPHPStringValue('em_incident_risk_parametrization','Estimativa de Risco associado a Incidente'));
            $moMailer->addContent('incidentRiskParametrization');
            $moMailer->setParameter('asset',$msAssetName);
            $moMailer->setParameter('risk',$msRiskName);
            $moMailer->setParameter('original_parameters',$msOriginalParameters);
            $moMailer->setParameter('new_parameters',$msNewParameters);
            $moMailer->setFooterMessage(FWDLanguage::getPHPStringValue('em_incident_risk_parametrization_footer','Para maiores informa��es entre em contato com o security officer.'));
            $moMailer->send();
          }
        }
      }

      //$moRiskParameterValues = new RiskParametersValue($maNewRiskParameters[$miRisk][1],RISKPARAMETER_INCIDENTRISK);
     // $moRiskParameterValues = new RiskParametersValue($maNewRiskParameters[$miRisk],RISKPARAMETER_INCIDENTRISK);
     // $moRiskParameterValues->setValues($miIncidentRisk);
	// isso daqui agora � enviado um email para o dono do risco, perguntando se ele deseja alterar, ao inves de alterar diretamente - Piero
      
      //obtem os controles relacionados ao risco
      $maRiskControl = new RMRiskControl();
      $maRiskControl->createFilter($miRisk,'rc_risk_id');
      $maRiskControl->select();
      while($maRiskControl->fetch()){
        $maControlIds[] = $maRiskControl->getFieldValue('rc_control_id');
      }
    }
    $maControlIds = array_unique(array_filter($maControlIds));

    $moIncidentControl = new CIIncidentControl();
    $moIncidentControl->createFilter($miIncidentId,'incident_id');
    $moIncidentControl->select();
    $maCurrentControls = array();
    while($moIncidentControl->fetch()){
      $maCurrentControls [] = $moIncidentControl->getFieldValue('control_id');
    }
    $maNewControls = array_diff($maControlIds,$maCurrentControls);
    $moIncControl2 = new CIIncidentControl();
    $moIncControl2->setFieldValue('incident_id',$miIncidentId);
    foreach($maNewControls as $miValue){
      $moIncControl2->setFieldValue('control_id',$miValue);
      $miIncControlId = $moIncControl2->insert(true);

      //verifica se o controle tem efici�ncia abilitada
      $moControlEff = new WKFControlEfficiency();
      if($moControlEff->fetchById($miValue)){
        $moControl = new RMControl();
        $moControl->fetchById($miValue);
        $miDateTest = ISMSLib::ISMSTime();
        $moTask = new WKFTask();
        $moConfig = new ISMSConfig();
        $moTask->setFieldValue('task_activity',ACT_INC_CONTROL_INDUCTION);
        $moTask->setFieldValue('task_context_id',$miIncControlId);
        $moTask->setFieldValue('task_creator_id',$moConfig->getConfig(USER_INCIDENT_MANAGER));
        $moTask->setFieldValue('task_receiver_id',$moControl->getResponsible());
        $moTask->setFieldValue('task_date_created',mktime (0, 0, 0, date("m",$miDateTest), date("d",$miDateTest),  date("Y",$miDateTest)));
        $moTask->setFieldValue('task_is_visible',true);
        $moTask->insert();
      }
    }

    $moQueryAssets = new QueryAssetsByIncidentId(FWDWebLib::getConnection());
    $moQueryAssets->setIncidentId($miIncidentId);
    $maAssets = $moQueryAssets->getValues();
    
/*   $maRiskParametersList = unserialize($moSession->getAttrRiskParameterList());
   
    foreach($maRiskParametersList as $RiskId=>$RiskParameters) {
    	foreach($RiskParameters as $parameter=>$value) {
    		if(!$value) {
    			continue;
    		}
    	$moIncidentRiskParameter = new CIIncidentRiskParameter();
    	$moIncidentRiskParameter->setFieldValue('incident_risk_id',$miIncidentRisk);
    	$moIncidentRiskParameter->setFieldValue('parameter_name_id',$parameter);
    	$moIncidentRiskParameter->setFieldValue('parameter_value_id',$value);
    	$moIncidentRiskParameter->insert(false);
    	}	
    } */
    
    if(count($maNewRisks)>0 || count($maAssets)>1){
      
      //Abrir popup com op��es
      echo "if (!soPopUpManager.getPopUpById('popup_incident_risk_association_options')) {
              isms_open_popup('popup_incident_risk_association_options','packages/improvement/popup_incident_risk_association_options.php?incident=".$miIncidentId."&risks=".implode(':',$maNewRisks)."','','true');
              soWindow = soPopUpManager.getPopUpById('popup_incident_risk_association_options');
              soWindow.setCloseEvent(
                function(){
                  soWindow = soPopUpManager.getPopUpById('popup_incident_risk_association_edit').getOpener();
                  if(soWindow.refresh_grid) soWindow.refresh_grid();
                  soPopUpManager.closePopUp('popup_incident_risk_association_edit'); 
                }
              );
            }
            ";
      return;
    }
    echo "soWindow = soPopUpManager.getPopUpById('popup_incident_risk_association_edit').getOpener();"
        ."if(soWindow.refresh_grid) soWindow.refresh_grid();"
        ."soPopUpManager.closePopUp('popup_incident_risk_association_edit');"
        ."soWindow=null;";
  }
}

class OpenUserLookUpEvent extends FWDRunnable {
  public function run(){
    echo "isms_open_popup('popup_associate_users','packages/policy/popup_associate_users.php','','true',402,700);
          soWindow = soPopUpManager.getPopUpById('popup_associate_users');
          soWindow.setCloseEvent(function(){ soPopUpManager.closePopUp('popup_incident_risk_association_edit'); });
    ";
  }
}

class DispatchAlertsEvent extends FWDRunnable {
  public function run(){
    $msDebug = '';

    $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    $miIncidentId = FWDWebLib::getObject('var_incident_id')->getValue();

    $moHandler = new QueryAssetAndControlResponsiblesByRisk(FWDWebLib::getConnection());
    $moHandler->setIncidentId($miIncidentId);
    $moHandler->setRiskIds(FWDWebLib::getObject('new_risks_selected')->getValue());
    $moHandler->makeQuery();
    $moHandler->executeQuery();

    while($moHandler->fetch()){
      // envia alerta
      $miAlertType = ($moHandler->getFieldValue('type')==CONTEXT_ASSET?WKF_ALERT_INC_ASSET:WKF_ALERT_INC_CONTROL);
      $moAlert = new WKFAlert();
      $moAlert->setFieldValue('alert_type', $miAlertType);
      $moAlert->setFieldValue('alert_context_id', $moHandler->getFieldValue('relation_id'));
      $moAlert->setFieldValue('alert_creator_id', $miUserId);
      $moAlert->setFieldValue('alert_receiver_id', $moHandler->getFieldValue('responsible'));
      $moAlert->setFieldValue('alert_justification', '');
      $moAlert->setFieldValue('alert_date', ISMSLib::ISMSTime());
      $moAlert->insert();
    }

    $maUsers = array_filter(explode(':',FWDWebLib::getObject('users_ids')->getValue()));
    foreach($maUsers as $miReceiverId){
      // envia alerta
      $moAlert = new WKFAlert();
      $moAlert->setFieldValue('alert_type', WKF_ALERT_INCIDENT);
      $moAlert->setFieldValue('alert_context_id', $miIncidentId);
      $moAlert->setFieldValue('alert_creator_id', $miUserId);
      $moAlert->setFieldValue('alert_receiver_id', $miReceiverId);
      $moAlert->setFieldValue('alert_justification', '');
      $moAlert->setFieldValue('alert_date', ISMSLib::ISMSTime());
      $moAlert->insert();
    }

    echo "soWindow = soPopUpManager.getPopUpById('popup_incident_risk_association_edit').getOpener();"
        ."if(soWindow.refresh_grid) soWindow.refresh_grid();"
        ."soPopUpManager.closePopUp('popup_incident_risk_association_edit');"
        ."soWindow=null;";
  }
}

class PopulateAssetControlGrid extends FWDRunnable {
  public function run() {
    $moGrid = FWDWebLib::getObject('grid_control_asset');
    $moGrid->execEventPopulate();
  }
}

class PopulateAllRisksGrid extends FWDRunnable {
  public function run() {
    $moGrid = FWDWebLib::getObject('grid_all_risks');
    $moGrid->execEventPopulate();
  }
}

class PopulateGridSelectedRisks extends FWDRunnable {
  public function run() {
    $moGrid = FWDWebLib::getObject('grid_selected_risks');
    $moGrid->execEventPopulate();

    echo " trigger_event('populate_all_risks',3); ";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new SaveEvent("save_event"));
    $moStartEvent->addAjaxEvent(new PopulateAssetControlGrid("populate_control_asset"));
    $moStartEvent->addAjaxEvent(new PopulateAllRisksGrid("populate_all_risks"));
    $moStartEvent->addAjaxEvent(new PopulateGridSelectedRisks("populate_selected_risks"));
    $moStartEvent->addAjaxEvent(new OpenUserLookUpEvent("open_user_look_up"));
    $moStartEvent->addAjaxEvent(new DispatchAlertsEvent("dispatch_alerts"));

    $moGridAllRisks = FWDWebLib::getObject('grid_all_risks');
    $moHandlerAllRisks = new QueryGridRiskByControlAsset(FWDWebLib::getConnection());
    $moHandlerAllRisks->setAssetIds(FWDWebLib::getObject('assets_search')->getValue());
    $moHandlerAllRisks->setControlIds(FWDWebLib::getObject('controls_search')->getValue());
    $moHandlerAllRisks->setRiskIdsNotIN(FWDWebLib::getObject('original_risks_selected')->getValue().':'.FWDWebLib::getObject('new_risks_selected')->getValue());
    $moGridAllRisks->setQueryHandler($moHandlerAllRisks);
    $moGridAllRisks->setObjFWDDrawGrid(new GridAllRisks());

    $moGridSelectedRisks = FWDWebLib::getObject('grid_selected_risks');
    $moHandlerSelectedRisks = new QueryGridRiskByControlAsset(FWDWebLib::getConnection());
    $moHandlerSelectedRisks->setAssetIds(FWDWebLib::getObject('assets_search')->getValue());
    $moHandlerSelectedRisks->setControlIds(FWDWebLib::getObject('controls_search')->getValue());
    $moHandlerSelectedRisks->setRiskIdsIN(FWDWebLib::getObject('original_risks_selected')->getValue() .':'. FWDWebLib::getObject('new_risks_selected')->getValue().":1");
    $moGridSelectedRisks->setQueryHandler($moHandlerSelectedRisks);
    $moGridSelectedRisks->setObjFWDDrawGrid(new GridSelectedRisks());

    $moGridControlAsset = FWDWebLib::getObject('grid_control_asset');
    $moHandlerControlAsset = new QueryGridIncidentControlAssetSearch(FWDWebLib::getConnection());
    $moHandlerControlAsset->setAssetIds(FWDWebLib::getObject('assets_search')->getValue());
    $moHandlerControlAsset->setControlIds(FWDWebLib::getObject('controls_search')->getValue());
    $moGridControlAsset->setQueryHandler($moHandlerControlAsset);
    $moGridControlAsset->setObjFWDDrawGrid(new GridControlAsset());
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    FWDWebLib::getObject('var_incident_id')->setValue(FWDWebLib::getObject('par_incident_id')->getValue());
    $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    $moSession->deleteAttribute("incidentRiskParameters");

    $msJavascriptCode='<script language="javascript"></script>';;
    if(FWDWebLib::getObject('var_incident_id')->getValue()){
      $moQueryRisks = new QueryRisksByIncidentId(FWDWebLib::getConnection());
      $moQueryRisks->setIncidentId(FWDWebLib::getObject('var_incident_id')->getValue());
      $maValues = $moQueryRisks->getValues();
      if(count($maValues)){
        $msJavascriptCode .='<script language="javascript"> populate_grids_screen_event(); </script>';
        FWDWebLib::getObject('original_risks_selected')->setValue(implode(':',$maValues));
        $moQueryAssetControl = new QueryAssetAndControlByRisk(FWDWebLib::getConnection());
        $moQueryAssetControl->setRiskIds(implode(':',$maValues));
        $maAssetControl = $moQueryAssetControl->getValues();
        $maControls = array();
        $maAssets = array();
        foreach ($maAssetControl as $miKey => $msValue){
          switch($msValue){
            case 'asset':
              $maAssets[] = $miKey;
            break;
            case 'control':
              $maControls[] = $miKey;
            break;
            default: break;
          }
        }
        FWDWebLib::getObject('controls_search')->setValue(implode(':',$maControls));
        FWDWebLib::getObject('assets_search')->setValue(implode(':',$maAssets));
      }


    }

    //instala a seguran�a de ACL na p�gina
    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        function populate_grids_screen_event(){
          gobi('grid_selected_risks').setPopulate(true);
          gobi('grid_all_risks').setPopulate(true);
          gobi('grid_control_asset').setPopulate(true);

          trigger_event('populate_selected_risks',3);
          trigger_event('populate_control_asset',3);

        }

        function remove_selected_incident_risks(){
          var maNewRisks = new Array();
          var moNewRisks = gebi('new_risks_selected');
          var moOldRisks = gebi('original_risks_selected');
          
          if(moNewRisks.value){  
              var maRisks = moNewRisks.value.split(':');
              var maGridValue = gobi('grid_selected_risks').getValue();
              for(i=0;i < maRisks.length; i++){
                 if(!in_array(maRisks[i], maGridValue)){
                    maNewRisks.push(maRisks[i]);
                 }
              }
              moNewRisks.value = maNewRisks.join(':');
              gobi('grid_all_risks').setPopulate(true);
              gobi('grid_selected_risks').setPopulate(true);
              trigger_event('populate_selected_risks',3);
            } else if(moOldRisks.value) {
                var maRisks = moOldRisks.value.split(':');
                var maGridValue = gobi('grid_selected_risks').getValue();
                for(i=0;i < maRisks.length; i++){
                   if(!in_array(maRisks[i], maGridValue)){
                      maNewRisks.push(maRisks[i]);
                   }
                }
                moOldRisks.value = maNewRisks.join(':');
                gobi('grid_all_risks').setPopulate(true);
                gobi('grid_selected_risks').setPopulate(true);
                trigger_event('populate_selected_risks',3);                
            }
          
        }

        function refresh_grids(){
          gobi('grid_control_asset').setPopulate(true);
          gobi('grid_all_risks').setPopulate(true);

          trigger_event('populate_control_asset',3);
          trigger_event('populate_all_risks',3);
        }
        function set_controls(psControls){
          var moControl = gebi('controls_search');
          if(moControl.value)
            gebi('controls_search').value = gebi('controls_search').value + ':' + psControls;
          else
            gebi('controls_search').value = psControls;
          refresh_grids();
        }
        function set_asset(piAsset){
          var moAsset = gebi('assets_search');
          if(moAsset.value){
            moAsset.value = moAsset.value + ':' + piAsset;
          }else{
            moAsset.value = piAsset;
          }
          refresh_grids();
        }
        function set_users(psUsers){
          gebi('users_ids').value = psUsers;
          trigger_event('dispatch_alerts',3);
        }

        function refresh_risks(){
          gobi('grid_all_risks').refresh();
        }
      </script>
    <?
    echo $msJavascriptCode;
  }
}
//testa se a licen�a utilizada permite acessar o m�dulo de incidente
ISMSLib::testUserPermissionToIncidentMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_incident_risk_association_edit.xml");

?>