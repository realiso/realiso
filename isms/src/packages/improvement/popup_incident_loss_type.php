<?php

include_once "include.php";

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    
    $miIncidentId = FWDWebLib::getObject('url_param_incident_id')->getValue();

    $moIncident = new CIIncident();
    if ($moIncident->fetchById($miIncidentId)) {
      $loss_type = $moIncident->getFieldValue('incident_loss_type');
      
      if ($loss_type == INCIDENT_LOSS_TYPE_DIRECT) {
      	$moWebLib->getObject('popup_incident_loss_type_memo')->setValue(FWDLanguage::getPHPStringValue('mx_direct','Direta'));
      } else {
      	$moWebLib->getObject('popup_incident_loss_type_memo')->setValue(FWDLanguage::getPHPStringValue('mx_indirect','Indireta'));
      }
    }

    $moWebLib->getObject('popup_incident_loss_type_memo')->setAttrReadonly(true);
    
    $moEvent = new FWDClientEvent();
    $moEvent->setAttrEvent('onClick');
    $moEvent->setAttrValue("soPopUpManager.closePopUp('popup_incident_loss_type');");
    $moWebLib->getObject('close')->addObjFWDEvent($moEvent);

    //instala a seguran�a de ACL na p�gina
    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject("popup_incident_loss_type_dialog"),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject("popup_incident_loss_type_dialog"));
  }
}

// testa se a licen�a utilizada permite acessar o m�dulo de incidente
ISMSLib::testUserPermissionToIncidentMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_incident_loss_type.xml');

?>