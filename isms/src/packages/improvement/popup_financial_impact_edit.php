<?php

include_once 'include.php';

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $miIncidentId = FWDWebLib::getObject('param_incident_id')->getValue();
    
    $maParameters = array();
    
    $msParametersIds = FWDWebLib::getObject('param_ids')->getValue();
    if($msParametersIds){
      $maParametersIds = explode(':',$msParametersIds);
      $maParametersValues = explode(':',FWDWebLib::getObject('param_values')->getValue());
      for($i=0;$i<count($maParametersIds);$i++){
        $maParameters[$maParametersIds[$i]] = str_replace(',','',$maParametersValues[$i]);
      }
    }elseif($miIncidentId){
      $moIncidentFinancialImpact = new CIIncidentFinancialImpact();
      $moIncidentFinancialImpact->createFilter($miIncidentId,'incident_id');
      $moIncidentFinancialImpact->select();
      while($moIncidentFinancialImpact->fetch()){
        $miParameterId = $moIncidentFinancialImpact->getFieldByAlias('classification_id')->getValue();
        $mfParameterValue = $moIncidentFinancialImpact->getFieldByAlias('financial_impact_value')->getValue();
        $maParameters[$miParameterId] = $mfParameterValue;
      }
    }
    
    $moContextClassification = new ISMSContextClassification();
    $moContextClassification->createFilter(CONTEXT_CLASSIFICATION_FINANCIAL_IMPACT,'classif_type');
    $moContextClassification->select();
    
    $moViewGroup = FWDWebLib::getObject('container');
    
    $maParametersIds = array();
    
    $miCounter = 0;
    $miTop = 0;
    $miStaticsLeft = 0;
    $miTextsLeft = 215;
    while($moContextClassification->fetch()){
      $miId = $moContextClassification->getFieldByAlias('classif_id')->getValue();
      $msName = $moContextClassification->getFieldByAlias('classif_name')->getValue();
      $miCounter++;
      
      $maParametersIds[] = $miId;
      
      $moStatic = new FWDStatic();
      $moStatic->setObjFWDBox(new FWDBox($miStaticsLeft,$miTop,20,205));
      $moStatic->setAttrHorizontal('right');
      $moStatic->setValue($msName);
      $moViewGroup->addObjFWDView($moStatic);
      
      $moToolTip = new FWDToolTip();
      $moToolTip->setAttrShowDelay(1000);
      $moToolTip->setAttrWidth(155);
      $moToolTip->setValue($msName);
      $moStatic->addEvent($moToolTip);
      
      $moMask = new FWDMaskAutoComplete();
      $moMask->setType(MASK_MONEY);
      
      $moEvent = new FWDClientEvent();
      $moEvent->setAttrEvent('onKeyUp');
      $moEvent->setValue('updateTotal();',true);
      
      $moText = new FWDText();
      $moText->setAttrName("financial_impact_$miCounter");
      $moText->setAttrClass('FWDText_AlignRight');
      $moText->setObjFWDBox(new FWDBox($miTextsLeft,$miTop,20,135));
      if(isset($maParameters[$miId])){
        $moText->setValue(number_format($maParameters[$miId],2));
      }else{
        $moText->setValue('0.00');
      }
      $moText->addObjFWDMask($moMask);
      $moText->addEvent($moEvent);
      $moViewGroup->addObjFWDView($moText);
      
      $miTop+= 30;
    }
    FWDWebLib::getObject('parameters_ids')->setValue(implode(':',$maParametersIds));
    FWDWebLib::getObject('container')->getObjFWDBox()->setAttrHeight($miCounter * 30);
    FWDWebLib::getObject('dialog')->getObjFWDBox()->setAttrHeight($miCounter * 40);
    FWDWebLib::getObject('footer')->getObjFWDBox()->setAttrTop($miCounter * 30 + 20);
  
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        
        function save(){
          var moParent = soPopUpManager.getPopUpById('popup_financial_impact_edit').getOpener();
          var msParametersIds = gebi('parameters_ids').value;
          var maValues = [];
          var i = 1;
          var moValue = gebi('financial_impact_'+i);
          while(moValue){
            maValues.push(moValue.value);
            i++;
            moValue = gebi('financial_impact_'+i);
          }
          moParent.setFinancialImpact(msParametersIds,maValues.join(':'));
          moParent = msParametersIds = maValues = moValue = null;
          soPopUpManager.closePopUp('popup_financial_impact_edit');
        }

        function format_currency_fields() {
          var i = 1;
          var moValue = gebi('financial_impact_'+i);
          while(moValue){
            moValue.value = format_currency(moValue.value);
            moValue = gebi('financial_impact_'+i);
            i++;
          }
          moValue = i = null;
					updateTotal();
				}
        
        function updateTotal(){
          var i = 1;
          var mfSum = 0;
          var moValue = gebi('financial_impact_'+i);
          while(moValue){
            var miValue = parseFloat(moValue.value.replace(/,/g,'')); 
            mfSum+= miValue?miValue:0;
            i++;
            moValue = gebi('financial_impact_'+i);
          }
          moValue = miValue = i = null;
          gobi('total').setValue(format_currency(new String(mfSum.toFixed(2))));
        }
        
        format_currency_fields(); 
      </script>
    <?
  }
}

//testa se a licen�a utilizada permite acessar o m�dulo de incidente
ISMSLib::testUserPermissionToIncidentMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_financial_impact_edit.xml');

?>