<?php
include_once "include.php";
include_once $handlers_ref . "grid/improvement/QueryGridIncidentSearch.php";

class GridSearch extends FWDDrawGrid {
  public function DrawItem(){
    switch($this->ciColumnIndex){
      case 1:
        $msGfxRef = FWDWebLib::getInstance()->getGfxRef();
        $moIcon = new FWDIcon(new FWDBox(2,4));
        $moIcon->setAttrSrc("{$msGfxRef}icon-ci_incident.gif");
    return $moIcon->draw();
      break;  
      default:
        return parent::drawItem();
      break;
    }
  }
}

class SearchEvent extends FWDRunnable {
  public function run(){
    $moGrid = FWDWebLib::getObject('grid_incident_search');
    $moGrid->execEventPopulate();
  }
}

class InsertEvent extends FWDRunnable {
  public function run() {    
    $maGridValue = FWDWebLib::getObject('grid_incident_search')->getValue();    
    if (count($maGridValue)) {        
      echo "soPopUp = soPopUpManager.getPopUpById('popup_incident_search');
        soWindow = soPopUp.getOpener();            
        soWindow.set_incident('{$maGridValue[0]}');
        soPopUpManager.closePopUp('popup_incident_search');";
    }
    else echo "js_show('warning');";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
    public function run(){
      $moStartEvent = FWDStartEvent::getInstance();
      $moStartEvent->setScreenEvent(new ScreenEvent("")); 
      $moStartEvent->addAjaxEvent(new SearchEvent("search_event"));
      $moStartEvent->addAjaxEvent(new InsertEvent("insert_event"));
      
      $moGrid = FWDWebLib::getObject("grid_incident_search");
      $moHandler = new QueryGridIncidentSearch(FWDWebLib::getConnection());
      
      $moHandler->setName(FWDWebLib::getObject("var_name")->getValue());
      
      $moGrid->setQueryHandler($moHandler);
      $moGrid->setObjFwdDrawGrid(new GridSearch());
    }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        function search(){
          gobi('warning').hide();
          gebi('var_name').value = gebi('incident_name').value;
          gobi('grid_incident_search').setPopulate(true);
          trigger_event("search_event","3");
        }
      </script>
    <? 
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_incident_search.xml");
?>