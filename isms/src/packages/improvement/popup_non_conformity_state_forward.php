<?php
include_once "include.php";

class StateForwardEvent extends FWDRunnable {
  public function run() {
    $miNCId = FWDWebLib::getObject('nc_id')->getValue();
    $moCtxUserTest = new CINonConformity();
    $moCtxUserTest->testPermissionToEdit($miNCId);

    $moNonConformity = new CINonConformity();
    $moNonConformity->fetchById($miNCId);

    $maWKFAction = $moNonConformity->getStateForwardAction();
    $miWKFAction = isset($maWKFAction['confirm'])?$maWKFAction['confirm']:0;
    $moNonConformity->stateForward($miWKFAction?$miWKFAction:WKF_ACTION_NONE);

    echo "soWindow = soPopUpManager.getPopUpById('popup_non_conformity_state_forward').getOpener();"
        ."if (soWindow.refresh_grid){ soWindow.refresh_grid() }"
        ."soWindow=null;"
        ."soPopUpManager.closePopUp('popup_non_conformity_state_forward');";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new StateForwardEvent('state_forward_event'));

    //instala a seguran�a de ACL na p�gina
    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
  }
}


class ScreenEvent extends FWDRunnable {
  public function run(){
    $miNCId = FWDWebLib::getObject('nc_id')->getValue();
    $moNonConformity = new CINonConformity();
    $maWKFAction = $moNonConformity->getStateForwardAction($miNCId);
    $miWKFAction = isset($maWKFAction['confirm'])?$maWKFAction['confirm']:0;
    switch($miWKFAction) {
      case WKF_ACTION_CI_NC_SEND_TO_RESPONSIBLE:{
        $msTitle = FWDLanguage::getPHPStringValue('tt_non_conformity_send_to_responsible','Enviar para Respons�vel');
        $msMessage = FWDLanguage::getPHPStringValue('st_non_conformity_send_to_responsible',"Deseja enviar a N�o Conformidade para o Respons�vel?");
      break;
      }
      case WKF_ACTION_SEND_NC_TO_APPROVE:{
        $msTitle = FWDLanguage::getPHPStringValue('tt_non_conformity_send_to_ap_responsible','Enviar para a Aceita��o dos Planos de A��o');
        $msMessage = FWDLanguage::getPHPStringValue('st_non_conformity_send_to_ap_responsible',"Deseja enviar a N�o Conformidade para a Aceita��o dos Planos de A��o?");
      break;
      }
      case WKF_ACTION_CI_NC_SEND_TO_WAITING_CONCLUSION:{
        $msTitle = FWDLanguage::getPHPStringValue('tt_non_conformity_send_to_approve','Enviar para a Conclus�o dos Planos de A��o');
        $msMessage = FWDLanguage::getPHPStringValue('st_non_conformity_send_to_approve',"Deseja enviar a N�o Conformidade para a Conclus�o dos Planos de A��o?");
      break;
      }
      default:
        return false;
      break;
    }

    FWDWebLib::getObject('confirm_title')->setValue($msTitle);
    FWDWebLib::getObject('confirm_message')->setValue($msMessage);

    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        soPopUpManager.getPopUpById('popup_non_conformity_state_forward').setCloseEvent(
        function(){
          soWindow = soPopUpManager.getPopUpById('popup_non_conformity_state_forward').getOpener();
          if (soWindow.close_popup) soWindow.close_popup();
          soWindow = null;
        });
      </script>
    <?
  }
}
//testa se a licen�a utilizada permite acessar o m�dulo de incidente
ISMSLib::testUserPermissionToIncidentMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_non_conformity_state_forward.xml");

?>