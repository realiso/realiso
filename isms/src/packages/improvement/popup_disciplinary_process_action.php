<?php
include_once "include.php";

class SaveEvent extends FWDRunnable {
  public function run(){
    $miIncidentId = FWDWebLib::getObject('incident_id')->getValue();
    $miUserId = FWDWebLib::getObject('user_id')->getValue();

    $moIncUser = new CIIncidentUser();
    $moIncUser->createFilter($miIncidentId, 'incident_id');
    $moIncUser->createFilter($miUserId, 'user_id');
    $moIncUser->setFieldValue('action_taken', FWDWebLib::getObject('action_description')->getValue());
    $moIncUser->update();
    echo 'soWindow = self.getOpener();
          if(soWindow.refresh_grid) soWindow.refresh_grid();   
          self.close();';
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
    $moStartEvent->addAjaxEvent(new SaveEvent('save_event'));
  }
}

function testPermissionToEditActionTaken() {
  $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
  if (in_array('M.CI.5.2', $maACLs)) 
    trigger_error(FWDLanguage::getPHPStringValue('st_denied_permission_to_edit_action_taken','Voc� n�o tem permiss�o para registrar uma a��o em um processo disciplinar.'), E_USER_ERROR);
}

class ScreenEvent extends FWDRunnable {
  public function run(){    
    testPermissionToEditActionTaken();
    
    $miIncidentId = FWDWebLib::getObject('incident_id')->getValue();
    $miUserId = FWDWebLib::getObject('user_id')->getValue();
    
    $moIncUser = new CIIncidentUser();
    $moIncUser->fetchByIncidentUser($miIncidentId, $miUserId);
    
    $moUser = new ISMSUser();
    $moUser->fetchById($miUserId);
    
    $moIncident = new CIIncident();
    $moIncident->fetchById($miIncidentId);
    
    FWDWebLib::getObject('action_description')->setValue($moIncUser->getFieldValue('action_taken'));
    FWDWebLib::getObject('incident_name')->concatValue($moIncident->getFieldValue('incident_name'));
    FWDWebLib::getObject('user_name')->concatValue($moUser->getFieldValue('user_name'));
    
    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));    
  }
}

ISMSLib::testUserPermissionToIncidentMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_disciplinary_process_action.xml');
?>