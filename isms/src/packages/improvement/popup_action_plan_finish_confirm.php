<?php
include_once "include.php";
include_once $handlers_ref . "grid/QueryGridUserTasks.php";

class SaveEfficiencyRevisionEvent extends FWDRunnable {
  public function run() {
    $miAPId = FWDWebLib::getObject('ap_id')->getValue();
    $moActionPlan = new CIActionPlan();
    $moActionPlan->fetchById($miAPId);
    
    $moActionPlan->testPermissionToEdit($miAPId);
    
    $miTaskId = FWDWebLib::getObject('task_id')->getValue();
    $msTaskRevision = "";
    
    $msDateConclusion = ISMSLib::getTimestamp($moActionPlan->getFieldValue('ap_dateconclusion'));

    $msDateRevision = FWDWebLib::getObject('ap_date_efficiency_revision')->getTimestamp();
    $miDaysBefore = intval(FWDWebLib::getObject('ap_days_before')->getValue());

    if ($msDateConclusion > $msDateRevision) {
      echo "js_show('label_revision_warning');";
      return;
    }

    $moActionPlan = new CIActionPlan();
    $moActionPlan->setFieldValue('ap_dateefficiencyrevision',$msDateRevision);
    $moActionPlan->setFieldValue('ap_daysbefore',$miDaysBefore?$miDaysBefore:0);
    $moActionPlan->update($miAPId);

    $moTask = new WKFTask();
    $moTask->setFieldValue('task_date_accomplished', ISMSLib::ISMSTime());
    $moTask->setFieldValue('task_is_visible', 0);
    $moTask->update($miTaskId);

    if(ISMSLib::getConfigById(GENERAL_MANUAL_DATA_CONTROL)){
      $moImprovimentSchedule = new ImprovementSchedule(ISMSLib::getCurrentUserId());
      $moImprovimentSchedule->checkSchedule();

      $moTaskEff = new WKFTask();
      $moTaskEff->createFilter($miAPId,'task_context_id','=');
      $moTaskEff->createFilter('1','task_is_visible','=');
      $moTaskEff->createFilter(ISMSLib::getCurrentUserId(),'task_receiver_id','=');
      $moTaskEff->select();
      if($moTaskEff->fetch()){
        $miDate = mktime (0, 0, 0, date("m",ISMSLib::ISMSTime()), date("d",ISMSLib::ISMSTime()),  date("Y",ISMSLib::ISMSTime()));
        if($miDate > $msDateRevision){
          /*Comentei o c�digo abaixo, pois gerava inconsist�ncia com a feature de abrir pr�xima task automaticamente.
          
          /*task no passado, deve-se mostrar a popup da tarefa de definir a efici�ncia do plano de a��o */
          //$msTaskRevision .= "if(soWindow.open_task_ap_revision) soWindow.open_task_ap_revision(".$moTaskEff->getFieldValue('task_id').",".$moTaskEff->getFieldValue('task_context_id').");";
        }
      }
    }
    
    $msNextTaskJS = "";
		$mbOpenNextTask = (bool)array_filter(explode(':',FWDWebLib::getObject('next_task_controller')->getValue()));
		if ($mbOpenNextTask) {
		  $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
		  $moHandlerTask = new QueryGridUserTasks(FWDWebLib::getConnection());
      $moHandlerTask->setUser($miUserId);
      $moHandlerTask->setOrderBy('task_date_created','-');
      $moHandlerTask->makeQuery();
      $moHandlerTask->executeQuery();
      if ($moHandlerTask->fetch()) {
  		  $msNextTaskJS = 'soWindow.setTimeout("'.ISMSLib::getTaskPopup($moHandlerTask->getFieldValue('task_id'),
  		                                        $moHandlerTask->getFieldValue('task_activity'),
  		                                        $moHandlerTask->getFieldValue('task_context_id'),
  		                                        $moHandlerTask->getFieldValue('task_context_type'),
  		                                        2).',1");';
      }
		}
		
    echo "soWindow=soPopUpManager.getPopUpById('popup_action_plan_finish_confirm').getOpener();"
        ."if (soWindow.refresh_grids) soWindow.refresh_grids();"
        . $msTaskRevision
        ."if (soWindow.close_popup) soWindow.close_popup();"
        ."soWindow=null;"
        ."soPopUpManager.closePopUp('popup_action_plan_finish_confirm');";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new SaveEfficiencyRevisionEvent('save_efficiency_revision_event'));

    //instala a seguran�a de ACL na p�gina
    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $miAutoTaskOpen = FWDWebLib::getObject('auto_task_open')->getValue();
    if (!$miAutoTaskOpen) FWDWebLib::getObject('next_task')->setShouldDraw(false);
    if ($miAutoTaskOpen==2) FWDWebLib::getObject('next_task_controller')->checkItem(1);
    
  $miAPId = FWDWebLib::getObject('ap_id')->getValue();
  $moActionPlan = new CIActionPlan();
  $moActionPlan->fetchById($miAPId);

  $msMessage = FWDWebLib::getObject('confirm_message')->getValue();
  $msName = $moActionPlan->getName();
  if(strlen($msName)> 50){
    $msName = substr($msName,0,47) . '...';
  }
  $msMessage = str_replace('%name%',$msName,$msMessage);
  FWDWebLib::getObject('confirm_message')->setValue($msMessage);

  FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        gebi('ap_days_before').focus();
      </script>
    <?
  }
}
//testa se a licen�a utilizada permite acessar o m�dulo de incidente
ISMSLib::testUserPermissionToIncidentMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_action_plan_finish_confirm.xml");

?>