<?php
include_once "include.php";
include_once $handlers_ref . "select/improvement/QuerySelectNCProcess.php";
include_once $handlers_ref . "grid/QueryGridUserTasks.php";

class ContextEvent extends FWDRunnable {
	public function run(){
		$miTaskId = FWDWebLib::getObject('task_id')->getValue();

		//teste para verificar se o sistema n�o est� sendo hakeado
		$moTaskTest = new WKFTask();
		$moTaskTest->fetchById($miTaskId);

		if($moTaskTest->getFieldValue('task_is_visible')){

			$moTaskTest->testPermissionToExecute($miTaskId);
			$miContextId = $moTaskTest->getFieldValue('task_context_id');
			$msJustification = FWDWebLib::getObject('task_justification')->getValue();
			$miAction = FWDWebLib::getObject('action')->getValue();

			$moContextObject = new ISMSContextObject();
			$moContext = $moContextObject->getContextObjectByContextId($miContextId);
			$moContext->fetchById($miContextId);
			$moContext->stateForward($miAction, $msJustification);

			$moTask = new WKFTask();
			$moTask->setFieldValue('task_date_accomplished', ISMSLib::ISMSTime());
			$moTask->setFieldValue('task_is_visible', 0);
			$moTask->update($miTaskId);

			$msNextTaskJS = "";
			$mbOpenNextTask = (bool)array_filter(explode(':',FWDWebLib::getObject('next_task_controller')->getValue()));
			if ($mbOpenNextTask) {
		  $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
		  $moHandlerTask = new QueryGridUserTasks(FWDWebLib::getConnection());
		  $moHandlerTask->setUser($miUserId);
		  $moHandlerTask->setOrderBy('task_date_created','-');
		  $moHandlerTask->makeQuery();
		  $moHandlerTask->executeQuery();
		  if ($moHandlerTask->fetch()) {
		  	$msNextTaskJS = 'soWindow.setTimeout("'.ISMSLib::getTaskPopup($moHandlerTask->getFieldValue('task_id'),
		  	$moHandlerTask->getFieldValue('task_activity'),
		  	$moHandlerTask->getFieldValue('task_context_id'),
		  	$moHandlerTask->getFieldValue('task_context_type'),2).',1");';
		  }
			}

			echo "soWindow = soPopUpManager.getPopUpById('popup_non_conformity_approval_task').getOpener();"
			."if (soWindow.refresh_grids)"
			."  soWindow.refresh_grids();"
			.$msNextTaskJS
			."soPopUpManager.closePopUp('popup_non_conformity_approval_task');";

		}
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));
		$moStartEvent->addAjaxEvent(new ContextEvent('context_event'));
	}
}

function truncate($psString, $piChars) {
	if (strlen($psString) > $piChars)
	return substr($psString,0,$piChars-3)."...";
	else
	return $psString;
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$miAutoTaskOpen = FWDWebLib::getObject('auto_task_open')->getValue();
		if (!$miAutoTaskOpen) FWDWebLib::getObject('next_task')->setShouldDraw(false);
		if ($miAutoTaskOpen==2) FWDWebLib::getObject('next_task_controller')->checkItem(1);

		$mbDumpHTML = true;
		$miTaskId = FWDWebLib::getObject('task_id')->getValue();
		$moTask = new WKFTask();
		$moTask->fetchById($miTaskId);

		if($moTask->getFieldValue('task_is_visible')){
				
			//teste para verificar se o sistema n�o est� sendo hakeado
			$moTask->testPermissionToExecute($miTaskId);

			FWDWebLib::getObject('task_date')->setValue(ISMSLib::getISMSDate($moTask->getFieldValue('task_date_created')));

			$moUser = new ISMSUser();
			$moUser->fetchById($moTask->getFieldValue('task_creator_id'));
			FWDWebLib::getObject('task_inquirer')->setValue($moUser->getFieldValue('user_name'));

			FWDWebLib::getObject('task_name')->setValue(ISMSActivity::getDescription($moTask->getFieldValue('task_activity')));


			$miNCId = $moTask->getFieldValue('task_context_id');


			$moNonConformity = new CINonConformity();
			if($moNonConformity->fetchById($moTask->getFieldValue('task_context_id'))){
				$msNonConformityName = truncate($moNonConformity->getFieldValue('nc_name'),35);
				$msNonConformityDescription = truncate($moNonConformity->getFieldValue('nc_description'),35);
				$msNCLink = "<a href='javascript:open_visualize({$miNCId},".CONTEXT_CI_NON_CONFORMITY.",\"".uniqid()."\")'>".$msNonConformityName."</a>";

				FWDWebLib::getObject('nc_name')->setAttrStringNoEscape(true);
				FWDWebLib::getObject('nc_name')->setValue($msNCLink);
				FWDWebLib::getObject('nc_description')->setValue($msNonConformityDescription);

				$moSelect = FWDWebLib::getObject('nc_processes');
				$moHandler = new QuerySelectNCProcess(FWDWebLib::getConnection());
				$moHandler->setNc($miNCId);
				$maValues = $moHandler->getValues();
				$msValues = '';
				foreach($maValues as $maOption) {
					$moItem = new FWDItem();
					$moItem->setAttrKey($maOption[0]);
					$moItem->setValue($maOption[1]);
					$moSelect->addObjFWDItem($moItem);
					unset($moItem);
					$msValues.=":$maOption[0]";
				}

			}else{
				$mbDumpHTML = false;
			}
			if($mbDumpHTML){

				FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
			}else{
				return FWDLanguage::getPHPStringValue('st_denied_permission_to_execute_task','Voc� n�o tem permiss�o para executar a tarefa ');
			}

		}
		?>
<script language="javascript">
        gebi('task_justification').focus();
        soPopUpManager.getPopUpById('popup_non_conformity_approval_task').setCloseEvent(
        function(){
          soWindow = soPopUpManager.getPopUpById('popup_non_conformity_approval_task').getOpener();
          if (soWindow.close_popup) soWindow.close_popup();
          soWindow = null;
        });
      </script>
		<?
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_non_conformity_approval_task.xml");
?>