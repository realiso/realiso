<?php
include_once "include.php";
include_once $handlers_ref . "grid/improvement/QueryGridNCProcess.php";
include_once $handlers_ref . "QueryContextHierarchicalStructure.php";

function getCurrentIds(){
  $msValues = FWDWebLib::getObject('var_current_ids')->getValue();
  $maIds = array_filter(explode(':', $msValues));
  return $maIds;
}

class DrawGridSearch extends FWDDrawGrid {
  public function DrawItem(){
    switch($this->ciColumnIndex){
      case 1:
        $miProcessValue = $this->caData[1];
        $msProcessColor = RMRiskConfig::getRiskColor($miProcessValue);
        $this->coCellBox->setIconSrc("icon-process_".$msProcessColor.".gif");
        return parent::drawItem();
        break;
      case 2:
        $miProcessValue = $this->caData[1];
        $msLinkColor = RMRiskConfig::getRiskColor($miProcessValue,true);
        $msLink = "<font color=".$msLinkColor.">".$this->caData[2]."</font>";
        $this->coCellBox->setAttrStringNoEscape("true");
        $this->coCellBox->setValue("<a href='javascript:open_visualize(".$this->caData[4].",".CONTEXT_PROCESS.",\"".uniqid()."\")'>{$msLink}</a>");
        return $this->coCellBox->draw();
        break;
      default:
        return parent::drawItem();
        break;
    }
  }
}


class SearchProcessEvent extends FWDRunnable {
  public function run(){
    $moGrid = FWDWebLib::getObject('grid_process_search');
    $moGrid->execEventPopulate();
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new SearchProcessEvent('search_process_event'));
    
    $moGrid = FWDWebLib::getObject('grid_process_search');
    $moHandler = new QueryGridNCProcess(FWDWebLib::getConnection());
    $moHandler->setNameFilter(FWDWebLib::getObject('name_filter')->getValue());
    $moHandler->setAreaFilter(FWDWebLib::getObject('area_filter')->getValue());
    $moHandler->setExcluded(getCurrentIds());
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new DrawGridSearch());

    $moGrid = FWDWebLib::getObject('grid_current_processes');
    $moHandler = new QueryGridNCProcess(FWDWebLib::getConnection());
    $moHandler->showProcesses(getCurrentIds());
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new DrawGridSearch());
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    FWDWebLib::getObject('var_current_ids')->setValue(FWDWebLib::getObject('current_ids')->getValue());
    
    $moSelect = FWDWebLib::getObject('process_area_id');
    $moQuery = new QueryContextHierarchicalStructure(FWDWebLib::getConnection());
    $moQuery->setContextType(CONTEXT_AREA);
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    foreach ($moQuery->getContexts() as $maContext) {
      list($miContextId,$msHierarchicalName) = $maContext;
      $moSelect->setItemValue($miContextId,$msHierarchicalName);
    }
    
    $moGrid = FWDWebLib::getObject('grid_current_processes');
    $moHandler = new QueryGridNCProcess(FWDWebLib::getConnection());
    $moHandler->showProcesses(getCurrentIds());
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new DrawGridSearch());
    
    //instala a seguran�a de ACL na p�gina
    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
?>
<script language="javascript">
  gebi('process_name').focus();
  
  function refresh_grid() {
    js_refresh_grid('grid_process_search');
    js_refresh_grid('grid_current_processes');
  }
  function enter_process_search_event(e)
	{
		if (!e) e = window.event;
		if(e['keyCode']==13) {
      gebi('name_filter').value = gebi('process_name').value;
      gebi('area_filter').value = gebi('process_area_id').value;
      gebi('var_current_ids').value = gobi('grid_current_processes').getAllIds().join(':');
	    trigger_event("search_process_event","3");
		}
	}
	FWDEventManager.addEvent(gebi('process_name'), 'keydown', enter_process_search_event);
</script>
<?
  }
}
//testa se a licen�a utilizada permite acessar o m�dulo de incidente
ISMSLib::testUserPermissionToIncidentMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load('popup_process_search.xml');

?>