<?php

include_once "include.php";

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();

    $moWebLib->getObject('save')->setAttrDisplay('false');
    
    $miIncidentId = FWDWebLib::getObject('url_param_incident_id')->getValue();

    $moIncident = new CIIncident();
    if ($moIncident->fetchById($miIncidentId)) {
      $solution = $moIncident->getFieldValue('incident_solution_description');
      $moWebLib->getObject('popup_incident_solution_memo')->setValue(str_replace("<br/>", "\n", $solution));
    }

    $moWebLib->getObject('popup_incident_solution_memo')->setAttrReadonly(true);
    
    $moEvent = new FWDClientEvent();
    $moEvent->setAttrEvent('onClick');
    $moEvent->setAttrValue("soPopUpManager.closePopUp('popup_incident_solution');");
    $moWebLib->getObject('close')->addObjFWDEvent($moEvent);

    //instala a seguran�a de ACL na p�gina
    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('popup_incident_solution_dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('popup_incident_solution_dialog'));
  }
}

// testa se a licen�a utilizada permite acessar o m�dulo de incidente
ISMSLib::testUserPermissionToIncidentMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_incident_solution.xml');

?>