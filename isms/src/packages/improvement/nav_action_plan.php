<?php

include_once 'include.php';
include_once $handlers_ref . 'select/improvement/QuerySelectNcActionPlan.php';
include_once $handlers_ref . 'grid/improvement/QueryGridActionPlan.php';
include_once $classes_isms_ref . 'nonauto/ISMSContextDrawGrid.php';
include_once $handlers_ref . 'QueryTaskByContext.php';

class ActionPlanDrawGrid extends ISMSContextDrawGrid {

  public function __construct(){
    parent::__construct('icon-ci_action_plan.gif',CONTEXT_CI_ACTION_PLAN);
    $this->setupToolTipCreateModify('creator_name','date_created','modifier_name','date_modified');
    $this->setColumnType('ap_datedeadline', ISMSContextDrawGrid::TYPE_SHORT_DATE);
    $this->setColumnType('ap_dateconclusion', ISMSContextDrawGrid::TYPE_SHORT_DATE);
    $this->setColumnType('ap_dateefficiencyrevision', ISMSContextDrawGrid::TYPE_DATE);
    $this->setColumnType('ap_dateefficiencymeasured', ISMSContextDrawGrid::TYPE_DATE);
    $this->setColumnType('ap_state', ISMSContextDrawGrid::TYPE_STATE);
  }

 public function drawItem(){

   switch($this->ciColumnIndex){
      case $this->getIndexByAlias('ap_name'):
        $this->coCellBox->setAttrStringNoEscape("true");
        $moActionPlan = new CIActionPlan();
        if($moActionPlan->userCanEdit($this->getFieldValue('ap_id'))){
          $this->coCellBox->setValue("<a href='javascript:open_edit(".$this->getFieldValue('ap_id').");'>".$this->coCellBox->getValue()."</a>");
        }else{
          $this->coCellBox->setValue("<a href='javascript:open_visualize(".$this->getFieldValue('ap_id').",".CONTEXT_CI_ACTION_PLAN .",\"".uniqid()."\");'>".$this->coCellBox->getValue()."</a>");
        }
        return $this->coCellBox->draw();
        break;
        
      case $this->getIndexByAlias('ap_actiontype'):
          $msStr = '';
          switch($this->coCellBox->getValue()){
              case ACTION_PLAN_CORRECTIVE_TYPE:
                $msStr = FWDLanguage::getPHPStringValue('mx_corrective', 'Corretiva');
              break;
              case ACTION_PLAN_PREVENTIVE_TYPE:
                $msStr = FWDLanguage::getPHPStringValue('mx_preventive', 'Preventiva');
                break;
              default:
                $msStr = '';
              break;
          }
          $this->coCellBox->setValue($msStr);
          $msReturn = '';
          $miUserId = ISMSLib::getCurrentUserId();

          $queryTaskByContext = new QueryTaskByContext();
          $queryTaskByContext->setContextId($this->getFieldValue('ap_id'));
          $queryTaskByContext->makeQuery();
           
          if($queryTaskByContext->getReceiverId() == $miUserId) {
            $moVariable = new FWDVariable();
            $moVariable->setAttrNoEscape('false');
            $moVariable->setAttrName('task_id_'.$this->getFieldValue('ap_id'));
            
            $moVariable->setValue($queryTaskByContext->getTaskId());
            $msReturn .= $moVariable->drawBody();            
          }
          return $this->coCellBox->draw() . " " . $msReturn . " ";
      break;
      
      case $this->getIndexByAlias('count_nc'):
        $this->coCellBox->setAttrStringNoEscape("true");
        if(ISMSLib::userHasACL('M.CI.3')){
          $this->coCellBox->setValue("<a href='javascript:go_to_nav_nc(".$this->getFieldValue('ap_id').");'>".$this->coCellBox->getValue()."</a>");
        }
        return $this->coCellBox->draw();
      break;
      
      default:
        return parent::drawItem();
      break;
    }
  }

  protected function setupMenu(){
    $miUserId = ISMSLib::getCurrentUserId();
    $miActionPlanId = $this->coCellBox->getValue();
    $moActionPlan = new CIActionPlan();
    $moActionPlan->fetchById($miActionPlanId);
    $miState = $moActionPlan->getContextState() ;
    
    /*obtem o proximo estado da NC*/
    $maMenuACLs = array('edit','delete','approve','meassure','define_date_meassure','policy_mode','non_conformity');
    $maAllowed = array();
    if (FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(POLICY_MODE))
      $maAllowed[] = 'policy_mode';
    if($moActionPlan->userCanEdit($miActionPlanId)){ // teste se o usuario pode editar
      $maAllowed[] = 'edit';
      if( $miState == CONTEXT_STATE_AP_WAITING_CONCLUSION || $miState == CONTEXT_STATE_AP_WAITING_MEASUREMENT ){
        $maAllowed[] = 'non_conformity';
      }
    }
    if($moActionPlan->userCanDelete($miActionPlanId)){ // teste se o usuario pode remover
      $maAllowed[] = 'delete';
    }

    $queryTaskByContext = new QueryTaskByContext();
    $queryTaskByContext->setContextId($miActionPlanId);
    $queryTaskByContext->makeQuery();

    if($queryTaskByContext->getReceiverId() == $miUserId){
      
      switch($miState){
        case CONTEXT_STATE_AP_PENDANT:{
             $maAllowed[] = 'approve';
         break;
        }
        case CONTEXT_STATE_AP_WAITING_MEASUREMENT:{
            if($moActionPlan->getFieldValue('ap_dateefficiencyrevision')){
              $maAllowed[] = 'meassure';
            }else{
              $maAllowed[] = 'define_date_meassure';
            }
          break;
        }
        default:{ break; }
      }
    }

    $moACL = FWDACLSecurity::getInstance();
    $moACL->setNotAllowed(array_diff($maMenuACLs,$maAllowed));
    $moMenu = FWDWebLib::getObject('menu');
    $moGrid = FWDWebLib::getObject('grid_action_plan');
    $msLine = $moGrid->getAttrName().'_acl_'.$this->ciRowIndex;
    FWDGridMenuACLSecurity::getInstance()->installGridMenuACL($moMenu,$moACL,$moGrid,$msLine);
  }

}

class ConfirmRemove extends FWDRunnable {
  public function run() {
    $msTitle = FWDLanguage::getPHPStringValue('tt_ap_remove','Remover Plano de A��o');
    $msMessage = FWDLanguage::getPHPStringValue('st_ap_remove_message','Voc� tem certeza que deseja remover o plano de a��o <b>%name%</b>?');

    $moActionPlan = new CIActionPlan();
    $miActionPlanId = intval(FWDWebLib::getObject('selected_ap_id')->getValue());
    $moActionPlan->fetchById($miActionPlanId);
    $msName = ISMSLib::truncateString($moActionPlan->getFieldValue('ap_name'), 70);
    $msMessage = str_replace('%name%',$msName,$msMessage);

    $msEventValue = "soPopUpManager.getPopUpById('popup_confirm').getOpener().remove();";

    ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,50);
  }
}

class RemoveEvent extends FWDRunnable {
  public function run() {
    $miActionPlanId = intval(FWDWebLib::getObject('selected_ap_id')->getValue());

    $moCtxUserTest = new CIActionPlan();
    $moCtxUserTest->fetchById($miActionPlanId);
    $moCtxUserTest->testPermissionToDelete($miActionPlanId);

    $moActionPlan = new CIActionPlan();
    if($moActionPlan->isDeletable($miActionPlanId)){
      $moActionPlan->delete($miActionPlanId);
      FWDWebLib::getObject('grid_action_plan')->execEventPopulate();
      echo 'refresh_grid();';
    }else{
      $moActionPlan->showDeleteError();
    }
  }
}

class OpenPopupNcAssociateEvent extends FWDRunnable {
  public function run(){
    $moQuery = new QuerySelectNcActionPlan();
    $moQuery->setActionPlan(FWDWebLib::getObject('selected_ap_id')->getValue());
    $msIds = implode(':',$moQuery->getIds());
    echo "isms_open_popup('popup_nc_associate','packages/improvement/popup_nc_associate.php?ids=$msIds','','true');"
        ."gobi('grid_action_plan').hideGlass();";
  }
}

class UpdateNcActionPlanRelationEvent extends FWDRunnable {
  public function run(){
    $miActionPlanId = FWDWebLib::getObject('selected_ap_id')->getValue();
    $moActionPlan = new CIActionPlan();
    $moActionPlan->setFieldValue('ap_id',$miActionPlanId);
    $maNcs = array_filter(explode(':',FWDWebLib::getObject('var_associated_ids')->getValue()));
    $moActionPlan->updateRelation(new CINcActionPlan(),'nc_id',$maNcs);
    echo "refresh_grid();";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
    $moStartEvent->addAjaxEvent(new ConfirmRemove('confirm_remove'));
    $moStartEvent->addAjaxEvent(new RemoveEvent('remove_event'));
    $moStartEvent->addAjaxEvent(new OpenPopupNcAssociateEvent('open_popup_nc_associate'));
    $moStartEvent->addAjaxEvent(new UpdateNcActionPlanRelationEvent('update_nc_ap_relation'));

    //instala a seguran�a de ACL na p�gina
    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
    FWDMenuACLSecurity::getInstance()->installMenuACL(FWDWebLib::getObject('menu'),FWDACLSecurity::getInstance());

    $moGrid = FWDWebLib::getObject('grid_action_plan');
    $moHandler = new QueryGridActionPlan(FWDWebLib::getConnection());
    
    if(FWDWebLib::getObject('par_nc_id')->getValue()){
      $moHandler->setNC(FWDWebLib::getObject('par_nc_id')->getValue());
    }
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new ActionPlanDrawGrid());
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moActionPlan = new CIActionPlan();
    if(!$moActionPlan->userCanInsert()){
      FWDWebLib::getObject('insert')->setShouldDraw(false);
    }
    if(ISMSLib::getConfigById(GENERAL_MANUAL_DATA_CONTROL)){
      $mbHasManualData = "var sbHasManualData=1;";
    }else{
      $mbHasManualData = "var sbHasManualData=0;";
    }
    
    if(FWDWebLib::getObject('par_nc_id')->getValue()){
      $moNC = new CINonConformity();
      $moNC->fetchById(FWDWebLib::getObject('par_nc_id')->getValue());
      FWDWebLib::getObject('ap_grid_title')->setValue(FWDWebLib::getObject('ap_grid_title')->getValue() . " " .str_replace('%nc_name%',$moNC->getName(),FWDLanguage::getPHPStringValue('tt_nc_filter_grid_action_plan',"(filtrado pela n�o conformidade '<b>%nc_name%</b>')")));
    }
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language='javascript'>
        <?echo $mbHasManualData?>
        function open_task_ap_revision(task_id,context_id) {
          isms_open_popup('popup_action_plan_revision_task', 'packages/improvement/popup_action_plan_revision_task.php?task='+task_id+'&ap='+context_id, '', 'true');
        }

        function refresh_grid(){
          gobi('grid_action_plan').refresh();
        }

        function refresh_grids(){
          gobi('grid_action_plan').refresh();
        }
        
        function remove(){
          trigger_event('remove_event',3);
          refresh_grid();
        }

        function open_popup_nc_associate(){
          gobi('grid_action_plan').showGlass();
          gebi('selected_ap_id').value = FWDMenu.value;
          trigger_event('open_popup_nc_associate',3);
        }

        function set_ncs(psIds){
          gebi('var_associated_ids').value = psIds;
          trigger_event('update_nc_ap_relation',3);
        }
        
        function open_edit(piId){
          isms_open_popup('popup_action_plan_edit','packages/improvement/popup_action_plan_edit.php?action_plan='+piId,'','true');
        }
        
        function go_to_nav_nc(piId){
          isms_change_to_sibling(3,'nav_non_conformity.php?ap_id='+piId);
        }
        
      </script>
    <?
  }
}

// Testa se a licen�a utilizada permite acessar este m�dulo
//ISMSLib::testUserPermissionTo%module%Mode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('nav_action_plan.xml');

?>
