<?php
include_once "include.php";
include_once $handlers_ref . "select/improvement/QuerySelectCategory.php";
include_once $handlers_ref . "QueryGetReportClassification.php";
include_once $handlers_ref . "select/QuerySelectNonConformityType.php";


/*
 * Colocar aqui todos os relat�rios que possuem filtros.
 * Para cada linha (relat�rio) da grid � criado um evento
 * que faz com que quando um relat�rio seja selecionado,
 * ele esconda todos os filtros da tela e mostre somente
 * os filtros pertinentes ao relat�rio clicado.
 */
class GridReports extends FWDDrawGrid {
  public function drawItem(){
    switch($this->ciColumnIndex){
      case 2:{
        $moEvent = new FWDClientEvent();
        $moEvent->setAttrEvent('onClick');
        $moEvent->setAttrValue("FilterManager.showFilters('{$this->caData[1]}');");
        $this->coCellBox->addObjFWDEvent($moEvent);
        return parent::drawItem();
      }
      default:{
        return parent::drawItem();
      }
    }
  }
}

class ISMSReportEvent extends FWDRunnable {
	  
  private function getReportIncidentFilter(){
  	$moFilter = new ISMSReportIncidentFilter();
  	$miStatus = (int) FWDWebLib::getObject('select_incident_status')->getValue();
  	$miCategory = (int) FWDWebLib::getObject('select_incident_category')->getValue();
  	$miLossType = (int) FWDWebLib::getObject('select_incident_loss_type')->getValue();
  	$msOrderByType = FWDWebLib::getObject('select_incident_order_by_type')->getValue();
  	
  	$dateStart = FWDWebLib::getObject('filter_incident_date_start')->getTimestamp();
  	$dateFinish = FWDWebLib::getObject('filter_incident_date_finish')->getTimestamp();
  	
  	if ($miStatus != 0) $moFilter->setStatus($miStatus);
  	if ($miCategory != 0) $moFilter->setCategory($miCategory);
  	if ($miLossType != 0) $moFilter->setLossType($miLossType);
  	if ($dateStart) $moFilter->setDateStart($dateStart);
  	if ($dateFinish) $moFilter->setDateFinish($dateFinish);

    return $moFilter;
  }
  
  private function getReportActionPlanFilter(){
  	$moFilter = new ISMSReportActionPlanFilter();
  	$miActionType = (int) FWDWebLib::getObject('select_action_type')->getValue();
  	$moFilter->setActionType($miActionType);
    return $moFilter;
  }  
  
  private function getReportIncidentAccompanimentFilter(){
  	$moFilter = $this->getReportIncidentFilter();
  	
  	$moFilter->setDateStart(0);
  	$moFilter->setDateFinish(0);
  	
  	if (FWDWebLib::getObject('filter_immediate_disposal_start')->getTimestamp())
  		$moFilter->setDisposalDateStart(FWDWebLib::getObject('filter_immediate_disposal_start')->getTimestamp());
  	if (FWDWebLib::getObject('filter_immediate_disposal_finish')->getTimestamp())
  		$moFilter->setDisposalDateFinish(FWDWebLib::getObject('filter_immediate_disposal_finish')->getTimestamp());
  	if (FWDWebLib::getObject('filter_solution_start')->getTimestamp())
  		$moFilter->setSolutionDateStart(FWDWebLib::getObject('filter_solution_start')->getTimestamp());
  	if (FWDWebLib::getObject('filter_solution_finish')->getTimestamp())
  		$moFilter->setSolutionDateFinish(FWDWebLib::getObject('filter_solution_finish')->getTimestamp());
  	  	
    return $moFilter;
  }
  
  private function getReportOccurrenceFilter(){
  	$moFilter = new ISMSReportOccurrenceFilter();
  	$miStatus = (int) FWDWebLib::getObject('select_occurrence_status')->getValue();  	  	
  	if ($miStatus != 0) $moFilter->setStatus($miStatus);
    return $moFilter;
  }
  
  private function getReportNonConformityFilter(){
  	$moFilter = new ISMSReportNonConformityFilter();
  	$miClassification = (int) FWDWebLib::getObject('select_nc_classification')->getValue();  	  	
  	$miCapability = (int) FWDWebLib::getObject('select_nc_capability')->getValue();
  	$miStatus = (int) FWDWebLib::getObject('select_nc_status')->getValue();
  	if ($miClassification != 0) $moFilter->setClassificationType($miClassification);
  	if ($miCapability != 0) $moFilter->setCapability($miCapability);
  	if ($miStatus != 0) $moFilter->setStatus($miStatus);
    return $moFilter;
  }
  
  private function getReportNonConformityAccompanimentFilter(){
  	$moFilter = $this->getReportNonConformityFilter();
  	
  	if (FWDWebLib::getObject('filter_estimated_conclusion_start')->getTimestamp())
  		$moFilter->setEstimatedConclusionDateStart(FWDWebLib::getObject('filter_estimated_conclusion_start')->getTimestamp());
  	if (FWDWebLib::getObject('filter_estimated_conclusion_finish')->getTimestamp())
  		$moFilter->setEstimatedConclusionDateFinish(FWDWebLib::getObject('filter_estimated_conclusion_finish')->getTimestamp());
  	if (FWDWebLib::getObject('filter_conclusion_start')->getTimestamp())
  		$moFilter->setConclusionDateStart(FWDWebLib::getObject('filter_conclusion_start')->getTimestamp());
  	if (FWDWebLib::getObject('filter_conclusion_finish')->getTimestamp())
  		$moFilter->setConclusionDateFinish(FWDWebLib::getObject('filter_conclusion_finish')->getTimestamp());
  	
    return $moFilter;
  }
  
  private function getReportElementsFilter() {  	
  	$moFilter = new ISMSReportElementsFilter();  	
  	$maCheckboxValue = FWDWebLib::getObject('filter_elements_controller')->getAllItemsCheck();  	
  	foreach($maCheckboxValue as $moItem) {
      switch ($moItem->getAttrKey()) {
        case 'process':
          $moFilter->showProcess(true);
        break;
        case 'asset':
          $moFilter->showAsset(true);
        break;
        case 'risk':
          $moFilter->showRisk(true);
        break;
        case 'control':
          $moFilter->showControl(true);
        break;
      }
    }  	
    return $moFilter;  	
  }

  /*
   * Essa fun��o serve para retornar o filtro do relat�rio.
   * De acordo com o relat�rio selecionado, chama um fun��o
   * espec�fica para montar o filtro do relat�rio em quest�o.
   */
  private function getFilter() {
    $maValue = FWDWebLib::getObject('grid_reports')->getValue();
    $moReportFilter = null;
    switch($maValue[0]) {
      case 'report_occurrence_by_incident':   						$moReportFilter = $this->getReportIncidentFilter();										break;
      case 'report_user_by_incident':   									$moReportFilter = $this->getReportIncidentFilter();										break;
      case 'report_incident_by_responsible':							$moReportFilter = $this->getReportIncidentFilter();										break;
      case 'report_occurrence_accompaniment':							$moReportFilter = $this->getReportOccurrenceFilter();									break;
      case 'report_incident_accompaniment':								$moReportFilter = $this->getReportIncidentAccompanimentFilter();  		break;
      case 'report_nonconformity_by_process':							$moReportFilter = $this->getReportNonConformityFilter();							break;
      case 'report_control_nonconformity_by_process':			$moReportFilter = $this->getReportNonConformityFilter();							break;
      case 'report_nonconformity_accompaniment':					$moReportFilter = $this->getReportNonConformityAccompanimentFilter();	break;
      case 'report_financial_impact_by_incident':					$moReportFilter = $this->getReportIncidentFilter();										break;
      case 'report_elements_affected_by_incident':				$moReportFilter = $this->getReportElementsFilter();										break;
      case 'report_control_by_process':				$moReportFilter = $this->getReportElementsFilter();										break;
      
      case 'report_action_plan_by_user':				$moReportFilter = $this->getReportActionPlanFilter();										break;
      case 'report_action_plan_by_process':				$moReportFilter = $this->getReportActionPlanFilter();										break;
      case 'report_ap_without_doc':				        $moReportFilter = $this->getReportActionPlanFilter();										break;
      
      default:                                						$moReportFilter = new FWDReportFilter();															break;
    }
    $moReportFilter->setManualClassification(FWDWebLib::getObject('select_report_classification')->getValue());
    $moReportFilter->setFileType((int) FWDWebLib::getObject('select_report_type')->getValue());
    
    FWDWebLib::getObject('report_comment')->getObjFWDString()->setAttrNoEscape('true');
    
    $moReportFilter->setComment(FWDWebLib::convertToISO(FWDWebLib::getObject('report_comment')->getValue(), true));
    return $moReportFilter;
  }

  /*
   * Fun��o para retornar o relat�rio que deve ser gerado.
   * � utilizada quando o relat�rio que foi clicado deve
   * chamar um outro relat�rio.
   */
  private function getReport(){
    $maGridValue = FWDWebLib::getObject('grid_reports')->getValue();
    $msReport = $maGridValue[0];
    switch($msReport){
      case 'report_pendant_tasks_ci':
        return str_replace(':','',FWDWebLib::getObject('pendant_tasks_filter_radiobox')->getValue());
      break;
      default:
        return $msReport;
      break;
    }
  }

  /*
   * Verifica se o filtro do relat�rio foi preenchido corretamente.
   */
  public function validateFilters(){
    $maValue = FWDWebLib::getObject('grid_reports')->getValue();
    switch($maValue[0]) {
    	case 'report_elements_affected_by_incident':
        if (count(FWDWebLib::getObject('filter_elements_controller')->getAllItemsCheck())) return true;
        else echo "js_show('elements_filter_warning');";
      break;
      
      default:{
        return true;
      }
    }
  }

  public function run(){
    $maValue = FWDWebLib::getObject('grid_reports')->getValue();
    if(isset($maValue[0])){
      if($this->validateFilters()){
        set_time_limit(3000);

        $mbDontForceDownload = (FWDWebLib::getObject("select_report_type")->getValue()==REPORT_FILETYPE_HTML);

        $moWindowReport = new ISMSReportWindow(INCIDENT_MODE);
        if($mbDontForceDownload) $moWindowReport->open();

        $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
        $moSession->deleteAttribute("filter");
        $moSession->addAttribute("filter");
        $moSession->setAttrFilter($this->getFilter());

        $msReport = $this->getReport();

        if($mbDontForceDownload){
          $moWindowReport->setWaitLoadingComplete(true);
          $moWindowReport->loadReport($msReport);
        }else{
          $moWindowReport->forceReportDownload($msReport);
        }
      }
    }else{
      echo "js_show('warning');";
    }
  }
}

/*
 * Fun��o que retorna um array associativo do tipo array('report_category' => array('report1', 'report2', ...))
 * para mapear quais relat�rios pertencem a quais categorias
 */
function getReportCategoryArray(){
  $moConfig = new ISMSConfig();
  $mbHasRevision = $moConfig->getConfig(GENERAL_REVISION_ENABLED);
  $mbHasCost = $moConfig->getConfig(GENERAL_COST_ENABLED);
  
  $maIncidentReportAux1 = array(
      'report_occurrence_by_incident' => array(FWDLanguage::getPHPStringValue('mx_report_occurrence_by_incident','Ocorr�ncias por Incidente'),'M.CI.6.1.1'),
      'report_user_by_incident' => array(FWDLanguage::getPHPStringValue('mx_report_user_by_incident','Processo Disciplinar'),'M.CI.6.1.2'),
      'report_incident_by_responsible' => array(FWDLanguage::getPHPStringValue('mx_report_incident_by_responsible','Coleta de Evid�ncias'),'M.CI.6.1.3'),
      'report_occurrence_accompaniment' => array(FWDLanguage::getPHPStringValue('mx_report_occurrence_accompaniment','Acompanhamento de Ocorr�ncia'),'M.CI.6.1.4'),
      'report_incident_accompaniment' => array(FWDLanguage::getPHPStringValue('mx_report_incident_accompaniment','Acompanhamento de Incidente'),'M.CI.6.1.5')
  );

  if($mbHasCost){
    $maIncidentReportFinancial = array(  
      'report_financial_impact_by_incident' => array(FWDLanguage::getPHPStringValue('mx_report_financial_impact_by_incident','Impacto Financeiro por Incidente'),'M.CI.6.1.6')
    );
  }else{
    $maIncidentReportFinancial = array();
  }
  
  if($mbHasRevision){
    $maIncidentReportControlRevision = array (
      'report_incident_control_revision' => array(FWDLanguage::getPHPStringValue('mx_report_incident_control_revision','Revis�es de Controles ocasionadas por Incidentes'),'M.CI.6.1.7')
    );
  }else{
    $maIncidentReportControlRevision = array();
  }
  
  $maIncidentReportAux2 = array(
    'report_elements_affected_by_incident' => array(FWDLanguage::getPHPStringValue('mx_report_elements_affected_by_incident','Elementos afetados por Incidentes'),'M.CI.6.1.8'),
    'report_risk_auto_probability_value' => array(FWDLanguage::getPHPStringValue('mx_report_risk_auto_probability_value','C�lculo Autom�tico de Probabilidade dos Riscos'),'M.CI.6.1.9')
  );

  $maIncidentReport = array_merge($maIncidentReportAux1,$maIncidentReportFinancial,$maIncidentReportControlRevision,$maIncidentReportAux2);
  
  $maNCReport = array(
    'report_nonconformity_by_process' => array(FWDLanguage::getPHPStringValue('mx_report_nonconformity_by_process','N�o Conformidade por Processo'),'M.CI.6.2.1'),
    'report_control_nonconformity_by_process' => array(FWDLanguage::getPHPStringValue('mx_report_control_nonconformity_by_process','N�o Conformidade de Controle por Processo'),'M.CI.6.2.2'),
    'report_nonconformity_accompaniment' => array(FWDLanguage::getPHPStringValue('mx_report_nonconformity_accompaniment','Acompanhamento de N�o Conformidade'),'M.CI.6.2.3'),    
    'report_action_plan_by_user' => array(FWDLanguage::getPHPStringValue('mx_report_action_plan_by_user','Plano de A��o por Usu�rio'),'M.CI.6.2.4'),
    'report_action_plan_by_process' => array(FWDLanguage::getPHPStringValue('mx_report_action_plan_by_process','Plano de A��o por Processo'),'M.CI.6.2.5')
  );
  
  $maIncidentManagerReport = array(
    'report_pendant_tasks_ci' => array(FWDLanguage::getPHPStringValue('mx_report_ci_pendant_tasks','Tarefas pendentes da Melhoria Cont�nua'),'M.CI.6.3.1'),
    'report_incident_without_risk' => array(FWDLanguage::getPHPStringValue('mx_report_incident_without_risk','Incidente sem Risco'),'M.CI.6.3.2'),
    'report_nc_without_ap' => array(FWDLanguage::getPHPStringValue('mx_report_nc_without_ap','N�o Conformidade Sem Plano de A��o'),'M.CI.6.3.3'),
    'report_ap_without_doc' => array(FWDLanguage::getPHPStringValue('mx_report_ap_without_doc','Plano de A��o sem Documento'),'M.CI.6.3.4'),
    'report_incident_without_occurrence' => array(FWDLanguage::getPHPStringValue('mx_report_incidents_without_occurrence','Incidentes sem Ocorr�ncia'),'M.CI.6.3.5'),
  );

  return array(
    'incident' => $maIncidentReport,
    'nc' => $maNCReport,
    'ci_manager' => $maIncidentManagerReport
  );
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new ISMSReportEvent("function_report"));
    $moStartEvent->addAjaxEvent(new ShowReportsEvent("show_reports"));
    
		/* Popula o select de Categoria de Incidentes */
    $moSelect = FWDWebLib::getObject('select_incident_category');
    $moHandler = new QuerySelectCategory(FWDWebLib::getConnection());
    $moSelect->setQueryHandler($moHandler);
    $moSelect->populate();
    
    /* Popula o select do Status do Incidente */
    $moSelect = FWDWebLib::getObject('select_incident_status');
    $moContext = new ISMSContextObject();
    $moSelect->setItemValue(CONTEXT_STATE_INC_OPEN,$moContext->getContextStateAsString(CONTEXT_STATE_INC_OPEN));
    $moSelect->setItemValue(CONTEXT_STATE_INC_DIRECTED,$moContext->getContextStateAsString(CONTEXT_STATE_INC_DIRECTED));
    $moSelect->setItemValue(CONTEXT_STATE_INC_PENDANT_DISPOSAL,$moContext->getContextStateAsString(CONTEXT_STATE_INC_PENDANT_DISPOSAL));
    $moSelect->setItemValue(CONTEXT_STATE_INC_WAITING_SOLUTION,$moContext->getContextStateAsString(CONTEXT_STATE_INC_WAITING_SOLUTION));
    $moSelect->setItemValue(CONTEXT_STATE_INC_PENDANT_SOLUTION,$moContext->getContextStateAsString(CONTEXT_STATE_INC_PENDANT_SOLUTION));
    $moSelect->setItemValue(CONTEXT_STATE_INC_SOLVED,$moContext->getContextStateAsString(CONTEXT_STATE_INC_SOLVED));
    
    /* Popula o select do Status da Ocorr�ncia */
    $moSelect = FWDWebLib::getObject('select_occurrence_status');
    $moContext = new ISMSContextObject();
    $moSelect->setItemValue(CONTEXT_STATE_PENDANT,$moContext->getContextStateAsString(CONTEXT_STATE_PENDANT));
    $moSelect->setItemValue(CONTEXT_STATE_APPROVED,$moContext->getContextStateAsString(CONTEXT_STATE_APPROVED));
    $moSelect->setItemValue(CONTEXT_STATE_DENIED,$moContext->getContextStateAsString(CONTEXT_STATE_DENIED));
    
    /* Popula o select do Status da N�o Conformidade */
    $moSelect = FWDWebLib::getObject('select_nc_status');
    $moContext = new ISMSContextObject();
    $moSelect->setItemValue(CONTEXT_STATE_CI_SENT,$moContext->getContextStateAsString(CONTEXT_STATE_CI_SENT));
    $moSelect->setItemValue(CONTEXT_STATE_CI_OPEN,$moContext->getContextStateAsString(CONTEXT_STATE_CI_OPEN));
    $moSelect->setItemValue(CONTEXT_STATE_CI_DIRECTED,$moContext->getContextStateAsString(CONTEXT_STATE_CI_DIRECTED));
    $moSelect->setItemValue(CONTEXT_STATE_CI_NC_PENDANT,$moContext->getContextStateAsString(CONTEXT_STATE_CI_NC_PENDANT));
    $moSelect->setItemValue(CONTEXT_STATE_CI_AP_PENDANT,$moContext->getContextStateAsString(CONTEXT_STATE_CI_AP_PENDANT));
    $moSelect->setItemValue(CONTEXT_STATE_CI_WAITING_CONCLUSION,$moContext->getContextStateAsString(CONTEXT_STATE_CI_WAITING_CONCLUSION));
    $moSelect->setItemValue(CONTEXT_STATE_CI_FINISHED,$moContext->getContextStateAsString(CONTEXT_STATE_CI_FINISHED));
    $moSelect->setItemValue(CONTEXT_STATE_CI_CLOSED,$moContext->getContextStateAsString(CONTEXT_STATE_CI_CLOSED));
  }
}

// popula a grid com os relat�rios de acordo com a op��o do filtro de relat�rios
class ShowReportsEvent extends FWDRunnable {
  public function run(){
    echo "FilterManager.showFilters('no_filter_available');";
  
    $moGrid = FWDWebLib::getObject('grid_reports');
    $moGrid->setObjFwdDrawGrid(new GridReports());
    $msReportCategory = FWDWebLib::getObject("report_category")->getValue();
    $maReportCategory = getReportCategoryArray();
    $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
  
    if ($msReportCategory != 'all_reports') {
      $maReports = $maReportCategory[$msReportCategory];
      $miLineCount = 0;
      foreach ($maReports as $msReportKey => $maReportACL) {
        if(!in_array($maReportACL[1] , $maACLs)){
          $miLineCount++;
          $moGrid->setItem(1, $miLineCount, $msReportKey);
          $moGrid->setItem(2, $miLineCount, $maReportACL[0]);
        }
      }
    }else{
      $miLineCount = 0;
      foreach ($maReportCategory as $maReports) {
        foreach ($maReports as $msReportKey => $maReportACL) {
          if(!in_array($maReportACL[1] , $maACLs)){
            $miLineCount++;
            $moGrid->setItem(1, $miLineCount, $msReportKey);
            $moGrid->setItem(2, $miLineCount, $maReportACL[0]);
          }
        }
      }
    }
    $moGrid->execEventPopulate();
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $moGrid = $moWebLib->getObject('grid_reports');
    $moGrid->setObjFwdDrawGrid(new GridReports());

    /*
     * Popula o select com os tipos de formato de relat�rios e
     * o select com os tipos de classifica��o de relat�rios.
     */
    $moFormatSelect = FWDWebLib::getObject('select_report_type');
    $maAvailableFormats = FWDReportGenerator::getAvailableFormats(ISMSLib::getSupportedReportFormats());
    foreach ($maAvailableFormats as $miKey => $msValue) {
      $moFormatSelect->setItemValue($miKey, $msValue);
    }
    $moHandler = new QueryGetReportClassification(FWDWebLib::getConnection());
    $moHandler->makeQuery();
    $maAvailableClassifications = $moHandler->executeQuery();
    $moClassificationSelect = FWDWebLib::getObject('select_report_classification');
    foreach ($maAvailableClassifications as $miKey => $msValue) {
      $moClassificationSelect->setItemValue($msValue, $msValue);
    }
    
	$moSelectNonConformityType = new QuerySelectNonConformityType();
	$moSelect = FWDWebLib::getObject('select_nc_classification');
	$moSelect->setQueryHandler($moSelectNonConformityType);
	$moSelect->populate(); 

    //instala a seguran�a de ACL na p�gina
    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());

    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
     ?>
      <script language="javascript">
        gobi('grid_reports').setPopulate(true);
        trigger_event('show_reports',3);
        
        FilterManager = {
          'selectedReport': '',
          'visibleFilters': ['no_filter_available'],
          'showFilter': function(psFilterId){
            this.visibleFilters.push(psFilterId);
            js_show(psFilterId);
          },
          'showFilters': function(psReportId){
              
            for(var i=0;i<this.visibleFilters.length;i++){
              js_hide(this.visibleFilters[i]);
            }

            this.visibleFilters = [];
            if(psReportId==this.selectedReport){
              this.showFilter('no_filter_available');
              this.selectedReport = '';
            }else{
              this.selectedReport = psReportId;
              switch(psReportId){              	            
                case 'report_occurrence_by_incident':{
                  this.showFilter('filter_incident');
                  this.showFilter('filter_incident_order_by');
                  break;
                }
                case 'report_user_by_incident':{
                  this.showFilter('filter_incident');
                  break;
                }
                case 'report_incident_by_responsible':{
                  this.showFilter('filter_incident');
                  break;
                }
                case 'report_occurrence_accompaniment':{
                  this.showFilter('filter_occurrence');
                  break;
                }
                case 'report_incident_accompaniment':{
                  this.showFilter('filter_incident');
                  this.showFilter('filter_incident_dates');
                  break;
                }
                case 'report_nonconformity_by_process':{
                  this.showFilter('filter_nc');
                  break;
                }
                case 'report_control_nonconformity_by_process':{
                  this.showFilter('filter_nc');
                  break;
                }
                case 'report_nonconformity_accompaniment':{
                  this.showFilter('filter_nc');
                  this.showFilter('filter_nonconformity_dates');
                  break;
                }
                case 'report_financial_impact_by_incident':{
                  this.showFilter('filter_incident');
                  break;
                }
                case 'report_elements_affected_by_incident':{
                  this.showFilter('filter_elements');
                  break;
                }
                case 'report_pendant_tasks_ci':{
                  this.showFilter('report_pendant_tasks_filter');
                  break;
                }

                case 'report_action_plan_by_user':{
                	this.showFilter('filter_action_plan');
					break;
                }

                case 'report_action_plan_by_process':{
                	this.showFilter('filter_action_plan');
					break;
                }

                case 'report_ap_without_doc':{
                	this.showFilter('filter_action_plan');
					break;
                }
                                                
                default: this.showFilter('no_filter_available');
              }
            }
          },
          'hideWarnings': function(){
            js_hide('warning');
            js_hide('elements_filter_warning');
          }
        };
        
        gobi('grid_reports').onRequiredCheckNotOk = function(){
          gobi('warning').show();
        }
        
        gobi('filter_elements_controller').onRequiredCheckNotOk = function(){
          gobi('elements_filter_warning').show();
        }
        
      </script>
    <?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_report.xml");
?>