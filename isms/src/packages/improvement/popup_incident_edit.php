<?php
include_once "include.php";
include_once $handlers_ref . "grid/improvement/QueryGridOccurrence.php";
include_once $handlers_ref . "select/improvement/QuerySelectCategory.php";
include_once $handlers_ref . "select/improvement/QuerySelectOccurrence.php";

class UpdateCurrentOccurrencesEvent extends FWDRunnable {
  public function run() {
    $moHandler = new QueryGridOccurrence(FWDWebLib::getConnection());
    $maIds = array_filter(explode(":",FWDWebLib::getObject('occurrences_ids')->getValue()));
    if(count($maIds)){
      $moHandler->showOccurrences($maIds);
      $maCurrentOccurrences = $moHandler->getSelectOccurrences();
      echo ISMSLib::getJavascriptPopulateSelect('occurrences', serialize($maCurrentOccurrences));
    }
  }
}

class OpenaffectedProductServicePopupEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $msPopUpId = 'popup_incident_affected_product_service';
    $msXmlFile = $moWebLib->getSysRef().'packages/improvement/popup_incident_affected_product_service.xml';
    $moWebLib->xml_load($msXmlFile,false);

    $moWebLib->getObject('popup_incident_affected_product_service_memo')->setValue(str_replace('<br/>','\n',FWDWebLib::convertToISO(FWDWebLib::getObject('pog_incident_affected_product_service')->getValue(), true)));

    $moDialog = $moWebLib->getObject('popup_incident_affected_product_service_dialog');

    // Evento do close
    $moEvent = new FWDClientEvent();
    $moEvent->setAttrEvent('onClick');
    $moEvent->setAttrValue("soPopUpManager.closePopUp('popup_incident_affected_product_service');");
    $moWebLib->getObject('close')->addObjFWDEvent($moEvent);

    // Evento do save
    $psConfirmEvent = "soPopUp = soPopUpManager.getPopUpById('popup_incident_affected_product_service'); soWindow = soPopUp.getOpener(); soWindow.setInnerHTML('incident_affected_product_service',gebi('popup_incident_affected_product_service_memo').value);";
    $moEvent = new FWDClientEvent();
    $moEvent->setAttrEvent('onClick');
    $moEvent->setAttrValue($psConfirmEvent."soPopUpManager.closePopUp('popup_incident_affected_product_service');");
    $moWebLib->getObject('save')->addObjFWDEvent($moEvent);

    $msXml = str_replace(array("\n","\r",'"'),array(' ','','\"'),$moDialog->draw());

    echo "isms_open_popup('popup_incident_affected_product_service','','','true',412,440);"
        ."soPopUpManager.getPopUpById('popup_incident_affected_product_service').setHtmlContent(\"$msXml\");";
  }
}

class OpenEvidencePopupEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $msPopUpId = 'popup_incident_evidence';
    $msXmlFile = $moWebLib->getSysRef().'packages/improvement/popup_incident_evidence.xml';
    $moWebLib->xml_load($msXmlFile,false);

    $moWebLib->getObject('popup_incident_evidence_memo')->setValue(str_replace('<br/>','\n',FWDWebLib::convertToISO(FWDWebLib::getObject('pog_incident_evidences')->getValue(), true)));

    $moDialog = $moWebLib->getObject('popup_incident_evidence_dialog');

    // Evento do close
    $moEvent = new FWDClientEvent();
    $moEvent->setAttrEvent('onClick');
    $moEvent->setAttrValue("soPopUpManager.closePopUp('popup_incident_evidence');");
    $moWebLib->getObject('close')->addObjFWDEvent($moEvent);

    // Evento do save
    $psConfirmEvent = "soPopUp = soPopUpManager.getPopUpById('popup_incident_evidence'); soWindow = soPopUp.getOpener(); soWindow.setInnerHTML('incident_evidences',gebi('popup_incident_evidence_memo').value);";
    $moEvent = new FWDClientEvent();
    $moEvent->setAttrEvent('onClick');
    $moEvent->setAttrValue($psConfirmEvent."soPopUpManager.closePopUp('popup_incident_evidence');");
    $moWebLib->getObject('save')->addObjFWDEvent($moEvent);

    $msXml = "<div style='top:26;position:absolute;'>";    
    $msXml .= str_replace(array("\n","\r",'"'),array(' ','','\"'),$moDialog->draw());
    $msXml .= "</div>";
   
    echo "isms_open_popup('popup_incident_evidence','','','true',412,440);"
        ."soPopUpManager.getPopUpById('popup_incident_evidence').setHtmlContent(\"$msXml\");";
  }
}
//incident_affected_product_service
class OpenIncidentCountPlanEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $msPopUpId = 'popup_incident_accounts_plan';
    $msXmlFile = $moWebLib->getSysRef().'packages/improvement/popup_incident_accounts_plan.xml';
    $moWebLib->xml_load($msXmlFile,false);

    $moWebLib->getObject('popup_incident_accounts_plan_memo')->setValue(str_replace('<br/>','\n',FWDWebLib::convertToISO(FWDWebLib::getObject('pog_incident_accounts_plan')->getValue(), true)));

    $moDialog = $moWebLib->getObject('popup_incident_accounts_plan_dialog');

    // Evento do close
    $moEvent = new FWDClientEvent();
    $moEvent->setAttrEvent('onClick');
    $moEvent->setAttrValue("soPopUpManager.closePopUp('popup_incident_accounts_plan');");
    $moWebLib->getObject('close')->addObjFWDEvent($moEvent);

    // Evento do save
    $psConfirmEvent = "soPopUp = soPopUpManager.getPopUpById('popup_incident_accounts_plan'); soWindow = soPopUp.getOpener(); soWindow.setInnerHTML('incident_accounts_plan',gebi('popup_incident_accounts_plan_memo').value);";
    $moEvent = new FWDClientEvent();
    $moEvent->setAttrEvent('onClick');
    $moEvent->setAttrValue($psConfirmEvent."soPopUpManager.closePopUp('popup_incident_accounts_plan');");
    $moWebLib->getObject('save')->addObjFWDEvent($moEvent);

    $msXml = str_replace(array("\n","\r",'"'),array(' ','','\"'),$moDialog->draw());

    echo "isms_open_popup('popup_incident_accounts_plan','','','true',412,440);"
        ."soPopUpManager.getPopUpById('popup_incident_accounts_plan').setHtmlContent(\"$msXml\");";
  }
}


class OpenDisposalPopupEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $msPopUpId = 'popup_incident_disposal';
    $msXmlFile = $moWebLib->getSysRef().'packages/improvement/popup_incident_disposal.xml';
    $moWebLib->xml_load($msXmlFile,false);

    $moWebLib->getObject('popup_incident_disposal_memo')->setValue(str_replace('<br/>','\n',FWDWebLib::convertToISO(FWDWebLib::getObject('pog_incident_disposal')->getValue(), true)));

    $moDialog = $moWebLib->getObject('popup_incident_disposal_dialog');

    // Evento do close
    $moEvent = new FWDClientEvent();
    $moEvent->setAttrEvent('onClick');
    $moEvent->setAttrValue("soPopUpManager.closePopUp('popup_incident_disposal');");
    $moWebLib->getObject('close')->addObjFWDEvent($moEvent);

    // Evento do save
    $psConfirmEvent = "soPopUp = soPopUpManager.getPopUpById('popup_incident_disposal'); soWindow = soPopUp.getOpener(); soWindow.setInnerHTML('incident_disposal',gebi('popup_incident_disposal_memo').value);";
    $moEvent = new FWDClientEvent();
    $moEvent->setAttrEvent('onClick');
    $moEvent->setAttrValue($psConfirmEvent."soPopUpManager.closePopUp('popup_incident_disposal');");
    $moWebLib->getObject('save')->addObjFWDEvent($moEvent);

    $msXml = "<div style='top:26;position:absolute;'>";
    $msXml .= str_replace(array("\n","\r",'"'),array(' ','','\"'),$moDialog->draw());
    $msXml .= "</div>";
    
    echo "isms_open_popup('popup_incident_disposal','','','true',412,440);"
        ."soPopUpManager.getPopUpById('popup_incident_disposal').setHtmlContent(\"$msXml\");";
  }
}

class OpenSolutionPopupEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $msPopUpId = 'popup_incident_solution';
    $msXmlFile = $moWebLib->getSysRef().'packages/improvement/popup_incident_solution.xml';
    $moWebLib->xml_load($msXmlFile,false);

    $moWebLib->getObject('popup_incident_solution_memo')->setValue(str_replace('<br/>','\n',FWDWebLib::convertToISO(FWDWebLib::getObject('pog_incident_solution')->getValue(), true)));

    $moDialog = $moWebLib->getObject('popup_incident_solution_dialog');

    // Evento do close
    $moEvent = new FWDClientEvent();
    $moEvent->setAttrEvent('onClick');
    $moEvent->setAttrValue("soPopUpManager.closePopUp('popup_incident_solution');");
    $moWebLib->getObject('close')->addObjFWDEvent($moEvent);

    // Evento do save
    $psConfirmEvent = "soPopUp = soPopUpManager.getPopUpById('popup_incident_solution'); soWindow = soPopUp.getOpener(); soWindow.setInnerHTML('incident_solution',gebi('popup_incident_solution_memo').value);";
    $moEvent = new FWDClientEvent();
    $moEvent->setAttrEvent('onClick');
    $moEvent->setAttrValue($psConfirmEvent."soPopUpManager.closePopUp('popup_incident_solution');");
    $moWebLib->getObject('save')->addObjFWDEvent($moEvent);

    $msXml = "<div style='top:26;position:absolute;'>";
    $msXml .= str_replace(array("\n","\r",'"'),array(' ','','\"'),$moDialog->draw());
    $msXml .= "</div>";
    
    echo "isms_open_popup('popup_incident_solution','','','true',412,440);"
        ."soPopUpManager.getPopUpById('popup_incident_solution').setHtmlContent(\"$msXml\");";
  }
}

class SaveEvent extends FWDRunnable {
  public function run() {
    $miIncidentId = FWDWebLib::getObject('incident_id')->getValue();
    $miUserId = ISMSLib::getCurrentUserId();
    $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();

    $moWebLib = FWDWebLib::getInstance();
    $miDateTest = ISMSLib::ISMSTime();
    $miDate = mktime (0, 0, 0, date("m",$miDateTest), date("d",$miDateTest),  date("Y",$miDateTest));
    $miDateFinish1 = FWDWebLib::getObject('incident_date_limit')->getTimestamp();
    $miDateFinish2 = FWDWebLib::getObject('incident_date_finish')->getTimestamp();
    
    $moDate = FWDWebLib::getObject('date');
    $maTime = explode(":",FWDWebLib::getObject('hour')->getValue());
    $msDateTimestamp = mktime(isset($maTime[0])?$maTime[0]:0, isset($maTime[1])?$maTime[1]:0, 0, $moDate->getMonth(), $moDate->getDay(), $moDate->getYear());    
    
    $mbShowPrevFinishWarning = false;
    
    if((FWDWebLib::getObject('incident_date_limit')->getValue())&&($miDateFinish1 < $miDate)){
      $mbShowPrevFinishWarning = true;
    }elseif((FWDWebLib::getObject('incident_date_finish')->getValue())&&($miDateFinish2 < $miDate)){
      $mbShowPrevFinishWarning = true;
    }elseif($msDateTimestamp > ISMSLib::ISMSTime()){
      echo "gobi('date_warning').show();";
      exit();
    }
    
    if($mbShowPrevFinishWarning){
      echo "gobi('wn_prev_finish').show();";
      exit();
    }

    $mbHasPermission = true;

    if($miIncidentId){
      $moIncident = new CIIncident();
      $moIncident->fetchById($miIncidentId);
      $miApproverId = $moIncident->getApprover();
      $miState = $moIncident->getContextState();

      $mbACLPermisssion = false;
      if($miIncidentId){
        $mbACLPermission = (in_array('M.CI.2.3',$maACLs))?false:true;
      }else{
        $mbACLPermission = (in_array('M.CI.2.2',$maACLs))?false:true;
      }

      if(($miUserId!=$miApproverId) && (!$mbACLPermission)){
        $moIncident = new CIIncident();
        $mbHasPermission = false;
        if($miState==CONTEXT_STATE_INC_DIRECTED){
          $moIncident->setFieldValue("incident_date_limit",FWDWebLib::getObject('incident_date_limit')->getTimestamp());
          $moIncident->setFieldValue("incident_disposal_description",FWDWebLib::getObject('pog_incident_disposal')->getValue());
        }elseif($miState==CONTEXT_STATE_INC_WAITING_SOLUTION){
          $moIncident->setFieldValue("incident_date_finish",FWDWebLib::getObject('incident_date_finish')->getTimestamp());
          $moIncident->setFieldValue("incident_solution_description",FWDWebLib::getObject('pog_incident_solution')->getValue());
        }
      }
    }

		$msEvController = FWDWebLib::getObject('evidence_controller')->getValue();
		if(substr($msEvController,0,1)==':') $miEvController = substr($msEvController,1);
		else $miEvController = $msEvController;

    if($mbHasPermission){
      $moIncident = new CIIncident();

      $miCategoryId = FWDWebLib::getObject('incident_category')->getValue();
      if (!$miCategoryId) $miCategoryId = 'null';
      $moIncident->setFieldValue("incident_category_id",$miCategoryId);
      $moIncident->setFieldValue("incident_responsible_id",FWDWebLib::getObject('responsible_id')->getValue());
      $moIncident->setFieldValue("incident_date",$msDateTimestamp);

      $moIncident->fillFieldsFromForm();

      $moIncident->setFieldValue("incident_disposal_description",FWDWebLib::getObject('pog_incident_disposal')->getValue());
      $moIncident->setFieldValue("incident_solution_description",FWDWebLib::getObject('pog_incident_solution')->getValue());

      $moIncident->setFieldValue('incident_product_service',FWDWebLib::getObject('pog_incident_affected_product_service')->getValue());
      $moIncident->setFieldValue('incident_evidences',FWDWebLib::getObject('pog_incident_evidences')->getValue());
      $moIncident->setFieldValue('incident_accounts_plan',FWDWebLib::getObject('pog_incident_accounts_plan')->getValue());
      
      if ($miEvController) $moIncident->setFieldValue('incident_evidence_req_comment', FWDWebLib::getObject('incident_evidence_comment')->getValue());
    }

    $miIncidentId = $moIncident->insertOrUpdate($miIncidentId);
    echo "gebi('incident_id').value=$miIncidentId;";	
		
    if($mbHasPermission && $miEvController){
      $msComment = FWDWebLib::getObject('incident_evidence_comment')->getValue();
      if($msComment){
        $msCurrentHash = FWDWebLib::getObject('incident_evidence_req_comment_hash')->getValue();
        $msNewHash = md5($msComment);
        if($msNewHash!=$msCurrentHash){
          // Coment�rio modificado -> envia alerta
          $moAlert = new WKFAlert();
          $moAlert->setFieldValue('alert_type', WKF_ALERT_INC_EVIDENCE_REQUIRED);
          $moAlert->setFieldValue('alert_context_id', $miIncidentId);
          $moAlert->setFieldValue('alert_creator_id', $miUserId);
          $moAlert->setFieldValue('alert_receiver_id', ISMSLib::getConfigById(USER_EVIDENCE_MANAGER));
          $moAlert->setFieldValue('alert_justification', $msComment);
          $moAlert->setFieldValue('alert_date', ISMSLib::ISMSTime());
          $moAlert->insert();
        }
      }
    }

    $maInitialOccurrences = array_filter(explode(':',FWDWebLib::getObject('initial_occurrences_ids')->getValue()));
    $maOccurrences = array_filter(explode(':',FWDWebLib::getObject('occurrences_ids')->getValue()));
    $maAssociateOccurrences = array_diff($maOccurrences,$maInitialOccurrences);
    $maDisassociateOccurrences = array_diff($maInitialOccurrences,$maOccurrences);
    foreach($maAssociateOccurrences as $miOccurrenceId) {
      $moOccurrence = new CIOccurrence();
      $moOccurrence->setFieldValue('incident_id',$miIncidentId);
      $moOccurrence->update($miOccurrenceId);
    }
    foreach($maDisassociateOccurrences as $miOccurrenceId) {
      $moOccurrence = new CIOccurrence();
      $moOccurrence->setFieldValue('incident_id',"NULL");
      $moOccurrence->update($miOccurrenceId);
    }

    $msFinancialImpactIds = FWDWebLib::getObject('financial_impact_ids')->getValue();
    $msFinancialImpactValues = FWDWebLib::getObject('financial_impact_values')->getValue();
    if($msFinancialImpactIds){
      $maFinancialImpactIds = explode(':',$msFinancialImpactIds);
      $maFinancialImpactValues = explode(':',$msFinancialImpactValues);
      for($i=0;$i<count($maFinancialImpactValues);$i++){
        $maFinancialImpactValues[$i] = str_replace(',','',$maFinancialImpactValues[$i]);
      }

      $maParameters = array();

      if($miIncidentId){
        $moIncidentFinancialImpact = new CIIncidentFinancialImpact();
        $moIncidentFinancialImpact->createFilter($miIncidentId,'incident_id');
        $moIncidentFinancialImpact->select();
        while($moIncidentFinancialImpact->fetch()){
          $miParameterId = $moIncidentFinancialImpact->getFieldByAlias('classification_id')->getValue();
          $mfParameterValue = $moIncidentFinancialImpact->getFieldByAlias('financial_impact_value')->getValue();
          $maParameters[$miParameterId] = $mfParameterValue;
        }
      }

      // Salva os par�metros de impacto financeiro
      for($i=0;$i<count($maFinancialImpactIds);$i++){
        $miParameterId = $maFinancialImpactIds[$i];
        $mfParameterValue = $maFinancialImpactValues[$i];
        if(!isset($maParameters[$miParameterId])){
          // insert
          $moIncidentFinancialImpact = new CIIncidentFinancialImpact();
          $moIncidentFinancialImpact->setFieldValue('incident_id',$miIncidentId);
          $moIncidentFinancialImpact->setFieldValue('classification_id',$miParameterId);
          $moIncidentFinancialImpact->setFieldValue('financial_impact_value',$mfParameterValue);
          $moIncidentFinancialImpact->insert();
        }elseif($maParameters[$miParameterId]!=$mfParameterValue){
          // update
          $moIncidentFinancialImpact = new CIIncidentFinancialImpact();
          $moIncidentFinancialImpact->createFilter($miIncidentId,'incident_id');
          $moIncidentFinancialImpact->createFilter($miParameterId,'classification_id');
          $moIncidentFinancialImpact->setFieldValue('financial_impact_value',$mfParameterValue);
          $moIncidentFinancialImpact->update();
        }
      }

    }

    /** MUDAN�A DE ESTADO DE CONTEXTO **/
    $moIncident = new CIIncident();

    echo "refresh_grid();";
    if (!$moIncident->getStateForwardConfirmation($miIncidentId)) {
      echo "close_popup();";
    }
  }
}

class ConfirmStateChangeEvent extends FWDRunnable {
  protected $ciAction;
  public function run(){
    $miIncidentId = FWDWebLib::getObject('incident_id')->getValue();
    $moIncident = new CIIncident();
    $moIncident->fetchById($miIncidentId);

    $moIncident->stateForward($this->ciAction);
    echo "soWindow = soPopUpManager.getPopUpById('popup_incident_edit').getOpener();"
        ."if(soWindow.refresh_grid) soWindow.refresh_grid();"
        ."soPopUpManager.closePopUp('popup_incident_edit');";
  }
}

class ConfirmSendToResponsibleEvent extends ConfirmStateChangeEvent {
  protected $ciAction = WKF_ACTION_INC_SEND_TO_RESPONSIBLE;
}

class ConfirmSendToAppDisposalEvent extends ConfirmStateChangeEvent {
  protected $ciAction = WKF_ACTION_INC_SEND_TO_APP_DISPOSAL;
}

class ConfirmSendToAppSolutionEvent extends ConfirmStateChangeEvent {
  protected $ciAction = WKF_ACTION_INC_SEND_TO_APP_SOLUTION;
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new UpdateCurrentOccurrencesEvent("update_current_occurrences"));
    $moStartEvent->addAjaxEvent(new OpenSolutionPopupEvent("open_solution_popup_event"));
    $moStartEvent->addAjaxEvent(new OpenDisposalPopupEvent("open_disposal_popup_event"));
    $moStartEvent->addAjaxEvent(new SaveEvent("save_event"));
    $moStartEvent->addAjaxEvent(new ConfirmSendToResponsibleEvent("confirm_send_to_responsible"));
    $moStartEvent->addAjaxEvent(new ConfirmSendToAppDisposalEvent("confirm_send_to_app_disposal"));
    $moStartEvent->addAjaxEvent(new ConfirmSendToAppSolutionEvent("confirm_send_to_app_solution"));

    $moStartEvent->addAjaxEvent(new OpenaffectedProductServicePopupEvent("open_affected_product_service_popup_event"));
    $moStartEvent->addAjaxEvent(new OpenEvidencePopupEvent("open_evidence_popup_event"));
    $moStartEvent->addAjaxEvent(new OpenIncidentCountPlanEvent("open_incident_accounts_plan_popup_event"));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    /*
     * Popula o select com as categorias
     */
    $moHandler = new QuerySelectCategory(FWDWebLib::getConnection());
    $moSelect = FWDWebLib::getObject('incident_category');
    $moSelect->setQueryHandler($moHandler);
    $moSelect->populate();

    $miIncidentId = FWDWebLib::getObject('param_incident_id')->getValue();
    FWDWebLib::getObject('incident_id')->setValue($miIncidentId);
    $moIncident = new CIIncident();

    if ($moIncident->fetchById($miIncidentId)) {
      $mbIncidentExist = true;
      $moCtxUserTest = new CIIncident();
      $moCtxUserTest->testPermissionToEdit($miIncidentId);

      $miResponsibleId = $moIncident->getFieldValue('incident_responsible_id');
      $moUser = new ISMSUser();
      $moUser->fetchById($miResponsibleId);
      $msUserName = $moUser->getFieldValue('user_name');

      $moIncident->fillFormFromFields();

	  $moIncidentDate = ISMSLib::getISMSDate($moIncident->getFieldValue('incident_date'));
	  if($moIncidentDate){
	  	$msDateTime = explode(' ', $moIncidentDate);	  
      	FWDWebLib::getObject('date')->setValue($msDateTime[0]);
      	FWDWebLib::getObject('hour')->setValue($msDateTime[1]);
	  }
	  
	  FWDWebLib::getObject('incident_evidence_comment')->setValue($moIncident->getFieldValue('incident_evidence_req_comment'));
      FWDWebLib::getObject('incident_evidence_req_comment_hash')->setValue(md5($moIncident->getFieldValue('incident_evidence_req_comment')));

      FWDWebLib::getObject('responsible_id')->setValue($miResponsibleId);
      FWDWebLib::getObject('incident_responsible')->setValue($msUserName);
      FWDWebLib::getObject('incident_category')->setValue($moIncident->getFieldValue('incident_category_id'));
      $msEscapedDisposal = str_replace("\n",'<br/>',$moIncident->getFieldValue('incident_disposal_description'));
      FWDWebLib::getObject('incident_disposal')->setValue($msEscapedDisposal);
      FWDWebLib::getObject('pog_incident_disposal')->setValue($msEscapedDisposal);
      $msEscapedSolution = str_replace("\n",'<br/>',$moIncident->getFieldValue('incident_solution_description'));
      FWDWebLib::getObject('incident_solution')->setValue($msEscapedSolution);
      FWDWebLib::getObject('pog_incident_solution')->setValue($msEscapedSolution);

      $msEscapedProductService =  str_replace("\n",'<br/>',$moIncident->getFieldValue('incident_product_service'));
      FWDWebLib::getObject('incident_affected_product_service')->setValue($msEscapedProductService);
      FWDWebLib::getObject('pog_incident_affected_product_service')->setValue($msEscapedProductService);


      $msEscapedEvidence =  str_replace("\n",'<br/>',$moIncident->getFieldValue('incident_evidences'));
      FWDWebLib::getObject('incident_evidences')->setValue($msEscapedEvidence);
      FWDWebLib::getObject('pog_incident_evidences')->setValue($msEscapedEvidence);

      $msEscapedCountPlan =  str_replace("\n",'<br/>',$moIncident->getFieldValue('incident_accounts_plan'));
      FWDWebLib::getObject('incident_accounts_plan')->setValue($msEscapedCountPlan);
      FWDWebLib::getObject('pog_incident_accounts_plan')->setValue($msEscapedCountPlan);


      $moSelect = FWDWebLib::getObject('occurrences');
      $moHandler = new QuerySelectOccurrence(FWDWebLib::getConnection());
      $moHandler->setIncidentId($miIncidentId);
      $moHandler->makeQuery();
      $moHandler->executeQuery();
      $maValues = $moHandler->getValues();
      $msValues = '';
      foreach($maValues as $maOption) {
        $moItem = new FWDItem();
        $moItem->setAttrKey($maOption[0]);
        $moItem->setValue($maOption[1]);
        $moSelect->addObjFWDItem($moItem);
        unset($moItem);
        $msValues.=":$maOption[0]";
      }
      FWDWebLib::getObject('initial_occurrences_ids')->setValue($msValues);
      FWDWebLib::getObject('occurrences_ids')->setValue($msValues);
    } else {
      $mbIncidentExist = false;
      $moCtxUserTest = new CIIncident();
      $moCtxUserTest->testPermissionToInsert();
      
      // popula data e hora automaticamente
      FWDWebLib::getObject('date')->setTimestamp(ISMSLib::ISMSTime());
      FWDWebLib::getObject('hour')->setValue(date("H", ISMSLib::ISMSTime()) . ":" . date("i", ISMSLib::ISMSTime()));
    }

    $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
    $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    $miApproverId = $moIncident->getApprover();
    $mbACLPermisssion = false;
    if($miIncidentId){
      $mbACLPermission = (in_array('M.CI.2.3',$maACLs))?false:true;
    }else{
      $mbACLPermission = (in_array('M.CI.2.2',$maACLs))?false:true;
    }

    if(($miUserId!=$miApproverId) && (!$mbACLPermission)){
      FWDWebLib::getObject('add_occurrences')->setShouldDraw(false);
      FWDWebLib::getObject('remove_occurrence')->setShouldDraw(false);
      FWDWebLib::getObject('search_responsible')->setShouldDraw(false);

      FWDWebLib::getObject('incident_name')->setAttrDisabled('true');
      FWDWebLib::getObject('incident_category')->setAttrDisabled('true');

      FWDWebLib::getObject('vb_incident_accounts_plan')->setShouldDraw(false);
      FWDWebLib::getObject('vb_incident_evidences')->setShouldDraw(false);
      FWDWebLib::getObject('vb_incident_affected_product_service')->setShouldDraw(false);

      FWDWebLib::getObject('incident_loss_type')->setAttrDisabled('true');

      FWDWebLib::getObject('readonly_incident_date_limit')->setShouldDraw(false);
      FWDWebLib::getObject('readonly_incident_date_finish')->setShouldDraw(false);

      if($moIncident->getContextState()==CONTEXT_STATE_INC_DIRECTED){
        FWDWebLib::getObject('readonly_incident_date_limit')->setShouldDraw(false);
        FWDWebLib::getObject('incident_date_finish')->setShouldDraw(false);
        FWDWebLib::getObject('edit_solution')->setShouldDraw(false);
        FWDWebLib::getObject('readonly_incident_date_finish')->setValue(FWDWebLib::getObject('incident_date_finish')->getValue());
        FWDWebLib::getObject('readonly_incident_date_finish')->setShouldDraw(true);


      }elseif($moIncident->getContextState()==CONTEXT_STATE_INC_WAITING_SOLUTION){
        FWDWebLib::getObject('readonly_incident_date_finish')->setShouldDraw(false);
        FWDWebLib::getObject('incident_date_limit')->setShouldDraw(false);
        FWDWebLib::getObject('edit_disposal')->setShouldDraw(false);
        FWDWebLib::getObject('readonly_incident_date_limit')->setValue(FWDWebLib::getObject('incident_date_limit')->getValue());
        FWDWebLib::getObject('readonly_incident_date_limit')->setShouldDraw(true);

        FWDWebLib::getObject('required')->setAttrElements(str_replace(':incident_date_limit','',FWDWebLib::getObject('required')->getAttrElements()));
      }
      FWDWebLib::getObject('financial_impact')->setShouldDraw(false);
    }else{
      FWDWebLib::getObject('readonly_incident_date_limit')->setShouldDraw(false);
      FWDWebLib::getObject('readonly_incident_date_finish')->setShouldDraw(false);

      $moContextClassification = new ISMSContextClassification();
      $moContextClassification->createFilter(CONTEXT_CLASSIFICATION_FINANCIAL_IMPACT,'classif_type');
      $moContextClassification->select();

      $miParameterCount = 0;
      while($moContextClassification->fetch()) $miParameterCount++;

      $miPopUpHeight = $miParameterCount * 30 + 110;
      FWDWebLib::getObject('popup_financial_impact_edit_height')->setValue($miPopUpHeight);
    }

    //desabilita o botao de custo se as configura��es de custo do sistema estiverem desabilitadas
    $moConfig = new ISMSConfig();
    if(!$moConfig->getConfig(GENERAL_COST_ENABLED)){
      FWDWebLib::getObject('financial_impact')->setShouldDraw(false);
    }

    //instala a seguran�a de ACL na p�gina
    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        gebi('incident_name').focus();

        function setInnerHTML(psElement,psValue){
          psValue = psValue.replace(/\n/g,'<br/>');
          gobi(psElement).setValue(psValue);
          gebi('pog_'+psElement).value = psValue;
        }

        function set_occurrences(psCurrentIds) {
          gebi('occurrences_ids').value = psCurrentIds;
          psCurrentIds=null;
          trigger_event('update_current_occurrences',3);
        }

        function set_user(piId, psName) {
          gebi('responsible_id').value = piId;
          gobi('incident_responsible').setValue(psName);
        }

        function setFinancialImpact(psParametersIds,psValues){
          gebi('financial_impact_ids').value = psParametersIds;
          gebi('financial_impact_values').value = psValues;
        }

        function refresh_grid() {
          soWindow = soPopUpManager.getPopUpById('popup_incident_edit').getOpener();
          if (soWindow.refresh_grid) soWindow.refresh_grid();
          soWindow=null;
        }

        function close_popup() {
          soPopUpManager.closePopUp('popup_incident_edit');
        }

        function setEvidenceRequirement(psComment){
          gebi('incident_evidence_comment').value = psComment;
        }

      </script>
    <?
  }
}
//testa se a licen�a utilizada permite acessar o m�dulo de incidente
ISMSLib::testUserPermissionToIncidentMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_incident_edit.xml");

?>