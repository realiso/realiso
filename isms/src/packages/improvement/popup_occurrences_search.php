<?php
include_once "include.php";
include_once $handlers_ref . "grid/improvement/QueryGridOccurrence.php";

function getCurrentIds(){
  $msValues = FWDWebLib::getObject('var_current_ids')->getValue();
  $maIds = array_filter(explode(':', $msValues));
  if (!count($maIds)) $maIds=array(0);
  return $maIds;
}

class DrawGridSearch extends FWDDrawGrid {
  public function drawItem(){
    switch($this->ciColumnIndex){
      case 1:
        $this->coCellBox->setIconSrc("icon-ci_occurrence.gif");
        return $this->coCellBox->draw();
      default:
        return parent::drawItem();
    }
  }
}

class SearchEvent extends FWDRunnable {
  public function run(){
    $moGrid = FWDWebLib::getObject('grid_search');
    $moGrid->execEventPopulate();
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new SearchEvent('search_event'));

    $moGrid = FWDWebLib::getObject('grid_search');
    $moHandler = new QueryGridOccurrence(FWDWebLib::getConnection());
    $moHandler->setDescription(FWDWebLib::getObject('var_description')->getValue());
    $moHandler->setExcluded(getCurrentIds());
    $moHandler->showFreeOnly();
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new DrawGridSearch());

    $moGrid = FWDWebLib::getObject('grid_current');
    $moHandler = new QueryGridOccurrence(FWDWebLib::getConnection());
    /*$miIncidentId = FWDWebLib::getObject('incident_id')->getValue();
    if ($miIncidentId)
      $moHandler->setIncident($miIncidentId);*/
    $moHandler->showOccurrences(getCurrentIds());
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new DrawGridSearch());
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    FWDWebLib::getObject('var_current_ids')->setValue(FWDWebLib::getObject('current_ids')->getValue());

    $moGrid = FWDWebLib::getObject('grid_current');
    $moHandler = new QueryGridOccurrence(FWDWebLib::getConnection());
    $moHandler->showOccurrences(getCurrentIds());
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new DrawGridSearch());

    //instala a seguran�a de ACL na p�gina
    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        gebi('description').focus();
      
        function refresh_grid(){
          js_refresh_grid('grid_search');
          js_refresh_grid('grid_current');
        }
        function enter_search_event(e){
          if(!e) e = window.event;
          if(e['keyCode']==13){
            gebi('var_description').value = gebi('description').value;
            trigger_event("search_event","3");
          }
        }
        FWDEventManager.addEvent(gebi('description'), 'keydown', enter_search_event);
        refresh_grid();
      </script>
    <?
  }
}

//testa se a licen�a utilizada permite acessar o m�dulo de incidente
ISMSLib::testUserPermissionToIncidentMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_occurrences_search.xml");

?>