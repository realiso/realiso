<?php
include_once "include.php";
include_once $handlers_ref . "grid/improvement/QueryGridDisciplinaryProcess.php";

class DisciplinaryProcessDrawGrid extends FWDDrawGrid {
  public function drawItem(){    
    switch($this->ciColumnIndex){
      case $this->getIndexByAlias('incident_id'):
        $this->coCellBox->setIconSrc("icon-ci_disciplinaryprocess.gif");
        $this->coCellBox->setObjFWDToolTip(ISMSLib::getInstance()->getToolTipCreateModify($this->getFieldValue('user_create_name'),$this->getFieldValue('date_create'),$this->getFieldValue('user_edit_name'),$this->getFieldValue('date_edit')));
        return $this->coCellBox->draw();
      case $this->getIndexByAlias('incident_name'):
        $this->coCellBox->setAttrStringNoEscape("true");
        if( ISMSLib::userHasACL('M.CI.5.2') ){
          $this->coCellBox->setValue("<a href='javascript:open_proc_action(".$this->getFieldValue('incident_id').",".$this->getFieldValue('user_id').");'>".$this->coCellBox->getValue()."</a>");
        }
        return $this->coCellBox->draw();
        break;
      default:
        return parent::drawItem();
    }
  }
}

class IncidentUserConfirmRemove extends FWDRunnable {
  public function run() {
    $msTitle = FWDLanguage::getPHPStringValue('tt_incident_user_remove','Remover Usu�rio');
    $msMessage = FWDLanguage::getPHPStringValue('st_incident_user_remove',"Voc� tem certeza que deseja remover o usu�rio <b>%user_name%</b> do processo disciplinar do incidente <b>%incident_name%</b>?");
    
    $miIncidentId = FWDWebLib::getObject('incident_id_to_delete')->getValue();
    $miUserId = FWDWebLib::getObject('user_id_to_delete')->getValue();
    
    $moIncident = new CIIncident();
    $moIncident->fetchById($miIncidentId);
    $msIncidentName = ISMSLib::truncateString($moIncident->getFieldValue('incident_name'), 45);
    
    $moUser = new ISMSUser();
    $moUser->fetchById($miUserId);
    $msUserName = ISMSLib::truncateString($moUser->getFieldValue('user_name'), 25);

    $msMessage = str_replace('%user_name%',$msUserName,$msMessage);
    $msMessage = str_replace('%incident_name%',$msIncidentName,$msMessage);
    
    $msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');".
                    "soWindow = soPopUp.getOpener();".
                    "soWindow.remove_incident_user();";
    
    ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,60);
  }
}

class RemoveIncidentUserEvent extends FWDRunnable {
  public function run() {
    $moIncidentUser = new CIIncidentUser();
    
    $miIncidentId = FWDWebLib::getObject('incident_id_to_delete')->getValue();
    $miUserId = FWDWebLib::getObject('user_id_to_delete')->getValue();
    
    $moIncidentUser->createFilter($miIncidentId,'incident_id');
    $moIncidentUser->createFilter($miUserId,'user_id');
    $moIncidentUser->delete();
    
    echo "refresh_grid();";    
  }
}

class FilterEvent extends FWDRunnable {
  public function run() {
    $moGrid = FWDWebLib::getObject('grid_disciplinary_process');
    $moGrid->execEventPopulate(); 
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new IncidentUserConfirmRemove('incident_user_confirm_remove'));
    $moStartEvent->addAjaxEvent(new RemoveIncidentUserEvent('remove_incident_user_event'));
    $moStartEvent->addAjaxEvent(new FilterEvent('filter_event'));
    
    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
        
    $moGrid = FWDWebLib::getObject("grid_disciplinary_process");
    $moHandler = new QueryGridDisciplinaryProcess(FWDWebLib::getConnection());
    $moHandler->setIncidentName(FWDWebLib::getObject("filter_incident")->getValue());
    $moHandler->setUserName(FWDWebLib::getObject("filter_user")->getValue());
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new DisciplinaryProcessDrawGrid('grid_disciplinary_process'));
    
    /* Verifica se a coluna de A��o Realizada deve ser exibida ou n�o */
    $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
    if (in_array('M.CI.5.1', $maACLs)) {
      $moGridColumns = FWDWebLib::getObject('grid_disciplinary_process')->getColumns();
      $moGridColumns[5]->setAttrDisplay("false");
    }
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        function refresh_grid(){
          js_refresh_grid('grid_disciplinary_process');
        }
        
        function remove_incident_user() {
          trigger_event('remove_incident_user_event',3);
          refresh_grid();
        }
        
        function open_proc_action(piIncident,piUser){
          isms_open_popup('popup_disciplinary_process_action','packages/improvement/popup_disciplinary_process_action.php?incident='+piIncident+'&user='+piUser,'','true');
        }
        
      </script>
    <?
  }
}

ISMSLib::testUserPermissionToIncidentMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_disciplinary_process.xml");
?>