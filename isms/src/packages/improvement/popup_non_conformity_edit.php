<?php
include_once "include.php";
include_once $handlers_ref . "grid/improvement/QueryGridNCProcess.php";
include_once $handlers_ref . "select/improvement/QuerySelectNCProcess.php";
include_once $handlers_ref . 'select/improvement/QuerySelectNcActionPlan.php';
include_once $handlers_ref . 'select/QuerySelectNonConformityType.php';

function save($poContext,$pbHasSensitiveChanges,$piContextId=0){
  if($piContextId){
      $poContext->testPermissionToEdit($piContextId); 
      $poContext->update($piContextId, true, $pbHasSensitiveChanges);
    }else{
      $poContext->testPermissionToInsert(); 
      $poContext->insert(true);
    }
    
    $poContext->setFieldValue('nc_id', $piContextId);

    $maActionPlans = array_filter(explode(':',FWDWebLib::getObject('var_action_plans')->getValue()));
    $poContext->updateRelation(new CINcActionPlan(),'ap_id',$maActionPlans);

    $maProcesses = array_filter(explode(':',FWDWebLib::getObject('var_processes')->getValue()));
    $poContext->updateRelation(new CINonConformityProcess() ,'process_id',$maProcesses);

    $moNonConformity = new CINonConformity();

    echo 'soWindow = self.getOpener();'
        .'if(soWindow.refresh_grid) soWindow.refresh_grid();';
    if (!$moNonConformity->getStateForwardConfirmation($piContextId)) {
      echo 'self.close();';
    }  
}

function fillFieldsFromForm($poNc){
  if(FWDWebLib::getObject('nc_responsible_id')->getValue()){
    $poNc->setFieldValue('nc_responsible_id'      ,FWDWebLib::getObject('nc_responsible_id')->getValue());
  }

  $poNc->setFieldValue('nc_name'                ,addslashes(FWDWebLib::getObject('nc_name')->getValue()));
  $poNc->setFieldValue('nc_name'                ,FWDWebLib::getObject('nc_name')->getValue());
  $poNc->setFieldValue('nc_description'         ,FWDWebLib::getObject('nc_description')->getValue());
  $poNc->setFieldValue('nc_cause'               ,FWDWebLib::getObject('nc_cause')->getValue());
  $poNc->setFieldValue('nc_classification'      ,FWDWebLib::getObject('nc_classification')->getValue());
  $poNc->setFieldValue('nc_capability'          ,FWDWebLib::getObject('nc_capability')->getValue());
  if(ISMSLib::getConfigById(GENERAL_MANUAL_DATA_CONTROL)){
    $poNc->setFieldValue("nc_datesent",FWDWebLib::getObject('date_create_nc')->getTimestamp());
  }
  return $poNc;
}

function get_control_name($piId){
  $moControl = new RMControl();
  $moControl->fetchById($piId);
  return $moControl->getName();
}

function get_user_name($piId){
  $moUser = new ISMSUser();
  $moUser->fetchById($piId);
  return $moUser->getName();
}

class GetUserNameEvent extends FWDRunnable {
  public function run(){
    $msUserField = FWDWebLib::getObject('var_user_field')->getValue();
    $miUserId = FWDWebLib::getObject($msUserField)->getValue();
    $msUserName = get_user_name($miUserId);
    echo "gobi('static_$msUserField').setValue('$msUserName');";
  }
}

class SaveEvent extends FWDRunnable {
  
  private function processesHasSensitiveChanges(){
    $varProcesses = FWDWebLib::getObject('var_processes');
    
    $varProcessesIdSerial = FWDWebLib::getObject('var_loaded_processes');
    
    $processesValues = explode(":", $varProcesses->getValue());
    $processesIdSerialValues = explode(":", $varProcessesIdSerial->getValue());
    
    if(array_diff($processesValues, $processesIdSerialValues) || array_diff($processesIdSerialValues, $processesValues))
      return true;
      
    return false;
  }
  
  public function run() {
    $miNcId = FWDWebLib::getObject('nc_id')->getValue();
    $moNc = new CINonConformity();
    $moNc = fillFieldsFromForm($moNc);

    $moNc->setHash(FWDWebLib::getObject('hash')->getValue());    

    $mbHasSensitiveChanges = $moNc->hasSensitiveChanges();
    $mbProcessesHasSensitiveChanges = $this->processesHasSensitiveChanges();
    
    $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    
    if($miNcId && ($mbHasSensitiveChanges || $mbProcessesHasSensitiveChanges) && $miUserId != $moNc->getApprover()){
      $msTitle = FWDLanguage::getPHPStringValue('tt_reapproval','Reaprova��o');
      $msMessage = FWDLanguage::getPHPStringValue('st_sensitive_data_reapproval','Voc� est� alterando dados sens�veis, que necessitam passar por uma reaprova��o. Deseja confirmar as altera��es?');
      $msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
                      "soWindow = soPopUp.getOpener();" .
                      "soWindow.trigger_event('confirm_edit',3);";
      ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,75);
    }else{
      save($moNc,$mbHasSensitiveChanges,$miNcId);
    }
  } 
}

class ConfirmEditEvent extends FWDRunnable {
  public function run(){
    $miNcId = FWDWebLib::getObject('nc_id')->getValue();
    $moNc = new CINonConformity();
    $moNc = fillFieldsFromForm($moNc);    
    save($moNc, true, $miNcId);
  }
}

class RefreshCurrentActionPlansEvent extends FWDRunnable {
  public function run(){
    $moHandler = new QuerySelectNcActionPlan();
    $moHandler->setActionPlanIds(FWDWebLib::getObject('var_action_plans')->getValue());
    $moSelect = FWDWebLib::getObject('current_action_plans');
    $moSelect->setQueryHandler($moHandler);
    $moSelect->populate();
    $moSelect->execEventPopulate();
  }
}

class RefreshCurrentProcessesEvent extends FWDRunnable {
  public function run(){
    $moHandler = new QuerySelectNcProcess();
    $moHandler->setProcessIds(FWDWebLib::getObject('var_processes')->getValue());
    $moSelect = FWDWebLib::getObject('current_processes');
    $moSelect->setQueryHandler($moHandler);
    $moSelect->populate();
    $moSelect->execEventPopulate();
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
    $moStartEvent->addAjaxEvent(new SaveEvent('save_event'));
    $moStartEvent->addAjaxEvent(new GetUserNameEvent('get_user_name'));
    $moStartEvent->addAjaxEvent(new RefreshCurrentActionPlansEvent('refresh_action_plans'));
    $moStartEvent->addAjaxEvent(new RefreshCurrentProcessesEvent('refresh_processes'));
    $moStartEvent->addAjaxEvent(new ConfirmEditEvent('confirm_edit'));
    
  }
}

function disableAllElements(){
  FWDWebLib::getObject('nc_name')->setAttrDisabled('true');
  FWDWebLib::getObject('nc_description')->setAttrDisabled('true');
  FWDWebLib::getObject('nc_classification')->setAttrDisabled('true');
  FWDWebLib::getObject('nc_capability')->setAttrDisabled('true');
  FWDWebLib::getObject('nc_cause')->setAttrDisabled('true');

  FWDWebLib::getObject('add_processes')->setShouldDraw(false);
  FWDWebLib::getObject('remove_processes')->setShouldDraw(false);
  FWDWebLib::getObject('responsible_edit')->setShouldDraw(false);
  FWDWebLib::getObject('add_action_plans')->setShouldDraw(false);
  FWDWebLib::getObject('remove_action_plans')->setShouldDraw(false);
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $miNcId = FWDWebLib::getObject('nc_id')->getValue();
    $moCtxUserTest = new CINonConformity();

    if($miNcId){
      //$moCtxUserTest->testPermissionToEdit($miNcId);
      $moNc = new CINonConformity();
      $moNc->fetchById($miNcId);

      FWDWebLib::getObject('static_nc_responsible_id')->setValue(get_user_name($moNc->getFieldValue('nc_responsible_id')));
      FWDWebLib::getObject('static_nc_sender_id')->setValue(get_user_name($moNc->getFieldValue('nc_sender_id')));

	  $moHandler = new QuerySelectNonConformityType();
	  $moSelect = FWDWebLib::getObject('nc_classification');
	  $moSelect->setQueryHandler($moHandler);
	  $moSelect->populate();      
      
      $moHandler = new QuerySelectNcActionPlan();
      $moHandler->setNc($miNcId);
      $moSelect = FWDWebLib::getObject('current_action_plans');
      $moSelect->setQueryHandler($moHandler);
      $moSelect->populate();

      $moHandler = new QuerySelectNcProcess();
      $moHandler->setNc($miNcId);
      $moSelect = FWDWebLib::getObject('current_processes');
      $moSelect->setQueryHandler($moHandler);
      $moSelect->populate();
      
      $moNc->fillFormFromFields();      
      
      FWDWebLib::getObject('hash')->setValue($moNc->getHash());

      // para verifica��o se foi alterado algum processo.
      $loadedProcesses = FWDWebLib::getObject('var_loaded_processes');
      $loadedProcesses->setValue(implode(":", $moSelect->getItemsKeys()));
      
      /*sess�o para abilitar / desablitar elementos da tela*/
      $miResponsibleId = $moNc->getFieldValue('nc_responsible_id');
      $miGNC = $moNc->getApprover();
      $miCreator = $moNc->getFieldValue('nc_sender_id');
      $miUserId = ISMSLib::getCurrentUserId();
      $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
      switch($moNc->getContextState()){
        case CONTEXT_STATE_CI_SENT:{
          if($miUserId==$miCreator && $miUserId!=$miGNC && (!in_array('M.CI.7.3',$maACLs))){
            disableAllElements();
            FWDWebLib::getObject('nc_name')->setAttrDisabled('false');
            FWDWebLib::getObject('nc_description')->setAttrDisabled('false');
            FWDWebLib::getObject('add_processes')->setShouldDraw(true);
            FWDWebLib::getObject('remove_processes')->setShouldDraw(true);
          }else{
            FWDWebLib::getObject('add_action_plans')->setShouldDraw(false);
            FWDWebLib::getObject('remove_action_plans')->setShouldDraw(false);
          }
          if(ISMSLib::getConfigById(GENERAL_MANUAL_DATA_CONTROL)){
            FWDWebLib::getObject('nc_datesent')->setShouldDraw(false);
            FWDWebLib::getObject('vg_date_create_nc')->setAttrDisplay("true");
            FWDWebLib::getObject('date_create_nc')->setValue(ISMSLib::getISMSShortDate($moNc->getFieldValue('nc_datesent')));
          }
          break;
        }
        case CONTEXT_STATE_CI_OPEN:{
          if($miUserId == $miGNC){
            FWDWebLib::getObject('add_action_plans')->setShouldDraw(false);
            FWDWebLib::getObject('remove_action_plans')->setShouldDraw(false);
            if(ISMSLib::getConfigById(GENERAL_MANUAL_DATA_CONTROL)){
              FWDWebLib::getObject('nc_datesent')->setShouldDraw(false);
              FWDWebLib::getObject('vg_date_create_nc')->setAttrDisplay("true");
              FWDWebLib::getObject('date_create_nc')->setValue(ISMSLib::getISMSShortDate($moNc->getFieldValue('nc_datesent')));
            }
          } else {
            disableAllElements();
          }
          break;
        }
        case CONTEXT_STATE_CI_DIRECTED:{
          disableAllElements();

          // se for gestor de nc pode modificar o(s) processo(s).
          if($miUserId==$miGNC){
            FWDWebLib::getObject('add_processes')->setShouldDraw(true);
            FWDWebLib::getObject('remove_processes')->setShouldDraw(true);
          }
                    
          FWDWebLib::getObject('add_action_plans')->setShouldDraw(true);
          FWDWebLib::getObject('remove_action_plans')->setShouldDraw(true);
          FWDWebLib::getObject('nc_cause')->setAttrDisabled('false');
          if(ISMSLib::getConfigById(GENERAL_MANUAL_DATA_CONTROL)){
            FWDWebLib::getObject('nc_datesent')->setShouldDraw(false);
            FWDWebLib::getObject('vg_date_create_nc')->setAttrDisplay("true");
            FWDWebLib::getObject('date_create_nc')->setValue(ISMSLib::getISMSShortDate($moNc->getFieldValue('nc_datesent')));
          }
          break;
        }
        case CONTEXT_STATE_CI_NC_PENDANT:{
          disableAllElements();
          break;
        }
        case CONTEXT_STATE_DENIED: {
          FWDWebLib::getObject('nc_classification')->setAttrDisabled('true');
          FWDWebLib::getObject('nc_capability')->setAttrDisabled('true');
          FWDWebLib::getObject('nc_cause')->setAttrDisabled('true');

          FWDWebLib::getObject('responsible_edit')->setShouldDraw(false);
          FWDWebLib::getObject('add_action_plans')->setShouldDraw(false);
          FWDWebLib::getObject('remove_action_plans')->setShouldDraw(false);          
          break;
        }        
        default:{
          disableAllElements();
         break;
        }
      }
      $msNewRequiredIds = '';
      /*torna os campos obrigat�rios dependendo de qual o estado na n�o conformidade*/
      switch($moNc->getContextState()){
        case CONTEXT_STATE_CI_OPEN:{
          FWDWebLib::getObject('label_nc_responsible_id')->setAttrMustfill('true');
          FWDWebLib::getObject('label_nc_responsible_id')->setValue('<b>'.FWDWebLib::getObject('label_nc_responsible_id')->getValue().'</b>');
          FWDWebLib::getObject('label_nc_classification')->setAttrMustfill('true');
          FWDWebLib::getObject('label_nc_classification')->setValue('<b>'.FWDWebLib::getObject('label_nc_classification')->getValue().'</b>');
          FWDWebLib::getObject('label_nc_capability')->setAttrMustfill('true');
          FWDWebLib::getObject('label_nc_capability')->setValue('<b>'.FWDWebLib::getObject('label_nc_capability')->getValue().'</b>');
          if(ISMSLib::getConfigById(GENERAL_MANUAL_DATA_CONTROL)){
            FWDWebLib::getObject('label_nc_datesent')->setAttrMustFill("true");
            FWDWebLib::getObject('label_nc_datesent')->setValue("<b>".FWDWebLib::getObject('label_nc_datesent')->getValue()."</b>");
            $msNewRequiredIds .= ':date_create_nc';
          }
          $msNewRequiredIds .= ':nc_responsible_id:nc_classification:nc_capability';
          break;
        }
        case CONTEXT_STATE_CI_NC_PENDANT:
        case CONTEXT_STATE_CI_DIRECTED:{
          FWDWebLib::getObject('label_nc_responsible_id')->setAttrMustfill('true');
          FWDWebLib::getObject('label_nc_responsible_id')->setValue('<b>'.FWDWebLib::getObject('label_nc_responsible_id')->getValue().'</b>');
          FWDWebLib::getObject('label_nc_classification')->setAttrMustfill('true');
          FWDWebLib::getObject('label_nc_classification')->setValue('<b>'.FWDWebLib::getObject('label_nc_classification')->getValue().'</b>');
          FWDWebLib::getObject('label_nc_capability')->setAttrMustfill('true');
          FWDWebLib::getObject('label_nc_capability')->setValue('<b>'.FWDWebLib::getObject('label_nc_capability')->getValue().'</b>');

          $msNewRequiredIds .= ':nc_responsible_id:nc_classification:nc_capability';

          FWDWebLib::getObject('label_nc_cause')->setAttrMustfill('true');
          FWDWebLib::getObject('label_nc_cause')->setValue('<b>'.FWDWebLib::getObject('label_nc_cause')->getValue().'</b>');
          FWDWebLib::getObject('label_current_action_plans')->setAttrMustfill('true');
          FWDWebLib::getObject('label_current_action_plans')->setValue('<b>'.FWDWebLib::getObject('label_current_action_plans')->getValue().'</b>');

          $msNewRequiredIds .= ':nc_cause:current_action_plans';
          
          if(ISMSLib::getConfigById(GENERAL_MANUAL_DATA_CONTROL)){
            FWDWebLib::getObject('label_nc_datesent')->setAttrMustFill("true");
            FWDWebLib::getObject('label_nc_datesent')->setValue("<b>".FWDWebLib::getObject('label_nc_datesent')->getValue()."</b>");
            $msNewRequiredIds .= ':date_create_nc';
            
            FWDWebLib::getObject('nc_datesent')->setShouldDraw(true);
            FWDWebLib::getObject('vg_date_create_nc')->setAttrDisplay("false");
            FWDWebLib::getObject('date_create_nc')->setValue(ISMSLib::getISMSShortDate($moNc->getFieldValue('nc_datesent')));            
          }
          
          break;
        }
        default:{
          if(ISMSLib::getConfigById(GENERAL_MANUAL_DATA_CONTROL)){
            FWDWebLib::getObject('label_nc_datesent')->setAttrMustFill("true");
            FWDWebLib::getObject('label_nc_datesent')->setValue("<b>".FWDWebLib::getObject('label_nc_datesent')->getValue()."</b>");
            $msNewRequiredIds .= ':date_create_nc';
            
            FWDWebLib::getObject('nc_datesent')->setShouldDraw(true);
            FWDWebLib::getObject('vg_date_create_nc')->setAttrDisplay("false");
            FWDWebLib::getObject('date_create_nc')->setValue(ISMSLib::getISMSShortDate($moNc->getFieldValue('nc_datesent')));            
          }
          break;
        }
      }

      FWDWebLib::getObject('RequiredCheck')->setAttrElements(FWDWebLib::getObject('RequiredCheck')->getAttrElements().$msNewRequiredIds);
      FWDWebLib::getObject('var_nc_id')->setValue(FWDWebLib::getObject('nc_id')->getValue());
    }else{
      $moCtxUserTest->testPermissionToInsert();
    }
    //instala a seguran�a de ACL na p�gina
    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">

        if(gebi('nc_name').disabled==false)
          gebi('nc_name').focus();

        function set_control(piId){
          gebi(gebi('var_control_field').value).value = piId;
          trigger_event('get_control_name',3);
        }

        function set_user(piId){
          gebi(gebi('var_user_field').value).value = piId;
          trigger_event('get_user_name',3);
        }

        function set_action_plans(psIds){
          gebi('var_action_plans').value = psIds;
          trigger_event('refresh_action_plans',3);
        }

        function set_processes(psIds){
          gebi('var_processes').value = psIds;
          trigger_event('refresh_processes',3);
        }

        function close_popup(){
          soPopUpManager.closePopUp('popup_non_conformity_edit');
        }

        function refresh_grid(){
          soWindow = self.getOpener();
          if(soWindow.refresh_grid){
            soWindow.refresh_grid();
          }
        }

        function refresh_grids(){
          refresh_grid();
        }

      </script>
    <?
  }
}

//testa se a licen�a utilizada permite acessar o m�dulo de incidente
ISMSLib::testUserPermissionToIncidentMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_non_conformity_edit.xml');

?>