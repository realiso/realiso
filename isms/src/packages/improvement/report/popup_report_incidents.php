<?php

set_time_limit(3600);
include_once 'include.php';

class IncidentLevelIterator extends FWDReportLevelIterator {
  
  public function __construct(){
    parent::__construct('incident_id');
    $msIconSrc = FWDWebLib::getInstance()->getGfxRef()."icon-ci_incident.gif";
    FWDWebLib::getObject('incident_id')->setAttrSrc($msIconSrc);
  }
  
  public function fetch(FWDDBDataSet $poDataSet){
    FWDWebLib::getObject('incident_name')->setValue($poDataSet->getFieldByAlias('incident_name')->getValue());
  }
  
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moFilter = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter();
    
    FWDWebLib::getObject('incident_level')->setLevelIterator(new IncidentLevelIterator());
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport(FWDWebLib::getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    
    $moComponent->setReportFilter($moFilter);
    
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('mx_incidents','Incidentes'));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_report_incidents.xml');

?>