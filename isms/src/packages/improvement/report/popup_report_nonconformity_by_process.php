<?php
set_time_limit(3600);
include_once 'include.php';

class ProcessLevelIterator extends FWDReportLevelIterator {
  public function __construct(){
    parent::__construct('process_id');
  }

  public function fetch(FWDDBDataSet $poDataSet){
  	$miProcessValue = $poDataSet->getFieldByAlias("process_value")->getValue();		
		$msIcon = FWDWebLib::getInstance()->getGfxRef() . 'icon-process_' . RMRiskConfig::getRiskColor($miProcessValue) . '.png'; 
    $msName = $poDataSet->getFieldByAlias('process_name')->getValue();        
    FWDWebLib::getObject('process_icon')->setAttrSrc($msIcon);
    FWDWebLib::getObject('process_name')->setValue($msName);
  }
}

class NCLevelIterator extends FWDReportLevelIterator {  
	public function __construct(){
    parent::__construct('nc_id');
  }

  public function fetch(FWDDBDataSet $poDataSet){
    $msIcon = 'icon-ci_nonconformity.png';
    $msName = $poDataSet->getFieldByAlias('nc_name')->getValue();    
    FWDWebLib::getObject('nc_icon')->setAttrSrc(FWDWebLib::getInstance()->getGfxRef().$msIcon);
    FWDWebLib::getObject('nc_name')->setValue($msName);
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    FWDWebLib::getObject('level_process')->setLevelIterator(new ProcessLevelIterator());
    FWDWebLib::getObject('level_nc')->setLevelIterator(new NCLevelIterator());
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport(FWDWebLib::getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());    
    $moComponent->setReportFilter(FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter());    
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('mx_nonconformity_by_process','N�o Conformidade por Processo'));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_report_nonconformity_by_process.xml');
?>