<?php
set_time_limit(3600);
include_once "include.php";

class UserLevelIterator extends FWDReportLevelIterator {  

  public function __construct(){
    parent::__construct("user_id");
  }
  
  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msGfxRef = $moWebLib->getGfxRef();

    $msUserName = $poDataSet->getFieldByAlias("user_name")->getValue();
    
    $msIcon = $msGfxRef . 'icon-user.png';

    $moWebLib->getObject('user_icon')->setAttrSrc($msIcon);
    $moWebLib->getObject('user_name')->setValue($msUserName);    
  }
}

class TaskLevelIterator extends FWDReportLevelIterator {

  public function __construct(){
    parent::__construct("task_activity");
  }

  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msGfxRef = $moWebLib->getGfxRef();
    
    $miTaskActivity = $poDataSet->getFieldByAlias("task_activity")->getValue();
    $miTaskAmount = $poDataSet->getFieldByAlias("task_count")->getValue();   
    
    $moWebLib->getObject('task_description')->setValue(ISMSActivity::getDescription($miTaskActivity));
    $moWebLib->getObject('task_amount')->setValue($miTaskAmount);
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $moWebLib->getObject("level_user")->setLevelIterator(new UserLevelIterator());
    $moWebLib->getObject("level_task")->setLevelIterator(new TaskLevelIterator());
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport($moWebLib->getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    $moComponent->setReportFilter($moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter());
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('rs_summarized_pending_tasks', "Tarefas Pendentes Resumidas"));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_report_summarized_pendant_tasks_ci.xml");
?>