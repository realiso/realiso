<?php
set_time_limit(3600);
include_once "include.php";

class NCLevelIterator extends FWDReportLevelIterator {

  public function __construct(){
    parent::__construct("nc_id");
  }
  
  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msGfxRef = $moWebLib->getGfxRef();
   
    $msNCName = $poDataSet->getFieldByAlias("nc_name")->getValue();
    if($poDataSet->getFieldByAlias("responsible_name")->getValue()){
      $msNCResponsibleName = $poDataSet->getFieldByAlias("responsible_name")->getValue();
    }else{
      $moConfig = new ISMSConfig();
      $moUser = new ISMSUser();
      $moUser->fetchById($moConfig->getConfig(USER_NON_CONFORMITY_MANAGER));
      $msNCResponsibleName = $moUser->getName() . FWDLanguage::getPHPStringValue('st_nc_manager_with_parentheses',' (Gestor de N�o Conformidade)');;
    }
    $msIcon = $msGfxRef . 'icon-ci_nonconformity.png';

    $moWebLib->getObject('nc_icon')->setAttrSrc($msIcon);
    $moWebLib->getObject('nc_name')->setValue($msNCName);
    $moWebLib->getObject('nc_responsible_name')->setValue($msNCResponsibleName);
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $moWebLib->getObject("level_nc")->setLevelIterator(new NCLevelIterator());
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport($moWebLib->getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    $moComponent->setReportFilter($moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter());
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('rs_nc_without_ap', "N�o Conformidades sem Planos de A��o"));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_report_nc_without_ap.xml");
?>