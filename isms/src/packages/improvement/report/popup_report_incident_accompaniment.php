<?php
set_time_limit(3600);
include_once 'include.php';

class IncidentLevelIterator extends FWDReportLevelIterator {  
	public function __construct(){
    parent::__construct('incident_id');
  }

  public function fetch(FWDDBDataSet $poDataSet){
    $msIcon = 'icon-ci_incident.png';
    $msName = $poDataSet->getFieldByAlias('incident_name')->getValue();        
    FWDWebLib::getObject('incident_icon')->setAttrSrc(FWDWebLib::getInstance()->getGfxRef().$msIcon);
    FWDWebLib::getObject('incident_name')->setValue($msName);
  }
}

class InfoLevelIterator extends FWDReportLevelIterator {  
	public function __construct(){
    parent::__construct('incident_state');
  }

  public function fetch(FWDDBDataSet $poDataSet){    
    $miState = $poDataSet->getFieldByAlias('incident_state')->getValue();
    $msState = ISMSContextObject::getContextStateAsString($miState);        
    $msCreator = $poDataSet->getFieldByAlias('incident_responsible_name')->getValue();    
    FWDWebLib::getObject('incident_responsible_name')->setValue($msCreator);
    FWDWebLib::getObject('incident_state')->setValue($msState);
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    FWDWebLib::getObject('level_incident')->setLevelIterator(new IncidentLevelIterator());
    FWDWebLib::getObject('level_info')->setLevelIterator(new InfoLevelIterator());
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport(FWDWebLib::getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());    
    $moComponent->setReportFilter(FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter());    
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('mx_incident_accompaniment','Acompanhamento de Incidente'));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_report_incident_accompaniment.xml');
?>