<?php

set_time_limit(3600);
include_once 'include.php';

class DpLevelIterator extends FWDReportLevelIterator {
  
  public function __construct(){
    parent::__construct('dp_id');
    $msIconSrc = FWDWebLib::getInstance()->getGfxRef()."icon-ci_disciplinaryprocess.gif";
    FWDWebLib::getObject('dp_id')->setAttrSrc($msIconSrc);
  }
  
  public function fetch(FWDDBDataSet $poDataSet){
    FWDWebLib::getObject('dp_name')->setValue($poDataSet->getFieldByAlias('dp_name')->getValue());
  }
  
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moFilter = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter();
    
    FWDWebLib::getObject('dp_level')->setLevelIterator(new DpLevelIterator());
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport(FWDWebLib::getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    
    $moComponent->setReportFilter($moFilter);
    
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('mx_disciplinary_process','Processos Disciplinares'));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_report_disciplinary_process.xml');

?>