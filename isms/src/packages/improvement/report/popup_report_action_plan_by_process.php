<?php

set_time_limit(3600);
include_once 'include.php';

class ProcessLevelIterator extends FWDReportLevelIterator {
  
  public function __construct(){
    parent::__construct('process_id');
  }
  
  public function fetch(FWDDBDataSet $poDataSet){
    $miValue = $poDataSet->getFieldByAlias('process_value')->getValue();
    $msIcon = FWDWebLib::getInstance()->getGfxRef() . 'icon-process_' . RMRiskConfig::getRiskColor($miValue) . '.png';
    FWDWebLib::getObject('process_icon')->setAttrSrc($msIcon);
    FWDWebLib::getObject('process_name')->setValue($poDataSet->getFieldByAlias('process_name')->getValue());    
    FWDWebLib::getObject('responsible_name')->setValue($poDataSet->getFieldByAlias('responsible_name')->getValue());
  }
  
}
class ApLevelIterator extends FWDReportLevelIterator {
  
  public function __construct(){
    parent::__construct('ap_id');
  }
  
  public function fetch(FWDDBDataSet $poDataSet){
    FWDWebLib::getObject('ap_icon')->setAttrSrc(FWDWebLib::getInstance()->getGfxRef()."icon-ci_action_plan.png");
    FWDWebLib::getObject('ap_name')->setValue($poDataSet->getFieldByAlias('ap_name')->getValue());
    FWDWebLib::getObject('ap_justification')->setValue($poDataSet->getFieldByAlias('ap_justification')->getValue());
    FWDWebLib::getObject('ap_date')->setValue($poDataSet->getFieldByAlias('ap_date')->getValue());
  }
  
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moFilter = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter();
    
    FWDWebLib::getObject('process_level')->setLevelIterator(new ProcessLevelIterator());
    
    FWDWebLib::getObject('ap_level')->setLevelIterator(new ApLevelIterator());
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport(FWDWebLib::getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    
    $moComponent->setReportFilter($moFilter);
    
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('tt_action_plan_by_process','Plano de A��o por Processo'));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_report_action_plan_by_process.xml');

?>