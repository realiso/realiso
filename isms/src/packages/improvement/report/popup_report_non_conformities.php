<?php

set_time_limit(3600);
include_once 'include.php';

class NcLevelIterator extends FWDReportLevelIterator {
  
  public function __construct(){
    parent::__construct('nc_id');
    $msIconSrc = FWDWebLib::getInstance()->getGfxRef()."icon-ci_nonconformity.gif";
    FWDWebLib::getObject('nc_id')->setAttrSrc($msIconSrc);
  }
  
  public function fetch(FWDDBDataSet $poDataSet){
    FWDWebLib::getObject('nc_name')->setValue($poDataSet->getFieldByAlias('nc_name')->getValue());
  }
  
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moFilter = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter();
    
    FWDWebLib::getObject('nc_level')->setLevelIterator(new NcLevelIterator());
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport(FWDWebLib::getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    
    $moComponent->setReportFilter($moFilter);
    
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('mx_non_conformities','N�o Conformidades'));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_report_non_conformities.xml');

?>