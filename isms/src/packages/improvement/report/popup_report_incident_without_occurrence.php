<?php
set_time_limit(3600);
include_once "include.php";

class IncidentLevelIterator extends FWDReportLevelIterator {

  public function __construct(){
    parent::__construct("incident_id");
  }
  
  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msGfxRef = $moWebLib->getGfxRef();
   
    $msIncidentName = $poDataSet->getFieldByAlias("incident_name")->getValue();
    $msIncidentResponsibleName = $poDataSet->getFieldByAlias("responsible_name")->getValue();

    
    $msIcon = $msGfxRef . 'icon-ci_incident.png';

    $moWebLib->getObject('incident_icon')->setAttrSrc($msIcon);
    $moWebLib->getObject('incident_name')->setValue($msIncidentName);
    $moWebLib->getObject('incident_responsible_name')->setValue($msIncidentResponsibleName);
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $moWebLib->getObject("level_incident")->setLevelIterator(new IncidentLevelIterator());
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport($moWebLib->getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    $moComponent->setReportFilter($moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter());
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('rs_incidents_without_occurrence', "Incidentes sem Ocorrência"));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_report_incident_without_occurrence.xml");
?>