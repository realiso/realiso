<?php
include_once "include.php";
include_once $handlers_ref . "grid/QueryGridUserTasks.php";
include_once $handlers_ref . "QueryIncidentRiskParameter.php";

class ApproveEvent extends FWDRunnable {
	public function run(){
		$miTaskId = FWDWebLib::getObject('task_id')->getValue();

		//teste para verificar se o sistema n�o est� sendo hakeado
		$moTaskTest = new WKFTask();
		$moTaskTest->fetchById($miTaskId);

		if($moTaskTest->getFieldValue('task_is_visible')){

			$moTaskTest->testPermissionToExecute($miTaskId);
			$miContextId = $moTaskTest->getFieldValue('task_context_id');
			//$msJustification = FWDWebLib::getObject('task_justification')->getValue();

			$miAction = FWDWebLib::getObject('action')->getValue();

			$moTask = new WKFTask();

			$moTaskMetadata = new WKFTaskMetadata();
			//$moTaskMetadata->setFieldValue('task_id',$miTaskId);
			$moTaskMetadata->createFilter($miTaskId,'task_id');
			$moTaskMetadata->select();
			$moTaskMetadata->fetch();
			$miIncidentId = $moTaskMetadata->getFieldByAlias('metadata_context_id')->getValue();

			$queryIncident = new QueryIncidentRiskParameter(FWDWebLib::getConnection());
			$queryIncident->setIncidentId($miIncidentId);
			//   	$queryIncident->setParameterName($parameter);
			$queryIncident->makeQuery();
			$queryIncident->executeQuery();
			//$queryReturn = $queryIncident->getDataset();
			$parameterList = array();
			while($queryIncident->fetch()){
				$parameterList[$queryIncident->getFieldValue('ci_parameter_name')] = $queryIncident->getFieldValue('ci_parameter_value');
			}

			$moRiskParameters = array();

			foreach($parameterList as $parameterId=>$valueId) {
				$moRiskQuery = new RMRiskValue();
				$moRiskQuery->createElementIdFilter($moTask->getFieldValue('task_context_id'));
				$moRiskQuery->createParameterIdFilter($parameterId);
				$moRiskQuery->setValueId($valueId);
				$moRiskQuery->update();
			}

			$moTask->setFieldValue('task_date_accomplished', ISMSLib::ISMSTime());
			$moTask->setFieldValue('task_is_visible', 0);
			$moTask->update($miTaskId);

			echo "soWindow = soPopUpManager.getPopUpById('popup_new_risk_parameter_approval_task').getOpener();"
			."if (soWindow.refresh_grids)"
			."  soWindow.refresh_grids();"
			."soPopUpManager.closePopUp('popup_new_risk_parameter_approval_task');";

			/*
				$moContextObject = new ISMSContextObject();
				$moContext = $moContextObject->getContextObjectByContextId($miContextId);
				$moContext->fetchById($miContextId);
				$moContext->stateForward($miAction, $msJustification);

				$msNextTaskJS = "";
				$mbOpenNextTask = (bool)array_filter(explode(':',FWDWebLib::getObject('next_task_controller')->getValue()));
				if ($mbOpenNextTask) {
				$miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
				$moHandlerTask = new QueryGridUserTasks(FWDWebLib::getConnection());
				$moHandlerTask->setUser($miUserId);
				$moHandlerTask->setOrderBy('task_date_created','-');
				$moHandlerTask->makeQuery();
				$moHandlerTask->executeQuery();
				if ($moHandlerTask->fetch()) {
				$msNextTaskJS = 'soWindow.setTimeout("'.ISMSLib::getTaskPopup($moHandlerTask->getFieldValue('task_id'),
				$moHandlerTask->getFieldValue('task_activity'),
				$moHandlerTask->getFieldValue('task_context_id'),
				$moHandlerTask->getFieldValue('task_context_type'),
				2).',1");';
				}
				}
				echo "soWindow = soPopUpManager.getPopUpById('popup_simple_task_view').getOpener();"
				."if (soWindow.refresh_grids)"
				."  soWindow.refresh_grids();"
				.$msNextTaskJS
				."soPopUpManager.closePopUp('popup_simple_task_view');";
				*/
		}
	}
}

class DenyEvent extends FWDRunnable {
	public function run(){
		$miTaskId = FWDWebLib::getObject('task_id')->getValue();

		//teste para verificar se o sistema n�o est� sendo hakeado
		$moTaskTest = new WKFTask();
		$moTaskTest->fetchById($miTaskId);

		if($moTaskTest->getFieldValue('task_is_visible')){
			$moTaskTest->testPermissionToExecute($miTaskId);
			$miContextId = $moTaskTest->getFieldValue('task_context_id');
			$moTask->setFieldValue('task_date_accomplished', ISMSLib::ISMSTime());
			$moTask->setFieldValue('task_is_visible', 0);
			$moTask->update($miTaskId);

			echo "soWindow = soPopUpManager.getPopUpById('popup_new_risk_parameter_approval_task').getOpener();"
			."if (soWindow.refresh_grids)"
			."  soWindow.refresh_grids();"
			."soPopUpManager.closePopUp('popup_new_risk_parameter_approval_task');";
		}
	}
}


class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));
		$moStartEvent->addAjaxEvent(new ApproveEvent('approve_event'));
		$moStartEvent->addAjaxEvent(new DenyEvent('deny_event'));
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){

		$mbDumpHTML = true;
		$miTaskId = FWDWebLib::getObject('task_id')->getValue();
		$moTask = new WKFTask();
		$moTask->fetchById($miTaskId);

		if($moTask->getFieldValue('task_is_visible')){
			//teste para verificar se o sistema n�o est� sendo hakeado
			$moTask->testPermissionToExecute($miTaskId);

			$moTaskMetadata = new WKFTaskMetadata();
			//$moTaskMetadata->setFieldValue('task_id',$miTaskId);
			$moTaskMetadata->createFilter($miTaskId,'task_id');
			$moTaskMetadata->select();
			$moTaskMetadata->fetch();
			$miIncidentId = $moTaskMetadata->getFieldByAlias('metadata_context_id')->getValue();

			$queryIncident = new QueryIncidentRiskParameter(FWDWebLib::getConnection());
			$queryIncident->setIncidentId($miIncidentId);
			//   	$queryIncident->setParameterName($parameter);
			$queryIncident->makeQuery();
			$queryIncident->executeQuery();
			//$queryReturn = $queryIncident->getDataset();
			$parameterList = array();
			while($queryIncident->fetch()){
				$parameterList[$queryIncident->getFieldValue('ci_parameter_name')] = $queryIncident->getFieldValue('ci_parameter_value');
			}


			$maParameters = array();
			$moHandler = new QueryParameterNames(FWDWebLib::getConnection());
			$moHandler->makeQuery();
			$moHandler->executeQuery();
			$moDataset = $moHandler->getDataset();
			while($moDataset->fetch()){
				$miId = $moDataset->getFieldByAlias('parametername_id')->getValue();
				$msName = $moDataset->getFieldByAlias('parametername_name')->getValue();
				$maParameters[$miId] = $msName;
			}

			$maValuesNames = array();
			$moHandler = new QueryParameterValueNames(FWDWebLib::getConnection());
			$moHandler->setType(RISKPARAMETER_RISK);
			$moHandler->makeQuery();
			$moHandler->executeQuery();
			$moDataset = $moHandler->getDataset();
			while($moDataset->fetch()){
				$miId = $moDataset->getFieldByAlias('value_id')->getValue();
				$msName = $moDataset->getFieldByAlias('value_name')->getValue();
				$maValuesNames[$miId] = $msName;
			}

			$moRiskParameters = array();

			$moRiskQuery = new RMRiskValue();
			$moRiskQuery->createElementIdFilter($moTask->getFieldValue('task_context_id'));
			$moRiskQuery->select();
			while($moRiskQuery->fetch()) {
				$moRiskParameters[$moRiskQuery->getFieldByAlias('riskvalue_parameter_id')->getValue()] = $moRiskQuery->getFieldByAlias('riskvalue_value_id')->getValue();
			}


			$parameterString = "";

			foreach($parameterList as $parameterId => $valueId) {
				if($valueId != $moRiskParameters[$parameterId])
				$parameterString .= $maParameters[$parameterId] . " " . FWDLanguage::getPHPStringValue('st_from_task','de') . " " . $maValuesNames[$valueId] . " " . FWDLanguage::getPHPStringValue('st_to_task','para') . " " . $maValuesNames[$moRiskParameters[$parameterId]] . "<br>\n";
			}

			FWDWebLib::getObject('context_name')->setValue($parameterString);


			FWDWebLib::getObject('task_date')->setValue(ISMSLib::getISMSDate($moTask->getFieldValue('task_date_created')));

			$moUser = new ISMSUser();
			$moUser->fetchById($moTask->getFieldValue('task_creator_id'));
			FWDWebLib::getObject('task_inquirer')->setValue($moUser->getFieldValue('user_name'));

			//FWDWebLib::getObject('task_name')->setValue(ISMSActivity::getDescription($moTask->getFieldValue('task_activity')));

			$moContextObject = new ISMSContextObject();
			$moContext = $moContextObject->getContextObjectByContextId($moTask->getFieldValue('task_context_id'));
			if($moContext){
				if($moContext->fetchById($moTask->getFieldValue('task_context_id'))){
					//FWDWebLib::getObject('context_type')->setValue($moContext->getLabel().":");

					$msContextName = $moContext->getName();
					if (strlen($msContextName) > 35) {
						$msContextName = substr($msContextName,0,32)."...";
					}
					$msContextLink = "<a href='javascript:open_visualize(".$moContext->getId().",".$moContext->getContextType().",\"".uniqid()."\")'>".$msContextName."</a>";
					FWDWebLib::getObject('task_name')->setAttrStringNoEscape(true);
					FWDWebLib::getObject('task_name')->setValue($msContextLink);
				}else{
					$mbDumpHTML = false;
				}
			}else{
				$mbDumpHTML = false;
			}
			if($mbDumpHTML){
				FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
			}else{
				return FWDLanguage::getPHPStringValue('st_denied_permission_to_execute_task','Voc� n�o tem permiss�o para executar a tarefa ');
			}

		}
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_new_risk_parameter_approval_task.xml");
?>