<?php
include_once "include.php";
include_once $handlers_ref . "grid/improvement/QueryGridIncidentProcessSearch.php";
include_once $handlers_ref . "QueryProcessesByIncidentId.php";

function close_popup(){
    echo "soWindow = soPopUpManager.getPopUpById('popup_incident_process_association_edit').getOpener();
    if (soWindow.refresh_grid) soWindow.refresh_grid();
    soPopUpManager.closePopUp('popup_incident_process_association_edit');";
}
class GridSearch extends FWDDrawGrid {
  public function DrawItem(){
    switch($this->ciColumnIndex){
      case 1:
        $msGfxRef = FWDWebLib::getInstance()->getGfxRef();
        $moIcon = new FWDIcon(new FWDBox(2,4));
        $this->coCellBox->setIconSrc("icon-process_".RMRiskConfig::getRiskColor($this->caData[3]).".gif");
        return parent::drawItem();
        break;
      case 2:
        $this->coCellBox->setAttrStringNoEscape("true");
        $this->coCellBox->setValue("<a href='javascript:open_visualize(".$this->caData[1].",".CONTEXT_PROCESS.",\"".uniqid()."\")'>{$this->caData[2]}</a>");
        return $this->coCellBox->draw();
        break;
      
      default:
        return parent::drawItem();
      break;
    }
  }
}
class SaveSuggestedEvent extends FWDRunnable {
  public function run() {
    $maValues = array();
    if(FWDWebLib::getObject('var_ids_suggested')->getValue())
      $maValues = array_filter(explode(':',FWDWebLib::getObject('var_ids_suggested')->getValue()));
    if(count($maValues)){
      $moIncProc = new CIIncidentProcess();
      $moIncProc->setFieldValue('incident_id',FWDWebLib::getObject('var_id')->getValue());
      foreach($maValues as $miValue){
          $moIncProc->setFieldValue('process_id',$miValue);
          $moIncProc->insert();
      }
    }
    close_popup();
  }
}

class SaveEvent extends FWDRunnable {
  public function run() {
    $maValues = array();
    $maValuesDefault = array();
    if(FWDWebLib::getObject('var_ctx_ids')->getValue())
      $maValues = explode(':',FWDWebLib::getObject('var_ctx_ids')->getValue());
    if(FWDWebLib::getObject('var_ids_original')->getValue())
      $maValuesDefault = explode(':',FWDWebLib::getObject('var_ids_original')->getValue());

    $maValuesInsert= array_filter(array_diff($maValues,$maValuesDefault));
    $maValuesDelete = array_filter(array_diff($maValuesDefault,$maValues));
    if((count($maValuesInsert))||(count($maValuesDelete))){
      $moIncProc = new CIIncidentProcess();
      $moIncProc->setFieldValue('incident_id',FWDWebLib::getObject('var_id')->getValue());
      foreach($maValuesInsert as $miValue){
          $moIncProc->setFieldValue('process_id',$miValue);
          $moIncProc->insert();
      }
      
      foreach($maValuesDelete as $miValue){
          $moIncProcDel = new CIIncidentProcess();
          $moIncProcDel->getFieldByAlias('incident_id')->addFilter(new FWDDbFilter('=',FWDWebLib::getObject('var_id')->getValue()));
          $moIncProcDel->getFieldByAlias('process_id')->addFilter(new FWDDbFilter('=',$miValue));
          $moIncProcDel->select();
          $moDelete = new CIIncidentProcess();
          while($moIncProcDel->fetch()){
            $moDelete->delete($moIncProcDel->getFieldValue('context_id'),true);
          }
      }
      close_popup();
    }elseif(!count($maValuesDefault)){
      $moHandler = new QueryGridIncidentProcessSearch(FWDWebLib::getConnection());
      $moHandler->setFilter(true);
      $moHandler->setId(FWDWebLib::getObject('var_id')->getValue());
      $maValues = $moHandler->getValues();
      if(count($maValues)){
        $msValues = implode(':',$maValues);
        $msTitle = FWDLanguage::getPHPStringValue('tt_save_suggested_incident_processes','Relacionar Processos Sugeridos');
        $msMessage = FWDLanguage::getPHPStringValue('st_save_suggested_incident_processes',"Voc� deseja relacionar os processos sugeridos ao  incidente <b>%context_name%</b>?");
        $moCtx = new CIIncident();
        $moCtx->fetchById(FWDWebLib::getObject('var_id')->getValue());
        $msMessage = str_replace("%context_name%",ISMSLib::truncateString($moCtx->getName(), 70),$msMessage);
        $msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
                      "soWindow = soPopUp.getOpener();" .
                      "soWindow.save_suggested_processes('{$msValues}');";
        $msEventDenied = "soPopUpManager.closePopUp('popup_incident_process_association_edit');";
        ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,50,$msEventDenied);
      }else{
        close_popup();
      }
    }else{
      close_popup();
    }
     
  }
}

class PopulateSearchGrid extends FWDRunnable {
  public function run() {
    FWDWebLib::getObject('grid_search')->execEventPopulate();
  }
}

class PopulateSelectedGrid extends FWDRunnable{
  public function run() {
    FWDWebLib::getObject('grid_selected')->execEventPopulate();
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new PopulateSearchGrid("populate_search_grid"));
    $moStartEvent->addAjaxEvent(new PopulateSelectedGrid("populate_selected_grid"));
    $moStartEvent->addAjaxEvent(new SaveEvent("save_event"));
    $moStartEvent->addAjaxEvent(new SaveSuggestedEvent("save_suggested_event"));
     
    $miCtxId = FWDWebLib::getObject("var_id")->getValue();
    
    $moGrid = FWDWebLib::getObject("grid_search");
    $moHandler = new QueryGridIncidentProcessSearch(FWDWebLib::getConnection());
    $moHandler->setName(FWDWebLib::getObject("var_filter_name")->getValue());
    $moHandler->setFilter(FWDWebLib::getObject("var_check_controller")->getValue()?true:false);
    $moHandler->setIdsNotIn(FWDWebLib::getObject('var_ctx_ids')->getValue());
    $moHandler->setId($miCtxId);
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new GridSearch());

    $moGridSelected = FWDWebLib::getObject("grid_selected");
    $moHandlerSelected = new QueryGridIncidentProcessSearch(FWDWebLib::getConnection());
    $moHandlerSelected->setName(FWDWebLib::getObject("var_filter_name")->getValue());
    $moHandlerSelected->setId($miCtxId);
    $moHandlerSelected->setIdsIn(FWDWebLib::getObject('var_ctx_ids')->getValue().':1');
    $moGridSelected->setQueryHandler($moHandlerSelected);
    $moGridSelected->setObjFwdDrawGrid(new GridSearch());
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    //teste para verificar se o sistema n�o est� sendo hakeado
    $moCtxUserTest = new RMRiskControl();
    $moCtxUserTest->testPermissionToInsert();
    
    $miCtxId = FWDWebLib::getObject("param_id")->getValue();
    FWDWebLib::getObject("var_id")->setValue($miCtxId);
    $moQuery = new QueryProcessesByIncidentId(FWDWebLib::getConnection());
    $moQuery->setIncidentId($miCtxId);
    $maCtxIds = $moQuery->getValues();
    FWDWebLib::getObject('var_ctx_ids')->setValue(implode(':',$maCtxIds));
    FWDWebLib::getObject('var_ids_original')->setValue(FWDWebLib::getObject('var_ctx_ids')->getValue());
    
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
?>
<script language="javascript">
  gebi('tx_filter_name').focus();
  
  function save_suggested_processes(psValues){
    gebi('var_ids_suggested').value=psValues;
    trigger_event('save_suggested_event',3);
  }
  function refresh_grids(){
     gobi('grid_search').setPopulate(true);
     gobi('grid_selected').setPopulate(true);
     trigger_event('populate_search_grid',3);
     trigger_event('populate_selected_grid',3);
  }
  function search(){
    gebi('var_filter_name').value = gebi('tx_filter_name').value;
    gebi('var_check_controller').value = gebi('check_controller').value;
    gobi('grid_search').setPopulate(true);
    trigger_event('populate_search_grid',3);
  }
  function select_grid_lines(){
     var moIds = gebi('var_ctx_ids');
     if(moIds.value)
         moIds.value = moIds.value + ':' + gobi('grid_search').getValue().join(':');
     else
         moIds.value = gobi('grid_search').getValue().join(':');
     refresh_grids();
  }
  function remove_selected(){
     var maNewCtx = new Array();
     var moNewCtx = gebi('var_ctx_ids');
     if(moNewCtx.value){
        var maCtx = moNewCtx.value.split(':');
        var maGridSelected = gobi('grid_selected').getValue();
        for(i=0;i < maCtx.length; i++){
           if(!in_array(maCtx[i], maGridSelected)){
              maNewCtx.push(maCtx[i]);
           }
        }
        moNewCtx.value = maNewCtx.join(':');
        refresh_grids();
      }
  }
  function enter_search_event(e)
  {
    if (!e) e = window.event;
    if(e['keyCode']==13){
      search();
    }
  }
  FWDEventManager.addEvent(gebi('tx_filter_name'), 'keydown', enter_search_event);
</script>
<?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_incident_process_association_edit.xml");
?>