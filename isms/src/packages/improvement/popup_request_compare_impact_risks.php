<?php
include_once "include.php";
include_once $handlers_ref . "QueryIncidentRiskParameter.php";

define('TASK_INCIDENT_RISK_METADATA',1);

class YesEvent extends FWDRunnable {
	public function run(){
		$moWebLib = FWDWebLib::getInstance();
		$moSession = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId());
		$newRiskValues = unserialize($moSession->getAttrUpdateRiskTree());
		//$miIncidentId = unserialize($moSession->getAttrIncidentId());
		$miIncidentId = $moSession->getAttrIncidentId();
		
		$miUserId = $moSession->getUserId();
        
		
		foreach($newRiskValues as $miRisk => $riskParameters) {
			$moRisk = new RMRisk();
        	$moRisk->fetchById($miRisk);
        	$miAssetId = $moRisk->getFieldValue('risk_asset_id');
        	$msRiskName = $moRisk->getFieldValue('risk_name');
        	$moAsset = new RMAsset();
        	$moAsset->fetchById($miAssetId);
        	$miAssetSecResponsible = $moAsset->getFieldValue('asset_security_responsible_id');
        	$msAssetName = $moAsset->getFieldValue('asset_name');
        	
        	if ($miAssetSecResponsible != $miUserId) { //Enviar e-mail se usu�rio logado != resp. seguran�a do ativo.
        		
    				
    			$moTask = new WKFTask();
  		        $moTask->setFieldValue('task_context_id',$miRisk);
          		$moTask->setFieldValue('task_receiver_id',$miAssetSecResponsible);
          		$moTask->setFieldValue('task_creator_id', $miUserId);
          		$moTask->setFieldValue('task_activity', ACT_NEW_RISK_PARAMETER_APPROVAL);
          		$moTask->setFieldValue('task_is_visible', 1);
          		$moTask->setFieldValue('task_email_sent',0);
          		$moTask->setFieldValue('task_date_created',ISMSLib::ISMSTime());
          		$moTaskId = $moTask->insert(true);	

          		$moTaskMetadata = new WKFTaskMetadata();
          		$moTaskMetadata->setFieldValue('task_id',$moTaskId);
          		$moTaskMetadata->setFieldValue('metadata_context_id',$miIncidentId);
          		$moTaskMetadata->setFieldValue('metadata_type',TASK_INCIDENT_RISK_METADATA);
          		$moTaskMetadata->insert();
          		
          		
//          		$moUser = new ISMSUser();
//      		    $moUser->fetchById($miAssetSecResponsible);
          
	        } else {
	        	foreach($riskParameters as $parameterId => $valueId) {
	        		$moRiskValue = new RMRiskValue();
    	    		$moRiskValue->createElementIdFilter($miRisk);
    	    		$moRiskValue->createParameterIdFilter($parameterId);
    	    		$moRiskValue->setValueId($valueId);
    	    		$moRiskValue->update();
    	    		
    	    		
	        	}
	        	
        	
    	    }
		}
		
		echo "soPopUpManager.closePopUp('popup_incident_risk_parametrization'); soPopUpManager.closePopUp('popup_request_compare_impact_risks'); ";
		
		
	}
};


class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new YesEvent("YesEvent"));
    
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
/*    $msEvent = "soPopUpManager.getRootWindow().location = '".$moWebLib->getSysRefBasedOnTabMain()."login.php';";
    $moEvent = new FWDClientEvent();
    $moEvent->setAttrEvent('onClick');
    $moEvent->setAttrValue($msEvent);
    FWDWebLib::getObject("vb_ok")->addObjFWDEvent($moEvent);
    $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    $moSession->logout();
   
    echo "<script language='javascript'>soPopUpManager.getPopUpById('popup_no_access').setCloseEvent( function close_popup_no_access() { ".$msEvent." } );</script>";
  */
    
    $moWebLib->dump_html(FWDWebLib::getObject('dialog'));  
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_request_compare_impact_risks.xml");

?>