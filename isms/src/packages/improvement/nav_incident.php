<?php
include_once "include.php";
include_once $handlers_ref . "grid/improvement/QueryGridIncident.php";

class IncidentDrawGrid extends FWDDrawGrid {
  public function drawItem(){
    switch($this->ciColumnIndex){
      case $this->getIndexByAlias('incident_id'):

        $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
        $maMenuACLs = array('edit','remove','disciplinary_process','risk','process','send_to_responsible','send_to_app_disposal','send_to_app_solution','finish','approve','simple_approve','send_to_approval', 'view_loss_type', 'view_immediate_disposal', 'view_evidence', 'view_solution');
        $maAllowed = array();
        $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();

        $mbIsIncidentManager = ($miUserId == ISMSConfig::getConfig(USER_INCIDENT_MANAGER));
        $mbIsResponsible = ($miUserId == $this->getFieldValue('incident_responsible_id'));
        $miState = $this->getFieldValue('incident_state');

        $mbResponsibleIsSet = (bool)$this->getFieldValue('incident_responsible_id');
        $mbDisposalIsSet = (bool)trim($this->getFieldValue('incident_disposal_description'));
        $mbSolutionIsSet = (bool)trim($this->getFieldValue('incident_solution_description'));
        $mbDateLimitIsSet = (bool)$this->getFieldValue('incident_date_limit');
        $mbDateFinishIsSet = (bool)$this->getFieldValue('incident_date_finish');

        if($mbIsIncidentManager){
          if($miState!=CONTEXT_STATE_INC_SOLVED){
            $maAllowed[] = 'edit';
            $maAllowed[] = 'disciplinary_process';
            $maAllowed[] = 'process';
          } else if($miState == CONTEXT_STATE_INC_SOLVED){
            $maAllowed[] = 'view_loss_type';
            $maAllowed[] = 'view_immediate_disposal';
            $maAllowed[] = 'view_evidence';
            $maAllowed[] = 'view_solution';
          }
          $maAllowed[] = 'risk';
          $maAllowed[] = 'remove';
        }else{
          if(!in_array('M.CI.2.4',$maACLs)){
            $maAllowed[] = 'remove';
          }
          if(($miState!=CONTEXT_STATE_INC_SOLVED)&&(!in_array('M.CI.2.3',$maACLs))){
            //se possue permiss�o para editar sem restri��o
            $maAllowed[] = 'edit';
          }elseif($mbIsResponsible){
            if($miState==CONTEXT_STATE_INC_DIRECTED || $miState==CONTEXT_STATE_INC_WAITING_SOLUTION){
              $maAllowed[] = 'edit';
            }
          }
          
          if($miState == CONTEXT_STATE_INC_SOLVED){
            $maAllowed[] = 'view_loss_type';
            $maAllowed[] = 'view_immediate_disposal';
            $maAllowed[] = 'view_evidence';          	
            $maAllowed[] = 'view_solution';
          }          
        }
        //Copiei as instru��es abaixo de CIIncident (getStateForwardAction), mantendo apenas os 'confirm' e substituindo por $miAction;
        $miAction=0;
        if( ($miState == CONTEXT_STATE_DENIED) && $mbIsResponsible){
          //send_to_approval
          $miAction = WKF_ACTION_SEND_TO_APPROVAL;
        }
        if( ($miState == CONTEXT_STATE_PENDANT) && $mbIsIncidentManager){
          $miAction = ACT_INC_APPROVAL;
        }
        if($miState==CONTEXT_STATE_INC_OPEN){
          if($mbIsResponsible) {
            if ($mbDisposalIsSet && $mbDateLimitIsSet && $mbSolutionIsSet && $mbDateFinishIsSet){
              $miAction = WKF_ACTION_INC_SEND_TO_APP_SOLUTION;
            }
          }elseif($mbResponsibleIsSet && $mbDateLimitIsSet && $mbIsIncidentManager){
            $miAction = WKF_ACTION_INC_SEND_TO_RESPONSIBLE;
          }
        }
        if($miState==CONTEXT_STATE_INC_DIRECTED){
          if($mbDisposalIsSet && $mbDateLimitIsSet){
            if($mbIsIncidentManager){
              if($mbSolutionIsSet && $mbDateFinishIsSet){
                $miAction = WKF_ACTION_INC_SEND_TO_APP_SOLUTION;
              }
            }else{
              $miAction = WKF_ACTION_INC_SEND_TO_APP_DISPOSAL;
            }
          }
        }
        if($miState==CONTEXT_STATE_INC_WAITING_SOLUTION){
          if($mbSolutionIsSet && $mbDateFinishIsSet){
            $miAction = WKF_ACTION_INC_SEND_TO_APP_SOLUTION;
          }
        }
        switch($miAction) {
          case WKF_ACTION_INC_SEND_TO_RESPONSIBLE:
            $maAllowed[] = 'send_to_responsible';
            break;
          case WKF_ACTION_INC_SEND_TO_APP_DISPOSAL:
            $maAllowed[] = 'send_to_app_disposal';
            break;
          case WKF_ACTION_SEND_TO_APPROVAL:
            $maAllowed[] = 'send_to_approval';
            break;
          case WKF_ACTION_INC_SEND_TO_APP_SOLUTION:
            if ($mbIsIncidentManager)
              $maAllowed[] = 'finish';
            else{
              if($mbIsResponsible)
                $maAllowed[] = 'send_to_app_solution';
            }
            break;
          default:break;
        }

        if ($this->getFieldValue('task_receiver')==$miUserId) {
          if($miAction == ACT_INC_APPROVAL){
            $maAllowed[] = 'simple_approve';
          }else{
            $maAllowed[] = 'approve';
          }
        }

        $moACL = FWDACLSecurity::getInstance();
        $moACL->setNotAllowed(array_diff($maMenuACLs,$maAllowed));
        $moMenu = FWDWebLib::getObject('menu');
        $moGrid = FWDWebLib::getObject('grid_incident');
        $msLine = $moGrid->getAttrName().'_acl_'.$this->ciRowIndex;
        FWDGridMenuACLSecurity::getInstance()->installGridMenuACL($moMenu,$moACL,$moGrid,$msLine);

        $this->coCellBox->setObjFWDToolTip(ISMSLib::getInstance()->getToolTipCreateModify($this->getFieldValue('user_create_name'),$this->getFieldValue('date_create'),$this->getFieldValue('user_edit_name'),$this->getFieldValue('date_edit')));
        $this->coCellBox->setIconSrc("icon-ci_incident.gif");
        
        $moVariable = new FWDVariable();
        $moVariable->setAttrNoEscape('false'); 
        $moVariable->setAttrName('task_id_'.$this->getFieldValue('incident_id'));
        $moVariable->setValue($this->getFieldValue('task_id'));
        
        return $this->coCellBox->draw().$moVariable->drawBody();
        break;
        
      case $this->getIndexByAlias('incident_name'):
        $this->coCellBox->setAttrStringNoEscape("true");
        if( $this->getFieldValue('incident_state') != CONTEXT_STATE_INC_SOLVED 
            && (
                (ISMSConfig::getConfig(USER_INCIDENT_MANAGER) == ISMSLib::getCurrentUserId())
                ||
                ISMSLib::userHasACL('M.CI.2.3')
                ||
                (
                  ISMSLib::getCurrentUserId() == $this->getFieldValue('incident_responsible_id') 
                  && 
                    (
                      $this->getFieldValue('incident_state') == CONTEXT_STATE_INC_DIRECTED
                      ||
                      $this->getFieldValue('incident_state') == CONTEXT_STATE_INC_WAITING_SOLUTION
                    )
                )
              )
          ){
          $this->coCellBox->setValue("<a href='javascript:open_edit(".$this->getFieldValue('incident_id').");'>".$this->coCellBox->getValue()."</a>");
        }else{
          $this->coCellBox->setValue("<a href='javascript:open_visualize(".$this->getFieldValue('incident_id').",".CONTEXT_CI_INCIDENT.",\"".uniqid()."\");'>".$this->coCellBox->getValue()."</a>");
        }
        return $this->coCellBox->draw();
      break;
      
      case $this->getIndexByAlias('incident_category'):
        if (!$this->coCellBox->getValue())
          $this->coCellBox->setValue(FWDLanguage::getPHPStringValue('mx_no_category','Sem Categoria'));
        return parent::drawItem();
        break;
        
      case $this->getIndexByAlias('incident_loss_type'):
        if ($this->coCellBox->getValue()==INCIDENT_LOSS_TYPE_DIRECT)
          $this->coCellBox->setValue(FWDLanguage::getPHPStringValue('mx_direct','Direta'));
        else
          $this->coCellBox->setValue(FWDLanguage::getPHPStringValue('mx_indirect','Indireta'));
        return parent::drawItem();
        break;
        
      case $this->getIndexByAlias('incident_date_limit'):
        if ($this->coCellBox->getValue())
          $this->coCellBox->setValue(ISMSLib::getISMSShortDate($this->coCellBox->getValue()));
        return parent::drawItem();
        break;

      case $this->getIndexByAlias('risk_count'):
        $this->coCellBox->setAttrStringNoEscape("true");
        if(ISMSLib::userHasACL('M.RM') && ISMSLib::userHasACL('M.RM.4')){
          $this->coCellBox->setValue("<a href='javascript:go_to_nav_risk(".$this->getFieldValue('incident_id').");'>".$this->coCellBox->getValue()."</a>");
        }
        return parent::drawItem();
        break;
        
      case $this->getIndexByAlias('incident_state'):
        $this->coCellBox->setValue(ISMSContextObject::getContextStateAsString($this->coCellBox->getValue()));
        return parent::drawItem();
        break;
        
      case $this->getIndexByAlias('incident_id_show'):
          $this->coCellBox->setValue($this->getFieldValue('incident_id'));
        return parent::drawItem();
        break;        
        
      default:
        return parent::drawItem();
    }
  }
}

class IncidentConfirmRemove extends FWDRunnable {
  public function run() {
    $msTitle = FWDLanguage::getPHPStringValue('tt_incident_remove_confirm','Remover Incidente');
    $msMessage = FWDLanguage::getPHPStringValue('st_incident_remove_confirm',"Voc� tem certeza que deseja remover o incidente <b>%incident_name%</b>?");

    $miId = FWDWebLib::getObject('selected_incident_id')->getValue();
    $moIncident = new CIIncident();
    $moIncident->fetchById($miId);
    $msIncidentName = ISMSLib::truncateString($moIncident->getName(), 70);    
    $msMessage = str_replace('%incident_name%',$msIncidentName,$msMessage);

    $msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
                    "soWindow = soPopUp.getOpener();" .
                    "soWindow.remove();";
    ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,50);
  }
}

class RemoveIncidentEvent extends FWDRunnable {
  public function run() {
    $miIncidentId = intval(FWDWebLib::getObject('selected_incident_id')->getValue());
    $moCtxUserTest = new CIIncident();
    $moCtxUserTest->testPermissionToDelete($miIncidentId);

    $moIncident = new CIIncident();
    $moIncident->delete($miIncidentId);
  }
}

class FilterEvent extends FWDRunnable {
  public function run() {
    $moGrid = FWDWebLib::getObject('grid_incident');
    $moGrid->execEventPopulate();
  }
}

class StateForwardEvent extends FWDRunnable {
  public function run() {
    $miIncidentId = FWDWebLib::getObject('selected_incident_id')->getValue();
    $moIncident = new CIIncident();
    $moIncident->getStateForwardConfirmation($miIncidentId);
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new FilterEvent('filter_event'));
    $moStartEvent->addAjaxEvent(new StateForwardEvent('state_forward_event'));
    $moStartEvent->addAjaxEvent(new RemoveIncidentEvent('remove_event'));
    $moStartEvent->addAjaxEvent(new IncidentConfirmRemove('confirm_remove'));


    //instala a seguran�a de ACL na p�gina
    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
    FWDMenuACLSecurity::getInstance()->installMenuACL(FWDWebLib::getObject('menu'),FWDACLSecurity::getInstance());

    $moGrid = FWDWebLib::getObject("grid_incident");
    $moHandler = new QueryGridIncident(FWDWebLib::getConnection());
    $moHandler->setStatusFilter(FWDWebLib::getObject('filter_status')->getValue());
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new IncidentDrawGrid('grid_incident'));
  }
}


class ScreenEvent extends FWDRunnable {
  public function run(){
    $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();

    if((ISMSLib::getConfigById(USER_INCIDENT_MANAGER) != $miUserId)&&(in_array('M.CI.2.2',$maACLs))){
      FWDWebLib::getObject('insert')->setShouldDraw(false);
    }

    $maIncStates = array( CONTEXT_STATE_PENDANT,
                          CONTEXT_STATE_INC_OPEN,
                          CONTEXT_STATE_INC_DIRECTED,
                          CONTEXT_STATE_INC_PENDANT_DISPOSAL,
                          CONTEXT_STATE_INC_WAITING_SOLUTION,
                          CONTEXT_STATE_INC_PENDANT_SOLUTION,
                          CONTEXT_STATE_INC_SOLVED,
                          CONTEXT_STATE_DENIED);

    $maFilterSelect = FWDWebLib::getObject('select_filter_status');
    foreach($maIncStates as $miItem){
      $moItem = new FWDItem();
      $moItem->setAttrKey($miItem);
      $moItem->setValue(ISMSContextObject::getContextStateAsString($miItem));
      $maFilterSelect->addObjFWDItem($moItem);
    }


    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        function refresh_grid(){
          js_refresh_grid('grid_incident');
        }
        function refresh_grids(){
          js_refresh_grid('grid_incident');
        }
       function remove() {
          trigger_event('remove_event',3);
          refresh_grid();
        }

        function open_edit(piId){
          isms_open_popup('popup_incident_edit','packages/improvement/popup_incident_edit.php?incident='+piId,'','true');
        }
        /*pog avan�ado para redirecionar para a tab de riscos filtrando pelo incidente relacionado ao risco*/
        function go_to_nav_risk(piId){
          setTimeout(function(){parent.parent.soTabSubManager.changeTab(2,'packages/risk/tab_risk_management.php?context_type=2831&context_id='+piId);},1);
        }

      </script>
    <?
  }
}
//testa se a licen�a utilizada permite acessar o m�dulo de incidente
ISMSLib::testUserPermissionToIncidentMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_incident.xml");

?>