<?php
include_once "include.php";
include_once $handlers_ref . "grid/improvement/QueryGridOccurrence.php";

class OccurrenceDrawGrid extends FWDDrawGrid {
  public function drawItem(){
    switch($this->ciColumnIndex){
      case $this->getIndexByAlias('occurrence_id'):
        $miUserId = ISMSLib::getCurrentUserId();

        $miOccurrenceId = $this->coCellBox->getValue();
        $miState = $this->getFieldValue('occurrence_state');
        $moOccurrence = new CIOccurrence();
        $moOccurrence->fetchById($miOccurrenceId);
        $maMenuACLs = array('edit','delete','approve','assoc_inc');
        $maAllowed = array();
        if (($miUserId == ISMSLib::getConfigById(USER_INCIDENT_MANAGER))) { //usuario eh gestor de incidentes
          $maAllowed[] = 'delete';
          $maAllowed[] = 'edit';
        }else{
          if(ISMSLib::userHasACL('M.CI.1.4')){
            //usu�rio tem permiss�o de deletar sem restri��o
            $maAllowed[] = 'delete';
          }
          if( ( $miState==CONTEXT_STATE_PENDANT && $miUserId==$moOccurrence->getCreator() ) || ISMSLib::userHasACL('M.CI.1.3') ) { 
            //O criador s� pode editar enquanto o estado for 'Pendente' ou o usu�rio tem permiss�o de editar sem restri��o
            $maAllowed[] = 'edit';
          }
        }
        /*se a ocorr�ncia n�o tiver um incidente associado a ela abilita o menu para associar incidente*/
        if(!$this->getFieldValue('incident_id') && $miState==CONTEXT_STATE_PENDANT && ( $miUserId==$moOccurrence->getCreator() || ISMSLib::userHasACL('M.CI.1.3') || $miUserId==ISMSLib::getConfigById(USER_INCIDENT_MANAGER)) ){
          $maAllowed[] = 'assoc_inc';
        }
        
        if ($this->getFieldValue('task_receiver')==$miUserId) {
          $maAllowed[] = 'approve';
        }
        
        
        $moACL = FWDACLSecurity::getInstance();
        $moACL->setNotAllowed(array_diff($maMenuACLs,$maAllowed));
        $moMenu = FWDWebLib::getObject('menu');
        $moGrid = FWDWebLib::getObject('grid_occurrence');
        $msLine = $moGrid->getAttrName().'_acl_'.$this->ciRowIndex;
        FWDGridMenuACLSecurity::getInstance()->installGridMenuACL($moMenu,$moACL,$moGrid,$msLine);
        
        $this->coCellBox->setObjFWDToolTip(ISMSLib::getInstance()->getToolTipCreateModify($this->getFieldValue('user_create_name'),$this->getFieldValue('date_create'),$this->getFieldValue('user_edit_name'),$this->getFieldValue('date_edit')));
        $this->coCellBox->setIconSrc("icon-ci_occurrence.gif");

        $moVariable = new FWDVariable();
        $moVariable->setAttrNoEscape('false'); 
        $moVariable->setAttrName('task_id_'.$this->getFieldValue('occurrence_id'));
        $moVariable->setValue($this->getFieldValue('task_id'));

        return $this->coCellBox->draw().$moVariable->drawBody();
      
      case $this->getIndexByAlias('occurrence_date'):
        if ($this->coCellBox->getValue())
          $this->coCellBox->setValue(ISMSLib::getISMSDate($this->coCellBox->getValue()));
        return parent::drawItem();
        break;
        
      case $this->getIndexByAlias('occurrence_id_show'):
          $this->coCellBox->setValue($this->getFieldValue('occurrence_id'));
        return parent::drawItem();
        break;

      case $this->getIndexByAlias('occurrence_description'):
        if( ISMSLib::getCurrentUserId() == ISMSLib::getConfigById(USER_INCIDENT_MANAGER )
            || ( $this->getFieldValue('occurrence_state')==CONTEXT_STATE_PENDANT && ISMSLib::getCurrentUserId()==$this->getFieldValue('user_create_id') )
            || ISMSLib::userHasACL('M.CI.1.3')
          ){
            $this->coCellBox->setAttrStringNoEscape("true");
            $this->coCellBox->setValue("<a href='javascript:open_edit(".$this->getFieldValue('occurrence_id').");'>".$this->coCellBox->getValue()."</a>");
          }
        return $this->coCellBox->draw();
        break;
        
      case $this->getIndexByAlias('incident_name'):
        $this->coCellBox->setAttrStringNoEscape("true");
        if( $this->getFieldValue('incident_state') != CONTEXT_STATE_INC_SOLVED 
            && (
                (ISMSConfig::getConfig(USER_INCIDENT_MANAGER) == ISMSLib::getCurrentUserId())
                ||
                ISMSLib::userHasACL('M.CI.2.3')
                ||
                (
                  ISMSLib::getCurrentUserId() == $this->getFieldValue('incident_responsible_id') 
                  &&
                    (
                      $this->getFieldValue('incident_state') == CONTEXT_STATE_INC_DIRECTED
                      ||
                      $this->getFieldValue('incident_state') == CONTEXT_STATE_INC_WAITING_SOLUTION
                    )
                )
              )
          ){
          $this->coCellBox->setValue("<a href='javascript:open_incident_edit(".$this->getFieldValue('incident_id').");'>".$this->coCellBox->getValue()."</a>");
        }else{
          $this->coCellBox->setValue("<a href='javascript:open_visualize(".$this->getFieldValue('incident_id').",".CONTEXT_CI_INCIDENT.",\"".uniqid()."\");'>".$this->coCellBox->getValue()."</a>");
        }
        return $this->coCellBox->draw();
      break;

      case $this->getIndexByAlias('occurrence_state'):
        $this->coCellBox->setValue(ISMSContextObject::getContextStateAsString($this->coCellBox->getValue()));
        return $this->coCellBox->draw(); 
        break;
        
      default:
        return parent::drawItem();
    }
  }
}

class OccurrenceConfirmRemove extends FWDRunnable {
  public function run() {
    $msTitle = FWDLanguage::getPHPStringValue('tt_occurrence_remove','Remover Ocorr�ncia');
    $msMessage = FWDLanguage::getPHPStringValue('st_occurrence_remove',"Voc� tem certeza de que deseja remover a ocorr�ncia <b>%occurrence_text%</b>?");
    
    $moOccurrence = new CIOccurrence();
    $miOccurrenceId = intval(FWDWebLib::getObject('selected_occurrence_id')->getValue());
    $moOccurrence->fetchById($miOccurrenceId);
    $msOccurrence = ISMSLib::truncateString($moOccurrence->getFieldValue('occurrence_description'),70);
    $msMessage = mb_ereg_replace('%occurrence_text%',$msOccurrence,$msMessage);
    
    $msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
                    "soWindow = soPopUp.getOpener();" .
                    "soWindow.remove_occurrence();";
    
    ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,50);
  }
}

class AssocIncEvent extends FWDRunnable {
  public function run() {
    $miOccurrenceId = intval(FWDWebLib::getObject('selected_occurrence_id')->getValue());

    $moCtxUserTest = new CIOccurrence();
    $moCtxUserTest->testPermissionToEdit($miOccurrenceId);

    $moOccurrence = new CIOccurrence();
    $moOccurrence->setFieldValue('incident_id', FWDWebLib::getObject('selected_incidente_id')->getValue());
    $moOccurrence->update($miOccurrenceId);
    echo "gebi('selected_incidente_id').value = 0; refresh_grid();";
  }
}

class RemoveOccurrenceEvent extends FWDRunnable {
  public function run() {
    $miOccurrenceId = intval(FWDWebLib::getObject('selected_occurrence_id')->getValue());

    $moCtxUserTest = new CIOccurrence();
    $moCtxUserTest->testPermissionToDelete($miOccurrenceId);

    $moOccurrence = new CIOccurrence();
    if ($moOccurrence->isDeletable($miOccurrenceId)) {
      $moOccurrence->delete($miOccurrenceId,true);
      echo "refresh_grid();";
    } else {
      $moOccurrence->showDeleteError();
    }
    
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new OccurrenceConfirmRemove('occurrence_confirm_remove'));
    $moStartEvent->addAjaxEvent(new RemoveOccurrenceEvent('remove_occurrence_event'));
    $moStartEvent->addAjaxEvent(new AssocIncEvent('assoc_inc_event'));

    //instala a seguran�a de ACL na p�gina
    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
    FWDMenuACLSecurity::getInstance()->installMenuACL(FWDWebLib::getObject('menu'),FWDACLSecurity::getInstance());
    
    $moGrid = FWDWebLib::getObject("grid_occurrence");
    $moHandler = new QueryGridOccurrence(FWDWebLib::getConnection());
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new OccurrenceDrawGrid('grid_occurrence'));
  }
}


class ScreenEvent extends FWDRunnable {
  public function run(){
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        function refresh_grid() {
          js_refresh_grid('grid_occurrence');
        }
        function refresh_grids() { //tive que colocar esse metodo tb, por causa da chamada da tela de aprovacao de ocorrencia
          js_refresh_grid('grid_occurrence');
        }
        function remove_occurrence() {
          trigger_event('remove_occurrence_event',3);
          refresh_grid();
        }
        function open_edit(piId){
          isms_open_popup('popup_occurrence_edit','packages/improvement/popup_occurrence_edit.php?occurrence='+piId,'','true');
        }
        
        function open_incident_edit(piId){
          isms_open_popup('popup_incident_edit','packages/improvement/popup_incident_edit.php?incident='+piId,'','true');
        }
        
        function set_incident(piId){
          gebi('selected_incidente_id').value = piId;
          trigger_event('assoc_inc_event',3);
        }
      </script>
    <?
  }
}
//testa se a licen�a utilizada permite acessar o m�dulo de incidente
ISMSLib::testUserPermissionToIncidentMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_occurrence.xml");

?>