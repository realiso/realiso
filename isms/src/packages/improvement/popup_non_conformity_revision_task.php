<?php
include_once "include.php";

class SaveEvent extends FWDRunnable {
	public function run() {
		$miTaskId = FWDWebLib::getObject('task_id')->getValue();
		$moTask = new WKFTask();
		$moTask->fetchById($miTaskId);

		if($moTask->getFieldValue('task_is_visible')){

			$moTask->testPermissionToExecute($miTaskId);

			$miNCId = FWDWebLib::getObject('nc_id')->getValue();
			$moNonConformity = new CINonConformity();
			$miIsEfficient = trim(FWDWebLib::getObject('efficiencyController')->getValue(),':');
			$moNonConformity->setFieldValue('nc_is_efficient',$miIsEfficient?true:false);
			$moNonConformity->setFieldValue('nc_date_efficiency_measured',ISMSLib::ISMSTime());

			$moCtxUserTest = new CINonConformity();
			$moCtxUserTest->testPermissionToEdit($miNCId);
			$moNonConformity->update($miNCId);

			$moNonConformity->stateForward(WKF_ACTION_NONE);

			$moTask = new WKFTask();
			$moTask->setFieldValue('task_date_accomplished',ISMSLib::ISMSTime());
			$moTask->setFieldValue('task_is_visible',false);
			$moTask->update($miTaskId);
			echo "soWindow = soPopUpManager.getPopUpById('popup_non_conformity_revision_task').getOpener();"
			."if (soWindow.refresh_grids)"
			."  soWindow.refresh_grids();"
			."soPopUpManager.closePopUp('popup_non_conformity_revision_task');";

		}
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));
		$moStartEvent->addAjaxEvent(new SaveEvent("save_event"));
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$miNCId = FWDWebLib::getObject('nc_id')->getValue();
		$miTaskId = FWDWebLib::getObject('task_id')->getValue();

		$moTask = new WKFTask();
		$moTask->fetchById($miTaskId);

		if($moTask->getFieldValue('task_is_visible')){

			$moTask->testPermissionToExecute($miTaskId);

			$moNonConformity = new CINonConformity();
			$moNonConformity->fetchById($miNCId);

			FWDWebLib::getObject('name')->setValue($moNonConformity->getFieldValue('nc_name'));
			FWDWebLib::getObject('description')->setValue($moNonConformity->getFieldValue('nc_description'));
			FWDWebLib::getObject('action_plan')->setValue($moNonConformity->getFieldValue('nc_action_plan'));

			$msToolTipText = trim($moNonConformity->getFieldValue('nc_name'));
			if($msToolTipText!=''){
				$moToolTip = new FWDToolTip();
				$moToolTip->setAttrShowDelay(1000);
				$moToolTip->setValue($msToolTipText);
				FWDWebLib::getObject('name')->addObjFWDEvent($moToolTip);
			}

			//instala a seguran�a de ACL na p�gina
			FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
			FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));

		}
	}
}
FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_non_conformity_revision_task.xml");
?>