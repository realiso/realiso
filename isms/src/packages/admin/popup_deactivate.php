<?php

include_once 'include.php';

class DeactivateAccountEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $moISMSSaaS = new ISMSSaaS();
    
    $msMessage = FWDLanguage::getPHPStringValue('em_deactivate_account_email_message',"Foi feita uma solicita��o de desativa��o de conta pelo cliente '%client%'.<br><b>Motivo:</b><br>%deactivation_reason%");
    $msMessage = str_replace(
      array('%client%','%deactivation_reason%'),
      array($moISMSSaaS->getConfig(ISMSSaaS::LICENSE_CLIENT_NAME),FWDWebLib::getObject('deactivation_reason')->getValue()),
      $msMessage
    );
    
    // Envia o email pra Axur
    ini_set('SMTP',ISMSLib::getConfigById(GENERAL_SMTP_SERVER));
    $msHost = ISMSLib::getConfigById(GENERAL_SMTP_SERVER);
    if ($msHost != 'localhost' && $msHost != '127.0.0.1'){
    	$msUser = ISMSLib::getConfigById(EXTERNAL_MAIL_USER);
    	$msPassword = ISMSLib::getConfigById(EXTERNAL_MAIL_PASSWORD);
    	$miPort = ISMSLib::getConfigById(EXTERNAL_MAIL_PORT);
    	$msEnc = ISMSLib::getConfigById(EXTERNAL_MAIL_ENCRYPTION);
    	$moEmail = new FWDEmail(true, $msHost, $miPort, $msUser, $msPassword, $msEnc);
    } else {
    	$moEmail = new FWDEmail();
    }
    $moEmail->setTo($moISMSSaaS->getConfig(ISMSSaaS::SAAS_REPORT_EMAIL));
    $moEmail->setFrom(ISMSLib::getConfigById(GENERAL_SMTP_DEFAULT_MAIL_SENDER));
    $moEmail->setSenderName(ISMSLib::getConfigById(GENERAL_SMTP_DEFAULT_MAIL_SENDER_NAME));
    $moEmail->setSubject(FWDLanguage::getPHPStringValue('em_deactivate_account',"Desativa��o de Conta"));
    $moEmail->setMessage($msMessage);
    $moEmail->send();
    
    // Destr�i a sess�o de todos usu�rios
    ISMSLib::evacuateSystem();
    $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->destroySession();
    
    // Bloqueia a inst�ncia
    $moISMSSaaS = new ISMSSaaS();
    $moISMSSaaS->setConfig(ISMSSaaS::SYSTEM_IS_ACTIVE,0);
    
    echo "soPopUpManager.getRootWindow().location = '{$moWebLib->getSysRefBasedOnTabMain()}login.php';";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
    $moStartEvent->addAjaxEvent(new DeactivateAccountEvent('deactivate_account'));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_deactivate.xml');

?>