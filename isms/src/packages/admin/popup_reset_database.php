<?php

include_once 'include.php';
include_once $handlers_ref . "QueryCleanDataBase.php";

/**
* Classe que abre um popup solicitando a confirma��o da exclus�o de todos os dados do sistema.
*/
class OpenResetDatabaseConfirmPopup extends FWDRunnable {
  public function run(){
    $msTitle = FWDLanguage::getPHPStringValue('st_database_permanent_removal','Remo��o permanente da Base da dados');
    $msMessage = FWDLanguage::getPHPStringValue('st_question_database_permanent_removal',"TODOS os dados ser�o apagados! Deseja continuar?");

    $msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
                    "soWindow = soPopUp.getOpener();" .
                    "soWindow.remove();";

    ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,60);
  }
}

class ResetDatabase extends FWDRunnable {
  public function run(){

    $cleanUpData = $this->CleanupUsersTasksWarningsAuditFromDataBase();
    $addConfirmation = $this->AddConfirmationInISMSConfig();

    // FALTA REALIZAR OS TRATAMENTOS DE ERRO.
    if($cleanUpData == 1 && $addConfirmation == 1){
        echo "  gobi('st_database_removed').show(); ";
        echo "  gobi('tt_reset_database_question').hide(); ";
        echo "  gobi('bt_reset_database').hide(); ";
    }
  }

  private function CleanupUsersTasksWarningsAuditFromDataBase(){
    $moCleanDataBaseQuery = new QueryCleanDataBase(FWDWebLib::getConnection());
    $moCleanDataBaseQuery->makeQuery();
    $moCleanDataBaseQuery->executeQuery();

    // FALTA IMPLEMENTAR ERROR
    return 1;
  }

  private function AddConfirmationInISMSConfig(){
    $moConfig = new ISMSConfig();
    $date = date('Y-m-d H:i:s');
    $moConfig->setConfig(RDB_RESET_DATA_BASE, 1);
    $moConfig->setConfig(RDB_DATE_RESET_DATA_BASE, $date);

    // FALTA IMPLEMENTAR ERROR
    return 1;
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
    $moStartEvent->addAjaxEvent(new OpenResetDatabaseConfirmPopup("open_reset_database_confirm_popup_event"));
    $moStartEvent->addAjaxEvent(new ResetDatabase("reset_database_event"));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));

    ?>
      <script language="javascript">
        function remove() { trigger_event('reset_database_event','3'); }
      </script>
    <?

  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_reset_database.xml');
?>
