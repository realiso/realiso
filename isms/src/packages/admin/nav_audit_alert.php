<?php
include_once "include.php";
include_once $handlers_ref . "grid/QueryGridAuditAlert.php";
include_once $handlers_ref . "select/QuerySelectAuditAlertCreator.php";
include_once $handlers_ref . "select/QuerySelectAuditAlertReceiver.php";
include_once $handlers_ref . "QueryGetReportClassification.php";

class FilterAuditAlertEvent extends FWDRunnable {
	public function run() {
		$moGrid = FWDWebLib::getObject('grid_audit_alert');
		$moGrid->execEventPopulate();	
	}
}

class GridAuditAlert extends FWDDrawGrid {
  public function drawItem(){    
    switch($this->ciColumnIndex){
      case 1:
      	if ($this->coCellBox->getValue())
      		$this->coCellBox->setValue(ISMSLib::getISMSDate($this->coCellBox->getValue()));     		
      	return $this->coCellBox->draw(); 
      break;
      
      case 4:
      	$moAlert = new WKFAlert();      	
      	$this->coCellBox->setValue($moAlert->getDescription($this->caData[6], $this->caData[7], $this->caData[8], $this->caData[9]));
      	return $this->coCellBox->draw();
      break;
            
      default:
        return parent::drawItem();
      break;
    }
  }
}

class ISMSReportEvent extends FWDRunnable {
	
	private function getFilter() {
		$moReportFilter = new ISMSReportAuditAlertFilter();
		
		$moReportFilter->setDateStart(FWDWebLib::getObject("date_start")->getTimestamp());
		$moReportFilter->setDateFinish(FWDWebLib::getObject("date_end")->getTimestamp());
		$moReportFilter->setCreator(FWDWebLib::getObject("filter_creator")->getValue());
		$moReportFilter->setReceiver(FWDWebLib::getObject("filter_receiver")->getValue());		
		
		$moReportFilter->setManualClassification(FWDWebLib::getObject('select_report_classification')->getValue());
	  $moReportFilter->setFileType(FWDWebLib::getObject('select_report_type')->getValue());
    
    FWDWebLib::getObject('report_comment')->getObjFWDString()->setAttrNoEscape('true');
    
	  $moReportFilter->setComment(FWDWebLib::convertToISO(FWDWebLib::getObject('report_comment')->getValue(), true));
	  
	  return $moReportFilter;
	}
	
  public function run(){  	
  	set_time_limit(3000);   

    $mbDontForceDownload = (FWDWebLib::getObject("select_report_type")->getValue()==REPORT_FILETYPE_HTML);

    $moWindowReport = new ISMSReportWindow(ADMIN_MODE);
    if($mbDontForceDownload) $moWindowReport->open();

    $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    $moSession->deleteAttribute("filter");
    $moSession->addAttribute("filter");    
    $moSession->setAttrFilter($this->getFilter());
    
    $msReport = "report_audit_alert";
    	
    if($mbDontForceDownload){
      $moWindowReport->setWaitLoadingComplete(true);	      
      $moWindowReport->loadReport($msReport);
    }
    else $moWindowReport->forceReportDownload($msReport);  	  	
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));   
    $moStartEvent->addAjaxEvent(new FilterAuditAlertEvent("filter_audit_alert"));
    $moStartEvent->addAjaxEvent(new ISMSReportEvent("function_report"));
    
    $moGrid = FWDWebLib::getObject("grid_audit_alert");
    $moGridHandler = new QueryGridAuditAlert(FWDWebLib::getConnection());
    $moGridHandler->setDateStart(FWDWebLib::getObject("date_start")->getTimestamp());
		$moGridHandler->setDateEnd(FWDWebLib::getObject("date_end")->getTimestamp());
		$moGridHandler->setCreator(FWDWebLib::getObject("filter_creator")->getValue());
		$moGridHandler->setReceiver(FWDWebLib::getObject("filter_receiver")->getValue());
		$moGrid->setQueryHandler($moGridHandler);
    $moGrid->setObjFwdDrawGrid(new GridAuditAlert());
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
  	$moHandler = new QuerySelectAuditAlertCreator(FWDWebLib::getConnection());    
    $moSelect = FWDWebLib::getObject('select_filter_creator');
    $moSelect->setQueryHandler($moHandler);
    $moSelect->populate();
    
    $moHandler = new QuerySelectAuditAlertReceiver(FWDWebLib::getConnection());    
    $moSelect = FWDWebLib::getObject('select_filter_receiver');
    $moSelect->setQueryHandler($moHandler);
    $moSelect->populate();
    
    /*
     * Popula o select com os tipos de formato de relatórios e
     * o select com os tipos de classificação de relatórios.
     */
    $moFormatSelect = FWDWebLib::getObject('select_report_type');
    $maAvailableFormats = FWDReportGenerator::getAvailableFormats(ISMSLib::getSupportedReportFormats());
    foreach ($maAvailableFormats as $miKey => $msValue) {
    	$moFormatSelect->setItemValue($miKey, $msValue);    	
    }
    $moHandler = new QueryGetReportClassification(FWDWebLib::getConnection());
    $moHandler->makeQuery();
    $maAvailableClassifications = $moHandler->executeQuery();
    $moClassificationSelect = FWDWebLib::getObject('select_report_classification');
    foreach ($maAvailableClassifications as $miKey => $msValue) {
      $moClassificationSelect->setItemValue($msValue, $msValue);
    }
    
    //instala a segurança de acls do sistema nos elementos da tela
    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
    
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_audit_alert.xml");
?>