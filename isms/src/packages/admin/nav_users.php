<?php
include_once "include.php";
include_once $handlers_ref . "grid/QueryGridSystemUsers.php";
include_once $handlers_ref . "QueryUserHasTasksOrAlerts.php";

class UserRemoveConfirm extends FWDRunnable {
  public function run(){
    $msTitle = FWDLanguage::getPHPStringValue('tt_remove_user','Remover Usuário');
    $msMessage = FWDLanguage::getPHPStringValue('st_user_remove_confirm',"Você tem certeza de que deseja remover o usuário <b>%user_name%</b>?");

    $moUser = new ISMSUser();
    $moUser->fetchById(FWDWebLib::getObject('selected_user_id')->getValue());
    $msUserName = ISMSLib::truncateString($moUser->getFieldValue('user_name'), 70);
    $msMessage = str_replace("%user_name%",$msUserName,$msMessage);

    $msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
                    "soWindow = soPopUp.getOpener();" .
                    "soWindow.remove_user();";

    ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,50);
  }
}

class RemoveUserEvent extends FWDRunnable {
  public function run(){
    $miUserId = FWDWebLib::getObject('selected_user_id')->getValue();
    $moUser = new ISMSUser();
    if ($moUser->isDeletable($miUserId)) {
      $moQuery = new QueryUserHasTasksOrAlerts(FWDWebLib::getConnection());
      $moQuery->setUserId($miUserId);
      $moQuery->makeQuery();
      $moQuery->executeQuery();
      if ($moQuery->getHasTasksOrAlerts()) {
        $maExcludeUsers[] = $miUserId;
        $msTitle = FWDLanguage::getPHPStringValue('tt_choose_user_warning','Aviso de Escolha de Usuário');
        $msMessage = FWDLanguage::getPHPStringValue('st_choose_user_message',"Na tela a seguir, escolha o usuário que receberá as tarefas e os avisos pendentes deste usuário.");
        echo "isms_open_popup('popup_user_search','../../popup_user_search.php?exclude_users=" . serialize($maExcludeUsers) . "','','true',402,600);";
        ISMSLib::openOk($msTitle,$msMessage,"",40);
      } else {
        $moUser->delete($miUserId);

	$syncSite = new ISMSSyncSite();
      	$syncSite->deleteLogin($miUserId);
      }
    } else {
      $moUser->showDeleteError($miUserId);
    }
  }
}

class TransferUserEvent extends FWDRunnable {
  public function run() {
    $miUserId = FWDWebLib::getObject('selected_user_id')->getValue();
    $miTransferUserId = FWDWebLib::getObject('transfer_user_id')->getValue();

    $moTaskCreatorDelete = new WKFTask();
    $moTaskCreatorDelete->createFilter($miUserId,'task_creator_id');
    $moTaskCreatorDelete->createFilter(null,'task_date_accomplished','notnull');
    $moTaskCreatorDelete->select();
    while($moTaskCreatorDelete->fetch()) {
      $moTaskCreatorDelete->delete($moTaskCreatorDelete->getFieldValue('task_id'));
    }
    
    $moTaskCreator = new WKFTask();
    $moTaskCreator->createFilter($miUserId,'task_creator_id');
    $moTaskCreator->createFilter(null,'task_date_accomplished','null');
    $moTaskCreator->select();
    while($moTaskCreator->fetch()) {
      $moTask = new WKFTask();
      $moTask->setFieldValue('task_creator_id',$miTransferUserId);
      $moTask->update($moTaskCreator->getFieldValue('task_id'));
    }

    $moTaskReceiver = new WKFTask();
    $moTaskReceiver->createFilter($miUserId,'task_receiver_id');
    $moTaskReceiver->createFilter(null,'task_date_accomplished','notnull');
    $moTaskReceiver->select();
    while($moTaskReceiver->fetch()) {
      $moTaskReceiver->delete($moTaskReceiver->getFieldValue('task_id'));
    }
    
    $moTaskReceiver = new WKFTask();
    $moTaskReceiver->createFilter($miUserId,'task_receiver_id');
    $moTaskReceiver->createFilter(null,'task_date_accomplished','null');
    $moTaskReceiver->select();
    while($moTaskReceiver->fetch()) {
      $moTask = new WKFTask();
      $moTask->setFieldValue('task_receiver_id',$miTransferUserId);
      $moTask->update($moTaskReceiver->getFieldValue('task_id'));
    }

    $moAlertCreator = new WKFAlert();
    $moAlertCreator->createFilter($miUserId,'alert_creator_id');
    $moAlertCreator->select();
    while($moAlertCreator->fetch()) {
      $moAlert = new WKFAlert();
      $moAlert->setFieldValue('alert_creator_id',$miTransferUserId);
      $moAlert->update($moAlertCreator->getFieldValue('alert_id'));
    }

    $moUser = new ISMSUser();
    $moUser->fetchById($miUserId);
    $msDeletedName = $moUser->getFieldValue('user_name');
    $moUser->delete($miUserId);

    if(ISMSLib::getConfigById(GENERAL_EMAIL_ENABLED)){
      $moEmailPreferences = new ISMSEmailPreferences();
      if($moEmailPreferences->getDigestType($miTransferUserId)=='none'){
        $msDate = ISMSLib::getISMSDate();
        $moReceiver = new ISMSUser();
        $moReceiver->fetchById($miTransferUserId);
        $msReceiverEmail = $moReceiver->getFieldValue('user_email');
        $moEmailPreferences = new ISMSEmailPreferences();
        $msFormat = $moEmailPreferences->getEmailFormat($miTransferUserId);
        $msSystemName = ISMSLib::getSystemName();
        $moMailer = new ISMSMailer();
        $moMailer->setFormat($msFormat);
        $moMailer->setTo($msReceiverEmail);
        $moMailer->setSubject("[$msSystemName] " . date('d/m/Y') . ' - '.FWDLanguage::getPHPStringValue('em_alerts_and_tasks_transfer','Transferência de Tarefas e Alertas'));
        $moMailer->addContent('taskAndAlertTransferHeader');
        $moMailer->addContent('taskAndAlertTransfer');
        $moMailer->setParameter('user',$msDeletedName);
      }
      $moMailer->send();
    }

    echo "refresh_grid();";
  }
}

class ResetAllPasswordConfirm extends FWDRunnable {
  public function run() {
    $msTitle = FWDLanguage::getPHPStringValue('tt_reset_all_users_password','Resetar senha de todos os usuários');
    $msMessage = FWDLanguage::getPHPStringValue('st_reset_all_users_passwords_confirm',"Você tem certeza que deseja resetar a senha de <b>todos os usuários</b> e enviar a nova senha para seus e-mails?");

    $msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
            "soWindow = soPopUp.getOpener();" .
            "soWindow.reset_all_password();";

    ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,50);
  }
}

class ResetAllPasswordEvent extends FWDRunnable {
  public function run() {
   if(ISMSLib::getConfigById(GENERAL_EMAIL_ENABLED)){
      $moUser = new ISMSUser();
      $moUser->select();
      $msSystemName = ISMSLib::getSystemName();
      while($moUser->fetch()) {
        $miUserId = $moUser->getFieldValue('user_id');
        $moPassPolicy = new ISMSPasswordPolicy();
        $msNewPassword = $moPassPolicy->generateRandomPassword();
        $moUser2 = new ISMSUser();
        $moUser2->setFieldValue('user_password', md5($msNewPassword));
        $moUser2->update($miUserId,false);
        unset($moUser2);
        $msReceiverEmail = $moUser->getFieldValue('user_email');
        $moEmailPreferences = new ISMSEmailPreferences();
        $msFormat = $moEmailPreferences->getEmailFormat($miUserId);
        $moMailer = new ISMSMailer();
        $moMailer->setFormat($msFormat);
        $moMailer->setTo($msReceiverEmail);
        $moMailer->setSubject("[$msSystemName] " . date('d/m/Y') . ' - '.FWDLanguage::getPHPStringValue('em_new_password','Nova Senha'));
        $moMailer->addContent('newPassword');
        $moMailer->setParameter('new_password',$msNewPassword);
        $moMailer->send();
        unset($moMailer);
        unset($moEmailPreferences);
      }
   }else{
      trigger_error("Trying to reset a user password with the general email disabled!",E_USER_ERROR);
   }
  }
}

class UnblockUserEvent extends FWDRunnable {
  public function run() {
    $miUserId = FWDWebLib::getObject('selected_user_id')->getValue();
    $moUser = new ISMSUser();
    $moUser->setFieldValue('user_wrong_logon_attempts', 0);
    $moUser->setFieldValue('user_is_blocked', false);
    $moUser->update($miUserId);
    echo "refresh_grid()";
  }
}

class BlockUserEvent extends FWDRunnable {
  public function run() {
    $miUserId = FWDWebLib::getObject('selected_user_id')->getValue();
    $moUser = new ISMSUser();
    $moUser->setFieldValue('user_is_blocked', true);
    $moUser->update($miUserId);
    echo "refresh_grid()";
  }
}

class GridUsers extends FWDDrawGrid {
  public function DrawItem(){
    switch($this->ciColumnIndex){
      case 1:
        $maMenuACLs = array('block','unblock');
        $maAllowed = array();
        $msGfxRef = FWDWebLib::getInstance()->getGfxRef();
        $moIcon = new FWDIcon(new FWDBox(2,4));
        if ($this->caData[9]) {
          $maAllowed[] = 'unblock';
          $moIcon->setAttrSrc("{$msGfxRef}icon-user_blocked.gif");
        } else {
          $maAllowed[] = 'block';
          $moIcon->setAttrSrc("{$msGfxRef}icon-user.gif");
        }
        $this->coCellBox->setObjFWDToolTip(ISMSLib::getInstance()->getToolTipCreateModify($this->caData[5],$this->caData[6],$this->caData[7],$this->caData[8]));
        $moACL = FWDACLSecurity::getInstance();
        $moACL->setNotAllowed(array_diff($maMenuACLs,$maAllowed));
        $moMenu = FWDWebLib::getObject('menu');
        $moGrid = FWDWebLib::getObject('grid_users');
        $msLine = $moGrid->getAttrName().'_acl_'.$this->ciRowIndex;
        FWDGridMenuACLSecurity::getInstance()->installGridMenuACL($moMenu,$moACL,$moGrid,$msLine);
        return $moIcon->draw();
      break;

      default:
        return parent::drawItem();
      break;
    }
  }
}

class ShowPopupUsersReachedLimit extends FWDRunnable {
  public function run(){
    echo ISMSLib::openLicenseLimitReached("users");
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new UserRemoveConfirm("user_remove_confirm"));
    $moStartEvent->addAjaxEvent(new RemoveUserEvent("remove_user_event"));
    $moStartEvent->addAjaxEvent(new ResetAllPasswordConfirm("reset_all_password_confirm"));
    $moStartEvent->addAjaxEvent(new ResetAllPasswordEvent("reset_all_password_event"));
    $moStartEvent->addAjaxEvent(new TransferUserEvent("transfer_user_event"));
    $moStartEvent->addAjaxEvent(new BlockUserEvent("block_user_event"));
    $moStartEvent->addAjaxEvent(new UnblockUserEvent("unblock_user_event"));
    $moStartEvent->addAjaxEvent(new ShowPopupUsersReachedLimit("show_limit_users_reached"));

    //instala a segurança de ACL na página
    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
    FWDMenuACLSecurity::getInstance()->installMenuACL(FWDWebLib::getObject('menu'),FWDACLSecurity::getInstance());

    $moGrid = FWDWebLib::getObject("grid_users");
    $moHandler = new QueryGridSystemUsers(FWDWebLib::getConnection());
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new GridUsers());
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){

    $moConfig = new ISMSConfig();
    if(!$moConfig->getConfigFromDB(GENERAL_EMAIL_ENABLED)){
      FWDWebLib::getObject('reset_password')->setAttrDisplay("false");
    }

    if(!$moConfig->getConfigFromDB(AD_SERVER)){
      FWDWebLib::getObject('import_ad_users')->setAttrDisplay("false");
    } else {
      FWDWebLib::getObject('reset_password')->setAttrDisplay("false");
    }

    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
    <script language="javascript">
    function openPopupLimitUsersReached(){
      trigger_event('show_limit_users_reached',3);
    }
    function refresh_grid() {
      js_refresh_grid('grid_users');
    }
    function reset_all_password() {
      trigger_event('reset_all_password_event',3);
    }
    function remove_user() {
      trigger_event('remove_user_event',3);
      refresh_grid();
    }

    function set_user(id, name) {
      gebi('transfer_user_id').value = id;
      trigger_event('transfer_user_event',3);
    }
    </script>
  <?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_users.xml");
?>
