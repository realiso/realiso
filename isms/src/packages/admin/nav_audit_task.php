<?php
include_once "include.php";
include_once $handlers_ref . "grid/QueryGridAuditTask.php";
include_once $handlers_ref . "select/QuerySelectAuditTaskCreator.php";
include_once $handlers_ref . "select/QuerySelectAuditTaskReceiver.php";
include_once $handlers_ref . "QueryAuditTaskActivity.php";
include_once $handlers_ref . "QueryGetReportClassification.php";

class FilterAuditTaskEvent extends FWDRunnable {
	public function run() {
		$moGrid = FWDWebLib::getObject('grid_audit_task');
		$moGrid->execEventPopulate();	
	}
}

class GridAuditTask extends FWDDrawGrid {
  public function drawItem(){    
    switch($this->ciColumnIndex){
      case 1: case 2:
      	if ($this->coCellBox->getValue())
      		$this->coCellBox->setValue(ISMSLib::getISMSDate($this->coCellBox->getValue()));     		
      	return $this->coCellBox->draw(); 
      break;
      
      case 5:
        $msContextName = $this->caData[6];
        $msAction = ISMSActivity::getDescription($this->caData[7]);
        if($msContextName){
          $this->coCellBox->setValue("{$msAction}: {$msContextName}");
        }else{
          $this->coCellBox->setValue($msAction);
        }
        return $this->coCellBox->draw();
      break;
            
      default:
        return parent::drawItem();
      break;
    }
  }
}

class ISMSReportEvent extends FWDRunnable {
	
	private function getFilter() {
		$moReportFilter = new ISMSReportAuditTaskFilter();
		
		$moReportFilter->setDateCreationStart(FWDWebLib::getObject("created_date_start")->getTimestamp());
		$moReportFilter->setDateCreationFinish(FWDWebLib::getObject("created_date_end")->getTimestamp());
		$moReportFilter->setCreator(FWDWebLib::getObject("filter_creator")->getValue());
		$moReportFilter->setReceiver(FWDWebLib::getObject("filter_receiver")->getValue());
		$moReportFilter->setActivity(FWDWebLib::getObject("filter_activity")->getValue());
		
		$moReportFilter->setManualClassification(FWDWebLib::getObject('select_report_classification')->getValue());
	  $moReportFilter->setFileType(FWDWebLib::getObject('select_report_type')->getValue());
    
    FWDWebLib::getObject('report_comment')->getObjFWDString()->setAttrNoEscape('true');
    
	  $moReportFilter->setComment(FWDWebLib::convertToISO(FWDWebLib::getObject('report_comment')->getValue(), true));
	  
	  return $moReportFilter;
	}
	
  public function run(){  	
  	set_time_limit(3000);   

    $mbDontForceDownload = (FWDWebLib::getObject("select_report_type")->getValue()==REPORT_FILETYPE_HTML);

    $moWindowReport = new ISMSReportWindow(ADMIN_MODE);
    if($mbDontForceDownload) $moWindowReport->open();

    $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    $moSession->deleteAttribute("filter");
    $moSession->addAttribute("filter");    
    $moSession->setAttrFilter($this->getFilter());
    
    $msReport = "report_audit_task";
    	
    if($mbDontForceDownload){
      $moWindowReport->setWaitLoadingComplete(true);	      
      $moWindowReport->loadReport($msReport);
    }
    else $moWindowReport->forceReportDownload($msReport);  	  	
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));   
		$moStartEvent->addAjaxEvent(new FilterAuditTaskEvent("filter_audit_task"));
		$moStartEvent->addAjaxEvent(new ISMSReportEvent("function_report"));
  	
    $moConfig = new ISMSConfig();
    $databaseWasRemoved = $moConfig->getConfigFromDB(RDB_RESET_DATA_BASE);
    $whenDatabaseWasRemoved = $moConfig->getConfigFromDB(RDB_DATE_RESET_DATA_BASE);

    $moGrid = FWDWebLib::getObject("grid_audit_task");
    $moGridHandler = new QueryGridAuditTask(FWDWebLib::getConnection());
		$moGridHandler->setCreatedDateStart(FWDWebLib::getObject("created_date_start")->getTimestamp());
		$moGridHandler->setCreatedDateEnd(FWDWebLib::getObject("created_date_end")->getTimestamp());
		$moGridHandler->setCreator(FWDWebLib::getObject("filter_creator")->getValue());
		$moGridHandler->setReceiver(FWDWebLib::getObject("filter_receiver")->getValue());
		$moGridHandler->setActivity(FWDWebLib::getObject("filter_activity")->getValue());
    $moGridHandler->isDatabaseWasRemove($databaseWasRemoved);
    $moGridHandler->getWhenDatabaseWasRemoved($whenDatabaseWasRemoved);

		$moGrid->setQueryHandler($moGridHandler);
    $moGrid->setObjFwdDrawGrid(new GridAuditTask());       
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
  	
		$moHandler = new QuerySelectAuditTaskCreator(FWDWebLib::getConnection());    
    $moSelect = FWDWebLib::getObject('select_filter_creator');
    $moSelect->setQueryHandler($moHandler);
    $moSelect->populate();
    
    $moHandler = new QuerySelectAuditTaskReceiver(FWDWebLib::getConnection());    
    $moSelect = FWDWebLib::getObject('select_filter_receiver');
    $moSelect->setQueryHandler($moHandler);
    $moSelect->populate();
    
    $moSelect = FWDWebLib::getObject('select_filter_activity');
    $moSelectActivity = new QueryAuditTaskActivity(FWDWebLib::getConnection());
    $moSelectActivity->makeQuery();
  	$moSelectActivity->executeQuery();
  	while($moSelectActivity->getDataset()->fetch()) {
  		$moSelectActivityDataset = $moSelectActivity->getDataset();
  		$miSelectId = $moSelectActivityDataset->getFieldByAlias('activity_id')->getValue();
  		$msSelectValue = ISMSActivity::getDescription($miSelectId);
  		$moItem = new FWDItem();
  		$moItem->setAttrKey($miSelectId);
  		$moItem->setValue($msSelectValue);
  		$moSelect->addObjFWDItem($moItem);
  	}
  	
  	/*
     * Popula o select com os tipos de formato de relatórios e
     * o select com os tipos de classificação de relatórios.
     */
    $moFormatSelect = FWDWebLib::getObject('select_report_type');
    $maAvailableFormats = FWDReportGenerator::getAvailableFormats(ISMSLib::getSupportedReportFormats());
    foreach ($maAvailableFormats as $miKey => $msValue) {
    	$moFormatSelect->setItemValue($miKey, $msValue);    	
    }
    $moHandler = new QueryGetReportClassification(FWDWebLib::getConnection());
    $moHandler->makeQuery();
    $maAvailableClassifications = $moHandler->executeQuery();
    $moClassificationSelect = FWDWebLib::getObject('select_report_classification');
    foreach ($maAvailableClassifications as $miKey => $msValue) {
      $moClassificationSelect->setItemValue($msValue, $msValue);
    }
  	
    //instala a segurança de acls do sistema nos elementos da tela
    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
    
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));	           
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_audit_task.xml");
?>