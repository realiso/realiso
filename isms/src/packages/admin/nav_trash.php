<?php

include_once "include.php";
include_once $handlers_ref . "QueryTrash.php";
include_once $handlers_ref . "grid/QueryGridTrash.php";
include_once $handlers_ref . "QueryConflictedContextNames.php";
include_once $handlers_ref . "QueryContextsSubStructure.php";
include_once $handlers_ref . "QueryTrashContextByUserDeleted.php";
include_once $handlers_ref . "QueryTrashUsersByProfileDeleted.php";
include_once $handlers_ref . "QueryDocumentsByContextId.php";

/*** DEFINI��O DOS CONFLITOS DE RESTAURA��O ***/
// CONFLITO DE ESTRUTURA: ocorre quando � necess�ria a restaura��o de uma
// estrutura de pais e pelo menos um deles n�o est� selecionado
// CONFLITO DE NOME: ocorre quando existe um contexto no sistema com o mesmo
// tipo e o mesmo nome de um selecionado
// ATEN��O: QUANDO FOR FEITA UMA OP��O DE N�O RESTAURAR ELEMENTOS COM CONFLITO DE
// NOME, � SEMPRE NECESS�RIO VERIFICAR SE ESTA A��O GERAR� CONFLITOS DE ESTRUTURA

class DetectConflictsOnRestoreEvent extends FWDRunnable {
	public function run() {
		// flag dizendo se existe algum contexto deletado selecionado para ser restaurado que est�
		//relacionado a um usu�rio excluido e que n�o est� selecionada para ser restaurado
		$mbUserConflictWithContext = false;

		$maSelectedElements = FWDWebLib::unserializeString(FWDWebLib::getObject("contexts")->getValue());
		// todos usu�rios deletados existentes na lixeira
		$maDeletedUsersAll = array();
		// usu�rios selecionados para serem restaurados
		$maDeletedUsersSelect = array();
		//usu�rios existentes na lixeira que n�o est�o sendo restaurados
		$maDeletedUsersQuery = array();
		//armazena os contextos que n�o podem ser restaurados devido a rela��o com um usu�rio que est�o na lixeira
		$maCtxNotRestaured = array();
		//indica se ocorreu pelo menos um conflito de nome
		$mbNameConflict = false;
		//armazena os possiveis conflitos de nome
		$maNameConflicts = array();
		// armazena os docs selecionados para serem restaurados
		$maDocsToBeRestored = array();
		//flag q indica se ocorreu conflito entre um documento e um contexto relacionado a ele e que est� na lixeira mas
		//nao est� sendo restaurado no momento
		$mbDocConflitWithContext = false;

		// monta array com todos os elementos logicamente deletados (id => type)
		$moTrashQuery = new QueryTrash(FWDWebLib::getConnection());
		$moTrashQuery->makeQuery();
		$moTrashQuery->executeQuery();
		$maAllElements = array();
		foreach ($moTrashQuery->getContexts() as $maContext) {
			$maAllElements[$maContext["context_id"]] = $maContext["context_type"];
			if($maContext["context_type"]==CONTEXT_USER){
				$maDeletedUsersAll[] = $maContext["context_id"];
			}
		}
		$maElementsNotSelected = array_diff($maAllElements,$maSelectedElements);
		//monta array com todos os usu�rios / documentos deletados que foram selecionados para serem restaurados
		foreach($maSelectedElements  as $miId => $miType){
			if($miType == CONTEXT_USER){
				$maDeletedUsersSelect[] = $miId;
			}elseif($miType == CONTEXT_DOCUMENT){
				$maDocsToBeRestored[] = $miId;
			}
		}

		//monta array com os usu�rios deletados que n�o foram selecionados para a restaura��o
		$maDeletedUsersQuery = array_diff($maDeletedUsersAll,$maDeletedUsersSelect);
		if(count($maDeletedUsersQuery)){
			$moQueryContextByUser = new QueryTrashContextByUserDeleted(FWDWebLib::getConnection());
			$moQueryContextByUser->setUsersIds($maDeletedUsersQuery);
			$maCtxNotRestaured = $moQueryContextByUser->getIds();
			if(count($maCtxNotRestaured)){
				$mbUserConflictWithContext = true;
				$maSelectedElements = array_diff($maSelectedElements,$maCtxNotRestaured);
			}
		}

		//verifica se os documentos selecionados para serem restaurados podem ser restaurados
		//s� ser�o restaurados os documentos que nao cont�m rela��o com os contextos q possuem documento
		//ou os documentos que est�o relacionados, por�m, que pelo menos um dos contextos relacionados ao documento
		//tamb�m est�o selecionados para serem restaurados
		if(FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(POLICY_MODE)){
			foreach($maDocsToBeRestored as $miKey){
				$moQueryCtxByDoc = new QueryContextsByDocumentId(FWDWebLib::getConnection());
				$moQueryCtxByDoc->setId($miKey);
				$moQueryCtxByDoc->setExecByTrash(false);
				$maCtxIds = $moQueryCtxByDoc->getValue();
				if(count($maCtxIds)==0){
					//o documento n�o tem um contexto n�o deletado que est� relacionado a ele
					$moQueryCtxByDocDel = new QueryContextsByDocumentId(FWDWebLib::getConnection());
					$moQueryCtxByDocDel->setId($miKey);
					$moQueryCtxByDocDel->setExecByTrash(true);
					$maCtxIdsDel = $moQueryCtxByDocDel->getValue();
					if(count($maCtxIdsDel)){
						$maIntersec = array_intersect($maCtxIdsDel,array_keys($maSelectedElements));
						if(count($maIntersec)==0){
							//todos os contextos relacionados ao documento est�o na lixeira e n�o foram selecionados para serem restaurados
							$mbDocConflitWithContext = true;
							$maSelectedElements = array_diff_key($maSelectedElements,array($miKey=>CONTEXT_DOCUMENT));
						}
					}
				}
			}
		}
		// objeto respons�vel por instanciar objetos de contexto
		$moContextObject = new ISMSContextObject();

		// conflitos encontrados
		$mbStructureConflict = false;
		$mbNameConflict = false;

		//arrays de contextos com conflitos
		$maStructureConflicts = array();
		$maNameConflicts = array();

		/***** DETECTAR CONFLITOS DE ESTRUTURA *****/
		// detectar os elementos selecionados que possuam algum pai logicamente
		// deletado e n�o selecionado (seta o flag e monta o array)
		foreach($maSelectedElements as $miCurrElemId => $miCurrElemType){
			$moContext = $moContextObject->getContextObjectByContextId($miCurrElemId,true);
			// pega todos os ancestrais do contexto
			$maSuperContexts = $moContext->getSuperContexts($miCurrElemId,true);

			////se for do tipo documento - pega os contextos que o documento
			if(count($maSuperContexts)>0){
				// destes, pegar soh aqueles que est�o logicamente deletados
				$maDeletedSuperContexts = array_intersect($maSuperContexts,array_keys($maAllElements));
				// dos resultantes, tirar aqueles que est�o selecionados
				$maConflictedSuperContexts = array_diff($maDeletedSuperContexts,array_keys($maSelectedElements));
				// se o contexto atual possui conflito de estrutura, sinaliza isso

				if(count($maConflictedSuperContexts)>0){
					//$moSuperContext = $moContextObject->getContextObjectByContextId($maConflictedSuperContexts[1],true);
					//$maStructureConflicts[$miCurrElemId] = $moSuperContext->getContextType();
					$maStructureConflicts[$miCurrElemId] = $miCurrElemType;
				}
			}
		}
		$mbStructureConflict = (count($maStructureConflicts)>0);

		if(count($maSelectedElements)){
			/***** DETECTAR CONFLITOS DE NOME *****/
			// detectar se exitem elementos no sistema com o mesmo tipo e o mesmo nome
			// de elemntos selecionados (seta o flag e monta o array)
			$moQuery = new QueryConflictedContextNames(FWDWebLib::getConnection());
			$moQuery->setContextIds(array_keys($maSelectedElements));
			$moQuery->makeQuery();
			$moQuery->executeQuery();
			$maNameConflicts = $moQuery->getConflictedContexts();

			if (count($maNameConflicts)>0) {
				$mbNameConflict = true;
			}
		}

		// armazena os arrays serializados de conflitos para poder recuperar
		// os dados a partir de outros serverevents


		echo "gebi('structure_conflicts').value = '".serialize($maStructureConflicts)."';";
		echo "gebi('name_conflicts').value = '".serialize($maNameConflicts)."';";
		echo "gebi('contexts').value = '".serialize($maSelectedElements)."';";
		echo "gebi('has_conflict_with_user_deleted').value = '". $mbUserConflictWithContext ."';";
		echo "gebi('has_conflict_with_document_context').value = '". $mbDocConflitWithContext ."';";
		echo "gebi('ctx_cant_be_restaured').value = '".serialize($maCtxNotRestaured)."';";

		//---------DEBUG-----------------
		//$debug = "STRUCTURE CONFLICTS = ".serialize($maStructureConflicts)."\\n";
		//$debug.= "NAME CONFLICTS = ".serialize($maNameConflicts);
		//echo "alert('".$debug."');";
		//---------DEBUG-----------------


		// caso sejam encontrados conflitos, abrir uma popup para obter
		// a a��o desejada pelo usu�rio, sen�o, basta restaurar os contextos
		if ($mbStructureConflict && $mbNameConflict) {
			echo "trigger_event('open_structure_and_name_conflict_popup','3');";
		}
		elseif ($mbStructureConflict) {
			echo "trigger_event('open_structure_conflict_popup','3');";
		}
		elseif ($mbNameConflict) {
			echo "trigger_event('open_name_conflict_popup','3');";
		}
		else {
			echo "trigger_event('open_restore_confirm_popup','3');";
		}
	}
}

class OpenStructureAndNameConflictPopup extends FWDRunnable {
	public function run() {
		$moWebLib = FWDWebLib::getInstance();
		$msPopUpId = 'popup_confirm';
		$msXmlFile = $moWebLib->getSysRef().'packages/admin/popup_restore_structure_and_name_conflict.xml';
		$msIconSrc = FWDWebLib::getInstance()->getGfxRef().'icon-exclamation.gif';
		$moWebLib->xml_load($msXmlFile,false);
		$moPanel = $moWebLib->getObject('confirm_panel');
		$moButtons = $moWebLib->getObject('confirm_buttons');
		$moWebLib->getObject('confirm_icon')->setAttrSrc($msIconSrc);

		$moPanelBox = $moPanel->getObjFWDBox();
		$miPanelHeight = $moPanelBox->getAttrHeight();
		$miPanelWidth = $moPanelBox->getAttrWidth();

		// centraliza a viewgroup dos botoes 'Restaurar' e 'Cancelar'
		$miConfirmPanelWidth = $moPanel->getObjFWDBox()->getAttrWidth();
		$miConfirmButtonsWidth = $moButtons->getObjFWDBox()->getAttrWidth();
		$miDelta = floor( ($miConfirmPanelWidth - $miConfirmButtonsWidth) / 2 );
		$moButtons->getObjFWDBox()->setAttrLeft($miDelta);

		// Evento do 'Cancelar'
		$moEvent = new FWDClientEvent();
		$moEvent->setAttrEvent('onClick');
		$moEvent->setAttrValue("soPopUpManager.closePopUp('$msPopUpId');");
		$moWebLib->getObject('confirm_viewbutton_no')->addObjFWDEvent($moEvent);

		// Evento do 'Restaurar'
		$msConfirmEvent = "soPopUp = soPopUpManager.getPopUpById('$msPopUpId');" .
                      "soWindow = soPopUp.getOpener();" .
                      "soWindow.handle_structure_and_name_conflicts(gebi('radio_controller').value,gebi('check_controller').value);";
		$moEvent = new FWDClientEvent();
		$moEvent->setAttrEvent('onClick');
		$moEvent->setAttrValue($msConfirmEvent."soPopUpManager.closePopUp('$msPopUpId');");
		$moWebLib->getObject('confirm_viewbutton_yes')->addObjFWDEvent($moEvent);

		$msXml = str_replace(array("\n",'"'),array(' ','\"'),$moPanel->draw());
		echo "isms_open_popup('$msPopUpId','','','true',$miPanelHeight,$miPanelWidth);"
		."soPopUpManager.getPopUpById('$msPopUpId').setHtmlContent(\"$msXml\");";
	}
}

class OpenStructureConflictPopup extends FWDRunnable {
	public function run() {
		$moWebLib = FWDWebLib::getInstance();
		$msPopUpId = 'popup_confirm';
		$msXmlFile = $moWebLib->getSysRef().'packages/admin/popup_restore_structure_conflict.xml';
		$msIconSrc = FWDWebLib::getInstance()->getGfxRef().'icon-exclamation.gif';
		$moWebLib->xml_load($msXmlFile,false);
		$moPanel = $moWebLib->getObject('confirm_panel');
		$moButtons = $moWebLib->getObject('confirm_buttons');
		$moWebLib->getObject('confirm_icon')->setAttrSrc($msIconSrc);

		$moPanelBox = $moPanel->getObjFWDBox();
		$miPanelHeight = $moPanelBox->getAttrHeight();
		$miPanelWidth = $moPanelBox->getAttrWidth();

		// centraliza a viewgroup dos botoes 'Restaurar' e 'Cancelar'
		$miConfirmPanelWidth = $moPanel->getObjFWDBox()->getAttrWidth();
		$miConfirmButtonsWidth = $moButtons->getObjFWDBox()->getAttrWidth();
		$miDelta = floor( ($miConfirmPanelWidth - $miConfirmButtonsWidth) / 2 );
		$moButtons->getObjFWDBox()->setAttrLeft($miDelta);

		// Evento do 'Cancelar'
		$moEvent = new FWDClientEvent();
		$moEvent->setAttrEvent('onClick');
		$moEvent->setAttrValue("soPopUpManager.closePopUp('$msPopUpId');");
		$moWebLib->getObject('confirm_viewbutton_no')->addObjFWDEvent($moEvent);

		// Evento do 'Restaurar'
		$msConfirmEvent = "soPopUp = soPopUpManager.getPopUpById('$msPopUpId');" .
                      "soWindow = soPopUp.getOpener();" .
                      "soWindow.handle_structure_conflicts(gebi('radio_controller').value,gebi('check_controller').value);";
		$moEvent = new FWDClientEvent();
		$moEvent->setAttrEvent('onClick');
		$moEvent->setAttrValue($msConfirmEvent."soPopUpManager.closePopUp('$msPopUpId');");
		$moWebLib->getObject('confirm_viewbutton_yes')->addObjFWDEvent($moEvent);

		$msXml = str_replace(array("\n",'"'),array(' ','\"'),$moPanel->draw());
		echo "isms_open_popup('$msPopUpId','','','true',$miPanelHeight,$miPanelWidth);"
		."soPopUpManager.getPopUpById('$msPopUpId').setHtmlContent(\"$msXml\");";
	}
}

class OpenNameConflictPopup extends FWDRunnable {
	public function run() {
		$moWebLib = FWDWebLib::getInstance();
		$msPopUpId = 'popup_confirm';
		$msXmlFile = $moWebLib->getSysRef().'packages/admin/popup_restore_name_conflict.xml';
		$msIconSrc = FWDWebLib::getInstance()->getGfxRef().'icon-exclamation.gif';
		$moWebLib->xml_load($msXmlFile,false);
		$moPanel = $moWebLib->getObject('confirm_panel');
		$moButtons = $moWebLib->getObject('confirm_buttons');
		$moWebLib->getObject('confirm_icon')->setAttrSrc($msIconSrc);

		$moPanelBox = $moPanel->getObjFWDBox();
		$miPanelHeight = $moPanelBox->getAttrHeight();
		$miPanelWidth = $moPanelBox->getAttrWidth();

		// centraliza a viewgroup dos botoes 'Restaurar' e 'Cancelar'
		$miConfirmPanelWidth = $moPanel->getObjFWDBox()->getAttrWidth();
		$miConfirmButtonsWidth = $moButtons->getObjFWDBox()->getAttrWidth();
		$miDelta = floor( ($miConfirmPanelWidth - $miConfirmButtonsWidth) / 2 );
		$moButtons->getObjFWDBox()->setAttrLeft($miDelta);

		// Evento do 'Cancelar'
		$moEvent = new FWDClientEvent();
		$moEvent->setAttrEvent('onClick');
		$moEvent->setAttrValue("soPopUpManager.closePopUp('$msPopUpId');");
		$moWebLib->getObject('confirm_viewbutton_no')->addObjFWDEvent($moEvent);

		// Evento do 'Restaurar'
		$msConfirmEvent = "soPopUp = soPopUpManager.getPopUpById('$msPopUpId');" .
                      "soWindow = soPopUp.getOpener();" .
                      "soWindow.handle_name_conflicts(gebi('radio_controller').value);";
		$moEvent = new FWDClientEvent();
		$moEvent->setAttrEvent('onClick');
		$moEvent->setAttrValue($msConfirmEvent."soPopUpManager.closePopUp('$msPopUpId');");
		$moWebLib->getObject('confirm_viewbutton_yes')->addObjFWDEvent($moEvent);

		$msXml = str_replace(array("\n",'"'),array(' ','\"'),$moPanel->draw());
		echo "isms_open_popup('$msPopUpId','','','true',$miPanelHeight,$miPanelWidth);"
		."soPopUpManager.getPopUpById('$msPopUpId').setHtmlContent(\"$msXml\");";
	}
}

class RemoveStructureConflicts extends FWDRunnable {
	public function run() {
		$maContexts = FWDWebLib::unserializeString(FWDWebLib::getObject("contexts")->getValue());
		$maStructureConflicts = FWDWebLib::unserializeString(FWDWebLib::getObject("structure_conflicts")->getValue());
		$maContexts = array_diff_assoc($maContexts,$maStructureConflicts);

		echo "gebi('contexts').value = '".serialize($maContexts)."';";
	}
}

class RemoveNameConflicts extends FWDRunnable {
	public function run() {
		$maContexts = FWDWebLib::unserializeString(FWDWebLib::getObject("contexts")->getValue());
		$maNameConflicts = FWDWebLib::unserializeString(FWDWebLib::getObject("name_conflicts")->getValue());

		$maContexts = array_diff_assoc($maContexts,$maNameConflicts);

		echo "gebi('contexts').value = '".serialize($maContexts)."';";

		// IMPORTANTE: como foram removidos contextos que estavam selecionados,
		// podem ter sido gerados conflitos de estrutura
		// MAIS IMPORANTE AINDA: por disparar um evento encadeado para a detec��o
		// de poss�veis novos conflitos, este evento (RemoveNameConflicts)
		// DEVE SER O �LTIMO A SER CHAMADO caso sejam necess�rios encadear outros
		// eventos
		echo "trigger_event('detect_conflicts_on_restore_event','3');";
	}
}

class AddNeededParentStructure extends FWDRunnable {
	public function run() {
		$maContexts = FWDWebLib::unserializeString(FWDWebLib::getObject("contexts")->getValue());
		$maStructureConflicts = FWDWebLib::unserializeString(FWDWebLib::getObject("structure_conflicts")->getValue());

		// monta array com todos os elementos logicamente deletados (id => type)
		$moTrashQuery = new QueryTrash(FWDWebLib::getConnection());
		$moTrashQuery->makeQuery();
		$moTrashQuery->executeQuery();
		$maAllElements = array();
		foreach ($moTrashQuery->getContexts() as $maContext) {
			$maAllElements[$maContext["context_id"]] = $maContext["context_type"];
		}

		//array contendo todos os ids de contexto que n�o podem ser restaurados devido a depend�ncia de pelo menos um usu�rio que est� deletado
		$maCtxCantBeRestaured = FWDWebLib::unserializeString(FWDWebLib::getObject("ctx_cant_be_restaured")->getValue());
		//armazena os contextos que n�o podem ser restaurados pois pelo menos um parent dele esta relacionado a pelo menos um usu�rio deletado
		$maContextsToBeRemoved = array();

		$moContextObject = new ISMSContextObject();
		$maNeededContexts = array();
		foreach($maStructureConflicts as $miCurrElemId => $miCurrElemType){
			$moContext = $moContextObject->getContextObjectByContextId($miCurrElemId,true);
			// pegar todos os ancestrais do contexto
			$maSuperContexts = $moContext->getSuperContexts($miCurrElemId,true);
			// destes, pegar soh aqueles que est�o logicamente deletados
			$maDeletedSuperContexts = array_intersect($maSuperContexts,array_keys($maAllElements));
			// dos resultantes, tirar aqueles que est�o selecionados
			$maNeededSuperContexts = array_diff($maDeletedSuperContexts,array_keys($maContexts));
			//verifica se pelo menos um dos pais tem problema de depencencia de usu�rio deletado
			if(count(array_intersect($maNeededSuperContexts,array_keys($maCtxCantBeRestaured)))){
				$maContextsToBeRemoved[$miCurrElemId] = $miCurrElemType;
			}else{
				foreach($maNeededSuperContexts as $miNeededId){
					$maNeededContexts[$miNeededId] = $maAllElements[$miNeededId];
				}
			}
		}

		//remove os contextos que possuem dependentes que est�o relacionados a usu�rios deletados
		if(count($maContextsToBeRemoved)){
			$maContexts = array_diff($maContexts,$maContextsToBeRemoved);
			echo "gebi('has_conflict_with_user_deleted').value = true;";
		}
		$maContexts = $maContexts+$maNeededContexts;
		echo "gebi('contexts').value = '".serialize($maContexts)."';";
	}
}

class RestoreEvent extends FWDRunnable {
	public function run() {
		//teste anti hack
		$maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
		if(in_array('A.L.1',$maACLs)){
			trigger_error("Unauthorized user trying to restore a context using the system trash!",E_USER_ERROR);
		}else{
			$maContexts = FWDWebLib::unserializeString(FWDWebLib::getObject("contexts")->getValue());
			$mbAssocIndividualy = false;
			$mbShowNotRestauredContext = FWDWebLib::getObject("has_conflict_with_user_deleted")->getValue();
			$mbShowNotRestauredDocuments = FWDWebLib::getObject("has_conflict_with_document_context")->getValue();
			//---------DEBUG-----------------
			//$debug = "APTOS A RESTAURA��O = ".implode(":",array_keys($maContexts))."\\n";
			//if ($maP = FWDWebLib::getObject("contexts_to_associate")->getValue()) {
			//  $maP = FWDWebLib::unserializeString($maP);
			//  $debug.= "PENDENTES = ".implode(":",array_keys($maP));
			//}
			//echo "alert('".$debug."');";
			//---------DEBUG-----------------

			$moContextObject = new ISMSContextObject();
			$maDocumentIdsToRestore = array();
			foreach ($maContexts as $miContextId => $miContextType) {
				$moContext = $moContextObject->getContextObject($miContextType,true);
				$moContext->restore($miContextId);

				/*
				 * Se possuir o m�dulo de documenta��o e o contexto tiver
				 * documento, atualiza os leitores desse documento.
				 * Tamb�m existe a possibilidade de ser um Risco. Nessa caso,
				 * deve-se pegar todos os controles do risco e atualizar seus
				 * leitores tamb�m.
				 */
				if (FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(POLICY_MODE)) {
					if ($moContext->hasDocument()) {
						$moContext->updateReaders($miContextId);
						//obtem os documentos relacionados ao contexto sendo restaurado
						$moQueryDocs = new QueryDocumentsByContextId(FWDWebLib::getConnection());
						$moQueryDocs->setId($miContextId);
						$moQueryDocs->setExecByTrash(true);
						$maDocumentIdsToRestore = array_merge($maDocumentIdsToRestore,$moQueryDocs->getValue());
					}elseif($moContext->getContextType() == CONTEXT_RISK) {
						$moRC = new RMRiskControl();
						$maControls = $moRC->getControlsFromRisk($miContextId);
						$moControl = new RMControl();
						foreach ($maControls as $miControlId) $moControl->updateReaders($miControlId);
					}
				}

				unset($moContext);
			}
			if (FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(POLICY_MODE)) {
				//restaura os documentos relacionados aos contextos que foram restaurados
				$maDocsToRestore = array_filter(array_unique(array_diff($maDocumentIdsToRestore,array_keys($maContexts))));
				foreach($maDocsToRestore as $miDocId){
					$moContext = $moContextObject->getContextObject(CONTEXT_DOCUMENT,true);
					$moContext->restore($miDocId);
				}
			}

			if(count($maContexts)){
				/*mostra a msg de elementos restaurados com sucesso somente se pelo menos 1 elementos foi restaurado*/
				echo " gobi('st_restored_warning').show(); setTimeout('gobi(\'st_restored_warning\').hide();',5000);";
			}
			echo "refresh_grid();";
			if (FWDWebLib::getObject("associate_individually")->getValue()) {
				$msContextsToAssociate = FWDWebLib::getObject("contexts_to_associate")->getValue();
				if($msContextsToAssociate){
					$mbAssocIndividualy = true;
					echo "isms_open_popup('popup_associate_elements','popup_associate_elements.php?elems=".$msContextsToAssociate."','','true',403,590);";
				}
			}else
			$mbAssocIndividualy = true;

			if($mbAssocIndividualy){
				if($mbShowNotRestauredContext){
					echo "trigger_event('open_info_context_not_restaured',3);";
				}elseif($mbShowNotRestauredDocuments){
					echo "trigger_event('open_info_document_not_restaured',3);";
				}
			}
		}//end test anti hack
	}
}

class OpenInfoDocumentNotRestaured extends FWDRunnable{
	public function run(){
		$msTitle = FWDLanguage::getPHPStringValue('tt_document_not_restored','Documentos n�o restaurados');
		$msMessage = FWDLanguage::getPHPStringValue('st_not_restored_documents_message',"Documentos selecionados n�o puderam ser restaurados, pois est�o relacionados a um ou mais elementos que permanecem removidos!");
		ISMSLib::openOk($msTitle,$msMessage,'',50);
	}
}

class OpenInfoContextNotRestaured extends FWDRunnable{
	public function run(){
		$msTitle = FWDLanguage::getPHPStringValue('tt_items_not_restored','Elementos n�o restaurados');
		$msMessage = FWDLanguage::getPHPStringValue('st_not_restored_items_message',"Elementos selecionados n�o puderam ser restaurados, pois est�o relacionados a um ou mais usu�rios que permanecem removidos!");
		ISMSLib::openOk($msTitle,$msMessage,'',50);
	}
}

class OpenInfoUserNotExcluded extends FWDRunnable{
	public function run(){
		$msTitle = FWDLanguage::getPHPStringValue('tt_users_not_deleted','Usu�rios n�o removidos');
		$msMessage = FWDLanguage::getPHPStringValue('st_not_restored_users_message',"Os Usu�rios selecionados n�o puderam ser removidos, pois est�o relacionados a itens existentes na lixeira.");
		ISMSLib::openOk($msTitle,$msMessage,'',50);
	}
}

class OpenInfoUserAndProfileNotExcluded extends FWDRunnable{
	public function run(){
		$msTitle = FWDLanguage::getPHPStringValue('tt_users_and_profile_not_deleted','Usu�rios e perfis n�o removidos');
		$msMessage = FWDLanguage::getPHPStringValue('st_not_excluded_users_and_profile_message',"Os usu�rios selecionados n�o puderam ser removidos, pois est�o relacionados a itens existentes na lixeira.<br/> Os perfis selecionados n�o puderam ser removidos, pois est�o relacionados a usu�rios existentes na lixeira que n�o foram selecionados para remo��o ou que n�o puderam ser exclu�dos.");
		ISMSLib::openOk($msTitle,$msMessage,'',100);
	}
}

class OpenInfoProfileNotExcluded extends FWDRunnable{
	public function run(){
		$msTitle = FWDLanguage::getPHPStringValue('tt_profile_not_deleted','Perfis n�o removidos');
		$msMessage = FWDLanguage::getPHPStringValue('st_not_removed_profiles_message',"Os perfis selecionados n�o puderam ser removidos, pois est�o relacionados a usu�rios existentes na lixeira que n�o foram selecionados ou que n�o puderam ser removidos.");
		ISMSLib::openOk($msTitle,$msMessage,'',60);
	}
}

class OpenRemoveConfirmPopup extends FWDRunnable {
	public function run(){
		$msTitle = FWDLanguage::getPHPStringValue('tt_items_permanent_removal','Remo��o permanente de elementos');

		$msMessage = FWDLanguage::getPHPStringValue('st_not_selected_items_remove_confirm',"Elementos n�o selecionados que forem sub-elementos ou estiverem associados a elementos selecionados ser�o removidos. Deseja continuar?");

		$msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
                    "soWindow = soPopUp.getOpener();" .
                    "soWindow.remove();";

		ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,60);
	}
}

class OpenRestoreConfirmPopup extends FWDRunnable {
	public function run() {
		$msTitle = FWDLanguage::getPHPStringValue('tt_items_restoral','Restaura��o de elementos');

		$msMessage = FWDLanguage::getPHPStringValue('st_items_restore_confirm',"Deseja restaurar os elementos selecionados?");

		$msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
                    "soWindow = soPopUp.getOpener();" .
                    "soWindow.restore();";

		ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,60);
	}
}

class RemoveEvent extends FWDRunnable {
	public function run() {
		$maElements = FWDWebLib::getObject("grid_trash")->getValue();
		$maDeletedUsersSelect = array();
		$maDeletedUsersAll = array();
		$maDeletedUsersQuery = array();
		$mbUserConflictWithContext = false;
		$maProfileSelected = array();
		$mbProfileConflitWithUser = false;
		$maProfileSelectedOk = array();
		// array de elementos selecionados (id => type)
		$maSelectedElements = array();
		foreach ($maElements as $maElement) {
			//retira os usu�rios do array de deletados para que eles sejam deletados
			//por ultimo
			if($maElement[3] == CONTEXT_USER)
			$maDeletedUsersSelect[$maElement[1]] = $maElement[3];
			elseif($maElement[3] == CONTEXT_PROFILE)
			$maProfileSelected[$maElement[1]] = $maElement[3];
			else
			$maSelectedElements[$maElement[1]] = $maElement[3];
		}

		// objeto respons�vel por instanciar objetos de contexto
		$moContextObject = new ISMSContextObject();
		$maDeleteElementIds = array_keys($maSelectedElements);

		//armazenar� os usu�rios que podem ser deletados do sistema
		$maDeletedUsersSelectOk = array();
		//retira os usu�rios selecionados para serem deletados caso exista algum contexto na lixeira
		//que n�o foi selecionado e que est� relacionado com um usu�rio selecionado para ser excluido
		if(count($maDeletedUsersSelect)){
			foreach($maDeletedUsersSelect as $miKey => $miType){
				$moQueryContextByUser = new QueryTrashContextByUserDeleted(FWDWebLib::getConnection());
				$moQueryContextByUser->setUsersIds(array($miKey));
				$maCtxByUser = $moQueryContextByUser->getIds();
				if(count($maCtxByUser)){
					$maCtxNotSelected = array_diff(array_keys($maCtxByUser),$maDeleteElementIds);
					if(count($maCtxNotSelected)){
						$mbUserConflictWithContext = true;
					}else{
						$maDeletedUsersSelectOk[]=$miKey;
					}
				}else{
					$maDeletedUsersSelectOk[]=$miKey;
				}
			}
		}

		//retira do array de selecionados os profiles que possuem usu�rios que n�o est�o selecionados
		//para serem deletados da lixeira
		foreach($maProfileSelected as $miKey=>$miType){
			$moHadleProfile = new QueryTrashUsersByProfileDeleted(FWDWebLib::getConnection());
			$moHadleProfile->setProfileIds(array($miKey));
			$maUsers = array_diff($moHadleProfile->getValue(),$maDeletedUsersSelectOk);
			if(count($maUsers)){
				$mbProfileConflitWithUser = true;
			}else{
				$maProfileSelectedOk[] = $miKey;
			}
		}

		//--------DEBUG------------
		//echo "alert('CONTEXTOS APTOS A REMOCAO:  ".implode(":",$maDeleteElementIds)."# USU�RIOS APTOS A REMO��O=".implode(':',$maDeletedUsersSelectOk)   ."');";
		//--------DEBUG------------
			
		//teste anti hack
		$maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
		if(in_array('A.L.3',$maACLs)){
			trigger_error("Unauthorized user trying to delete a context using the system trash!",E_USER_ERROR);
		}else{
			foreach($maDeleteElementIds as $miElementId){
				$moContext = $moContextObject->getContextObjectByContextId($miElementId,true);
				$moContext->delete($miElementId,true,true);
			}
			foreach($maDeletedUsersSelectOk as $miElementId){
				$moContext = new ISMSUser();
				$moContext->delete($miElementId,true,true);
			}
			foreach($maProfileSelectedOk as $miElementId){
				$moContext = new ISMSProfile();
				$moContext->delete($miElementId,true,true);
			}

			if(count($maDeleteElementIds)||count($maDeletedUsersSelectOk)||count($maProfileSelectedOk)){
				echo " gobi('st_removed_warning').show(); setTimeout('gobi(\'st_removed_warning\').hide();',5000);";
			}
			if($mbUserConflictWithContext && $mbProfileConflitWithUser){
				echo "refresh_grid();trigger_event('open_info_user_and_profile_not_excluded',3);";
			}elseif($mbUserConflictWithContext){
				echo "refresh_grid();trigger_event('open_info_user_not_excluded',3);";
			}elseif($mbProfileConflitWithUser){
				echo "refresh_grid();trigger_event('open_info_profile_not_excluded',3);";
			}else{
				echo "refresh_grid();";
			}
		}
	}
}

class OpenRemoveAllConfirmPopup extends FWDRunnable {
	public function run(){
		$msTitle = FWDLanguage::getPHPStringValue('tt_all_items_permanent_removal','Remo��o permanente de todos os elementos');
		$msMessage = FWDLanguage::getPHPStringValue('st_all_items_remove_confirm',"Todos os elementos da lixeira ser�o exlu�dos permanentemente! Deseja continuar?");

		$msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
                    "soWindow = soPopUp.getOpener();" .
                    "soWindow.remove_all();";

		ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,50);
	}
}

class RemoveAllEvent extends FWDRunnable {
	public function run() {
		//teste anti hack
		$maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
		if(in_array('A.L.3',$maACLs)){
			trigger_error("Unauthorized user trying to delete a context using the system trash!",E_USER_ERROR);
		}else{
			$moTrashQuery = new QueryTrash(FWDWebLib::getConnection());
			$moTrashQuery->makeQuery();
			$moTrashQuery->executeQuery();
			$moContextObject = new ISMSContextObject();
			$maUsersDelete = array();
			$maProfileSelected = array();
			$maDocumentDelete = array();
			$maIncidentDelete = array();
			$mbDeleteCommon = false;
			foreach ($moTrashQuery->getContexts() as $maContext) {
				//retira os usus�rios para deletalos por ultimo evitando assim erro de sql
				//por causa dos restrict das chaves estrangeiras dos usu�rios nas tabelas de contexto

				file_put_contents("/tmp/testeandoAAAA.txt", "punto com ". $maContext["context_id"] . "\n", FILE_APPEND);
				if($maContext["context_type"]==CONTEXT_USER){
					$maUsersDelete[] = $maContext["context_id"];
				}elseif($maContext["context_type"] == CONTEXT_PROFILE){
					$maProfileSelected[] = $maContext["context_id"];
				}elseif($maContext["context_type"] == CONTEXT_DOCUMENT){
					$maDocumentDelete[] = $maContext["context_id"];
				}elseif($maContext["context_type"] == CONTEXT_CI_INCIDENT){
					$maIncidentDelete[] = $maContext["context_id"];
				}else{
					$moContext = $moContextObject->getContextObject($maContext["context_type"],true);
					file_put_contents("/tmp/testeandoAAAA.txt", "punto com ". $maContext["context_id"] . "\n", FILE_APPEND);
					$moContext->delete($maContext["context_id"], true,true);
					$mbDeleteCommon = true;
				}
			}

file_put_contents("/tmp/testeandoAAAA.txt", "punto com ". var_export($maDocumentDelete, true) . "\n", FILE_APPEND);
			//deleta os documentos que est�o na lixeira
			foreach($maDocumentDelete as $miKey){
				file_put_contents("/tmp/testeandoAAAA.txt", "punto com ". $miKey . "\n", FILE_APPEND);
				$moContext = new PMDocument();
				$moContext->delete($miKey, true,true);
			}

			//deleta os incidentes que est�o na lixeira
			foreach($maIncidentDelete as $miKey){
				$moContext = new CIIncident();
				$moContext->delete($miKey, true,true);
			}

			//deleta os usu�rios que est�o na lixeira
			foreach($maUsersDelete as $miKey){
				$moContext = new ISMSUser();
				$moContext->delete($miKey, true,true);
			}

			//deleta os profiles que est�o na lixeira
			foreach($maProfileSelected as $miKey){
				$moContext = new ISMSProfile();
				$moContext->delete($miKey, true,true);
			}

			if($mbDeleteCommon || count($maProfileSelected) || count($maUsersDelete)){
				echo "  gobi('st_removed_warning').show(); setTimeout('gobi(\'st_removed_warning\').hide();',5000);";
			}
			echo "refresh_grid();";
		}
	}
}

class GridTrash extends FWDDrawGrid {
	public function drawItem(){
		switch($this->ciColumnIndex){
			case 1:
				$msGfxRef = FWDWebLib::getInstance()->getGfxRef();
				$moIcon = new FWDIcon(new FWDBox(2,4));
				$msIconSrc = ISMSContextObject::getContextObject($this->caData[3],true,true)->getIcon($this->caData[1]);
				$moIcon->setAttrSrc($msGfxRef.$msIconSrc);
				$this->coCellBox->setValue(" ");
				$this->coCellBox->setFWDIconCode($moIcon->draw());
				return parent::drawItem();
				break;

			case 3:
				$this->coCellBox->setValue(ISMSContextObject::getContextObject($this->caData[3],true)->getLabel());
				return parent::drawItem();
				break;

			default:
				return parent::drawItem();
				break;
		}
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run() {
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));

		$moStartEvent->addAjaxEvent(new DetectConflictsOnRestoreEvent("detect_conflicts_on_restore_event"));

		$moStartEvent->addAjaxEvent(new OpenStructureAndNameConflictPopup("open_structure_and_name_conflict_popup"));
		$moStartEvent->addAjaxEvent(new OpenStructureConflictPopup("open_structure_conflict_popup"));
		$moStartEvent->addAjaxEvent(new OpenNameConflictPopup("open_name_conflict_popup"));

		$moStartEvent->addAjaxEvent(new RemoveStructureConflicts("remove_structure_conflicts"));
		$moStartEvent->addAjaxEvent(new RemoveNameConflicts("remove_name_conflicts"));
		$moStartEvent->addAjaxEvent(new AddNeededParentStructure("add_needed_parent_structure"));

		$moStartEvent->addAjaxEvent(new OpenRestoreConfirmPopup("open_restore_confirm_popup"));
		$moStartEvent->addAjaxEvent(new RestoreEvent("restore_event"));

		$moStartEvent->addAjaxEvent(new OpenRemoveConfirmPopup("open_remove_confirm_popup_event"));
		$moStartEvent->addAjaxEvent(new RemoveEvent("remove_event"));
		$moStartEvent->addAjaxEvent(new OpenInfoContextNotRestaured("open_info_context_not_restaured"));
		$moStartEvent->addAjaxEvent(new OpenInfoUserNotExcluded("open_info_user_not_excluded"));
		$moStartEvent->addAjaxEvent(new OpenInfoUserAndProfileNotExcluded("open_info_user_and_profile_not_excluded"));
		$moStartEvent->addAjaxEvent(new OpenInfoProfileNotExcluded("open_info_profile_not_excluded"));
		$moStartEvent->addAjaxEvent(new OpenInfoDocumentNotRestaured("open_info_document_not_restaured"));

		$moStartEvent->addAjaxEvent(new OpenRemoveAllConfirmPopup("open_remove_all_confirm_popup_event"));
		$moStartEvent->addAjaxEvent(new RemoveAllEvent("remove_all_event"));

		$moGrid = FWDWebLib::getObject("grid_trash");
		$moHandler = new QueryGridTrash(FWDWebLib::getConnection());
		$moGrid->setQueryHandler($moHandler);
		$moGrid->setObjFwdDrawGrid(new GridTrash());
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		//instala a seguran�a de ACL na p�gina
		FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
<script language="javascript">
    function cleanup_variables(pbCleanContexts) {
      if (pbCleanContexts)
        gebi('contexts').value = '';
      gebi('structure_conflicts').value = '';
      gebi('name_conflicts').value = '';
      gebi('associate_individually').value = 0;
      gebi('contexts_to_associate').value = '';
      pbCleanContexts = null;
      gebi('has_conflict_with_user_deleted').value = false;
      gebi('ctx_cant_be_restaured').value = 'a:0:{}';
    }
    
    function refresh_grid() {
      js_refresh_grid('grid_trash');
      cleanup_variables(true);
    }
    
    function show_restored_event(){
      gobi('st_restored_warning').show(); setTimeout('gobi("st_restored_warning").hide();',5000);
    }
    
    function handle_structure_and_name_conflicts(psRadio, psCheck) {
      psCheck = (psCheck ? 1 : 0);
      gebi('associate_individually').value = psCheck;
      
      switch(psRadio) {
        
        case 'all' :
          if (psCheck==1) {
         <? /* ACAO: remover conflitos de estrutura (serao tratados caso a caso)
            // e ignorar conflitos de nome */ ?>
            gebi('contexts_to_associate').value = gebi('structure_conflicts').value;
            trigger_event('remove_structure_conflicts','3');
          }
          else
         <? /* ACAO: adicionar os necessarios para restaurar conflitos de estrutura
            // e ignorar conflitos de nome */ ?>
            trigger_event('add_needed_parent_structure','3');
          
          trigger_event('restore_event','3');
          break;
        
        case 'only_not_conflicted' :
         <? /* ACAO: remover conflitos de estrutura e de nome
            // importante! o remove_name_conflicts ja faz uma nova deteccao de
            // possiveis conflitos de estrutura e realiza as acoes necessarias */ ?>
          trigger_event('remove_structure_conflicts','3');
          trigger_event('remove_name_conflicts','3');
          break;
        
        case 'all_but_structure_conflicted' :
         <? /* ACAO: remover os conflitos de estrutura e ignorar conflitos de nome */ ?>
          trigger_event('remove_structure_conflicts','3');
          trigger_event('restore_event','3');
          break;
        
        case 'all_but_name_conflicted' :
          if (psCheck==1) {
         <? /* ACAO: remover conflitos de estrutura (serao tratados caso a caso)
            // e remover conflitos de nome 
            // importante! o remove_name_conflicts ja faz uma nova deteccao de
            // possiveis conflitos de estrutura e realiza as acoes necessarias */ ?>
            gebi('contexts_to_associate').value = gebi('structure_conflicts').value;
            trigger_event('remove_structure_conflicts','3');
            trigger_event('remove_name_conflicts','3');
          }
          else {
         <? /* ACAO: adicionar os necessarios para restaurar conflitos de estrutura
            // e remover conflitos de nome
            // importante! o remove_name_conflicts ja faz uma nova deteccao de
            // possiveis conflitos de estrutura e realiza as acoes necessarias */ ?>
            trigger_event('add_needed_parent_structure','3');
            trigger_event('remove_name_conflicts','3');
          }
          break;
        
        default:break;
      }
      psRadio = psCheck = null;
    }
    
    function handle_structure_conflicts(psRadio, psCheck) {
      psCheck = (psCheck ? 1 : 0);
      gebi('associate_individually').value = psCheck;
      switch(psRadio) {
        case 'all' :
          if (psCheck==1) {
         <? /* ACAO: remover conflitos de estrutura (serao tratados caso a caso) */ ?>
            gebi('contexts_to_associate').value = gebi('structure_conflicts').value;
            trigger_event('remove_structure_conflicts','3');
          }
          else {
         <? /* ACAO: adicionar os necessarios para restaurar conflitos de estrutura */ ?>
            trigger_event('add_needed_parent_structure','3');
          }
          
          trigger_event('restore_event','3');
          break;
        
        case 'only_not_conflicted' :
         <? /* ACAO: remover os conflitos de estrutura */ ?>
          trigger_event('remove_structure_conflicts','3');
          trigger_event('restore_event','3');
          break;
        
        default:break;
      }
      psRadio = psCheck = null;
    }
    
    function handle_name_conflicts(psRadio) {
      switch(psRadio) {
        
        case 'all' :
         <? /* ACAO: ignorar conflitos de nome */ ?>
          trigger_event('restore_event','3');
          break;
        
        case 'only_not_conflicted' :
         <? /* ACAO: remover conflitos de nome
          // importante! o remove_name_conflicts ja faz uma nova deteccao de
          // possiveis conflitos de estrutura e realiza as acoes necessarias */ ?>
          trigger_event('remove_name_conflicts','3');
          break;
        
        default:break;
      }
      psRadio = null;
    }
    
    function restore() { trigger_event('restore_event','3'); }
    function remove() { trigger_event('remove_event','3'); }
    function remove_all() { trigger_event('remove_all_event','3'); }
    
    function trash_serialize_and_store(paArray) {
      var i,serialized_info,arr;
      
      if (is_array(paArray)) {
        serialized_info = 'a:' + paArray.length + ':{';
        for (i=0; i<paArray.length; i++) {
          arr = paArray[i].split(':');
          serialized_info += 'i:' + arr[0] + ';' + 's:' + arr[1].length + ':"' + arr[1] + '";';
        }
        serialized_info += '}';
      }
      else {
        arr = paArray.split(':');
        serialized_info = 'a:1:{i:' + arr[0] + ';' + 's:' + arr[1].length + ':"' + arr[1] + '";' + '}';
      }
      
      gebi('contexts').value = serialized_info;
      
      i = serialized_info = arr = null;
    }
    </script>
         <?
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_trash.xml");
?>