<?php
include_once 'include.php';

include_once $handlers_ref . "QueryParameterNames.php";
include_once $handlers_ref . "QueryAllParameterValuesName.php";
include_once $handlers_ref . "QueryRCParameterValueNames.php";
include_once $handlers_ref . "QuerySpecialUsers.php";
include_once $handlers_ref . "QueryRiskUpdate.php";
include_once $handlers_ref . "QueryParameterValueCountUpdate.php";
include_once $handlers_ref . "QueryRiskProbabilityUpdate.php";
include_once $handlers_ref . "QueryContextClassification.php";
include_once $handlers_ref . "QueryControlCostNames.php";
include_once $handlers_ref . "QueryGetAllRiskControlIds.php";
include_once $handlers_ref . "QueryUpdateAllValues.php";
include_once $classes_isms_ref . "nonauto/admin/ISMSDefaultConfig.php";

class SaveCustomNonConformityTypesEvent extends FWDRunnable {
	public function run(){
		$msScript = '';
		$initId = 62250; //para manter a compatibilidade dos ids anteriores
		for($i=1;$i<=16;$i++){
			$miId = ($initId + $i);
			$msName = FWDWebLib::getObject("non_conform_text_$i")->getValue();
			$mbChecked = FWDWebLib::getObject("non_conform_checkbox_$i")->getAttrCheck() && $msName;
	
			if($mbChecked){		
				$moNonConformityTypes = new ISMSNonConformityTypes();
				$fetched = $moNonConformityTypes->fetchById($miId);

				if(empty($fetched)){
					//inserir
					$moNonConformityTypes->setFieldValue('non_conform_id',$miId);
					$moNonConformityTypes->setFieldValue('non_conform_name',$msName);
					$moNonConformityTypes->insert(true);
				} else {
					//update
					$moNonConformityTypes->setFieldValue('non_conform_name',$msName);
					$moNonConformityTypes->update($miId);
				}
			} else {
				//delete
				$moNonConformityTypes = new ISMSNonConformityTypes();
				$moNonConformityTypes->delete($miId);
				$msScript.= "gebi('non_conform_text_$i').value='';";
			}
			
		}	
		echo "{$msScript} gobi('save_warning').show(); setTimeout('gobi(\\'save_warning\\').hide()',2000);";
	}
}

class RestoreLogosEvent extends FWDRunnable {
	public function run(){
	  	$moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());

      	$msDirectory = (ISMSLib::isCompiled() ? $GLOBALS['custom_gfx_ref'] : '../../custom_gfx/');

      	$logoFilePath = $msDirectory . 'custom_logo.png';
      	$logoSmallFilePath = $msDirectory . 'custom_logo_small.png'; 
      	
      	if(file_exists($logoFilePath))
      		unlink($logoFilePath);

      	if(file_exists($logoSmallFilePath))
      		unlink($logoSmallFilePath);
      	$config = new ISMSConfig();
      	$config->setConfig(LIGHT_COLOR, "");
      	$config->setConfig(DARK_COLOR, "");
      		
		echo "gobi('save_warning_after_login').show(); setTimeout('gobi(\\'save_warning_after_login\\').hide()',7000);";
	}
}

class SaveCustomInterfaceCompleteEvent extends FWDRunnable {
	public function run(){
	  	$moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
	  	
	  	if($moSession->attributeExists('logosSaveComplete')){
			if($moSession->attributeExists('uploadErrorCode') && $moSession->getAttrUploadErrorCode()){
				// Trata erro no upload
				$miErrorCode = $moSession->getAttrUploadErrorCode();
				
				if($miErrorCode & FWDFile::E_MAX_SIZE){
					echo "gobi('warning_max_size').show();";
				}else{
					echo "gobi('warning_upload_error').show();";
				}		
			}
			else if($moSession->attributeExists('invalidImageError'))
			{
				echo "gobi('warning_invalid_image_type').show(); setTimeout('gobi(\\'warning_invalid_image_type\\').hide()',5000);";
			}
			else {
				echo "gobi('save_warning_after_login').show(); setTimeout('gobi(\\'save_warning_after_login\\').hide()',5000);";
			}
	  	} else {
	  		echo "setTimeout('trigger_event(\"save_custom_interface_complete_event\",3)',200);";
	  	}

	}
}

class SubmitEvent extends FWDRunnable {
	public function run(){
	    $moWebLib = FWDWebLib::getInstance();
	    $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
	    
	    $logoFile = FWDWebLib::getObject('logo');
	    $uploadedLogoFile = $logoFile->isUploaded();
	    
	    $logoFileError = 0;
	    
	    if($uploadedLogoFile){
	    	$logoFileError = $logoFile->getErrorCode();
	      	if($logoFileError==FWDFile::E_NONE){
		        
	      		$msDirectory = (ISMSLib::isCompiled() ? $GLOBALS['custom_gfx_ref'] : '../../custom_gfx/');

	      		if(is_writable($msDirectory)){
				  $logoFile->copyTo($msDirectory, "custom_logo.png");

				  if(!$this->resizeImage($msDirectory . "custom_logo.png", 266, 84)){
				  	unlink($msDirectory . "custom_logo.png");
				  	
	  		        if(!$moSession->attributeExists('invalidImageError')){
			          $moSession->addAttribute('invalidImageError');
					}				  	
				  }				  
				  
		        } else {
	      			trigger_error("Directory '{$msDirectory}' is not writable.",E_USER_ERROR);
		        }
	      	} else {
	
	      		if(!$moSession->attributeExists('uploadErrorCode')){
		          $moSession->addAttribute('uploadErrorCode');
		        }
		        $moSession->setAttrUploadErrorCode($logoFileError);   
	      		
	      	}
	    }
	    
	    $logoSmallFile = FWDWebLib::getObject('logoSmall');
	    $uploadedLogoSmallFile = $logoSmallFile->isUploaded();
	    
	    $logoSmallFileError = 0;       
	    
	    if($uploadedLogoSmallFile){
	    	$logoSmallFileError = $logoSmallFile->getErrorCode();
	      	if($logoSmallFileError==FWDFile::E_NONE){
		        
	      		$msDirectory = (ISMSLib::isCompiled() ? $GLOBALS['custom_gfx_ref'] : '../../custom_gfx/');
	
	      		if(is_writable($msDirectory)){
				  $logoSmallFile->copyTo($msDirectory, "custom_logo_small.png");

				  if(!$this->resizeImage($msDirectory . "custom_logo_small.png", 133, 42)){
				  	unlink($msDirectory . "custom_logo_small.png");
				  	
	  		        if(!$moSession->attributeExists('invalidImageError')){
			          $moSession->addAttribute('invalidImageError');
					}				  	
				  }
	  
		        } else {
	      			trigger_error("Directory '{$msDirectory}' is not writable.",E_USER_ERROR);
	      		}
	      	} else {
	
		        if(!$moSession->attributeExists('uploadErrorCode')){
		          $moSession->addAttribute('uploadErrorCode');
		        }
		        $moSession->setAttrUploadErrorCode($logoSmallFileError);
		                 		
	      	}
		
	    }
	    
		// Sinaliza que, com ou sem arquivo, com ou sem erro, acabou o submit
		if(!$moSession->attributeExists("logosSaveComplete")){
			$moSession->addAttribute("logosSaveComplete");
		}
		$moSession->setAttrLogosSaveComplete(true);
	}
	
	// Ideia: mover para uma lib que trata imagens.
	public function resizeImage($filename, $width, $height){
		$image = null;
		$image_info = getimagesize($filename);
		$image_type = $image_info[2];
		
		if($image_type == IMAGETYPE_JPEG) {
			$image = imagecreatefromjpeg($filename);
		} elseif($image_type == IMAGETYPE_GIF) {
			$image = imagecreatefromgif($filename);
		} elseif($image_type == IMAGETYPE_PNG) {
			$image = imagecreatefrompng($filename);
		} else {
			return false;
		}
		
		$imageW = imagesx($image);
		$imageH = imagesy($image);
		$newW = 0;
		$newH = 0;
		
		if($imageW > $imageH){
			$newW = $height*imagesx($image)/imagesy($image);
			$newH = $height;
		} else if($imageW <= $imageH)
		{
			$newW = $width;
			$newH = $width*imagesy($image)/imagesx($image);
		} 
		
	    $new_image = imagecreatetruecolor($width, $height);
		$white = imagecolorallocate($new_image, 255, 255, 255);
		imagefill($new_image, 0, 0, $white); 	    
	    
		imagecopyresampled($new_image, $image, 0, 0, 0, 0, $newW, $newH, $imageW, $imageH);
	    imagepng($new_image, $filename);
	    
	    return true;
	}
}

class SaveCustomInterfaceEvent extends FWDRunnable {
	public function run() {
		$moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
		
		// previne bugs com lixo na sessao.
		if($moSession->attributeExists('uploadErrorCode'))
			$moSession->deleteAttribute('uploadErrorCode');

		if($moSession->attributeExists('invalidImageError'))
			$moSession->deleteAttribute('invalidImageError');
			
		if($moSession->attributeExists('logosSaveComplete'))
			$moSession->deleteAttribute('logosSaveComplete');			
		
		$config = new ISMSConfig();
		$lightColor = FWDWebLib::getPOST('lightColor');
		if ($lightColor){
			$config->setConfig(LIGHT_COLOR, $lightColor);
		}
		
		$darkColor = FWDWebLib::getPOST('darkColor');
		if ($darkColor){
			$config->setConfig(DARK_COLOR, $darkColor);
		}
			
		echo "js_submit('upload','ajax'); setTimeout('trigger_event(\"save_custom_interface_complete_event\",3)',200);";
	}
}

class SaveCurrencyEvent extends FWDRunnable {
	public function run() {
		$moConfig = new ISMSConfig();
		$msConfig = FWDWebLib::getPOST('currency');
		if ($msConfig){
			$moConfig->setConfig(GENERAL_CURRENCY_IDENTIFIER, $msConfig);
		}
		echo "gobi('save_warning').show(); setTimeout('gobi(\\'save_warning\\').hide()',5000);";
	}
}

class SaveClassificationEvent extends FWDRunnable {
  public function run() { 
    $mbError = false;
    $maSelectedItems = FWDWebLib::getObject("classification_controller")->getAllItemsCheck();
    $miTotalAreaTypes = $miTotalAreaPrios = $miTotalProcessTypes = $miTotalProcessPrios = $miTotalRiskTypes = $miTotalEventTypes = $miTotalControlTypes = $miTotalDocumentTypes = $miTotalRegisterTypes = 0;
    foreach ($maSelectedItems as $moItem) {
      switch(substr($moItem->getAttrKey(),0,12)) {
        case "cl_type_area" : $miTotalAreaTypes++; break;
        case "cl_prio_area" : $miTotalAreaPrios++; break;
        case "cl_type_proc" : $miTotalProcessTypes++; break;
        case "cl_prio_proc" : $miTotalProcessPrios++; break;
        case "cl_type_risk" : $miTotalRiskTypes++; break;
        case "cl_type_even" : $miTotalEventTypes++; break;
        case "cl_type_cont" : $miTotalControlTypes++; break;
        case "cl_type_docu" : $miTotalDocumentTypes++; break;
        case "cl_type_regi" : $miTotalRegisterTypes++; break;
      }
    }
    
    if ( (($miTotalAreaTypes<3)||($miTotalAreaPrios<3) ) && (FWDWebLib::getObject("cl_area")->getShouldDraw()) ) {
      echo "gebi('classification_select').selectedIndex = 0;"
          ."hack_change_classification ('cl_area');";
      $mbError = true;
    }
    elseif ( (($miTotalProcessTypes<3)||($miTotalProcessPrios<3) ) && (FWDWebLib::getObject("cl_process")->getShouldDraw()) ) {
      echo "gebi('classification_select').selectedIndex = 1;"
           ."hack_change_classification ('cl_process');";
      $mbError = true;
    }
    elseif ( ($miTotalRiskTypes<3) && (FWDWebLib::getObject("cl_risk")->getShouldDraw()) ) {
      echo "gebi('classification_select').selectedIndex = 2;"
           ."hack_change_classification ('cl_risk');";
      $mbError = true;
    }
    elseif ( ($miTotalEventTypes<3) && (FWDWebLib::getObject("cl_event")->getShouldDraw()) ) {
      echo "gebi('classification_select').selectedIndex = 3;"
           ."hack_change_classification ('cl_event');";
      $mbError = true;
    }
    elseif ( ($miTotalControlTypes<3) && (FWDWebLib::getObject("cl_control")->getShouldDraw()) ) {
      echo "gebi('classification_select').selectedIndex = 4;"
           ."hack_change_classification ('cl_control');";
      $mbError = true;
    }
    elseif ( ($miTotalDocumentTypes<3) && (FWDWebLib::getObject("cl_document")->getShouldDraw()) ) {    	 
      echo "gebi('classification_select').selectedIndex = 5;"
           ."hack_change_classification ('cl_document');";
      $mbError = true;
    }
    elseif ( ($miTotalRegisterTypes<3) && (FWDWebLib::getObject("cl_register")->getShouldDraw()) ) {
      echo "gebi('classification_select').selectedIndex = 6;"
           ."hack_change_classification ('cl_register');";
      $mbError = true;
    }
    if ($mbError) {
      echo "gobi('classification_error').show();";
      return;
    }
    
    $moQueryContextClassification = new QueryContextClassification(FWDWebLib::getConnection());
    $moQueryContextClassification->makeQuery();
    $moQueryContextClassification->executeQuery();
    $maClassificationNames = $moQueryContextClassification->getClassificationNames();
    $maClassificationIds = $moQueryContextClassification->getClassificationIds();
          
    $maNewClassifications = array();
    $maSelectedItems = FWDWebLib::getObject("classification_controller")->getAllItemsCheck();

    foreach ($maSelectedItems as $moItem) {
      $msKey = $moItem->getAttrKey();
      $msName = trim(FWDWebLib::getObject($msKey)->getValue());
      
      if ($msName) {
        $maNewClassifications[$msKey] = $msName;
        $moISMSContextClassification = new ISMSContextClassification();
        $moISMSContextClassification->setFieldValue('classif_name',$msName);
        
        $labels = array('cl_prio_area', 'cl_prio_process');
        $weight = (int) str_replace($labels, '', $msKey);
        if($weight > 0)
        	$moISMSContextClassification->setFieldValue('classif_weight', $weight);
        
        // atualiza classifica��o
        if (isset($maClassificationIds[$msKey])) {
          $moISMSContextClassification->update($maClassificationIds[$msKey]);
        }
        // insere classifica��o
        else {
          $miClassifContextType =
            strstr($msKey,"area") ? CONTEXT_AREA :
            (strstr($msKey,"process") ? CONTEXT_PROCESS :
              (strstr($msKey,"risk") ? CONTEXT_RISK :
                (strstr($msKey,"event") ? CONTEXT_EVENT :
                  (strstr($msKey,"control") ? CONTEXT_CONTROL :
                    (strstr($msKey,"document") ? CONTEXT_DOCUMENT : 
                      (strstr($msKey,"register") ? CONTEXT_REGISTER : -1))))));
                
          $miClassifType =
            strstr($msKey,"type") ? CONTEXT_CLASSIFICATION_TYPE :
            (strstr($msKey,"prio") ? CONTEXT_CLASSIFICATION_PRIORITY : -1);
          
          if ($miClassifContextType<0 || $miClassifType<0)
            trigger_error("Invalid Classification Field: $msKey",E_USER_ERROR);
          
          $moISMSContextClassification->setFieldValue('classif_context_type',$miClassifContextType);
          $moISMSContextClassification->setFieldValue('classif_type',$miClassifType);
          
          $labels = array('cl_prio_area', 'cl_prio_process');
          $weight = (int) str_replace($labels, '', $msKey);
          if($weight > 0)
             $moISMSContextClassification->setFieldValue('classif_weight', $weight);
          
          $moISMSContextClassification->insert();
        }
        unset($moISMSContextClassification);
      }
    }
    /* php 5.0.x nao possui a fun��o array_diff_key */
    // deleta classifica��es
    $maDeleteIdsByFieldName = FWDWebLib::fwd_array_diff_key($maClassificationIds,$maNewClassifications);
    $msCleanFields = "";
    if (count($maDeleteIdsByFieldName)>0) {
      $moISMSContextClassification = new ISMSContextClassification();
      $maNotDeletable = array();
      foreach ($maDeleteIdsByFieldName as $msFieldName => $miClassificationId) {
        if (strstr($msFieldName,"area")) {
          $moContextType = new RMArea();
          $msPrefix = "area_";
        } elseif (strstr($msFieldName,"process")) {
          $moContextType = new RMProcess();
          $msPrefix = "process_";
        } elseif (strstr($msFieldName,"risk")) {
          $moContextType = new RMRisk();
          $msPrefix = "risk_";
        } elseif (strstr($msFieldName,"event")) {
          $moContextType = new RMEvent();
          $msPrefix = "event_";
        } elseif (strstr($msFieldName,"control")) {
          $moContextType = new RMControl();
          $msPrefix = "control_";
        } elseif (strstr($msFieldName,"document")) {
          $moContextType = new PMDocument();
          $msPrefix = "document_";
        } elseif (strstr($msFieldName,"register")) {
          $moContextType = new PMRegister();
          $msPrefix = "register_";
        }
        $msField = 
          strstr($msFieldName,"type") ? $msPrefix."type" :
            (strstr($msFieldName,"prio") ? $msPrefix."priority" : "");

        //<POG>
        // Os alias dos campos de documentos e registros n�o seguem o padr�o, at� porque tipo de documento � outra coisa
        if($msField=='document_type'){
          $msField = 'document_classification';
        }elseif($msField=='register_type'){
          $msField = 'register_classification';
        }
        //</POG>

        $moContextType->createFilter($miClassificationId,$msField);
        $moContextType->select();
        if ($moContextType->fetch()) {
          $maNotDeletable[] = $maClassificationNames[$msFieldName];
          continue;
        }
        $moISMSContextClassification->delete($miClassificationId);
        $moJs = new FWDJsEvent(JS_SET_CONTENT,$msFieldName,"");
        $msCleanFields .= $moJs->render();
      }
      if (count($maNotDeletable) > 0) {
        $msTitle = FWDLanguage::getPHPStringValue('tt_remove_classification_error','Erro ao remover Classifica��o');
        $msMessage = FWDLanguage::getPHPStringValue('st_remove_classification_error',"Suas altera��es foram salvas. No entanto, n�o foi poss�vel remover as seguintes classifica��es, pois est�o sendo usadas por um ou mais elementos: <b>%not_deletable%</b>.");
        $msMessage = str_replace("%not_deletable%",implode(", ",$maNotDeletable),$msMessage);
        ISMSLib::openOk($msTitle,$msMessage,"",60);
      }
    }
    echo $msCleanFields;
    echo "gobi('save_warning').show();"
         ."setTimeout('gobi(\\'save_warning\\').hide();',5000);";
  }
}

Class OpenRiskParametersNamesConfirm extends FWDRunnable {
  public function run() {
    $maPNameAssociation = FWDWebLib::unserializeString(FWDWebLib::getObject("risk_par_association")->getValue());
    $maSelectedItems = FWDWebLib::getObject("risk_par_controller")->getAllItemsCheck();
    if (count($maSelectedItems)<1) {
      echo "gobi('risk_parameters_names_error').show();";
      return;
    }
    if (count($maPNameAssociation) != count($maSelectedItems)) {
      $msTitle = FWDLanguage::getPHPStringValue('tt_save_parameters_names','Salvar Nomes de Par�metros');
      $msMessage = FWDLanguage::getPHPStringValue('st_change_amount_of_parameters_confirm',"Alterar a quantidade de par�metros resultar� na desparametriza��o do sistema.<BR><b>Voc� tem certeza que deseja fazer isso?</b>");
      $msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm').getOpener().save_pname();";
      ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,50);
    }else {
      echo " save_pname(); ";
    }
  }
}

class SaveRiskParametersNamesEvent extends FWDRunnable {
  public function run() {
    $maPNameAssociation = FWDWebLib::unserializeString(FWDWebLib::getObject("risk_par_association")->getValue());
    $maPNameAssociationNew = array();
    $moControllerPName = FWDWebLib::getObject("risk_par_controller");
    $maSelectedItems = $moControllerPName->getAllItemsCheck();
    $maNewRiskParItens = array();
    
    foreach ($maSelectedItems as $moItem) {
      $msKey = $moItem->getAttrKey();
      $msPNameName = trim(FWDWebLib::getObject($msKey)->getValue());
      if ($msPNameName) {
        $moPName = new RMParameterName();
        $moPName->setFieldValue('parametername_name', $msPNameName);
        if (isset($maPNameAssociation[$msKey])) {
          $moPName->update($maPNameAssociation[$msKey]);
          $maPNameAssociationNew[$msKey] = $maPNameAssociation[$msKey];
          unset($maPNameAssociation[$msKey]);
        }else {
          $miPNameID = $moPName->insert(true);
          $maPNameAssociationNew[$msKey] = $miPNameID;
          $maNewRiskParItens[] = $miPNameID;
        }
      }
    }
    // Deleta os parametros deselecionados
    $msCleanFields = "";
    if (count($maPNameAssociation) != 0) {
      foreach ($maPNameAssociation as $msMey => $miId) {
        $moPName = new RMParameterName();
        $moPName->delete($miId);
        $moJs = new FWDJsEvent(JS_SET_CONTENT,$msMey,"");
        $msCleanFields .= $moJs->render();
      }
    }
    
    //cria os novos parametros para a rela��o de risco x controle com o valor 'Do Not Affect'
    if(count($maNewRiskParItens)){
      $moRCQuery = new QueryGetAllRiskControlIds(FWDWebLib::getConnection());
      $maValues = $moRCQuery->getValue();
      $moRiskControl = new RMRiskControl();
      $moRiskControl->select();
      foreach($maValues as $miRCId){
        foreach($maNewRiskParItens as $miValue){
          $moRCValue = new RMRiskControlValue();
          $moRCValue->setFieldValue('rcvalue_parameter_name_id',$miValue);
          $moRCValue->setFieldValue('rcvalue_value_name_id', 1);
          $moRCValue->setFieldValue('rcvalue_rc_id', $miRCId);
          $moRCValue->insert();
        }
      }
    }
    
    //Atualiza os valores de risco
    $moQueryRiskUpdate = new QueryRiskUpdate(FWDWebLib::getConnection());
    $moQueryRiskUpdate->makeQuery();
    $moQueryRiskUpdate->executeQuery();
    
    $msPNameAssociation = serialize($maPNameAssociationNew);
    $msPNameAssociation = str_replace('\\','\\\\"',$msPNameAssociation);
    $msPNameAssociation = str_replace('"','\"',$msPNameAssociation);
    echo $msCleanFields;
    echo "gebi('risk_par_association').value = '$msPNameAssociation';";
    echo "gobi('save_warning').show();"
        ."setTimeout('gobi(\\'save_warning\\').hide()',5000);";
  }
}

class OpenNumberOfRiskParametersConfirm extends FWDRunnable {
  public function run() {
    $miPreviousRiskValueCount = ISMSLib::getConfigById(RISK_VALUE_COUNT);
    $miRiskValueCount = ltrim(FWDWebLib::getObject("risk_value_count")->getValue(),":");
    if ($miPreviousRiskValueCount != $miRiskValueCount) {
      $msTitle = FWDLanguage::getPHPStringValue('tt_save_parameters_values','Salvar Valores de Par�metros');
      $msMessage = FWDLanguage::getPHPStringValue('st_change_amount_of_risks_confirm',"Alterar a quantidade de riscos do sistema resultar� na necessidade de refazer todos os c�lculos de risco no sistema!<BR><b>Voc� tem certeza que deseja fazer isso?</b>");
      
      $msEventValue = " soPopUpManager.getPopUpById('popup_confirm').getOpener().save_number_of_risk_parameters_event();";
      ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,75);
    }
    else {
      echo " save_number_of_risk_parameters_event(); ";
    }
  }
}

class SaveNumberOfRiskParameters extends FWDRunnable {
  public function run() {
    set_time_limit(3000);
    $miRiskCount = ltrim(FWDWebLib::getObject("risk_value_count")->getValue(),":");
    $miPreviousRiskValueCount = ISMSLib::getConfigById(RISK_VALUE_COUNT);
    $maRiskControlIds = FWDWebLib::unserializeString(FWDWebLib::getObject('var_risk_control_ids')->getValue());
    $maAssetRiskIds = FWDWebLib::unserializeString(FWDWebLib::getObject('var_asset_risk_ids')->getValue());
    $maAssetRiskIdsNew = array();
    $maRiskControlIdsNew = array();
    $miOptimist = ltrim(FWDWebLib::getObject("optimist_choice")->getValue(),":");
    $mbOptimist = $miOptimist==1?true:false;
    $msEcho = "";
    for ($miI=1; $miI<=$miRiskCount; $miI++){
      //Salva a tela Ativo e Risco
      $moPValue = new RMParameterValueName();
      $moPValue->setFieldValue('parametervalue_value', $miI);
      $moPValue->setFieldValue('parametervalue_importance', FWDWebLib::getObject("importance".$miRiskCount."_".$miI)->getValue());
      $moPValue->setFieldValue('parametervalue_impact', FWDWebLib::getObject("impact".$miRiskCount."_".$miI)->getValue());
      $moPValue->setFieldValue('parametervalue_rprob', FWDWebLib::getObject("riskprob".$miRiskCount."_".$miI)->getValue());
      if(isset($maAssetRiskIds[$miI])){
        $moPValue->update($maAssetRiskIds[$miI]);
        $maAssetRiskIdsNew[$miI] = $maAssetRiskIds[$miI];
      }else{
        $miNewId = $moPValue->insert(true);
        $maAssetRiskIdsNew[$miI] = $miNewId;
      }
      //Salva a tela Risco x Controle
      $moRCPValue = new RMRCParameterValueName();
      $moRCPValue->setFieldValue('rcparametervalue_value', ($miI-1));
      $moRCPValue->setFieldValue('rcparametervalue_impact', FWDWebLib::getObject("rc_impact".$miRiskCount."_".($miI-1))->getValue());
      $moRCPValue->setFieldValue('rcparametervalue_prob',FWDWebLib::getObject("rc_prob".$miRiskCount."_".($miI-1))->getValue());
      if(isset($maRiskControlIds[$miI-1])){
        $moRCPValue->update($maRiskControlIds[$miI-1]);
        $maRiskControlIdsNew[$miI-1] = $maRiskControlIds[$miI-1];
      }else{
        $miNewId = $moRCPValue->insert(true);
        $maRiskControlIdsNew[$miI-1] = $miNewId;
      }
    }
    
    //Atualiza os valores de risco, caso o numero de parametros seja diferente.
    if ($miRiskCount != $miPreviousRiskValueCount) {
      $moQueryParameterValueCountUpdate = new QueryParameterValueCountUpdate(FWDWebLib::getConnection());
      $moQueryParameterValueCountUpdate->setValueCount($miRiskCount);
      $moQueryParameterValueCountUpdate->setOptimist($mbOptimist);
      $moQueryParameterValueCountUpdate->makeQuery();
      $moQueryParameterValueCountUpdate->executeQuery(0,0,true);
      //atualiza o ISMSConfig
      $miRiskLow = ISMSLib::getConfigById(RISK_LOW);
      $miRiskHigh = ISMSLib::getConfigById(RISK_HIGH);

      $riskLevel = ISMSLib::getConfigById(RISK_LEVEL);

      if($riskLevel == 5){
      	$miRiskMidLow = ISMSLib::getConfigById(RISK_MID_LOW);
      	$miRiskMidHigh = ISMSLib::getConfigById(RISK_MID_HIGH);
      }
      
      $miPreviousPow = $miPreviousRiskValueCount * $miPreviousRiskValueCount;
      $miAtualPow = $miRiskCount * $miRiskCount;
      $miNewRiskHigh = min(number_format( ( $miRiskHigh * $miAtualPow ) / $miPreviousPow,0), $miAtualPow );
      $miNewRiskLow = min(number_format( ( $miRiskLow * $miAtualPow ) / $miPreviousPow,0), $miAtualPow );
      
      if($riskLevel == 5){
      	$miNewRiskMidHigh = min(number_format( ( $miRiskMidHigh * $miAtualPow ) / $miPreviousPow,0), $miAtualPow );
      	$miNewRiskMidLow = min(number_format( ( $miRiskMidLow * $miAtualPow ) / $miPreviousPow,0), $miAtualPow );
      }
      
      //garante que o valor minimo dos limites de risco seja 1
      if($miNewRiskHigh <1){$miNewRiskHigh =1;}
      if($miNewRiskLow <1){$miNewRiskLow =1 ;}
      
      if($riskLevel == 5){
      	if($miNewRiskMidHigh <1){$miNewRiskMidHigh =1;}
      	if($miNewRiskMidLow <1){$miNewRiskMidLow =1 ;}      
      }
      
      $moRiskLimits = new RMRiskLimits();
      $moRiskLimits->setFieldValue('risk_limits_low' ,$miNewRiskLow);
      $moRiskLimits->setFieldValue('risk_limits_high',$miNewRiskHigh);
      
      if($riskLevel == 5){
      	$moRiskLimits->setFieldValue('risk_limits_mid_low', $miNewRiskMidLow);
      	$moRiskLimits->setFieldValue('risk_limits_mid_high', $miNewRiskMidHigh);
      } else {
      	$moRiskLimits->setFieldValue('risk_limits_mid_low', 0);
      	$moRiskLimits->setFieldValue('risk_limits_mid_high', 0);
      }
      
      $moRiskLimits->insert();

      ISMSLib::setConfigById(RISK_LOW, $miNewRiskLow);
      ISMSLib::setConfigById(RISK_HIGH,$miNewRiskHigh);
      
      if($riskLevel == 5){
      	ISMSLib::setConfigById(RISK_MID_HIGH,$miNewRiskMidHigh);
      	ISMSLib::setConfigById(RISK_MID_LOW,$miNewRiskMidLow);
      }
      
      if($moRiskLimits->getApprover() == ISMSLib::getCurrentUserId()){
      	
      	if($riskLevel == 3){
      		$msEcho .="gebi('risk_limits_1').value = $miNewRiskLow;";
        	$msEcho .="gebi('risk_limits_2').value = $miNewRiskHigh;";
        }else if($riskLevel == 5){
        	$msEcho .="gebi('risk_limits_1').value = $miNewRiskLow;";
        	$msEcho .="gebi('risk_limits_2').value = $miNewRiskMidLow;";
        	
        	$msEcho .="gebi('risk_limits_3').value = $miNewRiskMidHigh;";
        	$msEcho .="gebi('risk_limits_4').value = $miNewRiskHigh;";
        }        
        
      }else{
        $moUser = new ISMSUser();
        $moUser->fetchById($moRiskLimits->getApprover());
        $msWarning = FWDWebLib::getObject('wn_user_is_not_chairman')->getValue();
        $msWarning = str_replace("%chairman_name%", $moUser->getName(), $msWarning);
        $msEcho .= "gobi('wn_user_is_not_chairman').setValue('$msWarning');";
        $msEcho .= "gobi('wn_user_is_not_chairman').show();";
        $msEcho .= "hack_change_customize('custom_quantity_rm_parameters');";
      }
      
      //Atualiza os valores de risco
      $moQueryRiskUpdate = new QueryRiskUpdate(FWDWebLib::getConnection());
      $moQueryRiskUpdate->makeQuery();
      $moQueryRiskUpdate->executeQuery();
      // Atualiza os limites de quantidade de incidente para o Calculo Autom�tico de probabilidade
      if(ISMSLib::hasModule(INCIDENT_MODE)){
        $moQueryRiskProbabilityUpdate = new QueryRiskProbabilityUpdate();
        $moQueryRiskProbabilityUpdate->setNewValuesCount($miRiskCount);
        $moQueryRiskProbabilityUpdate->makeQuery();
        $moQueryRiskProbabilityUpdate->executeQuery();
      }
    }
    //Apaga 4 e 5 quando passar de 5 para 3
    if ( ($miRiskCount==3) && (ISMSLib::getConfigById(RISK_VALUE_COUNT)==5) ) {
      $moPValue = new RMParameterValueName();
      $moRCPValue = new RMRCParameterValueName();
      for ($miI=4; $miI<=5; $miI++) {
        if ($maAssetRiskIds[$miI]) {
          $moPValue->delete($maAssetRiskIds[$miI]);
        }
        if ($maRiskControlIds[$miI-1]) {
          $moRCPValue->delete($maRiskControlIds[$miI-1]);
        }
      }
    }


    $maRiskControlIds = serialize($maRiskControlIdsNew);
    $maRiskControlIds = str_replace('\\','\\\\"',$maRiskControlIds);
    $maRiskControlIds = str_replace('"','\"',$maRiskControlIds);
    echo "gebi('var_risk_control_ids').value = '$maRiskControlIds';";
    $maAssetRiskIds = serialize($maAssetRiskIdsNew);
    $maAssetRiskIds = str_replace('\\','\\\\"',$maAssetRiskIds);
    $maAssetRiskIds = str_replace('"','\"',$maAssetRiskIds);
    echo "gebi('var_asset_risk_ids').value = '$maAssetRiskIds';";
    //seta o novo valor da quantidade de riscos do sistema
    ISMSLib::setConfigById(RISK_VALUE_COUNT,$miRiskCount);



    echo $msEcho;
    echo "gebi('var_risk_value_count').value = '$miRiskCount';";
    echo "gobi('save_warning').show(); setTimeout('gobi(\\'save_warning\\').hide()',5000);";
  }
}

class SaveControlCostNamesEvent extends FWDRunnable {
  public function run() {
    ISMSLib::setConfigById(CONTROL_COST_1,FWDWebLib::getObject("cname1")->getValue());
    ISMSLib::setConfigById(CONTROL_COST_2,FWDWebLib::getObject("cname2")->getValue());
    ISMSLib::setConfigById(CONTROL_COST_3,FWDWebLib::getObject("cname3")->getValue());
    ISMSLib::setConfigById(CONTROL_COST_4,FWDWebLib::getObject("cname4")->getValue());
    ISMSLib::setConfigById(CONTROL_COST_5,FWDWebLib::getObject("cname5")->getValue());
    echo "gobi('save_warning').show(); setTimeout('gobi(\\'save_warning\\').hide()',5000);";
  }
}

class SaveFinancialImpactParametersNamesEvent extends FWDRunnable {
  public function run() {
    $msScript = '';
    if(ISMSLib::hasModule(INCIDENT_MODE)){
      $moContextClassification = new ISMSContextClassification();
      $moContextClassification->createFilter(CONTEXT_CLASSIFICATION_FINANCIAL_IMPACT,'classif_type');
      $moContextClassification->select();
      
      for($i=1;$i<=8;$i++){
        $miId = FWDWebLib::getObject("fin_imp_id_$i")->getValue();
        $msName = FWDWebLib::getObject("fin_imp_text_$i")->getValue();
        $mbChecked = FWDWebLib::getObject("fin_imp_checkbox_$i")->getAttrCheck() && $msName;
        if($miId){
          if($mbChecked){
            // update
            $moContextClassification = new ISMSContextClassification();
            $moContextClassification->setFieldValue('classif_name',$msName);
            $moContextClassification->update($miId);
          }else{
            // delete
            $moIncFinancialImpact = new CIIncidentFinancialImpact();
            $moIncFinancialImpact->createFilter($miId,'classification_id');
            $moIncFinancialImpact->delete();

            $moContextClassification = new ISMSContextClassification();
            $moContextClassification->delete($miId);
            $msScript.= "gebi('fin_imp_id_$i').value='';";
            $msScript.= "gebi('fin_imp_text_$i').value='';";
          }
        }elseif($mbChecked){
          // insert
          $moContextClassification = new ISMSContextClassification();
          $moContextClassification->setFieldValue('classif_name',$msName);
          $moContextClassification->setFieldValue('classif_context_type',CONTEXT_CI_INCIDENT);
          $moContextClassification->setFieldValue('classif_type',CONTEXT_CLASSIFICATION_FINANCIAL_IMPACT);
          $miId = $moContextClassification->insert(true);
          $msScript.= "gebi('fin_imp_id_$i').value=$miId;";
        }
      }
      
      $miCounter = 0;
      while($moContextClassification->fetch()){
        $miId = $moContextClassification->getFieldByAlias('classif_id')->getValue();
        $msName = $moContextClassification->getFieldByAlias('classif_name')->getValue();
        $miCounter++;
        FWDWebLib::getObject('fin_imp_id_'.$miCounter)->setValue($miId);
        FWDWebLib::getObject('fin_imp_text_'.$miCounter)->setValue($msName);
        FWDWebLib::getObject('fin_imp_text_'.$miCounter)->setAttrDisabled('false');
        FWDWebLib::getObject('fin_imp_checkbox_'.$miCounter)->setAttrCheck('true');
      }
    }
    
    echo "{$msScript} gobi('save_warning').show(); setTimeout('gobi(\\'save_warning\\').hide()',2000);";
  }
}



class SaveSpecialUsersEvent extends FWDRunnable {
  public function run() {
    ISMSLib::setConfigById(USER_CHAIRMAN,           FWDWebLib::getObject('user_chairman_id')->getValue());
    ISMSLib::setConfigById(USER_ASSET_CONTROLLER,   FWDWebLib::getObject('user_asset_controller_id')->getValue());
    ISMSLib::setConfigById(USER_CONTROL_CONTROLLER, FWDWebLib::getObject('user_control_controller_id')->getValue());
    ISMSLib::setConfigById(USER_LIBRARIAN,          FWDWebLib::getObject('user_librarian_id')->getValue());
    
    if (ISMSLib::hasModule(INCIDENT_MODE)) {
      ISMSLib::setConfigById(USER_INCIDENT_MANAGER,              FWDWebLib::getObject('user_incident_manager_id')->getValue());
      ISMSLib::setConfigById(USER_DISCIPLINARY_PROCESS_MANAGER,  FWDWebLib::getObject('user_disciplinary_process_manager_id')->getValue());
      ISMSLib::setConfigById(USER_NON_CONFORMITY_MANAGER,        FWDWebLib::getObject('user_non_conformity_manager_id')->getValue());
      ISMSLib::setConfigById(USER_EVIDENCE_MANAGER,              FWDWebLib::getObject('user_evidence_manager_id')->getValue());
      ISMSLib::setConfigById(USER_DOCUMENT_AUDITOR,              FWDWebLib::getObject('user_document_auditor_id')->getValue());
    }
    echo "gobi('save_warning').show(); setTimeout('gobi(\\'save_warning\\').hide()',5000);";
  }
}


class SaveControlEvent extends FWDRunnable {
  public function run() {
    $maControlFields = array("control_revision"=>GENERAL_REVISION_ENABLED,"control_test"=>GENERAL_TEST_ENABLED);
    $maSelectedItems = FWDWebLib::getObject("control_controller")->getAllItemsCheck();
    //Atualiza os itens marcados
    $maCheckedItens = array();
    foreach ($maSelectedItems as $moItem) {
      $maCheckedItens[] = $moItem->getAttrKey();
    }
    foreach ($maControlFields as $miKey=>$miValue){
      if(in_array($miKey,$maCheckedItens)){
        ISMSLib::setConfigById($miValue,"1");
      }else{
        ISMSLib::setConfigById($miValue,"0");
      }
    }
    echo "gobi('save_warning').show();"
        ."setTimeout('gobi(\\'save_warning\\').hide()',5000);";
  }
}

Class SaveFeaturesEvent extends FWDRunnable {
  public function run() {
    $maFeatureslFields = array( "solicitor"           => GENERAL_SOLICITOR_ENABLED,
                                "percentual_risk"     => GENERAL_PERCENTUAL_RISK,
                                "auto_doc_creation"   => GENERAL_AUTOMATIC_DOCUMENTS,
                                "control_cost"        => GENERAL_COST_ENABLED,
                                "manual_versioning"   => GENERAL_DOCUMENTS_MANUAL_VERSIONING,
                                "manual_data_control" => GENERAL_MANUAL_DATA_CONTROL,
                                "data_collection"     => GENERAL_DATA_COLLECTION_ENABLED,
                                "risk_formula"        => RISK_FORMULA_TYPE
    );
    $maFeatures = FWDWebLib::getObject('system_features_controller')->getAllItemsCheck();
    //Atualiza os itens marcados
    $maCheckedItens = array();
    foreach ($maFeatures as $moItem) {
      $maCheckedItens[] = $moItem->getAttrKey();
    }
    $mbChangedToType1 = false;
    $mbChangedToType2 = false;
    foreach ($maFeatureslFields as $miKey=>$miValue){    
      if(in_array($miKey,$maCheckedItens)){
        if ($miKey == "risk_formula") {
          if (ISMSLib::getConfigById(RISK_FORMULA_TYPE) != RISK_FORMULA_TYPE_2) {
            $mbChangedToType2 = true;
          }            
          ISMSLib::setConfigById($miValue, RISK_FORMULA_TYPE_2);
        }
        else {
          ISMSLib::setConfigById($miValue,"1");
        }
      }else{
        if ($miKey == "risk_formula") {
          if (ISMSLib::getConfigById(RISK_FORMULA_TYPE) != RISK_FORMULA_TYPE_1) {
            $mbChangedToType1 = true;
          }
          ISMSLib::setConfigById($miValue, RISK_FORMULA_TYPE_1);
        }
        else {
          ISMSLib::setConfigById($miValue,"0");
        }
      }      
    }
    
    if ($mbChangedToType1 || $mbChangedToType2) {
      echo "gobi('risk_values_updating').show(); updateAllValues();";
    }    
    else {
      echo "gobi('save_warning').show(); setTimeout('gobi(\\'save_warning\\').hide()',5000);";
    }
  }
}

Class UpdateAllValuesEvent extends FWDRunnable {
  public function run() {    
    //Atualiza os valores de risco
    set_time_limit(3600);
    $moQueryRiskUpdate = new QueryUpdateAllValues(FWDWebLib::getConnection());
    $moQueryRiskUpdate->makeQuery();
    $moQueryRiskUpdate->executeQuery();
    echo "gobi('save_warning').hide();";
    echo "gobi('risk_values_updating').hide();";
    echo "gobi('risk_values_updated').show(); setTimeout('gobi(\\'risk_values_updated\\').hide()',5000);";
  }
}

function save_risk_limits($poContext){
  //teste para verificar se o sistema n�o est� sendo hakeado
  $moCtxUserTest = new RMRiskLimits();
  $moCtxUserTest->testPermissionToInsert();

  $poContext->insert();
  
  $riskLevel = ISMSLib::getConfigById(RISK_LEVEL);
  if($riskLevel == 3){
	ISMSLib::setConfigById(RISK_LOW, $poContext->getFieldValue('risk_limits_low'));
	ISMSLib::setConfigById(RISK_HIGH,$poContext->getFieldValue('risk_limits_high'));
  }else if($riskLevel == 5){
	ISMSLib::setConfigById(RISK_LOW, $poContext->getFieldValue('risk_limits_low'));
	ISMSLib::setConfigById(RISK_MID_LOW, $poContext->getFieldValue('risk_limits_mid_low'));
	ISMSLib::setConfigById(RISK_MID_HIGH, $poContext->getFieldValue('risk_limits_mid_high'));
	ISMSLib::setConfigById(RISK_HIGH,$poContext->getFieldValue('risk_limits_high'));  	
  }
  
  $msEcho = "";
  $moRiskLimits = new RMRiskLimits();
  if($moRiskLimits->getApprover() != ISMSLib::getCurrentUserId()){
    $moUser = new ISMSUser();
    $moUser->fetchById($moRiskLimits->getApprover());
    $msWarning = FWDWebLib::getObject('wn_user_is_not_chairman_risk_limits')->getValue();
    $msWarning = str_replace("%chairman_name%", $moUser->getName(), $msWarning);
    $msEcho .= "gobi('wn_user_is_not_chairman_risk_limits').setValue('$msWarning');";
    $msEcho .= "gobi('wn_user_is_not_chairman_risk_limits').show();";
  }

  echo $msEcho;
  echo "gobi('save_warning').show(); setTimeout('gobi(\\'save_warning\\').hide()',5000);";
}

class ConfirmEditEvent extends FWDRunnable {
  public function run(){
    $moRiskLimits = new RMRiskLimits();
    
    $riskLevel = ltrim(FWDWebLib::getObject('risk_level_value')->getValue(),":");
    if($riskLevel == 3){
	    $moRiskLimits->setFieldValue('risk_limits_low' ,FWDWebLib::getObject('risk_limits_1')->getValue());
	    $moRiskLimits->setFieldValue('risk_limits_high',FWDWebLib::getObject('risk_limits_2')->getValue());
	  	$moRiskLimits->setFieldValue('risk_limits_mid_low' , 0);
	    $moRiskLimits->setFieldValue('risk_limits_mid_high' , 0);
  	} else if($riskLevel == 5){
	    $moRiskLimits->setFieldValue('risk_limits_low' ,FWDWebLib::getObject('risk_limits_1')->getValue());
	    $moRiskLimits->setFieldValue('risk_limits_mid_low' ,FWDWebLib::getObject('risk_limits_2')->getValue());
	    $moRiskLimits->setFieldValue('risk_limits_mid_high' ,FWDWebLib::getObject('risk_limits_3')->getValue());
	    $moRiskLimits->setFieldValue('risk_limits_high',FWDWebLib::getObject('risk_limits_4')->getValue());  		
  	}
    
  	ISMSLib::setConfigById(RISK_LEVEL, $riskLevel);
    save_risk_limits($moRiskLimits);
  }
}

class SaveRiskLimitsEvent extends FWDRunnable {
  public function run(){
    $moRiskLimits = new RMRiskLimits();

    $riskLevel = ltrim(FWDWebLib::getObject('risk_level_value')->getValue(),":");
    
    if($riskLevel == 3){
	    $moRiskLimits->setFieldValue('risk_limits_low' ,FWDWebLib::getObject('risk_limits_1')->getValue());
	    $moRiskLimits->setFieldValue('risk_limits_high',FWDWebLib::getObject('risk_limits_2')->getValue());
	  	$moRiskLimits->setFieldValue('risk_limits_mid_low' , 0);
	    $moRiskLimits->setFieldValue('risk_limits_mid_high' , 0);
  	} else if($riskLevel == 5){
	    $moRiskLimits->setFieldValue('risk_limits_low' ,FWDWebLib::getObject('risk_limits_1')->getValue());
	    $moRiskLimits->setFieldValue('risk_limits_mid_low' ,FWDWebLib::getObject('risk_limits_2')->getValue());
	    $moRiskLimits->setFieldValue('risk_limits_mid_high' ,FWDWebLib::getObject('risk_limits_3')->getValue());
	    $moRiskLimits->setFieldValue('risk_limits_high',FWDWebLib::getObject('risk_limits_4')->getValue());  		
  	}
  
    $miRiskHighMax = ISMSLib::getConfigById(RISK_VALUE_COUNT);
    $miRiskHighMax*= $miRiskHighMax;
    
    if($riskLevel == 3){    
    	$miRiskLow = $moRiskLimits->getFieldValue('risk_limits_low');
    	$miRiskHigh = $moRiskLimits->getFieldValue('risk_limits_high');
    } else if($riskLevel == 5){
    	$miRiskLow = $moRiskLimits->getFieldValue('risk_limits_low');
    	$miRiskMidLow = $moRiskLimits->getFieldValue('risk_limits_mid_low');
    	$miRiskMidHigh = $moRiskLimits->getFieldValue('risk_limits_mid_high');
    	$miRiskHigh = $moRiskLimits->getFieldValue('risk_limits_high');    	
    }
    
    echo "
      js_hide('low_limit_warning');
      js_hide('low_gt_high_warning');
      js_hide('two_gt_three_warning');
      js_hide('three_gt_four_warning');
      js_hide('four_gt_max_warning');
      js_hide('high_gt_max_warning');   
            
      js_hide('wn_user_is_not_chairman_risk_limits');
      ";   
    
    if($riskLevel == 3){
	    if($miRiskLow > $miRiskHigh){
	      echo "js_show('low_gt_high_warning');";
	      return;
	    }
	    
	    if($miRiskHigh > $miRiskHighMax){
	      echo "js_show('high_gt_max_warning');";
	      return;
	    }
	    
	    if($miRiskLow < 1) {
	      echo "js_show('low_limit_warning');";
	      return;
	    }
    } else if($riskLevel == 5){
    	
    	if($miRiskLow > $miRiskMidLow){
	      echo "js_show('low_gt_high_warning');";
	      return;
	    }
	    
    	if($miRiskMidLow > $miRiskMidHigh){
	      echo "js_show('two_gt_three_warning');";
	      return;
	    }
		
    	if($miRiskMidHigh > $miRiskHigh){
	      echo "js_show('three_gt_four_warning');";
	      return;
	    }    	    
	    
	    if($miRiskHigh > $miRiskHighMax){
	      echo "js_show('four_gt_max_warning');";
	      return;
	    }
	    
	    if($miRiskLow < 1) {
	      echo "js_show('low_limit_warning');";
	      return;
	    }    	
    }
    
    //$moRiskLimits->setHash(FWDWebLib::getObject('hash_risk_limits')->getValue());
    if($moRiskLimits->hasSensitiveChanges()){
      $miUserId = ISMSLib::getCurrentUserId();
      if($miUserId==$moRiskLimits->getApprover()){
        ISMSLib::setConfigById(RISK_LEVEL, $riskLevel);
      	save_risk_limits($moRiskLimits);
      }else{
        $msTitle = FWDLanguage::getPHPStringValue('tt_reapproval','Reaprova��o');
        $msMessage = FWDLanguage::getPHPStringValue('st_sensitive_data_reapproval','Voc� est� alterando dados sens�veis, que necessitam passar por uma reaprova��o. Deseja confirmar as altera��es?');
        $msEventValue = " soPopUpManager.getPopUpById('popup_confirm').getOpener().trigger_event('confirm_risk_limits_edit',3);";
        ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,75);
      }
    } //else{}
  }
}

class ConfirmRiskParWeightSave extends FWDRunnable {
  public function run() {
    $maAssociation = FWDWebLib::unserializeString(FWDWebLib::getObject('association')->getValue());
    $mbAllZero = true;
    $mbChange = false;
    for($miI=1;$miI<=count($maAssociation);$miI++) {
      $miWeight = $maAssociation[$miI][1];
      $miNewWeight = FWDWebLib::getObject("parameter_weight_{$miI}")->getValue();
      if ($miNewWeight) {
        $mbAllZero = false;
      }
      if ($miWeight != $miNewWeight) {
        $mbChange = true;
      }
    }
    if ($mbAllZero) {
      echo "js_show('at_least_one_positive_warning');";
      exit;
    } else {
      echo "js_hide('at_least_one_positive_warning');";
    }
    if ($mbChange){
      $msTitle = FWDLanguage::getPHPStringValue('tt_sensitive_data','Dados Sens�veis');
      $msMessage = FWDLanguage::getPHPStringValue('st_risk_parameters_weight_edit_confirm','Voc� est� alterando dados sens�veis. Ao alterar o peso dos par�metros os valores de todos os riscos e ativos do sistema ser�o alterados de acordo. Deseja confirmar as altera��es?');
      $msEventValue = "soPopUpManager.getPopUpById('popup_confirm').getOpener().trigger_event('save_risk_par_weight',3);";
      ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,75);
    }
  }
}

class SaveRiskParWeight extends FWDRunnable {
  public function run(){
    $maAssociation = FWDWebLib::unserializeString(FWDWebLib::getObject('association')->getValue());
    foreach($maAssociation as $miIndex=>$maParameter) {
      $moPName = new RMParameterName();
      $moPName->setFieldValue('parametername_weight',FWDWebLib::getObject("parameter_weight_{$miIndex}")->getValue());
      $moPName->createFilter($maParameter[0],'parametername_id');
      $moPName->update();
    }
    $moQueryRiskUpdate = new QueryRiskUpdate(FWDWebLib::getConnection());
    $moQueryRiskUpdate->makeQuery();
    $moQueryRiskUpdate->executeQuery();
    echo "gobi('save_warning').show(); setTimeout('gobi(\\'save_warning\\').hide()',5000);";
  }
}

class SaveReportClassificationEvent extends FWDRunnable {
  public function run(){
    ISMSLib::setConfigById(GENERAL_SYSTEM_CLASSIFICATION, FWDWebLib::getObject('classification')->getValue());
    ISMSLib::setConfigById(GENERAL_SYSTEM_CLASSIFICATION_DEST,FWDWebLib::getObject('classification_dest')->getValue());
    echo "gobi('save_warning').show(); setTimeout('gobi(\\'save_warning\\').hide()',5000);";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new OpenRiskParametersNamesConfirm("open_risk_parameters_names_confirm"));
    $moStartEvent->addAjaxEvent(new SaveRiskParametersNamesEvent("save_risk_parameters_names_event"));
    $moStartEvent->addAjaxEvent(new SaveControlEvent("save_control_event"));
    $moStartEvent->addAjaxEvent(new SaveSpecialUsersEvent("save_special_users_event"));
    $moStartEvent->addAjaxEvent(new SaveClassificationEvent("save_classification_event"));
    $moStartEvent->addAjaxEvent(new SaveFeaturesEvent("save_features_event"));
    $moStartEvent->addAjaxEvent(new SaveControlCostNamesEvent("save_control_cost_names_event_event"));
    $moStartEvent->addAjaxEvent(new SaveFinancialImpactParametersNamesEvent("save_financial_impact_parameters_names_event"));
    $moStartEvent->addAjaxEvent(new SaveNumberOfRiskParameters("save_number_of_risk_parameters_event"));
    $moStartEvent->addAjaxEvent(new OpenNumberOfRiskParametersConfirm("open_number_of_risk_parameters_confirm"));
    $moStartEvent->addAjaxEvent(new SaveRiskLimitsEvent('save_risk_limits_event'));
    $moStartEvent->addAjaxEvent(new ConfirmEditEvent('confirm_risk_limits_edit'));
    $moStartEvent->addAjaxEvent(new ConfirmRiskParWeightSave('confirm_risk_par_weight_save'));
    $moStartEvent->addAjaxEvent(new SaveRiskParWeight('save_risk_par_weight'));
    $moStartEvent->addAjaxEvent(new SaveReportClassificationEvent('save_report_classification_event'));
    $moStartEvent->addAjaxEvent(new UpdateAllValuesEvent('update_all_values_event'));
    $moStartEvent->addAjaxEvent(new SaveCurrencyEvent('save_currency_event'));
    $moStartEvent->addAjaxEvent(new SaveCustomInterfaceEvent('save_custom_interface_event'));
    $moStartEvent->addAjaxEvent(new SaveCustomInterfaceCompleteEvent('save_custom_interface_complete_event'));
    $moStartEvent->addAjaxEvent(new RestoreLogosEvent('restore_logos_event'));
    $moStartEvent->addSubmitEvent(new SubmitEvent("upload"));
    $moStartEvent->addAjaxEvent(new SaveCustomNonConformityTypesEvent('save_custom_non_conformity_types_event'));
    
    $moWebLib = FWDWebLib::getInstance();
    $moDialog = FWDWebLib::getObject("dialog");
    
    $moWebLib->xml_load($moWebLib->getSysRef().'/packages/admin/custom_risk_parameters_names.xml',false);
    $moDialog->addObjFWDView(FWDWebLib::getObject("custom_risk_parameters_names"));
    
    $moWebLib->xml_load($moWebLib->getSysRef().'/packages/admin/custom_quantity_rm_parameters.xml',false);
    $moDialog->addObjFWDView(FWDWebLib::getObject("custom_quantity_rm_parameters"));
    
    $moWebLib->xml_load($moWebLib->getSysRef().'/packages/admin/custom_asset_values_name.xml',false);
    $moDialog->addObjFWDView(FWDWebLib::getObject("custom_asset_values_name"));
    
    $moWebLib->xml_load($moWebLib->getSysRef().'/packages/admin/custom_risk_values_name.xml',false);
    $moDialog->addObjFWDView(FWDWebLib::getObject("custom_risk_values_name"));
    
    $moWebLib->xml_load($moWebLib->getSysRef().'/packages/admin/custom_risk_control_values_name.xml',false);
    $moDialog->addObjFWDView(FWDWebLib::getObject("custom_risk_control_values_name"));
    
    $moWebLib->xml_load($moWebLib->getSysRef().'/packages/admin/custom_control_cost_name.xml',false);
    $moDialog->addObjFWDView(FWDWebLib::getObject("custom_control_cost_name"));
    
    $moWebLib->xml_load($moWebLib->getSysRef().'/packages/admin/custom_financial_impact_parameters.xml',false);
    $moDialog->addObjFWDView(FWDWebLib::getObject("custom_financial_impact_parameters"));
    
    $moWebLib->xml_load($moWebLib->getSysRef().'/packages/admin/custom_system_features.xml',false);
    $moDialog->addObjFWDView(FWDWebLib::getObject("custom_system_features"));
    
    $moWebLib->xml_load($moWebLib->getSysRef().'/packages/admin/custom_special_users.xml',false);
    $moDialog->addObjFWDView(FWDWebLib::getObject("custom_special_users"));

    $moWebLib->xml_load($moWebLib->getSysRef().'/packages/admin/custom_control.xml',false);
    $moDialog->addObjFWDView(FWDWebLib::getObject("custom_control_revision_and_test"));
    
    $moWebLib->xml_load($moWebLib->getSysRef().'/packages/admin/custom_type_and_priority_parametrization.xml',false);
    $moDialog->addObjFWDView(FWDWebLib::getObject("custom_type_and_priority_parametrization"));
    
    $moWebLib->xml_load($moWebLib->getSysRef().'/packages/admin/custom_risk_limits.xml',false);
    $moDialog->addObjFWDView(FWDWebLib::getObject("custom_risk_limits"));
    
    $moWebLib->xml_load($moWebLib->getSysRef().'/packages/admin/custom_risk_parameters_weight.xml',false);
    $moDialog->addObjFWDView(FWDWebLib::getObject("custom_risk_parameters_weight"));
    
    $moWebLib->xml_load($moWebLib->getSysRef().'/packages/admin/custom_report_classification.xml',false);
    $moDialog->addObjFWDView(FWDWebLib::getObject("custom_report_classification"));
    
    $moWebLib->xml_load($moWebLib->getSysRef().'/packages/admin/custom_currency.xml',false);
    $moDialog->addObjFWDView(FWDWebLib::getObject("custom_currency"));
    
    $moWebLib->xml_load($moWebLib->getSysRef().'/packages/admin/custom_interface.xml',false);
    $moDialog->addObjFWDView(FWDWebLib::getObject("custom_interface"));        

    $moWebLib->xml_load($moWebLib->getSysRef().'/packages/admin/custom_non_conformity_types.xml',false);
    $moDialog->addObjFWDView(FWDWebLib::getObject("custom_non_conformity_types"));         
    
    /* Se n�o possuir o m�dulo de documenta��o, n�o deve mostrar os tipos de classifica��o de documento e registro. */
    if(!ISMSLib::hasModule(POLICY_MODE)){
      FWDWebLib::getObject('item_document')->setShouldDraw(false);
      FWDWebLib::getObject('item_register')->setShouldDraw(false);
      FWDWebLib::getObject('cl_document')->setShouldDraw(false);
      FWDWebLib::getObject('cl_register')->setShouldDraw(false);
      FWDWebLib::getObject('has_pm_module')->setValue('0');
    }
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    //for�a o carregamento das configura��es do banco na sess�o, importante para pegar as informa��es
    //que o ACT possivelmente alterou no banco
    $moConfigForce = new ISMSConfig();
    $moConfigForce->loadConfigForced();
    
    /********** Procedimentos relacionados as Dimens�es de Import�ncia e Impacto **********/
    $maPNameFields = array("risk_par_1","risk_par_2","risk_par_3","risk_par_4","risk_par_5","risk_par_6","risk_par_7","risk_par_8");
    $maPNameAssociation = array();
    $moPNameHandler = new QueryParameterNames(FWDWebLib::getConnection());
    $moPNameHandler->makeQuery();
    $moPNameHandler->executeQuery();
    $moControllerPName = FWDWebLib::getObject("risk_par_controller");
    $miI = 0;
    while ($moPNameHandler->fetch()) {
      $miId = $moPNameHandler->getFieldValue('parametername_id');
      FWDWebLib::getObject($maPNameFields[$miI])->setValue($moPNameHandler->getFieldValue('parametername_name'));
      $maPNameAssociation[$maPNameFields[$miI]] = $miId;
      $moControllerPName->checkItem($maPNameFields[$miI++]);
    }
    for ($miI; $miI<count($maPNameFields); $miI++) {
      FWDWebLib::getObject($maPNameFields[$miI])->setAttrDisabled("true");
    }
    FWDWebLib::getObject('risk_par_association')->setValue(serialize($maPNameAssociation));
    
    /********** Procedimentos relacionados a quantidade de par�metros do risco **********/
    $miRiskCount = ISMSLib::getConfigById(RISK_VALUE_COUNT);
    FWDWebLib::getObject("risk_value_count")->checkItem($miRiskCount);
    if($miRiskCount==3){
      FWDWebLib::getObject('vg_custom_risk_control_values_name_5')->setAttrDisplay('false');
      FWDWebLib::getObject('vg_asset_val_5')->setAttrDisplay('false');
      FWDWebLib::getObject('vg_risk_val_5')->setAttrDisplay('false');
    }else{
      FWDWebLib::getObject('vg_custom_risk_control_values_name_3')->setAttrDisplay('false');
      FWDWebLib::getObject('vg_asset_val_3')->setAttrDisplay('false');
      FWDWebLib::getObject('vg_risk_val_3')->setAttrDisplay('false');
    }
    FWDWebLib::getObject('var_risk_value_count')->setValue($miRiskCount);

    /********** Procedimentos relacionados aos Valores dos Par�metros de (Ativo e Risco)**********/
    $maPValueAssociation = array();
    $moPValueHandler = new QueryAllParameterValuesName(FWDWebLib::getConnection());
    $moPValueHandler->makeQuery();
    $moPValueHandler->executeQuery();
    $moPValueDataset = $moPValueHandler->getDataset();
    $maAssetRiskIds = array();
    while ($moPValueDataset->fetch()) {
      $miValue = $moPValueDataset->getFieldByAlias('parametervalue_value')->getValue();
      FWDWebLib::getObject("importance".$miRiskCount."_".$miValue)->setValue($moPValueDataset->getFieldByAlias('parametervalue_importance')->getValue());
      FWDWebLib::getObject("impact".$miRiskCount."_".$miValue)->setValue($moPValueDataset->getFieldByAlias('parametervalue_impact')->getValue());
      FWDWebLib::getObject("riskprob".$miRiskCount."_".$miValue)->setValue($moPValueDataset->getFieldByAlias('parametervalue_rprob')->getValue());
      $maAssetRiskIds[$miValue] = $moPValueDataset->getFieldByAlias('parametervalue_id')->getValue();;
    }
    FWDWebLib::getObject('var_asset_risk_ids')->setValue(serialize($maAssetRiskIds));
    
    /********** Procedimentos relacionados aos Valores dos Par�metros de (Risco x Controle)**********/
    $moRCPValueHandler = new QueryRCParameterValueNames(FWDWebLib::getConnection());
    $moRCPValueHandler->makeQuery();
    $moRCPValueHandler->executeQuery();
    $moRCPValueDataset = $moRCPValueHandler->getDataset();
    $maRiskControlIds = array();
    while ($moRCPValueDataset->fetch()) {
      $miRCValue = intval($moRCPValueDataset->getFieldByAlias('rc_parametervalue_value')->getValue());
      FWDWebLib::getObject("rc_impact".$miRiskCount."_".$miRCValue)->setValue($moRCPValueDataset->getFieldByAlias('rc_parametervalue_impact')->getValue());
      FWDWebLib::getObject("rc_prob".$miRiskCount."_".$miRCValue)->setValue($moRCPValueDataset->getFieldByAlias('rc_parametervalue_probability')->getValue());
      $maRiskControlIds[$miRCValue] = $moRCPValueDataset->getFieldByAlias('rc_parametervalue_id')->getValue();
    }
    FWDWebLib::getObject('var_risk_control_ids')->setValue(serialize($maRiskControlIds));
    
    /********** Procedimentos relacionados aos Nomes dos Custos dos controles **********/
    $moHandlerControlCost = new QueryControlCostNames(FWDWebLib::getConnection());
    $maControlNames = $moHandlerControlCost->getCostNames();
    FWDWebLib::getObject("cname1")->setValue($maControlNames[0]);
    FWDWebLib::getObject("cname2")->setValue($maControlNames[1]);
    FWDWebLib::getObject("cname3")->setValue($maControlNames[2]);
    FWDWebLib::getObject("cname4")->setValue($maControlNames[3]);
    FWDWebLib::getObject("cname5")->setValue($maControlNames[4]);
    
    /********** Procedimentos relacionados aos par�metros de impacto financeiro **********/
    $moContextClassification = new ISMSContextClassification();
    $moContextClassification->createFilter(CONTEXT_CLASSIFICATION_FINANCIAL_IMPACT,'classif_type');
    $moContextClassification->select();
    
    $miCounter = 0;
    while($moContextClassification->fetch()){
      $miCounter++;
      FWDWebLib::getObject('fin_imp_id_'.$miCounter)->setValue($moContextClassification->getFieldByAlias('classif_id')->getValue());
      FWDWebLib::getObject('fin_imp_text_'.$miCounter)->setValue($moContextClassification->getFieldByAlias('classif_name')->getValue());
      FWDWebLib::getObject('fin_imp_text_'.$miCounter)->setAttrDisabled('false');
      FWDWebLib::getObject('fin_imp_checkbox_'.$miCounter)->setAttrCheck('true');
    }
    
    /********** Procedimentos relacionados �s Features do sistema **********/
    $moFeaturesController = FWDWebLib::getObject('system_features_controller');
    if(ISMSLib::getConfigById(GENERAL_SOLICITOR_ENABLED))            $moFeaturesController->checkItem("solicitor");
    if(ISMSLib::getConfigById(GENERAL_PERCENTUAL_RISK))              $moFeaturesController->checkItem("percentual_risk");
    if(ISMSLib::getConfigById(GENERAL_AUTOMATIC_DOCUMENTS))          $moFeaturesController->checkItem("auto_doc_creation");
    if(ISMSLib::getConfigById(GENERAL_COST_ENABLED))                 $moFeaturesController->checkItem("control_cost");
    if(ISMSLib::getConfigById(GENERAL_DOCUMENTS_MANUAL_VERSIONING))  $moFeaturesController->checkItem("manual_versioning");
    if(ISMSLib::getConfigById(GENERAL_MANUAL_DATA_CONTROL))          $moFeaturesController->checkItem("manual_data_control");
    if(ISMSLib::getConfigById(GENERAL_DATA_COLLECTION_ENABLED))      $moFeaturesController->checkItem("data_collection");
    if(ISMSLib::getConfigById(RISK_FORMULA_TYPE) == RISK_FORMULA_TYPE_2) {
      $moFeaturesController->checkItem("risk_formula");
    }

    /********** Procedimentos relacionados as configura��es dos CONTROLES **********/
    if(ISMSLib::getConfigById(GENERAL_REVISION_ENABLED)){
      FWDWebLib::getObject("control_controller")->checkItem('control_revision');
    }
    if(ISMSLib::getConfigById(GENERAL_TEST_ENABLED)){
      FWDWebLib::getObject("control_controller")->checkItem('control_test');
    }

    /********** Procedimentos relacionados aos Usu�rios Especiais **********/
    $moQuerySpecialUsers = new QuerySpecialUsers(FWDWebLib::getConnection());
    $moQuerySpecialUsers->makeQuery();
    $moQuerySpecialUsers->executeQuery();
    $maSpecialUsers = $moQuerySpecialUsers->getSpecialUsers();
    FWDWebLib::getObject('user_chairman_id')->setValue($maSpecialUsers['chairman_id']);
    FWDWebLib::getObject('user_chairman_name')->setValue($maSpecialUsers['chairman_name']);
    FWDWebLib::getObject('user_asset_controller_id')->setValue($maSpecialUsers['asset_controller_id']);
    FWDWebLib::getObject('user_asset_controller_name')->setValue($maSpecialUsers['asset_controller_name']);
    FWDWebLib::getObject('user_control_controller_id')->setValue($maSpecialUsers['control_controller_id']);
    FWDWebLib::getObject('user_control_controller_name')->setValue($maSpecialUsers['control_controller_name']);
    FWDWebLib::getObject('user_librarian_id')->setValue($maSpecialUsers['librarian_id']);
    FWDWebLib::getObject('user_librarian_name')->setValue($maSpecialUsers['librarian_name']);
    FWDWebLib::getObject('user_document_auditor_id')->setValue($maSpecialUsers['document_auditor_id']);
    FWDWebLib::getObject('user_document_auditor_name')->setValue($maSpecialUsers['document_auditor_name']);    
    
    $mbHasCIModule = ISMSLib::hasModule(INCIDENT_MODE);
    if (!$mbHasCIModule) {
      FWDWebLib::getObject('viewgroup_continual_improvement')->setShouldDraw(false);
    } else {
      FWDWebLib::getObject('user_incident_manager_id')->setValue($maSpecialUsers['incident_manager_id']);
      FWDWebLib::getObject('user_incident_manager_name')->setValue($maSpecialUsers['incident_manager_name']);
      FWDWebLib::getObject('user_disciplinary_process_manager_id')->setValue($maSpecialUsers['disciplinary_process_manager_id']);
      FWDWebLib::getObject('user_disciplinary_process_manager_name')->setValue($maSpecialUsers['disciplinary_process_manager_name']);
      FWDWebLib::getObject('user_non_conformity_manager_id')->setValue($maSpecialUsers['non_conformity_manager_id']);
      FWDWebLib::getObject('user_non_conformity_manager_name')->setValue($maSpecialUsers['non_conformity_manager_name']);
      FWDWebLib::getObject('user_evidence_manager_id')->setValue($maSpecialUsers['evidence_manager_id']);
      FWDWebLib::getObject('user_evidence_manager_name')->setValue($maSpecialUsers['evidence_manager_name']);
    }

    /********** Procedimentos relacionados a Classifica��o **********/      
    $maClassificationFields =
      array("cl_type_area1","cl_type_area2","cl_type_area3","cl_type_area4","cl_type_area5","cl_type_area6","cl_type_area7","cl_type_area8",
            "cl_prio_area1","cl_prio_area2","cl_prio_area3","cl_prio_area4","cl_prio_area5","cl_prio_area6","cl_prio_area7","cl_prio_area8",
            "cl_type_process1","cl_type_process2","cl_type_process3","cl_type_process4","cl_type_process5","cl_type_process6","cl_type_process7","cl_type_process8",
            "cl_prio_process1","cl_prio_process2","cl_prio_process3","cl_prio_process4","cl_prio_process5","cl_prio_process6","cl_prio_process7","cl_prio_process8",
            "cl_type_risk1","cl_type_risk2","cl_type_risk3","cl_type_risk4","cl_type_risk5","cl_type_risk6","cl_type_risk7","cl_type_risk8",
            "cl_type_event1","cl_type_event2","cl_type_event3","cl_type_event4","cl_type_event5","cl_type_event6","cl_type_event7","cl_type_event8",
            "cl_type_control1","cl_type_control2","cl_type_control3","cl_type_control4","cl_type_control5","cl_type_control6","cl_type_control7","cl_type_control8"
            );
    if(ISMSLib::hasModule(POLICY_MODE)){
      $maClassificationFields = array_merge($maClassificationFields,
        array(
          "cl_type_document1","cl_type_document2","cl_type_document3","cl_type_document4","cl_type_document5","cl_type_document6","cl_type_document7","cl_type_document8",
          "cl_type_register1","cl_type_register2","cl_type_register3","cl_type_register4","cl_type_register5","cl_type_register6","cl_type_register7","cl_type_register8"
        )
      );
    }
    $moQueryContextClassification = new QueryContextClassification(FWDWebLib::getConnection());
    $moQueryContextClassification->makeQuery();
    $moQueryContextClassification->executeQuery();
    $maClassificationNames = $moQueryContextClassification->getClassificationNames();
    $moClassificationController = FWDWebLib::getObject("classification_controller");
    foreach ($maClassificationNames as $msFieldName => $msClassificationName) {
      FWDWebLib::getObject($msFieldName)->setValue($msClassificationName);
      $moClassificationController->checkItem($msFieldName);
    }
    foreach (array_diff($maClassificationFields,array_keys($maClassificationNames)) as $msFieldName) {
      FWDWebLib::getObject($msFieldName)->setAttrDisabled("true");
    }  
    /********** Procedimentos relacionados � moeda  **********/
    
    $msCurrency = $moConfigForce->getConfig(GENERAL_CURRENCY_IDENTIFIER);
    FWDWebLib::getObject('currency')->setValue($msCurrency);

    /********** Procedimeentos relacionados ao Limite dos Riscos **********/
    $moHandlerRiskLimits = new QueryRiskLimits();
    $moHandlerRiskLimits->makeQuery();
    $moHandlerRiskLimits->executeQuery();
    $moHandlerRiskLimits->fetch();
    $moRiskLimits = new RMRiskLimits();
    $moRiskLimits->setFieldValue('risk_limits_low',$moHandlerRiskLimits->getFieldValue('risk_limits_low'));
    $moRiskLimits->setFieldValue('risk_limits_high',$moHandlerRiskLimits->getFieldValue('risk_limits_high'));

    $riskLevel = ISMSLib::getConfigById(RISK_LEVEL);
    if($riskLevel == 3){
		$moRiskLimits->setFieldValue('risk_limits_low',$moHandlerRiskLimits->getFieldValue('risk_limits_low'));
    	$moRiskLimits->setFieldValue('risk_limits_high',$moHandlerRiskLimits->getFieldValue('risk_limits_high'));
    } else if($riskLevel == 5) {
		$moRiskLimits->setFieldValue('risk_limits_low',$moHandlerRiskLimits->getFieldValue('risk_limits_low'));
		$moRiskLimits->setFieldValue('risk_limits_mid_low',$moHandlerRiskLimits->getFieldValue('risk_limits_mid_low'));
		$moRiskLimits->setFieldValue('risk_limits_mid_high',$moHandlerRiskLimits->getFieldValue('risk_limits_mid_high'));
   		$moRiskLimits->setFieldValue('risk_limits_high',$moHandlerRiskLimits->getFieldValue('risk_limits_high'));
    }
    
    if($riskLevel == 3){
    	FWDWebLib::getObject('risk_limits_1')->setValue($moRiskLimits->getFieldValue('risk_limits_low'));
    	FWDWebLib::getObject('risk_limits_2')->setValue($moRiskLimits->getFieldValue('risk_limits_high'));
    } else if($riskLevel == 5) {
    	FWDWebLib::getObject('risk_limits_1')->setValue($moRiskLimits->getFieldValue('risk_limits_low'));
    	FWDWebLib::getObject('risk_limits_2')->setValue($moRiskLimits->getFieldValue('risk_limits_mid_low'));
    	FWDWebLib::getObject('risk_limits_3')->setValue($moRiskLimits->getFieldValue('risk_limits_mid_high'));
    	FWDWebLib::getObject('risk_limits_4')->setValue($moRiskLimits->getFieldValue('risk_limits_high'));
    	
    	FWDWebLib::getObject('label_risk_limits_3')->setAttrDisplay("true");
    	FWDWebLib::getObject('risk_limits_3')->setAttrDisplay("true");
    	
    	FWDWebLib::getObject('label_risk_limits_4')->setAttrDisplay("true");
    	FWDWebLib::getObject('risk_limits_4')->setAttrDisplay("true");
    	
    	FWDWebLib::getObject('risk_treatment_message2')->setAttrDisplay("true");
    }
    
    FWDWebLib::getObject('hash_risk_limits')->setValue($moRiskLimits->getHash());
    FWDWebLib::getObject('risk_level_value')->setValue($riskLevel);
    
    
    /********** Procedimeentos relacionados aos pesos dos par�metros do risco **********/
    $moPName = new RMParameterName();
    $maParameters = $moPName->getParameters();
    $miCount=0;
    $maAssociation=array();
    foreach($maParameters as $miParameterId=>$maParameter) {
      $miCount++;
      $maAssociation[$miCount] = array($miParameterId,$maParameter[1]);
      FWDWebLib::getObject("parameter_name_{$miCount}")->setValue($maParameter[0].":");
      FWDWebLib::getObject("parameter_weight_{$miCount}")->setValue($maParameter[1]);
    }
    for($miCount=$miCount+1;$miCount<=8;$miCount++) { //No m�ximo 8 par�metros
      FWDWebLib::getObject("parameter_name_{$miCount}")->setShouldDraw(false);
      FWDWebLib::getObject("parameter_weight_{$miCount}")->setShouldDraw(false);
    }
    FWDWebLib::getObject('association')->setValue(serialize($maAssociation));
    
    /********** Procedimentos relacionados a classifica��o dos relat�rios **********/
    FWDWebLib::getObject('classification')->setValue(ISMSLib::getConfigById(GENERAL_SYSTEM_CLASSIFICATION));
    FWDWebLib::getObject('classification_dest')->setValue(ISMSLib::getConfigById(GENERAL_SYSTEM_CLASSIFICATION_DEST));
    
    /********** Cores customizadas **********/
    FWDWebLib::getObject('lightColor')->setValue(ISMSLib::getConfigById(LIGHT_COLOR));
    FWDWebLib::getObject('darkColor')->setValue(ISMSLib::getConfigById(DARK_COLOR));
    
    
    /********** Procedimentos relacionados aos par�metros de n�o conformidade **********/
    $moNonConformityTypes = new ISMSNonConformityTypes();
    $moNonConformityTypes->select();
    $miCounter = 0;
    while($moNonConformityTypes->fetch()){
      $miCounter++;
		FWDWebLib::getObject('non_conform_text_'.$miCounter)->setValue($moNonConformityTypes->getFieldByAlias('non_conform_name')->getValue());
		FWDWebLib::getObject('non_conform_text_'.$miCounter)->setAttrDisabled('false');
		FWDWebLib::getObject('non_conform_checkbox_'.$miCounter)->setAttrCheck('true');
    }   

    
  //instala a seguran�a de ACL na p�gina
  FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());

  // seta como vis�vel a primeira tela de classifica��o que o usu�rio tem permiss�o para ver
  $maTypePrioDivs = array('cl_area','cl_process','cl_risk','cl_event','cl_control','cl_document','cl_register');
  $msSelectedDivDisplay = '';
  foreach($maTypePrioDivs as $msValue){
    if (FWDWebLib::getObject($msValue)->getShouldDraw()){
      $msSelectedDivDisplay = $msValue;
      break;
    }
  }
  if($msSelectedDivDisplay){
    FWDWebLib::getObject($msSelectedDivDisplay)->setAttrDisplay("true");
    FWDWebLib::getObject('var_select_classification_panel')->setValue($msSelectedDivDisplay);
  }

  // exibe a primeira tela de configura��o que o usu�rio tem permiss�o para ver
  if (FWDWebLib::getObject("custom_risk_parameters_names")->getShouldDraw())
    FWDWebLib::getObject("custom_risk_parameters_names")->setAttrDisplay("true");
  elseif (FWDWebLib::getObject("custom_quantity_rm_parameters")->getShouldDraw())
    FWDWebLib::getObject("custom_quantity_rm_parameters")->setAttrDisplay("true");
  elseif (FWDWebLib::getObject("custom_asset_values_name")->getShouldDraw())
    FWDWebLib::getObject("custom_asset_values_name")->setAttrDisplay("true");
  elseif (FWDWebLib::getObject("custom_risk_values_name")->getShouldDraw())
    FWDWebLib::getObject("custom_risk_values_name")->setAttrDisplay("true");
  elseif (FWDWebLib::getObject("custom_risk_control_values_name")->getShouldDraw())
    FWDWebLib::getObject("custom_risk_control_values_name")->setAttrDisplay("true");
  elseif (FWDWebLib::getObject("custom_control_cost_name")->getShouldDraw())
    FWDWebLib::getObject("custom_control_cost_name")->setAttrDisplay("true");
  elseif (FWDWebLib::getObject("custom_financial_impact_parameters")->getShouldDraw())
    FWDWebLib::getObject("custom_financial_impact_parameters")->setAttrDisplay("true");
  elseif (FWDWebLib::getObject("custom_system_features")->getShouldDraw())
    FWDWebLib::getObject("custom_system_features")->setAttrDisplay("true");
  elseif (FWDWebLib::getObject("custom_special_users")->getShouldDraw())
    FWDWebLib::getObject("custom_special_users")->setAttrDisplay("true");
  elseif (FWDWebLib::getObject("custom_type_and_priority_parametrization")->getShouldDraw())
    FWDWebLib::getObject("custom_type_and_priority_parametrization")->setAttrDisplay("true");
  elseif (FWDWebLib::getObject("custom_control_revision_and_test")->getShouldDraw())
    FWDWebLib::getObject("custom_control_revision_and_test")->setAttrDisplay("true");
  elseif (FWDWebLib::getObject("custom_risk_limits")->getShouldDraw())
    FWDWebLib::getObject("custom_risk_limits")->setAttrDisplay("true");
  elseif (FWDWebLib::getObject("custom_risk_parameters_weight")->getShouldDraw())
    FWDWebLib::getObject("custom_risk_parameters_weight")->setAttrDisplay("true");
  elseif (FWDWebLib::getObject("custom_report_classification")->getShouldDraw())
    FWDWebLib::getObject("custom_report_classification")->setAttrDisplay("true");
 
  // teste para retirar campos referentes ao m�dulo de documenta��o
  if(!ISMSLib::hasModule(POLICY_MODE)){
    FWDWebLib::getObject('auto_doc_creation')->setShouldDraw(false);
    FWDWebLib::getObject('auto_doc_creation_help')->setShouldDraw(false);
    FWDWebLib::getObject('auto_doc_creation_checkbox')->setShouldDraw(false);
  }

  // teste para retirar campos referentes ao m�dulo de incidentes
  if(!ISMSLib::hasModule(INCIDENT_MODE)){
    FWDWebLib::getObject('custom_financial_impact_parameters')->setShouldDraw(false);
    FWDWebLib::getObject('name_custom_financial_impact_parameters')->setShouldDraw(false);
    FWDWebLib::getObject('has_ci_module')->setValue('0');
    
  }
   
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
    if(!gebi('rc_impact3_0').value) gebi('rc_impact3_0').value = '0';
    if(!gebi('rc_impact5_0').value) gebi('rc_impact5_0').value = '0';
    if(!gebi('rc_prob3_0').value) gebi('rc_prob3_0').value = '0';
    if(!gebi('rc_prob5_0').value) gebi('rc_prob5_0').value = '0';
    
    js_show = function(id){
      gebi(id).style.display='block';
      gebi(id).style.visibility='visible';
    }
    
    js_hide = function(id){
      gebi(id).style.display='none';
      gebi(id).style.visibility='hidden';
    }

    for(var siI=1;siI<=8;siI++){
      if(gebi('parameter_weight_'+siI)){
        if(!gebi('parameter_weight_'+siI).value){
          gebi('parameter_weight_'+siI).value='0';
        }
      }
    }
    
    function changeIsFillable(paFields,psPanel,psPanelToShow){
      for (var i=0; i < paFields.length; i++){
        if(gebi(paFields[i])){
          gobi(paFields[i]).isFillable = function(){
            var msElement = paFields[i];
            var msPanelToShow = psPanelToShow;
            var msPanel = psPanel;
            if(gebi(msPanel).style.display=='none'){
              return false;
            }else{
              if(!this.isFilled()){
                hack_change_customize(msPanelToShow);
              }
              return true;
            }
          }
        }
      }
    }

    var maAsset3   = new Array('importance3_1','importance3_2','importance3_3');
    var maAsset5   = new Array('importance5_1','importance5_2','importance5_3','importance5_4','importance5_5');
    var maRisk3   = new Array('impact3_1','impact3_2','impact3_3','riskprob3_1','riskprob3_2','riskprob3_3');
    var maRisk5   = new Array('impact5_1','impact5_2','impact5_3','impact5_4','impact5_5','riskprob5_1','riskprob5_2','riskprob5_3','riskprob5_4','riskprob5_5');
    var maRiskControl3 = new Array('rc_impact3_0','rc_impact3_1','rc_impact3_2','rc_prob3_0','rc_prob3_1','rc_prob3_2');
    var maRiskControl5 = new Array('rc_impact5_0','rc_impact5_1','rc_impact5_2','rc_impact5_3','rc_impact5_4','rc_prob5_0','rc_prob5_1','rc_prob5_2','rc_prob5_3','rc_prob5_4');
    changeIsFillable(maAsset3       ,'vg_asset_val_3'                       ,'custom_asset_values_name');
    changeIsFillable(maRisk3        ,'vg_risk_val_3'                        ,'custom_risk_values_name');
    changeIsFillable(maRiskControl3 ,'vg_custom_risk_control_values_name_3' ,'custom_risk_control_values_name');
    changeIsFillable(maRiskControl5 ,'vg_custom_risk_control_values_name_5' ,'custom_risk_control_values_name');
    changeIsFillable(maAsset5       ,'vg_asset_val_5'                       ,'custom_asset_values_name');
    changeIsFillable(maRisk5        ,'vg_risk_val_5'                        ,'custom_risk_values_name');
    maAsset3 = maRisk3 = maRiskControl3 = maAssetRisk5 = maRisk5 = maRiskControl5 = null;

    function hack_change_customize(psSelectId){
      if(gebi(gebi('var_selected_panel').value)){
        js_hide(gebi('var_selected_panel').value)
      }
      gebi('var_selected_panel').value = psSelectId;
      if (gebi(gebi('var_selected_panel').value)){
        js_show(gebi('var_selected_panel').value);
        gebi('customization_select').value = gebi('var_selected_panel').value;
      }
    }
    
    function change_customize () {
      if(gebi('var_selected_panel').value && gebi('var_selected_panel').value!=undefined){
        if(gebi(gebi('var_selected_panel').value)){
          js_hide(gebi('var_selected_panel').value)
        }
      }
      gebi('var_selected_panel').value = gebi('customization_select').value;
      if (gebi(gebi('var_selected_panel').value)){
        js_show(gebi('var_selected_panel').value);
      }
    }
    
    function change_classification () {
      if(gebi('var_select_classification_panel').value && gebi('var_select_classification_panel').value!=undefined){
        if(gebi(gebi('var_select_classification_panel').value)){
          js_hide(gebi('var_select_classification_panel').value)
        }
      }
      gebi('var_select_classification_panel').value = gebi('classification_select').value;
      if (gebi(gebi('classification_select').value)){
        js_show(gebi('classification_select').value);
      }
    }
    
    function hack_change_classification (psPanel) {
      if(gebi(gebi('var_select_classification_panel').value)){
        js_hide(gebi('var_select_classification_panel').value);
      }
      gebi('classification_select').value = psPanel;
      gebi('var_select_classification_panel').value = psPanel;
      if (gebi(gebi('classification_select').value)){
        js_show(gebi('classification_select').value);
      }
    }

    function save_number_of_risk_parameters_event(){
      trigger_event('save_number_of_risk_parameters_event',3);
    }
    
    function save_parametrization(){
      var maFieldNames = new Array(
        "cl_type_area1","cl_type_area2","cl_type_area3","cl_type_area4","cl_type_area5","cl_type_area6","cl_type_area7","cl_type_area8",
        "cl_prio_area1","cl_prio_area2","cl_prio_area3","cl_prio_area4","cl_prio_area5","cl_prio_area6","cl_prio_area7","cl_prio_area8",
        "cl_type_process1","cl_type_process2","cl_type_process3","cl_type_process4","cl_type_process5","cl_type_process6","cl_type_process7","cl_type_process8",
        "cl_prio_process1","cl_prio_process2","cl_prio_process3","cl_prio_process4","cl_prio_process5","cl_prio_process6","cl_prio_process7","cl_prio_process8",
        "cl_type_risk1","cl_type_risk2","cl_type_risk3","cl_type_risk4","cl_type_risk5","cl_type_risk6","cl_type_risk7","cl_type_risk8",
        "cl_type_event1","cl_type_event2","cl_type_event3","cl_type_event4","cl_type_event5","cl_type_event6","cl_type_event7","cl_type_event8",
        "cl_type_control1","cl_type_control2","cl_type_control3","cl_type_control4","cl_type_control5","cl_type_control6","cl_type_control7","cl_type_control8"
      );
      if (gebi('has_pm_module').value == '1') {
        maFieldNames.concat(
          new Array(
            "cl_type_document1","cl_type_document2","cl_type_document3","cl_type_document4","cl_type_document5","cl_type_document6","cl_type_document7","cl_type_document8",
            "cl_type_register1","cl_type_register2","cl_type_register3","cl_type_register4","cl_type_register5","cl_type_register6","cl_type_register7","cl_type_register8"
          )
        );
      }
      for (var i=0; i < maFieldNames.length; i++) {
        if (!trim(gebi(maFieldNames[i]).value) && (gebi(maFieldNames[i]).disabled==false))
          disable_checkbox_clean_text_field('classification_controller',maFieldNames[i]);
      }
      maFieldNames = i = null;
    }
    
    function set_chairman(piId, psName){
      gebi('user_chairman_id').value = piId;
      gobi('user_chairman_name').setValue(psName);
    }
    
    function set_asset_controller(piId, psName){
      gebi('user_asset_controller_id').value = piId;
      gobi('user_asset_controller_name').setValue(psName);
    }
    
    function set_control_controller(piId, psName){
      gebi('user_control_controller_id').value = piId;
      gobi('user_control_controller_name').setValue(psName);
    }
    
    function set_librarian(piId, psName){
      gebi('user_librarian_id').value = piId;
      gobi('user_librarian_name').setValue(psName);
    }
    
    function set_incident_manager(piId, psName){
      gebi('user_incident_manager_id').value = piId;
      gobi('user_incident_manager_name').setValue(psName);
    }
    
    function set_disciplinary_process_manager(piId, psName){
      gebi('user_disciplinary_process_manager_id').value = piId;
      gobi('user_disciplinary_process_manager_name').setValue(psName);
    }
    
    function set_non_conformity_manager(piId, psName){
      gebi('user_non_conformity_manager_id').value = piId;
      gobi('user_non_conformity_manager_name').setValue(psName);
    }
    
    function set_evidence_manager(piId, psName){
      gebi('user_evidence_manager_id').value = piId;
      gobi('user_evidence_manager_name').setValue(psName);
    }

    function set_document_auditor(piId, psName){
        gebi('user_document_auditor_id').value = piId;
        gobi('user_document_auditor_name').setValue(psName);
      }
    
    ISMSFinancialConfigManager = {
      'csSelected': 'cost_names',
      'changeSelection': function(psSelection){
        if(psSelection!=this.csSelected){
          gebi('financial_config_selector').value = psSelection;
          js_hide('vg_'+this.csSelected);
          this.csSelected = psSelection;
          js_show('vg_'+this.csSelected);
        }
      },
      'toggleCheck':function(piIndex){
        gebi('fin_imp_text_'+piIndex).disabled = !gebi('fin_imp_text_'+piIndex).disabled;
      },
      'isDeleting': function(){
        if(gebi('has_ci_module').value){
          for(var i=1;i<=8;i++){
            if(gebi('fin_imp_id_'+i).value){
              if(!gebi('fin_imp_text_'+i).value || !gobi('financial_impact_controller_'+i).isChecked()){
                return true;
              }
            }
          }
        }
        return false;
      }
    }
    
    function updateAllValues() {
      trigger_event('update_all_values_event', 3);
    }
    
    function change_risk_value(piNum) {
      if (!(piNum == 3 || piNum == 5)) return false;
      var miOtherNum = (piNum == 3) ? 5 : 3;
      var maAssetRiskFields = new Array('importance','impact','riskprob');
      var maRiskControlFields = new Array('rc_impact','rc_prob');
      var miKbegin = 0;
      var miKend = 0;

      if(piNum==3){
        miKend = 1;
        js_hide('vg_asset_val_5');
        js_hide('vg_risk_val_5');
        js_hide('vg_custom_risk_control_values_name_5');
        js_show('vg_asset_val_3');
        js_show('vg_risk_val_3');
        js_show('vg_custom_risk_control_values_name_3');
        if ((gebi('risk_value_count').value == 5)&&(gebi('var_risk_value_count').value == 5)){
          js_show('optimist_group');
        }
      }else{
        miKbegin = 1;
        js_hide('vg_asset_val_3');
        js_hide('vg_risk_val_3');
        js_hide('vg_custom_risk_control_values_name_3');
        js_show('vg_asset_val_5');
        js_show('vg_risk_val_5');
        js_show('vg_custom_risk_control_values_name_5');
        js_hide('optimist_group');
        //setDefaultRiskParValue3_to_5_change();
      }
      
      for (var miI=0; miI<maAssetRiskFields.length; miI++) {
        for(var miK=1;miK<=3;miK++){
          gebi(maAssetRiskFields[miI] + piNum + '_'+ (miK + miKbegin)).value = gebi(maAssetRiskFields[miI] + miOtherNum + '_' + (miK + miKend)).value;
        }
      }
      for (var miI=0; miI<maRiskControlFields.length; miI++) {
        for(var miK=0;miK<3;miK++){
          gebi(maRiskControlFields[miI] + piNum + '_' + (miK + miKbegin)).value = gebi(maRiskControlFields[miI] + miOtherNum + '_' + (miK + miKend)).value;
        }
      }
      
      piNum = maAssetRiskFields = maRiskControlFields = miOtherNum = miI = miK = miKbegin = miKend = null;
    }
    
    function restore_risk_value_on_change(paValues,piDefaultValue){
      for (var miI=0; miI<paValues.length; miI++) {
        if(!gebi(paValues[miI]).value){
          gebi(paValues[miI]).value = piDefaultValue;
        }
      }
    }
  
    function save_pname() {
      trigger_event('save_risk_parameters_names_event','3');
    }

	function change_risk_level(level){

		if(level == 3){
			js_show('risk_treatment_message');
			js_hide('risk_treatment_message2');

			js_hide('label_risk_limits_3');	
			js_hide('risk_limits_3');	
			
			js_hide('label_risk_limits_4');	
			js_hide('risk_limits_4');
						
		} else if(level == 5) {
			js_hide('risk_treatment_message');
			js_show('risk_treatment_message2');	

			js_show('label_risk_limits_3');	
			js_show('risk_limits_3');	
			
			js_show('label_risk_limits_4');	
			js_show('risk_limits_4');	
		}

		
	}
    

    var sbShowPercent = <?=(ISMSLib::getConfigById(GENERAL_PERCENTUAL_RISK)?'true':'false')?>;
    
    function updatePercentRisk1(){
      if(sbShowPercent){
        gobi('percent_risk_1').setValue('('+getRiskPercentualValue(<?=$miRiskCount?>,gebi('risk_limits_1').value)+')');
      }
    }
    
    function updatePercentRisk2(){
      if(sbShowPercent){
        gobi('percent_risk_2').setValue('('+getRiskPercentualValue(<?=$miRiskCount?>,gebi('risk_limits_2').value)+')');
      }
    }

    function updatePercentRisk3(){
        if(sbShowPercent){
          gobi('percent_risk_3').setValue('('+getRiskPercentualValue(<?=$miRiskCount?>,gebi('risk_limits_3').value)+')');
        }
      }

    function updatePercentRisk4(){
        if(sbShowPercent){
          gobi('percent_risk_4').setValue('('+getRiskPercentualValue(<?=$miRiskCount?>,gebi('risk_limits_4').value)+')');
        }
      }    
    
    
    function save_risk_limits(){

      js_hide('low_limit_warning');
      js_hide('low_gt_high_warning');
      js_hide('two_gt_three_warning');
      js_hide('three_gt_four_warning');
      js_hide('four_gt_max_warning');
       
      js_hide('high_gt_max_warning');       
      js_hide('wn_user_is_not_chairman_risk_limits');

 	  if(gebi('risk_level_value').value == 3){
        
	      var miRisk1 = parseInt(gebi('risk_limits_1').value);
	      var miRisk2 = parseInt(gebi('risk_limits_2').value);
	      if(miRisk1 < 1){
	        js_show('low_limit_warning');
	      }else if(miRisk2 > <?=$miRiskCount*$miRiskCount?>){
	        js_show('high_gt_max_warning');
	      }else if(miRisk1 > miRisk2){
	        js_show('low_gt_high_warning');
	      }else{
	        trigger_event('save_risk_limits_event',3);
	      }

	  } else if(gebi('risk_value_count').value == 5){

	      var miRisk1 = parseInt(gebi('risk_limits_1').value);
	      var miRisk2 = parseInt(gebi('risk_limits_2').value);
	      var miRisk3 = parseInt(gebi('risk_limits_3').value);
	      var miRisk4 = parseInt(gebi('risk_limits_4').value);
	      
	      if(miRisk1 < 1){
	        js_show('low_limit_warning');
	      }else if(miRisk4 > <?=$miRiskCount*$miRiskCount?>){
	        js_show('four_gt_max_warning');
	      }else if(miRisk1 > miRisk2){
	        js_show('low_gt_high_warning');
	      }else if(miRisk2 > miRisk3){
		    js_show('two_gt_three_warning');
		  }else if(miRisk3 > miRisk4){
		    js_show('three_gt_four_warning');
		  }else{
	        trigger_event('save_risk_limits_event',3);
	      }

	  }
	  
    }
        
    updatePercentRisk1();
    updatePercentRisk2();
    updatePercentRisk3();
    updatePercentRisk4();    
        
    function confirm_risk_par_weight_save(){
      var mbOk = false;
      for(var i=1;i<=<?=count($maParameters)?>;i++){
        if(!isNaN(gebi('parameter_weight_'+i).value)){
          if(parseInt(gebi('parameter_weight_'+i).value) > 0){
            mbOk = true;
          }
        }
        else {
          mbOk = false;
          break;
        }
      }
      if (mbOk) trigger_event('confirm_risk_par_weight_save',3);
      else js_show('at_least_one_positive_warning');
      mbOk = null;
    }
    
    function disable_checkbox_clean_text_field(psControllerName,psCheckBoxKey) {
      if (!trim(gebi(psCheckBoxKey).value)) {
        gebi(psCheckBoxKey).value = '';
        gebi(psCheckBoxKey).disabled = true;
        gfx_check(psControllerName,psCheckBoxKey);
      }
    }

    var light = true;
    function setColor(color){
        var el = "lightColor";
		if(!light){
			el = "darkColor";
		}
		if(!color){
			color = gebi(el).value;
		}else{
			gebi(el).value = color;
		}
		gebi(el).style.backgroundColor = color;
    }

    if(gebi("lightColor").value){
    	setColor(gebi("lightColor").value);	
    }

    if(gebi("darkColor").value){
    	light=false;
    	setColor(gebi("darkColor").value);
    	light=true;	
    }
    

      <? 
        $moDefaultConfig = new ISMSDefaultConfig();
        echo $moDefaultConfig->getJSCode();
      ?>

      </script>
    <?   
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('nav_customization.xml');
?>