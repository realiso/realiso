<?php
include_once "include.php";
include_once $handlers_ref . "select/QuerySelectProfile.php";
include_once $handlers_ref . "QueryTotalUsers.php";

class ResetPasswordConfirm extends FWDRunnable {
  public function run() {
    $msTitle = FWDLanguage::getPHPStringValue('tt_reset_user_password','Resetar senha de usu�rio');
    $msMessage = FWDLanguage::getPHPStringValue('st_reset_user_password_confirm',"Voc� tem certeza que deseja resetar a senha do usu�rio e enviar a nova senha para seu e-mail?");
    
    $msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
            "soWindow = soPopUp.getOpener();" .
            "soWindow.reset_password();";
    
    ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,50);
  }
}

class ResetPasswordEvent extends FWDRunnable {
  public function run() {
    //teste para verificar se o sistema n�o est� sendo hakeado
    $moCtxUserTest = new ISMSUser();
    $moCtxUserTest->testPermissionToEdit(FWDWebLib::getObject("user_id")->getValue());
    
    if(ISMSLib::getConfigById(GENERAL_EMAIL_ENABLED)){
      
      $miUserId = FWDWebLib::getObject("user_id")->getValue();    
      $moUser = new ISMSUser();
      $moPassPolicy = new ISMSPasswordPolicy();
      $msNewPassword = $moPassPolicy->generateRandomPassword();
      $moUser->setFieldValue('user_password', md5($msNewPassword));
      $moUser->setFieldValue('user_must_change_password', true);
      if ($miUserId) {
        $moUser->update($miUserId);
        $moUser->fetchById($miUserId);
        $msReceiverEmail = $moUser->getFieldValue('user_email');
        $moEmailPreferences = new ISMSEmailPreferences();
        $msFormat = $moEmailPreferences->getEmailFormat($miUserId);
        $msSystemName = ISMSLib::getSystemName();
        $moMailer = new ISMSMailer();
        $moMailer->setFormat($msFormat);
        $moMailer->setTo($msReceiverEmail);
        $moMailer->setSubject("[$msSystemName] " . date('d/m/Y') . ' - '.FWDLanguage::getPHPStringValue('em_new_password','Nova Senha'));
        $moMailer->addContent('newPassword');
        $moMailer->setParameter('new_password',$msNewPassword);
        $moMailer->send();
        
        $moUserPasswordHistory = new ISMSUserPasswordHistory();
        $moUserPasswordHistory->addPassword(md5($msNewPassword),$miUserId);
      }
      echo "hide_warnings();";
      echo "js_show('warning_password_changed');";
    }else{
      trigger_error("Trying to resset a user password with the general email disabled!",E_USER_ERROR);
    }
  }
}

// Verifica se o insert/update n�o ir� exceder o limite de usu�rios
function checkProfile($piProfileId) {
  // carrega informa��es do perfil
  $moProfileId = new ISMSProfile();
  $moProfileId->fetchById($piProfileId);
  
  // s� testa o limite de usu�rios se o perfil do usu�rio que ser� inserido for diferente de leitor de documento
  if ($moProfileId->getFieldValue('profile_identifier') <> PROFILE_DOCUMENT_READER) {
    $moLicense = new ISMSLicense();
    $moQueryTotalUsers = new QueryTotalUsers(FWDWebLib::getConnection());
    $moQueryTotalUsers->makeQuery();
    $moQueryTotalUsers->executeQuery();
    if ($moQueryTotalUsers->getTotalUsers() >= $moLicense->getMaxUsers()) {      
      return false;      
    }
  }
  
  return true;
}

class SaveUserEvent extends FWDRunnable {
  public function run() {
    
    $miUserId = FWDWebLib::getObject("user_id")->getValue();
    //teste para verificar se o sistema n�o est� sendo hakeado no evento de update
    if($miUserId){
      $moCtxUserTest = new ISMSUser();
      $moCtxUserTest->testPermissionToEdit($miUserId);
    }
    
    $msUserName = FWDWebLib::getObject("user_name")->getValue();
    $msUserLogin = FWDWebLib::getObject("user_login")->getValue();
    $msUserEmail = FWDWebLib::getObject("user_email")->getValue();
    $miUserLanguage = FWDWebLib::getObject("select_language")->getValue();
    $miUserProfile = FWDWebLib::getObject("select_profile")->getValue();
    $msLastUserLogin = FWDWebLib::getObject("last_login")->getValue();
    $msLastUserEmail = FWDWebLib::getObject("last_email")->getValue();
    $mbChangePassword = trim(FWDWebLib::getObject("changePasswordController")->getValue(),":")?true:false;
    $mbAcessOldDocuments = trim(FWDWebLib::getObject("accessOldDocumentsController")->getValue(),":")?true:false;

	$moUser = new ISMSUser();
  	if($moUser->fetchByLogin($msUserEmail) && $moUser->getContextState($moUser->getFieldValue('user_id')) == CONTEXT_STATE_DELETED) { //nao pode inserir usuario com email na lixeira.
	  echo "hide_warnings();";
      echo "js_show('warning_existing_trash_email_error');";
      echo "gebi('user_email').focus();gebi('user_email').value='';";
      return;
	}
	$moUser = new ISMSUser();
	if($moUser->fetchByLogin($msUserLogin) && $moUser->getContextState($moUser->getFieldValue('user_id')) == CONTEXT_STATE_DELETED ) { //nao pode inserir usuario com login na lixeira.
      echo "hide_warnings();";
      echo "js_show('warning_existing_trash_login_error');";
      echo "gebi('user_login').focus();gebi('user_login').value='';";
      return;
    }
    $moUser = new ISMSUser();
    if ($msUserLogin != $msLastUserLogin && $msUserLogin != $msLastUserEmail && $moUser->fetchByLogin($msUserLogin)) { //nao pode inserir usuario com login que jah existe.
      echo "hide_warnings();";
      echo "js_show('warning_existing_login_error');";
      echo "gebi('user_login').focus();gebi('user_login').value='';";
      return;
    }
    $moUser->removeAllFilters();
    $moUser = new ISMSUser();
    if ($msUserEmail != $msLastUserEmail && $moUser->fetchByLogin($msUserEmail)) { //nao pode inserir usuario com email que jah existe.
      echo "hide_warnings();";
      echo "js_show('warning_existing_email_error');";
      echo "gebi('user_email').focus();gebi('user_email').value='';";
      return;
    }
    $moUser->removeAllFilters();
    
    /* POG para nao alterar o getValue da FWDView: concatenar um caractere no
     * inicio e outro no final da string em javascript e remov�-los em php */
    $msNewPasswd = FWDWebLib::getObject("pog_new_passwd")->getValue();
    $msNewPasswd = substr($msNewPasswd,1,strlen($msNewPasswd)-2);
    $msNewPasswdConfirm = FWDWebLib::getObject("pog_new_passwd_confirm")->getValue();
    $msNewPasswdConfirm = substr($msNewPasswdConfirm,1,strlen($msNewPasswdConfirm)-2);
    
    if ($msNewPasswd || $msNewPasswdConfirm) {
      if ($msNewPasswd===$msNewPasswdConfirm) {
        $moPassPolicy = new ISMSPasswordPolicy(); 
        if ($moPassPolicy->checkPassword($msNewPasswd)) {
          
          if ($miUserId) {
            // verificar senha atual
            $moUser2 = new ISMSUser();
            $moUser2->fetchById($miUserId);
            $moUser->setFieldValue('user_password',md5($msNewPasswd));
          }
          else {
            // insercao de usuario: tudo ok
            $moUser->setFieldValue('user_password',md5($msNewPasswd));
          }
          
        }
        else {
          echo "hide_warnings();";
          echo "js_show('warning_password_policy_error');";
          return;
        }
      }else{
        echo "hide_warnings();";
        echo "js_show('warning_different_passwords_error');";
        return;
      }
    }
    
    $moUser->setFieldValue('user_name', $msUserName);
    $moUser->setFieldValue('user_login', $msUserLogin);
    $moUser->setFieldValue('user_email', $msUserEmail);
    $moUser->setFieldValue('user_language', $miUserLanguage);
    $moUser->setFieldValue('user_profile_id', $miUserProfile);
    $moUser->setFieldValue('user_must_change_password', $mbChangePassword);
    
    $miCurrentUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    $mbRefresh = false;
    
    if ($miUserId){
       /*if (!checkProfile($miUserProfile)) {
        $msTitle = FWDLanguage::getPHPStringValue('tt_profile_edit_error','Erro ao modificar o perfil');
        $msMessage = FWDLanguage::getPHPStringValue('wn_users_limit_reached','O n�mero de usu�rios licenciados foi alcan�ado. A partir de agora somente usu�rios com o perfil <b>Leitor de Documentos</b> (usu�rios que possuem somente permiss�o de leitura de documentos) poder�o ser utilizados. Adquira mais licen�as de usu�rios clicando <a target="_blank" href=%link%>aqui</a>.');
        $msMessage = str_replace('%link%', ISMSLib::getBuyNowUrl(), $msMessage);
        ISMSLib::openOk($msTitle, $msMessage, '', 85);
        die(1);
       } */ // Como estamos fazendo apenas update n�o importa se eu atingi o numero m�ximo de usu�rios (estava causando bug, n�o deixando alterar o �nico user em uma conta de 1 -> Francisco
       $moUser->update($miUserId);

       $syncSite = new ISMSSyncSite();
       $syncSite->updateLogin($miUserId, $msUserLogin, $msUserEmail);
      
       //caso tenha sido alterada o profile do usu�rio que est� logado no sistema
       //as permiss�es desse usu�rio s�o atualizadas na sess�o
       if( ($miUserId == $miCurrentUserId) && (FWDWebLib::getObject("pog_current_profile")->getValue()!=$miUserProfile) ){
            $moProfile = new ISMSProfile();
            $moProfile->setFieldValue('profile_id', $miUserProfile);
            $caUserProfile = $moProfile->getDeniedAcls(true);
            $mbRefresh = true;
       }
    }else {
 
      if (!checkProfile($miUserProfile)) {
        $msTitle = FWDLanguage::getPHPStringValue('tt_user_insert_error','Limite de usu�rios alcan�ado');
        $msMessage = FWDLanguage::getPHPStringValue('wn_users_limit_reached','O n�mero de usu�rios licenciados foi alcan�ado. A partir de agora somente usu�rios com o perfil <b>Leitor de Documentos</b> (usu�rios que possuem somente permiss�o de leitura de documentos) poder�o ser utilizados. Adquira mais licen�as de usu�rios clicando <a target="_blank" href=%link%>aqui</a>.');
        $msMessage = str_replace('%link%', ISMSLib::getBuyNowUrl(), $msMessage);
        ISMSLib::openOk($msTitle, $msMessage, '', 85);
        die(1);
      }
      $miUserId = $moUser->insert(true);

      $syncSite = new ISMSSyncSite();
      $syncSite->insertLogin($miUserId, $msUserLogin, $msUserEmail);

      
      if ($miUserId) {
        /*
         * Enviar email avisando o usu�rio. 
         */
        $moMailer = new ISMSMailer();        
        $moMailer->setTo($msUserEmail);
        $msSystemName = ISMSLib::getSystemName();
        $moMailer->setSubject("[$msSystemName] " . date('d/m/Y') . ' - '.FWDLanguage::getPHPStringValue('em_new_user','Bem-vindo ao Real ISMS'));        
        $moMailer->addContent('ismsNewUser');
        $moMailer->setParameter('user_name',$msUserName);
        $moMailer->setParameter('user_login',$msUserLogin);        
        $moMailer->setParameter('user_password',$msNewPasswd);
        $moSaasAdmin = new ISMSSaaSAdmin();
        if(in_array(ISMSSaaS::getConfig(ISMSSaaS::LICENSE_TYPE), ISMSLicense::getTrialLicenseTypes())){
          $msUrl = $moSaasAdmin->getConfig(ISMSSaaSAdmin::TRIAL_URL).ISMSSaaS::getConfig(ISMSSaaS::LICENSE_CLIENT_NAME);
        }else{
          $msUrl = $moSaasAdmin->getConfig(ISMSSaaSAdmin::COMMERCIAL_URL).ISMSSaaS::getConfig(ISMSSaaS::LICENSE_CLIENT_NAME);
        }        
        $moMailer->setParameter('link',$msUrl);
        $moMailer->setFooterMessage(FWDLanguage::getPHPStringValue('em_new_user_footer_bl','<b>Para maiores informa��es entre em contato com o security officer.</b>'));        
        $moMailer->send();
      }
    }
    
    if ($msNewPasswd) { //Se o usuario indicou uma nova senha, acrescentar no banco de hist�rico de senhas.
      $moUserPasswordHistory = new ISMSUserPasswordHistory();
      $moUserPasswordHistory->addPassword(md5($msNewPasswd),$miUserId);
    }
    
    if ($mbAcessOldDocuments){ //Verifica se deve liberar o acesso aos documentos j� criados para o novo usu�rio.

        $moPMDocument = new PMDocument();
        $moPMDocument->select();
        while($moPMDocument->fetch()) {
          
          $doc_id = $moPMDocument->getFieldValue('document_id');
         
          $moPMDocumentReader = new PMDocumentReader();
          $moPMDocumentReader->createFilter($doc_id,'document_id');
          $moPMDocumentReader->createFilter($miUserId,'user_id');
          $moPMDocumentReader->select();
          if (!$moPMDocumentReader->fetch()) {
            $moPMDocumentReader->setFieldValue('document_id',$doc_id);
            $moPMDocumentReader->setFieldValue('user_id',$miUserId);
            $moPMDocumentReader->setFieldValue('manual',true);
            $moPMDocumentReader->insert();
          }

        }
              
    }

    /*
     * Verifica se a l�ngua foi alterada e se o usu�rio que est� sendo
     * editado � o mesmo que est� usando o sistema. Caso positivo, faz
     * o refresh no sistema para atualizar a tradu��o.
     */    
    if (($miCurrentUserId == $miUserId) && ($miUserLanguage != FWDWebLib::getObject('current_language')->getValue())) $mbRefresh = true;    
        
    $msRefresh = "";
    if ($mbRefresh) {
      if(ISMSLib::getAdminPermission()) {
        $msRefresh = "if (soPopUpManager.getRootWindow().refresh) soPopUpManager.getRootWindow().refresh();";
      } elseif(ISMSLib::getGestorPermission()) {
        $msRefresh = 'soTabManager.getRootWindow().location.href = "../../tab_main.php";';
      } else {
        echo "isms_open_popup('popup_no_access','popup_no_access.php','','true',122,350);";
        return;
      }
    }
    echo "soWindow = soPopUpManager.getPopUpById('popup_user_edit').getOpener();"
        ."if (soWindow.refresh_grid)"        
        ."  soWindow.refresh_grid();"
        . $msRefresh        
        ."soPopUpManager.closePopUp('popup_user_edit');";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new SaveUserEvent("save_user_event"));
    $moStartEvent->addAjaxEvent(new ResetPasswordConfirm("reset_password_confirm"));
    $moStartEvent->addAjaxEvent(new ResetPasswordEvent("reset_password_event"));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moHandler = new QuerySelectProfile(FWDWebLib::getConnection());    
    $moSelect = FWDWebLib::getObject('select_profile');
    $moSelect->setQueryHandler($moHandler);
    $moSelect->populate();
    
    // Preenche o ToolTip da pol�tica de senhas
    $moPassPolicy = new ISMSPasswordPolicy();
    FWDWebLib::getObject('tooltip_helper_password_policy')->setValue($moPassPolicy->getPolicyDescriptionAsString());
    
    /*
     * Popula o select com as linguagens dispon�veis para o sistema 
     */
    $moLanguageSelect = FWDWebLib::getObject('select_language');
    $maAvailableLanguages = FWDLanguage::getLanguages();
    foreach ($maAvailableLanguages as $miKey => $msValue) {
      $moLanguageSelect->setItemValue($miKey, $msValue);      
    }
    
    $miUserId = FWDWebLib::getObject("user_id")->getValue();    
    $moWindowTitle = FWDWebLib::getObject("window_title");
    $moPasswordText = FWDWebLib::getObject("new_password");
    $moPasswordConfirmText = FWDWebLib::getObject("new_password_confirm");
    
    if ($miUserId) {
       $moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_user_editing', "Edi��o de Usu�rio"));
       FWDWebLib::getObject("reset_password")->setAttrDisplay("true");

      //teste para verificar se o sistema n�o est� sendo hakeado
      $moCtxUserTest = new  ISMSUser();
      $moCtxUserTest->testPermissionToEdit($miUserId);

      $moUser = new ISMSUser();
      $moUser->fetchById($miUserId);      
      FWDWebLib::getObject("user_name")->setValue($moUser->getFieldValue('user_name'));
      FWDWebLib::getObject("user_login")->setValue($moUser->getFieldValue('user_login'));
      FWDWebLib::getObject("last_login")->setValue($moUser->getFieldValue('user_login'));
      FWDWebLib::getObject("user_email")->setValue($moUser->getFieldValue('user_email'));
      FWDWebLib::getObject("last_email")->setValue($moUser->getFieldValue('user_email'));
      FWDWebLib::getObject("select_language")->checkItem($moUser->getFieldValue('user_language'));
      FWDWebLib::getObject("select_profile")->checkItem($moUser->getFieldValue('user_profile_id'));
      FWDWebLib::getObject("pog_current_profile")->setValue($moUser->getFieldValue('user_profile_id'));
      if ($moUser->getFieldValue('user_must_change_password')) FWDWebLib::getObject('changePasswordController')->checkItem(1);
      /*
       * Guarda a l�ngua atual do usu�rio para na hora de salvar verificar
       * se deve dar um submit na pagina para atualizar as strings caso
       * a l�ngua tenha sido alterada.
       */
      FWDWebLib::getObject('current_language')->setValue($moUser->getFieldValue('user_language')); 

      /*
       * Se o usu�rio for AD ent�o n�o precisa mostrar campo de senha, etc...
       */
      if($moUser->isAD()){
        FWDWebLib::getObject("new_password")->setAttrDisplay("false");
        FWDWebLib::getObject("new_passwd")->setAttrDisplay("false");
        FWDWebLib::getObject("static_helper_password_policy")->setAttrDisplay("false");
        FWDWebLib::getObject("new_password_confirm")->setAttrDisplay("false");
        FWDWebLib::getObject("new_passwd_confirm")->setAttrDisplay("false");
        FWDWebLib::getObject("label_change_password_on_login")->setAttrDisplay("false");
        FWDWebLib::getObject("changePassword")->setAttrDisplay("false");
        FWDWebLib::getObject("reset_password")->setAttrDisplay("false");

        FWDWebLib::getObject("user_name")->setAttrDisabled("true");
        FWDWebLib::getObject("user_login")->setAttrDisabled("true");
        FWDWebLib::getObject("user_email")->setAttrDisabled("true");
      }

    }
    else {
      //teste para verificar se o sistema n�o est� sendo hakeado
      $moCtxUserTest = new ISMSUser();
      $moCtxUserTest->testPermissionToInsert();
      

      $moPasswordText->setValue(FWDLanguage::getPHPStringValue('lb_password_cl', "Senha:"));
      $moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_user_adding', "Adi��o de Usu�rio"));
      
      $moPasswordText->setAttrMustFill("true");
      $moPasswordConfirmText->setAttrMustFill("true");
    }   
  
    if(!ISMSLib::getConfigById(GENERAL_EMAIL_ENABLED)){
      FWDWebLib::getObject('reset_password')->setShouldDraw(false);
    }

    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
      gebi('user_name').focus();
      
      function reset_password() {
        trigger_event('reset_password_event', 3);
      }
      function isEMailValid(){
        var mbReturn = false;
        moElem = gebi('user_email'); 
        re=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if(re.exec(moElem.value)){
          mbReturn = true;
        }
        moElem = re = null;
        return mbReturn;
      }
      
      function hide_warnings() {
        js_hide('warning_current_password_error');
        js_hide('warning_different_passwords_error');
        js_hide('warning_password_policy_error');
        js_hide('warning_password_changed');
        js_hide('warning_existing_login_error');
        js_hide('warning_existing_email_error');
        js_hide('warning_existing_trash_email_error');
        js_hide('warning_existing_trash_login_error');
        gobi('warning_wrong_email').hide();
      }
      </script>
    <?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_user_edit.xml");
?>
