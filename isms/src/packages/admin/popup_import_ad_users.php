<?php
include_once "include.php";
include_once $handlers_ref . "select/QuerySelectProfile.php";
include_once $handlers_ref . "QueryTotalUsers.php";

class LoadUsersEvent extends FWDRunnable {
  public function run(){

    $ismsAd = new ISMSActiveDirectory();
    $users = $ismsAd->getAllUsers();

    if(!$users){
      echo "gobi('no_users_warning').show(); setTimeout('gobi(\\'no_users_warning\\').hide()',5000);";
      return;
    }

    $tmpAdUser = new TmpAdUser();
    $tmpAdUser->deleteAll();

    // s� exibe contas que n�o existam ainda no sistema. (identificado pelo login)
    for ($i=0; $i < $users["count"]; $i++){
      $user = new ISMSUser();

      if (!$user->fetchByLogin($users[$i]["samaccountname"][0])){
        $tmpAdUser = new TmpAdUser();
        $tmpAdUser->setFieldValue("name", $users[$i]["givenname"][0] . " " . $users[$i]["sn"][0]);
        $tmpAdUser->setFieldValue("login", $users[$i]["samaccountname"][0]);
        $tmpAdUser->setFieldValue("email", $users[$i]["mail"][0]);
        $tmpAdUser->insert();
      }
    }

    echo "js_refresh_grid('grid_ad_users');";
  }
}

class ImportUserSelectedEvent extends FWDRunnable {

  // Verifica se o insert/update n�o ir� exceder o limite de usu�rios
  private function checkLicense($piProfileId, $numberOfNewUsers = 0) {
    $moProfileId = new ISMSProfile();
    $moProfileId->fetchById($piProfileId);
    
    // s� testa o limite de usu�rios se o perfil do usu�rio que ser� inserido for diferente de leitor de documento
    if ($moProfileId->getFieldValue('profile_identifier') <> PROFILE_DOCUMENT_READER) {
      $moLicense = new ISMSLicense();
      $moQueryTotalUsers = new QueryTotalUsers(FWDWebLib::getConnection());
      $moQueryTotalUsers->makeQuery();
      $moQueryTotalUsers->executeQuery();

      $totalUsersTest = $moQueryTotalUsers->getTotalUsers() + $numberOfNewUsers;

      if ($totalUsersTest > $moLicense->getMaxUsers()) {      
        return false;      
      }
    }
    
    return true;
  }

  public function run(){
    $soWebLib = FWDWebLib::getInstance();

    $selectedValues = $soWebLib->getObject('grid_ad_users')->getSelectValue();

    if(!$selectedValues){
      echo "gobi('no_users_selected_warning').show();"; 
      echo "setTimeout('gobi(\\'no_users_selected_warning\\').hide()', 5000);";
      return;
    }

    // checar licen�a
    $numberOfNewUsers = count($selectedValues);
    $profile = FWDWebLib::getObject("select_profile")->getValue();

    if(!$this->checkLicense($profile, $numberOfNewUsers)){
      echo "gobi('license_limit_warning').show();"; 
      echo "setTimeout('gobi(\\'license_limit_warning\\').hide()', 5000);";
      return;
    }

    foreach($selectedValues as $key => $value){
      $tmpAdUser = new TmpAdUser();
      $tmpAdUser->createFilter($value, 'login');
      $tmpAdUser->select();

      $userName = "";
      $userLogin = "";
      $userEmail = "";

      while($tmpAdUser->fetch()){
        $userName = $tmpAdUser->getFieldValue("name");
        if(!$userName)
          $userName = "Real User";

        $userLogin = $tmpAdUser->getFieldValue("login");

        $userEmail = $tmpAdUser->getFieldValue("email");

        if(!$userEmail)
          $userEmail = $userLogin . "@change.your.email.com";
      }
      
      $ismsUser = new ISMSUser();
      $ismsUser->setFieldValue('user_name', $userName);
      $ismsUser->setFieldValue('user_login', $userLogin);
      $ismsUser->setFieldValue('user_email', $userEmail);
      $ismsUser->setFieldValue('user_language', LANGUAGE_DEFAULT);
      $ismsUser->setFieldValue('user_profile_id', $profile);
      $ismsUser->setFieldValue('user_password', "th1zUs3rD0ntN33daP455");
      $ismsUser->setFieldValue('user_is_ad', true);

      $ismsUser->insert();

      $tmpAdUser->delete();
    }

    echo "js_refresh_grid('grid_ad_users');";
  }
}

class ScreenBeforeEvent extends FWDRunnable {

  private function loadProfileSelect(){
    $moHandler = new QuerySelectProfile(FWDWebLib::getConnection());    

    $soWebLib = FWDWebLib::getInstance();

    $moSelect = FWDWebLib::getObject('select_profile');
    $moSelect->setQueryHandler($moHandler);
    $moSelect->populate();
  }

  private function loadAdUsersGrid(){
    // definir um QueryGridAdUsers
    $soGridDataSet = new FWDDBDataSet(FWDWebLib::getConnection()); 
    $soGridDataSet->addFWDDBField(new FWDDBField("sname", "name", DB_STRING)); 
    $soGridDataSet->addFWDDBField(new FWDDBField("slogin", "login", DB_STRING)); 
    $soGridDataSet->addFWDDBField(new FWDDBField("semail", "email", DB_STRING)); 

    $soWebLib = FWDWebLib::getInstance();

    $soWebLib->getObject('grid_ad_users')->setDataSet($soGridDataSet);
    $soWebLib ->getObject('grid_ad_users')->setQuery("select sname as name, slogin as login, semail as email from tmp_ad_user");
    $soWebLib ->getObject('grid_ad_users')->populate();
  }

  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new LoadUsersEvent("load_users"));
    $moStartEvent->addAjaxEvent(new ImportUserSelectedEvent("import_user_selected"));
    
    $this->loadAdUsersGrid();
    $this->loadProfileSelect();
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_import_ad_users.xml");
?>