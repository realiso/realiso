<?php

include_once 'include.php';
include_once $classes_isms_ref.'nonauto/FeedbackEvent.php';

class LogoutEvent extends FWDRunnable {
  public function run() {
      $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
      $moSession->logout();
      echo 'document.location.href = "'.FWDWebLib::getInstance()->getSysRef().'login.php";';
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new LogoutEvent("logout_event"));
    $moStartEvent->addAjaxEvent(new FeedbackEvent('feedback_event'));

    if(!ISMSLib::getGestorPermission()){
      $moHeaderChangeMode = FWDWebLib::getObject('header_change_mode');
      $moHeaderUserPrefBox = FWDWebLib::getObject('header_user_pref')->getObjFWDBox();

      $moHeaderChangeMode->setShouldDraw(false);

      $miNewLeft = $moHeaderUserPrefBox->getAttrLeft() + $moHeaderChangeMode->getObjFWDBox()->getAttrWidth();
      $moHeaderUserPrefBox->setAttrLeft($miNewLeft);
    }else{
      switch(FWDLanguage::getSelectedLanguage()){
        case LANGUAGE_PORTUGUESE_ISMS: break;
        case LANGUAGE_ENGLISH_ISMS:
          $miShiftRight = 40;
          FWDWebLib::getObject('header_change_mode')->getObjFWDBox()->setAttrLeft(FWDWebLib::getObject('header_change_mode')->getObjFWDBox()->getAttrLeft()+$miShiftRight);
          FWDWebLib::getObject('header_change_mode')->getObjFWDBox()->setAttrWidth(FWDWebLib::getObject('header_change_mode')->getObjFWDBox()->getAttrWidth()-$miShiftRight);

          FWDWebLib::getObject('vb_goto_gestor')->getObjFWDBox()->setAttrWidth(FWDWebLib::getObject('vb_goto_gestor')->getObjFWDBox()->getAttrWidth()-$miShiftRight);
          FWDWebLib::getObject('st_goto_gestor')->getObjFWDBox()->setAttrWidth(FWDWebLib::getObject('st_goto_gestor')->getObjFWDBox()->getAttrWidth()-$miShiftRight);

          FWDWebLib::getObject('header_user_pref')->getObjFWDBox()->setAttrLeft(FWDWebLib::getObject('header_user_pref')->getObjFWDBox()->getAttrLeft()+$miShiftRight);
        break;
        default: break;
      }

    }
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){

    $moConfig = new ISMSConfig();
    $moWebLib = FWDWebLib::getInstance();
    $moISMSLib = ISMSLib::getInstance();
    $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());

    $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    $moSession->setMode(ADMIN_MODE);

    $moConfig = new ISMSConfig();
    if($moConfig->getConfigFromDB(CLIENT_ID_ASAAS)) {
      $moISMSASaaS = new ISMSASaaS();
      $moISMSASaaS->setNewCheckPointASaaS(utf8_encode("Acessou �rea administrativa"), utf8_encode("M�dulo administrativo"));
    }

    $databaseWasRemoved = $moConfig->getConfigFromDB(RDB_RESET_DATA_BASE);
    $whenDatabaseWasRemoved = $moConfig->getConfigFromDB(RDB_DATE_RESET_DATA_BASE);

    if($databaseWasRemoved==0 && !$whenDatabaseWasRemoved){
        $moWebLib->getObject('cleanbase_header_user_pref')->setAttrDisplay("true");
    }
    else{
        $moWebLib->getObject('cleanbase_header_user_pref')->setAttrDisplay("false");
    }

    $moWebLib->getObject('user_name')->concatValue($moSession->getUserName(true));

    //  seta o link de help
    $msLink = 'http://support.realiso.com';
    $moHelpEvent = new FWDClientEvent();
    $moHelpEvent->setAttrEvent('onClick');
    $moHelpEvent->setAttrValue("goto_url('{$msLink}',true);");
    FWDWebLib::getObject('help_button')->addObjFWDEvent($moHelpEvent);

    if(in_array(ISMSSaaS::getConfig(ISMSSaaS::LICENSE_TYPE), ISMSLicense::getTrialLicenseTypes())){
      $moSaasAdmin = new ISMSSaaSAdmin();
      $msUri = ISMSLib::getBuyNowUrl();
      $moEvent = new FWDClientEvent();
      $moEvent->setAttrEvent('onClick');
      $moEvent->setAttrValue("goto_url('{$msUri}',true);");
      FWDWebLib::getObject('buy_now_link')->addObjFWDEvent($moEvent);
    }else{
      $moBuyNow = FWDWebLib::getObject('buy_now');
      $moSurveyBox = FWDWebLib::getObject('survey')->getObjFWDBox();

      $moBuyNow->setShouldDraw(false);

      $miNewLeft = $moSurveyBox->getAttrLeft() - $moBuyNow->getObjFWDBox()->getAttrWidth();
      $moSurveyBox->setAttrLeft($miNewLeft);
    }

    $msClassification = ISMSLib::getConfigById(GENERAL_SYSTEM_CLASSIFICATION);
    $msClassificationDest = ISMSLib::getConfigById(GENERAL_SYSTEM_CLASSIFICATION_DEST);
    if(trim($msClassification) && trim($msClassificationDest)){
      FWDWebLib::getObject('system_classification')->setValue("<b>{$msClassification}</b>: {$msClassificationDest}");
    }

    $miContextType = FWDWebLib::getObject('context_type')->getValue();
    if ($miContextType == CONTEXT_USER)
    FWDWebLib::getObject('tabgroup_main')->setAttrLoadTabItemIndex(1);

    $msLastLogin = $moSession->getLastLogin();
    $msLastIP = $moSession->getLastIP();
    $msLabelLastLogin = FWDLanguage::getPHPStringValue('to_last_login_cl','�ltimo Login:');
    $msLabelIP = FWDLanguage::getPHPStringValue('to_ip_cl','IP:');
    if($msLastLogin){
      $msTooltipLastLogin = "$msLabelLastLogin $msLastLogin &nbsp; $msLabelIP $msLastIP";
      FWDWebLib::getObject('last_login')->setValue($msTooltipLastLogin);
    }else{
      FWDWebLib::getObject('last_login')->setShouldDraw(false);
    }

    $moEvent = new FWDClientEvent();
    $moEvent->setAttrEvent('onClick');
    $msEvent = "goto_url('" . FWDLanguage::getPHPStringValue('st_survey_link','http://www.surveymonkey.com/s.aspx?sm=Iv5Lu2xahyHtsNrd2ShHBA_3d_3d') . "',true);";
    $moEvent->setAttrValue($msEvent);
    FWDWebLib::getObject('survey')->addObjFWDEvent($moEvent);

    //instala a seguran�a de ACL na p�gina
    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());

    FWDWebLib::getObject('dialog')->setAttrDisplay('false');
    $moWebLib->dump_html(FWDWebLib::getObject('dialog'));
?>
<script language="javascript">
  setISMSMode(ADMIN_MODE);
  window.onresize = center_dialog_div;
  document.body.className = 'BODYBackground';
  center_dialog_div();
  js_show('dialog');

  function refresh(){
    document.forms[0].submit();
  }
</script>
<?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("tab_main.xml");
?>
<!--[if lt IE 7]>
<script defer type='text/javascript' language='javascript'>
  correct_png();
</script>
<![endif]-->