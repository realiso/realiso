<?php include_once 'include.php';

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
  	$color = FWDWebLib::getObject('color')->getValue();
  	if(!$color){
  		$color = 'aaaaaa';
  	}
  	FWDWebLib::getObject('colorPicker')->setAttrSelectedColor("#".$color);
  }
}
FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('colorPicker.xml');
FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));

?>
