<?php
include_once "include.php";
include_once $handlers_ref . "grid/improvement/QueryGridCategory.php";
include_once $handlers_ref . "grid/improvement/QueryGridSolutions.php";

class SolutionsDrawGrid extends FWDDrawGrid {
  public function drawItem(){
    switch($this->ciColumnIndex){
      case $this->getIndexByAlias('solution_id'):
        $maMenuACLs = array('edit','delete');
        $maAllowed = array();
        if(ISMSLib::userHasACL('M.L.6.5')){
          $maAllowed[] = 'edit';
        }
        if(ISMSLib::userHasACL('M.L.6.6')){
          $maAllowed[] = 'delete';
        }
        $moACL = FWDACLSecurity::getInstance();
        $moACL->setNotAllowed(array_diff($maMenuACLs,$maAllowed));
        $moMenu = FWDWebLib::getObject('menu_solution');
        $moGrid = FWDWebLib::getObject('grid_solutions');
        $msLine = $moGrid->getAttrName().'_acl_'.$this->ciRowIndex;
        FWDGridMenuACLSecurity::getInstance()->installGridMenuACL($moMenu,$moACL,$moGrid,$msLine);

        $this->coCellBox->setIconSrc("icon-ci_solution.gif");
        $this->coCellBox->setObjFWDToolTip(ISMSLib::getInstance()->getToolTipCreateModify($this->getFieldValue('user_create_name'),$this->getFieldValue('date_create'),$this->getFieldValue('user_edit_name'),$this->getFieldValue('date_edit')));
        return $this->coCellBox->draw();
        break;
      case $this->getIndexByAlias('solution_problem'):
        $this->coCellBox->setAttrStringNoEscape("true");
        if(ISMSLib::userHasACL('M.L.6.5')){
          $this->coCellBox->setValue("<a href='javascript:open_inc_solution(".$this->getFieldValue('solution_id').",".CONTEXT_CI_SOLUTION.",\"".uniqid()."\")'>".$this->getFieldValue('solution_problem')."</a>");
        }else{
          $this->coCellBox->setValue("<a href='javascript:open_visualize(".$this->getFieldValue('solution_id').",".CONTEXT_CI_SOLUTION.",\"".uniqid()."\")'>".$this->getFieldValue('solution_problem')."</a>");
        }
        return $this->coCellBox->draw();
        break;
    break;
      default:
        return parent::drawItem();
    }
  }
}

class CategoryDrawGrid extends FWDDrawGrid {
  public $caACL = array();
  public function drawItem(){
    switch($this->ciColumnIndex){
      case $this->getIndexByAlias('category_id'):
        $maMenuACLs = array('mi_edit','delete');
        $maAllowed = array();
        if(ISMSLib::userHasACL('M.L.6.2')){
          $maAllowed[] = 'mi_edit';
        }
        if(ISMSLib::userHasACL('M.L.6.3')){
          $maAllowed[] = 'delete';
        }
        $moACL = FWDACLSecurity::getInstance();
        $moACL->setNotAllowed(array_diff($maMenuACLs,$maAllowed));
        $moMenu = FWDWebLib::getObject('menu');
        $moGrid = FWDWebLib::getObject('grid_category');
        $msLine = $moGrid->getAttrName().'_acl_'.$this->ciRowIndex;
        FWDGridMenuACLSecurity::getInstance()->installGridMenuACL($moMenu,$moACL,$moGrid,$msLine);
        $this->coCellBox->setIconSrc("icon-ci_category.gif");
        $this->coCellBox->setObjFWDToolTip(ISMSLib::getInstance()->getToolTipCreateModify($this->getFieldValue('user_create_name'),$this->getFieldValue('date_create'),$this->getFieldValue('user_edit_name'),$this->getFieldValue('date_edit')) );
        return $this->coCellBox->draw();
      break;
      case $this->getIndexByAlias('category_name'):
          $this->coCellBox->setAttrStringNoEscape("true");
          $this->coCellBox->setValue("<a href='javascript:enter_category(".$this->getFieldValue('category_id').",".CONTEXT_CI_CATEGORY.",\"".uniqid()."\")'>".$this->getFieldValue('category_name')."</a>");
          return $this->coCellBox->draw();
        break;
      default:
        return parent::drawItem();
    }
  }
}

class CategoryConfirmRemove extends FWDRunnable {
  public function run() {
    $msTitle = FWDLanguage::getPHPStringValue('tt_category_remove','Remover Categoria');
    $msMessage = FWDLanguage::getPHPStringValue('st_category_remove',"Voc� tem certeza de que deseja remover a categoria <b>%category_name%</b>?");

    $miCategoryId = intval(FWDWebLib::getObject('selected_category_id')->getValue());
    $moCategory = new CICategory();
    $moCategory->fetchById($miCategoryId);
    $msCategoryName = ISMSLib::truncateString($moCategory->getFieldValue('category_name'), 70);
    $msMessage = str_replace('%category_name%',$msCategoryName,$msMessage);

    $msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
                    "soWindow = soPopUp.getOpener();" .
                    "soWindow.remove_category();";

    ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,50);
  }
}

class RemoveCategoryEvent extends FWDRunnable {
  public function run() {
    $moIncident = new CIIncident();
    $moIncident->getFieldByAlias('incident_category_id')->addFilter(new FWDDbFilter('=',FWDWebLib::getObject('selected_category_id')->getValue()));
    $moIncident->select();
    if($moIncident->fetch()){
        $msTitle = FWDLanguage::getPHPStringValue('tt_category_remove_error','Erro ao Remover Categoria');
        $msMessage = FWDLanguage::getPHPStringValue('st_category_remove_error',"N�o � possivel remover a categoria <b>%category_name%</b> porque ela esta relacionada a um ou mais incidentes!");

        $miCategoryId = FWDWebLib::getObject('selected_category_id')->getValue();
        $moCategory = new CICategory();
        $moCategory->fetchById($miCategoryId);
        $msCategoryName = $moCategory->getFieldValue('category_name');
        $msMessage = str_replace('%category_name%',$msCategoryName,$msMessage);
        ISMSLib::openOk($msTitle,$msMessage,"",50);
    }else{
      $miCategoryId = intval(FWDWebLib::getObject('selected_category_id')->getValue());
      $moCtxUserTest = new CICategory();
      $moCtxUserTest->testPermissionToDelete($miCategoryId);

      $moCategory = new CICategory();
      $moCategory->delete($miCategoryId);
      echo "refresh_grid();";
    }
  }
}

class SolutionConfirmRemove extends FWDRunnable {
  public function run() {
    $msTitle = FWDLanguage::getPHPStringValue('tt_solution_remove','Remover Solu��o');
    $msMessage = FWDLanguage::getPHPStringValue('st_solution_remove',"Voc� tem certeza de que deseja remover esta solu��o?");

    $msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
                    "soWindow = soPopUp.getOpener();" .
                    "soWindow.remove_solution();";

    ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,50);
  }
}

class RemoveSolutionEvent extends FWDRunnable {
  public function run() {
    $miSolutionId = intval(FWDWebLib::getObject('selected_solution_id')->getValue());
    $moCtxUserTest = new CISolution();
    $moCtxUserTest->testPermissionToDelete($miSolutionId);

    $moSolution = new CISolution();
    $moSolution->delete($miSolutionId);
  }
}

class ChangeScrollingPath extends FWDRunnable{
  public function run(){
    echo ISMSLib::getPathScrollCode(LIBRARIES_MODE,CONTEXT_CI_CATEGORY,FWDWebLib::getObject('root_category_id')->getValue(),true,'enter_category');
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new CategoryConfirmRemove('category_confirm_remove'));
    $moStartEvent->addAjaxEvent(new RemoveCategoryEvent('remove_category_event'));
    $moStartEvent->addAjaxEvent(new SolutionConfirmRemove('solution_confirm_remove'));
    $moStartEvent->addAjaxEvent(new RemoveSolutionEvent('remove_solution_event'));
    $moStartEvent->addAjaxEvent(new ChangeScrollingPath('change_scrolling_path'));

    //instala a seguran�a de ACL na p�gina
    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
    //FWDMenuACLSecurity::getInstance()->installMenuACL(FWDWebLib::getObject('menu'),FWDACLSecurity::getInstance());

    $moGrid = FWDWebLib::getObject("grid_category");
    $moHandler = new QueryGridCategory(FWDWebLib::getConnection());
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new CategoryDrawGrid('grid_category'));

    //categoryid
    $miCategoryId = FWDWebLib::getObject('root_category_id')->getValue();
    $moGrid = FWDWebLib::getObject("grid_solutions");
    $moHandler = new QueryGridSolutions(FWDWebLib::getConnection());
    $moHandler->setCategoryId($miCategoryId);
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new SolutionsDrawGrid('grid_solutions'));
  }
}


class ScreenEvent extends FWDRunnable {
  public function run(){
    $msEcho = "";
    $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    if($moSession->attributeExists('categoryid')){
      $msEcho = " enter_category(".$moSession->getAttrCategoryId().");";
      $moSession->deleteAttribute('categoryid');
    }

    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        function refresh_grid(){
          if(gebi('root_category_id').value != 0){
            js_refresh_grid('grid_solutions');
          }else{
            js_refresh_grid('grid_category');
          }
        }
        function remove_category() {
          trigger_event('remove_category_event',3);
          refresh_grid();
        }
        function remove_solution() {
          trigger_event('remove_solution_event',3);
          refresh_grid();
        }
        function enter_category(category_id) {
          gebi('root_category_id').value = category_id;
          if (category_id > 0) {
            js_show('solutions_viewgroup');
            js_hide('category_viewgroup');
          } else {
            js_show('category_viewgroup');
            js_hide('solutions_viewgroup');
          }
          refresh_grid();
          trigger_event('change_scrolling_path',3);
        }
        
        function open_inc_solution(piId){
          isms_open_popup('popup_incident_solution_edit','packages/libraries/popup_incident_solution_edit.php?solution='+piId,'','true',312,400);
        }
        
        <? echo $msEcho ?>
      </script>
    <?
    ISMSLib::getPathScrollCode(LIBRARIES_MODE,CONTEXT_CI_CATEGORY,FWDWebLib::getObject('root_category_id')->getValue(),false,'enter_category');
  }
}
//testa se a licen�a utilizada permite acessar o m�dulo de incidente
ISMSLib::testUserPermissionToIncidentMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_incident_category.xml");

?>