<?php
include_once 'include.php';

function getCategoryFromFields(){
  $moCategory = new RMCategory();

  $msCategoryName = FWDWebLib::getObject('category_name')->getValue();
  $msCategoryDescription = FWDWebLib::getObject('category_description')->getValue();
  $miCategoryParent = FWDWebLib::getObject('root_category_id')->getValue();

  if($miCategoryParent) $moCategory->setFieldValue('category_parent_id', $miCategoryParent);
  $moCategory->setFieldValue('category_name', $msCategoryName);
  $moCategory->setFieldValue('category_description', $msCategoryDescription);
  
  return $moCategory;
}

function save($poContext,$pbHasSensitiveChanges,$piContextId=0){
  if($piContextId){
    //teste para verificar se o sistema n�o est� sendo hakeado no evento de update
    $moCtxUserTest = new RMCategory();
    $moCtxUserTest->testPermissionToEdit($piContextId);
    
    $poContext->update($piContextId,true,$pbHasSensitiveChanges);
  }else{
    $poContext->insert();
  }
  echo "soWindow = soPopUpManager.getPopUpById('popup_category_edit').getOpener();"
      ."if(soWindow.refresh_grid) soWindow.refresh_grid();"
      ."soPopUpManager.closePopUp('popup_category_edit');";
}

class ConfirmEditEvent extends FWDRunnable {
  public function run(){
    $miCategoryId = FWDWebLib::getObject('category_id')->getValue();
    $moCategory = getCategoryFromFields();
    save($moCategory,true,$miCategoryId);
  }
}

class SaveCategoryEvent extends FWDRunnable {
  public function run() {
    $miCategoryId = FWDWebLib::getObject('category_id')->getValue();
    $moCategory = getCategoryFromFields();
    $moCategory->setHash(FWDWebLib::getObject('hash')->getValue());
    $mbHasSensitiveChanges = $moCategory->hasSensitiveChanges();
    $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    if($miCategoryId && $mbHasSensitiveChanges && $miUserId != $moCategory->getApprover()){
      $msTitle = FWDLanguage::getPHPStringValue('tt_reapproval','Reaprova��o');
      $msMessage = FWDLanguage::getPHPStringValue('st_sensitive_data_reapproval','Voc� est� alterando dados sens�veis, que necessitam passar por uma reaprova��o. Deseja confirmar as altera��es?');
      $msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
                      "soWindow = soPopUp.getOpener();" .
                      "soWindow.trigger_event('confirm_edit',3);";
      ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,75);
    }else{
      save($moCategory,$mbHasSensitiveChanges,$miCategoryId);
    }
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
    $moStartEvent->addAjaxEvent(new SaveCategoryEvent('save_category_event'));
    $moStartEvent->addAjaxEvent(new ConfirmEditEvent('confirm_edit'));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $miCategoryId = FWDWebLib::getObject('category_id')->getValue();
    $moWindowTitle = FWDWebLib::getObject('window_title');

    $moIcon = FWDWebLib::getObject('tooltip_icon');
    if($miCategoryId){
      //teste para verificar se o sistema n�o est� sendo hakeado
      $moCtxUserTest = new RMCategory();
      $moCtxUserTest->testPermissionToEdit($miCategoryId);

      $moCategory = new RMCategory();
      $moCategory->fetchById($miCategoryId);
      FWDWebLib::getObject('category_name')->setValue($moCategory->getFieldValue('category_name'));
      FWDWebLib::getObject('category_description')->setValue($moCategory->getFieldValue('category_description'));
      $moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_category_editing', 'Edi��o de Categoria'));

      $moToolTip = ISMSLib::getInstance()->getToolTipCreateModifyById($miCategoryId);
      $moToolTip->setAttrWidth(350);
      $moIcon->addObjFWDEvent($moToolTip);
      FWDWebLib::getObject('hash')->setValue($moCategory->getHash());
    }else{
      //teste para verificar se o sistema n�o est� sendo hakeado
      $moCtxUserTest = new RMCategory();
      $moCtxUserTest->testPermissionToInsert();

      $moIcon->setAttrDisplay('false');
      $moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_category_adding', 'Adi��o de Categoria'));
    }

    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        gebi('category_name').focus();
      </script>
    <?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_category_edit.xml');

?>