<?php
include_once "include.php";
include_once $handlers_ref . "grid/QueryGridStandards.php";
include_once $handlers_ref . "QueryTotalStandards.php";

class GridStandards extends FWDDrawGrid {
  public function drawItem(){
    switch($this->ciColumnIndex){
      case $this->getIndexByAlias('standard_id'):
        $this->coCellBox->setIconSrc("icon-standard.gif");
        $this->coCellBox->setObjFWDToolTip(ISMSLib::getInstance()->getToolTipCreateModify($this->getFieldValue('user_create_name'),$this->getFieldValue('date_create'),$this->getFieldValue('user_edit_name'),$this->getFieldValue('date_edit')));
        return parent::drawItem();
        break;
      case $this->getIndexByAlias('standard_name'):
        $this->coCellBox->setAttrStringNoEscape("true");
        if(ISMSLib::userHasACL('M.L.3.1')){
          $this->coCellBox->setValue("<a href='javascript:open_edit(".$this->getFieldValue('standard_id').",".CONTEXT_STANDARD.",\"".uniqid()."\")'>".$this->getFieldValue('standard_name')."</a>");
        }else{
          $this->coCellBox->setValue("<a href='javascript:open_visualize(".$this->getFieldValue('standard_id').",".CONTEXT_STANDARD.",\"".uniqid()."\")'>".$this->getFieldValue('standard_name')."</a>");
        }
        return $this->coCellBox->draw();
        break;
      case $this->getIndexByAlias('date_create'):
       	$this->coCellBox->setValue(ISMSLib::getISMSShortDate($this->getFieldValue('date_create')));
       	return $this->coCellBox->draw();
       break;
      default:
        return parent::drawItem();
        break;
    }
  }
}

class StandardConfirmRemove extends FWDRunnable {
  public function run(){
  	$miStandardId = FWDWebLib::getObject('selected_standard_id')->getValue();  	
  	
  	$moStandard = new RMStandard();
  	$moStandard->fetchById($miStandardId);
  	$msStandardName = ISMSLib::truncateString($moStandard->getFieldValue('standard_name'), 70);
  	
    $msTitle = FWDLanguage::getPHPStringValue('tt_remove_standard','Remover Norma');
    $msMessage = FWDLanguage::getPHPStringValue('st_remove_standard_confirm',"Voc� tem certeza que deseja remover a norma <b>%standard_name%</b>?");
    $msMessage = str_replace('%standard_name%', $msStandardName, $msMessage);
           
    $msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
    				"soWindow = soPopUp.getOpener();" .
    				"soWindow.remove_standard();";
			  		
    ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,50);
  }
}

class RemoveStandardEvent extends FWDRunnable {
  public function run(){  	
  	$miStandardId = FWDWebLib::getObject('selected_standard_id')->getValue();  	
  	$moStandard = new RMStandard();
  	$moStandard->delete($miStandardId);
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));  	
    $moStartEvent->addAjaxEvent(new StandardConfirmRemove('standard_confirm_remove'));
    $moStartEvent->addAjaxEvent(new RemoveStandardEvent('remove_standard_event'));
    //instala a seguran�a de ACL na p�gina
		FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
		FWDMenuACLSecurity::getInstance()->installMenuACL(FWDWebLib::getObject('menu'),FWDACLSecurity::getInstance());
    
    $moGrid = FWDWebLib::getObject("grid_standards");
    $moHandler = new QueryGridStandards(FWDWebLib::getConnection());        	
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new GridStandards());   
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));    
	?>
		<script language="javascript">
		function refresh_grid() {
			js_refresh_grid('grid_standards');		
		}
		
		function remove_standard() {
			trigger_event('remove_standard_event', 3);
			refresh_grid();				
		}
    
    function open_edit(piId){
      isms_open_popup('popup_standard_edit','packages/libraries/popup_standard_edit.php?standard='+piId,'','true');
    }
    
		</script>
	<?           
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_standards.xml");
?>