<?php
set_time_limit(3600); //1 hora de execu��o 
include_once "include.php";

include_once $handlers_ref . "QueryTreeCategory.php";
include_once $handlers_ref . "QueryTreeSectionBestPractice.php";
include_once $handlers_ref . "QueryTreeStandard.php";
include_once $handlers_ref . "QueryTreeTemplate.php";

define("LIBRARY_TYPE_AOL", 0);
define("LIBRARY_TYPE_XML", 1);

/*!!!!!!!!!!!!!!!!!!IMPORTANTE!!!!!!!!!!!!!!!!!!!
PARA PODER IMPORTAR PARA O SISTEMA UM ARQUIVO DE BIBLIOTECA O USU�RIO 
LOGADO DEVE TER AS SEGUINTES PERMIS�ES:
M.L.4.2 -> importar elementos da biblioteca
M.L.1.7 -> inserir categoria
M.L.1.8 -> inserir evento
M.L.2.7 -> inserir sess�o
M.L.2.8 -> inserir melhor pr�tica
M.L.3.3 -> inserir norma
M.L.5.3 -> inserir modelo de documento
as permiss�es de inserir s�o importantes pois, se o usu�rio n�o tiver essas permiss�es, 
pelos testes antihack do sistema ele n�o pode inserir um desses contextos no sistema
assim a funcionalidade de importar dessa tela, s� aparece se o usu�rio tiver todas essas ACLs
*/

class StaticVariables {
  static $sbShowExportOptions = false; //$sbShowExportOptions: quando true, e n�o compilado, mostra as op��es de exporta��o com nome de cliente ou arquivo de licen�a
  static $csCertificate = "-----BEGIN CERTIFICATE-----
MIIEFzCCA4CgAwIBAgIBATANBgkqhkiG9w0BAQQFADCBujELMAkGA1UEBhMCQlIx
GjAYBgNVBAgTEVJpbyBHcmFuZGUgZG8gU3VsMRUwEwYDVQQHEwxQb3J0byBBbGVn
cmUxIjAgBgNVBAoTGUF4dXIgSW5mb3JtYXRpb24gU2VjdXJpdHkxFjAUBgNVBAsT
DWxpY2Vuc2UgZGVwdC4xHDAaBgNVBAMTE0F4dXIgTGljZW5zZSBTeXN0ZW0xHjAc
BgkqhkiG9w0BCQEWD2Fsc0BheHVyLmNvbS5icjAeFw0wNDA2MDkxNzU0NTBaFw0w
OTA2MTAxNzU0NTBaMIG6MQswCQYDVQQGEwJCUjEaMBgGA1UECBMRUmlvIEdyYW5k
ZSBkbyBTdWwxFTATBgNVBAcTDFBvcnRvIEFsZWdyZTEiMCAGA1UEChMZQXh1ciBJ
bmZvcm1hdGlvbiBTZWN1cml0eTEWMBQGA1UECxMNbGljZW5zZSBkZXB0LjEcMBoG
A1UEAxMTQXh1ciBNb25pdG9yIFN5c3RlbTEeMBwGCSqGSIb3DQEJARYPYW1zQGF4
dXIuY29tLmJyMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCzKZ+FEPy6DbvF
IOQ6hp6pCnth0kr5FFSh39pMsq0ytv4RRAxnWxjPvyfx0w7vpnKbARF8zDaStN5K
JIxocA7SYmYM2ItYZdAu+6JU1m9/EYT8eb7w110cYdEIJJr+CPrx2iivXVYBbl9J
VkKlf/1xFnkwhIlZ6CYAwFfnxdIswwIDAQABo4IBKTCCASUwCQYDVR0TBAIwADAP
BglghkgBhvhCAQ0EAhYAMB0GA1UdDgQWBBTAEH8Y7ZVtOpNxtuO5djrXmDp59zCB
5wYDVR0jBIHfMIHcgBTIsD9pVTsIJx25scfn1t6xQmdPZaGBwKSBvTCBujELMAkG
A1UEBhMCQlIxGjAYBgNVBAgTEVJpbyBHcmFuZGUgZG8gU3VsMRUwEwYDVQQHEwxQ
b3J0byBBbGVncmUxIjAgBgNVBAoTGUF4dXIgSW5mb3JtYXRpb24gU2VjdXJpdHkx
FjAUBgNVBAsTDWxpY2Vuc2UgZGVwdC4xHDAaBgNVBAMTE0F4dXIgTGljZW5zZSBT
eXN0ZW0xHjAcBgkqhkiG9w0BCQEWD2Fsc0BheHVyLmNvbS5icoIBADANBgkqhkiG
9w0BAQQFAAOBgQAabwBzSL+Ee3up3MxfE0gz4UAmjtaJMTlmclz+fGX22CVVwMXX
1WhH4QacRA9UnZp5tYEAwPMXpOGrlgR0tAkI3O5SaLaki6qc07h5mqfczN2L2BMp
LvUwF7gTTmNITm+ooJRP96Q+16G/UhUp4FNQvJyG2OmovKnH/8ZKuGgwlQ==
-----END CERTIFICATE----- ";
}

class LibraryExportAOLBegin extends FWDRunnable {
  public function run() {
    $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    if (!$moSession->attributeExists("libraryExportUploadComplete"))
      $moSession->addAttribute("libraryExportUploadComplete");  
    $moSession->setAttrLibraryExportUploadComplete(false);
    //echo "js_submit('library_export_upload','ajax');setTimeout('trigger_event(\"library_export\",3)',200);";
    echo "js_submit('library_export_upload','ajax');setTimeout('trigger_event(\"library_aol_export\",3)',200);";
  }
}

class LibraryExportXMLBegin extends FWDRunnable {
  public function run() {
    $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    if (!$moSession->attributeExists("libraryExportUploadComplete"))
      $moSession->addAttribute("libraryExportUploadComplete");  
    $moSession->setAttrLibraryExportUploadComplete(false);
    echo "js_submit('library_export_upload','ajax');setTimeout('trigger_event(\"library_xml_export\",3)',200);";
  }
}

class LibraryExportUpload extends FWDRunnable {
  public function run() {
    $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    $moFile = FWDWebLib::getObject('export_options_file');
    $miError = $moFile->getErrorCode();
    if($miError==FWDFile::E_NONE && $moFile->getFileName()){
      $moLicense = new FWDLicense(StaticVariables::$csCertificate);
      $moLicense->loadLicense($moFile->getTempFileName());
      if (!$moSession->attributeExists("libraryExportClientName"))
        $moSession->addAttribute("libraryExportClientName");  
      $moSession->setAttrLibraryExportClientName($moLicense->getAttribute('client'));
    }
    $moSession->setAttrLibraryExportUploadComplete(true);
  }
}

class LibraryAOLExport extends FWDRunnable {
	public function run() {
	  LibraryExport::run(LIBRARY_TYPE_AOL);
	}
}

class LibraryXMLExport extends FWDRunnable {
	public function run() {
	  LibraryExport::run(LIBRARY_TYPE_XML);
	}
}

/**
 * Classe LibraryExport. Evento de exporta��o das bibliotecas.
 * 
 * <p>Classe, disparada por evento, respons�vel por pegar os elementos marcados e export�-los para XML.</p>
 * 
 * @package ISMS
 * @subpackage libraries
 */
class LibraryExport{
    public static function run($type) {
		$moWebLib = FWDWebLib :: getInstance();

        $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
        if($moSession->attributeExists("libraryExportUploadComplete")){
          if($moSession->getAttrLibraryExportUploadComplete()){
            $moSession->deleteAttribute("libraryExportUploadComplete");
          } else {
            
            if($type == LIBRARY_TYPE_AOL){
              echo "setTimeout('trigger_event(\"library_aol_export\",3)',200);";
            } if($type == LIBRARY_TYPE_XML){
              echo "setTimeout('trigger_event(\"library_xml_export\",3)',200);";
            }
            
          }
        }

		/* Inicia a exporta��o das Categorias (Biblioteca de eventos) */
		$moCategoryController = $moWebLib->getObject("controller_export_category");
		$maCategoryAllItemsCheck = $moCategoryController->getAllItemsCheck();
		$maCategoryExportTree = array(); //Array contendo todas as categorias, na ordem correta para exporta��o (pai antes de filho).
		$moHandler = new QueryTreeCategory($moWebLib->getConnection());
		$moHandler->makeQuery();
		$moHandler->executeQuery();
		while ($moHandler->getDataset()->fetch()) {
    		$moHandlerDataset = $moHandler->getDataset();
    		$miCategoryId = $moHandlerDataset->getFieldByAlias("id")->getValue();
    		$miCategoryLevel = $moHandlerDataset->getFieldByAlias("node_level")->getValue();
    		$maCategoryExportTree[$miCategoryId] = $miCategoryLevel;
		}
		$maCategoryCheckValues = array(); //Array contendo o id de cada item marcado, associado ao valor do id de seu pai.
		foreach ($maCategoryAllItemsCheck as $moItem) {
			$moCategory = new RMCategory();
			$miCategoryId = $moItem->getAttrKey();
			if ($moCategory->fetchById($miCategoryId))
				$maCategoryCheckValues[$miCategoryId] = $moCategory->getFieldValue("category_parent_id");
		}

		/* Elimina da �rvore de exporta��o os itens n�o marcados e aqueles que tem pai, mas cujo pai n�o est� marcado */
		foreach ($maCategoryExportTree as $miCategoryId => $miCategoryLevel) {
			if (array_key_exists($miCategoryId, $maCategoryCheckValues)) {
  				$miCategoryParentId = (int)$maCategoryCheckValues[$miCategoryId];
					if (($miCategoryParentId)&&($miCategoryParentId!=0)&&(($miCategoryParentId!=NULL))) {
							if (array_key_exists($miCategoryParentId, $maCategoryExportTree))
								continue;
							else
								unset ($maCategoryExportTree[$miCategoryId]);
						}
				} 
				else
					unset ($maCategoryExportTree[$miCategoryId]);
		}
		
		/* Inicia a exporta��o das Melhores Pr�ticas */
		$moSectionBestPracticeController = $moWebLib->getObject("controller_export_sectionbestpractice");
		$maSectionBestPracticeAllItemsCheck = $moSectionBestPracticeController->getAllItemsCheck();
		$maSectionBestPracticeExportTree = array(); //Array contendo todas as melhroes pr�ticas, na ordem correta para exporta��o (pai antes de filho).
		$moHandler = new QueryTreeSectionBestPractice($moWebLib->getConnection());
		$moHandler->makeQuery();
		$moHandler->executeQuery();
		while ($moHandler->getDataset()->fetch()) {
			$moHandlerDataset = $moHandler->getDataset();
			$miSectionBestPracticeId = $moHandlerDataset->getFieldByAlias("id")->getValue();
			$miSectionBestPracticeLevel = $moHandlerDataset->getFieldByAlias("node_level")->getValue();
			$maSectionBestPracticeExportTree[$miSectionBestPracticeId] = $miSectionBestPracticeLevel;
		}
		$maSectionBestPracticeCheckValues = array(); //Array contendo o id de cada item marcado, associado ao valor do id de seu pai.
		
		foreach ($maSectionBestPracticeAllItemsCheck as $moItem) {
			$moSectionBestPractice = new RMSectionBestPractice();
			$miSectionBestPracticeId = $moItem->getAttrKey();
			if ($moSectionBestPractice->fetchById($miSectionBestPracticeId)) {
				$maSectionBestPracticeCheckValues[$miSectionBestPracticeId] = $moSectionBestPractice->getFieldValue("section_parent_id");
			}
		}
		
		/* Elimina da �rvore de exporta��o os itens n�o marcados e aqueles que tem pai, mas cujo pai n�o est� marcado */
		foreach ($maSectionBestPracticeExportTree as $miSectionBestPracticeId => $miSectionBestPracticeLevel) {
			if (array_key_exists($miSectionBestPracticeId, $maSectionBestPracticeCheckValues)) {
				$miSectionBestPracticeParentId = (int)$maSectionBestPracticeCheckValues[$miSectionBestPracticeId];
				if (($miSectionBestPracticeParentId)&&($miSectionBestPracticeParentId!=0)&&(($miSectionBestPracticeParentId!=NULL))) {
					if (array_key_exists($miSectionBestPracticeParentId, $maSectionBestPracticeExportTree))
						continue;
					else
						unset ($maSectionBestPracticeExportTree[$miSectionBestPracticeId]);
				}
			} else
				unset ($maSectionBestPracticeExportTree[$miSectionBestPracticeId]);
		}

		/* Inicia a exporta��o das Normas */
		$moStandardController = $moWebLib->getObject("controller_export_standard");
		$maStandardAllItemsCheck = $moStandardController->getAllItemsCheck();
		$maStandardExportTree = array(); //Array que ir� conter as normas a serem exportadas
		/* Inclui na �rvore de exporta��o os itens marcados */
		foreach ($maStandardAllItemsCheck as $moItem) {
			$moStandard = new RMStandard();
			$miStandardId = $moItem->getAttrKey();
			if ($moStandard->fetchById($miStandardId)) {
        if ($moStandard->getContextState($miStandardId) != CONTEXT_STATE_DELETED) {
          $maStandardExportTree[$miStandardId] = $miStandardId;
        }
      }
		}
    
    /* Inicia a exporta��o dos Templates */
    $moTemplateController = $moWebLib->getObject("controller_export_template");
    $maTemplateAllItemsCheck = $moTemplateController->getAllItemsCheck();
    $maTemplateExportTree = array(); //Array que ir� conter os modelos de documentos a serem exportados
    /* Inclui na �rvore de exporta��o os itens marcados */
    foreach ($maTemplateAllItemsCheck as $moItem) {
      $moTemplate = new PMTemplate();
      $miTemplateId = $moItem->getAttrKey();
      if ($moTemplate->fetchById($miTemplateId))
        $maTemplateExportTree[$miTemplateId] = $miTemplateId;
}
	    if($moSession->attributeExists("libraryExportClientName")){
      $msClientName = $moSession->getAttrLibraryExportClientName();
      $moSession->deleteAttribute("libraryExportClientName");
    } elseif($GLOBALS['load_method']!=XML_STATIC && StaticVariables::$sbShowExportOptions) {
      $msClientName = $moWebLib->getObject('client_name')->getValue();
    } else {
      $moLicense = new ISMSLicense();
      $msClientName = $moLicense->getClientName();
    }
    
    /* Cria um array contendo os quatro arrays de exporta��o, serializa, e coloca na se��o */
    $maLibraryExport = array("category"=>$maCategoryExportTree, "sectionbestpractice"=>$maSectionBestPracticeExportTree, "standard"=>$maStandardExportTree, "template"=>$maTemplateExportTree);
    
    if ($msClientName)
      $maLibraryExport["client_name"]=$msClientName;
	  $msLibraryExport = serialize($maLibraryExport);
	  $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
	  
	  if (!$moSession->attributeExists("libraryexport"))
        $moSession->addAttribute("libraryexport");  

      $moSession->setAttrLibraryExport($msLibraryExport);
        
      if (!$moSession->attributeExists("libraryexportdone"))
        $moSession->addAttribute("libraryexportdone");

      if (!$moSession->attributeExists("libraryexporttype")){
        $moSession->addAttribute("libraryexporttype");
        $moSession->setAttrLibraryExportType($type);
      }
        
      $moSession->setAttrLibraryExportDone(false);
      echo "gebi('force_report_download_iframe').src = 'export.php';setTimeout('trigger_event(\"library_export_done\",3)',200);";
	}
}

/**
 * Classe LibraryExportDone. Classe para remover o vidro quando terminar a exporta��o dos dados.
 * 
 * <p>Classe usada para remover o vidro, quando terminar a exporta��o dos dados.</p>
 * 
 * @package ISMS
 * @subpackage libraries
 */
class LibraryExportDone extends FWDRunnable {
  public function run() {
    $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    if ($moSession->attributeExists("libraryexportdone")) {
      if ($moSession->getAttrLibraryExportDone("LibraryExportDone")) {
        $moSession->deleteAttribute("LibraryExportDone");
        $moSession->deleteAttribute("libraryexporttype");
        
        echo "soTabManager.unlockTabs();";
      } else {
        echo "setTimeout('trigger_event(\"library_export_done\",3)',200);";
      }
    }
  }
}

/**
 * Classe LibraryImport. Classe para importar as bibliotecas marcadas.
 * 
 * <p>Classe, disparada por evento, respons�vel por incluir no BD as bibliotecas selecionadas.</p>
 * 
 * @package ISMS
 * @subpackage libraries
 */
class LibraryImport extends FWDRunnable {
	public function run() {
		$moWebLib = FWDWebLib :: getInstance();
		
		//Pega a lista de classes da vari�vel
		$msLibraryImport = $moWebLib->getObject('library_import')->getValue();
		$maLibraryImport = FWDWebLib::unserializeString($msLibraryImport);
    
		if($maLibraryImport) {
			/* Category */
			if (isset($maLibraryImport["category"])) {
				$maCategory = &$maLibraryImport["category"];
				$moCategoryController = $moWebLib->getObject("controller_import_category");
				$maCategoryAllItemsCheck = $moCategoryController->getAllItemsCheck();
				$maCategoryImportTree = array(); //array dos itens marcados
				foreach($maCategoryAllItemsCheck as $moItem) {
					$miCategoryId = $moItem->getAttrKey();
					$maCategoryImportTree[$miCategoryId]=$miCategoryId;
				}
	
				foreach($maCategory as $miCategoryId => $maCategoryAttr) {
					if (isset($maCategoryImportTree[$miCategoryId])) {
						if (isset($maCategoryAttr["category_parent_id"])) {
							$miCategoryParentId = $maCategoryAttr["category_parent_id"];
							if (!isset($maCategory[$miCategoryParentId])) {
								unset($maCategory[$miCategoryId]);
							}
						}
					} else {
						unset($maCategory[$miCategoryId]);
					}	
				}
				
				//Insere as categorias no banco, guardando a referencia da antiga id para a nova.
				$maCategoryNewIds=array();
				foreach($maCategory as $miCategoryId=>$maCategoryAttr) {
					$moCategory = new RMCategory();
					foreach($maCategoryAttr as $msField=>$msValue) {
						if ($msField != "classId" && $msField != "classValue" && $msField != "className") {
							if ($msField == "category_parent_id" && isset($maCategoryNewIds[$msValue])) {
								$moCategory->setFieldValue($msField,$maCategoryNewIds[$msValue]);
							}	else {						
								$moCategory->setFieldValue($msField,/*utf8_encode*/($msValue));
							}
						}
					}
					$miCategoryInsert = $moCategory->insert();
					$maCategoryNewIds[$miCategoryId] = $miCategoryInsert;
					
					//Com a nova id, inserimos os eventos.
					$maEventNewIds=array();
					if (isset($maLibraryImport["event"]) && isset($maLibraryImport["event"][$miCategoryId])) {
						$maEvent = $maLibraryImport["event"][$miCategoryId];
						foreach($maEvent as $miEventId=>$maEventAttr) {
							$moEvent = new RMEvent();
							foreach($maEventAttr as $msField=>$msValue) {
								if ($msField != "classId" && $msField != "className") {
									if ($msField == "event_category_id" && isset($maCategoryNewIds[$msValue])) {
										$moEvent->setFieldValue($msField,$maCategoryNewIds[$msValue]);
									} else {						
										$moEvent->setFieldValue($msField,/*utf8_encode*/($msValue));
									}
								}	
							}
							$miEventInsert = $moEvent->insert();
							$maEventNewIds[$miEventId] = $miEventInsert;
						}
					}
				}
			}
			/* Fim Category */
	
			/* Best Practice */
			if (isset($maLibraryImport["sectionbestpractice"])) {
				$maSection = &$maLibraryImport["sectionbestpractice"];
				$moSectionController = $moWebLib->getObject("controller_import_sectionbestpractice");
				$maSectionAllItemsCheck = $moSectionController->getAllItemsCheck();
				
				$maSectionImportTree = array(); //array dos itens marcados
				foreach($maSectionAllItemsCheck as $moItem) {
					$miSectionId = $moItem->getAttrKey();
					$maSectionImportTree[$miSectionId]=$miSectionId;
				}
	
				foreach($maSection as $miSectionId => $maSectionAttr) {
					if (isset($maSectionImportTree[$miSectionId])) {
						if (isset($maSectionAttr["section_parent_id"])) {
							$miSectionParentId = $maSectionAttr["section_parent_id"];
							if (!isset($maSection[$miSectionParentId])) {
								unset($maSection[$miSectionId]);
							}
						}
					} else {
						unset($maSection[$miSectionId]);
					}	
				}
				
				//Insere as secoes no banco, guardando a referencia da antiga id para a nova.
				$maSectionNewIds=array();
				foreach($maSection as $miSectionId=>$maSectionAttr) {
					$moSection = new RMSectionBestPractice();
					foreach($maSectionAttr as $msField=>$msValue) {
						if ($msField != "classId" && $msField != "classValue" && $msField != "className") {
							if ($msField == "section_parent_id" && isset($maSectionNewIds[$msValue])) {
								$moSection->setFieldValue($msField,$maSectionNewIds[$msValue]);
							}	else {						
								$moSection->setFieldValue($msField,/*utf8_encode*/($msValue));
							}
						}
					}
					$miSectionInsert = $moSection->insert();
					$maSectionNewIds[$miSectionId] = $miSectionInsert;
					
					//Com a nova id, inserimos as melhores praticas.
					if (isset($maLibraryImport["bestpractice"]) && isset($maLibraryImport["bestpractice"][$miSectionId])) {
						$maBestPractice = $maLibraryImport["bestpractice"][$miSectionId];
						foreach($maBestPractice as $miBestPracticeId => $maBestPracticeAttr) {
							$moBestPractice = new RMBestPractice();
							foreach($maBestPracticeAttr as $msField=>$msValue) {
								if ($msField != "classId" && $msField != "className") {
									if ($msField == "section_best_practice_id" && isset($maSectionNewIds[$msValue])) {
										$moBestPractice->setFieldValue($msField,$maSectionNewIds[$msValue]);
									} else {						
										$moBestPractice->setFieldValue($msField,/*utf8_encode*/($msValue));
									}
								}	
							}
							$miBestPracticeInsert = $moBestPractice->insert();
							$maBestPracticeNewIds[$miBestPracticeId] = $miBestPracticeInsert;
						}
					}
				}
			}
			/* Fim Best Practice */
			
      /* Standard */
			if (isset($maLibraryImport["standard"])) {
				$maStandard = &$maLibraryImport["standard"];
				$moStandardController = $moWebLib->getObject("controller_import_standard");
				$maStandardAllItemsCheck = $moStandardController->getAllItemsCheck();
				$maStandardImportTree = array(); //array dos itens marcados
				foreach($maStandardAllItemsCheck as $moItem) {
					$miStandardId = $moItem->getAttrKey();
					$maStandardImportTree[$miStandardId]=$miStandardId;
				}
				foreach($maStandard as $miStandardId => $maStandardAttr) {
					if (isset($maStandardImportTree[$miStandardId])) {
						if (isset($maStandardAttr["standard_parent_id"])) {
							$miStandardParentId = $maStandardAttr["standard_parent_id"];
							if (!isset($maStandard[$miStandardParentId])) {
								unset($maStandard[$miStandardId]);
							}
						}
					} else {
						unset($maStandard[$miStandardId]);
					}	
				}
				
				//Insere as normas no banco
				$maStandardNewIds=array();
				foreach($maStandard as $miStandardId=>$maStandardAttr) {
					$moStandard = new RMStandard();
					foreach($maStandardAttr as $msField=>$msValue) {
						if ($msField != "classId" && $msField != "classValue" && $msField != "className") {
							$moStandard->setFieldValue($msField,/*utf8_encode*/($msValue));
						}
					}
					$miStandardInsert = $moStandard->insert();
					$maStandardNewIds[$miStandardId] = $miStandardInsert;
				}
			}
			/* Fim Standard */
			
			/* BestPracticeEvent */
			if (isset($maLibraryImport["bestpracticeevent"])) {
				$maBestPracticeEvent = &$maLibraryImport["bestpracticeevent"];
				foreach($maBestPracticeEvent as $miBestPracticeEventId=>$maBestPracticeEventAttr)
				{
					if (isset($maEventNewIds[$maBestPracticeEventAttr["event_id"]]) && isset($maBestPracticeNewIds[$maBestPracticeEventAttr["best_practice_id"]])) {
						$moBestPracticeEvent = new RMBestPracticeEvent();
						$moBestPracticeEvent->setFieldValue("event_id",$maEventNewIds[$maBestPracticeEventAttr["event_id"]]);
						$moBestPracticeEvent->setFieldValue("best_practice_id",$maBestPracticeNewIds[$maBestPracticeEventAttr["best_practice_id"]]);
            $moBestPracticeEvent->insert();
					}
				}
			}
			/* Fim BestPracticeEvent */
			
			/* BestPracticeStandard */
			if (isset($maLibraryImport["bestpracticestandard"])) {
				$maBestPracticeStandard = &$maLibraryImport["bestpracticestandard"];
				foreach($maBestPracticeStandard as $miBestPracticeStandardId=>$maBestPracticeStandardAttr)
				{
					if (isset($maStandardNewIds[$maBestPracticeStandardAttr["standard_id"]]) && isset($maBestPracticeNewIds[$maBestPracticeStandardAttr["best_practice_id"]])) {
						$moBestPracticeStandard = new RMBestPracticeStandard();
						$moBestPracticeStandard->setFieldValue("standard_id",$maStandardNewIds[$maBestPracticeStandardAttr["standard_id"]]);
						$moBestPracticeStandard->setFieldValue("best_practice_id",$maBestPracticeNewIds[$maBestPracticeStandardAttr["best_practice_id"]]);
					  $moBestPracticeStandard->insert();
          }
				}
			}
			/* Fim BestPracticeStandard */
      
      
      
      /* Template */
      if (isset($maLibraryImport["template"])) {
        $maTemplate = &$maLibraryImport["template"];
        $moTemplateController = $moWebLib->getObject("controller_import_template");
        $maTemplateAllItemsCheck = $moTemplateController->getAllItemsCheck();
        $maTemplateImportTree = array(); //array dos itens marcados
        foreach($maTemplateAllItemsCheck as $moItem) {
          $miTemplateId = $moItem->getAttrKey();
          $maTemplateImportTree[$miTemplateId]=$miTemplateId;
        }
        
        //Insere as normas no banco
        $maTemplateNewIds=array();
        foreach($maTemplate as $miTemplateId=>$maTemplateAttr) {
          $moTemplate = new PMTemplate();
          foreach($maTemplateAttr as $msField=>$msValue) {
            if ($msField != "classId" && $msField != "classValue" && $msField != "className") {
              $moTemplate->setFieldValue($msField,/*utf8_encode*/($msValue));
            }
          }
          $miTemplateInsert = $moTemplate->insert();
          $maTemplateNewIds[$miTemplateId] = $miTemplateInsert;
        }
      }
      /* Fim Template */
      
      /* TemplateBestPractice */
      if (isset($maLibraryImport["templatebestpractice"])) {
        $maTemplateBestPractice = &$maLibraryImport["templatebestpractice"];
        foreach($maTemplateBestPractice as $maTemplateBestPracticeAttr)
        {
          if (isset($maTemplateNewIds[$maTemplateBestPracticeAttr["template_id"]]) && isset($maBestPracticeNewIds[$maTemplateBestPracticeAttr["best_practice_id"]])) {
            $moTemplateBestPractice = new PMTemplateBestPractice();
            $moTemplateBestPractice->setFieldValue("template_id",$maTemplateNewIds[$maTemplateBestPracticeAttr["template_id"]]);
            $moTemplateBestPractice->setFieldValue("best_practice_id",$maBestPracticeNewIds[$maTemplateBestPracticeAttr["best_practice_id"]]);
            $moTemplateBestPractice->insert();
          }
        }
      }
      /* Fim TemplateBestPractice */
      
      /* Template File */
      if (isset($maLibraryImport["templatefile"])) {
        $maTemplateFile = &$maLibraryImport["templatefile"];
        foreach($maTemplateFile as $maTemplateFileAttr)
        {
          if (isset($maTemplateNewIds[$maTemplateFileAttr["template_id"]])) {
            $miTemplateId = $maTemplateNewIds[$maTemplateFileAttr["template_id"]];
            $msFileContent = base64_decode($maTemplateFileAttr["file_content"]);
            $moCrypt = new FWDCrypt();
            if (is_writable("../policy/files/")) {
              $msHashFileName = md5(uniqid());
              $msPath = "../policy/files/{$msHashFileName}.aef";
              file_put_contents($msPath,$msFileContent);
              $miFileSize = filesize($msPath);
            }
            $moTemplate = new PMTemplate();
            $moTemplate->setFieldValue('template_path',$msPath);
            $moTemplate->setFieldValue('template_file_name',$maTemplateFileAttr["template_file_name"]);
            $moTemplate->setFieldValue('template_file_size',$miFileSize);
            $moTemplate->update($miTemplateId);
            
            $moTemplateContent = new PMTemplateContent();
            $moTemplateContent->setFieldValue('template_content',base64_decode($maTemplateFileAttr["template_content"]));
            $moTemplateContent->setFieldValue('template_content_id',$miTemplateId);
            $moTemplateContent->insert();
          }
        }
      }
      /* Fim Template File */
		}
		echo "js_submit(this);";
	}
}

// extende a classe FWDNodeBuilder para incluir CheckBoxes nos nodos de Categoria
class NodeBuilderCategory extends FWDNodeBuilder {
	public function buildNode($psId, $psValue) {
		$moNode = new FWDTree();
		$moNode->setAttrName($psId);
		$moCheck = new FWDCheckBox();
		$moCheck->setAttrName('checkbox_export_category' . $psId);
		$moCheck->setAttrKey($psId);
		$moCheck->setAttrCheck(true);
		$moCheck->setAttrController('controller_export_category');
		$moCheck->execute();
		$moNode->addObjFWDView($moCheck);
		$moStatic = new FWDStatic();
		$moStatic->setValue($psValue);
		$moNode->addObjFWDView($moStatic);
		return $moNode;
	}
}

// extende a classe FWDNodeBuilder para incluir CheckBoxes nos nodos de Melhores Pr�ticas
class NodeBuilderSectionBestPractice extends FWDNodeBuilder {
	public function buildNode($psId, $psValue) {
		$moNode = new FWDTree();
		$moNode->setAttrName($psId);
		$moCheck = new FWDCheckBox();
		$moCheck->setAttrName('checkbox_export_sectionbestpractice' . $psId);
		$moCheck->setAttrKey($psId);
		$moCheck->setAttrCheck(true);
		$moCheck->setAttrController('controller_export_sectionbestpractice');
		$moCheck->execute();
		$moNode->addObjFWDView($moCheck);
		$moStatic = new FWDStatic();
		$moStatic->setValue($psValue);
		$moNode->addObjFWDView($moStatic);
		return $moNode;
	}
}

// extende a classe FWDNodeBuilder para incluir CheckBoxes nos nodos de Normas
class NodeBuilderStandard extends FWDNodeBuilder {
	public function buildNode($psId, $psValue) {
		$moNode = new FWDTree();
		$moNode->setAttrName($psId);
		$moStatic = new FWDStatic();
		$moStatic->setValue($psValue);
		$moCheck = new FWDCheckBox();
		$moCheck->setAttrName('checkbox_export_standard' . $psId);
		$moCheck->setAttrKey($psId);
		$moCheck->setAttrCheck(true);
		$moCheck->setAttrController('controller_export_standard');
		$moCheck->execute();
		$moNode->addObjFWDView($moCheck);
		$moNode->addObjFWDView($moStatic);
		return $moNode;
	}
}

// extende a classe FWDNodeBuilder para incluir CheckBoxes nos nodos de Modelos de Documentos
class NodeBuilderTemplate extends FWDNodeBuilder {
  public function buildNode($psId, $psValue) {
    $moNode = new FWDTree();
    $moNode->setAttrName($psId);
    $moStatic = new FWDStatic();
    $moStatic->setValue($psValue);
    $moCheck = new FWDCheckBox();
    $moCheck->setAttrName('checkbox_export_template' . $psId);
    $moCheck->setAttrKey($psId);
    $moCheck->setAttrCheck(true);
    $moCheck->setAttrController('controller_export_template');
    $moCheck->execute();
    $moNode->addObjFWDView($moCheck);
    $moNode->addObjFWDView($moStatic);
    return $moNode;
  }
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run() {
      $moWebLib = FWDWebLib :: getInstance();
      $moStartEvent = FWDStartEvent :: getInstance();
      $moStartEvent->setScreenEvent(new ScreenEvent(""));
      
      $moStartEvent->addAjaxEvent(new LibraryAOLExport('library_aol_export'));
      $moStartEvent->addAjaxEvent(new LibraryXMLExport('library_xml_export'));		
      	
      $moStartEvent->addAjaxEvent(new LibraryExportDone('library_export_done'));
      $moStartEvent->addAjaxEvent(new LibraryImport('library_import'));
        
      $moStartEvent->addAjaxEvent(new LibraryExportAOLBegin('library_export_aol_begin'));
      $moStartEvent->addAjaxEvent(new LibraryExportXMLBegin('library_export_xml_begin'));
      
      $moStartEvent->addSubmitEvent(new LibraryExportUpload('library_export_upload'));
      
      if(in_array(ISMSSaaS::getConfig(ISMSSaaS::LICENSE_TYPE), ISMSLicense::getTrialLicenseTypes())){
        exit();
      }
	}
}

class ScreenEvent extends FWDRunnable {
	public function run() {
		$moWebLib = FWDWebLib::getInstance();
		$moFile = $moWebLib->getObject('file_aol');
	  $msImportButtons = "gobi('pack_import_buttons').hide();
                        gobi('pack_import_file').show();";
    if($moFile->isUploaded() && $moFile->getTempFileName()){
        $fileExtension = pathinfo($moFile->getFileName(), PATHINFO_EXTENSION);
	    $miFileError = $moFile->getErrorCode();
		  if($miFileError==FWDFile::E_NONE){
		  	$moLibraryImport = new ISMSLibraryImport($moFile);

		  	if(strtolower($fileExtension) == "aol"){
		  	  $moLibraryImport->parseFileAOL();
		  	} else if(strtolower($fileExtension) == "xml") {
		  	  $moLibraryImport->parseFileXML();
		  	}

		  	if ($moLibraryImport->getErrorMsg()) {
		  		$msMsg = FWDLanguage::getPHPStringValue('wn_error_invalid_file','Erro: arquivo inv�lido.');
          FWDWebLib::getObject('file_aol_error')->setValue($msMsg);
          FWDWebLib::getObject('file_aol_error')->setAttrDisplay('true');
		  	} else {
		  		$moLibraryShowImport = new ISMSLibraryShowImport($moLibraryImport);
		  		$moLibraryShowImport->showImportElements();
          $msImportButtons = "gobi('pack_import_buttons').show();
                              gobi('pack_import_file').hide();
                              gebi('import_buttons').className='FWDPanel_lightgreen';
                              gebi('export_buttons').className='FWDPanel_lightblue';";
          FWDWebLib::getObject('export_options')->setAttrDisplay('false');
          FWDWebLib::getObject('file_aol_error')->setAttrDisplay('false');
		  	}
			}else{
			 	$msMsg = FWDLanguage::getPHPStringValue('wn_error_invalid_file','Erro: arquivo inv�lido.');
				if($miFileError & FWDFile::E_MAX_SIZE) $msMsg.= '<br>'.FWDLanguage::getPHPStringValue('wn_maximum_size_exceeded','Tamanho m�ximo excedido.');
				if($miFileError & FWDFile::E_EXTENSION) $msMsg.= '<br>'.FWDLanguage::getPHPStringValue('wn_extension_not_allowed','Extens�o n�o permitida.');
        FWDWebLib::getObject('file_aol_error')->setValue($msMsg);
        FWDWebLib::getObject('file_aol_error')->setAttrDisplay('true');
			}
  	}
    
    $msTooltipText = FWDLanguage::getPHPStringValue('to_max_file_size',"Tamanho m�ximo de arquivo: %max_file_size%");
    $msMaxFileSize = ISMSLib::formatFileSize(ISMSSaaS::getConfig(ISMSSaaS::MAX_UPLOAD_FILE_SIZE));
    $msTooltipText = str_replace('%max_file_size%',$msMaxFileSize,$msTooltipText);
    FWDWebLib::getObject('tooltip_max_file_size')->setValue($msTooltipText);
    
  	$moHandler = new QueryTreeCategory($moWebLib->getConnection());
		$moHandler->makeQuery();
		$moHandler->executeQuery();
		$moDbTreeBase = $moWebLib->getObject('dbtree_export_category');
		$moDbTreeBase->setDataSet($moHandler->getDataSet());
		$moDbTreeBase->setNodeBuilder(new NodeBuilderCategory());
		$moDbTreeBase->buildTree();
		
		$moHandler = new QueryTreeSectionBestPractice($moWebLib->getConnection());
		$moHandler->makeQuery();
		$moHandler->executeQuery();
		$moDbTreeBase = $moWebLib->getObject('dbtree_export_sectionbestpractice');
		$moDbTreeBase->setDataSet($moHandler->getDataSet());
		$moDbTreeBase->setNodeBuilder(new NodeBuilderSectionBestPractice());
		$moDbTreeBase->buildTree();
		
		$moHandler = new QueryTreeStandard($moWebLib->getConnection());
		$moHandler->makeQuery();
		$moHandler->executeQuery();
		$moDbTreeBase = $moWebLib->getObject('dbtree_export_standard');
		$moDbTreeBase->setDataSet($moHandler->getDataSet());
		$moDbTreeBase->setNodeBuilder(new NodeBuilderStandard());
		$moDbTreeBase->buildTree();
    
    $moHandler = new QueryTreeTemplate($moWebLib->getConnection());
    $moHandler->makeQuery();
    $moHandler->executeQuery();
    $moDbTreeBase = $moWebLib->getObject('dbtree_export_template');
    $moDbTreeBase->setDataSet($moHandler->getDataSet());
    $moDbTreeBase->setNodeBuilder(new NodeBuilderTemplate());
    $moDbTreeBase->buildTree();
    
    if ($GLOBALS['load_method']==XML_STATIC || !StaticVariables::$sbShowExportOptions) {
      FWDWebLib::getObject('export_options')->setShouldDraw(false);
    }
    
        
    //instala a seguran�a de ACL na p�gina
		FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
		$moWebLib->dump_html(FWDWebLib :: getObject('dialog'));
		?>
		<script language="javascript">
    <?=$msImportButtons?>
    
    soTabManager.unlockTabs();
    if(gebi('export_select'))
      gebi('export_select').value = 1;
    if(gebi('import_select'))
      gebi('import_select').value = 1;
		function change_panel_export()
		{
      var msSelected = 0; 
      if(gebi('export_select'))
        msSelected = gebi('export_select').value;
			if(gebi('panel_export_category'))
				js_hide('panel_export_category');
			if(gebi('panel_export_sectionbestpractice'))
				js_hide('panel_export_sectionbestpractice');
			if(gebi('panel_export_standard'))
				js_hide('panel_export_standard');
      if(gebi('panel_export_template'))
        js_hide('panel_export_template');
			switch(msSelected)
			{
				case '1':if(gebi('panel_export_category')) js_show('panel_export_category'); break;
				case '2':if(gebi('panel_export_sectionbestpractice')) js_show('panel_export_sectionbestpractice'); break;
				case '3':if(gebi('panel_export_standard')) js_show('panel_export_standard'); break;
        case '4':if(gebi('panel_export_template')) js_show('panel_export_template'); break;
				default: break;
			}
		}
		
		function change_panel_import()
		{
      var msSelected = '0';
      if(gebi('import_select'))
        msSelected = gebi('import_select').value;
			if(gebi('panel_import_category'))
				js_hide('panel_import_category');
			if(gebi('panel_import_sectionbestpractice'))
				js_hide('panel_import_sectionbestpractice');
			if(gebi('panel_import_standard'))
				js_hide('panel_import_standard');
      if(gebi('panel_import_template'))
        js_hide('panel_import_template');
			switch(msSelected)
			{
				case '1':if(gebi('panel_import_category')) js_show('panel_import_category'); break;
				case '2':if(gebi('panel_import_sectionbestpractice')) js_show('panel_import_sectionbestpractice'); break;
				case '3':if(gebi('panel_import_standard')) js_show('panel_import_standard'); break;
        case '4':if(gebi('panel_import_template')) js_show('panel_import_template'); break;
				default: break;
			}
		}
    change_panel_export;
    change_panel_import;
		</script>
		<?
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_import_export.xml");
?>