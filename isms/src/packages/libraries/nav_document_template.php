<?php
include_once "include.php";
include_once $handlers_ref . "grid/policy/QueryGridDocumentTemplate.php";

class DownloadEvent extends FWDRunnable {
  public function run() {
    $moWebLib = FWDWebLib::getInstance();
    $miContextId = $moWebLib->getObject('read_document_id')->getValue();
    $moPMTemplate = new PMTemplate();
    
    $moPMTemplate->testPermissionToEdit($miContextId);
    
    if ($moPMTemplate->fetchById($miContextId)) {
      $msPath = $moPMTemplate->getFieldValue('template_path');
      $msFileName = $moPMTemplate->getFieldValue('template_file_name');
      header('Cache-Control: ');// leave blank to avoid IE errors
      header('Pragma: ');// leave blank to avoid IE errors
      header('Content-type: application/octet-stream');
      header('Content-Disposition: attachment; filename="'.rawurlencode($msFileName).'"');
      $moCrypt = new FWDCrypt();
      $moFP = fopen($msPath,'rb');
      $moCrypt->setIV(fread($moFP,strlen($moCrypt->getIV())));
      while(!feof($moFP)) {
        echo $moCrypt->decryptNoBase64(fread($moFP,16384));
      }
      fclose($moFP);
    }
  }
}

class ContextDrawGrid extends FWDDrawGrid {
  public function drawItem(){
    switch($this->ciColumnIndex){
      case $this->getIndexByAlias('context_id'):{
        $this->coCellBox->setIconSrc(ISMSLib::getIcon($this->getFieldValue('context_file_name')) );
        $this->coCellBox->setObjFWDToolTip(ISMSLib::getInstance()->getToolTipCreateModify($this->getFieldValue('user_create_name'),$this->getFieldValue('date_create'),$this->getFieldValue('user_edit_name'),$this->getFieldValue('date_edit') ));
        return parent::drawItem();
      }case $this->getIndexByAlias('context_name'):{
        $this->coCellBox->setAttrStringNoEscape("true");
        
        if(ISMSLib::userHasACL('M.L.5.1') && $this->getFieldValue('file_path') && is_file($this->getFieldValue('file_path')) ){ 
          $this->coCellBox->setValue("<a href=javascript:read('".$this->getFieldValue('context_id')."');>".$this->coCellBox->getValue()."</a>");
        }else{
          $this->coCellBox->setValue("<a href='javascript:open_visualize(".$this->getFieldValue('context_id').",".CONTEXT_DOCUMENT_TEMPLATE.",\"".uniqid()."\")'>".$this->getFieldValue('context_name')."</a>");
        }
        return parent::drawItem();
      }case $this->getIndexByAlias('context_type'):{
        if ($this->getFieldValue('context_type')) {
          $moCtxObj = new ISMSContextObject();
          $moCtxObj = $moCtxObj->getContextObject($this->getFieldValue('context_type'));
          if($moCtxObj){
            $this->coCellBox->setValue($moCtxObj->getLabel());
          }else{
            if($this->getFieldValue('context_type')==3801){
              $this->coCellBox->setValue(FWDLanguage::getPHPStringValue('si_management','Gest�o'));
            }
          }
        }
        else {
          $this->coCellBox->setValue('-');
        }
        return parent::drawItem();
      }case $this->getIndexByAlias('context_state'):{
        $moCtxObj = new ISMSContextObject();
        $this->coCellBox->setValue($moCtxObj->getContextStateAsString($this->getFieldValue('context_state')));
        return parent::drawItem();
      }default:{
        return parent::drawItem();
      }
    }
  }
}

class FilterEvent extends FWDRunnable {
  public function run() {
    $moGrid = FWDWebLib::getObject('grid_template');
    $moGrid->execEventPopulate(); 
  }
}

class ContextConfirmRemove extends FWDRunnable {
  public function run(){
    $msTitle = FWDLanguage::getPHPStringValue('tt_remove_document_template','Remover Modelo de Documento');
    $msMessage = FWDLanguage::getPHPStringValue('st_remove_document_template_confirm',"Voc� tem certeza de que deseja remover o modelo de documento <b>%context_name%</b>?");
    
    $moPMDocument = new PMTemplate();
    $moPMDocument->fetchById(FWDWebLib::getObject('selected_context_id')->getValue());
    $msContextName = ISMSLib::truncateString($moPMDocument->getFieldValue('template_name'), 70);
    $msMessage = str_replace("%context_name%",$msContextName,$msMessage);
    
    $msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');".
                    "soWindow = soPopUp.getOpener();" .
                    "soWindow.remove_context();";
    
    ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,50);
  }
}

class RemoveContextEvent extends FWDRunnable {
  public function run(){
    $miContextId = FWDWebLib::getObject('selected_context_id')->getValue();
    $moContext = new PMTemplate();
    $moContext->delete($miContextId);
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new ContextConfirmRemove('context_confirm_remove'));
    $moStartEvent->addAjaxEvent(new RemoveContextEvent('remove_context'));
    $moStartEvent->addAjaxEvent(new FilterEvent('filter_all'));
    $moStartEvent->addSubmitEvent(new DownloadEvent("download"));
    
    //instala a seguran�a de ACL na p�gina
    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
    FWDMenuACLSecurity::getInstance()->installMenuACL(FWDWebLib::getObject('menu'),FWDACLSecurity::getInstance());
    
    
    
    $moGrid = FWDWebLib::getObject("grid_template");
    $moHandler = new QueryGridDocumentTemplate(FWDWebLib::getConnection());
    $miType = FWDWebLib::getObject('filter_type')->getValue();
    if($miType){
      $moHandler->setContextType($miType);
    }
    //filtro para exibir os tempaltes relacionados a uma melhor pr�tica
    if(FWDWebLib::getObject('type')->getValue()==CONTEXT_BEST_PRACTICE){
      $moContextObject = new ISMSContextObject();
      $maIds = explode(':',FWDWebLib::getObject('id')->getValue());
      $miId = $maIds[0];
      $moContext = $moContextObject->getContextObjectByContextId($miId);
      $moContext->fetchById($miId);
      $msContextType = $moContext->getLabel();
      $msContextName = $moContext->getName();
      FWDWebLib::getObject('grid_title')->setValue(" - ".$msContextType." ".$msContextName,false);
      $moHandler->setBestPracticeFilter($miId);
    }
    $moHandler->setState(FWDWebLib::getObject('filter_status')->getValue());
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new ContextDrawGrid());
  }
}


class ScreenEvent extends FWDRunnable {
  public function run(){
  
  

  FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        function refresh_grid(){
          js_refresh_grid('grid_template');
        }
        function remove_context(){
          trigger_event('remove_context',3);
          refresh_grid();
        }
        
        function read(piDocumentId) {
          gebi('read_document_id').value=piDocumentId;
          js_submit('download','ajax');
        }
        
      </script>
    <?
  }
}
//testa se a licen�a utilizada permite acessar o m�dulo de documenta��o
  ISMSLib::testUserPermissionToPoliceMode();

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_document_template.xml");

?>