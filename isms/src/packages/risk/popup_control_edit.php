<?php

include_once "include.php";
include_once $handlers_ref . "grid/QueryGridBestPracticeSearch.php";
include_once $handlers_ref . "grid/QueryGridBestPracticeSearchById.php";
include_once $handlers_ref . "QueryControlBP.php";
include_once $handlers_ref . "select/QuerySelectControlType.php";
include_once $handlers_ref . "select/QuerySelectReadableDocumentByContext.php";

set_time_limit(300);

function getControlFromFields(){
  $moControl = new RMControl();

  $moControl->setFieldValue('control_responsible_id',FWDWebLib::getObject('responsible_id')->getValue());
  $moControl->setFieldValue('control_name',FWDWebLib::getObject('control_name')->getValue());
  $moControl->setFieldValue('control_description',FWDWebLib::getObject('control_description')->getValue());
  //$moControl->setFieldValue('control_document',FWDWebLib::getObject('control_doc')->getValue());
  $moControl->setFieldValue('control_evidence',FWDWebLib::getObject('control_evidence')->getValue());
  $moControl->setFieldValue('control_days_before',FWDWebLib::getObject('control_task_alert')->getValue());
  
  $moConfig = new ISMSConfig();
  if($moConfig->getConfig(GENERAL_MANUAL_DATA_CONTROL)){
    if(FWDWebLib::getObject('date_realization')->getValue()){
      $moControl->setFieldValue('control_date_implemented',FWDWebLib::getObject('date_realization')->getTimeStamp());
    }else{
      $moControl->setFieldValue('control_date_implemented','null');
    }
  }else{
    if(FWDWebLib::getObject('control_implementation_date')->getValue()){
      $moControl->setFieldValue('control_date_implemented',FWDWebLib::getObject('control_implementation_date')->getValue());
    }else{
      $moControl->setFieldValue('control_date_implemented','null');
    }
  }
  
  $moControl->setFieldValue('control_date_deadline',FWDWebLib::getObject('implementation_limit')->getTimestamp());
  $miControlType = FWDWebLib::getObject('select_control_type')->getValue();
  if($miControlType) 
    $moControl->setFieldValue('control_type',$miControlType);
  else
    $moControl->setFieldValue('control_type', 'null');
  
  $mbHasPMModule = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(POLICY_MODE);
  if (!$mbHasPMModule) {
    $moControl->setFieldValue('control_document',FWDWebLib::getObject('control_doc')->getValue());
  }
  
  return $moControl;
}

function save($poContext,$pbHasSensitiveChanges,$piContextId=0){
  $moWebLib = FWDWebLib::getInstance();
  $miControlId = $piContextId;
  $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
  
  if($miControlId=='')
  	$miControlId=0;
    
  //teste para verificar se o sistema n�o est� sendo hakeado no evento de update
  if($miControlId){
    $moCtxUserTest = new RMControl();
    
    $moCtxResponsibleTest = new RMControl();
    $moCtxResponsibleTest->fetchById($miControlId);
    $moCtxUserTest->testPermissionToEdit($miControlId,$moCtxResponsibleTest->getResponsible());
    
    $moCtxUserTest->testPermissionToEdit($piContextId,$poContext->getResponsible());
  }

  $moControl = $poContext;
  $msCostOp = 'none';
  $msRevOp = 'none';
  $msTestOp = 'none';  
  
  $moConfig = new ISMSConfig();
  $mbRevisionIsEnabled = $moConfig->getConfig(GENERAL_REVISION_ENABLED);
  $mbCostIsEnabled = $moConfig->getConfig(GENERAL_COST_ENABLED);
  $mbTestIsEnabled = $moConfig->getConfig(GENERAL_TEST_ENABLED);
  if($mbCostIsEnabled){
    //salva os custos do controle caso exista c�digo para ser salvo
    $msCostCode = $moWebLib->getObject('control_cost_code')->getValue();
    if($msCostCode){
      $moCost = new RMControlCost();
      $maCostCode = array();
      $maCostCode = FWDWebLib::unserializeString($msCostCode);
      if($moCost->fetchById($miControlId)){
        $msCostOp = 'update';
      }else{
        $msCostOp = 'insert';
      }
      $moCost->setFieldValue('control_cost_1',$maCostCode[0]);
      $moCost->setFieldValue('control_cost_2',$maCostCode[1]);
      $moCost->setFieldValue('control_cost_3',$maCostCode[2]);
      $moCost->setFieldValue('control_cost_4',$maCostCode[3]);
      $moCost->setFieldValue('control_cost_5',$maCostCode[4]);
    }
  }
  if($mbRevisionIsEnabled){
    // verifica se o checkbox de revis�o est� habilitado
    // se estiver, salva as altera��es, se n�o estiver, deleta a revis�o do controle
    if($moWebLib->getObject('controller_revision')->getValue()){
      $msRevisionSensitive = FWDWebLib::getObject('var_revision_sensitive')->getValue();
      $msRevision = FWDWebLib::getObject('var_revision')->getValue();
      
      if($msRevisionSensitive){
        $moRevision = new WKFControlEfficiency();
        
        $maRevisionSensitive = FWDWebLib::unserializeString(FWDWebLib::convertToIso($msRevisionSensitive,true), false);
        $maRevision = FWDWebLib::unserializeString(FWDWebLib::convertToIso($msRevision,true), true);
        
        if(is_array($maRevision)){
          $maRevision = array_merge($maRevision,$maRevisionSensitive);
        }else{
          $maRevision = $maRevisionSensitive;
        }
        $maRevision['schedule'] = FWDWebLib::unserializeString($maRevision['schedule']);
        unset($maRevision['schedule']['schedule_id']);
        
        if($moRevision->fetchById($miControlId)){
          $msRevOp = 'update';
          $miScheduleId = $moRevision->getFieldValue('schedule');
        }else{
          $msRevOp = 'insert';
          $miScheduleId = 0;
        }
        $moSchedule = new WKFSchedule();
        $moSchedule->fillFieldsFromArray($maRevision['schedule']);
        $miScheduleId = $moSchedule->insertOrUpdate($miScheduleId);
        
        $moRevision->setFieldValue('expected_efficiency',$maRevision['controller_ee']);
        $moRevision->setFieldValue('value_1',$maRevision['revision_value_1'],false);
        $moRevision->setFieldValue('value_2',$maRevision['revision_value_2'],false);
        $moRevision->setFieldValue('value_3',$maRevision['revision_value_3'],false);
        $moRevision->setFieldValue('value_4',$maRevision['revision_value_4'],false);
        $moRevision->setFieldValue('value_5',$maRevision['revision_value_5'],false);
        $moRevision->setFieldValue('schedule',$miScheduleId);
        $moRevision->setFieldValue('metric' , $maRevision['revision_metric'],false);
        $moRevision->setFieldValue('observation' , $maRevision['observation'],false);
      }
    }else{
      $msRevOp = 'delete';
    }
  }
  if($mbTestIsEnabled){
    // verifica se o checkbox de teste est� habilitado
    // se estiver, salva as altera��es, se n�o estiver, deleta o teste do controle
    if($moWebLib->getObject('controller_test')->getValue()){
      $msTestSensitive = FWDWebLib::getObject('var_test_sensitive')->getValue();
      $msTest = FWDWebLib::getObject('var_test')->getValue();
      
      if($msTestSensitive){
        $moTest = new WKFControlTest();
        
        $maTestSensitive = FWDWebLib::unserializeString(FWDWebLib::convertToIso($msTestSensitive,true), false);
        $maTest = FWDWebLib::unserializeString(FWDWebLib::convertToIso($msTest,true), true);
        
        if(is_array($maTest)){
          $maTest = array_merge($maTest,$maTestSensitive);
        }else{
          $maTest = $maTestSensitive;
        }
        $maTest['schedule'] = FWDWebLib::unserializeString($maTest['schedule']);
        unset($maTest['schedule']['schedule_id']);
        
        if($moTest->fetchById($miControlId)){
          $msTestOp = 'update';
          $miScheduleId = $moTest->getFieldValue('schedule');
        }else{
          $msTestOp = 'insert';
          $miScheduleId = 0;
        }
        $moSchedule = new WKFSchedule();
        $moSchedule->fillFieldsFromArray($maTest['schedule']);
        $miScheduleId = $moSchedule->insertOrUpdate($miScheduleId);
        
        $moTest->setFieldValue('description',$maTest['description'],false);
        $moTest->setFieldValue('schedule',$miScheduleId);
      }
      
    }else{
      $msTestOp = 'delete';
    }
  }
  
  if($miControlId){
    $moControl->update($miControlId,true,$pbHasSensitiveChanges);
  }else{
    $miControlId = $moControl->insert(true);
  }

  //dispara o workflow de implementa��o do controle
  if(trim($moWebLib->getObject('var_has_implemented_control')->getValue())){
    $moWKFImp = new RMControlImplementationAccept();
    if($moWebLib->getObject('implementation_limit')->getValue()){
      $moWKFImp->setFieldValue('control_date_deadline',$moWebLib->getObject('implementation_limit')->getTimestamp());
    }else{
      $moWKFImp->setFieldValue('control_date_implemented','null');
    }
    $moWKFImp->update($miControlId);
  }

  $msNewBestPractices = $moWebLib->getObject('control_model_ids')->getValue();
  if($msNewBestPractices){
    $maNewBestPractices = array_filter(explode(':',$msNewBestPractices));
  }else{
    $maNewBestPractices = array();
  }
  $msOldBestPractices = $moWebLib->getObject('control_model_ids_initial')->getValue();
  if($msOldBestPractices){
    $maOldBestPractices = array_filter(explode(':',$msOldBestPractices));
  }else{
    $maOldBestPractices = array();
  }
  $maToInsert = array_diff($maNewBestPractices,$maOldBestPractices);
  $maToDelete = array_diff($maOldBestPractices,$maNewBestPractices);

  $moControlBestPractice = new RMControlBestPractice();
  if(count($maToInsert)) $moControlBestPractice->insertBestPractices($miControlId,$maToInsert);
  if(count($maToDelete)) $moControlBestPractice->deleteBestPractices($miControlId,$maToDelete);
  
  switch($msCostOp){
    case 'insert':
      $moCost->setFieldValue('control_id',$miControlId);
      $moCost->insert();
      break;
    case 'update':
      $moCost->update($miControlId);
      break;
  }
  
  $miRevTaskId = 0;
  switch($msRevOp){
    case 'insert':
      $moRevision->setFieldValue('control_efficiency_id',$miControlId);
      $moRevision->insert();
      break;
    case 'update':
      $moRevision->update($miControlId);
      break;
    case 'delete':
      $moRevision = new WKFControlEfficiency();
      $moRevision->delete($miControlId);
      break;
  }

  $miTestTaskId = 0;
  switch($msTestOp){
    case 'insert':
      $moTest->setFieldValue('control_test_id',$miControlId);
      $moTest->insert();
      break;
    case 'update':
      $moTest->update($miControlId);
      break;
    case 'delete':
      $moTest = new WKFControlTest();
      $moTest->delete($miControlId);
      break;
  }
  
  $moControl = new RMControl();
  $moControl->setFieldValue('control_id',$miControlId);
  $moControl->updateActive();
  
  $mbHasPMModule = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(POLICY_MODE);
  if ($mbHasPMModule) {
    $moDocContext = new PMDocContext();
    $moDocContext->createFilter($miControlId,'context_id');
    $moDocContext->select();
    while($moDocContext->fetch()) {
      $moDocContext->updateAutoApprovers($moDocContext->getFieldValue('document_id'));
    }
  }

  if($miRevTaskId){
    echo "self.getOpener().open_task_control_revision($miRevTaskId,$miControlId);";
  }
  if($miTestTaskId){
    echo "self.getOpener().open_task_control_test($miTestTaskId,$miControlId);";
  }

  echo "soOpener = self.getOpener();"
      ."if(soOpener.refresh_grid) soOpener.refresh_grid();"
      ."self.close();";
      
  return $miControlId;
}

class ConfirmEditEvent extends FWDRunnable {
  public function run(){
    $moControl = getControlFromFields();
    $miControlId = FWDWebLib::getObject('control_id')->getValue();
    save($moControl,true,$miControlId);
  }
}

class ImplementControlEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $miDateTest = ISMSLib::ISMSTime();
    $miDate = mktime (0, 0, 0, date("m",$miDateTest), date("d",$miDateTest),  date("Y",$miDateTest));
    $miInitialDeadline = $moWebLib->getObject('initial_deadline')->getValue();
    $miNewDeadline = $moWebLib->getObject('implementation_limit')->getTimestamp();
    if($miNewDeadline!=$miInitialDeadline && $miNewDeadline < $miDate){
      echo "gobi('st_control_edit_warning_deadline').show();";
    }else{
      echo "
	    set_date_impl();
	    if(gebi('var_has_revision').value==1){
	      if(gebi('st_control_revision'))
          gobi('st_control_revision').show();
	      if(gebi('controller_revision_1')){
          js_show('controller_revision_1');
	        uncheckCheckBox('controller_revision',1);
        }
	    }
	    if(gebi('var_has_test').value==1){
	      if(gebi('st_control_tests'))
          gobi('st_control_tests').show();
	      if(gebi('controller_test_1'))
          js_show('controller_test_1');
	      uncheckCheckBox('controller_test',1);
	    }
	    gobi('vb_implementation').hide();
	    gobi('vb_remove_implementation').show();
	    gebi('var_has_implemented_control').value=1;
	    js_hide('viewgroup_deadline');
	    
	    var soStatic = gobi('st_deadline');
	    soStatic.show();
	    soStatic.setValue(gebi('implementation_limit').value);
      gobi('st_control_edit_warning_deadline').hide();
      ";
    }
  }
}

class SaveControlEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $mbCanSave = true;
    $moConfig = new ISMSConfig();

    if($moConfig->getConfig(GENERAL_MANUAL_DATA_CONTROL)){
      if(FWDWebLib::getObject('var_initial_implementation_date')->getValue() && !trim(FWDWebLib::getObject('date_realization')->getValue())){
        echo "gobi('st_control_edit_warning_implementation_is_null').show();";
        $mbCanSave = false;
      }
    }
    
    $miDateTest = ISMSLib::ISMSTime();
        
    $miDate = mktime (0, 0, 0, date("m",$miDateTest), date("d",$miDateTest),  date("Y",$miDateTest));
    $miInitialDeadline = $moWebLib->getObject('initial_deadline')->getValue();
    $miNewDeadline = $moWebLib->getObject('implementation_limit')->getTimestamp();
    if($miNewDeadline!=$miInitialDeadline && $miNewDeadline < $miDate){
      echo "gobi('st_control_edit_warning_deadline').show();";
      $mbCanSave = false;
    }
    
    $miDateRealization = $moWebLib->getObject('date_realization')->getTimestamp();
    if($miDateRealization > $miDate){
      echo "gobi('st_control_edit_warning_implementation_cannot_after_today').show();";
      $mbCanSave = false;
    }
    
    if($mbCanSave){
      echo "gobi('st_control_edit_warning_deadline').hide();";
      echo "gobi('st_control_edit_warning_implementation_is_null').hide();";

      $moControl = getControlFromFields();
      
      $miControlId = FWDWebLib::getObject('control_id')->getValue();
      $moControl->setHash(FWDWebLib::getObject('hash')->getValue());
      
      $moConfig = new ISMSConfig();
      $mbRevisionIsEnabled = $moConfig->getConfig(GENERAL_REVISION_ENABLED);
      $mbCostIsEnabled = $moConfig->getConfig(GENERAL_COST_ENABLED);
      $mbTestIsEnabled = $moConfig->getConfig(GENERAL_TEST_ENABLED);
      
      $maControllers = FWDWebLib::unserializeString(FWDWebLib::getObject('controllers')->getValue());
      $mbTestIsChecked = FWDWebLib::getObject('controller_test')->getValue();
      $mbRevIsChecked = FWDWebLib::getObject('controller_revision')->getValue();
      $mbTestWasChecked = $maControllers['controller_test'];
      $mbRevWasChecked = $maControllers['controller_revision'];
      
      if($mbTestIsChecked XOR $mbTestWasChecked){
        $mbHasSensitiveChanges = true;
      }elseif($mbRevIsChecked XOR $mbRevWasChecked){
        $mbHasSensitiveChanges = true;
      }else{
        $mbHasSensitiveChanges = false;
      }
      
      if($mbRevisionIsEnabled && !$mbHasSensitiveChanges){
        // verifica se os campos sens�veis da revis�o foram modificados
        if(FWDWebLib::getObject('controller_revision')->getValue()){
          $msRevisionSensitiveFields = FWDWebLib::getObject('var_revision_sensitive')->getValue();
          if($msRevisionSensitiveFields){
            $msHash = md5(FWDWebLib::convertToIso($msRevisionSensitiveFields,true));
            if(FWDWebLib::getObject('control_revision_hash')->getValue()!=$msHash){
              $mbHasSensitiveChanges = true;
            }
          }
        }
      }
      if($mbTestIsEnabled && !$mbHasSensitiveChanges){
        // verifica se os campos sens�veis do teste foram modificados
        if(FWDWebLib::getObject('controller_test')->getValue()){
          $msTestSensitiveFields = FWDWebLib::getObject('var_test_sensitive')->getValue();
          if($msTestSensitiveFields){
            $msHash = md5(FWDWebLib::convertToIso($msTestSensitiveFields,true));
            if(FWDWebLib::getObject('control_test_hash')->getValue()!=$msHash){
              $mbHasSensitiveChanges = true;
            }
          }
        }
      }
      
      if(!$mbHasSensitiveChanges){
        $msNewBestPractices = $moWebLib->getObject('control_model_ids')->getValue();
        if($msNewBestPractices){
          $maNewBestPractices = array_filter(explode(':',$msNewBestPractices));
        }else{
          $maNewBestPractices = array();
        }
        $msOldBestPractices = $moWebLib->getObject('control_model_ids_initial')->getValue();
        if($msOldBestPractices){
          $maOldBestPractices = array_filter(explode(':',$msOldBestPractices));
        }else{
          $maOldBestPractices = array();
        }
        
        $maToInsert = array_diff($maNewBestPractices,$maOldBestPractices);
        $maToDelete = array_diff($maOldBestPractices,$maNewBestPractices);
        
        if(count($maToInsert) || count($maToDelete)){
          $mbHasSensitiveChanges = true;
        }
      }
      if ($miControlId) {
        $moControlAux = new RMControl();
        $moControlAux->fetchById($miControlId);
        $miControlState = $moControlAux->getContextState();
        $mbControlStatePendant = (bool)($miControlState == CONTEXT_STATE_PENDANT);
      }
      if(!$mbHasSensitiveChanges) $mbHasSensitiveChanges = $moControl->hasSensitiveChanges();
      $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
      if($miControlId && $mbHasSensitiveChanges && $miUserId != $moControl->getApprover() && !$mbControlStatePendant){
        $msTitle = FWDLanguage::getPHPStringValue('tt_reapproval','Reaprova��o');
        $msMessage = FWDLanguage::getPHPStringValue('st_sensitive_data_reapproval','Voc� est� alterando dados sens�veis, que necessitam passar por uma reaprova��o. Deseja confirmar as altera��es?');
        $msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
                        "soWindow = soPopUp.getOpener();" .
                        "soWindow.trigger_event('confirm_edit',3);";
        ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,75);
      }else{
        $miControlId = save($moControl,$mbHasSensitiveChanges,$miControlId);
      }
            
      /*
       * Envia um alerta/email na hora em que o controle � criado/editado,
       * ao inv�s de enviar somente que chegar X dias antes. Caso o usu�rio
       * corrente seja o respons�vel n�o precisa fazer nada.
       */      
      $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
      $miResponsibleId = $moControl->getFieldByAlias('control_responsible_id')->getValue();
      if($miUserId != $miResponsibleId && $moControl->getFieldValue('control_date_implemented')=='null'){
      	/*
      	 * Envia alerta
      	 */
      	$moAlert = new WKFAlert();
      	$moAlert->setFieldValue('alert_context_id', $miControlId);
      	$moAlert->setFieldValue('alert_creator_id', $miUserId);
      	$moAlert->setFieldValue('alert_receiver_id', $miResponsibleId);
      	$moAlert->setFieldValue('alert_type', WKF_ALERT_IMPLEMENTATION_ALERT);
      	$moAlert->setFieldValue('alert_date', ISMSLib::ISMSTime());
      	$moAlert->insert();
      }
    }
  }
}

class ControlConfirmRemoveImplementation extends FWDRunnable {
  public function run(){
    $miControlId = FWDWebLib::getObject('control_id')->getValue();

    $moControl = new RMControl();
    $moControl->fetchById($miControlId);
    $msControlName = ISMSLib::truncateString($moControl->getFieldValue('control_name'), 50);

    $moConfig = new ISMSConfig();
    $mbRevisionIsEnabled = $moConfig->getConfig(GENERAL_REVISION_ENABLED);
    $mbTestIsEnabled = $moConfig->getConfig(GENERAL_TEST_ENABLED);
    $msRevision = '';
    $msTests = '';
    $msContent='';
    $msAnd = '';
    $msIni = FWDLanguage::getPHPStringValue("st_result_in_removing","Isto resultar� na remo��o");
    $msEnd = FWDLanguage::getPHPStringValue("st_this_control","deste controle");
    if($mbRevisionIsEnabled)
       $msRevision = FWDLanguage::getPHPStringValue("st_of_revision","da revis�o");
    if($mbTestIsEnabled)
      $msTests = FWDLanguage::getPHPStringValue("st_of_test","do teste");
    if(($mbRevisionIsEnabled)&&($mbTestIsEnabled))
      $msAnd = FWDLanguage::getPHPStringValue("st_and","e");

    if(($mbRevisionIsEnabled)||($mbTestIsEnabled))
      $msContent = "<br>{$msIni} {$msTests} {$msAnd} {$msRevision} {$msEnd}!";
    else
      $msContent = "";
    $msTitle = FWDLanguage::getPHPStringValue('tt_remove_control_implementation','Remover a impementa��o do controle');

    $msMessage = FWDLanguage::getPHPStringValue('st_remove_control_implementation_confirm',"Voc� tem certeza de que deseja remover a implementa��o do controle <b>%control_name%</b>?");
    $msMessage = str_replace('%control_name%', $msControlName, $msMessage);

    $msMessageFinal = $msMessage.$msContent;
    $msEventValue = " soPopUpManager.getPopUpById('popup_confirm').getOpener().remove_control_implementation();";
    ISMSLib::openConfirm($msTitle,$msMessageFinal,$msEventValue,70);
  }
}

class RemoveImplementationControlEvent extends FWDRunnable {
  public function run(){
    $miControlId = FWDWebLib::getObject('control_id')->getValue();
    if($miControlId){
      //teste para verificar se o sistema n�o est� sendo hakeado
      $moCtxUserTest = new RMControl();
      $moCtxResponsibleTest = new RMControl();
      $moCtxResponsibleTest->fetchById($miControlId);
      $moCtxUserTest->testPermissionToEdit($miControlId,$moCtxResponsibleTest->getResponsible());
      
      //deleta a revis�o do controle
      $moRev = new WKFControlEfficiency();
      $moRev->delete($miControlId);
      //deleta o teste do controle
      $moTest = new WKFControlTest();
      $moTest->delete($miControlId);

      $moWKFImp = new RMControlImplementationAccept();
      $moWKFImp->setFieldValue('control_date_implemented','null');
      $moWKFImp->update($miControlId);

      $moWKFAlert = new WKFAlert();
      $moWKFAlert->getFieldByAlias('alert_context_id')->addFilter(new FWDDBFilter('=', $miControlId));
      $moWKFAlert->getFieldByAlias('alert_type')->addFilter(new FWDDBFilter('=',WKF_ALERT_REAL_EFFICIENCY ));
      $moWKFAlert->getFieldByAlias('alert_type')->addFilter(new FWDDBFilter('=', WKF_ALERT_TEST_CONTROL));
      $moWKFAlert->getFieldByAlias('alert_type')->setFilterSeparator(false);
      $moWKFAlert->delete();

      $moWKFTask = new WKFTask();
      $moWKFTask->getFieldByAlias('task_context_id')->addFilter(new FWDDBFilter('=',$miControlId));
      $moWKFTask->getFieldByAlias('task_activity')->addFilter(new FWDDBFilter('=',ACT_CONTROL_TEST));
      $moWKFTask->getFieldByAlias('task_activity')->addFilter(new FWDDBFilter('=',ACT_REAL_EFFICIENCY));
      $moWKFTask->getFieldByAlias('task_activity')->setFilterSeparator(false);
      $moWKFTask->delete();
    }
    echo "
      gobi('st_date_realization').setValue('');
      gebi('control_implementation_date').value = '';
      gebi('date_realization').value = '';
      if(gebi('control_test_code'))
        gebi('control_test_code').value = '';
      if(gebi('control_test_description'))
        gebi('control_test_description').value = '';
      if(gebi('control_revision_code'))  
        gebi('control_revision_code').value = '';
      if(gebi('st_control_tests'))
        gobi('st_control_tests').hide();
      if(gebi('st_control_revision'))
        gobi('st_control_revision').hide();
      if(gebi('controller_revision_1'))
        js_hide('controller_revision_1');
      if(gebi('controller_test_1'))
        js_hide('controller_test_1');
      if(gebi('controller_revision'))
        gebi('controller_revision').value='';
      if(gebi('controller_test'))
        gebi('controller_test').value='';
      if(gebi('vb_control_revision'))
        js_hide('vb_control_revision');
      if(gebi('control_tests'))
        js_hide('control_tests');
      js_show('viewgroup_deadline');
      js_show('control_task_alert');
      gobi('st_deadline').hide();
      gobi('vb_remove_implementation').hide();
      gebi('var_initial_implementation_date').value= '';
    ";
    $moConfig = new ISMSConfig();
    if(!$moConfig->getConfig(GENERAL_MANUAL_DATA_CONTROL)){
      echo "
        gobi('vb_implementation').show();
      ";
    }
  }
}


class PopulateSelBestPractice extends FWDRunnable{
  public function run(){
    $moHandler = new QueryGridBestPracticeSearchById(FWDWebLib::getConnection());
    $msIds = FWDWebLib::getObject('control_model_ids')->getValue();
    $moHandler->setSectionsIds($msIds);
    $maAux = $moHandler->getSelectSessionInfo();
    $msAux = serialize($maAux);
    echo ISMSLib::getJavascriptPopulateSelect('sel_ctrl_model', $msAux);
  }
}


class DocumentViewEvent extends FWDRunnable {
  public function run() {
    $moWebLib = FWDWebLib::getInstance();
    $miDocumentId = $moWebLib->getObject('select_control_document')->getValue();
    $miUserId = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    
    $moDocument = new PMDocument();
    $moDocument->testPermissionToRead($miDocumentId);
    $moDocument->fetchById($miDocumentId);
    $moPMDocInstance = $moDocument->getPublishedInstance();
    $miDocInstanceId = $moPMDocInstance->getFieldValue('doc_instance_id');
    
    $moReader = new PMDocumentReader();
    $moReader->createFilter($miDocumentId,'document_id');
    $moReader->createFilter($miUserId,'user_id');
    $moReader->select();
    if ($moReader->fetch()) {
      $moReader->setFieldValue('user_has_read_document',1);
      $moReader->update();
    }
    
    $moPMDocReadHistory = new PMDocReadHistory();
    $moPMDocReadHistory->setFieldValue('user_id',$miUserId);
    $moPMDocReadHistory->setFieldValue('document_id',$miDocInstanceId);
    $moPMDocReadHistory->setFieldValue('read_date',ISMSLib::ISMSTime());
    $moPMDocReadHistory->insert();
    
    $mbIsLink = (bool)$moPMDocInstance->getFieldValue('doc_instance_is_link');
    if ($mbIsLink) {
      $msURL = $moPMDocInstance->getFieldValue('doc_instance_link');
      if ($msURL)
        echo "js_open('{$msURL}','document_link',740,502,'center_screen');";
    } else {
      echo "js_submit('download_file','ajax');";
    }
  }
}

class DownloadFileEvent extends FWDRunnable {
  public function run() {
    $moWebLib = FWDWebLib::getInstance();
    $miDocumentId = $moWebLib->getObject('select_control_document')->getValue();
    $miUserId = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    
    $moDocument = new PMDocument();
    $moDocument->testPermissionToRead($miDocumentId);
    $moDocument->fetchById($miDocumentId);
    $moPMDocInstance = $moDocument->getPublishedInstance();
    
    $msPath = $moWebLib->getSysRef()."packages/policy/".$moPMDocInstance->getFieldValue('doc_instance_path');
    $msFileName = $moPMDocInstance->getFieldValue('doc_instance_file_name');
    header('Cache-Control: ');// leave blank to avoid IE errors
    header('Pragma: ');// leave blank to avoid IE errors
    header('Content-type: application/octet-stream');
    header('Content-Disposition: attachment; filename="'.rawurlencode($msFileName).'"');
    $moCrypt = new FWDCrypt();
    $moFP = fopen($msPath,'rb');
    $moCrypt->setIV(fread($moFP,strlen($moCrypt->getIV())));
    while(!feof($moFP)) {
      echo $moCrypt->decryptNoBase64(fread($moFP,16384));
    }
    fclose($moFP);
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new PopulateSelBestPractice("sel_best_practice"));
    $moStartEvent->addAjaxEvent(new SaveControlEvent("save_control"));
    $moStartEvent->addAjaxEvent(new ControlConfirmRemoveImplementation('control_confirm_implementation_remove'));
    $moStartEvent->addAjaxEvent(new RemoveImplementationControlEvent('remove_control_implementation'));
    $moStartEvent->addAjaxEvent(new ConfirmEditEvent('confirm_edit'));
    $moStartEvent->addAjaxEvent(new ImplementControlEvent('implementation_ctrl_event'));
    $moStartEvent->addAjaxEvent(new DocumentViewEvent('document_view_event'));
    $moStartEvent->addSubmitEvent(new DownloadFileEvent('download_file'));
    $miAuxDate = ISMSLib::ISMSTime();
    FWDWebLib::getObject('control_implementation_date_aux')->setValue($miAuxDate);
    FWDWebLib::getObject('control_implementation_date_string')->setValue(ISMSLib::getISMSShortDate($miAuxDate,true));

  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $miControlId = $moWebLib->getObject('param_control_id')->getValue();
    $moWebLib->getObject('control_id')->setValue($miControlId);
    $moConfig = new ISMSConfig();
    $mbRevisionIsEnabled = $moConfig->getConfig(GENERAL_REVISION_ENABLED);
    $mbCostIsEnabled = $moConfig->getConfig(GENERAL_COST_ENABLED);
    $mbTestIsEnabled = $moConfig->getConfig(GENERAL_TEST_ENABLED);
    $mbManualData = $moConfig->getConfig(GENERAL_MANUAL_DATA_CONTROL);
    $mbHasDateAccomplishment = false;

    $moHandler = new QuerySelectControlType(FWDWebLib::getConnection());    
    $moSelect = FWDWebLib::getObject('select_control_type');
    $moSelect->setQueryHandler($moHandler);
    $moSelect->populate();

    $msCurrency = ISMSLib::getCurrencyString();
    $moWebLib->getObject('st_control_cost_simble')->setValue($msCurrency);
    
    /*
     * Se possui o m�dulo de documenta��o, troca o campo texto
     * por select, e mostra um bot�o para o usu�rio visualizar
     * o documento selecionado. Se n�o houver documento associado,
     * mostrar mensagem indicando tal situa��o.
     */
    $mbHasPMModule = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(POLICY_MODE);
    if ($mbHasPMModule) {
      if($miControlId){
        FWDWebLib::getObject('document_without_policy')->setShouldDraw(false);
        FWDWebLib::getObject('document_with_policy')->setShouldDraw(true);
        
        $moHandler = new QuerySelectReadableDocumentByContext(FWDWebLib::getConnection());
        $moHandler->setContextId($miControlId);
        $moSelect = FWDWebLib::getObject('select_control_document');
        $moSelect->setQueryHandler($moHandler);
        $moSelect->populate();
        
        if (!$moSelect->getValue()) {
          FWDWebLib::getObject('no_published_documents')->setValue(FWDLanguage::getPHPStringValue('st_no_published_documents','N�o h� documentos publicados'));
          FWDWebLib::getObject('no_published_documents')->setAttrDisplay('true');
          FWDWebLib::getObject('control_document_create')->setAttrDisplay('true');
          FWDWebLib::getObject('select_control_document')->setAttrDisplay('false');
          FWDWebLib::getObject('control_document_view')->setShouldDraw(false);
          FWDWebLib::getObject('control_document_properties')->setShouldDraw(false);
        }
      }else{
        FWDWebLib::getObject('document_without_policy')->setShouldDraw(false);
        FWDWebLib::getObject('document_with_policy')->setShouldDraw(false);
        FWDWebLib::getObject('st_control_document')->setShouldDraw(false);
      }
      
      /* Provisoriamente, como os documentos passaram a ser acessados
       * a partir do menu de contexto, estou removendo a linha de
       * documento da tela de edi��o
       */
       FWDWebLib::getObject('st_control_document')->setShouldDraw(false);
       FWDWebLib::getObject('document_with_policy')->setShouldDraw(false);
       
    } else {
      FWDWebLib::getObject('document_without_policy')->setShouldDraw(true);
      FWDWebLib::getObject('document_with_policy')->setShouldDraw(false);
    }

    if(!$mbRevisionIsEnabled){
      $moWebLib->getObject('st_control_revision')->setShouldDraw(false);
      $moWebLib->getObject('check_control_revision')->setShouldDraw(false);
      $moWebLib->getObject('var_has_revision')->setValue('');

    }
    if(!$mbTestIsEnabled){
      $moWebLib->getObject('st_control_tests')->setShouldDraw(false);
      $moWebLib->getObject('check_control_tests')->setShouldDraw(false);
      $moWebLib->getObject('var_has_test')->setvalue('');

    }
    if(!$mbCostIsEnabled){
      $moWebLib->getObject('control_impact')->setShouldDraw(false);
      $moWebLib->getObject('st_control_cost')->setShouldDraw(false);
      $moWebLib->getObject('st_control_cost_simble')->setShouldDraw(false);
      $moWebLib->getObject('st_cost')->setShouldDraw(false);
    }
    
    $moIcon = FWDWebLib::getObject("tooltip_icon");
    if($miControlId){
      $moControl = new RMControl();
        $moControl->fetchById($miControlId);
        $miResponsibleId = $moControl->getFieldValue('control_responsible_id');
  
        //teste para verificar se o sistema n�o est� sendo hakeado
        $moCtxUserTest = new RMControl();
        $moCtxUserTest->testPermissionToEdit($miControlId,$moControl->getResponsible());

      if($miResponsibleId){
        $moUser = new ISMSUser();
        if($moUser->fetchById($miResponsibleId))
          $moWebLib->getObject('control_responsible')->setValue($moUser->getName());

        $moWebLib->getObject('responsible_id')->setValue($miResponsibleId);
        $moWebLib->getObject('control_name')->setValue($moControl->getFieldValue('control_name'));
        $moWebLib->getObject('control_description')->setValue($moControl->getFieldValue('control_description'));
        $moWebLib->getObject('control_doc')->setValue($moControl->getFieldValue('control_document'));
        $moWebLib->getObject('control_evidence')->setValue($moControl->getFieldValue('control_evidence'));
        $moWebLib->getObject('control_task_alert')->setValue($moControl->getFieldValue('control_days_before'));
        $moWebLib->getObject('select_control_type')->checkItem($moControl->getFieldValue('control_type'));
        if($moControl->getFieldValue('control_date_deadline')){
          $moWebLib->getObject('implementation_limit')->setValue(ISMSLib::getISMSShortDate($moControl->getFieldValue('control_date_deadline')));
          $moWebLib->getObject('initial_deadline')->setValue($moWebLib->getObject('implementation_limit')->getTimestamp());
        }
        if($moControl->getFieldValue('control_date_implemented')){
          if($mbManualData){
            FWDWebLib::getObject('vg_realization')->setAttrDisplay("true");
            FWDWebLib::getObject('st_deadline')->setAttrDisplay("false");
            FWDWebLib::getObject('st_date_realization')->setAttrDisplay("false");
            FWDWebLib::getObject('var_initial_implementation_date')->setValue($moControl->getFieldValue('control_date_implemented'));
            FWDWebLib::getObject('date_realization')->setValue(ISMSLib::getISMSShortDate($moControl->getFieldValue('control_date_implemented')));
          }else{
            FWDWebLib::getObject('st_date_realization')->setAttrDisplay("true");
            $moWebLib->getObject('st_date_realization')->setValue(ISMSLib::getISMSShortDate($moControl->getFieldValue('control_date_implemented',true)));
            $moWebLib->getObject('control_implementation_date')->setValue(ISMSLib::getTimestamp($moControl->getFieldValue('control_date_implemented')));
            $moWebLib->getObject('viewgroup_deadline')->setAttrDisplay('false');
            $moWebLib->getObject('st_deadline')->setValue($moWebLib->getObject('implementation_limit')->getValue());
          }
          $mbHasDateAccomplishment = true;
          $moWebLib->getObject('vb_remove_implementation')->setAttrDisplay('true');
        }else{
          if($mbManualData){
            FWDWebLib::getObject('vg_realization')->setAttrDisplay("true");
            FWDWebLib::getObject('st_deadline')->setAttrDisplay("false");
          }else{
            $moWebLib->getObject('vb_implementation')->setAttrDisplay('true');
            $moWebLib->getObject('st_deadline')->setAttrDisplay('false');
          }
        }
      }
      
      $moHandler = new QueryControlBP(FWDWebLib::getConnection());
      $moHandler->setControlId($miControlId);
      $maIds = $moHandler->getBestPracticesControl();
      if($maIds){
        $msIds = serialize($maIds);

        $maAuxIds = array();
        foreach($maIds as $miId)
          $maAuxIds[] = $miId[0];
        $msAuxIds = implode(':',array_unique(array_filter($maAuxIds)));
        $moWebLib->getObject('sel_values_serialized')->setValue($msIds);
        $moWebLib->getObject('control_model_ids')->setValue($msAuxIds);
        $moWebLib->getObject('control_model_ids_initial')->setValue($msAuxIds);
      }
      if($mbRevisionIsEnabled && $mbHasDateAccomplishment){
        $moRevision = new WKFControlEfficiency();
        if($moRevision->fetchById($miControlId)){
          FWDWebLib::getObject('controller_revision')->setValue(':1');
          FWDWebLib::getObject('vb_control_revision')->setAttrDisplay('true');
          
          $moSchedule = new WKFSchedule();
          $moSchedule->fetchById($moRevision->getFieldValue('schedule'));
          $maSchedule = $moSchedule->getFieldValues();
          unset($maSchedule['schedule_id']);
          
          $maRevisionSensitive = array(
            'controller_ee' => $moRevision->getFieldValue('expected_efficiency'),
            'schedule' => serialize($maSchedule)
          );
          
          $maRevision = array(
            'revision_value_1' => htmlspecialchars($moRevision->getFieldValue('value_1'), ENT_QUOTES),
            'revision_value_2' => htmlspecialchars($moRevision->getFieldValue('value_2'), ENT_QUOTES),
            'revision_value_3' => htmlspecialchars($moRevision->getFieldValue('value_3'), ENT_QUOTES),
            'revision_value_4' => htmlspecialchars($moRevision->getFieldValue('value_4'), ENT_QUOTES),
            'revision_value_5' => htmlspecialchars($moRevision->getFieldValue('value_5'), ENT_QUOTES),
            'revision_metric'  => htmlspecialchars($moRevision->getFieldValue('metric'), ENT_QUOTES),
          	'observation'  => htmlspecialchars($moRevision->getFieldValue('observation'), ENT_QUOTES)
          );
          
          $msRevisionSensitive = serialize($maRevisionSensitive);
          $msRevision = serialize($maRevision);
          
          FWDWebLib::getObject('var_revision_sensitive')->setValue($msRevisionSensitive);
          FWDWebLib::getObject('var_revision')->setValue($msRevision);
          FWDWebLib::getObject('control_revision_hash')->setValue(md5($msRevisionSensitive));
        }
        FWDWebLib::getObject('st_control_revision')->setAttrDisplay('true');
        FWDWebLib::getObject('check_control_revision')->setAttrDisplay('true');
      }
      if($mbTestIsEnabled && $mbHasDateAccomplishment){
        $moTest = new WKFControlTest();
        if($moTest->fetchById($miControlId)){
          FWDWebLib::getObject('controller_test')->setValue(':1');
          FWDWebLib::getObject('control_tests')->setAttrDisplay('true');
          
          $moSchedule = new WKFSchedule();
          $moSchedule->fetchById($moTest->getFieldValue('schedule'));
          
          $maTestSensitive = array(
            'schedule' => serialize($moSchedule->getFieldValues())
          );
          
          $maTest = array(
            'description' => htmlspecialchars($moTest->getFieldValue('description'), ENT_QUOTES)
          );
          
          $msTestSensitive = serialize($maTestSensitive);
          $msTest = serialize($maTest);
          
          FWDWebLib::getObject('var_test_sensitive')->setValue($msTestSensitive);
          FWDWebLib::getObject('var_test')->setValue($msTest);
          FWDWebLib::getObject('control_test_hash')->setValue(md5($msTestSensitive));
        }
        FWDWebLib::getObject('st_control_tests')->setAttrDisplay('true');
        FWDWebLib::getObject('check_control_tests')->setAttrDisplay('true');
      }
      $moToolTip = ISMSLib::getInstance()->getToolTipCreateModifyById($miControlId);
      $moIcon->addObjFWDEvent($moToolTip);
      $moCost = new RMControlCost();
      if($moCost->fetchById($miControlId)){
        $mfCost1 = $moCost->getFieldValue('control_cost_1');
        $mfCost2 = $moCost->getFieldValue('control_cost_2');
        $mfCost3 = $moCost->getFieldValue('control_cost_3');
        $mfCost4 = $moCost->getFieldValue('control_cost_4');
        $mfCost5 = $moCost->getFieldValue('control_cost_5');
        $mfTotal = ($mfCost1 + $mfCost2 + $mfCost3 + $mfCost4 + $mfCost5);
        $moWebLib->getObject('st_cost')->setValue(number_format($mfTotal,2));
      }
      FWDWebLib::getObject('hash')->setValue($moControl->getHash());
    }else{
      //teste para verificar se o sistema n�o est� sendo hakeado
      $moCtxUserTest = new RMControl();
      $moCtxUserTest->testPermissionToInsert();
      
      $moIcon->setAttrDisplay("false");
      $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
      $miUserId = $moSession->getUserId();
      $moUser = new ISMSUser();
      $moUser->fetchById($miUserId);
      FWDWebLib::getObject("responsible_id")->setValue($miUserId);
      FWDWebLib::getObject("control_responsible")->setValue($moUser->getFieldValue('user_name'));
      $moWebLib->getObject('control_edit_title')->setValue(FWDLanguage::getPHPStringValue('tt_control_adding', "Adi��o de Controle"));

      $moWebLib->getObject('st_deadline')->setAttrDisplay('false');
      if($mbManualData){
        FWDWebLib::getObject('vg_realization')->setAttrDisplay("true");
      }else{
        $moWebLib->getObject('vb_implementation')->setAttrDisplay('true');
      
      }
    }
    
    $msControllers = serialize(array(
      'controller_test' => $moWebLib->getObject('controller_test')->getValue(),
      'controller_revision' => $moWebLib->getObject('controller_revision')->getValue()
    ));
    FWDWebLib::getObject('controllers')->setValue($msControllers);


    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        
        <? echo ISMSLib::getJavascriptPopulateSelect('sel_ctrl_model', FWDWebLib::getObject('sel_values_serialized')->getValue()); ?>
        gebi('sel_values_serialized').value = 'a:0:{}';

        function remove_control_implementation(){
            trigger_event('remove_control_implementation',3)
        }
        function set_date_impl(){
            gobi('st_date_realization').setValue(gebi('control_implementation_date_string').value);
            gebi('control_implementation_date').value = gebi('control_implementation_date_aux').value;
        }
        function set_user(id, name){
          gebi('responsible_id').value = id;
          gobi('control_responsible').setValue(name);
          id = name = null;
        }
        function setBestPracticeIds(psBestPracticesIds){
          gebi('control_model_ids').value = psBestPracticesIds;
          psBestPracticesIds=null;
          trigger_event('sel_best_practice',3);
        }
        function setControlCostCode(psControlCostCode){
          gebi('control_cost_code').value = psControlCostCode;
          psControlCostCode = null;
        }
        function setControlCostValue(psControlCostValue) {
          if(gebi('st_cost')) 
             gobi('st_cost').setValue(format_currency(psControlCostValue));
        }
        function uncheckCheckBox(psControllerId,psKey){
          var foController = gebi(psControllerId);
          var foImg = gebi(psControllerId+'_'+psKey);
          var fiPos = foImg.src.lastIndexOf('/');
          var fsDir = foImg.src.substr(0,fiPos+1);
          fsNewImgSrc = fsDir+'uncheck.gif';
          foImg.src = fsNewImgSrc;
          foControler = foImg = fiPos = fsDir = fsNewImgrc = null;
        }
        
        function set_test(psSensitiveFields,psFields){
          gebi('var_test_sensitive').value = psSensitiveFields;
          gebi('var_test').value = psFields;
        }
        
        function set_revision(psSensitiveFields,psFields){
          gebi('var_revision_sensitive').value = psSensitiveFields;
          gebi('var_revision').value = psFields;
        }
        
      </script>
    <?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_control_edit.xml");

?>