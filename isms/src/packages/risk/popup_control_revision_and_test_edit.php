<?php

include_once "include.php";
include_once $handlers_ref . "grid/QueryGridEditRevisionAndTest.php";

include_once $handlers_ref . "grid/QueryGridControlTestHistory.php";
include_once $handlers_ref . "grid/QueryGridControlRevisionHistory.php";

class GridControl extends FWDDrawGrid {
  public function DrawItem(){
    switch($this->ciColumnIndex){
      case $this->getIndexByAlias('control_id'):
        /*menu de contexto*/
        $maMenuACLs = array('revision','test');
        $maAllowed = array();
        if($this->getFieldValue('has_revision')){
          $maAllowed[] = 'revision';
        }
        if($this->getFieldValue('has_test')){
          $maAllowed[] = 'test';
        }
        $moACL = FWDACLSecurity::getInstance();
        $moACL->setNotAllowed(array_diff($maMenuACLs,$maAllowed));
        $moMenu = FWDWebLib::getObject('menu');
        $moGrid = FWDWebLib::getObject('grid_control');
        $msLine = $moGrid->getAttrName().'_acl_'.$this->ciRowIndex;
        FWDGridMenuACLSecurity::getInstance()->installGridMenuACL($moMenu,$moACL,$moGrid,$msLine);
      
        $msGfxRef = FWDWebLib::getInstance()->getGfxRef();
        $moIcon = new FWDIcon(new FWDBox(2,4));
        if($this->getFieldValue("control_active")==1)
          $moIcon->setAttrSrc("{$msGfxRef}icon-control.gif");
        else
          $moIcon->setAttrSrc("{$msGfxRef}icon-control_red.gif");
        return $moIcon->draw();
      break;
      case $this->getIndexByAlias('control_name'):
        $this->coCellBox->setAttrStringNoEscape("true");
        $this->coCellBox->setValue("<a href='javascript:open_visualize(".$this->caData[1].",".CONTEXT_CONTROL.",\"".uniqid()."\")'>{$this->caData[2]}</a>");
        return $this->coCellBox->draw();
        break;
      default:
        return parent::drawItem();
      break;
    }
  }
}

class GridControlRevisionHistory extends FWDDrawGrid {
  public function DrawItem(){
    switch($this->ciColumnIndex){
      case $this->getIndexByAlias('date_to_do'):
        $this->coCellBox->setValue(ISMSLib::getISMSShortDate($this->getFieldValue('date_to_do')));
      return parent::drawItem();
      break;
      case $this->getIndexByAlias('date_realized'):
        $this->coCellBox->setValue(ISMSLib::getISMSShortDate($this->getFieldValue('date_realized')));
        return parent::drawItem();
      break;
      case $this->getIndexByAlias('real_efficiency'):

        return parent::drawItem();
      break;
      case $this->getIndexByAlias('expected_efficiency'):

        return parent::drawItem();
      break;
      default:
        return parent::drawItem();
      break;
    }
  }
}

class GridControlTestHistory extends FWDDrawGrid {
  public function DrawItem(){
    switch($this->ciColumnIndex){
      case $this->getIndexByAlias('date_to_do'):
        $this->coCellBox->setValue(ISMSLib::getISMSShortDate($this->getFieldValue('date_to_do')));
        return parent::drawItem();
      break;
      case $this->getIndexByAlias('date_realized'):
        $this->coCellBox->setValue(ISMSLib::getISMSShortDate($this->getFieldValue('date_realized')));
        return parent::drawItem();
      break;
      case $this->getIndexByAlias('test_value'):
        if($this->coCellBox->getValue()==1){
          $this->coCellBox->setValue(FWDLanguage::getPHPStringValue('st_control_test_ok','Ok'));
        }else{
          $this->coCellBox->setValue(FWDLanguage::getPHPStringValue('st_control_test_not_ok','N�o Ok'));
        }
        return parent::drawItem();
      break;
      default:
        return parent::drawItem();
      break;
    }
  }
}

class SearchControlEvent extends FWDRunnable {
  public function run() {
    FWDWebLib::getObject('grid_control')->setAttrPopulate("true");
    FWDWebLib::getObject('grid_control')->execEventPopulate();
  }
}

class RevisionControlEvent extends FWDRunnable {
  public function run() {
    FWDWebLib::getObject('grid_revision_history')->setAttrPopulate("true");
    FWDWebLib::getObject('grid_revision_history')->execEventPopulate();
  }
}

class TestControlEvent extends FWDRunnable {
  public function run() {
    FWDWebLib::getObject('grid_test_history')->setAttrPopulate("true");
    FWDWebLib::getObject('grid_test_history')->execEventPopulate();
  }
}

class RevisionConfirmRemove extends FWDRunnable {
  public function run(){
    $maValues = explode(':',FWDWebLib::getObject('var_revision_test_ids')->getValue());
    $moControlRevisionHistory = new RMControlEfficiencyHistory();
    $moControlRevisionHistory->createFilter($maValues[1].':'.$maValues[2],'control_date_todo','=');
    $moControlRevisionHistory->createFilter($maValues[0],'control_id','=');
    $moControlRevisionHistory->select();
    if($moControlRevisionHistory->fetch()){
      $msTitle = FWDLanguage::getPHPStringValue('tt_remove_control','Remover Revis�o do Controle');
      $msMessage = FWDLanguage::getPHPStringValue('st_revision_remove_confirm',"Voc� tem certeza de que deseja remover a revis�o com Data Prevista em <b>%deadline_date%</b> e Data de Realiza��o em <b>%realized_date%</b> ?");
      
      $msMessage = str_replace('%deadline_date%', ISMSLib::getISMSDate($moControlRevisionHistory->getFieldValue('control_date_todo')), $msMessage);
      $msMessage = str_replace('%realized_date%', ISMSLib::getISMSDate($moControlRevisionHistory->getFieldValue('control_date_realized')), $msMessage);
      
      $msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
                      "soWindow = soPopUp.getOpener();" .
                      "soWindow.remove_revision();";
      ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,60);
    }
  }
}

class RemoveRevisionEvent extends FWDRunnable {
  public function run(){
    $maValues = explode(':',FWDWebLib::getObject('var_revision_test_ids')->getValue());
    $moControlRevisionHistory = new RMControlEfficiencyHistory();
    $moControlRevisionHistory->createFilter($maValues[1].':'.$maValues[2],'control_date_todo','=');
    $moControlRevisionHistory->createFilter($maValues[0],'control_id','=');
    $moControlRevisionHistory->delete($maValues[0]);
  }
}


class TestConfirmRemove extends FWDRunnable {
  public function run(){
    $maValues = explode(':',FWDWebLib::getObject('var_revision_test_ids')->getValue());
    $moControlTestHistory = new RMControlTestHistory();
    $moControlTestHistory->createFilter($maValues[1].':'.$maValues[2],'control_date_todo','=');
    $moControlTestHistory->createFilter($maValues[0],'control_id','=');
    $moControlTestHistory->select();
    if($moControlTestHistory->fetch()){
      $msTitle = FWDLanguage::getPHPStringValue('tt_remove_control','Remover Teste do Controle');
      $msMessage = FWDLanguage::getPHPStringValue('st_test_remove_confirm',"Voc� tem certeza de que deseja remover o teste com Data Prevista em <b>%deadline_date%</b> e Data de Realiza��o em <b>%realized_date%</b> ?");
      
      $msMessage = str_replace('%deadline_date%', ISMSLib::getISMSDate($moControlTestHistory->getFieldValue('control_date_todo')), $msMessage);
      $msMessage = str_replace('%realized_date%', ISMSLib::getISMSDate($moControlTestHistory->getFieldValue('control_date_realized')), $msMessage);
      
      $msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
                      "soWindow = soPopUp.getOpener();" .
                      "soWindow.remove_test();";
      ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,60);
    }
  }
}

class RemoveTestEvent extends FWDRunnable {
  public function run(){
    $maValues = explode(':',FWDWebLib::getObject('var_revision_test_ids')->getValue());
    $moControlTestHistory = new RMControlTestHistory();
    $moControlTestHistory->createFilter($maValues[1].':'.$maValues[2],'control_date_todo','=');
    $moControlTestHistory->createFilter($maValues[0],'control_id','=');
    $moControlTestHistory->delete($maValues[0]);
  }
}

class RevisionEdit extends FWDRunnable {
  public function run(){
    $maValues = explode(':',FWDWebLib::getObject('var_revision_test_ids')->getValue());
    $msReturn = "";
    $msExpectedEff = "";
    $moRevision = new WKFControlEfficiency();
    $moRevision->fetchById($maValues[0]);
    if(count($maValues)>=6){
      $msReturn .=" gebi('date_realized').value ='". ISMSLib::getISMSShortDate($maValues[4] . ':' . $maValues[5] . ':' . $maValues[6])."'; 
                    gebi('date_to_do').value = '".ISMSLib::getISMSShortDate($maValues[1] . ':' . $maValues[2] . ':' . $maValues[3])."';
      ";
    }else{
      $msExpectedEff .= " gfx_radio('controller_ee',".$moRevision->getFieldValue('expected_efficiency').");";
    }
    $msReturn .= "
      gobi('tx_5').setValue('".addslashes($moRevision->getFieldValue('value_5'))."');
      gobi('tx_4').setValue('".addslashes($moRevision->getFieldValue('value_4'))."');
      gobi('tx_3').setValue('".addslashes($moRevision->getFieldValue('value_3'))."');
      gobi('tx_2').setValue('".addslashes($moRevision->getFieldValue('value_2'))."');
      gobi('tx_1').setValue('".addslashes($moRevision->getFieldValue('value_1'))."');
      gebi('pn_revision_edit').style.display='block';
    ";
    echo $msExpectedEff . $msReturn;
  }
}

class ResivionUpdateByInsert extends FWDRunnable {
  public function run(){
    $moControlRevisionHistory = new RMControlEfficiencyHistory();
    $maValues = explode(':',FWDWebLib::getObject('var_revision_test_ids')->getValue());
    
    $moControlRevisionHistory->createFilter(FWDWebLib::getObject('date_to_do')->getTimestamp(),'control_date_todo','=');
    $moControlRevisionHistory->createFilter($maValues[0],'control_id','=');

    $moControlRevisionHistory->setFieldValue('control_date_realized',FWDWebLib::getObject('date_realized')->getTimestamp());
    $moControlRevisionHistory->setFieldValue('control_date_todo',FWDWebLib::getObject('date_to_do')->getTimestamp());
    $moControlRevisionHistory->setFieldValue('control_real_eff',str_replace(':','',FWDWebLib::getObject('controller_er')->getValue()));
    
    $moControlRevisionHistory->update($maValues[0]);

    echo "
      gobi('grid_revision_history').refresh();
      gebi('pn_revision_edit').style.display='none';
      gebi('pn_edit').style.display='none';
      gobi('vb_search').show();
    ";
  }
}

class ResivionSave extends FWDRunnable {
  public function run(){
    $maValues = explode(':',FWDWebLib::getObject('var_revision_test_ids')->getValue());
    $moControlRevisionHistory = new RMControlEfficiencyHistory();
    /*indica edi��o do hist�rico*/
    if(isset($maValues[1]) && isset($maValues[2])){
      $moControlRevisionHistory->createFilter($maValues[1].':'.$maValues[2],'control_date_todo','=');
      $moControlRevisionHistory->createFilter($maValues[0],'control_id','=');
    }else{
      $moControlRevHistoryTest = new RMControlEfficiencyHistory();
      $moControlRevHistoryTest->createFilter(FWDWebLib::getObject('date_to_do')->getTimestamp(),'control_date_todo','=');
      $moControlRevHistoryTest->createFilter($maValues[0],'control_id','=');
      $moControlRevHistoryTest->select();
      if($moControlRevHistoryTest->fetch()){
        echo "trigger_event('revision_edit_by_insert',3);";
        exit();
      }
    }
    $moControlRevisionHistory->setFieldValue('control_date_realized',FWDWebLib::getObject('date_realized')->getTimestamp());
    $moControlRevisionHistory->setFieldValue('control_date_todo',FWDWebLib::getObject('date_to_do')->getTimestamp());
    $moControlRevisionHistory->setFieldValue('control_real_eff',str_replace(':','',FWDWebLib::getObject('controller_er')->getValue()));
    
    if(isset($maValues[1]) && isset($maValues[2])){
      $moControlRevisionHistory->update($maValues[0]);
    }else{
      $moControlRevisionHistory->setFieldValue('control_expected_eff',str_replace(':','',FWDWebLib::getObject('controller_ee')->getValue()));
      $moControlRevisionHistory->setFieldValue('control_id',$maValues[0]);
      $moControlRevisionHistory->insert($maValues[0]);
    }
    echo "
      gobi('grid_revision_history').refresh();
      gebi('pn_revision_edit').style.display='none';
      gebi('pn_edit').style.display='none';
      gobi('vb_search').show();
    ";
  }
}

class RevisionEditByInsertConfirm extends FWDRunnable {
  public function run(){
    $msTitle = FWDLanguage::getPHPStringValue('tt_replace_control_revision','Substitui��o de Revis�o do Controle <b>%control_name%</b>');
    $msTitle = str_replace('%control_name%', FWDWebLib::getObject('var_control_name_selected')->getValue(),$msTitle);
    $msMessage = FWDLanguage::getPHPStringValue('st_revision_update_by_insert_confirm',"J� existe uma revis�o de controle na Data Prevista em <b>%date_to_do%</b>. Voc� tem certeza que deseja substituir essa revis�o do controle ?");
    $msMessage = str_replace('%date_to_do%', ISMSLib::getISMSShortDate(FWDWebLib::getObject('date_to_do')->getTimestamp(),true), $msMessage);

    $msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
                    "soWindow = soPopUp.getOpener();" .
                    "soWindow.update_by_insert();";
    ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,70);
  }
}

class TestEdit extends FWDRunnable {
  public function run(){
    $maValues = explode(':',FWDWebLib::getObject('var_revision_test_ids')->getValue());
    $msReturn = "";
    if(count($maValues)>=6){
      $msReturn .=" gebi('date_realized_test').value ='". ISMSLib::getISMSShortDate($maValues[4] . ':' . $maValues[5] . ':' . $maValues[6])."'; 
                    gebi('date_to_do_test').value = '".ISMSLib::getISMSShortDate($maValues[1] . ':' . $maValues[2] . ':' . $maValues[3])."';
      ";
    }
    echo $msReturn . " gebi('pn_test_edit').style.display='block'; ";
  }
}

class TestSave extends FWDRunnable {
  public function run(){
    $maValues = explode(':',FWDWebLib::getObject('var_revision_test_ids')->getValue());
    $moControlTestHistory = new RMControlTestHistory();
    /*indica edi��o do hist�rico*/
    if(isset($maValues[1]) && isset($maValues[2])){
      $moControlTestHistory->createFilter($maValues[1].':'.$maValues[2],'control_date_todo','=');
      $moControlTestHistory->createFilter($maValues[0],'control_id','=');
    }else{
      $moControlTestHistoryAux = new RMControlTestHistory();
      $moControlTestHistoryAux->createFilter(FWDWebLib::getObject('date_to_do_test')->getTimestamp(),'control_date_todo','=');
      $moControlTestHistoryAux->createFilter($maValues[0],'control_id','=');
      $moControlTestHistoryAux->select();
      if($moControlTestHistoryAux->fetch()){
        echo "trigger_event('test_edit_by_insert',3);";
        exit();
      }
    }
    $moControlTestHistory->setFieldValue('control_date_realized',FWDWebLib::getObject('date_realized_test')->getTimestamp());
    $moControlTestHistory->setFieldValue('control_date_todo',FWDWebLib::getObject('date_to_do_test')->getTimestamp());
    $moControlTestHistory->setFieldValue('control_test_value',str_replace(':','',FWDWebLib::getObject('controller_test')->getValue()));
    $moControlTestHistory->setFieldValue('control_test_description',FWDWebLib::getObject('test_description')->getValue());
    
    if(isset($maValues[1]) && isset($maValues[2])){
      $moControlTestHistory->update($maValues[0]);
    }else{
      $moControlTestHistory->setFieldValue('control_id',$maValues[0]);
      $moControlTestHistory->insert($maValues[0]);
    }
    echo "
      gobi('grid_test_history').refresh();
      gebi('pn_test_edit').style.display='none';
      gebi('pn_edit').style.display='none';
      gobi('vb_search').show();
    ";
  }
}

class TestUpdateByInsert extends FWDRunnable {
  public function run(){
    $moControlTestHistory = new RMControlTestHistory();
    $maValues = explode(':',FWDWebLib::getObject('var_revision_test_ids')->getValue());
    
    $moControlTestHistory->createFilter(FWDWebLib::getObject('date_to_do_test')->getTimestamp(),'control_date_todo','=');
    $moControlTestHistory->createFilter($maValues[0],'control_id','=');

    $moControlTestHistory->setFieldValue('control_date_realized',FWDWebLib::getObject('date_realized_test')->getTimestamp());
    $moControlTestHistory->setFieldValue('control_date_todo',FWDWebLib::getObject('date_to_do_test')->getTimestamp());
    $moControlTestHistory->setFieldValue('control_test_description',FWDWebLib::getObject('test_description')->getValue());
    $moControlTestHistory->setFieldValue('control_test_value',str_replace(':','',FWDWebLib::getObject('controller_test')->getValue()));
    
    $moControlTestHistory->update($maValues[0]);

    echo "
      gobi('grid_test_history').refresh();
      gebi('pn_test_edit').style.display='none';
      gebi('pn_edit').style.display='none';
      gobi('vb_search').show();
    ";
  }
}

class TestEditByInsertConfirm extends FWDRunnable {
  public function run(){
    $msTitle = FWDLanguage::getPHPStringValue('tt_replace_control_test','Substitui��o de Teste do Controle <b>%control_name%</b>');
    $msTitle = str_replace('%control_name%', FWDWebLib::getObject('var_control_name_selected')->getValue(),$msTitle);
    $msMessage = FWDLanguage::getPHPStringValue('st_test_update_by_insert_confirm',"J� existe um teste de controle na Data Prevista em <b>%date_to_do%</b>. Voc� tem certeza que deseja substituir esse teste do controle ?");
    $msMessage = str_replace('%date_to_do%', ISMSLib::getISMSShortDate(FWDWebLib::getObject('date_to_do_test')->getTimestamp(),true), $msMessage);

    $msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
                    "soWindow = soPopUp.getOpener();" .
                    "soWindow.update_test_by_insert();";
    ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,70);
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new SearchControlEvent("search_control_event"));
    $moStartEvent->addAjaxEvent(new RevisionControlEvent("grid_revision_event"));
    $moStartEvent->addAjaxEvent(new TestControlEvent("grid_test_event"));
    
    $moStartEvent->addAjaxEvent(new RevisionConfirmRemove("revision_confirm_remove"));
    $moStartEvent->addAjaxEvent(new RemoveRevisionEvent("revision_remove"));
    $moStartEvent->addAjaxEvent(new TestConfirmRemove("test_confirm_remove"));
    $moStartEvent->addAjaxEvent(new RemoveTestEvent("test_remove"));
    
    $moStartEvent->addAjaxEvent(new RevisionEdit("revision_edit"));
    $moStartEvent->addAjaxEvent(new ResivionSave("revision_save"));
    $moStartEvent->addAjaxEvent(new TestEdit("test_edit"));
    $moStartEvent->addAjaxEvent(new TestSave("test_save"));
    $moStartEvent->addAjaxEvent(new RevisionEditByInsertConfirm("revision_edit_by_insert"));
    $moStartEvent->addAjaxEvent(new ResivionUpdateByInsert("revision_update_by_insert"));
    $moStartEvent->addAjaxEvent(new TestEditByInsertConfirm("test_edit_by_insert"));
    $moStartEvent->addAjaxEvent(new TestUpdateByInsert("test_update_by_insert"));

    $moGrid = FWDWebLib::getObject("grid_control");
    $moHandler = new QueryGridEditRevisionAndTest(FWDWebLib::getConnection());
    $moHandler->setName(FWDWebLib::getObject('var_control_name')->getValue());
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new GridControl());
    
    $moGridTest = FWDWebLib::getObject('grid_test_history');
    $moHandlerTest = new QueryGridControlTestHistory(FWDWebLib::getConnection());
    $moHandlerTest->setId(FWDWebLib::getObject('control_test_revision_id')->getValue());
    $moGridTest->setQueryHandler($moHandlerTest);
    $moGridTest->setObjFWDDrawGrid(new GridControlTestHistory());
    
    $moGridRevision = FWDWebLib::getObject('grid_revision_history');
    $moHandlerRevision = new QueryGridControlRevisionHistory(FWDWebLib::getConnection());
    $moHandlerRevision->setId(FWDWebLib::getObject('control_test_revision_id')->getValue());
    $moGridRevision->setQueryHandler($moHandlerRevision);
    $moGridRevision->setObjFWDDrawGrid(new GridControlRevisionHistory());
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    FWDWebLib::getObject('var_original_title_revision_add')->setValue(FWDLanguage::getPHPStringValue('tt_control_efficiency_history_add_title','Inserir Revis�o do Controle'));
    FWDWebLib::getObject('var_original_title_test_add')->setValue(FWDLanguage::getPHPStringValue('tt_control_test_history_add_title','Inserir Teste do Controle'));
    
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
?>
<script language="javascript">
  gebi('var_original_title_revision').value = gobi('st_control_revision_name').getValue();
  gebi('var_original_title_test').value = gobi('st_control_test_name').getValue();
  gebi('var_original_title_revision_edit').value = gobi('st_edit_revision_control').getValue();
  gebi('var_original_title_test_edit').value = gobi('st_edit_test_control').getValue();
  
  function refresh_grid() {
    js_refresh_grid('grid_control');
  }
  
  function revision_exec(){
    gebi('pn_revision').style.display='block';
    gebi('pn_test').style.display='none';
    var maValues = FWDMenu.value.split(':');
    gebi('control_test_revision_id').value = maValues[0];
    gobi('st_control_revision_name').setValue(gebi('var_original_title_revision').value + '<b> (' + maValues[1]+')</b>');
    gebi('var_control_name_selected').value = maValues[1];
    gobi('grid_revision_history').setPopulate(true);
    trigger_event('grid_revision_event',3);
  }
  
  function test_exec(){
    gebi('pn_revision').style.display='none';
    gebi('pn_test').style.display='block';
    var maValues = FWDMenu.value.split(':');
    gebi('control_test_revision_id').value = maValues[0];
    gobi('st_control_test_name').setValue(gebi('var_original_title_test').value + '<b> (' + maValues[1]+')</b>');
    gebi('var_control_name_selected').value = maValues[1];
    gobi('grid_test_history').setPopulate(true);
    trigger_event('grid_test_event',3);
  }

  function edit_revision(){
    trigger_event('revision_edit',3);
    gobi('vb_search').hide();
    gebi('var_revision_test_ids').value = FWDMenu.value;
    var maValues = FWDMenu.value.split(':');
    gobi('st_edit_revision_control').setValue(gebi('var_original_title_revision_edit').value + '<b> (' + gebi('var_control_name_selected').value +')</b>');
    gfx_radio('controller_ee',maValues[7]);
    gfx_radio('controller_er',maValues[8]);
    gebi('pn_test_edit').style.display='none';
    gebi('pn_edit').style.display='block';
  }
  
  function edit_test(){
    trigger_event('test_edit',3);
    gobi('vb_search').hide();
    gebi('var_revision_test_ids').value = FWDMenu.value;
    var maValues = FWDMenu.value.split(':');
    gobi('st_edit_test_control').setValue(gebi('var_original_title_test_edit').value + '<b> (' + gebi('var_control_name_selected').value +')</b>');
    gfx_radio('controller_test',maValues[7]);
    if(maValues[8])
      gebi('test_description').value = maValues[8];
    gebi('pn_revision_edit').style.display='none';
    gebi('pn_edit').style.display='block';
  }

  function insert_revision(){
    if(FWDMenu.value){
      var maValues = FWDMenu.value.split(':');
      gebi('var_revision_test_ids').value = maValues[0]+':';
      trigger_event('revision_edit',3);
      gobi('vb_search').hide();
      uncheck_radio('controller_er');
      gebi('date_realized').value = '';
      gebi('date_to_do').value = '';
      gobi('st_edit_revision_control').setValue(gebi('var_original_title_revision_add').value + '<b> (' + gebi('var_control_name_selected').value +')</b>');
      gebi('pn_edit').style.display='block';
    }
  }

  function insert_test(){
    if(FWDMenu.value){
      gobi('vb_search').hide();
      var maValues = FWDMenu.value.split(':');
      gebi('var_revision_test_ids').value = maValues[0]+':';
      uncheck_radio('controller_test');
      gebi('date_realized_test').value = '';
      gebi('date_to_do_test').value = '';
      gebi('test_description').value = '';
      gobi('st_edit_test_control').setValue(gebi('var_original_title_test_add').value + '<b> (' + gebi('var_control_name_selected').value +')</b>');
      gebi('pn_revision_edit').style.display='none';
      gebi('pn_edit').style.display='block';
      gebi('pn_test_edit').style.display='block';
    }
  }

  function update_by_insert(){
    trigger_event('revision_update_by_insert',3);
  }
  
  function update_test_by_insert(){
    trigger_event('test_update_by_insert',3);
  }
  
  function remove_revision(){
    trigger_event('revision_remove',3);
    gobi('grid_revision_history').refresh();
  }
  
  function remove_test(){
    trigger_event('test_remove',3);
    gobi('grid_test_history').refresh();
  }
  
  </script>
<?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_control_revision_and_test_edit.xml");
?>