<?php
include_once "include.php";
include_once $handlers_ref . "grid/QueryGridRiskControl.php";

class UpdateRiskValuesEvent extends FWDRunnable {
  public function run(){
    $moRisk = new RMRisk();
    $moRisk->fetchById(FWDWebLib::getObject("risk_id")->getValue());
    $miRiskValue = $moRisk->getFieldValue("risk_value");
    $miRiskResidualValue = $moRisk->getFieldValue("risk_residual_value");
    
    $msRiskColor = RMRiskConfig::getRiskColor($miRiskValue,true);
    $msRiskResidualColor = RMRiskConfig::getRiskColor($miRiskResidualValue,true);
    
    $miRiskValue = RMRiskConfig::getRiskValueFormat($miRiskValue);
    $miRiskResidualValue = RMRiskConfig::getRiskValueFormat($miRiskResidualValue);
    
    echo "gobi('risk_value').setValue('<font color=\'$msRiskColor\' size=\'4\'>".$miRiskValue."</font>');";
    echo "gobi('risk_residual_value').setValue('<font color=\'$msRiskResidualColor\' size=\'4\'>".$miRiskResidualValue."</font>');";
  }
}

class DissociateControlsEvent extends FWDRunnable {
  public function run(){
    //teste para verificar se o sistema n�o est� sendo hakeado
    $miRiskId = FWDWebLib::getObject("param_risk_id")->getValue();
    $moCtxUserTest = new RMRiskControl();
    $moCtxResponsibleTest = new RMRisk();
    $moCtxResponsibleTest->fetchById($miRiskId);
    $moCtxUserTest->testPermissionToEdit($miRiskId,$moCtxResponsibleTest->getResponsibleToACLPermition());
    
    $maGridValues = FWDWebLib::getObject("grid_risk_control")->getValue();
    
    $mbHasPMModule = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(POLICY_MODE) ? true : false;
    
    $moRiskControl = new RMRiskControl();
    foreach ($maGridValues as $maGridValue) {
      $moRiskControl->delete($maGridValue[1],true);
      /*
       * Se possuir o m�dulo de documenta��o, deve atualizar os leitores
       * do documento do controle que foi desassociado.
       */
      if ($mbHasPMModule) {
        $moControl = new RMControl();
        $moControl->updateReaders($maGridValue[3]);
      }
    }
    
    echo "soPopUpManager.getPopUpById('popup_risk_control_edit').getWindow().refresh_grid();" .
         "soPopUpManager.getPopUpById('popup_risk_control_edit').getOpener().refresh_grid();";
  }
}

class GridRiskControl extends FWDDrawGrid {
  public function drawItem(){
    switch($this->ciColumnIndex){
      case 1:
        $msGfxRef = FWDWebLib::getInstance()->getGfxRef();
        $moIcon = new FWDIcon(new FWDBox(2,4));
        if($this->caData[4]==1)
        	$moIcon->setAttrSrc("{$msGfxRef}icon-control.gif");
        else
        	$moIcon->setAttrSrc("{$msGfxRef}icon-control_red.gif");
        return $moIcon->draw();
        break;
      
      case 2:
        $this->coCellBox->setAttrStringNoEscape("true");
        $this->coCellBox->setValue("<a href='javascript:open_visualize(".$this->caData[3].",".CONTEXT_CONTROL.",\"".uniqid()."\")'>{$this->caData[2]}</a>");
        return $this->coCellBox->draw();
        break;
      
      default:
        return parent::drawItem();
        break;
    }
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new UpdateRiskValuesEvent("update_risk_values_event"));
    $moStartEvent->addAjaxEvent(new DissociateControlsEvent("dissociate_controls_event"));
    
    $moWebLib = FWDWebLib::getInstance();
    
    $moGrid = FWDWebLib::getObject("grid_risk_control");
    $moHandler = new QueryGridRiskControl(FWDWebLib::getConnection());
    $moHandler->setRiskId(FWDWebLib::getObject("risk_id")->getValue());
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new GridRiskControl());
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $miRiskId = $moWebLib->getObject("param_risk_id")->getValue();
    
    //teste para verificar se o sistema n�o est� sendo hakeado
    $moCtxUserTest = new RMRiskControl();
    $moCtxUserTest->testPermissionToEdit($miRiskId);
    
    $moWindowTitle = $moWebLib->getObject("window_title");
    if($miRiskId){
      $moWindowTitle->setValue(FWDLanguage::getPHPStringValue("tt_control_risk_association_editing", "Edi��o de Associa��o Risco-Controle"));
      
      FWDWebLib::getObject("risk_id")->setValue($miRiskId);
      
      $moRisk = new RMRisk();
      $moAsset = new RMAsset();
      $moRisk->fetchById($miRiskId);
      $moAsset->fetchById($moRisk->getFieldValue("risk_asset_id"));
      
      FWDWebLib::getObject("risk_name")->setValue($moRisk->getFieldValue("risk_name"));
      FWDWebLib::getObject("asset_name")->setValue($moAsset->getFieldValue("asset_name"));
      
      $miRiskValue = $moRisk->getFieldValue("risk_value");
      $miRiskResidualValue = $moRisk->getFieldValue("risk_residual_value");
      
      $msRiskColor = RMRiskConfig::getRiskColor($miRiskValue,true);
      $msRiskResidualColor = RMRiskConfig::getRiskColor($miRiskResidualValue,true);
      
      $miRiskValue = RMRiskConfig::getRiskValueFormat($miRiskValue);
      $miRiskResidualValue = RMRiskConfig::getRiskValueFormat($miRiskResidualValue);
      
      FWDWebLib::getObject("risk_value")->getObjFWDString()->setAttrNoEscape("true");
      FWDWebLib::getObject("risk_value")->setValue("<font color='$msRiskColor' size='4'>$miRiskValue</font>");
      FWDWebLib::getObject("risk_residual_value")->getObjFWDString()->setAttrNoEscape("true");
      FWDWebLib::getObject("risk_residual_value")->setValue("<font color='$msRiskResidualColor' size='4'>$miRiskResidualValue</font>");
    }
    FWDWebLib::getInstance()->dump_html($moWebLib->getObject('dialog'));
?>
<script language="javascript">
  function refresh_grid() {
    js_refresh_grid('grid_risk_control');   
  }
  
  
  function update_risk_values() {
    trigger_event('update_risk_values_event','3');
  }
</script>
<?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_risk_control_edit.xml');
?>