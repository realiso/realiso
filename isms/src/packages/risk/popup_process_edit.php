<?php

include_once 'include.php';
include_once $handlers_ref . "QueryContextHierarchicalStructure.php";
include_once $handlers_ref . "select/QuerySelectProcessType.php";
include_once $handlers_ref . "select/QuerySelectProcessPriority.php";
include_once $handlers_ref . "select/QuerySelectReadableDocumentByContext.php";

function getProcessFromFields(){
	$moProcess = new RMProcess();

	$msProcessName = FWDWebLib::getObject('process_name')->getValue();
	$miProcessResponsible = FWDWebLib::getObject('responsible_id')->getValue();
	$miArea = FWDWebLib::getObject('select_area')->getValue();
	$msProcessDescription = FWDWebLib::getObject('process_description')->getValue();
	//$msProcessDocument = FWDWebLib::getObject('process_document')->getValue();
	$miProcessType = FWDWebLib::getObject('select_process_type')->getValue();
	$miProcessPriority = FWDWebLib::getObject('select_process_priority')->getValue();

	$moProcess->setFieldValue('process_name', $msProcessName);
	$moProcess->setFieldValue('process_responsible_id', $miProcessResponsible);
	$moProcess->setFieldValue('process_area_id', $miArea);
	$moProcess->setFieldValue('process_description', $msProcessDescription);
	//$moProcess->setFieldValue('process_document', $msProcessDocument);
	if($miProcessType)
	$moProcess->setFieldValue('process_type', $miProcessType);
	else
	$moProcess->setFieldValue('process_type', 'null');

	if($miProcessPriority)
	$moProcess->setFieldValue('process_priority', $miProcessPriority);
	else
	$moProcess->setFieldValue('process_priority', 'null');

	$mbHasPMModule = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(POLICY_MODE);
	if (!$mbHasPMModule) {
		$msProcessDocument = FWDWebLib::getObject('process_document')->getValue();
		$moProcess->setFieldValue('process_document', $msProcessDocument);
	}

	return $moProcess;
}

function save($poContext,$pbHasSensitiveChanges,$piContextId=0){
	$miId = $piContextId;
	if($miId){
		//teste para verificar se o sistema n�o est� sendo hakeado
		$moCtxUserTest = new RMProcess();
		$moCtxResponsibleTest = new RMProcess();
		$moCtxResponsibleTest->fetchById($piContextId);
		$moCtxUserTest->testPermissionToEdit($piContextId,$moCtxResponsibleTest->getResponsible());

		$poContext->update($miId,true,$pbHasSensitiveChanges);
	}else{
		$miId = $poContext->insert(true);
	}

	/*
	 * Se possui o m�dulo de documenta��o, verifica se ocorreu
	 * alguma mudan�a na associa��o dos usu�rios com o processo
	 * e, caso necess�rio, grava as altera��es.
	 *
	 * Atualiza tamb�m os aprovadores autom�ticos dos documentos
	 * associados a esse contexto.
	 */
	$mbHasPMModule = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(POLICY_MODE);
	if ($mbHasPMModule) {
		$moProcessUser = new PMProcessUser();
		$maInitialUserValues = explode(':', FWDWebLib::getObject('initial_users_ids')->getValue());
		$maCurrentUserValues = explode(':', FWDWebLib::getObject('current_users_ids')->getValue());
		unset($maInitialUserValues[0], $maCurrentUserValues[0]);
		$maInsertUser = array_unique(array_diff($maCurrentUserValues,$maInitialUserValues));
		$maDeleteUser = array_unique(array_diff($maInitialUserValues,$maCurrentUserValues));

		if(count($maInsertUser)) $moProcessUser->insertUsers($miId, $maInsertUser);
		if(count($maDeleteUser)) $moProcessUser->deleteUsers($miId, $maDeleteUser);
			
		/*
		 * Atualiza os leitores dos documentos dos elementos associados ao processo
		 * caso ocorra mudan�a nos usu�rios associados ao processo.
		 */
		if (count($maInsertUser) || count($maDeleteUser)) $poContext->updateReaders($miId);
			
		/*
		 * Caso o processo mude de �rea, deve-se atualizar os leitores
		 * dos documentos da �rea antiga e da nova �rea
		 */
		$miOldAreaId = FWDWebLib::getObject('current_area_id')->getValue();
		$miNewAreaId = $poContext->getFieldValue('process_area_id');
		if ($miOldAreaId && ($miOldAreaId != $miNewAreaId)) {
			$moArea = new RMArea();
			$moArea->updateReaders($miOldAreaId);
			$moArea->updateReaders($miNewAreaId);
		}

		/*
		 * Insere o respons�vel na tabela de usuarios do processo
		 * (se ele j� n�o estiver).
		 */
		$moPMProcessUser = new PMProcessUser();
		$miResponsibleId = FWDWebLib::getObject('responsible_id')->getValue();
		if ($miResponsibleId && !in_array($miResponsibleId, $maCurrentUserValues)) {
			
			$search = new PMProcessUser();
			$search->createFilter($miId, 'process_id');
			$search->createFilter($miResponsibleId, 'user_id');
			$search->select();
			if(!$search->fetch()){
				$moPMProcessUser->setFieldValue('process_id',$miId);
				$moPMProcessUser->setFieldValue('user_id',$miResponsibleId);
				$moPMProcessUser->insert();
			}
		}

		$moDocContext = new PMDocContext();
		$moDocContext->createFilter($miId,'context_id');
		$moDocContext->select();
		while($moDocContext->fetch()) {
			$moDocContext->updateAutoApprovers($moDocContext->getFieldValue('document_id'));
		}
	}

	echo "soWindow = soPopUpManager.getPopUpById('popup_process_edit').getOpener();"
	."if(soWindow.refresh_grid) soWindow.refresh_grid();"
	."soPopUpManager.closePopUp('popup_process_edit');";
}

class ConfirmEditEvent extends FWDRunnable {
	public function run(){
		$miProcessId = FWDWebLib::getObject('process_id')->getValue();
		$moProcess = getProcessFromFields();
		save($moProcess,true,$miProcessId);
	}
}

class SaveProcessEvent extends FWDRunnable {
	public function run(){
		$miProcessId = FWDWebLib::getObject('process_id')->getValue();
		$moProcess = getProcessFromFields();
		$moProcess->setHash(FWDWebLib::getObject('hash')->getValue());
		$mbHasSensitiveChanges = $moProcess->hasSensitiveChanges();
		$miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
		if($miProcessId && $mbHasSensitiveChanges && $miUserId != $moProcess->getApprover()){
			$msTitle = FWDLanguage::getPHPStringValue('tt_reapproval','Reaprova��o');
			$msMessage = FWDLanguage::getPHPStringValue('st_sensitive_data_reapproval','Voc� est� alterando dados sens�veis, que necessitam passar por uma reaprova��o. Deseja confirmar as altera��es?');
			$msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
                      "soWindow = soPopUp.getOpener();" .
                      "soWindow.trigger_event('confirm_edit',3);";
			ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,75);
		}else{
			save($moProcess,$mbHasSensitiveChanges,$miProcessId);
		}
	}
}


class DocumentViewEvent extends FWDRunnable {
	public function run() {
		$moWebLib = FWDWebLib::getInstance();
		$miDocumentId = $moWebLib->getObject('select_process_document')->getValue();
		$miUserId = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();

		$moDocument = new PMDocument();
		$moDocument->testPermissionToRead($miDocumentId);
		$moDocument->fetchById($miDocumentId);
		$moPMDocInstance = $moDocument->getPublishedInstance();
		$miDocInstanceId = $moPMDocInstance->getFieldValue('doc_instance_id');

		$moReader = new PMDocumentReader();
		$moReader->createFilter($miDocumentId,'document_id');
		$moReader->createFilter($miUserId,'user_id');
		$moReader->select();
		if ($moReader->fetch()) {
			$moReader->setFieldValue('user_has_read_document',1);
			$moReader->update();
		}

		$moPMDocReadHistory = new PMDocReadHistory();
		$moPMDocReadHistory->setFieldValue('user_id',$miUserId);
		$moPMDocReadHistory->setFieldValue('document_id',$miDocInstanceId);
		$moPMDocReadHistory->setFieldValue('read_date',ISMSLib::ISMSTime());
		$moPMDocReadHistory->insert();

		$mbIsLink = (bool)$moPMDocInstance->getFieldValue('doc_instance_is_link');
		if ($mbIsLink) {
			$msURL = $moPMDocInstance->getFieldValue('doc_instance_link');
			if ($msURL)
			echo "js_open('{$msURL}','document_link',740,502,'center_screen');";
		} else {
			echo "js_submit('download_file','ajax');";
		}
	}
}

class DownloadFileEvent extends FWDRunnable {
	public function run() {
		$moWebLib = FWDWebLib::getInstance();
		$miDocumentId = $moWebLib->getObject('select_process_document')->getValue();
		$miUserId = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();

		$moDocument = new PMDocument();
		$moDocument->testPermissionToRead($miDocumentId);
		$moDocument->fetchById($miDocumentId);
		$moPMDocInstance = $moDocument->getPublishedInstance();

		$msPath = $moWebLib->getSysRef()."packages/policy/".$moPMDocInstance->getFieldValue('doc_instance_path');
		$msFileName = $moPMDocInstance->getFieldValue('doc_instance_file_name');
		header('Cache-Control: ');// leave blank to avoid IE errors
		header('Pragma: ');// leave blank to avoid IE errors
		header('Content-type: application/octet-stream');
		header('Content-Disposition: attachment; filename="'.rawurlencode($msFileName).'"');
		$moCrypt = new FWDCrypt();
		$moFP = fopen($msPath,'rb');
		$moCrypt->setIV(fread($moFP,strlen($moCrypt->getIV())));
		while(!feof($moFP)) {
			echo $moCrypt->decryptNoBase64(fread($moFP,16384));
		}
		fclose($moFP);
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(''));
		$moStartEvent->addAjaxEvent(new SaveProcessEvent('save_process_event'));
		$moStartEvent->addAjaxEvent(new ConfirmEditEvent('confirm_edit'));
		$moStartEvent->addAjaxEvent(new DocumentViewEvent('document_view_event'));
		$moStartEvent->addSubmitEvent(new DownloadFileEvent('download_file'));
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		/*
		 * Verifica se possui o m�dulo de documenta��o para mostrar o bot�o de associar usu�rios.
		 */
		$mbHasPMModule = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(POLICY_MODE);
		if (!$mbHasPMModule){
			FWDWebLib::getObject('btn_associate_users')->setShouldDraw(false);
		}
		$moSelect = FWDWebLib::getObject('select_area');
		$moQuery = new QueryContextHierarchicalStructure(FWDWebLib::getConnection());
		$moQuery->setContextType(CONTEXT_AREA);
		$moQuery->makeQuery();
		$moQuery->executeQuery();
		foreach ($moQuery->getContexts() as $maContext) {
			list($miContextId,$msHierarchicalName) = $maContext;
			$moSelect->setItemValue($miContextId,$msHierarchicalName);
		}

		$miProcessId = FWDWebLib::getObject('param_process_id')->getValue();
		FWDWebLib::getObject('process_id')->setValue($miProcessId);
		$miAreaId = FWDWebLib::getObject('area_id')->getValue();
		$moWindowTitle = FWDWebLib::getObject('window_title');
		$moIcon  = FWDWebLib::getObject('tooltip_icon');

		/*
		 * Se possui o m�dulo de documenta��o, troca o campo texto
		 * por select, e mostra um bot�o para o usu�rio visualizar
		 * o documento selecionado. Se n�o houver documento associado,
		 * mostrar mensagem indicando tal situa��o.
		 */
		if ($mbHasPMModule) {
			if($miProcessId){
				FWDWebLib::getObject('document_without_policy')->setShouldDraw(false);
				FWDWebLib::getObject('document_with_policy')->setShouldDraw(true);

				$moHandler = new QuerySelectReadableDocumentByContext(FWDWebLib::getConnection());
				$moHandler->setContextId($miProcessId);
				$moSelect = FWDWebLib::getObject('select_process_document');
				$moSelect->setQueryHandler($moHandler);
				$moSelect->populate();

				if (!$moSelect->getValue()) {
					FWDWebLib::getObject('no_published_documents')->setValue(FWDLanguage::getPHPStringValue('st_no_published_documents','N�o h� documentos publicados'));
					FWDWebLib::getObject('no_published_documents')->setAttrDisplay('true');
					FWDWebLib::getObject('process_document_create')->setAttrDisplay('true');
					FWDWebLib::getObject('select_process_document')->setAttrDisplay('false');
					FWDWebLib::getObject('process_document_view')->setShouldDraw(false);
					FWDWebLib::getObject('process_document_properties')->setShouldDraw(false);
				}
			}else{
				FWDWebLib::getObject('document_without_policy')->setShouldDraw(false);
				FWDWebLib::getObject('document_with_policy')->setShouldDraw(false);
				FWDWebLib::getObject('st_process_document')->setShouldDraw(false);
			}
			/* Provisoriamente, como os documentos passaram a ser acessados
			 * a partir do menu de contexto, estou removendo a linha de
			 * documento da tela de edi��o
			 */
			FWDWebLib::getObject('st_process_document')->setShouldDraw(false);
			FWDWebLib::getObject('document_with_policy')->setShouldDraw(false);

		} else {
			FWDWebLib::getObject('document_without_policy')->setShouldDraw(true);
			FWDWebLib::getObject('document_with_policy')->setShouldDraw(false);
		}

		$moHandler = new QuerySelectProcessType(FWDWebLib::getConnection());
		$moSelect = FWDWebLib::getObject('select_process_type');
		$moSelect->setQueryHandler($moHandler);
		$moSelect->populate();

		$moHandler = new QuerySelectProcessPriority(FWDWebLib::getConnection());
		$moSelect = FWDWebLib::getObject('select_process_priority');
		$moSelect->setQueryHandler($moHandler);
		$moSelect->populate();

		if($miProcessId){
			$moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_process_editing', 'Edi��o de Processo'));

			$moProcess = new RMProcess();
			$moProcess->fetchById($miProcessId);
			FWDWebLib::getObject('process_name')->setValue($moProcess->getFieldValue('process_name'));
			$miResponsibleId = $moProcess->getFieldValue('process_responsible_id');

			//teste para verificar se o sistema n�o est� sendo hakeado
			$moCtxUserTest = new RMProcess();
			$moCtxUserTest->testPermissionToEdit($miProcessId,$miResponsibleId);

			$moUser = new ISMSUser();
			$moUser->fetchById($miResponsibleId);
			FWDWebLib::getObject('responsible_id')->setValue($miResponsibleId);
			FWDWebLib::getObject('initial_responsible_id')->setValue($miResponsibleId);
			FWDWebLib::getObject('select_area')->checkItem($moProcess->getFieldValue('process_area_id'));
			FWDWebLib::getObject('process_responsible')->setValue($moUser->getFieldValue('user_name'));
			FWDWebLib::getObject('process_document')->setValue($moProcess->getFieldValue('process_document'));
			FWDWebLib::getObject('process_description')->setValue($moProcess->getFieldValue('process_description'));
			FWDWebLib::getObject('select_process_type')->checkItem($moProcess->getFieldValue('process_type'));
			FWDWebLib::getObject('select_process_priority')->checkItem($moProcess->getFieldValue('process_priority'));
			FWDWebLib::getObject('hash')->setValue($moProcess->getHash());

			$moToolTip = ISMSLib::getInstance()->getToolTipCreateModifyById($miProcessId);
			$moToolTip->setAttrWidth(375);
			$moIcon->addObjFWDEvent($moToolTip);

			/*
			 * Se possui o m�dulo de documenta��o, armazena os ids dos
			 * usu�rios associados ao processo e da �rea atual.
			 */
			if ($mbHasPMModule) {
				$moProcessUser = new PMProcessUser();
				$moProcessUser->createFilter($miProcessId, 'process_id');
				$moProcessUser->select();
				$msUserIds = '';
				while($moProcessUser->fetch()) $msUserIds .= ':' . $moProcessUser->getFieldValue('user_id');
				FWDWebLib::getObject('initial_users_ids')->setValue($msUserIds);
				FWDWebLib::getObject('current_users_ids')->setValue($msUserIds);
				FWDWebLib::getObject('current_area_id')->setValue($moProcess->getFieldValue('process_area_id'));
			}
		}else{
			//teste para verificar se o sistema n�o est� sendo hakeado
			$moCtxUserTest = new RMProcess();
			$moCtxUserTest->testPermissionToInsert();

			// Na inser��o, n�o permite associar usu�rios para evitar tentar associar respons�vel 2x
			FWDWebLib::getObject('btn_associate_users')->setShouldDraw(false);

			$moIcon->setAttrDisplay('false');
			$moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
			$miUserId = $moSession->getUserId();
			$moUser = new ISMSUser();
			$moUser->fetchById($miUserId);
			FWDWebLib::getObject('responsible_id')->setValue($miUserId);
			FWDWebLib::getObject('initial_responsible_id')->setValue($miUserId);
			FWDWebLib::getObject('process_responsible')->setValue($moUser->getFieldValue('user_name'));

			if($miAreaId) FWDWebLib::getObject('select_area')->checkItem($miAreaId);

			$moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_process_adding', 'Adi��o de Processo'));
		}

		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
<script language="javascript">
        gebi('process_name').focus();
      
        function set_user(id, name){
          gebi('responsible_id').value = id;
          gobi('process_responsible').setValue(name);
        }
        
        function set_users(psIds) {
          gebi('current_users_ids').value = psIds;          
        }
      </script>
		<?
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_process_edit.xml');

?>