<?php
include_once "include.php";
include_once $handlers_ref . "grid/QueryGridControlSearch.php";

class GridRiskControlSearch extends FWDDrawGrid {
  public function DrawItem(){
    switch($this->ciColumnIndex){
      case 1:
        $msGfxRef = FWDWebLib::getInstance()->getGfxRef();
        $moIcon = new FWDIcon(new FWDBox(2,4));
        if($this->caData[3]==1)
        	$moIcon->setAttrSrc("{$msGfxRef}icon-control.gif");
        else
        	$moIcon->setAttrSrc("{$msGfxRef}icon-control_red.gif");
        return $moIcon->draw();
        break;
      
      case 2:
        $this->coCellBox->setAttrStringNoEscape("true");
        $this->coCellBox->setValue("<a href='javascript:open_visualize(".$this->caData[1].",".CONTEXT_CONTROL.",\"".uniqid()."\")'>{$this->caData[2]}</a>");
        return $this->coCellBox->draw();
        break;
      
      default:
        return parent::drawItem();
      break;
    }
  }
}

class SearchControlEvent extends FWDRunnable {
  public function run() {
    $moGrid = FWDWebLib::getObject('grid_control_search');
    $moGrid->execEventPopulate();
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new SearchControlEvent("search_control_event"));
    
    $miRiskId = FWDWebLib::getObject("param_risk_id")->getValue();
    
    FWDWebLib::getObject("risk_id")->setValue($miRiskId);
    
    $moGrid = FWDWebLib::getObject("grid_control_search");
    $moHandler = new QueryGridControlSearch(FWDWebLib::getConnection());
    $moHandler->setName(FWDWebLib::getObject("var_control_name")->getValue());
    $moHandler->setRiskId(FWDWebLib::getObject("param_risk_id")->getValue());
    $moHandler->setOnlySuggested(FWDWebLib::getObject("var_check_controller")->getValue()?true:false);
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new GridRiskControlSearch());
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    //teste para verificar se o sistema n�o est� sendo hakeado
    $moCtxUserTest = new RMRiskControl();
    $moCtxUserTest->testPermissionToEdit(FWDWebLib::getObject("risk_id")->getValue());
    
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
?>
<script language="javascript">
  gebi('control_name').focus();
  
  function refresh_grid() {
    gobi('grid_control_search').setPopulate(true);
    js_refresh_grid('grid_control_search');
  }
    function enter_risk_control_search_event(e)
	{
		if (!e) e = window.event;
		if(e['keyCode']==13) {
      gebi('var_control_name').value = gebi('control_name').value;
      gebi('var_check_controller').value = gebi('check_controller').value;
      gobi('grid_control_search').setPopulate(true);
	    trigger_event("search_control_event","3");
		}
	}
	FWDEventManager.addEvent(gebi('control_name'), 'keydown', enter_risk_control_search_event);
</script>
<?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_risk_control_search.xml");
?>