<?php
include_once "include.php";
include_once $handlers_ref . "grid/QueryGridRiskSearch.php";

class GridControlRiskSearch extends FWDDrawGrid {
  public function drawItem(){
    switch($this->ciColumnIndex){
      case 1:
        $msRiskColor = RMRiskConfig::getRiskColor($this->getFieldValue('risk_residual_value'));
        $msGfxRef = FWDWebLib::getInstance()->getGfxRef();
        $moIcon = new FWDIcon(new FWDBox(2,4));
        $moIcon->setAttrSrc("{$msGfxRef}icon-risk_".$msRiskColor.".gif");
        return $moIcon->draw();
        break;
      
      case $this->getIndexByAlias('risk_name'):
        $miRiskValue = $this->getFieldValue('risk_residual_value');
        $msLinkColor = RMRiskConfig::getRiskColor($miRiskValue,true);
        $msLink = "<font color=".$msLinkColor.">".$this->getFieldValue('risk_name')."</a>";
        $this->coCellBox->setAttrStringNoEscape("true");
        $this->coCellBox->setValue("<a href='javascript:open_visualize(".$this->getFieldValue('risk_id').",".CONTEXT_RISK.",\"".uniqid()."\")'>{$msLink}</a>");
        return $this->coCellBox->draw();
        
      default:
        return parent::drawItem();
        break;
    }
  }
}

class SearchRiskEvent extends FWDRunnable {
  public function run() {
    $moGrid = FWDWebLib::getObject('grid_risk_search');
    $moGrid->execEventPopulate();
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new SearchRiskEvent("search_risk_event"));
    
    $miControlId = FWDWebLib::getObject("param_control_id")->getValue();
    
    FWDWebLib::getObject("control_id")->setValue($miControlId);
    
    $moGrid = FWDWebLib::getObject("grid_risk_search");
    $moHandler = new QueryGridRiskSearch(FWDWebLib::getConnection());
    $moHandler->setName(FWDWebLib::getObject("var_risk_name")->getValue());
    $moHandler->setAssetName(FWDWebLib::getObject("var_asset_name")->getValue());
    $moHandler->setControlId(FWDWebLib::getObject("param_control_id")->getValue());
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new GridControlRiskSearch());
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    //teste para verificar se o sistema n�o est� sendo hakeado
    $moCtxUserTest = new RMRiskControl();
    $moCtxUserTest->testPermissionToInsert();
    
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        
        function refresh_grid(){
          gobi('grid_risk_search').setPopulate(true);
          js_refresh_grid('grid_risk_search');
        }
        
        function search(){
          gebi('var_risk_name').value = gebi('risk_name').value;
          gebi('var_asset_name').value = gebi('asset_name').value;
          gobi('grid_risk_search').setPopulate(true);
          trigger_event('search_risk_event',3);
        }
        
      </script>
    <?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_control_risk_search.xml");
?>