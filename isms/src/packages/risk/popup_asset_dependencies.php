<?php
include_once "include.php";
include_once $handlers_ref . "QueryAssetPossibleDependencies.php";
include_once $handlers_ref . "QueryAssetPossibleDependents.php";
include_once $handlers_ref . "QueryAssetDependencies.php";
include_once $handlers_ref . "QueryAssetDependents.php";
include_once $handlers_ref . "select/QuerySelectAssetOrder.php";

class SaveDependenciesEvent extends FWDRunnable {
  public function run() {

    //teste para verificar se o sistema n�o est� sendo hakeado
    $moAsset = new RMAsset();
    $moAsset->fetchById(FWDWebLib::getObject("asset_id")->getValue());
    $moCtxUserTest = new RMAsset();
    $moCtxUserTest->testPermissionToEdit(FWDWebLib::getObject("asset_id")->getValue(),$moAsset->getResponsible());

    $moWebLib = FWDWebLib::getInstance();
    $miAssetId = $moWebLib->getObject("asset_id")->getValue();
    $miDependency = $moWebLib->getObject("param_dependency")->getValue();
    $mbDependency = $miDependency==1?true:false;
    $moSelect = $moWebLib->getObject('asset_dep');
    $maSelectItems = $moSelect->getItemsKeys();

    $moDependency = new RMAssetDependency();

    $msAliasId = $mbDependency?"dependent_id":"asset_id";
    $moDependency->setAliasId($msAliasId);
    $moDependency->delete($miAssetId);
    $msNotAliasId = $mbDependency?"asset_id":"dependent_id";
    foreach($maSelectItems as $miSelectAssetId) {
      $moDependency = new RMAssetDependency();
      $moDependency->setFieldValue($msAliasId, $miAssetId);
      $moDependency->setFieldValue($msNotAliasId, $miSelectAssetId);
      $moDependency->insert();
    }
    //$this->populateGridQuery();
    echo "soWindow = soPopUpManager.getPopUpById('popup_asset_dependencies').getOpener();"
    ."if(soWindow.refresh_grid) soWindow.refresh_grid();"
    ."soPopUpManager.closePopUp('popup_asset_dependencies');";
  }
}

class PutValueEvent extends FWDRunnable {
  public function run() {
    $moWebLib = FWDWebLib::getInstance();
    $listAssetDep = $moWebLib->getObject('asset_dep');
    $varPutList = $moWebLib->getObject('var_put_list');

    $items = explode(",", $varPutList->getValue());

    $querySelectAssetOrder = new QuerySelectAssetOrder();
    $querySelectAssetOrder->setAssets($items);
    $querySelectAssetOrder->makeQuery();
    $querySelectAssetOrder->executeQuery();

    while($querySelectAssetOrder->fetch()){
      $moItem = new FWDItem();
      $moItem->setAttrKey($querySelectAssetOrder->getFieldValue('asset_id'));
      $moItem->setValue($querySelectAssetOrder->getFieldValue('asset_name')) ;
      $moItem->setAttrClass('asset_' . RMRiskConfig::getRiskColor( $querySelectAssetOrder->getFieldValue('asset_value')));
      $listAssetDep->addObjFWDItem($moItem);
    }
    $listAssetDep->execEventPopulate();
  }
}

class DelValueEvent extends FWDRunnable {
  public function run() {
    $moWebLib = FWDWebLib::getInstance();
    $listAssetAll = $moWebLib->getObject('asset_all');
    $varDelList = $moWebLib->getObject('var_del_list');

    $items = explode(",", $varDelList->getValue());

    $querySelectAssetOrder = new QuerySelectAssetOrder();
    $querySelectAssetOrder->setAssets($items);
    $querySelectAssetOrder->makeQuery();
    $querySelectAssetOrder->executeQuery();

    while($querySelectAssetOrder->fetch()){
      $moItem = new FWDItem();
      $moItem->setAttrKey($querySelectAssetOrder->getFieldValue('asset_id'));
      $moItem->setValue($querySelectAssetOrder->getFieldValue('asset_name')) ;
      $moItem->setAttrClass('asset_' . RMRiskConfig::getRiskColor( $querySelectAssetOrder->getFieldValue('asset_value')));
      $listAssetAll->addObjFWDItem($moItem);
      $listAssetAll->execEventPopulate();
    }
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new SaveDependenciesEvent("save_asset_dependencies"));
    $moStartEvent->addAjaxEvent(new PutValueEvent("put_value_event"));
    $moStartEvent->addAjaxEvent(new DelValueEvent("del_value_event"));

    $miAssetId = $moWebLib->getObject("param_asset_id")->getValue();
    $moWebLib->getObject("asset_id")->setValue($miAssetId);

  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $moWindowTitle = $moWebLib->getObject('window_title');
    $moListName = $moWebLib->getObject('list_name');

    $miAssetId = $moWebLib->getObject("asset_id")->getValue();

    //teste para verificar se o sistema n�o est� sendo hakeado
    $moAsset = new RMAsset();
    $moAsset->fetchById($miAssetId);
    $moCtxUserTest = new RMAsset();
    $moCtxUserTest->testPermissionToEdit($miAssetId,$moAsset->getResponsible());


    $miDependency = $moWebLib->getObject("param_dependency")->getValue();
    $mbDependency = $miDependency==1?true:false;
    $moSelectAll = $moWebLib->getObject('asset_all');
    $moSelectDep = $moWebLib->getObject('asset_dep');

    if ($mbDependency) {
      $moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_dependencies_adjustment',"Ajuste de Depend�ncias"));
      $moListName->setValue(FWDLanguage::getPHPStringValue('st_dependencies_list',"Lista de Depend�ncias"));
    }
    else {
      $moWindowTitle->setValue(FWDLanguage::getPHPStringValue('tt_dependents_adjustment',"Ajuste de Dependentes"));
      $moListName->setValue(FWDLanguage::getPHPStringValue('st_dependents_list',"Lista de Dependentes"));
    }

    /*
     * Popula o select com as poss�veis depend�ncias ou poss�veis dependentes do ativo
     */
    $moQuery = $mbDependency ? new QueryAssetPossibleDependencies($moWebLib->getConnection()) : new QueryAssetPossibleDependents($moWebLib->getConnection());
    $moQuery->setAsset($miAssetId);
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    $maSelectAllItems = $mbDependency ? $moQuery->getAssetDependencies() : $moQuery->getAssetDependents();
    if (count($maSelectAllItems) > 0) {
      foreach($maSelectAllItems as $maItem) {
        $moItem = new FWDItem();
        $moItem->setAttrKey($maItem['asset_id']);
        $moItem->setValue($maItem['asset_name']);
        $moItem->setAttrClass('asset_'.RMRiskConfig::getRiskColor($maItem['asset_value']));
        $moSelectAll->addObjFWDItem($moItem);
      }
    }

    /*
     * Popula o select com as depend�ncias ou dependentes diretos do ativo
     */
    $moQuery = $mbDependency ? new QueryAssetDependencies($moWebLib->getConnection()) : new QueryAssetDependents($moWebLib->getConnection());
    $moQuery->setAsset($miAssetId);
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    $maSelectDepItems = $mbDependency ? $moQuery->getAssetDependencies() : $moQuery->getAssetDependents();
    if (count($maSelectDepItems) > 0) {
      foreach($maSelectDepItems as $maItem) {
        $moItem = new FWDItem();
        $moItem->setAttrKey($maItem['asset_id']);
        $moItem->setValue($maItem['asset_name']);
        $moItem->setAttrClass('asset_'.RMRiskConfig::getRiskColor($maItem['asset_value']));
        $moSelectDep->addObjFWDItem($moItem);
      }
    }

    $moWebLib->dump_html($moWebLib->getObject('dialog'));
    ?>
<script>
		function setPutList(){
			var list_asset_all = gebi('asset_all');
			var list_asset_dep = gebi('asset_dep');

			var listTmp = new Array();
			  
			gebi('var_put_list').value = '';

			for (var i = 0; i < list_asset_dep.length; i++){
				listTmp.push(list_asset_dep.options[i].value);
			}

			var isSelected = false;
			for (var i = 0; i < list_asset_all.length; i++){
				if (list_asset_all.options[i].selected){
					listTmp.push(list_asset_all.options[i].value);
					isSelected = true;
				}
			}

			if(!isSelected) return;
			
			var j=0;
			while (j<list_asset_all.length) {
				if (list_asset_all.options[j].selected) {
					list_asset_all.options[j]=null;
				}
				else
					j++;
			}

			gebi('var_put_list').value = listTmp;

			trigger_event('put_value_event', 3);
		}


		function setDelList(){
			var list_asset_all = gebi('asset_all');
			var list_asset_dep = gebi('asset_dep');

			var listTmp = new Array();
			  
			gebi('var_del_list').value = '';

			for (var i = 0; i < list_asset_all.length; i++){
				listTmp.push(list_asset_all.options[i].value);
			}
			
			var isSelected = false;
			for (var i = 0; i < list_asset_dep.length; i++){
				if (list_asset_dep.options[i].selected){
					listTmp.push(list_asset_dep.options[i].value);
					isSelected = true;
				}
			}

			if(!isSelected) return;
			
			var j=0;
			while (j<list_asset_dep.length) {
				if (list_asset_dep.options[j].selected) {
					list_asset_dep.options[j]=null;
				}
				else
					j++;
			}

			gebi('var_del_list').value = listTmp;

			trigger_event('del_value_event', 3);
		}
  		
		function SelectAll(listbox) {
			for (i=0;(i<listbox.length);i++){
				listbox.options[i].selected = true;
			}	
		}
	</script>
    <?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_asset_dependencies.xml");

?>