<?php
include_once "include.php";
include_once $handlers_ref . "grid/QueryGridProcess.php";
include_once $handlers_ref . "select/QuerySelectArea.php";
include_once $handlers_ref . "select/QuerySelectProcessPriority.php";
include_once $handlers_ref . "select/QuerySelectProcessType.php";
include_once $handlers_ref . "select/QuerySelectProcessResponsibles.php";

class SearchEvent extends FWDRunnable {
	public function run(){
		$moGrid = FWDWebLib::getObject('grid_process');
		$moGrid->execEventPopulate();
	}
}

class GridProcess extends FWDDrawGrid {
	public function drawItem(){
		switch($this->ciColumnIndex){
			case $this->getIndexByAlias('process_value'):
				$mbHideEdit = false;
				$mbHideRemove = false;
				$mbHideAssociateAsset = false;
				if(!ISMSLib::userHasACL('M.RM.2.4')){
					if(ISMSLib::userHasACL('M.RM.2.7')){
						if ($this->getFieldValue('process_responsible_id') == ISMSLib::getCurrentUserId())
						$mbHideEdit=false;
						else
						$mbHideEdit=true;
					} else {
						$mbHideEdit=true;
					}
				}

				if(!ISMSLib::userHasACL('M.RM.2.5')){
					if(ISMSLib::userHasACL('M.RM.2.8')){
						if ($this->getFieldValue('process_responsible_id') == ISMSLib::getCurrentUserId())
						$mbHideRemove=false;
						else
						$mbHideRemove=true;
					} else {
						$mbHideRemove=true;
					}
				}
				if($this->getFieldValue('process_responsible_id') != ISMSLib::getCurrentUserId()){
					$mbHideAssociateAsset = true;
				}
				if ($mbHideEdit || $mbHideRemove || $mbHideAssociateAsset ) {
					$moACL = FWDACLSecurity::getInstance();
					$maDenied = array();
					if($mbHideEdit)
					$maDenied[] = 'perm_to_edit';
					if($mbHideRemove)
					$maDenied[] = 'perm_to_remove';
					if($mbHideAssociateAsset)
					$maDenied[] = 'perm_to_associate_asset';

					$moACL->setNotAllowed($maDenied);
					$moMenu = FWDWebLib::getObject('menu');
					$moGrid = FWDWebLib::getObject('grid_process');
					$msLine = $moGrid->getAttrName().'_acl_'.$this->ciRowIndex;
					FWDGridMenuACLSecurity::getInstance()->installGridMenuACL($moMenu,$moACL,$moGrid,$msLine);
				}

				$miProcessValue = $this->getFieldValue('process_value');
				$msProcessColor = RMRiskConfig::getRiskColor($miProcessValue);
				$this->coCellBox->setIconSrc("icon-process_".$msProcessColor.".gif");
				$this->coCellBox->setObjFWDToolTip(ISMSLib::getInstance()->getToolTipCreateModify($this->getFieldValue('user_create_name'),$this->getFieldValue('date_create'),$this->getFieldValue('user_edit_name'),$this->getFieldValue('date_edit')));
				return parent::drawItem();
				break;

			case $this->getIndexByAlias('process_name'):
				$miProcessValue = $this->getFieldValue('process_value');
				$msLinkColor = RMRiskConfig::getRiskColor($miProcessValue,true);
				$msLink = "<font color=".$msLinkColor.">".$this->getFieldValue('process_name')."</a>";
				$this->coCellBox->setAttrStringNoEscape("true");
				if(ISMSLib::userHasACL('M.RM.2.2') && ISMSLib::userHasACL('M.RM.3')){
					$this->coCellBox->setValue("<a href='javascript:go_to_nav_asset(" . $this->getFieldValue('process_id') . ");'>{$msLink}</a>");
				}else{
					$this->coCellBox->setValue("<a href='javascript:open_visualize(" . $this->getFieldValue('process_id') . ",".CONTEXT_PROCESS.",\"".uniqid()."\")'>{$msLink}</a>");
				}
				return $this->coCellBox->draw();
				break;

			case $this->getIndexByAlias('process_area'):
				$this->coCellBox->setAttrStringNoEscape("true");
				if(ISMSLib::userHasACL('M.RM.1') && ISMSLib::userHasACL('M.RM.1.2')){
					$this->coCellBox->setValue("<a href='javascript:go_to_nav_area(" . $this->getFieldValue('area_id') . ");'>" . $this->getFieldValue('process_area') . "</a>");
				}
				return $this->coCellBox->draw();
				break;

			case $this->getIndexByAlias('process_asset_count'):
				$this->coCellBox->setAttrStringNoEscape("true");
				if(ISMSLib::userHasACL('M.RM.3') && ISMSLib::userHasACL('M.RM.2.2')){
					$this->coCellBox->setValue("<a href='javascript:go_to_nav_asset(" . $this->getFieldValue('process_id') . ");'>" . $this->getFieldValue('process_asset_count') . "</a>");
				}
				return $this->coCellBox->draw();
				break;

			case $this->getIndexByAlias('process_user_count'):
				//$this->coCellBox->setAttrStringNoEscape("true");
				if(!$this->coCellBox->getValue()) $this->coCellBox->setValue('0');
				return $this->coCellBox->draw();
				break;

			case $this->getIndexByAlias('process_state'):
				$this->coCellBox->setValue(ISMSContextObject::getContextStateAsString($this->coCellBox->getValue()));
				return $this->coCellBox->draw();
				break;

			default:
				return parent::drawItem();
				break;
		}
	}
}

class ProcessConfirmRemove extends FWDRunnable {
	public function run(){
			
		$processId = FWDWebLib::getObject('selected_process_id')->getValue();
		$moProcess = new RMProcess();
		$moProcess->fetchById($processId);
		$msProcessName = ISMSLib::truncateString($moProcess->getFieldValue('process_name'), 70);
			
		$nonConformityAssociated = new CINonConformityProcess();
		$nonConformities = $nonConformityAssociated->countByProcess($processId);
		if(!ISMSLib::getConfigById(GENERAL_CASCADE_ON) && $nonConformities > 0){
			$msTitle = FWDLanguage::getPHPStringValue('tt_process_remove__error','Erro ao remover processo');
			$msMessage = FWDLanguage::getPHPStringValue('st_process_remove_non_conformity_error',"N�o � poss�vel remover o processo <b>%process_name%</b> pois ele est� associado a n�o conformidades. Para remover processos associados a n�o conformidades, ative a op��o \"Delete Cascade\" na aba \"Admin -> Configura��o\".");
			$msMessage = str_replace("%process_name%",$msProcessName,$msMessage);
			ISMSLib::openOk($msTitle,$msMessage,'',80);
		}else{
			$msTitle = FWDLanguage::getPHPStringValue('tt_remove_process','Remover Processo');
			$msMessage = FWDLanguage::getPHPStringValue('st_process_remove_confirm',"Voc� tem certeza que deseja remover o processo <b>%process_name%</b>?");
			if(ISMSLib::getConfigById(GENERAL_CASCADE_ON) && $nonConformities > 0){
				$msMessage .= FWDLanguage::getPHPStringValue('st_process_remove_non_conformity_warning',"<br><b>Aten��o:</b> As n�o conformidades associadas a este processo tamb�m ser�o removidas!");
			}
				
			$msMessage = str_replace("%process_name%",$msProcessName,$msMessage);

			$msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
	                    "soWindow = soPopUp.getOpener();" .
	                    "soWindow.remove_process();";

			ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,50);
		}
	}
}

class AssociateAssetsToProcessEvent extends FWDRunnable {
	public function run(){
		$miProcessId = FWDWebLib::getObject('selected_process_id')->getValue();
		$maAssets = FWDWebLib::getObject('selected_assets_ids')->getValue();

		$maAssets = explode(",", $maAssets);

		$moProcessAsset = new RMProcessAsset();
		$moProcessAsset->setFieldValue('process_id', $miProcessId);
		foreach ($maAssets as $miAssetId) {
			$moProcessAsset->setFieldValue('asset_id', $miAssetId);
			$moProcessAsset->insert();
		}
	}
}

class RemoveProcessEvent extends FWDRunnable {
	public function run(){
		$miProcessId = FWDWebLib::getObject('selected_process_id')->getValue();
		$moProcess = new RMProcess();
		$moProcess->delete($miProcessId);
		/*
		 * Se possuir o m�dulo de documenta��o atualiza os leitores do documento do processo.
		 */
		if (FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(POLICY_MODE))
		$moProcess->updateReaders($miProcessId);
	}
}

class RedirectToProcessArea extends FWDRunnable {
	public function run(){
		$moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
		$moSession->deleteAttribute('areaid');
		$moSession->addAttribute('areaid');
		$moSession->setAttrAreaId(FWDWebLib::getObject('selected_process_area')->getValue());
		echo "isms_change_to_sibling(1,'nav_area.php?uniqid=".uniqid()."');";
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));
		$moStartEvent->addAjaxEvent(new ProcessConfirmRemove('process_confirm_remove'));
		$moStartEvent->addAjaxEvent(new RemoveProcessEvent('remove_process_event'));
		$moStartEvent->addAjaxEvent(new AssociateAssetsToProcessEvent('associate_assets_to_process_event'));
		$moStartEvent->addAjaxEvent(new SearchEvent('search'));
		$moStartEvent->addAjaxEvent(new RedirectToProcessArea('redirect_to_process_area'));
		//instala a seguran�a de ACL na p�gina
		FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
		FWDMenuACLSecurity::getInstance()->installMenuACL(FWDWebLib::getObject('menu'),FWDACLSecurity::getInstance());

		$moGrid = FWDWebLib::getObject("grid_process");
		$moHandler = new QueryGridProcess(FWDWebLib::getConnection());
		if(FWDWebLib::getObject('par_scrolling_filter')->getValue()){
			FWDWebLib::getObject('filter_scrolling')->setValue(FWDWebLib::getObject('par_scrolling_filter')->getValue());
			FWDWebLib::getObject('area_id')->setValue(FWDWebLib::getObject('par_scrolling_filter')->getValue());
		}

		// Popula o select do filtro por respons�vel
		$moSelect = FWDWebLib::getObject('responsible');
		$moSelectHandler = new QuerySelectProcessResponsibles();
		if(!ISMSLib::userHasACL('M.RM.2.1')){
			$moSelectHandler->setUserId(ISMSLib::getCurrentUserId());
		}
		$moSelect->setQueryHandler($moSelectHandler);
		$moSelect->populate();

		// Popula o select do filtro por �rea
		$moSelect = FWDWebLib::getObject('area');
		$moSelect->setQueryHandler(new QuerySelectArea());
		$moSelect->populate();

		// Popula o select do filtro por prioridade
		$moSelect = FWDWebLib::getObject('priority');
		$moSelect->setQueryHandler(new QuerySelectProcessPriority());
		$moSelect->populate();

		// Popula o select do filtro por tipo
		$moSelect = FWDWebLib::getObject('type');
		$moSelect->setQueryHandler(new QuerySelectProcessType());
		$moSelect->populate();

		// Popula o select do filtro por estado
		$moSelect = FWDWebLib::getObject('state');
		$maStatus = array(CONTEXT_STATE_PENDANT,CONTEXT_STATE_APPROVED,CONTEXT_STATE_DENIED);
		foreach($maStatus as $miStatus){
			$moItem = new FWDItem();
			$moItem->setAttrKey($miStatus);
			$moItem->setValue(ISMSContextObject::getContextStateAsString($miStatus));
			$moSelect->addObjFWDItem($moItem);
		}

		// Seta os filtros
		if(strpos(FWDWebLib::getObject('search_in_name_only')->getValue(),'1')!==false){
			$moHandler->setName(FWDWebLib::convertToISO(FWDWebLib::getObject('name_filter')->getValue(),true));
		}else{
			$moHandler->setNameDescription(FWDWebLib::convertToISO(FWDWebLib::getObject('name_filter')->getValue(),true));
		}
		$moHandler->setResponsible(FWDWebLib::getObject('responsible')->getValue());
		$moHandler->setMaxAssets(FWDWebLib::getObject('max_assets')->getValue());
		$moHandler->setMinAssets(FWDWebLib::getObject('min_assets')->getValue());
		$moHandler->setValueFilter(array_filter(explode(':',FWDWebLib::getObject('value_filter_controller')->getValue())));
		$moHandler->setType(FWDWebLib::getObject('type')->getValue());
		$moHandler->setPriority(FWDWebLib::getObject('priority')->getValue());
		$moHandler->setState(FWDWebLib::getObject('state')->getValue());

		if(!ISMSLib::userHasACL('M.RM.2.1')){
			$moHandler->setUserId(ISMSLib::getCurrentUserId());
		}

		$miAreaId = FWDWebLib::getObject('area_id')->getValue();
		if($miAreaId){
			$moHandler->setArea($miAreaId);
			FWDWebLib::getObject('area')->setValue($miAreaId);
			FWDWebLib::getObject('area')->setAttrDisabled('true');
		}else{
			$moHandler->setArea(FWDWebLib::getObject('area')->getValue());
		}

		$moHandler->setAsset(FWDWebLib::getObject('par_filter_by_asset')->getValue());

		$moGrid->setQueryHandler($moHandler);
		$moGrid->setObjFwdDrawGrid(new GridProcess());
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		FWDWebLib::getObject('search_in_name_only')->setValue('1');

		if(FWDWebLib::getObject('par_filter_by_asset')->getValue()){
			$moAsset = new RMAsset();
			$moAsset->fetchById(FWDWebLib::getObject('par_filter_by_asset')->getValue());
			$msTitle = FWDWebLib::getObject('grid_process_name')->getValue(). " " . FWDLanguage::getPHPStringValue('grid_process_asset_filter',"(filtrados pelo ativo '<b>%asset_name%</b>')");
			$msTitle = str_replace("%asset_name%", $moAsset->getName(), $msTitle);
			FWDWebLib::getObject('grid_process_name')->setValue($msTitle);
		}
		//grid_process_name
		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
<script language="javascript">
        function refresh_grid() {
          js_refresh_grid('grid_process');
        }
        
        function set_assets(assets) {
          gebi('selected_assets_ids').value = assets;
          trigger_event('associate_assets_to_process_event', 3);
          refresh_grid();
        }
        
        function remove_process() {
          trigger_event('remove_process_event', 3);
          refresh_grid();
        }
        
        function go_to_nav_asset(piId){
          if(gebi('filter_scrolling').value!=0){
            isms_change_to_sibling(3,'nav_asset.php?scrollfilter='+piId+':'+gebi('filter_scrolling').value);
          }else{
            isms_change_to_sibling(3,'nav_asset.php?scrollfilter='+piId);
          }
        }
        
        function go_to_nav_area(piId){
          gebi('selected_process_area').value = piId;
          trigger_event('redirect_to_process_area',3);
        }
        
      </script>
		<?
		if(FWDWebLib::getObject('filter_scrolling')->getValue())
		ISMSLib::getPathScrollCode(RISK_MANAGEMENT_MODE,CONTEXT_AREA,FWDWebLib::getObject('filter_scrolling')->getValue(),false,'');
		else
		ISMSLib::getPathScrollCode(RISK_MANAGEMENT_MODE,CONTEXT_PROCESS,'',false,'');

	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_process.xml");
?>