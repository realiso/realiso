<?php
include_once "include.php";

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moConfig = new ISMSConfig();
    if($moConfig->getConfigFromDB(CLIENT_ID_ASAAS)) {
        $moISMSASaaS = new ISMSASaaS();
        $moISMSASaaS->setNewCheckPointASaaS(utf8_encode("Acessou M�dulo Gest�o de Risco"), utf8_encode("M�dulo Gest�o de Risco"));
    }

  	$miContextType = FWDWebLib::getObject('context_type')->getValue();
    $miContextId = FWDWebLib::getObject('context_id')->getValue();

    if ($miContextType) {
    	switch ($miContextType) {
    		case CONTEXT_AREA:
    			FWDWebLib::getObject('tab_group')->setAttrLoadTabItemIndex(1);
    			$moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    			$moSession->addAttribute('areaid');
  				$moSession->setAttrAreaId($miContextId);
    		break;
    		case CONTEXT_PROCESS:
    			FWDWebLib::getObject('tab_group')->setAttrLoadTabItemIndex(2);
    			$moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    		break;
    		case CONTEXT_ASSET:
    			FWDWebLib::getObject('tab_group')->setAttrLoadTabItemIndex(3);
    			$moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    			$moSession->addAttribute('processid');
  				$moSession->setAttrProcessId($miContextId);
    		break;
    		case CONTEXT_RISK:
    			FWDWebLib::getObject('tab_group')->setAttrLoadTabItemIndex(4);
    			$moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    			$moSession->addAttribute('assetid');
  				$moSession->setAttrAssetId($miContextId);
    		break;
    		case CONTEXT_CONTROL:
    			FWDWebLib::getObject('tab_group')->setAttrLoadTabItemIndex(5);
    			$moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    		break;
        case CONTEXT_CI_INCIDENT:
          /*Advanced POG -> utilizado para apartir da nav_incident ir para a nav_risk filtrando pelo incidente*/
          FWDWebLib::getObject('tab_group')->setAttrLoadTabItemIndex(4);
          $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
          $moSession->deleteAttribute('incidentid');
          $moSession->addAttribute('incidentid');
          $moSession->setAttrIncidentId($miContextId);
        BREAK;
    	}
    }

    $mbShowHelp = true;
    $msHelpJS = "";
    $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    $miUserId = $moSession->getUserId(true);
    $moUser = new ISMSUser();
    if ($moUser->fetchById($miUserId)){
      $mbShowHelp = (bool)$moUser->getFieldValue("show_help");
    }
    if ($mbShowHelp) {
      $msHelpJS = "isms_open_popup('popup_help','popup_help.php','','true');";
    }

    //instala a seguran�a de ACL na p�gina
		FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        <?=$msHelpJS?>
      </script>
    <?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("tab_risk_management.xml");
?>