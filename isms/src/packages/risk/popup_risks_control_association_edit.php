<?php

include_once "include.php";

set_time_limit(240);

class SaveEvent extends FWDRunnable {
  public function run(){
    $miControlId = FWDWebLib::getObject('par_control')->getValue();
    $maRisksIds = array_filter(explode(':',FWDWebLib::getObject('par_risks')->getValue()));
    
        // Teste para verificar se o sistema n�o est� sendo hackeado
    $moRiskControlTest = new RMRiskControl();
    $moRiskControlTest->testPermissionToEdit($miControlId);

    foreach($maRisksIds as $miRiskId){
      $moRiskControl = new RMRiskControl();
      
      $moRiskParameter = FWDWebLib::getObject("risk_parameters_{$miRiskId}");
      $maValues = $moRiskParameter->getValues();
      
      $moRiskControl->setFieldValue('rc_probability',$maValues['prob']);
      $moRiskControl->setFieldValue('rc_risk_id',$miRiskId);
      $moRiskControl->setFieldValue('rc_control_id',$miControlId);
      
      $miRiskControlId = $moRiskControl->insert(true);
      
      $moRiskParametersValue = new RiskParametersValue($moRiskParameter,RISKPARAMETER_RISKCONTROL);
      $moRiskParametersValue->setValues($miRiskControlId);
    }
    
    // Se possuir o m�dulo de documenta��o, deve atualizar os leitores do documento
    if(ISMSLib::hasModule(POLICY_MODE)){
      $moControl = new RMControl();
      $moControl->updateReaders($miControlId);
    }
    
    echo "soPopUpManager.getPopUpById('popup_control_risk_edit').getWindow().refresh_grid();" .
         "soPopUpManager.getPopUpById('popup_control_risk_edit').getOpener().refresh_grid();" .
         "soPopUpManager.closePopUp('popup_control_risk_search');".
         "self.close();";
  }
}

function getParametersNames(){
/*
  return array(
    '1' => '1� Parametro',
    '2' => '2� Parametro',
    '3' => '3� Parametro',
    '4' => '4� Parametro',
    '5' => '5� Parametro',
    '6' => '6� Parametro',
    '7' => '7� Parametro',
    '8' => '8� Parametro',
  );
//*/
  $maParameters = FWDWebLib::getObject('ParametersNames');
  if($maParameters===null){
    $maParameters = array();
    $moHandler = new QueryParameterNames(FWDWebLib::getConnection());
    $moHandler->makeQuery();
    $moHandler->executeQuery();
    $moDataset = $moHandler->getDataset();
    while($moDataset->fetch()){
      $miId = $moDataset->getFieldByAlias('parametername_id')->getValue();
      $msName = $moDataset->getFieldByAlias('parametername_name')->getValue();
      $maParameters[$miId] = $msName;
    }
    FWDWebLib::addObject('ParametersNames',$maParameters);
  }
  return $maParameters;
}

function getValuesNames(){
/*
  return array(
    '1' => 'valor grande 1',
    '2' => 'valor grande 2',
    '3' => 'valor grande 3',
  );
//*/
  $maValuesNames = FWDWebLib::getObject('ValuesNames');
  if($maValuesNames===null){
    $maValuesNames = array();
    $moHandler = new QueryParameterValueNames(FWDWebLib::getConnection());
    $moHandler->setType(RISKPARAMETER_RISKCONTROL);
    $moHandler->makeQuery();
    $moHandler->executeQuery();
    $moDataset = $moHandler->getDataset();
    while($moDataset->fetch()){
      $miId = $moDataset->getFieldByAlias('value_id')->getValue();
      $msName = $moDataset->getFieldByAlias('value_name')->getValue();
      $maValuesNames[$miId] = $msName;
    }
    FWDWebLib::addObject('ValuesNames',$maValuesNames);
  }
  return $maValuesNames;
}

function getProbabilityValues(){
/*
  return array(
    '1' => 'valor grande 1',
    '2' => 'valor grande 2',
    '3' => 'valor grande 3',
  );
//*/
  $maProbabilityValues = FWDWebLib::getObject('ProbabilityValues');
  if($maProbabilityValues===null){
    $maProbabilityValues = array();
    $moHandler = new QueryProbabilityValueNames(FWDWebLib::getConnection());
    $moHandler->setType(RISKPARAMETER_RISKCONTROL);
    $moHandler->makeQuery();
    $moHandler->executeQuery();
    $moDataset = $moHandler->getDataset();
    while($moDataset->fetch()){
      $miId = $moDataset->getFieldByAlias('value_id')->getValue();
      $msName = $moDataset->getFieldByAlias('value_name')->getValue();
      $maProbabilityValues[$miId] = $msName;
    }
    FWDWebLib::addObject('ProbabilityValues',$maProbabilityValues);
  }
  return $maProbabilityValues;
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
    $moStartEvent->addAjaxEvent(new SaveEvent('save_event'));
    
    // Busca no banco os nomes dos par�metros
    $maParameters = getParametersNames();
    // Busca no banco os nomes dos valores
    $maValues = getValuesNames();
    // Busca no banco os nomes dos valores de probabilidade
    $maProbabilityValues = getProbabilityValues();
    
    $maRisksIds = explode(':',FWDWebLib::getObject('par_risks')->getValue());
    
    foreach($maRisksIds as $miRiskId){
      $moRiskParameter = new RiskParameters();
      $moRiskParameter->setAttrName("risk_parameters_$miRiskId");
      FWDWebLib::addObject("risk_parameters_$miRiskId",$moRiskParameter);
      $moRiskParameter->setAttrType('control');
      $moRiskParameter->setObjFWDBox(new FWDBox());
      foreach($maParameters as $miId=>$msName){
        $moRiskParameter->addParameter($miId,$msName);
      }
      foreach($maValues as $miId=>$msName){
        $moRiskParameter->addValue($miId,$msName);
      }
      foreach($maProbabilityValues as $miId=>$msName){
        $moRiskParameter->addProbabilityValue($miId,$msName);
      }
    }
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $maRisksIds = explode(':',$moWebLib->getObject('par_risks')->getValue());
    
    // Teste para verificar se o sistema n�o est� sendo hackeado
    $moRiskControlTest = new RMRiskControl();
    $moRiskControlTest->testPermissionToEdit(FWDWebLib::getObject('par_control')->getValue());
    
    $miControlId = FWDWebLib::getObject('par_control')->getValue();
    $moControl = new RMControl();
    $moControl->fetchById($miControlId);
    $msControlName = $moControl->getName();
    FWDWebLib::getObject('control_name')->setValue($msControlName);
    FWDWebLib::getObject('tooltip_control_name')->setValue($msControlName);
    
    // Busca no banco os nomes dos par�metros
    $maParameters = getParametersNames();
    // Busca no banco os nomes dos valores
    $maValues = getValuesNames();
    // Busca no banco os nomes dos valores de probabilidade
    $maProbabilityValues = getProbabilityValues();
    
    $moOuterViewGroup = $moWebLib->getObject('risk_parameters');
    $moInnerViewGroup = $moWebLib->getObject('lines');
    
    $moInnerViewGroup->setAttrClass('simple_border');
    
    $miMaxSelectWidth = 150;
    $miMaxStaticWidth = 250;
    $miHorizontalMargin = 10;
    $miStaticWidth = 160;
    $miLineHeight = 36;
    $miStaticHeight = 32;
    $miRiskParameterHeight = 25;
    $miHeaderHeight = 25;
    $miHorizontalOffset = floor($miHorizontalMargin/2);
    
    $miStaticVerticalOffset = floor(($miLineHeight - $miStaticHeight)/2);
    $miRiskParameterVerticalOffset = floor(($miLineHeight - $miRiskParameterHeight)/2);
    
    $miViewGroupWidth = $moInnerViewGroup->getObjFWDBox()->getAttrWidth();
    $miColumns = count($maParameters) + 1;
    $miColumnsTotalWidth = $miViewGroupWidth - $miStaticWidth - 20;
    if($miColumnsTotalWidth/$miColumns > $miMaxSelectWidth){
      $miStaticWidth = $miViewGroupWidth - $miColumns * $miMaxSelectWidth;
      if($miStaticWidth > $miMaxStaticWidth){
        $miStaticWidth = $miMaxStaticWidth;
        $miColumnsTotalWidth = $miColumns * $miMaxSelectWidth;
      }else{
        $miColumnsTotalWidth = $miViewGroupWidth - $miStaticWidth - 20;
      }
    }
    $miSelectWidth = $miColumnsTotalWidth/$miColumns - $miHorizontalMargin;

    $miLeft = $miStaticWidth;
    $maParameters['prob'] = FWDLanguage::getPHPStringValue('mx_probability','Probabilidade');
    foreach($maParameters as $msName){
      $moStatic = new FWDStatic();
      $moStatic->setObjFWDBox(new FWDBox($miLeft+$miHorizontalOffset,0,$miHeaderHeight,$miSelectWidth));
      $moStatic->setValue($msName);
      $moOuterViewGroup->addObjFWDView($moStatic);
      $miLeft+= $miSelectWidth + $miHorizontalMargin;
      
      $moToolTip = new FWDToolTip();
      $moToolTip->setAttrShowDelay(1000);
      $moToolTip->setValue($msName);
      $moStatic->addObjFWDEvent($moToolTip);
    }
    unset($maParameters['prob']);

    $moRiskParameter = new RiskParameters();
    $moRiskParameter->setAttrName("risk_parameters_all");
    $moRiskParameter->setAttrType('risk');
    $moRiskParameter->setAttrClass('');
    $moRiskParameter->setAttrHorizontal('true');
    $moRiskParameter->setAttrValueNameWidth($miSelectWidth);
    $moRiskParameter->setAttrHorizontalMargin($miHorizontalMargin);
    $moRiskParameter->setObjFWDBox(new FWDBox($miStaticWidth,25,0,0));
    $moOuterViewGroup->addObjFWDView($moRiskParameter);
    foreach($maParameters as $miId=>$msName){
      $moRiskParameter->addParameter($miId,$msName);
    }
    foreach($maValues as $miId=>$msName){
      $moRiskParameter->addValue($miId,$msName);
    }
    foreach($maProbabilityValues as $miId=>$msName){
      $moRiskParameter->addProbabilityValue($miId,$msName);
    }
    $moRiskParameter->build();

    $miTop = 0;
    $mbEven = false;
    
    foreach($maRisksIds as $miRiskId){
      $moRisk = new RMRisk();
      $moRisk->fetchById($miRiskId);
      $msRiskName = $moRisk->getFieldValue('risk_name');
      
      $moLine = new FWDViewGroup();
      $moLine->setObjFWDBox(new FWDBox(0,$miTop,$miLineHeight,$miViewGroupWidth-20));
      $moLine->setAttrClass($mbEven?'BdZ0':'BdZ1');
      $moInnerViewGroup->addObjFWDView($moLine);
      
      $moStatic = new FWDMemoStatic();
      $moStatic->setAttrScrollbar('true');
      $moStatic->setAttrName("label_risk_parameters_$miRiskId");
      $moStatic->setAttrClass('MemoStatic_RiskName');
      $moStatic->setObjFWDBox(new FWDBox(0,$miStaticVerticalOffset,$miStaticHeight,$miStaticWidth));
      $moStatic->setValue($msRiskName);
      $moLine->addObjFWDView($moStatic);
      
      $moToolTip = new FWDToolTip();
      $moToolTip->setAttrShowDelay(1000);
      $moToolTip->setValue($msRiskName);
      $moStatic->addObjFWDEvent($moToolTip);
      
      $moRiskParameter = $moWebLib->getObject("risk_parameters_$miRiskId");
      $moRiskParameter->setAttrClass('');
      $moRiskParameter->setAttrHorizontal('true');
      $moRiskParameter->setAttrValueNameWidth($miSelectWidth);
      $moRiskParameter->setAttrHorizontalMargin($miHorizontalMargin);
      $moRiskParameter->setObjFWDBox(new FWDBox($miStaticWidth,$miRiskParameterVerticalOffset,0,0));
      /*
      $moRiskParameterValues = new RiskParametersValue($moRiskParameter,RISKPARAMETER_RISKCONTROL);
      $moRiskParameterValues->getValues($miRiskId);
      /*/
      $moRiskParameter->build();
      //*/
      $moLine->addObjFWDView($moRiskParameter);
      
      $moRiskHash = new FWDVariable();
      $moRiskHash->setAttrName("risk_hash_$miRiskId");
      $moRiskHash->setValue($moRiskParameter->getHash());
      $moLine->addObjFWDView($moRiskHash);
      
      $miTop+= $miLineHeight;
      $mbEven = !$mbEven;
    }
    
    FWDWebLib::getInstance()->dump_html($moWebLib->getObject('dialog'));
    ?>
      <script language="javascript">
        
        gebi('risk_parameters').object = {
          'getRiskParameters': function(){
            if(!this.caRiskParameters){
              var maIds = js_get_get_parameter('risks').split(':');
              this.caRiskParameters = [];
              for(var i=0;i<maIds.length;i++){
                this.caRiskParameters.push(gebi('risk_parameters_'+maIds[i]));
                this.caRiskParameters[i].object.onRequiredCheckOk = null;
                this.caRiskParameters[i].object.onRequiredCheckNotOk = null;
              }
              maIds = null;
            }
            return this.caRiskParameters;
          },
          'isFilled': function(){
            var maOk = [];
            var maNotOk = [];
            var maRiskParameters = this.getRiskParameters();
            for(var i=0;i<maRiskParameters.length;i++){
              if(maRiskParameters[i].object.isFilled()){
                maOk.push(maRiskParameters[i].id);
              }else{
                maNotOk.push(maRiskParameters[i].id);
              }
            }
            js_required_check_feedback(maOk,maNotOk);
            var mbReturn = (maNotOk.length==0);
            maOk = maNotOk = maRiskParameters = null;
            return mbReturn;
          },
          'onRequiredCheckOk': function(){},
          'onRequiredCheckNotOk': function(){},
          'setParameterValue': function(psParamId,psValueId){
            var maRiskParameters = this.getRiskParameters();
            for(var i=0;i<maRiskParameters.length;i++){
              maRiskParameters[i].object.setParameterValue(psParamId,psValueId);
            }
            maRiskParameters = null;
          }
        };
        
        gobi('risk_parameters_all').setParameterValue = function(psParamId,psValueId){
          var moSelect = gebi(this.csName+'_'+psParamId);
          moSelect.value = psValueId;
          moSelect.className = moSelect.options[moSelect.selectedIndex].className;
          gobi('risk_parameters').setParameterValue(psParamId,psValueId);
          moSelect = psParamId = psValueId = null;
        }
        
      </script>
    <?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_risks_control_association_edit.xml');

?>