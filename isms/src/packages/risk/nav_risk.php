<?php
include_once "include.php";
include_once $handlers_ref . "grid/QueryGridRisk.php";
include_once $handlers_ref . "select/QuerySelectAsset.php";

/*
function adjustColumns() {
  $moLicense = new ISMSLicense();
  if ($moLicense->isEMS()) {
    $maColumns = FWDWebLib::getObject('grid_risk')->getColumns();
    foreach ($maColumns as $moColumn) {
      if ($moColumn->getAttrAlias() == 'risk_impact') $moColumn->setAttrDisplay("true");
    }
  }
}
*/

class SearchEvent extends FWDRunnable {
  public function run(){
    $moGrid = FWDWebLib::getObject('grid_risk');
    $moGrid->execEventPopulate();
  }
}

class ConfirmRemove extends FWDRunnable {
  public function run(){
    $maRisks = FWDWebLib::getObject('grid_risk')->getValue();
    $miClickedRiskId = FWDWebLib::getObject('selected_id')->getValue();
    if(!in_array($miClickedRiskId,$maRisks)){
      $maRisks = array($miClickedRiskId);
    }
    $miRiskCount = count($maRisks);
    
    $miRiskId = FWDWebLib::getObject('selected_id')->getValue();
    $msTitle = FWDLanguage::getPHPStringValue('tt_remove_risk','Remover Risco');
    $miHeight = 75;
    
    $msWarning = '';
    if(ISMSLib::hasModule(INCIDENT_MODE)){
      $moIncidentRisk = new CIIncidentRisk();
      foreach($maRisks as $miRiskId){
        $moIncidentRisk->createFilter($miRiskId,'risk_id','in');
      }
      $moIncidentRisk->select();
      if($moIncidentRisk->fetch()){
        $miHeight = 115;
        if($miRiskCount==1){
          $msWarning = FWDLanguage::getPHPStringValue('st_risk_remove_has_incident',"<br><br><b>Aten��o: O risco possui incidente(s) associados(s).</b>");
        }else{
          $msWarning = FWDLanguage::getPHPStringValue('st_risk_remove_many_have_incident',"<br><br><b>Aten��o: Pelo menos um risco possui incidente(s) associados(s).</b>");
        }
      }
    }
    
    if($miRiskCount==1){
      $moRisk = new RMRisk();
      $moRisk->fetchById($maRisks[0]);
      $msRiskName = ISMSLib::truncateString($moRisk->getFieldValue('risk_name'), 70);
      $msMessage = FWDLanguage::getPHPStringValue('st_risk_remove_confirm',"Voc� tem certeza que deseja remover o risco <b>%risk_name%</b>?");
      $msMessage = str_replace("%risk_name%",$msRiskName,$msMessage);
    }else{
      $msMessage = FWDLanguage::getPHPStringValue('st_risk_remove_many_confirm',"Voc� tem certeza que deseja remover os <b>%count%</b> riscos selecionados?");
      $msMessage = str_replace("%count%",$miRiskCount,$msMessage);
    }
    
    $msMessage.= $msWarning;
    $msEventValue = "soPopUpManager.getPopUpById('popup_confirm').getOpener().remove();";
    
    ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,$miHeight);
    echo "gobi('grid_risk').hideGlass();";
  }
}

class RemoveEvent extends FWDRunnable {
  public function run(){
    $maRisks = FWDWebLib::getObject('grid_risk')->getValue();
    $miClickedRiskId = FWDWebLib::getObject('selected_id')->getValue();
    if(!in_array($miClickedRiskId,$maRisks)){
      $maRisks = array($miClickedRiskId);
    }
    
    $mbHasPMModule = ISMSLib::hasModule(POLICY_MODE);
    
    if($mbHasPMModule){
      $maControls = array();
    }
    
    // Se possuir o m�dulo de documenta��o, atualiza os leitores dos documentos
    // dos controles que mitigam os riscos. Deve primeiro pegar os controles
    // associados com os riscos, e somente depois de deletar os riscos, atualizar
    // os leitores dos documentos dos controles.
    foreach($maRisks as $miRiskId){
      if($mbHasPMModule){
        $moRC = new RMRiskControl();
        $maControls = array_merge($maControls,$moRC->getControlsFromRisk($miRiskId));
      }
      $moRisk = new RMRisk();
      $moRisk->delete($miRiskId);
    }
    
    if($mbHasPMModule){
      $maControls = array_unique($maControls);
      $moControl = new RMControl();
      foreach($maControls as $miControlId){
        $moControl->updateReaders($miControlId);
      }
    }
    
  }
}

class CancelRiskTreatmentEvent extends FWDRunnable {
  public function run(){
    $miRiskId = FWDWebLib::getObject("selected_id")->getValue();

    //teste para verificar se o sistema n�o est� sendo hakeado
    $moCtxUserTest = new RMRisk();
    $moRisk = new RMRisk();
    $moRisk->fetchById($miRiskId);
    
    // para cancelar o tratamento do risco o usu�rio deve ser respons�vel do ativo ou pela seguran�a do ativo
    if (($moRisk->getResponsibleToACLPermition() != ISMSLib::getCurrentUserId()) && ($moRisk->getSecondResponsibleToACLPermition() != ISMSLib::getCurrentUserId())) {
      $msString = FWDLanguage::getPHPStringValue('st_denied_permission_to_edit','Voc� n�o tem permiss�o para editar o(a)').' '.strtolower($moRisk->getLabel()).'.';
      trigger_error($msString,E_USER_ERROR);
    }
    $moAcceptRisk = new RMAcceptRisk();
    $moAcceptRisk->setFieldValue("risk_accept_mode", 0);
    $moAcceptRisk->setFieldValue("risk_accept_justification", "");
    $moAcceptRisk->update($miRiskId);
    echo "refresh_grid();";
  }
}

class GridRisk extends FWDDrawGrid {
  public function drawItem(){
    switch($this->ciColumnIndex){
      case $this->getIndexByAlias('risk_id'):
        $maNotAllowed=array();
        $mbHideControlAssociation = false;
        $mbHideEdit = false;
        $mbHideRemove = false;
        if(!ISMSLib::userHasACL('M.RM.4.4')){
          if(ISMSLib::userHasACL('M.RM.4.9')){
            if (($this->getFieldValue('risk_responsible_id') == ISMSLib::getCurrentUserId()) || ($this->getFieldValue('asset_responsible_id') == ISMSLib::getCurrentUserId()))
              $mbHideEdit=false;
            else
              $mbHideEdit=true;
          } else {
            $mbHideEdit=true;
          }
          if ($mbHideEdit) {
            $maNotAllowed []= "perm_to_edit";
          }
        }
        if($this->getFieldValue('risk_value')==0){
          $mbHideControlAssociation = true;
          $maNotAllowed[] = "ZeroRiskValue";    
        }
        if(!ISMSLib::userHasACL('M.RM.4.5')){
          if(ISMSLib::userHasACL('M.RM.4.10')){
            if (($this->getFieldValue('risk_responsible_id') == ISMSLib::getCurrentUserId()) || ($this->getFieldValue('asset_responsible_id') == ISMSLib::getCurrentUserId()))
              $mbHideRemove=false;
            else
              $mbHideRemove=true;
          } else {
            $mbHideRemove=true;
          }
        }
        if ($mbHideRemove) {
          $maNotAllowed[] = 'perm_to_remove';
        }
        
        if((ISMSLib::getCurrentUserId() == $this->getFieldValue('risk_responsible_id')) || (ISMSLib::getCurrentUserId() == $this->getFieldValue('asset_responsible_id'))){
          if($this->getFieldValue('risk_accept_mode')){
            $maNotAllowed[] = 'treatment';
          }else{
            $maNotAllowed[] = 'cancel_treatment';
          }
        }else{
          $maNotAllowed[] = 'treatment';
          $maNotAllowed[] = 'cancel_treatment';
        }
        $moACL = FWDACLSecurity::getInstance();
        $moACL->setNotAllowed($maNotAllowed);

        $moMenu = FWDWebLib::getObject('menu');
        $moGrid = FWDWebLib::getObject('grid_risk');
        $msLine = $moGrid->getAttrName().'_acl_'.$this->ciRowIndex;
        FWDGridMenuACLSecurity::getInstance()->installGridMenuACL($moMenu,$moACL,$moGrid,$msLine);
 
        $miRiskValue = $this->getFieldValue('risk_residual');
        if ($this->getFieldValue('risk_accept_mode')) $msRiskColor = 'green';
        else $msRiskColor = RMRiskConfig::getRiskColor($miRiskValue);
        $this->coCellBox->setIconSrc("icon-risk_".$msRiskColor.".gif");
        $this->coCellBox->setObjFWDToolTip(ISMSLib::getInstance()->getToolTipCreateModify($this->getFieldValue('user_create_name'),$this->getFieldValue('date_create'),$this->getFieldValue('user_edit_name'),$this->getFieldValue('date_edit')));
        return parent::drawItem();
        break;
      
      case $this->getIndexByAlias('risk_name'): 
        $miRiskValue = $this->getFieldValue('risk_residual');
        $msLinkColor = RMRiskConfig::getRiskColor($miRiskValue,true);
        if ($this->getFieldValue('risk_accept_mode')) $msLinkColor = 'green';
        $msLink = "<font color=".$msLinkColor.">".$this->getFieldValue('risk_name')."</font>";
        $this->coCellBox->setAttrStringNoEscape("true");
        if(ISMSLib::userHasACL('M.RM.4.4') || ( ISMSLib::userHasACL('M.RM.4.9') && $this->getFieldValue('risk_responsible_id') == ISMSLib::getCurrentUserId() ) ){
          $this->coCellBox->setValue("<a href='javascript:open_risk_edit(".$this->getFieldValue('risk_id').");'>{$msLink}</a>");
        }else{
          $this->coCellBox->setValue("<a href='javascript:open_visualize(".$this->getFieldValue('risk_id').",".CONTEXT_RISK.",\"".uniqid()."\")'>{$msLink}</a>");
        }
        return $this->coCellBox->draw();
        break;

      case $this->getIndexByAlias('risk_asset_name'):
        $this->coCellBox->setAttrStringNoEscape("true");
        $msLink = $this->getFieldValue('risk_asset_name');
        if(ISMSLib::userHasACL('M.RM.3.5') || ( ISMSLib::userHasACL('M.RM.3.8') && $this->getFieldValue('asset_responsible_id') == ISMSLib::getCurrentUserId() ) ){
          $this->coCellBox->setValue("<a href='javascript:open_asset_edit(".$this->getFieldValue('asset_id').");'>{$msLink}</a>");
        }else{
          $this->coCellBox->setValue("<a href='javascript:open_visualize(".$this->getFieldValue('asset_id').",".CONTEXT_ASSET.",\"".uniqid()."\")'>{$msLink}</a>");
        }
        return $this->coCellBox->draw();
        break;
      
      case $this->getIndexByAlias('risk_control_count'):
        $this->coCellBox->setAttrStringNoEscape("true");
        if(!$this->coCellBox->getValue()) $this->coCellBox->setValue(0);
        if(ISMSLib::userHasACL('M.RM.4.2') && ISMSLib::userHasACL('M.RM.5')){
          $this->coCellBox->setValue("<a href='javascript:go_to_nav_control(".$this->getFieldValue('risk_id').");'>".$this->coCellBox->getValue()."</a>");
        }
        return $this->coCellBox->draw();
        break;
      case $this->getIndexByAlias('risk_accept_state'):
        $msAcceptType = "";
        if ($this->getFieldValue('risk_control_count') > 0) {
        	$msAcceptType = FWDLanguage::getPHPStringValue('gs_reduced','Reduzido');
        }
        switch ($this->getFieldValue('risk_accept_mode')) {
          case RISK_ACCEPT_MODE_ACCEPT   : $msAcceptType = FWDLanguage::getPHPStringValue('gs_restrained','Retido'); break;
          case RISK_ACCEPT_MODE_TRANSFER : $msAcceptType = FWDLanguage::getPHPStringValue('gs_transferred','Transferido'); break;
          case RISK_ACCEPT_MODE_AVOID    : $msAcceptType = FWDLanguage::getPHPStringValue('gs_avoided','Evitado'); break;
          default:break;
        }
        $this->coCellBox->setValue($msAcceptType);
        return parent::drawItem();
        break;

      case $this->getIndexByAlias('risk_value'):
        $miRiskValue = $this->getFieldValue('risk_value');
        $msLinkColor = RMRiskConfig::getRiskColor($miRiskValue,true);
        $miRiskValue = RMRiskConfig::getRiskValueFormat($miRiskValue);
        $msLink = "<font color=".$msLinkColor.">".$miRiskValue."</font>";
        $this->coCellBox->setValue($msLink);
        return parent::drawItem();
        break;
      
      case $this->getIndexByAlias('risk_residual'):
        $miRiskValue = $this->getFieldValue('risk_residual');
        $msLinkColor = RMRiskConfig::getRiskColor($miRiskValue,true);
        $miRiskValue = RMRiskConfig::getRiskValueFormat($miRiskValue);
        $msLink = "<font color=".$msLinkColor.">".$miRiskValue."</font>";
        $this->coCellBox->setValue($msLink);
        return parent::drawItem();
        break;
          
      case $this->getIndexByAlias('risk_state'):
        $this->coCellBox->setValue(ISMSContextObject::getContextStateAsString($this->coCellBox->getValue()));
        return $this->coCellBox->draw(); 
        break;

      case $this->getIndexByAlias('status_justification'):
        if (trim($this->coCellBox->getValue())) $this->coCellBox->setIconSrc("icon-detail.gif");
        else $this->coCellBox->setIconSrc("");
        return $this->coCellBox->draw();
      break;
      
      default:
        return parent::drawItem();
        break;
    }
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new ConfirmRemove('confirm_remove'));
    $moStartEvent->addAjaxEvent(new RemoveEvent('remove_event'));
    $moStartEvent->addAjaxEvent(new CancelRiskTreatmentEvent('cancel_risk_treatment_event'));
    $moStartEvent->addAjaxEvent(new SearchEvent('search'));
    
    
    //instala a seguran�a de ACL na p�gina
    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
    FWDMenuACLSecurity::getInstance()->installMenuACL(FWDWebLib::getObject('menu'),FWDACLSecurity::getInstance());
    
    $moGrid = FWDWebLib::getObject('grid_risk');
    $moHandler = new QueryGridRisk(FWDWebLib::getConnection());
    $miSessionAssetId = FWDWebLib::getObject("session_asset_id")->getValue();

    if(FWDWebLib::getObject('par_scrolling_filter')->getValue())
      FWDWebLib::getObject('filter_scrolling')->setValue(FWDWebLib::getObject('par_scrolling_filter')->getValue());
    
    if(FWDWebLib::getObject('par_control')->getValue()){
      $maAuxControlId = explode(':',FWDWebLib::getObject('par_control')->getValue());
      if(trim($maAuxControlId[0])!=''){
        $maAuxCtrl = explode(';',$maAuxControlId[0]);
        FWDWebLib::getObject('control_id')->setValue($maAuxCtrl[1]);
      }
      FWDWebLib::getObject('filter_scrolling_aux')->setValue(FWDWebLib::getObject('par_control')->getValue());
    }
    elseif($miSessionAssetId){ 
      FWDWebLib::getObject('asset_id')->setValue($miSessionAssetId);
      FWDWebLib::getObject('filter_scrolling')->setValue($miSessionAssetId);
    }
    elseif(FWDWebLib::getObject('par_scrolling_filter')->getValue()){
      $maIds = explode(':',FWDWebLib::getObject('par_scrolling_filter')->getValue());
      if(isset($maIds[0]))
        FWDWebLib::getObject('asset_id')->setValue($maIds[0]);
    }
    
    // Popula o select do filtro por ativo
    $moSelect = FWDWebLib::getObject('asset_filter');
    $moSelect->setQueryHandler(new QuerySelectAsset());
    $moSelect->populate();
    
    // Popula o select do filtro por estado
    $moSelect = FWDWebLib::getObject('state');
    $maStatus = array(CONTEXT_STATE_PENDANT,CONTEXT_STATE_APPROVED,CONTEXT_STATE_DENIED);
    foreach($maStatus as $miStatus){
      $moItem = new FWDItem();
      $moItem->setAttrKey($miStatus);
      $moItem->setValue(ISMSContextObject::getContextStateAsString($miStatus));
      $moSelect->addObjFWDItem($moItem);
    }
    
    // Seta os filtros
    if(strpos(FWDWebLib::getObject('search_in_name_only')->getValue(),'1')!==false){
      $moHandler->setName(FWDWebLib::convertToISO(FWDWebLib::getObject('name_filter')->getValue(),true));
    }else{
      $moHandler->setNameDescription(FWDWebLib::convertToISO(FWDWebLib::getObject('name_filter')->getValue(),true));
    }
    $moHandler->setMinRiskValue(FWDWebLib::getObject('min_risk_value')->getValue());
    $moHandler->setMaxRiskValue(FWDWebLib::getObject('max_risk_value')->getValue());
    $moHandler->setMinResidualValue(FWDWebLib::getObject('min_residual_value')->getValue());
    $moHandler->setMaxControls(FWDWebLib::getObject('max_controls')->getValue());
    $moHandler->setMinControls(FWDWebLib::getObject('min_controls')->getValue());
    $moHandler->setState(FWDWebLib::getObject('state')->getValue());
    $moHandler->setTreatmentTypes(array_filter(explode(':',FWDWebLib::getObject('treatment_type_controller')->getValue())));
    
    $miAssetId = FWDWebLib::getObject('asset_id')->getValue();
    $miControlId = trim(FWDWebLib::getObject('control_id')->getValue());
    
    if($miAssetId){
      FWDWebLib::getObject('asset_filter')->setValue($miAssetId);
      FWDWebLib::getObject('asset_filter')->setAttrDisabled('true');
    }else{
      $miAssetId = FWDWebLib::getObject('asset_filter')->getValue();
    }
    
    if($miAssetId || $miControlId){
      $moHandler->setAsset($miAssetId);
      $moHandler->setControl($miControlId);
    }elseif(!ISMSLib::userHasACL('M.RM.4.1')){
      $moHandler->setUserId(ISMSLib::getCurrentUserId());
    }
    
    $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    if($moSession->attributeExists('incidentid')){
      FWDWebLib::getObject('incident_filter')->setValue($moSession->getAttrIncidentId());
      $moSession->deleteAttribute('incidentid');
    }
    
    if(FWDWebLib::getObject('incident_filter')->getValue()){
      $moHandler->setIncident(FWDWebLib::getObject('incident_filter')->getValue());
    }
    
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new GridRisk());
    
    //adjustColumns();
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
  
    //adjustColumns();
    
    if(FWDWebLib::getObject('incident_filter')->getValue()){
      $moIncident = new CIIncident();
      $moIncident->fetchById(FWDWebLib::getObject('incident_filter')->getValue());
      FWDWebLib::getObject('tt_grid_risk')->setValue(FWDWebLib::getObject('tt_grid_risk')->getValue() . " " . str_replace('%incident_name%', $moIncident->getName(),FWDLanguage::getPHPStringValue('tt_incident_filter',"filtrados pelo incidente '<b>%incident_name%</b>'")));
    }

    FWDWebLib::getObject('search_in_name_only')->setValue('1');
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        function go_to_nav_control(piId){
          if(gebi('filter_scrolling_aux').value!=0){
            isms_change_to_sibling(5,'nav_control.php?scrollfilter='+gebi('filter_scrolling').value+'&risk='+gebi('filter_scrolling_aux').value+':'+CONTEXT_RISK+';'+piId);
          }else if(gebi('filter_scrolling').value!=0){
            isms_change_to_sibling(5,'nav_control.php?scrollfilter='+piId+':'+gebi('filter_scrolling').value);
          }else{
            isms_change_to_sibling(5,'nav_control.php?scrollfilter='+piId);
          }
        }

      
        function open_risk_edit(piId){
          isms_open_popup('popup_risk_edit','packages/risk/popup_risk_edit.php?risk='+piId+'&event=0','','true');
        }
        
        function open_asset_edit(piId){
          isms_open_popup('popup_asset_edit','packages/risk/popup_asset_edit.php?asset='+piId,'','true');
        }
        
        function refresh_grid(){
          js_refresh_grid('grid_risk');
        }
        function remove(){
          trigger_event('remove_event',3);
          refresh_grid();
        }
        function nav_risk_open_popup(psId,psPath,psDummy,psModal,piHeight,piWidth){
          isms_open_popup(psId,psPath,'',psModal,piHeight,piWidth);
          psId = psPath = psDummy = psModal = piHeight = piWidth = null;
        }
      </script>
    <?
      
    if(FWDWebLib::getObject('filter_scrolling')->getValue())
      if(FWDWebLib::getObject('filter_scrolling_aux')->getValue())
        ISMSLib::getPathScrollCode(RISK_MANAGEMENT_MODE,CONTEXT_RISK,FWDWebLib::getObject('filter_scrolling')->getValue(),false,'',FWDWebLib::getObject('filter_scrolling_aux')->getValue());
      else
        ISMSLib::getPathScrollCode(RISK_MANAGEMENT_MODE,CONTEXT_ASSET,FWDWebLib::getObject('filter_scrolling')->getValue(),false,'',FWDWebLib::getObject('filter_scrolling_aux')->getValue());
    else
      ISMSLib::getPathScrollCode(RISK_MANAGEMENT_MODE,CONTEXT_RISK,FWDWebLib::getObject('filter_scrolling')->getValue(),false,'',FWDWebLib::getObject('filter_scrolling_aux')->getValue());
      
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_risk.xml");

?>
