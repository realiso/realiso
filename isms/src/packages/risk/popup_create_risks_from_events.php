<?php
include_once "include.php";
include_once $handlers_ref . "QueryCategoryParentsEvents.php";

set_time_limit(300);

class GridEvents extends FWDDrawGrid {
  public function DrawItem(){
    switch($this->ciColumnIndex){
      case 1:
        $msGfxRef = FWDWebLib::getInstance()->getGfxRef();
        $moIcon = new FWDIcon(new FWDBox(4,12));
        $moIcon->setAttrSrc("{$msGfxRef}icon-event.gif");
        return $moIcon->draw();
        break;
      
      case 2:
        $this->coCellBox->setAttrStringNoEscape("true");
        $msUniqId = uniqid();
        $this->coCellBox->setValue("<a href='javascript:open_visualize(".$this->caData[1].",".CONTEXT_EVENT.",\"".uniqid()."\")'>{$this->caData[2]}</a>");
        return $this->coCellBox->draw();
        break;
      default:
        return parent::drawItem();
        break;
    }
  }
}

class CreateRisksFromEventsEvent extends FWDRunnable {
  public function run() {
    $moGrid = FWDWebLib::getObject('grid_events');
    
    //teste para verificar se o sistema n�o est� sendo hakeado
    $moCtxUserTest = new RMAsset();
    $moCtxTest = new RMAsset();
    $moCtxTest->fetchById(FWDWebLib::getObject("param_asset_id")->getValue());
    //teste se o usu�rio tem permiss�o para editar um ativo
    $moCtxUserTest->testPermissionToEdit(FWDWebLib::getObject("param_asset_id")->getValue(),$moCtxTest->getResponsible());
    
    $moQuery = new QueryCategoryParentsEvents(FWDWebLib::getConnection());
    $moQuery->setCategory(FWDWebLib::getObject("param_category_id")->getValue());
    $moQuery->setAsset(FWDWebLib::getObject("param_asset_id")->getValue());
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    /* php 5.0.x nao possui a fun��o array_diff_key */
    $maEvents = FWDWebLib::fwd_array_diff_key($moQuery->getEvents(),array_flip($moGrid->getValue()));
    
    $miAssetId = FWDWebLib::getObject("param_asset_id")->getValue();
    $miCategoryId = FWDWebLib::getObject("param_category_id")->getValue();
    
    $moRisk = new RMRisk();
    $moRisk->setFieldValue("risk_value",0);
    $moRisk->setFieldValue("risk_residual_value",0);
    foreach ($maEvents as $miEventId => $maValues) {
      $moRisk->setFieldValue("risk_event_id",$miEventId);
      $moRisk->setFieldValue("risk_asset_id",$miAssetId);
      $moRisk->setFieldValue("risk_name",$maValues['event_description'],false);
      $moRisk->setFieldValue("risk_impact",$maValues['event_impact'],false);
      
      $moRisk->insert();
    }
    
    echo "soWindow = soPopUpManager.getPopUpById('popup_create_risks_from_events').getOpener();"
        ."if (soWindow) {"// esta window apenas existir� se esta popup foi aberta pela
                          // edi��o de um ativo diretamente da nav_asset (sem passar
                          // antes pela popup de visualiza��o do ativo - se passasse
                          // pela popup de visualiza��o, n�o ocorreria o refresh da grid
                          // da nav_asset pois as popups de edi��o e de visualiza��o
                          // ja teriam sido fechadas, e a refer�ncia para o refresh_grid()
                          // teria se perdido.
        ."  if (soWindow.refresh_grid)"
        ."    soWindow.refresh_grid();"
        ."}"
        ."soPopUpManager.closePopUp('popup_create_risks_from_events');";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent("")); 
    $moStartEvent->addAjaxEvent(new CreateRisksFromEventsEvent("create_risks_from_events_event"));

    $miCategoryId = FWDWebLib::getObject("var_category_id")->getValue();
    if(!$miCategoryId)
      $miCategoryId = FWDWebLib::getObject("param_category_id")->getValue();  

    $miAssetId = FWDWebLib::getObject("var_asset_id")->getValue();
    if(!$miAssetId)
      $miAssetId = FWDWebLib::getObject("param_asset_id")->getValue();  

    $moCategory = new RMCategory();
    $moCategory->fetchById($miCategoryId);
    FWDWebLib::getObject("category_name")->setValue($moCategory->getFieldValue("category_name"));
    
    $moGrid = FWDWebLib::getObject("grid_events");
    $moGrid->setObjFwdDrawGrid(new GridEvents());
    
    $moQuery = new QueryCategoryParentsEvents(FWDWebLib::getConnection());
    $moQuery->setCategory($miCategoryId);
    $moQuery->setAsset($miAssetId);
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    $miRow = 1;
    foreach ($moQuery->getEvents() as $miEventId => $maValues) {
      $moGrid->setItem(1,$miRow,$miEventId);
      $moGrid->setItem(2,$miRow,$maValues['event_description']);
      $moGrid->setItem(3,$miRow++,$maValues['event_impact']);
    }


  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $miAssetId = FWDWebLib::getObject("param_asset_id")->getValue();
    if($miAssetId)
      FWDWebLib::getObject('var_asset_id')->setValue($miAssetId);
    
    
    $miCategoryId = FWDWebLib::getObject("param_category_id")->getValue();
    if($miCategoryId)
      FWDWebLib::getObject('var_category_id')->setValue($miCategoryId);
    
    
     //teste para verificar se o sistema n�o est� sendo hakeado
    $moCtxUserTest = new RMAsset();
    $moAsset = new RMAsset();
    $moAsset->fetchById($miAssetId);
    //teste se o usu�rio tem permiss�o para editar um ativo
    $moCtxUserTest->testPermissionToEdit($miAssetId,$moAsset->getResponsible());

    //testa se o usu�rio tem permiss�o para inserir um risco
    $moCtxUserTest2 = new RMRisk();
    $moCtxUserTest2->testPermissionToInsert();
    

    FWDWebLib::getObject("asset_name")->setValue($moAsset->getFieldValue("asset_name"));
    

    
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
?>
<script language="javascript">
  function refresh_grid() {
    js_refresh_grid('grid_events');
  }
</script>
<?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_create_risks_from_events.xml");
?>