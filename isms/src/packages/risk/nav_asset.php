<?php
include_once "include.php";
include_once $handlers_ref . "grid/QueryGridAsset.php";
include_once $handlers_ref . "QueryTotalAssets.php";
include_once $handlers_ref . "select/QuerySelectAssetResponsibles.php";
include_once $handlers_ref . "select/QuerySelectCategory.php";
include_once $handlers_ref . "select/QuerySelectAssetAsset.php";

class SearchEvent extends FWDRunnable {
  public function run(){
    $moGrid = FWDWebLib::getObject('grid_asset');
    $moGrid->execEventPopulate();
  }
}

class ConfirmRemove extends FWDRunnable {
  public function run(){
    $msTitle = FWDLanguage::getPHPStringValue('tt_remove_asset','Remover Ativo');
    $msMessage = FWDLanguage::getPHPStringValue('st_asset_remove_confirm',"Voc� tem certeza que deseja remove o ativo <b>%asset_name%</b>?");

    $moAsset = new RMAsset();
    $moAsset->fetchById(FWDWebLib::getObject('selected_asset_id')->getValue());
    $msAssetName = ISMSLib::truncateString($moAsset->getFieldValue('asset_name'), 70);
    $msMessage = str_replace("%asset_name%",$msAssetName,$msMessage);

    $msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
                    "soWindow = soPopUp.getOpener();" .
                    "soWindow.remove();";

    ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue);
  }
}

class RemoveEvent extends FWDRunnable {
  public function run(){
    $miSelectedId = FWDWebLib::getObject('selected_asset_id')->getValue();
    $moAsset = new RMAsset();
    $moAsset->delete($miSelectedId);
    /*
     * Se possuir o m�dulo de documenta��o atualiza os leitores do documento do ativo.
     */
    if (FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(POLICY_MODE))
    $moAsset->updateReaders($miSelectedId);
  }
}
class GridAsset extends FWDDrawGrid {

  public function drawItem(){
    switch($this->ciColumnIndex){
      case $this->getIndexByAlias('asset_value'):
        $maDeniedACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
        $mbHideEdit = false;
        $mbHideRemove = false;
        $mbHideAssociateProcess = false;

        if(!ISMSLib::userHasACL('M.RM.3.5')){
          if(ISMSLib::userHasACL('M.RM.3.8')){
            if ($this->getFieldValue('asset_responsible_id') == ISMSLib::getCurrentUserId())
            $mbHideEdit=false;
            else
            $mbHideEdit=true;
          } else {
            $mbHideEdit=true;
          }
        }
        if(!ISMSLib::userHasACL('M.RM.3.6')){
          if(ISMSLib::userHasACL('M.RM.3.9')){
            if ($this->getFieldValue('asset_responsible_id') == ISMSLib::getCurrentUserId())
            $mbHideRemove=false;
            else
            $mbHideRemove=true;
          } else {
            $mbHideRemove=true;
          }
        }
        if($this->getFieldValue('asset_responsible_id') != ISMSLib::getCurrentUserId()){
          $mbHideAssociateProcess = true;
        }
        if ($mbHideEdit || $mbHideRemove || $mbHideAssociateProcess) {
          $moACL = FWDACLSecurity::getInstance();
          $maDenied = array();
          if($mbHideEdit)
          $maDenied[] = 'perm_to_edit';
          if($mbHideRemove)
          $maDenied[] = 'perm_to_remove';
          if($mbHideAssociateProcess)
          $maDenied[] = 'perm_to_associate_process';

          $moACL->setNotAllowed($maDenied);
          $moMenu = FWDWebLib::getObject('menu');
          $moGrid = FWDWebLib::getObject('grid_asset');
          $msLine = $moGrid->getAttrName().'_acl_'.$this->ciRowIndex;
          FWDGridMenuACLSecurity::getInstance()->installGridMenuACL($moMenu,$moACL,$moGrid,$msLine);
        }

        $miAssetValue = $this->caData[1];
        $msAssetColor = RMRiskConfig::getRiskColor($miAssetValue);
        $this->coCellBox->setIconSrc("icon-asset_".$msAssetColor.".gif");
        $this->coCellBox->setObjFWDToolTip(ISMSLib::getInstance()->getToolTipCreateModify($this->getFieldValue('user_create_name'),$this->getFieldValue('date_create'),$this->getFieldValue('user_edit_name'),$this->getFieldValue('date_edit')));
        return parent::drawItem();
        break;

      case $this->getIndexByAlias('asset_name'):
        $miAssetValue = $this->caData[1];
        $msLinkColor = RMRiskConfig::getRiskColor($miAssetValue,true);
        $msLink = "<font color=".$msLinkColor.">".$this->caData[2]."</a>";
        $this->coCellBox->setAttrStringNoEscape("true");
        if(ISMSLib::userHasACL('M.RM.3.2')){
          $this->coCellBox->setValue("<a href='javascript:go_to_nav_risk(".$this->getFieldValue('asset_id').");'>{$msLink}</a>");
        }
        return $this->coCellBox->draw();
        break;

      case $this->getIndexByAlias('asset_risk_count'):
        $this->coCellBox->setAttrStringNoEscape("true");
        if(!$this->coCellBox->getValue()) $this->coCellBox->setValue(0);
        if(ISMSLib::userHasACL('M.RM.3.2')){
          $this->coCellBox->setValue("<a href='javascript:go_to_nav_risk(".$this->getFieldValue('asset_id').");'>".$this->coCellBox->getValue()."</a>");
        }
        return $this->coCellBox->draw();
        break;
      case $this->getIndexByAlias('asset_process_count'):
        $this->coCellBox->setAttrStringNoEscape("true");
        if(!$this->coCellBox->getValue()) $this->coCellBox->setValue(0);
        if(ISMSLib::userHasACL('M.RM.2')){
          $this->coCellBox->setValue("<a href='javascript:go_to_nav_process(".$this->getFieldValue('asset_id').");'>".$this->coCellBox->getValue()."</a>");
        }
        return $this->coCellBox->draw();
        break;

      // corre��o do bug http://informant.zendesk.com/tickets/1147.
      case $this->getIndexByAlias('asset_dependents_count'):
        $this->coCellBox->setAttrStringNoEscape("true");
        if( ISMSLib::userHasACL('M.RM.3.6')
        || ( ISMSLib::userHasACL('M.RM.3.9') && $this->getFieldValue('asset_responsible_id') == ISMSLib::getCurrentUserId() )
        ){
          $this->coCellBox->setValue("<a href='javascript:go_to_nav_asset_dependents(".$this->getFieldValue('asset_id').");'>".$this->coCellBox->getValue()."</a>");
        }
        return $this->coCellBox->draw();
        break;
      case $this->getIndexByAlias('asset_dependencies_count'):
        $this->coCellBox->setAttrStringNoEscape("true");
        if( ISMSLib::userHasACL('M.RM.3.6')
        || ( ISMSLib::userHasACL('M.RM.3.9') && $this->getFieldValue('asset_responsible_id') == ISMSLib::getCurrentUserId() )
        ){
          $this->coCellBox->setValue("<a href='javascript:go_to_nav_asset_depencencies(".$this->getFieldValue('asset_id').");'>".$this->coCellBox->getValue()."</a>");
        }
        return $this->coCellBox->draw();
        break;

      case $this->getIndexByAlias('asset_state'):
        $this->coCellBox->setValue(ISMSContextObject::getContextStateAsString($this->coCellBox->getValue()));
        return $this->coCellBox->draw();
        break;

      default:
        return parent::drawItem();
        break;
    }
  }
}

class DuplicateEvent extends FWDRunnable {
  public function run() {
    $miAssetId = FWDWebLib::getObject('selected_asset_id')->getValue();
    $moAsset = new RMAsset();
    $moAsset->fetchById($miAssetId);
    $msNewAssetName = $moAsset->getFieldValue('asset_name') .' - '.FWDLanguage::getPHPStringValue('st_asset_copy','C�pia');

    $moNewAsset = new RMAsset();
    $moNewAsset->setFieldValue('asset_security_responsible_id',$moAsset->getFieldValue('asset_security_responsible_id'));
    $moNewAsset->setFieldValue('asset_responsible_id',$moAsset->getFieldValue('asset_responsible_id'));
    $moNewAsset->setFieldValue('asset_category_id',$moAsset->getFieldValue('asset_category_id'));
    $moNewAsset->setFieldValue('asset_name',$msNewAssetName,false);
    $moNewAsset->setFieldValue('asset_description',$moAsset->getFieldValue('asset_description'),false);
    $moNewAsset->setFieldValue('asset_document',$moAsset->getFieldValue('asset_document'));
    $moNewAsset->setFieldValue('asset_cost',$moAsset->getFieldValue('asset_cost'));
    $moNewAsset->setFieldValue('asset_legality',$moAsset->getFieldValue('asset_legality'));
    $moNewAsset->setFieldValue('asset_justification',$moAsset->getFieldValue('asset_justification'),false);
    $miNewAssetId = $moNewAsset->insert(true);

    $moRisk = new RMRisk();
    $moRisk->createFilter($miAssetId,'risk_asset_id');
    $moRisk->select();
    while($moRisk->fetch()) {
      if ($moRisk->getContextState($moRisk->getFieldValue('risk_id'))!=CONTEXT_STATE_DELETED) {
        $moRisk->duplicateRiskOnAsset($miNewAssetId);
      }
    }
    echo "refresh_grid(); isms_open_popup('popup_asset_edit','packages/risk/popup_asset_edit.php?asset={$miNewAssetId}','','true');";
  }
}

class ScreenBeforeEvent extends FWDRunnable {

  protected function populateGridQuery(){
    $query = new FWDDBDataSet(FWDWebLib::getConnection());
    $query->setQuery("truncate table asset_dependencies_cache");
    $query->execute();

    $query = new FWDDBDataSet(FWDWebLib::getConnection());
    $query->setQuery("select fkcontext as id from view_rm_asset_active");
    $query->addFWDDBField(new FWDDBField('id','id'  ,DB_NUMBER));
    $query->execute();

    QuerySelectAssetAsset::load();

    $queryInsert = new FWDDBDataSet(FWDWebLib::getConnection());
    while($query->fetch()){
      $id = $query->getFieldByAlias("id")->getValue();
      $dependencies = QuerySelectAssetAsset::getAssetDependenciesCount($id);
      $dependents = QuerySelectAssetAsset::getAssetDependentsCount($id);
      $queryInsert->setQuery("insert into asset_dependencies_cache (fkasset, asset_dependents_count, asset_dependencies_count) values (".$id.",".$dependents.",".$dependencies.")");
      $queryInsert->execute();
    }
  }

  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
    $moStartEvent->addAjaxEvent(new ConfirmRemove('confirm_remove'));
    $moStartEvent->addAjaxEvent(new RemoveEvent('remove_event'));
    $moStartEvent->addAjaxEvent(new SearchEvent('search'));
    $moStartEvent->addAjaxEvent(new DuplicateEvent('duplicate_event'));
    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
    FWDMenuACLSecurity::getInstance()->installMenuACL(FWDWebLib::getObject('menu'),FWDACLSecurity::getInstance());

    if(isset($_POST["fwd_ajax_event_name"])){
      $order = $_POST["fwd_ajax_event_name"];

      if($order == 'grid_assetpopulate' || $order == "grid_assetrefresh"){
        $this->populateGridQuery();
      }
    }

    $moGrid = FWDWebLib::getObject('grid_asset');
    $moHandler = new QueryGridAsset(FWDWebLib::getConnection());

    $miSessionProcessId = FWDWebLib::getObject("session_process_id")->getValue();
    if ($miSessionProcessId){
      FWDWebLib::getObject('process_id')->setValue($miSessionProcessId);
      $moProcess = new RMProcess();
      if($moProcess->fetchById($miSessionProcessId)){
        FWDWebLib::getObject('filter_scrolling')->setValue($miSessionProcessId);
        $miAreaId = $moProcess->getFieldValue('process_area_id');
        if($miAreaId)
        FWDWebLib::getObject('filter_scrolling')->setValue($miSessionProcessId.':'.$miAreaId);
      }
      else
      FWDWebLib::getObject('filter_scrolling')->setValue($miSessionProcessId);

    }elseif(FWDWebLib::getObject('par_scrolling_filter')->getValue()){
      $maIds = explode(':',FWDWebLib::getObject('par_scrolling_filter')->getValue());
      if(isset($maIds[0]))
      FWDWebLib::getObject('process_id')->setValue($maIds[0]);
      FWDWebLib::getObject('filter_scrolling')->setValue(FWDWebLib::getObject('par_scrolling_filter')->getValue());
    }
     
    // Popula o select do filtro por respons�vel
    $moSelect = FWDWebLib::getObject('responsible');
    $moSelectHandler = new QuerySelectAssetResponsibles();
    if(!ISMSLib::userHasACL('M.RM.3.1')){
      $moSelectHandler->setUserId(ISMSLib::getCurrentUserId());
    }
    $moSelect->setQueryHandler($moSelectHandler);
    $moSelect->populate();

    // Popula o select do filtro por categoria
    $moSelect = FWDWebLib::getObject('category');
    $moSelect->setQueryHandler(new QuerySelectCategory());
    $moSelect->populate();

    // Popula o select do filtro por estado
    $moSelect = FWDWebLib::getObject('state');
    $maStatus = array(CONTEXT_STATE_PENDANT,CONTEXT_STATE_APPROVED,CONTEXT_STATE_DENIED);
    foreach($maStatus as $miStatus){
      $moItem = new FWDItem();
      $moItem->setAttrKey($miStatus);
      $moItem->setValue(ISMSContextObject::getContextStateAsString($miStatus));
      $moSelect->addObjFWDItem($moItem);
    }

    // Seta os filtros
    if(strpos(FWDWebLib::getObject('search_in_name_only')->getValue(),'1')!==false){
      $moHandler->setName(FWDWebLib::convertToISO(FWDWebLib::getObject('name_filter')->getValue(),true));
    }else{
      $moHandler->setNameDescription(FWDWebLib::convertToISO(FWDWebLib::getObject('name_filter')->getValue(),true));
    }
    $moHandler->setResponsible(FWDWebLib::getObject('responsible')->getValue());
    $moHandler->setCategory(FWDWebLib::getObject('category')->getValue());
    $moHandler->setMaxProcesses(FWDWebLib::getObject('max_processes')->getValue());
    $moHandler->setMinProcesses(FWDWebLib::getObject('min_processes')->getValue());
    $moHandler->setMaxRisks(FWDWebLib::getObject('max_risks')->getValue());
    $moHandler->setMinRisks(FWDWebLib::getObject('min_risks')->getValue());
    $moHandler->setValueFilter(array_filter(explode(':',FWDWebLib::getObject('value_filter_controller')->getValue())));
    $moHandler->setState(FWDWebLib::getObject('state')->getValue());

    $miProcessId = FWDWebLib::getObject('process_id')->getValue();
    if($miProcessId){
      $moHandler->setProcess($miProcessId);
    }else{
      if(!ISMSLib::userHasACL('M.RM.3.1')){
        $moHandler->setUserId(ISMSLib::getCurrentUserId());
      }
    }

    /*filtro de ativos dependentes / depend�ncias*/
    if(FWDWebLib::getObject('par_asset_id')->getValue() && FWDWebLib::getObject('par_asset_filter_type')->getValue()){
      $moAsset = new RMAsset();
      $moAsset->fetchById(FWDWebLib::getObject('par_asset_id')->getValue());
      if( ISMSLib::userHasACL('M.RM.3.6')
      || ( ISMSLib::userHasACL('M.RM.3.9') && $moAsset->getResponsible() == ISMSLib::getCurrentUserId() )
      ){
        $moHandler->setAsset(FWDWebLib::getObject('par_asset_id')->getValue());
        if( FWDWebLib::getObject('par_asset_filter_type')->getValue() == 'dependents'){
          $moHandler->showDependents(true);
        }elseif( FWDWebLib::getObject('par_asset_filter_type')->getValue() == 'dependencies' ){
          $moHandler->showDependencies(true);
        }

      }
    }
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new GridAsset());
  }
}

class ScreenEvent extends FWDRunnable {

  public function run(){
    FWDWebLib::getObject('search_in_name_only')->setValue('1');

    if(FWDWebLib::getObject('par_asset_id')->getValue() && FWDWebLib::getObject('par_asset_filter_type')->getValue()){
      $moAsset = new RMAsset();
      $moAsset->fetchById(FWDWebLib::getObject('par_asset_id')->getValue());
      if( ISMSLib::userHasACL('M.RM.3.6')
      || ( ISMSLib::userHasACL('M.RM.3.9') && $moAsset->getResponsible() == ISMSLib::getCurrentUserId() )
      ){
        $msGridName = FWDWebLib::getObject('asset_grid_name')->getValue();
        switch(FWDWebLib::getObject('par_asset_filter_type')->getValue()){
          case 'dependents':
            $msGridName .=" ".FWDLanguage::getPHPStringValue('grid_asset_filter_dependents',"(dependentes do ativo '<b>%asset_name%</b>')");
            break;
          case 'dependencies':
            $msGridName .=" ".FWDLanguage::getPHPStringValue('grid_asset_filter_dependents',"(depend�ncias do ativo '<b>%asset_name%</b>')");
            break;
          default:
            break;
        }
        FWDWebLib::getObject('asset_grid_name')->setValue(str_replace('%asset_name%',$moAsset->getName(),$msGridName));
      }
    }

    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
<script language="javascript">
          function refresh_grid(){
            js_refresh_grid('grid_asset');
          }
          function remove(){
            trigger_event('remove_event',3);
            refresh_grid();
          }
        function nav_asset_open_popup(psId,psPath,psDummy,psModal,piHeight,piWidth){
          setTimeout(
            function(){
              isms_open_popup(psId,psPath,'',psModal,piHeight,piWidth);
              psId = psPath = psDummy = psModal = piHeight = piWidth = null;
            },
            1
          );
        }
          
        function nav_risk_open_popup(psId,psPath,psDummy,psModal,piHeight,piWidth){
          isms_open_popup(psId,psPath,'',psModal,piHeight,piWidth);
          psId = psPath = psDummy = psModal = piHeight = piWidth = null;
        }
        function go_to_nav_risk(piId){
          if(gebi('filter_scrolling').value!=0){
            isms_change_to_sibling(4,'nav_risk.php?scrollfilter='+piId+':'+gebi('filter_scrolling').value);
          }else{
            isms_change_to_sibling(4,'nav_risk.php?scrollfilter='+piId);
          }
        }
        function go_to_nav_process(piId){
          isms_change_to_sibling(2,'nav_process.php?asset_id='+piId);
        }
        
        function go_to_nav_asset_dependents(piId){
          isms_change_to_sibling(3,'nav_asset.php?asset_id='+piId+'&asset_filter_type=dependents');
        }
        
        function go_to_nav_asset_depencencies(piId){
          isms_change_to_sibling(3,'nav_asset.php?asset_id='+piId+'&asset_filter_type=dependencies');
        }
        
      </script>
    <?
    if(FWDWebLib::getObject('filter_scrolling')->getValue())
    ISMSLib::getPathScrollCode(RISK_MANAGEMENT_MODE,CONTEXT_PROCESS,FWDWebLib::getObject('filter_scrolling')->getValue(),false,'');
    else
    ISMSLib::getPathScrollCode(RISK_MANAGEMENT_MODE,CONTEXT_ASSET,'',false,'');
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_asset.xml");
?>
