<?php
include_once "include.php";
include_once $handlers_ref . "grid/QueryGridControl.php";

class SearchEvent extends FWDRunnable {
  public function run(){
    $moGrid = FWDWebLib::getObject('grid_control');
    $moGrid->execEventPopulate();
  }
}

class GridControl extends FWDDrawGrid {
  public function drawItem(){    
    switch($this->ciColumnIndex){    
      case $this->getIndexByAlias('control_id'):
        $maDeniedACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
        $mbHideEdit = false;
        $mbHideRemove = false;
        
        if(!ISMSLib::userHasACL('M.RM.5.4')){
          if(ISMSLib::userHasACL('M.RM.5.7')){
            if ($this->getFieldValue('control_responsible_id') == ISMSLib::getCurrentUserId())
              $mbHideEdit=false;
            else
              $mbHideEdit=true;
          } else {
            $mbHideEdit=true;
          }
        }
        if(!ISMSLib::userHasACL('M.RM.5.5')){
          if(ISMSLib::userHasACL('M.RM.5.8')){
            if ($this->getFieldValue('control_responsible_id') == ISMSLib::getCurrentUserId())
              $mbHideRemove=false;
            else
              $mbHideRemove=true;
          } else {
            $mbHideRemove=true;
          }
        }

        if ($mbHideEdit || $mbHideRemove) {
          $moACL = FWDACLSecurity::getInstance();
          $maDenied = array();
          if($mbHideEdit)
            $maDenied[] = 'perm_to_edit';
          if($mbHideRemove)
            $maDenied[] = 'perm_to_remove';
          $moACL->setNotAllowed($maDenied);
          $moMenu = FWDWebLib::getObject('menu');
          $moGrid = FWDWebLib::getObject('grid_control');
          $msLine = $moGrid->getAttrName().'_acl_'.$this->ciRowIndex;
          FWDGridMenuACLSecurity::getInstance()->installGridMenuACL($moMenu,$moACL,$moGrid,$msLine);
        }

        if($this->getFieldValue('context_is_active')==1) 
          $this->coCellBox->setIconSrc("icon-control.gif");
        else
          $this->coCellBox->setIconSrc("icon-control_red.gif");
        $this->coCellBox->setObjFWDToolTip(ISMSLib::getInstance()->getToolTipCreateModify($this->getFieldValue('user_create_name'),$this->getFieldValue('date_create'),$this->getFieldValue('user_edit_name'),$this->getFieldValue('date_edit')));
        return parent::drawItem();
        break;
      
      case $this->getIndexByAlias('control_name'):
        $this->coCellBox->setAttrStringNoEscape("true");
        if(ISMSLib::userHasACL('M.RM.5.4') || (ISMSLib::userHasACL('M.RM.5.7') && ISMSLib::getCurrentUserId() == $this->getFieldValue('control_responsible_id') )){
          $this->coCellBox->setValue("<a href='javascript:open_edit(".$this->getFieldValue('control_id').");'>".$this->getFieldValue('control_name')."</a>");
        }else{
          $this->coCellBox->setValue("<a href='javascript:open_visualize(".$this->getFieldValue('control_id').",".CONTEXT_CONTROL.",\"".uniqid()."\")'>{$this->getFieldValue('control_name')}</a>");
        }
        return $this->coCellBox->draw(); 
        break;
        
      case $this->getIndexByAlias('control_cost'):
         $moConfig = new ISMSConfig();
         if($moConfig->getConfig(GENERAL_COST_ENABLED)){
            $this->coCellBox->setValue(number_format($this->getFieldValue('control_cost'),2));
            if(ISMSLib::userHasACL('M.RM.5.4') || (ISMSLib::userHasACL('M.RM.5.7') && ISMSLib::getCurrentUserId() == $this->getFieldValue('control_responsible_id') )){
              $this->coCellBox->setAttrStringNoEscape("true");
              $this->coCellBox->setValue("<a href='javascript:open_control_cost(".$this->getFieldValue('control_id').");'>".$this->coCellBox->getValue()."</a>");
            }
            return $this->coCellBox->draw();
         }
        break;
      
      case $this->getIndexByAlias('risk_count'):
           $this->coCellBox->setValue(intval($this->getFieldValue('risk_count')));
           $this->coCellBox->setAttrStringNoEscape("true");
           if(ISMSLib::userHasACL('M.RM.5.2') || ISMSLib::userHasACL('M.RM.4')){
            $this->coCellBox->setValue("<a href='javascript:go_to_nav_risk(".$this->getFieldValue('control_id').");'>".$this->coCellBox->getValue()."</a>");
           }
           return $this->coCellBox->draw();
        break;
      
      case $this->getIndexByAlias('control_state'):
        $this->coCellBox->setValue(ISMSContextObject::getContextStateAsString($this->coCellBox->getValue()));
        return $this->coCellBox->draw(); 
      break;
      
      default:
        return parent::drawItem();
        break;
    }
  }
}

class ControlConfirmRemove extends FWDRunnable {
  public function run(){
    $miControlId = FWDWebLib::getObject('selected_control_id')->getValue();    
    
    $moControl = new RMControl();
    $moControl->fetchById($miControlId);
    $msControlName = ISMSLib::truncateString($moControl->getFieldValue('control_name'), 70);
    
    $msTitle = FWDLanguage::getPHPStringValue('tt_remove_control','Remover Controle');
    $msMessage = FWDLanguage::getPHPStringValue('st_control_remove_confirm',"Voc� tem certeza de que deseja remover o controle <b>%control_name%</b>?");
    $msMessage = str_replace('%control_name%', $msControlName, $msMessage);
           
    $msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
            "soWindow = soPopUp.getOpener();" .
            "soWindow.remove_control();";
            
    ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,50);
  }
}

class RemoveControlEvent extends FWDRunnable {
  public function run(){    
    $miControlId = FWDWebLib::getObject('selected_control_id')->getValue();    
    $moControl = new RMControl();
    $moControl->delete($miControlId);
    /*
     * Se possuir o m�dulo de documenta��o atualiza os leitores do documento do processo. 
     */
    if (FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(POLICY_MODE))
      $moControl->updateReaders($miControlId);
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    //teste para ver se o custo esta abilitado no sistema
    $moConfig = new ISMSConfig();
    if(!$moConfig->getConfig(GENERAL_COST_ENABLED)){
      FWDWebLib::getObject('col_grid_control_cost')->setAttrDisplay('false');
      FWDWebLib::getObject('col_grid_control_cost')->setShouldDraw(false);
      FWDWebLib::getObject('col_grid_control_cost')->getObjFWDBox()->setAttrWidth(1);
    }
    
    
    
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new ControlConfirmRemove('control_confirm_remove'));
    $moStartEvent->addAjaxEvent(new RemoveControlEvent('remove_control_event'));
    $moStartEvent->addAjaxEvent(new SearchEvent('search'));
    //instala a seguran�a de ACL na p�gina
    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
    FWDMenuACLSecurity::getInstance()->installMenuACL(FWDWebLib::getObject('menu'),FWDACLSecurity::getInstance());
    
    
    $moGrid = FWDWebLib::getObject("grid_control");
    $moHandler = new QueryGridControl(FWDWebLib::getConnection());
    
    $miSessionRiskId = FWDWebLib::getObject("session_risk_id")->getValue();
    if($miSessionRiskId){
      FWDWebLib::getObject('risk_id')->setValue($miSessionRiskId);
    }elseif(FWDWebLib::getObject('par_scrolling_filter')->getValue()){
      $maIds = explode(':',FWDWebLib::getObject('par_scrolling_filter')->getValue());
      if(isset($maIds[0])){
        FWDWebLib::getObject('risk_id')->setValue($maIds[0]);
      }
      FWDWebLib::getObject('filter_scrolling')->setValue(FWDWebLib::getObject('par_scrolling_filter')->getValue());
    }
    if(trim(FWDWebLib::getObject('par_risk')->getValue())){
      $maAuxControlId = explode(':',FWDWebLib::getObject('par_risk')->getValue());
      $maAuxCtrl = explode(';',$maAuxControlId[count($maAuxControlId)-1]);
      FWDWebLib::getObject('risk_id')->setValue($maAuxCtrl[1]);
      FWDWebLib::getObject('filter_scrolling_aux')->setValue(FWDWebLib::getObject('par_risk')->getValue());
    }
    
    $moHandler->setRisk(FWDWebLib::getObject('risk_id')->getValue());
    
    if(!ISMSLib::userHasACL('M.RM.5.1')){
      $moHandler->setUserId(ISMSLib::getCurrentUserId());
    }
    
    // Popula o select do filtro por estado
    $moSelect = FWDWebLib::getObject('state');
    $maStatus = array(CONTEXT_STATE_PENDANT,CONTEXT_STATE_APPROVED,CONTEXT_STATE_DENIED);
    foreach($maStatus as $miStatus){
      $moItem = new FWDItem();
      $moItem->setAttrKey($miStatus);
      $moItem->setValue(ISMSContextObject::getContextStateAsString($miStatus));
      $moSelect->addObjFWDItem($moItem);
    }
    
    // Seta os filtros
    if(strpos(FWDWebLib::getObject('search_in_name_only')->getValue(),'1')!==false){
      $moHandler->setName(FWDWebLib::convertToISO(FWDWebLib::getObject('name_filter')->getValue(),true));
    }else{
      $moHandler->setNameDescription(FWDWebLib::convertToISO(FWDWebLib::getObject('name_filter')->getValue(),true));
    }
    $moHandler->setMaxRisks(FWDWebLib::getObject('max_risks')->getValue());
    $moHandler->setMinRisks(FWDWebLib::getObject('min_risks')->getValue());
    $moHandler->setState(FWDWebLib::getObject('state')->getValue());
    $moHandler->setIsActive(str_replace(':','',FWDWebLib::getObject('active_filter')->getValue()));
    
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new GridControl()); 
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    FWDWebLib::getObject('active_filter')->setValue(':0');
    FWDWebLib::getObject('search_in_name_only')->setValue('1');
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
    <script language="javascript">
    function refresh_grid() {
      js_refresh_grid('grid_control');    
    }
    function remove_control() {
      trigger_event('remove_control_event', 3);
      refresh_grid();        
    }
    function refresh_grids(){
      refresh_grid();
    }
    function open_task_control_revision(task_id,context_id){
      isms_open_popup('popup_control_revision_task', 'packages/risk/popup_control_revision_task.php?task='+task_id+'&control='+context_id, '', 'true');
    }
    function open_task_control_test(task_id,context_id){
      isms_open_popup('popup_control_test_task', 'packages/risk/popup_control_test_task.php?task='+task_id+'&control='+context_id, '', 'true');
    }
    function open_edit(piId){
      isms_open_popup('popup_control_edit','packages/risk/popup_control_edit.php?control='+piId,'','true');
    }
    
    function open_control_cost(piId){
      isms_open_popup('popup_control_cost','packages/risk/popup_control_cost.php?id='+piId,'','true');
    }
    
    function go_to_nav_risk(piId){
      if(gebi('filter_scrolling_aux').value!=0){
        isms_change_to_sibling(4,'nav_risk.php?scrollfilter='+gebi('filter_scrolling').value+'&control='+gebi('filter_scrolling_aux').value+':'+ CONTEXT_CONTROL +';'+piId);
      }else{
        isms_change_to_sibling(4,'nav_risk.php?scrollfilter='+gebi('filter_scrolling').value+'&control='+ CONTEXT_CONTROL +';'+piId);
      }
    }
    </script>
  <?
    if(FWDWebLib::getObject('filter_scrolling')->getValue())
       ISMSLib::getPathScrollCode(RISK_MANAGEMENT_MODE,CONTEXT_RISK,FWDWebLib::getObject('filter_scrolling')->getValue(),false,'',FWDWebLib::getObject('filter_scrolling_aux')->getValue());  
    else
      ISMSLib::getPathScrollCode(RISK_MANAGEMENT_MODE,CONTEXT_CONTROL,FWDWebLib::getObject('filter_scrolling')->getValue(),false,'',FWDWebLib::getObject('filter_scrolling_aux')->getValue());
  }
}
FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_control.xml");
?>