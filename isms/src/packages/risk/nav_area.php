<?php
include_once "include.php";
include_once $handlers_ref . "grid/QueryGridArea.php";
include_once $handlers_ref . "select/QuerySelectAreaPriority.php";
include_once $handlers_ref . "select/QuerySelectAreaType.php";
include_once $handlers_ref . "select/QuerySelectAreaResponsibles.php";

class SearchEvent extends FWDRunnable {
  public function run(){
    $moGrid = FWDWebLib::getObject('grid_area');
    $moGrid->execEventPopulate();
  }
}

class GridArea extends FWDDrawGrid {
  public function drawItem(){
    switch($this->ciColumnIndex){
      case $this->getIndexByAlias('area_value'):
        $maDeniedACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
        $mbHideEdit = false;
        $mbHideRemove = false;
        if(!ISMSLib::userHasACL('M.RM.1.4')){
          if(ISMSLib::userHasACL('M.RM.1.8')){
            if ($this->getFieldValue('area_responsible_id') == ISMSLib::getCurrentUserId())
              $mbHideEdit=false;
            else
              $mbHideEdit=true;
          }else{
            $mbHideEdit=true;
          }
        }
        
        if(!ISMSLib::userHasACL('M.RM.1.5')){
          if(ISMSLib::userHasACL('"M.RM.1.9')){
            if ($this->getFieldValue('area_responsible_id') == ISMSLib::getCurrentUserId())
              $mbHideRemove=false;
            else
              $mbHideRemove=true;
          }else{
            $mbHideRemove=true;
          }
        }

        if ($mbHideEdit || $mbHideRemove) {
          $moACL = FWDACLSecurity::getInstance();
          $maDenied = array();
          if($mbHideEdit)
            $maDenied[] = 'perm_to_edit';
          if($mbHideRemove)
            $maDenied[] = 'perm_to_remove';

          $moACL->setNotAllowed($maDenied);
          $moMenu = FWDWebLib::getObject('menu');
          $moGrid = FWDWebLib::getObject('grid_area');
          $msLine = $moGrid->getAttrName().'_acl_'.$this->ciRowIndex;
          FWDGridMenuACLSecurity::getInstance()->installGridMenuACL($moMenu,$moACL,$moGrid,$msLine);
        }
        
        $miAreaValue = $this->getFieldValue('area_value');
        $msAreaColor = RMRiskConfig::getRiskColor($miAreaValue);
        $this->coCellBox->setIconSrc("icon-area_".$msAreaColor.".gif");
        $this->coCellBox->setObjFWDToolTip(ISMSLib::getInstance()->getToolTipCreateModify($this->getFieldValue('user_create_name'),$this->getFieldValue('date_create'),$this->getFieldValue('user_edit_name'),$this->getFieldValue('date_edit')));
        return parent::drawItem();
        break;
      
      case $this->getIndexByAlias('area_name'):
        $msValue = $this->coCellBox->getValue();
        $miAreaValue = $this->getFieldValue('area_value');
        $msLinkColor = RMRiskConfig::getRiskColor($miAreaValue,true);
        $msLink = "<font color=".$msLinkColor.">".$msValue."</a>";
        $this->coCellBox->setAttrStringNoEscape("true");
        if(ISMSLib::userHasACL('M.RM.1.2')){
          $this->coCellBox->setValue("<a href='javascript:enter_area(".$this->getFieldValue('area_id').");'>{$msLink}</a>");
        }else{
          $this->coCellBox->setValue("<a href='javascript:open_visualize(".$this->getFieldValue('area_id').",".CONTEXT_AREA.",\"".uniqid()."\")'>{$msLink}</a>");
        }
        return $this->coCellBox->draw();
        break;
      
      case $this->getIndexByAlias('subarea_count'):
        if (!$this->coCellBox->getValue()) $this->coCellBox->setValue(0);
        $this->coCellBox->setAttrStringNoEscape("true");
        if(ISMSLib::userHasACL('M.RM.1.2')){
          $this->coCellBox->setValue("<a href='javascript:enter_area(".$this->getFieldValue('area_id').");'>".$this->coCellBox->getValue()."</a>");
        }
        return $this->coCellBox->draw();
        break;
      
      case $this->getIndexByAlias('area_process_count'):
        if (!$this->coCellBox->getValue()) $this->coCellBox->setValue(0);
        $this->coCellBox->setAttrStringNoEscape("true");
        if(ISMSLib::userHasACL('M.RM.1.3') && ISMSLib::userHasACL('M.RM.2') ){
          $this->coCellBox->setValue("<a href='javascript:go_to_nav_process(".$this->getFieldValue('area_id').");'>".$this->coCellBox->getValue()."</a>");
        }
        return $this->coCellBox->draw();
        break;

      case $this->getIndexByAlias('area_state'):
        $this->coCellBox->setValue(ISMSContextObject::getContextStateAsString($this->coCellBox->getValue()));
        return $this->coCellBox->draw(); 
        break;
      
      default:
        return parent::drawItem();
        break;
    }
  }
}

class AreaConfirmRemove extends FWDRunnable {
  public function run(){
    $msTitle = FWDLanguage::getPHPStringValue('tt_remove_area','Remover �rea');
    $msMessage = FWDLanguage::getPHPStringValue('st_area_remove_confirm',"Voc� tem certeza de que deseja remover a �rea <b>%area_name%</b>?");
    
    $moArea = new RMArea();
    $moArea->fetchById(FWDWebLib::getObject('selected_area_id')->getValue());
    $msAreaName = ISMSLib::truncateString($moArea->getFieldValue('area_name'), 70);
    $msMessage = str_replace("%area_name%",$msAreaName,$msMessage);
    
    $msEventValue = "soPopUp = soPopUpManager.getPopUpById('popup_confirm');" .
                    "soWindow = soPopUp.getOpener();" .
                    "soWindow.remove_area();";
    
    ISMSLib::openConfirm($msTitle,$msMessage,$msEventValue,50);
  }
}

class RemoveAreaEvent extends FWDRunnable {
  public function run(){    
    $miAreaId = FWDWebLib::getObject('selected_area_id')->getValue();    
    $moArea = new RMArea();
    $moArea->delete($miAreaId);
    /*
     * Se possuir o m�dulo de documenta��o atualiza os leitores do documento da �rea. 
     */
    if (FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(POLICY_MODE))
      $moArea->updateReaders($miAreaId);
  }
}

class EnterAreaEvent extends FWDRunnable {
  public function run() {
    $moWebLib = FWDWebLib::getInstance();
    $miRootAreaId = intval($moWebLib->getObject('root_area_id')->getValue());
    $maDeniedACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
    if (!$miRootAreaId) {
      if (in_array("M.RM.1.7",$maDeniedACLs))
        echo "js_hide('viewbutton_insert');";
      else
        echo "js_show('viewbutton_insert');";
    }
    else {
      if (in_array("M.RM.1.6",$maDeniedACLs))
        echo "js_hide('viewbutton_insert');";
      else
        echo "js_show('viewbutton_insert');";
    }
    echo "refresh_grid();trigger_event('change_scrolling_path',3);";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));    
    $moStartEvent->addAjaxEvent(new AreaConfirmRemove('area_confirm_remove'));
    $moStartEvent->addAjaxEvent(new RemoveAreaEvent('remove_area_event'));
    $moStartEvent->addAjaxEvent(new ChangeScrollingPath('change_scrolling_path'));
    $moStartEvent->addAjaxEvent(new EnterAreaEvent('enter_area'));
    $moStartEvent->addAjaxEvent(new SearchEvent('search'));
    
    //instala a seguran�a de ACL na p�gina
    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
    FWDMenuACLSecurity::getInstance()->installMenuACL(FWDWebLib::getObject('menu'),FWDACLSecurity::getInstance());

    $moGrid = FWDWebLib::getObject("grid_area");
    $moHandler = new QueryGridArea(FWDWebLib::getConnection()); 
        
    if (!FWDWebLib::getObject("root_area_id")->getValue())
      FWDWebLib::getObject("root_area_id")->setValue(FWDWebLib::getObject("area_id")->getValue());
    
    if ((!FWDWebLib::getObject("root_area_id")->getValue())&&(FWDWebLib::getObject("par_area_id")->getValue())){
       FWDWebLib::getObject("root_area_id")->setValue(FWDWebLib::getObject("par_area_id")->getValue());
    }
    
    // Popula o select do filtro por respons�vel
    $moSelect = FWDWebLib::getObject('responsible');
    $moSelectHandler = new QuerySelectAreaResponsibles();
    $moSelectHandler->setUserId(ISMSLib::getCurrentUserId());
    $moSelect->setQueryHandler($moSelectHandler);
    $moSelect->populate();
    
    // Popula o select do filtro por prioridade
    $moSelect = FWDWebLib::getObject('priority');
    $moSelect->setQueryHandler(new QuerySelectAreaPriority());
    $moSelect->populate();
    
    // Popula o select do filtro por tipo
    $moSelect = FWDWebLib::getObject('type');
    $moSelect->setQueryHandler(new QuerySelectAreaType());
    $moSelect->populate();
    
    // Popula o select do filtro por estado
    $moSelect = FWDWebLib::getObject('state');
    $maStatus = array(CONTEXT_STATE_PENDANT,CONTEXT_STATE_APPROVED,CONTEXT_STATE_DENIED);
    foreach($maStatus as $miStatus){
      $moItem = new FWDItem();
      $moItem->setAttrKey($miStatus);
      $moItem->setValue(ISMSContextObject::getContextStateAsString($miStatus));
      $moSelect->addObjFWDItem($moItem);
    }
    
    // Seta os filtros
    if(strpos(FWDWebLib::getObject('search_in_name_only')->getValue(),'1')!==false){
      $moHandler->setName(FWDWebLib::convertToISO(FWDWebLib::getObject('name_filter')->getValue(),true));
    }else{
      $moHandler->setNameDescription(FWDWebLib::convertToISO(FWDWebLib::getObject('name_filter')->getValue(),true));
    }
    $moHandler->setResponsible(FWDWebLib::getObject('responsible')->getValue());
    $moHandler->setMaxSubAreas(FWDWebLib::getObject('max_sub_areas')->getValue());
    $moHandler->setMinSubAreas(FWDWebLib::getObject('min_sub_areas')->getValue());
    $moHandler->setMaxProcesses(FWDWebLib::getObject('max_processes')->getValue());
    $moHandler->setMinProcesses(FWDWebLib::getObject('min_processes')->getValue());
    $moHandler->setValueFilter(array_filter(explode(':',FWDWebLib::getObject('value_filter_controller')->getValue())));
    $moHandler->setType(FWDWebLib::getObject('type')->getValue());
    $moHandler->setPriority(FWDWebLib::getObject('priority')->getValue());
    $moHandler->setState(FWDWebLib::getObject('state')->getValue());
    
    $moHandler->setRootArea(FWDWebLib::getObject('root_area_id')->getValue());
    $moHandler->setUserId(ISMSLib::getCurrentUserId());
    
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new GridArea());
    
    $maDeniedACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
    
    if(FWDWebLib::getObject("root_area_id")->getValue()){
      if(!ISMSLib::userHasACL('M.RM.1.7') && !ISMSLib::userHasACL('M.RM.1.6')){
        FWDWebLib::getObject('viewbutton_insert')->setAttrDisplay("false");
      }
    }else{
      if(!ISMSLib::userHasACL('M.RM.1.7')){
        FWDWebLib::getObject('viewbutton_insert')->setAttrDisplay("false");
      }
    }
  }
}

class ChangeScrollingPath extends FWDRunnable{
  public function run(){
    echo ISMSLib::getPathScrollCode(RISK_MANAGEMENT_MODE,CONTEXT_AREA,FWDWebLib::getObject('root_area_id')->getValue(),true,"enter_area");
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    FWDWebLib::getObject('search_in_name_only')->setValue('1');
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
    <script language="javascript">
    function refresh_grid() {
      js_refresh_grid('grid_area');    
    }
    
    function remove_area() {
      trigger_event('remove_area_event', 3);
      refresh_grid();        
    }
    
    function enter_area(area_id) {          
      gebi('root_area_id').value = area_id;
      trigger_event('enter_area',3);        
    }
    
    function go_to_nav_process(piId){
      isms_change_to_sibling(2,'nav_process.php?scrollfilter='+piId);
    }
    </script>
  <?
    ISMSLib::getPathScrollCode(RISK_MANAGEMENT_MODE,CONTEXT_AREA,FWDWebLib::getObject('root_area_id')->getValue(),false,'enter_area');
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_area.xml");
?>