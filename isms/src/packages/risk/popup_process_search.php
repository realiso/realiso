<?php
include_once "include.php";
include_once $handlers_ref . "QueryProcessFromAsset.php";
include_once $handlers_ref . "grid/QueryGridProcessSearch.php";
include_once $handlers_ref . "QueryContextHierarchicalStructure.php";

class GridProcessSearch extends FWDDrawGrid {
  public function DrawItem(){
    switch($this->ciColumnIndex){
      case 1:
        $miProcessValue = $this->caData[1];
        $msProcessColor = RMRiskConfig::getRiskColor($miProcessValue);
        $this->coCellBox->setIconSrc("icon-process_".$msProcessColor.".gif");
        return parent::drawItem();
        break;
      case 2:
        $miProcessValue = $this->caData[1];
        $msLinkColor = RMRiskConfig::getRiskColor($miProcessValue,true);
        $msLink = "<font color=".$msLinkColor.">".$this->caData[2]."</font>";
        $this->coCellBox->setAttrStringNoEscape("true");
        $this->coCellBox->setValue("<a href='javascript:open_visualize(".$this->caData[4].",".CONTEXT_PROCESS.",\"".uniqid()."\")'>{$msLink}</a>");
        return $this->coCellBox->draw();
        break;
      default:
        return parent::drawItem();
        break;
    }
  }
}

class AssociateProcessesEvent extends FWDRunnable {
  public function run(){    
    $moWebLib = FWDWebLib::getInstance();
    $miAssetId = $moWebLib->getObject('asset_id')->getValue();

    //teste para verificar se o sistema n�o est� sendo hakeado
    $moCtxUserTest = new RMProcessAsset();
    $moCtxTest = new RMAsset();
    $moCtxTest->fetchById($miAssetId);
    $moCtxUserTest->testPermissionToEdit($miAssetId,$moCtxUserTest->getResponsible());
    
    $msNewProcesses = $moWebLib->getObject('current_processes_ids')->getValue();
    if($msNewProcesses){
      $maNewProcesses = explode(':',$msNewProcesses);
    }else{
      $maNewProcesses = array();
    }
    
    $moQuery = new QueryProcessFromAsset(FWDWebLib::getConnection());
    $moQuery->setAssetId($miAssetId);
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    $maOldProcesses = $moQuery->getProcesses();
    
    $maToInsert = array_diff($maNewProcesses,$maOldProcesses);
    $maToDelete = array_diff($maOldProcesses,$maNewProcesses);
    $moProcessAsset = new RMProcessAsset();
    if(count($maToInsert)) $moProcessAsset->insertProcesses($miAssetId,$maToInsert);
    if(count($maToDelete)) $moProcessAsset->deleteProcesses($miAssetId,$maToDelete);
    
    /*
     * Se possuir o m�dulo de documenta��o, deve atualizar os leitores do documento do ativo 
     */
    if (FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(POLICY_MODE)) {     
      $moAsset= new RMAsset();              
      $moAsset->updateReaders($miAssetId);          
    }
    
    echo "soWindow = soPopUpManager.getPopUpById('popup_process_search').getOpener();"
        ."if (soWindow.refresh_grid)"
        ."  soWindow.refresh_grid();"
        ."soPopUpManager.closePopUp('popup_process_search');";
  }
}

class SearchProcessEvent extends FWDRunnable {
  public function run(){
    $moGrid = FWDWebLib::getObject('grid_process_search');
    $moGrid->execEventPopulate();
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new SearchProcessEvent('search_process_event'));    
    $moStartEvent->addAjaxEvent(new AssociateProcessesEvent('associate_processes_event'));
    
    $moSearchGrid = FWDWebLib::getObject('grid_process_search');
    $moSearchGrid->setObjFwdDrawGrid(new GridProcessSearch());
    $moSearchHandler = new QueryGridProcessSearch(FWDWebLib::getConnection());
    
    $moCurrentGrid = FWDWebLib::getObject('grid_current_processes');
    $moCurrentGrid->setObjFwdDrawGrid(new GridProcessSearch());
    $moCurrentHandler = new QueryGridProcessSearch(FWDWebLib::getConnection());
    
    $moSearchHandler->setNameFilter(FWDWebLib::getObject('name_filter')->getValue());
    $moSearchHandler->setAreaFilter(FWDWebLib::getObject('area_filter')->getValue());
    
    $miAssetId = FWDWebLib::getObject('param_asset_id')->getValue();
    if($miAssetId){
      FWDWebLib::getObject('asset_id')->setValue($miAssetId);
    }else{
      $miAssetId = FWDWebLib::getObject('asset_id')->getValue();
    }
    
    $msCurrentIds = FWDWebLib::getObject('current_processes_ids')->getValue();
    if($msCurrentIds){
      $maCurrentIds = explode(':',$msCurrentIds);
      $moSearchHandler->setExcludedIds($maCurrentIds);
      $moCurrentHandler->setIds($maCurrentIds);
    }
    else {
      $moCurrentHandler->setEmpty(true);
    }
    
    $moSearchGrid->setQueryHandler($moSearchHandler);
    $moCurrentGrid->setQueryHandler($moCurrentHandler);
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    //teste para verificar se o sistema n�o est� sendo hakeado
    $moCtxUserTest = new RMProcessAsset();
    $moCtxUserTest->testPermissionToInsert();
    
    $moSelect = FWDWebLib::getObject('process_area_id');
    $moQuery = new QueryContextHierarchicalStructure(FWDWebLib::getConnection());
    $moQuery->setContextType(CONTEXT_AREA);
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    foreach ($moQuery->getContexts() as $maContext) {
      list($miContextId,$msHierarchicalName) = $maContext;
      $moSelect->setItemValue($miContextId,$msHierarchicalName);
    }
    
    $moQuery = new QueryProcessFromAsset(FWDWebLib::getConnection());
    $moQuery->setAssetId(FWDWebLib::getObject('param_asset_id')->getValue());
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    FWDWebLib::getObject('current_processes_ids')->setValue(implode(":",$moQuery->getProcesses()));
    
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
?>
<script language="javascript">
  gebi('process_name').focus();
  function refresh_grid() {    
    gobi('grid_current_processes').setPopulate(true);
    gobi('grid_current_processes').refresh();
    //gobi('grid_process_search').setPopulate(true);
    //gobi('grid_process_search').refresh();
  }
  function enter_process_search_event(e)
	{
		if (!e) e = window.event;
		if(e['keyCode']==13) {
      gebi('name_filter').value = gebi('process_name').value;
      gebi('area_filter').value = gebi('process_area_id').value;
      gebi('current_processes_ids').value = gobi('grid_current_processes').getAllIds().join(':');
      gobi('grid_process_search').setPopulate(true);
	    trigger_event("search_process_event","3");
		}
	}
	FWDEventManager.addEvent(gebi('process_name'), 'keydown', enter_process_search_event);
  refresh_grid();
</script>
<?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load('popup_process_search.xml');

?>