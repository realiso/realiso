<?php
include_once "include.php";

class SaveEvent extends FWDRunnable {
	public function run() {
    $moWebLib = FWDWebLib::getInstance();
    if(!FWDWebLib::getObject('var_save_cost')->getValue()){
     
      $maValues = array();
      $maValues[] = str_replace(",","",$moWebLib->getObject('cost_1')->getValue());
      $maValues[] = str_replace(",","",$moWebLib->getObject('cost_2')->getValue());
      $maValues[] = str_replace(",","",$moWebLib->getObject('cost_3')->getValue());
      $maValues[] = str_replace(",","",$moWebLib->getObject('cost_4')->getValue());
      $maValues[] = str_replace(",","",$moWebLib->getObject('cost_5')->getValue());
      $mfCostValue = array_sum($maValues);
      $msValues = serialize($maValues);
      echo "soPopUpManager.getPopUpById('popup_control_cost').getOpener().setControlCostCode('{$msValues}');
            soPopUpManager.getPopUpById('popup_control_cost').getOpener().setControlCostValue('{$mfCostValue}');
            soPopUpManager.closePopUp('popup_control_cost');";
    }else{
      $moConfig = new ISMSConfig();
      if($moConfig->getConfig(GENERAL_COST_ENABLED)){
        //salva os custos do controle caso exista c�digo para ser salvo
          $moCost = new RMControlCost();
          $moCostTst = new RMControlCost();
          $moCost->setFieldValue('control_cost_1',str_replace(",","",$moWebLib->getObject('cost_1')->getValue()));
          $moCost->setFieldValue('control_cost_2',str_replace(",","",$moWebLib->getObject('cost_2')->getValue()));
          $moCost->setFieldValue('control_cost_3',str_replace(",","",$moWebLib->getObject('cost_3')->getValue()));
          $moCost->setFieldValue('control_cost_4',str_replace(",","",$moWebLib->getObject('cost_4')->getValue()));
          $moCost->setFieldValue('control_cost_5',str_replace(",","",$moWebLib->getObject('cost_5')->getValue()));
          if($moCostTst->fetchById(FWDWebLib::getObject('control_id')->getValue())){
            $moCost->update(FWDWebLib::getObject('control_id')->getValue());
          }else{
            $moCost->setFieldValue('control_id',FWDWebLib::getObject('control_id')->getValue());
            $moCost->insert();
          }
        }
        echo "var moParent = soPopUpManager.getPopUpById('popup_control_cost').getOpener();"
        ."if(moParent.refresh_grid) moParent.refresh_grid();"
        ."self.close();";
      }
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new SaveEvent("save_event"));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
			$moWebLib = FWDWebLib::getInstance();
      $miControlId = $moWebLib->getObject('control_id')->getValue();
      
      //teste para verificar se o sistema n�o est� sendo hakeado
      $moCtxUserTest = new RMControl();
      if($miControlId){
        $moControl = new RMControl();
        $moControl->fetchById($miControlId);
      }else{
        $moCtxUserTest->testPermissionToInsert();      
      }
      
			$moConfig = new ISMSConfig();
			$maConfigCte = array();
			$maConfigCte[]= CONTROL_COST_1;
			$maConfigCte[]= CONTROL_COST_2;
			$maConfigCte[]= CONTROL_COST_3;
			$maConfigCte[]= CONTROL_COST_4;
			$maConfigCte[]= CONTROL_COST_5;

            $msCurrency = ISMSLib::getCurrencyString();			
			for($miI=1;$miI<=6;$miI++){
				$moWebLib->getObject("st_money_symbol_$miI")->setValue($msCurrency);
			}
			for($miI=1;$miI<=5;$miI++){
				$msCost{$miI} = trim($moConfig->getConfig($maConfigCte[$miI-1]));
			  if($msCost{$miI}!='')
  				$moWebLib->getObject("st_cost_{$miI}")->setValue($msCost{$miI});
			}
			
			$msCostValues = trim($moWebLib->getObject('cost_values')->getValue());
			if($msCostValues){
				$maCostValues = FWDWebLib::unserializeString($msCostValues);
						$moWebLib->getObject('cost_1')->setValue($maCostValues[0]);
						$moWebLib->getObject('cost_2')->setValue($maCostValues[1]);
						$moWebLib->getObject('cost_3')->setValue($maCostValues[2]);
						$moWebLib->getObject('cost_4')->setValue($maCostValues[3]);
						$moWebLib->getObject('cost_5')->setValue($maCostValues[4]);				
			}else{
				
				if($miControlId){
					$moCost = new RMControlCost();
					if($moCost->fetchById($miControlId)){

						$moWebLib->getObject('cost_1')->setValue($moCost->getFieldValue('control_cost_1'));
						$moWebLib->getObject('cost_2')->setValue($moCost->getFieldValue('control_cost_2'));
						$moWebLib->getObject('cost_3')->setValue($moCost->getFieldValue('control_cost_3'));
						$moWebLib->getObject('cost_4')->setValue($moCost->getFieldValue('control_cost_4'));
						$moWebLib->getObject('cost_5')->setValue($moCost->getFieldValue('control_cost_5'));
					}
          FWDWebLib::getObject('vb_label_confirm_save')->setValue(FWDLanguage::getPHPStringValue('vb_save','Salvar'));
          FWDWebLib::getObject('var_save_cost')->setValue(1);
				}
			}
			$moWebLib->dump_html($moWebLib->getObject('dialog'));
			?>
				<script language="javascript">
          gebi('cost_1').focus();
          
				function atualiza_total() {
					cost_1 = parseFloat(gebi('cost_1').value.replace(/,/g,'')); 
					cost_2 = parseFloat(gebi('cost_2').value.replace(/,/g,''));
					cost_3 = parseFloat(gebi('cost_3').value.replace(/,/g,''));
					cost_4 = parseFloat(gebi('cost_4').value.replace(/,/g,''));
					cost_5 = parseFloat(gebi('cost_5').value.replace(/,/g,''));
					cost_1 = cost_1?cost_1:0;
					cost_2 = cost_2?cost_2:0;
					cost_3 = cost_3?cost_3:0;
					cost_4 = cost_4?cost_4:0;
					cost_5 = cost_5?cost_5:0;
					var total = new String((cost_1 + cost_2 + cost_3 + cost_4 + cost_5).toFixed(2));
					gobi('total').setValue(format_currency(total));
				}
				function format_currency_fields() {
					gebi('cost_1').value = format_currency(gebi('cost_1').value);
					gebi('cost_2').value = format_currency(gebi('cost_2').value);
					gebi('cost_3').value = format_currency(gebi('cost_3').value);
					gebi('cost_4').value = format_currency(gebi('cost_4').value);
					gebi('cost_5').value = format_currency(gebi('cost_5').value);
					atualiza_total();
				}
				format_currency_fields();
				</script>
			<?
  }
}
	FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
	FWDWebLib::getInstance()->xml_load("popup_control_cost.xml");
?>