<?php
set_time_limit(3600);
include_once "include.php";

class ControlLevelIterator extends FWDReportLevelIterator {  

  public function __construct(){
    parent::__construct("control_id");
  }
	
  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msGfxRef = $moWebLib->getGfxRef();
   
    $msControlName = $poDataSet->getFieldByAlias("control_name")->getValue();    
		$mbControlIsActive = $poDataSet->getFieldByAlias("control_is_active")->getValue();		
				
		$msIcon = $msGfxRef;    
    if ($mbControlIsActive) $msIcon .= 'icon-control.png';
    else $msIcon .= 'icon-control_red.png';

    $moWebLib->getObject('control_icon')->setAttrSrc($msIcon);    
    $moWebLib->getObject('control_name')->setValue($msControlName);        
  }
}

class InfoLevelIterator extends FWDReportLevelIterator {  

  public function __construct(){
    parent::__construct("control_history_date_todo");
  }
	
  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msGfxRef = $moWebLib->getGfxRef();
   
    $msControlDateTodo = $poDataSet->getFieldByAlias("control_history_date_todo")->getValue();    
		$msControlDateAccomplishment = $poDataSet->getFieldByAlias("control_hist_date_accomplish")->getValue();
		$miControlEE = $poDataSet->getFieldByAlias("control_expected_efficiency")->getValue();
		$miControlRE = $poDataSet->getFieldByAlias("control_real_efficiency")->getValue();	
		$miControlJustification = $poDataSet->getFieldByAlias("control_justification")->getValue();		

		$moWebLib->getObject('control_date_todo')->setValue(ISMSLib::getISMSShortDate($msControlDateTodo));
		$moWebLib->getObject('control_date_accomplishment')->setValue(ISMSLib::getISMSShortDate($msControlDateAccomplishment));
    $moWebLib->getObject('control_ee')->setValue($miControlEE);    
    $moWebLib->getObject('control_re')->setValue($miControlRE);
    $moWebLib->getObject('control_justification')->setValue($miControlJustification); 
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();    
    $moWebLib->getObject("level_control")->setLevelIterator(new ControlLevelIterator());    
    $moWebLib->getObject("level_info")->setLevelIterator(new InfoLevelIterator());
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport($moWebLib->getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    $moComponent->setReportFilter($moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter());
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('rs_follow_up_of_control_efficiency', "Acompanhamento da Efic�cia dos Controles"));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_report_control_efficiency_accompaniment.xml");
?>