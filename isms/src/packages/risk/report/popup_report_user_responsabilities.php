<?php
set_time_limit(3600);
include_once "include.php";

class UserLevelIterator extends FWDReportLevelIterator {  

  public function __construct(){
    parent::__construct("user_id");
  }
	
  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msGfxRef = $moWebLib->getGfxRef();

    $msUserName = $poDataSet->getFieldByAlias("user_name")->getValue();    
    
    $msIcon = $msGfxRef . 'icon-user.png';    
    
    $moWebLib->getObject('user_icon')->setAttrSrc($msIcon);
    $moWebLib->getObject('user_name')->setValue($msUserName);    
  }
}

class ContextLevelIterator extends FWDReportLevelIterator {

	protected $ciContextNameWidth = 0;

  public function __construct(){
    parent::__construct("context_id");
    $this->ciContextNameWidth = FWDWebLib::getObject('context_name')->getAttrWidth();
  }
  
  public function changedLevel(FWDDBDataSet $poDataSet){
    return true;
  }

  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msGfxRef = $moWebLib->getGfxRef();

		$msContextType = $poDataSet->getFieldByAlias("context_type")->getValue();		
    $msContextName = $poDataSet->getFieldByAlias("context_name")->getValue();
    $miContextValue = $poDataSet->getFieldByAlias("context_value")->getValue();    
		
		$msContextIcon = '';
		$miContextTypeWidth = 0;
		$miContextType = 0;
		
		switch ($msContextType) {
			case 'special_user':
				$msContextIcon = 'user';
				$maSpecialUsers = ISMSLib::getSpecialUsers(RISK_MANAGEMENT_MODE);
				$msContextName = $maSpecialUsers[$poDataSet->getFieldByAlias("context_id")->getValue()];
				$miContextTypeWidth = 97;				
			break;
			
			case 'area':
				$msContextIcon = 'area';
				$miContextTypeWidth = 37;
				$miContextType = CONTEXT_AREA;			
			break;
			
			case 'process':
				$msContextIcon = 'process';
				$miContextTypeWidth = 62;
				$miContextType = CONTEXT_PROCESS;
			break;
			
			case 'asset':
				$msContextIcon = 'asset';
				$miContextTypeWidth = 38;
				$miContextType = CONTEXT_ASSET;
			break;
			
			case 'asset_security':
				$msContextIcon = 'asset';
				$miContextTypeWidth = 137;
				$miContextType = CONTEXT_ASSET;
			break;
			
			case 'control':				
				$msContextIcon = 'control';
				$miContextTypeWidth = 59;
				$miContextType = CONTEXT_CONTROL;
			break;
		}		
		
		if ($miContextType == CONTEXT_CONTROL) {
			if ($miContextValue) $msIcon = $msGfxRef . 'icon-control.png';
    	else $msIcon = $msGfxRef . 'icon-control_red.png';
		}	
    else if ($miContextType) {
    	$msIcon = $msGfxRef . "icon-{$msContextIcon}_" . RMRiskConfig::getRiskColor($miContextValue) . '.png';
    }
    else $msIcon = $msGfxRef . 'icon-' . $msContextIcon . '.png';
		
		$msContextLabel = '';
		if ($miContextType) {
    	$moContextObject = new ISMSContextObject();
    	$moContext = $moContextObject->getContextObject($miContextType);
			$msContextLabel = FWDWebLib::convertToISO($moContext->getLabel());
		}
		else {
			$msContextLabel = FWDLanguage::getPHPStringValue('rs_special_user', "Usu�rio Especial");
		}
		
		if ($msContextType == 'asset_security') $msContextLabel = $msContextLabel . ' (' . FWDLanguage::getPHPStringValue('rs_security_resp', "Resp. Seguran�a") . ')';  
		
		/*
		 * Ajusta a largura devido ao fato dos labels dos
		 * contextos possuirem tamanhos diferentes
		 */				
		$moWebLib->getObject('context_name')->setAttrWidth($this->ciContextNameWidth-$miContextTypeWidth);
		$moWebLib->getObject('context_icon')->setAttrSrc($msIcon);
    $moWebLib->getObject('context_type')->setValue($msContextLabel.":");
    $moWebLib->getObject('context_type')->setAttrWidth($miContextTypeWidth);
    $moWebLib->getObject('context_name')->setValue($msContextName);    
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $moWebLib->getObject("level_user")->setLevelIterator(new UserLevelIterator());
    $moWebLib->getObject("level_context")->setLevelIterator(new ContextLevelIterator());
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport($moWebLib->getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    $moComponent->setReportFilter($moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter());
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('rs_users_responsibilities', "Responsabilidades dos Usu�rios"));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_report_user_responsabilities.xml");
?>