<?php

set_time_limit(3600);
include_once 'include.php';

class StandardLevelIterator extends FWDReportLevelIterator {
  
  private $caTotalIds = array();
  private $caAppliedIds = array();
  
  public function __construct(){
    parent::__construct('standard_id');
  }
  
  public function advance($poDataSet){
    parent::advance($poDataSet);
    $this->caTotalIds = array();
    $this->caAppliedIds = array();
  }  
  
  public function fetch(FWDDBDataSet $poDataSet){    
    $this->caTotalIds = array_unique(array_merge($this->caTotalIds, array($poDataSet->getFieldByAlias('bp_id')->getValue())));
    if ($poDataSet->getFieldByAlias('bp_applied')->getValue())     
      $this->caAppliedIds = array_unique(array_merge($this->caAppliedIds, array($poDataSet->getFieldByAlias('bp_id')->getValue())));
    
    $mbIsApplied = $poDataSet->getFieldByAlias('bp_id')->getValue();
    $msStandardName = $poDataSet->getFieldByAlias('standard_name')->getValue();
    FWDWebLib::getObject('standard_icon')->setAttrSrc(FWDWebLib::getInstance()->getGfxRef().'icon-standard.png');
    if ($mbIsApplied) FWDWebLib::getObject('standard_name')->setValue($msStandardName);
    else FWDWebLib::getObject('standard_name')->setValue($msStandardName . " (" . FWDLanguage::getPHPStringValue('rs_not_used',"N�o Utilizada") . ")");
    
    $msFooter = FWDLanguage::getPHPStringValue('rs_report_conformity_footer',"Total de Melhores Pr�ticas = %total% / Total de Melhores Pr�ticas Aplicadas = %applied%");
    $msFooter = str_replace(array("%total%", "%applied%"), array(count($this->caTotalIds), count($this->caAppliedIds)), $msFooter);
    FWDWebLib::getObject('stats')->setValue($msFooter);
  }
  
}
class BpAppliedLevelIterator extends FWDReportLevelIterator {
  
  private $ciLastStandardId = 0;
  
  public function __construct(){
    parent::__construct('bp_applied');
  }
  
  public function changedLevel(FWDDBDataSet $poDataSet){
    if ($poDataSet->getFieldByAlias('bp_id')->getValue()) return ($this->ciLastStandardId != $poDataSet->getFieldByAlias('standard_id')->getValue()) ? true : parent::changedLevel($poDataSet);
    else return false;    
  }
  
  public function fetch(FWDDBDataSet $poDataSet){
    $mbIsAppplied = $poDataSet->getFieldByAlias('bp_applied')->getValue();    
    if ($mbIsAppplied) FWDWebLib::getObject('bp_applied')->setValue(FWDLanguage::getPHPStringValue('rs_applied_best_practices',"Melhores Pr�ticas Aplicadas"));
    else FWDWebLib::getObject('bp_applied')->setValue(FWDLanguage::getPHPStringValue('rs_not_applied_best_practices',"Melhores Pr�ticas N�o Aplicadas"));
    
    $this->ciLastStandardId = $poDataSet->getFieldByAlias('standard_id')->getValue();
  }
}
class BpLevelIterator extends FWDReportLevelIterator {
  
  public function __construct(){
    parent::__construct('bp_id');
  }
  
  public function changedLevel(FWDDBDataSet $poDataSet){
    if ($poDataSet->getFieldByAlias('bp_id')->getValue()) return parent::changedLevel($poDataSet);
    else return false;
  }
  
  public function fetch(FWDDBDataSet $poDataSet){
    FWDWebLib::getObject('bp_icon')->setAttrSrc(FWDWebLib::getInstance()->getGfxRef().'icon-best_practice.png');
    FWDWebLib::getObject('bp_name')->setValue($poDataSet->getFieldByAlias('bp_name')->getValue());
  }
  
}
class ControlAppliedLevelIterator extends FWDReportLevelIterator {
  
  private $ciLastBPId = 0;
  
  public function __construct(){
    parent::__construct('control_applied');
  }
  
  public function changedLevel(FWDDBDataSet $poDataSet){
    if ($poDataSet->getFieldByAlias('control_id')->getValue()) return ($this->ciLastBPId != $poDataSet->getFieldByAlias('bp_id')->getValue()) ? true : parent::changedLevel($poDataSet);
    else return false;
  }
  
  public function fetch(FWDDBDataSet $poDataSet){
    $mbIsAppplied = $poDataSet->getFieldByAlias('control_applied')->getValue();    
    if ($mbIsAppplied) FWDWebLib::getObject('control_applied')->setValue(FWDLanguage::getPHPStringValue('rs_used_controls',"Controles Utilizados"));
    else FWDWebLib::getObject('control_applied')->setValue(FWDLanguage::getPHPStringValue('rs_not_used_controls',"Controles N�o Utilizados"));
    $this->ciLastBPId = $poDataSet->getFieldByAlias('bp_id')->getValue();
  }
}
class ControlLevelIterator extends FWDReportLevelIterator {
  
  public function __construct(){
    parent::__construct('control_id');
  }  
  
  public function changedLevel(FWDDBDataSet $poDataSet){
    if ($poDataSet->getFieldByAlias('control_id')->getValue()) return parent::changedLevel($poDataSet);
    else return false;
  }
  
  public function fetch(FWDDBDataSet $poDataSet){
    if ($poDataSet->getFieldByAlias('control_is_active')->getValue()) $msIcon = 'icon-control.png';
    else $msIcon = 'icon-control_red.png';
    FWDWebLib::getObject('control_responsible')->setValue($poDataSet->getFieldByAlias('control_responsible')->getValue());
    FWDWebLib::getObject('control_name')->setValue($poDataSet->getFieldByAlias('control_name')->getValue());    
    FWDWebLib::getObject('control_icon')->setAttrSrc(FWDWebLib::getInstance()->getGfxRef().$msIcon);
  }
  
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moFilter = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter();
    
    FWDWebLib::getObject('standard_level')->setLevelIterator(new StandardLevelIterator());
    
    FWDWebLib::getObject('bp_applied_level')->setLevelIterator(new BpAppliedLevelIterator());
    
    FWDWebLib::getObject('bp_level')->setLevelIterator(new BpLevelIterator());
    
    FWDWebLib::getObject('control_applied_level')->setLevelIterator(new ControlAppliedLevelIterator());
    
    FWDWebLib::getObject('control_level')->setLevelIterator(new ControlLevelIterator());
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport(FWDWebLib::getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    
    $moComponent->setReportFilter($moFilter);
    
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('tt_conformity','Conformidade'));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_report_conformity.xml');

?>