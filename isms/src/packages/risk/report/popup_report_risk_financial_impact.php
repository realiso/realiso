<?php

set_time_limit(3600);
include_once 'include.php';

class RiskLevelIterator extends FWDReportLevelIterator {
  
  private $csSrc;
  protected $cfTotal = 0;
  
  
  public function __construct(){
    parent::__construct('risk_id');
    $this->csSrc = FWDWebLib::getInstance()->getGfxRef().'icon-risk_%s.gif';
  }
  
  public function fetch(FWDDBDataSet $poDataSet){
    $mfRiskValue = $poDataSet->getFieldByAlias('risk_value')->getValue();
    $mfRiskResidualValue = $poDataSet->getFieldByAlias('risk_residual_value')->getValue();
    $mfRiskImpact = $poDataSet->getFieldByAlias('risk_impact')->getValue();
    
    $this->cfTotal+= $mfRiskImpact;
    
    if($poDataSet->getFieldByAlias('risk_is_accepted')->getValue()){
      $msColor = 'green';
    }else{
      $msColor = RMRiskConfig::getRiskColor($mfRiskResidualValue);
    }
    FWDWebLib::getObject('risk_id')->setAttrSrc(sprintf($this->csSrc,$msColor));
    
    FWDWebLib::getObject('risk_name')->setValue($poDataSet->getFieldByAlias('risk_name')->getValue());
    FWDWebLib::getObject('risk_value')->setValue(RMRiskConfig::getRiskValueFormat($mfRiskValue));
    FWDWebLib::getObject('risk_residual_value')->setValue(RMRiskConfig::getRiskValueFormat($mfRiskResidualValue));
    FWDWebLib::getObject('risk_impact')->setValue('$ '.number_format($mfRiskImpact,2,'.',','));
    
    FWDWebLib::getObject('total_impact')->setValue('$ '.number_format($this->cfTotal,2,'.',','));
  }
  
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moFilter = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter();
    
    FWDWebLib::getObject('risk_level')->setLevelIterator(new RiskLevelIterator());
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport(FWDWebLib::getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    
    $moComponent->setReportFilter($moFilter);
    
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('mx_risk_financial_impact','Impacto Financeiro dos Riscos'));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_report_risk_financial_impact.xml');

?>