<?php

set_time_limit(3600);
include_once 'include.php';

class AreaLevelIterator extends FWDReportLevelIterator {
  
  public function __construct(){
    parent::__construct('area_id');
  }
  
  public function fetch(FWDDBDataSet $poDataSet){
    $miAreaValue = $poDataSet->getFieldByAlias('area_value')->getValue();
    $msIconSrc = FWDWebLib::getInstance()->getGfxRef()."icon-area_".RMRiskConfig::getRiskColor($miAreaValue).".gif";
    
    FWDWebLib::getObject('area_id')->setAttrSrc($msIconSrc);
    FWDWebLib::getObject('area_name')->setValue($poDataSet->getFieldByAlias('area_name')->getValue());
    FWDWebLib::getObject('area_responsible')->setValue($poDataSet->getFieldByAlias('area_responsible')->getValue());
  }
  
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moFilter = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter();
    
    FWDWebLib::getObject('area_level')->setLevelIterator(new AreaLevelIterator());
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport(FWDWebLib::getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    
    $moComponent->setReportFilter($moFilter);
    
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('mx_areas','�reas'));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_report_areas.xml');

?>