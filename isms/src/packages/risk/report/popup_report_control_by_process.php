<?php
set_time_limit(3600);
include_once "include.php";

class ProcessLevelIterator extends FWDReportLevelIterator {  

  public function __construct(){
  	parent::__construct("process_id"); 
  }
	
  public function fetch(FWDDBDataSet $poDataSet) {

  	$moWebLib = FWDWebLib::getInstance();
    $msProcessName = $poDataSet->getFieldByAlias("process_name")->getValue();
    $moWebLib->getObject('process_name')->setValue($msProcessName);
    
  }
  
}

class AssetHeaderLevelIterator extends FWDReportLevelIterator {  

  public function __construct(){
  	parent::__construct("asset_id");
  }
	
  public function fetch(FWDDBDataSet $poDataSet){}
  
}

class AssetLevelIterator extends FWDReportLevelIterator {  

  public function __construct(){
  	parent::__construct("asset_id");
  }
	
  public function fetch(FWDDBDataSet $poDataSet){

  	$moWebLib = FWDWebLib::getInstance();

    $msAssetName = $poDataSet->getFieldByAlias("asset_name")->getValue();
    $moWebLib->getObject('asset_name')->setValue($msAssetName);

    $miAssetDescription = $poDataSet->getFieldByAlias("asset_description")->getValue();
	$moWebLib->getObject('asset_description')->setValue($miAssetDescription); 

    $msAssetCategory = $poDataSet->getFieldByAlias("asset_category")->getValue();
    $moWebLib->getObject('asset_category')->setValue($msAssetCategory);    
    
    $msAssetRelavance = $poDataSet->getFieldByAlias("asset_relavance")->getValue();
    $moWebLib->getObject('asset_relavance')->setValue($msAssetRelavance);
     
  }
  
}

class RiskHeaderLevelIterator extends FWDReportLevelIterator {

  public function __construct(){
    parent::__construct("risk_id");
  }

  public function fetch(FWDDBDataSet $poDataSet){}
  
}

class RiskLevelIterator extends FWDReportLevelIterator {

  public function __construct(){
    parent::__construct("risk_id");
  }

  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();

    $msRiskEventDescription = $poDataSet->getFieldByAlias("risk_name")->getValue();
    $moWebLib->getObject("risk_name")->setValue($msRiskEventDescription);
    
    $msRiskDescription = $poDataSet->getFieldByAlias("risk_description")->getValue();
    $moWebLib->getObject("risk_description")->setValue($msRiskDescription);
    
    $msRiskEstimation = $poDataSet->getFieldByAlias("risk_estimation")->getValue();
    $moWebLib->getObject("risk_estimation")->setValue($msRiskEstimation);
    
    $msRiskResidual = $poDataSet->getFieldByAlias("risk_residual")->getValue();
    $moWebLib->getObject("risk_residual")->setValue($msRiskResidual);
    
  }
  
}

class ControlHeaderLevelIterator extends FWDReportLevelIterator {

  public function __construct(){
    parent::__construct("control_name");
  }

  public function fetch(FWDDBDataSet $poDataSet){}
}

class ControlLevelIterator extends FWDReportLevelIterator {

  public function __construct(){
    parent::__construct("control_name");
  }

  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();

    $msControlName = $poDataSet->getFieldByAlias("control_name")->getValue();
    $moWebLib->getObject("control_name")->setValue($msControlName);
    
    $msControlDescription = $poDataSet->getFieldByAlias("control_description")->getValue();
    $moWebLib->getObject("control_description")->setValue($msControlDescription);
    
    $msControlEvidence = $poDataSet->getFieldByAlias("control_evidence")->getValue();
    $moWebLib->getObject("control_evidence")->setValue($msControlEvidence);
    
    $msControlType = $poDataSet->getFieldByAlias("control_type")->getValue();
    $moWebLib->getObject("control_type")->setValue($msControlType);
    
    $msControlBestPractice = $poDataSet->getFieldByAlias("control_best_practice")->getValue();
    $moWebLib->getObject("control_best_practice")->setValue($msControlBestPractice);
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    
    $moWebLib->getObject('level_process')->setLevelIterator(new ProcessLevelIterator());
	$moWebLib->getObject('level_asset_header')->setLevelIterator(new AssetHeaderLevelIterator());
	$moWebLib->getObject('level_asset')->setLevelIterator(new AssetLevelIterator());
    $moWebLib->getObject('level_risk_header')->setLevelIterator(new RiskHeaderLevelIterator());
    $moWebLib->getObject('level_risk')->setLevelIterator(new RiskLevelIterator());
    $moWebLib->getObject('level_control_header')->setLevelIterator(new ControlHeaderLevelIterator());
	$moWebLib->getObject('level_control')->setLevelIterator(new ControlLevelIterator());
	
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport($moWebLib->getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());

    $moComponent->setReportFilter($moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter());
    
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('rs_general_risk', "Geral de Riscos"));
    $moComponent->generate();    
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_report_control_by_process.xml");
?>