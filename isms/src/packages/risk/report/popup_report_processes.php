<?php

set_time_limit(3600);
include_once 'include.php';

class ProcessLevelIterator extends FWDReportLevelIterator {
  
  public function __construct(){
    parent::__construct('process_id');
  }
  
  public function fetch(FWDDBDataSet $poDataSet){
    $miProcessValue = $poDataSet->getFieldByAlias('process_value')->getValue();
    $msIconSrc = FWDWebLib::getInstance()->getGfxRef()."icon-process_".RMRiskConfig::getRiskColor($miProcessValue).".gif";
    
    FWDWebLib::getObject('process_id')->setAttrSrc($msIconSrc);
    FWDWebLib::getObject('process_name')->setValue($poDataSet->getFieldByAlias('process_name')->getValue());
    FWDWebLib::getObject('process_responsible')->setValue($poDataSet->getFieldByAlias('process_responsible')->getValue());
  }
  
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moFilter = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter();
    
    FWDWebLib::getObject('process_level')->setLevelIterator(new ProcessLevelIterator());
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport(FWDWebLib::getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    
    $moComponent->setReportFilter($moFilter);
    
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('mx_processes','Processos'));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_report_processes.xml');

?>