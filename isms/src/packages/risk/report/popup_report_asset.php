<?php
set_time_limit(3600);
include_once "include.php";

class AreaLevelIterator extends FWDReportLevelIterator {  

  public function __construct(){
    parent::__construct("area_id");
  }
  
  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msGfxRef = $moWebLib->getGfxRef();

    $msAreaName = $poDataSet->getFieldByAlias("area_name")->getValue();
    $miAreaValue = $poDataSet->getFieldByAlias("area_value")->getValue();
    
    $msIcon = $msGfxRef . 'icon-area_' . RMRiskConfig::getRiskColor($miAreaValue) . '.png';

    $moWebLib->getObject('area_icon')->setAttrSrc($msIcon);
    $moWebLib->getObject('area_name')->setValue($msAreaName);
  }
}

class ProcessLevelIterator extends FWDReportLevelIterator {

  public function __construct(){
    parent::__construct("process_id");
  }
  
  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msGfxRef = $moWebLib->getGfxRef();

    $msProcessName = $poDataSet->getFieldByAlias("process_name")->getValue();
    $miProcessValue = $poDataSet->getFieldByAlias("process_value")->getValue();    
    
    $msIcon = $msGfxRef . 'icon-process_' . RMRiskConfig::getRiskColor($miProcessValue) . '.png';    
    
    $moWebLib->getObject('process_icon')->setAttrSrc($msIcon);
    $moWebLib->getObject('process_name')->setValue($msProcessName);
  }
}

class AssetLevelIterator extends FWDReportLevelIterator {

  public function __construct(){
    parent::__construct("asset_id");
  }

  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msGfxRef = $moWebLib->getGfxRef();

    $msAssetName = $poDataSet->getFieldByAlias("asset_name")->getValue();    
    $msAssetResponsibleName = $poDataSet->getFieldByAlias("responsible_name")->getValue();
    $miAssetValue = $poDataSet->getFieldByAlias("asset_value")->getValue();     
    
    $msIcon = $msGfxRef . 'icon-asset_' . RMRiskConfig::getRiskColor($miAssetValue) . '.png';

    $moWebLib->getObject('asset_icon')->setAttrSrc($msIcon);
    $moWebLib->getObject('asset_name')->setValue($msAssetName);
    $moWebLib->getObject('asset_responsible_name')->setValue($msAssetResponsibleName);
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){    
    $moWebLib = FWDWebLib::getInstance();
    $moFilter = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter();    
    
    switch ($moFilter->getReportType()) {
      case 'report_asset_by_area_and_process':
        FWDWebLib::getInstance()->xml_load("popup_report_asset_by_area_and_process.xml", false);
        $moWebLib->getObject("level_area")->setLevelIterator(new AreaLevelIterator());
        $moWebLib->getObject("level_process")->setLevelIterator(new ProcessLevelIterator());
        $moWebLib->getObject("level_asset")->setLevelIterator(new AssetLevelIterator());
      break;  
      
      case 'report_asset_by_area':
        FWDWebLib::getInstance()->xml_load("popup_report_asset_by_area.xml", false);
        $moWebLib->getObject("level_area")->setLevelIterator(new AreaLevelIterator());
        $moWebLib->getObject("level_asset")->setLevelIterator(new AssetLevelIterator());
      break;
      
      case 'report_asset_by_process':
        FWDWebLib::getInstance()->xml_load("popup_report_asset_by_process.xml", false);
        $moWebLib->getObject("level_process")->setLevelIterator(new ProcessLevelIterator());
        $moWebLib->getObject("level_asset")->setLevelIterator(new AssetLevelIterator());
      break;
      
      case 'report_asset_by_asset':
        FWDWebLib::getInstance()->xml_load("popup_report_asset.xml", false);
        $moWebLib->getObject("level_asset")->setLevelIterator(new AssetLevelIterator());
      break;
    }    
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport($moWebLib->getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    $moComponent->setReportFilter($moFilter);
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('rs_assets', "Ativos"));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
/*
 * Ao inv�s do xml_load ser chamado aqui, ele � chamado
 * baseado no filtro do relat�rio que o usu�rio escolheu.
 * Com isso, � necess�rio chamar a fun��o start() manualmente.
 */
FWDStartEvent::getInstance()->start();
?>