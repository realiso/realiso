<?php

set_time_limit(3600);
include_once 'include.php';

class BestPracticeLevelIterator extends FWDReportLevelIterator {
  
  public function __construct(){
    parent::__construct('best_practice_id');
    $msIconSrc = FWDWebLib::getInstance()->getGfxRef()."icon-best_practice.gif";
    FWDWebLib::getObject('best_practice_id')->setAttrSrc($msIconSrc);
  }
  
  public function fetch(FWDDBDataSet $poDataSet){
    FWDWebLib::getObject('best_practice_name')->setValue($poDataSet->getFieldByAlias('best_practice_name')->getValue());
  }
  
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moFilter = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter();
    
    FWDWebLib::getObject('best_practice_level')->setLevelIterator(new BestPracticeLevelIterator());
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport(FWDWebLib::getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    
    $moComponent->setReportFilter($moFilter);
    
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('mx_best_practices','Melhores Pr�ticas'));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_report_best_practices.xml');

?>