<?php
set_time_limit(3600);
include_once "include.php";

class RiskLevelIterator extends FWDReportLevelIterator {

  public function __construct(){
    parent::__construct("risk_id");
  }

  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();

    $msGfxRef = $moWebLib->getGfxRef();

    $msRiskName = $poDataSet->getFieldByAlias("risk_name")->getValue();
    $msRiskImpact = $poDataSet->getFieldByAlias("risk_impact")->getValue() ? ' / '.$poDataSet->getFieldByAlias("risk_impact")->getValue() : '';
    $msAssetName = $poDataSet->getFieldByAlias("asset_name")->getValue();    
		$miRiskResidualValue = $poDataSet->getFieldByAlias("risk_residual_value")->getValue();
		$miRiskIsAccepted = $poDataSet->getFieldByAlias("risk_accept_mode")->getValue();
		
		if ($miRiskIsAccepted) $msIcon = $msGfxRef . 'icon-risk_green.png';
    else $msIcon = $msGfxRef . 'icon-risk_' . RMRiskConfig::getRiskColor($miRiskResidualValue) . '.png';

    $moWebLib->getObject('risk_icon')->setAttrSrc($msIcon);    
    $moWebLib->getObject('risk_name')->setValue($msRiskName.$msRiskImpact);
    $moWebLib->getObject('asset_name')->setValue($msAssetName);
  }
}

class ControlLevelIterator extends FWDReportLevelIterator {  

  public function __construct(){
    parent::__construct("control_id");
  }
	
  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msGfxRef = $moWebLib->getGfxRef();

    $msControlName = $poDataSet->getFieldByAlias("control_name")->getValue();    
    $mbControlIsActive = $poDataSet->getFieldByAlias("control_is_active")->getValue();
    
    $msIcon = $msGfxRef;
    
    if ($mbControlIsActive) $msIcon .= 'icon-control.png';
    else $msIcon .= 'icon-control_red.png';

    $moWebLib->getObject('control_icon')->setAttrSrc($msIcon);
    $moWebLib->getObject('control_name')->setValue($msControlName);    
  }
}


class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $moFilter = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter();
    
    $moWebLib->getObject("level_risk")->setLevelIterator(new RiskLevelIterator());
    $moWebLib->getObject("level_control")->setLevelIterator(new ControlLevelIterator());
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport($moWebLib->getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    $moComponent->setReportFilter($moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter());
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('rs_controls_per_risk', "Controles por Risco"));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_report_controls_by_risk.xml");
?>