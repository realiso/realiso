<?php
set_time_limit(3600);
include_once "include.php";

class BestPracticeLevelIterator extends FWDReportLevelIterator {

  public function __construct(){
    parent::__construct("best_practice_id");
  }

  public function changedLevel(FWDDBDataSet $poDataSet){
    if ($poDataSet->getFieldByAlias('best_practice_id')->getValue()) return parent::changedLevel($poDataSet);
    else return false;
  }

  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $mbIsPreReport = $moWebLib->getObject('pre_report')->getValue();
    
    $msGfxRef = $moWebLib->getGfxRef();

    $miBestPracticeId = $poDataSet->getFieldByAlias("best_practice_id")->getValue();
    $msBestPracticeName = $poDataSet->getFieldByAlias("best_practice_name")->getValue();    
    $msBestPracticeJustification = trim($poDataSet->getFieldByAlias("best_practice_justification")->getValue());
    $mbIsApplied = $poDataSet->getFieldByAlias("control_risk_id")->getValue() ? 1 : 0;
    
    $msIcon = $msGfxRef . 'icon-best_practice.png';    
    
    $moWebLib->getObject('best_practice_icon')->setAttrSrc($msIcon);    
    $moWebLib->getObject('best_practice_name')->setValue($msBestPracticeName);
    
    $msJustification = '';    
    if ($mbIsApplied) {
    	$moWebLib->getObject('best_practice_applicability')->setValue(FWDLanguage::getPHPStringValue('rs_applied',"Aplicado"));
    	if ($msBestPracticeJustification) $msJustification = $msBestPracticeJustification;
    	else $msJustification = FWDLanguage::getPHPStringValue('rs_reduction_of_risks',"Redu��o de riscos.");
    }
    else {
    	$moWebLib->getObject('best_practice_applicability')->setValue(FWDLanguage::getPHPStringValue('rs_not_applied',"N�o Aplicado"));
    	if ($msBestPracticeJustification) $msJustification = $msBestPracticeJustification;
    	else $msJustification = FWDLanguage::getPHPStringValue('rs_no_risks_application_control',"At� o momento n�o foram identificados riscos que justifiquem a aplica��o do controle.");
    }
    
    if ($mbIsPreReport) {
      $moWebLib->getObject('best_practice_justification')->setAttrStringNoEscape("true");
      $msJustification = "<a id='best_practice_{$miBestPracticeId}' href='javascript:open_justification_window($miBestPracticeId,$mbIsApplied);'>" . $msJustification . "</a>";        
    }
    $moWebLib->getObject('best_practice_justification')->setValue($msJustification);
  }
}

class ControlLevelIterator extends FWDReportLevelIterator {

  private $ciLastControlId = 0;
  
  public function __construct(){
    parent::__construct("control_id");
  }
  
  public function changedLevel(FWDDBDataSet $poDataSet){
    if ($poDataSet->getFieldByAlias('doc_instance_id')->getValue()) return ($this->ciLastControlId != $poDataSet->getFieldByAlias('best_practice_id')->getValue()) ? true : parent::changedLevel($poDataSet);
    else return false;
  }
  
  public function fetch(FWDDBDataSet $poDataSet){
  
    $this->ciLastControlId = $poDataSet->getFieldByAlias("best_practice_id")->getValue();
    $moWebLib = FWDWebLib::getInstance();
    $msGfxRef = $moWebLib->getGfxRef();

    $msControlName = $poDataSet->getFieldByAlias("control_name")->getValue();    
    $mbControlIsActive = $poDataSet->getFieldByAlias("control_is_active")->getValue();
    
    $msIcon = $msGfxRef;    
    if ($mbControlIsActive) $msIcon .= 'icon-control.png';
    else $msIcon .= 'icon-control_red.png';

    $moWebLib->getObject('control_icon')->setAttrSrc($msIcon);
    $moWebLib->getObject('control_name')->setValue($msControlName);
  }
}


class DocInstanceLevelIterator extends FWDReportLevelIterator {

  public function __construct(){
    parent::__construct("doc_instance_id");
  }
  
  public function changedLevel(FWDDBDataSet $poDataSet){
    if ($poDataSet->getFieldByAlias('doc_instance_id')->getValue()) return parent::changedLevel($poDataSet);
    else return false;
  }

  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msIcon = $moWebLib->getGfxRef();
    $msIcon .= PMDocument::getDocumentIcon($poDataSet->getFieldByAlias("file_name")->getValue(),$poDataSet->getFieldByAlias("is_link")->getValue());

    $moWebLib->getObject('document_icon')->setAttrSrc($msIcon);
    $moWebLib->getObject('document_name')->setValue($poDataSet->getFieldByAlias("document_name")->getValue());
  }
}


class ShowJustificationEvent extends FWDRunnable {
  public function run(){    
    $miBestPracticeId = FWDWebLib::getObject('bp_id')->getValue();
    $mbIsApplied = FWDWebLib::getObject('bp_is_applied')->getValue();
    
    $moBestPractice = new RMBestPractice();
    $moBestPractice->fetchById($miBestPracticeId);
    $msJustification = trim($moBestPractice->getFieldValue('best_practice_justification'));
    
    $moWebLib = FWDWebLib::getInstance();
    $msPopUpId = 'popup_report_statement_of_applicability_justificative';
    $msXmlFile = $moWebLib->getSysRef().'packages/risk/report/popup_report_statement_of_applicability_justificative.xml';
    $moWebLib->xml_load($msXmlFile,false);
    
    if (!$msJustification) {
	    if ($mbIsApplied) $msJustification = FWDLanguage::getPHPStringValue('rs_reduction_of_risks',"Redu��o de riscos.");
	    else $msJustification = FWDLanguage::getPHPStringValue('rs_no_risks_application_control',"At� o momento n�o foram identificados riscos que justifiquem a aplica��o do controle.");
    }
    
    $moWebLib->getObject('popup_justificative_memo')->setValue(str_replace("<br/>","\n",$msJustification));
    
    $moDialog = $moWebLib->getObject('popup_justificative_dialog');    
    
    // Evento do close
    $moEvent = new FWDClientEvent();
    $moEvent->setAttrEvent('onClick');
    $moEvent->setAttrValue("soPopUpManager.closePopUp('popup_report_statement_of_applicability_justificative');");
    $moWebLib->getObject('popup_justificative_viewbutton_close')->addObjFWDEvent($moEvent);
    
    // Evento do save
    $psConfirmEvent = "soPopUp = soPopUpManager.getPopUpById('popup_report_statement_of_applicability_justificative'); soWindow = soPopUp.getOpener(); soWindow.saveJustification($miBestPracticeId,gebi('popup_justificative_memo').value);";
    $moEvent = new FWDClientEvent();
    $moEvent->setAttrEvent('onClick');
    $moEvent->setAttrValue($psConfirmEvent."soPopUpManager.closePopUp('popup_report_statement_of_applicability_justificative');");
    $moWebLib->getObject('popup_justificative_viewbutton_save')->addObjFWDEvent($moEvent);
    
    $msXml = str_replace(array("\n",'"'),array(' ','\"'),$moDialog->draw());
    
    echo "isms_open_popup('popup_report_statement_of_applicability_justificative','','','true',412,440);"
        ."soPopUpManager.getPopUpById('popup_report_statement_of_applicability_justificative').setHtmlContent(\"$msXml\");";
  }
}

class SaveJustificationEvent extends FWDRunnable {
  public function run(){
    $miBestPracticeId = FWDWebLib::getObject('bp_id')->getValue();
    $msJustification = FWDWebLib::getObject('bp_justification')->getValue();
    
    $moBestPractice = new RMBestPractice();    
    $moBestPractice->setFieldValue('best_practice_justification', $msJustification);
    $moBestPractice->update($miBestPracticeId,false);
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new ShowJustificationEvent("show_justification_event"));
    $moStartEvent->addAjaxEvent(new SaveJustificationEvent("save_justification_event"));    
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $moFilter = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter();
        
    /* Vari�vel para indicar se � um relat�rio ou pr�-relat�rio */     
    if($moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->attributeExists("IsPreReport"))
      $mbIsReport = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrIsPreReport() ? false : true;
    else
      $mbIsReport = true;
    
    $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->deleteAttribute("isPreReport");
    
    $moWebLib->getObject("level_best_practice")->setLevelIterator(new BestPracticeLevelIterator());
    $moWebLib->getObject("level_control")->setLevelIterator(new ControlLevelIterator());
    $moWebLib->getObject("level_doc_instance")->setLevelIterator(new DocInstanceLevelIterator());
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport($moWebLib->getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    $msReportType = '';
    if ($mbIsReport) {
    	$msReportType = FWDLanguage::getPHPStringValue('rs_statement_of_applicability',"Declara��o de Aplicabilidade");
    	FWDWebLib::getObject('bp_id')->setShouldDraw(false);
    	FWDWebLib::getObject('bp_is_applied')->setShouldDraw(false);
    	FWDWebLib::getObject('bp_justification')->setShouldDraw(false);
    	FWDWebLib::getObject('pre_report')->setShouldDraw(false);
    	$moComponent->setReportFilter($moFilter);
    }
    else {
    	$msReportType = FWDLanguage::getPHPStringValue('rs_statement_of_applicability_pre_report',"Pr�-Relat�rio de Declara��o de Aplicabilidade");
    	$moWebLib->getObject("pre_report")->setValue(1);    	
    	$moFilter->setClassification(REPORT_CLASS_NONE);
    	$moComponent->setReportFilter($moFilter);
    	$moBestPractice = new RMBestPractice();
	  	//$moBestPractice->cleanJustifications(); removendo a limpeza das justificativas para implementar como processo separado
	  	FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    }    
    $moComponent->setReportName($msReportType);
    $moComponent->generate();
    ?>
      <script language="javascript">
        function open_justification_window(piBestPracticeId, pbIsApplied) {
          gebi('bp_id').value = piBestPracticeId;
          gebi('bp_is_applied').value = pbIsApplied;
          trigger_event('show_justification_event',3);
        }
        
        function saveJustification(piBestPracticeId,psValue) {
          psValue = psValue.replace(/\n/g,'<br/>');
          gebi('best_practice_'+piBestPracticeId).innerHTML = psValue;
          gebi('bp_justification').value = psValue;
          gebi('bp_id').value = piBestPracticeId;
          trigger_event('save_justification_event',3);
        }
      </script>
    <?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_report_statement_of_applicability.xml");
?>