<?php
set_time_limit(3600);
include_once "include.php";

class ControlPlanningLevelIterator extends FWDReportLevelIterator {  

  public function __construct(){
    parent::__construct("is_planned");
  }
	
  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();    
   
    $miIsPlanned = $poDataSet->getFieldByAlias("is_planned")->getValue();
    
    if ($miIsPlanned == CONTROL_IS_PLANNED) {
    	$moWebLib->getObject('control_planning_status')->setValue(FWDLanguage::getPHPStringValue('rs_planned_controls', 'Controles Planejados'));
    	$moWebLib->getObject('date_type')->setValue(FWDLanguage::getPHPStringValue('rs_implementation_deadline', 'Prazo de Implementação'));
    }
    else {
    	$moWebLib->getObject('control_planning_status')->setValue(FWDLanguage::getPHPStringValue('rs_implemented_controls', 'Controles Implementados'));
    	$moWebLib->getObject('date_type')->setValue(FWDLanguage::getPHPStringValue('rs_implementation_date', 'Data de Implementação'));
    }
  }
}

class ControlLevelIterator extends FWDReportLevelIterator {  

  public function __construct(){
    parent::__construct("control_id");
  }
	
  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msGfxRef = $moWebLib->getGfxRef();
   
    $msControlName = $poDataSet->getFieldByAlias("control_name")->getValue();    
		$mbControlIsActive = $poDataSet->getFieldByAlias("control_is_active")->getValue();		
		$msControlResponsibleName = $poDataSet->getFieldByAlias("responsible_name")->getValue();
		$miIsPlanned = $poDataSet->getFieldByAlias("is_planned")->getValue();		
		if ($miIsPlanned == CONTROL_IS_PLANNED) $msControlDate = $poDataSet->getFieldByAlias("control_deadline")->getValue(); 
		else $msControlDate = $poDataSet->getFieldByAlias("control_date_implemented")->getValue();
		
		$msIcon = $msGfxRef;    
    if ($mbControlIsActive) $msIcon .= 'icon-control.png';
    else $msIcon .= 'icon-control_red.png';

    $moWebLib->getObject('control_icon')->setAttrSrc($msIcon);    
    $moWebLib->getObject('control_name')->setValue($msControlName);    
    $moWebLib->getObject('control_date')->setValue(ISMSLib::getISMSShortDate($msControlDate));
    $moWebLib->getObject('control_responsible_name')->setValue($msControlResponsibleName);
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $moWebLib->getObject("level_control_planning")->setLevelIterator(new ControlPlanningLevelIterator());
    $moWebLib->getObject("level_control")->setLevelIterator(new ControlLevelIterator());    
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport($moWebLib->getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    $moComponent->setReportFilter($moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter());
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('rs_planning_of_controls', "Planejamento dos Controles"));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_report_control_planning.xml");
?>