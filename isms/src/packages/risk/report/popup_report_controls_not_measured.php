<?php
set_time_limit(3600);
include_once "include.php";

class ControlLevelIterator extends FWDReportLevelIterator {  

  public function __construct(){
    parent::__construct("control_id");
  }
	
  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msGfxRef = $moWebLib->getGfxRef();
   
    $msControlName = $poDataSet->getFieldByAlias("control_name")->getValue();		
		$mbControlIsActive = $poDataSet->getFieldByAlias("control_is_active")->getValue();
		$msControlEfficiencyLimit = $poDataSet->getFieldByAlias("control_efficiency_limit_date")->getValue();
		$msControlTestLimit = $poDataSet->getFieldByAlias("control_test_limit_date")->getValue();
		
		$msIcon = $msGfxRef;    
    if ($mbControlIsActive) $msIcon .= 'icon-control.png';
    else $msIcon .= 'icon-control_red.png'; 

    $moWebLib->getObject('control_icon')->setAttrSrc($msIcon);    
    $moWebLib->getObject('control_name')->setValue($msControlName);
    if ($msControlEfficiencyLimit) $moWebLib->getObject('control_efficiency_limit_date')->setValue(ISMSLib::getISMSShortDate($msControlEfficiencyLimit));
    else $moWebLib->getObject('control_efficiency_limit_date')->setValue('---');
    if ($msControlTestLimit) $moWebLib->getObject('control_test_limit_date')->setValue(ISMSLib::getISMSShortDate($msControlTestLimit));
    else $moWebLib->getObject('control_test_limit_date')->setValue('---');
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $moWebLib->getObject("level_control")->setLevelIterator(new ControlLevelIterator());    
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport($moWebLib->getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    $moComponent->setReportFilter($moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter());
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('rs_not_measured_controls', "Controles n�o Medidos"));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_report_controls_not_measured.xml");
?>