<?php
set_time_limit(3600);
include_once "include.php";

class ControlLevelIterator extends FWDReportLevelIterator {  

  public function __construct(){
    parent::__construct("control_id");
  }
	
  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msGfxRef = $moWebLib->getGfxRef();
   
    $msControlName = $poDataSet->getFieldByAlias("control_name")->getValue();    
		$mbControlIsActive = $poDataSet->getFieldByAlias("control_is_active")->getValue();		
		$msControlAccomplishment = $poDataSet->getFieldByAlias("control_date_implemented")->getValue();
		$msControlDescription = $poDataSet->getFieldByAlias("control_description")->getValue();
		$msIcon = $msGfxRef;    
    if ($mbControlIsActive) $msIcon .= 'icon-control.png';
    else $msIcon .= 'icon-control_red.png';

    $moWebLib->getObject('control_icon')->setAttrSrc($msIcon);    
    $moWebLib->getObject('control_name')->setValue($msControlName);
    if($msControlAccomplishment){
      $moWebLib->getObject('control_accomplishment')->setValue(ISMSLib::getISMSShortDate($msControlAccomplishment));
    }else{
      $moWebLib->getObject('control_accomplishment')->setValue('--');
    }
    
    $moWebLib->getObject('control_description')->setValue($msControlDescription);
  }
}

class BestPracticeLevelIterator extends FWDReportLevelIterator {

  public function __construct(){
    parent::__construct("best_practice_id");
  }

  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msGfxRef = $moWebLib->getGfxRef();

    $msBestPracticeName = $poDataSet->getFieldByAlias("best_practice_name")->getValue();		
		$msBestPracticeDocument = $poDataSet->getFieldByAlias("best_practice_document")->getValue();
				
		$msIcon = $msGfxRef . 'icon-standard.png';   

    $moWebLib->getObject('best_practice_icon')->setAttrSrc($msIcon);    
    $moWebLib->getObject('best_practice_name')->setValue($msBestPracticeName);    
    $moWebLib->getObject('best_practice_document')->setValue($msBestPracticeDocument);
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();        
    $moWebLib->getObject("level_control")->setLevelIterator(new ControlLevelIterator());
    $moWebLib->getObject("level_best_practice")->setLevelIterator(new BestPracticeLevelIterator());
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport($moWebLib->getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    $moComponent->setReportFilter($moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter());
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('rs_summary_of_controls', "Resumo de Controles"));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_report_control_summary.xml");
?>