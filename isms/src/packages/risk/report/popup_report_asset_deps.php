<?php
set_time_limit(3600);
include_once "include.php";

class AssetLevelIterator extends FWDReportLevelIterator {
  public function __construct(){
    parent::__construct("asset_id");
  }
	
  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msGfxRef = $moWebLib->getGfxRef();
    
    $msAssetName = $poDataSet->getFieldByAlias("asset_name")->getValue();	    
    $miAssetValue = $poDataSet->getFieldByAlias("asset_value")->getValue();	
    $msRespName = $poDataSet->getFieldByAlias("asset_responsible")->getValue();
    
    $msIcon = $msGfxRef . 'icon-asset_' . RMRiskConfig::getRiskColor($miAssetValue) . '.png';
    $moWebLib->getObject('asset_icon')->setAttrSrc($msIcon);
    $moWebLib->getObject('asset_name')->setValue(FWDLanguage::getPHPStringValue('rs_asset_cl_bl', '<b>Ativo:</b>').'&nbsp;&nbsp;'.$msAssetName);
    $moWebLib->getObject('asset_responsible')->setValue($msRespName);
  }
}

class DepLevelIterator extends FWDReportLevelIterator {
  public function __construct(){
    parent::__construct("dep_type");
  }
	
  public function fetch(FWDDBDataSet $poDataSet){
    $miType = $poDataSet->getFieldByAlias("dep_type")->getValue();    
    $msType = ($miType == REPORT_DEPENDENCY_TYPE) ? 
                  FWDLanguage::getPHPStringValue('si_dependencies',"DependÍncias") :
                  FWDLanguage::getPHPStringValue('si_dependents',"Dependentes");
    FWDWebLib::getObject('dep_type')->setValue($msType);
  }
}

class InfoLevelIterator extends FWDReportLevelIterator {
  public function __construct(){
    parent::__construct("dep_id");
  }

  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msGfxRef = $moWebLib->getGfxRef();

    $msAssetName = $poDataSet->getFieldByAlias("dep_name")->getValue();
    $miAssetValue = $poDataSet->getFieldByAlias("dep_value")->getValue();
    $msRespName = $poDataSet->getFieldByAlias("dep_responsible")->getValue();
    
    $msIcon = $msGfxRef . 'icon-asset_' . RMRiskConfig::getRiskColor($miAssetValue) . '.png';
    $moWebLib->getObject('dep_icon')->setAttrSrc($msIcon);
    $moWebLib->getObject('dep_name')->setValue($msAssetName);
    $moWebLib->getObject('dep_responsible')->setValue($msRespName);
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $moWebLib->getObject("level_asset")->setLevelIterator(new AssetLevelIterator());
    $moWebLib->getObject("level_dep")->setLevelIterator(new DepLevelIterator());
    $moWebLib->getObject("level_info")->setLevelIterator(new InfoLevelIterator());
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport($moWebLib->getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    $moComponent->setReportFilter($moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter());
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('si_assets_deps',"Ativos - DependÍncias e Dependentes"));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_report_asset_deps.xml");
?>