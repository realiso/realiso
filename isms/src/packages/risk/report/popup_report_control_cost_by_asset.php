<?php
set_time_limit(3600);
include_once "include.php";

class AssetLevelIterator extends FWDReportLevelIterator {

  public function __construct(){
    parent::__construct("asset_id");
  }

  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msGfxRef = $moWebLib->getGfxRef();

    $msAssetName = $poDataSet->getFieldByAlias("asset_name")->getValue(); 
    $miAssetValue = $poDataSet->getFieldByAlias("asset_value")->getValue();     
    
    $msIcon = $msGfxRef . 'icon-asset_' . RMRiskConfig::getRiskColor($miAssetValue) . '.png';

    $moWebLib->getObject('asset_icon')->setAttrSrc($msIcon);
    $moWebLib->getObject('asset_name')->setValue($msAssetName);    
  }
}

class ControlLevelIterator extends FWDReportLevelIterator {  

  public function __construct(){
    parent::__construct("control_id");
  }
	
  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msGfxRef = $moWebLib->getGfxRef();
    $moConfig = new ISMSConfig();
    $msCurrency = ISMSLib::getCurrencyString();
    
    $msControlName = $poDataSet->getFieldByAlias("control_name")->getValue();    
    $mbControlIsActive = $poDataSet->getFieldByAlias("control_is_active")->getValue();
    $miControlCost = $poDataSet->getFieldByAlias("control_cost")->getValue();
    
    $msIcon = $msGfxRef;
    
    if ($mbControlIsActive) $msIcon .= 'icon-control.png';
    else $msIcon .= 'icon-control_red.png';

    $moWebLib->getObject('control_icon')->setAttrSrc($msIcon);
    $moWebLib->getObject('control_name')->setValue($msControlName);    
    $moWebLib->getObject('control_cost')->setValue("$msCurrency ".number_format($miControlCost,2,'.',','));
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $moWebLib->getObject("level_asset")->setLevelIterator(new AssetLevelIterator());
    $moWebLib->getObject("level_control")->setLevelIterator(new ControlLevelIterator());    
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport($moWebLib->getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    $moComponent->setReportFilter($moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter());
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('rs_cost_of_controls_per_asset', "Custo dos Controles por Ativo"));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_report_control_cost_by_asset.xml");
?>