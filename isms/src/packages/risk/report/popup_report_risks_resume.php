<?php
set_time_limit(3600);
include_once "include.php";

class SnameIterator extends FWDReportLevelIterator {

	public function __construct(){
		parent::__construct('ordem');
	}

	public function fetch(FWDDBDataSet $poDataSet){
		$moWebLib = FWDWebLib::getInstance();
		$origin = $poDataSet->getFieldByAlias('origin')->getValue();
		$ordem = $poDataSet->getFieldByAlias('ordem')->getValue();

		$title_summaryResidualRiskXsummaryRisk = FWDLanguage::getPHPStringValue('tt_summary_of_parameterized_risks','Riscos Potenciais vs. Riscos Residuais');
		$title_summaryResidualRisk = FWDLanguage::getPHPStringValue('tt_risk_management_status','N�vel de Risco');
		$title_summaryRisk = FWDLanguage::getPHPStringValue('tt_general_summary_of_risks','Risco Estimado vs. N�o-Estimado');
		
		$title_risk_treated = FWDLanguage::getPHPStringValue('rb_treated','Tratados');
		$title_risk_not_treated = FWDLanguage::getPHPStringValue('rb_not_treated','N�o Tratados');
		
		if($origin == $title_summaryRisk){
			if($ordem == $title_risk_treated) {
				$ordem = '&nbsp; &nbsp; ' . $title_risk_treated;
			} else if($ordem == $title_risk_not_treated) {
				$ordem = '&nbsp; &nbsp; ' . $title_risk_not_treated;
			}
			
			$moWebLib->getObject('col4')->setValue($poDataSet->getFieldByAlias('col8')->getValue());
			$moWebLib->getObject('col5')->setValue($poDataSet->getFieldByAlias('col9')->getValue());
			
		} else if($origin == $title_summaryResidualRiskXsummaryRisk){
			$moWebLib->getObject('col4')->setValue($poDataSet->getFieldByAlias('col6')->getValue());
			$moWebLib->getObject('col5')->setValue($poDataSet->getFieldByAlias('col7')->getValue());		

		} if($origin == $title_summaryResidualRisk){
			$moWebLib->getObject('col4')->setValue($poDataSet->getFieldByAlias('col4')->getValue());
			$moWebLib->getObject('col5')->setValue($poDataSet->getFieldByAlias('col5')->getValue());			
		}
		
		$moWebLib->getObject('col1')->setValue($poDataSet->getFieldByAlias('col1')->getValue());
		$moWebLib->getObject('col2')->setValue($poDataSet->getFieldByAlias('col2')->getValue());
		$moWebLib->getObject('col3')->setValue($poDataSet->getFieldByAlias('col3')->getValue());			

		$moWebLib->getObject('ordem')->setValue($ordem);
	}
}

class CabecalhoIterator extends FWDReportLevelIterator {

	public function __construct(){
		parent::__construct('origin');
	}

	public function fetch(FWDDBDataSet $poDataSet){
		$moWebLib = FWDWebLib::getInstance();
		$origin = $poDataSet->getFieldByAlias('origin')->getValue();

		$title_summaryResidualRiskXsummaryRisk = FWDLanguage::getPHPStringValue('tt_summary_of_parameterized_risks','Riscos Potenciais vs. Riscos Residuais');
		$title_summaryResidualRisk = FWDLanguage::getPHPStringValue('tt_risk_management_status','N�vel de Risco');
		$title_summaryRisk = FWDLanguage::getPHPStringValue('tt_general_summary_of_risks','Risco Estimado vs. N�o-Estimado');		
		
		if($origin == $title_summaryResidualRiskXsummaryRisk){
			$moWebLib->getObject('col1Head')->setValue('');
			$moWebLib->getObject('col2Head')->setValue('');
			$moWebLib->getObject('col3Head')->setValue('');
			$moWebLib->getObject('col4Head')->setValue('');			
			$moWebLib->getObject('col4Head')->setValue(FWDLanguage::getPHPStringValue('gc_capability','Potencial'));
			$moWebLib->getObject('col5Head')->setValue(FWDLanguage::getPHPStringValue('rb_residual','Residual'));
		
		}else if($origin == $title_summaryResidualRisk){
			$moWebLib->getObject('col1Head')->setValue(FWDLanguage::getPHPStringValue('cb_high','Alto'));
			$moWebLib->getObject('col2Head')->setValue(FWDLanguage::getPHPStringValue('cb_medium','M�dio'));
			$moWebLib->getObject('col3Head')->setValue(FWDLanguage::getPHPStringValue('cb_low','Baixo'));
			$moWebLib->getObject('col4Head')->setValue(FWDLanguage::getPHPStringValue('cb_not_parameterized','N�o Estimados'));
			$moWebLib->getObject('col5Head')->setValue(FWDLanguage::getPHPStringValue('lb_total','Total'));			

		} else if($origin == $title_summaryRisk){
			$moWebLib->getObject('col1Head')->setValue('');
			$moWebLib->getObject('col2Head')->setValue('');
			$moWebLib->getObject('col3Head')->setValue('');
			$moWebLib->getObject('col4Head')->setValue('');
			$moWebLib->getObject('col5Head')->setValue('');
		}

		$moWebLib->getObject('origin')->setValue($origin);
	}

	protected function cleanImg(){
		$moWebLib = FWDWebLib::getInstance();
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(''));
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$moWebLib = FWDWebLib::getInstance();
		$moWebLib->getObject('head')->setLevelIterator(new CabecalhoIterator());
		$moWebLib->getObject('detail')->setLevelIterator(new SnameIterator());

		$moComponent = new FWDReportGenerator();
		$moComponent->setReport($moWebLib->getObject('report'));
		$moComponent->setReportTemplate(new ISMSReportTemplate());
		$moComponent->setReportFilter($moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter());
		$moComponent->setReportName(FWDLanguage::getPHPStringValue('lb_indicators','Indicadores'));
		$moComponent->generate();
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_report_risks_resume.xml');
?>