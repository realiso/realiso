<?php
set_time_limit(3600);
include_once "include.php";

class AssetLevelIterator extends FWDReportLevelIterator {

  public function __construct(){
    parent::__construct("asset_id");
  }

  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msGfxRef = $moWebLib->getGfxRef();

    $msAssetName = $poDataSet->getFieldByAlias("asset_name")->getValue();    
    $msAssetResponsibleName = $poDataSet->getFieldByAlias("responsible_name")->getValue();
    $miAssetValue = $poDataSet->getFieldByAlias("asset_value")->getValue();
    $miRiskCount = $poDataSet->getFieldByAlias("risk_count")->getValue();
    
    $msIcon = $msGfxRef . 'icon-asset_' . RMRiskConfig::getRiskColor($miAssetValue) . '.png';

    $moWebLib->getObject('asset_icon')->setAttrSrc($msIcon);
    $moWebLib->getObject('asset_name')->setValue($msAssetName);
    $moWebLib->getObject('asset_responsible_name')->setValue($msAssetResponsibleName);
    $moWebLib->getObject('risk_count')->setValue($miRiskCount);
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){  	
    $moWebLib = FWDWebLib::getInstance();
    $moFilter = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter();        
    $moWebLib->getObject("level_asset")->setLevelIterator(new AssetLevelIterator()); 
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport($moWebLib->getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    $moComponent->setReportFilter($moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter());
    $msReportType = '';
    if ($moFilter->mustFilterOnlyByHighRisks()) $msReportType = FWDLanguage::getPHPStringValue('rs_top_10_assets_with_highest_risks', "Top 10 Ativos com Mais Riscos Altos");
    else $msReportType = FWDLanguage::getPHPStringValue('rs_top_10_assets_with_most_risks', "Top 10 Ativos com Mais Riscos");    
    $moComponent->setReportName($msReportType);
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_report_top10_assets_with_more_risks.xml");
?>