<?php
set_time_limit(3600);
include_once "include.php";

class ProcessLevelIterator extends FWDReportLevelIterator {  

  public function __construct(){
    parent::__construct("process_id");
  }
	
  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msGfxRef = $moWebLib->getGfxRef();
   
    $msProcessName = $poDataSet->getFieldByAlias("process_name")->getValue();				
		$msProcessResponsibleName = $poDataSet->getFieldByAlias("responsible_name")->getValue();
		$miProcessValue = $poDataSet->getFieldByAlias("process_value")->getValue();
		
		$msIcon = $msGfxRef . 'icon-process_' . RMRiskConfig::getRiskColor($miProcessValue) . '.png';    

    $moWebLib->getObject('process_icon')->setAttrSrc($msIcon);    
    $moWebLib->getObject('process_name')->setValue($msProcessName);
    $moWebLib->getObject('process_responsible_name')->setValue($msProcessResponsibleName);
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $moWebLib->getObject("level_process")->setLevelIterator(new ProcessLevelIterator());    
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport($moWebLib->getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    $moComponent->setReportFilter($moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter());
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('rs_processes_without_assets', "Processos sem Ativos"));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_report_processes_without_assets.xml");
?>