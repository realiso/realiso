<?php
set_time_limit(3600);
include_once "include.php";

class RiskLevelIterator extends FWDReportLevelIterator {  

  public function __construct(){
    parent::__construct("risk_id");
  }
	
  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msGfxRef = $moWebLib->getGfxRef();
   
    $msRiskName = $poDataSet->getFieldByAlias("risk_name")->getValue();		
    $msRiskImpact = $poDataSet->getFieldByAlias("risk_impact")->getValue() ? ' / '.$poDataSet->getFieldByAlias("risk_impact")->getValue() : '';
    $msAssetName = $poDataSet->getFieldByAlias("asset_name")->getValue();
		$miRiskIsAccepted = $poDataSet->getFieldByAlias("risk_accept_mode")->getValue();
		$miRiskValue = $poDataSet->getFieldByAlias("risk_value")->getValue();
		$miRiskResidualValue = $poDataSet->getFieldByAlias("risk_residual_value")->getValue();
		
		$moFilter = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter();
    $miValueRiskColor = ($moFilter->getRiskValueType() == RISK_VALUE_TYPE_REAL_VALUE) ? $miRiskValue : $miRiskResidualValue;

		$msIcon = $msGfxRef;    
    if ($miRiskIsAccepted) $msIcon = $msGfxRef . 'icon-risk_green.png';
    else $msIcon = $msGfxRef . 'icon-risk_' . RMRiskConfig::getRiskColor($miValueRiskColor) . '.png';

    $moWebLib->getObject('risk_icon')->setAttrSrc($msIcon);    
    $moWebLib->getObject('risk_name')->setValue($msRiskName.$msRiskImpact);
    $moWebLib->getObject('asset_name')->setValue($msAssetName);
    $moWebLib->getObject('risk_value')->setValue(RMRiskConfig::getRiskValueFormat($miRiskValue));
    $moWebLib->getObject('risk_residual_value')->setValue(RMRiskConfig::getRiskValueFormat($miRiskResidualValue));
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $moWebLib->getObject("level_risk")->setLevelIterator(new RiskLevelIterator());    
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport($moWebLib->getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    $moComponent->setReportFilter($moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter());
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('rs_risks_values', "Valores dos Riscos"));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_report_risk_values.xml");
?>