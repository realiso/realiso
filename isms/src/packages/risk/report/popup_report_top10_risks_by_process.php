<?php
set_time_limit(3600);
include_once "include.php";

class ProcessLevelIterator extends FWDReportLevelIterator {  

  public function __construct(){
    parent::__construct("process_id");
  }
	
  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msGfxRef = $moWebLib->getGfxRef();

    $msProcessName = $poDataSet->getFieldByAlias("process_name")->getValue();    
    $miProcessValue = $poDataSet->getFieldByAlias("process_value")->getValue();
    
    $msIcon = $msGfxRef . 'icon-process_' . RMRiskConfig::getRiskColor($miProcessValue) . '.png';
        
    $moWebLib->getObject('process_icon')->setAttrSrc($msIcon);
    $moWebLib->getObject('process_name')->setValue($msProcessName);    
  }
}

class RiskLevelIterator extends FWDReportLevelIterator {

  public function __construct(){
    parent::__construct("risk_id");
  }

  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();

    $msGfxRef = $moWebLib->getGfxRef();

    $msRiskName = $poDataSet->getFieldByAlias("risk_name")->getValue();
    $msRiskImpact = $poDataSet->getFieldByAlias("risk_impact")->getValue() ? ' / '.$poDataSet->getFieldByAlias("risk_impact")->getValue() : '';
    $miRiskValue = $poDataSet->getFieldByAlias("risk_value")->getValue();        
		$miRiskResidualValue = $poDataSet->getFieldByAlias("risk_residual_value")->getValue();
		$miRiskIsAccepted = $poDataSet->getFieldByAlias("risk_accept_mode")->getValue();
		
		if ($miRiskIsAccepted) $msIcon = $msGfxRef . 'icon-risk_green.png';
    else $msIcon = $msGfxRef . 'icon-risk_' . RMRiskConfig::getRiskColor($miRiskResidualValue) . '.png';

    $moWebLib->getObject('risk_icon')->setAttrSrc($msIcon);    
    $moWebLib->getObject('risk_name')->setValue($msRiskName.$msRiskImpact);
    $moWebLib->getObject('risk_value')->setValue(RMRiskConfig::getRiskValueFormat($miRiskValue));
    $moWebLib->getObject('risk_residual_value')->setValue(RMRiskConfig::getRiskValueFormat($miRiskResidualValue));    
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $moFilter = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter();
    
    $moWebLib->getObject("level_process")->setLevelIterator(new ProcessLevelIterator());
    $moWebLib->getObject("level_risk")->setLevelIterator(new RiskLevelIterator());    
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport($moWebLib->getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    $moComponent->setReportFilter($moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter());
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('rs_top_10_risks_per_process',"Top 10 Riscos por Processo"));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_report_top10_risks_by_process.xml");
?>