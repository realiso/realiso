<?php
set_time_limit(3600);
include_once "include.php";

class AreaLevelIterator extends FWDReportLevelIterator {  

  public function __construct(){
    parent::__construct("area_id");
  }
	
  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msGfxRef = $moWebLib->getGfxRef();
   
    $msAreaName = $poDataSet->getFieldByAlias("area_name")->getValue();
		$miAreaValue = $poDataSet->getFieldByAlias("area_value")->getValue();
		
		$msIcon = $msGfxRef . 'icon-area_' . RMRiskConfig::getRiskColor($miAreaValue) . '.png';    

    $moWebLib->getObject('area_icon')->setAttrSrc($msIcon);    
    $moWebLib->getObject('area_name')->setValue($msAreaName);    
  }
}

class ControlLevelIterator extends FWDReportLevelIterator {  

  public function __construct(){
    parent::__construct("control_id");
  }
	
  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msGfxRef = $moWebLib->getGfxRef();
    $moConfig = new ISMSConfig();
    $msCurrency = ISMSLib::getCurrencyString();

    $msControlName = $poDataSet->getFieldByAlias("control_name")->getValue();    
    $mbControlIsActive = $poDataSet->getFieldByAlias("control_is_active")->getValue();
    $miControlCost = $poDataSet->getFieldByAlias("control_cost")->getValue();
    
    $msIcon = $msGfxRef;
    
    if ($mbControlIsActive) $msIcon .= 'icon-control.png';
    else $msIcon .= 'icon-control_red.png';

    $moWebLib->getObject('control_icon')->setAttrSrc($msIcon);
    $moWebLib->getObject('control_name')->setValue($msControlName);    
    $moWebLib->getObject('control_cost')->setValue("$msCurrency ".number_format($miControlCost,2,'.',','));
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $moWebLib->getObject("level_area")->setLevelIterator(new AreaLevelIterator());
    $moWebLib->getObject("level_control")->setLevelIterator(new ControlLevelIterator());    
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport($moWebLib->getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    $moComponent->setReportFilter($moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter());
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('rs_cost_of_controls_per_area', "Custo dos Controles por �rea"));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_report_control_cost_by_area.xml");
?>