<?php
set_time_limit(3600);
include_once "include.php";

class AssetLevelIterator extends FWDReportLevelIterator {  

  public function __construct(){
    parent::__construct("asset_id");
  }
	
  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msGfxRef = $moWebLib->getGfxRef();

    $msAssetName = $poDataSet->getFieldByAlias("asset_name")->getValue();    
    $miAssetValue = $poDataSet->getFieldByAlias("asset_value")->getValue();     
    
    $msIcon = $msGfxRef . 'icon-asset_' . RMRiskConfig::getRiskColor($miAssetValue) . '.png';

    $moWebLib->getObject('asset_icon')->setAttrSrc($msIcon);
    $moWebLib->getObject('asset_name')->setValue($msAssetName);
  }
}

class RiskLevelIterator extends FWDReportLevelIterator {

  public function __construct(){
    parent::__construct("risk_id");
  }
  
  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msGfxRef = $moWebLib->getGfxRef();

    $msRiskName = $poDataSet->getFieldByAlias("risk_name")->getValue();
    $msRiskImpact = $poDataSet->getFieldByAlias("risk_impact")->getValue() ? ' / '.$poDataSet->getFieldByAlias("risk_impact")->getValue() : '';
    $miRiskValue = $poDataSet->getFieldByAlias("risk_value")->getValue();
		$miRiskResidualValue = $poDataSet->getFieldByAlias("risk_residual_value")->getValue();
		$miRiskIsAccepted = $poDataSet->getFieldByAlias("risk_accept_mode")->getValue();
		
		if ($miRiskIsAccepted) $msIcon = $msGfxRef . 'icon-risk_green.png';
    else $msIcon = $msGfxRef . 'icon-risk_' . RMRiskConfig::getRiskColor($miRiskResidualValue) . '.png';
    
    $mbHasAnyControl = ($miRiskValue != $miRiskResidualValue) ? 1 : 0;    
    
    $msStatus = '';
    if ($mbHasAnyControl) $msStatus = FWDLanguage::getPHPStringValue('rs_reduced','Reduzido');
    else $msStatus = FWDLanguage::getPHPStringValue('rs_not_treated','N�o Tratado');
		
		$msAcceptType = '';
		switch ($miRiskIsAccepted) {
			case RISK_ACCEPT_MODE_ACCEPT:
				$msAcceptType = FWDLanguage::getPHPStringValue('rs_restrained','Retido');
			break;
			
			case RISK_ACCEPT_MODE_TRANSFER:
				$msAcceptType = FWDLanguage::getPHPStringValue('rs_transferred','Transferido');
			break;
			
			case RISK_ACCEPT_MODE_AVOID:
				$msAcceptType = FWDLanguage::getPHPStringValue('rs_avoided','Evitado');
			break;
		}
		
		if ($mbHasAnyControl && $msAcceptType) $msStatus .= ' ' . FWDLanguage::getPHPStringValue('rs_and','e') . ' ' . $msAcceptType;
    else if ($msAcceptType) $msStatus = $msAcceptType;		
		
    $moWebLib->getObject('risk_icon')->setAttrSrc($msIcon);    
    $moWebLib->getObject('risk_name')->setValue($msRiskName.$msRiskImpact);
    $moWebLib->getObject('risk_value')->setValue(RMRiskConfig::getRiskValueFormat($miRiskValue));
    $moWebLib->getObject('risk_residual_value')->setValue(RMRiskConfig::getRiskValueFormat($miRiskResidualValue));
    $moWebLib->getObject('risk_status')->setValue($msStatus);
  }
}

class ControlLevelIterator extends FWDReportLevelIterator {

  public function __construct(){
    parent::__construct("control_id");
  }

  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msGfxRef = $moWebLib->getGfxRef();

    $msControlName = $poDataSet->getFieldByAlias("control_name")->getValue();    
		$mbControlIsActive = $poDataSet->getFieldByAlias("control_is_active")->getValue();
		$msControlDeadline = $poDataSet->getFieldByAlias("control_deadline")->getValue();
		$msControlResponsibleName = $poDataSet->getFieldByAlias("control_responsible_name")->getValue();
		
		$msIcon = $msGfxRef;    
    if ($mbControlIsActive) $msIcon .= 'icon-control.png';
    else $msIcon .= 'icon-control_red.png';

    $moWebLib->getObject('control_icon')->setAttrSrc($msIcon);    
    $moWebLib->getObject('control_name')->setValue($msControlName);
    $moWebLib->getObject('control_responsible_name')->setValue($msControlResponsibleName);
    $moWebLib->getObject('control_deadline')->setValue(ISMSLib::getISMSShortDate($msControlDeadline));
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $moWebLib->getObject("level_asset")->setLevelIterator(new AssetLevelIterator());
    $moWebLib->getObject("level_risk")->setLevelIterator(new RiskLevelIterator());
    $moWebLib->getObject("level_control")->setLevelIterator(new ControlLevelIterator());
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport($moWebLib->getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    $moComponent->setReportFilter($moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter());
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('rs_plan_of_risks_treatment', "Plano de Tratamento dos Riscos"));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_report_risk_treatment_plan.xml");
?>