<?php

set_time_limit(3600);
include_once 'include.php';

class AssetLevelIterator extends FWDReportLevelIterator {
  
  public function __construct(){
    parent::__construct('asset_id');
  }
  
  public function fetch(FWDDBDataSet $poDataSet){
    $miAssetValue = $poDataSet->getFieldByAlias('asset_value')->getValue();
    $msIconSrc = FWDWebLib::getInstance()->getGfxRef()."icon-asset_".RMRiskConfig::getRiskColor($miAssetValue).".png";
    $msAssetName = $poDataSet->getFieldByAlias('asset_name')->getValue();
    $msSumImportanceValue = $poDataSet->getFieldByAlias('asset_sum_importance_value')->getValue();
    
    FWDWebLib::getObject('asset_icon')->setAttrSrc($msIconSrc);
    FWDWebLib::getObject('asset_name')->setValue(FWDLanguage::getPHPStringValue('rs_asset_cl_bl', '<b>Ativo:</b>').'&nbsp;&nbsp;'.$msAssetName.' (Total: '.$msSumImportanceValue.')');
    FWDWebLib::getObject('asset_responsible')->setValue($poDataSet->getFieldByAlias('asset_responsible')->getValue());
  }  
}

class RelevanceLevelIterator extends FWDReportLevelIterator {
  
  public function __construct(){
    parent::__construct('asset_parameter');
  }
  
  public function fetch(FWDDBDataSet $poDataSet){
    $msParameter = $poDataSet->getFieldByAlias('asset_parameter')->getValue();    
    $msImportance = $poDataSet->getFieldByAlias('asset_importance')->getValue();
    $msImportanceValue = $poDataSet->getFieldByAlias('asset_importance_value')->getValue();
    
    FWDWebLib::getObject('relevance')->setValue($msParameter.': '.$msImportance.' ('.$msImportanceValue.')');
  }  
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moFilter = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter();
    
    FWDWebLib::getObject('asset_level')->setLevelIterator(new AssetLevelIterator());
    FWDWebLib::getObject('relevance_level')->setLevelIterator(new RelevanceLevelIterator());
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport(FWDWebLib::getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    
    $moComponent->setReportFilter($moFilter);
    
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('si_asset_relevance',"Relevāncia dos Ativos"));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_report_asset_relevance.xml');
?>