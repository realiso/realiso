<?php

set_time_limit(3600);
include_once 'include.php';

class AssetLevelIterator extends FWDReportLevelIterator {
  
  public function __construct(){
    parent::__construct('asset_id');
  }
  
  public function fetch(FWDDBDataSet $poDataSet){
    $miAssetValue = $poDataSet->getFieldByAlias('asset_value')->getValue();
    $msIconSrc = FWDWebLib::getInstance()->getGfxRef()."icon-asset_".RMRiskConfig::getRiskColor($miAssetValue).".gif";
    
    FWDWebLib::getObject('asset_id')->setAttrSrc($msIconSrc);
    FWDWebLib::getObject('asset_name')->setValue($poDataSet->getFieldByAlias('asset_name')->getValue());
    FWDWebLib::getObject('asset_responsible')->setValue($poDataSet->getFieldByAlias('asset_responsible')->getValue());
  }
  
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moFilter = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter();
    
    FWDWebLib::getObject('asset_level')->setLevelIterator(new AssetLevelIterator());
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport(FWDWebLib::getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    
    $moComponent->setReportFilter($moFilter);
    
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('mx_assets','Ativos'));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_report_assets.xml');

?>