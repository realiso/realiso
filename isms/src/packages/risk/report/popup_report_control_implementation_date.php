<?php
set_time_limit(3600);
include_once "include.php";

class ControlImplementationLevelIterator extends FWDReportLevelIterator {  

  public function __construct(){
    parent::__construct("implemented_before_deadline");
  }
	
  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();    
   
    $miImplementedBeforeDeadline = $poDataSet->getFieldByAlias("implemented_before_deadline")->getValue();
    
    if ($miImplementedBeforeDeadline == CONTROL_IMPLEMENTED_BEFORE_DEADLINE) $moWebLib->getObject('control_implementation_status')->setValue(FWDLanguage::getPHPStringValue('rs_controls_implemented_within_stated_period', 'Controles implementados dentro do prazo'));
    else $moWebLib->getObject('control_implementation_status')->setValue(FWDLanguage::getPHPStringValue('rs_controls_implemented_outside_stated_period', 'Controles implementados fora do prazo'));
  }
}

class ControlLevelIterator extends FWDReportLevelIterator {  

  public function __construct(){
    parent::__construct("control_id");
  }
	
  public function fetch(FWDDBDataSet $poDataSet){
    $moWebLib = FWDWebLib::getInstance();
    $msGfxRef = $moWebLib->getGfxRef();
   
    $msControlName = $poDataSet->getFieldByAlias("control_name")->getValue();    
		$mbControlIsActive = $poDataSet->getFieldByAlias("control_is_active")->getValue();		
		$msControlResponsibleName = $poDataSet->getFieldByAlias("responsible_name")->getValue();
		$msControlDeadline = $poDataSet->getFieldByAlias("control_deadline")->getValue();
		$msControlDateImplemented = $poDataSet->getFieldByAlias("control_date_implemented")->getValue();
		
		$msControlDateImplemented = $msControlDateImplemented ? ISMSLib::getISMSShortDate($msControlDateImplemented) : "";
		$msControlDeadline = $msControlDeadline ? ISMSLib::getISMSShortDate($msControlDeadline) : "";		
		
		$msIcon = $msGfxRef;    
    if ($mbControlIsActive) $msIcon .= 'icon-control.png';
    else $msIcon .= 'icon-control_red.png';

    $moWebLib->getObject('control_icon')->setAttrSrc($msIcon);    
    $moWebLib->getObject('control_name')->setValue($msControlName);    
    $moWebLib->getObject('control_deadline')->setValue($msControlDeadline);
    $moWebLib->getObject('control_date_implemented')->setValue($msControlDateImplemented);
    $moWebLib->getObject('control_responsible_name')->setValue($msControlResponsibleName);
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $moWebLib->getObject("level_control_implementation_status")->setLevelIterator(new ControlImplementationLevelIterator());
    $moWebLib->getObject("level_control")->setLevelIterator(new ControlLevelIterator());    
    
    $moComponent = new FWDReportGenerator();
    $moComponent->setReport($moWebLib->getObject('report'));
    $moComponent->setReportTemplate(new ISMSReportTemplate());
    $moComponent->setReportFilter($moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getAttrFilter());
    $moComponent->setReportName(FWDLanguage::getPHPStringValue('rs_date_of_implementation_of_controls', "Data de Implementação dos Controles"));
    $moComponent->generate();
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_report_control_implementation_date.xml");
?>