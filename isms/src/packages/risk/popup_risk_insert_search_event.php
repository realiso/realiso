<?php
include_once "include.php";
include_once $handlers_ref . "grid/QueryGridEventSearch.php";
include_once $handlers_ref . "QueryContextHierarchicalStructure.php";

class GridEventSearch extends FWDDrawGrid {
  public function DrawItem(){
    switch($this->ciColumnIndex){
      case 1:
        $msGfxRef = FWDWebLib::getInstance()->getGfxRef();
        $moIcon = new FWDIcon(new FWDBox(4,4));
        $moIcon->setAttrSrc("{$msGfxRef}icon-event.gif");
        return $moIcon->draw();
        break;
      
      case 2:
        $this->coCellBox->setAttrStringNoEscape("true");
        $this->coCellBox->setValue("<a href='javascript:open_visualize(".$this->caData[1].",".CONTEXT_EVENT.",\"".uniqid()."\")'>{$this->caData[2]}</a>");
        return $this->coCellBox->draw();
        break;
      
      case 4:
        $this->coCellBox->setAttrStringNoEscape("true");
        $this->coCellBox->setValue("<a href='javascript:open_visualize(".$this->caData[5].",".CONTEXT_CATEGORY.",\"".uniqid()."\")'>{$this->caData[4]}</a>");
        return $this->coCellBox->draw();
        break;
      
      default:
        return parent::drawItem();
        break;
    }
  }
}

class SearchEventEvent extends FWDRunnable {
  public function run() {
    $moGrid = FWDWebLib::getObject('grid_event_search');
    $moGrid->execEventPopulate();
  }
}

class ScreenBeforeEvent extends FWDRunnable {
    public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent("")); 
    $moStartEvent->addAjaxEvent(new SearchEventEvent("search_event_event"));
    
    $moGrid = FWDWebLib::getObject("grid_event_search");
    $moHandler = new QueryGridEventSearch(FWDWebLib::getConnection());
    $moHandler->setDescription(FWDWebLib::getObject("var_event_description")->getValue());
    $moHandler->setCategoryWithParentsFilter(FWDWebLib::getObject("var_event_category_id")->getValue());
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new GridEventSearch());
    }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    //teste para verificar se o sistema n�o est� sendo hakeado
    $moCtxUserTest = new RMRisk();
    $moCtxUserTest->testPermissionToInsert();
    
    $moSelect = FWDWebLib::getObject('event_category_id');
    $moQuery = new QueryContextHierarchicalStructure(FWDWebLib::getConnection());
    $moQuery->setContextType(CONTEXT_CATEGORY);
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    foreach ($moQuery->getContexts() as $maContext) {
      list($miContextId,$msHierarchicalName) = $maContext;
      $moSelect->setItemValue($miContextId,$msHierarchicalName);
    }
    
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
?>
<script language="javascript">
  gebi('event_description').focus();
  
  function refresh_grid() {
    gobi('grid_event_search').setPopulate(true);
    js_refresh_grid('grid_event_search');
  }
  function enter_risk_insert_search_event(e)
  {
    if (!e) e = window.event;
    if(e['keyCode']==13) {
      gebi('var_event_description').value = gebi('event_description').value;
      gebi('var_event_category_id').value = gebi('event_category_id').value;
      gobi('grid_event_search').setPopulate(true);
      trigger_event("search_event_event","3");
    }
  }
  FWDEventManager.addEvent(gebi('event_description'), 'keydown', enter_risk_insert_search_event);
</script>
<?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_risk_insert_search_event.xml");
?>