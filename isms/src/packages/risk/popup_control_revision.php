<?php

include_once 'include.php';

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(''));
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$miControlId = FWDWebLib::getObject('control')->getValue();
		$moRevision = new WKFControlEfficiency();
		if($moRevision->fetchById($miControlId)){
			$moSchedule = new WKFSchedule();
			$moSchedule->fetchById($moRevision->getFieldValue('schedule'));
			$maSchedule = $moSchedule->getFieldValues();
			unset($maSchedule['schedule_id']);
				
			$maRevision = array(
		            'revision_value_1' => $moRevision->getFieldValue('value_1'),
		            'revision_value_2' => $moRevision->getFieldValue('value_2'),
		            'revision_value_3' => $moRevision->getFieldValue('value_3'),
		            'revision_value_4' => $moRevision->getFieldValue('value_4'),
		            'revision_value_5' => $moRevision->getFieldValue('value_5'),
		            'revision_metric'  => $moRevision->getFieldValue('metric'),
					'observation'  => $moRevision->getFieldValue('observation'),
		            'controller_ee' => $moRevision->getFieldValue('expected_efficiency'),
		            'schedule' => serialize($maSchedule)
			);

			for($i=1;$i<=5;$i++){
				FWDWebLib::getObject('revision_value_'.$i)->setValue($maRevision['revision_value_'.$i]);
			}
			FWDWebLib::getObject('revision_control_metric')->setValue($maRevision['revision_metric']);
			FWDWebLib::getObject('observation_control')->setValue($maRevision['observation']);

			FWDWebLib::getObject('controller_ee')->setValue($maRevision['controller_ee']);
			FWDWebLib::getObject('var_schedule')->setValue($maRevision['schedule']);
		}

		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
		?>
<script language="javascript">
        
        function set_schedule(psSerializedSchedule){
          gebi('var_schedule').value = psSerializedSchedule;
        }
        
        function confirm(){
          var maSensitiveFields = {};
          var maFields = {};
          
          maSensitiveFields['schedule'] = gebi('var_schedule').value;
          maSensitiveFields['controller_ee'] = gebi('controller_ee').value;
          for(var i=1;i<=5;i++){
            maFields['revision_value_'+i] = gebi('revision_value_'+i).value;
          }
          maFields['revision_metric'] = gebi('revision_control_metric').value;

          maFields['observation'] = gebi('observation_control').value;
          
          self.getOpener().set_revision(soUnSerializer.serialize(maSensitiveFields),soUnSerializer.serialize(maFields));
          self.close();
        }
        
      </script>
		<?
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_control_revision.xml');

?>