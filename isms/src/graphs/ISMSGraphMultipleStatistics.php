<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

include_once $handlers_ref . "graphs/QueryChartStatistics.php";

/**
 * Classe que representa um gr�fico de ativos e valores de custo.
 *
 * <p>Classe que representa um gr�fico de ativos e valores de custo.</p>
 *
 * @package ISMS
 * @subpackage graphs
 */
class ISMSGraphMultipleStatistics extends ISMSGraphSingleStatistic {
  
  protected $caFeatures = array();
  
	public function addDataSet($maValues, $piColor, $psLabel) {
		$this->addLineLayer($maValues, $piColor, $psLabel);
	  $this->setLineWidth(2);
	}
	
	public function setFeatures($paFeatures) {		
		foreach ($paFeatures as $maFeature) {
			if (count($maFeature)==2)	$this->caFeatures[] = array($maFeature[0], $maFeature[1], -1);
			else $this->caFeatures[] = $maFeature;
			$this->caTypes[] = $maFeature[0];
		}
    $this->ciMinRecords = 2*count($this->caTypes);
  }
	
  public function init() {
    $moQuery = new QueryChartStatistics(FWDWebLib::getConnection());    
    $moQuery->setTypes($this->caTypes);    
		$moQuery->setStartDate($this->ciStartDate);
		$moQuery->setFinishDate($this->ciFinishDate);
		$moQuery->setContext($this->ciContextId);
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    $maData = $moQuery->getStatistics();
    
    $this->setTitle($this->csName);
    
    if(count($maData) && count($maData[$this->caTypes[0]]['values'])>=2){
    	$maDates = array();
	    foreach ($maData[$this->caTypes[0]]['dates'] as $msDate)
	    	$maDates[] = ISMSLib::getISMSShortDate($msDate);
    	
	    $this->xAxis->setLabels($maDates);
	    $this->xAxis->setLabelStep(count($maDates)/10);
	    $this->addLegend(50,295,false);
	    
	    foreach ($this->caFeatures as $maFeatures)
	    	$this->addDataSet($maData[$maFeatures[0]]['values'], $maFeatures[2], $maFeatures[1]);
      return true;
    }else{
      return false;
    }
  }
    
	public function makeGraph() {
		return parent::makeGraph(90,35,810,230);
  }  
}
?>