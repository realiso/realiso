<?php
include_once $handlers_ref . "graphs/QueryChartResidualRisk.php";

/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe que representa um gr�fico em barra com a quantidade de riscos residuais.
 *
 * <p>Classe que representa um gr�fico em barra com a quantidade de riscos residuais, separando-os em
 * riscos vermelhos, amarelos, verdes e azuis.</p>
 *
 * @package ISMS
 * @subpackage graphs
 */
class ISMSGraphResidualRisk extends ISMSGraphBarChart {
  protected $ciResultCount;
  public function __construct() {
    parent::__construct();
    $moQuery = new QueryChartResidualRisk(FWDWebLib::getConnection());
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    $maResidualRiskSummary = $moQuery->getResidualRiskSummary();
    $maData = array();
    $maLabels = array();
    $maColors = array();
    $miHighValue=0;
    foreach($maResidualRiskSummary as $msName=>$miValue) {
      if (!$miValue) continue;
      $maData[] = $miValue;
      $miHighValue = $miValue>$miHighValue?$miValue:$miHighValue;
      
      $riskLevel = ISMSLib::getConfigById(RISK_LEVEL);
      
	    if($riskLevel == 3){      
      
      
	      switch($msName) {
	        case "risk_high":
	          $maColors[] = 0xCA4343;
	          $maLabels[] = FWDLanguage::getPHPStringValue('st_high_risk','Risco Alto');
	          break;
	        case "risk_mid":
	          $maColors[] = 0xFBE33C;
	          $maLabels[] = FWDLanguage::getPHPStringValue('st_medium_risk','Risco M�dio');
	          break;
	        case "risk_low":
	          $maColors[] = 0x42B13E;
	          $maLabels[] = FWDLanguage::getPHPStringValue('st_low_risk','Risco Baixo');
	          break;
	        case "risk_np":
	          $maColors[] = 0x3939D2;
	          $maLabels[] = FWDLanguage::getPHPStringValue('st_non_parameterized_risk','Risco N�o Estimado');
	          break;
	        default:
	          break;
	      }
		      
	    } else if($riskLevel == 5){
	    	
	      switch($msName) {
	        case "risk_high":
	          $maColors[] = 0x000000;
	          $maLabels[] = FWDLanguage::getPHPStringValue('st_high_risks','Riscos Altos');
	          break;
	          
	        case "risk_mid_high":
	          $maColors[] = 0x990000;
	          $maLabels[] = FWDLanguage::getPHPStringValue('st_medium_high_risks','Riscos M�dio Altos');
	          break;          
	          
	        case "risk_mid":
	          $maColors[] = 0xFF6C00;
	          $maLabels[] = FWDLanguage::getPHPStringValue('st_medium_risks','Riscos M�dios');
	          break;
	          
	        case "risk_mid_low":
	          $maColors[] = 0xDAA520;
	          $maLabels[] = FWDLanguage::getPHPStringValue('st_medium_low_risks','Riscos M�dio Baixos');
	          break;
	                    
	        case "risk_low":
	          $maColors[] = 0x008000;
	          $maLabels[] = FWDLanguage::getPHPStringValue('st_low_risks','Riscos Baixos');
	          break;
	          
	        case "risk_np":
	          $maColors[] = 0x0000FF;
	          $maLabels[] = FWDLanguage::getPHPStringValue('st_non_parameterized_risks','Riscos N�o Estimados');
	          break;
	        default:
	          break;
	      }  	    	
	    	
	    } 
      
    }
    $this->ciResultCount=count($maData);
    $this->setTitle(FWDLanguage::getPHPStringValue('mx_residual_risks_summary','Sum�rio de Riscos Residuais'));
    $this->config($maData,$maLabels,$maColors,$miHighValue);
  }
  
  public function getTotal() {
    return $this->ciResultCount;
  }
}
?>