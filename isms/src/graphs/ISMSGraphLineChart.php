<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe que representa um gr�fico em linha.
 *
 * <p>Classe que representa um gr�fico em linha.</p>
 *
 * @package ISMS
 * @subpackage graphs
 */
class ISMSGraphLineChart extends XYChart {
 
  protected $cbHasEnoughData = false;

  protected $coLineLayer = null;
 
  protected $csTitle;
  
  protected $csColor;
  
  protected $cbHasLegend = false;
  protected $ciLegendX;
  protected $ciLegendY;
  protected $cbLegendVertical;
  protected $csLegendFont;
  protected $ciLegendFontSize;

  public function __construct($piWidth=940, $piHeight=380, $psBgColor=0xF1F8FF, $psEdgeColor=-1, $piRaisedEffect=0) {
    parent::__construct($piWidth,$piHeight,$psBgColor,$psEdgeColor,$piRaisedEffect);
  }
  
  public function setTitle($psTitle,$psColor=0xD9EAFE) {
    $this->csTitle = $psTitle;
    $this->csColor = $psColor;
  }

  public function addLineLayer($paData, $piColor=-1, $psName='', $piDepth=0){
    if(count($paData)){
      $this->cbHasEnoughData = true;
    }
    $this->coLineLayer = parent::addLineLayer($paData,$piColor,$psName,$piDepth);
  }
  
  public function addLineLayer2($poDataCombineMethod=null,$piDepth=0){
    $this->coLineLayer = parent::addLineLayer2($poDataCombineMethod,$piDepth);
  }
  
  public function setLineWidth($piWidth){
    $this->coLineLayer->setLineWidth($piWidth);
  }

  public function addDataSet($paData, $piColor=-1, $psName=''){
    if(count($paData)){
      $this->cbHasEnoughData = true;
      return $this->coLineLayer->addDataSet($paData,$piColor,$psName);
    }
  }

  public function addLegend($piX,$piY,$pbVertical=true,$psFont=GRAPH_FONT,$piFontSize=10){
    $this->cbHasLegend = true;
    $this->ciLegendX = $piX;
    $this->ciLegendY = $piY;
    $this->cbLegendVertical = $pbVertical;
    $this->csLegendFont = $psFont;
    $this->ciLegendFontSize = $piFontSize;
  }
  
  protected function makeTitle(){
    $moTitle = $this->addTitle2(TopLeft,$this->csTitle,GRAPH_FONT_BOLD,10,0x555555,$this->gradientColor(0, 0, 0, 24, 0xE3EFFF, 0x9EBFF4));
    $moTitle->setMargin2(6,5,5,5);
  }
  
  protected function makeLegend(){
    if($this->cbHasLegend){
      parent::addLegend($this->ciLegendX,$this->ciLegendY,$this->cbLegendVertical,$this->csLegendFont,$this->ciLegendFontSize);
    }
  }
  
  public function makeGraph($piX=50,$piY=35,$piWidth=850,$piHeight=210) {
    if($this->cbHasEnoughData){
      $this->yAxis->setLabelStyle(GRAPH_FONT);
      $this->makeLegend();
      $this->makeTitle();
      $this->setPlotArea($piX, $piY, $piWidth, $piHeight,0xffffff, -1, -1, 0xcccccc, 0xcccccc);
    }else{
      $msMessage = FWDLanguage::getPHPStringValue('st_chart_not_enough_data','N�o h� dados coletados suficientes para gerar esse gr�fico.');
      $this->addText(0,180,$msMessage,GRAPH_FONT,12,0x000000,5)->setWidth(940);
    }
    return $this;
  }

}

?>