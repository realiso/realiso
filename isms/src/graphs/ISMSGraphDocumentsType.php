<?php
include_once $handlers_ref . "graphs/QueryChartDocumentsType.php";

/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe que representa o gr�fico de tipos de documentos.
 *
 * <p>Classe que representa o gr�fico de tipos de documentos.</p>
 *
 * @package ISMS
 * @subpackage graphs
 */
class ISMSGraphDocumentsType extends ISMSGraphPieChart {  
  public function __construct() {
    parent::__construct();
    $moQuery = new QueryChartDocumentsType(FWDWebLib::getConnection());
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    $maResult = $moQuery->getDocumentsType();
    
    $maData = array();
    $maLabels = array();
    $maColors = array(0xCA4343, 0xFBE33C, 0x42B13E, 0x3939D2, 0xF87431, 0x4E8975, 0xFFA018, 0x1F86FF, 0x888888, 0xFEDCBA);
    foreach($maResult as $miType => $miTotal) {      
      $maData[] = $miTotal;
      if ($miType == DOCUMENT_TYPE_MANAGEMENT) {
      	$maLabels[] = FWDLanguage::getPHPStringValue('mx_management','Gest�o');
      }
      else if ($miType == DOCUMENT_TYPE_OTHERS) {
      	$maLabels[] = FWDLanguage::getPHPStringValue('mx_others','Outros');
      }
      else {
	      $moContextObject = new ISMSContextObject();
	      $moContext = $moContextObject->getContextObject($miType);      
	      $maLabels[] = ($moContext ? $moContext->getLabel() : FWDLanguage::getPHPStringValue('mx_without_type','Sem Tipo'));
      }      
    }
    $this->setTitle(FWDLanguage::getPHPStringValue('mx_amount_of_documents_by_type','Quantidade de Documentos por Tipo'));
    $this->config($maData,$maLabels,$maColors);
  }
}
?>