<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe que representa um gr�fico estilo pizza.
 *
 * <p>Classe que representa um gr�fico estilo pizza.</p>
 *
 * @package ISMS
 * @subpackage graphs
 */
class ISMSGraphPieChart extends PieChart {

  protected $cbHasEnoughData = false;

  protected $csTitle;
  
  protected $csColor;

  public function __construct($piWidth=940, $piHeight=380, $psBgColor=0xF1F8FF, $psEdgeColor=-1, $piRaisedEffect=0) {
    parent::__construct($piWidth,$piHeight,$psBgColor,$psEdgeColor,$piRaisedEffect);
  }
  
  public function setTitle($psTitle,$psColor=0xD9EAFE) {
    $this->csTitle = $psTitle;
    $this->csColor = $psColor;
  }
  
  public function config($paData,$paLabels,$paColors) {
    if(count($paData)){
      $this->cbHasEnoughData = true;
      $this->setData($paData,$paLabels);
      $this->setColors2(DataColor, $paColors);
      $this->setLabelStyle(GRAPH_FONT,8)->setBackground(Transparent);
      $this->setLabelFormat("<*block,valign=absmiddle*>{label} - {value} ({percent|2}%)");
    }
  }

  protected function makeTitle(){
    $moTitle = $this->addTitle2(TopLeft,$this->csTitle,GRAPH_FONT_BOLD,10,0x555555,$this->gradientColor(0, 0, 0, 24, 0xE3EFFF, 0x9EBFF4));
    $moTitle->setMargin2(6,5,5,5);
  }

  public function makeGraph($piWidth=470,$piHeight=190,$piRadius=100) {
    if($this->cbHasEnoughData){
      $this->makeTitle();
      $this->setPieSize($piWidth, $piHeight, $piRadius);
      $this->set3D();
      $this->setLabelLayout(SideLayout);
      return $this;
    }else{
      $msMessage = FWDLanguage::getPHPStringValue('st_chart_not_enough_data','N�o h� dados coletados suficientes para gerar esse gr�fico.');
      $this->addText(0,180,$msMessage,GRAPH_FONT,12,0x000000,5)->setWidth(940);
      return $this;
    }
  }

}
?>