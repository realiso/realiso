<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

include_once $handlers_ref . "graphs/QueryChartStatistics.php";
 
/**
 * Classe que representa um gr�fico de ativos e valores de custo.
 *
 * <p>Classe que representa um gr�fico de ativos e valores de custo.</p>
 *
 * @package ISMS
 * @subpackage graphs
 */
class ISMSGraphSingleStatistic extends ISMSGraphLineChart {
	
	protected $csName = "";
	protected $caTypes = array();
	protected $ciContextId = 0;
  protected $ciStartDate = "";
  protected $ciFinishDate = "";
	
	public function setName($psName) {
    $this->csName = $psName;
  }
  
  public function setContext($piContextId) {
    $this->ciContextId = $piContextId;
  }
	
	public function setTypes($paTypes) {
    $this->caTypes = $paTypes;
  }
  
  public function setStartDate($piTimestamp) {
    $this->ciStartDate = $piTimestamp;
  }
  
  public function setFinishDate($piTimestamp) {
    $this->ciFinishDate = $piTimestamp;
  }
	
	public function __construct() {
    parent::__construct(940,360);
  }
	
  public function init() {
    $moQuery = new QueryChartStatistics(FWDWebLib::getConnection());    
    $moQuery->setTypes($this->caTypes);
		$moQuery->setContext($this->ciContextId);
		$moQuery->setStartDate($this->ciStartDate);
		$moQuery->setFinishDate($this->ciFinishDate);
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    $maData = $moQuery->getStatistics();
    
    $this->setTitle($this->csName);
    
    if(count($maData) && count($maData[$this->caTypes[0]]['values'])>=2){
    	$maDates = array();
	    foreach ($maData[$this->caTypes[0]]['dates'] as $msDate)
	    	$maDates[] = ISMSLib::getISMSShortDate($msDate);

	    $this->xAxis->setLabels($maDates);
	    $this->xAxis->setLabelStep(count($maDates)/10);	    
	    $this->addLineLayer2();
	    $this->setLineWidth(2);
	    $this->addDataSet($maData[$this->caTypes[0]]['values'], 0xff0000);
      return true;
    }else{
      return false;
    }
  }
  
  public function makeGraph($piX=50,$piY=35,$piWidth=850,$piHeight=300) {
    if($this->init()){
      $this->makeLegend();
      $this->makeTitle();
      $this->setPlotArea($piX, $piY, $piWidth, $piHeight,0xffffff, -1, -1, 0xcccccc, 0xcccccc);
    }else{
      //return null;
      $this->setBorder(0xF1F8FF);
      $msMessage = FWDLanguage::getPHPStringValue('st_chart_not_enough_data','N�o h� dados coletados suficientes para gerar esse gr�fico.');
      $this->addText(0,180,$msMessage,GRAPH_FONT,12,0x000000,5)->setWidth(940);
    }
    return $this;
  }
}
?>