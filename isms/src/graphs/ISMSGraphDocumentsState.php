<?php
include_once $handlers_ref . "graphs/QueryChartDocumentsState.php";

/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe que representa o gr�fico de estados de documentos.
 *
 * <p>Classe que representa o gr�fico de estados de documentos.</p>
 *
 * @package ISMS
 * @subpackage graphs
 */
class ISMSGraphDocumentsState extends ISMSGraphPieChart {  
  public function __construct() {
    parent::__construct();
    $moQuery = new QueryChartDocumentsState(FWDWebLib::getConnection());
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    $maResult = $moQuery->getDocumentsState();
    
    $maData = array();
    $maLabels = array();
    $maColors = array(0x3939D2, 0xF87431, 0x4E8975, 0xFBE33C);
    foreach($maResult as $miState => $miTotal) {      
      $maData[] = $miTotal;            
      $maLabels[] = ISMSContextObject::getContextStateAsString($miState);      
    }
    $this->setTitle(FWDLanguage::getPHPStringValue('mx_amount_of_documents_by_status','Quantidade de Documentos por Status'));
    $this->config($maData,$maLabels,$maColors);
  }
}
?>