<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe ISMSReport.
 *
 * <p>Classe que implementa um relatório.</p>
 * @package ISMS
 * @subpackage report
 * @abstract
 */
abstract class ISMSReport extends FWDReport {	
 /**
  * Cria e executa a query e desenha o relatório.
  *
  * <p>Cria a query, executa-a (método parent::executeQuery() deve ser chamado no
  * final do makeQuery() ) e desenha o relatório.</p>
  * @access public
  */
	public function draw() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		$this->init();
		$this->makeQuery();
		
		/*
		 * POG para detectar final de um relatório
		 */
		$moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
		$msId = sprintf("#%x",crc32( "report_has_finished" . $_SERVER['SCRIPT_FILENAME']));
		if($moSession->attributeExists($msId)){  		
	  	$miCont = $moSession->{'getAttr'.$msId}();	
	 		$miCont--;
	 		$moSession->{'setAttr'.$msId}($miCont);
		}
		$moSession->commit();
		
		return parent::draw();		
	}
}
?>