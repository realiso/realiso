<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportTemplate.
 *
 * <p>Classe que implementa o template default para header e footer de relatórios.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportTemplate implements FWDReportITemplate {

  /**
   * Construtor da classe.
   *
   * <p>Construtor da classe ISMSReportTemplate. Carrega o template
   * default de relatórios, seta o gestor e a data.</p>
   * @access public
   */
  public function __construct() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    FWDWebLib::getInstance()->xml_load("report_template.xml", false);
    $this->setManager();
    $this->setDate();
  }

  /**
   * Seta o nome e o ip do gestor.
   *
   * <p>Seta o nome e o ip do gestor.</p>
   * @access public
   */
  public function setManager() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $moHeaderManagerName = FWDWebLib::getObject('header_manager_name');
    $moHeaderManagerAddress = FWDWebLib::getObject('header_manager_address');
    $moHeaderClientName = FWDWebLib::getObject('header_client_name');
    $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());

    $msName = $moSession->getUserName(true);

    $moLicense = new ISMSLicense();
    $moHeaderManagerName->setValue(FWDLanguage::getPHPStringValue('rs_manager_cl', "Gestor:") . " " . $msName);
    $moHeaderClientName->setValue($moLicense->getClientName());
    $moHeaderManagerAddress->setValue(FWDLanguage::getPHPStringValue('rs_ip_cl', "IP:") . " " . $moSession->getRemoteAddress());
  }

  /**
   * Seta a data do relatório.
   *
   * <p>Seta a data do relatório.</p>
   * @access public
   */
  public function setDate() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $moHeaderDate = FWDWebLib::getObject('header_date');
    $moHeaderDate->setValue(ISMSLib::getISMSDate());
  }

  /**
   * Seta a o tipo do relatório.
   *
   * <p>Seta o tipo do relatório.</p>
   * @access public
   * @param string $psValue Tipo do relatório
   */
  public function setReportType($psValue) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $moHeaderType = FWDWebLib::getObject('header_type');
    $moHeaderType->setValue($psValue);
  }

  /**
   * Seta um comentário para o relatório.
   *
   * <p>Seta um comentário para o relatório.</p>
   * @access public
   * @param string $psValue Comentário
   */
  public function setReportComment($psValue) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $moHeaderComment = FWDWebLib::getObject('header_comment');
    $msValue = '';
    if(strlen($psValue)>40){
      for($miI=0;$miI<strlen($psValue);$miI=$miI+30){
        $msSub = substr($psValue,$miI,40);
        if(strpos($msSub,' ')===false)
          $msValue .= substr($msSub,0,30) . ' ';
        else
          $msValue .= substr($msSub,0,30);
      }
    }else{
      $msValue = $psValue;
    }

    $moHeaderComment->setValue($msValue);
  }

  /**
   * Seta a classificação do relatório.
   *
   * <p>Seta a classificação do relatório.</p>
   * @access public
   * @param string $psValue Classificação do relatório
   */
  public function setReportPrivacy($psValue) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $moHeaderPrivacy = FWDWebLib::getObject('header_privacy');
    $moHeaderPrivacy->setValue($psValue);
  }

  /**
   * Retorna os Headers do relatório.
   *
   * <p>Retorna os Headers do relatório.</p>
   * @access public
   * @return array Headers do relatório
   */
  public function getHeaders() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $maHeaders = array();
    $maHeaders[]= FWDWebLib::getObject('report_header_blank');
    if (!trim(FWDWebLib::getObject('header_comment')->getValue())) $maHeaders[]= FWDWebLib::getObject('report_header_5');
    else {
      $maHeaders[]= FWDWebLib::getObject('report_header_comment');
      $maHeaders[]= FWDWebLib::getObject('report_header_4');
    }
    $maHeaders[]= FWDWebLib::getObject('report_header_3');
    $maHeaders[]= FWDWebLib::getObject('report_header_2');
    $maHeaders[]= FWDWebLib::getObject('report_header_1');
    return $maHeaders;
  }

  /**
   * Retorna o Footer do relatório.
   *
   * <p>Retorna o Footer do relatório.</p>
   * @access public
   * @return FWDReportLine Footer do relatório
   */
  public function getFooter() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $moReportFooter = FWDWebLib::getObject('report_footer');
    return $moReportFooter;
  }
}
?>