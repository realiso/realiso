<?php

include_once "include.php";
set_time_limit(3000);

// Verifica se o usuário não está tentando acessar o sistema pela URL errada
ISMSLib::checkUrl();

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		$moWebLib = FWDWebLib::getInstance();

        $msSrc = 'https://docs.google.com/spreadsheet/embeddedform?formkey=dHV0cUg1bktPY3l6VFpHNUtrZ2FHa3c6MQ&hl=pt';
		$moSurveyDiv = new FWDIFrame(new FWDBox(-400,318,340,760));
		$moSurveyDiv->setName("survey_dv");
		$moSurveyDiv->setSrc($msSrc);

		$moWebLib->getObject('dialog')->addObjFWDView($moSurveyDiv);
		$moWebLib->dump_html($moWebLib->getObject('dialog'));
	}

}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("survey_isms_pt.xml");
?>
<!--[if lt IE 7]>
<script defer type='text/javascript' language='javascript'>
  correct_png();
</script>
<![endif]-->



