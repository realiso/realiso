<?php
include_once "include.php";
include_once $handlers_ref . "grid/QueryGridContextSearch.php";

/*
  * Fun��o ustrlen (mesmo que strlen, s� que para strings unicode).
  * C�digo retirado do f�rum do php.
  */
function ustrlen($s) {
  $a = preg_split("//u", $s);
  $i = -2;
  foreach ($a as $b)
    $i++;
  return $i;
}

/*
 * Fun��o para ajustar o trecho em que a string que foi pesquisada aparece.
 */ 
function makePreview($psSearchString, $psFullString, $piCutOff) {
  $msFullString = strip_tags($psFullString);
  $msFullString = str_replace($psSearchString,'<b>'.$psSearchString.'</b>',$msFullString);
  // for necess�rio para n�o quebrar caracteres unicodo no meio, gerando erro na pesquisa (n�o achei uma maneir melhor de fazer isso)
  for ($miI = ($piCutOff-1); (($miI < strlen($msFullString)) && ($msFullString[$miI]!=" ")); $miI++) {
    $piCutOff++;
  }  
  $msFullString = substr($msFullString, 0, $piCutOff - 1);
  return $msFullString . "...";
}

function truncate($psString, $piChars) {
  if (strlen($psString) > $piChars)
    return substr($psString,0,$piChars-3)."...";
  else
    return $psString;
}

class GridContextSearch extends FWDDrawGrid {
  public function drawItem(){
    switch($this->ciColumnIndex){
      case 1:
        $miType = $this->caData[2];
        $miId = $this->caData[3];
        $msString = $this->caData[4];
        
        $moContextName = FWDWebLib::getObject('context_name');
        $moContextName->setAttrDisplay("true");
        $moContextPreview = FWDWebLib::getObject('context_preview');
        $moContextPreview->setAttrDisplay("true");
        $moContextPath = FWDWebLib::getObject('context_path');
        $moContextPath->setAttrDisplay("true");
        $moToolTipName = FWDWebLib::getObject('tooltip_name');
        $moToolTipPreview = FWDWebLib::getObject('tooltip_preview');
        $moToolTipPath = FWDWebLib::getObject('tooltip_path');
        
        $moContextObject = new ISMSContextObject();
        $moContext = $moContextObject->getContextObject($miType);
        $moContext->fetchById($miId);
        $msIconCode = $moContext->getIconCode($miId,true,true);
    
        // Nome do contexto linkando para janela de visualiza��o
        $moContextName->setAttrStringNoEscape("true");
        $msContextName = $moContext->getName();
        if (strlen($msContextName) > 90) {
          $moToolTipName->setValue($msContextName,true);
          $msContextName = makePreview("", $msContextName, 90);
        } else{
          $moToolTipName->setValue("",true);
        }
        if ($miType==CONTEXT_USER){
          $moContextName->setValue("{$msContextName}");
        }elseif ($miType==CONTEXT_DOCUMENT) {
          $moContextName->setValue("{$msContextName}");
        } else{
          $moContextName->setValue("<a href='javascript:open_visualize({$miId},{$miType},\"".uniqid()."\")'><b>{$msContextName}</b></a>");
        }
        $moContextName->setAttrClass("SearchResultName");
        
        // Preview do contexto
        $msToolTipValue = ISMSLib::truncateString($msString, 500);
        
        $moToolTipPreview->setValue($msToolTipValue,true);
        $moToolTipPreview->setAttrWidth(478);
        
        $msSearchString = FWDWebLib::convertToISO(FWDWebLib::getObject('new_string')->getValue(),true);
        if (trim($msSearchString))
          $moContextPreview->setValue(makePreview($msSearchString, $msString, 120));
        $moContextPreview->setAttrClass("SearchResultPreview");
        
        // Caminho para se chegar no contexto pelo sistema
        $moContextPath->setAttrStringNoEscape("true");

        $msContextPathAux = str_replace("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;","-> ",$moContext->getSystemPath());

        $msSearchPath = "[ ";
        $maSearchPaths = explode("->",$msContextPathAux);
        
        $moToolTipPath->setValue(strip_tags(str_replace("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;","",$msContextPathAux)),true);
        foreach($maSearchPaths as $miIndex=>$msSearchPathPart) {
          //$msSearchPathPart = preg_replace("/(<a[^>]*>)([^<]*)(<\/a>)/e","'\\1'.truncate('\\2',20).'\\3'",$msSearchPathPart);
          if ($miIndex==count($maSearchPaths)-1) {
            $msSearchPath .= "<font class='SearchResultPath_Bold'>".$msSearchPathPart."</font>&nbsp;&nbsp;&nbsp;";
          } else {
            $msSearchPath .= "<font class='SearchResultPath'>".$msSearchPathPart." </font>&nbsp;&nbsp;&nbsp;";
          }
        }
        $msSearchPath .= "]";

        //$moEvent = new FWDEventHandler($moToolTipPath->getAttrEvent(),'teste_'.$this->ciRowIndex);        
        //$moEvent->setAttrContent($moToolTipPath);
        
        $msRender = ""; //$moEvent->render();
        //div para colocar o conte�do do path que foi gerado, teve que ser feito via div
        //porque a posi��o dos �cones quando estava sendo usado o static �ra totalmente
        //diferente entre o IE e o Firefox                  
        $moDiv = new FWDDiv($moContextPath->getObjFWDBox());
        $moDiv->setClass("SearchResultPath");
        $moDiv->setExtraAttribute($msRender);
        $moDiv->setValue($msSearchPath);
        
        return $msIconCode . $moContextName->draw().$moContextPreview->draw().$moDiv->draw();
      break;
    }
  }
}

/* 
 * Fun��o callback para retirar do array a string all que � retornada
 * quando se utiliza um checkbox para selecionar todos os outros elementos.
 */
function extract_string_all ($psValue) {return ($psValue=='all')?0:1;}

/*
 * Fun��o que retorna os tipos de contextos que devem ser filtrados na busca.
 */
function getContextTypes() {
  $maContextTypes = array_filter(explode(":", FWDWebLib::getObject('var_context_type_controller')->getValue()));
  if (in_array(CONTEXT_EVENT, $maContextTypes)) $maContextTypes[] = CONTEXT_CATEGORY;
  if (in_array(CONTEXT_BEST_PRACTICE, $maContextTypes)) $maContextTypes[] = CONTEXT_SECTION_BEST_PRACTICE; 
  if (in_array('all', $maContextTypes)) $maContextTypes = array_filter($maContextTypes, 'extract_string_all');
  return $maContextTypes;
}

/*
 * Fun��o que atualiza os timestamps dos variables. 
 */
function updateTimestamps() {
  FWDWebLib::getObject('var_creation_date_start')->setValue(FWDWebLib::getObject('creation_date_start')->getTimestamp());
  FWDWebLib::getObject('var_creation_date_end')->setValue(FWDWebLib::getObject('creation_date_end')->getTimestamp());
  FWDWebLib::getObject('var_edition_date_start')->setValue(FWDWebLib::getObject('edition_date_start')->getTimestamp());
  FWDWebLib::getObject('var_edition_date_end')->setValue(FWDWebLib::getObject('edition_date_end')->getTimestamp());
}

class SearchContextEvent extends FWDRunnable {
  public function run(){
    $moGrid = FWDWebLib::getObject('grid_context_search');
    $moGrid->execEventPopulate();
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new SearchContextEvent('search_context_event'));
    
    if (FWDWebLib::getObject("update_timestamps")->getValue()=='1') {
      updateTimestamps();
      echo "gebi('update_timestamps').value = '0';";
    }
    
    $moGrid = FWDWebLib::getObject("grid_context_search");
    $moHandler = new QueryGridContextSearch(FWDWebLib::getConnection());

    $stringFilter = str_replace("\\","\\\\\\\\",FWDWebLib::convertToISO(FWDWebLib::getObject('var_new_string')->getValue(),true));
    $stringFilter = str_replace("/","\\/",$stringFilter);
    
    $stringFilter = str_replace("'","\'",$stringFilter);
    $stringFilter = str_replace("{","\\{",$stringFilter);
    $stringFilter = str_replace(".","\\.",$stringFilter);
    $stringFilter = str_replace("*","\\*",$stringFilter);
    $stringFilter = str_replace("(","\\(",$stringFilter);
    $stringFilter = str_replace(")","\\)",$stringFilter);
    $stringFilter = str_replace("&","\\&",$stringFilter);
    $stringFilter = str_replace("%","\\\\%%",$stringFilter);
    $stringFilter = str_replace("}","\\}",$stringFilter);
    $stringFilter = str_replace("]","\\]",$stringFilter);
    $stringFilter = str_replace("[","\\[",$stringFilter);

    $moHandler->setStringFilter($stringFilter);
    $contextTypes = getContextTypes();
    
    $moHandler->setContextTypes(getContextTypes());
    $moHandler->setCreationDateStart(FWDWebLib::getObject('var_creation_date_start')->getValue());
    $moHandler->setCreationDateEnd(FWDWebLib::getObject('var_creation_date_end')->getValue());
    $moHandler->setEditionDateStart(FWDWebLib::getObject('var_edition_date_start')->getValue());
    $moHandler->setEditionDateEnd(FWDWebLib::getObject('var_edition_date_end')->getValue());
    
    if(FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(POLICY_MODE)){
      $moHandler->setIsAdvancedSearchDocument(false);
      $moHandler->setIsAdvancedSearchTemplate(false);
    }
    
    $moGrid->setQueryHandler($moHandler);
    $moGrid->setObjFwdDrawGrid(new GridContextSearch());
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $msStringParameter = utf8_encode(FWDWebLib::getObject('string')->getValue());
    if ($msStringParameter) {
      FWDWebLib::getObject('new_string')->setValue($msStringParameter);
      FWDWebLib::getObject('var_new_string')->setValue($msStringParameter);
      FWDWebLib::getObject('var_context_type_controller')->setValue(FWDWebLib::getObject('context_type_controller')->getValue());
      FWDWebLib::getObject('update_timestamps')->setValue('1');
      FWDWebLib::getObject('grid_context_search')->setAttrPopulate('true');
    }
    
    //caso a licen�a nao d� permiss�o para o modulo de documenta��o
    if(!FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(POLICY_MODE)){
      FWDWebLib::getObject('document_filter_search')->setShouldDraw(false);
	  
      $templateFilterSearch = FWDWebLib::getObject('template_filter_search');
      if($templateFilterSearch)
      	$templateFilterSearch->setShouldDraw(false);
    }

    //instala a seguran�a de ACL na p�gina
    $maItems = FWDWebLib::getObject('context_type_controller')->getContent();
    $maNotAlloewd = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
    foreach($maItems as $moItem){
      if($moItem->getAttrTag()){
          $maTags = explode(':',$moItem->getAttrTag());
          foreach($maTags as $msTag){
            if (($msTag) && in_array($msTag,$maNotAlloewd)){
              $moItem->setAttrDisabled("true");
              $moItem->setAttrCheck('false');
            }
          }
       }
    }
    
    //instala a seguran�a de ACL na p�gina
    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
    
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
?>
<script language="javascript">
  gebi('new_string').focus();

  function refresh_grid() {
    gobi("grid_context_search").setPopulate(true);
    js_refresh_grid('grid_context_search');
  }
</script>
<?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("nav_search.xml");
?>