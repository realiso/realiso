<?php
  
  class MigDataSet extends FWDDBDataSet{
     /**
    * Busca um registro do resultado e avan�a para o pr�ximo.
    *
    * <p>M�todo para buscar um registro do resultado e avan�ar para o pr�ximo.</p>
    * @access public
    * @return boolean Verdadeiro (sucesso) ou Falso (falha)
    */
    public function fetch(){
      $maData = $this->coDatabase->fetch();
      if($maData && ($this->ciLimit==0 || $this->ciCurrentRow < $this->ciLimit)){
        $this->ciCurrentRow++;
        foreach($maData as $msAlias => $msData){
          $moField = $this->getFieldByAlias($msAlias);
          if($moField){
            $moField->setValue($msData);
          }else{
              $this->addFWDDBField(new FWDDBField($msAlias, $msAlias, DB_UNKNOWN, $msData));
          }
        }
        return true;
      }else{
        return false;
      }
    }
  }
  
  class Import_v2 {
    protected $cbIsMigratable=true; //false quando for passado um par�metro inv�lido para o construtor da classe.
    protected $coSrcDatabase; //BD antigo
    protected $coDstDatabase; //BD novo
    protected $caMapState=array(); //mapeamento dos estados antigos para os novos.
    protected $csSrcTableName; //nome da tabela no bd origem, se diferente da tabela do bd destino.
    protected $csDstTableName; //nome da tabela no bd destino.
    protected $caDstFieldName=array(); //array contendo o nome dos campos no bd destino.
    protected $caDstFieldAlias=array(); //array contendo o alias dos campos no bd destino.
    protected $caDstFieldType=array(); //array contendo o tipo dos campos no bd destino.
    protected $caDstFieldMap=array(); //array contendo o campo no bd src equivalente ao campo no bd destino.
    protected $csSrcFields; //Campos para pesquisa do bd origem.
    protected $csSrcOrderBy; //campos da instru��o order by.
    protected $cbInsertContext; //true se deve inserir contexto para a tabela. false caso contrario.
    protected $ciContextType; //tipo do contexto que dever� ser inserido, caso cbInsertContext seja true;
    protected $cbContextStateAsField=false; //true se o estado do contexto deve vir do banco antigo.
    protected $csContextStateField; //nome do campo de estado do contexto, quando vier do banco antigo.
    protected $ciStaticContextState; //estado do contexto, quando o mesmo nao vier do banco antigo.
    protected $caResultNewIds=array(); //mapeamento de id antiga para o contexto inserido.
    protected $caExternalIds=array(); //array de arrays de mapeamentos de ids inseridos anteriormente, que serao usados na tabela atual.
    protected $csSrcPkId; //Campo que contem a id na tabela do bd origem.
    protected $caCompareName=array(); //array com nome dos campos cujos textos serao comparados.
    protected $caCompareParent=array(); //array com o id do pai de cada registro.
    protected $caCompareField=array(); //array com os campos para comparacao da tabela origem
    protected $cbCompareRecursive; //true se a comparacao deve ser feita recursivamente, ou seja, se deve-se verificar o pai de cada pai e assim por diante.
    protected $csCompareExternal; //se o pai deve vir de uma tabela inserida anteriormente, indica o nome da tabela externa.
    protected $cbExternalContext=false; //usado pelo wkf_task para dizer que o contexto vem de uma tabela externa.
    protected $cbInsertOnExternalNotFound=false; //quando a id externa nao for encontrada, deve inserir mesmo assim se true, e nao insere se false.
    protected $caFieldStaticValue=array();
    protected $caCtrlPeriod = array(); //mapeamento do per�odo de schedule dos controles
    protected $caNotMandatoryExternal=array(); //array com os externals que n�o s�o obrigatorios. => Corre��o de bug que nao inseria riscos.
    
    
    protected $cbHasContextCreator = false; // indica se o usu�rio criador/respons�vel deve ser inserido em isms_context_date
    protected $csContextCreatorIdField = ''; // alias do campo que identifica o usu�rio
    protected $csContextCreatorNameField = ''; // alias do campo do nome do usu�rio
    protected $caContextCreatorMap = array(); // array com o mapeamento dos ids dos usu�rios
    
    /**
     * M�todo construtor da classe. Gera vari�veis que ser�o usadas na migra��o do bd antigo para o novo.
     * @param string psTable Nome da tabela referente ao novo bd.
     * @param mixed pmExternalIds Um ou mais arrays contendo mapeamento de ids para novos contextos.
     */
    public function __construct($poSrcDatabase, $poDstDatabase, $psTable='',$pmExternalIds='') {
      $this->coSrcDatabase = $poSrcDatabase;
      $this->coDstDatabase = $poDstDatabase;
      $mmExternalIds = array();
      for ($i = 3; $i < func_num_args(); ++$i) {
         $mmExternalIds[] = func_get_arg($i);
      }
      
      /* Mapeamento dos estados do bd antigo para o novo */
      $this->csContextStateField="nState";
      $this->caMapState[0] = 2700;
      $this->caMapState[1] = 2701;
      $this->caMapState[2] = 2702;
      $this->caMapState[3] = 2703;
      $this->caMapState[4] = 2704;
      // mapeamento do periodo de schedule dos controles
      $this->caCtrlPeriod[1] = 7801; 
      $this->caCtrlPeriod[2] = 7802;
      $this->caCtrlPeriod[3] = 7803;
      
      switch($psTable) {
        case "isms_profile":
          $this->csDstTableName="isms_profile";
          $this->caDstFieldName=array("fkContext","sName");
          $this->caDstFieldAlias=array("context_id","name");
          $this->caDstFieldType=array(DB_NUMBER,DB_STRING);
          $this->caDstFieldMap=array(null,"zName");
          $this->cbInsertContext=true;
          $this->ciContextType=2817;
          $this->ciStaticContextState=2700;
          $this->csSrcPkId = "skProfile";
        break;
        case "isms_user":
          $this->csDstTableName="isms_user";
          $this->caDstFieldName=array("fkContext","fkProfile","sName","sLogin","sPassword","sEmail","nIP","nLanguage","dLastLogin");
          $this->caDstFieldAlias=array("context_id","profile_id","name","login","password","email","IP","language","last_login");
          $this->caDstFieldType=array(DB_NUMBER,DB_NUMBER,DB_STRING,DB_STRING,DB_STRING,DB_STRING,DB_NUMBER,DB_NUMBER,DB_DATETIME);
          $this->caDstFieldMap=array(null,"skProfile","zName","zLogin","zPassword","zEmail","nIP","nLanguage","dLastLogin");
          $this->cbInsertContext=true;
          $this->ciContextType=2816;
          $this->ciStaticContextState=2702;
          $this->csSrcPkId = "skUser";
          $this->caExternalIds["profile_id"] = $mmExternalIds[0];
        break;
        case "isms_config":
          $this->caExternalIds["user_id"] = $mmExternalIds[0];
        break;
        case "rm_area":	
        	$this->csSrcFields = "skArea, skParent, a.skUser, a.zName, zDescription, nValue, nState, u.zName as user_name";
          $this->csSrcTableName="rm_area a join isms_user u on(a.skUser=u.skUser)";
          $this->cbHasContextCreator = true;
          $this->csContextCreatorIdField = 'skUser';
          $this->csContextCreatorNameField = 'user_name';	
          $this->csDstTableName="rm_area";
          $this->csSrcOrderBy="skArea ASC";
          $this->caDstFieldName=array("fkContext","fkParent","fkResponsible","sName","tDescription","nValue");
          $this->caDstFieldAlias=array("context_id","parent_id","responsible_id","name","description","value");
          $this->caDstFieldType=array(DB_NUMBER,DB_NUMBER,DB_NUMBER,DB_STRING,DB_STRING,DB_NUMBER);
          $this->caDstFieldMap=array(null,"skParent","skUser","zName","zDescription","nValue");
          $this->cbInsertContext=true;
          $this->ciContextType=2801;
          $this->cbContextStateAsField=true;
          $this->csContextStateField='nState';
          $this->caContextCreatorMap = $mmExternalIds[0];
          $this->csSrcPkId = "skArea";
          $this->caExternalIds["responsible_id"] = $mmExternalIds[0];
          $this->caExternalIds["parent_id"] = &$this->caResultNewIds;
          $this->cbInsertOnExternalNotFound = true;
        break;
        case "rm_process":
        	$this->csSrcFields = "skProcess, skArea, p.skUser, p.zName, zDescription, nValue, zLink, nState, u.zName as user_name";
          $this->csSrcTableName="rm_process p join isms_user u on(p.skUser=u.skUser)";
          $this->cbHasContextCreator = true;
          $this->csContextCreatorIdField = 'skUser';
          $this->csContextCreatorNameField = 'user_name';
          $this->csDstTableName="rm_process";
          $this->caDstFieldName=array("fkContext","fkArea","fkResponsible","sName","tDescription","nValue","sDocument");
          $this->caDstFieldAlias=array("context_id","area_id","responsible_id","name","description","value","document");
          $this->caDstFieldType=array(DB_NUMBER,DB_NUMBER,DB_NUMBER,DB_STRING,DB_STRING,DB_NUMBER,DB_STRING);
          $this->caDstFieldMap=array(null,"skArea","skUser","zName","zDescription","nValue","zLink");
          $this->cbInsertContext=true;
          $this->ciContextType=2802;
          $this->cbContextStateAsField=true;
          $this->csContextStateField='nState';
          $this->caContextCreatorMap = $mmExternalIds[1];
          $this->csSrcPkId = "skProcess";
          $this->caExternalIds["area_id"] = $mmExternalIds[0];
          $this->caExternalIds["responsible_id"] = $mmExternalIds[1];
        break;
        case "rm_category":
          $this->csDstTableName="rm_category";
          $this->csSrcOrderBy="skCategory ASC";
          $this->caDstFieldName=array("fkContext","fkParent","sName","tDescription");
          $this->caDstFieldAlias=array("context_id","parent_id","name","description");
          $this->caDstFieldType=array(DB_NUMBER,DB_NUMBER,DB_STRING,DB_STRING);
          $this->caDstFieldMap=array(null,"skParent","zName","zDescription");
          $this->cbInsertContext=true;
          $this->ciContextType=2806;
          $this->ciStaticContextState=2702;
          $this->csSrcPkId = "skCategory";
          $this->caExternalIds["parent_id"] = &$this->caResultNewIds;
          $this->buildCompare("rm_category");
          $this->cbInsertOnExternalNotFound = true;
        break;
        case "rm_asset":
        	$this->csSrcFields = "skAsset, skCategory, a.skUser, a.skSecurityResponsible, a.zName, zDescription, nValue, nState, u.zName as user_name";
          $this->csSrcTableName="rm_asset a join isms_user u on(a.skUser=u.skUser)";
          $this->cbHasContextCreator = true;
          $this->csContextCreatorIdField = 'skUser';
          $this->csContextCreatorNameField = 'user_name';          
          $this->csDstTableName="rm_asset";
          $this->caDstFieldName=array("fkContext","fkCategory","fkResponsible","fkSecurityResponsible","sName","tDescription","nValue");
          $this->caDstFieldAlias=array("context_id","category_id","responsible_id","security_responsible_id","name","description","value");
          $this->caDstFieldType=array(DB_NUMBER,DB_NUMBER,DB_NUMBER,DB_NUMBER,DB_STRING,DB_STRING,DB_NUMBER);
          $this->caDstFieldMap=array(null,"skCategory","skUser","skSecurityResponsible","zName","zDescription","nValue");
          $this->cbInsertContext=true;
          $this->ciContextType=2803;
          $this->cbContextStateAsField=true;
          $this->csContextStateField='nState';
          $this->caContextCreatorMap = $mmExternalIds[1];
          $this->csSrcPkId="skAsset";
          $this->caExternalIds["category_id"]=$mmExternalIds[0];
          $this->caExternalIds["responsible_id"]=$mmExternalIds[1];
          $this->caExternalIds["security_responsible_id"]=$mmExternalIds[1];
        break;
        case "rm_asset_value":
          $this->csDstTableName="rm_asset_value";
          $this->caExternalIds["asset_id"]=$mmExternalIds[0];
          $this->caExternalIds["parameter_name"]=$mmExternalIds[1];
          $this->caExternalIds["parameter_value_name"]=$mmExternalIds[2];
        break;
        case "rm_process_asset":        	
        	$this->csSrcFields = "skProcessAsset,skProcess, skAsset, nState, skCreator, u.zName as user_name";
          $this->csSrcTableName="rm_process_asset pa join isms_user u on(pa.skCreator=u.skUser)";
          $this->cbHasContextCreator = true;
          $this->csContextCreatorIdField = 'skCreator';
          $this->csContextCreatorNameField = 'user_name';
          $this->csDstTableName="rm_process_asset";
          $this->caDstFieldName=array("fkContext","fkProcess","fkAsset");
          $this->caDstFieldAlias=array("context_id","process_id","asset_id");
          $this->caDstFieldType=array(DB_NUMBER,DB_NUMBER,DB_NUMBER);
          $this->caDstFieldMap=array(null,"skProcess","skAsset");
          $this->cbInsertContext=true;
          $this->ciContextType=2810;
          $this->cbContextStateAsField=true;
          $this->csContextStateField='nState';
          $this->caContextCreatorMap = $mmExternalIds[2];
          $this->csSrcPkId="skProcessAsset";
          $this->caExternalIds["process_id"]=$mmExternalIds[0];
          $this->caExternalIds["asset_id"]=$mmExternalIds[1];
        break;
        case "rm_event":
          $this->csSrcTableName="rm_threat t JOIN rm_threat_category c ON (t.skThreat = c.skThreat)";
          $this->csDstTableName="rm_event";
          $this->caDstFieldName=array("fkContext","fkCategory","sDescription","tObservation");
          $this->caDstFieldAlias=array("context_id","category_id","description","observation");
          $this->caDstFieldType=array(DB_NUMBER,DB_NUMBER,DB_STRING,DB_STRING);
          $this->caDstFieldMap=array(null,"skCategory","zName","zDescription");
          $this->cbInsertContext=true;
          $this->ciContextType=2807;
          $this->ciStaticContextState=2702;
          $this->csSrcPkId="skThreat";
          $this->caExternalIds["category_id"]=$mmExternalIds[0];
          $this->buildCompare("rm_event");
        break;
        case "rm_risk":        	
        	$this->csSrcFields = "skRisk, skThreat, skAsset, r.zName, zDescription, nValue, nValueResidual, bAcceptRisk, nAcceptState, zAcceptJustification, nState, skCreator, u.zName as user_name";
          $this->csSrcTableName="rm_risk r join isms_user u on(r.skCreator=u.skUser)";
          $this->cbHasContextCreator = true;
          $this->csContextCreatorIdField = 'skCreator';
          $this->csContextCreatorNameField = 'user_name';
          $this->csDstTableName="rm_risk";
          $this->caDstFieldName=array("fkContext","fkEvent","fkAsset","sName","tDescription","nValue","nValueResidual","nAcceptMode","nAcceptState","sAcceptJustification");
          $this->caDstFieldAlias=array("context_id","event_id","asset_id","name","description","value","value_residual","accept_mode","accept_state","accept_justification");
          $this->caDstFieldType=array(DB_NUMBER,DB_NUMBER,DB_NUMBER,DB_STRING,DB_STRING,DB_NUMBER,DB_NUMBER,DB_NUMBER,DB_NUMBER,DB_STRING);
          $this->caDstFieldMap=array(null,"skThreat","skAsset","zName","zDescription","nValue","nValueResidual","bAcceptRisk","nAcceptState","zAcceptJustification");
          $this->cbInsertContext=true;
          $this->ciContextType=2804;
          $this->cbContextStateAsField=true;
          $this->csContextStateField='nState';
          $this->caContextCreatorMap = $mmExternalIds[2];
          $this->csSrcPkId="skRisk";
          $this->caExternalIds["event_id"]=$mmExternalIds[0];
          $this->caExternalIds["asset_id"]=$mmExternalIds[1];
          $this->caNotMandatoryExternal[]="event_id";
        break;
        case "rm_risk_value":
          $this->csDstTableName="rm_risk_value";
          $this->caExternalIds["risk_id"]=$mmExternalIds[0];
          $this->caExternalIds["parameter_name"]=$mmExternalIds[1];
          $this->caExternalIds["parameter_value_name"]=$mmExternalIds[2];
        break;
        case "rm_asset_asset":
          $this->csDstTableName="rm_asset_asset";
          $this->caDstFieldName=array("fkAsset","fkDependent");
          $this->caDstFieldAlias=array("asset_id","dependent_id");
          $this->caDstFieldType=array(DB_NUMBER,DB_NUMBER);
          $this->caDstFieldMap=array("skAsset","skDependent");
          $this->cbInsertContext=false;
          $this->caExternalIds["asset_id"]=$mmExternalIds[0];
          $this->caExternalIds["dependent_id"]=$mmExternalIds[0];
        break;
        case "rm_control":
        	$this->csSrcFields = "skControl, c.skUser, c.zName, zDescription, zDocuments, bIsActive, bFlagImplAlert, bFlagImplExpired, dDateImplementation, dDateRealization, nState, u.zName as user_name";
          $this->csSrcTableName="rm_control c join isms_user u on(c.skUser=u.skUser)";
          $this->cbHasContextCreator = true;
          $this->csContextCreatorIdField = 'skUser';
          $this->csContextCreatorNameField = 'user_name';        
          $this->csDstTableName="rm_control";
          $this->caDstFieldName=array("fkContext","fkResponsible","sName","tDescription","sDocument","bIsActive","bFlagImplAlert","bFlagImplExpired","dDateDeadline","dDateImplemented");
          $this->caDstFieldAlias=array("context_id","responsible_id","name","description","document","is_active","flag_impl_alert","flag_impl_expired","date_deadline","date_implemented");
          $this->caDstFieldType=array(DB_NUMBER,DB_NUMBER,DB_STRING,DB_STRING,DB_STRING,DB_NUMBER,DB_NUMBER,DB_NUMBER,DB_DATETIME,DB_DATETIME);
          $this->caDstFieldMap=array(null,"skUser","zName","zDescription","zDocuments","bIsActive","bFlagImplAlert","bFlagImplExpired","dDateImplementation","dDateRealization");
          $this->cbInsertContext=true;
          $this->ciContextType=2805;
          $this->cbContextStateAsField=true;          
          $this->csContextStateField='nState';
          $this->caContextCreatorMap = $mmExternalIds[0];          
          $this->csSrcPkId="skControl";
          $this->caExternalIds["responsible_id"]=$mmExternalIds[0];
        break;
        case "rm_risk_control":	
        	$this->csSrcFields = "skRiskControl, skRisk, skControl, zJustificative, nState, skCreator, u.zName as user_name";
          $this->csSrcTableName="rm_risk_control rc join isms_user u on(rc.skCreator=u.skUser)";
          $this->cbHasContextCreator = true;
          $this->csContextCreatorIdField = 'skCreator';
          $this->csContextCreatorNameField = 'user_name';        	
          $this->csDstTableName="rm_risk_control";
          $this->caDstFieldName=array("fkContext","fkRisk","fkControl","tJustification");
          $this->caDstFieldAlias=array("context_id","risk_id","control_id","justification");
          $this->caDstFieldType=array(DB_NUMBER,DB_NUMBER,DB_NUMBER,DB_STRING);
          $this->caDstFieldMap=array(null,"skRisk","skControl","zJustificative");
          $this->cbInsertContext=true;
          $this->ciContextType=2813;
          $this->cbContextStateAsField=true;
          $this->csContextStateField='nState';
          $this->caContextCreatorMap = $mmExternalIds[2];
          $this->csSrcPkId="skRiskControl";
          $this->caExternalIds["risk_id"]=$mmExternalIds[0];
          $this->caExternalIds["control_id"]=$mmExternalIds[1];
        break;
        case "rm_risk_control_value":
          $this->csDstTableName="rm_risk_control_value";
          $this->caExternalIds["risk_control_id"]=$mmExternalIds[0];
          $this->caExternalIds["parameter_name"]=$mmExternalIds[1];
          $this->caExternalIds["rc_parameter_value_name"]=$mmExternalIds[2];
        break;
        case "rm_section_best_practice":
          $this->csSrcTableName="rm_section_template";
          $this->csSrcOrderBy="skSectionTemplate ASC";
          $this->csDstTableName="rm_section_best_practice";
          $this->caDstFieldName=array("fkContext","fkParent","sName");
          $this->caDstFieldAlias=array("context_id","parent_id","name");
          $this->caDstFieldType=array(DB_NUMBER,DB_NUMBER,DB_STRING);
          $this->caDstFieldMap=array(null,"skParentTemplate","zName");
          $this->cbInsertContext=true;
          $this->ciContextType=2811;
          $this->ciStaticContextState=2702;
          $this->csSrcPkId="skSectionTemplate";
          $this->caExternalIds["parent_id"]=&$this->caResultNewIds;
          $this->buildCompare("rm_section_best_practice");
          $this->cbInsertOnExternalNotFound = true;
        break;
        case "rm_best_practice":
          $this->csSrcTableName="rm_template";
          $this->csDstTableName="rm_best_practice";
          $this->caDstFieldName=array("fkContext","fkSectionBestPractice","sName","tDescription","nControlType","sClassification","tImplementationGuide","tMetric","tJustification","sDocument");
          $this->caDstFieldAlias=array("context_id","section_best_practice_id","name","description","control_type","classification","implementation_guide","metric","justification","document");
          $this->caDstFieldType=array(DB_NUMBER,DB_NUMBER,DB_STRING,DB_STRING,DB_NUMBER,DB_STRING,DB_STRING,DB_STRING,DB_STRING,DB_STRING);
          $this->caDstFieldMap=array(null,"skSectionTemplate","zName","zDescription","nControlType","zClassification","zImplementationGuide","zMetric","zJustification","zReference");
          $this->cbInsertContext=true;
          $this->ciContextType=2808;
          $this->ciStaticContextState=2702;
          $this->csSrcPkId="skTemplate";
          $this->caExternalIds["section_best_practice_id"]=$mmExternalIds[0];
          $this->buildCompare("rm_best_practice");
        break;
        case "rm_control_best_practice":
          $this->csSrcTableName="rm_control c JOIN rm_template t ON (c.skTemplate = t.skTemplate)";
          $this->csDstTableName="rm_control_best_practice";
          $this->caDstFieldName=array("fkContext","fkBestPractice","fkControl");
          $this->caDstFieldAlias=array("context_id","best_practice_id","control_id");
          $this->caDstFieldType=array(DB_NUMBER,DB_NUMBER,DB_NUMBER);
          $this->caDstFieldMap=array(null,"skTemplate","skControl");
          $this->cbInsertContext=true;
          $this->ciContextType=2815;
          $this->ciStaticContextState=2702;
          $this->csSrcPkId="skControl";
          $this->caExternalIds["best_practice_id"]=$mmExternalIds[0];
          $this->caExternalIds["control_id"]=$mmExternalIds[1];
        break;
        case "rm_control_efficiency_history":
          $this->csSrcTableName="rm_history_control";
          $this->csDstTableName="rm_control_efficiency_history";
          $this->caDstFieldName=array("fkControl","dDateTodo","dDateAccomplishment","nRealEfficiency","nExpectedEfficiency");
          $this->caDstFieldAlias=array("control_id","date_todo","date_accomplishment","real_efficiency","expected_efficiency");
          $this->caDstFieldType=array(DB_NUMBER,DB_DATETIME,DB_DATETIME,DB_NUMBER,DB_NUMBER);
          $this->caDstFieldMap=array("skControl","dDateTodo","dDateRealized","nRealEfficiency","nExpectedEfficiency");
          $this->cbInsertContext=false;
          $this->caExternalIds["control_id"]=$mmExternalIds[0];
        break;
        case "wkf_control_efficiency":
          $this->csSrcTableName="wkf_schedule s JOIN rm_control c ON (c.skSchedule = s.skSchedule) where bEnable=1";
          $this->csDstTableName="wkf_control_efficiency";
          $this->caDstFieldName=array("fkControlEfficiency","nRealEfficiency","nExpectedEfficiency","sMetric1","sMetric2","sMetric3","sMetric4","sMetric5","dDateNext","dDateLimit","nValue","nPeriod","bFlagAlert");
          $this->caDstFieldAlias=array("control_efficiency_id","real_efficiency","expected_efficiency","metric1","metric2","metric3","metric4","metric5","date_next","date_limit","value","period","flag_alert");
          $this->caDstFieldType=array(DB_NUMBER,DB_NUMBER,DB_NUMBER,DB_STRING,DB_STRING,DB_STRING,DB_STRING,DB_STRING,DB_DATETIME,DB_DATETIME,DB_NUMBER,DB_NUMBER,DB_NUMBER);
          $this->caDstFieldMap=array("skSchedule","nRealEfficiency","nExpectedEfficiency","zMetric1","zMetric2","zMetric3","zMetric4","zMetric5","dDateNext","dDateLimit","nValue","nPeriod","bFlagAlert");
          $this->cbInsertContext=false;
          $this->caExternalIds["control_efficiency_id"]=$mmExternalIds[0];
          $this->caExternalIds["period"]=$this->caCtrlPeriod;
        break;
        case "rm_risk_limits":
          $this->csSrcTableName="isms_configuration";
          $this->csDstTableName="rm_risk_limits";
          $this->caDstFieldName=array("fkContext","nLow","nHigh");
          $this->caDstFieldAlias=array("context_id","low","high");
          $this->caDstFieldType=array(DB_NUMBER,DB_NUMBER,DB_NUMBER);
          $this->caDstFieldMap=array(null,"nLowRisk","nHighRisk");
          $this->cbInsertContext=true;
          $this->ciContextType=2822;
          $this->csContextStateField="nRiskState";
          $this->csSrcPkId="skPolicy";
        break;
        case "wkf_task":
          //$this->csSrcTableName="wkf_task t JOIN context c ON t.skContext = c.skContext";
          $this->csSrcTableName=" wkf_task t JOIN context c ON (t.skContext = c.skContext) WHERE skTask NOT IN (select skTask from wkf_task WHERE skReceiver = skCreator AND bVisible = 0) ";
          $this->csDstTableName="wkf_task";
          $this->caDstFieldName=array("pkTask","fkContext","fkCreator","fkReceiver","nActivity","bVisible","dDateAccomplished","dDateCreated","bEmailSent");
          $this->caDstFieldAlias=array("task_id","context_id","creator_id","receiver_id","activity","visible","date_realized","date_created","email_sent");
          $this->caDstFieldType=array(DB_NUMBER,DB_NUMBER,DB_NUMBER,DB_NUMBER,DB_NUMBER,DB_NUMBER,DB_DATETIME,DB_DATETIME,DB_NUMBER);
          $this->caDstFieldMap=array(null,"skId","skCreator","skReceiver","nActivity","bVisible","dDateRealized","dDateCreated",null);
          $this->csSrcPkId="skTask";
          $this->cbInsertContext=false;
          $this->cbExternalContext=true;
          $maAreaActivity = array(3=>2201,6=>2202,9=>2203,12=>2204,14=>2205,16=>2206,19=>2207,20=>2208,22=>2209,24=>2210);
          $this->caExternalIds["activity"]=$maAreaActivity;
          $this->caExternalIds["creator_id"]=$mmExternalIds[8];
          $this->caExternalIds["receiver_id"]=$mmExternalIds[8];
          $this->caExternalIds[1]=$mmExternalIds[0];
          $this->caExternalIds[2]=$mmExternalIds[1];
          $this->caExternalIds[3]=$mmExternalIds[2];
          $this->caExternalIds[4]=$mmExternalIds[3];
          $this->caExternalIds[5]=$mmExternalIds[4];
          $this->caExternalIds[6]=$mmExternalIds[5];
          $this->caExternalIds[7]=$mmExternalIds[6];
          $this->caExternalIds[8]=$mmExternalIds[3];
          $this->caExternalIds[9]=$mmExternalIds[7];
          $this->caFieldStaticValue["email_sent"]=1;
        break;
        case "wkf_alert":
          $this->csSrcTableName="wkf_alert a left join wkf_task t on (a.sktask=t.sktask)";
          $this->csDstTableName="wkf_alert";
          $this->caDstFieldName=array("pkAlert","fkContext","fkReceiver","fkCreator","tJustification","nType","dDate");
          $this->caDstFieldAlias=array("alert_id","task_id","receiver_id","creator_id","justification","type","alert_date");
          $this->caDstFieldType=array(DB_NUMBER,DB_NUMBER,DB_NUMBER,DB_NUMBER,DB_STRING,DB_NUMBER,DB_DATETIME);
          $this->caDstFieldMap=array(null,"skTask","skUser","skCreator","zJustification","nStatus","dDate");
          $this->cbInsertContext=false;
          $maAlertStatus = array(1=>9201,2=>9202,3=>9203,4=>9204,5=>9205,6=>9206,7=>9207,8=>9208);
          $this->caExternalIds["type"]=$maAlertStatus;
          $this->caExternalIds["task_id"]=$mmExternalIds[0];
          $this->caExternalIds["creator_id"]=$mmExternalIds[1];
          $this->caExternalIds["receiver_id"]=$mmExternalIds[1];
        break;
        
        default:
          $this->cbIsMigratable = false;
        break;
      }
    }
    
    /** 
     * M�todo para criar as vari�veis usadas na compara��o de registros antigos com registros j� existentes.
     * @param string psTable nome da tabela na qual a compara��o ser� baseada.
     */
    protected function buildCompare($psTable) {
      switch($psTable) {
        case "rm_category":
          $msFieldContext="fkContext";
          $msFieldName="sName";
          $msFieldParent="fkParent";
          $this->caCompareField["context"]="skCategory";
          $this->caCompareField["name"]="zName";
          $this->caCompareField["parent"]="skParent";
          $this->cbCompareRecursive=true;
        break;
        case "rm_section_best_practice":
          $msFieldContext="fkContext";
          $msFieldName="sName";
          $msFieldParent="fkParent";
          $this->caCompareField["context"]="skSectionTemplate";
          $this->caCompareField["name"]="zName";
          $this->caCompareField["parent"]="skParentTemplate";
          $this->cbCompareRecursive=true;
        break;
        case "rm_event":
          $msFieldContext="fkContext";
          $msFieldName="sDescription";
          $msFieldParent="fkCategory";
          $this->caCompareField["context"]="skThreat";
          $this->caCompareField["name"]="zName";
          $this->caCompareField["parent"]="skCategory";
          $this->csCompareExternal="category_id";
          $this->cbCompareRecursive=false;
        break;
        case "rm_best_practice":
          $msFieldContext="fkContext";
          $msFieldName="sName";
          $msFieldParent="fkSectionBestPractice";
          $this->caCompareField["context"]="skTemplate";
          $this->caCompareField["name"]="zName";
          $this->caCompareField["parent"]="skSectionTemplate";
          $this->csCompareExternal="section_best_practice_id";
          $this->cbCompareRecursive=false;
        break;
        default:
          return;
      }
      
      $moDataset = new FWDDBDataSet($this->coDstDatabase);
      $moDataset->addFWDDBField(new FWDDBField("$msFieldContext" ,"$msFieldContext",DB_UNKNOWN));
      $moDataset->addFWDDBField(new FWDDBField("$msFieldName"    ,"$msFieldName"   ,DB_UNKNOWN));
      $moDataset->addFWDDBField(new FWDDBField("$msFieldParent"  ,"$msFieldParent" ,DB_UNKNOWN));
      
      $moDataset->setQuery("SELECT $msFieldContext,$msFieldName,$msFieldParent FROM $psTable");
      $moDataset->execute();
      while($moDataset->fetch()) {
        $miContext=$moDataset->getFieldByAlias($msFieldContext)->getValue();
        $msName=$moDataset->getFieldByAlias($msFieldName)->getValue();
        $miParent=$moDataset->getFieldByAlias($msFieldParent)->getValue();
        $this->caCompareName[$miContext]=$msName;
        $this->caCompareParent[$miContext]=$miParent;
      }
    }
    
    /**
     * M�todo para realizar a migra��o de dados do bd antigo para o novo.
     * @return array Array mapeando ids antigos para novos, ou apenas ids antigos, em casos em que n�o h� cria��o de contexto.
     */
    public function migrate() {
      if ($this->cbIsMigratable) {
        $moSrcDataset = new MigDataSet($this->coSrcDatabase);
        $moSrcDataset->setQuery("SELECT ".($this->csSrcFields?
                                                $this->csSrcFields:
                                                "*"
                                           )." FROM ".(
                                                         $this->csSrcTableName?
                                                              $this->csSrcTableName:
                                                              $this->csDstTableName
                                                       ).(  $this->csSrcOrderBy?
                                                               " ORDER BY {$this->csSrcOrderBy}"
                                                                :""
                                                          )
                                 );
        $moSrcDataset->execute();
        
        $moDstDataset = new FWDDBDataSet($this->coDstDatabase,$this->csDstTableName);
        foreach($this->caDstFieldName as $miFieldIndex => $msFieldName) {
          $moDstDataset->addFWDDBField(new FWDDBField($msFieldName,$this->caDstFieldAlias[$miFieldIndex],$this->caDstFieldType[$miFieldIndex]));
        }
        while($moSrcDataset->fetch()) {
          $mbInsert=true;
          $mbEqual=false;
          if (count($this->caCompareName) > 0) { //prossegue caso tenha-se definido um campo para comparacao.
            $miSrcId = $moSrcDataset->getFieldByAlias($this->caCompareField["context"])->getValue();
            $msSrcName = trim($moSrcDataset->getFieldByAlias($this->caCompareField["name"])->getValue());
            $miSrcParent = $moSrcDataset->getFieldByAlias($this->caCompareField["parent"])->getValue();
            if ($this->csCompareExternal)
              $maSrcParent = $this->caExternalIds[$this->csCompareExternal];
            else
              $maSrcParent[$miSrcId]=$miSrcParent;
            //Acabamos de definir o pai de cada id
            
            foreach($this->caCompareName as $miDstId=>$msDstName) {
              if ($msSrcName==trim($msDstName)) {
                if ($this->cbCompareRecursive) { //se a comparacao for recursiva, verifica se os pais sao equivalentes em cada nivel
                  $miCheckSrcId = $miSrcId;
                  $miCheckSrcParent = $miSrcParent;
                  $miCheckDstId = $miDstId;
                  $miCheckDstParent = $this->caCompareParent[$miCheckDstId];
                  while(true) { //continua a pesquisar os pais enquanto existirem ou ateh que nao sejam equivalentes.
                    if (!$miCheckSrcParent && !$miCheckDstParent) {
                      $mbEqual = true;
                      break;
                    } else {
                      if ($miCheckSrcParent && isset($this->caResultNewIds[$miCheckSrcParent]) && $this->caResultNewIds[$miCheckSrcParent] == $miCheckDstParent) {
                        $miCheckSrcId = $miCheckSrcParent;
                        $miCheckSrcParent = isset($maSrcParent[$miCheckSrcId])?$maSrcParent[$miCheckSrcId]:0;
                        $miCheckDstId = $miCheckDstParent;
                        $miCheckDstParent = $this->caCompareParent[$miCheckDstId];
                      } else {
                        $mbEqual=false;
                        break;
                      }
                    }
                  }
                  if ($mbEqual) { //se os campos forem iguais, mapeia a id antiga para a id existente.
                    $this->caResultNewIds[$miSrcId]=$miDstId; 
                    break;
                  }
                } else { //se nao for recursiva, verifica-se apenas o pai do id em questao.
                  $miCheckDstParent = $this->caCompareParent[$miDstId];
                  if (isset($maSrcParent[$miSrcParent]) && $maSrcParent[$miSrcParent] == $miCheckDstParent) {
                    $mbEqual = true;
                  } 
                  if ($mbEqual) { //se os campos forem iguais, mapeia a id antiga para a id existente.
                    $this->caResultNewIds[$miSrcId]=$miDstId;
                    break;
                  }
                }
              } 
            }
          }
          if (!$mbEqual) { //se os campos nao tiverem sido definidos como iguais, prossegue para insercao.
            if ($this->cbInsertContext) { //insere contexto, nos casos necessarios.
              $miContextState = $this->ciStaticContextState?$this->ciStaticContextState:($this->cbContextStateAsField?(isset($this->caMapState[$moSrcDataset->getFieldByAlias($this->csContextStateField)->getValue()])?$this->caMapState[$moSrcDataset->getFieldByAlias($this->csContextStateField)->getValue()]:2702):2702);                  
              $msCreatorId = 0;
              $msCreatorName = 'Realiso Online';
              if ($this->cbHasContextCreator) {
              	$msCreatorId = $this->caContextCreatorMap[$moSrcDataset->getFieldByAlias($this->csContextCreatorIdField)->getValue()];
              	$msCreatorName = $moSrcDataset->getFieldByAlias($this->csContextCreatorNameField)->getValue();
              }
              $miContextId = $this->insertContext($this->ciContextType,$miContextState, $msCreatorId, $msCreatorName);
              $miSrcPkId = $moSrcDataset->getFieldByAlias($this->csSrcPkId)->getValue();
              $moDstDataset->getFieldByAlias("context_id")->setValue($miContextId);
            }
            foreach($this->caDstFieldAlias as $miFieldIndex=>$msFieldAlias) {
              if ($this->caDstFieldMap[$miFieldIndex]) {
                if ($msFieldAlias=="context_id" && $this->cbExternalContext) { //exclusivo wkf_task, pois o contexto vem de varias tabelas externas.
                  $miSrcPkId = $moSrcDataset->getFieldByAlias($this->csSrcPkId)->getValue();
                  $miContextType=$moSrcDataset->getFieldByAlias("nType")->getValue();
                  $this->cbInsertContext=true;
                  $miContextId = isset($this->caExternalIds[$miContextType][$moSrcDataset->getFieldByAlias($this->caDstFieldMap[$miFieldIndex])->getValue()])?$this->caExternalIds[$miContextType][$moSrcDataset->getFieldByAlias($this->caDstFieldMap[$miFieldIndex])->getValue()]:"NULL";
                  if (!$miContextId || $miContextId == "NULL") { 
                    $mbInsert=false; 
                  } else {
                    $moDstDataset->getFieldByAlias($msFieldAlias)->setValue($miContextId,true,false);
                  }
                }
                elseif (isset($this->caExternalIds[$msFieldAlias])) { //caso a id venha de uma tabela externa, utiliza o mapeamento.
                  $miFieldId = $moSrcDataset->getFieldByAlias($this->caDstFieldMap[$miFieldIndex])->getValue();
                  $miExternalId = isset($this->caExternalIds[$msFieldAlias][$miFieldId])?$this->caExternalIds[$msFieldAlias][$miFieldId]:"NULL";
                  if (!$this->cbInsertOnExternalNotFound && (!in_array($msFieldAlias,$this->caNotMandatoryExternal)) && (!$miExternalId || $miExternalId=="NULL")) {
                    $mbInsert=false;
                  }
                  $moDstDataset->getFieldByAlias($msFieldAlias)->setValue($miExternalId,true,false);
                } else {
                  $mmValue = $moSrcDataset->getFieldByAlias($this->caDstFieldMap[$miFieldIndex])->getValue();
                  $moDstDataset->getFieldByAlias($msFieldAlias)->setValue($mmValue,true,false);
                }
              } elseif(isset($this->caFieldStaticValue[$msFieldAlias])) {
                $moDstDataset->getFieldByAlias($msFieldAlias)->setValue($this->caFieldStaticValue[$msFieldAlias],true,false);
              }
            }
            if ($mbInsert) {
              $moDstDataset->insert();
              if ($this->cbInsertContext) {
                $this->caResultNewIds[$miSrcPkId]=$miContextId;
              } else {
                $this->caResultNewIds[] = "";
              }
            }
          }
        }
        return $this->caResultNewIds;
      } else return false;
    }
    
    /** 
     * M�todo para inserir um contexto.
     * @param integer piType Tipo do contexto (2800 a 2821).
     * @param integer piState Estado do contexto (2700 a 2705).
     * @return integer id do contexto criado.
     */
    protected function insertContext($piType,$piState,$piUserId=0,$piUserName='Realiso Online') {
      if (!$piType)
        return 0;
      $moContextDS = new FWDDBDataSet($this->coDstDatabase,"isms_context");
      $moContextDS->addFWDDBField(new FWDDBField("pkContext" ,"context_id" ,DB_NUMBER));
      $moContextDS->addFWDDBField(new FWDDBField("nType"     ,"type"       ,DB_NUMBER));
      $moContextDS->addFWDDBField(new FWDDBField("nState"    ,"state"      ,DB_NUMBER));
      $moContextDS->getFieldByAlias("type")->setValue($piType);
      $moContextDS->getFieldByAlias("state")->setValue($piState);
      $miContextId = $moContextDS->insert(true);
      
      $moContextDateDS = new FWDDBDataSet($this->coDstDatabase,"isms_context_date");
      $moContextDateDS->addFWDDBField(new FWDDBField("fkContext" ,"context_id" ,DB_NUMBER));
      $moContextDateDS->addFWDDBField(new FWDDBField("nAction"   ,"action"     ,DB_NUMBER));
      $moContextDateDS->addFWDDBField(new FWDDBField("dDate"     ,"ctx_date"       ,DB_DATETIME));
      $moContextDateDS->addFWDDBField(new FWDDBField("nUserId"   ,"user_id"    ,DB_NUMBER));
      $moContextDateDS->addFWDDBField(new FWDDBField("sUserName" ,"user_name"  ,DB_STRING));
      
      $moContextDateDS->getFieldByAlias("context_id")->setValue($miContextId);
      $moContextDateDS->getFieldByAlias("action")->setValue(2901);
      $moContextDateDS->getFieldByAlias("ctx_date")->setValue(time());
      $moContextDateDS->getFieldByAlias("user_id")->setValue($piUserId);
      $moContextDateDS->getFieldByAlias("user_name")->setValue($piUserName, true, false);
      $moContextDateDS->insert(true);
      $moContextDateDS->getFieldByAlias("context_id")->setValue($miContextId);
      $moContextDateDS->getFieldByAlias("action")->setValue(2902);
      $moContextDateDS->getFieldByAlias("ctx_date")->setValue(time());
      $moContextDateDS->getFieldByAlias("user_id")->setValue($piUserId);
      $moContextDateDS->getFieldByAlias("user_name")->setValue($piUserName, true, false);
      $moContextDateDS->insert(true);
      
      return $miContextId;
    }    

    /**
     * M�todo para migrar olog de auditoria.
     * <p>M�todo para migrar olog de auditoria.</p>
     */
    public function migrate_log() {     
      $maActions = array(
        /*ADT_ACTION_CREATE*/ 2601 => FWDLanguage::getPHPStringValue('ad_created', 'Criado(a)'),
        /*ADT_ACTION_EDIT*/ 2602 => FWDLanguage::getPHPStringValue('ad_edited', 'Editado(a)'),        
        /*ADT_ACTION_GARBAGE_REMOVE*/ 2604 => FWDLanguage::getPHPStringValue('ad_removed_from_bin', 'Removido(a) da lixeira')
      );
      
      $moSrcDataset = new FWDDBDataSet($this->coSrcDatabase);
      $moSrcDataset->addFWDDBField(new FWDDBField("dDate"       ,"log_date"        ,DB_DATETIME));
      $moSrcDataset->addFWDDBField(new FWDDBField("u.zName"     ,"user_name"   ,DB_STRING));
      $moSrcDataset->addFWDDBField(new FWDDBField("nActionType" ,"action_type" ,DB_NUMBER));
      $moSrcDataset->addFWDDBField(new FWDDBField("l.zName"     ,"target"      ,DB_STRING));
      $moSrcDataset->setQuery("SELECT dDate as log_date, u.zName as user_name, nActionType as action_type, l.zName as target FROM isms_audit_log l join isms_user u on(l.skuser=u.skuser)");
      $moSrcDataset->execute();
      
      $moDstDataset = new FWDDBDataSet($this->coDstDatabase,"isms_audit_log");
      $moDstDataset->addFWDDBField(new FWDDBField("dDate","dDate",DB_DATETIME));
      $moDstDataset->addFWDDBField(new FWDDBField("sUser","sUser",DB_STRING));
      $moDstDataset->addFWDDBField(new FWDDBField("nAction","nAction",DB_NUMBER));
      $moDstDataset->addFWDDBField(new FWDDBField("sTarget","sTarget",DB_STRING));
      $moDstDataset->addFWDDBField(new FWDDBField("tDescription","tDescription",DB_STRING));
      
      $i = 0;
      while ($moSrcDataset->fetch()) {
        $moDstDataset->getFieldByAlias('dDate')->setValue($moSrcDataset->getFieldByAlias('log_date')->getValue());
        $moDstDataset->getFieldByAlias('sUser')->setValue($moSrcDataset->getFieldByAlias('user_name')->getValue(), true, false);
        $moDstDataset->getFieldByAlias('sTarget')->setValue($moSrcDataset->getFieldByAlias('target')->getValue(), true, false);
        
        $miAction = 0;
        switch ($moSrcDataset->getFieldByAlias('action_type')->getValue()) {
          case 1: $miAction = 2601;break;
          case 2: $miAction = 2602;break;
          case 3: $miAction = 2604;break;
        }       
        $moDstDataset->getFieldByAlias('nAction')->setValue($miAction);       
        $moDstDataset->getFieldByAlias('tDescription')->setValue($maActions[$miAction] . ' ' . $moSrcDataset->getFieldByAlias('target')->getValue() . ' por ' . $moSrcDataset->getFieldByAlias('user_name')->getValue(), true, false);    
        
        $moDstDataset->insert();
        $i++;
      }     
      return $i;
    }
    
    /**
     * M�todo para migrar as configura��es.
     * <p>No bd antigo, as configura��es ficavam em apenas um registro.
     * No novo bd cada configura��o � um registro.</p>
     */
    public function migrateConfiguration() {
      $maUserIds = $this->caExternalIds["user_id"];
      $moConfigDS = new FWDDBDataSet($this->coSrcDatabase);

      $moConfigDS->addFWDDBField(new FWDDBField("" ,"skPolicy"               ,DB_NUMBER));
      $moConfigDS->addFWDDBField(new FWDDBField("" ,"skControlController"    ,DB_NUMBER));
      $moConfigDS->addFWDDBField(new FWDDBField("" ,"skAssetController"      ,DB_NUMBER));
      $moConfigDS->addFWDDBField(new FWDDBField("" ,"skChiefSecurityOfficer" ,DB_NUMBER));
      $moConfigDS->addFWDDBField(new FWDDBField("" ,"skChairman"             ,DB_NUMBER));
      $moConfigDS->addFWDDBField(new FWDDBField("" ,"nLowRisk"               ,DB_NUMBER));
      $moConfigDS->addFWDDBField(new FWDDBField("" ,"nHighRisk"              ,DB_NUMBER));
      $moConfigDS->addFWDDBField(new FWDDBField("" ,"nRiskState"             ,DB_NUMBER));
      
      $moConfigDS->setQuery("
SELECT skPolicy as skPolicy,
       skControlController as skControlController,
       skAssetController as skAssetController,
       skChiefSecurityOfficer as skChiefSecurityOfficer,
       skChairman as skChairman,
       nLowRisk as nLowRisk,
       nHighRisk as nHighRisk,
       nRiskState as nRiskState
  FROM isms_configuration
");
      $moConfigDS->execute();
      
      $moDstConfigDS = new FWDDBDataSet($this->coDstDatabase,"isms_config");
      $moDstConfigDS->addFWDDBField(new FWDDBField("pkConfig","config_id",DB_NUMBER));
      $moDstConfigDS->addFWDDBField(new FWDDBField("sValue","value",DB_STRING));
      $moDstConfigDS->select();
      $maDstConfigExists = array();
      while($moDstConfigDS->fetch()) {
        $maDstConfigExists[$moDstConfigDS->getFieldByAlias("config_id")->getValue()]=true;
      }
      $maUserMap["skControlController"]=804;
      $maUserMap["skAssetController"]=803;
      $maUserMap["skChairman"]=801;
      while($moConfigDS->fetch()) {
        foreach($maUserMap as $msFieldName=>$miFieldMap) {
          $moDstConfigDS->getFieldByAlias("config_id")->setValue($miFieldMap);
          $miUserId = isset($maUserIds[$moConfigDS->getFieldByAlias($msFieldName)->getValue()])?$maUserIds[$moConfigDS->getFieldByAlias($msFieldName)->getValue()]:"NULL";
          $moDstConfigDS->getFieldByAlias("value")->setValue($miUserId);
          if (isset($maDstConfigExists[$miFieldMap])) {
            $moDstConfigDS->getFieldByAlias("config_id")->clearFilters();
            $moFilter = new FWDDBFilter("=", $miFieldMap);
            $moDstConfigDS->getFieldByAlias("config_id")->addFilter($moFilter);
            $moDstConfigDS->update();
          } else {
            $moDstConfigDS->insert();
          }
          $this->caResultNewIds[] = "";
        }
      }
      
      $moDstConfigDS->getFieldByAlias("config_id")->setValue(7403);
      $moDstConfigDS->getFieldByAlias("value")->setValue(3);
      if (isset($maDstConfigExists[7403])) {
        $moDstConfigDS->getFieldByAlias("config_id")->clearFilters();
        $moFilter = new FWDDBFilter("=", '7403');
        $moDstConfigDS->getFieldByAlias("config_id")->addFilter($moFilter);
        $moDstConfigDS->update();
      } else {
        $moDstConfigDS->insert();
      }
      return $this->caResultNewIds;
    }
    
    /**
     * M�todo para migrar os nomes dos par�metros.
     * Nomes fixos: Confidencialidade, Integridade, Disponibilidade e Legalidade
     */
    public function migrateParameterName() {
      $moDstParameterNameDS = new FWDDBDataSet($this->coDstDatabase,"rm_parameter_name");
      $moDstParameterNameDS->addFWDDBField(new FWDDBField("pkParameterName" ,"parametername_id",DB_NUMBER));
      $moDstParameterNameDS->addFWDDBField(new FWDDBField("sName"           ,"name"            ,DB_STRING));
      $maParameterName = array("C"=>"Confidencialidade", "I"=>"Integridade", "D"=>"Disponibilidade", "L"=>"Legalidade");
      foreach($maParameterName as $msParameterNameLetter=>$msParameterName) {
        $moDstParameterNameDS->getFieldByAlias("name")->setValue($msParameterName);
        $miParameterNameId = $moDstParameterNameDS->insert(true);
        $this->caResultNewIds[$msParameterNameLetter] = $miParameterNameId;
      }
      return $this->caResultNewIds;
    }
    
    /**
     * M�todo para migrar os nomes dos valores dos par�metros.
     */
    public function migrateParameterValueName() {
      $moDstParameterValueNameDS = new FWDDBDataSet($this->coDstDatabase,"rm_parameter_value_name");
      $moDstParameterValueNameDS->addFWDDBField(new FWDDBField("pkValueName"      ,"valuename_id" ,DB_NUMBER));
      $moDstParameterValueNameDS->addFWDDBField(new FWDDBField("nValue"           ,"value"        ,DB_NUMBER));
      $moDstParameterValueNameDS->addFWDDBField(new FWDDBField("sImportance"      ,"importance"   ,DB_STRING));
      $moDstParameterValueNameDS->addFWDDBField(new FWDDBField("sImpact"          ,"impact"       ,DB_STRING));
      $moDstParameterValueNameDS->addFWDDBField(new FWDDBField("sRiskProbability" ,"probability"  ,DB_STRING));
      
      $moDstParameterValueNameDS->getFieldByAlias("value")->setValue(1);
      $moDstParameterValueNameDS->getFieldByAlias("importance")->setValue("Baixa",true,false);
      $moDstParameterValueNameDS->getFieldByAlias("impact")->setValue("Baixo",true,false);
      $moDstParameterValueNameDS->getFieldByAlias("probability")->setValue("Improv�vel",true,false);
      $miParameterValueNameId = $moDstParameterValueNameDS->insert(true);
      $this->caResultNewIds[1] = $miParameterValueNameId;
      
      $moDstParameterValueNameDS->getFieldByAlias("value")->setValue(2);
      $moDstParameterValueNameDS->getFieldByAlias("importance")->setValue("M�dia",true,false);
      $moDstParameterValueNameDS->getFieldByAlias("impact")->setValue("M�dio",true,false);
      $moDstParameterValueNameDS->getFieldByAlias("probability")->setValue("Poss�vel",true,false);
      $miParameterValueNameId = $moDstParameterValueNameDS->insert(true);
      $this->caResultNewIds[2] = $miParameterValueNameId;
      
      $moDstParameterValueNameDS->getFieldByAlias("value")->setValue(3);
      $moDstParameterValueNameDS->getFieldByAlias("importance")->setValue("Alta",true,false);
      $moDstParameterValueNameDS->getFieldByAlias("impact")->setValue("Alto",true,false);
      $moDstParameterValueNameDS->getFieldByAlias("probability")->setValue("Prov�vel",true,false);
      $miParameterValueNameId = $moDstParameterValueNameDS->insert(true);
      $this->caResultNewIds[3] = $miParameterValueNameId;
      
      return $this->caResultNewIds;
    }
    
    /**
     * M�todo para migrar os nomes dos valores dos par�metros de risco x controle.
     */
    public function migrateRCParameterValueName() {
      $moDstRCParameterValueNameDS = new FWDDBDataSet($this->coDstDatabase,"rm_rc_parameter_value_name");
      $moDstRCParameterValueNameDS->addFWDDBField(new FWDDBField("pkRCValueName"  ,"rcvaluename_id" ,DB_NUMBER));
      $moDstRCParameterValueNameDS->addFWDDBField(new FWDDBField("nValue"         ,"value"          ,DB_NUMBER));
      $moDstRCParameterValueNameDS->addFWDDBField(new FWDDBField("sRCProbability" ,"rc_probability" ,DB_STRING));
      $moDstRCParameterValueNameDS->addFWDDBField(new FWDDBField("sControlImpact" ,"control_impact" ,DB_STRING));
      
      $moDstRCParameterValueNameDS->getFieldByAlias("value")->setValue(0);
      $moDstRCParameterValueNameDS->getFieldByAlias("rc_probability")->setValue("0",true,false);
      $moDstRCParameterValueNameDS->getFieldByAlias("control_impact")->setValue("0",true,false);
      $miRCParameterValueNameId = $moDstRCParameterValueNameDS->insert(true);
      $this->caResultNewIds[0] = $miRCParameterValueNameId;
      
      $moDstRCParameterValueNameDS->getFieldByAlias("value")->setValue(1);
      $moDstRCParameterValueNameDS->getFieldByAlias("rc_probability")->setValue("1",true,false);
      $moDstRCParameterValueNameDS->getFieldByAlias("control_impact")->setValue("1",true,false);
      $miRCParameterValueNameId = $moDstRCParameterValueNameDS->insert(true);
      $this->caResultNewIds[1] = $miRCParameterValueNameId;
      
      $moDstRCParameterValueNameDS->getFieldByAlias("value")->setValue(2);
      $moDstRCParameterValueNameDS->getFieldByAlias("rc_probability")->setValue("2",true,false);
      $moDstRCParameterValueNameDS->getFieldByAlias("control_impact")->setValue("2",true,false);
      $miRCParameterValueNameId = $moDstRCParameterValueNameDS->insert(true);
      $this->caResultNewIds[2] = $miRCParameterValueNameId;
      
      return $this->caResultNewIds;
    }
    
    /**
     * M�todo para migrar os valores dos par�metros do ativo, risco e risco x controle
     */
    public function migrateParameterValueTable() {
      switch($this->csDstTableName) {
        case "rm_asset_value":
          $moSrcDataset = new FWDDBDataSet($this->coSrcDatabase);
          $moSrcDataset->setQuery("SELECT skAsset, nImportanceC, nImportanceI, nImportanceD, nImportanceL, zJustificationC, zJustificationI, zJustificationD, zJustificationL FROM rm_asset");

          $moSrcDataset->addFWDDBField(new FWDDBField("fkAsset"         ,"skAsset"        ,DB_NUMBER));
          $moSrcDataset->addFWDDBField(new FWDDBField("nImportanceC"    ,"nImportanceC"    ,DB_NUMBER));
          $moSrcDataset->addFWDDBField(new FWDDBField("nImportanceI"    ,"nImportanceI"    ,DB_NUMBER));
          $moSrcDataset->addFWDDBField(new FWDDBField("nImportanceD"    ,"nImportanceD"    ,DB_NUMBER));
          $moSrcDataset->addFWDDBField(new FWDDBField("nImportanceL"    ,"nImportanceL"    ,DB_NUMBER));
          $moSrcDataset->addFWDDBField(new FWDDBField("zJustificationC" ,"zJustificationC" ,DB_STRING));
          $moSrcDataset->addFWDDBField(new FWDDBField("zJustificationI" ,"zJustificationI" ,DB_STRING));
          $moSrcDataset->addFWDDBField(new FWDDBField("zJustificationD" ,"zJustificationD" ,DB_STRING));
          $moSrcDataset->addFWDDBField(new FWDDBField("zJustificationL" ,"zJustificationL" ,DB_STRING));
          
          
          $moSrcDataset->execute();
          
          $moDstDataset = new FWDDBDataSet($this->coDstDatabase,"rm_asset_value");
          $moDstDataset->addFWDDBField(new FWDDBField("fkAsset"         ,"asset_id"          ,DB_NUMBER));
          $moDstDataset->addFWDDBField(new FWDDBField("fkValueName"     ,"value_name_id"     ,DB_NUMBER));
          $moDstDataset->addFWDDBField(new FWDDBField("fkParameterName" ,"parameter_name_id" ,DB_NUMBER));
          $moDstDataset->addFWDDBField(new FWDDBField("tJustification"  ,"justification"     ,DB_STRING));
          
          while($moSrcDataset->fetch()) {
            $miAssetId = $moSrcDataset->getFieldByAlias("skAsset")->getValue();
            if (isset($this->caExternalIds["asset_id"][$miAssetId])) {
              foreach($this->caExternalIds["parameter_name"] as $msParameterNameLetter=>$miParameterNameId) {
                $miImportanceId = $moSrcDataset->getFieldByAlias("nImportance".$msParameterNameLetter)->getValue();
                $msJustification = $moSrcDataset->getFieldByAlias("zJustification".$msParameterNameLetter)->getValue();
                $moDstDataset->getFieldByAlias("asset_id")->setValue($this->caExternalIds["asset_id"][$miAssetId]);
                $moDstDataset->getFieldByAlias("value_name_id")->setValue($this->caExternalIds["parameter_value_name"][$miImportanceId]);
                $moDstDataset->getFieldByAlias("parameter_name_id")->setValue($miParameterNameId);
                $moDstDataset->getFieldByAlias("justification")->setValue($msJustification,true,false);
                $moDstDataset->insert();
                $this->caResultNewIds[] = "";
              }
            }
          }
          return $this->caResultNewIds;
        break;
        case "rm_risk_value":
          $moSrcDataset = new FWDDBDataSet($this->coSrcDatabase);
          $moSrcDataset->setQuery("SELECT skRisk, nProbability, zProbability1, zProbability2, zProbability3, nImpactC, nImpactI, nImpactD, nImpactL FROM rm_risk");
          $moSrcDataset->addFWDDBField(new FWDDBField("skRisk"        ,"skRisk"        ,DB_NUMBER));
          $moSrcDataset->addFWDDBField(new FWDDBField("nProbability"  ,"nProbability"  ,DB_NUMBER));
          $moSrcDataset->addFWDDBField(new FWDDBField("zProbability1" ,"zProbability1" ,DB_STRING));
          $moSrcDataset->addFWDDBField(new FWDDBField("zProbability2" ,"zProbability2" ,DB_STRING));
          $moSrcDataset->addFWDDBField(new FWDDBField("zProbability3" ,"zProbability3" ,DB_STRING));
          $moSrcDataset->addFWDDBField(new FWDDBField("nImpactC"      ,"nImpactC"      ,DB_NUMBER));
          $moSrcDataset->addFWDDBField(new FWDDBField("nImpactI"      ,"nImpactI"      ,DB_NUMBER));
          $moSrcDataset->addFWDDBField(new FWDDBField("nImpactD"      ,"nImpactD"      ,DB_NUMBER));
          $moSrcDataset->addFWDDBField(new FWDDBField("nImpactL"      ,"nImpactL"      ,DB_NUMBER));
          
          
          $moSrcDataset->execute();
          
          $moDstDataset = new FWDDBDataSet($this->coDstDatabase,"rm_risk_value");
          $moDstDataset->addFWDDBField(new FWDDBField("fkRisk"          ,"risk_id"           ,DB_NUMBER));
          $moDstDataset->addFWDDBField(new FWDDBField("fkValueName"     ,"value_name_id"     ,DB_NUMBER));
          $moDstDataset->addFWDDBField(new FWDDBField("fkParameterName" ,"parameter_name_id" ,DB_NUMBER));
          $moDstDataset->addFWDDBField(new FWDDBField("tJustification"  ,"justification"     ,DB_STRING));
          
          $moDstDataset2 = new FWDDBDataSet($this->coDstDatabase,"rm_risk");
          $moDstDataset2->addFWDDBField(new FWDDBField("fkContext","risk_id",DB_NUMBER));
          $moDstDataset2->addFWDDBField(new FWDDBField("fkProbabilityValueName","probability_value_name_id",DB_NUMBER));
          
          while($moSrcDataset->fetch()) {
            $miRiskId = $moSrcDataset->getFieldByAlias("skRisk")->getValue();
            if (isset($this->caExternalIds["risk_id"][$miRiskId])) {
              foreach($this->caExternalIds["parameter_name"] as $msParameterNameLetter=>$miParameterNameId) {
                $miImpactId = $moSrcDataset->getFieldByAlias("nImpact".$msParameterNameLetter)->getValue();
                $moDstDataset->getFieldByAlias("risk_id")->setValue($this->caExternalIds["risk_id"][$miRiskId]);
                $moDstDataset->getFieldByAlias("value_name_id")->setValue($this->caExternalIds["parameter_value_name"][$miImpactId]);
                $moDstDataset->getFieldByAlias("parameter_name_id")->setValue($miParameterNameId);
                $moDstDataset->insert();
                $this->caResultNewIds[] = "";
              }
              $miProbabilityId = $moSrcDataset->getFieldByAlias("nProbability")->getValue();
              $moDstDataset2->getFieldByAlias("probability_value_name_id")->setValue($this->caExternalIds["parameter_value_name"][$miProbabilityId]);
              $moFilter = new FWDDBFilter("=", $this->caExternalIds["risk_id"][$miRiskId]);
              $moDstDataset2->getFieldByAlias("risk_id")->clearFilters();
              $moDstDataset2->getFieldByAlias("risk_id")->addFilter($moFilter);
              $moDstDataset2->update();
            }
          }
          return $this->caResultNewIds;
        break;
        case "rm_risk_control_value":
          $moSrcDataset = new FWDDBDataSet($this->coSrcDatabase);
          $moSrcDataset->addFWDDBField(new FWDDBField("skRiskControl","skRiskControl" ,DB_NUMBER));
          $moSrcDataset->addFWDDBField(new FWDDBField("nProbability" ,"nProbability"  ,DB_NUMBER));
          $moSrcDataset->addFWDDBField(new FWDDBField("nImpactC"     ,"nImpactC"      ,DB_NUMBER));
          $moSrcDataset->addFWDDBField(new FWDDBField("nImpactI"     ,"nImpactI"      ,DB_NUMBER));
          $moSrcDataset->addFWDDBField(new FWDDBField("nImpactD"     ,"nImpactD"      ,DB_NUMBER));
          $moSrcDataset->addFWDDBField(new FWDDBField("nImpactL"     ,"nImpactL"      ,DB_NUMBER));
          $moSrcDataset->addFWDDBField(new FWDDBField("nImpactL"     ,"nImpactL"      ,DB_NUMBER));
          $moSrcDataset->setQuery("SELECT skRiskControl, nProbability, nImpactC, nImpactI, nImpactD, nImpactL FROM rm_risk_control");
          $moSrcDataset->execute();
          
          $moDstDataset = new FWDDBDataSet($this->coDstDatabase,"rm_risk_control_value");
          $moDstDataset->addFWDDBField(new FWDDBField("fkRiskControl"  ,"risk_control_id"  ,DB_NUMBER));
          $moDstDataset->addFWDDBField(new FWDDBField("fkRCValueName"  ,"rc_value_name_id" ,DB_NUMBER));
          $moDstDataset->addFWDDBField(new FWDDBField("fkParameterName","parameter_name_id",DB_NUMBER));
          $moDstDataset->addFWDDBField(new FWDDBField("tJustification" ,"justification"    ,DB_STRING));
          
          $moDstDataset2 = new FWDDBDataSet($this->coDstDatabase,"rm_risk_control");
          $moDstDataset2->addFWDDBField(new FWDDBField('fkContext'              ,'risk_control_id'           ,DB_NUMBER));
          $moDstDataset2->addFWDDBField(new FWDDBField('fkProbabilityValueName' ,'probability_value_name_id' ,DB_NUMBER));
          $moDstDataset2->addFWDDBField(new FWDDBField('fkRisk'                 ,'rc_risk_id'                ,DB_NUMBER));
          $moDstDataset2->addFWDDBField(new FWDDBField('fkControl'              ,'rc_control_id'             ,DB_NUMBER));
          $moDstDataset2->addFWDDBField(new FWDDBField('tJustification'         ,'rc_justification'          ,DB_STRING));
          
          while($moSrcDataset->fetch()) {
            $miRiskControlId = $moSrcDataset->getFieldByAlias("skRiskControl")->getValue();
            if (isset($this->caExternalIds["risk_control_id"][$miRiskControlId])) {
              foreach($this->caExternalIds["parameter_name"] as $msParameterNameLetter=>$miParameterNameId) {
                $miImpactId = $moSrcDataset->getFieldByAlias("nImpact".$msParameterNameLetter)->getValue();
                if (isset($this->caExternalIds["rc_parameter_value_name"][$miImpactId])) {
                  $moDstDataset->getFieldByAlias("risk_control_id")->setValue($this->caExternalIds["risk_control_id"][$miRiskControlId]);
                  $moDstDataset->getFieldByAlias("rc_value_name_id")->setValue($this->caExternalIds["rc_parameter_value_name"][$miImpactId]);
                  $moDstDataset->getFieldByAlias("parameter_name_id")->setValue($miParameterNameId);
                  $moDstDataset->insert();
                  $this->caResultNewIds[] = "";
                }
              }
              $miProbabilityId = $moSrcDataset->getFieldByAlias("nProbability")->getValue();
              $moDstDataset2->getFieldByAlias("probability_value_name_id")->setValue($this->caExternalIds["rc_parameter_value_name"][$miProbabilityId]);
              $moFilter = new FWDDBFilter("=", $this->caExternalIds["risk_control_id"][$miRiskControlId]);
              $moDstDataset2->getFieldByAlias("risk_control_id")->clearFilters();
              $moDstDataset2->getFieldByAlias("risk_control_id")->addFilter($moFilter);
              $moDstDataset2->update();
            }
          }
          return $this->caResultNewIds;
        break;
        default:
          return 0;
      }
    }
    
    /**
     * M�todo para deletar os dados que possam ter sido inseridos por essa classe.
     */
    public function delete() {
      $moDeleteDS = new FWDDBDataSet($this->coDstDatabase);
      $moDeleteDS->setQuery("DELETE FROM isms_scope");
      $moDeleteDS->execute();
      $moDeleteDS->setQuery("DELETE FROM isms_policy");
      $moDeleteDS->execute();
      $moDeleteDS->setQuery("DELETE FROM rm_asset_value");
      $moDeleteDS->execute();
      $moDeleteDS->setQuery("DELETE FROM rm_risk_value");
      $moDeleteDS->execute();
      $moDeleteDS->setQuery("DELETE FROM rm_risk_control_value");
      $moDeleteDS->execute();
      $moDeleteDS->setQuery("DELETE FROM rm_standard");
      $moDeleteDS->execute();
      $moDeleteDS->setQuery("DELETE FROM isms_audit_log");
      $moDeleteDS->execute();
      $moDeleteDS->setQuery("DELETE FROM wkf_alert");
      $moDeleteDS->execute();
      $moDeleteDS->setQuery("DELETE FROM wkf_task");
      $moDeleteDS->execute();
      $moDeleteDS->setQuery("DELETE FROM rm_risk_limits");
      $moDeleteDS->execute();
      $moDeleteDS->setQuery("DELETE FROM wkf_control_efficiency");
      $moDeleteDS->execute();
      $moDeleteDS->setQuery("DELETE FROM rm_control_efficiency_history");
      $moDeleteDS->execute();
      $moDeleteDS->setQuery("DELETE FROM rm_control_best_practice");
      $moDeleteDS->execute();
      $moDeleteDS->setQuery("DELETE FROM rm_best_practice");
      $moDeleteDS->execute();
      $moDeleteDS->setQuery("DELETE FROM rm_section_best_practice");
      $moDeleteDS->execute();
      $moDeleteDS->setQuery("DELETE FROM rm_risk_control");
      $moDeleteDS->execute();
      $moDeleteDS->setQuery("DELETE FROM rm_control");
      $moDeleteDS->execute();
      $moDeleteDS->setQuery("DELETE FROM isms_config");
      $moDeleteDS->execute();
      $moDeleteDS->setQuery("DELETE FROM rm_risk");
      $moDeleteDS->execute();
      $moDeleteDS->setQuery("DELETE FROM rm_process_asset");
      $moDeleteDS->execute();
      $moDeleteDS->setQuery("DELETE FROM rm_asset_asset");
      $moDeleteDS->execute();
      $moDeleteDS->setQuery("DELETE FROM rm_event");
      $moDeleteDS->execute();
      $moDeleteDS->setQuery("DELETE FROM rm_asset");
      $moDeleteDS->execute();
      $moDeleteDS->setQuery("DELETE FROM rm_category");
      $moDeleteDS->execute();
      $moDeleteDS->setQuery("DELETE FROM rm_process");
      $moDeleteDS->execute();
      $moDeleteDS->setQuery("DELETE FROM rm_area");
      $moDeleteDS->execute();
      $moDeleteDS->setQuery("DELETE FROM isms_user");
      $moDeleteDS->execute();
      $moDeleteDS->setQuery("DELETE FROM isms_profile");
      $moDeleteDS->execute();
      $moDeleteDS->setQuery("DELETE FROM isms_context");
      $moDeleteDS->execute();
      $moDeleteDS->setQuery("DELETE FROM isms_context_date");
      $moDeleteDS->execute();
      $moDeleteDS->setQuery("DELETE FROM rm_parameter_name");
      $moDeleteDS->execute();
      $moDeleteDS->setQuery("DELETE FROM rm_parameter_value_name");
      $moDeleteDS->execute();
      $moDeleteDS->setQuery("DELETE FROM rm_rc_parameter_value_name");
      $moDeleteDS->execute();
      return true;
    }
  }
?>