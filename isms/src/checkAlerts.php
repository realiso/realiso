<?php 
/**
* ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
*/

$baseDir = '/mnt/realiso/newisms/instances/';
$lib_ref = "lib/";
require_once($lib_ref . "adodb/adodb.inc.php");
require_once($lib_ref . "fpdf/fpdf.php");
require_once($lib_ref . 'fwd0.php');
require_once($lib_ref . 'fwd1.php');
require_once($lib_ref . 'fwd2.php');
require_once($lib_ref . 'fwd3.php');

/*
 * Classe para checar nos sistemas se existem alertas agendados e disparar os emails.
 */
class checkAlerts {
	
	public $moConfig = '';
	
	function __construct($path){
		$this->moConfig = new FWDConfig($path);
		if (!$this->moConfig->load()) {
			trigger_error('error_config_corrupt',E_USER_ERROR);
		}
		// preciso verificar se a intancia est� v�lida
		$moSaaSConfig = new ISMSSaas();
		if(!$moSaaSConfig->getConfig(SYSTEM_IS_ACTIVE)){
			// Cria tarefas agendadas e envia alertas de tarefas atrasadas.
			WKFSchedule::checkSchedules();
			//Envia os alertas referentes � implementa��o dos controles
			$moControlSchedule = new ControlSchedule($this->getUserId());
			$moControlSchedule->checkImplementation();
		}
	}
	
	static function scanDirs($baseDir = '.'){
		if (!is_dir($baseDir)){
			echo "path inv�lida -> $baseDir<BR>\n";
			return;
		}
		$dir = opendir($baseDir);
		$instances = scandir($dir);
		foreach($instances as $instance){
			if (is_dir($baseDir.$instance)){
				echo "checando $instance...\n";
				new checkAlerts($baseDir.$instance.'/config.php');			
			}
		}
		
	}

}

checkAlerts::scanDirs($baseDir.'saas');
checkAlerts::scanDirs($baseDir.'evaluation');

?>