<?php
include_once "include.php";

class LogoutEvent extends FWDRunnable {
	public function run() {  		  		
  		$moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
  		$moSession->logout();
		header("Location: login.php");
	}
}

FWDStartEvent::getInstance()->addBeforeAjax(new LogoutEvent(""));
FWDStartEvent::getInstance()->start();

?>