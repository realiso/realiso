<?php
include_once "include.php";

$soWebLib = FWDWebLib::getInstance();

$soWebLib->xml_load("about_isms.xml");

$soISMSLib = ISMSLib::getInstance();
FWDWebLib::getObject('version')->concatValue($soISMSLib->getISMSVersion());
FWDWebLib::getObject('build')->concatValue($soISMSLib->getISMSBuild());
$maDate = getdate();
FWDWebLib::getObject('copyright')->concatValue($maDate['year']);

$soWebLib->dump_html(FWDWebLib::getObject('dialog'));
?>