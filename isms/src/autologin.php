<?php
include_once "include.php";

function login($dataEncrypted){
	$private_key = 'WawLQKv8EFCqUSTmT2vtTwFdUVsrWrIDabA6mbfTTbJ2zSxfQ9';

        $dataEncoded = decrypt($dataEncrypted, $private_key);
	$data = json_decode($dataEncoded);

        if($data[2]+120 < time()){
                header("Location: /404.html");
                die();
        }

	$moCrypt = new FWDCrypt();
	$login = $moCrypt->getIV().$moCrypt->encrypt($data[0]);
	$pass = $moCrypt->getIV().$moCrypt->encrypt($data[1]);

	clean_cache();
	set_cookies($login, $pass);
}

function set_cookies($login, $pass){
	$ismssaas = new ISMSSaaS();
	$instance = $ismssaas->getConfig(ISMSSaaS::LICENSE_CLIENT_NAME);

	setCookie("ISMSLANGUAGE", '3302', time()+60*60*24*365);
	setCookie("ISMSLOGIN", $login);
	setCookie("ISMSPASSWORD", $pass);
	setCookie("SITELOGIN", true);

	header("Location: /$instance/sys/default.php");
}

function clean_cache(){
  if (isset($_SERVER['HTTP_COOKIE'])) {
    $cookies = explode(';', $_SERVER['HTTP_COOKIE']);
    foreach($cookies as $cookie) {
      $parts = explode('=', $cookie);
      $name = trim($parts[0]);
      setcookie($name, '', time()-1000);
      setcookie($name, '', time()-1000, '/');
    }
  }
}

function decrypt($string, $key){
	$result = '';
	$string = base64_decode($string);

	for($i=0; $i<strlen($string); $i++){
		$char = substr($string, $i, 1);
		$keychar = substr($key, ($i % strlen($key))-1, 1);
		$char = chr(ord($char)-ord($keychar));
		$result.=$char;
	}
	return $result;
}

$dataEncrypted = $_GET['data'];

login($dataEncrypted);

?>
