<?php
include_once "include.php";

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){ 
    //instala a seguran�a de ACL na p�gina
    FWDACLSecurity::getInstance()->installSecurity(FWDWebLib::getObject('dialog'),FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs());
		
    if (FWDWebLib::getObject("tab_summary_advanced")->getShouldDraw())
      FWDWebLib::getObject("tab_summary_basic")->setShouldDraw(false);
    
    $moConfig = new ISMSConfig();
    if(!$moConfig->getConfig(GENERAL_DATA_COLLECTION_ENABLED)){
      FWDWebLib::getObject('tab_statistics')->setShouldDraw(false);
    }

    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("tab_dashboard.xml");
?>