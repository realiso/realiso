<?php

include_once 'include.php';

class SaveEventGeralPreferences extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $miUserId = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getUserId(true);
    $msUserEmail = FWDWebLib::getObject('user_email')->getValue();
    $msUserLanguage = FWDWebLib::getObject('user_language')->getValue();

    /* POG para nao alterar o getValue da FWDView: concatenar um caractere no
     * inicio e outro no final da string em javascript e remov�-los em php */
    $msCurrentPasswd = FWDWebLib::getObject("pog_current_passwd")->getValue();
    $msCurrentPasswd = substr($msCurrentPasswd,1,strlen($msCurrentPasswd)-2);
    $msNewPasswd = FWDWebLib::getObject("pog_new_passwd")->getValue();
    $msNewPasswd = substr($msNewPasswd,1,strlen($msNewPasswd)-2);
    $msNewPasswdConfirm = FWDWebLib::getObject("pog_new_passwd_confirm")->getValue();
    $msNewPasswdConfirm = substr($msNewPasswdConfirm,1,strlen($msNewPasswdConfirm)-2);
    
    $moUser = new ISMSUser();
    $moUser->fetchById($miUserId);
    $msCurrentEmail = $moUser->getFieldValue('user_email');
    if ($msCurrentEmail != $msUserEmail) {
      $moUser = new ISMSUser();
      $moUser->createFilter($msUserEmail,'user_email');
      $moUser->select();
      if ($moUser->fetch()) {
        echo "hide_warnings();";
        echo "js_show('warning_existing_email_error');";
        return;
      }
    }
    
    $moUser = new ISMSUser();
    $moUser->setFieldValue('user_email',$msUserEmail);
    $moUser->setFieldValue('user_language',$msUserLanguage);
    $mbInsertPassword = false;
    if ($msNewPasswd || $msNewPasswdConfirm || FWDWebLib::getObject("current_passwd")->getAttrDisabled()) {
      if ($msNewPasswd===$msNewPasswdConfirm) {
        $moPassPolicy = new ISMSPasswordPolicy();
        if ($moPassPolicy->checkPassword($msNewPasswd)) {
          $moUserPasswordHistory = new ISMSUserPasswordHistory();
          if ($moUserPasswordHistory->checkPassword(md5($msNewPasswd))) {
            // verificar senha atual
            $moUser2 = new ISMSUser();
            $moUser2->fetchById($miUserId);
            if (trim($moUser2->getFieldValue('user_password'))==md5($msCurrentPasswd)) {
              $moUser->setFieldValue('user_password',md5($msNewPasswd));
              $mbInsertPassword = true;
            }
            else {
              echo "hide_warnings();";
              echo "js_show('warning_current_password_error');";
              return;
            }
          } else {
            echo "hide_warnings();";
            echo "js_show('warning_existing_password_error');";
            return;
          }
        }
        else {
          echo "hide_warnings();";
          echo "js_show('warning_password_policy_error');";
          return;
        }
      }else{
        echo "hide_warnings();";
        echo "js_show('warning_different_passwords_error');";
        return;
      }
    }


    if (ISMSLib::getConfigById(GENERAL_SOLICITOR_ENABLED)) {
      $miDateStart = FWDWebLib::getObject('solicitor_start_date')->getTimestamp();
      $miDateFinish = FWDWebLib::getObject('solicitor_finish_date')->getTimestamp();
      $miSolicitorId = FWDWebLib::getObject('solicitor_id')->getValue();
      $miDateToday = mktime (0, 0, 0, date("m",ISMSLib::ISMSTime()), date("d",ISMSLib::ISMSTime()),  date("Y",ISMSLib::ISMSTime()));
      $miDateMinFinish = mktime (0, 0, 0, date("m",ISMSLib::ISMSTime()), date("d",ISMSLib::ISMSTime()+86400),  date("Y",ISMSLib::ISMSTime()));

      $mbCreateSolicitor = false;
      if ($miSolicitorId) {
        if (!($miDateStart && $miDateFinish)) {
          echo "hide_warnings();";
          echo "js_show('warning_missing_dates');";
          return;
        }
        else if (($miDateStart && $miDateFinish) && $miDateStart > $miDateFinish) {
          echo "hide_warnings();";
          echo "js_show('warning_wrong_dates');";
          return;
        }elseif($miDateStart < $miDateToday){
          echo "hide_warnings();";
          echo "js_show('warning_begin_date_too_early');";
          return;
        }else if ($miDateFinish < ISMSLib::ISMSTime()) {
          echo "hide_warnings();";
          echo "js_show('warning_past_date');";
          return;
        }
        $mbCreateSolicitor = true;
      }
      if (($miDateStart || $miDateFinish) && !$miSolicitorId) {
        echo "hide_warnings();";
        echo "js_show('warning_missing_solicitor');";
        return;
      }

    if ($mbCreateSolicitor) {
        $moOldSolicitor = new ISMSSolicitor();
        $moOldSolicitor->createFilter($miUserId, 'user_id');
        $moOldSolicitor->delete();

        $moNewSolicitor = new ISMSSolicitor();
        $moNewSolicitor->setFieldValue('user_id', $miUserId);
        $moNewSolicitor->setFieldValue('solicitor_id', $miSolicitorId);
        $moNewSolicitor->setFieldValue('date_begin', $miDateStart);
        $moNewSolicitor->setFieldValue('date_finish', $miDateFinish);
        $moNewSolicitor->insert();
      }
    }
    if ($mbInsertPassword) {
      setcookie('ISMSPASSWORD',false);
      $moUserPasswordHistory = new ISMSUserPasswordHistory();
      $moUserPasswordHistory->addPassword(md5($msNewPasswd));
    }
    $moUser->update($miUserId);

    /*
     * Verifica se o usu�rio alterou a linguagem do sistema.
     * Caso tenha alterado, deve-se fazer um refresh no sistema
     * para atualizar as strings.
     */
    if (FWDWebLib::getObject('current_language')->getValue() != $msUserLanguage) {
       setcookie('ISMSLANGUAGE',$msUserLanguage,time()+60*60*24*365); //1-year cookie
       echo "soWindow = soPopUpManager.getPopUpById('popup_user_preferences').getOpener();"
                      ."if(soWindow.refresh) soWindow.refresh();";
    }

    echo "soPopUpManager.closePopUp('popup_user_preferences');";
  }
}

class SaveEventEmailPreferences extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $moEmailPreferences = new ISMSEmailPreferences();
    $msDigest = substr($moWebLib->getObject('digest')->getValue(),1);
    $msFormat = substr($moWebLib->getObject('format_types')->getValue(),1);
    $maMessageTypes = array_filter(explode(':',$moWebLib->getObject('message_types')->getValue()));
    $moEmailPreferences->setDigestType($msDigest);
    $moEmailPreferences->setEmailFormat($msFormat);
    $moEmailPreferences->setMessageTypes($maMessageTypes);
    echo "soPopUpManager.closePopUp('popup_user_preferences');";
  }
}

class SearchUserEvent extends FWDRunnable {
  public function run(){
    $moSolicitor = new ISMSSolicitor();
    $moSolicitor->createFilter(ISMSLib::ISMSTime(), 'date_begin', '<=');
    $moSolicitor->createFilter(ISMSLib::ISMSTime(), 'date_finish', '>=');
    $moSolicitor->select();

    $maExcludeUsers = array();
    $maExcludeUsers[] = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId(true);
    while ($moSolicitor->fetch()) {
      $maExcludeUsers[] = $moSolicitor->getFieldValue('user_id');
    }
    $msRefUrl = FWDWebLib::getObject('var_admin_page')->getValue()==1?"../../":"";
    echo "isms_open_popup('popup_user_search','{$msRefUrl}popup_user_search.php?exclude_users=" . serialize($maExcludeUsers) . "','','true',402,600);";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
    $moStartEvent->addAjaxEvent(new SaveEventGeralPreferences('save_event_geral_preferences'));
    $moStartEvent->addAjaxEvent(new SaveEventEmailPreferences('save_event_email_preferences'));
    if (ISMSLib::getConfigById(GENERAL_SOLICITOR_ENABLED)) {
      $moStartEvent->addAjaxEvent(new SearchUserEvent('search_user_event'));
    }

    $moWebLib = FWDWebLib::getInstance();
    $moDialog = FWDWebLib::getObject("dialog");

    $moWebLib->xml_load($moWebLib->getSysRef().'popup_user_profile.xml',false);
    $moDialog->addObjFWDView(FWDWebLib::getObject("vg_user_profile"));

    // S� carrega a tela de prefer�ncias de email se os emails estiverem habilitados.
    if (ISMSLib::getConfigById(GENERAL_EMAIL_ENABLED)) {
      $moWebLib->xml_load($moWebLib->getSysRef().'popup_email_preferences.xml',false);
      $moDialog->addObjFWDView(FWDWebLib::getObject("vg_email_preferences"));
    }
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $moWebLib = FWDWebLib::getInstance();
    $miUserId = $moWebLib->getSessionById(ISMSLib::getCurrentSessionId())->getUserId(true);
    $moUser = new ISMSUser();
    $moUser->fetchById($miUserId);
    $moSel = $moWebLib->getObject('sel_preference_div_show')->getValue();

    /*
     * Guarda a l�ngua atual do usu�rio para na hora de salvar verificar
     * se deve dar um submit na pagina para atualizar as strings caso
     * a l�ngua tenha sido alterada.
     */
    FWDWebLib::getObject('current_language')->setValue($moUser->getFieldByAlias('user_language')->getValue());

    /*
     * Adiciona a op��o 'Geral'
     */
    $moOptSelect = FWDWebLib::getObject('sel_preference_div_show');
    $moOptSelect->setItemValue('1', FWDLanguage::getPHPStringValue('si_general', "Geral"));

    /*
     * Popula o select com as linguagens dispon�veis para o sistema
     */
    $moLanguageSelect = FWDWebLib::getObject('user_language');
    $maAvailableLanguages = FWDLanguage::getLanguages();
    foreach ($maAvailableLanguages as $miKey => $msValue) {
      $moLanguageSelect->setItemValue($miKey, $msValue);
    }

    //parte de preferencias do usu�rio
    $moWebLib->getObject('user_name')->setValue($moUser->getFieldByAlias('user_name')->getValue());
    $moWebLib->getObject('user_login')->setValue($moUser->getFieldByAlias('user_login')->getValue());
    $moWebLib->getObject('user_email')->setValue($moUser->getFieldByAlias('user_email')->getValue());
    $moWebLib->getObject('user_language')->setValue($moUser->getFieldByAlias('user_language')->getValue());

    $msHasSolicitor = '';
    if (ISMSLib::getConfigById(GENERAL_SOLICITOR_ENABLED)) {
      $msHasSolicitor .='var sbHasSolicitorModule = 1;';
      $miSolicitorId = 0;
      $moSolicitor = new ISMSSolicitor();
      $moSolicitor->createFilter($miUserId, 'user_id');
      $moSolicitor->select();
      if ($moSolicitor->fetch()) $miSolicitorId = $moSolicitor->getFieldValue('solicitor_id');
      if ($miSolicitorId) {
        $moUserSolicitor = new ISMSUser();
        $moUserSolicitor->fetchById($miSolicitorId);
        $moWebLib->getObject('user_solicitor')->setValue($moUserSolicitor->getFieldByAlias('user_name')->getValue());
        $moWebLib->getObject('solicitor_id')->setValue($miSolicitorId);
        $moWebLib->getObject('solicitor_start_date')->setValue(ISMSLib::getISMSShortDate($moSolicitor->getFieldValue('date_begin')));
        $moWebLib->getObject('solicitor_finish_date')->setValue(ISMSLib::getISMSShortDate($moSolicitor->getFieldValue('date_finish')));
      }
    }else{
      $msHasSolicitor .='var sbHasSolicitorModule = 0;';
      $moWebLib->getObject('solicitor_sub_title')->setShouldDraw(false);
      $moWebLib->getObject('st_solicitor_name')->setShouldDraw(false);
      $moWebLib->getObject('user_solicitor')->setShouldDraw(false);
      $moWebLib->getObject('vb_solicitor_search')->setShouldDraw(false);
      $moWebLib->getObject('st_solicitor_start_date')->setShouldDraw(false);
      $moWebLib->getObject('warning_wrong_dates')->setShouldDraw(false);
      $moWebLib->getObject('vg_solicitor_start_date')->setShouldDraw(false);
      $moWebLib->getObject('warning_past_date')->setShouldDraw(false);
      $moWebLib->getObject('warning_missing_dates')->setShouldDraw(false);
      $moWebLib->getObject('warning_missing_solicitor')->setShouldDraw(false);
      $moWebLib->getObject('st_solicitor_finish_date')->setShouldDraw(false);
      $moWebLib->getObject('vg_solicitor_finish_date')->setShouldDraw(false);
    }

    /*
     * Verifica se os emails est�o habilitados.
     * Verifica se a op��o de digest das mensagens est� habilitada.
     */
    if (ISMSLib::getConfigById(GENERAL_EMAIL_ENABLED)) {
      if(!FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(POLICY_MODE)){
        FWDWebLib::getObject('st_documents_email')->setShouldDraw(false);
        FWDWebLib::getObject('message_types_document')->setShouldDraw(false);
      }
      $moEmailPreferences = new ISMSEmailPreferences();
      $moWebLib->getObject('digest')->setValue($moEmailPreferences->getDigestType());
      $moWebLib->getObject('format_types')->setValue($moEmailPreferences->getEmailFormat());
      $moWebLib->getObject('message_types')->setValue($moEmailPreferences->getMessageTypes());
      $moOptSelect->setItemValue('2', FWDLanguage::getPHPStringValue('si_email', "Email"));
      if (!ISMSLib::getConfigById(GENERAL_ALLOW_DIGESTS)) {
        FWDWebLib::getObject('viewgroup_digest')->setAttrDisplay("false");
      }
    }

    $moConfig = new ISMSConfig();
    if($moConfig->getConfigFromDB(AD_SERVER)){
      FWDWebLib::getObject('label_email')->setAttrDisplay("false");
      FWDWebLib::getObject('user_email')->setAttrDisplay("false");
      FWDWebLib::getObject('lb_change_password_title')->setAttrDisplay("false");
      FWDWebLib::getObject('lb_change_password')->setAttrDisplay("false");
      FWDWebLib::getObject('chk_change_password')->setAttrDisplay("false");
      FWDWebLib::getObject('label_current_password')->setAttrDisplay("false");
      FWDWebLib::getObject('current_passwd')->setAttrDisplay("false");
      FWDWebLib::getObject('label_new_password')->setAttrDisplay("false");
      FWDWebLib::getObject('new_passwd')->setAttrDisplay("false");
      FWDWebLib::getObject('lb_password_helper')->setAttrDisplay("false");
      FWDWebLib::getObject('label_new_password_confirm')->setAttrDisplay("false");
      FWDWebLib::getObject('new_passwd_confirm')->setAttrDisplay("false");      
    }

    FWDWebLib::getObject('var_admin_page')->setValue(FWDWebLib::getObject('adminPage')->getValue());
    $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    $msLastLogin = $moSession->getLastLogin();
    $msLastIP = $moSession->getLastIP();

    // Preenche o ToolTip da pol�tica de senhas
    $moPassPolicy = new ISMSPasswordPolicy();
    FWDWebLib::getObject('tooltip_helper_password_policy')->setValue($moPassPolicy->getPolicyDescriptionAsString());

    $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
    //retira as informa��es de prefer�ncia do dashboard se o usu�rio n�o tiver permiss�o de acessar o dashboard
    if((in_array('M.D.1',$maACLs))&&(in_array('M.D.2',$maACLs))){
      FWDWebLib::getObject('vb_edit_dashboard_pref')->setShouldDraw(false);
      FWDWebLib::getObject('st_dashboard_preferences')->setShouldDraw(false);
      FWDWebLib::getObject('st_help_dashboar_preferences')->setShouldDraw(false);
    }

    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        <?echo $msHasSolicitor?>
        gebi('user_email').focus();

        var soCurrentCfg = gebi('var_current_config')

        function set_user(id, name) {
          gebi('solicitor_id').value = id;
          gobi('user_solicitor').setValue(name);
        }

        function hide_current_cfg() {
          if (soCurrentCfg.value)
              js_hide(soCurrentCfg.value);
        }

        function hide_warnings() {
          js_hide('warning_current_password_error');
          js_hide('warning_different_passwords_error');
          js_hide('warning_password_policy_error');
          js_hide('warning_existing_password_error');
          js_hide('warning_existing_email_error');
          if(sbHasSolicitorModule){
            js_hide('warning_wrong_dates');
            js_hide('warning_missing_dates');
            js_hide('warning_missing_solicitor');
            js_hide('warning_past_date');
            js_hide('warning_begin_date_too_early');
          }
        }

        function change_config () {
          hide_current_cfg();
          soCurrentCfg.value = gebi('sel_preference_div_show').value;
          switch (gebi('sel_preference_div_show').value) {
            case '1' :
                if (gebi('vg_user_profile'))
                js_show('vg_user_profile');
                soCurrentCfg.value = 'vg_user_profile';
              break;
            case '2' :
                if (gebi('vg_email_preferences'))
                js_show('vg_email_preferences');
                soCurrentCfg.value = 'vg_email_preferences';
              break;
            default:break;
          }
        }

        function isEMailValid(){
          moElem = gebi('user_email');

          with (moElem) {
          	apos=value.indexOf("@");
          	dotpos=value.lastIndexOf(".");

          	if(apos<1||dotpos-apos<2)
              return false;
			else 
              return true;
          }
        }
        gobi('pack_change_password').disable();
      </script>
    <?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_user_preferences.xml');
?>