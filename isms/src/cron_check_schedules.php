<?php
set_time_limit(6000);
include_once "include.php";
include_once $handlers_ref . "QueryCurrentActiveUsers.php";
echo "verificando";
//Cria tarefas agendadas e envia alertas de tarefas atrasadas.
WKFSchedule::checkSchedules();

$moQueryUser = new QueryCurrentActiveUsers(FWDWebLib::getConnection());
$moQueryUser->makeQuery();

if($moQueryUser->executeQuery()){
	$moDataset = $moQueryUser->getDataset();
	$userCount = 0;
	while($moDataset->fetch()){

	  	$userLogin = $moDataset->getFieldByAlias('user_login')->getValue();
	  	$moUser = new ISMSUser();
	  	$moUser->fetchByLogin($userLogin);
	  	
	  	// Muda a lingua dos alertas
	  	$userLanguage = $moUser->getFieldValue('user_language');
		if($userLanguage){
			FWDLanguage::selectLanguage($moUser->getFieldValue('user_language'));
		}
		
		// Envia os alertas referentes � implementa��o dos controles
		//++$userCount;
		//print $userCount . ". UserId: " . $moDataset->getFieldByAlias('user_id')->getValue() . "<br>";
		$moControlSchedule = new ControlSchedule($moDataset->getFieldByAlias('user_id')->getValue());
		$moControlSchedule->checkImplementation(); //usa o userId

		// For�a um update do isActive dos controles com implementa��o ou tarefas pendentes
		//$moControlSchedule->updateControlsIsActive(); // n�o utiliza o userId;

		if(ISMSLib::hasModule(INCIDENT_MODE)){
			// Cria as n�o-conformidades que forem necess�rias
			$moControlSchedule->checkNonConformities();
		}

		// Envia os alertas de deadline dos documentos
		if(ISMSLib::hasModule(POLICY_MODE)){
			$moDocumentSchedule = new DocumentSchedule($moDataset->getFieldByAlias('user_id')->getValue());
			$moDocumentSchedule->checkDeadline();
		}
		// Executa o schedule da Gest�o de Incidente
		if(ISMSLib::hasModule(INCIDENT_MODE)){
			$moImprovementSchedule = new ImprovementSchedule($moDataset->getFieldByAlias('user_id')->getValue());
			$moImprovementSchedule->checkSchedule();
		}
	}
	
	$miChairmanId = ISMSLib::getConfigById(USER_CHAIRMAN);	
	$moControlSchedule = new ControlSchedule($miChairmanId);
	// For�a um update do isActive dos controles com implementa��o ou tarefas pendentes
	$moControlSchedule->updateControlsIsActive();	
	
}
// Executa o schedule de risco (c�lculo autom�tico de probabilidade)
if(ISMSLib::hasModule(INCIDENT_MODE)){
	$moRiskSchedule = new CIRiskSchedule();
	$moRiskSchedule->checkSchedule();
}
echo "foi";
?>