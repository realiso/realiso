<?php
include_once "include.php";

class UserAnsweredEvent extends FWDRunnable {
  public function run() {
    $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    $miUserId = $moSession->getUserId(true);
    $moUser = new ISMSUser();
    $moUser->setFieldValue('user_answered_survey',true);
    $moUser->update($miUserId);
    echo "self.close()";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new UserAnsweredEvent('user_answered_event'));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $soLicense = new ISMSLicense();
    $msLinkConcat = str_replace(' ','_',$soLicense->getClientName()) . "_" . ISMSLib::ISMSTime();
    $msUrl = FWDLanguage::getPHPStringValue('st_survey_link','http://www.surveymonkey.com/s.aspx?sm=Iv5Lu2xahyHtsNrd2ShHBA_3d_3d').'?'.$msLinkConcat;
    FWDWebLib::getObject('link_survey')->setValue($msUrl);
    
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(""));
FWDWebLib::getInstance()->xml_load("popup_survey.xml");
?>