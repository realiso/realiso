<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryProcessesFromArea.
 *
 * <p>Consulta para buscar os processos de uma �rea (e de suas sub-�reas,
 * se existirem).</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryProcessesFromArea extends FWDDBQueryHandler {
  
  protected $ciAreaId = 0;

  protected $caProcesses = array();
  
  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField('pkProcess','process_id',DB_NUMBER));
  }
  
  public function setArea($piAreaId) {
    $this->ciAreaId = $piAreaId;
  }
  //n�o filtra os deletados
  public function makeQuery() {
    $this->csSQL = "SELECT pkProcess as process_id from ".FWDWebLib::getFunctionCall("get_processes_from_area({$this->ciAreaId})")."
											JOIN isms_context c ON(c.pkContext = pkProcess and c.nState <> " . CONTEXT_STATE_DELETED . ")";
  }
  
  public function executeQuery() {
    parent::executeQuery();
    while ($this->coDataSet->fetch()) {
      $this->caProcesses[] = $this->coDataSet->getFieldByAlias("process_id")->getValue();
    }
  }
  
  /**
   * Retorna os processos de uma �rea (e de suas sub-�reas, se existirem).
   * 
   * <p>M�todo para retornar os processos de uma �rea (e de suas sub-�reas,
   * se existirem).</p>
   * @access public 
   * @return array Array de ids de processos
   */ 
  public function getProcesses() {
    return $this->caProcesses;
  }
}
?>