<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryAssetDependents.
 *
 * <p>Consulta que retorna uma lista com os dependentes de um ativo.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryAssetDependents extends FWDDBQueryHandler {

  protected $ciAssetId = 0;
  protected $cbRecursive = false;
  protected $caAssetDependents = array();

  public function __construct($poDB){
    parent::__construct($poDB);

    $this->coDataSet->addFWDDBField(new FWDDBField('aa.fkDependent','asset_id'   ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('a.sName'       ,'asset_name' ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('a.nValue'      ,'asset_value',DB_NUMBER));
    
    $this->coDataSet->setOrderBy("asset_name", "+");
  }

  public function makeQuery(){
    if($this->ciAssetId != 0){
      if($this->cbRecursive){
        $this->csSQL = "SELECT DISTINCT
                          a.fkContext as asset_id,
                          a.sName as asset_name,
                          a.nValue as asset_value
                        FROM
                        ".FWDWebLib::getFunctionCall("get_asset_dependents(".$this->ciAssetId.")")." ad
                        JOIN view_rm_asset_active a ON (a.fkContext = ad.pkAsset)";
      }else{
        $this->csSQL = "SELECT aa.fkDependent as asset_id, a.sName as asset_name, a.nValue as asset_value
                        FROM view_rm_asset_active a
                        JOIN view_rm_asset_asset_active aa ON (a.fkContext = aa.fkDependent)
                        WHERE aa.fkAsset = {$this->ciAssetId}";
      }
    }
  }

  public function executeQuery(){
    parent::executeQuery();
    $this->caAssetDependents = array();
    while($this->coDataSet->fetch()){
      $this->caAssetDependents[] = array(
        'asset_id' => $this->coDataSet->getFieldByAlias("asset_id")->getValue(),
        'asset_name' => $this->coDataSet->getFieldByAlias("asset_name")->getValue(),
        'asset_value' => $this->coDataSet->getFieldByAlias("asset_value")->getValue()
      );
    }
  }

  public function getAssetDependents(){
    return $this->caAssetDependents;
  }

  public function setAsset($piAssetId){
    $this->ciAssetId = $piAssetId;
  }

  public function setRecursive($pbRecursive){
    $this->cbRecursive = $pbRecursive;
  }

}

?>