<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryContextCreateModify.
 *
 * <p>Consulta para buscar os nomes dos usu�rios que criaram/editaram contextos.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryContextCreateModify extends FWDDBQueryHandler {

	private $ciContextId = '';

  public function __construct($poDB){
    parent::__construct($poDB);
  	$this->coDataSet->addFWDDBField(new FWDDBField("uc.sName","user_create_name", DB_STRING));
	  $this->coDataSet->addFWDDBField(new FWDDBField("cc.dDate","date_create", DB_DATETIME));
	  $this->coDataSet->addFWDDBField(new FWDDBField("ud.sName","user_edit_name", DB_STRING));
	  $this->coDataSet->addFWDDBField(new FWDDBField("ce.dDate","date_edit", DB_DATETIME));
  }
  
  public function makeQuery(){
    $msWhere = ($this->ciContextId!='')?" where ch.context_id = {$this->ciContextId} ":'';
    
    $this->csSQL = "SELECT ch.context_creator_name as user_create_name, ch.context_date_created as date_create, ch.context_modifier_name as user_edit_name, ch.context_date_modified as date_edit
		    		FROM context_history ch $msWhere";
  }

	public function setContextId($piContextId){
		$this->ciContextId = $piContextId;
	}
}
?>