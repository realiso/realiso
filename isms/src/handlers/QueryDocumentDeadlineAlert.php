<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryDocumentDeadlineAlert
 *
 * <p>Consulta para pesquisar os documentos que tem deadline pr�xima e 
 * que ainda n�o tiveram alertas enviados.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryDocumentDeadlineAlert extends FWDDBQueryHandler {
	
	private $ciUserId;

  public function __construct($poDB,$piUserId){
    parent::__construct($poDB);
    $this->ciUserId = $piUserId;
    
    $this->coDataSet->addFWDDBField(new FWDDBField('d.fkContext'  ,'id'         ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('d.dDeadline'  ,'deadline'   ,DB_DATETIME));
    $this->coDataSet->addFWDDBField(new FWDDBField('d.nDaysBefore','days_before',DB_NUMBER));
  }

  public function makeQuery(){
    $this->csSQL ="
                  SELECT d.fkContext as id, d.dDeadline as deadline, d.nDaysBefore as days_before
                  FROM view_pm_document_active d
                  WHERE
                    d.fkAuthor=". $this->ciUserId ." AND
                    d.dDeadline IS NOT NULL AND
                    (d.bFlagDeadlineAlert=0 OR d.bFlagDeadlineAlert IS NULL)
";
  }
}
?>