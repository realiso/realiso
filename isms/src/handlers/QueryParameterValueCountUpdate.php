<?php

/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryParameterValueCountUpdate.
 *
 * <p>Update que atualiza o n�mero de valores de par�metros de risco</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryParameterValueCountUpdate extends FWDDBQueryHandler {

  protected $ciNewValueCount = 0;
  protected $cbOptimist = true;

  /**
   * Construtor da classe, armazena o nome do usu�rio que acessa o banco.
   * 
   * <p>Construtor da classe, armazena o nome do usu�rio que acessa o banco.</p>
   * @access public
   * @param FWDDB $poDB objeto de acesso a base de dados 
   */   
  public function __construct($poDB) {
    parent::__construct($poDB);

  }

  /**
  * Define o n�mero de par�metros ap�s a atualiza��o. 
  * 
  * <p>Define o n�mero de par�metros que existir�o ap�s a atualiza��o.</p>
  * @access public
  * @param integer $piNewValueCount n�mero de valores
  */   
  public function setValueCount($piNewValueCount) {
    $this->ciNewValueCount = $piNewValueCount;
  }

  /**
  * Define se a atualiza��o ser� otimista ou pessimista. 
  * 
  * <p>Define se a atualiza��o, caso o valor de novos par�metros seja 5, ser� otimista ou pessimista.</p>
  * @access public
  * @param boolean $pbOptimist Verdadeiro, se otimista. Falso, se pessimista.
  */   
  public function setOptimist($pbOptimist) {
    $this->cbOptimist = $pbOptimist;
  }

  public function makeQuery(){
    if($this->ciNewValueCount==5){
      // De 3 para 5
      if(FWDWebLib::getDatabaseType()==DB_ORACLE){
        $this->csSQL = "BEGIN param_value_3_to_5; END;";
      }else{
        $this->csSQL = "UPDATE rm_risk_value
                        SET fkValueName = (
                          SELECT nv.pkValueName
                          FROM rm_parameter_value_name nv JOIN rm_parameter_value_name ov ON (nv.nValue = ov.nValue*2-1)
                          WHERE ov.pkValueName = fkValueName
                        );
                        UPDATE rm_asset_value
                        SET fkValueName = (
                          SELECT nv.pkValueName
                          FROM rm_parameter_value_name nv JOIN rm_parameter_value_name ov ON (nv.nValue = ov.nValue*2-1)
                          WHERE ov.pkValueName = fkValueName
                        );
                        UPDATE rm_risk
                        SET fkProbabilityValueName = (
                          SELECT nv.pkValueName
                          FROM rm_parameter_value_name nv JOIN rm_parameter_value_name ov ON (nv.nValue = ov.nValue*2-1)
                          WHERE ov.pkValueName = fkProbabilityValueName
                        );
                        UPDATE rm_risk_control
                        SET fkProbabilityValueName = (
                          SELECT nv.pkValueName
                          FROM rm_parameter_value_name nv JOIN rm_parameter_value_name ov ON (nv.nValue = ov.nValue*2-1)
                          WHERE ov.pkValueName = fkProbabilityValueName
                        );
                        ";
      }
    }elseif($this->cbOptimist){
      // De 5 para 3 (otimista)
      if(FWDWebLib::getDatabaseType()==DB_ORACLE){
        $this->csSQL = "BEGIN param_value_5_to_3_optimist; END;";
      }else{
        $this->csSQL = "UPDATE rm_risk_value
                        SET fkValueName = (
                          SELECT nv.pkValueName
                          FROM rm_parameter_value_name nv JOIN rm_parameter_value_name ov ON (nv.nValue = CEILING(ov.nValue/2.0))
                          WHERE ov.pkValueName = fkValueName
                        );
                        UPDATE rm_asset_value
                        SET fkValueName = (
                          SELECT nv.pkValueName
                          FROM rm_parameter_value_name nv JOIN rm_parameter_value_name ov ON (nv.nValue = CEILING(ov.nValue/2.0))
                          WHERE ov.pkValueName = fkValueName
                        );
                        UPDATE rm_risk
                        SET fkProbabilityValueName = (
                          SELECT nv.pkValueName
                          FROM rm_parameter_value_name nv JOIN rm_parameter_value_name ov ON (nv.nValue = CEILING(ov.nValue/2.0))
                          WHERE ov.pkValueName = fkProbabilityValueName
                        );
                        UPDATE rm_risk_control
                        SET fkProbabilityValueName = (
                          SELECT nv.pkValueName
                          FROM rm_parameter_value_name nv JOIN rm_parameter_value_name ov ON (nv.nValue = CEILING(ov.nValue/2.0))
                          WHERE ov.pkValueName = fkProbabilityValueName
                        );
                        ";
      }
    }else{
      // De 5 para 3 (pessimista)
      if(FWDWebLib::getDatabaseType()==DB_ORACLE){
        $this->csSQL = "BEGIN param_value_5_to_3_pessimist; END;";
      }else{
        $this->csSQL = "UPDATE rm_risk_value
                        SET fkValueName = (
                          SELECT nv.pkValueName
                          FROM rm_parameter_value_name nv JOIN rm_parameter_value_name ov ON (nv.nValue = CEILING((ov.nValue+.5)/2.0))
                          WHERE ov.pkValueName = fkValueName
                        );
                        UPDATE rm_asset_value
                        SET fkValueName = (
                          SELECT nv.pkValueName
                          FROM rm_parameter_value_name nv JOIN rm_parameter_value_name ov ON (nv.nValue = CEILING((ov.nValue+.5)/2.0))
                          WHERE ov.pkValueName = fkValueName
                        );
                        UPDATE rm_risk
                        SET fkProbabilityValueName = (
                          SELECT nv.pkValueName
                          FROM rm_parameter_value_name nv JOIN rm_parameter_value_name ov ON (nv.nValue = CEILING((ov.nValue+.5)/2.0))
                          WHERE ov.pkValueName = fkProbabilityValueName
                        );
                        UPDATE rm_risk_control
                        SET fkProbabilityValueName = (
                          SELECT nv.pkValueName
                          FROM rm_parameter_value_name nv JOIN rm_parameter_value_name ov ON (nv.nValue = CEILING((ov.nValue+.5)/2.0))
                          WHERE ov.pkValueName = fkProbabilityValueName
                        );
                        ";
      }
    }
  }

}
?>