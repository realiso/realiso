<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryControlBestPracticesFromSections.
 *
 * <p>Consulta que retorna as melhores pr�ticas de um determinado Controle.</p>
 * @package ISMS
 * @subpackage select
 */
class QueryControlBestPracticeFromSections extends FWDDBQueryHandler {
  private $ciSectionsBP;
  
  public function __construct($poDB) {
    parent::__construct($poDB); 

    $this->coDataSet->addFWDDBField(new FWDDBField('bp.fkContext','best_practice_id',DB_NUMBER));
  }

  public function setSectionBPId($paSectcionsBP){
  		$this->ciSectionsBP = $paSectcionsBP;
  }       
  
  public function makeQuery(){
    
    $this->csSQL ="
								SELECT bp.fkContext as best_practice_id FROM rm_best_practice  bp
								JOIN isms_context ctx_bp ON (ctx_bp.pkContext = bp.fkContext AND ctx_bp.pkContext <> ".CONTEXT_STATE_DELETED.")
								JOIN rm_control_best_practice cbp ON (cbp.fkBestPractice = bp.fkContext)
								JOIN isms_context ctx_ctrl ON (ctx_ctrl.pkContext = cbp.fkControl and ctx_ctrl.pkContext <> ".CONTEXT_STATE_DELETED.")
								WHERE bp.fkSectionBestPractice IN 
								( 
										" . FWDWebLib::selectConstant("$this->ciSectionsBP as pkSection") . "								    
								    UNION
								    SELECT pkSection FROM ".FWDWebLib::getFunctionCall("get_sub_sections($this->ciSectionsBP)") . "
								    JOIN isms_context ctx_sec ON (pkSection = ctx_sec.pkContext AND ctx_sec.nState <> ".CONTEXT_STATE_DELETED.")
								)
								    
								";
  }
  
  public function getIds(){
		$this->makeQuery();
		if($this->executeQuery()){
			$maReturn = array();
			$moDataSet = $this->getDataset();
			while($moDataSet->fetch()){
				$maReturn[] = $moDataSet->getFieldByAlias('best_practice_id')->getValue();
			}
			return $maReturn;
		}
		else
			return array();
	} 
}
?>