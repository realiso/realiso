<?php
class QueryIncidentRiskParameterUpdate extends FWDDBQueryHandler {

	private $ciId;
	private $ciIncident;
	private $ciParameterName;
	private $ciParameterValue;
	
  public function __construct($poDB){
    parent::__construct($poDB);

    $this->coDataSet->addFWDDBField(new FWDDBField('ci.fkContext',                'ci_id',                        DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('ci.fkincident',  			  'ci_incident',  				  DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('ci.fkparametername',          'ci_parameter_name',            DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('ci.fkparametervalue',         'ci_parameter_value',           DB_NUMBER));
  }

  public function makeQuery(){
    $this->csSQL ="UPDATE ci_incident_risk_parameter set fkParameterValue=$this->ciParameterValue
 WHERE fkincident = $this->ciIncident and fkparametername = $this->ciParameterName";
  }
  
  public function setIncidentId($incident) {
  	$this->ciIncident = $incident;
  }
  
  public function setParameterName($parameter) {
  	$this->ciParameterName = $parameter;
  }
  
  public function setParameterValue($value) {
  	$this->ciParameterValue = $value;
  }
  
};
?>