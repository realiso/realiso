<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryDigestTasks.
 *
 * <p>Consulta para retornar os dados necessários para montar o digest.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryDigestTasks extends FWDDBQueryHandler {

  protected $ciUserId = 0;

  protected $csDigestType = 'full';

  public function __construct($poDB){
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField('t.pkTask',      'task_id',        DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('t.fkContext',   'task_context_id',DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('t.nActivity',   'task_activity',  DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('t.dDateCreated','task_date',      DB_DATETIME));
    $this->coDataSet->addFWDDBField(new FWDDBField('u.sName',       'task_sender',    DB_STRING));
 }

  public function setUserId($piUserId){
    $this->ciUserId = $piUserId;
  }

  public function setDigestType($psType){
    if($psType=='full' || $psType=='subjects'){
      $this->csDigestType = $psType;
    }else{
      trigger_error("Invalid digest type ($psType).",E_USER_WARNING);
    }
  }

  public function makeQuery(){
    if(!$this->ciUserId){
      trigger_error("User id is not defined.",E_USER_ERROR);
    }else{
      if($this->csDigestType=='subjects'){
        $this->csSQL = "SELECT
                          t.pkTask as task_id,
                          t.nActivity as task_activity
                        FROM wkf_task t
                        WHERE t.fkReceiver = {$this->ciUserId}
                          AND t.bEmailSent = 0
                          AND t.bVisible = 1
                        ORDER BY t.dDateCreated";
      }else{
        $this->csSQL = "SELECT
                          t.pkTask as task_id,
                          t.fkContext as task_context_id,
                          t.nActivity as task_activity,
                          t.dDateCreated as task_date,
                          u.sName as task_sender
                        FROM wkf_task t
                          JOIN isms_user u ON (u.fkContext = t.fkCreator)
                        WHERE t.fkReceiver = {$this->ciUserId}
                          AND t.bEmailSent = 0
                          AND t.bVisible = 1
                        ORDER BY t.dDateCreated";
      }
    }
  }

}
?>