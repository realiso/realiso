<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryIncidentRiskByRiskAndIncident.
 *
 * <p>Consulta para retornar o id da relacao incidente->risco atraves do risco e do incidente.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryIncidentRiskByRiskAndIncident extends FWDDBQueryHandler {  
  
  protected $ciIncidentId = "";
  protected $ciRiskId = "";

  public function __construct($poDB) {
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField("fkcontext", "ci_incident_risk_id",     DB_NUMBER));
  }
  
  public function setIncidentId($piIncidentId){
    $this->ciIncidentId = $piIncidentId;
  
  }
  
  public function setRiskId($piRiskId) {
  	$this->ciRiskId = $piRiskId;
  }

  
    
  public function makeQuery() {
  $this->csSQL ="
SELECT fkcontext as ci_incident_risk_id
  FROM ci_incident_risk
  JOIN isms_context ctx_r ON ctx_r.pkcontext = fkcontext AND ctx_r.nstate <> 2705 
  WHERE fkincident =  $this->ciIncidentId AND fkrisk=$this->ciRiskId
  
";  
  }
  
  public function getValue(){
    $this->makeQuery();
    $this->executeQuery();    
    $this->coDataSet->fetch();
    return($this->coDataSet->getFieldByAlias("ci_incident_risk_id")->getValue());
  }
}
?>