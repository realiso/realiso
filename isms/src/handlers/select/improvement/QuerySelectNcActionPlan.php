<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QuerySelectNcActionPlan.
 *
 * <p></p>
 * @package ISMS
 * @subpackage handlers
 */
class QuerySelectNcActionPlan extends FWDDBQueryHandler {

  protected $ciNc = 0;
  protected $ciActionPlan = 0;
  
  protected $caNcIds = null;
  protected $caActionPlanIds = null;

  public function __construct($poDB=null){
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField('','select_id'   ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('','select_value',DB_STRING));
  }

  public function setNc($piNc){
    $this->ciNc = $piNc;
  }

  public function setActionPlan($piActionPlan){
    $this->ciActionPlan = $piActionPlan;
  }

  public function setNcIds($psNcIds){
    $this->caNcIds = array_filter(explode(':',$psNcIds));
  }

  public function setActionPlanIds($psActionPlanIds){
    $this->caActionPlanIds = array_filter(explode(':',$psActionPlanIds));
  }

  public function makeQuery(){
    if($this->ciNc){
      $msSelectId = 'acpl.fkcontext';
      $msSelectName = 'acpl.sname';
      $msFrom = 'ci_nc_action_plan ncap JOIN view_ci_action_plan_active acpl ON (acpl.fkcontext = ncap.fkactionplan)';
      $msWhere = "ncap.fknc = {$this->ciNc}";
    }elseif($this->ciActionPlan){
      $msSelectId = 'n.fkcontext';
      $msSelectName = 'n.sname';
      $msFrom = 'ci_nc_action_plan ncap JOIN view_ci_nc_active n ON (n.fkcontext = ncap.fknc)';
      $msWhere = "ncap.fkactionplan = {$this->ciActionPlan}";
    }elseif($this->caNcIds!==null){
      $msSelectId = 'n.fkcontext';
      $msSelectName = 'n.sname';
      $msFrom = 'view_ci_nc_active n';
      if(count($this->caNcIds)>0){
        $msWhere = 'n.fkcontext IN ('.implode(',',$this->caNcIds).')';
      }else{
        $msWhere = '1=0';
      }
    }elseif($this->caActionPlanIds!==null){
      $msSelectId = 'acpl.fkcontext';
      $msSelectName = 'acpl.sname';
      $msFrom = 'view_ci_action_plan_active acpl';
      if(count($this->caActionPlanIds)>0){
        $msWhere = 'acpl.fkcontext IN ('.implode(',',$this->caActionPlanIds).')';
      }else{
        $msWhere = '1=0';
      }
    }else{
      trigger_error("You must specify a filter.",E_USER_ERROR);
    }
    
    $this->csSQL = "SELECT
                      {$msSelectId} AS select_id,
                      {$msSelectName} AS select_value
                    FROM {$msFrom}
                    WHERE {$msWhere}
                    ORDER BY select_value";
  }
  
  public function getIds(){
    $this->makeQuery();
    $this->executeQuery();
    $maIds = array();
    while($this->fetch()){
      $maIds[] = $this->getFieldValue('select_id');
    }
    return $maIds;
  }

}

?>