<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QuerySelectControlEfficiency.
 *
 * <p>Consulta que retorna uma lista com os controles que tem hist�rico de revis�o de efici�ncia.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QuerySelectControlTest extends FWDDBQueryHandler {
  
  public function __construct($poDB) {
    parent::__construct($poDB); 

    $this->coDataSet->addFWDDBField(new FWDDBField('ch.fkControl','select_id'   ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('ct.sName'    ,'select_value',DB_STRING));
  }

  public function makeQuery() {
    $this->csSQL = "SELECT DISTINCT ch.fkControl as select_id, ct.sName as select_value 
                    FROM rm_control_test_history ch
                    JOIN rm_control ct ON (ch.fkControl = ct.fkContext)
                    JOIN isms_context c ON (ct.fkContext = c.pkContext AND c.nState <> " . CONTEXT_STATE_DELETED . ")
                    GROUP BY ch.fkControl, ct.sName HAVING COUNT(*) > 1
                    ORDER BY ct.sName";
  }
}
?>