<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QuerySelectAreaType.
 *
 * <p>Consulta que retorna uma lista com os tipos de �reas do sistema.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QuerySelectNonConformityType extends FWDDBQueryHandler {

  protected $id = 0;
	
  public function __construct($poDB=null) {
    parent::__construct($poDB); 

    $this->coDataSet->addFWDDBField(new FWDDBField('pkClassification','select_id'   ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('sName'           ,'select_value',DB_STRING));
  }

  public function makeQuery() {
    $this->csSQL =	"SELECT pkClassification as select_id, sName as select_value " .
					"FROM isms_non_conformity_types ";
    
    if($this->id) {
    	$this->csSQL .= " WHERE pkClassification = $this->id ";
    } else {
		$this->csSQL .= " ORDER BY sName ";
    }
  }
  
  public function setTypeId($id) {
  	$this->id = $id;
  }
  
  public function getTypeName() {
    $this->makeQuery();
    $this->executeQuery();
    $maName = '';
    
    $this->coDataSet->fetch();
    $maName = $this->coDataSet->getFieldByAlias("select_value")->getValue();

    return $maName;	
  }
}
?>
