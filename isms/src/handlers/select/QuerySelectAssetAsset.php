<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QuerySelectAssetAsset.
 *
 * <p>Seleciona os dados de view_rm_asset_asset_active, guardando os dados para uma pesquisa mais rapida.
 * � possivel fazer a contagem dos dependentes e dependencias de um determinado ativo. </p>
 * @package ISMS
 * @subpackage handlers
 */
class QuerySelectAssetAsset {

  public static $dependentAsset;
  public static $assetDependent;

  public static function load() {
    
    self::$dependentAsset = array();
    self::$assetDependent = array();
    
    $query = new FWDDBDataSet(FWDWebLib::getConnection());
    $query->setQuery("SELECT fkasset, fkdependent FROM view_rm_asset_asset_active");
    $query->addFWDDBField(new FWDDBField('fkasset','fkasset'  ,DB_NUMBER));
    $query->addFWDDBField(new FWDDBField('fkdependent','fkdependent'  ,DB_NUMBER));
    $query->execute();    

    while($query->fetch())
    {
      self::$dependentAsset[ $query->getFieldByAlias("fkdependent")->getValue() ][] = $query->getFieldByAlias("fkasset")->getValue();
      self::$assetDependent[ $query->getFieldByAlias("fkasset")->getValue() ][] = $query->getFieldByAlias("fkdependent")->getValue();
    }
  }
  
  public static function getDependencies($assetId, &$tmpList, &$list){
    $assetList = array();

    if(isset($list[$assetId]))
    {
      $assetList = $list[$assetId];
      foreach($assetList as $key => $value){
        $tmpList[] = $value;
        self::getDependencies($value, $tmpList, $list);
      }
    }
  }
  
  // esse m�todo � mais rapido que o array_unique() do php.
  public static function array_unique_fast($array){
    return array_merge(array_flip(array_flip($array)));
  } 

  public static function getAssetDependencies($assetId){
    $assets = array();
    self::getDependencies($assetId, $assets, self::$dependentAsset);
    $assets_unique = self::array_unique_fast($assets); 

    return $assets_unique;
  }
  
  public static function getAssetDependenciesCount($assetId){
    return count(self::getAssetDependencies($assetId));
  }

  public static function getAssetDependents($assetId){
    $assets = array();
    self::getDependencies($assetId, $assets, self::$assetDependent);
    $assets_unique = self::array_unique_fast($assets); 

    return $assets_unique;
  }
  
  public static function getAssetDependentsCount($assetId){
    return count(self::getAssetDependents($assetId));
  }
}
?>
