<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QuerySelectSolicitor.
 *
 * <p>Consulta que retorna uma lista com os usu�rios que s�o representados por um usu�rio.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QuerySelectSolicitor extends FWDDBQueryHandler {
	
	protected $ciSolicitorId = 0;
		
  public function __construct($poDB) {
    parent::__construct($poDB); 

    $this->coDataSet->addFWDDBField(new FWDDBField('s.fkUser','select_id'   ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('u.sName' ,'select_value',DB_STRING));

  }

	public function setSolicitor($piSolicitorId) {
		$this->ciSolicitorId = $piSolicitorId;
	}
		
	public function makeQuery() {	
		/*
		 * Antes de fazer a consulta, deleta da tabela os registros
		 * em que o intervalo de procura��o j� expirou.
		 */
		$moSolicitor = new ISMSSolicitor();		
		$moSolicitor->createFilter(ISMSLib::ISMSTime(), 'date_finish', '<');
		$moSolicitor->delete();
		
		$msDate = ISMSLib::getTimestampFormat(ISMSLib::ISMSTime());		
		$this->csSQL = "SELECT s.fkUser as select_id, u.sName as select_value
										FROM isms_solicitor s JOIN isms_user u ON (s.fkUser = u.fkContext AND s.fkSolicitor = {$this->ciSolicitorId})
										WHERE $msDate >= s.ddatebegin AND $msDate <= s.ddatefinish";
	}
}
?>