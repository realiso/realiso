<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QuerySelectAsset.
 *
 * <p>Consulta que retorna uma lista com os ativos do sistema.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QuerySelectAssetFromProcess extends FWDDBQueryHandler {

  private $ciProcessId = 0;
  private $ciAreaId = 0;
  
  public function setAreaId($piAreaId) {
    $this->ciAreaId = $piAreaId;
  }	  
  
  public function setProcessId($piProcessId) {
    $this->ciProcessId = $piProcessId;
  }	
	
  public function __construct($poDB=null) {
    parent::__construct($poDB); 

    $this->coDataSet->addFWDDBField(new FWDDBField('a.fkContext','select_id'   ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('a.sName'    ,'select_value',DB_STRING));
  }
  
  public function makeQuery(){
  	$joins="";
  	$processFilter="";
  	$areaFilter="";
  	
  	if($this->ciProcessId){
  		$processFilter = " AND pa.fkprocess = " . $this->ciProcessId . " ";
  	}
  	
  	if($this->ciAreaId){
  		$joins .= " JOIN view_rm_process_active p on (p.fkcontext = pa.fkprocess)
					  JOIN view_rm_area_active a on (a.fkcontext = p.fkarea) ";
  		
  		$areaFilter = " AND a.fkcontext = " . $this->ciAreaId . " ";
  	}
  	
  	if(!$this->ciProcessId && !$this->ciAreaId){
		$this->csSQL = "SELECT a.fkContext as select_id, a.sName as select_value
	                    FROM view_rm_asset_active a ORDER BY a.sName";  
  	}else{
	  	$this->csSQL = " SELECT raa.fkcontext as select_id, raa.sname as select_value 
	  					 FROM view_rm_process_asset_active pa 
						 JOIN view_rm_asset_active raa on (raa.fkcontext = pa.fkasset)
	  					 {$joins}
	 					 WHERE 1=1 {$processFilter} {$areaFilter} ";
  	}  	
  }
}
?>