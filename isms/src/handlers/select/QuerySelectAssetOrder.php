<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QuerySelectAssetOrder.
 *
 * <p></p>
 * @package ISMS
 * @subpackage handlers
 */
class QuerySelectAssetOrder extends FWDDBQueryHandler {

  protected $assets;
  
  public function __construct($poDB=null) {
    parent::__construct($poDB); 
    $this->coDataSet->addFWDDBField(new FWDDBField('a.fkContext','asset_id'   ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('a.sName'    ,'asset_name' ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('a.nValue'   ,'asset_value',DB_NUMBER));    
  }

  public function setAssets($arr){
    $this->assets = $arr;
  }
  
  public function makeQuery() {
    if(count($this->assets) > 0){
      $str = implode(', ', $this->assets);
      $this->csSQL = "select fkcontext as asset_id, sname as asset_name, nvalue as asset_value from view_rm_asset_active where fkcontext in ($str) order by sname ";
    }else{
      $this->csSQL = "select * from view_rm_asset_active where fkcontext = -1";
    }
  }
}
?>