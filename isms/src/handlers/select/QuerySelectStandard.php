<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QuerySelectStandard.
 *
 * <p>Consulta que retorna uma lista com as normas de uma melhor pr�tica.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QuerySelectStandard extends FWDDBQueryHandler {
	
	protected $ciBestPracticeId = 0;
	
	protected $caStandards = array();
	
	protected $cbGetAllStandards = false;
	
	protected $caValues = array();
	
  public function __construct($poDB) {
    parent::__construct($poDB); 

    $this->coDataSet->addFWDDBField(new FWDDBField('s.fkContext','select_id'   ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('s.sName'    ,'select_value',DB_STRING));
  }

	public function setBestPractice($pbBestPracticeId) {
		$this->ciBestPracticeId = $pbBestPracticeId;
	}
		
	public function makeQuery() {		
		$miBestPracticeId = $this->ciBestPracticeId ? $this->ciBestPracticeId : 0; 
		
		$miCount = count($this->caStandards);
		
		if ($miCount) {
			$msStandards = implode(',', $this->caStandards);
			$this->csSQL = "SELECT s.fkContext as select_id, s.sName as select_value
							FROM rm_standard s							
							JOIN isms_context c ON (s.fkContext = c.pkContext AND c.nState <> " . CONTEXT_STATE_DELETED . ")
							WHERE s.fkContext IN ($msStandards)";				
		} else if ($this->cbGetAllStandards) {
			$this->csSQL = "SELECT s.fkContext as select_id, s.sName as select_value FROM view_rm_standard_active s";							
		}
		else {		
			$this->csSQL = "SELECT s.fkContext as select_id, s.sName as select_value
							FROM rm_standard s
							JOIN rm_best_practice_standard bps ON (s.fkContext = bps.fkStandard AND bps.fkBestPractice = $miBestPracticeId)
							JOIN isms_context c ON (s.fkContext = c.pkContext AND c.nState <> " . CONTEXT_STATE_DELETED . ")";
		}				
	}
	
	public function executeQuery() {
		parent::executeQuery();			
		while ($this->coDataSet->fetch()) {			
			$this->caValues[] = array($this->coDataSet->getFieldByAlias("select_id")->getValue(), $this->coDataSet->getFieldByAlias("select_value")->getValue());
		}
	}
	
	public function setAllStandards($pbAllStandards) {
		$this->cbGetAllStandards = $pbAllStandards; 
	}
	
	public function setStandards($paStandards) {
		$this->caStandards = $paStandards; 
	}
	
	public function getValues() {
		return $this->caValues;
	}			
}
?>