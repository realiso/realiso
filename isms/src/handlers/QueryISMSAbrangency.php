<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryISMSAbrangency.
 *
 * <p>Consulta para popular o grid de abrangência do SGSI.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryISMSAbrangency extends FWDDBQueryHandler {

  protected $caISMSAbrangency = array();

  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField("","ordem", DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("","count", DB_NUMBER));
  }

  public function makeQuery() {
    $this->csSQL = "
SELECT 'total_areas' as ordem, count(*) as count
FROM rm_area a
JOIN isms_context ctx ON (a.fkContext = ctx.pkContext AND ctx.nState != ".CONTEXT_STATE_DELETED.")
  UNION
SELECT 'total_processes' as ordem, count(*) as count
FROM rm_process p
JOIN isms_context ctx ON (p.fkContext = ctx.pkContext AND ctx.nState != ".CONTEXT_STATE_DELETED.")
  UNION
SELECT 'total_assets' as ordem, count(*) as count
FROM rm_asset a
JOIN isms_context ctx ON (a.fkContext = ctx.pkContext AND ctx.nState != ".CONTEXT_STATE_DELETED.")

  UNION

SELECT 'managed_areas' as ordem, count(*) as count
FROM rm_area a
JOIN isms_context ctx ON (a.fkContext = ctx.pkContext AND ctx.nState != ".CONTEXT_STATE_DELETED." AND a.nValue>0)
  UNION
SELECT 'managed_processes' as ordem, count(*) as count
FROM rm_process p
JOIN isms_context ctx ON (p.fkContext = ctx.pkContext AND ctx.nState != ".CONTEXT_STATE_DELETED." AND p.nValue>0)
  UNION
SELECT 'managed_assets' as ordem, count(*) as count
FROM rm_asset a
JOIN isms_context ctx ON (a.fkContext = ctx.pkContext AND ctx.nState != ".CONTEXT_STATE_DELETED." AND a.nValue>0)
";
  }

  public function executeQuery() {
    parent::executeQuery();
    $this->caISMSAbrangency = array();
    while ($this->coDataSet->fetch())
      $this->ISMSAbrangency[$this->coDataSet->getFieldByAlias("ordem")->getValue()] = $this->coDataSet->getFieldByAlias("count")->getValue();
  }

  public function getISMSAbrangency() {
    return $this->ISMSAbrangency;
  }
}
?>