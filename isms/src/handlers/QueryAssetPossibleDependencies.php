<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
include_once $handlers_ref . "select/QuerySelectAssetAsset.php";
/**
 * Classe QueryAssetPossibleDependencies.
 *
 * <p>Consulta que retorna uma lista com as dependências possíveis de um ativo.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryAssetPossibleDependencies extends FWDDBQueryHandler {

  protected $ciAssetId = 0;
  protected $caAssetDependencies = array();

  public function __construct($poDB) {
    parent::__construct($poDB);

    $this->coDataSet->addFWDDBField(new FWDDBField('a.fkContext','asset_id'   ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('a.sName'    ,'asset_name' ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('a.nValue'   ,'asset_value',DB_NUMBER));

    $this->coDataSet->setOrderBy("asset_name", "+");
  }

  public function makeQuery() {
    if ($this->ciAssetId != 0) {
       
      QuerySelectAssetAsset::load();
      
      $assets = array();
      QuerySelectAssetAsset::getDependencies($this->ciAssetId, $assets, QuerySelectAssetAsset::$assetDependent);
      $assets = array_unique($assets);
       
      $notIn = "";
      if($assets && count($assets)){
        $notIn .= "a.fkContext NOT IN (".(implode(',',$assets)).") AND";
      }
       
      $this->csSQL = "SELECT a.fkContext as asset_id, a.sName as asset_name, a.nValue as asset_value
											FROM view_rm_asset_active a
											WHERE
											{$notIn}
											a.fkContext NOT IN (SELECT fkAsset FROM view_rm_asset_asset_active WHERE fkDependent = {$this->ciAssetId}) AND
											a.fkContext <> {$this->ciAssetId}";
    }
  }

  public function executeQuery() {
    parent::executeQuery();
    $this->caAssetDependencies=array();
    while ($this->coDataSet->fetch()) {
      $this->caAssetDependencies[] = array(
							'asset_id' => $this->coDataSet->getFieldByAlias("asset_id")->getValue(),
							'asset_name' => $this->coDataSet->getFieldByAlias("asset_name")->getValue(),	
							'asset_value' => $this->coDataSet->getFieldByAlias("asset_value")->getValue()
      );
    }
  }

  public function getAssetDependencies() {
   	return $this->caAssetDependencies;
 	}

 	public function setAsset($piAssetId) {
 	  $this->ciAssetId = $piAssetId;
 	}
}
?>