<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QuerySummaryBestPractice.
 *
 * <p>Consulta para popular o grid de sum�rio de melhores pr�ticas.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QuerySummaryBestPractice extends FWDDBQueryHandler {

  protected $caBestPracticeSummary = array();
  
  public function __construct($poDB=null){
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField('s.fkContext','standard_id'  ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('s.sName'    ,'standard_name',DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField(''           ,'total'        ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField(''           ,'applied'      ,DB_NUMBER));
  }

  public function makeQuery(){
    $this->csSQL = "
    SELECT
      s.fkContext AS standard_id,
      s.sName AS standard_name,
      total,
      applied
    FROM 
      view_rm_standard_active s
      LEFT JOIN (
          SELECT bps.fkStandard, count(DISTINCT bps.fkBestPractice) AS total
          FROM view_rm_bp_standard_active bps
          GROUP BY bps.fkStandard
      ) t ON (t.fkStandard = s.fkContext)
      LEFT JOIN (
          SELECT bps.fkStandard, count(DISTINCT bps.fkBestPractice) AS applied
          FROM
            view_rm_bp_standard_active bps
            JOIN view_rm_control_bp_active cbp ON (cbp.fkBestPractice = bps.fkBestPractice)
          GROUP BY bps.fkStandard
      ) a ON (a.fkStandard = t.fkStandard)
    ORDER BY s.sName
    ";
  }

  public function executeQuery(){
    parent::executeQuery();
    $this->caBestPracticeSummary = array();
    while($this->coDataSet->fetch()){
      $miId = $this->coDataSet->getFieldByAlias('standard_id')->getValue();
      $msName = $this->coDataSet->getFieldByAlias('standard_name')->getValue();
      $miTotal = $this->coDataSet->getFieldByAlias('total')->getValue();
      $miApplied = $this->coDataSet->getFieldByAlias('applied')->getValue();
      
      $this->caBestPracticeSummary[$miId] = array(
        'name' => $msName,
        'total' => ($miTotal?$miTotal:0),
        'applied' => ($miApplied?$miApplied:0)
      );
    }
  }

  public function getBestPracticeSummary(){
    return $this->caBestPracticeSummary;
  }

}

?>