<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryDigestAlerts.
 *
 * <p>Consulta para retornar os dados necessários para montar o digest.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryDigestAlerts extends FWDDBQueryHandler {

  protected $ciUserId = 0;

  protected $csDigestType = 'full';

  public function __construct($poDB) {
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField('a.pkAlert',       'alert_id',           DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('u.fkContext',     'user_id',            DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('cn.context_type', 'alert_context_type', DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('a.fkContext',     'alert_context_id',   DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('cn.context_name', 'alert_context_name', DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('u.sName',         'alert_sender',       DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('a.tJustification','alert_justification',DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('a.nType',       	'alert_type',       DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('a.dDate',         'alert_date',         DB_DATETIME));
 }

  public function setUserId($piUserId){
    $this->ciUserId = $piUserId;
  }

  public function setDigestType($psType){
    if($psType=='full' || $psType=='subjects'){
      $this->csDigestType = $psType;
    }else{
      trigger_error("Invalid digest type ($psType).",E_USER_WARNING);
    }
  }

  public function makeQuery(){
    if(!$this->ciUserId){
      trigger_error("User id is not defined.",E_USER_ERROR);
    }else{
      if($this->csDigestType=='subjects'){
        $this->csSQL = "SELECT
                          a.pkAlert as alert_id,
                          cn.context_type as alert_context_type,
                          a.nType as alert_type,
                          a.fkContext as alert_context_id,
                          cn.context_name as alert_context_name
                        FROM
                          wkf_alert a
                          JOIN context_names cn ON (a.fkContext = cn.context_id)
                        WHERE a.fkReceiver = {$this->ciUserId}
                          AND a.bEmailSent = 0
                        ORDER BY a.dDate";
      }else{
        $this->csSQL = "SELECT
                          a.pkAlert as alert_id,
                          cn.context_type as alert_context_type,
                          a.fkContext as alert_context_id,
                          cn.context_name as alert_context_name,
                          u.sName as alert_sender,
                          a.tJustification as alert_justification,
                          a.nType as alert_type,
                          a.dDate as alert_date
                        FROM
                          wkf_alert a
                          JOIN isms_user u ON (u.fkContext = a.fkCreator)
                          JOIN context_names cn ON (a.fkContext = cn.context_id)
                        WHERE a.fkReceiver = {$this->ciUserId}
                          AND a.bEmailSent = 0
                        ORDER BY a.dDate";
      }
    }
  }

}
?>