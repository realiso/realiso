<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryParameterControlValue.
 *
 * <p>Consulta que retorna os valores dos parametros dos controles associados ao risco cujo id seja diferente
 * do id do controle setado no queryhandler.</p>
 * @package ISMS
 * @subpackage select
 */
class QueryParameterControlValue extends FWDDBQueryHandler {
	private $ciControlId = 0;
	private $ciRiskId = 0;

	public function __construct($poDB){
		parent::__construct($poDB);

		$this->coDataSet->addFWDDBField(new FWDDBField('','value_id',DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField('','parameter_value',DB_NUMBER));
	}

	public function setControlId($piControlId){
		$this->ciControlId = $piControlId;
	}

	public function setRiskId($piRiskId){
		$this->ciRiskId = $piRiskId;
	}

	public function makeQuery(){
		$msWhere ='';
		if($this->ciControlId){
			$msWhere = "AND rc.fkControl <> ".$this->ciControlId;
		}
		$this->csSQL ="
					SELECT rcv.fkparametername as value_id, sum(rcpvn.nvalue) as parameter_value FROM rm_risk_control rc
					JOIN rm_control c ON (c.fkContext = rc.fkControl AND c.bIsActive = 1)
					JOIN isms_context ctx ON (ctx.pkContext = c.fkContext AND ctx.nState <> " . CONTEXT_STATE_DELETED . ")
					JOIN rm_risk_control_value rcv ON (rcv.fkRiskControl = rc.fkContext)
					JOIN rm_rc_parameter_value_name rcpvn ON (rcpvn.pkRCValueName = rcv.fkRCValueName)
					   WHERE rc.fkRisk= ". $this->ciRiskId ." 
					   $msWhere
					GROUP BY rcv.fkparametername
		";
	}

	public function getParameterValues(){
		$this->makeQuery();
		$maReturn = array();
		if($this->executeQuery()){
			$moDataset = $this->getDataset();
			while($moDataset->fetch()){
				$maReturn[$moDataset->getFieldByAlias("value_id")->getValue()]=$moDataset->getFieldByAlias("parameter_value")->getValue();
			}
		}
		return $maReturn;
	}
}
?>