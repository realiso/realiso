<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryCountDocumentsByType
 *
 * <p>Consulta para calcular o n�mero de documentos de cada tipo e o numero de
 * instancias de contexto de cada tipo para calcular o percentual de cada tipo.
 * </p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryCountDocumentsByType extends FWDDBQueryHandler {
  protected $caDocCount = array(  CONTEXT_AREA=>0,
                                  CONTEXT_PROCESS=>0,
                                  CONTEXT_ASSET=>0,
                                  CONTEXT_CONTROL=>0,
                                );
  
  protected $ciUserId = 0;
  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField('','context_type'  ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('','num_ctx'       ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('','docs_per_type' ,DB_NUMBER));
  }

  public function setUserId($piUserId){
    $this->ciUserId = $piUserId;
  }
  
  public function makeQuery() {
  $msContextsType = CONTEXT_AREA.",".CONTEXT_PROCESS.",".CONTEXT_ASSET.",".CONTEXT_CONTROL;
  $msWhere = '';
  $msIN='';
  $msWhereContent='';
  if($this->ciUserId){
    $msContextsType = CONTEXT_AREA.",".CONTEXT_PROCESS.",".CONTEXT_ASSET.",".CONTEXT_CONTROL;

    $msWhereContent .="
        SELECT fkContext FROM view_rm_area_active 
          WHERE fkResponsible = {$this->ciUserId}
        UNION
        SELECT fkContext FROM view_rm_process_active 
          WHERE fkResponsible = {$this->ciUserId}
        UNION
        SELECT fkContext FROM view_rm_asset_active 
          WHERE fkResponsible = {$this->ciUserId}
        UNION
        SELECT fkContext FROM view_rm_control_active 
          WHERE fkResponsible = {$this->ciUserId}
     ";
    $msWhere .="
       WHERE dc.fkContext IN ( 
          $msWhereContent
        )
       ";
    $msIN = "
      AND ctx.pkContext IN (
          $msWhereContent
      )
      ";
  }

    $this->csSQL = "
  
  SELECT ctx.nType as context_type, 
       count(ctx.pkContext) as num_ctx, 
       doc.docs_per_type as docs_per_type
  FROM view_isms_context_active ctx
    LEFT JOIN (
      SELECT context_type as context_type, 
             count(context_type) as docs_per_type FROM 
        (
          SELECT ctx.nType as context_type 
            FROM view_isms_context_active ctx
              JOIN pm_doc_context dc ON (ctx.pkContext = dc.fkContext)
              JOIN view_pm_published_docs docp ON (docp.fkContext = dc.fkDocument)
              $msWhere
            GROUP BY dc.fkDocument, ctx.nType
        )tb_doc_type_context
        GROUP BY tb_doc_type_context.context_type
    ) doc ON (ctx.nType = doc.context_type)
  WHERE ctx.nType IN ($msContextsType)
         $msIN
  GROUP BY ctx.nType, doc.docs_per_type
  ORDER BY ctx.nType
";
  }
  
  
  public function executeQuery() {
    parent::executeQuery();
    while($this->coDataSet->fetch()){
      $this->caDocCount[$this->coDataSet->getFieldByAlias('context_type')->getValue()] 
            = array(  'all'=>$this->coDataSet->getFieldByAlias("num_ctx")->getValue(),
                      'doc'=>$this->coDataSet->getFieldByAlias("docs_per_type")->getValue()
               );
    }
  }
  
  public function getValues() {
    $this->makeQuery();
    $this->executeQuery();
    return $this->caDocCount;
  }
}
?>