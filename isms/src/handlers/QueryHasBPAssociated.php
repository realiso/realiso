<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryHasBPAssociated.
 *
 * <p>Consulta que verifica se a norma possui alguma melhor pr�tica associada.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryHasBPAssociated extends FWDDBQueryHandler {
  private $ciStandardId;

  public function __construct($poDB) {
    parent::__construct($poDB); 

    $this->coDataSet->addFWDDBField(new FWDDBField('','best_practice_id'  ,DB_NUMBER));
  }

  public function setStandardId($piStandardId){
  	$this->ciStandardId = $piStandardId;
  }
  
  public function makeQuery(){    
    $this->csSQL ="SELECT fkBestPractice as best_practice_id FROM view_rm_bp_standard_active WHERE fkStandard = " . $this->ciStandardId;
  }
  
  public function hasBPAssociated(){
		$this->makeQuery();
		if($this->executeQuery()){
			$moDataSet = $this->getDataset();
			if ($moDataSet->fetch()){
        return true;
      }
      else {
        return false;
      }
    }
	} 
}
?>