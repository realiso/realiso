<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QuerySummaryAbnormalityPM.
 *
 * <p>Consulta para popular o grid de sum�rio de anormalidades da gest�o de politicas.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QuerySummaryAbnormalityPM extends FWDDBQueryHandler {

  protected $caValues = array();

  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField("ordem","ordem", DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("count","count", DB_NUMBER));
 }

  public function makeQuery() {
    $msCurrentTime = ISMSLib::getTimestampFormat(time());
    $miDateTest = ISMSLib::ISMSTime();
    $moConfig = new ISMSConfig();
    $msWhere = '';
    $maFilterTypes = array(ACT_DOCUMENT_REVISION,ACT_DOCUMENT_APPROVAL,ACT_DOCUMENT_TEMPLATE_APPROVAL);
    $miDate = mktime (0, 0, 0, date("m",$miDateTest), date("d",$miDateTest+86400),  date("Y",$miDateTest));
    $miDateLate = mktime (0, 0, 0, date("m",$miDateTest), date("d",$miDateTest - (86400*6)),  date("Y",$miDateTest));
    
    $msDate = ISMSLib::getTimestampFormat($miDate);
    $msDateLate = ISMSLib::getTimestampFormat($miDateLate);
    
    if(count($maFilterTypes)){
      $msWhere .= "AND wt.nActivity IN (".implode(',',$maFilterTypes).") ";
    }
    
    $msNumDates = "";
    switch (FWDWebLib::getConnection()->getDatabaseType()) {
      case DB_MSSQL:{
        $msNumDates = " DATEDIFF(day, dt1.dDate, dt2.dDate) ";
        break;
      }case DB_POSTGRES:{
        $msNumDates = " EXTRACT (DAYS FROM dt2.dDate  - dt1.dDate) ";
        break;
      }case DB_ORACLE:{
        $msNumDates = " floor(dt2.dDate - dt1.dDate) ";
        break;
      }
    }

    
    $this->csSQL = "
"./* tarefas pendentes da gent�o de pol�ticas */"
SELECT 'pendant_tasks' as ordem, count(*) as count
FROM wkf_task wt
WHERE wt.bVisible = 1
$msWhere

UNION
"./* relat�rio de documentos com uma media alta de revis�o
   'Freq�encia muito alta significa documentos que s�o revisados em m�dia mais de uma vez por semana'
  (N�mero de revis�es) / ( [ (data de moficica��o) - (data de cria��o) ] / 7) > 1 -- dai � muito frequente
   f�rmula acima � equivalente a: (N�mero de revis�es) * 7 / ( (data de moficica��o) - (data de cria��o) ) > 1*/ "
SELECT 'docs_with_high_frequency_revision' as ordem, count(*) as count
   FROM view_pm_document_active d
   JOIN isms_context_date dt1 ON (dt1.fkContext = d.fkContext AND dt1.nAction = 2901)
   JOIN isms_context_date dt2 ON (dt2.fkContext = d.fkContext AND dt2.nAction = 2902)
   WHERE (  $msNumDates > 0 AND
           ( (SELECT count(*) FROM view_pm_doc_instance_active di WHERE di.fkDocument = d.fkContext AND di.dEndProduction IS NOT NULL ) * 7 )
             /
             ( $msNumDates )
            > 1
        )OR( $msNumDates = 0 AND
             (SELECT count(*) FROM view_pm_doc_instance_active di WHERE di.fkDocument = d.fkContext AND di.dEndProduction IS NOT NULL ) > 1
        )
UNION
"./* documentos com leitura pendente */"
SELECT 'docs_with_pendant_read' as ordem, count(*) as count from view_pm_published_docs d
   WHERE d.fkContext IN ( SELECT dr.fkDocument FROM view_pm_doc_readers_active dr WHERE dr.bHasRead = 0 OR dr.bHasRead IS NULL ) 

UNION
"./* elementos do RM sem documentos associados*/"
SELECT 'rm_contexts_without_document' as ordem, sum(res.count) as count
FROM(
  SELECT 'area' as ordem, count(*) as count 
     FROM view_rm_area_active a
     WHERE a.fkContext NOT IN (SELECT da.fkContext FROM view_pm_doc_context_active da
                               JOIN view_pm_published_docs dp ON (dp.fkContext = da.fkDocument)
                              )
  UNION 

  SELECT 'process' as ordem, count(*) as count 
     FROM view_rm_process_active p
     WHERE p.fkContext NOT IN (SELECT da.fkContext FROM view_pm_doc_context_active da
                               JOIN view_pm_published_docs dp ON (dp.fkContext = da.fkDocument)
                              )
  UNION 

  SELECT 'asset' as ordem, count(*) as count 
     FROM view_rm_asset_active a
     WHERE a.fkContext NOT IN (SELECT da.fkContext FROM view_pm_doc_context_active da
                               JOIN view_pm_published_docs dp ON (dp.fkContext = da.fkDocument)
                              )
  UNION 

  SELECT 'control' as ordem, count(*) as count 
     FROM view_rm_control_active c
     WHERE c.fkContext NOT IN (SELECT da.fkContext FROM view_pm_doc_context_active da
                               JOIN view_pm_published_docs dp ON (dp.fkContext = da.fkDocument)
                              )
  UNION 

  SELECT 'action_plan' as ordem, count(*) as count 
     FROM view_ci_action_plan_active ap
     WHERE ap.fkContext NOT IN (SELECT da.fkContext FROM view_pm_doc_context_active da
                               JOIN view_pm_published_docs dp ON (dp.fkContext = da.fkDocument)
                              )
  )res

UNION
"./* documentos nunca lidos*/"
SELECT 'pm_document_never_read' as ordem, count(d.fkContext) as count FROM view_pm_published_docs d
   WHERE d.fkContext NOT IN (
     SELECT di.fkDocument FROM view_pm_doc_instance_active di
     JOIN pm_doc_read_history drh ON ( drh.fkInstance = di.fkContext )
  )

UNION
"./* documentos sem registro*/"
SELECT 'pm_document_without_register' as ordem, count(d.fkContext) as count FROM view_pm_published_docs d
   WHERE d.fkContext NOT IN (
     SELECT dr.fkDocument FROM view_pm_doc_registers_active dr
  )

UNION
"./* usu�rios com leitura pendente*/"
SELECT 'pm_user_with_pendant_reads' as ordem, count(*) as count
   FROM view_isms_user_active
   WHERE fkContext IN ( SELECT fkUser 
         FROM view_pm_doc_readers_active 
                           WHERE bHasRead = 0 OR bHasRead IS NULL
                      )

";
  }

  public function executeQuery() {
    parent::executeQuery();
    while ($this->coDataSet->fetch())
      $this->caValues[$this->coDataSet->getFieldByAlias("ordem")->getValue()] = $this->coDataSet->getFieldByAlias("count")->getValue();
  }

  public function getValues() {
    $this->makeQuery();
    $this->executeQuery();
    return $this->caValues;
  }
  
}
?>