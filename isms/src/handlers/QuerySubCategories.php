<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QuerySubCategories.
 *
 * <p>Consulta para buscar as sub-categorias de uma categoria.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QuerySubCategories extends FWDDBQueryHandler {
  
  protected $ciCategoryId = 0;
  
  protected $caSubCategories = array();
  
  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField('pkCategory','category_id',DB_NUMBER));
  }
  
  public function setCategory($piCategoryId) {
    $this->ciCategoryId = $piCategoryId;
  }
  
  public function makeQuery() {
    $this->csSQL = "SELECT pkCategory as category_id from ".FWDWebLib::getFunctionCall("get_sub_categories({$this->ciCategoryId})")."
    									JOIN isms_context c ON(c.pkContext = pkCategory and c.nState <> " . CONTEXT_STATE_DELETED . ")
";
  }
  
  public function executeQuery() {
    parent::executeQuery();
    while ($this->coDataSet->fetch()) {
      $this->caSubCategories[] = $this->coDataSet->getFieldByAlias("category_id")->getValue();
    }
  }
  
  /**
   * Retorna as sub-categorias de uma categoria.
   * 
   * <p>M�todo para retornar as sub-categorias de uma categoria.</p>
   * @access public 
   * @return array Array de ids das sub-categorias
   */ 
  public function getSubCategories() {
    return $this->caSubCategories;
  }
}
?>