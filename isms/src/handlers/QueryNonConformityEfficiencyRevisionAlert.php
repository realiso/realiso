<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryNonConformityEfficiencyRevisionAlert
 *
 * <p>Consulta para pesquisar as n�o conformidades que tem
 * revis�o pr�xima e que ainda n�o tiveram alertas enviados.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryNonConformityEfficiencyRevisionAlert extends FWDDBQueryHandler {

  public function __construct($poDB){
    parent::__construct($poDB);

    $this->coDataSet->addFWDDBField(new FWDDBField('nc.fkContext',                'nc_id',                        DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('nc.dDateEfficiencyRevision',  'nc_efficiency_revision_date',  DB_DATETIME));
    $this->coDataSet->addFWDDBField(new FWDDBField('nc.nDaysBefore',              'nc_days_before',               DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('nc.fkActionPlanResponsible',  'nc_ap_responsible',   DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('nc.fkResponsible',            'nc_responsible_id',            DB_NUMBER));
  }

  public function makeQuery(){
    $this->csSQL ="
                  SELECT nc.fkContext as nc_id,
                         nc.dDateEfficiencyRevision as nc_efficiency_revision_date,
                         nc.nDaysBefore as nc_days_before,
                         nc.fkActionPlanResponsible as nc_ap_responsible,
                         nc.fkResponsible as nc_responsible_id
                  FROM view_ci_nc_active nc
                  WHERE
                    nc.dDateEfficiencyRevision IS NOT NULL AND
                    (nc.bFlagRevisionAlert=0 OR nc.bFlagRevisionAlert IS NULL)
";
  }
}
?>