<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryControlBP.
 *
 * <p>Consulta que retorna as melhores pr�ticas de um determinado Controle.</p>
 * @package ISMS
 * @subpackage select
 */
class QueryControlBP extends FWDDBQueryHandler {
  private $ciControlId;

  public function __construct($poDB) {
    parent::__construct($poDB); 

    $this->coDataSet->addFWDDBField(new FWDDBField('','best_practice_id'  ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('','best_practice_name',DB_STRING));
  }

  public function setControlId($psControlId){
  	$this->ciControlId = $psControlId;
  }
  
  public function makeQuery(){
    $msWhere = ''; 
    if($this->ciControlId)
    	$msWhere .= " where ctrl.fkContext = $this->ciControlId ";
    
    $this->csSQL ="
SELECT bp.fkcontext as best_practice_id, bp.sname as best_practice_name
  FROM rm_best_practice bp
	  JOIN isms_context c ON (bp.fkContext = c.pkContext AND c.nState <> " . CONTEXT_STATE_DELETED . " )
		JOIN rm_control_best_practice cbp ON (bp.fkContext = cbp.fkBestPractice)
		JOIN rm_control ctrl ON (cbp.fkControl = ctrl.fkContext)
    " . $msWhere ;
  }
  
  public function getBestPracticesControl(){
		$this->makeQuery();
		if($this->executeQuery()){
			$maReturn = array();
			$moDataSet = $this->getDataset();
			while($moDataSet->fetch()){
				$maReturn[] = array($moDataSet->getFieldByAlias('best_practice_id')->getValue(),$moDataSet->getFieldByAlias('best_practice_name')->getValue());
			}
			return $maReturn;
		}
		else
			return array();
	} 
}
?>