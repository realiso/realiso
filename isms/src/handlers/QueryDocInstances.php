<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryDocInstances
 *
 * <p>Consulta para pegar os dados de uma inst�ncia (incluindo dados do documento).</p>
 * @package ISMS
 * @subpackage classes
 */
class QueryDocInstances extends FWDDBQueryHandler {

  protected $ciDocumentId = 0;
  protected $ciInstanceId = 0;
  protected $caStatus = array();
  protected $orderBy = '';

  /**
  * Construtor.
  *
  * <p>Construtor da classe.</p>
  * @access public
  */
  public function __construct($poDB){
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField('d.fkContext'       ,'document_id'              ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField(''                  ,'document_instance_id'     ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('d.fkAuthor'        ,'document_author'          ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('d.fkMainApprover'  ,'document_main_approver'   ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField(''                  ,'user_has_approved'        ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('d.sName'           ,'document_name'            ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('di.sFileName'      ,'document_file_name'       ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('d.dDateProduction' ,'document_date_production' ,DB_DATETIME));
    $this->coDataSet->addFWDDBField(new FWDDBField('di.sPath'          ,'document_file_path'       ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('di.bIsLink'        ,'document_is_link'         ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('di.sLink'          ,'document_link'            ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField(''                  ,'document_status'          ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField(''                  ,'user_is_approver'         ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField(''                  ,'user_is_reader'           ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField(''                  ,'count_approvers'          ,DB_NUMBER));
  }

  public function getDocumentId(){
    return $this->ciDocumentId;
  }

  public function setDocumentId($piDocumentId){
    $this->ciDocumentId = $piDocumentId;
  }

  public function getInstanceId(){
    return $this->ciInstanceId;
  }

  public function setInstanceId($piInstanceId){
    $this->ciInstanceId = $piInstanceId;
  }

  public function getStatus(){
    return $this->caStatus;
  }

  public function setStatus($paStatus){
    $this->caStatus = $paStatus;
  }

  public function addStatus($piStatus){
    $this->caStatus[] = $piStatus;
  }
  
  public function setOrderByDocumentInstanceId($order){
  	if(!$order)
  		$this->orderBy = '';
  	else
  		$this->orderBy = 'document_instance_id';
  }

  public function makeQuery(){
    $miUserId = ISMSLib::getCurrentUserId();
    $maFilters = array();
    // Pega s� os documentos que o usu�rio pode ver
    $maFilters[] = "(
      d.fkMainApprover = {$miUserId}
      OR d.fkAuthor = {$miUserId}
      OR (
        (
          dis.doc_instance_status = ".CONTEXT_STATE_DOC_PUBLISHED."
          OR dis.doc_instance_status = ".CONTEXT_STATE_DOC_OBSOLETE."
        )
        AND (
          dr.fkDocument IS NOT NULL
          OR da.fkDocument IS NOT NULL
        )
      )
      OR (
        da.fkDocument IS NOT NULL
        AND (
          dis.doc_instance_status = ".CONTEXT_STATE_DOC_APPROVAL."
          OR dis.doc_instance_status = ".CONTEXT_STATE_DOC_TO_BE_PUBLISHED."
        )
      )
    )";
    
    if($this->ciDocumentId){
      $maFilters[] = 'd.fkContext = '.$this->ciDocumentId;
    }
    
    if($this->ciInstanceId){
      $maFilters[] = 'dis.doc_instance_id = '.$this->ciInstanceId;
    }
    
    if(count($this->caStatus)){
      $maFilters[] = "dis.doc_instance_status IN (".implode(',',$this->caStatus).")";
    }
    
    $msWhere = '';
    if(count($maFilters)){
      $msWhere = " WHERE ".implode(' AND ', $maFilters);
    }
    
    $msOrderBy = '';
    if($this->orderBy != ''){
    	$msOrderBy = " ORDER BY $this->orderBy DESC ";
    }
    
    $this->csSQL = "SELECT DISTINCT
                      d.fkContext AS document_id,
                      dis.doc_instance_id AS document_instance_id,
                      d.fkAuthor AS document_author,
                      d.fkMainApprover AS document_main_approver,
                      CASE
                        WHEN d.fkMainApprover = {$miUserId} THEN d.bHasApproved
                        WHEN da.bHasApproved IS NOT NULL THEN da.bHasApproved
                        ELSE NULL
                      END AS user_has_approved,
                      d.sName AS document_name,
                      di.sFileName AS document_file_name,
                      d.dDateProduction AS document_date_production,
                      di.sPath AS document_file_path,
                      di.bIsLink AS document_is_link,
                      di.sLink AS document_link,
                      dis.doc_instance_status AS document_status,
                      CASE WHEN da.fkDocument IS NULL THEN 0 ELSE 1 END AS user_is_approver,
                      CASE WHEN dr.fkDocument IS NULL THEN 0 ELSE 1 END AS user_is_reader,
                      a.approvers_count AS count_approvers
                    FROM
                      view_pm_document_active d
                      LEFT JOIN view_pm_doc_instance_status dis ON (dis.document_id = d.fkContext)
                      LEFT JOIN pm_doc_approvers da ON (da.fkDocument = d.fkContext AND da.fkUser = {$miUserId})
                      LEFT JOIN pm_doc_readers dr ON (dr.fkDocument = d.fkContext AND dr.fkUser = {$miUserId} AND dr.bDenied=0)
                      LEFT JOIN pm_doc_instance di ON (di.fkContext = dis.doc_instance_id)
                      LEFT JOIN ( SELECT fkDocument, count(fkUser) AS approvers_count FROM pm_doc_approvers GROUP BY fkDocument ) a ON (a.fkDocument = d.fkContext)
                    {$msWhere} {$msOrderBy}";
  }

}

?>