<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QuerySubAreas.
 *
 * <p>Consulta para buscar as sub-�reas de uma �rea.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QuerySubAreas extends FWDDBQueryHandler {
  
  protected $ciAreaId = 0;
  
  protected $caSubAreas = array();
  
  public function __construct($poDB) {
    parent::__construct($poDB);

    $this->coDataSet->addFWDDBField(new FWDDBField('pkArea','area_id',DB_NUMBER));
  }
  
  public function setArea($piAreaId) {
    $this->ciAreaId = $piAreaId;
  }
  
  public function makeQuery() {
    $this->csSQL = "SELECT pkArea as area_id from ".FWDWebLib::getFunctionCall("get_sub_areas({$this->ciAreaId})")."
    									JOIN isms_context c ON(c.pkContext = pkArea and c.nState <> " . CONTEXT_STATE_DELETED . ")
		";
  }
  
  public function executeQuery() {
    parent::executeQuery();
    while ($this->coDataSet->fetch()) {
      $this->caSubAreas[] = $this->coDataSet->getFieldByAlias("area_id")->getValue();
    }
  }
  
  /**
   * Retorna as sub-�reas de uma �rea.
   * 
   * <p>M�todo para retornar as sub-�reas de uma �rea.</p>
   * @access public 
   * @return array Array de ids das sub-�reas
   */ 
  public function getSubAreas() {
    return $this->caSubAreas;
  }
}
?>