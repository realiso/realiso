<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryMyElements.
 *
 * <p>Consulta para popular o grid 'meus elementos'.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryMyElements extends FWDDBQueryHandler {

  protected $caMyElements = array();

	protected $caUserPermissions = array();

  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField("ordem","ordem", DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("count","count", DB_NUMBER));
 }
  
  public function setUserPermissions($paPermissions) {
    $this->caUserPermissions = $paPermissions;
  }

  public function makeQuery() {
    $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    
    $msResponsibleArea = $msResponsibleProcess = $msResponsibleAsset = $msResponsibleControl = '';  	
  	if (isset($this->caUserPermissions['list_area'],$this->caUserPermissions['list_process'],$this->caUserPermissions['list_asset'],$this->caUserPermissions['list_control'])) {
	    $msResponsibleArea = (($miUserId && !$this->caUserPermissions['list_area']) ? " a.fkResponsible = ".$miUserId." AND" : "");
	  	$msResponsibleProcess = (($miUserId && !$this->caUserPermissions['list_process']) ? " AND u.fkContext = ".$miUserId : "");
	    $msResponsibleAsset = (($miUserId && !$this->caUserPermissions['list_asset']) ? " AND u.fkContext = ".$miUserId : "");    
	    $msResponsibleControl = (($miUserId && !$this->caUserPermissions['list_control']) ? " c.fkResponsible = ".$miUserId." AND" : "");
  	}
    
    $this->csSQL = "
SELECT 'areas_without_processes' as ordem, count(*) as count
FROM rm_area a
JOIN isms_context ctx ON (a.fkContext = ctx.pkContext AND ctx.nState != ".CONTEXT_STATE_DELETED.")
WHERE $msResponsibleArea a.fkContext NOT IN ( 
	SELECT fkArea FROM rm_process JOIN isms_context c ON (fkContext = pkContext AND nState != ".CONTEXT_STATE_DELETED.")
)
  UNION
SELECT 'processes_without_assets' as ordem, count(*) as count
FROM rm_process p
JOIN isms_context ctx ON (p.fkContext = ctx.pkContext AND ctx.nState != ".CONTEXT_STATE_DELETED.")
JOIN isms_user u ON (p.fkResponsible = u.fkContext $msResponsibleProcess)
WHERE p.fkContext NOT IN (SELECT fkProcess FROM rm_process_asset)
  UNION
SELECT 'assets_without_risks' as ordem, count(*) as count
FROM rm_asset a
JOIN isms_context ctx ON (a.fkContext = ctx.pkContext AND ctx.nState != ".CONTEXT_STATE_DELETED.")
JOIN isms_user u ON (a.fkResponsible = u.fkContext $msResponsibleAsset)
WHERE a.fkContext NOT IN (SELECT fkAsset FROM rm_risk)
  UNION
SELECT 'controls_without_risks' as ordem, count(*) as count
FROM rm_control c
JOIN isms_context ctx ON (c.fkContext = ctx.pkContext AND ctx.nState != ".CONTEXT_STATE_DELETED.")
WHERE $msResponsibleControl c.fkContext NOT IN (
	SELECT rc.fkControl FROM rm_risk_control rc WHERE rc.fkRisk NOT IN 
	(SELECT fkContext FROM isms_context WHERE nType = ".CONTEXT_RISK." AND nState != ".CONTEXT_STATE_DELETED.")
)
";
  }

  public function executeQuery() {
    parent::executeQuery();
    $this->caMyElements = array();
    while ($this->coDataSet->fetch())
      $this->caMyElements[$this->coDataSet->getFieldByAlias("ordem")->getValue()] = $this->coDataSet->getFieldByAlias("count")->getValue();
  }

  public function getMyElements() {
    return $this->caMyElements;
  }
}
?>