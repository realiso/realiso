<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryProbabilityValueNames.
 *
 * <p>Consulta para buscar os nomes dos valores da probabilidade.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryProbabilityValueNames extends FWDDBQueryHandler {

  protected $csType;

  public function __construct($poDB) {
    parent::__construct($poDB); 

    $this->coDataSet->addFWDDBField(new FWDDBField(''     ,'value_id'  ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('','value_name',DB_STRING));
  }

  public function makeQuery(){
    if(isset($this->csType)){
      if($this->csType==RISKPARAMETER_RISK){
        $this->csSQL = "SELECT pkValueName as value_id, sRiskProbability as value_name "
                      ."FROM rm_parameter_value_name "
                      ."ORDER BY nValue";
      }else{
        $this->csSQL = "SELECT pkRCValueName as value_id, sRCProbability as value_name "
                      ."FROM rm_rc_parameter_value_name "
                      ."ORDER BY nValue";
      }
    }else{
      trigger_error('Type must be defined.',E_USER_ERROR);
    }
  }
  
  public function setType($psType){
    if($psType==RISKPARAMETER_RISK || $psType==RISKPARAMETER_RISKCONTROL){
      $this->csType = $psType;
    }else{
      trigger_error('Invalid type. Type must be risk or control.',E_USER_ERROR);
    }
  }

}
?>