<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryRegisterCanEdit.
 *
 * <p></p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryRegisterCanEdit extends FWDDBQueryHandler {

  private $ciRegisterId;

  public function __construct($poDB) {
    parent::__construct($poDB); 

    $this->coDataSet->addFWDDBField(new FWDDBField('r.fkContext','register_id',DB_NUMBER));
  }

  public function setRegisterId($piRegisterId){
    $this->ciRegisterId = $piRegisterId;
  }
  
  public function makeQuery(){
    $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    $this->csSQL = "SELECT r.fkContext AS register_id
                    FROM view_pm_register_active r
                    WHERE r.fkResponsible = $miUserId
                      AND r.fkContext = {$this->ciRegisterId}";
  }  
}
?>