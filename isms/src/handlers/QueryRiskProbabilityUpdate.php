<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryRiskProbabilityUpdate.
 *
 * <p>Update que calcula os novos limites de quantidade de incidentes para o
 * c�lculo autom�tico de probabilidade quando o n�mero de valores � modificado.
 * </p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryRiskProbabilityUpdate extends FWDDBQueryHandler {
  
  protected $ciNewValuesCount = 0;
  
 /**
  * Construtor da classe, armazena o nome do usu�rio que acessa o banco.
  * 
  * <p>Construtor da classe, armazena o nome do usu�rio que acessa o banco.</p>
  * @access public
  * @param FWDDB $poDB objeto de acesso a base de dados 
  */
  public function __construct($poDB=null){
    parent::__construct($poDB);
  }

 /**
  * Define o novo n�mero de valores de probabilidade.
  * 
  * <p>Define o novo n�mero de valores de probabilidade (deve ser 3 ou 5).</p>
  * @access public
  * @param integer $piNewValuesCount Novo n�mero de valores de probabilidade
  */
  public function setNewValuesCount($piNewValuesCount){
    $this->ciNewValuesCount = $piNewValuesCount;
  }

  public function makeQuery(){
    /*
      Exemplo de convers�o dos intervalos:
                          1    2    3    4
      Com 3 valores: 0 - 40 - 80 - infinito
      Com 5 valores: 0 - 20 - 40 - 60 - 80 - infinito
    */
    if($this->ciNewValuesCount==3){
      if(FWDWebLib::getDatabaseType()==DB_ORACLE){
        $this->csSQL = "BEGIN risk_prob_5_to_3; END;";
      }else{
        // Update das linhas do valor 1
        $this->csSQL = "UPDATE ci_risk_probability
                        SET nIncidentAmount = (
                          SELECT
                            rp2.nIncidentAmount
                          FROM
                            ci_risk_probability rp1
                            JOIN ci_risk_probability rp2 ON (rp1.fkRisk = rp2.fkRisk)
                            JOIN rm_parameter_value_name vn1 ON (vn1.pkValueName = rp1.fkValueName)
                            JOIN rm_parameter_value_name vn2 ON (vn2.pkValueName = rp2.fkValueName)
                          WHERE
                            vn1.nValue = 1
                            AND vn2.nValue = 2
                            AND rp1.fkRisk = ci_risk_probability.fkRisk
                        )
                        WHERE fkValueName = (
                          SELECT pkValueName FROM rm_parameter_value_name WHERE nValue = 1 
                        );";
        
        // Update das linhas do valor 2
        $this->csSQL.= "UPDATE ci_risk_probability
                        SET nIncidentAmount = (
                          SELECT
                            rp4.nIncidentAmount
                          FROM
                            ci_risk_probability rp2
                            JOIN ci_risk_probability rp4 ON (rp2.fkRisk = rp4.fkRisk)
                            JOIN rm_parameter_value_name vn2 ON (vn2.pkValueName = rp2.fkValueName)
                            JOIN rm_parameter_value_name vn4 ON (vn4.pkValueName = rp4.fkValueName)
                          WHERE
                            vn2.nValue = 2
                            AND vn4.nValue = 4
                            AND rp2.fkRisk = ci_risk_probability.fkRisk
                        )
                        WHERE fkValueName = (
                          SELECT pkValueName FROM rm_parameter_value_name WHERE nValue = 2 
                        );";
        
        // Delete das linhas dos valores 3 e 4
        $this->csSQL.= "DELETE FROM ci_risk_probability
                        WHERE EXISTS (
                          SELECT *
                          FROM rm_parameter_value_name vn
                          WHERE vn.pkValueName = fkValueName
                            AND vn.nValue >= 3
                        );";
      }
    }elseif($this->ciNewValuesCount==5){
      if(FWDWebLib::getDatabaseType()==DB_ORACLE){
        $this->csSQL = "BEGIN risk_prob_3_to_5; END;";
      }else{
        // Insert select pra criar linhas do valor 4
        $this->csSQL = "INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount)
                        SELECT rp2.fkrisk, vn4.pkValueName, rp2.nIncidentAmount
                        FROM
                          ci_risk_probability rp2
                          JOIN rm_parameter_value_name vn2 ON (vn2.pkValueName = rp2.fkValueName),
                          rm_parameter_value_name vn4
                        WHERE
                          vn2.nValue = 2
                          AND vn4.nValue = 4;";
        
        // Insert select pra criar linhas do valor 3
        $this->csSQL.= "INSERT INTO ci_risk_probability (fkrisk, fkvaluename, nincidentamount)
                        SELECT rp1.fkrisk, vn3.pkValueName, ROUND((rp1.nIncidentAmount + rp2.nIncidentAmount)/2,0)
                        FROM
                          ci_risk_probability rp1
                          JOIN ci_risk_probability rp2 ON (rp1.fkRisk = rp2.fkRisk)
                          JOIN rm_parameter_value_name vn1 ON (vn1.pkValueName = rp1.fkValueName)
                          JOIN rm_parameter_value_name vn2 ON (vn2.pkValueName = rp2.fkValueName),
                          rm_parameter_value_name vn3
                        WHERE
                          vn1.nValue = 1
                          AND vn2.nValue = 2
                          AND vn3.nValue = 3;";
        
        // Update das linhas do valor 2
        $this->csSQL.= "UPDATE ci_risk_probability
                        SET nIncidentAmount = (
                          SELECT
                            rp1.nIncidentAmount
                          FROM
                            ci_risk_probability rp1
                            JOIN ci_risk_probability rp2 ON (rp1.fkRisk = rp2.fkRisk)
                            JOIN rm_parameter_value_name vn1 ON (vn1.pkValueName = rp1.fkValueName)
                            JOIN rm_parameter_value_name vn2 ON (vn2.pkValueName = rp2.fkValueName)
                          WHERE
                            vn1.nValue = 1
                            AND vn2.nValue = 2
                            AND rp1.fkRisk = ci_risk_probability.fkRisk
                        )
                        WHERE fkValueName = (
                          SELECT pkValueName FROM rm_parameter_value_name WHERE nValue = 2
                        );";
        
        // Update das linhas do valor 1
        $this->csSQL.= "UPDATE ci_risk_probability
                        SET nIncidentAmount = ROUND(nIncidentAmount/2,0)
                        WHERE fkValueName = (
                          SELECT pkValueName FROM rm_parameter_value_name WHERE nValue = 1
                        );";
      }
    }else{
      trigger_error("The number of probability values must be 3 or 5.",E_USER_ERROR);
    }
  }

}

?>