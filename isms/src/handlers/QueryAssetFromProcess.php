<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryAssetFromProcess.
 *
 * <p>Consulta para buscar os ativos de um processo.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryAssetFromProcess extends FWDDBQueryHandler {
  
  private $ciProcessId = 0;
  
  private $caAssets = array();
  
  public function __construct($poDB) {
    parent::__construct($poDB);

    $this->coDataSet->addFWDDBField(new FWDDBField("fkAsset","asset_id", DB_NUMBER));
  }
  
  public function setProcessId($piProcessId) {
    $this->ciProcessId = $piProcessId;
  }
  
  public function makeQuery() {
    $this->csSQL = "SELECT fkAsset as asset_id FROM rm_process_asset WHERE fkProcess = {$this->ciProcessId}";
  }
  
  public function executeQuery() {
    parent::executeQuery();
    $this->caAssets = array();
    while ($this->coDataSet->fetch()) {
      $this->caAssets[] = $this->coDataSet->getFieldByAlias("asset_id")->getValue();
    }
  }
  
  /**
   * Retorna os ativos de um processo.
   * 
   * <p>M�todo para retornar os ativos de um processo.</p>
   * @access public 
   * @return array Array de ids dos ativos
   */ 
  public function getAssets() {
    return $this->caAssets;
  }
}
?>