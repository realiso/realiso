<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryRiskLimits.
 *
 * <p>Consulta que retorna a inst�ncia de limites de risco que realmente vale (a �nica aprovada).</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryRiskLimits extends FWDDBQueryHandler {

  public function __construct($poDB=null){
    parent::__construct($poDB); 
    $this->coDataSet->addFWDDBField(new FWDDBField('rl.fkContext','risk_limits_id'  ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('rl.nLow'     ,'risk_limits_low' ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('rl.nHigh'    ,'risk_limits_high',DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('rl.nMidLow'     ,'risk_limits_mid_low' ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('rl.nMidHigh'    ,'risk_limits_mid_high',DB_NUMBER));    
  }

  public function makeQuery(){
    $this->csSQL = "SELECT
                      rl.fkContext AS risk_limits_id,
                      rl.nLow AS risk_limits_low,
                      rl.nHigh AS risk_limits_high,
                      rl.nMidLow AS risk_limits_mid_low,
                      rl.nMidHigh AS risk_limits_mid_high
                    FROM
                      rm_risk_limits rl
                      JOIN isms_context c ON (rl.fkContext = c.pkContext)
                    WHERE c.nState = ".CONTEXT_STATE_APPROVED;
  }

}

?>