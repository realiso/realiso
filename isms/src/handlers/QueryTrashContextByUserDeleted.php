<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryTrashContextByUserDeleted.
 *
 * <p>Consulta para receber todos os contextos deletados que possuem usu�rios deletados.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryTrashContextByUserDeleted extends FWDDBQueryHandler {	
	
	protected $csUsersIds;
	
	public function __construct($poDB) {
		parent::__construct($poDB);	

		$this->coDataSet->addFWDDBField(new FWDDBField("","context_id", DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("","context_type", DB_NUMBER));
	}
	
	public function setUsersIds($paUsersIds) {
		$this->csUsersIds = implode(',',$paUsersIds);
	}
	
	public function makeQuery() {

		$this->csSQL = "
SELECT fkContext as context_id, ctx.nType as context_type FROM rm_area obj JOIN isms_context ctx ON (ctx.pkContext = obj.fkContext AND ctx.nState = 2705) WHERE obj.fkResponsible IN ($this->csUsersIds)
UNION
SELECT fkContext as context_id, ctx.nType as context_type FROM rm_process obj JOIN isms_context ctx ON (ctx.pkContext = obj.fkContext AND ctx.nState = 2705)  WHERE obj.fkResponsible IN ($this->csUsersIds)
UNION  
SELECT fkContext as context_id, ctx.nType as context_type FROM rm_asset obj JOIN isms_context ctx ON (ctx.pkContext = obj.fkContext AND ctx.nState = 2705) WHERE obj.fkResponsible IN ($this->csUsersIds) OR obj.fkSecurityResponsible IN ($this->csUsersIds)
UNION 
SELECT fkContext as context_id, ctx.nType as context_type FROM rm_control obj JOIN isms_context ctx ON (ctx.pkContext = obj.fkContext AND ctx.nState = 2705) WHERE obj.fkResponsible IN ($this->csUsersIds)
UNION
SELECT fkContext as context_id, ctx.nType as context_type FROM pm_document obj JOIN isms_context ctx ON (ctx.pkContext = obj.fkContext AND ctx.nState = 2705) WHERE obj.fkAuthor IN ($this->csUsersIds) OR obj.fkMainApprover IN ($this->csUsersIds) 
UNION 
SELECT fkContext as context_id, ctx.nType as context_type FROM pm_register obj JOIN isms_context ctx ON (ctx.pkContext = obj.fkContext AND ctx.nState = 2705) WHERE obj.fkResponsible IN ($this->csUsersIds)
UNION 
SELECT fkContext as context_id, ctx.nType as context_type FROM ci_incident obj JOIN isms_context ctx ON (ctx.pkContext = obj.fkContext AND ctx.nState = 2705) WHERE obj.fkResponsible IN ($this->csUsersIds)
UNION 
SELECT fkContext as context_id, ctx.nType as context_type FROM ci_action_plan obj JOIN isms_context ctx ON (ctx.pkContext = obj.fkContext AND ctx.nState = 2705) WHERE obj.fkResponsible IN ($this->csUsersIds)
UNION 
SELECT fkContext as context_id, ctx.nType as context_type FROM ci_nc obj JOIN isms_context ctx ON (ctx.pkContext = obj.fkContext AND ctx.nState = 2705) WHERE obj.fkResponsible IN ($this->csUsersIds)
";
	}
	
  public function getIds(){
		$this->makeQuery();
		if($this->executeQuery()){
			$maReturn = array();
			$moDataSet = $this->getDataset();
			while($moDataSet->fetch()){
				$maReturn[$moDataSet->getFieldByAlias('context_id')->getValue()] = $moDataSet->getFieldByAlias('context_type')->getValue();
			}
			return $maReturn;
		}
		else
			return array();
	} 
	
	
	 
}
?>