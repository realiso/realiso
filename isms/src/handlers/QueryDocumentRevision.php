<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryDocumentRevision.
 *
 * <p>Consulta para obter a vers�o sendo revisada de um documento.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryDocumentRevision extends FWDDBQueryHandler {

  protected $ciDocumentId = 0;

  public function __construct($poDB){
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField('di.fkContext',             'doc_instance_id',              DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('di.fkDocument',            'document_id',                  DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('di.nMajorVersion',         'doc_instance_major_version',   DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('di.nRevisionVersion',      'doc_instance_revision_version',DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('di.tRevisionJustification','doc_instance_revision_justif', DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('di.sPath',                 'doc_instance_path',            DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('di.tModifyComment',        'doc_instance_modify_comment',  DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('di.dBeginProduction',      'doc_instance_begin_production',DB_DATETIME));
    $this->coDataSet->addFWDDBField(new FWDDBField('di.dEndProduction',        'doc_instance_end_production',  DB_DATETIME));
    $this->coDataSet->addFWDDBField(new FWDDBField('di.sFileName',             'doc_instance_file_name',       DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('di.bIsLink',               'doc_instance_is_link',         DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('di.sLink',                 'doc_instance_link',            DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('di.dBeginApprover',        'doc_instance_begin_approver',  DB_DATETIME));
    $this->coDataSet->addFWDDBField(new FWDDBField('di.dBeginRevision',        'doc_instance_begin_revision',  DB_DATETIME));
 }
  
  public function setDocument($piDocumentId){
    $this->ciDocumentId = $piDocumentId;
  }

  public function makeQuery(){
    $this->csSQL = "
SELECT di.fkContext AS doc_instance_id,
       di.fkDocument AS document_id,
       di.nMajorVersion AS doc_instance_major_version,
       di.nRevisionVersion AS doc_instance_revision_version,
       di.tRevisionJustification AS doc_instance_revision_justif,
       di.sPath AS doc_instance_path,
       di.tModifyComment AS doc_instance_modify_comment,
       di.dBeginProduction AS doc_instance_begin_production,
       di.dEndProduction AS doc_instance_end_production,
       di.sFileName AS doc_instance_file_name,
       di.bIsLink AS doc_instance_is_link,
       di.sLink AS doc_instance_link,
       di.dBeginApprover AS doc_instance_begin_approver,
       di.dBeginRevision AS doc_instance_begin_revision
  FROM pm_doc_instance di
    JOIN pm_document d ON (di.fkDocument = d.fkContext)
    JOIN pm_doc_instance cv ON (cv.fkContext = d.fkCurrentVersion)
  WHERE di.fkDocument = {$this->ciDocumentId}
    AND di.fkContext != d.fkCurrentVersion
    AND di.nMajorVersion = cv.nMajorVersion
";
  }

}

?>