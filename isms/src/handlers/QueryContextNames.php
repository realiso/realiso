<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryContextNames.
 *
 * <p>Consulta para retornar os nomes dos contextos do sistema. � poss�vel filtrar
 * os contextos por tipo e estado.
 * deletados e de um dado tipo.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryContextNames extends FWDDBQueryHandler {
  
  protected $ciState = 0;
  
  protected $ciType = 0;
  
  protected $caContexts = array();
  
  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField("fkContext", "context_id",   DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("sName",     "context_name", DB_STRING));
 }
  
  public function setState($piState) {
    $this->ciState = $piState;
  }
  
  public function setType($piType) {
    $this->ciType = $piType;
  }
  
  public function makeQuery() {
    $msState = $this->ciState ? " context_state != ".$this->ciState." "  :  "";
    $msType = $this->ciType ? " context_type = ".$this->ciType." "  :  "";
    
    $msWhere = "";
    if ((strlen($msState)>0) && (strlen($msType)>0))
      $msWhere = "WHERE $msState AND $msType";
    elseif (strlen($msState)>0)
      $msWhere = "WHERE $msState";
    elseif (strlen($msType)>0)
      $msWhere = "WHERE $msType";
    
    $this->csSQL = "SELECT a.context_id as context_id, a.context_name as context_name
                    FROM context_names a
                    $msWhere";
  }
  
  public function executeQuery() {
    parent::executeQuery();
    while ($this->coDataSet->fetch()) {
      $miContextId = $this->coDataSet->getFieldByAlias("context_id")->getValue();
      $miContextName = $this->coDataSet->getFieldByAlias("context_name")->getValue();
      $this->caContexts[$miContextId] = $miContextName;
    }
  }
  
  /**
   * Retorna o array de contextos ativos de um dado tipo.
   * 
   * <p>M�todo que retorna o array de contextos ativos de um dado tipo.</p>
   * @access public 
   * @return array Array de contextos
   */ 
  public function getContexts() {
    return $this->caContexts;
  }
}
?>