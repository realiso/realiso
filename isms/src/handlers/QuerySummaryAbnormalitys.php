<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QuerySummaryAbnormalitys.
 *
 * <p>Consulta para popular o grid de sum�rio de anormalidades.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QuerySummaryAbnormalitys extends FWDDBQueryHandler {

  protected $caAbnormalitysSummary = array();

  protected $ciUserId = 0;

  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField("ordem","ordem", DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("count","count", DB_NUMBER));
 }

  public function setUserId($psUserId) {
    $this->ciUserId = $psUserId;
  }

  public function makeQuery() {
    $msCurrentTime = ISMSLib::getTimestampFormat(time());
    
    $moConfig = new ISMSConfig();
    $mbHasRevision = $moConfig->getConfig(GENERAL_REVISION_ENABLED);
    $mbHasTests = $moConfig->getConfig(GENERAL_TEST_ENABLED);    
    $msWhere = ' AND wt.nActivity >= 2201 AND wt.nActivity <= 2220 ';
    $maFilterTypes = array();
    /*retirado escopo e pol�tica das tarefas pendentes*/
    $maFilterTypes[]='2218';
    $maFilterTypes[]='2219';

    if(!$mbHasRevision){
      $maFilterTypes[]= '2208';
    }
    if(!$mbHasTests){
        $maFilterTypes[] = '2216';
    }

    if(count($maFilterTypes)){
      $msWhere .= "AND wt.nActivity NOT IN (".implode(',',$maFilterTypes).") ";
    }
    
    $this->csSQL = "
SELECT 'pendant_tasks' as ordem, count(*) as count
FROM wkf_task wt".
($this->ciUserId ? " JOIN isms_user u ON (wt.fkReceiver = u.fkContext AND u.fkContext = ".$this->ciUserId.")" : "")."
WHERE wt.bVisible = 1
$msWhere
  UNION
SELECT 'areas_without_processes' as ordem, count(*) as count
FROM rm_area a
JOIN isms_context ctx ON (a.fkContext = ctx.pkContext AND ctx.nState <> ".CONTEXT_STATE_DELETED.")".
($this->ciUserId ? " JOIN isms_user u ON (a.fkResponsible = u.fkContext AND u.fkContext = ".$this->ciUserId.")" : "")."
WHERE a.fkContext NOT IN (SELECT fkArea FROM rm_process)
  UNION
SELECT 'processes_without_assets' as ordem, count(*) as count
FROM rm_process p
JOIN isms_context ctx ON (p.fkContext = ctx.pkContext AND ctx.nState <> ".CONTEXT_STATE_DELETED.")".
($this->ciUserId ? " JOIN isms_user u ON (p.fkResponsible = u.fkContext AND u.fkContext = ".$this->ciUserId.")" : "")."
WHERE p.fkContext NOT IN (SELECT fkProcess FROM rm_process_asset)
  UNION
SELECT 'assets_without_risks' as ordem, count(*) as count
FROM rm_asset a
JOIN isms_context ctx ON (a.fkContext = ctx.pkContext AND ctx.nState <> ".CONTEXT_STATE_DELETED.")".
($this->ciUserId ? " JOIN isms_user u ON (a.fkResponsible = u.fkContext AND u.fkContext = ".$this->ciUserId.")" : "")."
WHERE a.fkContext NOT IN (SELECT fkAsset FROM rm_risk)
  UNION
SELECT 'non_parametrized_risks' as ordem, count(*) as count
FROM rm_risk r
JOIN isms_context ctx ON (r.fkContext = ctx.pkContext AND ctx.nState <> ".CONTEXT_STATE_DELETED.")".
($this->ciUserId ? "JOIN isms_context_date cd ON (r.fkContext = cd.fkContext AND cd.nAction = ".ACTION_EDIT." AND cd.nUserId = ".$this->ciUserId.")" : "")."
WHERE r.nValue = 0 OR r.nValue IS NULL
  UNION
"/*SELECT 'non_measured_controls' as ordem, count(*) as count
FROM isms_context ctx
JOIN rm_control c ON (ctx.pkContext = c.fkContext AND ctx.nState <> ".CONTEXT_STATE_DELETED.")".
($this->ciUserId ? " JOIN isms_user u ON (c.fkResponsible = u.fkContext AND u.fkContext = ".$this->ciUserId.")" : "")."
LEFT JOIN wkf_control_efficiency wce ON (c.fkContext = wce.fkControlEfficiency AND wce.dDateLimit <= ".$msCurrentTime.")
LEFT JOIN wkf_control_test wct ON (c.fkContext = wct.fkControlTest AND wct.dDateLimit <= ".$msCurrentTime.")
WHERE
wce.fkControlEfficiency IS NOT NULL OR wct.fkControlTest IS NOT NULL*/
."
SELECT 'controls_without_risks' as ordem, count(*) as count
FROM rm_control c
JOIN isms_context ctx ON (c.fkContext = ctx.pkContext AND ctx.nState <> ".CONTEXT_STATE_DELETED.")".
($this->ciUserId ? " JOIN isms_user u ON (c.fkResponsible = u.fkContext AND u.fkContext = ".$this->ciUserId.")" : "")."
WHERE c.fkContext NOT IN (SELECT fkControl FROM rm_risk_control)
";
  }

  public function executeQuery() {
    parent::executeQuery();
    $this->caAbnormalitysSummary = array();
    while ($this->coDataSet->fetch())
      $this->caAbnormalitysSummary[$this->coDataSet->getFieldByAlias("ordem")->getValue()] = $this->coDataSet->getFieldByAlias("count")->getValue();
  }

  public function getAbnormalitysSummary() {
    return $this->caAbnormalitysSummary;
  }
}
?>