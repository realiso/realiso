<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryDocumentReferences.
 *
 * <p>Consulta que busca as referÍncias de um documento.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryDocumentReferences extends FWDDBQueryHandler {
	
	private $ciDocumentId;
  private $caReferences;
  
  public function __construct($poDB,$piDocumentId){
    parent::__construct($poDB);
    $this->ciDocumentId = $piDocumentId;
    
    $this->coDataSet->addFWDDBField(new FWDDBField('','ref_id',DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('','ref_name',DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('','ref_link',DB_STRING));
  }

  public function makeQuery(){
    $this->csSQL = "SELECT
											r.pkReference as ref_id,
											r.sName as ref_name,
											r.sLink as ref_link
										FROM
											pm_doc_reference r
											JOIN view_pm_document_active doc ON (r.fkDocument = doc.fkContext AND doc.fkContext = {$this->ciDocumentId})
										ORDER BY
											r.sName";
  }
  
  public function executeQuery() {
    parent::executeQuery();
    while ($this->coDataSet->fetch()) {
      $this->caReferences[$this->coDataSet->getFieldByAlias("ref_id")->getValue()] = array($this->coDataSet->getFieldByAlias("ref_name")->getValue(), $this->coDataSet->getFieldByAlias("ref_link")->getValue());      
    }
  }
  
  public function getReferences() {
    return $this->caReferences;
  }
}
?>