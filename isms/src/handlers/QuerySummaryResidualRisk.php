<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QuerySummaryResidualRisk.
 *
 * <p>Consulta para popular o grid de sum�rio de risco residual.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QuerySummaryResidualRisk extends FWDDBQueryHandler {

  protected $caResidualRiskSummary = array();

  protected $ciUserId = 0;
  
  protected $caUserPermissions = array();

  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField("ordem","ordem", DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("count","count", DB_NUMBER));
 }

  public function setUserId($psUserId) {
    $this->ciUserId = $psUserId;
  }
  
  public function setUserPermissions($paPermissions) {
    $this->caUserPermissions = $paPermissions;
  }

  public function makeQuery() {
  	$msUserJoin_ar = $msUserJoin_p = $msUserJoin_a = $msUserJoin_r = '';  	
  	if (isset($this->caUserPermissions['list_area'],$this->caUserPermissions['list_process'],$this->caUserPermissions['list_asset'],$this->caUserPermissions['list_risk'])) {
	  	$msUserJoin_ar = (($this->ciUserId && !$this->caUserPermissions['list_area']) ? " WHERE a.fkResponsible = ".$this->ciUserId : "");
	  	$msUserJoin_p = (($this->ciUserId && !$this->caUserPermissions['list_process']) ? " WHERE p.fkResponsible = ".$this->ciUserId : "");
	    $msUserJoin_a = (($this->ciUserId && !$this->caUserPermissions['list_asset']) ? " WHERE a.fkResponsible = ".$this->ciUserId : "");    
	    $msUserJoin_r = (($this->ciUserId && !$this->caUserPermissions['list_risk']) ? " JOIN rm_asset a ON (r.fkAsset = a.fkContext AND a.fkResponsible=".$this->ciUserId.")" : "");
  	}
    
  	$riskLevel = ISMSLib::getConfigById(RISK_LEVEL);
    if($riskLevel == 3){
  	
	    $this->csSQL = "
	SELECT 'area_high' as ordem, count(*) as count
	FROM rm_area a
	JOIN isms_context c ON (a.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED." AND a.nValue >= ".ISMSLib::getConfigById(RISK_HIGH).")
	$msUserJoin_ar
	  UNION
	SELECT 'area_mid' as ordem, count(*) as count
	FROM rm_area a
	JOIN isms_context c ON (a.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED." AND a.nValue > ".ISMSLib::getConfigById(RISK_LOW)." AND a.nValue < ".ISMSLib::getConfigById(RISK_HIGH).")
	$msUserJoin_ar
	  UNION
	SELECT 'area_low' as ordem, count(*) as count
	FROM rm_area a
	JOIN isms_context c ON (a.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED." AND (a.nValue <= ".ISMSLib::getConfigById(RISK_LOW)." AND a.nValue > 0) )
	$msUserJoin_ar
	  UNION
	SELECT 'area_np' as ordem, count(*) as count
	FROM rm_area a
	JOIN isms_context c ON (a.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED." AND (a.nValue = 0 OR a.nValue IS NULL) )
	$msUserJoin_ar
	  UNION
	SELECT 'area_total' as ordem, count(*) as count
	FROM rm_area a
	JOIN isms_context c ON (a.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED.")
	$msUserJoin_ar
	
	  UNION
	
	SELECT 'process_high' as ordem, count(*) as count
	FROM rm_process p
	JOIN isms_context c ON (p.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED." AND p.nValue >= ".ISMSLib::getConfigById(RISK_HIGH).")
	$msUserJoin_p
	  UNION
	SELECT 'process_mid' as ordem, count(*) as count
	FROM rm_process p
	JOIN isms_context c ON (p.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED." AND p.nValue > ".ISMSLib::getConfigById(RISK_LOW)." AND p.nValue < ".ISMSLib::getConfigById(RISK_HIGH).")
	$msUserJoin_p
	  UNION
	SELECT 'process_low' as ordem, count(*) as count
	FROM rm_process p
	JOIN isms_context c ON (p.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED." AND (p.nValue <= ".ISMSLib::getConfigById(RISK_LOW)." AND p.nValue > 0) )
	$msUserJoin_p
	  UNION
	SELECT 'process_np' as ordem, count(*) as count
	FROM rm_process p
	JOIN isms_context c ON (p.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED." AND (p.nValue = 0 OR p.nValue IS NULL) )
	$msUserJoin_p
	  UNION
	SELECT 'process_total' as ordem, count(*) as count
	FROM rm_process p
	JOIN isms_context c ON (p.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED.")
	$msUserJoin_p
	
	  UNION
	
	SELECT 'asset_high' as ordem, count(*) as count
	FROM rm_asset a
	JOIN isms_context c ON (a.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED." AND a.nValue >= ".ISMSLib::getConfigById(RISK_HIGH).")
	$msUserJoin_a
	  UNION
	SELECT 'asset_mid' as ordem, count(*) as count
	FROM rm_asset a
	JOIN isms_context c ON (a.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED." AND a.nValue > ".ISMSLib::getConfigById(RISK_LOW)." AND a.nValue < ".ISMSLib::getConfigById(RISK_HIGH).")
	$msUserJoin_a
	  UNION
	SELECT 'asset_low' as ordem, count(*) as count
	FROM rm_asset a
	JOIN isms_context c ON (a.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED." AND (a.nValue <= ".ISMSLib::getConfigById(RISK_LOW)." AND a.nValue > 0 ) )
	$msUserJoin_a
	  UNION
	SELECT 'asset_np' as ordem, count(*) as count
	FROM rm_asset a
	JOIN isms_context c ON (a.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED." AND (a.nValue = 0 OR a.nValue IS NULL) )
	$msUserJoin_a
	  UNION
	SELECT 'asset_total' as ordem, count(*) as count
	FROM rm_asset a
	JOIN isms_context c ON (a.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED.")
	$msUserJoin_a
						
		UNION
	
	SELECT 'risk_high' as ordem, count(*) as count
	FROM rm_risk r
	JOIN isms_context c ON (r.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED." AND r.nValueResidual >= ".ISMSLib::getConfigById(RISK_HIGH).")
	$msUserJoin_r
	  UNION
	SELECT 'risk_mid' as ordem, count(*) as count
	FROM rm_risk r
	JOIN isms_context c ON (r.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED." AND r.nValueResidual > ".ISMSLib::getConfigById(RISK_LOW)." AND r.nValueResidual < ".ISMSLib::getConfigById(RISK_HIGH).")
	$msUserJoin_r
	  UNION
	SELECT 'risk_low' as ordem, count(*) as count
	FROM rm_risk r
	JOIN isms_context c ON (r.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED." AND (r.nValueResidual <= ".ISMSLib::getConfigById(RISK_LOW)." AND r.nValueResidual > 0 ) )
	$msUserJoin_r
	  UNION
	SELECT 'risk_np' as ordem, count(*) as count
	FROM rm_risk r
	JOIN isms_context c ON (r.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED." AND (r.nValueResidual = 0 OR r.nValueResidual IS NULL) )
	$msUserJoin_r
	  UNION
	SELECT 'risk_total' as ordem, count(*) as count
	FROM rm_risk r
	JOIN isms_context c ON (r.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED.")
	$msUserJoin_r
	";
	
	
    } else if($riskLevel == 5) {

    	
	    $this->csSQL = "
	SELECT 'area_high' as ordem, count(*) as count
	FROM rm_area a
	JOIN isms_context c ON (a.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED." AND a.nValue > ".ISMSLib::getConfigById(RISK_HIGH).")
	$msUserJoin_ar
	  UNION
	  
	SELECT 'area_mid_high' as ordem, count(*) as count
	FROM rm_area a
	JOIN isms_context c ON (a.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED." AND a.nValue > ".ISMSLib::getConfigById(RISK_MID_HIGH)." AND a.nValue <= ".ISMSLib::getConfigById(RISK_HIGH).")
	$msUserJoin_ar
	  UNION	  
	  
	  
	SELECT 'area_mid' as ordem, count(*) as count
	FROM rm_area a
	JOIN isms_context c ON (a.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED." AND a.nValue > ".ISMSLib::getConfigById(RISK_MID_LOW)." AND a.nValue <= ".ISMSLib::getConfigById(RISK_MID_HIGH).")
	$msUserJoin_ar
	  UNION
	  
	SELECT 'area_mid_low' as ordem, count(*) as count
	FROM rm_area a
	JOIN isms_context c ON (a.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED." AND (a.nValue > ".ISMSLib::getConfigById(RISK_LOW) . " AND a.nValue <= " . ISMSLib::getConfigById(RISK_MID_LOW) . ") )
	$msUserJoin_ar
	  UNION	  
	  
	  
	SELECT 'area_low' as ordem, count(*) as count
	FROM rm_area a
	JOIN isms_context c ON (a.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED." AND (a.nValue <= ".ISMSLib::getConfigById(RISK_LOW)." AND a.nValue > 0) )
	$msUserJoin_ar
	  UNION
	SELECT 'area_np' as ordem, count(*) as count
	FROM rm_area a
	JOIN isms_context c ON (a.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED." AND (a.nValue = 0 OR a.nValue IS NULL) )
	$msUserJoin_ar
	  UNION
	SELECT 'area_total' as ordem, count(*) as count
	FROM rm_area a
	JOIN isms_context c ON (a.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED.")
	$msUserJoin_ar
	
	  UNION
	
	SELECT 'process_high' as ordem, count(*) as count
	FROM rm_process p
	JOIN isms_context c ON (p.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED." AND p.nValue > ".ISMSLib::getConfigById(RISK_HIGH).")
	$msUserJoin_p
	  UNION
	SELECT 'process_mid_high' as ordem, count(*) as count
	FROM rm_process p
	JOIN isms_context c ON (p.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED." AND p.nValue > ".ISMSLib::getConfigById(RISK_MID_HIGH)." AND p.nValue <= ".ISMSLib::getConfigById(RISK_HIGH).")
	$msUserJoin_p
	  UNION	  
	  
	SELECT 'process_mid' as ordem, count(*) as count
	FROM rm_process p
	JOIN isms_context c ON (p.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED." AND p.nValue > ".ISMSLib::getConfigById(RISK_MID_LOW)." AND p.nValue <= ".ISMSLib::getConfigById(RISK_MID_HIGH).")
	$msUserJoin_p
	  UNION
	  
	SELECT 'process_mid_low' as ordem, count(*) as count
	FROM rm_process p
	JOIN isms_context c ON (p.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED." AND (p.nValue > ".ISMSLib::getConfigById(RISK_LOW)." AND p.nValue <= ". ISMSLib::getConfigById(RISK_MID_LOW) . ") )
	$msUserJoin_p
	  UNION	  
	  
	SELECT 'process_low' as ordem, count(*) as count
	FROM rm_process p
	JOIN isms_context c ON (p.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED." AND (p.nValue <= ".ISMSLib::getConfigById(RISK_LOW)." AND p.nValue > 0) )
	$msUserJoin_p
	  UNION
	SELECT 'process_np' as ordem, count(*) as count
	FROM rm_process p
	JOIN isms_context c ON (p.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED." AND (p.nValue = 0 OR p.nValue IS NULL) )
	$msUserJoin_p
	  UNION
	SELECT 'process_total' as ordem, count(*) as count
	FROM rm_process p
	JOIN isms_context c ON (p.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED.")
	$msUserJoin_p
	
	  UNION
	
	SELECT 'asset_high' as ordem, count(*) as count
	FROM rm_asset a
	JOIN isms_context c ON (a.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED." AND a.nValue > ".ISMSLib::getConfigById(RISK_HIGH).")
	$msUserJoin_a
	  UNION
	  
	SELECT 'asset_mid_high' as ordem, count(*) as count
	FROM rm_asset a
	JOIN isms_context c ON (a.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED." AND a.nValue > ".ISMSLib::getConfigById(RISK_MID_HIGH)." AND a.nValue <= ".ISMSLib::getConfigById(RISK_HIGH).")
	$msUserJoin_a
	  UNION
	  	  
	SELECT 'asset_mid' as ordem, count(*) as count
	FROM rm_asset a
	JOIN isms_context c ON (a.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED." AND a.nValue > ".ISMSLib::getConfigById(RISK_MID_LOW)." AND a.nValue <= ".ISMSLib::getConfigById(RISK_MID_HIGH).")
	$msUserJoin_a
	  UNION
	  
	SELECT 'asset_mid_low' as ordem, count(*) as count
	FROM rm_asset a
	JOIN isms_context c ON (a.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED." AND (a.nValue > ".ISMSLib::getConfigById(RISK_LOW)." AND a.nValue <= ".ISMSLib::getConfigById(RISK_MID_LOW). ") )
	$msUserJoin_a
	  UNION	  
	  
	SELECT 'asset_low' as ordem, count(*) as count
	FROM rm_asset a
	JOIN isms_context c ON (a.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED." AND (a.nValue <= ".ISMSLib::getConfigById(RISK_LOW)." AND a.nValue > 0 ) )
	$msUserJoin_a
	  UNION
	SELECT 'asset_np' as ordem, count(*) as count
	FROM rm_asset a
	JOIN isms_context c ON (a.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED." AND (a.nValue = 0 OR a.nValue IS NULL) )
	$msUserJoin_a
	  UNION
	SELECT 'asset_total' as ordem, count(*) as count
	FROM rm_asset a
	JOIN isms_context c ON (a.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED.")
	$msUserJoin_a
						
		UNION
	
	SELECT 'risk_high' as ordem, count(*) as count
	FROM rm_risk r
	JOIN isms_context c ON (r.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED." AND r.nValueResidual > ".ISMSLib::getConfigById(RISK_HIGH).")
	$msUserJoin_r
	  UNION
	  
	SELECT 'risk_mid_high' as ordem, count(*) as count
	FROM rm_risk r
	JOIN isms_context c ON (r.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED." AND r.nValueResidual > ".ISMSLib::getConfigById(RISK_MID_HIGH)." AND r.nValueResidual <= ".ISMSLib::getConfigById(RISK_HIGH).")
	$msUserJoin_r
	  UNION
	  	  
	SELECT 'risk_mid' as ordem, count(*) as count
	FROM rm_risk r
	JOIN isms_context c ON (r.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED." AND r.nValueResidual > ".ISMSLib::getConfigById(RISK_MID_LOW)." AND r.nValueResidual <= ".ISMSLib::getConfigById(RISK_MID_HIGH).")
	$msUserJoin_r
	  UNION
	  
	SELECT 'risk_mid_low' as ordem, count(*) as count
	FROM rm_risk r
	JOIN isms_context c ON (r.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED." AND (r.nValueResidual > ".ISMSLib::getConfigById(RISK_LOW)." AND r.nValueResidual <= " . ISMSLib::getConfigById(RISK_MID_LOW) . ") )
	$msUserJoin_r
	  UNION
	  
	SELECT 'risk_low' as ordem, count(*) as count
	FROM rm_risk r
	JOIN isms_context c ON (r.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED." AND (r.nValueResidual <= ".ISMSLib::getConfigById(RISK_LOW)." AND r.nValueResidual > 0 ) )
	$msUserJoin_r
	  UNION
	SELECT 'risk_np' as ordem, count(*) as count
	FROM rm_risk r
	JOIN isms_context c ON (r.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED." AND (r.nValueResidual = 0 OR r.nValueResidual IS NULL) )
	$msUserJoin_r
	  UNION
	SELECT 'risk_total' as ordem, count(*) as count
	FROM rm_risk r
	JOIN isms_context c ON (r.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED.")
	$msUserJoin_r
	";
    	
    }
	
  }

  public function executeQuery() {
    parent::executeQuery();
    $this->caResidualRiskSummary = array();
    while ($this->coDataSet->fetch())
      $this->caResidualRiskSummary[$this->coDataSet->getFieldByAlias("ordem")->getValue()] = $this->coDataSet->getFieldByAlias("count")->getValue();
  }

  public function getResidualRiskSummary() {
    return $this->caResidualRiskSummary;
  }
}
?>