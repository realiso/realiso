<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryTrashUsersByProfileDeleted.
 *
 * <p>Obtem todos os usu�rios que est�o relacionados ao profile que se quer deletar.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryTrashUsersByProfileDeleted extends FWDDBQueryHandler {

  private $caProfileIds = array();
  
  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField("", "user_id", DB_NUMBER));
 }

  public function setProfileIds($paProfileIds) {
    $this->caProfileIds = $paProfileIds;
  }

  public function makeQuery() {
    $this->csSQL = "
      SELECT u.fkContext as user_id
         FROM isms_user u
         JOIN isms_profile p ON (p.fkContext = u.fkProfile)
         WHERE p.fkContext IN (". implode(',',$this->caProfileIds) .")
  ";
}

  public function getValue() {
    $this->makeQuery();
    $this->executeQuery();
    $maUserIds = array();
    while($this->coDataSet->fetch()){
      $maUserIds[] = $this->coDataSet->getFieldByAlias("user_id")->getValue();
    }
    
    return $maUserIds;
  }
}
?>