<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryUserHasTasksOrAlerts.
 *
 * <p>Consulta para indicar se um usu�rio possui tarefas ou alertas pendentes.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryUserHasTasksOrAlerts extends FWDDBQueryHandler {

  private $ciUserId = 0;
  
  private $caContexts = array();
  
  private $cbHasTasksOrAlerts = false;
  
  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField("ordem","ordem", DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("count","count", DB_NUMBER));
 }

  public function setUserId($psUserId) {
    $this->ciUserId = $psUserId;
  }

  public function makeQuery() {
    $this->csSQL = "
SELECT 'task' as ordem, count(*) as count
FROM wkf_task t
JOIN isms_context ctx ON (t.fkContext = ctx.pkContext AND ctx.nState != ".CONTEXT_STATE_DELETED.")
WHERE t.fkCreator = ".$this->ciUserId." OR t.fkReceiver = ".$this->ciUserId."

UNION

SELECT 'alert' as ordem, count(*) as count
FROM wkf_alert a
JOIN isms_context ctx ON (a.fkContext = ctx.pkContext AND ctx.nState != ".CONTEXT_STATE_DELETED.")
WHERE a.fkCreator = ".$this->ciUserId."
";
  }

  public function executeQuery() {
    parent::executeQuery();
    
    $this->caContexts = array();
    while ($this->coDataSet->fetch()) {
      $this->caContexts[$this->coDataSet->getFieldByAlias("ordem")->getValue()] = $this->coDataSet->getFieldByAlias("count")->getValue();
    }
    
    if (array_sum($this->caContexts)>0)
      $this->cbHasTasksOrAlerts=true;
    else
      $this->cbHasTasksOrAlerts=false;
  }

  public function getHasTasksOrAlerts() {
     return $this->cbHasTasksOrAlerts;
  }
}
?>