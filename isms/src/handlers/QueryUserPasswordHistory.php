<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryUserPasswordHistory.
 *
 * <p>Consulta para retornar histórico de senhas de usuários.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryUserPasswordHistory extends FWDDBQueryHandler {

  private $ciUserId = 0;
  private $caPasswordHistory=array();
  
  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField("ph.fkUser","user_id", DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("ph.dDatePasswordChanged","user_date_password_changed", DB_DATETIME));
 }

  public function setUserId($psUserId) {
    $this->ciUserId = $psUserId;
  }

  public function makeQuery() {
    $maFilters = array();
    if ($this->ciUserId) {
      $maFilters[] = "ph.fkUser='{$this->ciUserId}'";
    }
    $msWhere = count($maFilters)?" WHERE ".implode(" AND ",$maFilters):"";
    $this->csSQL = "
                    SELECT ph.fkuser as user_id,
                           ph.ddatepasswordchanged as user_date_password_changed
                    FROM isms_user_password_history ph
                    $msWhere
                    ORDER BY user_date_password_changed DESC
                   ";
  }

  public function executeQuery() {
    parent::executeQuery();
    
    while ($this->coDataSet->fetch()) {
      $this->caPasswordHistory[] = array($this->coDataSet->getFieldByAlias('user_id')->getValue(),$this->coDataSet->getFieldByAlias('user_date_password_changed')->getValue());
    }
  }

  public function getPasswordHistory() {
     return $this->caPasswordHistory;
  }
}
?>