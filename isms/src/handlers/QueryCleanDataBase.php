<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryCleanDataBase.
 *
 * <p>Update que atualiza os contextos para delete. Delete que remove as tarefas e alertas.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryCleanDataBase extends FWDDBQueryHandler {

    public function makeQuery() {
        $this->csSQL = "DELETE FROM wkf_alert WHERE ddate <= now(); ";
        $this->csSQL.="DELETE FROM wkf_task WHERE ddatecreated <= now(); ";
        $this->csSQL.="UPDATE isms_context SET nstate = 2705 WHERE ntype IN (2801,2802,2803,2804,2805,2823,2823,2826,2827,2828,2829,2830,2831,2832,2836,2837) OR (ntype = 2816 AND pkcontext > 13);";
    }
}
?>
