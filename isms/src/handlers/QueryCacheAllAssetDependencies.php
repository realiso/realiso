<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryCacheAllAssetDependencies.
 *
 * <p>Concentra todas as dependencias de todos os ativos em uma tabela cache.</p>
 * @package ISMS
 * @subpackage handlers
 */

include_once "select/QuerySelectAssetAsset.php";

class QueryCacheAllAssetDependencies extends FWDDBQueryHandler {

  public function makeQuery() {}  
  
  public function cache(){
    $query = new FWDDBDataSet(FWDWebLib::getConnection());
    $query->setQuery("truncate table all_asset_dependencies_cache");
    $query->execute();

    $query = new FWDDBDataSet(FWDWebLib::getConnection());
    $query->setQuery("select fkcontext as id from view_rm_asset_active");
    $query->addFWDDBField(new FWDDBField('id','id'  ,DB_NUMBER));
    $query->execute();

    QuerySelectAssetAsset::load();
    
    $queryInsert = new FWDDBDataSet(FWDWebLib::getConnection());

    while($query->fetch()){
      $assetId = $query->getFieldByAlias("id")->getValue();
      $assetDependencies = QuerySelectAssetAsset::getAssetDependencies($assetId);

      foreach($assetDependencies as $dependencyId){
        $queryInsert->setQuery("insert into all_asset_dependencies_cache(asset, dependency) values ($assetId,$dependencyId);");
        $queryInsert->execute();
      }
    }
  } 

}

?>