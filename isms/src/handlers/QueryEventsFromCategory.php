<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryEventsFromCategory.
 *
 * <p>Consulta para buscar os eventos de uma categoria (e de suas sub-categorias,
 * se existirem).</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryEventsFromCategory extends FWDDBQueryHandler {
  
  protected $ciCategoryId = 0;
  
  protected $caEvents = array();
  
  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField('pkEvent','event_id',DB_NUMBER));
  }
  
  public function setCategory($piCategoryId) {
    $this->ciCategoryId = $piCategoryId;
  }
  
  public function makeQuery() {
    $this->csSQL = "SELECT pkEvent as event_id from ".FWDWebLib::getFunctionCall("get_events_from_category({$this->ciCategoryId})")."
								    JOIN isms_context c ON(c.pkContext = pkEvent and c.nState <> 2705)";
 }
  
  public function executeQuery() {
    parent::executeQuery();
    while ($this->coDataSet->fetch()) {
      $this->caEvents[] = $this->coDataSet->getFieldByAlias("event_id")->getValue();
    }
  }
  
  /**
   * Retorna os eventos de uma categoria (e de suas sub-categorias, se existirem).
   * 
   * <p>M�todo para retornar os eventos de uma categoria (e de suas sub-categorias,
   * se existirem).</p>
   * @access public 
   * @return array Array de ids de eventos
   */ 
  public function getEvents() {
    return $this->caEvents;
  }
}
?>