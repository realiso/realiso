<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryTaskByContext
 *
 * <p></p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryTaskByContext extends FWDDBQueryHandler {

	private $contextId;

	public function __construct($poDB=null){
		parent::__construct($poDB);

		$this->coDataSet->addFWDDBField(new FWDDBField("",	"task_id",               DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("",	"task_context_id",       DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("",	"task_receiver_id",      DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("",	"task_creator_id",       DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("",	"task_activity",         DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("",	"task_is_visible",       DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("",	"task_email_sent",       DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("",	"task_date_created",     DB_DATETIME));
		$this->coDataSet->addFWDDBField(new FWDDBField("",	"task_date_accomplished",DB_DATETIME));
	}

	public function setContextId($value){
		$this->contextId = $value;
	}

	public function getReceiverId(){
		return $this->getFieldValue('task_receiver_id');
	}
	
	public function getTaskId(){
		return $this->getFieldValue('task_id');
	}

	public function makeQuery(){
		$this->csSQL = "SELECT 	pktask AS task_id,
			    			fkcontext AS task_context_id, 
			    			fkreceiver AS task_receiver_id, 
			    			fkcreator AS task_creator_id, 
			    			nactivity AS task_activity, 
			    			bvisible AS task_is_visible, 
			    			bemailsent AS task_email_sent, 
			    			ddatecreated AS task_date_created, 
			    			ddateaccomplished AS task_date_accomplished 
			    	FROM wkf_task WHERE fkcontext = {$this->contextId}";

		$this->executeQuery();
		$this->fetch();
	}
}

?>