<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryRegisterCanRead.
 *
 * <p></p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryRegisterCanRead extends FWDDBQueryHandler {

  private $ciRegisterId;

  public function __construct($poDB) {
    parent::__construct($poDB); 

    $this->coDataSet->addFWDDBField(new FWDDBField('r.fkContext','reg_id',DB_NUMBER));
  }

  public function setRegisterId($piRegisterId){
    $this->ciRegisterId = $piRegisterId;
  }
  
  public function makeQuery(){
    $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    $this->csSQL = "SELECT r.fkContext AS reg_id
                    FROM view_pm_register_active r
                    WHERE r.fkContext = {$this->ciRegisterId}
                      AND (
                        EXISTS (
                          "./* documentos que o usu�rio pode ler */"
                          SELECT d.fkContext AS readable_doc_id, d.sName AS doc_name
                          FROM
                            pm_document d
                            JOIN isms_context c ON (c.pkContext = d.fkContext AND c.nState = ".CONTEXT_STATE_DOC_APPROVED.")
                            JOIN pm_doc_registers dr ON (d.fkContext = dr.fkDocument AND dr.fkRegister = r.fkContext)
                          WHERE (
                            d.fkMainApprover = $miUserId
                            OR (
                              d.dDateProduction <= '".date('Y-m-d')."'
                              AND (
                                d.fkAuthor = $miUserId
                                OR exists(
                                  SELECT *
                                  FROM pm_doc_approvers da
                                  WHERE da.fkDocument = d.fkContext
                                    AND da.fkUser = $miUserId
                                )
                                OR exists(
                                  SELECT *
                                  FROM pm_doc_readers dr
                                  WHERE dr.fkDocument = d.fkContext
                                    AND dr.fkUser = $miUserId
                                    AND dr.bDenied = 0
                                )
                              )
                            )
                          )
                          AND NOT EXISTS (
                            SELECT *
                            FROM pm_register_readers rr
                            WHERE rr.fkRegister = r.fkContext
                              AND rr.fkUser = $miUserId
                              AND rr.bDenied = 1
                          )
                        )
                        OR EXISTS (
                          "./* usu�rios na tabela de leitores de registro */"
                          SELECT rr.fkRegister
                          FROM 
                            pm_register_readers rr
                          WHERE 
                            rr.fkRegister = r.fkContext
                            AND rr.fkUser = $miUserId
                            AND rr.bDenied = 0
                        )
                      )";
  }

}

?>