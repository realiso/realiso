<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryTop5IncidentsPerAsset.
 *
 * <p>Consulta para popular o grid de top 5 ativos com mais incidentes.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryTop5IncidentsPerAsset extends FWDDBQueryHandler {

  protected $caValues = array();

  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField("","asset_id", DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("","asset_name", DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("","asset_inc_count", DB_NUMBER));
  }

  public function makeQuery() {
    $this->csSQL = "SELECT
											a.fkContext as asset_id,
											a.sName as asset_name,
											count(DISTINCT ir.fkIncident) as asset_inc_count
										FROM
											view_rm_asset_active a
											JOIN view_rm_risk_active r ON (a.fkContext = r.fkAsset)
											JOIN view_ci_incident_risk_active ir ON (r.fkContext = ir.fkRisk)
										GROUP BY
											a.fkContext, a.sName
										ORDER BY
											asset_inc_count DESC";
  }

  public function executeQuery() {
    parent::executeQuery(5);
    $this->caValues = array();
    while ($this->coDataSet->fetch())
      $this->caValues[$this->coDataSet->getFieldByAlias("asset_id")->getValue()] = array($this->coDataSet->getFieldByAlias("asset_name")->getValue(), $this->coDataSet->getFieldByAlias("asset_inc_count")->getValue());
  }

  public function getValues() {
  	$this->makeQuery();
    $this->executeQuery();
    return $this->caValues;
  }
}
?>