<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryRCRiskUpdate.
 *
 * <p>Update que calcula o novo valor do risco, dado um id de uma associa��o
 * risco-controle.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryRCRiskUpdate extends FWDDBQueryHandler {
  
  protected $ciRiskId = 0;
  
  /**
   * Construtor da classe, armazena o nome do usu�rio que acessa o banco.
   * 
   * <p>Construtor da classe, armazena o nome do usu�rio que acessa o banco.</p>
   * @access public
   * @param FWDDB $poDB objeto de acesso a base de dados 
   */   
  public function __construct($poDB) {
    parent::__construct($poDB);

  }
  
 /**
  * Define o ID do risco a ser atualizado. 
  * 
  * <p>Define o ID do risco a ser atualizado, nome do m�todo � gen�rico para compatibilidade com a atualiza��o de ativo e risco x controle.</p>
  * @access public
  * @param integer $piRiskControlId id da associa��o risco-controle
  */   
  public function setElementId($piRiskControlId) {
    $moRiskControl = new RMRiskControl();
    $moRiskControl->fetchById($piRiskControlId);
    $this->ciRiskId = $moRiskControl->getFieldValue("rc_risk_id");
  }
  
  public function makeQuery() {
  	if (FWDWebLib::getDatabaseType() == DB_ORACLE)
    	$this->csSQL = "UPDATE rm_risk set nValue=".FWDWebLib::getFunctionUserDB()."get_aut_risk_value(fkContext),nValueResidual=".FWDWebLib::getFunctionUserDB()."get_aut_risk_value_residual(fkContext) where fkContext={$this->ciRiskId}";
    else
    	$this->csSQL = "UPDATE rm_risk set nValue=".FWDWebLib::getFunctionUserDB()."get_risk_value(fkContext),nValueResidual=".FWDWebLib::getFunctionUserDB()."get_risk_value_residual(fkContext) where fkContext={$this->ciRiskId}";
  }
  
}
?>