<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryTop10RiskByProcess.
 *
 * <p>Consulta para popular o grid do sum�rio Top 10 Riscos X Processos.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryTop10RiskByProcess extends FWDDBQueryHandler {

  protected $caValues = array();
  
  public function __construct($poDB=null){
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField('', 'process_name', DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('', 'asset_name', DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('', 'risk_name', DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('', 'risk_id', DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('', 'process_weight', DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('', 'risk_residual_value', DB_NUMBER));
  }

  public function makeQuery(){
  	$this->csSQL = "
		SELECT
			min(p.sname) AS process_name, 
			a.sname AS asset_name, 
			r.sname AS risk_name, 
			r.fkcontext AS risk_id,
			min(cc.nweight) AS process_weight,
			max(r.nvalueresidual) AS risk_residual_value
		FROM view_rm_risk_active r 
		LEFT JOIN view_rm_process_asset_active pa ON (pa.fkasset = r.fkasset)
		LEFT JOIN view_rm_asset_active a ON (a.fkcontext = pa.fkasset)
		LEFT JOIN view_rm_process_active p ON (p.fkcontext = pa.fkprocess)
		LEFT JOIN isms_context_classification cc ON (cc.pkclassification = p.fkpriority) 
		GROUP BY asset_name, risk_name, risk_id 
		ORDER BY risk_residual_value DESC, process_weight ASC
		LIMIT 10  	
  	";
  }
  
  public function executeQuery() {
    parent::executeQuery();
    
    while($this->coDataSet->fetch()){
      $riskId = $this->coDataSet->getFieldByAlias('risk_id')->getValue();
      $riskName = $this->coDataSet->getFieldByAlias('risk_name')->getValue();
      $assetName = $this->coDataSet->getFieldByAlias('asset_name')->getValue();
      $processName = $this->coDataSet->getFieldByAlias('process_name')->getValue();
      
      $this->caValues[$riskId] = array(
        'risk_name' => $riskName,
        'asset_name' => $assetName,
        'process_name' => $processName
      );
    }
  }

  public function getValues() {
    $this->makeQuery();
    $this->executeQuery();
    return $this->caValues;
  }  
}

?>