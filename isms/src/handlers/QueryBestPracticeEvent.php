<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryBestPracticeEvent.
 *
 * <p>Consulta para auxiliar na exporta��o de eventos ligados � melhores pr�ticas.</p>
 * @package ISMS
 * @subpackage handlers
 */

class QueryBestPracticeEvent extends FWDDBQueryHandler {

	protected $caEventIds;
	protected $caBestPracticeIds;
	protected $caBestPracticeEvents = array();
	
  public function __construct($poDB) {
    parent::__construct($poDB); 

    $this->coDataSet->addFWDDBField(new FWDDBField('fkContext'     ,'BestPracticeEventId',DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('fkBestPractice','BestPracticeId'     ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('fkEvent'       ,'EventId'            ,DB_NUMBER));
  }


  public function makeQuery(){
  	$maFilters = array();
		if (count($this->caBestPracticeIds)) {
      $maAux = array();
      foreach($this->caBestPracticeIds as $maBestPracticeIds) {
        $maAux[] = "fkBestPractice IN (".implode(",",$maBestPracticeIds).")";
      }
      if (count($maAux)) {
        $maFilters[] = "(".implode(" OR ",$maAux).")";
      }
		}
		if (count($this->caEventIds)) {
      $maAux = array();
      foreach($this->caEventIds as $maEventIds) {
        $maAux[] = "fkEvent IN (".implode(",",$maEventIds).")";
      }
      if (count($maAux)) {
        $maFilters[] = "(".implode(" OR ",$maAux).")";
      }
		}
    $msWhere = count($maFilters)?' WHERE '.implode(' AND ',$maFilters):'';
    $this->csSQL = "SELECT fkContext as BestPracticeEventId, fkBestPractice as BestPracticeId, fkEvent as EventId
										FROM rm_best_practice_event".$msWhere;
  }
  
  public function executeQuery() {
    parent::executeQuery();
    while ($this->coDataSet->fetch()) {
      $this->caBestPracticeEvents[] = $this->coDataSet->getFieldByAlias("BestPracticeEventId")->getValue();
    }
  }
  
  public function getBestPracticeEvents() {
  	return $this->caBestPracticeEvents;
  }
  
	public function setBestPracticeIds($paBestPracticeIds) {
    $this->caBestPracticeIds = array_chunk($paBestPracticeIds,1000);
	}
	
	public function setEventIds($paEventIds) {
    $this->caEventIds = array_chunk($paEventIds,1000);
	}
}
?>