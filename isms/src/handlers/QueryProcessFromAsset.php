<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryProcessFromAsset.
 *
 * <p>Consulta para buscar os processos de um ativo.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryProcessFromAsset extends FWDDBQueryHandler {
  
  private $ciAssetId = 0;
  
  private $caProcesses = array();
  
  public function __construct($poDB) {
    parent::__construct($poDB);

    $this->coDataSet->addFWDDBField(new FWDDBField("fkProcess","process_id", DB_NUMBER));
  }
  
  public function setAssetId($piAssetId) {
    $this->ciAssetId = $piAssetId;
  }
  
  public function makeQuery() {
    $this->csSQL = "SELECT fkProcess as process_id FROM rm_process_asset WHERE fkAsset = {$this->ciAssetId}";
  }
  
  public function executeQuery() {
    parent::executeQuery();
    $this->caProcesses = array();
    while ($this->coDataSet->fetch()) {
      $this->caProcesses[] = $this->coDataSet->getFieldByAlias("process_id")->getValue();
    }
  }
  
  /**
   * Retorna os processos de um ativo.
   * 
   * <p>M�todo para retornar os processos de um ativo.</p>
   * @access public 
   * @return array Array de ids dos processos
   */ 
  public function getProcesses() {
    return $this->caProcesses;
  }
}
?>