<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryAssetUpdate.
 *
 * <p>Update que calcula o novo valor dos riscos de um determinado ativo</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryAssetUpdate extends FWDDBQueryHandler {
  
  protected $ciAssetId = 0;
  
  /**
   * Construtor da classe, armazena o nome do usu�rio que acessa o banco.
   * 
   * <p>Construtor da classe, armazena o nome do usu�rio que acessa o banco.</p>
   * @access public
   * @param FWDDB $poDB objeto de acesso a base de dados 
   */   
  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField('fkContext','fkContext'   ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('nValue'   ,'nValue',DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('nValueResidual'   ,'nValueResidual',DB_NUMBER));
  }
  
 /**
  * Define o ID do ativo a ser atualizado. 
  * 
  * <p>Define o ID do ativo a ser atualizado, nome do m�todo � gen�rico para compatibilidade com a atualiza��o de ativo e risco x controle.</p>
  * @access public
  * @param integer $piRiskId id do risco
  */   
  public function setElementId($piAsserId) {
    $this->ciAssetId = $piAsserId;
  }
  
  public function makeQuery() {
  	if (FWDWebLib::getDatabaseType() == DB_ORACLE)
    	$this->csSQL = "UPDATE rm_risk set nValue=".FWDWebLib::getFunctionUserDB()."get_aut_risk_value(fkContext),nValueResidual=".FWDWebLib::getFunctionUserDB()."get_aut_risk_value_residual(fkContext) where fkAsset={$this->ciAssetId}";
    else
    	$this->csSQL =" UPDATE rm_risk set nValue=".FWDWebLib::getFunctionUserDB()."get_risk_value(fkContext),nValueResidual=".FWDWebLib::getFunctionUserDB()."get_risk_value_residual(fkContext) where fkAsset={$this->ciAssetId};";
      
  }
  
}

/**
 * Classe QueryAssetUpdateSubquery
 * <p>Classe complementar a QueryAssetUpdate, criada para resolver a segunda query de update feita por ela.
 * Pela nova restri��o do sistema n�o podem mais existir queries onde mais de uma instru��o de sql seja passada.
 * 
 * @package ISMS
 * @subpackage handlers
 */
class QueryAssetUpdateSubquery extends FWDDBQueryHandler {
  
  protected $ciAssetId = 0;
  
  /**
   * Construtor da classe, armazena o nome do usu�rio que acessa o banco.
   * 
   * <p>Construtor da classe, armazena o nome do usu�rio que acessa o banco.</p>
   * @access public
   * @param FWDDB $poDB objeto de acesso a base de dados 
   */   
  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField('fkContext','fkContext'   ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('nValue'   ,'nValue',DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('nValueResidual'   ,'nValueResidual',DB_NUMBER));
  }
  
 /**
  * Define o ID do ativo a ser atualizado. 
  * 
  * <p>Define o ID do ativo a ser atualizado, nome do m�todo � gen�rico para compatibilidade com a atualiza��o de ativo e risco x controle.</p>
  * @access public
  * @param integer $piRiskId id do risco
  */   
  public function setElementId($piAsserId) {
    $this->ciAssetId = $piAsserId;
  }
  
  public function makeQuery() {
  	if (FWDWebLib::getDatabaseType() == DB_ORACLE)
    	return;
    else
      //nescess�ria a linha abaixo pois quando o risco est� tratado e o ativo estava desparametrizado e depois � parametrizado,
      //a trigger do risco n�o dispara o update do valor do ativo na tabela do ativo, assim deve-se calcular novamente o valor do ativo
    	$this->csSQL.=" UPDATE rm_asset set nValue=".FWDWebLib::getFunctionUserDB()."get_asset_value(fkContext) where fkContext={$this->ciAssetId};";
  }
  
}
?>