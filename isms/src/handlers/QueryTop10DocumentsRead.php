<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryTop10DocumentsRead
 *
 * <p>Consulta para retornar os 10 documentos mais lidos no sistema./p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryTop10DocumentsRead extends FWDDBQueryHandler {
  protected $caDocCount = array();
  
  protected $ciUserId = 0;
  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField('' ,'document_id'   ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('' ,'document_name' ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('' ,'read_count'    ,DB_NUMBER));
  }
  
  public function setUserId($piUserId){
    $this->ciUserId = $piUserId;
  }
  public function makeQuery() {
  $msWhere = '';
  if($this->ciUserId)
  $msWhere .= "   WHERE doc.fkMainApprover = ".$this->ciUserId;

  $this->csSQL = "
SELECT doc.fkContext as document_id, 
       doc.sname as document_name,
       count(doc_inst.fkContext) as read_count 
  FROM view_pm_doc_instance_active doc_inst
   JOIN pm_doc_read_history doc_hist ON ( doc_inst.fkContext = doc_hist.fkInstance )
   JOIN view_pm_document_active doc ON ( doc.fkContext = doc_inst.fkDocument )
   $msWhere
   GROUP BY doc.fkContext, doc.sName
   ORDER BY read_count DESC
";
  }
  
  public function executeQuery() {
    parent::executeQuery(10,0);
    while($this->coDataSet->fetch()){
      $this->caDocCount[] 
            = array(  'id' => $this->coDataSet->getFieldByAlias("document_id")->getValue(),
                      'name' => $this->coDataSet->getFieldByAlias("document_name")->getValue(),
                      'count' => $this->coDataSet->getFieldByAlias("read_count")->getValue()
              );
    }
  }
  
  public function getValues() {
    $this->makeQuery();
    $this->executeQuery();
    return $this->caDocCount;
  }
}
?>