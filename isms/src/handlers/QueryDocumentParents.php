<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryDocumentParents.
 *
 * <p>Consulta para buscar o documento pai de um documento, o pai do pai e assim
 * por diante.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryDocumentParents extends FWDDBQueryHandler {
  
  protected $ciDocumentId = 0;
  
  protected $cbExecByTrash = false;
  
  public function __construct($poDB,$pbExecByTrash = false){
    parent::__construct($poDB);
    $this->cbExecByTrash = $pbExecByTrash;
    
    $this->coDataSet->addFWDDBField(new FWDDBField('','document_id',DB_NUMBER));
  }
  
  public function setDocumentId($piDocumentId){
    $this->ciDocumentId = $piDocumentId;
  }
  
  public function makeQuery(){
    if($this->cbExecByTrash){
      $msFuncName = 'get_document_parents_trash';
    }else{
      $msFuncName = 'get_document_parents';
    }
    $this->csSQL = "SELECT document_id as document_id
                    FROM " . FWDWebLib::getFunctionCall("$msFuncName({$this->ciDocumentId})");
  }  
}
?>