<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryLastControlRevision.
 *
 * <p>Query para obter a data da �ltima revis�o de um controle. Se n�o houver
 * sido feita nenhuma revis�o, pega a data de cria��o do controle.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryLastControlRevision extends FWDDBQueryHandler {

  protected $ciControlId = 0;
  
  protected $csDateLastRevision = '';
  protected $csDateNextRevision = '';

  public function __construct($poDB=null){
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField(''                 ,'date_last_revision' ,DB_DATETIME));
    $this->coDataSet->addFWDDBField(new FWDDBField('s.dnextoccurrence','date_next_revision' ,DB_DATETIME));
  }

  public function setControlId($piControlId){
    $this->ciControlId = $piControlId;
  }

  public function makeQuery(){
    if($this->ciControlId){
      $this->csSQL = "SELECT
                        CASE
                          WHEN ceh.ddatetodo IS NOT NULL THEN ceh.ddatetodo
                          WHEN ceh.ddatetodo IS NULL THEN ch.context_date_created
                        END AS date_last_revision,
                        s.dnextoccurrence AS date_next_revision
                      FROM
                        context_history ch
                        LEFT JOIN wkf_control_efficiency ce ON (ce.fkcontrolefficiency = ch.context_id)
                        LEFT JOIN rm_control_efficiency_history ceh ON (ceh.fkcontrol = ce.fkcontrolefficiency)
                        LEFT JOIN wkf_schedule s ON (s.pkschedule = ce.fkschedule)
                      WHERE
                        ch.context_id = {$this->ciControlId}
                        AND NOT EXISTS (
                          SELECT *
                          FROM rm_control_efficiency_history ceh2
                          WHERE
                            ceh2.fkcontrol = ce.fkcontrolefficiency
                            AND ceh2.ddatetodo > ceh.ddatetodo
                        )";
    }else{
      trigger_error("You must specify the control id.",E_USER_ERROR);
    }
  }
  
  protected function executeAndFetch($piControlId){
    $this->setControlId($piControlId);
    $this->makeQuery();
    $this->executeQuery();
    $this->fetch();
    $this->csDateLastRevision = $this->getFieldValue('date_last_revision');
    $this->csDateNextRevision = $this->getFieldValue('date_next_revision');
  }
  
  public function getLastRevision($piControlId){
    if($piControlId!=$this->ciControlId){
      $this->executeAndFetch($piControlId);
    }
    return $this->csDateLastRevision;
  }
  
  public function getNextRevision($piControlId){
    if($piControlId!=$this->ciControlId){
      $this->executeAndFetch($piControlId);
    }
    return $this->csDateNextRevision;
  }
  
}

?>