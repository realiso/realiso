<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryChartIncidentLossType.
 *
 * <p>Consulta para popular o gr�fico com a propor��o de incidentes com seu tipo de perda.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryChartIncidentLossType extends FWDDBQueryHandler {

  protected $caIncidentLossTypeSummary = array();
  protected $ciUserId = 0;

  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField("ordem","ordem", DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("count","count", DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("","orderby", DB_NUMBER));
  }

  public function setUserId($psUserId) {
    $this->ciUserId = $psUserId;
  }

  public function makeQuery() {
    $msAndUser = ($this->ciUserId ? " AND i.fkResponsible = ".$this->ciUserId : "");
    $this->csSQL = "
                    SELECT 'direct_loss' AS ordem, COUNT(*) AS count, 1 as orderby
                    FROM view_ci_incident_active i 
                    WHERE i.nLossType='".INCIDENT_LOSS_TYPE_DIRECT."' $msAndUser
                    UNION
                    SELECT 'indirect_loss' AS ordem, COUNT(*) AS count, 2 as orderby 
                    FROM view_ci_incident_active i 
                    WHERE i.nLossType='".INCIDENT_LOSS_TYPE_INDIRECT."' $msAndUser
                    ORDER BY orderby
                    ";
  }

  public function executeQuery() {
    parent::executeQuery();
    $this->caIncidentLossTypeSummary = array();
    while ($this->coDataSet->fetch())
      $this->caIncidentLossTypeSummary[$this->coDataSet->getFieldByAlias("ordem")->getValue()] = $this->coDataSet->getFieldByAlias("count")->getValue();
  }

  public function getIncidentLossTypeSummary() {
    return $this->caIncidentLossTypeSummary;
  }
}
?>