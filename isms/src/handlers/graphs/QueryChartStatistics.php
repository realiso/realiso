<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryChartStatistics.
 *
 * <p>Consulta para popular os gráficos de estatísticas.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryChartStatistics extends FWDDBQueryHandler {

	protected $caStatistics = array();
	protected $ciContextId = 0;
  protected $caTypes = array();
  protected $csStartDate = "";
  protected $csFinishDate = "";

  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField("fkContext","context_id", DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("sType","stat_type", DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("dDate","stat_date", DB_DATETIME));
    $this->coDataSet->addFWDDBField(new FWDDBField("nValue","stat_value", DB_NUMBER));
  }

	public function setContext($piContextId) {
    $this->ciContextId = $piContextId;
  }

  public function setTypes($paTypes) {
    $this->caTypes = $paTypes;
  }
  
  public function setStartDate($piTimestamp) {
    $this->csStartDate = $piTimestamp ? ISMSLib::getTimestampFormat($piTimestamp) : "";
  }
  
  public function setFinishDate($piTimestamp) {
    $this->csFinishDate = $piTimestamp ? ISMSLib::getTimestampFormat($piTimestamp): "";
  }

  public function makeQuery() {
  	$maFilters = array();  	
  	$maTypeFilter = array();
  	foreach($this->caTypes as $msType) $maTypeFilter[] = "sType = '" . $msType . "'";  	
  	$maFilters[] = '(' . implode(' OR ', $maTypeFilter) . ')';	  	
  	if ($this->csStartDate) $maFilters[] = "dDate >= " . $this->csStartDate;
  	if ($this->csFinishDate) $maFilters[] = "dDate <= " . $this->csFinishDate;
  	if ($this->ciContextId) $maFilters[] = "fkContext = " . $this->ciContextId;
  	$msWhere = 'WHERE ' . implode(' AND ', $maFilters);  	
    $this->csSQL = "SELECT fkContext as context_id, sType as stat_type, dDate as stat_date, nValue as stat_value
    								FROM view_isms_context_hist_active
    								$msWhere
    								ORDER BY sType, dDate";
  }

  public function executeQuery() {
    parent::executeQuery();
    $this->caStatistics = array();
    while ($this->coDataSet->fetch()) {
      $this->caStatistics[$this->coDataSet->getFieldByAlias("stat_type")->getValue()]['values'][] = $this->coDataSet->getFieldByAlias("stat_value")->getValue();
      $this->caStatistics[$this->coDataSet->getFieldByAlias("stat_type")->getValue()]['dates'][] = $this->coDataSet->getFieldByAlias("stat_date")->getValue();
    }
  }

  public function getStatistics() {
    return $this->caStatistics;
  }
}
?>