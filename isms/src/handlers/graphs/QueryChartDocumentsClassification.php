<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryChartDocumentsClassification.
 *
 * <p>Consulta para popular o gráfico de quantidade de documentos por classificação.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryChartDocumentsClassification extends FWDDBQueryHandler {

  protected $caDocumentsClassification = array();  

  public function __construct($poDB) {
    parent::__construct($poDB);

    $this->coDataSet->addFWDDBField(new FWDDBField("",	"doc_classif_name",				DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("",	"doc_classif_total",	DB_NUMBER));
  }

  public function makeQuery() {
    $this->csSQL = "SELECT c.sName as doc_classif_name, count(d.fkClassification) as doc_classif_total
										FROM view_pm_published_docs d JOIN isms_context_classification c ON (d.fkClassification = c.pkClassification)
										GROUP BY c.pkClassification, c.sName";
  }

  public function executeQuery() {
    parent::executeQuery();
    $this->caDocumentsClassification = array();
    while ($this->coDataSet->fetch())
      $this->caDocumentsClassification[] = array($this->coDataSet->getFieldByAlias("doc_classif_name")->getValue(),$this->coDataSet->getFieldByAlias("doc_classif_total")->getValue());
  }

  public function getDocumentsClassification() {
    return $this->caDocumentsClassification;
  }
}
?>