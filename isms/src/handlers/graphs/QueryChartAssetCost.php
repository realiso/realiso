<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryChartAssetCost.
 *
 * <p>Consulta para popular o gr�fico de ativos e valores de custo.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryChartAssetCost extends FWDDBQueryHandler {

  protected $caAssetCostSummary = array();
  protected $ciUserId = 0;

  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField("ordem","ordem", DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("count","count", DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("","orderby", DB_NUMBER));
  }

  public function setUserId($psUserId) {
    $this->ciUserId = $psUserId;
  }

  public function makeQuery() {
    $this->csSQL = "SELECT '1mil_cost' AS ordem, COUNT(*) AS count, 1 as orderby 
                    FROM view_rm_asset_active a
                    JOIN isms_context c ON (a.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED.")
                    WHERE a.nCost >= 1000000
                    UNION
                    SELECT '500k_cost' AS ordem, COUNT(*) AS count, 2 as orderby 
                    FROM view_rm_asset_active a
                    JOIN isms_context c ON (a.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED.")
                    WHERE a.nCost < 1000000 AND a.nCost >= 500000
                    UNION
                    SELECT '100k_cost' AS ordem, COUNT(*) AS count, 3 as orderby
                    FROM view_rm_asset_active a
                    JOIN isms_context c ON (a.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED.")
                    WHERE a.nCost < 500000 AND a.nCost >= 100000
                    UNION
                    SELECT 'low_cost' AS ordem, COUNT(*) AS count, 4 as orderby
                    FROM view_rm_asset_active a
                    JOIN isms_context c ON (a.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED.")
                    WHERE a.nCost < 100000
                    ORDER BY orderby
                    ";
  }

  public function executeQuery() {
    parent::executeQuery();
    $this->caAssetCostSummary = array();
    while ($this->coDataSet->fetch())
      $this->caAssetCostSummary[$this->coDataSet->getFieldByAlias("ordem")->getValue()] = $this->coDataSet->getFieldByAlias("count")->getValue();
  }

  public function getAssetCostSummary() {
    return $this->caAssetCostSummary;
  }
}
?>