<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryChartDocumentRegisters.
 *
 * <p>Consulta para popular o gr�fico dos 10 documentos com mais registros.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryChartDocumentRegisters extends FWDDBQueryHandler {

  protected $caDocumentRegisters = array();

  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField("","document_name", 	DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("","document_registers", 	DB_NUMBER));
  }

  public function makeQuery() {
    $this->csSQL = "SELECT d.sName as document_name, count(dr.fkRegister) as document_registers
										FROM view_pm_published_docs d JOIN view_pm_doc_registers_active dr ON (d.fkContext = dr.fkDocument)
										GROUP BY d.fkContext, d.sName
                    ORDER BY document_registers DESC";
  }

  public function executeQuery() {
    parent::executeQuery(10,0);
    $this->caDocumentRegisters = array();
    while ($this->coDataSet->fetch())
      $this->caDocumentRegisters[] = array($this->coDataSet->getFieldByAlias("document_name")->getValue(),$this->coDataSet->getFieldByAlias("document_registers")->getValue());
  }

  public function getDocumentRegisters() {
    return $this->caDocumentRegisters;
  }
}
?>