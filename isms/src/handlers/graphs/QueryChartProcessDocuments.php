<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryChartProcessDocuments.
 *
 * <p>Consulta para popular o gr�fico dos 10 processos com mais documentos.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryChartProcessDocuments extends FWDDBQueryHandler {

  protected $caProcessDocuments = array();

  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField("","process_name", 	DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("","process_documents", 	DB_NUMBER));
  }

  public function makeQuery() {
    $this->csSQL = "SELECT p.sName as process_name, count(dc.fkDocument) as process_documents
										FROM view_rm_process_active p
										JOIN view_pm_doc_context_active dc ON (p.fkContext = dc.fkContext)
										JOIN view_pm_published_docs pd ON (dc.fkDocument = pd.fkContext)
										GROUP BY p.fkContext, p.sName
                    ORDER BY process_documents DESC";
  }

  public function executeQuery() {
    parent::executeQuery(10,0);
    $this->caProcessDocuments = array();
    while ($this->coDataSet->fetch())
      $this->caProcessDocuments[] = array($this->coDataSet->getFieldByAlias("process_name")->getValue(),$this->coDataSet->getFieldByAlias("process_documents")->getValue());
  }

  public function getProcessDocuments() {
    return $this->caProcessDocuments;
  }
}
?>