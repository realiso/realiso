<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryChartAssetEstimatedCost.
 *
 * <p>Consulta para popular o gr�fico de sum�rio estimativa de custos de ativos.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryChartAssetEstimatedCost extends FWDDBQueryHandler {

  protected $caAssetEstimatedCostSummary = array();
  protected $ciUserId = 0;

  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField("ordem","ordem", DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("count","count", DB_NUMBER));
  }

  public function setUserId($psUserId) {
    $this->ciUserId = $psUserId;
  }

  public function makeQuery() {
    $this->csSQL = "SELECT 'estimated_cost' AS ordem, COUNT(*) AS count 
                    FROM view_rm_asset_active a
                    JOIN isms_context c ON (a.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED.")
                    WHERE a.nCost > 0
                    UNION
                    SELECT 'not_estimated_cost' AS ordem, COUNT(*) AS count 
                    FROM view_rm_asset_active a
                    JOIN isms_context c ON (a.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED.")
                    WHERE a.nCost <= 0";
  }

  public function executeQuery() {
    parent::executeQuery();
    $this->caAssetEstimatedCostSummary = array();
    while ($this->coDataSet->fetch())
      $this->caAssetEstimatedCostSummary[$this->coDataSet->getFieldByAlias("ordem")->getValue()] = $this->coDataSet->getFieldByAlias("count")->getValue();
  }

  public function getAssetEstimatedCostSummary() {
    return $this->caAssetEstimatedCostSummary;
  }
}
?>