<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryChartProcessUsers.
 *
 * <p>Consulta para popular o gr�fico dos 10 processos com mais pessoas envolvidas.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryChartProcessUsers extends FWDDBQueryHandler {

  protected $caProcessUsers = array();

  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField("","process_name", 	DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("","process_users", 	DB_NUMBER));
  }

  public function makeQuery() {
    $this->csSQL = "SELECT p.sName as process_name, count(pu.fkUser) as process_users
										FROM view_rm_process_active p JOIN view_pm_process_user_active pu ON (p.fkContext = pu.fkProcess)
										GROUP BY p.fkContext, p.sName
                    ORDER BY process_users DESC";
  }

  public function executeQuery() {
    parent::executeQuery(10,0);
    $this->caProcessUsers = array();
    while ($this->coDataSet->fetch())
      $this->caProcessUsers[] = array($this->coDataSet->getFieldByAlias("process_name")->getValue(),$this->coDataSet->getFieldByAlias("process_users")->getValue());
  }

  public function getProcessUsers() {
    return $this->caProcessUsers;
  }
}
?>