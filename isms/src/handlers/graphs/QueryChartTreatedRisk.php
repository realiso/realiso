<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryChartTreatedRisk.
 *
 * <p>Consulta para popular o gr�fico com a quantidade de riscos potenciais.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryChartTreatedRisk extends FWDDBQueryHandler {

  protected $caTreatedRiskSummary = array();
  protected $ciUserId = 0;

  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField("ordem","ordem", DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("count","count", DB_NUMBER));
  }

  public function makeQuery() {
    $this->csSQL = "
                    SELECT 'control' AS ordem, COUNT(DISTINCT fkRisk) AS count
                    FROM view_rm_risk_control_active rc
                    JOIN isms_context c ON (rc.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED.")
                    UNION 
                    SELECT 'green' AS ordem, COUNT(fkContext) AS count
                    FROM view_rm_risk_active r
                    JOIN isms_context c ON (r.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED.")
                    WHERE (r.nValue <= ".ISMSLib::getConfigById(RISK_LOW)." AND r.nValue > 0)
                    UNION
                    SELECT 'accepted' AS ordem, COUNT(fkContext) as count
                    FROM view_rm_risk_active r
                    JOIN isms_context c ON (r.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED.")
                    WHERE (r.nAcceptMode=72601)
                    UNION
                    SELECT 'transferred' AS ordem, COUNT(fkContext) as count
                    FROM view_rm_risk_active r
                    JOIN isms_context c ON (r.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED.")
                    WHERE (r.nAcceptMode=72602)
                    UNION
                    SELECT 'avoided' AS ordem, COUNT(fkContext) as count
                    FROM view_rm_risk_active r
                    JOIN isms_context c ON (r.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED.")
                    WHERE (r.nAcceptMode=72603)
                    ";
  }

  public function executeQuery() {
    parent::executeQuery();
    $this->caTreatedRiskSummary = array();
    while ($this->coDataSet->fetch())
      $this->caTreatedRiskSummary[$this->coDataSet->getFieldByAlias("ordem")->getValue()] = $this->coDataSet->getFieldByAlias("count")->getValue();
  }

  public function getTreatedRiskSummary() {
    return $this->caTreatedRiskSummary;
  }
}
?>