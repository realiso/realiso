<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryChartDocumentsAccessed.
 *
 * <p>Consulta para popular o gr�fico com a quantidade de documentos acessados.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryChartDocumentsAccessed extends FWDDBQueryHandler {

  protected $caDocumentsAccessed = array();

  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField("data","data", DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("count","count", DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("ordem","ordem", DB_NUMBER));
  }

  public function makeQuery() {
    $maSql = array();
    $miMonth = date("m");
    for($miI=11;$miI>=0;$miI--) {
      $msPeriod = date('m/Y',mktime(0,0,0,$miMonth-$miI,1,date('Y')));
      $msSql = "SELECT '{$msPeriod}' as data, COUNT(*) as count, {$miI} as ordem
                FROM view_pm_doc_instance_active doc_inst
                JOIN isms_context_date cd ON (doc_inst.fkContext = cd.fkContext)
                JOIN pm_doc_read_history r ON (doc_inst.fkContext = r.fkInstance)
                WHERE 
                  r.dDate >= '".date('Y-m-d',mktime(0,0,0,$miMonth-$miI,1,date("Y")))."' AND 
                  r.dDate < '".date('Y-m-d',mktime(0,0,0,$miMonth-$miI+1,1,date("Y")))."' AND
                  cd.nAction = ".ACTION_CREATE."
               ";
      $maSql[] = $msSql;
    }
    $this->csSQL = implode(" UNION ",$maSql) . "ORDER BY ordem DESC";
  }

  public function executeQuery() {
    parent::executeQuery();
    $this->caDocumentsAccessed = array();
    while ($this->coDataSet->fetch())
      $this->caDocumentsAccessed[$this->coDataSet->getFieldByAlias("data")->getValue()] = $this->coDataSet->getFieldByAlias("count")->getValue();
  }

  public function getDocumentsAccessed() {
    return $this->caDocumentsAccessed;
  }
}
?>