<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryChartControlTest.
 *
 * <p>Consulta para popular o gr�fico do hist�rico da revis�o de efici�ncia dos controles.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryChartControlTest extends FWDDBQueryHandler {

  protected $caControlTestSummary = array();
  protected $ciControlId = 0;

  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField("date_accomplished","date_accomplished", DB_DATETIME));
    $this->coDataSet->addFWDDBField(new FWDDBField("value","value", DB_NUMBER));
  }

  public function setControlId($piControlId) {
    $this->ciControlId = $piControlId;
  }

  public function makeQuery() {
    $this->csSQL = "
										SELECT dDateAccomplishment as date_accomplished, bTestedValue as value
                    FROM rm_control_test_history ch
										JOIN view_isms_context_active c ON (ch.fkControl = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED." AND ch.fkControl=".$this->ciControlId.")
                    ORDER BY date_accomplished 
";
  }

  public function executeQuery() {
    parent::executeQuery();
    $this->caControlTestSummary = array();
    while ($this->coDataSet->fetch())
      $this->caControlTestSummary[$this->coDataSet->getFieldByAlias("date_accomplished")->getValue()] = $this->coDataSet->getFieldByAlias("value")->getValue();
  }

  public function getControlTestSummary() {
    return $this->caControlTestSummary;
  }
}
?>