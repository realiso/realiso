<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe QueryTooManyIncidents
 *
 * <p>Consulta para verificar se um risco tem mais incidentes do que o esperado
 * num determinado per�odo.</p>
 * @package ISMS
 * @subpackage classes
 */
class QueryTooManyIncidents extends FWDDBQueryHandler {

  protected $ciRiskId;
  protected $ciInitialTime;
  protected $ciFinalTime;

  /**
  * Construtor.
  * 
  * <p>Construtor da classe.</p>
  * @access public 
  */
  public function __construct($poDB){
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField('','incidents_count_cmp',DB_NUMBER));
  }
  
  public function setRiskId($piRiskId){
    $this->ciRiskId = $piRiskId;
  }

  public function setInitialTime($piInitialTime){
    $this->ciInitialTime = mktime(0, 0, 0, date("m",$piInitialTime), date("d",$piInitialTime),  date("Y",$piInitialTime));
  }

  public function setFinalTime($piFinalTime){
    $this->ciFinalTime = mktime(0, 0, 0, date("m",$piFinalTime), date("d",$piFinalTime),  date("Y",$piFinalTime));
  }
  
  public function makeQuery(){
    $msInitialDate = ISMSLib::getTimestampFormat($this->ciInitialTime);
    $msFinalDate = ISMSLib::getTimestampFormat($this->ciFinalTime);
    $this->csSQL = "SELECT
                      CASE
                        WHEN rc.incident_count > rv_max.nIncidentAmount THEN 1
                        WHEN rc.incident_count <= rv_min.nIncidentAmount THEN -1
                        ELSE 0 
                      END as incidents_count_cmp
                    FROM
                      rm_risk r
                      JOIN ci_risk_probability rv_max ON (rv_max.fkValueName = r.fkProbabilityValueName AND rv_max.fkRisk = r.fkContext)
                      JOIN rm_parameter_value_name pv ON (pv.pkValueName = r.fkProbabilityValueName)
                      JOIN (
                        SELECT
                          ir.fkRisk as risk_id,
                          count(ir.fkIncident) as incident_count
                        FROM
                          ci_incident_risk ir
                          JOIN view_ci_incident_active i ON (i.fkContext = ir.fkIncident)
                        WHERE i.dDate >= $msInitialDate AND i.dDate <= $msFinalDate
                        GROUP BY ir.fkRisk
                      ) rc ON (rc.risk_id = r.fkContext)
                      LEFT JOIN rm_parameter_value_name pv_min ON (
                        rv_max.fkValueName = r.fkProbabilityValueName
                        AND pv_min.nValue < pv.nValue
                        AND NOT EXISTS (
                          SELECT *
                          FROM rm_parameter_value_name pv2
                          WHERE
                            pv2.nValue < pv.nValue
                            AND pv2.nValue > pv_min.nValue
                        )
                      )
                      LEFT JOIN ci_risk_probability rv_min ON (rv_min.fkValueName = pv_min.pkValueName AND rv_min.fkRisk = r.fkContext)
                    WHERE r.fkContext = {$this->ciRiskId}";
  }

}

?>