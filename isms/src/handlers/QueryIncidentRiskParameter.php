<?php
/**
 * Classe QueryIncidentRiskParameter
 *
 * <p></p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryIncidentRiskParameter extends FWDDBQueryHandler {

	private $ciIncident;
	private $ciParameterName;
	
  public function __construct($poDB){
    parent::__construct($poDB);

    $this->coDataSet->addFWDDBField(new FWDDBField('ci.fkContext',                'ci_id',                        DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('ci.fkincident',  			  'ci_incident',  				  DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('ci.fkparametername',          'ci_parameter_name',            DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('ci.fkparametervalue',         'ci_parameter_value',           DB_NUMBER));
  }

  public function makeQuery(){
    $this->csSQL ="
SELECT ci.fkContext as ci_id,
       ci.fkincident as ci_incident,
       ci.fkparametername as ci_parameter_name,
       ci.fkparametervalue as ci_parameter_value
   FROM ci_incident_risk_parameter ci
   WHERE ci.fkincident = $this->ciIncident";
    if($this->ciParameterName) {
    	$this->csSQL .= " and ci.fkparametername = $this->ciParameterName";
    }
  }
  
  public function setIncidentId($incident) {
  	$this->ciIncident = $incident;
  }
  
  public function setParameterName($parameter) {
  	$this->ciParameterName = $parameter;
  }
  
};
?>