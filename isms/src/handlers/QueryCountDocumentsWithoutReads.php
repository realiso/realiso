<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryCountDocumentsWithoutReads.
 *
 * <p>Consulta para calcular o n�mero total de documentos em que pelo menos um
 * de seus leitores ainda n�o leu o documento.
 * </p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryCountDocumentsWithoutReads extends FWDDBQueryHandler {
  protected $caDocCount = array();
  
  protected $ciUserId = 0;
  
  public function __construct($poDB) {
    parent::__construct($poDB);

    $this->coDataSet->addFWDDBField(new FWDDBField('' ,'doc_count'    ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('' ,'doc_type_sel' ,DB_NUMBER));
  }
  
  public function setUserId($piUserId){
    $this->ciUserId = $piUserId;
  }

  public function makeQuery() {
  $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
  $msReaderJoin = "JOIN isms_context c ON (c.pkContext = doc_p.fkContext)
            LEFT JOIN pm_doc_approvers da ON (da.fkDocument = doc_p.fkContext AND da.fkUser = $miUserId)
            LEFT JOIN pm_doc_readers dr ON (dr.fkDocument = doc_p.fkContext AND dr.fkUser = $miUserId AND dr.bDenied=0)
            LEFT JOIN pm_doc_instance di ON (doc_p.fkCurrentVersion = di.fkContext)";
  $msReaderWhere = "
    WHERE (
      doc_p.fkMainApprover = $miUserId
      OR (c.nState = ".CONTEXT_STATE_DOC_DEVELOPING." AND doc_p.fkAuthor = $miUserId)
      OR (c.nState = ".CONTEXT_STATE_DOC_PENDANT." AND da.fkDocument IS NOT NULL)
      OR (
        (
          dr.fkDocument IS NOT NULL
          OR da.fkDocument IS NOT NULL
          OR doc_p.fkAuthor = $miUserId
        )AND(
          di.nMajorVersion > 0
          AND c.nState = ".CONTEXT_STATE_DOC_APPROVED."
          AND doc_p.dDateProduction <= '".date('Y-m-d')."'
        )
      )
    )";
  
  
  $msWhereAll = '';
  $msWhere = '';
  if($this->ciUserId){
    $msWhere .= " AND doc_p.fkMainApprover = ".$this->ciUserId;
  }
    $this->csSQL = "
SELECT 
  'all_doc_read' as doc_type_sel, 
  count(fkContext) as doc_count
FROM (
  SELECT DISTINCT doc_p.fkContext
  FROM pm_doc_read_history doc_r
    JOIN view_pm_doc_instance_active doc_i ON (doc_r.fkInstance = doc_i.fkContext)
    JOIN view_pm_published_docs doc_p ON (doc_p.fkContext = doc_i.fkDocument)
    $msReaderJoin
    $msReaderWhere
    $msWhere
) doc

UNION

SELECT 
  'doc_not_read' as doc_type_sel,
  count(doc_p.fkContext) as doc_count
FROM view_pm_published_docs doc_p
$msReaderJoin
$msReaderWhere
AND doc_p.fkContext NOT IN (
  SELECT DISTINCT d.fkContext
  FROM pm_doc_read_history doc_r
    JOIN view_pm_doc_instance_active doc_i ON (doc_r.fkInstance = doc_i.fkContext)
    JOIN view_pm_published_docs d ON (d.fkContext = doc_i.fkDocument)
)
$msWhere
";
  }
  
  public function executeQuery() {
    parent::executeQuery();
    while($this->coDataSet->fetch()){
      $this->caDocCount[ $this->coDataSet->getFieldByAlias("doc_type_sel")->getValue() ] = 
        $this->coDataSet->getFieldByAlias("doc_count")->getValue();
    }
  }
  
  public function getValues(){
    $this->makeQuery();
    $this->executeQuery();
    return $this->caDocCount;
  }
}
?>