<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryCountDocumentsByState.
 *
 * <p>Consulta para calcular o n�mero de documentos em cada estado do workflow.
 * </p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryCountDocumentsByState extends FWDDBQueryHandler {
  
  protected $caDocCount;
  
  protected $ciUserId = 0;
  
  public function __construct($poDB){
    parent::__construct($poDB);
    
    $this->caDocCount = array(
      CONTEXT_STATE_DOC_DEVELOPING      => 0,
      CONTEXT_STATE_DOC_APPROVAL        => 0,
      CONTEXT_STATE_DOC_TO_BE_PUBLISHED => 0,
      CONTEXT_STATE_DOC_PUBLISHED       => 0,
      CONTEXT_STATE_DOC_REVISION        => 0,
    );
    
    $this->coDataSet->addFWDDBField(new FWDDBField('dis.doc_instance_status'       ,'doc_state',DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('count(dis.doc_instance_status)','count'    ,DB_NUMBER));
  }
  
  public function setUserId($piUserId){
    $this->ciUserId = $piUserId;
  }

  public function makeQuery(){
    $maFilters = array();
    
    $maFilters[] = "d.nType NOT IN (".CONTEXT_REGISTER.")";
    
    $maFilters[] = "dis.doc_instance_status != ".CONTEXT_STATE_DOC_OBSOLETE;
    
    if($this->ciUserId){
      // Pega s� os documentos que o usu�rio pode ver
      $msJoin = "LEFT JOIN pm_doc_approvers da ON (da.fkDocument = d.fkContext AND da.fkUser = {$this->ciUserId})
                 LEFT JOIN pm_doc_readers dr ON (dr.fkDocument = d.fkContext AND dr.fkUser = {$this->ciUserId} AND dr.bDenied=0)
                 LEFT JOIN pm_doc_instance di ON (di.fkContext = dis.doc_instance_id)";
      $maFilters[] = "(
        d.fkMainApprover = {$this->ciUserId}
        OR d.fkAuthor = {$this->ciUserId}
        OR (
          (
            dis.doc_instance_status = ".CONTEXT_STATE_DOC_PUBLISHED."
            OR dis.doc_instance_status = ".CONTEXT_STATE_DOC_OBSOLETE."
          )
          AND (
            dr.fkDocument IS NOT NULL
            OR da.fkDocument IS NOT NULL
          )
        )
        OR (
          da.fkDocument IS NOT NULL
          AND (
            dis.doc_instance_status = ".CONTEXT_STATE_DOC_APPROVAL."
            OR dis.doc_instance_status = ".CONTEXT_STATE_DOC_TO_BE_PUBLISHED."
          )
        )
      )";
    }else{
      // Pega todos documentos
      $msJoin = '';
    }
    
    $msWhere = '';
    if(count($maFilters)){
      $msWhere = " WHERE ".implode(' AND ', $maFilters);
    }
    
    $this->csSQL = "SELECT
                      dis.doc_instance_status as doc_state,
                      count(dis.doc_instance_status) as count
                    FROM
                      view_pm_document_active d
                      LEFT JOIN view_pm_doc_instance_status dis ON (dis.document_id = d.fkContext)
                      {$msJoin}
                    {$msWhere}
                    GROUP BY doc_instance_status";
  }
  
  public function executeQuery(){
    parent::executeQuery();
    while($this->coDataSet->fetch()){
      $miStatus = $this->getFieldValue('doc_state');
      $miCount = $this->getFieldValue('count');
      $this->caDocCount[$miStatus] = $miCount;
    }
  }
  
  public function getValues(){
    $this->makeQuery();
    $this->executeQuery();
    return $this->caDocCount;
  }

}

?>