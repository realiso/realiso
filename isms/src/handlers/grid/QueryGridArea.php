<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGridArea.
 *
 * <p>Consulta para popular a grid de �reas.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryGridArea extends FWDDBQueryHandler {

  protected $ciUserId = 0;

  protected $csName = '';

  protected $csNameDescription = '';

  protected $ciResponsible = 0;

  protected $caValueFilter = array();

  protected $ciState = 0;

  protected $ciMaxProcesses = 0;

  protected $ciMinProcesses = 0;

  protected $ciMaxSubAreas = 0;

  protected $ciMinSubAreas = 0;

  protected $ciType = 0;

  protected $ciPriority = 0;

  protected $ciRootArea = 0;

  public function __construct($poDB=null){
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField('a.fkContext','area_id'            ,DB_NUMBER  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('a.sName'    ,'area_name'          ,DB_STRING  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('u.sName'    ,'area_responsible'   ,DB_STRING  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('a.nValue'   ,'area_value'         ,DB_NUMBER  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('c.nState'   ,'area_state'         ,DB_NUMBER  ));
    $this->coDataSet->addFWDDBField(new FWDDBField(''           ,'area_process_count' ,DB_NUMBER  ));
    $this->coDataSet->addFWDDBField(new FWDDBField(''           ,'subarea_count'      ,DB_NUMBER  ));
    $this->coDataSet->addFWDDBField(new FWDDBField(''           ,'user_create_name'   ,DB_STRING  ));
    $this->coDataSet->addFWDDBField(new FWDDBField(''           ,'date_create'        ,DB_DATETIME));
    $this->coDataSet->addFWDDBField(new FWDDBField(''           ,'user_edit_name'     ,DB_STRING  ));
    $this->coDataSet->addFWDDBField(new FWDDBField(''           ,'date_edit'          ,DB_DATETIME));
    $this->coDataSet->addFWDDBField(new FWDDBField(''           ,'area_responsible_id',DB_NUMBER  ));
  }

  public function setUserId($piUserId){
    $this->ciUserId = $piUserId;
  }

  public function setName($psName){
    $this->csName = $psName;
  }

  public function setNameDescription($psNameDescription){
    $this->csNameDescription = $psNameDescription;
  }

  public function setResponsible($piResponsible){
    $this->ciResponsible = $piResponsible;
  }

  public function setValueFilter($paValueFilter){
    $this->caValueFilter = $paValueFilter;
  }

  public function setState($piState){
    $this->ciState = $piState;
  }

  public function setMaxProcesses($piMaxProcesses){
    $this->ciMaxProcesses = $piMaxProcesses;
  }

  public function setMinProcesses($piMinProcesses){
    $this->ciMinProcesses = $piMinProcesses;
  }

  public function setMaxSubAreas($piMaxSubAreas){
    $this->ciMaxSubAreas = $piMaxSubAreas;
  }

  public function setMinSubAreas($piMinSubAreas){
    $this->ciMinSubAreas = $piMinSubAreas;
  }

  public function setType($piType){
    $this->ciType = $piType;
  }

  public function setPriority($piPriority){
    $this->ciPriority = $piPriority;
  }

  public function setRootArea($piRootArea){
    $this->ciRootArea = $piRootArea;
  }

  public function makeQuery(){
    $maFilters = array();
    $maHavingFilters = array();
    
    if($this->ciUserId){
      if(!ISMSLib::userHasACL('M.RM.1.1')){
        if(ISMSLib::userHasACL('M.RM.1.2')){
          $msFunctionCall = FWDWebLib::getFunctionCall("get_areas_by_user({$this->ciUserId})");
        }else{
          $msFunctionCall = FWDWebLib::getFunctionCall("get_superareas_by_user({$this->ciUserId})");
        }
        $maFilters[] = "a.fkContext IN (SELECT * FROM {$msFunctionCall})";
      }
    }
    
    if($this->csName){
      $maFilters[] = FWDWebLib::getCaseInsensitiveLike('a.sName',$this->csName);
    }
    
    if($this->csNameDescription){
      $maFilters[] = '('
                        .FWDWebLib::getCaseInsensitiveLike('a.sName',$this->csNameDescription)
                        .' OR '
                        .FWDWebLib::getCaseInsensitiveLike('a.tDescription',$this->csNameDescription)
                    .')';
    }
    
    if($this->ciResponsible){
      $maFilters[] = "a.fkResponsible = {$this->ciResponsible}";
    }
    
    $miCountValueFilter = count($this->caValueFilter);
    if($miCountValueFilter > 0 && $miCountValueFilter < 4){
      $miRiskLow = ISMSLib::getConfigById(RISK_LOW);
      $miRiskHigh = ISMSLib::getConfigById(RISK_HIGH);
      $maValueFilters = array();
      foreach($this->caValueFilter as $msValueFilter){
        switch($msValueFilter){
          case 'not_estimated': $maValueFilters[] = "a.nValue = 0";                                         break;
          case 'low':           $maValueFilters[] = "a.nValue <= {$miRiskLow} AND a.nValue != 0";           break;
          case 'medium':        $maValueFilters[] = "a.nValue > {$miRiskLow} AND a.nValue < {$miRiskHigh}"; break;
          case 'high':          $maValueFilters[] = "a.nValue >= {$miRiskHigh}";                            break;
        }
      }
      $maFilters[] = '('.implode(' OR ',$maValueFilters).')';
    }
    
    if($this->ciState){
      $maFilters[] = "c.nState = {$this->ciState}";
    }
    
    if($this->ciMaxProcesses){
      $maHavingFilters[] = "COUNT(p.fkContext) <= {$this->ciMaxProcesses}";
    }
    
    if($this->ciMinProcesses){
      $maHavingFilters[] = "COUNT(p.fkContext) >= {$this->ciMinProcesses}";
    }
    
    if($this->ciMaxSubAreas){
      $maFilters[] = "sub.area_count <= {$this->ciMaxSubAreas}";
    }
    
    if($this->ciMinSubAreas){
      $maFilters[] = "sub.area_count >= {$this->ciMinSubAreas}";
    }
    
    if($this->ciType){
      $maFilters[] = "a.fkType = {$this->ciType}";
    }
    
    if($this->ciPriority){
      $maFilters[] = "a.fkPriority = {$this->ciPriority}";
    }
    
    if($this->ciRootArea){
      $maFilters[] = "a.fkParent = {$this->ciRootArea}";
    }else{
      $maFilters[] = "a.fkParent IS NULL";
    }
    
    if(count($maFilters)==0){
      $msWhere = '';
    }else{
      $msWhere = ' WHERE '.implode(' AND ',$maFilters);
    }
    if(count($maHavingFilters)==0){
      $msHaving = '';
    }else{
      $msHaving = ' HAVING '.implode(' AND ',$maHavingFilters);
    }
    $this->csSQL = "SELECT DISTINCT
                      a.fkContext AS area_id,
                      a.sName AS area_name,
                      u.sName AS area_responsible,
                      a.nValue AS area_value,
                      c.nState AS area_state,
                      COUNT(p.fkContext) AS area_process_count,
                      sub.area_count AS subarea_count,
                      ch.context_creator_name AS user_create_name,
                      ch.context_date_created AS date_create,
                      ch.context_modifier_name AS user_edit_name,
                      ch.context_date_modified AS date_edit,
                      a.fkResponsible AS area_responsible_id
                    FROM
                      rm_area a
                      JOIN isms_context c ON (c.pkContext = a.fkContext AND c.nState != ".CONTEXT_STATE_DELETED.")
                      JOIN isms_user u ON (u.fkContext = a.fkResponsible)
                      JOIN context_history ch ON (ch.context_id = c.pkContext)
                      LEFT JOIN view_rm_process_active p ON (p.fkArea = a.fkContext)
                      LEFT JOIN (
                        SELECT fkParent, COUNT(fkContext) AS area_count
                        FROM view_rm_area_active a
                        GROUP BY fkParent
                      ) sub ON (sub.fkParent = a.fkContext)
                    {$msWhere}
                    GROUP BY
                      a.fkContext,
                      a.sName,
                      u.sName,
                      a.nValue,
                      c.nState,
                      sub.area_count,
                      u.sName,
                      ch.context_creator_name,
                      ch.context_date_created,
                      ch.context_modifier_name,
                      ch.context_date_modified,
                      a.fkResponsible
                    {$msHaving}";
  }

}

?>