<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGridControl.
 *
 * <p>Consulta para popular a grid de controles.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryGridControl extends FWDDBQueryHandler {

  protected $ciUserId = 0;

  protected $ciRisk = 0;

  protected $csName = '';

  protected $csNameDescription = '';

  protected $ciMaxRisks = 0;

  protected $ciMinRisks = 0;
  
  protected $ciIsActive = 0;

  protected $ciState = 0;
  
  public function __construct($poDB=null){
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField('ctrl.fkContext'          ,'control_id'            ,DB_NUMBER  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('ctrl.sName'              ,'control_name'          ,DB_STRING  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('ctrl.bisactive'          ,'context_is_active'     ,DB_NUMBER  ));
    $this->coDataSet->addFWDDBField(new FWDDBField(''                        ,'control_cost'          ,DB_NUMBER  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('ctrl.fkResponsible'      ,'control_responsible_id',DB_NUMBER  ));
    $this->coDataSet->addFWDDBField(new FWDDBField(''                        ,'risk_count'            ,DB_NUMBER  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('ch.context_creator_name' ,'user_create_name'      ,DB_STRING  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('ch.context_date_created' ,'date_create'           ,DB_DATETIME));
    $this->coDataSet->addFWDDBField(new FWDDBField('ch.context_modifier_name','user_edit_name'        ,DB_STRING  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('ch.context_date_modified','date_edit'             ,DB_DATETIME));
    $this->coDataSet->addFWDDBField(new FWDDBField('c.nState'                ,'control_state'         ,DB_NUMBER  ));
  }

  public function setUserId($piUserId){
    $this->ciUserId = $piUserId;
  }

  public function setRisk($piRisk){
    $this->ciRisk = $piRisk;
  }

  public function setName($psName){
    $this->csName = $psName;
  }

  public function setNameDescription($psNameDescription){
    $this->csNameDescription = $psNameDescription;
  }

  public function setMaxRisks($piMaxRisks){
    $this->ciMaxRisks = $piMaxRisks;
  }

  public function setMinRisks($piMinRisks){
    $this->ciMinRisks = $piMinRisks;
  }

  public function setState($piState){
    $this->ciState = $piState;
  }

  public function setIsActive($piIsActive){
    $this->ciIsActive = $piIsActive;
  }

  public function makeQuery(){
    $maFilters = array();
    $maHavingFilters = array();
    
    if($this->ciUserId){
      /*
          Todos os controles que ele � respons�vel
          Todos os controles dos riscos dos ativos em que � respons�vel
          Todos os controles dos riscos dos ativos em que � respons�vel pela seguran�a do ativo
          Todos os controles dos riscos dos ativos relacionados aos processos em que � respons�vel
          Todos os controles dos riscos dos ativos relacionados aos processos das �reas em que � respons�vel,
          Todos os controles dos riscos dos ativos relacionados aos processos das sub-�reas das �reas em que � respons�vel
      */
      
      $msFunctionCall = FWDWebLib::getFunctionCall("get_areas_and_subareas_by_user({$this->ciUserId})");
      $maFilters[] = "(
                        ctrl.fkResponsible = {$this->ciUserId}
                        OR ctrl.fkContext IN (
                          SELECT r_c.fkControl
                            FROM view_rm_risk_control_active r_c
                            JOIN view_rm_risk_active r ON ( r_c.fkRisk = r.fkContext )
                            JOIN view_rm_asset_active a ON (r.fkAsset = a.fkContext)
                            WHERE   a.fkResponsible = {$this->ciUserId}
                                    OR a.fkSecurityResponsible = {$this->ciUserId}
                                    OR a.fkContext IN (
                                      SELECT p_a.fkAsset
                                      FROM view_rm_process_asset_active p_a 
                                      JOIN view_rm_process_active p ON (p.fkContext = p_a.fkProcess)
                                      WHERE  p.fkArea IN (SELECT area_id FROM {$msFunctionCall})
                                             OR p.fkResponsible = {$this->ciUserId}
                                    )
                        )
                      )";
    }
    
    if($this->ciRisk){
      $maFilters[] = "rc.fkRisk = {$this->ciRisk}";
    }
    
    if($this->csName){
      $maFilters[] = FWDWebLib::getCaseInsensitiveLike('ctrl.sName',$this->csName);
    }
    
    if($this->csNameDescription){
      $maFilters[] = '('
                        .FWDWebLib::getCaseInsensitiveLike('ctrl.sName',$this->csNameDescription)
                        .' OR '
                        .FWDWebLib::getCaseInsensitiveLike('ctrl.tDescription',$this->csNameDescription)
                    .')';
    }
    
    if($this->ciMaxRisks){
      $maHavingFilters[] = "COUNT(rc.fkRisk) <= {$this->ciMaxRisks}";
    }
    
    if($this->ciMinRisks){
      $maHavingFilters[] = "COUNT(rc.fkRisk) >= {$this->ciMinRisks}";
    }
    
    if($this->ciState){
      $maFilters[] = "c.nState = {$this->ciState}";
    }
    
    if($this->ciIsActive){
      $maFilters[] = "ctrl.bIsActive = ".($this->ciIsActive==1?1:0);
    }
    
    if(count($maFilters)==0){
      $msWhere = '';
    }else{
      $msWhere = ' WHERE '.implode(' AND ',$maFilters);
    }
    if(count($maHavingFilters)==0){
      $msHaving = '';
    }else{
      $msHaving = ' HAVING '.implode(' AND ',$maHavingFilters);
    }
    $this->csSQL = "SELECT
                      ctrl.fkContext AS control_id,
                      ctrl.sName AS control_name,
                      ctrl.bisactive AS context_is_active,
                      (SELECT CASE WHEN SUM(cc.nCost1+cc.nCost2+cc.nCost3+cc.nCost4+cc.nCost5) IS NULL THEN 0 ELSE SUM(cc.nCost1+cc.nCost2+cc.nCost3+cc.nCost4+cc.nCost5) END FROM rm_control_cost cc WHERE cc.fkControl = ctrl.fkContext) AS control_cost,
                      COUNT(rc.fkRisk) AS risk_count,
                      ctrl.fkResponsible AS control_responsible_id,
                      ch.context_creator_name AS user_create_name,
                      ch.context_date_created AS date_create,
                      ch.context_modifier_name AS user_edit_name,
                      ch.context_date_modified AS date_edit,
                      c.nState as control_state
                    FROM
                      isms_context c
                      JOIN rm_control ctrl ON (ctrl.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED.")
                      JOIN context_history ch ON (ch.context_id = c.pkContext)
                      LEFT JOIN view_rm_risk_control_active rc ON (rc.fkControl = ctrl.fkContext)
                    {$msWhere}
                    GROUP BY
                      ctrl.fkContext,
                      ctrl.sName,
                      ctrl.bisactive,
                      ch.context_creator_name,
                      ch.context_date_created,
                      ch.context_modifier_name,
                      ch.context_date_modified,
                      ctrl.fkResponsible,
                      c.nState
                    {$msHaving}";
  }

}

?>