<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGridAssetSearch.
 *
 * <p>Consulta para popular o grid de pesquisa de ativos.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryGridAssetSearch extends FWDDBQueryHandler {

  protected $csNameFilter = '';

  protected $ciCategoryId = 0;
  
  protected $ciProcessId = 0;
  
  protected $caIds = array();
  
  protected $caExcludedIds = array();
  
  protected $cbExcludeRelated = false;
  
  private $cbEmpty = false;

  public function __construct($poDB){
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField('','asset_id', DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('','asset_name', DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('','asset_state', DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('','asset_category_id', DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('','asset_category_name', DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('','asset_value', DB_NUMBER));
  }

  public function setName($psNameFilter){
    $this->csNameFilter = $psNameFilter;
  }

  public function setCategory($piCategoryId){
    $this->ciCategoryId = $piCategoryId;
  }
  
  public function setProcess($piProcessId){
    $this->ciProcessId = $piProcessId;
  }
  
  public function setIds($paIds){
    $this->caIds = $paIds;
  }
  
  public function setExcludedIds($paExcludedIds){
    $this->caExcludedIds = $paExcludedIds;
  }
  
  public function setExcludeRelated($pbExcludeRelated){
    $this->cbExcludeRelated = $pbExcludeRelated;
  }

  public function setEmpty($pbEmpty){
    $this->cbEmpty = $pbEmpty;
  }

  public function makeQuery(){
    $msWhere = '';
    
    if($this->cbEmpty){
      $msWhere.= " AND a.fkContext = 0";
    }
    
    if($this->csNameFilter!==''){
      $msWhere.= " AND upper(a.sName) like upper('%{$this->csNameFilter}%')";
    }
    if($this->ciCategoryId){
      $msWhere.= " AND cat.fkContext = {$this->ciCategoryId}";
    }
    // Filtro por ids
    if(count($this->caIds)){
      $msWhere .= " AND a.fkContext IN (".implode(',',$this->caIds).")";
    }
    // Filtro por ids (excluir da consulta)
    if(count($this->caExcludedIds)){
      $msWhere .= " AND a.fkContext NOT IN (".implode(',',$this->caExcludedIds).")";
    }
    // Filtro por processo
    if($this->ciProcessId){
      $msWhere .= " AND a.fkContext IN (
        SELECT fkAsset
        FROM rm_process_asset
        WHERE fkProcess = {$this->ciProcessId}
      )";
    }
    
    $maSelects = array();
    if($this->cbExcludeRelated && count($this->caExcludedIds)>0){
      foreach($this->caExcludedIds as $miExcludedId){
        $maSelects[]= "SELECT DISTINCT pkAsset FROM ".FWDWebLib::getFunctionCall("get_asset_dependencies(".$miExcludedId.")");
        $maSelects[]= "SELECT DISTINCT pkAsset FROM ".FWDWebLib::getFunctionCall("get_asset_dependents(".$miExcludedId.")");
      }
      $msWhere.= " AND a.fkContext NOT IN (".implode(' UNION ',$maSelects).")";
    }

    $this->csSQL = "
SELECT a.fkContext as asset_id,
       a.sName as asset_name,
       a.nValue as asset_value,
       c.nState as asset_state,
       cat.fkContext as asset_category_id,
       cat.sName as asset_category_name 
  FROM rm_asset a
       JOIN isms_context c ON (c.pkContext = a.fkContext AND c.nState <> ".CONTEXT_STATE_DELETED." ), 
       rm_category cat
  WHERE a.fkCategory = cat.fkContext 
        $msWhere
";
  }
}
?>