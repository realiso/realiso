<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGridTrash.
 *
 * <p>Consulta para popular o grid de lixo do sistema.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryGridTrash extends FWDDBQueryHandler {
  
  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField("trash.fkContext",   "context_id",       DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("trash.sName",       "context_name",     DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("trash.sType",       "context_type",     DB_NUMBER));
  }
  
  public function makeQuery() {
    $this->csSQL = "SELECT context_id, context_name, context_type
                    FROM context_names
                    WHERE context_state = ".CONTEXT_STATE_DELETED;
    $this->coDataSet->setOrderBy("context_type","-");
  }
}
?>