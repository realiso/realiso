<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGridIncidentSearch.
 *
 * <p>Consulta para popular o grid de pesquisa de incidentes.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryGridIncidentSearch extends FWDDBQueryHandler {

  protected $csName = '';

  public function __construct($poDB){
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField('','incident_id', DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('','incident_name', DB_STRING));
  }

  public function setName($psName){
    $this->csName = trim($psName);
  }

  public function makeQuery(){
    $this->maFilters = array();
    $msFrom = 'view_ci_incident_active i';
    
    // Filtro por nome
    if($this->csName){
      $this->maFilters[] = "upper(i.sName) like upper('%".$this->csName."%')";
    }
    
    /* 
      restri��o que existe na nav de incidente, quando o incidente foi resolvido ele n�o pode ser mais editado,
       assim, n�o pode mais relacionar ocorr�ncias com o incidente
    */
    $this->maFilters[] = "c.nState <> ".CONTEXT_STATE_INC_SOLVED;
    
    // Monta a query
    $this->csSQL = "SELECT i.fkContext as incident_id, 
                           i.sName as incident_name
                    FROM $msFrom
                    JOIN isms_context c ON (c.pkContext = i.fkContext)
                    ";
                    
    if(count($this->maFilters) > 0){
      $this->csSQL.= ' WHERE '.implode(' AND ', $this->maFilters);
    }
  }
}
?>