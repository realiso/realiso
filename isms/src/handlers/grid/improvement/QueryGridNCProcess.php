<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe QueryGridNCProcess
 *
 * <p>Consulta para popular a grid de processos de n�o conformidades.</p>
 * @package ISMS
 * @subpackage classes
 */
class QueryGridNCProcess extends FWDDBQueryHandler {

  protected $csNameFilter;
  protected $ciAreaFilter = 0;
  protected $caExcludedIds = array();
  protected $caShowIds = array();
  protected $cbShowIds = false;
  protected $cbShowFree = false;
  protected $ciNCId = 0;

  /**
  * Construtor.
  * 
  * <p>Construtor da classe.</p>
  * @access public 
  */
  public function __construct($poDB){
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField('np.fkProcess',  'process_id',         DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('p.sName',       'process_name',       DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('a.sName',       'process_area_name',  DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('p.nValue',      'process_value',      DB_NUMBER));
  }
  
  public function makeQuery(){
    $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    $maFilters = array();
    if ($this->csNameFilter) {
      $maFilters[] = "p.sName like '%".$this->csNameFilter."%'";
    }
    if (count($this->caExcludedIds)) {
      $maFilters[] = "p.fkContext NOT IN (".implode(',',$this->caExcludedIds).")";
    }
    if ($this->cbShowIds) {
      $maFilters[] = "p.fkContext IN (".(count($this->caShowIds)?implode(',',$this->caShowIds):0).")";
    }
    if (intval($this->ciNCId) > 0) {
      $maFilters[] = "np.fkNC = {$this->ciNCId}";
    }
    if($this->ciAreaFilter){
      $maFilters[] = "a.fkContext = ".$this->ciAreaFilter;
    }
    
    $msWhere = count($maFilters)?" WHERE ".implode(" AND ",$maFilters):"";            
    $this->csSQL = "SELECT DISTINCT
                      p.fkContext as process_id, 
                      p.sName as process_name,
                      p.nValue as process_value,
                      a.sName as process_area_name
                    FROM
                      view_rm_process_active p
                      LEFT JOIN ci_nc_process np ON (np.fkProcess = p.fkContext)
                      JOIN isms_context ctx ON (p.fkContext=ctx.pkContext)
                      JOIN rm_area a ON (p.fkArea = a.fkContext)
                    $msWhere";
  }

  public function setNameFilter($psNameFilter) {
    $this->csNameFilter = $psNameFilter;
  }

  public function setAreaFilter($piAreaFilter) {
    $this->ciAreaFilter = $piAreaFilter;
  }

  public function setExcluded($paExcludedIds){
    $this->caExcludedIds = $paExcludedIds;
  }
  
  public function showProcesses($paIds) {
    $this->cbShowIds = true;
    $this->caShowIds = $paIds;
  }
  
  public function setNC($piNCId) {
    $this->ciNCId = $piNCId;
  }
  
  
  public function getSelectProcesses(){
    $this->makeQuery();
    if($this->executeQuery()){
      $maReturn = array();
      $moDataSet = $this->getDataset();
      while($moDataSet->fetch()){
        $maReturn[] = array($moDataSet->getFieldByAlias('process_id')->getValue(),$moDataSet->getFieldByAlias('process_name')->getValue());
      }
      return $maReturn;
    }
    else
      return array();
  } 
}

?>