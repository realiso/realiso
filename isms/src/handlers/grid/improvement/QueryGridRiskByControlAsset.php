<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGridIncidentControlAssetSearch.
 *
 * <p>Consulta para popular o grid do filtro de riscos por controle e ativo.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryGridRiskByControlAsset extends FWDDBQueryHandler {  
  
  protected $csControlIds = "";
  
  protected $csAssetIds = "";
  
  protected $ciRiskIdsIN = "";
  
  protected $ciRiskIdsNotIN = "";
  public function __construct($poDB) {
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField("fkContext", "context_id",     DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("sName",     "context_name",   DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("",          "context_value",  DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("a.sName",   "asset_name",     DB_STRING));
  }
  
  public function setControlIds($psControlIds) {
    $this->csControlIds = $psControlIds;
  }
  
  public function setAssetIds($psAssetIds){
    $this->csAssetIds = $psAssetIds;
  
  }

  public function setRiskIdsIN($psRiskIds){
    $this->ciRiskIdsIN = $psRiskIds;
  }
  
  public function setRiskIdsNotIN($psRiskIds){
    $this->ciRiskIdsNotIN = $psRiskIds;
  }
  
    
  public function makeQuery() {
    $msWhereControl = "";
    $msWhereAsset = "";
    $msWhereRiskRC = "";
    $msWhereRiskNotRC = "";
    $msWhereRisk = "";
    $msWhereRiskNot = "";
    $msSQLRC = "";
    $msSQLRisk = "";
    
    $msRiskIdsIN = implode(',',array_filter(explode(':',$this->ciRiskIdsIN)));
    if($msRiskIdsIN)
      $msWhereRiskRC = " AND rc.fkRisk IN ($msRiskIdsIN)";

    $msAuxRiskIdsNotIN = implode(',',array_filter(explode(':',$this->ciRiskIdsNotIN)));
    if($msAuxRiskIdsNotIN)
      $msWhereRiskNotRC = " AND rc.fkRisk NOT IN ($msAuxRiskIdsNotIN)";


    $msAuxControlIds = implode(',',array_filter(explode(':',$this->csControlIds)));
    if($msAuxControlIds){
      $msWhereControl = " AND rc.fkControl IN ($msAuxControlIds)";
  
      $msSQLRC = " SELECT r.fkContext as context_id, 
             r.sName as context_name,
             r.nValueResidual as context_value,
             a.sName as asset_name
        FROM view_rm_risk_active r
        JOIN view_rm_risk_control_active rc ON (r.fkContext = rc.fkRisk $msWhereControl $msWhereRiskRC $msWhereRiskNotRC)
        JOIN view_rm_asset_active a ON (r.fkAsset = a.fkContext)
      ";
    }

    $msAuxAssetIds = implode(',',array_filter(explode(':',$this->csAssetIds)));
    if($msAuxAssetIds)
      $msWhereAsset = " WHERE fkAsset IN ($msAuxAssetIds)";
    if($msWhereAsset){
      if($msRiskIdsIN)
          $msWhereRisk = " AND r.fkContext IN ($msRiskIdsIN)";
       
      if($msAuxRiskIdsNotIN)
        $msWhereRiskNot = " AND r.fkContext NOT IN ($msAuxRiskIdsNotIN)";
      $msSQLRisk = "
        SELECT r.fkContext as context_id, 
               r.sName as context_name,
               r.nValueResidual as context_value,
               a.sName as asset_name
          FROM view_rm_risk_active r
          JOIN view_rm_asset_active a ON (r.fkAsset = a.fkContext)
        $msWhereAsset
        $msWhereRisk
        $msWhereRiskNot
      ";
    }

    if($msSQLRC && $msSQLRisk){
      $this->csSQL = $msSQLRC . " UNION " . $msSQLRisk;
    }else{
      $this->csSQL = $msSQLRC . $msSQLRisk;
    }
  }
}
?>