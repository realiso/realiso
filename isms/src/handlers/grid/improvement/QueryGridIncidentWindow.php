<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe QueryGridIncidentWindow
 *
 * <p>Consulta para popular a grid de janela de incidentes de um risco.</p>
 * @package ISMS
 * @subpackage classes
 */
class QueryGridIncidentWindow extends FWDDBQueryHandler {

  protected $ciRiskId = 0;
  protected $csInitialDate = '';
  protected $csFinalDate = '';

  /**
  * Construtor.
  * 
  * <p>Construtor da classe.</p>
  * @access public 
  */
  public function __construct($poDB){
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField('i.fkContext','incident_id',  DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('i.sName',    'incident_name',DB_STRING));
  }
  
  public function setRiskId($piRiskId){
    $this->ciRiskId = $piRiskId;
  }

  public function setInitialDate($piInitialDate){
  	$maDate = getdate($piInitialDate);
  	$miDate = mktime(0, 0, 0, $maDate['mon'], $maDate['mday'], $maDate['year']);                 
    $this->csInitialDate = ISMSLib::getTimestampFormat($miDate,true);
  }

  public function setFinalDate($piFinalDate){
  	$maDate = getdate($piFinalDate);
  	$miDate = mktime(0, 0, 0, $maDate['mon'], $maDate['mday'], $maDate['year']);
    $this->csFinalDate = ISMSLib::getTimestampFormat($miDate,true);
  }

  public function makeQuery(){
    $maFilters = array();

		$maFilters[] = "ir.fkRisk = {$this->ciRiskId}";

    if($this->csInitialDate){
      $maFilters[] = "i.dDate >= {$this->csInitialDate}";
    }

    if($this->csFinalDate){
      $maFilters[] = "i.dDate <= {$this->csFinalDate}";
    }
    
    $msWhere = count($maFilters)?" WHERE ".implode(" AND ",$maFilters):"";
    $this->csSQL = "SELECT
                      i.fkContext as incident_id,
                      i.sName as incident_name
                    FROM
                      view_ci_incident_active i
                      JOIN ci_incident_risk ir ON (ir.fkIncident = i.fkContext)                      
                    $msWhere
                    ";
  }

}

?>