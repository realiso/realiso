<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe QueryGridCategory
 *
 * <p>Consulta para popular a grid de categorias.</p>
 * @package ISMS
 * @subpackage classes
 */
class QueryGridCategory extends FWDDBQueryHandler {

  /**
  * Construtor.
  * 
  * <p>Construtor da classe.</p>
  * @access public 
  */
  public function __construct($poDB){
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField('c.fkContext',  'category_id',    DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('c.sName',      'category_name',  DB_NUMBER));

    $this->coDataSet->addFWDDBField(new FWDDBField('ch.context_creator_name',   'user_create_name', DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('ch.context_date_created',   'date_create',      DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('ch.context_modifier_name',  'user_edit_name',   DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('ch.context_date_modified',  'date_edit',        DB_STRING));
  }
  
  public function makeQuery(){
    $this->csSQL = "SELECT
                      c.fkContext as category_id,
                      c.sName as category_name,
                      ch.context_creator_name as user_create_name, 
                      ch.context_date_created as date_create, 
                      ch.context_modifier_name as user_edit_name, 
                      ch.context_date_modified as date_edit
                    FROM
                      view_ci_category_active c
                      JOIN context_history ch ON (c.fkContext = ch.context_id)
                    ";
  }

}

?>