<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe QueryGridDisciplinaryProcess
 *
 * <p>Consulta para popular a grid de processo disciplinar.</p>
 * @package ISMS
 * @subpackage classes
 */
class QueryGridDisciplinaryProcess extends FWDDBQueryHandler {

  protected $csUserName = "";
  
  protected $csIncidentName = "";

  /**
  * Construtor.
  * 
  * <p>Construtor da classe.</p>
  * @access public 
  */
  public function __construct($poDB){
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField('iu.fkIncident',    'incident_id',    DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('iu.fkUser',        'user_id',        DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('u.sName',          'user_name',      DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('i.sName',          'incident_name',  DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('iu.tDescription',  'description',    DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('iu.tActionTaken',  'action_taken',   DB_STRING));
    
    $this->coDataSet->addFWDDBField(new FWDDBField('ch.context_creator_name',   'user_create_name', DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('ch.context_date_created',   'date_create',      DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('ch.context_modifier_name',  'user_edit_name',   DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('ch.context_date_modified',  'date_edit',        DB_STRING));
  }
  
  public function setIncidentName($psIncidentName) {
    $this->csIncidentName = $psIncidentName;
  }
  
  public function setUserName($psUserName) {
    $this->csUserName = $psUserName;
  }
  
  public function makeQuery(){    
    $maFilters = array();
    
    if($this->csIncidentName){
      $maFilters[] =  "upper(i.sName) like upper('%{$this->csIncidentName}%')";
    }
    
    if($this->csUserName){
      $maFilters[] =  "upper(u.sName) like upper('%{$this->csUserName}%')";
    }
    
    $msWhere = "";
    if (count($maFilters)) $msWhere = "WHERE " . implode(' AND ', $maFilters);
    
    $this->csSQL = "SELECT
                      iu.fkIncident as incident_id,
                      iu.fkUser as user_id,
                      u.sName as user_name,
                      i.sName as incident_name,
                      iu.tDescription as description,
                      iu.tActionTaken as action_taken,
                      ch.context_creator_name as user_create_name, 
                      ch.context_date_created as date_create, 
                      ch.context_modifier_name as user_edit_name, 
                      ch.context_date_modified as date_edit
                    FROM
                      ci_incident_user iu
                      JOIN view_ci_incident_active i ON (iu.fkIncident = i.fkContext)
                      JOIN view_isms_user_active u ON (iu.fkUser = u.fkContext)
                      JOIN context_history ch ON (i.fkContext = ch.context_id)
                    $msWhere";
  }
}
?>