<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe QueryGridIncidentByControlId
 *
 * <p>Consulta para popular a grid de processo disciplinar.</p>
 * @package ISMS
 * @subpackage classes
 */
class QueryGridIncidentByControlId extends FWDDBQueryHandler {
  
  protected $ciControlId=0;
  protected $csDateBegin="";
  protected $csDateEnd="";
  /**
  * Construtor.
  * 
  * <p>Construtor da classe.</p>
  * @access public 
  */
  public function __construct($poDB){
    parent::__construct($poDB);

    $this->coDataSet->addFWDDBField(new FWDDBField('i.fkContext' ,'context_id'   ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('i.sName'     ,'context_name' ,DB_STRING));
  }
  
  public function makeQuery(){
    $this->csSQL = "
        SELECT i.fkContext as context_id,
               i.sName as context_name
          FROM ci_incident i
          JOIN ci_incident_control ic ON (i.fkContext = ic.fkIncident)
          JOIN rm_control c ON (c.fkContext = ic.fkControl AND c.fkContext = ". $this->ciControlId ." )
          WHERE i.dDate > ". $this->csDateBegin ." AND i.dDate < ".$this->csDateEnd;
  }

  public function setControlId($piControlId) {
    $this->ciControlId = $piControlId;
  }

  public function setDateBegin($piTimeStamp){
    $this->csDateBegin = ISMSLib::getTimestampFormat(mktime(0, 0, 0, date("m",$piTimeStamp), date("d",$piTimeStamp),  date("Y",$piTimeStamp)));
  }
  
  public function setDateEnd($piTimeStamp){
    $this->csDateEnd = ISMSLib::getTimestampFormat(mktime(0, 0, 0, date("m",$piTimeStamp), date("d",$piTimeStamp),  date("Y",$piTimeStamp)));
  }
  
  public function getCountQuery(){
    $this->makeQuery();
    $miReturn = 0;
    if($this->executeQuery()){
      $moDataSet = $this->getDataset();
      while($moDataSet->fetch()){
        $miReturn++;
      }
    }
    return $miReturn;
  } 
}

?>