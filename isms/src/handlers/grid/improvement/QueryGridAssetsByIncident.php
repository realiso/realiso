<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe QueryGridAssetsByIncident
 *
 * <p>Consulta para popular a grid de ativos do incidente.</p>
 * @package ISMS
 * @subpackage classes
 */
class QueryGridAssetsByIncident extends FWDDBQueryHandler {

  protected $ciIncident;

  /**
  * Construtor.
  * 
  * <p>Construtor da classe.</p>
  * @access public 
  */
  public function __construct($poDB){
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField('a.fkContext'           ,'context_id'                  ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('a.sName'               ,'context_name'                ,DB_STRING));
  }
  
  public function setIncident($piId){
    $this->ciIncident = $piId;
  }
  public function makeQuery(){
    $maFilters = array();
    if ($this->ciIncident) {
      $maFilters[] = "ir.fkIncident = " . $this->ciIncident; 
    }

    $msWhere = count($maFilters)?" WHERE ".implode(" AND ",$maFilters):"";
    $this->csSQL = "
SELECT a.fkContext as context_id, 
       a.sName as context_name 
  FROM view_rm_asset_active a
       JOIN view_rm_risk_active r ON (a.fkContext = r.fkAsset)
       JOIN view_ci_incident_risk_active ir ON (r.fkContext = ir.fkRisk)
  $msWhere";
  }
}

?>