<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe QueryGridOccurrence
 *
 * <p>Consulta para popular a grid de ocorrências.</p>
 * @package ISMS
 * @subpackage classes
 */
class QueryGridOccurrence extends FWDDBQueryHandler {

  protected $caShowIds=array();
  protected $csDescription="";
  protected $caExcludedIds = array();
  protected $cbShowFreeOnly = false;
  protected $cbShowDenied = true;
  protected $ciIncidentId=0;
  
  /**
  * Construtor.
  * 
  * <p>Construtor da classe.</p>
  * @access public 
  */
  public function __construct($poDB){
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField('o.fkContext',     'occurrence_id',           DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('o.fkContext',     'occurrence_id_show',           DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('o.fkIncident',    'incident_id',             DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('o.tDescription',  'occurrence_description',  DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('o.dDate',      		'occurrence_date',         DB_DATETIME));
    $this->coDataSet->addFWDDBField(new FWDDBField('u.sName',         'user_name',               DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('i.sName',         'incident_name',           DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('ctx.nState',      'occurrence_state',        DB_STRING));
    
    $this->coDataSet->addFWDDBField(new FWDDBField('ch.context_creator_name',   'user_create_name', DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('ch.context_date_created',   'date_create',      DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('ch.context_modifier_name',  'user_edit_name',   DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('ch.context_date_modified',  'date_edit',        DB_STRING));
    
    $this->coDataSet->addFWDDBField(new FWDDBField('t.pkTask'       ,'task_id'                ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('t.fkReceiver'   ,'task_receiver'          ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('ctxd.nUserId'   ,'user_create_id'         ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('i.fkContext'    ,'incident_id'           ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('ctx2.nState,'   ,'incident_state'        ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('i.fkResponsible','incident_responsible_id',DB_NUMBER));
  }
  
  public function makeQuery(){
    $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    $maFilters = array();
    $maFilters[] = "(o.fkIncident IS NULL OR ctx2.nState != '".CONTEXT_STATE_DELETED."')"; 
    if ($this->csDescription) {
      $maFilters[] = "(o.tDescription like '%".$this->csDescription."%')";
    }
    if (count($this->caShowIds)) {
      $maFilters[] = "(o.fkContext IN (".implode(',',$this->caShowIds)."))";
    }
    if (count($this->caExcludedIds)) {
      $maFilters[] = "(o.fkContext NOT IN (".implode(',',$this->caExcludedIds)."))";
    }
    if ($this->cbShowFreeOnly) {
      $maFilters[] = "(o.fkIncident IS NULL AND ctx.nState = '".CONTEXT_STATE_APPROVED."')";
    }
    if (intval($this->ciIncidentId) > 0) {
      $maFilters[] = "(o.fkIncident = {$this->ciIncidentId})";
    }
    
    /*verifica permissão de listar todos*/
    $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
    if(in_array('M.CI.1.1',$maACLs)){
      $msUserIN = " AND u.fkContext = ".$miUserId;
    }else{
      $msUserIN = "";
    }
    
    $msWhere = count($maFilters)?" WHERE ".implode(" AND ",$maFilters):"";
    $this->csSQL = "SELECT
                      o.fkContext as occurrence_id,
                      o.fkContext as occurrence_id_show,
                      o.tDescription as occurrence_description,
                      o.dDate as occurrence_date,
                      u.sName as user_name,
                      i.sName as incident_name,
                      ctx.nState as occurrence_state,
                      ch.context_creator_name as user_create_name, 
                      ch.context_date_created as date_create, 
                      ch.context_modifier_name as user_edit_name, 
                      ch.context_date_modified as date_edit,
                      t.pkTask as task_id,
                      t.fkReceiver as task_receiver,
                      ctxd.nUserId as user_create_id,
                      i.fkContext as incident_id,
                      ctx2.nState as incident_state,
                      i.fkResponsible as incident_responsible_id
                    FROM
                      view_ci_occurrence_active o
                      JOIN isms_context ctx ON (o.fkContext=ctx.pkContext)
                      JOIN isms_context_date ctxd ON (ctx.pkContext = ctxd.fkContext AND ctxd.nAction = '".ACTION_CREATE."')
                      JOIN view_isms_user_active u ON (ctxd.nUserId = u.fkContext $msUserIN)
                      JOIN context_history ch ON (o.fkContext = ch.context_id)
                      LEFT JOIN view_ci_incident_active i ON (o.fkIncident = i.fkContext)
                      LEFT JOIN view_isms_context_active ctx2 ON (ctx2.pkContext = i.fkContext)
                      LEFT JOIN wkf_task t ON (t.fkContext=o.fkContext AND t.bVisible=1) 
                    $msWhere";
  }
  
  public function showOccurrences($paIds) {
    $this->caShowIds = $paIds;
  }
  
  public function setDescription($psDescription) {
    $this->csDescription = $psDescription;
  }
  
  public function setExcluded($paIds) {
    $this->caExcludedIds = $paIds;
  }
  
  public function showFreeOnly() {
    $this->cbShowFreeOnly = true;
  }
  
  public function setIncident($piIncidentId) {
    $this->ciIncidentId = $piIncidentId;
  }
  
  public function getSelectOccurrences(){
    $this->makeQuery();
    if($this->executeQuery()){
      $maReturn = array();
      $moDataSet = $this->getDataset();
      while($moDataSet->fetch()){
        $maReturn[] = array($moDataSet->getFieldByAlias('occurrence_id')->getValue(),str_replace(array("\n","\r"),array(' ',''),$moDataSet->getFieldByAlias('occurrence_description')->getValue()));
      }
      return $maReturn;
    }
    else
      return array();
  } 
}

?>