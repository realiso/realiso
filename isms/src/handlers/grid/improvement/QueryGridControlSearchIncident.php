<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGridControlSearchIncident.
 *
 * <p>Consulta para popular o grid de pesquisa de controles utilisada pelo modulo de incident.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryGridControlSearchIncident extends FWDDBQueryHandler {  
  
  protected $csName = "";
  
  protected $csIds = "";
  
  protected $csIdsNot = "";
  
  public function __construct($poDB) {
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField("fkContext", "control_id",   DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("sName",     "control_name", DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("bisactive",     "control_active", DB_NUMBER));
  }
  
  public function setName($psName) {
    $this->csName = $psName ? "upper(sName) like upper('%$psName%')" : "";
  }
  
  public function setIds($psIds){
    $this->csIds = $psIds;
  
  }
  
  public function setIdsNot($psIds){
    $this->csIdsNot = $psIds;
  }
  
  public function makeQuery() {
    $msWhere = '';
    if($this->csName)
      $msWhere .= " WHERE ".$this->csName;
    if($this->csIds){
      $msWhere = (($msWhere)? $msWhere . " AND  " : " WHERE ");
      $msWhere .= "fkContext IN (".implode(',',array_filter(explode(':',$this->csIds))).") ";
    }
    if($this->csIdsNot){
      $msWhere = (($msWhere)? $msWhere . " AND  " : " WHERE ");
      $msWhere .= "fkContext NOT IN (".implode(',',array_filter(explode(':',$this->csIdsNot))).") ";
    }
    
    $this->csSQL = "
SELECT fkContext as control_id, 
       sName as control_name, 
       bisactive as control_active
  FROM view_rm_control_active c
  $msWhere
";
  }
}
?>