<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGridActionPlan.
 *
 * <p></p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryGridActionPlan extends FWDDBQueryHandler {
  
  protected $ciNC = 0;
  
  public function __construct($poDB=null){
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField('acpl.fkcontext'              ,'ap_id'                    ,DB_NUMBER  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('u.sname'                     ,'ap_responsible'           ,DB_NUMBER  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('acpl.sname'                  ,'ap_name'                  ,DB_STRING  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('acpl.nactiontype'            ,'ap_actiontype'            ,DB_NUMBER  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('acpl.ddatedeadline'          ,'ap_datedeadline'          ,DB_DATETIME));
    $this->coDataSet->addFWDDBField(new FWDDBField('acpl.ddateconclusion'        ,'ap_dateconclusion'        ,DB_DATETIME));
    $this->coDataSet->addFWDDBField(new FWDDBField('acpl.bisefficient'           ,'ap_isefficient'           ,DB_NUMBER  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('acpl.ddateefficiencyrevision','ap_dateefficiencyrevision',DB_DATETIME));
    $this->coDataSet->addFWDDBField(new FWDDBField('acpl.ddateefficiencymeasured','ap_dateefficiencymeasured',DB_DATETIME));
    $this->coDataSet->addFWDDBField(new FWDDBField('ctx.nstate'                  ,'ap_state'                 ,DB_NUMBER  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('ch.context_creator_name'     ,'creator_name'             ,DB_STRING  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('ch.context_date_created'     ,'date_created'             ,DB_DATETIME));
    $this->coDataSet->addFWDDBField(new FWDDBField('ch.context_modifier_name'    ,'modifier_name'            ,DB_STRING  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('ch.context_date_modified'    ,'date_modified'            ,DB_DATETIME));
    $this->coDataSet->addFWDDBField(new FWDDBField('t.pkTask'                    ,'task_id'                  ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('t.fkReceiver'                ,'task_receiver'            ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField(''                            ,'count_nc'                 ,DB_NUMBER));
  }

  public function setNC($piNC){
    $this->ciNC = $piNC;
  }

  public function makeQuery(){
    
    $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    /*verifica permiss�o de listar todos*/
    $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
    $maFilters = array();
    if(in_array('M.CI.7.1',$maACLs)){
      $maFilters[] =" acpl.fkResponsible = ".$miUserId;
    }
    
    if($this->ciNC){
      $msNCJoin = " JOIN ci_nc_action_plan ncap ON (ncap.fkActionPlan = acpl.fkContext) ";
      $msNCJoin .=" JOIN view_ci_nc_active nc ON (nc.fkContext = ncap.fkNC AND  ncap.fkNc = ".$this->ciNC.")";
    }else{
      $msNCJoin = '';
    }
    
    $msWhere = count($maFilters)?" WHERE ".implode(" AND ",$maFilters):"";


    $this->csSQL = "SELECT
                      acpl.fkcontext AS ap_id,
                      u.sname AS ap_responsible,
                      acpl.sname AS ap_name,
                      acpl.nactiontype AS ap_actiontype,
                      acpl.ddatedeadline AS ap_datedeadline,
                      acpl.ddateconclusion AS ap_dateconclusion,
                      acpl.bisefficient AS ap_isefficient,
                      acpl.ddateefficiencyrevision AS ap_dateefficiencyrevision,
                      acpl.ddateefficiencymeasured AS ap_dateefficiencymeasured,
                      ctx.nstate AS ap_state,
                      ch.context_creator_name AS creator_name,
                      ch.context_date_created AS date_created,
                      ch.context_modifier_name AS modifier_name,
                      ch.context_date_modified AS date_modified,
                      t.pkTask AS task_id,
                      t.fkReceiver AS task_receiver,
                      (SELECT count (ncap2.fkNC) FROM ci_nc_action_plan ncap2 JOIN view_ci_nc_active nc ON (ncap2.fkNC = nc.fkContext) WHERE ncap2.fkActionPlan = acpl.fkContext) as count_nc
                    FROM
                      view_ci_action_plan_active acpl
                      JOIN isms_user u ON (u.fkcontext = acpl.fkresponsible)
                      JOIN isms_context ctx ON (ctx.pkcontext = acpl.fkcontext)
                      JOIN context_history ch ON (ch.context_id = acpl.fkcontext)
                      LEFT JOIN wkf_task t ON (t.fkContext=acpl.fkContext AND t.bVisible=1) 
                      $msNCJoin
                      $msWhere
                      ";
  }

}

?>