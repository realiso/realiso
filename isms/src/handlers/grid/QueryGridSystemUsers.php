<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QuerySystemGridUsers.
 *
 * <p>Consulta para popular o grid de usu�rios do sistema.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryGridSystemUsers extends FWDDBQueryHandler {  

  public function __construct($poDB) {
    parent::__construct($poDB);

    $this->coDataSet->addFWDDBField(new FWDDBField("u.fkContext", "user_id",        DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("u.sName",     "user_name",      DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("u.sEmail",    "user_email",     DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("p.sName",     "profile_name",   DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("u.bIsBlocked","user_is_blocked",DB_NUMBER));

    $this->coDataSet->addFWDDBField(new FWDDBField("ch.context_creator_name","user_create_name", DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("ch.context_date_created","date_create", DB_DATETIME));
    $this->coDataSet->addFWDDBField(new FWDDBField("ch.context_modifier_name","user_edit_name", DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("ch.context_date_modified","date_edit", DB_DATETIME));

  }

  public function makeQuery() {
    $this->csSQL = "
SELECT 
  u.fkContext as user_id, 
  u.sName as user_name, 
  u.sEmail as user_email, 
  p.sName as profile_name,
  u.bIsBlocked as user_is_blocked,
  ch.context_creator_name as user_create_name, 
  ch.context_date_created as date_create, 
  ch.context_modifier_name as user_edit_name, 
  ch.context_date_modified as date_edit

  FROM isms_user u
  JOIN isms_context ctx ON (u.fkContext = ctx.pkContext AND ctx.nState != ".CONTEXT_STATE_DELETED.")
  JOIN isms_profile p ON (u.fkProfile = p.fkContext)
  JOIN context_history ch ON (u.fkContext = ch.context_id)
";
  }
}
?>