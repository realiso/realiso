<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGridDocuments
 *
 * <p>Consulta para popular as grids de documentos.</p>
 * @package ISMS
 * @subpackage classes
 */
class QueryGridDocuments extends FWDDBQueryHandler {

	protected $ciDocumentClassification;
	protected $ciDocumentStatus;
	protected $caTypesToShow = array();
	protected $caTypesToHide = array();
	protected $ciDocumentId = 0;
	protected $ciContextId = 0;

	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe.</p>
	 * @access public
	 */
	public function __construct($poDB){
		parent::__construct($poDB);

		$this->coDataSet->addFWDDBField(new FWDDBField('d.fkContext'             ,'document_id'               ,DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField(''                        ,'document_instance_id'      ,DB_UNKNOWN));
		$this->coDataSet->addFWDDBField(new FWDDBField('d.fkClassification'      ,'document_classification'   ,DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField('d.fkAuthor'              ,'document_author'           ,DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField('d.fkMainApprover'        ,'document_main_approver'    ,DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField(''                        ,'user_has_approved'         ,DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField('d.sName'                 ,'document_name'             ,DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField('d.fkCurrentVersion'      ,'document_current_version'  ,DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField('di.sFileName'            ,'document_file_name'        ,DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField('di.sPath'                ,'document_file_path'        ,DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField('di.bIsLink'              ,'document_is_link'          ,DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField('di.sLink'                ,'document_link'             ,DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField('di.sManualVersion'       ,'document_version'          ,DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField('di.nMajorVersion'        ,'document_major_version'    ,DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField('di.nRevisionVersion'     ,'document_revision_version' ,DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField('c.nState'                ,'document_status'           ,DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField('dis.doc_instance_status' ,'doc_instance_status'       ,DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField('d.nType'                 ,'document_type'             ,DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField('d.dDateProduction'       ,'document_date_production'  ,DB_DATETIME));
		$this->coDataSet->addFWDDBField(new FWDDBField(''                        ,'user_is_approver'          ,DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField(''                        ,'user_is_reader'            ,DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField(''                        ,'document_is_released'      ,DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField('u.sName'                 ,'responsible_name'          ,DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField(''                        ,'count_sub_doc'             ,DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField(''                        ,'count_approvers'           ,DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField(''                        ,'context_name'              ,DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField('d.fkParent'              ,'document_parent'           ,DB_NUMBER));
	}

	public function setDocumentClassification($piClassification){
		$this->ciDocumentClassification = $piClassification;
	}

	public function setDocumentStatus($piStatus){
		$this->ciDocumentStatus = $piStatus;
	}

	public function setTypesToShow($piType){
		$this->caTypesToShow = func_get_args();
	}

	public function setTypesToHide($piType){
		$this->caTypesToHide = func_get_args();
	}

	public function setDocumentId($piId){
		$this->ciDocumentId = $piId;
	}

	public function setContextId($piId){
		$this->ciContextId = $piId;
	}

	public function makeQuery(){
		$miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
		$maFilters = array();
		// Pega s� os documentos que o usu�rio pode ver
		$maFilters[] = "(
      d.fkMainApprover = {$miUserId}
      OR d.fkAuthor = {$miUserId}
      OR pu.fkuser = {$miUserId}
      OR cfg.svalue = '{$miUserId}'
      OR (
        (
          dis.doc_instance_status = ".CONTEXT_STATE_DOC_PUBLISHED."
          OR dis.doc_instance_status = ".CONTEXT_STATE_DOC_OBSOLETE."
        )
        AND (
          dr.fkDocument IS NOT NULL
          OR da.fkDocument IS NOT NULL
        )
      )
      OR (
        da.fkDocument IS NOT NULL
        AND (
          dis.doc_instance_status = ".CONTEXT_STATE_DOC_APPROVAL."
          OR dis.doc_instance_status = ".CONTEXT_STATE_DOC_TO_BE_PUBLISHED."
        )
      )
    )";

		if(count($this->caTypesToShow)){
			$maTypesToShow = array_diff($this->caTypesToShow,$this->caTypesToHide);
			$maFilters[] = "d.nType IN (".implode(',',$maTypesToShow).")";
		}elseif(count($this->caTypesToHide)){
			$maFilters[] = "d.nType NOT IN (".implode(',',$this->caTypesToHide).")";
		}

		if($this->ciDocumentClassification){
			$maFilters[] = 'd.fkClassification = '.$this->ciDocumentClassification;
		}

		if($this->ciDocumentStatus){
			$maFilters[] = "dis.doc_instance_status = {$this->ciDocumentStatus}";
		}

		if($this->ciDocumentId){
			$maFilters[] = 'd.fkContext = '.$this->ciDocumentId;
		}

		if ($this->ciContextId){
			$maFilters[] = 'dc.fkContext = '.$this->ciContextId;
		}

		$msWhere = '';
		if(count($maFilters)){
			$msWhere = " WHERE ".implode(' AND ', $maFilters);
		}
		$this->csSQL = "SELECT DISTINCT
                      d.fkContext AS document_id,
                      dis.doc_instance_id AS document_instance_id,
                      d.fkClassification AS document_classification,
                      d.fkAuthor AS document_author,
                      d.fkMainApprover AS document_main_approver,
                      CASE
                        WHEN d.fkMainApprover = {$miUserId} THEN d.bHasApproved
                        WHEN da.bHasApproved IS NOT NULL THEN da.bHasApproved
                        ELSE NULL
                      END AS user_has_approved,
                      d.sName AS document_name,
                      d.fkCurrentVersion AS document_current_version,
                      di.sFileName AS document_file_name,
                      di.sPath AS document_file_path,
                      di.bIsLink AS document_is_link,
                      di.sLink AS document_link,
                      di.sManualVersion AS document_version,
                      di.nMajorVersion as document_major_version,
                      di.nRevisionVersion as document_revision_version,
                      c.nState AS document_status,
                      dis.doc_instance_status AS doc_instance_status,
                      d.nType AS document_type,
                      d.dDateProduction AS document_date_production,
                      CASE WHEN da.fkDocument IS NULL THEN 0 ELSE 1 END AS user_is_approver,
                      CASE WHEN dr.fkDocument IS NULL THEN 0 ELSE 1 END AS user_is_reader,
                      CASE WHEN di.nMajorVersion > 0 THEN 1 ELSE 0 END AS document_is_released,
                      u.sName AS responsible_name,
                      sub.doc_count AS count_sub_doc,
                      sub2.approvers_count AS count_approvers,
                      ctxNames.str as context_name,
                      d.fkParent as document_parent 
                    FROM
                      pm_document d
                      JOIN isms_context c ON (c.pkContext = d.fkContext AND c.nState != ".CONTEXT_STATE_DELETED.")
                      LEFT JOIN view_pm_doc_instance_status dis ON (dis.document_id = d.fkContext)
                      LEFT JOIN ".FWDWebLib::getFunctionUserDB()."get_ctx_names_by_doc_id() ctxNames ON (ctxNames.id = d.fkContext) 
                      JOIN view_isms_user_active u ON (u.fkContext = d.fkMainApprover)
                      LEFT JOIN pm_doc_approvers da ON (da.fkDocument = d.fkContext AND da.fkUser = $miUserId)
                      LEFT JOIN pm_doc_readers dr ON (dr.fkDocument = d.fkContext AND dr.fkUser = $miUserId AND dr.bDenied=0)
                      LEFT JOIN pm_doc_instance di ON (di.fkContext = dis.doc_instance_id)
                      LEFT JOIN ( SELECT fkParent, count(fkContext) AS doc_count FROM view_pm_document_active doc GROUP BY fkParent ) sub ON (sub.fkParent = d.fkContext)
                      LEFT JOIN ( SELECT fkDocument, count(fkUser) AS approvers_count FROM pm_doc_approvers GROUP BY fkDocument ) sub2 ON (sub2.fkDocument = d.fkContext)
                      LEFT JOIN pm_doc_context dc ON (d.fkContext = dc.fkDocument)
                      LEFT JOIN pm_process_user pu ON (dc.fkContext = pu.fkprocess)
                      LEFT JOIN isms_config cfg ON (cfg.pkconfig = 810)
                      {$msWhere}";
	}

	public function getPermissions(){
		$miUserId = ISMSLib::getCurrentUserId();
		return PMDocument::getPermissions(
		$this->getFieldValue('document_status'),
		$this->getFieldValue('doc_instance_status'),
		($this->getFieldValue('document_main_approver')==$miUserId),
		($this->getFieldValue('document_author')==$miUserId),
		$this->getFieldValue('user_is_approver'),
		$this->getFieldValue('user_is_reader'),
		$this->getFieldValue('user_has_approved'),
		$this->getFieldValue('count_approvers'),
		$this->getFieldValue('document_is_link'),
		$this->getFieldValue('document_file_path'),
		$this->getFieldValue('document_link'),
		$this->getFieldValue('document_type')
		);
	}
}
?>