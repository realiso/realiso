<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGridCurrentUsers.
 *
 * <p>Consulta para popular a grid de usu�rios correntes de um process.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryGridCurrentUsers extends FWDDBQueryHandler {

  protected $cbEmpty = false;

  public function __construct($poDB){
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField("fkContext","user_id", DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("sName","user_name", DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("sLogin","user_login", DB_STRING));
  }

  public function showUsers($paUsers){
    $this->caUserIds = $paUsers;
  }

  public function setEmpty($pbEmpty){
    $this->cbEmpty = $pbEmpty;
  }

  public function makeQuery(){
    if($this->cbEmpty){
      $this->csSQL = "SELECT fkContext as user_id, sName as user_name, sLogin as user_login
                      FROM view_isms_user_active
                      WHERE 1=0";
    }else{
      $msCurrentIds = "";
      $msValues = "0";
      if(count($this->caUserIds)){
        $msValues = implode(',', $this->caUserIds);
      }
      $this->csSQL = "SELECT fkContext as user_id, sName as user_name, sLogin as user_login
          FROM view_isms_user_active WHERE fkContext IN ($msValues)";
    }
  }

}
?>