<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
 
/**
 * Classe QueryGridInstanceComments
 *
 * <p>Consulta para popular o grid de Comentários de Documentos</p>
 * @package ISMS
 * @subpackage classes
 */
class QueryGridInstanceComments extends FWDDBQueryHandler {
	
  protected $ciInstanceId;
  protected $ciUserId;
  protected $ciDocResponsible;
  /**
  * Construtor.
  * 
  * <p>Construtor da classe.</p>
  * @access public 
  */
	public function __construct($poDB) {
    parent::__construct($poDB); 
    $this->coDataSet->addFWDDBField(new FWDDBField("pkComment", "instance_comment_id", DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("fkInstance", "instance_id", DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("fkUser", "user_id", DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("tComment", "instance_comment", DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("dDate", "instance_comment_date", DB_DATETIME));
    $this->coDataSet->addFWDDBField(new FWDDBField("sName", "instance_comment_author", DB_STRING));
  }
  
  public function makeQuery() {
    $msWhere = '';
    if($this->ciInstanceId){
      $msWhere = " WHERE i.fkInstance = ".$this->ciInstanceId;
    }
    
    if(!$this->ciDocResponsible){
      if ($this->ciUserId) {
        if($msWhere){
          $msWhere .= " AND i.fkUser = ".$this->ciUserId;
        }else{
          $msWhere = " WHERE i.fkUser = ".$this->ciUserId;
        }
      } 
    }    

    $this->csSQL = "SELECT 
                      i.pkComment as instance_comment_id,
                      i.fkInstance as instance_id,
                      i.fkUser as user_id,
                      i.tComment as instance_comment,
                      i.dDate as instance_comment_date,
                      u.sName as instance_comment_author
                    FROM pm_instance_comment i
                    JOIN isms_context c ON (i.fkInstance=c.pkContext AND c.nState <> ".CONTEXT_STATE_DELETED.")
                    JOIN isms_user u ON (i.fkUser = u.fkContext)
                    $msWhere
                   ";
  } 
  
  public function setInstanceId($piInstanceId) {
    $this->ciInstanceId = $piInstanceId;
  }
  
  public function setUserId($piUserId) {
    $this->ciUserId = $piUserId;
  }
  
  public function setDocResponsible($piUserId){
    $this->ciDocResponsible = $piUserId;
  }
}
?>