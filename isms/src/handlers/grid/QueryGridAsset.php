<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGridAsset.
 *
 * <p>Consulta para popular a grid de ativos.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryGridAsset extends FWDDBQueryHandler {

  protected $ciUserId = 0;

  protected $ciProcess = 0;

  protected $csName = '';

  protected $csNameDescription = '';

  protected $ciResponsible = 0;
  
  protected $ciCategory = 0;

  protected $ciMaxProcesses = 0;

  protected $ciMinProcesses = 0;

  protected $ciMaxRisks = 0;

  protected $ciMinRisks = 0;
  
  protected $caValueFilter = array();

  protected $ciState = 0;
  
  protected $ciAsset = 0;
  
  protected $cbDependencies = false;
  
  protected $cbDependents = false;
  
  public function __construct($poDB=null){
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField('a.fkContext'    ,'asset_id'                  ,DB_NUMBER  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('u.sName'        ,'asset_responsible'         ,DB_STRING  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('sr.sName'       ,'asset_security_responsible',DB_STRING  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('a.sName'        ,'asset_name'                ,DB_STRING  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('a.nValue'       ,'asset_value'               ,DB_NUMBER  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('c.nState'       ,'asset_state'               ,DB_NUMBER  ));
    $this->coDataSet->addFWDDBField(new FWDDBField(''               ,'asset_risk_count'          ,DB_NUMBER  ));
    $this->coDataSet->addFWDDBField(new FWDDBField(''               ,'asset_process_count'       ,DB_NUMBER  ));
    $this->coDataSet->addFWDDBField(new FWDDBField(''               ,'user_create_name'          ,DB_STRING  ));
    $this->coDataSet->addFWDDBField(new FWDDBField(''               ,'date_create'               ,DB_DATETIME));
    $this->coDataSet->addFWDDBField(new FWDDBField(''               ,'user_edit_name'            ,DB_STRING  ));
    $this->coDataSet->addFWDDBField(new FWDDBField(''               ,'date_edit'                 ,DB_DATETIME));
    $this->coDataSet->addFWDDBField(new FWDDBField('a.fkResponsible','asset_responsible_id'      ,DB_NUMBER  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('cat.sName'      ,'category_name'             ,DB_STRING  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('cat.sName'      ,'asset_dependents_count'    ,DB_NUMBER  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('cat.sName'      ,'asset_dependencies_count'  ,DB_NUMBER  ));
  }                                                                   

  public function setUserId($piUserId){
    $this->ciUserId = $piUserId;
  }

  public function setProcess($piProcess){
    $this->ciProcess = $piProcess;
  }

  public function setName($psName){
    $this->csName = $psName;
  }

  public function setNameDescription($psNameDescription){
    $this->csNameDescription = $psNameDescription;
  }

  public function setResponsible($piResponsible){
    $this->ciResponsible = $piResponsible;
  }

  public function setCategory($piCategory){
    $this->ciCategory = $piCategory;
  }

  public function setMaxProcesses($piMaxProcesses){
    $this->ciMaxProcesses = $piMaxProcesses;
  }

  public function setMinProcesses($piMinProcesses){
    $this->ciMinProcesses = $piMinProcesses;
  }

  public function setMaxRisks($piMaxRisks){
    $this->ciMaxRisks = $piMaxRisks;
  }

  public function setMinRisks($piMinRisks){
    $this->ciMinRisks = $piMinRisks;
  }

  public function setValueFilter($paValueFilter){
    $this->caValueFilter = $paValueFilter;
  }

  public function setState($piState){
    $this->ciState = $piState;
  }

  public function setAsset($piAsset){
    $this->ciAsset = $piAsset;
  }
  
  public function showDependents( $pbDependents ){
      $this->cbDependents = $pbDependents;
  }
  
  public function showDependencies( $pbDependencies ){
      $this->cbDependencies = $pbDependencies;
  }
  
  public function makeQuery(){
    $maFilters = array();
    $maHavingFilters = array();
    
    if($this->ciUserId){
      /*
          Todos os ativos em que � respons�vel
          Todos os ativos em que � respons�vel pela seguran�a do ativo
          Todos os ativos relacionados aos processos em que � respons�vel
          Todos os ativos relacionados aos processos das �reas em que � respons�vel,
          Todos os ativos relacionados aos processos das sub-�reas das �reas em que � respons�vel
      */
      $msFunctionCall = FWDWebLib::getFunctionCall("get_areas_and_subareas_by_user({$this->ciUserId})");
      $maFilters[] = "(
                        a.fkResponsible = {$this->ciUserId}
                        OR a.fkSecurityResponsible = {$this->ciUserId}
                        OR a.fkContext IN (
                          SELECT p_a.fkAsset
                          FROM view_rm_process_asset_active p_a 
                          JOIN view_rm_process_active p ON (p.fkContext = p_a.fkProcess)
                          WHERE  p.fkArea IN (SELECT area_id FROM {$msFunctionCall})
                                 OR p.fkResponsible = {$this->ciUserId}
                        )
                      )";
    }
    
    if($this->ciProcess){
      $maFilters[] = "pa.fkProcess = {$this->ciProcess}";
    }
    
    if($this->csName){
      $maFilters[] = FWDWebLib::getCaseInsensitiveLike('a.sName',$this->csName);
    }
    
    if($this->csNameDescription){
      $maFilters[] = '('
                        .FWDWebLib::getCaseInsensitiveLike('a.sName',$this->csNameDescription)
                        .' OR '
                        .FWDWebLib::getCaseInsensitiveLike('a.tDescription',$this->csNameDescription)
                    .')';
    }
    
    if($this->ciResponsible){
      $maFilters[] = "a.fkResponsible = {$this->ciResponsible}";
    }
    
    if($this->ciCategory){
      $maFilters[] = "a.fkCategory = {$this->ciCategory}";
    }
    
    if($this->ciMaxProcesses){
      $maHavingFilters[] = "COUNT(pa.fkProcess) <= {$this->ciMaxProcesses}";
    }
    
    if($this->ciMinProcesses){
      $maHavingFilters[] = "COUNT(pa.fkProcess) >= {$this->ciMinProcesses}";
    }
    
    if($this->ciMaxRisks){
      $maFilters[] = "r.asset_risk_count <= {$this->ciMaxRisks}";
    }
    
    if($this->ciMinRisks){
      $maFilters[] = "r.asset_risk_count >= {$this->ciMinRisks}";
    }
    
    if($this->ciAsset && $this->cbDependents){
      $dependents = QuerySelectAssetAsset::getAssetDependents($this->ciAsset);

      if(count($dependents) < 1){
        $dependents_str = '-1';  
      }else{
        $dependents_str = implode(",", $dependents);
      }
      $maFilters[] = " a.fkContext IN ($dependents_str)";
    }
    
    if($this->ciAsset && $this->cbDependencies){
      $dependencies = QuerySelectAssetAsset::getAssetDependencies($this->ciAsset);
      
      if(count($dependencies) < 1){
        $dependencies_str = '-1';
      }else{
        $dependencies_str = implode(",", $dependencies);  
      }

      $maFilters[] = "a.fkContext IN ($dependencies_str)";
    }

    $miCountValueFilter = count($this->caValueFilter);
    if($miCountValueFilter > 0 && $miCountValueFilter < 4){
      $miRiskLow = ISMSLib::getConfigById(RISK_LOW);
      $miRiskHigh = ISMSLib::getConfigById(RISK_HIGH);
      $maValueFilters = array();
      foreach($this->caValueFilter as $msValueFilter){
        switch($msValueFilter){
          case 'not_estimated': $maValueFilters[] = "a.nValue = 0";                                         break;
          case 'low':           $maValueFilters[] = "a.nValue <= {$miRiskLow} AND a.nValue != 0";           break;
          case 'medium':        $maValueFilters[] = "a.nValue > {$miRiskLow} AND a.nValue < {$miRiskHigh}"; break;
          case 'high':          $maValueFilters[] = "a.nValue >= {$miRiskHigh}";                            break;
        }
      }
      $maFilters[] = '('.implode(' OR ',$maValueFilters).')';
    }
    
    if($this->ciState){
      $maFilters[] = "c.nState = {$this->ciState}";
    }
    
    if(count($maFilters)==0){
      $msWhere = '';
    }else{
      $msWhere = ' WHERE '.implode(' AND ',$maFilters);
    }
    if(count($maHavingFilters)==0){
      $msHaving = '';
    }else{
      $msHaving = ' HAVING '.implode(' AND ',$maHavingFilters);
    }
    
    $this->csSQL = "SELECT DISTINCT
                      a.fkContext AS asset_id,
                      u.sName AS asset_responsible,
                      sr.sName AS asset_security_responsible,
                      a.sName AS asset_name,
                      a.nValue AS asset_value,
                      c.nState AS asset_state,
                      CASE WHEN r.asset_risk_count IS NULL THEN 0 ELSE r.asset_risk_count END AS asset_risk_count,
                      count(pa.fkProcess) AS asset_process_count,
                      ch.context_creator_name AS user_create_name, 
                      ch.context_date_created AS date_create, 
                      ch.context_modifier_name AS user_edit_name, 
                      ch.context_date_modified AS date_edit,
                      a.fkResponsible AS asset_responsible_id,
                      cat.sName AS category_name, 
                      dependencies.asset_dependents_count as asset_dependents_count,
                      dependencies.asset_dependencies_count as asset_dependencies_count
                      
                      FROM
                      rm_asset a
                      LEFT JOIN asset_dependencies_cache dependencies on dependencies.fkasset = a.fkcontext
                      JOIN isms_context c ON (c.pkContext = a.fkContext AND c.nState != ".CONTEXT_STATE_DELETED.")
                      JOIN rm_category cat ON (cat.fkContext = a.fkCategory)
                      JOIN isms_user u on (u.fkContext = a.fkResponsible)
                      JOIN isms_user sr on (sr.fkContext = a.fkSecurityResponsible)
                      JOIN context_history ch ON (ch.context_id = c.pkContext)
                      LEFT JOIN view_rm_process_asset_active pa ON (pa.fkAsset = a.fkContext)
                      LEFT JOIN (
                        SELECT fkAsset, count(fkAsset) as asset_risk_count
                        FROM view_rm_risk_active ri
                        GROUP BY fkAsset
                      ) r ON (r.fkAsset = a.fkContext)
                    {$msWhere}
                    GROUP BY
                      a.fkContext,
                      u.sName,
                      sr.sName,
                      a.sName,
                      a.nValue,
                      c.nState,
                      r.asset_risk_count,
                      ch.context_creator_name,
                      ch.context_date_created,
                      ch.context_modifier_name,
                      ch.context_date_modified,
                      a.fkResponsible,
                      cat.sName,
                      dependencies.asset_dependents_count,
                      dependencies.asset_dependencies_count
                    {$msHaving}";
  }
}

?>