<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGridControlRisk.
 *
 * <p>Consulta para popular o grid de associação controle-risco.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryGridControlRisk extends FWDDBQueryHandler {	
	
  protected $ciControlId = 0;
	
	public function __construct($poDB) {
		parent::__construct($poDB);

		$this->coDataSet->addFWDDBField(new FWDDBField("rc.fkContext",    "rc_id",                DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("r.sName",         "risk_name",            DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("r.nValueResidual","risk_residual_value",  DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("r.fkContext",     "risk_id",              DB_NUMBER));
	}
  
  public function setControlId($piControlId) {
    $this->ciControlId = $piControlId;
  }
	
	public function makeQuery() {
    $this->csSQL = "
SELECT rc.fkContext as rc_id, r.sName as risk_name, r.nValueResidual as risk_residual_value, r.fkContext as risk_id
FROM rm_risk_control rc
      JOIN
     rm_risk r ON (rc.fkRisk = r.fkContext AND rc.fkControl = ".$this->ciControlId.")
      JOIN
     isms_context ctx ON (ctx.pkcontext = rc.fkRisk AND ctx.nstate != ".CONTEXT_STATE_DELETED.")
      JOIN
     isms_context ctx2 ON (ctx2.pkcontext = rc.fkControl AND ctx2.nstate != ".CONTEXT_STATE_DELETED.")";
	}
}
?>