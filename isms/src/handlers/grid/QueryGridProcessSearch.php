<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGridProcessSearch.
 *
 * <p>Consulta para popular o grid de pesquisa de processos.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryGridProcessSearch extends FWDDBQueryHandler {

  private $csNameFilter = "";

  private $ciAreaFilter = 0;

  private $caIds = array();

  private $caExcludedIds = array();
  
  private $cbEmpty = false;

  public function __construct($poDB){
    parent::__construct($poDB);

    $this->coDataSet->addFWDDBField(new FWDDBField('','process_id',         DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('','process_name',       DB_STRING));    
    $this->coDataSet->addFWDDBField(new FWDDBField('','process_area_name',  DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('','process_value',      DB_NUMBER));
  }

  public function setNameFilter($psNameFilter) {
    $this->csNameFilter = $psNameFilter;
  }

  public function setAreaFilter($piAreaFilter) {
    $this->ciAreaFilter = $piAreaFilter;
  }

  public function setIds($paIds){
    $this->caIds = $paIds;
  }

  public function setExcludedIds($paExcludedIds){
    $this->caExcludedIds = $paExcludedIds;
  }

  public function setEmpty($pbEmpty){
    $this->cbEmpty = $pbEmpty;
  }

  public function makeQuery(){
    $msWhere = '';
    
    if($this->cbEmpty){
      $msWhere.= " AND p.fkContext = 0";
    }
    
    if($this->csNameFilter){
      $msWhere.= " AND upper(p.sName) like upper('%{$this->csNameFilter}%')";
    }
    
    if($this->ciAreaFilter){
      $msWhere.= " AND ar.fkContext = {$this->ciAreaFilter}";
    }
    
    // Filtro por ids (incluir na consulta)
    if(count($this->caIds)){
      $msWhere .= " AND p.fkContext IN (".implode(',',$this->caIds).")";
    }
    
    // Filtro por ids (excluir da consulta)
    if(count($this->caExcludedIds)){
      $msWhere .= " AND p.fkContext NOT IN (".implode(',',$this->caExcludedIds).")";
    }
    
    $this->csSQL = "SELECT  
						p.fkContext as process_id,  
						p.sName as process_name,
						p.nValue as process_value,
						ar.sName as process_area_name 
					FROM  
						rm_process p JOIN isms_context c ON(p.fkContext = c.pkContext AND c.nState <> " . CONTEXT_STATE_DELETED . "),  
						rm_area ar
					WHERE  
						p.fkArea = ar.fkContext"
                    .$msWhere;
  }
}
?>