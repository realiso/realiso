<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGridStandardSearch.
 *
 * <p>Consulta para popular a grid de busca de normas.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryGridStandardSearch extends FWDDBQueryHandler {
	
	protected $csName = "";
	
	protected $caExcludedStandardIds = array();
	
	public function __construct($poDB) {
		parent::__construct($poDB);	

		$this->coDataSet->addFWDDBField(new FWDDBField("s.fkContext","standard_id", DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("s.sName","standard_name", DB_STRING));
	}
	
	public function setName($psName) {
		$this->csName = $psName ? "upper(sName) like upper('%$psName%')" : "";
	}
	
	public function dontShowStandards($paStandards) {
		$this->caExcludedStandardIds = $paStandards;
	}	
	
	public function makeQuery() {								
		$this->csSQL = "SELECT s.fkContext as standard_id, s.sName as standard_name
				FROM rm_standard s					
				JOIN isms_context c ON (s.fkContext = c.pkContext AND c.nState <> " . CONTEXT_STATE_DELETED . ")
				JOIN context_history ch ON (c.pkContext = ch.context_id) ";
		$miCount = count($this->caExcludedStandardIds);
		$msValues = implode(',', $this->caExcludedStandardIds);				
		if ($miCount && $this->csName)			
			$this->csSQL .= " WHERE {$this->csName} AND s.fkContext NOT IN ($msValues)";					
		else if ($this->csName)
			$this->csSQL .= " WHERE {$this->csName}";
		else if ($miCount)
			$this->csSQL .= " WHERE s.fkContext NOT IN ($msValues)";		
	} 
}
?>