<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGridUserAlerts.
 *
 * <p>Consulta para popular a grid de alertas do usu�rio.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryGridUserAlerts extends FWDDBQueryHandler {	
	
	protected $ciUserId = 0;
	
	public function __construct($poDB) {
		parent::__construct($poDB);	

		$this->coDataSet->addFWDDBField(new FWDDBField("a.pkAlert","alert_id", DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("a.dDate","alert_date", DB_DATETIME));
		$this->coDataSet->addFWDDBField(new FWDDBField("a.tJustification","alert_justification", DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField("a.nType","alert_type", DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("cn.context_id","alert_context_id", DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("cn.context_type","alert_context_type", DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("cn.context_name","alert_context_name", DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField("u.sName","alert_inquirer", DB_STRING));
	}
	
	public function setUser($piUserId) {
		$this->ciUserId = $piUserId;
	}

	public function makeQuery() {
		$this->ciUserId = $this->ciUserId ? $this->ciUserId : 0;

    $moConfig = new ISMSConfig();
    $mbHasRevision = $moConfig->getConfig(GENERAL_REVISION_ENABLED);
    $mbHasTests = $moConfig->getConfig(GENERAL_TEST_ENABLED);    
    $msWhere = '';
    $maFilterTypes = array();        
    if(!$mbHasRevision){
      $maFilterTypes[]= '9206';
    }
    if(!$mbHasTests){
        $maFilterTypes[] = '9209';
    }

    //sistema sem o m�dulo de documenta��o
    if(!FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(POLICY_MODE)){
        $maFilterTypes[] = '9211';
        $maFilterTypes[] = '9212';
        $maFilterTypes[] = '9213';
        $maFilterTypes[] = '9214';
        $maFilterTypes[] = '9215';
        $maFilterTypes[] = '9216';
    }
    
    if(count($maFilterTypes)){
      $msWhere = "AND a.nType NOT IN (".implode(',',$maFilterTypes).") ";
    }
    
    $msWhere .= " AND a.dDate <= " . ISMSLib::getTimestampFormat(ISMSLib::ISMSTime());
    $this->csSQL = "
SELECT a.pkAlert as alert_id,
       a.dDate as alert_date,
       a.tJustification as alert_justification,
       a.nType as alert_type,
       cn.context_id as alert_context_id,
       cn.context_type as alert_context_type,
       cn.context_name as alert_context_name,
       u.sName as alert_inquirer
  FROM wkf_alert a JOIN context_names cn ON (a.fkContext = cn.context_id)
    JOIN isms_user u ON (a.fkCreator = u.fkContext)
  WHERE a.fkReceiver = {$this->ciUserId}
          $msWhere
";
	} 
}
?>