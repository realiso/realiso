<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGridProcess.
 *
 * <p>Consulta para popular a grid de processos.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryGridProcess extends FWDDBQueryHandler {

  protected $csName = '';

  protected $csNameDescription = '';

  protected $ciResponsible = 0;

  protected $ciUserId = 0;

  protected $ciMaxAssets = 0;

  protected $ciMinAssets = 0;

  protected $caValueFilter = array();

  protected $ciType = 0;

  protected $ciPriority = 0;

  protected $ciState = 0;

  protected $ciProcess = 0;

  protected $ciArea = 0;

  protected $ciAsset = 0;
  
  public function __construct($poDB=null){
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField('p.fkContext'    ,'process_id'            ,DB_NUMBER  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('p.sName'        ,'process_name'          ,DB_STRING  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('u.sName'        ,'process_responsible'   ,DB_STRING  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('a.sName'        ,'process_area'          ,DB_STRING  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('c.nState'       ,'process_state'         ,DB_NUMBER  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('p.nValue'       ,'process_value'         ,DB_NUMBER  ));
    $this->coDataSet->addFWDDBField(new FWDDBField(''               ,'process_asset_count'   ,DB_NUMBER  ));
    $this->coDataSet->addFWDDBField(new FWDDBField(''               ,'user_create_name'      ,DB_STRING  ));
    $this->coDataSet->addFWDDBField(new FWDDBField(''               ,'date_create'           ,DB_DATETIME));
    $this->coDataSet->addFWDDBField(new FWDDBField(''               ,'user_edit_name'        ,DB_STRING  ));
    $this->coDataSet->addFWDDBField(new FWDDBField(''               ,'date_edit'             ,DB_DATETIME));
    $this->coDataSet->addFWDDBField(new FWDDBField('p.fkResponsible','process_responsible_id',DB_NUMBER  ));
    $this->coDataSet->addFWDDBField(new FWDDBField('a.fkContext'    ,'area_id'               ,DB_NUMBER  ));
    $this->coDataSet->addFWDDBField(new FWDDBField(''               ,'process_user_count'    ,DB_NUMBER  ));
  }

  public function setName($psName){
    $this->csName = $psName;
  }

  public function setNameDescription($psNameDescription){
    $this->csNameDescription = $psNameDescription;
  }

  public function setResponsible($piResponsible){
    $this->ciResponsible = $piResponsible;
  }

  public function setUserId($piUserId){
    $this->ciUserId = $piUserId;
  }

  public function setMaxAssets($piMaxAssets){
    $this->ciMaxAssets = $piMaxAssets;
  }

  public function setMinAssets($piMinAssets){
    $this->ciMinAssets = $piMinAssets;
  }

  public function setValueFilter($paValueFilter){
    $this->caValueFilter = $paValueFilter;
  }

  public function setType($piType){
    $this->ciType = $piType;
  }

  public function setPriority($piPriority){
    $this->ciPriority = $piPriority;
  }

  public function setState($piState){
    $this->ciState = $piState;
  }

  public function setProcess($piProcess){
    $this->ciProcess = $piProcess;
  }

  public function setArea($piArea){
    $this->ciArea = $piArea;
  }

  public function setAsset($piAsset){
    $this->ciAsset = $piAsset;
  }
  
  public function makeQuery(){
    $maFilters = array();
    $maHavingFilters = array();
    
    
    if($this->csName){
      $maFilters[] = FWDWebLib::getCaseInsensitiveLike('p.sName',$this->csName);
    }
    
    if($this->csNameDescription){
      $maFilters[] = '('
                        .FWDWebLib::getCaseInsensitiveLike('p.sName',$this->csNameDescription)
                        .' OR '
                        .FWDWebLib::getCaseInsensitiveLike('p.tDescription',$this->csNameDescription)
                    .')';
    }
    
    if($this->ciResponsible){
      $maFilters[] = "p.fkResponsible = {$this->ciResponsible}";
    }
    
    if($this->ciUserId){
      $msFunctionCall = FWDWebLib::getFunctionCall("get_areas_and_subareas_by_user({$this->ciUserId})");
      $maFilters[] = " ( p.fkArea IN (SELECT area_id FROM {$msFunctionCall}) OR p.fkResponsible = {$this->ciUserId} )";
    }

    if($this->ciMaxAssets){
      $maHavingFilters[] = "COUNT(pa.fkProcess) <= {$this->ciMaxAssets}";
    }
    
    if($this->ciMinAssets){
      $maHavingFilters[] = "COUNT(pa.fkProcess) >= {$this->ciMinAssets}";
    }
    
    $miCountValueFilter = count($this->caValueFilter);
    if($miCountValueFilter > 0 && $miCountValueFilter < 4){
      $miRiskLow = ISMSLib::getConfigById(RISK_LOW);
      $miRiskHigh = ISMSLib::getConfigById(RISK_HIGH);
      $maValueFilters = array();
      foreach($this->caValueFilter as $msValueFilter){
        switch($msValueFilter){
          case 'not_estimated': $maValueFilters[] = "p.nValue = 0";                                         break;
          case 'low':           $maValueFilters[] = "p.nValue <= {$miRiskLow} AND p.nValue != 0";           break;
          case 'medium':        $maValueFilters[] = "p.nValue > {$miRiskLow} AND p.nValue < {$miRiskHigh}"; break;
          case 'high':          $maValueFilters[] = "p.nValue >= {$miRiskHigh}";                            break;
        }
      }
      $maFilters[] = '('.implode(' OR ',$maValueFilters).')';
    }
    
    if($this->ciType){
      $maFilters[] = "p.fkType = {$this->ciType}";
    }
    
    if($this->ciPriority){
      $maFilters[] = "p.fkPriority = {$this->ciPriority}";
    }
    
    if($this->ciState){
      $maFilters[] = "c.nState = {$this->ciState}";
    }
    
    if($this->ciProcess){
      $maFilters[] = "p.fkContext = {$this->ciProcess}";
    }
    
    if($this->ciArea){
      $maFilters[] = "p.fkArea = {$this->ciArea}";
    }
    
    if($this->ciAsset){
      $msAssetJoin = "JOIN view_rm_process_asset_active pa ON (pa.fkProcess = p.fkContext AND pa.fkAsset = ". $this->ciAsset .")";
    }else{
      $msAssetJoin = 'LEFT JOIN view_rm_process_asset_active pa ON (pa.fkProcess = p.fkContext)';   
    }
    
    if(count($maFilters)==0){
      $msWhere = '';
    }else{
      $msWhere = ' WHERE '.implode(' AND ',$maFilters);
    }
    if(count($maHavingFilters)==0){
      $msHaving = '';
    }else{
      $msHaving = ' HAVING '.implode(' AND ',$maHavingFilters);
    }
    $this->csSQL = "SELECT
                      p.fkContext AS process_id,
                      p.sName AS process_name,
                      u.sName AS process_responsible,
                      a.sName AS process_area,
                      c.nState AS process_state,
                      p.nValue AS process_value,
                      COUNT(pa.fkProcess) AS process_asset_count,
                      ch.context_creator_name AS user_create_name,
                      ch.context_date_created AS date_create,
                      ch.context_modifier_name AS user_edit_name,
                      ch.context_date_modified AS date_edit,
                      p.fkResponsible AS process_responsible_id,
                      a.fkContext as area_id,
                      ( SELECT COUNT (fkUser) as count_user FROM pm_process_user pu WHERE pu.fkProcess = p.fkContext) as process_user_count
                    FROM
                      isms_context c
                      JOIN rm_process p ON (p.fkContext = c.pkContext AND c.nState != ".CONTEXT_STATE_DELETED.")
                      JOIN isms_user u ON (u.fkContext = p.fkResponsible)
                      JOIN rm_area a ON (a.fkContext = p.fkArea) 
                      JOIN context_history ch ON (ch.context_id = c.pkContext)
                      $msAssetJoin
                    {$msWhere}
                    GROUP BY
                      p.fkContext,
                      u.sName,
                      a.sName,
                      p.sName,
                      c.nState,
                      p.nValue,
                      ch.context_creator_name,
                      ch.context_date_created,
                      ch.context_modifier_name,
                      ch.context_date_modified,
                      p.fkResponsible,
                      a.fkContext
                    {$msHaving}";
  }

}

?>