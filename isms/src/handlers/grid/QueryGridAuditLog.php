<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGridAuditLog.
 *
 * <p>Consulta para popular o grid de log de auditoria.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryGridAuditLog extends FWDDBQueryHandler {
	
	protected $ciAction;
	protected $ciDateStart;
	protected $ciDateEnd;
	protected $ciDatabaseWasRemoved;
	protected $ciWhenDatabaseWasRemoved;
	protected $csUser="";

	
	public function __construct($poDB) {
		parent::__construct($poDB);	

		$this->coDataSet->addFWDDBField(new FWDDBField("dDate","log_date", DB_DATETIME));
		$this->coDataSet->addFWDDBField(new FWDDBField("tDescription","log_description", DB_STRING));		
	}	
	
	public function setDateStart($piDateStart) {
		$this->ciDateStart = $piDateStart;
	}
	
	public function setDateEnd($piDateEnd) {
		$this->ciDateEnd = $piDateEnd;
	}
	
	public function setAction($piAction) {
		$this->ciAction = $piAction;
	}
	
	public function setUser($psUser) {
		$this->csUser = $psUser;
	}

	public function isDatabaseWasRemove($databaseWasRemoved){
		$this->ciDatabaseWasRemoved = $databaseWasRemoved;
	}
	
	public function getWhenDatabaseWasRemoved($whenDatabaseWasRemoved){
		$this->ciWhenDatabaseWasRemoved = $whenDatabaseWasRemoved;
	}

	public function makeQuery() {

	$msWhere = " WHERE 1=1";

	if($this->ciDatabaseWasRemoved==1 and $this->ciWhenDatabaseWasRemoved != ''){
    	$msWhere.= " AND t.dDateCreated > (SELECT svalue::timestamp FROM isms_config WHERE pkconfig=7701) ";
    }
	    
    if($this->ciDateStart){
      $msWhere.= " AND dDate >= ".ISMSLib::getTimestampFormat($this->ciDateStart);
    }
    if($this->ciDateEnd){
      $msWhere.= " AND dDate <= ".ISMSLib::getTimestampFormat($this->ciDateEnd);
    }
    if($this->ciAction){
      $msWhere.= " AND nAction = ".$this->ciAction;
    }
    if ($this->csUser) {
    	$msWhere.= " AND sUser like '".$this->csUser."'";
    }
    
    $this->csSQL = "SELECT dDate as log_date, tDescription as log_description FROM isms_audit_log"
    							 .$msWhere;		
	} 

}
?>