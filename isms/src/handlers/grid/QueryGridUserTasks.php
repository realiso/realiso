<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGridUserTasks.
 *
 * <p>Consulta para popular o grid de tarefas do usu�rio.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryGridUserTasks extends FWDDBQueryHandler {

	protected $ciUserId = 0;

	public function __construct($poDB) {
		parent::__construct($poDB);

		$this->coDataSet->addFWDDBField(new FWDDBField("t.pkTask"       ,"task_id"           , DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("t.dDateCreated" ,"task_date_created" , DB_DATETIME));
		$this->coDataSet->addFWDDBField(new FWDDBField("t.nActivity"    ,"task_activity"     , DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("cn.context_id"  ,"task_context_id"   , DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("cn.context_type","task_context_type" , DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("cn.context_name","context_name"      , DB_STRING));
	}

	public function setUser($piUserId) {
		$this->ciUserId = $piUserId;
	}

	public function makeQuery() {
		$this->ciUserId = $this->ciUserId ? $this->ciUserId : 0;

    $moConfig = new ISMSConfig();
    $mbHasRevision = $moConfig->getConfig(GENERAL_REVISION_ENABLED);
    $mbHasTests = $moConfig->getConfig(GENERAL_TEST_ENABLED);
    $msWhere = '';
    $maFilterTypes = array();
    if(!$mbHasRevision){
      $maFilterTypes[]= '2208';
      $maFilterTypes[]=  ACT_INC_CONTROL_INDUCTION;
    }
    if(!$mbHasTests){
        $maFilterTypes[] = '2216';
    }

    //sistema sem o m�dulo de documenta��o
    if(!FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(POLICY_MODE)){
        $maFilterTypes[] = '2221';
        $maFilterTypes[] = '2222';
    }
    if(count($maFilterTypes)){
      $msWhere = "AND t.nActivity NOT IN (".implode(',',$maFilterTypes).") ";
    }

		$this->csSQL = "
SELECT t.pkTask as task_id, 
       t.dDateCreated as task_date_created, 
       t.nActivity as task_activity, 
       cn.context_id as task_context_id, 
       cn.context_type as task_context_type,
       cn.context_name as context_name
  FROM wkf_task t 
  JOIN context_names cn ON (t.fkContext = cn.context_id)
  WHERE t.bVisible = 1
        AND t.fkReceiver = {$this->ciUserId}
        $msWhere
";
	}

	public function setOrderBy($psAlias, $psOrder) {
  	$this->coDataSet->setOrderBy($psAlias, $psOrder);
  }
}
?>