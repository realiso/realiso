<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGridEventSearch.
 *
 * <p>Consulta para popular o grid de pesquisa de eventos.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryGridEventSearch extends FWDDBQueryHandler {

  protected $csDescription = '';

  protected $ciBestPracticeId = 0;

  protected $ciCategoryId = 0;
  
  protected $ciRootCategoryId = 0;
  
  protected $csCategory = '';

  protected $caEventIds = array();
  
  protected $caExcludedEventsIds = array();
  
  protected $ciAssetId = 0;
  
  private $cbEmpty = false;
  
  private $ciCategoryIdWithParents = '';

  public function __construct($poDB){
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField('e.fkContext',   'event_id',         DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('e.sDescription','event_description',DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('e.tImpact',     'event_impact',     DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('c.sName',       'event_category',   DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('c.fkContext',   'category_id',      DB_NUMBER));
  }

  public function setDescription($psDescription){
    $this->csDescription = $psDescription;
  }

  public function setBestPractice($piBestPracticeId){
    $this->ciBestPracticeId = $piBestPracticeId;
  }

  public function setCategory($piCategoryId){
    $this->ciCategoryId = $piCategoryId;
  }

  public function setCategoryWithParentsFilter($piCategoryId){
    $this->ciCategoryIdWithParents = $piCategoryId;
  }
  
  public function setRootCategory($piCategoryId){
    $this->ciRootCategoryId = $piCategoryId;
  }

  public function setCategoryName($psCategory){
    $this->csCategory = $psCategory;
  }

  public function setEvents($paEvents){
    $this->caEventIds = $paEvents;
  }

  public function dontShowEvents($paEventsIds){
    $this->caExcludedEventsIds = $paEventsIds;
  }

  public function setAsset($piAssetId){
    $this->ciAssetId = $piAssetId;
  }
  
  public function setEmpty($pbEmpty){
    $this->cbEmpty = $pbEmpty;
  }

  public function makeQuery(){
    $maFilters = array();
    
    if($this->cbEmpty){
      $maFilters[] = "e.fkContext = 0";
    }
    //filtro por uma categoria incluindo os eventos herdados das categorias "pai"
    if($this->ciCategoryIdWithParents){
      $maFilters[] = "(( c.fkContext = $this->ciCategoryIdWithParents) OR (c.fkContext IN (SELECT pkCategory FROM ".FWDWebLib::getFunctionCall("get_category_parents($this->ciCategoryIdWithParents)").") ) ) ";      
    }
    // Filtro por uma categoria
    if($this->ciCategoryId){
      $maFilters[] = " c.fkContext = $this->ciCategoryId ";
    }
    // Filtro por categoria raiz
    if($this->ciRootCategoryId){
      $moCategory = new RMCategory();
      $maParents = $moCategory->getCategoryParents($this->ciRootCategoryId);
      $msRootFilter = "c.fkContext NOT IN ($this->ciRootCategoryId";
      if(count($maParents)){
        $msRootFilter.= ','.implode(',', $maParents).')';
      }else{
        $msRootFilter.= ')';
      }
      $maFilters[] = $msRootFilter;
    }
    // Filtro por nome da categoria
    if($this->csCategory){
      $maFilters[] = "upper(c.sName) like upper('%{$this->csCategory}%')";
    }
    // Filtro por descricao
    if($this->csDescription){
      $maFilters[] = "upper(e.sDescription) like upper('%{$this->csDescription}%')";
    }
    // Filtro por ids
    if(count($this->caEventIds)){
      $maFilters[] = "e.fkContext IN (".implode(',',$this->caEventIds).")";
    }
    // Filtro por ids (n�o mostrar)
    if(count($this->caExcludedEventsIds)){
      $maFilters[] = "e.fkContext NOT IN (".implode(',',$this->caExcludedEventsIds).")";
    }
    // Filtro por ativo (n�o mostrar eventos[riscos] que ja estejam relacionados com o ativo)
    if($this->ciAssetId){
      $maFilters[] = " 
      e.fkContext NOT IN (
        SELECT rsk.fkEvent
          FROM view_rm_risk_active rsk
          WHERE rsk.fkAsset = ".$this->ciAssetId." 
            AND rsk.fkEvent IS NOT NULL)
      ";
    }
    // Filtro por melhor pr�tica
    if($this->ciBestPracticeId){
      $maFilters[] = "e.fkContext IN (
        SELECT fkEvent
        FROM rm_best_practice_event
        WHERE fkBestPractice = {$this->ciBestPracticeId}
      )";
    }
    // Monta a consulta
    $this->csSQL = "
SELECT e.fkContext as event_id, 
       e.sDescription as event_description, 
       e.tImpact as event_impact,
       c.sName as event_category, 
       c.fkContext as category_id
  FROM view_rm_event_active e
       JOIN view_rm_category_active c ON (e.fkCategory = c.fkContext)
";
    if(count($maFilters)){
      $this->csSQL.= " WHERE ".implode(' AND ',$maFilters);
    }

  }
}
?>