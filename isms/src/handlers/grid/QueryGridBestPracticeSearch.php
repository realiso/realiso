<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGridBestPracticeSearch.
 *
 * <p>Consulta para popular o grid de BestPractices.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryGridBestPracticeSearch extends FWDDBQueryHandler {

  private $ciSectionID;
  
  private $ciEvent;
  
  private $caExcludedIds;
  
  private $caIds;

  private $csNameFilter;

  private $caSectionsIds;
  
  private $cbEmpty = false;

  public function __construct($poDB){
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField('bp.fkcontext','best_practice_id',  DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('bp.sName',    'best_practice_name',DB_STRING));
  }

  public function setName($psName){
    $this->csNameFilter = $psName;
  }

  public function setIds($paIds){
    $this->caIds = $paIds;
  }

  public function setExcludedIds($paExcludedIds){
    $this->caExcludedIds = $paExcludedIds;
  }

  public function setSectionID($piSectionID){
    $this->ciSectionID = $piSectionID;
  }

  public function setSectionsIds($pmSectionsIds){
    if(is_array($pmSectionsIds)){
      $this->caSectionsIds = $pmSectionsIds;
    }else{
      $this->caSectionsIds = array_filter(explode(':',$pmSectionsIds));
    }
  }

  public function setEvent($piEventId){
    $this->ciEvent = $piEventId;
  }

  public function setEmpty($pbEmpty){
    $this->cbEmpty = $pbEmpty;
  }

  public function makeQuery(){
    $maFilters = array();
    
    if($this->cbEmpty){
      $maFilters[] = "bp.fkContext = 0";
    }
    
    // Filtro por se��o
    if($this->ciSectionID){
      $maFilters[] = "(bp.fkSectionBestPractice = {$this->ciSectionID} OR bp.fkSectionBestPractice IN ( SELECT pkSection FROM ".FWDWebLib::getFunctionCall("get_sub_sections({$this->ciSectionID})")." ))";
    }
    // Filtro por nome
    if($this->csNameFilter){
      $maFilters[] = "upper(bp.sName) like upper('%{$this->csNameFilter}%')";
    }
    // Filtro de se��es
    if($this->caSectionsIds){
      $maFilters[] = "bp.fkContext NOT IN (".implode(',',$this->caSectionsIds).")";
    }
    // Filtro por ids
    if(count($this->caIds)){
      $maFilters[] = "bp.fkContext IN (".implode(',',$this->caIds).")";
    }
    // Filtro por ids (excluir da consulta)
    if(count($this->caExcludedIds)){
      $maFilters[] = "bp.fkContext NOT IN (".implode(',',$this->caExcludedIds).")";
    }
    // Filtro por evento
    if($this->ciEvent){
      $maFilters[] = "bp.fkContext IN (
        SELECT fkBestPractice
        FROM rm_best_practice_event
        WHERE fkEvent = {$this->ciEvent}
      )";
    }
    // Monta a consulta
    $this->csSQL = "SELECT bp.fkcontext as best_practice_id, bp.sname as best_practice_name
                    FROM rm_best_practice bp
                    JOIN isms_context c ON (bp.fkContext = c.pkContext AND c.nState <> ".CONTEXT_STATE_DELETED.")";
    if(count($maFilters)){
      $this->csSQL.= " WHERE ".implode(' AND ',$maFilters);
    }
  }
}
?>