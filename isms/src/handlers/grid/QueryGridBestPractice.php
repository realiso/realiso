<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGridBestPractice.
 *
 * <p>Consulta para popular o grid de melhores pr�ticas.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryGridBestPractice extends FWDDBQueryHandler {	
		
	protected $ciSectionId = 0;
	
	public function __construct($poDB) {
		parent::__construct($poDB);	

		$this->coDataSet->addFWDDBField(new FWDDBField("res.context_id","context_id", DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("res.context_name","context_name", DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField("res.context_type","context_type", DB_NUMBER));
		
		$this->coDataSet->addFWDDBField(new FWDDBField("uc.sName","user_create_name", DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField("cc.dDate","date_create", DB_DATETIME));
		$this->coDataSet->addFWDDBField(new FWDDBField("ud.sName","user_edit_name", DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField("ce.dDate","date_edit", DB_DATETIME));
	}

	
	public function setRootSection($piSectionId) {
		$this->ciSectionId = $piSectionId;
	}

	public function makeQuery() {		
		$msUnion = "";
		$msWhere = " WHERE fkParent";
		if ($this->ciSectionId) {
			$msWhere .= " = {$this->ciSectionId}";
			$msUnion = " UNION						
					SELECT fkContext as context_id, sName as context_name, " . CONTEXT_BEST_PRACTICE . " as context_type
					FROM rm_best_practice
					WHERE fkSectionBestPractice = {$this->ciSectionId}" ;		
		} 
		else $msWhere .= " IS NULL";
						
		$this->csSQL = "SELECT res.context_id, res.context_name, res.context_type, ch.context_creator_name as user_create_name, ch.context_date_created as date_create, ch.context_modifier_name as user_edit_name, ch.context_date_modified as date_edit FROM
						(
						SELECT fkContext as context_id, sName as context_name, " . CONTEXT_SECTION_BEST_PRACTICE . " as context_type
						FROM rm_section_best_practice
						$msWhere
						$msUnion											
						) res            
            			JOIN isms_context c ON (res.context_id = c.pkContext AND c.nState <> " . CONTEXT_STATE_DELETED . ")
            			JOIN context_history ch ON (res.context_id = ch.context_id) 
						";            
    $this->coDataSet->setOrderBy("context_type","-");		
	} 
}
?>