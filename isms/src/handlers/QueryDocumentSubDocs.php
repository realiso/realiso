<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryDocumentSubDocs.
 *
 * <p>Consulta que busca os subdocumentos de um documento.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryDocumentSubDocs extends FWDDBQueryHandler {
	
	private $ciDocumentId;
  private $caSubDocs;
  
  public function __construct($poDB,$piDocumentId){
    parent::__construct($poDB);
    $this->ciDocumentId = $piDocumentId;
    
    $this->coDataSet->addFWDDBField(new FWDDBField('','doc_id',DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('','doc_name',DB_STRING));
  }

  public function makeQuery(){
    $this->csSQL = "SELECT
											sub.document_id as doc_id,
											d.sName as doc_name
										FROM
											" . FWDWebLib::getFunctionCall("get_sub_documents({$this->ciDocumentId})") . " sub
											JOIN view_pm_published_docs d ON (sub.document_id = d.fkContext)";
  }
  
  public function executeQuery() {
    parent::executeQuery();
    while ($this->coDataSet->fetch()) {
      $this->caSubDocs[$this->coDataSet->getFieldByAlias("doc_id")->getValue()] = $this->coDataSet->getFieldByAlias("doc_name")->getValue();      
    }
  }
  
  public function getSubDocs() {
    return $this->caSubDocs;
  }
}
?>