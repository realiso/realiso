<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryProfileIsDeletable.
 *
 * <p>Consulta para indicar se um perfil � delet�vel ou n�o.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryProfileIsDeletable extends FWDDBQueryHandler {

  private $ciProfileId = 0;
  
  private $cbIsDeletable = false;
  
  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField("ordem","ordem", DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("count","count", DB_NUMBER));
 }

  public function setProfileId($psProfileId) {
    $this->ciProfileId = $psProfileId;
  }

  public function makeQuery() {
    $this->csSQL = "
SELECT 'total_users' as ordem, count(*) as count
FROM isms_user u
JOIN isms_context ctx ON (u.fkContext = ctx.pkContext AND ctx.nState != ".CONTEXT_STATE_DELETED.")
WHERE u.fkProfile = ".$this->ciProfileId;
  }

  public function executeQuery() {
    parent::executeQuery();
    $miTotal = 0;
    if ($this->coDataSet->fetch()) {
      $miTotal = $this->coDataSet->getFieldByAlias("count")->getValue();
    }
    if ($miTotal>0)
      $this->cbIsDeletable = false;
    else
      $this->cbIsDeletable = true;
  }

  public function getIsDeletable() {
     return $this->cbIsDeletable;
  }
}
?>