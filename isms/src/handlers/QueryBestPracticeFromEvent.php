<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryBestPracticeFromEvent.
 *
 * <p>Consulta para buscar as melhores pr�ticas de um evento.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryBestPracticeFromEvent extends FWDDBQueryHandler {
  
  private $ciEventId = 0;
  
  private $caBestPractices = array();
  
  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField("fkBestPractice","best_practice_id", DB_NUMBER));
  }
  
  public function setEventId($piEventId) {
    $this->ciEventId = $piEventId;
  }
  
  public function makeQuery() {
    $this->csSQL = "SELECT fkBestPractice as best_practice_id FROM rm_best_practice_event WHERE fkEvent = {$this->ciEventId}";
  }
  
  public function executeQuery() {
    parent::executeQuery();
    $this->caBestPractices = array();
    while ($this->coDataSet->fetch()) {
      $this->caBestPractices[] = $this->coDataSet->getFieldByAlias("best_practice_id")->getValue();
    }
  }
  
  /**
   * Retorna as melhores pr�ticas de um evento.
   * 
   * <p>M�todo para retornar as melhores pr�ticas de um evento.</p>
   * @access public 
   * @return array Array de ids das melhores pr�ticas
   */ 
  public function getBestPractices() {
    return $this->caBestPractices;
  }
}
?>