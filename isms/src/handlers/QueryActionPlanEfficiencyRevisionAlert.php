<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryNonConformityEfficiencyRevisionAlert
 *
 * <p>Consulta para pesquisar as n�o conformidades que tem
 * revis�o pr�xima e que ainda n�o tiveram alertas enviados.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryActionPlanEfficiencyRevisionAlert extends FWDDBQueryHandler {

  public function __construct($poDB){
    parent::__construct($poDB);

    $this->coDataSet->addFWDDBField(new FWDDBField('ap.fkContext',                'ap_id',                        DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('ap.dDateEfficieapyRevision',  'ap_efficiency_revision_date',  DB_DATETIME));
    $this->coDataSet->addFWDDBField(new FWDDBField('ap.nDaysBefore',              'ap_days_before',               DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('ap.fkResponsible',            'ap_responsible_id',            DB_NUMBER));
  }

  public function makeQuery(){
    $this->csSQL ="
SELECT ap.fkContext as ap_id,
       ap.dDateEfficiencyRevision as ap_efficiency_revision_date,
       ap.nDaysBefore as ap_days_before,
       ap.fkResponsible as ap_responsible_id
   FROM view_ci_action_plan_active ap
   WHERE ap.dDateEfficiencyRevision IS NOT NULL AND
        (ap.bFlagRevisionAlert=0 OR ap.bFlagRevisionAlert IS NULL)
";
  }
}
?>