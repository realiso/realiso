<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryStandards.
 *
 * <p>Consulta para retornar as normas do sistema (id e nome).</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryStandards extends FWDDBQueryHandler {

  protected $caStandards = array();

  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField("fkContext","standard_id", DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("sName","standard_name", DB_STRING));
 }

  public function makeQuery() {
    $this->csSQL = "
SELECT fkContext as standard_id, sName as standard_name
FROM rm_standard bps
JOIN isms_context ctx ON (bps.fkContext = ctx.pkContext AND ctx.nState != ".CONTEXT_STATE_DELETED.")";
  }

  public function executeQuery() {
    parent::executeQuery();
    $this->caStandards = array();
    while ($this->coDataSet->fetch())
      $this->caStandards[$this->coDataSet->getFieldByAlias("standard_id")->getValue()] = trim(substr($this->coDataSet->getFieldByAlias("standard_name")->getValue(),0,240));
  }

  public function getStandards() {
    return $this->caStandards;
  }
}
?>