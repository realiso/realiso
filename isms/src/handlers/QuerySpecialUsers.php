<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QuerySpecialUsers.
 *
 * <p>Consulta para buscar os usu�rios especiais do ISMS.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QuerySpecialUsers extends FWDDBQueryHandler {
  
  protected $caSpecialUsers = array();
  
  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField("ordem",     "ordem",      DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("fkContext", "user_id",    DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("sName",     "user_name",  DB_STRING));
 }
  
  public function makeQuery() {
    $this->csSQL = "
SELECT 'chairman' as ordem, fkContext as user_id, sName as user_name FROM isms_user WHERE fkContext = ".ISMSLib::getConfigById(USER_CHAIRMAN)." 
UNION
SELECT 'asset_controller' as ordem, fkContext as user_id, sName as user_name FROM isms_user WHERE fkContext = ".ISMSLib::getConfigById(USER_ASSET_CONTROLLER)."
UNION
SELECT 'control_controller' as ordem, fkContext as user_id, sName as user_name FROM isms_user WHERE fkContext = ".ISMSLib::getConfigById(USER_CONTROL_CONTROLLER)."
UNION
SELECT 'librarian' as ordem, fkContext as user_id, sName as user_name FROM isms_user WHERE fkContext = ".ISMSLib::getConfigById(USER_LIBRARIAN)."
UNION
SELECT 'document_auditor' as ordem, fkContext as user_id, sName as user_name FROM isms_user WHERE fkContext = ".ISMSLib::getConfigById(USER_DOCUMENT_AUDITOR)."
";
    $mbHasCIModule = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(INCIDENT_MODE);
    if ($mbHasCIModule) {
      $this->csSQL .= "
UNION
SELECT 'incident_manager' as ordem, fkContext as user_id, sName as user_name FROM isms_user WHERE fkContext = ".ISMSLib::getConfigById(USER_INCIDENT_MANAGER)."
UNION
SELECT 'disciplinary_process_manager' as ordem, fkContext as user_id, sName as user_name FROM isms_user WHERE fkContext = ".ISMSLib::getConfigById(USER_DISCIPLINARY_PROCESS_MANAGER)."
UNION
SELECT 'non_conformity_manager' as ordem, fkContext as user_id, sName as user_name FROM isms_user WHERE fkContext = ".ISMSLib::getConfigById(USER_NON_CONFORMITY_MANAGER)."
UNION
SELECT 'evidence_manager' as ordem, fkContext as user_id, sName as user_name FROM isms_user WHERE fkContext = ".ISMSLib::getConfigById(USER_EVIDENCE_MANAGER)."
";
    }
  }
  
  public function executeQuery() {
    parent::executeQuery();
    while ($this->coDataSet->fetch()) {
      $msUser = $this->coDataSet->getFieldByAlias("ordem")->getValue();
      $miId = $this->coDataSet->getFieldByAlias("user_id")->getValue();
      $msName = $this->coDataSet->getFieldByAlias("user_name")->getValue();
      $this->caSpecialUsers[$msUser."_id"] = $miId;
      $this->caSpecialUsers[$msUser."_name"] = $msName;
    }
  }
  
  /**
   * Retorna os usu�rios especiais do ISMS.
   * 
   * <p>M�todo para retornar os usu�rios especiais do ISMS.</p>
   * @access public 
   * @return array Array de usu�rios especiais
   */ 
  public function getSpecialUsers() {
    return $this->caSpecialUsers;
  }
}
?>