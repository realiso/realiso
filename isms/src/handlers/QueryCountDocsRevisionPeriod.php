<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryCountDocsRevisionPeriod.
 *
 * <p>Data da ultima revis�o por per�odo de tempo (ou seja, ser� que as pessoas est�o 
 * atualizando os documentos) algo do tipo, tantos documentos tem ultima data de revis�o
 *  a 3 meses, tantos a 6 meses, tantos a 1 m�s</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryCountDocsRevisionPeriod extends FWDDBQueryHandler {
  protected $caDocCount = array();

  protected $ciUserId = 0;  

  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField('doc.fkContext'  ,'doc_id'         ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('inst.fkContext' ,'instance_id'    ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('dBeginRevision' ,'begin_revision' ,DB_DATETIME));
    $this->coDataSet->addFWDDBField(new FWDDBField('dBeginApprover' ,'end_revision'   ,DB_DATETIME));
  }

  public function setUserId($piUserId){
    $this->ciUserId = $piUserId;
  }

  public function makeQuery() {
  $msWhere = '';
  if($this->ciUserId)
  $msWhere .= "   WHERE doc.fkMainApprover = ".$this->ciUserId;

    $this->csSQL = "
SELECT doc.fkContext as doc_id,
       inst.fkContext as instance_id,
       inst.dBeginRevision as begin_revision,
       inst.dBeginApprover as end_revision
  FROM view_pm_document_active doc
  JOIN view_pm_doc_instance_active inst ON (  doc.fkCurrentVersion = inst.fkContext 
                                              AND inst.dBeginRevision IS NOT NULL 
                                              AND inst.dBeginApprover IS NOT NULL 
                                            )
  $msWhere
";
  }
  
  public function executeQuery() {
    parent::executeQuery();
    $this->caDocCount[WEEK_INTERVAL]=0;
    $this->caDocCount[MONTH_INTERVAL]=0;
    $this->caDocCount[TREE_MONTH_INTERVAL]=0;
    $this->caDocCount[SIX_MONTH_INTERVAL]=0;
    $this->caDocCount[YEAR_INTERVAL]=0;
   /*timesptamp verdadeiro de um ano=31557600 correspondendo a 365.25 days fonte
    * http://www.php.net/manual/pt_BR/function.fmod.php
    * 
   */
    while($this->coDataSet->fetch()){
      $miPeriod = ISMSLib::getTimestamp($this->coDataSet->getFieldByAlias("end_revision")->getValue())
                  - ISMSLib::getTimestamp($this->coDataSet->getFieldByAlias("begin_revision")->getValue());

      if($miPeriod <= 604800) // periodo semanal
        $this->caDocCount[WEEK_INTERVAL] = $this->caDocCount[WEEK_INTERVAL] + 1;
      elseif($miPeriod <= 2629800) //periodo_mensal
        $this->caDocCount[MONTH_INTERVAL] = $this->caDocCount[MONTH_INTERVAL] + 1;
      elseif($miPeriod <= 7889400)//periodo_trimestral
        $this->caDocCount[TREE_MONTH_INTERVAL] = $this->caDocCount[TREE_MONTH_INTERVAL] + 1;
      elseif($miPeriod <= 15778800)//periodo_semestral
        $this->caDocCount[SIX_MONTH_INTERVAL] = $this->caDocCount[SIX_MONTH_INTERVAL] + 1;
      else //periodo anual
        $this->caDocCount[YEAR_INTERVAL] = $this->caDocCount[YEAR_INTERVAL] + 1;
    }
  }
  
  public function getValues() {
    $this->makeQuery();
    $this->executeQuery();
    
    return $this->caDocCount;
  }
}
?>