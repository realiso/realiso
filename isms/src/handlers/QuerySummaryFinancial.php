<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QuerySummaryFinancial.
 *
 * <p>Consulta para popular o grid de sum�rio financeiro.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QuerySummaryFinancial extends FWDDBQueryHandler {

  protected $caFinancialSummary = array();

  protected $ciUserId = 0;

  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField("ordem","ordem", DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("count","financial_impact", DB_NUMBER));
 }

  public function setUserId($psUserId) {
    $this->ciUserId = $psUserId;
  }

  public function makeQuery() {
    $msUserJoin_r = ($this->ciUserId ? "JOIN isms_context_date cd ON (r.fkContext = cd.fkContext AND cd.nAction = ".ACTION_EDIT." AND cd.nUserId = ".$this->ciUserId.")" : "");
    $msUserJoin_c = ($this->ciUserId ? " JOIN isms_user u ON (c.fkResponsible = u.fkContext AND u.fkContext = ".$this->ciUserId.")" : "");
    $this->csSQL = "
SELECT 'total_asset' as ordem, sum(ncost) as financial_impact
FROM rm_asset a
JOIN isms_context ctx ON (a.fkContext = ctx.pkContext AND ctx.nState != ".CONTEXT_STATE_DELETED.")
  
  UNION

SELECT 'cost1_control' as ordem, sum(ncost1) as financial_impact
FROM rm_control_cost c
JOIN isms_context ctx ON (c.fkControl = ctx.pkContext AND ctx.nState != ".CONTEXT_STATE_DELETED.")
$msUserJoin_c
  UNION
SELECT 'cost2_control' as ordem, sum(ncost2) as financial_impact
FROM rm_control_cost c
JOIN isms_context ctx ON (c.fkControl = ctx.pkContext AND ctx.nState != ".CONTEXT_STATE_DELETED.")
$msUserJoin_c
  UNION
SELECT 'cost3_control' as ordem, sum(ncost3) as financial_impact
FROM rm_control_cost c
JOIN isms_context ctx ON (c.fkControl = ctx.pkContext AND ctx.nState != ".CONTEXT_STATE_DELETED.")
$msUserJoin_c
  UNION
SELECT 'cost4_control' as ordem, sum(ncost4) as financial_impact
FROM rm_control_cost c
JOIN isms_context ctx ON (c.fkControl = ctx.pkContext AND ctx.nState != ".CONTEXT_STATE_DELETED.")
$msUserJoin_c
  UNION
SELECT 'cost5_control' as ordem, sum(ncost5) as financial_impact
FROM rm_control_cost c
JOIN isms_context ctx ON (c.fkControl = ctx.pkContext AND ctx.nState != ".CONTEXT_STATE_DELETED.")
$msUserJoin_c

  UNION

SELECT 'potentially_low_risk' as ordem, sum(ncost) as financial_impact
FROM rm_risk r
JOIN isms_context ctx ON (r.fkContext = ctx.pkContext AND ctx.nState != ".CONTEXT_STATE_DELETED.")
$msUserJoin_r
WHERE r.nValue !=0 AND r.nValue <= ".ISMSLib::getConfigById(RISK_LOW)." AND r.fkContext NOT IN (SELECT fkRisk FROM rm_risk_control)
  UNION
SELECT 'mitigated_risk' as ordem, sum(ncost) as financial_impact
FROM rm_risk r
JOIN isms_context ctx ON (r.fkContext = ctx.pkContext AND ctx.nState != ".CONTEXT_STATE_DELETED.")
$msUserJoin_r
WHERE r.fkContext IN (SELECT fkRisk FROM rm_risk_control)
  UNION
SELECT 'avoided_risk' as ordem, sum(ncost) as financial_impact
FROM rm_risk r
JOIN isms_context ctx ON (r.fkContext = ctx.pkContext AND ctx.nState != ".CONTEXT_STATE_DELETED.")
$msUserJoin_r
WHERE r.nAcceptMode = ".RISK_ACCEPT_MODE_AVOID." 
  UNION
SELECT 'transfered_risk' as ordem, sum(ncost) as financial_impact
FROM rm_risk r
JOIN isms_context ctx ON (r.fkContext = ctx.pkContext AND ctx.nState != ".CONTEXT_STATE_DELETED.")
$msUserJoin_r
WHERE r.nAcceptMode = ".RISK_ACCEPT_MODE_TRANSFER."
  UNION
SELECT 'accepted_risk' as ordem, sum(ncost) as financial_impact
FROM rm_risk r
JOIN isms_context ctx ON (r.fkContext = ctx.pkContext AND ctx.nState != ".CONTEXT_STATE_DELETED.")
$msUserJoin_r
WHERE r.nAcceptMode = ".RISK_ACCEPT_MODE_ACCEPT."
  UNION
SELECT 'not_treated_and_medium_high_risk' as ordem, sum(ncost) as financial_impact
FROM rm_risk r
JOIN isms_context ctx ON (r.fkContext = ctx.pkContext AND ctx.nState != ".CONTEXT_STATE_DELETED.")
$msUserJoin_r
WHERE r.nAcceptMode = 0 AND r.nValue > ".ISMSLib::getConfigById(RISK_LOW)." AND r.fkContext NOT IN (SELECT fkRisk FROM rm_risk_control)
";
  }

  public function executeQuery() {
    parent::executeQuery();
    $this->caFinancialSummary = array();
    while ($this->coDataSet->fetch())
      $this->caFinancialSummary[$this->coDataSet->getFieldByAlias("ordem")->getValue()] = $this->coDataSet->getFieldByAlias("financial_impact")->getValue();
  }

  public function getFinancialSummary() {
    return $this->caFinancialSummary;
  }
}
?>