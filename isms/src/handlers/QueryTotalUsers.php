<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryTotalUsers.
 *
 * <p>Consulta para buscar o n�mero de usu�rios cadastrados no sistema.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryTotalUsers extends FWDDBQueryHandler {

  private $ciTotalUsers = 0;

  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField("total_users","total_users", DB_NUMBER));
 }

  public function makeQuery() {
    $this->csSQL = "SELECT
                    	count(*) as total_users
                    FROM
                    	isms_user u
                    	JOIN isms_profile p ON (u.fkProfile = p.fkContext AND p.nId <> " . PROFILE_DOCUMENT_READER . ") " .
    				"   JOIN isms_context ctx ON (u.fkContext = ctx.pkContext AND ctx.nState != ".CONTEXT_STATE_DELETED.")";
  }

  public function executeQuery() {
    parent::executeQuery();
    $this->ciTotalUsers = 0;
    if ($this->coDataSet->fetch())
      $this->ciTotalUsers = $this->coDataSet->getFieldByAlias("total_users")->getValue();
  }

  public function getTotalUsers() {
    return $this->ciTotalUsers;
  }

}
?>