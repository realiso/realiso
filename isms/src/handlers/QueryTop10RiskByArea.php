<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryTop10RiskByArea.
 *
 * <p>Consulta para popular o grid do sum�rio Top 10 Riscos X Area.</p>
 * @package ISMS
 * @subpackage handlers
 */
class QueryTop10RiskByArea extends FWDDBQueryHandler {

  protected $caValues = array();
  
  public function __construct($poDB=null){
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField('', 'area_name', DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('', 'asset_name', DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('', 'risk_name', DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('', 'risk_id', DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('', 'area_weight', DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('', 'risk_residual_value', DB_NUMBER));    
  }

  public function makeQuery(){
  	
  	$this->csSQL = "
		SELECT
			min(ar.sname) AS area_name, 
			a.sname AS asset_name, 
			r.sname AS risk_name, 
			r.fkcontext AS risk_id,
			min(cc.nweight) AS area_weight,
			max(r.nvalueresidual) AS risk_residual_value
		FROM view_rm_risk_active r 
		LEFT JOIN view_rm_process_asset_active pa ON (pa.fkasset = r.fkasset)
		LEFT JOIN view_rm_asset_active a ON (a.fkcontext = pa.fkasset)
		LEFT JOIN view_rm_process_active p ON (p.fkcontext = pa.fkprocess)
		LEFT JOIN view_rm_area_active ar ON (ar.fkcontext = p.fkarea)
		LEFT JOIN isms_context_classification cc ON (cc.pkclassification = ar.fkpriority) 
		GROUP BY asset_name, risk_name, risk_id 
		ORDER BY risk_residual_value DESC, area_weight ASC
		LIMIT 10  	 	
  	";
  }
  
  public function executeQuery() {
    parent::executeQuery();
    
    while($this->coDataSet->fetch()){
      $riskId = $this->coDataSet->getFieldByAlias('risk_id')->getValue();
      $riskName = $this->coDataSet->getFieldByAlias('risk_name')->getValue();
      $assetName = $this->coDataSet->getFieldByAlias('asset_name')->getValue();
      $areaName = $this->coDataSet->getFieldByAlias('area_name')->getValue();
      
      $this->caValues[$riskId] = array(
        'risk_name' => $riskName,
        'asset_name' => $assetName,
        'area_name' => $areaName
      );

    }
  }

  public function getValues() {
    $this->makeQuery();
    $this->executeQuery();
    return $this->caValues;
  }  
}

?>