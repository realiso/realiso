<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

class QueryDocumentModelRegisters extends FWDDBQueryHandler {

	private $ciDocumentId;

	public function __construct($poDB,$piDocumentId){
		parent::__construct($poDB);
		$this->ciDocumentId = $piDocumentId;

		$this->coDataSet->addFWDDBField(new FWDDBField('','reg_id',DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField('','reg_name',DB_STRING));
	}

	public function makeQuery(){
		$this->csSQL = "SELECT
							r.fkContext as reg_id,
							r.sName as reg_name
						FROM
							view_pm_register_active r
						WHERE
							r.fkDocument = {$this->ciDocumentId}
						ORDER BY
							r.sName";
	}

	public function executeQuery() {
		parent::executeQuery();
	}
}
?>