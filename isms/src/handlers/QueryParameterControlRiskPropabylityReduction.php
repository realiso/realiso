<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryControlRiskParametersValues.
 *
 * <p>Consulta que retorna a probabilidade de redu��o do risco pelos controles associados a ele.</p>
 * @package ISMS
 * @subpackage select
 */
class QueryParameterControlRiskPropabylityReduction extends FWDDBQueryHandler {
  private $ciRiskId;
	private $ciControlId;
  
  public function __construct($poDB){
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField('','prob_reduction_value',DB_NUMBER));
  }  

  public function setRiskId($piRiskId){
  	$this->ciRiskId = $piRiskId;
  }
  
  public function setControlId($piControlId){
  	$this->ciControlId = $piControlId;
  }

  public function makeQuery(){
		$msControlJoin = '';
		$msControlAnd = '';
		if($this->ciControlId){
			$msControlAnd = 'AND c.fkContext <> '.$this->ciControlId;
		}
		    
$this->csSQL ="
	SELECT sum(rcpvn.nvalue) as prob_reduction_value from rm_risk_control rc
	JOIN rm_rc_parameter_value_name rcpvn ON (rc.fkprobabilityvaluename = rcpvn.pkrcvaluename)
	JOIN rm_control c ON(c.fkContext = rc.fkControl AND c.bIsActive = 1)
	JOIN isms_context ctx ON (ctx.pkContext = c.fkContext AND ctx.nState <> " . CONTEXT_STATE_DELETED . ")
	WHERE rc.fkrisk =".$this->ciRiskId."
	$msControlAnd		
";
}
  
  public function getControlRiskParameterProbabylity(){
		$this->makeQuery();
		if($this->executeQuery()){
			$moDataset = $this->getDataset();
			$moDataset->fetch();
			return 	$moDataset->getFieldByAlias("prob_reduction_value")->getValue();
		}
		else
			return 0;
	} 
}
?>