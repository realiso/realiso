<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe DocumentsDrawGrid
 *
 * <p>Respons�vel por desenhar as grids de documentos.</p>
 * @package ISMS
 * @subpackage classes
 */
class DocumentsDrawGrid extends FWDDrawGrid {

  protected $csGridName;
  protected $ciTomorrow;
  protected $caCtxWithTemplate = array(CONTEXT_AREA,CONTEXT_PROCESS,CONTEXT_ASSET,CONTEXT_CONTROL,CONTEXT_SCOPE,CONTEXT_POLICY,DOCUMENT_TYPE_MANAGEMENT,CONTEXT_CI_ACTION_PLAN);
  
  public function __construct($psGridName){
    $this->csGridName = $psGridName;
    $this->ciTomorrow = strtotime('tomorrow');
  }
  
  public function drawItem(){
    switch($this->ciColumnIndex){
      case $this->getIndexByAlias('document_id'):{
        $miUserId = ISMSLib::getCurrentUserId();
        $miType = $this->getFieldValue('document_type');
        $miState = $this->getFieldValue('document_status');
        $mbIsAuthor = ($this->getFieldValue('document_author')==$miUserId);
        $mbIsMainApprover = ($this->getFieldValue('document_main_approver')==$miUserId);
        $mbIsApprover = $this->getFieldValue('user_is_approver');
        $mbHasApproved = $this->getFieldValue('user_has_approved');
        $miCountApprovers = $this->getFieldValue('count_approvers');
        $mbIsLink = $this->getFieldValue('document_is_link');
        $msFileName = $this->getFieldValue('document_file_name');
        
        $maMenuACLs = array(
          'delete',
          'associate_components',
          'edit_author',
          'edit_readers',
          'copy_readers',
          'edit_approvers',
          'revision',
          'approve',
          'edit',
          'manage_subdocs',
          'previous_versions',
          'registers',
          'references',
          'publish',
          'approval',
          'comments',
          'view_details',
          'view_references',
          'view_subdocs',
          'view_readers',
          'view_approvers',
          'view_components'
        );
        
        $maAllowed = $this->getPermissions();
        $moACL = FWDACLSecurity::getInstance();
        $moACL->setNotAllowed(array_diff($maMenuACLs,$maAllowed));
        $moMenu = FWDWebLib::getObject('menu');
        $moGrid = FWDWebLib::getObject($this->csGridName);
        $msLine = $moGrid->getAttrName().'_acl_'.$this->ciRowIndex;
        FWDGridMenuACLSecurity::getInstance()->installGridMenuACL($moMenu,$moACL,$moGrid,$msLine);
        
        $this->coCellBox->setIconSrc(PMDocument::getDocumentIcon($msFileName,$mbIsLink));
        return $this->coCellBox->draw();
      }
      case $this->getIndexByAlias('document_parent'):{
        $docParentId = $this->getFieldValue('document_parent');
        if($docParentId){
          $pmDocument = new PMDocument();
          $pmDocument->fetchById($docParentId);
          $docName = $this->getFieldValue('document_name');
          $docParentName = $pmDocument->getFieldValue('document_name');
          
          $msToolTipText = FWDLanguage::getPHPStringValue('document_is_subdocument', '<b>%document_name%</b> � um subdocumento.<br>Documento Raiz: <b>%document_parent%</b>');
          $msToolTipText = str_replace("%document_name%", $docName, $msToolTipText);
          $msToolTipText = str_replace("%document_parent%", $docParentName, $msToolTipText);
        
          $moStr = new FWDString();
          $moStr->setAttrValue($msToolTipText);
          
          $moToolTip = new FWDToolTip();
          $moToolTip->setObjFWDString($moStr);        
          
          $moToolTip->setAttrShowDelay(200);
          $this->coCellBox->setObjFWDToolTip($moToolTip);
          
          $cellValue = "<img src=../../gfx/parent_icon.png border=0>";
          $this->coCellBox->setValue($cellValue);
          return $this->coCellBox->draw();
        }
      }
      case $this->getIndexByAlias('document_name'):{
        $miUserId = ISMSLib::getCurrentUserId();
        $miDocumentId = $this->getFieldValue('document_id');
        $miDocInstance = $this->getFieldValue('document_instance_id');
        $miState = $this->getFieldValue('document_status');
        $mbIsMainApprover = ($this->getFieldValue('document_main_approver')==$miUserId);
        $mbIsApprover = $this->getFieldValue('user_is_approver');
        $mbHasApproved = $this->getFieldValue('user_has_approved');
        $mbIsLink = $this->getFieldValue('document_is_link');
        $msLink = $this->getFieldValue('document_link');
        $msFilePath = $this->getFieldValue('document_file_path');
        
        if(($mbIsLink && $msLink) || (!$mbIsLink && is_file($msFilePath))){
          if($mbIsLink){
            $msType = 'link';
            $this->coCellBox->setValue("<a href=$msLink target=_blank>".$this->coCellBox->getValue()."</a>");
          }else{
            $msType = 'file';
            $this->coCellBox->setValue("<a href=javascript:read('$miDocumentId','$miDocInstance','$msType');>".$this->coCellBox->getValue()."</a>");
          }
          //$this->coCellBox->setValue("<a href=javascript:read('$miDocumentId','$miDocInstance','$msType');>".$this->coCellBox->getValue()."</a>");
        }
        
        /* se o documento est� em revis�o
        $mbIsInRevision = false;
        if(($miState == CONTEXT_STATE_DOC_DEVELOPING) &&( $mbIsReleased )){
          $mbIsInRevision = true;
        }
        
        $miDocInstance = 0;
        if ($this->csGridName == 'grid_revision') {
          $miDocInstance = $this->getFieldValue('rev_instance_id');
        }
        elseif($miState==CONTEXT_STATE_DOC_PENDANT){
          if(($mbIsApprover && !$mbHasApproved) || ($mbIsMainApprover && !$mbMainHasApproved)){ //approve
            $miDocInstance = $this->getFieldValue('rev_instance_id');
          }elseif($mbIsApprover || $mbIsMainApprover){ //normal
            $miDocInstance = $this->getFieldValue('document_current_version');
          }
        }elseif($miState==CONTEXT_STATE_DOC_APPROVED || $mbIsReleased){
          $miDocInstance = $this->getFieldValue('document_current_version');
        }
        if($miDocInstance){
          if($miDocInstance==$this->getFieldValue('document_current_version')){
            $mbIsLink = $this->getFieldValue('cv_is_link');
            $msFilePath = $this->getFieldValue('cv_file_path');
          }else{
            $mbIsLink = $this->getFieldValue('rev_is_link');
            $msFilePath = $this->getFieldValue('rev_file_path');
          }
          if($mbIsLink){
            $this->coCellBox->setValue("<a href=javascript:read('".$miDocumentId."','".$miDocInstance."','link');>".$this->coCellBox->getValue()."</a>");
          }elseif($msFilePath && is_file($msFilePath)){
            $this->coCellBox->setValue("<a href=javascript:read('".$miDocumentId."','".$miDocInstance."','file');>".$this->coCellBox->getValue()."</a>");
          }
        }
        //*/
        return $this->coCellBox->draw();
      }
      case $this->getIndexByAlias('context_name'):{
        $msIconName = '';
        switch($this->getFieldValue('document_type')){
          case CONTEXT_AREA:          $msIconName="icon-area_gray.gif";     break;
          case CONTEXT_PROCESS:       $msIconName="icon-process_gray.gif";  break;
          case CONTEXT_ASSET:         $msIconName="icon-asset_gray.gif";    break;
          case CONTEXT_CONTROL:       $msIconName="icon-control.gif";       break;
          case CONTEXT_BEST_PRACTICE: $msIconName="icon-best_practice.gif"; break;
          case CONTEXT_POLICY:        $msIconName="icon-policy.gif";
              $this->coCellBox->setValue(str_replace("%context_policy%",ISMSContextObject::getContextObject(CONTEXT_POLICY)->getLabel(),$this->coCellBox->getValue()));
          break;
          case CONTEXT_SCOPE:         $msIconName="icon-scope.gif";
              $this->coCellBox->setValue(str_replace("%context_scope%",ISMSContextObject::getContextObject(CONTEXT_SCOPE)->getLabel(),$this->coCellBox->getValue()));
          break;
          case CONTEXT_CI_ACTION_PLAN: $msIconName="icon-ci_action_plan.gif"; break;
          default:                    $msIconName="";                       break;
        }
        
        $msValue = trim($this->coCellBox->getValue());
        if($msIconName){
          $msToolTipText = $msValue;
          $moToolTip = new FWDToolTip();
          $moToolTip->setAttrShowDelay(1000);
          //$msToolTipText = str_replace('\\','\\\\',$msToolTipText);
          //$msToolTipText = str_replace('"','\"',$msToolTipText);
          $moStr = new FWDString();
          $moStr->setAttrValue($msToolTipText);
          $moToolTip->setObjFWDString($moStr);
          $this->coCellBox->setObjFWDToolTip($moToolTip);
          $moIcon = new FWDIcon();
          $moIcon->setAttrSrc(FWDWebLib::getInstance()->getGfxRef().$msIconName);
          $this->coCellBox->setAttrStringNoEscape('true');
          $this->coCellBox->setValue($moIcon->draw().'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$msValue);
        }
        return $this->coCellBox->draw();
      }
      case $this->getIndexByAlias('count_sub_doc'):{
        if(!$this->coCellBox->getValue()) $this->coCellBox->setValue(0);
        return $this->coCellBox->draw();
      }
      case $this->getIndexByAlias('document_classification'):{
        $moContextClassification = new ISMSContextClassification();
        $moContextClassification->fetchById($this->coCellBox->getValue());
        $msContextClassification = $moContextClassification->getFieldValue('classif_name');
        $this->coCellBox->setValue($msContextClassification);
        return $this->coCellBox->draw();
      }
      case $this->getIndexByAlias('document_status'):{
        $moContext = new ISMSContextObject();
        $mbIsReleased = $this->getFieldValue('document_is_released');
        if(($this->coCellBox->getValue() == CONTEXT_STATE_DOC_DEVELOPING) AND ($mbIsReleased) ){
          $msStatus = $moContext->getContextStateAsString(CONTEXT_STATE_DOC_REVISION);
       }else{
          $msStatus = $moContext->getContextStateAsString($this->coCellBox->getValue());
        }
        $this->coCellBox->setValue($msStatus);
        
        // destaca documentos no estado publicado, mas com data futura
        $miState = $this->getFieldValue('document_status');
        if($miState==CONTEXT_STATE_DOC_APPROVED){
          $msDate = substr($this->getFieldValue('document_date_production'),0,strpos($this->getFieldValue('document_date_production'),' '));
          $miDate = ISMSLib::getTimestamp($msDate);
          if($miDate >= $this->ciTomorrow){
            $moToolTip = new FWDToolTip();
            $msToolTip = FWDLanguage::getPHPStringValue('to_future_publication',"O documento somente estar� dispon�vel para os leitores no dia %date%.");
            $moToolTip->setAttrShowDelay(1000);
            $moToolTip->setValue(str_replace('%date%',ISMSLib::getISMSShortDate($msDate),$msToolTip));
            $this->coCellBox->setObjFWDToolTip($moToolTip);
            $this->coCellBox->setAttrClass('DocFutPub');
          }
        }
        
        return $this->coCellBox->draw();
      }
      case $this->getIndexByAlias('document_version'):{
        if (!ISMSLib::getConfigById(GENERAL_DOCUMENTS_MANUAL_VERSIONING) || !$this->coCellBox->getValue()) {
          $miMajorVersion = $this->getFieldValue('document_major_version');
          $miRevisionVersion = $this->getFieldValue('document_revision_version');
          if(!$miMajorVersion) $miMajorVersion = 0;
          if(!$miRevisionVersion) $miRevisionVersion = 0;
          $this->coCellBox->setValue("{$miMajorVersion}.{$miRevisionVersion}");
        }
        return  parent::drawItem();
      }
      default:{
        return parent::drawItem();
      }
    }
  }
  
  public function getPermissions(){
    $miUserId = ISMSLib::getCurrentUserId();
    return PMDocument::getPermissions(
      $this->getFieldValue('document_status'),
      $this->getFieldValue('doc_instance_status'),
      ($this->getFieldValue('document_main_approver')==$miUserId),
      ($this->getFieldValue('document_author')==$miUserId),
      $this->getFieldValue('user_is_approver'),
      $this->getFieldValue('user_is_reader'),
      $this->getFieldValue('user_has_approved'),
      $this->getFieldValue('count_approvers'),
      $this->getFieldValue('document_is_link'),
      $this->getFieldValue('document_file_path'),
      $this->getFieldValue('document_link'),
      $this->getFieldValue('document_type')
    );
  }
  
}

?>