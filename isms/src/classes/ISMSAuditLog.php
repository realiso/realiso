<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

// A��es da auditoria
define("ADT_ACTION_CREATE", 2601);
define("ADT_ACTION_EDIT", 2602);
define("ADT_ACTION_SYSTEM_REMOVE", 2603);
define("ADT_ACTION_GARBAGE_REMOVE", 2604);
define("ADT_ACTION_RESTORE", 2605);
define("ADT_ACTION_LOGIN", 2606);
define("ADT_ACTION_LOGOUT", 2607);
define("ADT_ACTION_BAD_LOGIN", 2608);
define("ADT_ACTION_BLOCKED_LOGIN", 2609);
define("ADT_ACTION_LOGIN_FAILED",2610);
define("ADT_ACTION_LOGIN_ERROR",2611);

/**
 * Classe ISMSAuditLog.
 *
 * <p>Classe que representa a tabela de log do sistema.</p>
 * @package ISMS
 * @subpackage classes
 */
class ISMSAuditLog extends ISMSTable {

  protected $caAction = array();
		
 /**
  * Construtor.
  * 
  * <p>Construtor da classe ISMSAuditLog.</p>
  * @access public 
  */
  public function __construct(){		
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
  	parent::__construct("isms_audit_log");
  	
  	$this->csAliasId = '';  	
  	
  	$this->coDataset->addFWDDBField(new FWDDBField("dDate",				"log_date",					DB_DATETIME));
  	$this->coDataset->addFWDDBField(new FWDDBField("sUser",				"log_user",					DB_STRING));  	  	
  	$this->coDataset->addFWDDBField(new FWDDBField("nAction",			"log_action",				DB_NUMBER));  	
  	$this->coDataset->addFWDDBField(new FWDDBField("sTarget",			"log_element",			DB_STRING));
  	$this->coDataset->addFWDDBField(new FWDDBField("tDescription","log_description",	DB_STRING));
  	
  	$this->caAction = array(
  		ADT_ACTION_CREATE => FWDLanguage::getPHPStringValue('ad_created', 'Criado(a)'),
  		ADT_ACTION_EDIT => FWDLanguage::getPHPStringValue('ad_edited', 'Editado(a)'),
  		ADT_ACTION_SYSTEM_REMOVE => FWDLanguage::getPHPStringValue('ad_removed_from_system', 'Removido(a) do sistema'),
  		ADT_ACTION_GARBAGE_REMOVE => FWDLanguage::getPHPStringValue('ad_removed_from_bin', 'Removido(a) da lixeira'),
  		ADT_ACTION_RESTORE => FWDLanguage::getPHPStringValue('ad_restored', 'Restaurado(a)'),
  		ADT_ACTION_LOGIN => FWDLanguage::getPHPStringValue('ad_login', 'entrou no sistema'),
  		ADT_ACTION_LOGOUT => FWDLanguage::getPHPStringValue('ad_logout', 'saiu do sistema'),
      ADT_ACTION_BAD_LOGIN => FWDLanguage::getPHPStringValue('ad_action_bad_login', 'tentou efetuar login com uma senha inv�lida'),
      ADT_ACTION_BLOCKED_LOGIN => FWDLanguage::getPHPStringValue('ad_action_blocked_login', 'est� bloqueado e tentou efetuar login no sistema'),
      ADT_ACTION_LOGIN_FAILED => FWDLanguage::getPHPStringValue('ad_login_failed', 'tentou entrar no sistema por�m n�o possui permiss�o'),
      ADT_ACTION_LOGIN_ERROR =>  FWDLanguage::getPHPStringValue('ad_login_error', 'tentou efetuar login com um nome de usu�rio inv�lido')
  	);	
  }
 
 /**
  * Retorna a string que descreve a a��o.
  * 
  * <p>M�todo para retornar a string que descreve a a��o.
  * Dispara um erro caso o id da a��o passada n�o existir.</p>
  * @access public 
  * @param integer $piAction Id da a��o
  * @return string Descri��o da a��o
  */
  public function getAction($piAction) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
  	if (isset($this->caAction[$piAction])) return $this->caAction[$piAction];
  	else trigger_error('Action id not found:'.$piAction, E_USER_WARNING);
  }
  
  public function insert() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);    
    parent::insert();
  }
}
?>