<?
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSHandleJSError.
 *
 * <p></p>
 * @package FWD5
 * @subpackage eventClass
 */
class ISMSHandleJSError extends FWDRunnable {

  public function run(){
    $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    $miUserId = 0;
    
    if($moSession)
      $miUserId = $moSession->getUserId();    
    
    $msErrorMsg = "<b>FWD Exception Caught:</b> FWD JS ERROR<br>"
                 ."<br>&nbsp;&nbsp;&nbsp; File: ".$_SERVER['SCRIPT_FILENAME']
                 ."<br>&nbsp;&nbsp;&nbsp; User: $miUserId <br><br>";
    $maPost = $_POST;

    unset($maPost['isms_js_exception']);
    $msErrorDump = FWDWebLib::getPOST('isms_js_exception')
                  .'<pre>$_POST = '.var_export($maPost,true).'</pre>';
    
    ISMSSaasErrorHandler::sendErrorReport($msErrorMsg,$msErrorDump);
  }

}
?>