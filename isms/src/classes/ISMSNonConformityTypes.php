<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSNonConformityTypes.
 *
 * <p>Classe que representa a tabela de tipos de n�o conformidade.</p>
 * @package ISMS
 * @subpackage classes
 */
class ISMSNonConformityTypes extends ISMSTable {
  
 /**
  * Construtor.
  * 
  * <p>Construtor da classe ISMSNonConformityTypes.</p>
  * @access public 
  */
  public function __construct(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    parent::__construct("isms_non_conformity_types");
    
    $this->csAliasId = 'non_conform_id';

    $this->coDataset->addFWDDBField(new FWDDBField("pkClassification",     "non_conform_id",	DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("sName",  "non_conform_name",				DB_STRING));
  }
}
?>