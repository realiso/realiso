<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Interface para classes que representam uma associa��o de duas tabelas.
 *
 * <p>Interface para elementos que representam uma associa��o de duas tabelas.</p>
 * @package ISMS
 * @subpackage classes
 */
interface IAssociation {	
 /**
  * Retorna o identificador do primeiro contexto da rela��o.
  * 
  * <p>M�todo para retornar o identificador do primeiro contexto da rela��o.</p>
  * @access public 
  * @return string Alias
  */
  public function getFirstContextId();
 
 /**
  * Retorna o identificador do segundo contexto da rela��o.
  * 
  * <p>M�todo para retornar o identificador do segundo contexto da rela��o.</p>
  * @access public 
  * @return string Alias
  */
  public function getSecondContextId(); 
}
?>