<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

// Tipos de Classificações
define("CONTEXT_CLASSIFICATION_TYPE", 5501);
define("CONTEXT_CLASSIFICATION_PRIORITY", 5502);
define("CONTEXT_CLASSIFICATION_FINANCIAL_IMPACT", 5503);

/**
 * Classe ISMSContextClassification.
 *
 * <p>Classe que representa a tabela de classificação de contextos.</p>
 * @package ISMS
 * @subpackage classes
 */
class ISMSContextClassification extends ISMSTable {
  
 /**
  * Construtor.
  * 
  * <p>Construtor da classe ISMSContextClassification.</p>
  * @access public 
  */
  public function __construct(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    parent::__construct("isms_context_classification");
    
    $this->csAliasId = 'classif_id';
    
    $this->coDataset->addFWDDBField(new FWDDBField("pkClassification",     "classif_id",           DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("nContextType",         "classif_context_type", DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("sName",                "classif_name",         DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField("nClassificationType",  "classif_type",         DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("nWeight",  "classif_weight",         DB_NUMBER));
  }
}
?>