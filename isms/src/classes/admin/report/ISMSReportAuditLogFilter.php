<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportAuditLogFilter.
 *
 * <p>Classe que implementa o filtro do relat�rio de auditoria do log do sistema.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportAuditLogFilter extends FWDReportFilter {
	
	protected $ciDateStart = 0;
	protected $ciDateFinish = 0;	
	protected $ciAction = 0;
	protected $csUser = "";	

	public function setDateStart($piDate) {
		$this->ciDateStart = $piDate;
	}

	public function getDateStart() {
		return $this->ciDateStart;
	}

	public function setDateFinish($piDate) {
		$this->ciDateFinish = $piDate;
	}

	public function getDateFinish() {
		return $this->ciDateFinish;
	}

	public function setUser($psUser) {
		$this->csUser = $psUser;
	}

	public function getUser() {
		return $this->csUser;
	}
	
	public function setAction($piAction) {
		$this->ciAction = $piAction;
	}

	public function getAction() {
		return $this->ciAction;
	}
	
	public function getSummary(){
    $maFilterDate = array();
    $msDateLimit = '';
		if($this->ciDateStart){
    	$msDateLimit = FWDLanguage::getPHPStringValue('rs_log_after', 'Log depois de');
    	$msDateLimit .= ' ' . ISMSLib::getISMSShortDate($this->ciDateStart, true);
    }
    if($this->ciDateFinish){    	
    	if ($msDateLimit) {
    		$msDateLimit .= ' ' . FWDLanguage::getPHPStringValue('rs_and_before', 'e antes de');
    		$msDateLimit .= ' ' . ISMSLib::getISMSShortDate($this->ciDateFinish, true);
    	}
    	else {
    		$msDateLimit = FWDLanguage::getPHPStringValue('rs_log_before', 'Log antes de');
    		$msDateLimit .= ' ' . ISMSLib::getISMSShortDate($this->ciDateFinish, true);
    	}    	
    }
    if (!$msDateLimit) $msDateLimit .= FWDLanguage::getPHPStringValue('rs_any_date', 'Qualquer data');
    $maFilterDate[] = $msDateLimit;
    
    $maFilterUser = array();
    if ($this->csUser) $maFilterUser[] = $this->csUser;
    else $maFilterUser[] = FWDLanguage::getPHPStringValue('st_all_users', 'Todos usu�rios');
    
    $maFilterAction = array();
    if ($this->ciAction) {
			$moAuditLog = new ISMSAuditLog();			
			$maFilterAction[] = $moAuditLog->getAction($this->ciAction);
    }
    else $maFilterAction[] = FWDLanguage::getPHPStringValue('st_all', 'Todas');    
            
    $maFilters = array();
    $maFilters[] = array(
      'name' => FWDLanguage::getPHPStringValue('rs_creation_date_cl', 'Data de cria��o:'),
      'items' => $maFilterDate
    );
    $maFilters[] = array(
      'name' => FWDLanguage::getPHPStringValue('rs_creator_cl', 'Criador:'),
      'items' => $maFilterUser
    );
    $maFilters[] = array(
      'name' => FWDLanguage::getPHPStringValue('rs_action_cl', 'A��o:'),
      'items' => $maFilterAction
    );
    
    return $maFilters;
  }
}
?>