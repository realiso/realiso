<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe ISMSReportAuditTask.
 *
 * <p>Classe que implementa o relat�rio de auditoria de tarefas.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportAuditTask extends ISMSReport {
	
	/**
	 * Inicializa o relat�rio.
	 *
	 * <p>M�todo para inicializar o relat�rio.
	 * Necess�rio para funcionar com o XML compilado.</p>
	 * @access public
	 */	
	public function init() {
		parent::init();		
		
		$this->coDataSet->addFWDDBField(new FWDDBField("","task_id", DB_NUMBER));
		$this->coDataSet->addFWDDBField(new FWDDBField("","task_creation_date", DB_DATETIME));
		$this->coDataSet->addFWDDBField(new FWDDBField("","task_accomplished_date", DB_DATETIME));
		$this->coDataSet->addFWDDBField(new FWDDBField("","task_creator", DB_STRING));
		$this->coDataSet->addFWDDBField(new FWDDBField("","task_receiver", DB_STRING));		
		$this->coDataSet->addFWDDBField(new FWDDBField("","task_context_name", DB_STRING));		
		$this->coDataSet->addFWDDBField(new FWDDBField("","task_activity", DB_NUMBER));				
	}
	
	/**
	 * Executa a query do relat�rio.
	 *
	 * <p>M�todo para executar a query do relat�rio.</p>
	 * @access public
	 */
	public function makeQuery() {
		$moFilter = $this->getFilter();
		$maFilters = array();		
    if($moFilter->getCreator()){
      $maFilters[] = "t.fkCreator = ".$moFilter->getCreator();
    }
    if($moFilter->getReceiver()){
      $maFilters[] = "t.fkReceiver = ".$moFilter->getReceiver();
    }
    if($moFilter->getActivity()){
      $maFilters[] = "t.nActivity = ".$moFilter->getActivity();
    }
    if($moFilter->getDateCreationStart()){
      $maFilters[] = "t.dDateCreated >= ".ISMSLib::getTimestampFormat($moFilter->getDateCreationStart());
    }
    if($moFilter->getDateCreationFinish()){
      $maFilters[] = "t.dDateCreated <= ".ISMSLib::getTimestampFormat($moFilter->getDateCreationFinish());
    }
    
    $msWhere = implode(' AND ', $maFilters);
    if ($msWhere) $msWhere = ' WHERE ' . $msWhere;
    
		$this->csQuery = "SELECT t.pkTask as task_id, t.dDateCreated as task_creation_date, t.dDateAccomplished as task_accomplished_date,
												uc.sName as task_creator, ur.sName as task_receiver, cn.context_name as task_context_name,
												t.nActivity as task_activity
											FROM wkf_task t
											JOIN isms_context c ON (t.fkContext = c.pkContext)
											JOIN context_names cn ON (t.fkContext = cn.context_id)
											JOIN isms_user uc ON (t.fkCreator = uc.fkContext)
											JOIN isms_user ur ON (t.fkReceiver = ur.fkContext)
											$msWhere ORDER BY t.dDateCreated DESC";
		
		return parent::executeQuery();
	}
	
	/**
	 * Desenha o cabe�alho do relat�rio.
	 *
	 * <p>M�todo para desenhar o cabe�alho do relat�rio.</p>
	 * @access public
	 */
	public function drawHeader(){
    foreach($this->caHeaders as $moHeader){
      if($moHeader->getAttrName()=='header_filter'){
        $moHeaderFilter = $moHeader;
      }elseif($moHeader->getAttrName()=='header_filter_item'){
        $moHeaderFilterItem = $moHeader;
      }elseif($moHeader->getAttrName()=='report_header_blank'){
        $moHeaderBlank = $moHeader;
      }else{
        $this->coWriter->drawLine($moHeader,array());
      }
    }
    if ($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());
    
    $moFilterText = FWDWebLib::getObject('header_filter_text');
    $moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
    $maFilters = $this->coFilter->getSummary();
    foreach($maFilters as $maFilter){
      $moFilterText->setValue($maFilter['name']);
      $this->coWriter->drawLine($moHeaderFilter,array());
      foreach($maFilter['items'] as $msFilterItem){
        $moFilterItemText->setValue($msFilterItem);
        $this->coWriter->drawLine($moHeaderFilterItem,array());
      }
    }
    $this->coWriter->drawLine($moHeaderBlank,array());
  }
}
?>