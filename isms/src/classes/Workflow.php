<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

// A��es do workflow
define("WKF_ACTION_NONE",                         9500);
define("WKF_ACTION_EDIT",                         9501);
define("WKF_ACTION_APPROVE",                      9502);
define("WKF_ACTION_DENY",                         9503);
define("WKF_ACTION_COAPPROVE",                    9504);
define("WKF_ACTION_DELETE",                       9505);
define("WKF_ACTION_UNDELETE",                     9506);
define("WKF_ACTION_SEND_DOC_TO_APPROVE",          9507);
define("WKF_ACTION_DOC_REVISION",                 9508);
define("WKF_ACTION_CI_NC_SEND_TO_RESPONSIBLE",    9509);
define("WKF_ACTION_CI_NC_SEND_TO_AP_RESPONSIBLE", 9510);
define("WKF_ACTION_CI_NC_FINISH",                 9511);
define("WKF_ACTION_SEND_NC_TO_APPROVE",           9512);

define("WKF_ACTION_INC_SEND_TO_RESPONSIBLE",      9513);
define("WKF_ACTION_INC_SEND_TO_APP_DISPOSAL",     9514);
define("WKF_ACTION_INC_APP_DISPOSAL",             9515);
define("WKF_ACTION_INC_DENY_DISPOSAL",            9516);
define("WKF_ACTION_INC_SEND_TO_APP_SOLUTION",     9517);
define("WKF_ACTION_INC_APP_SOLUTION",             9518);
define("WKF_ACTION_INC_DENY_SOLUTION",            9519);

/*action plan*/
define("WKF_ACTION_AP_SEND_TO_WAINTING_CONCLUSION",    9520);
define("WKF_ACTION_AP_SEND_TO_WAINTING_MEASUREMENT",   9521);
define("WKF_ACTION_AP_WAINTING_MEASUREMENT",           9522);
/*NC*/
define("WKF_ACTION_CI_NC_CONCLUSION",                  9523);
define("WKF_ACTION_CI_NC_SEND_TO_WAITING_CONCLUSION",  9524);
/*incidente*/
define("WKF_ACTION_SEND_TO_APPROVAL",                  9525);

/**
 * Classe Workflow.
 *
 * <p>Classe que manipula os estados do workflow.
 * O workflow � respons�vel por disparar as devidas tarefas.
 * � ele que sabe quais e quantas tarefas ser�o disparadas, de acordo
 * com o estado do contexto utilizado.</p>
 * @package ISMS
 * @subpackage classes
 */
abstract class Workflow {

 /**
  * C�digo da atividade
  * @var integer
  * @access protected
  */
  protected $ciActivity = 0;

 /**
  * Construtor.
  *
  * <p>Construtor da classe Workflow.</p>
  * @access public
  */
  public function __construct($piActivity){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $this->ciActivity = $piActivity;
  }

 /**
  * Dispara uma tarefa.
  *
  * <p>M�todo para disparar uma tarefa.</p>
  * @access public
  *
  * @param integer $piActivity Id da atividade
  * @param integer $piContextId Id do contexto
  * @param integer $piTaskReceiverId Id do receptor da tarefa
  */
  public function dispatchTask($piActivity, $piContextId, $piTaskReceiverId){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $miCreatorId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();

    if(!$miCreatorId){
    	$miCreatorId = ISMSLib::getConfigById(USER_CHAIRMAN);
    	
        if(!$miCreatorId){
    		return;
    	}    	
    }
    
    $moTask = new WKFTask();
    $moTask->setFieldValue('task_context_id', $piContextId);
    $moTask->setFieldValue('task_creator_id', $miCreatorId);
    $moTask->setFieldValue('task_receiver_id', $piTaskReceiverId);
    $moTask->setFieldValue('task_activity', $piActivity);
    $moTask->setFieldValue('task_date_created', ISMSLib::ISMSTime());
    $moTask->setFieldValue('task_is_visible', 1);

    $moTask->insert();
  }

 /**
  * Dispara um alerta.
  *
  * <p>M�todo para disparar um alerta.</p>
  * @access public
  *
  * @param integer $piAlertType Tipo de alerta
  * @param IWorkflow $poContext Objeto de contexto
  * @param string $psJustification Justificativa
  * @param integer $piReceiver Id do destinat�rio do alerta (por default, � o respons�vel pelo contexto)
  */
  public function dispatchAlert($piAlertType, IWorkflow $poContext, $psJustification = '', $piReceiver=0){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $miReceiver = ($piReceiver?$piReceiver:$poContext->getResponsible());
    $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    $moAlert = new WKFAlert();
    $moAlert->setFieldValue('alert_type', $piAlertType);
    $moAlert->setFieldValue('alert_context_id', $poContext->getId());
    $moAlert->setFieldValue('alert_creator_id', $miUserId);
    $moAlert->setFieldValue('alert_receiver_id', $miReceiver);
    $moAlert->setFieldValue('alert_justification', $psJustification);
    $moAlert->setFieldValue('alert_date', ISMSLib::ISMSTime());

    $moAlert->insert();
  }

 /**
  * Avan�a o estado do contexto no workflow.
  *
  * <p>M�todo para avan�ar o estado do contexto no workflow.</p>
  * @access public
  *
  * @param IWorkflow $poContext Objeto de contexto
  * @param integer $piAction Tipo da a��o
  * @param string $psJustification Justificativa
  */
  abstract public function stateForward(IWorkflow $poContext, $piAction, $psJustification = '');

}
?>