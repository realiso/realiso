<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSUserPreferences.
 *
 * <p>Classe que manipula as prefer�ncias de usu�rio.</p>
 * @package ISMS
 * @subpackage classes
 */
class ISMSUserPreferences extends ISMSTable {
  
 /**
  * Id do usu�rio default
  * @var integer
  */
  protected $ciUserId;
  
 /**
  * Objeto de sess�o
  * @var ISMSSession
  */
  protected $coSession;
  
 /**
  * Construtor.
  * 
  * <p>Construtor da classe ISMSUserPreferences.</p>
  * @access public
  */
  public function __construct(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $this->coSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    $this->ciUserId = $this->coSession->getUserId(true);
    $this->coDataSet = new FWDDBDataSet(FWDWebLib::getConnection(),'isms_user_preference');
    $this->coDataSet->addFWDDBField(new FWDDBField('fkUser',     'preference_user', DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('sPreference','preference_id',   DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('tValue',     'preference_value',DB_STRING));
  }

 /**
  * Seta o valor de uma prefer�ncia de usu�rio.
  *
  * <p>Seta o valor de uma prefer�ncia de usu�rio.</p>
  * @param string $psPreferenceId Identificador da prefer�ncia
  * @param string $psValue Valor da prefer�ncia
  * @param string $piUserId Id do usu�rio
  * @access public
  */
  public function setPreference($psPreferenceId,$psValue,$piUserId=0){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $miUserId = ($piUserId?$piUserId:$this->ciUserId);
    if($miUserId==$this->ciUserId){
      if(!$this->coSession->attributeExists($psPreferenceId)){
        $this->coSession->addAttribute($psPreferenceId);
      }
      $this->coSession->{'setAttr'.$psPreferenceId}($psValue);
    }
    $this->coDataSet->getFieldByAlias('preference_user')->addFilter(new FWDDBFilter('=',$miUserId));
    $this->coDataSet->getFieldByAlias('preference_id')->addFilter(new FWDDBFilter('=',$psPreferenceId));
    $this->coDataSet->select();
    if($this->coDataSet->fetch()){
      $this->coDataSet->getFieldByAlias('preference_value')->setValue($psValue);
      $this->coDataSet->update();
    }else{
      $this->coDataSet->getFieldByAlias('preference_user')->setValue($miUserId);
      $this->coDataSet->getFieldByAlias('preference_id')->setValue($psPreferenceId);
      $this->coDataSet->getFieldByAlias('preference_value')->setValue($psValue);
      $this->coDataSet->insert();
    }
  }
  
 /**
  * Testa se existe uma prefer�nica de usu�rio.
  *
  * <p>Testa se existe uma prefer�nica de usu�rio, caso exista e o usu�rio da prefer�ncia for o usu�rio logado, 
  * carrega a prefer�nica para a sess�o caso ela n�o esteja armazenada na sess�o</p>
  * @param string $psPreferenceId Identificador da prefer�ncia
  * @param string $piUserId Id do usu�rio
  * @return string Valor da prefer�ncia
  * @access public
  */
  public function preferenceUserExistes($psPreferenceId,$piUserId = 0){
    $mbReturn = false;
    $miUserId = ($piUserId?$piUserId:$this->ciUserId);
    if($miUserId==$this->ciUserId){
      if($this->coSession->attributeExists($psPreferenceId)){
        $mbReturn = true;
      }else{
        $this->coDataSet->getFieldByAlias('preference_user')->addFilter(new FWDDBFilter('=',$miUserId));
        $this->coDataSet->getFieldByAlias('preference_id')->addFilter(new FWDDBFilter('=',$psPreferenceId));
        $this->coDataSet->select();
        if($this->coDataSet->fetch()){
          $this->coSession->addAttribute($psPreferenceId);
          $msValue = $this->coDataSet->getFieldByAlias('preference_value')->getValue();
          $this->coSession->{'setAttr'.$psPreferenceId}($msValue);
          $mbReturn = true;
        }
      }
    }else{
      $this->coDataSet->getFieldByAlias('preference_user')->addFilter(new FWDDBFilter('=',$miUserId));
      $this->coDataSet->getFieldByAlias('preference_id')->addFilter(new FWDDBFilter('=',$psPreferenceId));
      $this->coDataSet->select();
      if($this->coDataSet->fetch()){
        $mbReturn = true;
      }
    }
    return $mbReturn;
  }
 /**
  * Retorna o valor de uma prefer�ncia de usu�rio.
  *
  * <p>Retorna o valor de uma prefer�ncia de usu�rio.</p>
  * @param string $psPreferenceId Identificador da prefer�ncia
  * @param string $piUserId Id do usu�rio
  * @return string Valor da prefer�ncia
  * @access public
  */
  public function getPreference($psPreferenceId,$piUserId=0){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $miUserId = ($piUserId?$piUserId:$this->ciUserId);
    if($miUserId==$this->ciUserId){
      if($this->coSession->attributeExists($psPreferenceId)){
        return $this->coSession->{'getAttr'.$psPreferenceId}();
      }else{
        $this->coSession->addAttribute($psPreferenceId);
      }
    }
    $this->coDataSet->getFieldByAlias('preference_user')->addFilter(new FWDDBFilter('=',$miUserId));
    $this->coDataSet->getFieldByAlias('preference_id')->addFilter(new FWDDBFilter('=',$psPreferenceId));
    $this->coDataSet->select();
    if($this->coDataSet->fetch()){
      $msValue = $this->coDataSet->getFieldByAlias('preference_value')->getValue();
    }else{
      $msValue = '';
    }
    if($miUserId==$this->ciUserId){
      $this->coSession->{'setAttr'.$psPreferenceId}($msValue);
    }
    return $msValue;
  }
  
 /**
  * Deleta uma prefer�ncia de usu�rio.
  *
  * <p>Deleta uma prefer�ncia de usu�rio.</p>
  * @param string $psPreferenceId Identificador da prefer�ncia
  * @param string $piUserId Id do usu�rio
  * @access public
  */
  public function deletePreference($psPreferenceId,$piUserId=0){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $miUserId = ($piUserId?$piUserId:$this->ciUserId);
    if($miUserId==$this->ciUserId && $this->coSession->attributeExists($psPreferenceId)){
      $this->coSession->deleteAttribute($psPreferenceId);
    }
    $this->coDataSet->getFieldByAlias('preference_user')->addFilter(new FWDDBFilter('=',$miUserId));
    $this->coDataSet->getFieldByAlias('preference_id')->addFilter(new FWDDBFilter('=',$psPreferenceId));
    $this->coDataSet->delete();
  }

}
?>