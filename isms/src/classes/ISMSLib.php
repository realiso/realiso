<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

define('RISK_MANAGEMENT_MODE', 601);
define('LIBRARIES_MODE', 602);
define('ADMIN_MODE', 603);
define('MANAGER_MODE', 604);
define('POLICY_MODE', 605);
define('INCIDENT_MODE', 606);

define('LANGUAGE_PORTUGUESE_ISMS', 3301);
define('LANGUAGE_ENGLISH_ISMS', 3302);
define('LANGUAGE_CHINESE_ISMS', 3303);
define('LANGUAGE_SPANISH_ISMS', 3310);
define('LANGUAGE_PORTUGUESE_EMS', 3304);
define('LANGUAGE_ENGLISH_EMS', 3306);
define('LANGUAGE_PORTUGUESE_OHS', 3305);
define('LANGUAGE_ENGLISH_OHS', 3307);
define('LANGUAGE_ENGLISH_SOX', 3308);
define('LANGUAGE_ENGLISH_PCI', 3309);
define('LANGUAGE_GERMAN_SOX', 3310);
define('LANGUAGE_FRENCH_SOX', 3311);
define('LANGUAGE_PORTUGUESE_THREE', 3312);

define('SCHEDULE_BYDAY',7801);
define('SCHEDULE_BYWEEK',7802);
define('SCHEDULE_BYMONTH',7803);

define('WEEK_INTERVAL'      ,7941);
define('MONTH_INTERVAL'     ,7942);
define('TREE_MONTH_INTERVAL',7943);
define('SIX_MONTH_INTERVAL' ,7944);
define('YEAR_INTERVAL'      ,7945);

define('RISK_FORMULA_TYPE_1'      ,8301);
define('RISK_FORMULA_TYPE_2'      ,8302);

define('GRAPH_FONT', 'ARIALUNI.TTF'); // "verdana.ttf"
define('GRAPH_FONT_BOLD', 'ARIALUNI.TTF'); // "verdanab.ttf"


include_once $handlers_ref . "QueryContextCreateModify.php";

/**
 * Classe ISMSLib. Singleton. Implementa vari�veis e m�todos globais do ISMS.
 *
 * <p>Classe singleton que implementa vari�veis e m�todos globais do ISMS.
 * Garante a exist�ncia de uma e apenas uma inst�ncia da classe ISMSLib.</p>
 * @package ISMS
 * @subpackage classes
 */
class ISMSLib {

  /**
	 * Inst�ncia �nica da classe ISMSLib
	 * @staticvar ISMSLib
	 * @access private
	 */
  private static $coISMSLib = NULL;

  /**
	 * Atributo que controla a build do ISMS
	 * @var string
	 * @access private
	 */
  private $csBuild = '$Id: ISMSLib.php 3612 2011-10-03 15:09:49Z drosseti $';

  /**
	 * Atributo que controla a vers�o do ISMS
	 * @var string
	 * @access private
	 */
  private $csVersion = '1.3.11';

  /**
	 * Id da sess�o atual
	 * @var string
	 * @access private
	 */
  private static $csCurrentSessionId = '';

  /**
	 * Objeto de configura��o do ISMS
	 * @var Object ISMSConfig
	 * @access private
	 */
  private static $coISMSConfig;

  /**
   * Array de �cones de Documentos
   * @var array ExtMime
   * @access private
   */
  private static $caExtMime = array(
  ".xls.xlsx.csv.xlt,ext-xls.gif",
  ".htm.mht.html,ext-htm.gif",
  ".bmp,ext-bmp.gif",
  ".ini.inf,ext-ini.gif",
  ".jpg.png.gif,ext-jpg.gif",
  ".tif.tiff,ext-tiff.gif",
  ".mpg.mpeg.mp3.wav.avi.mov,ext-mpg.gif",
  ".dbx.pst.eml,ext-eml.gif",
  ".pdf,ext-pdf.gif",
  ".pps,ext-pps.gif",
  ".ppt.pptx,ext-ppt.gif",
  ".dll.exe.bat.com.pif.scr.cmd,ext-system.gif",
  ".txt,ext-txt.gif",
  ".doc.docx,ext-doc.gif",
  ".vsd.vss.vsx.vst.vdx,ext-vsd.gif",
  ".odt,ext-odt.gif",
  ".xml,ext-xml.gif",
  ".zip.gz.arj.rar.tar,ext-zip.gif" );

  private static $caContextsInfo = array();

  /**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe ISMSLib.</p>
	 * @access public
	 * @return ISMSLib Inst�ncia �nica da classe ISMSLib
	 * @static
	 */
  public function __construct() {
    self::$coISMSConfig = new ISMSConfig();
  }

  /**
	 * Retorna a inst�ncia �nica da classe ISMSLib (singleton).
	 *
	 * <p>Retorna a inst�ncia �nica da classe ISMSLib. Se a inst�ncia
	 * ainda n�o existir, cria a �nica inst�ncia. Se a inst�ncia j� existir,
	 * retorna a �nica inst�ncia (singleton).</p>
	 * @access public
	 * @return ISMSLib Inst�ncia �nica da classe ISMSLib
	 * @static
	 */
  public static function getInstance() {
    if (self::$coISMSLib == NULL) {
      self::$coISMSLib = new ISMSLib();
    }
    else {}	// inst�ncia �nica j� existe, retorne-a

    return self::$coISMSLib;
  }

  /**
	 * Retorna a bulid atual do ISMS.
	 *
	 * <p>M�todo para retornar a build atual do ISMS.</p>
	 * @access public
	 * @return string Build atual do ISMS
	 */
  public function getISMSBuild() {
    $maBuild = explode(" ",$this->csBuild);
    return $maBuild[2];
  }

  /**
	 * Retorna a tooltip de cria��o e modifica��o dos elementos do sistema.
	 *
	 * <p>M�todo para retornar a tooltip de cria��o e modifica��o dos elementos do sistema.</p>
	 * @access public
	 * @return string retornar a tooltip de cria��o e modifica��o dos elementos do sistema.
	 */
  public function getToolTipCreateModify($psCreateName,$psCreateDate,$psModifyName,$psModifyDate){
    $psCreate = FWDLanguage::getPHPStringValue('mx_created_by','Criado por:');
    $psOf = FWDLanguage::getPHPStringValue('mx_on','em');
    $psModify = FWDLanguage::getPHPStringValue('mx_modified_by','Modificado por:');
    $msToolTipText = "$psCreate <b>$psCreateName</b> $psOf <i>".ISMSLib::getISMSDate($psCreateDate)."</i><hr> $psModify <b>$psModifyName</b> $psOf <i>".ISMSLib::getISMSDate($psModifyDate)."</i>";
    $moToolTip = new FWDToolTip();
    $moToolTip->setAttrShowDelay(1000);
    $moToolTip->setAttrWidth(450);
    $moToolTip->setAttrOpacity(90);
    $moToolTip->setAttrClass('ToolTipCreateModify');
    //$msToolTipText = str_replace('\\','\\\\',$msToolTipText);
    //$msToolTipText = str_replace('"','\"',$msToolTipText);
    $moStr = new FWDString();
    $moStr->setAttrValue($msToolTipText);
    $moToolTip->setObjFWDString($moStr);
    return $moToolTip;
  }

  /**
	 * Retorna a vers�o atual do ISMS.
	 *
	 * <p>M�todo para retornar a vers�o atual do ISMS.</p>
	 * @access public
	 * @return string Vers�o atual do ISMS
	 */
  public function getISMSVersion() {
    return $this->csVersion;
  }

  /**
	 * Retorna a configura��o referente ao Id passado por parametro.
	 *
	 * <p>M�todo para retornar a configura��o referente ao Id passado por parametro</p>
	 * @access public
	 * @return string valor da configura��o referente ao Id
   * @param integer $piConfigId Id da configura��o do isms
	 */
  public static function getConfigById($piConfigId){
    return self::$coISMSConfig->getConfig($piConfigId);
  }

 /**
   * Retorna a string que representa a moeda configurada no sistema.
   * <p>M�todo para retornar a string que representa a moeda configurada no sistema.</p>
   * @access public
   * @return string valor da configura��o de moeda
   */
  public static function getCurrencyString(){
  	$currId = self::$coISMSConfig->getConfig(GENERAL_CURRENCY_IDENTIFIER);
  	$value = constant($currId);

  	if(!$value) $value = '$';

  	return $value;
  }


  /**
  * Seta uma determinada configura��o.
  *
  * <p>M�todo para setar uma determinada configura��o.</p>
  * @access public
  * @param integer $piConfigId Id da configura��o
  * @param string $psValue Valor da configura��o
  */
  public static function setConfigById($piConfigId,$psValue){
    self::$coISMSConfig->setConfig($piConfigId,$psValue);
  }

  /**
	 * Retorna o id da sess�o atual.
	 *
	 * <p>M�todo para retornar o id da sess�o atual.</p>
	 * @access public
	 * @return string Id da sess�o atual
	 */
  public static function getCurrentSessionId() {
    return self::$csCurrentSessionId;
  }

  /**
	 * Retorna a hora local corrigindo erro de GMT.
	 *
	 * <p>O PHP est� considerando o timezone do sistema operacional para calcular a hora
	 * Esta fun��o retorna a hora local corrigindo este erro.</p>
	 * @access public
	 * @return timestamp
	 */
  public static function ISMSTime() {
    return time();
  }

  /**
	 * Inicializa o sistema.
	 *
	 * <p>M�todo para inicializar o sistema.</p>
	 * @access public
	 */
  public function ISMS_INIT() {

    // este teste deve ser realizado antes de qualquer outro para evitar recurs�o
    $maTest = array( 'access_denied.php',
    'license_expired.php',
    'license_limit_reached.php',
    'damaged_system.php',
    'invalid_activation_code.php',
    'session_expired.php',
    'license_corrupted.php',
    );
    $msFileName = substr($_SERVER['PHP_SELF'],strrpos($_SERVER['PHP_SELF'], "/")+1);
    if (in_array($msFileName,$maTest)) {
      return;
    }

    include('contexts_info.php');
    foreach($laContextsInfo as $maContextInfo){
      define($maContextInfo['constant'],$maContextInfo['id']);
      self::$caContextsInfo[$maContextInfo['id']] = $maContextInfo;
    }

    $moWebLib = FWDWebLib::getInstance();
    self::$csCurrentSessionId = 'isms_session';
    $moWebLib->getSession(new ISMSSession(self::$csCurrentSessionId));

    FWDWebLib::getInstance()->setMetaKeywords('Realiso Corp.');

    FWDWebLib::getInstance()->avoidGoogle(true);

    $moLicense = new ISMSLicense();
    if ($moLicense->isISMS()) {
      $moWebLib->setWindowTitle("REAL ISMS - INFORMATION SECURITY MANAGEMENT SYSTEM");
      $moWebLib->setExtraCSS('isms_layout.css');
      $moWebLib->setFavIcon("gfx/realisms_favicon.ico");
    }
    else if ($moLicense->isEMS()) {
      $moWebLib->setWindowTitle("REAL EMS - ENVIRONMENTAL MANAGEMENT SYSTEM");
      $moWebLib->setExtraCSS('ems_layout.css');
      $moWebLib->setFavIcon("gfx/realems_favicon.ico");
    }
    else if ($moLicense->isOHS()) {
      $moWebLib->setWindowTitle("REAL OHS - OCCUPATIONAL HEALTH AND SAFETY MANAGEMENT SYSTEM");
      $moWebLib->setExtraCSS('ohs_layout.css');
      $moWebLib->setFavIcon("gfx/realohs_favicon.ico");
    }
    else if ($moLicense->isSOX() && $moLicense->isMinima()){
    	$moWebLib->setWindowTitle("MINIMARISK - RISK MANAGEMENT SYSTEM");
    	$moWebLib->setExtraCSS('minima_sox_layout.css');
    	$moWebLib->setFavIcon('gfx/minimasox_favicon.ico');
    }
    else if ($moLicense->isSOX()) {
      $moWebLib->setWindowTitle("REAL SOX - SARBANES OXLEY MANAGEMENT SYSTEM");
      $moWebLib->setExtraCSS('sox_layout.css');
      $moWebLib->setFavIcon("gfx/realsox_favicon.ico");
    }
    else if ($moLicense->isPCI()) {
      $moWebLib->setWindowTitle("REAL PCI - PAYMENT CARD INDUSTRY MANAGEMENT SYSTEM");
      $moWebLib->setExtraCSS('pci_layout.css');
      $moWebLib->setFavIcon("gfx/realpci_favicon.ico");
    }
	else if ($moLicense->isTHREE()) {
      $moWebLib->setWindowTitle("REAL 3380 - GERENCIAMENTO DE RISCO OPERACIONAL");
      $moWebLib->setExtraCSS('3380_layout.css');
      $moWebLib->setFavIcon("gfx/real3380_favicon.ico");
    }
    else {
      trigger_error('Invalid license type!', E_USER_ERROR);
    }

    $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());

    $miUserId = $moSession->getUserId();
    $moUser = new ISMSUser();
    $mbUserIsActive = false;
    if($moUser->fetchById($miUserId)){
      if($moUser->getContextState()!=CONTEXT_STATE_DELETED){
        $mbUserIsActive = true;
      }
      if(!$moSession->checkSession()){
      	$mbUserIsActive = false;
      }

      //Verifica sessao atual, nao permitindo que o usuario esteja logado em duas instancias diferentes.
      $msUserSessionId = $moUser->getFieldValue('user_session_id');
      if (substr($msUserSessionId,5) != session_id()) {
        $mbUserIsActive = false;
      }
    }


    if($mbUserIsActive){
      /*
                * Esse trecho pega a l�ngua do usu�rio e carrega as respectivas tradu��es.
                * O 'IF' � necess�rio pois na tela de login ainda n�o existe identificador
                * de usu�rio.
                */
	if (isset($_COOKIE['ISMSLANGUAGE'])) {

		$miCookieLanguage = $_COOKIE['ISMSLANGUAGE'];
		//$miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId(true);
  		//$moUser = new ISMSUser();


   		//$moUser->fetchById($miUserId);
   		if($moUser->getFieldByAlias('user_language')->getValue() != $miCookieLanguage) {

			//FWDLanguage::selectLanguage($miCookieLanguage);
			$moUser->setFieldValue('user_language',$miCookieLanguage);
			$moUser->update($miUserId);

   		}
	}

      if($moUser->getFieldValue('user_language')){
        FWDLanguage::selectLanguage($moUser->getFieldValue('user_language'));
        /*
                    * M�todo utilizado para setar o nome da linguagem. Como era feito antes (no init.php)
                    * n�o funcionava pq quando definia o nome da linguagem os arquivos de tradu��o n�o tinham sido
                    * carregados de acordo com a escolha do usu�rio.
                    */
        if ($moLicense->isEMS()) {
          FWDLanguage::setLanguageName(LANGUAGE_PORTUGUESE_EMS,FWDLanguage::getPHPStringValue('mx_portuguese', "Portugu�s"));
          //FWDLanguage::setLanguageName(LANGUAGE_ENGLISH_EMS,FWDLanguage::getPHPStringValue('mx_english', "Ingl�s"));
        }
        else if ($moLicense->isOHS()) {
          FWDLanguage::setLanguageName(LANGUAGE_PORTUGUESE_OHS,FWDLanguage::getPHPStringValue('mx_portuguese', "Portugu�s"));
          //FWDLanguage::setLanguageName(LANGUAGE_ENGLISH_OHS,FWDLanguage::getPHPStringValue('mx_english', "Ingl�s"));
        }
        else if ($moLicense->isSOX()) {
          FWDLanguage::setLanguageName(LANGUAGE_ENGLISH_SOX,FWDLanguage::getPHPStringValue('mx_english', "English"));
          FWDLanguage::setLanguageName(LANGUAGE_GERMAN_SOX,FWDLanguage::getPHPStringValue('mx_german', "Deutsch"));
          FWDLanguage::setLanguageName(LANGUAGE_FRENCH_SOX,FWDLanguage::getPHPStringValue('mx_french', "Fran�ais"));
        }
        else if ($moLicense->isPCI()) {
          FWDLanguage::setLanguageName(LANGUAGE_ENGLISH_PCI,FWDLanguage::getPHPStringValue('mx_english', "English"));
        }
		else if ($moLicense->isTHREE()) {
          FWDLanguage::setLanguageName(LANGUAGE_PORTUGUESE_THREE,FWDLanguage::getPHPStringValue('mx_portuguese', "Portugu�s"));
          //FWDLanguage::setLanguageName(LANGUAGE_GERMAN_SOX,FWDLanguage::getPHPStringValue('mx_german', "Deutsch"));
          //FWDLanguage::setLanguageName(LANGUAGE_FRENCH_SOX,FWDLanguage::getPHPStringValue('mx_french', "Fran�ais"));
        }
        else {
          FWDLanguage::setLanguageName(LANGUAGE_PORTUGUESE_ISMS,FWDLanguage::getPHPStringValue('mx_portuguese', "Portugu�s"));
          FWDLanguage::setLanguageName(LANGUAGE_ENGLISH_ISMS,FWDLanguage::getPHPStringValue('mx_english', "English"));
          FWDLanguage::setLanguageName(LANGUAGE_SPANISH_ISMS,FWDLanguage::getPHPStringValue('mx_spanish', "Espa�ol (Beta!)"));
          //FWDLanguage::setLanguageName(LANGUAGE_SPANISH_ISMS,FWDLanguage::getPHPStringValue('mx_spanish', "Espanhol"));
          //FWDLanguage::setLanguageName(LANGUAGE_CHINESE,FWDLanguage::getPHPStringValue('mx_chinese', "Chin�s (Beta)"));
        }
      }
    }

    FWDWebLib::getInstance()->addObject('isms_grid_user_preference',new ISMSGridUserPreferences());

    $maDontTest = array('default.php','login.php','logout.php','email_sender.php','ISMSActivation.php','popup_user_change_password.php','popup_password_request.php','cron_check_schedules.php', 
      'custom_gfx.php', 'survey_isms_pt.php', 'survey_isms_en.php', 'survey_isms_es.php', 'autologin.php', 'autologinteste.php', 'cron_sync_login.php','asaas_block_popup.php','asaas_block_popup_language.php', 
      'asaas_block.php', 'paypal_cancel.php', 'paypal_success.php');
    $msFileName = substr($_SERVER['PHP_SELF'],strrpos($_SERVER['PHP_SELF'], "/")+1);

    // SE a sess�o for v�lida, passa e reseta o timeout, sen�o, popup, desloga.
    if(!$mbUserIsActive && !in_array($msFileName,$maDontTest)){
      $moSession->destroySession();
      ISMSLib::openSessionExpired();
      die(1);
    } else {
    	$moSession->resetTimeout();
    }

    /*
    * POG para detectar final de um relat�rio
    */
    $msId = sprintf("#%x",crc32( "report_has_finished" . $_SERVER['SCRIPT_FILENAME']));
    if($moSession->attributeExists($msId)){
      $miCont = $moSession->{'getAttr'.$msId}();
      $miCont--;
      $moSession->{'setAttr'.$msId}($miCont);
      $moSession->commit();
      FWDStartEvent::getInstance()->addBeforeAjax(new ISMSReportHideLoadingEvent("hide_loading_report_isms"),HIGH);
    }
    $msTimezone = self::$coISMSConfig->getConfig(TIMEZONE);
    if ($msTimezone < 0){
    	$msTimezone = $msTimezone - 2*$msTimezone;
    	$msTimezone = "Etc/GMT+" . $msTimezone;
    } elseif ($msTimezone > 0){
    	$msTimezone = $msTimezone - 2*$msTimezone;
    	$msTimezone = "Etc/GMT" . $msTimezone;
    }
    else {
    	$msTimezone = "Etc/GMT+0";
    }
    date_default_timezone_set($msTimezone);
  }

  public function getToolTipCreateModifyById($piContextId){
    $moHandle = new QueryContextCreateModify(FWDWebLib::getConnection());
    $moHandle->setContextId($piContextId);
    $moHandle->makeQuery();

    if($moHandle->executeQuery()){
      $moDataset =  $moHandle->getDataset();
      if($moDataset->fetch()){
        $msCreateName = $moDataset->getFieldByAlias('user_create_name')->getValue();
        $msCreateDate = $moDataset->getFieldByAlias('date_create')->getValue();
        $msModifyName = $moDataset->getFieldByAlias('user_edit_name')->getValue();
        $msModifyDate = $moDataset->getFieldByAlias('date_edit')->getValue();
        return $this->getToolTipCreateModify($msCreateName,$msCreateDate,$msModifyName,$msModifyDate);
      }
    }
  }

  /**
  * Retorna a periodicidade baseado no per�odo e valor passados.
  *
  * <p>Retorna a periodicidade baseado no per�odo e valor passados.</p>
  * @access public
  * @param integer $piValue Valor
  * @param integer $piPeriod Per�odo
  * @return string Periodicidade
  */
  public static function getPeriodicity($piValue, $piPeriod) {
    $msPeriodicity = $piValue . ' ';
    switch ($piPeriod) {
      case SCHEDULE_BYDAY:
        if ($piValue > 1) $msPeriodicity .= FWDLanguage::getPHPStringValue('mx_days', " dias");
        else $msPeriodicity .= FWDLanguage::getPHPStringValue('mx_day', " dia");
        break;

      case SCHEDULE_BYWEEK:
        if ($piValue > 1) $msPeriodicity .= FWDLanguage::getPHPStringValue('mx_weeks', " semanas");
        else $msPeriodicity .= FWDLanguage::getPHPStringValue('mx_week', " semana");
        break;

      case SCHEDULE_BYMONTH:
        if ($piValue > 1) $msPeriodicity .= FWDLanguage::getPHPStringValue('mx_months', " meses");
        else $msPeriodicity .= FWDLanguage::getPHPStringValue('mx_month', " m�s");
        break;
    }
    return $msPeriodicity;
  }

  /**
  * Retorna o id e o nome de cada usu�rio especial.
  *
  * <p>Retorna o id e o nome de cada usu�rio especial.</p>
  * @access public
  * @param integer $piMode Quando passado, retorna apenas os usu�rios especiais daquele modo.
  * @return array Ex: array(USER_ASSET_CONTROLLER => 'Gestor de Ativo')
  */
  public static function getSpecialUsers($piMode=0) {
    $maRMMode = array(USER_CHAIRMAN => FWDLanguage::getPHPStringValue('mx_chairman', "Dire��o"),
    USER_ASSET_CONTROLLER => FWDLanguage::getPHPStringValue('mx_asset_manager', "Gestor de Ativo"),
    USER_CONTROL_CONTROLLER => FWDLanguage::getPHPStringValue('mx_control_manager', "Gestor de Controle"),
    USER_LIBRARIAN => FWDLanguage::getPHPStringValue('mx_library_manager', "Gestor de Biblioteca"));
    $maPMMode = array(
    USER_DOCUMENT_AUDITOR => FWDLanguage::getPHPStringValue('mx_document_auditor', "Auditor de Documentos"),
    );
    $maCIMode = array(USER_INCIDENT_MANAGER => FWDLanguage::getPHPStringValue('mx_incident_manager', "Gestor de Incidente"),
    USER_DISCIPLINARY_PROCESS_MANAGER => FWDLanguage::getPHPStringValue('mx_process_manager', "Gestor de Processo Disciplinar"),
    USER_NON_CONFORMITY_MANAGER => FWDLanguage::getPHPStringValue('mx_non_conformity_manager', "Gestor de N�o Conformidade"),
    USER_EVIDENCE_MANAGER => FWDLanguage::getPHPStringValue('mx_evidence_manager', "Gestor de Evid�ncia"));
    switch($piMode) {
      case RISK_MANAGEMENT_MODE:
        return $maRMMode;
      case POLICY_MODE:
        return $maPMMode;
      case INCIDENT_MODE:
        return $maCIMode;
      default:
        $maAllSpecialUsers = array();
        foreach($maRMMode as $miUserId=>$msUserName) {
          $maAllSpecialUsers[$miUserId]=$msUserName;
        }
        foreach($maPMMode as $miUserId=>$msUserName) {
          $maAllSpecialUsers[$miUserId]=$msUserName;
        }
        foreach($maCIMode as $miUserId=>$msUserName) {
          $maAllSpecialUsers[$miUserId]=$msUserName;
        }
        return $maAllSpecialUsers;
    }
  }

  /**
  * Abre uma PopUp de confirma��o.
  *
  * <p>Abre uma PopUp de confirma��o.</p>
  * @access public
  * @param string $psTitle T�tulo da PopUp
  * @param string $psMessage Mensagem a ser exibida
  * @param string $psConfirmEvent C�digo JavaScript do evento a ser executado quando o usu�rio confirma
  * @param integer $piMessageHeight Altura do static com a mensagem. � opcional e se omitido, o valor no XML � mantido.
  */
  public static function openConfirm($psTitle,$psMessage,$psConfirmEvent,$piMessageHeight=0,$psDeniedEvent = ''){
    $moWebLib = FWDWebLib::getInstance();
    $msPopUpId = 'popup_confirm';
    $msXmlFile = $moWebLib->getSysRef().'confirm.xml';
    $msIconSrc = $moWebLib->getSysRefBasedOnTabMain().'gfx/'.'icon-exclamation.gif';
    $moWebLib->xml_load($msXmlFile,false);

    $moPanel = $moWebLib->getObject('confirm_panel');
    $moTitle = $moWebLib->getObject('confirm_title');
    $moMessage = $moWebLib->getObject('confirm_message');
    $moButtons = $moWebLib->getObject('confirm_buttons');

    $moTitle->setValue($psTitle);
    $moMessage->setValue($psMessage);
    $moWebLib->getObject('confirm_icon')->setAttrSrc($msIconSrc);
    // Ajusta as alturas, caso o par�metro piMessageHeight tenha sido passado
    if($piMessageHeight>0){
      $moPanelBox = $moPanel->getObjFWDBox();
      $moMessageBox = $moMessage->getObjFWDBox();
      $moButtonsBox = $moButtons->getObjFWDBox();

      $miMessageTop = $moMessageBox->getAttrTop();
      $miMessageHeight = $piMessageHeight;
      $miButtonsTop = $miMessageTop + $miMessageHeight + 10;
      $miPanelHeight = $miButtonsTop + $moButtonsBox->getAttrHeight() + 10;
      $miPanelWidth = $moPanelBox->getAttrWidth();

      $moButtonsBox->setAttrTop($miButtonsTop);
      $moPanelBox->setAttrHeight($miPanelHeight);
      $moTitle->getObjFWDBox()->setAttrWidth($miPanelWidth);
    }else{
      $moPanelBox = $moPanel->getObjFWDBox();
      $miPanelHeight = $moPanelBox->getAttrHeight();
      $miPanelWidth = $moPanelBox->getAttrWidth();
    }

    // centraliza a viewgroup dos botoes 'sim' e 'n�o'
    $miConfirmPanelWidth = $moPanel->getObjFWDBox()->getAttrWidth();
    $miConfirmButtonsWidth = $moButtons->getObjFWDBox()->getAttrWidth();
    $miDelta = floor( ($miConfirmPanelWidth - $miConfirmButtonsWidth) / 2 );
    $moButtons->getObjFWDBox()->setAttrLeft($miDelta);

    // Evento do n�o
    $moEvent = new FWDClientEvent();
    $moEvent->setAttrEvent('onClick');
    $moEvent->setAttrValue($psDeniedEvent."soPopUpManager.closePopUp('$msPopUpId');");
    $moWebLib->getObject('confirm_viewbutton_no')->addObjFWDEvent($moEvent);
    // Evento do sim
    $moEvent = new FWDClientEvent();
    $moEvent->setAttrEvent('onClick');
    $moEvent->setAttrValue($psConfirmEvent."soPopUpManager.closePopUp('$msPopUpId');");
    $moWebLib->getObject('confirm_viewbutton_yes')->addObjFWDEvent($moEvent);

    $msXml = str_replace(array("\n",'"'),array(' ','\"'),$moPanel->draw());
    echo "isms_open_popup('$msPopUpId','','','true',$miPanelHeight,$miPanelWidth);"
    ."soPopUpManager.getPopUpById('$msPopUpId').setHtmlContent(\"$msXml\");";
  }

  /**
  * Abre uma PopUp de Ok.
  *
  * <p>Abre uma PopUp de Ok.</p>
  * @access public
  * @param string $psTitle T�tulo da PopUp
  * @param string $psMessage Mensagem a ser exibida
  * @param string $psConfirmEvent C�digo JavaScript do evento a ser executado quando o usu�rio pressiona o bot�o 'OK'
  * @param integer $piMessageHeight Altura do static com a mensagem. � opcional e se omitido, o valor no XML � mantido.
  */
  public static function openOk($psTitle,$psMessage,$psConfirmEvent="",$piMessageHeight=0){
    $moWebLib = FWDWebLib::getInstance();
    $msPopUpId = 'popup_ok';
    $msXmlFile = $moWebLib->getSysRef().'ok.xml';

    $msIconSrc = $moWebLib->getSysRefBasedOnTabMain().'gfx/'.'icon-exclamation.gif';

    $moWebLib->xml_load($msXmlFile,false);
    $moPanel = $moWebLib->getObject('ok_panel');
    $moMessage = $moWebLib->getObject('ok_message');
    $moButton = $moWebLib->getObject('viewbutton_ok');
    $moWebLib->getObject('ok_title')->setValue($psTitle);
    $moMessage->setValue($psMessage);
    $moWebLib->getObject('ok_icon')->setAttrSrc($msIconSrc);
    // Ajusta as alturas, caso o par�metro piMessageHeight tenha sido passado
    if($piMessageHeight>0){
      $moPanelBox = $moPanel->getObjFWDBox();
      $moMessageBox = $moMessage->getObjFWDBox();
      $moButtonBox = $moButton->getObjFWDBox();

      $miMessageTop = $moMessageBox->getAttrTop();
      $miMessageHeight = $piMessageHeight;
      $miButtonTop = $miMessageTop + $miMessageHeight + 10;
      $miPanelHeight = $miButtonTop + $moButtonBox->getAttrHeight() + 10;
      $miPanelWidth = $moPanelBox->getAttrWidth();

      $moButtonBox->setAttrTop($miButtonTop);
      $moPanelBox->setAttrHeight($miPanelHeight);
    }else{
      $moPanelBox = $moPanel->getObjFWDBox();
      $miPanelHeight = $moPanelBox->getAttrHeight();
      $miPanelWidth = $moPanelBox->getAttrWidth();
    }

    // centraliza o bot�o 'ok'
    $miOkPanelWidth = $moPanel->getObjFWDBox()->getAttrWidth();
    $miOkButtonWidth = $moButton->getObjFWDBox()->getAttrWidth();
    $miDelta = floor( ($miOkPanelWidth - $miOkButtonWidth) / 2 );
    $moButton->getObjFWDBox()->setAttrLeft($miDelta);

    // Evento do ok
    $moEvent = new FWDClientEvent();
    $moEvent->setAttrEvent('onClick');
    $moEvent->setAttrValue($psConfirmEvent."soPopUpManager.closePopUp('$msPopUpId');");
    $moButton->addObjFWDEvent($moEvent);

    $msXml = str_replace(array("\n",'"'),array(' ','\"'),$moPanel->draw());
    echo "isms_open_popup('$msPopUpId','','','true',$miPanelHeight,$miPanelWidth);"
    ."soPopUpManager.getPopUpById('$msPopUpId').setHtmlContent(\"$msXml\");";
  }

  /**
  * Exibe uma popup avisando que a sess�o atual expirou e redireciona para a tela de login.
  *
  * <p>Exibe uma popup avisando que a sess�o atual expirou e redireciona para a tela de login.</p>
  * @access public
  */
  public static function openSessionExpired(){
    $moWebLib = FWDWebLib::getInstance();
    $mbIsAjax = (FWDStartEvent::getInstance()->getAjaxEventName())?1:0;

    if(!$mbIsAjax) {
      $moPogDialog = new FWDDialog();
      $moPogDialog->setAttrName("dialog_sessionexpired");
      FWDWebLib::addObject("dialog_sessionexpired",$moPogDialog);
      FWDWebLib::getInstance()->dump_html($moPogDialog);
      echo "<script>";
      echo "isms_open_popup('popup_session_expired','".$moWebLib->getSysRefBasedOnTabMain()."session_expired.php','','true',122,350);";
      echo "</script>";
    }
    else {
      echo "isms_open_popup('popup_session_expired','".$moWebLib->getSysRefBasedOnTabMain()."session_expired.php','','true',122,350);";
    }
  }

  /**
  * Exibe uma popup avisando que a licen�a expirou e redireciona para a tela de login.
  *
  * <p>Exibe uma popup avisando que a licen�a expirou e redireciona para a tela de login.</p>
  * @access public
  * @param integer $piExpiracy Data de expira��o da licen�a (timestamp)
  */
  public static function openLicenseExpired($piExpiracy){
    $moWebLib = FWDWebLib::getInstance();
    $mbIsAjax = (FWDStartEvent::getInstance()->getAjaxEventName())?1:0;
    if(!$mbIsAjax) {
      if(!FWDWebLib::getObject('dialog')){
        $moPogDialog = new FWDDialog();
        $moPogDialog->setAttrName("dialog");
        FWDWebLib::addObject("dialog",$moPogDialog);
      }
      FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
      echo "<script>";
      echo "isms_open_popup('popup_license_expired','".$moWebLib->getSysRefBasedOnTabMain()."license_expired.php?expiracy=$piExpiracy','','true',122,350);";
      echo "</script>";
    }
    else {
      echo "isms_open_popup('popup_license_expired','".$moWebLib->getSysRefBasedOnTabMain()."license_expired.php?expiracy=$piExpiracy','','true',122,350);";
    }

    /* Envia email para Axur, informando expira��o da licen�a */
    $moConfig = new ISMSConfig();
    $msMailDefaultSender = $moConfig->getConfig(GENERAL_SMTP_DEFAULT_MAIL_SENDER);
    $msReportEmail = ISMSSaaS::getConfig(ISMSSaaS::SAAS_REPORT_EMAIL);
    $msMessage = FWDLanguage::getPHPStringValue('em_license_expired','A Licen�a do Cliente <b>%client_name%</b> expirou em <b>%expiracy%</b>');
    $msMessage = str_replace('%client_name%',ISMSSaaS::getConfig(ISMSSaaS::LICENSE_CLIENT_NAME),$msMessage);
    $msMessage = str_replace('%expiracy%',ISMSLib::getISMSShortDate($piExpiracy,true),$msMessage);

    $msHost = ISMSLib::getConfigById(GENERAL_SMTP_SERVER);
    if ($msHost != 'localhost' && $msHost != '127.0.0.1'){
    	$msUser = ISMSLib::getConfigById(EXTERNAL_MAIL_USER);
    	$msPassword = ISMSLib::getConfigById(EXTERNAL_MAIL_PASSWORD);
    	$miPort = ISMSLib::getConfigById(EXTERNAL_MAIL_PORT);
    	$msEnc = ISMSLib::getConfigById(EXTERNAL_MAIL_ENCRYPTION);
    	$moMail = new FWDEmail(true, $msHost, $miPort, $msUser, $msPassword, $msEnc);
    } else {
    	$moMail = new FWDEmail();
    }
    $moMail->setSenderName($moConfig->getConfig(GENERAL_SMTP_DEFAULT_MAIL_SENDER_NAME));
    $moMail->setFrom($msMailDefaultSender);
    $moMail->setTo($msReportEmail);
    $moMail->setSubject(FWDLanguage::getPHPStringValue('em_subject_license_expired','Licen�a Expirada'));
    $moMail->setHtml(true);
    $moMail->setMessage($msMessage);
    ini_set('SMTP', $moConfig->getConfig(GENERAL_SMTP_SERVER));
    $moMail->send();

  }

  /**
  * Exibe uma popup avisando que n�o foi poss�vel ativar o sistema.
  *
  * <p>Exibe uma popup avisando que n�o foi poss�vel ativar o sistema.</p>
  * @access public
  */
  public static function openDamagedSystem(){
    $moWebLib = FWDWebLib::getInstance();
    $mbIsAjax = (FWDStartEvent::getInstance()->getAjaxEventName())?1:0;

    if(!$mbIsAjax) {
      $moPogDialog = new FWDDialog();
      $moPogDialog->setAttrName("dialog_popup");
      FWDWebLib::addObject("dialog_popup",$moPogDialog);
      FWDWebLib::getInstance()->dump_html($moPogDialog);
      echo "<script>";
      echo "isms_open_popup('popup_damaged_system','".$moWebLib->getSysRef()."damaged_system.php','','true',122,350);";
      echo "</script>";
    }
    else {
      echo "isms_open_popup('popup_damaged_system','".$moWebLib->getSysRef()."damaged_system.php','','true',122,350);";
    }
  }

  /**
  * Exibe uma popup avisando que o c�digo de ativa��o est� incorreto.
  *
  * <p>Exibe uma popup avisando que o c�digo de ativa��o est� incorreto.</p>
  * @access public
  */
  public static function openInvalidActivationCode(){
    $moWebLib = FWDWebLib::getInstance();
    $mbIsAjax = (FWDStartEvent::getInstance()->getAjaxEventName())?1:0;

    if(!$mbIsAjax) {
      $moPogDialog = new FWDDialog();
      $moPogDialog->setAttrName("dialog_invalidactivation");
      FWDWebLib::addObject("dialog_invalidactivation",$moPogDialog);
      FWDWebLib::getInstance()->dump_html($moPogDialog);
      echo "<script>";
      echo "isms_open_popup('popup_invalid_activation_code','".$moWebLib->getSysRef()."invalid_activation_code.php','','true',122,350);";
      echo "</script>";
    }
    else {
      echo "isms_open_popup('popup_invalid_activation_code','".$moWebLib->getSysRef()."invalid_activation_code.php','','true',122,350);";
    }
  }

  /**
  * Exibe uma popup avisando que o acesso ao sistema foi negado e redireciona para a tela de login.
  *
  * <p>Exibe uma popup avisando que o acesso ao sistema foi negado e redireciona para a tela de login.</p>
  * @access public
  * @param string $psLicenseName Nome do arquivo de licen�a
  */
  public static function openAccessDenied($psLicenseName){
    $moWebLib = FWDWebLib::getInstance();
    $mbIsAjax = (FWDStartEvent::getInstance()->getAjaxEventName())?1:0;

    if(!$mbIsAjax) {
      $moPogDialog = new FWDDialog();
      $moPogDialog->setAttrName("dialog_acessdenied");
      FWDWebLib::addObject("dialog_acessdenied",$moPogDialog);
      FWDWebLib::getInstance()->dump_html($moPogDialog);
      echo "<script>";
      echo "isms_open_popup('popup_access_denied','".$moWebLib->getSysRefBasedOnTabMain()."access_denied.php?license_name=$psLicenseName','','true',122,350);";
      echo "</script>";
    }
    else {
      echo "isms_open_popup('popup_access_denied','".$moWebLib->getSysRefBasedOnTabMain()."access_denied.php?license_name=$psLicenseName','','true',122,350);";
    }
  }

  /**
  * Exibe uma popup avisando que o n�mero m�ximo de elementos de um atributo da licen�a foi atingido.
  *
  * <p>Exibe uma popup avisando que o n�mero m�ximo de elementos de um atributo da licen�a foi atingido.</p>
  * @access public
  * @param string $psAttributeAlias Alias do atributo
  */
  public static function openLicenseLimitReached($psAttributeAlias){
    $moWebLib = FWDWebLib::getInstance();
    $mbIsAjax = (FWDStartEvent::getInstance()->getAjaxEventName())?1:0;

    if(!$mbIsAjax) {
      $moPogDialog = new FWDDialog();
      $moPogDialog->setAttrName("dialog_licenselimit");
      FWDWebLib::addObject("dialog_licenselimit",$moPogDialog);
      FWDWebLib::getInstance()->dump_html($moPogDialog);
      echo "<script>";
      echo "isms_open_popup('popup_license_limit_reached','".$moWebLib->getSysRefBasedOnTabMain()."license_limit_reached.php?attribute=$psAttributeAlias','','true',132,350);";
      echo "</script>";
    }
    else {
      echo "isms_open_popup('popup_license_limit_reached','".$moWebLib->getSysRefBasedOnTabMain()."license_limit_reached.php?attribute=$psAttributeAlias','','true',132,350);";
    }
  }

  /**
	 * Retorna uma data no formato do ISMS.
	 *
	 * <p>M�todo para retornar uma data no formato do ISMS.</p>
	 * @access public
	 * @param string $psDate Data
	 * @param boolean $pbIsTimestamp � um timestamp ou uma data em string
	 * @return string Data
	 */
  public static function getISMSDate($psDate = "", $pbIsTimestamp = false) {
  	$moConfig = new ISMSConfig();
    if (!$pbIsTimestamp) {
      if ($psDate) $piTimestamp = self::getTimestamp($psDate);
      else $piTimestamp = ISMSLib::ISMSTime();
    }
    else $piTimestamp = $psDate;

    $msDateFormat = FWDLanguage::getPHPStringValue('mx_shortdate_format', "");
    if($moConfig->getConfig(GENERAL_INTERNATIONAL_TIME) == INTERNATIONAL_TIME_SYS_DEFAULT) {
    	if ($msDateFormat == "%m/%d/%Y")
    		return date("m/d/Y h:i a", $piTimestamp);
    	elseif ($msDateFormat == "%d/%m/%Y")
    		return date("d/m/Y H:i", $piTimestamp);
    	else
    	trigger_error("Invalid date format!", E_USER_WARNING);
    } else if ($moConfig->getConfig(GENERAL_INTERNATIONAL_TIME) == INTERNATIONAL_TIME_12_HOUR_FORMAT ) {
    	return date("m/d/Y h:i a", $piTimestamp);
    } else if( $moConfig->getConfig(GENERAL_INTERNATIONAL_TIME) == INTERNATIONAL_TIME_24_HOUR_FORMAT) {
    	return date("d/m/Y H:i", $piTimestamp);
    }
  }

  public static function getHour() {
  	$piTimestamp = ISMSLib::ISMSTime();
  	$moConfig = new ISMSConfig();
  	$msDateFormat = FWDLanguage::getPHPStringValue('mx_shortdate_format', "");
    if($moConfig->getConfig(GENERAL_INTERNATIONAL_TIME) == INTERNATIONAL_TIME_SYS_DEFAULT) {
    	if ($msDateFormat == "%m/%d/%Y")
    		return date("h", $piTimestamp);
    	elseif ($msDateFormat == "%d/%m/%Y")
    		return date("H", $piTimestamp);
    	else
    	trigger_error("Invalid date format!", E_USER_WARNING);
    } else if ($moConfig->getConfig(GENERAL_INTERNATIONAL_TIME) == INTERNATIONAL_TIME_12_HOUR_FORMAT ) {
    	return date("h", $piTimestamp);
    } else if( $moConfig->getConfig(GENERAL_INTERNATIONAL_TIME) == INTERNATIONAL_TIME_24_HOUR_FORMAT) {
    	return date("H", $piTimestamp);
    }

  }

  /**
	 * Retorna uma data "curta" (XX/XX/AAAA) no formato do ISMS.
	 *
	 * <p>M�todo para retornar uma data "curta" (XX/XX/AAAA) no formato do ISMS.</p>
	 * @access public
	 * @param string $psDate Data
	 * @param boolean $pbIsTimestamp � um timestamp ou uma data em string
	 * @return string Data
	 */
  public static function getISMSShortDate($psDate = "", $pbIsTimestamp = false) {
    if (!$pbIsTimestamp) {
      if ($psDate) $piTimestamp = self::getTimestamp($psDate);
      else $piTimestamp = ISMSLib::ISMSTime();
    }
    else $piTimestamp = $psDate;

    $msDateFormat = FWDLanguage::getPHPStringValue('mx_shortdate_format', "");
    if ($msDateFormat == "%m/%d/%Y")
    return date("m/d/Y", $piTimestamp);
    elseif ($msDateFormat == "%d/%m/%Y")
    return date("d/m/Y", $piTimestamp);
    else
    trigger_error("Invalid date format!", E_USER_WARNING);
  }

  /**
	 * Retorna um timestamp baseado em uma data no formato YYYY-MM-DD HH-NN-SS.
	 *
	 * <p>M�todo para retornar um timestamp baseado em uma data no formato YYYY-MM-DD HH-NN-SS.</p>
	 * @access public
	 * @param string $psDate Data
	 * @return integer Timestamp
	 */
  public static function getTimestamp($psDate) {
    return FWDWebLib::getInstance()->getConnection()->getTimestamp($psDate);
  }

  /**
	 * Retorna uma data no formato do banco.
	 *
	 * <p>M�todo para retornar uma data no formato do banco.</p>
	 * @access public
	 * @param string $piTimestamp Timestamp
	 * @return string Data
	 */
  public static function getTimestampFormat($piTimestamp) {
    return FWDWebLib::getInstance()->getConnection()->timestampFormat($piTimestamp);
  }

  /**
	 * Retorna se o usu�rio tem permiss�o para o modo Gestor.
	 *
	 * <p>Retorna se o usu�rio tem permiss�o para o modo Gestor do sistema</p>
	 * @access public
	 * @return boolean Indica se o usu�rio tem permiss�o para o modo Gestor.
	 */
  public static function getGestorPermission(){
    $mbReturn = true;
    $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
    if((in_array('M.D' , $maACLs))  &&
    (in_array('M.RM' , $maACLs)) &&
    (in_array('M.PM' , $maACLs)) &&
    (in_array('M.CI', $maACLs)) &&
    (in_array('M.L' , $maACLs)) &&
    (in_array('M.S' , $maACLs))
    )
    $mbReturn = false;
    return $mbReturn;
  }

  /**
	 * Retorna se o usu�rio tem permiss�o para o modo Admin.
	 *
	 * <p>Retorna se o usu�rio tem permiss�o para o modo Administrativo do sistema</p>
	 * @access public
	 * @return boolean Indica se o usu�rio tem permiss�o para o modo Admin.
	 */
  public static function getAdminPermission(){
    $mbReturn = true;
    $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
    if( (in_array('A.U' , $maACLs)) &&
    (in_array('A.P' , $maACLs)) &&
    (in_array('A.C' , $maACLs)) &&
    (in_array('A.S' , $maACLs)) &&
    (in_array('A.A' , $maACLs)) &&
    (in_array('A.L' , $maACLs)) &&
    (in_array('A.MA', $maACLs))
    )
    $mbReturn = false;
    return $mbReturn;
  }

  /**
  * Retorna o codigo do icone do contexto.
  *
  * <p>M�todo para retornar o icone do contexto.</p>
  * @access public
  * @return string codigo html do icone do contexto
  */
  public static function getIconCode($psIconName , $piValue = 0,$piTop = 0){
    $moIcon = new FWDIcon();
    switch($piValue){
      case -1:
        $moIcon->setAttrSrc(FWDWebLib::getInstance()->getGfxRef()."icon-home.gif");
        $moIcon->getObjFWDBox()->setAttrTop(2);
        return $moIcon->draw(). "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
        break;
      case -2:
        $moIcon->setAttrSrc(FWDWebLib::getInstance()->getGfxRef().$psIconName);
        $moIcon->getObjFWDBox()->setAttrTop($piTop);
        return $moIcon->draw(). "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
        break;
      default:
        $moIcon->setAttrSrc(FWDWebLib::getInstance()->getGfxRef().$psIconName.RMRiskConfig::getRiskColor($piValue).".gif");
        $moIcon->getObjFWDBox()->setAttrTop($piTop);
        return $moIcon->draw(). '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
        break;
    }
  }


  /**
  * Gera o pathScroling para o ISMS.
  *
  * <p>M�todo para gerar o pathScrolling e inserir na p�gina html os links do pathscroling.</p>
  * @access public
  * @param integer $piTab id da tab
  * @param integer $piContextType id do contexto
  * @param string $psContextId indices do filtro de ids dos links do pathscroling
  * @param string $pbIsAjax se a fun��o est� sendo executada em um evento ajax
  * @param string $psFuncCode fun��o que deve ser executada no link do pathscroling
  * @param string $psAuxContextId indices auxiliares (para a recurs�o (Ativo->Controle)) do filtro de ids dos links do pathscroling
  * @return array Array contendo o Caminho do contexto
  */
  public static function getPathScrollCode($piTab,$piContextType,$psContextId,$pbIsAjax,$psFuncCode,$psAuxContextIds = ''){
    $msHome = FWDLanguage::getPHPStringValue('mx_root',"Raiz");
    $moContextObj = new ISMSContextObject();
    $paContextId = explode(':',$psContextId);
    $maPath = array();
    if($psContextId){
      $miCont = 0;
      $moContext = $moContextObj->getContextObject($piContextType);
      $moContext->fetchById($paContextId[0]);

      switch($piContextType){
        case CONTEXT_AREA:
        case CONTEXT_CATEGORY:
        case CONTEXT_SECTION_BEST_PRACTICE:
        case CONTEXT_CI_CATEGORY:
          $maPath = $moContext->getSystemPathScroll($piTab,$piContextType,$psFuncCode);
          break;
        case CONTEXT_CONTROL:
        case CONTEXT_RISK:
          $maPath = $moContext->getSystemPathScroll($piTab,$piContextType,$psContextId,$psAuxContextIds);
          break;
        default:
          $maPath = $moContext->getSystemPathScroll($piTab,$piContextType,$psContextId);
          break;
      }
    }else{
      $miCont = 0;
      if($psFuncCode)
      $maPath[] = "<a href='javascript:{$psFuncCode}(0);'>";
      else
      $maPath[] = "<a href='javascript:isms_redirect_to_mode_scroll(".$piTab.", ".$piContextType.",0);'>";
      $maPath[]=$msHome;
      $maPath[]=ISMSLib::getIconCode('',-1);
      $maPath[]="</a>";
    }
    $psAuxPar='';

    if($psAuxContextIds){
      $maIdsCont = explode(':',$psAuxContextIds);
      foreach($maIdsCont as $msId){
        $maIds = explode(';',$msId);
        $moContextAux = $moContextObj->getContextObject($maIds[0]);
        $moContextAux->fetchById($maIds[1]);
        $maPath = array_merge($maPath,$moContextAux->getSystemPathScrollAux($piTab,$piContextType,$psContextId,$psAuxPar));
        if($psAuxPar=='')
        $psAuxPar .= isset($maIdsCont[$miCont])?($maIdsCont[$miCont]):'';
        else
        $psAuxPar .= isset($maIdsCont[$miCont])?':'.($maIdsCont[$miCont]):'';
        $miCont = $miCont+1;
      }
    }
    $msIconArrowCode = "&nbsp;&nbsp;".ISMSLib::getIconCode('icon-arrow-right.gif',-2,3)."&nbsp;&nbsp;";
    $msPath ='';
    $miContPath = count($maPath);
    for($miI=0;$miI<$miContPath;$miI++){
      if((isset($maPath[$miI+4]))&&($maPath[$miI+4]!=''))
      $maPath[$miI] = $maPath[$miI+4];
      else{
        $maPath[$miI]='';
        $maPath[$miI+1] = "<b>".$maPath[$miI+1]."</b>";
        $maPath[$miI+3]='';
      }
      if($msPath!=''){
        $msPath .= $msIconArrowCode . $maPath[$miI+2] . $maPath[$miI] . $maPath[$miI+1] . $maPath[$miI+3];
      }else
      $msPath .= $maPath[$miI+2] . $maPath[$miI] . $maPath[$miI+1] . $maPath[$miI+3];
      $miI=$miI+3;
    }
    $msPath = str_replace(array("\n","\r"),array(' ',''),$msPath);
    if($pbIsAjax){
      echo " gobi('scroll_path').setValue(\"$msPath\");";
    }else{
      echo "<script language=\"javascript\"> gobi('scroll_path').setValue(\"$msPath\");</script>";
    }

  }

  /**
   * Testa se o usu�rio logado tem permiss�o para acessar o m�dulo de
   * documenta��o.
   *
   * <p>Testa se o usu�rio logado tem permiss�o para acessar o m�dulo de
   * documenta��o, caso n�o tenha, exibe uma mensagem na tela e finaliza a
   * execu��o!</p>
   * @access public
   * @param string $psFileName Nome do arquivo
   * @return string Nome do �cone
   */
  public static function testUserPermissionToPoliceMode(){
    if(!FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(POLICY_MODE)){
      $msInfo = FWDLanguage::getPHPStringValue('st_denied_permission_to_access_policy_module','Voc� n�o tem permiss�o para acessar o m�dulo de Gest�o de Pol�ticas. <br><br>voc� est� utilizando uma licen�a que n�o permite acessar este m�dulo!');
      echo $msInfo;
      exit();
    }else{
      return true;
    }
  }

  /**
   * Testa se o usu�rio logado tem permiss�o para acessar o m�dulo de
   * Gest�o de Incidente.
   *
   * <p>Testa se o usu�rio logado tem permiss�o para acessar o m�dulo de
   * Gest�o de Incidente, caso n�o tenha, exibe uma mensagem na tela e finaliza a
   * execu��o!</p>
   * @access public
   * @return boolean Permiss�o
   */
  public static function testUserPermissionToIncidentMode(){
    if(!FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(INCIDENT_MODE)){
      $msInfo = FWDLanguage::getPHPStringValue('st_denied_permission_to_access_incident_module','Voc� n�o tem permiss�o para acessar o m�dulo de Melhoria Cont�nua. <br><br>Voc� est� utilizando uma licen�a que n�o permite acessar este m�dulo!');
      echo $msInfo;
      exit();
    }else{
      return true;
    }
  }

  /**
   * Retorna qual �cone deve ser usado para determinada extens�o de arquivo.
   *
   * <p>M�todo para retornar qual �cone deve ser usado para determinada extens�o de arquivo.</p>
   * @access public
   * @param string $psFileName Nome do arquivo
   * @return string Nome do �cone
   */
  public static function getIcon($psFileName){

    $maExtMime = self::$caExtMime;

    $maAux = explode(".",strtolower(trim($psFileName)));
    $msExtension = $maAux[count($maAux)-1];

    $msGfx = '';
    foreach($maExtMime as $msItem) {
      $maMime = explode(",", $msItem);
      $maExt = explode(".", $maMime[0]);
      foreach( $maExt as $msAux) {
        if (($msAux && $msAux==$msExtension) !== false) {
          $msGfx = $maMime[1];
          break;
        }
      }
      if ($msGfx) break;
    }
    if (!$msGfx) $msGfx = "ext-misc.gif";
    return $msGfx;
  }

  /**
  * Adiciona um periodo de tempo a um timestamp.
  *
  * <p>Adiciona um periodo de tempo a um timestamp.</p>
  * @access public
  * @param integer $piPeriodSize N�mero de unidades do periodo
  * @param integer $piPeriodUnit Unidade do per�odo
  * @param integer $piTime Timestamp inicial
  * @return integer Timestamp resultante
  */
  public static function addPeriodToTime($piPeriodSize,$piPeriodUnit,$piTime){
    switch($piPeriodUnit){
      case SCHEDULE_BYDAY:{
        return $piTime + $piPeriodSize * 86400;
      }
      case SCHEDULE_BYWEEK:{
        return $piTime + $piPeriodSize * 604800;
      }
      case SCHEDULE_BYMONTH:{
        return mktime(
        date('H',$piTime),
        date('i',$piTime),
        date('s',$piTime),
        date('m',$piTime) + $piPeriodSize,
        date('d',$piTime),
        date('Y',$piTime)
        );
      }
      default:{
        trigger_error("Invalid period unit '$piPeriodUnit'.",E_USER_ERROR);
      }
    }
  }

  /**
  * Subtrai um periodo de tempo de um timestamp.
  *
  * <p>Subtrai um periodo de tempo de um timestamp.</p>
  * @access public
  * @param integer $piPeriodSize N�mero de unidades do per�odo
  * @param integer $piPeriodUnit Unidade do per�odo
  * @param integer $piTime Timestamp inicial
  * @return integer Timestamp resultante
  */
  public static function subtractPeriodFromTime($piPeriodSize,$piPeriodUnit,$piTime){
    return self::addPeriodToTime(-$piPeriodSize,$piPeriodUnit,$piTime);
  }

  /**
  * Retorna a sess�o atual.
  *
  * <p>Retorna a sess�o atual.</p>
  * @access public
  * @return FWDSession Sess�o atual
  */
  public static function getCurrentSession(){
    return FWDWebLib::getInstance()->getSessionById(self::$csCurrentSessionId);
  }

  /**
  * Retorna o id do usu�rio da sess�o atual.
  *
  * <p>Retorna o id do usu�rio da sess�o atual.</p>
  * @access public
  * @return integer Id do usu�rio da sess�o atual
  */
  public static function getCurrentUserId(){
    return FWDWebLib::getInstance()->getSessionById(self::$csCurrentSessionId)->getUserId();
  }

  /**
  * Verifica se possui determinado m�dulo.
  *
  * <p>M�todo para verificar se possui determinado m�dulo.</p>
  * @access public
  * @param integer $piModule Id do m�dulo
  * @return boolean True sse possui o m�dulo
  */
  public static function hasModule($piModule){
    return self::getCurrentSession()->hasModule($piModule);
  }

  /**
  * Dada uma data inicial e um per�odo de tempo, calcula a data resultante.
  *
  * <p>Dada uma data inicial e um per�odo de tempo, calcula a data resultante.</p>
  * @access public
  * @param integer $piLastTime Timestamp da data inicial
  * @param integer $piPeriodUnitId Id da unidade de tempo do per�odo
  * @param integer $piPeriodsCount Quantidade de unidades de tempo do per�odo
  * @return integer Timestamp da data resultante
  */
  public static function getNextDate($piLastTime,$piPeriodUnitId,$piPeriodsCount){
    switch($piPeriodUnitId){
      case SCHEDULE_BYDAY:{
        return $piLastTime + $piPeriodsCount * 86400;
      }
      case SCHEDULE_BYWEEK:{
        return $piLastTime + $piPeriodsCount * 604800;
      }
      case SCHEDULE_BYMONTH:{
        list($miYear,$miMonth,$miDay,$miHour,$miMin,$miSec) = explode(':',date('Y:m:d:H:i:s',$piLastTime));
        $miNextDate = mktime($miHour,$miMin,$miSec,$miMonth+$piPeriodsCount,1,$miYear);
        $miNextDateMonthDays = date('t',$miNextDate);
        return mktime($miHour,$miMin,$miSec,$miMonth+$piPeriodsCount,min($miDay,$miNextDateMonthDays),$miYear);
      }
      default:{
        trigger_error("Invalid period unit identifier '$piPeriodUnitId'.",E_USER_ERROR);
      }
    }
  }

  /**
  * Retorna uma inst�ncia de um contexto do tipo especificado.
  *
  * <p>Retorna uma inst�ncia de um contexto do tipo especificado.</p>
  * @access public
  * @param integer $piType Tipo do contexto
  * @return ISMSContext Inst�ncia de contexto
  */
  public static function getContextObject($piType){
    if(isset(self::$caContextsInfo[$piType]) && self::$caContextsInfo[$piType]['class']){
      if(count(self::$caContextsInfo[$piType]['parameters'])>0){
        $maParameters = array();
        for($i=0;$i<count(self::$caContextsInfo[$piType]['parameters']);$i++){
          $maParameters[] = var_export(self::$caContextsInfo[$piType]['parameters'][$i],true);
        }
        $msCode = '$moContext = new '.self::$caContextsInfo[$piType]['class']
        .'('.implode(',',$maParameters).');';
        eval($msCode);
        return $moContext;
      }else{
        return new self::$caContextsInfo[$piType]['class'];
      }
    }else{
      return null;
    }
  }

  /**
  * Verifica se o usu�rio logado possui determinada ACL.
  *
  * <p>Verifica se o usu�rio logado possui determinada ACL.</p>
  * @access public
  * @param integer $psAclTag Tag da ACL
  * @return boolean True sse possui a ACL
  */
  public static function userHasACL($psAclTag){
    return !in_array($psAclTag,FWDWebLib::getInstance()->getSessionById(self::$csCurrentSessionId)->getUserDeniedACLs());
  }

  /**
  * Verifica se o sistema est� compilado.
  *
  * <p>Verifica se o sistema est� compilado.</p>
  * @access public
  * @return boolean True sse est� compilado
  */
  public static function isCompiled(){
    return (FWDWebLib::getInstance()->getXmlLoadMethod()==XML_STATIC?true:false);
  }

  /**
   * Retorna o c�digo javascript espec�fico para a janela de tarefa.
   *
   * <p>M�todo para retornar o c�digo javascript espec�fico para a janela de tarefa.</p>
   *
   * @access public
   * @param integer $piTaskId Id da Tarefa
   * @param integer $piActivity A��o que determina a tarefa
   * @param integer $piContextId Id do contexto sobre o qual a a��o ser� tomada
   * @param integer $piContextType Tipo do contexto
   * @return string C�digo javascript para abrir a janela da tarefa espec�fica
   */
  public static function getTaskPopup($piTaskId,$piActivity,$piContextId,$piContextType,$piAutoOpen=0) {
    switch($piActivity){
      case ACT_REAL_EFFICIENCY:
      case ACT_INC_CONTROL_INDUCTION:{
        // popup_control_revision
        return "isms_open_popup('popup_control_revision_task', 'packages/risk/popup_control_revision_task.php?task={$piTaskId}&control={$piContextId}&auto={$piAutoOpen}', '', 'true')";
      }case ACT_CONTROL_TEST:{
        return "isms_open_popup('popup_control_test_task', 'packages/risk/popup_control_test_task.php?task={$piTaskId}&control={$piContextId}&auto={$piAutoOpen}', '', 'true')";
      }case ACT_DOCUMENT_REVISION:{
        return "isms_open_popup('popup_document_revision_task', 'packages/policy/popup_document_revision_task.php?task={$piTaskId}&document={$piContextId}&auto={$piAutoOpen}', '', 'true')";
      }case ACT_NON_CONFORMITY_APPROVAL:{
        return "isms_open_popup('popup_non_conformity_approval_task', 'packages/improvement/popup_non_conformity_approval_task.php?task={$piTaskId}&nc={$piContextId}&auto={$piAutoOpen}', '', 'true')";
      }case ACT_ACTION_PLAN_FINISH_CONFIRM:{
        return "isms_open_popup('popup_action_plan_finish_confirm', 'packages/improvement/popup_action_plan_finish_confirm.php?task={$piTaskId}&ap={$piContextId}&auto={$piAutoOpen}', '', 'true')";
      }case ACT_ACTION_PLAN_REVISION:{
        return "isms_open_popup('popup_action_plan_revision_task', 'packages/improvement/popup_action_plan_revision_task.php?task={$piTaskId}&ap={$piContextId}&auto={$piAutoOpen}', '', 'true')";
      }case ACT_NEW_RISK_PARAMETER_APPROVAL: {
      	return "isms_open_popup('popup_new_risk_parameter_approval_task', 'packages/improvement/popup_new_risk_parameter_approval_task.php?task={$piTaskId}&ap={$piContextId}&auto={$piAutoOpen}', '', 'true')";
      }default:{
        switch($piContextType){
          case CONTEXT_RISK:
          case CONTEXT_EVENT:
          case CONTEXT_PROCESS_ASSET:
          case CONTEXT_RISK_CONTROL:{// popup_complex_task_view
            return "isms_open_popup('popup_complex_task_view', 'popup_complex_task_view.php?task={$piTaskId}&auto={$piAutoOpen}', '', 'true')";
          }case CONTEXT_RISK_LIMITS:{// popup_risk_limits_task_view
            return "isms_open_popup('popup_risk_limits_task_view', 'popup_risk_limits_task_view.php?task={$piTaskId}&auto={$piAutoOpen}', '', 'true')";
          }case CONTEXT_DOCUMENT:{// popup_document_read
            return "isms_open_popup('popup_document_read', 'packages/policy/popup_document_read.php?approve=1&document={$piContextId}&auto={$piAutoOpen}', '', 'true')";
          }case CONTEXT_CI_OCCURRENCE:{//popup_super_simple_task_view
            return "isms_open_popup('popup_simple_task_view', 'popup_simple_task_view.php?task={$piTaskId}&auto={$piAutoOpen}', '', 'true')";
          }case CONTEXT_CI_INCIDENT:{
            if($piActivity == ACT_INC_APPROVAL){
              return "isms_open_popup('popup_incident_approval_task', 'packages/improvement/popup_incident_approval_task.php?task={$piTaskId}&auto={$piAutoOpen}', '', 'true')";
            }else{//popup_incident_task_view
              return "isms_open_popup('popup_incident_task_view', 'packages/improvement/popup_incident_task_view.php?task={$piTaskId}&auto={$piAutoOpen}', '', 'true')";
            }
          }default:{//popup_simple_task_view
            return "isms_open_popup('popup_simple_task_view', 'popup_simple_task_view.php?task={$piTaskId}&auto={$piAutoOpen}', '', 'true')";
          }
        }
        return "";
      }
    }
  }

  /**
  * Verifica se o usu�rio n�o est� tentando acessar o sistema pela URL errada.
  *
  * <p>Verifica se o usu�rio n�o est� tentando acessar o sistema pela URL errada.</p>
  * <p>S� faz essa verifica��o se o sistema est� compilado.</p>
  * @access public
  */
  public static function checkUrl(){
    if(self::isCompiled()){
      $msCurrentUrl = FWDWebLib::getCurrentURL();
      $moSaasAdmin = new ISMSSaaSAdmin();
      if(in_array(ISMSSaaS::getConfig(ISMSSaaS::LICENSE_TYPE), ISMSLicense::getTrialLicenseTypes())){
        $msUrl = $moSaasAdmin->getConfig(ISMSSaaSAdmin::TRIAL_URL);
      }else{
        $msUrl = $moSaasAdmin->getConfig(ISMSSaaSAdmin::COMMERCIAL_URL);
      }
      if(substr($msCurrentUrl,0,strlen($msUrl)) != $msUrl){
        header('HTTP/1.x 404 Not Found');
        include 'not_found.html';
        exit();
      }
    }
  }

  /**
  * Evacua o sistema, ou seja, desloga todos usu�rios.
  *
  * <p>Evacua o sistema, ou seja, desloga todos usu�rios.</p>
  * @access public
  */
  public static function evacuateSystem(){
    $moDataset = new FWDDBDataSet(FWDWebLib::getConnection());
    $moDataset->setQuery("UPDATE isms_user SET sSession = ''");
    $moDataset->execute();
  }

  /**
  * Formata um tamanho de arquivo.
  *
  * <p>Formata um tamanho de arquivo.</p>
  * @access public
  * @param integer $piSize Tamanho do arquivo
  * @param integer $piDecimal N�mero de casas decimais
  * @return string Tamanho formatado
  */
  public static function formatFileSize($piSize, $piDecimal = 2){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

    if($piSize < 0) $piSize *= -1;
    if($piSize == '') $piSize = 0;

    $maSizes = array("B","KB","MB","GB","TB","PB","EB");

    for($i=0;$i<count($maSizes);$i++){
      if($piSize < 1024) break;
      $piSize /= 1024;
    }
    $msOut = "";
    if($i) $msOut = sprintf("%.{$piDecimal}f %s", $piSize, $maSizes[$i]);
    else $msOut = "$piSize bytes";
    return $msOut;
  }

  /**
  * Exibe uma popup avisando que a licen�a � inv�lida ou foi corrompida
  *
  * <p>Exibe uma popup avisando que a licen�a � inv�lida ou foi corrompida.</p>
  * @access public
  */
  public static function openLicenseCorrupted(){
    $moWebLib = FWDWebLib::getInstance();
    $mbIsAjax = (FWDStartEvent::getInstance()->getAjaxEventName())?1:0;

    if(!$mbIsAjax) {
      $moPogDialog = new FWDDialog();
      $moPogDialog->setAttrName("dialog_licensecorrupted");
      FWDWebLib::addObject("dialog_licensecorrupted",$moPogDialog);
      FWDWebLib::getInstance()->dump_html($moPogDialog);
      echo "<script>";
      echo "isms_open_popup('popup_license_corrupted','".$moWebLib->getSysRefBasedOnTabMain()."license_corrupted.php','','true',132,350);";
      echo "</script>";
    }
    else {
      echo "isms_open_popup('popup_license_corrupted','".$moWebLib->getSysRefBasedOnTabMain()."license_corrupted.php','','true',132,350);";
    }
  }

  /**
  * Trunca uma string.
  *
  * <p>M�todo para truncar uma string.</p>
  * @access public
  * @param string $psString String
  * @param integer $piMaxSize Tamanho m�ximo
  * @return string String truncada (caso necess�rio)

  public function truncateString($psString, $piMaxSize) {
  	while(!strchr("abcdefghijklmnopqrstuvxyzw.,/ABCDEFGHIJKLMNOPQRSTUWVXYZ;_(){}[]:-+=",$psString[$piMaxSize -1])){
  		$piMaxSize--;
  	}
    if (strlen($psString) > $piMaxSize) return mb_substr($psString, 0, $piMaxSize - 3, mb_detect_encoding($psString, 'UTF-8, ISO-8859-1')) . '...';
    else return $psString;
  }

  */

 /**
  * Trunca uma string.
  *
  * <p>M�todo para truncar uma string.</p>
  * @access public
  * @param string $psString String
  * @param integer $piMaxSize Tamanho m�ximo
  * @return string String truncada (caso necess�rio)
  */
  public static function truncateString($psString, $piMaxSize) {
  	$piMaxSize -= 3;
  	$psString = str_replace(chr(13),"",$psString); // tirando \r, por algum motivo o javascript fica doidao com isto - Piero

  	$encoding =  mb_detect_encoding($psString, 'UTF-8, ISO-8859-1');
  	$psString = mb_convert_encoding($psString, $encoding, 'UTF-8');

  	if(mb_strlen($psString, 'UTF-8') > $piMaxSize){
  	  $psString = mb_substr($psString, 0, $piMaxSize, 'UTF-8') . '...';
  	}

  	return $psString;
  }

  /**
   * Retorna uma string convertida em UTF-8
   * @param $psString
   * @return string convertida em UTF-8
   */
  public static function convertString($psString) {
  	return(mb_convert_encoding($psString,'UTF-8',mb_detect_encoding($psString, 'UTF-8, ISO-8859-1')));
  }

  /**
  * Retorna os relat�rios suportados.
  *
  * <p>M�todo para retornar os relat�rios suportados.</p>
  * @access public
  * @return array Relat�rios suportados.
  */
  public static function getSupportedReportFormats() {
    return array(
      REPORT_FILETYPE_HTML,
      REPORT_FILETYPE_HTML_DOWNLOAD,
      REPORT_FILETYPE_WORD,
      REPORT_FILETYPE_EXCEL
    );
  }

  /**
  * Retorna o nome do sistema baseado na licen�a.
  *
  * <p>M�todo para retornar o nome do sistema baseado na licen�a.</p>
  * @access public
  * @return string Nome do sistema
  */
  public static function getSystemName() {
    $msName = "";
    $moLicense = new ISMSLicense();
    if ($moLicense->isISMS()) {
      $msName = "Real ISMS";
    }
    elseif ($moLicense->isEMS()) {
      $msName = "Real EMS";
    }
    elseif ($moLicense->isOHS()) {
      $msName = "Real OHS";
    }
    elseif ($moLicense->isSOX()) {
      $msName = "Real SOX";
    }
    elseif ($moLicense->isPCI()) {
      $msName = "Real PCI";
    }
    else {
      $msName = "";
    }
    return $msName;
  }

  /**
  * Retorna o c�digo javascript para popular um select.
  *
  * <p>M�todo para retornar o c�digo javascript para ppopular um select.</p>
  * @access public
  * @param string $psSelect Nome do select
  * @param string $psValues Valores (devem estar serializados)
  * @return string C�digo javascript
  */
  public static function getJavascriptPopulateSelect($psSelect, $psValues) {
    //$psValues = str_replace("'", "\'", $psValues);
    $psValues = str_replace(
      array("'","\\","\"", "\r\n", "\n", "\r"),
      array("\'","\\\\","\\\"", " ", " ", " "), $psValues);
  	return "js_populate_select('$psSelect','{$psValues}');";
  }

  /**
  * Retorna a url da p�gina de buy now.
  *
  * <p>M�todo para retornar a url da p�gina de buy now.</p>
  * @access public
  * @return string URL
  */
  public function getBuyNowUrl() {
    $moLicense = new ISMSLicense();
    $moSaasAdmin = new ISMSSaaSAdmin();
    $msUri = $moSaasAdmin->getConfig(ISMSSaaSAdmin::BUY_NOW_URL);
    if ($moLicense->isISMS()) $msUri = str_replace('%system%', 'realisms', $msUri);
    else if ($moLicense->isEMS()) $msUri = str_replace('%system%', 'realems', $msUri);
    else if ($moLicense->isOHS()) $msUri = str_replace('%system%', 'realohs', $msUri);
    else if ($moLicense->isSOX()) $msUri = str_replace('%system%', 'realsox', $msUri);
    else if ($moLicense->isPCI()) $msUri = str_replace('%system%', 'realpci', $msUri);
    else $msUri = str_replace('%system%', '', $msUri);
    return $msUri;
  }
}
?>
