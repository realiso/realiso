<?
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSLicense.
 * 
 * <p>Classe que manipula a licen�a do ISMS.</p>
 * 
 * @package ISMS
 * @subpackage classes
 */
class ISMSLicense {

  /**
   * Constantes de tipo de licen�a do ISMS SaaS
   */
  const LICENSE_TYPE_TRIAL_ISMS = 5801;
  const LICENSE_TYPE_SUBSCRIPTION_ISMS = 5802;
  
  const LICENSE_TYPE_TRIAL_EMS = 5803;
  const LICENSE_TYPE_SUBSCRIPTION_EMS = 5804;
  
  const LICENSE_TYPE_TRIAL_OHS = 5805;
  const LICENSE_TYPE_SUBSCRIPTION_OHS = 5806;
  
  const LICENSE_TYPE_TRIAL_SOX = 5807;
  const LICENSE_TYPE_SUBSCRIPTION_SOX = 5808;
  
  const LICENSE_TYPE_TRIAL_PCI = 5809;
  const LICENSE_TYPE_SUBSCRIPTION_PCI = 5810;
  
  const LICENSE_TYPE_TRIAL_SOX_MINIMA = 5811;
  const LICENSE_TYPE_SUBSCRIPTION_SOX_MINIMA = 5812;

  const LICENSE_TYPE_TRIAL_THREE = 5813;
  const LICENSE_TYPE_SUBSCRIPTION_THREE = 5814;    

 /**
   * Nome do cliente
   * @var string
   * @access private
   */
  private $csClientName = "";
 
 /**
   * M�dulos habilitados
   * @var integer
   * @access private
   */
  private $csModules = '';
  
  /**
   * Array de m�dulos dispon�veis
   * @var array
   * @access private
   */
  private static $caModules = array(
    'RM' => RISK_MANAGEMENT_MODE,
    'PM' => POLICY_MODE,
    'CI' => INCIDENT_MODE  
  );
  
 /**
   * N�mero m�ximo de usu�rios
   * @var integer
   * @access private
   */
  private $ciMaxUsers = 0;
  
 /**
   * Data de ativa��o da licen�a (timestamp)
   * @var integer
   * @access private
   */
  private $ciActivationDate = 0;
  
 /**
   * Data de expira��o da licen�a (timestamp)
   * @var integer
   * @access private
   */
  private $ciExpiracy = 0;
  
 /**
   * N�mero m�ximo de usu�rios simult�neos
   * @var integer
   * @access private
   */
  private $ciMaxSimultaneousUsers = 0;
  
  /**
   * Tipo de Licen�a (commercial ou trial)
   * @var integer
   * @access private
   */
  private $ciLicenseType = 0;
  
 /**
  * Construtor.
  * 
  * <p>Construtor da classe ISMSLicense.</p>
  * 
  * @access public
  */
  public function __construct(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $moISMSSaaS = new ISMSSaaS();
    $this->csClientName           = $moISMSSaaS->getConfig(ISMSSaaS::LICENSE_CLIENT_NAME);
    $this->csModules              = $moISMSSaaS->getConfig(ISMSSaaS::LICENSE_MODULES);
    $this->ciMaxUsers             = $moISMSSaaS->getConfig(ISMSSaaS::LICENSE_MAX_USERS);
    $this->ciActivationDate       = $moISMSSaaS->getConfig(ISMSSaaS::LICENSE_ACTIVATION_DATE);
    $this->ciExpiracy             = $moISMSSaaS->getConfig(ISMSSaaS::LICENSE_EXPIRACY);
    $this->ciMaxSimultaneousUsers = $moISMSSaaS->getConfig(ISMSSaaS::LICENSE_MAX_SIMULTANEOUS_USERS);
    $this->ciLicenseType          = $moISMSSaaS->getConfig(ISMSSaaS::LICENSE_TYPE);
      
    if($this->getHash() != $moISMSSaaS->getConfig(ISMSSaaS::LICENSE_HASH)){
      ISMSLib::openLicenseCorrupted();
      $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
      $moSession->logout();
      die(1);
    }
    
/*    if($this->ciExpiracy && $this->ciExpiracy < ISMSLib::ISMSTime()){
      ISMSLib::openLicenseExpired($this->ciExpiracy);
      $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
      $moSession->logout();
      die(1);
    }*/
  }
  
  protected function getHash(){
    return md5($this->csClientName.$this->csModules.$this->ciMaxUsers.$this->ciActivationDate.$this->ciExpiracy.$this->ciMaxSimultaneousUsers.$this->ciLicenseType.'LICENSEHASH');
  }

 /**
  * Retorna o nome do cliente.
  * 
  * <p>M�todo para retornar o nome do cliente.</p>
  * 
  * @access public
  * @return string Nome do cliente
  */
  public function getClientName() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return $this->csClientName;
  }

 /**
  * Retorna os m�dulos do sistema.
  * 
  * <p>M�todo para retornar os m�dulos do sistema.</p>
  * 
  * @access public
  * @return string M�dulos [RM|PM|...]
  */
  public function getModules(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $maEnabledModules = explode('|', $this->csModules);
    $maModules = array();
    foreach($maEnabledModules as $msMod){
      if(isset(self::$caModules[$msMod])){
        $maModules[] = self::$caModules[$msMod];
      }
    }
    //teste criado para funcionar com as vers�es de licen�a antigas (da vers�o 2) 
    //que n�o continham a id�ia de m�dulo
    if(count($maModules)==0){
      $maModules[]='RM';
    }
    return $maModules;
  }
  
 /**
  * Libera a licen�a por tempo indeterminado.
  * 
  * <p>M�todo para liberar a licen�a por tempo indeterminado.</p>
  * @access public
  */
  public function unlockLicense(){
    if ($this->isSubscription()) {
      $moISMSSaaS = new ISMSSaaS();
      $moISMSSaaS->setConfig(ISMSSaaS::LICENSE_EXPIRACY,'');
      $this->ciExpiracy = '';
      $this->updateHash();
    }
    else {
      trigger_error('Cannot unlock license on a trial system!', E_USER_WARNING);
    }
  }
  
 /**
  * Retorna o n�mero m�ximo de usu�rios.
  * 
  * <p>M�todo para retornar o n�mero m�ximo de usu�rios.</p>
  * 
  * @access public
  * @return integer N�mero m�ximo de usu�rios
  */
  public function getMaxUsers() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return $this->ciMaxUsers;
  }
  
 /**
  * Altera o n�mero m�ximo de usu�rios.
  * 
  * <p>M�todo para alterar o n�mero m�ximo de usu�rios.</p>
  * 
  * @access public
  * @param integer $piMaxUsers Novo n�mero m�ximo de usu�rios
  */
  public function setMaxUsers($piMaxUsers){
    $moISMSSaaS = new ISMSSaaS();
    $moISMSSaaS->setConfig(ISMSSaaS::LICENSE_MAX_USERS,$piMaxUsers);
    $this->ciMaxUsers = $piMaxUsers;
  }
  
 /**
  * Retorna a data de ativa��o da licen�a (timestamp).
  * 
  * <p>M�todo para retornar a data de ativa��o da licen�a (timestamp).</p>
  * 
  * @access public
  * @return integer Data de expira��o (timestamp)
  */
  public function getActivationDate(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return $this->ciActivationDate;
  }
  
 /**
  * Retorna a data de expira��o da licen�a (timestamp).
  * 
  * <p>M�todo para retornar a data de expira��o da licen�a (timestamp).</p>
  * 
  * @access public
  * @return integer Data de expira��o (timestamp)
  */
  public function getExpiracy() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return $this->ciExpiracy;
  }
  
 /**
  * Retorna o n�mero m�ximo de usu�rios simult�neos.
  * 
  * <p>M�todo para retornar o n�mero m�ximo de usu�rios simult�neos.</p>
  * 
  * @access public
  * @return integer N�mero m�ximo de usu�rios simult�neos
  */
  public function getMaxSimultaneousUsers() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return $this->ciMaxSimultaneousUsers;
  }
  
 /**
  * Altera o n�mero m�ximo de usu�rios simult�neos.
  * 
  * <p>M�todo para alterar o n�mero m�ximo de usu�rios simult�neos.</p>
  * 
  * @access public
  * @param integer $piMaxUsers Novo n�mero m�ximo de usu�rios simult�neos
  */
  public function setMaxSimultaneousUsers($piMaxSimultaneousUsers){
    $moISMSSaaS = new ISMSSaaS();
    $moISMSSaaS->setConfig(ISMSSaaS::LICENSE_MAX_SIMULTANEOUS_USERS,$piMaxSimultaneousUsers);
    $this->ciMaxSimultaneousUsers = $piMaxSimultaneousUsers;
  }
  
 /**
  * Atualiza o hash da licen�a.
  * 
  * <p>Atualiza o hash da licen�a.</p>
  * @access public
  */
  public function updateHash(){
    $moISMSSaaS = new ISMSSaaS();
    $moISMSSaaS->setConfig(ISMSSaaS::LICENSE_HASH,$this->getHash());
  }
  
 /**
  * Retorna o tipo da licen�a
  * 
  * <p>M�todo para retornar o tipo da licen�a.</p>
  * 
  * @access public
  * @return integer Tipo da Licen�a
  */
  public function getLicenseType() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    if ($this->isTrial()) return FWDLanguage::getPHPStringValue('mx_trial','Trial');
    else return FWDLanguage::getPHPStringValue('mx_commercial','Comercial');
  }
  
  /**
  * Verifica se a licen�a � trial.
  * 
  * <p>M�todo para verificar se a licen�a � trial.</p>
  * 
  * @access public
  * @return boolean True se licen�a � trial.
  */
  public function isTrial() {
    return in_array($this->ciLicenseType, self::getTrialLicenseTypes()) ? true : false;
  }
  
  /**
  * Verifica se a licen�a � subscription.
  * 
  * <p>M�todo para verificar se a licen�a � subscription.</p>
  * 
  * @access public
  * @return boolean True se licen�a � subscription.
  */
  public function isSubscription() {
    return in_array($this->ciLicenseType, self::getSubscriptionLicenseTypes()) ? true : false;
  }
  
  /**
  * Verifica se a licen�a � EMS.
  * 
  * <p>M�todo para verificar se a licen�a � EMS.</p>
  * 
  * @access public
  * @return boolean True se licen�a � EMS.
  */
  public function isEMS() {
    return in_array($this->ciLicenseType, array(self::LICENSE_TYPE_SUBSCRIPTION_EMS, self::LICENSE_TYPE_TRIAL_EMS)) ? true : false;
  }
  
  /**
  * Verifica se a licen�a � OHS.
  * 
  * <p>M�todo para verificar se a licen�a � OHS.</p>
  * 
  * @access public
  * @return boolean True se licen�a � OHS.
  */
  public function isOHS() {
    return in_array($this->ciLicenseType, array(self::LICENSE_TYPE_SUBSCRIPTION_OHS, self::LICENSE_TYPE_TRIAL_OHS)) ? true : false;
  }
  
  /**
  * Verifica se a licen�a � ISMS.
  * 
  * <p>M�todo para verificar se a licen�a � ISMS.</p>
  * 
  * @access public
  * @return boolean True se licen�a � ISMS.
  */
  public function isISMS() {
    return in_array($this->ciLicenseType, array(self::LICENSE_TYPE_SUBSCRIPTION_ISMS, self::LICENSE_TYPE_TRIAL_ISMS)) ? true : false;
  }
  
  /**
  * Verifica se a licen�a � SOX.
  * 
  * <p>M�todo para verificar se a licen�a � SOX.</p>
  * 
  * @access public
  * @return boolean True se licen�a � SOX.
  */
  public function isSOX() {
    return in_array($this->ciLicenseType, self::getSOXLicenseTypes()) ? true : false;
  }
  
  /**
  * Verifica se a licen�a � PCI.
  * 
  * <p>M�todo para verificar se a licen�a � PCI.</p>
  * 
  * @access public
  * @return boolean True se licen�a � PCI.
  */
  public function isPCI() {
    return in_array($this->ciLicenseType, array(self::LICENSE_TYPE_SUBSCRIPTION_PCI, self::LICENSE_TYPE_TRIAL_PCI)) ? true : false;
  }
  
  /**
   * Verifica se a licensa � da MinimarisK
   * 
   *  <p>M�todo para verificar se a licensa � da minimarisk.</p>
   *  
   *  @access public
   *  @return boolean true se a licensa � minimarisk
   *  
   */
  public function isMinima() {
  	return in_array($this->ciLicenseType, self::getMinimaRiskLicenseTypes()) ? true : false;
  }
  
  /**
  * Verifica se a licen�a � 3380.
  * 
  * <p>M�todo para verificar se a licen�a � 3380.</p>
  * 
  * @access public
  * @return boolean True se licen�a � 3380.
  */
  public function isTHREE() {
    return in_array($this->ciLicenseType, self::getTHREELicenseTypes()) ? true : false;
  }  
  
  /**
  * Retorna os tipos de licen�a do EMS.
  * 
  * <p>M�todo para retornar os tipos de licen�a do EMS.</p>
  * 
  * @access public
  * @return array Licen�as do tipo EMS.
  */
  public static function getEMSLicenseTypes() {
    return array(
      self::LICENSE_TYPE_SUBSCRIPTION_EMS,
      self::LICENSE_TYPE_TRIAL_EMS
    );
  }
  
  /**
  * Retorna os tipos de licen�a do ISMS.
  * 
  * <p>M�todo para retornar os tipos de licen�a do ISMS.</p>
  * 
  * @access public
  * @return array Licen�as do tipo ISMS.
  */
  public static function getISMSLicenseTypes() {
    return array(
      self::LICENSE_TYPE_SUBSCRIPTION_ISMS,
      self::LICENSE_TYPE_TRIAL_ISMS
    );
  }
  
  /**
  * Retorna os tipos de licen�a do OHS.
  * 
  * <p>M�todo para retornar os tipos de licen�a do OHS.</p>
  * 
  * @access public
  * @return array Licen�as do tipo OHS.
  */
  public static function getOHSLicenseTypes() {
    return array(
      self::LICENSE_TYPE_SUBSCRIPTION_OHS,
      self::LICENSE_TYPE_TRIAL_OHS
    );
  }
  
  /**
  * Retorna os tipos de licen�a do SOX.
  * 
  * <p>M�todo para retornar os tipos de licen�a do SOX.</p>
  * 
  * @access public
  * @return array Licen�as do tipo SOX.
  */
  public static function getSOXLicenseTypes() {
    return array(
      self::LICENSE_TYPE_SUBSCRIPTION_SOX,
      self::LICENSE_TYPE_TRIAL_SOX,
      self::LICENSE_TYPE_SUBSCRIPTION_SOX_MINIMA,
      self::LICENSE_TYPE_TRIAL_SOX_MINIMA
    );
  }
  
  /**
  * Retorna os tipos de licen�a do PCI.
  * 
  * <p>M�todo para retornar os tipos de licen�a do PCI.</p>
  * 
  * @access public
  * @return array Licen�as do tipo PCI.
  */
  public static function getPCILicenseTypes() {
    return array(
      self::LICENSE_TYPE_SUBSCRIPTION_PCI,
      self::LICENSE_TYPE_TRIAL_PCI
    );
  }
  
  /**
  * Retorna os tipos de licen�a do 3380.
  * 
  * <p>M�todo para retornar os tipos de licen�a do 3380.</p>
  * 
  * @access public
  * @return array Licen�as do tipo 3380.
  */
  public static function getTHREELicenseTypes() {
    return array(
      self::LICENSE_TYPE_SUBSCRIPTION_THREE,
      self::LICENSE_TYPE_TRIAL_THREE
    );
  }  
  
  /**
  * Retorna os tipos de licen�a trial.
  * 
  * <p>M�todo para retornar os tipos de licen�a trial.</p>
  * 
  * @access public
  * @return array Licen�as do tipo trial.
  */
  public static function getTrialLicenseTypes() {
    return array(
      self::LICENSE_TYPE_TRIAL_ISMS,
      self::LICENSE_TYPE_TRIAL_EMS,
      self::LICENSE_TYPE_TRIAL_OHS,
      self::LICENSE_TYPE_TRIAL_SOX,
      self::LICENSE_TYPE_TRIAL_PCI,
      self::LICENSE_TYPE_TRIAL_SOX_MINIMA,
      self::LICENSE_TYPE_TRIAL_THREE
    );
  }
  
  /**
  * Retorna os tipos de licen�a subscription.
  * 
  * <p>M�todo para retornar os tipos de licen�a subscription.</p>
  * 
  * @access public
  * @return array Licen�as do tipo subscription.
  */
  public static function getSubscriptionLicenseTypes() {
    return array(
      self::LICENSE_TYPE_SUBSCRIPTION_ISMS,
      self::LICENSE_TYPE_SUBSCRIPTION_EMS,
      self::LICENSE_TYPE_SUBSCRIPTION_OHS,
      self::LICENSE_TYPE_SUBSCRIPTION_SOX,
      self::LICENSE_TYPE_SUBSCRIPTION_PCI,
      self::LICENSE_TYPE_SUBSCRIPTION_SOX_MINIMA,
      self::LICENSE_TYPE_SUBSCRIPTION_THREE
    );
  }
  
  /**
   * Retorna os tipos de licensa da MinimaRisk 
   *
   *<p>M�todo para retornar os tipos de licensa que fazem parte da minimaRisk.</p>
   *
   *@access public
   *@return array Licensas da minimaRisk
   */
  public static function getMinimaRiskLicenseTypes() {
  	return array(
  		self::LICENSE_TYPE_SUBSCRIPTION_SOX_MINIMA,
  		self::LICENSE_TYPE_TRIAL_SOX_MINIMA
  	);
  }
}
?>