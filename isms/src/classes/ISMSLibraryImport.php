<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSLibraryImport. Classe para ajudar na leitura dos arquivos XML.
 * 
 * <p>Classe para ajudar na leitura dos arquivos XML. Ela extrai listas de strings
 * de arquivos XML, exporta e importa essas listas.</p>
 *
 * @package ISMS
 * @subpackage classes
 */
class ISMSLibraryImport extends ISMSXMLParser {
  protected $coFile;
  protected $caClassList = array();
  protected $ciLevel;
  protected $ciClassId;
  protected $ciParentId;
  protected $csAttr;
  protected $csIdField;
  protected $csValueField;
  protected $csParentIdField; 
  protected $csLibrary;
  protected $cbGetData;
  
  /**
   * Construtor. Carrega o arquivo.
   * 
   * @access public
   * @param object poFile Arquivo criptografado. 
   */
	public function __construct($poFile) {
		$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
		$this->coFile = $poFile;
	}
    
  /**
   * Descriptografa o arquivo e retorna a lista de classes gerada pelo m�todo extractXMLStrings
   * @access public
   * @return array Array dos nomes e par�metros das classes a serem importadas
   */
  public function getClassList() {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
 		return $this->caClassList;
  }
  
  public function noErrorHandler() {}
  
  public function parseFileAOL() {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
  	$moCrypt = new FWDCrypt();
    $moFP = fopen($this->coFile->getTempFileName(),'rb');
    $moCrypt->setIV(fread($moFP,strlen($moCrypt->getIV())));
    $msDecryptedFileContent = "";
    set_error_handler(array($this,"noErrorHandler"));
    while($msFileContent = fgets($moFP)) {
    	$msDecryptedFileContent .= $moCrypt->decrypt($msFileContent);
    }
    restore_error_handler();
  	return parent::parseString($msDecryptedFileContent);
  }
  
  public function parseFileXML() {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $content = file_get_contents($this->coFile->getTempFileName());
    $content = preg_replace("/>(\n|\r| )+</", "><", $content);
    
    set_error_handler(array($this,"noErrorHandler"));

    restore_error_handler();
  	return parent::parseString($content);
  }  
  	
	/**
	 * M�todo executado quando � encontrada uma tag de in�cio no XML.
	 * @access protected
	 */
	protected function startHandler($prParser, $psTagName, $paAttributes){
		$maParameters = func_get_args();
        FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		$this->cbGetData = false; //Sempre que iniciar uma tag, o padr�o � n�o pegar os dados entre tags.
		if ($this->ciLevel > 1) { 
			foreach($paAttributes as $psName=>$psValue) {
				if (strcasecmp($psTagName,"class")==0 && strcasecmp($psName,"name")==0) {
					$this->csLibrary = '';
					$this->csIdField = '';
					$this->csValueField = '';
					$this->csParentIdField = '';
					switch(strtolower($psValue)) {
						case "rmcategory":
							$this->csLibrary = "category";
							$this->csIdField = "category_id";
							$this->csValueField = "category_name";
							break;
						case "rmsectionbestpractice":
							$this->csLibrary = "sectionbestpractice";
							$this->csIdField = "section_id";
							$this->csValueField = "section_name";
							break;
						case "rmstandard":
							$this->csLibrary = "standard";
							$this->csIdField = "standard_id";
							$this->csValueField = "standard_name";
							break;
						case "rmevent":
							$this->csLibrary = "event";
							$this->csIdField = "event_id";
							$this->csParentIdField = "event_category_id";
							break;
						case "rmbestpractice":
							$this->csLibrary = "bestpractice";
							$this->csIdField = "best_practice_id";
							$this->csParentIdField = "section_best_practice_id";
							break;
						case "rmbestpracticeevent":
							$this->csLibrary = "bestpracticeevent";
							$this->csIdField = "best_practice_event_id";
							break;
						case "rmbestpracticestandard":
							$this->csLibrary = "bestpracticestandard";
							$this->csIdField = "best_practice_standard_id";
							break;
            case "pmtemplate":
              $this->csLibrary = "template";
              $this->csIdField = "template_id";
              $this->csValueField = "template_name";
              break;
            case "pmtemplatebestpractice":
              $this->csLibrary = "templatebestpractice";
              $this->csIdField = "uniqid";
              break;
            case "noclass_templatefile":
              $this->csLibrary = "templatefile";
              $this->csIdField = "uniqid";
              break;
						default:
							break;
					}
					$this->caClassList[$this->csLibrary]["new"]["className"] = $psValue;
				} elseif(strcasecmp($psTagName,"field")==0) {
					$this->cbGetData = true;
					$this->csAttr = $psValue;
				} 
			}
		} elseif($this->ciLevel==1 && strcasecmp($psTagName,"client")==0) { //Verifica se o nome do cliente contido no arquivo de bibliotecas � o mesmo que o nome do cliente na licen�a
      $soLicense = new ISMSLicense();
      $msClientName = $soLicense->getClientName();
      if (!isset($paAttributes['NAME']) /*|| strcasecmp($paAttributes['NAME'],$msClientName)!=0*/) {
        $this->csErrorMsg = 'Library Import Error: Invalid Client';
        return;
      }
    }
    $this->ciLevel++;
  }

	/**
	 * M�todo executado quando s�o encontrados valores entre tags no XML.
	 * @access protected
	 */
  protected function dataHandler($prParser, $psData){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
  	if ($this->cbGetData && $this->csAttr) {
  		$maClassList = &$this->caClassList[$this->csLibrary]["new"];
  		$msIndex = $this->csAttr;
  		$msSpecialIndex = '';
	  	if ($this->csIdField == 'uniqid') {
        $this->ciClassId = uniqid();
      }
      if ($this->csIdField && strcasecmp($this->csAttr,$this->csIdField)==0) {
	  		$msSpecialIndex = "classId";
	  		$this->ciClassId = $psData;
	  	}
	  	if ($this->csValueField && strcasecmp($this->csAttr,$this->csValueField)==0) {
	  		$msSpecialIndex = "classValue";
	  	}
	  	if ($this->csParentIdField && strcasecmp($this->csAttr,$this->csParentIdField)==0) {
	  		$this->ciParentId = $psData;
	  	}
	  	if(isset($maClassList[$msIndex]) && $maClassList[$msIndex] != '') {
	  		$maClassList[$msIndex] .= $psData;
	  		if($msSpecialIndex)
	  			$maClassList[$msSpecialIndex] .= $psData;
	  	} 
	  	else {
	  		$maClassList[$msIndex] = $psData;
	  		if($msSpecialIndex)
	  			$maClassList[$msSpecialIndex] = $psData;
	  	}
  	}
  	unset($maClassList);
  	unset($msIndex);
  }

	/**
	 * M�todo executado quando � encontrada um fechamento de tag no XML.
	 * @access protected
	 */
  protected function endHandler($prParser, $psTagName){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
  	$maLibrary = &$this->caClassList[$this->csLibrary];
  	if (strcasecmp($psTagName,"class")==0 && isset($maLibrary["new"])) {
  		if ($this->csParentIdField) {
  			$miParentId = $maLibrary["new"][$this->csParentIdField];
  			$maLibrary[$miParentId][$this->ciClassId] = $maLibrary["new"];
  			unset($miParentId);
  		} else {
        $maLibrary[$this->ciClassId] = $maLibrary["new"];
  		}
			unset($maLibrary["new"]);
	  }
		$this->ciLevel--;
  }
}
?>