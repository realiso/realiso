<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe abstrata ISMSXMLParser. Classe base para parsers de XML.
 * 
 * <p>Classe base para parsers de XML.</p>
 *
 * @package ISMS
 * @subpackage classes
 */
abstract class ISMSXMLParser {
	
	protected $crParser;
	protected $csErrorMsg;
  
	protected function setupParser() {
		$this->crParser = xml_parser_create('ISO-8859-1');
    xml_set_object($this->crParser, $this);
    xml_set_element_handler($this->crParser,'startHandler','endHandler');
    xml_set_character_data_handler($this->crParser,'dataHandler');
	}
	
	protected function freeParser(){
    xml_parser_free($this->crParser);
  }
  
  public function parseFile($psFileName){
		if(!file_exists($psFileName) || !is_readable($psFileName)){
      $this->csErrorMsg = sprintf(
				"Could not open file '$psFileName'"
			);
      return false;
    }else{
      $mrFile = fopen($psFileName,'r');
    }
    $this->setupParser();
    while($msData = fread($mrFile,512)){
      $mbParse = xml_parse($this->crParser,$msData,feof($mrFile));
      if(!$mbParse){
        $this->csErrorMsg = sprintf(
          "XML error: %s in file $psFileName at line %d",
          xml_error_string(xml_get_error_code($this->crParser)),
          xml_get_current_line_number($this->crParser)
        );
        //trigger_error($msErrorMsg,E_USER_WARNING);
        $this->freeParser();
        return false;
      }
    }
    fclose($mrFile);
    $this->freeParser();
    return true;
  }
  
	public function parseString($psData){
    $this->setupParser();
    if (!$psData)
    	return false;
		$mbParse = xml_parse($this->crParser,$psData);
		if(!$mbParse){
		  $this->csErrorMsg = sprintf(
		    "XML error: %s at line %d",
		    xml_error_string(xml_get_error_code($this->crParser)),
		    xml_get_current_line_number($this->crParser)
		  );
		  //trigger_error($msErrorMsg,E_USER_WARNING);
		  $this->freeParser();
		  return false;
		}    
    $this->freeParser();
    return true;
  }
  
  public function getErrorMsg() {
  	return $this->csErrorMsg;
  }
  
  abstract protected function startHandler($prParser, $psTagName, $paAttributes);
  abstract protected function dataHandler($prParser, $psData);
  abstract protected function endHandler($prParser, $psTagName);
}
?>