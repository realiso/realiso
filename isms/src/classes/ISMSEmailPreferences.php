<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSEmailPreferences.
 *
 * <p>Calsse respons�vel por manipular as prefer�ncias de usu�rios relativas aos
 * emails do sistema.</p>
 * @package ISMS
 * @subpackage classes
 */
class ISMSEmailPreferences {

  protected $coUserPreferences;

  protected static $caMessageTypes = array(
    'area' => CONTEXT_AREA,
    'process' => array(CONTEXT_PROCESS,CONTEXT_PROCESS_ASSET),
    'asset' => array(CONTEXT_ASSET,CONTEXT_PROCESS_ASSET),
    'risk' => array(CONTEXT_RISK,CONTEXT_RISK_LIMITS,CONTEXT_ACCEPT_RISK,CONTEXT_RISK_CONTROL),
    'control' => array(CONTEXT_CONTROL,CONTEXT_ACCEPT_CONTROL_IMPLEMENTATION,CONTEXT_RISK_CONTROL,CONTEXT_CONTROL_BEST_PRACTICE),
    'standard' => array(CONTEXT_STANDARD,CONTEXT_BEST_PRACTICE_STANDARD),
    'event' => array(CONTEXT_EVENT,CONTEXT_CATEGORY,CONTEXT_BEST_PRACTICE_EVENT),
    'best_practice' => array(CONTEXT_BEST_PRACTICE,CONTEXT_SECTION_BEST_PRACTICE,CONTEXT_BEST_PRACTICE_EVENT,CONTEXT_BEST_PRACTICE_STANDARD,CONTEXT_CONTROL_BEST_PRACTICE),
    'document' => CONTEXT_DOCUMENT,
    'occurrence' => CONTEXT_CI_OCCURRENCE,
    'non_conformity' => CONTEXT_CI_NON_CONFORMITY,
    'incident' => CONTEXT_CI_INCIDENT,
    'action_plan' => CONTEXT_CI_ACTION_PLAN,
    'other' => array(CONTEXT_NONE,CONTEXT_SCOPE,CONTEXT_POLICY,CONTEXT_USER,CONTEXT_PROFILE,CONTEXT_DOCUMENT_TEMPLATE,CONTEXT_CI_INCIDENT_RISK,CONTEXT_CI_INCIDENT_CONTROL)
  );
  
  public function __construct(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    if(!FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(POLICY_MODE)){
      unset(self::$caMessageTypes['document']);
    }
    
    if(!FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(INCIDENT_MODE)){
      unset(self::$caMessageTypes['occurrence']);
      unset(self::$caMessageTypes['non_conformity']);
      unset(self::$caMessageTypes['incident']);
    }
    
    $this->coUserPreferences = new ISMSUserPreferences();
  }

  public function getDigestType($piUserId=0){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $msEmailPreferences = $this->coUserPreferences->getPreference('email_preferences',$piUserId);
    $msDigest = 'none';
    if($msEmailPreferences){
      $maEmailPreferences = FWDWebLib::unserializeString($msEmailPreferences);
      if(ISMSLib::getConfigById(GENERAL_ALLOW_DIGESTS) && isset($maEmailPreferences['digest'])){
        $msDigest = $maEmailPreferences['digest'];
      }
    }
    return $msDigest;
  }
  
  public function getMessageTypes($piUserId=0){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $msEmailPreferences = $this->coUserPreferences->getPreference('email_preferences',$piUserId);
    if($msEmailPreferences){
      $maEmailPreferences = FWDWebLib::unserializeString($msEmailPreferences);
      return $maEmailPreferences['message_types'];
    }else{
      return array_keys(self::$caMessageTypes);
    }
  }
  
  public function getEmailFormat($piUserId=0){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $msEmailPreferences = $this->coUserPreferences->getPreference('email_preferences',$piUserId);
    $msFormat = 'html';
    if($msEmailPreferences){
      $maEmailPreferences = FWDWebLib::unserializeString($msEmailPreferences);
      if(isset($maEmailPreferences['format'])){
        $msFormat = $maEmailPreferences['format'];
      }
    }
    return $msFormat;
  }

  public function setDigestType($psDigestType,$piUserId=0){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $msEmailPreferences = $this->coUserPreferences->getPreference('email_preferences',$piUserId);
    if($msEmailPreferences){
      $maEmailPreferences = FWDWebLib::unserializeString($msEmailPreferences);
    }else{
      $maEmailPreferences = array();
      $maEmailPreferences['message_types'] = array_keys(self::$caMessageTypes);      
    }
    $maEmailPreferences['digest'] = $psDigestType;
    $this->coUserPreferences->setPreference('email_preferences',serialize($maEmailPreferences));
  }

  public function setMessageTypes($paMessageTypes,$piUserId=0){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $msEmailPreferences = $this->coUserPreferences->getPreference('email_preferences',$piUserId);
    if($msEmailPreferences){
      $maEmailPreferences = FWDWebLib::unserializeString($msEmailPreferences);
    }else{
      $maEmailPreferences = array();
    }
    $maEmailPreferences['message_types'] = $paMessageTypes;
    $this->coUserPreferences->setPreference('email_preferences',serialize($maEmailPreferences));
  }
  
  public function setEmailFormat($psFormat,$piUserId=0){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $msEmailPreferences = $this->coUserPreferences->getPreference('email_preferences',$piUserId);
    if($msEmailPreferences){
      $maEmailPreferences = FWDWebLib::unserializeString($msEmailPreferences);
    }else{
      $maEmailPreferences = array();
      $maEmailPreferences['message_types'] = array_keys(self::$caMessageTypes);      
    }
    $maEmailPreferences['format'] = $psFormat;
    $this->coUserPreferences->setPreference('email_preferences',serialize($maEmailPreferences));
  }
  
  public function isMessageTypeSet($piMessageTypeCode,$piUserId=0){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $maMessageTypes = $this->getMessageTypes($piUserId);
    foreach($maMessageTypes as $msMessageType){
      if(array_key_exists($msMessageType, self::$caMessageTypes)){
        if(is_array(self::$caMessageTypes[$msMessageType])){
          if(in_array($piMessageTypeCode,self::$caMessageTypes[$msMessageType])){
            return true;
          }
        }else if(self::$caMessageTypes[$msMessageType]==$piMessageTypeCode){
          return true;
        }
      }
    }
    $mbUnknownType = true;
    foreach(self::$caMessageTypes as $mmTypes){
      if(is_array($mmTypes)){
        if(in_array($piMessageTypeCode,$mmTypes)){
          $mbUnknownType = false;
          break;
        }
      }else{
        if($mmTypes==$piMessageTypeCode){
          $mbUnknownType = false;
          break;
        }
      }
    }
    if($mbUnknownType){
      trigger_error("Unknown message type ($piMessageTypeCode).",E_USER_WARNING);
    }
    return false;
  }
}
?>