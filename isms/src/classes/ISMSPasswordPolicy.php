<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
 /**
 * Classe ISMSPasswordPolicy.
 *
 * <p>Classe que valida as configura��es de pol�tica de senha.</p>
 * @package ISMS
 * @subpackage classes
 */
class ISMSPasswordPolicy {
	
	public function checkPassword($psPassword)
	{
		/*$maParameters = func_get_args(); //Essa linha foi comentada para n�o gravar a senha do usu�rio no debug.
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);*/
    
		$miCharNum = ISMSLib::getConfigById(PASSWORD_CHAR_NUM);
		$miSpecialChars = ISMSLib::getConfigById(PASSWORD_SPECIAL_CHARS);
		$miCaseChars = ISMSLib::getConfigById(PASSWORD_CASE_CHARS);
		$miNumericChars = ISMSLib::getConfigById(PASSWORD_NUMERIC_CHARS);
		//Testa o comprimento
		if (strlen($psPassword) < $miCharNum)
			return false;
		
		//Testa se tem espa�os
		if (preg_match("/[\s]/", $psPassword))
        	return false;
		
		if ($miNumericChars > 0) {
			$maMatches[0]=0;
			preg_match_all("/[0-9]/",$psPassword,$maMatches);
			if (count($maMatches[0]) < $miNumericChars)
				return false;
		}
		
		if ($miSpecialChars > 0) {
			$maMatches[0]=0;
			preg_match_all("/[^a-zA-Z0-9]/",$psPassword,$maMatches);
			if (count($maMatches[0]) < $miSpecialChars)
				return false;
		}
		
		if ($miCaseChars > 0) {
			$maMatchesUpper[0]=0;
			$maMatchesLower[0]=0;
			preg_match_all("/[A-Z]/",$psPassword,$maMatchesUpper);
			preg_match_all("/[a-z]/",$psPassword,$maMatchesLower);
			if (count($maMatchesUpper[0])==0 || count($maMatchesLower[0]) == 0)
				return false;	
		}
		
		return true;
	}
	  
  /**
   * Gera uma senha aleat�ria com base na pol�tica de senhas
   * 
   * <p>M�todo para gerar uma senha aleat�ria com base na pol�tica de senhas</p>
   * @access public
   */ 
  public function generateRandomPassword() {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
   	$miCharNum = ISMSLib::getConfigById(PASSWORD_CHAR_NUM);
		$miSpecialChars = ISMSLib::getConfigById(PASSWORD_SPECIAL_CHARS);
		$miNumericChars = ISMSLib::getConfigById(PASSWORD_NUMERIC_CHARS);
		$miCaseChars = ISMSLib::getConfigById(PASSWORD_CASE_CHARS);
		
		$maSpecialChars = array('!','@','#','$','%','&','*');
		$maNumericChars = array('1','2','3','4','5','6','7','8','9','0');
		$maLowerCaseChars = array('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z');
		$maUpperCaseChars = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');

		$maPassword = array();
		
		while($miSpecialChars > 0)
		{
			$maPassword[] = $maSpecialChars[rand(0,count($maSpecialChars)-1)];
			$miSpecialChars--;
		}
		while($miNumericChars > 0)
		{
			$maPassword[] = $maNumericChars[rand(0,count($maNumericChars)-1)];
			$miNumericChars--;
		}
		$miCharsLeft = $miCharNum - count($maPassword);
		if ($miCharsLeft < 4) { 
			$miCharsLeft = 4;
		}
		while($miCharsLeft > 0) {
			if ($miCaseChars > 0) {
				if ($miCharsLeft%2==0) {
					$maPassword[] = $maLowerCaseChars[rand(0,count($maLowerCaseChars)-1)];
				} else {
					$maPassword[] = $maUpperCaseChars[rand(0,count($maUpperCaseChars)-1)];
				}
			} else {
				$maPassword[] = $maLowerCaseChars[rand(0,count($maLowerCaseChars)-1)];
			}
			$miCharsLeft--;
		}
		shuffle($maPassword);
		$msGeneratedPassword = implode("",$maPassword);
		return $msGeneratedPassword;
  }
  
  /**
   * Retorna um array com a descri��o da pol�tica de senhas
   * 
   * <p>M�todo para retornar um array com a descri��o da pol�tica de senhas.</p>
   * @access public
   */
  public function getPolicyDescription() {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $miCharNum = ISMSLib::getConfigById(PASSWORD_CHAR_NUM);
    $miSpecialChars = ISMSLib::getConfigById(PASSWORD_SPECIAL_CHARS);
    $miNumericChars = ISMSLib::getConfigById(PASSWORD_NUMERIC_CHARS);
    $miCaseChars = ISMSLib::getConfigById(PASSWORD_CASE_CHARS);
    $maPolicy = array();
    $maPolicy["char_num_policy_description"]=$miCharNum;
    $maPolicy["special_chars_policy_description"]=$miSpecialChars;
    $maPolicy["numeric_chars_policy_description"]=$miNumericChars;
    $maPolicy["case_chars_policy_description"]=$miCaseChars;
    return $maPolicy;    
  }
  
  public function getPolicyDescriptionAsString(){
    $msDescription = FWDLanguage::getPHPStringValue('mx_password_policy',"<b>Pol�tica de Senhas:</b><br/>");
    $maPolicy = $this->getPolicyDescription();
    foreach($maPolicy as $msPolicy=>$miPolicyValue){
      if($miPolicyValue > 0){
        switch($msPolicy) {
          case "char_num_policy_description":
            $msPolicyName = FWDLanguage::getPHPStringValue('mx_minimum_chars','N�mero m�nimo de caracteres');
            break;
          case "special_chars_policy_description":
            $msPolicyName = FWDLanguage::getPHPStringValue('mx_minimum_special_chars','N�mero m�nimo de caracteres especiais');
            break;
          case "numeric_chars_policy_description":
            $msPolicyName = FWDLanguage::getPHPStringValue('mx_minimum_numeric_chars','N�mero m�nimo de caracteres num�ricos');
            break;
          case "case_chars_policy_description":
            $msPolicyName = FWDLanguage::getPHPStringValue('mx_upper_and_lower_case_chars','Caracteres mai�sculos e min�sculos');
            $miPolicyValue = $miPolicyValue==1?FWDLanguage::getPHPStringValue('st_yes','Sim'):FWDLanguage::getPHPStringValue('st_no','N�o');
            break;
          default:
            $msPolicyName = "";
            $miPolicyName = "";
            break;
        }
        $msDescription.= "<br>$msPolicyName: <b>$miPolicyValue</b>";
      }
    }
    return $msDescription;
  }
  
}
?>