<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSMailer.
 *
 * <p>Classe responsável pelo envio dos emails gerados pelo sistema.</p>
 * @package ISMS
 * @subpackage classes
 */
class ISMSMailer {

  protected $coMailer;
  
  protected $caContents;
  
  protected $caCurrentContent;
  
  protected $csFooterMessage;
  
  private $caParameters = array(
    'taskWithoutContext' => array('activity','sender','link','date'),
    'task' => array('activity','sender','contextType','name','description','link','date'),
    'association' => array('activity','sender','contextType1','name1','description1','contextType2','name2','description2','link','date'),
    'alert' => array('sender','description','date'),
    'alertWithJustification' => array('sender','description','justification','date'),
    'taskSubject' => array('activity','link'),
    'alertSubject' => array('description'),
    'taskHeader' => array(),
    'separator' => array(),
    'alertHeader' => array(),
    'newPassword' => array('new_password'),
    'taskAndAlertTransferHeader' => array(),
    'taskAndAlertTransfer' => array('user'),
    'controlImplementation' => array('control','date'),
    'disciplinaryProcess' => array('users','incident'),
    'incidentRiskParametrization' => array('asset','risk','original_parameters','new_parameters'),
    'newPasswordRequest' => array(),
    'abandonedSystem' => array('client_name'),
    'ismsFeedback' => array('user_name','feedback_type','feedback'),
    'ismsNewUser' => array('user_name', 'user_login', 'user_password', 'link'),
  );
  
  public function __construct(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
  
    $this->csFooterMessage = FWDLanguage::getPHPStringValue('em_to_execute_task_check_activity_list','<b>Para executar a tarefa acesse o sistema e verifique sua lista de atividades.</b><br>Para maiores informações sobre esta tarefa entre em contato com o security officer.');
    
    $msHost = ISMSLib::getConfigById(GENERAL_SMTP_SERVER);
    if (ISMSLib::getConfigById(EXTERNAL_SERVER) && $msHost != 'localhost' && $msHost != '127.0.0.1'){
    	$msUser = ISMSLib::getConfigById(EXTERNAL_MAIL_USER);
    	$msPassword = ISMSLib::getConfigById(EXTERNAL_MAIL_PASSWORD);
    	$miPort = ISMSLib::getConfigById(EXTERNAL_MAIL_PORT);
    	$msEnc = ISMSLib::getConfigById(EXTERNAL_MAIL_ENCRYPTION);
    	$this->coMailer = new FWDEmail(true, $msHost, $miPort, $msUser, $msPassword, $msEnc);
    } else {
    	$this->coMailer = new FWDEmail();
    }
    $this->newEmail();
  }

  protected function replaceParameters($psTemplate,$paParams, $pbHTML){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $msContent = $psTemplate;
    if(count($paParams)){
      $maSearch = array();
      $maReplace = array();
      foreach($paParams as $msName=>$msValue){
        $maSearch[] = "%$msName%";
        if ($pbHTML) {
          $maReplace[] = $msValue;
        }
        else {
          $msValue = str_ireplace("<br>", PHP_EOL, $msValue);
          $maReplace[] = strip_tags($msValue);
        }
      }
      $msContent = str_replace($maSearch,$maReplace,$msContent);
    }
    return $msContent;
  }

  public function newEmail(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $this->caContents = array();
    $this->caCurrentContent = null;
    $moConfig = new ISMSConfig();
    $msMailDefaultSender = $moConfig->getConfig(GENERAL_SMTP_DEFAULT_MAIL_SENDER);
    $this->coMailer->setFrom($msMailDefaultSender);
    
    $msMailDefaultSenderName = $moConfig->getConfig(GENERAL_SMTP_DEFAULT_MAIL_SENDER_NAME);
    if(!$msMailDefaultSenderName)
      $msMailDefaultSenderName = "noreply";
    $this->coMailer->setSenderName($msMailDefaultSenderName);
  }

  public function addContent($psType){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    if(isset($this->caParameters[$psType])){
      $this->caContents[] = array('contentType' => $psType);
      $this->caCurrentContent = &$this->caContents[count($this->caContents)-1];
    }else{
      trigger_error("Unknown content type ($psType).",E_USER_ERROR);
    }
  }

  public function setParameter($psName,$psValue){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    if(!$this->caCurrentContent){
      trigger_error("Trying to set parameter of null content.",E_USER_ERROR);
    }elseif(!in_array($psName,$this->caParameters[$this->caCurrentContent['contentType']])){
      trigger_error("The parameter '$psName' is not defined in type '{$this->caCurrentContent['contentType']}'.",E_USER_ERROR);
    }else{
      $this->caCurrentContent[$psName] = $psValue;
    }
  }
  
  public function setSubject($psSubject){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $this->coMailer->setSubject($psSubject);
  }
  
  public function setTo($psTo){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $this->coMailer->setTo($psTo);
  }
  
  public function setFrom($psFrom){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $this->coMailer->setFrom($psFrom);
  }
  
  public function setFooterMessage($psMsg){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $this->csFooterMessage = $psMsg;
  }
  
  public function setFormat($psFormat){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    if ($psFormat == 'html') $this->coMailer->setHtml(true);
    else if ($psFormat == 'text') $this->coMailer->setHtml(false);
    else trigger_error("Invalid email format '$psFormat'.",E_USER_WARNING);
  }
  
  public function send(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $mbHTML = $this->coMailer->isHtml();    
    
    $msBody = '';
    foreach($this->caContents as $maContent){
      $msType = $maContent['contentType'];
      $msBody.= $this->replaceParameters(ISMSEmailTemplates::getTemplate($msType, $mbHTML),$maContent, $mbHTML);
    }
    $maMainParams = array(
      'body' => $msBody,
      'date' => date('d/m/Y'),
      'time' => date('H:i'),
      'footer_message' => $this->csFooterMessage
    );
    $msMessage = $this->replaceParameters(ISMSEmailTemplates::getTemplate('main', $mbHTML),$maMainParams, $mbHTML);
    $this->coMailer->setMessage($msMessage);    
    ini_set('SMTP',ISMSLib::getConfigById(GENERAL_SMTP_SERVER));
    $this->coMailer->send();
  }
}
?>