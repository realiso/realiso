<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe RMProcess.
 *
 * <p>Classe que representa a tabela de processos.</p>
 * @package ISMS
 * @subpackage classes
 */
class RMProcess extends ISMSContext implements IWorkflow {

	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe RMProcess.</p>
	 * @access public
	 */
	public function __construct(){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		parent::__construct("rm_process");
		$this->ciContextType = CONTEXT_PROCESS;
		$this->csAliasId = "process_id";
		$this->csDependenceAliasId = "process_area_id";
		$this->cbHasDocument = true;
		$this->coWorkflow = new RMWorkflow(ACT_PROCESS_APPROVAL);

		$this->coDataset->addFWDDBField(new FWDDBField("fkContext",       "process_id",             DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkResponsible",   "process_responsible_id", DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkArea",          "process_area_id",        DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("sName",           "process_name",           DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("tDescription",    "process_description",    DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("sDocument",       "process_document",       DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("nValue",          "process_value",          DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("tJustification",  "process_justification",  DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("fkType",          "process_type",           DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkPriority",      "process_priority",       DB_NUMBER));

		$this->caSensitiveFields = array('process_name', 'process_area_id', 'process_responsible_id');
		$this->caSearchableFields = array('process_name', 'process_description', 'process_justification');
	}

	/**
	 * Retorna o id do usu�rio que deve aprovar o contexto.
	 *
	 * <p>M�todo para retornar id do usu�rio que deve aprovar o contexto.</p>
	 * @access public
	 * @return integer Id do aprovador
	 */
	public function getApprover() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		$moArea = new RMArea();
		$moArea->fetchById($this->getFieldValue('process_area_id'));
		return $moArea->getFieldValue('area_responsible_id');
	}

	/**
	 * Retorna o id do usu�rio respons�vel pelo contexto.
	 *
	 * <p>M�todo para retornar id do usu�rio respons�vel pelo contexto.</p>
	 * @access public
	 * @return integer Id do respons�vel
	 */
	public function getResponsible() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		return $this->getFieldValue('process_responsible_id');
	}

	/**
	 * Retorna o nome do contexto.
	 *
	 * <p>M�todo para retornar o nome do contexto.</p>
	 * @access public
	 * @return string Nome do contexto
	 */
	public function getName() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		return $this->getFieldValue('process_name');
	}

	/**
	 * Retorna a descri��o do contexto.
	 *
	 * <p>M�todo para retornar a descri��o do contexto.</p>
	 * @access public
	 * @return string Descri��o do contexto
	 */
	public function getDescription() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		return $this->getFieldValue('process_description');
	}

	/**
	 * Retorna os pais do subordinante do contexto.
	 *
	 * <p>M�todo para retornar os pais do subordinante do contexto..</p>
	 * @access public
	 * @param boolean $pbExecByTrash se o metodo est� sendo chamado pela lixeira
	 * @return array Array de ids dos pais do subordinante do contexto
	 */
	public function getProcessParents($pbExecByTrash = false) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		$moArea = new RMArea();
		$miParentId = $this->getFieldValue($this->csDependenceAliasId);
		$maParents = $moArea->getAreaParents($miParentId,$pbExecByTrash);
		$maParents[] = $miParentId;
		return $maParents;
	}

	/**
	 * Retorna os ancestrais do contexto.
	 *
	 * <p>M�todo para retornar todos contextos acima de um determinado contexto na
	 * �rvore de depend�ncias.</p>
	 * @access public
	 * @param integer $piContextId Id do contexto
	 * @param boolean $pbExecByTrash se o metodo est� sendo chamado pela lixeira
	 * @return array Array de ids dos ancestrais do contexto
	 * /
	 public function getSuperContexts($piContextId,$pbExecByTrash = false){
	 $maParameters = func_get_args();
	 FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
	 $miAreaId = $this->getFieldValue('process_area_id')->getValue();
	 if(!$miAreaId){
	 $moProcess = new RMProcess();
	 $moProcess->fetchById($piContextId);
	 $miAreaId = $moProcess->getFieldValue('process_area_id')->getValue();
	 }
	 $moArea = new RMArea();
	 $maSuperContexts = $moArea->getSuperContexts($miAreaId,$pbExecByTrash);
	 $maSuperContexts[] = $miAreaId;
	 return $maSuperContexts;
	 }*/

	/**
	 * Retorna o �cone do contexto.
	 *
	 * <p>M�todo para retornar o �cone do contexto.</p>
	 * @access public
	 * @param integer $piContextId Id do contexto
	 * @return string Nome do �cone
	 */
	public function getIcon($piContextId = 0){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		return 'icon-process_gray.gif';
	}

	/**
	 * Retorna o label do contexto.
	 *
	 * <p>M�todo para retornar o label do contexto.</p>
	 * @access public
	 * @return string Label do contexto
	 */
	public function getLabel() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		return FWDLanguage::getPHPStringValue('mx_process', "Processo");
	}

	/**
	 * Retorna o valor do contexto.
	 *
	 * <p>M�todo para retornar o valor do contexto.</p>
	 * @access public
	 * @return integer valor do contexto
	 */
	public function getValue(){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		return $this->getFieldValue('process_value');
	}


	/**
	 * Retorna o caminho do contexto para a cria��o do pathscroll.
	 *
	 * <p>M�todo para retornar o caminho do contexto para a cria��o do pathscroll.</p>
	 * @access public
	 * @param integer $piTab id da tab
	 * @param integer $piContextType id do contexto
	 * @param string $psContextId indices do filtro de ids dos links do pathscroling
	 * @return array Array contendo o Caminho do contexto
	 */
	public function getSystemPathScroll($piTab,$piContextType,$psContextId){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		$msHome = FWDLanguage::getPHPStringValue('st_home','Raiz');
		$maPath=array();
		$paContextId = explode(':',$psContextId);
		$piAreaId = 0;
		if((isset($paContextId[1]))&&($paContextId[1]))
		$piAreaId = $paContextId[1];
		if($piAreaId){
			$moArea = new RMArea();
			$moArea->fetchById($paContextId[1]);
			$maPath = $moArea->getSystemPathScroll(RISK_MANAGEMENT_MODE,CONTEXT_PROCESS,'');
		}else{
			$maPath[] = "<a href='javascript:isms_redirect_to_mode_scroll(".$piTab.", ".$piContextType.",0);'>";
			$maPath[] = $msHome;
			$maPath[] = ISMSLib::getIconCode('',-1);
			$maPath[]="</a> ";;
		}
		$maPath[] = "<a href='javascript:isms_redirect_to_mode_scroll(" . $piTab . "," . $piContextType . ",". $piAreaId .")\';'>";
		$maPath[] = $this->getName();
		$maPath[] = ISMSLib::getIconCode('icon-process_',$this->getValue());
		$maPath[]="</a> ";;
		return $maPath;
	}

	/**
	 * Retorna o caminho do contexto.
	 *
	 * <p>M�todo para retornar o caminho do contexto.</p>
	 * @access public
	 * @return string Caminho do contexto
	 */
	public function getSystemPath() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		$moArea = new RMArea();
		$moArea->fetchById($this->getFieldValue('process_area_id'));
		$msPath = $moArea->getSystemPath();
		$msPath .= "&nbsp;".ISMSLib::getIconCode('icon-arrow-right.gif',-2,0).'&nbsp;' . ISMSLib::getIconCode('icon-process_',$this->getValue(),-4) . " <a href='javascript:isms_redirect_to_mode(" . RISK_MANAGEMENT_MODE . "," . CONTEXT_ASSET . "," . $this->getFieldValue($this->getAliasId()) . ")'>" . $this->getName() . "</a>";
		return $msPath;
	}

	/**
	 * Retorna os leitores e o respons�vel do processo.
	 *
	 * <p>Retorna os leitores e o respons�vel do processo.</p>
	 * @access public
	 * @return array Array com leitores + respons�vel do processo.
	 */
	public function getReadersAndResponsible($piProcessId) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		$moProcess = new RMProcess();
		$moProcess->fetchById($piProcessId);
		$miResponsibleId = $moProcess->getResponsible();

		$moProcessUser = new PMProcessUser();
		$moProcessUser->createFilter($piProcessId, 'process_id');
		$moProcessUser->select();
		$maProcessUserIds = array();
		while ($moProcessUser->fetch()) $maProcessUserIds[] = $moProcessUser->getFieldValue('user_id');

		if (!in_array($miResponsibleId,$maProcessUserIds)) $maProcessUserIds[] = $miResponsibleId;
		return $maProcessUserIds;
	}

	/**
	 * Atualiza os leitores do documento do processo
	 *
	 * <p>M�todo para atualizar os leitores do documento do processo.</p>
	 * @access public
	 * @param integer $piProcessId Id do processo
	 */
	public function updateReaders($piProcessId) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		/*
		 * Busca usu�rios associados ao processo
		 */
		$moProcessUser = new PMProcessUser();
		$moProcessUser->createFilter($piProcessId, 'process_id');
		$moProcessUser->select();
		$maProcessUserIds = array();
		while ($moProcessUser->fetch()) $maProcessUserIds[] = $moProcessUser->getFieldValue('user_id');
		 
		/*
		 * Para cada documento associado ao processo, atualiza seus leitores.
		 */
		$moDocContext = new PMDocContext();
		$moDocContext->createFilter($piProcessId, 'context_id');
		$moDocContext->select();
		while($moDocContext->fetch()) {
			$miDocId = $moDocContext->getFieldValue('document_id');
			$moDocReaders = new PMDocumentReader();
			$moDocReaders->updateUsers($miDocId, $maProcessUserIds);
		}
		 
		/*
		 * Atualiza os leitores do documento da �rea (e �reas pais).
		 * Busca a �rea (e os pais dessa �rea, se existir) que cont�m o processo.
		 * Para cada �rea associada ao processo, atualiza seus leitores.
		 */
		$moProcess = new RMProcess();
		$moProcess->fetchById($piProcessId);
		$miAreaId = $moProcess->getFieldValue('process_area_id');
		$moArea = new RMArea();
		$maAreaIds = array_merge(array($miAreaId), $moArea->getAreaParents($miAreaId));
		foreach ($maAreaIds as $miAreaId) $moArea->updateReaders($miAreaId);

		/*
		 * Atualiza os leitores do documento dos ativos.
		 * Busca todos os ativos do processo.
		 * Para cada ativo associado ao processo, atualiza seus leitores.
		 */
		$moProcessAsset = new RMProcessAsset();
		$moAsset = new RMAsset();
		$maAssetIds = $moProcessAsset->getAssetsFromProcess($piProcessId);
		foreach ($maAssetIds as $miAssetId) $moAsset->updateReaders($miAssetId);
	}


	/**
	 * Retorna se o usu�rio logado tem permi��o para inserir o contexto.
	 *
	 * <p>M�todo para retornar se o usu�rio tem permi��o para inserir o contexto no sistema.</p>
	 * @access public
	 * @return boolean Se o usu�ro tem permi��o para inserir o contexto no sistema
	 */
	protected function userCanInsert(){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		$mbReturn = false;
		if(ISMSLib::userHasACL('M.RM.2.6')){
			$mbReturn = true;
		}
		return $mbReturn;
	}

	/**
	 * Retorna se o usu�rio logado tem permi��o para editar o contexto.
	 *
	 * <p>M�todo para retornar se o usu�rio tem permi��o para editar o contexto no sistema.</p>
	 * @access protected
	 * @return boolean Se o usu�ro tem permi��o de editar o contexto no sistema
	 */
	public function userCanEdit($piContextId,$piUserResponsible = 0){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		if(!$this->contextIsOfType($piContextId,$this->ciContextType)) return false;
		$mbReturn = false;
		$miResponsibleId = $piUserResponsible;
		if(!$miResponsibleId){
			$miResponsibleId = $this->getFieldValue('process_responsible_id');
		}
		if(ISMSLib::userHasACL('M.RM.2.4')){
			$mbReturn = true;
		}elseif( ISMSLib::userHasACL('M.RM.2.7') && $miResponsibleId == ISMSLib::getCurrentUserId() ){
			$mbReturn = true;
		}
		return $mbReturn;
	}

	/**
	 * Retorna se o usu�rio logado tem permi��o para excluir o contexto.
	 *
	 * <p>M�todo para retornar se o usu�rio tem permi��o para excluir o contexto no sistema.</p>
	 * @access protected
	 * @return boolean Se o usu�ro tem permi��o de excluir o contexto no sistema
	 */
	protected function userCanDelete($piContextId){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		if(!$this->contextIsOfType($piContextId,$this->ciContextType)) return false;
		$mbReturn = false;
		if(ISMSLib::userHasACL('M.RM.2.5')){
			$mbReturn = true;
		}else{
			$moContext = new RMProcess();
			$moContext->fetchById($piContextId);
			if( ISMSLib::userHasACL('M.RM.2.8') && $moContext->getFieldValue('process_responsible_id') == ISMSLib::getCurrentUserId() ){
				$mbReturn = true;
			}
		}
		return $mbReturn;
	}

	/**
	 * Retorna o c�digo HTML do icone do contexto.
	 *
	 * <p>M�todo para retornar o c�digo HTML do contexto</p>
	 * @access public
	 * @param integer $piId Id do contexto
	 * @param integer $pbIsFetched id do contexto
	 * @return string contendo o c�digo HTML do icone do contexto
	 */
	public function getIconCode($piId, $pbIsFetched = false){
		if($pbIsFetched){
			$this->fetchById($piId);
		}
		return ISMSLib::getIconCode('icon-process_'.RMRiskConfig::getRiskColor($this->getValue()).'.gif',-2, 7);
	}

	/**
	 * Retorna o caminho para abrir a popup de edi��o desse contexto.
	 *
	 * <p>M�todo para retornar o caminho para abrir a popup de edi��o desse contexto.</p>
	 * @param integer $piContextId id do contexto
	 * @access public
	 * @return string caminho para abrir a popup de edi��o desse contexto
	 */
	public function getVisualizeEditEvent($piContextId,$psUniqId){
		$soSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
		$msPackagesRiskPath = ($soSession->getMode()==MANAGER_MODE) ? "packages/risk/" : "../../packages/risk/";
		$msPopupId = "popup_process_edit";

		return "isms_open_popup('{$msPopupId}','{$msPackagesRiskPath}{$msPopupId}.php?process=$piContextId','','true',312,440);"
		."soPopUpManager.getPopUpById('{$msPopupId}').setCloseEvent( function close_visualize_$psUniqId() {soPopUpManager.closePopUp('popup_visualize_$psUniqId');} );";
	}

	/**
	 * Deleta logicamente um contexto.
	 *
	 * <p>M�todo para deletar logicamente um contexto. Deve ser sobrecarregada
	 * por contextos para os quais for necess�ria.</p>
	 * @access public
	 * @param integer $piContextId id do contexto
	 */
	public function logicalDelete($piContextId) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		$moRel = new RMProcessAsset();
		$moRel->createFilter($piContextId,'process_id');
		$moRel->select();
		while($moRel->fetch()){
			// deletar tarefas do contexto deletado
			$moTasks = new WKFTask();
			$moTasks->createFilter($moRel->getFieldValue('process_asset_id'), 'task_context_id');
			$moTasks->delete();
			// deletar alertas do contexto deletado
			$moAlerts = new WKFAlert();
			$moAlerts->createFilter($moRel->getFieldValue('process_asset_id'), 'alert_context_id');
			$moAlerts->delete();
		}

		if(ISMSLib::getConfigById(GENERAL_CASCADE_ON)){
			$nonConformities = new CINonConformityProcess();
			$nonConformities->createFilter($piContextId, 'process_id');
			$nonConformities->select();
			while($nonConformities->fetch()){
				$nonConformity = new CINonConformity();
				$nonConformity->delete($nonConformities->getFieldValue("nc_id"));
			}
		}
	}
}
?>