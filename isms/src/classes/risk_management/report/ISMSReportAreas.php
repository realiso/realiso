<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportAreas.
 *
 * <p>Classe que implementa o relat�rio de �reas.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportAreas extends ISMSFilteredReport {
  
  public function __construct(){
    parent::__construct();
  }
  
 /**
  * Inicializa o relat�rio.
  *
  * <p>M�todo para inicializar o relat�rio.
  * Necess�rio para funcionar com o XML compilado.</p>
  * @access public
  */
  public function init(){
    parent::init();
    $this->coDataSet->addFWDDBField(new FWDDBField('a.fkContext','area_id'         ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('a.sName'    ,'area_name'       ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('u.sName'    ,'area_responsible',DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('a.nValue'   ,'area_value'      ,DB_STRING));
  }
  
 /**
  * Executa a query do relat�rio.
  *
  * <p>M�todo para executar a query do relat�rio.</p>
  * @access public
  */
  public function makeQuery(){
    $msValue = $this->coFilter->getValue();
    
    $maFilters = array();
    
    if($msValue){
      $miRiskLow = ISMSLib::getConfigById(RISK_LOW);
      $miRiskHigh = ISMSLib::getConfigById(RISK_HIGH);
      switch($msValue){
        case 'high':{
          $maFilters[] = "a.nValue >= {$miRiskHigh}";
          break;
        }
        case 'mid':{
          $maFilters[] = "a.nValue > {$miRiskLow}";
          $maFilters[] = "a.nValue < {$miRiskHigh}";
          break;
        }
        case 'low':{
          $maFilters[] = "a.nValue <= {$miRiskLow}";
          $maFilters[] = "a.nValue != 0";
          break;
        }
        case 'np':{
          $maFilters[] = "a.nValue = 0";
          break;
        }
      }
    }
    
    if(count($maFilters)==0){
      $msWhere = '';
    }else{
      $msWhere = ' WHERE '.implode(' AND ',$maFilters);
    }
    $this->csQuery = "SELECT
                        a.fkContext AS area_id,
                        a.sName AS area_name,
                        u.sName AS area_responsible,
                        a.nValue AS area_value
                      FROM
                        view_rm_area_active a
                        JOIN isms_user u ON (u.fkContext = a.fkResponsible)
                      {$msWhere}
                      ORDER BY a.sName";
    return parent::executeQuery();
  }

}

?>