<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

define('RISK_VALUE_TYPE_REAL_VALUE', 7101);
define('RISK_VALUE_TYPE_RESIDUAL_VALUE', 7102);

/**
 * Classe ISMSReportRiskValuesFilter.
 *
 * <p>Classe que implementa o filtro do relat�rio de valores dos riscos.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportRiskValuesFilter extends FWDReportFilter {

  protected $ciValueType = 0;
  protected $cbShowHighRisk = false;
  protected $cbShowMediumRisk = false;
  protected $cbShowLowRisk = false;
  protected $cbShowRiskWithNoValues = false;
  protected $caRiskClassifType = array();

  protected $cbShowMidHighRisk = false;
  protected $cbShowMidLowRisk = false;

  public function setRiskValueType($piRiskValueType) {
    $this->ciValueType = $piRiskValueType;
  }
  public function getRiskValueType() {
    return $this->ciValueType;
  }
  public function showHighRisk() {
    $this->cbShowHighRisk = true;
  }
  public function showMediumRisk() {
    $this->cbShowMediumRisk = true;
  }
  public function showLowRisk() {
    $this->cbShowLowRisk = true;
  }
  public function showRiskWithNoValues() {
    $this->cbShowRiskWithNoValues = true;
  }



  public function showMidLowRisk(){
    $this->cbShowMidLowRisk = true;
  }
  public function showMidHighRisk() {
    $this->cbShowMidHighRisk = true;
  }
  public function mustShowMidHighRisk(){
    return $this->cbShowMidHighRisk;
  }
  public function mustShowMidLowRisk(){
    return $this->cbShowMidLowRisk;
  }



  public function mustShowHighRisk() {
    return $this->cbShowHighRisk;
  }
  public function mustShowMediumRisk() {
    return $this->cbShowMediumRisk;
  }
  public function mustShowLowRisk() {
    return $this->cbShowLowRisk;
  }
  public function mustShowRiskWithNoValues() {
    return $this->cbShowRiskWithNoValues;
  }
  public function getRiskClassifType(){
    return $this->caRiskClassifType;
  }

  public function setRiskClassifType($paRiskClassifType){
    $this->caRiskClassifType = $paRiskClassifType;
  }
  public function getSummary(){
    $maFilterColor = array();
    if($this->ciValueType == RISK_VALUE_TYPE_REAL_VALUE){
      $maFilterColor[] = FWDLanguage::getPHPStringValue('rs_real_risk', 'Risco Potencial');
    }else{
      $maFilterColor[] = FWDLanguage::getPHPStringValue('rs_residual_risk', 'Risco Residual');
    }

    $maFilterValues = array();
    if($this->mustShowMidHighRisk()){
      $maFilterValues[] = FWDLanguage::getPHPStringValue('rs_very_high', 'Muito Alto');
    }
    if($this->mustShowHighRisk()){
      $maFilterValues[] = FWDLanguage::getPHPStringValue('rs_high', 'Alto');
    }
    if($this->mustShowMediumRisk()){
      $maFilterValues[] = FWDLanguage::getPHPStringValue('rs_medium', 'M�dio');
    }
    if($this->mustShowLowRisk()){
      $maFilterValues[] = FWDLanguage::getPHPStringValue('rs_low', 'Baixo');
    }
    if($this->mustShowMidLowRisk()){
      $maFilterValues[] = FWDLanguage::getPHPStringValue('rs_very_low', 'Muito Baixo');
    }
    if($this->mustShowRiskWithNoValues()){
      $maFilterValues[] = FWDLanguage::getPHPStringValue('rs_not_parameterized', 'N�o Estimados');
    }

    $maFilters = array();
    $maFilters[] = array(
      'name' => FWDLanguage::getPHPStringValue('rs_risk_color_based_on_cl', 'Cor do risco baseada no:'),
      'items' => $maFilterColor
    );
    $maFilters[] = array(
      'name' => FWDLanguage::getPHPStringValue('rs_risks_values_cl', 'Valores dos Riscos:'),
      'items' => $maFilterValues
    );

    $maRiskClassifType = $this->getRiskClassifType();

    if(count($maRiskClassifType)){
      $maFilters[] = array(
        'name' => FWDLanguage::getPHPStringValue('rs_risk_types_cl', 'Tipos de Risco:'),
        'items' => $maRiskClassifType
      );
    }

    return $maFilters;
  }
}
?>