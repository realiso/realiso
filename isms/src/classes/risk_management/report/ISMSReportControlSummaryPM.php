<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe ISMSReportControlSummary.
 *
 * <p>Classe que implementa o relat�rio de resumo de controles.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportControlSummaryPM extends ISMSReport {
	
	/**
	 * Inicializa o relat�rio.
	 *
	 * <p>M�todo para inicializar o relat�rio.
	 * Necess�rio para funcionar com o XML compilado.</p>
	 * @access public
	 */	
	public function init() {
		parent::init();

		$col1 = new FWDDBField("","control_id",DB_NUMBER);
		$col1->setLabel(' ');
		$col1->setShowColumn(false);
		
		$col2 = new FWDDBField("","control_name",DB_STRING);
		$col2->setLabel(FWDLanguage::getPHPStringValue('cb_control','Controle'));
		$col2->setShowColumn(true);		

		$col3 = new FWDDBField("","control_is_active",DB_NUMBER);
		$col3->setLabel(' ');
		$col3->setShowColumn(false);
		
		$col4 = new FWDDBField("","control_date_implemented",DB_DATETIME);
		$col4->setLabel(FWDLanguage::getPHPStringValue('gc_realization_date','Data Realiza��o'));
		$col4->setShowColumn(true);		
		
		$col5 = new FWDDBField("","control_description",DB_STRING);
		$col5->setLabel(FWDLanguage::getPHPStringValue('rs_description','Descri��o'));
		$col5->setShowColumn(true);
		
		$col6 = new FWDDBField("","context_id",DB_NUMBER);
		$col6->setLabel(' ');
		$col6->setShowColumn(false);		
		
		$col7 = new FWDDBField("","context_name",DB_STRING);
		$col7->setLabel(' ');
		$col7->setShowColumn(true);
		
		$col8 = new FWDDBField("","row_type",DB_STRING);
		$col8->setLabel(FWDLanguage::getPHPStringValue('gc_type','Tipo'));
		$col8->setShowColumn(true);			
		
		$this->coDataSet->addFWDDBField($col1);
		$this->coDataSet->addFWDDBField($col2);		
		$this->coDataSet->addFWDDBField($col3);
		$this->coDataSet->addFWDDBField($col4);
		$this->coDataSet->addFWDDBField($col5);
		$this->coDataSet->addFWDDBField($col6);
		$this->coDataSet->addFWDDBField($col7);
		$this->coDataSet->addFWDDBField($col8);
		
	}
	
	/**
	 * Executa a query do relat�rio.
	 *
	 * <p>M�todo para executar a query do relat�rio.</p>
	 * @access public
	 */
	public function makeQuery() {
    $moFilter = $this->getFilter();
    $maFilters = array();
    $msWhere ="";
    $msWhereDoc = "";
    $maClassifType = $moFilter->getControlClassifType();
    
    if(count($maClassifType)){
      if(isset($maClassifType['null'])){
        $maFilters[] = " c.fkType IS NULL ";
      }else{
        $maFilters[] = " c.fkType IN ( ".implode(',',array_keys($maClassifType)) ." ) ";
      }
    }

    if(count($maFilters)){
      $msWhere .= " AND ". implode(' AND ',$maFilters);
      $msWhereDoc .= " WHERE ". implode(' AND ',$maFilters);
    }

  
		$this->csQuery ="
SELECT res.control_id as control_id,
       res.control_name as control_name,
       res.control_is_active as control_is_active,
       res.control_date_implemented as control_date_implemented,
       res.control_description as control_description,
       res.context_id as context_id,
       res.context_name as context_name,
       res.row_type as row_type
FROM (
  SELECT c.fkContext as control_id,
         c.sName as control_name,
         c.bIsActive as control_is_active,
         c.dDateImplemented as control_date_implemented,
         CAST(c.tDescription AS varchar(250)) as control_description,
         bp.fkContext as context_id,
         bp.sName as context_name,
         'best_practice' as row_type
    FROM view_rm_control_active c
      LEFT JOIN rm_control_best_practice cbp ON (c.fkContext = cbp.fkControl)
      LEFT JOIN view_rm_best_practice_active bp ON (cbp.fkBestPractice = bp.fkContext)
    WHERE cbp.fkBestPractice IN (SELECT bp.fkContext FROM view_rm_best_practice_active bp)
          $msWhere
UNION 
  SELECT c.fkContext as control_id,
         c.sName as control_name,
         c.bIsActive as control_is_active,
         c.dDateImplemented as control_date_implemented,
         CAST(c.tDescription AS varchar(250)) as control_description,
         d.fkContext as context_id,
         d.sName as context_name,
         'document' as row_type
    FROM view_rm_control_active c
      JOIN view_pm_doc_context_active dc ON (dc.fkContext = c.fkContext)
      JOIN view_pm_published_docs d ON (d.fkContext = dc.fkDocument)
      $msWhereDoc
) res

ORDER BY res.control_is_active, res.control_id, res.row_type, res.context_name
";
		
		return parent::executeQuery();
	}
  
  /**
   * Desenha o cabe�alho do relat�rio.
   *
   * <p>M�todo para desenhar o cabe�alho do relat�rio.</p>
   * @access public
   */
  public function drawHeader(){
    foreach($this->caHeaders as $moHeader){
      if($moHeader->getAttrName()=='header_filter'){
        $moHeaderFilter = $moHeader;
      }elseif($moHeader->getAttrName()=='header_filter_item'){
        $moHeaderFilterItem = $moHeader;
      }elseif($moHeader->getAttrName()=='report_header_blank'){
        $moHeaderBlank = $moHeader;
      }else{
        $this->coWriter->drawLine($moHeader,array());
      }
    }
    if ($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());
    
    $moFilterText = FWDWebLib::getObject('header_filter_text');
    $moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
    $maFilters = $this->coFilter->getSummary();
    foreach($maFilters as $maFilter){
      $moFilterText->setValue($maFilter['name']);
      $this->coWriter->drawLine($moHeaderFilter,array());
      foreach($maFilter['items'] as $msFilterItem){
        $moFilterItemText->setValue($msFilterItem);
        $this->coWriter->drawLine($moHeaderFilterItem,array());
      }
    }
    $this->coWriter->drawLine($moHeaderBlank,array());
  }
  
}
?>