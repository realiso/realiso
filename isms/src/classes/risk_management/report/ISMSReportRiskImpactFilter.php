<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportRiskImpactFilter.
 *
 * <p>Classe que implementa o filtro do relat�rio de impacto dos riscos.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportRiskImpactFilter extends FWDReportFilter {
  
  protected $caControlClassifType = array();
  
  protected $caParameters = array();
  
  protected $caRiskClassifType = array();
  
  public function setParameters($paParameters) {
    $this->caParameters = $paParameters;
  }
	
	public function getParameters() {
		return $this->caParameters;
	}


  public function getRiskClassifType(){
    return $this->caRiskClassifType;
  }

  public function setRiskClassifType($paRiskClassifType){
    $this->caRiskClassifType = $paRiskClassifType;
  }


	public function getSummary(){		
    $maRiskClassifType = $this->getRiskClassifType();
    if(count($maRiskClassifType)){
      $maFilters[] = array(
        'name' => FWDLanguage::getPHPStringValue('lb_risk_types_cl', 'Tipos de Risco:'),
        'items' => $maRiskClassifType
      );
    }

    $maFilterParameter = array();
		foreach ($this->caParameters as $miParameter) {
			$moParameterName = new RMParameterName();
			$moParameterName->fetchById($miParameter);
			$maFilterParameter[] = $moParameterName->getFieldValue('parametername_name');
		}

    $maFilters[] = array(
      'name' => FWDLanguage::getPHPStringValue('lb_considered_parameters_cl', 'Par�metros Considerados:'),
      'items' => $maFilterParameter
    );
        
    return $maFilters;
  }
}
?>