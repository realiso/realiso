<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportAssetRelevance.
 *
 * <p>Classe que implementa o relat�rio que mostra a relev�ncia dos ativos.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportAssetRelevance extends ISMSReport {
  
 /**
  * Inicializa o relat�rio.
  *
  * <p>M�todo para inicializar o relat�rio.
  * Necess�rio para funcionar com o XML compilado.</p>
  * @access public
  */
  public function init(){
    parent::init();
    
	$col1 = new FWDDBField('a.fkcontext',       'asset_id',           DB_NUMBER);
	$col1->setLabel(' ');
	$col1->setShowColumn(false);
	
	$col2 = new FWDDBField('a.sName',           'asset_name',         DB_STRING);
	$col2->setLabel(FWDLanguage::getPHPStringValue('rs_asset','Ativo'));
	$col2->setShowColumn(true);		

	$col3 = new FWDDBField('a.nValue',          'asset_value',        DB_NUMBER);
	$col3->setLabel(' ');
	$col3->setShowColumn(false);
	
	$col4 = new FWDDBField('u.sName',           'asset_responsible',  DB_STRING);
	$col4->setLabel(FWDLanguage::getPHPStringValue('rs_responsible','Respons�vel'));
	$col4->setShowColumn(true);		
	
	$col5 = new FWDDBField('pn.sName',          'asset_parameter',    DB_STRING);
	$col5->setLabel(FWDLanguage::getPHPStringValue('si_asset_relevance','Relev�ncia do Ativo'));
	$col5->setShowColumn(true);
	
	$col6 = new FWDDBField('vn.sImportance',    'asset_importance',   DB_STRING);
	$col6->setLabel(FWDLanguage::getPHPStringValue('st_importance','Import�ncia do Ativo'));
	$col6->setShowColumn(true);

	$col7 = new FWDDBField('vn.nValue', 'asset_importance_value', DB_STRING);
	$col7->setLabel(FWDLanguage::getPHPStringValue('rs_importance_value','Valor da Import�ncia do Ativo'));
	$col7->setShowColumn(true);
	
	$col8 = new FWDDBField('', 'asset_sum_importance_value', DB_STRING);
	$col8->setLabel(' ');
	$col8->setShowColumn(false);
	
    
    $this->coDataSet->addFWDDBField($col1);
    $this->coDataSet->addFWDDBField($col2);
    $this->coDataSet->addFWDDBField($col3);
    $this->coDataSet->addFWDDBField($col4);
    $this->coDataSet->addFWDDBField($col5);
    $this->coDataSet->addFWDDBField($col6);
    $this->coDataSet->addFWDDBField($col7);
    $this->coDataSet->addFWDDBField($col8);
  }
  
 /**
  * Executa a query do relat�rio.
  *
  * <p>M�todo para executar a query do relat�rio.</p>
  * @access public
  */
  public function makeQuery(){    
    $this->csQuery = "SELECT
                      	a.fkContext as asset_id,
                      	a.sName as asset_name,
                      	a.nValue as asset_value,
                      	u.sName as asset_responsible,
                      	pn.sName as asset_parameter,
                      	vn.sImportance as asset_importance,
                      	vn.nValue as asset_importance_value,
                      	(SELECT SUM(vn2.nValue)
                      		FROM view_rm_asset_active a2
                      			LEFT JOIN rm_asset_value av2 ON (a2.fkContext = av2.fkAsset)
                      			LEFT JOIN rm_parameter_value_name vn2 ON (av2.fkValueName = vn2.pkValueName)
                      			WHERE a.fkContext = a2.fkContext
                      			GROUP BY  a.sName) as asset_sum_importance_value
                      FROM
                      	view_rm_asset_active a
                      	LEFT JOIN view_isms_user_active u ON (a.fkResponsible = u.fkContext)
                      	LEFT JOIN rm_asset_value av ON (a.fkContext = av.fkAsset)
                      	LEFT JOIN rm_parameter_name pn ON (av.fkParameterName = pn.pkParameterName)
                      	LEFT JOIN rm_parameter_value_name vn ON (av.fkValueName = vn.pkValueName)
                      ORDER BY
                      	a.sName, a.fkContext, pn.pkParametername";
   	//echo $this->csQuery;
    return parent::executeQuery();
  }
}
?>