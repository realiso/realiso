<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

define('REPORT_DEPENDENCY_TYPE', 64001);
define('REPORT_DEPENDENT_TYPE', 64002);

/**
 * Classe ISMSReportAssetDependenciesAndDependents.
 *
 * <p>Classe que implementa o relat�rio de depend�ncias e dependentes de ativos.</p>
 * @package ISMS
 * @subpackage report
 */

include_once $handlers_ref . "QueryCacheAllAssetDependencies.php";
include_once $handlers_ref . "QueryCacheAllAssetDependents.php";

class ISMSReportAssetDependenciesAndDependents extends ISMSReport {

  /**
   * Inicializa o relat�rio.
   *
   * <p>M�todo para inicializar o relat�rio.
   * Necess�rio para funcionar com o XML compilado.</p>
   * @access public
   */
  public function init() {
    parent::init();

	$col1 = new FWDDBField("","asset_id",DB_NUMBER);
	$col1->setLabel(' ');
	$col1->setShowColumn(false);
	
	$col2 = new FWDDBField("","asset_name",DB_STRING);
	$col2->setLabel(FWDLanguage::getPHPStringValue('rs_asset','Ativo'));
	$col2->setShowColumn(true);		

	$col3 = new FWDDBField("","asset_value",DB_NUMBER);
	$col3->setLabel(' ');
	$col3->setShowColumn(false);
	
	$col4 = new FWDDBField("","asset_responsible",DB_STRING);
	$col4->setLabel(FWDLanguage::getPHPStringValue('rs_responsible','Respons�vel'));
	$col4->setShowColumn(true);		

	$col5 = new FWDDBField("","dep_id",DB_NUMBER);
	$col5->setLabel(' ');
	$col5->setShowColumn(false);
	
	$col6 = new FWDDBField("","dep_name",DB_STRING);
	$col6->setLabel(' ');
	$col6->setShowColumn(false);     
    
	$col7 = new FWDDBField("","dep_value",DB_NUMBER);
	$col7->setLabel(' ');
	$col7->setShowColumn(false);   

	$col8 = new FWDDBField("","dep_responsible",DB_STRING);
	$col8->setLabel(FWDLanguage::getPHPStringValue('rs_responsible','Respons�vel') . '(' . FWDLanguage::getPHPStringValue('si_dependents','Dependentes') . ')');
	$col8->setShowColumn(true);   

	$col9 = new FWDDBField("","dep_type",DB_NUMBER);
	$col9->setLabel('64001=' . FWDLanguage::getPHPStringValue('si_dependencies','Depend�ncias') . ',' . '64002=' . FWDLanguage::getPHPStringValue('si_dependents','Dependentes'));
	$col9->setShowColumn(true); 
    
    $this->coDataSet->addFWDDBField($col1);
    $this->coDataSet->addFWDDBField($col2);
    $this->coDataSet->addFWDDBField($col3);
    $this->coDataSet->addFWDDBField($col4);
    $this->coDataSet->addFWDDBField($col5);
    $this->coDataSet->addFWDDBField($col6);
    $this->coDataSet->addFWDDBField($col7);
    $this->coDataSet->addFWDDBField($col8);
    $this->coDataSet->addFWDDBField($col9);
  }

  /**
   * Executa a query do relat�rio.
   *
   * <p>M�todo para executar a query do relat�rio.</p>
   * @access public
   */
  public function makeQuery() {

    $queryCacheAllAssetDependencies = new QueryCacheAllAssetDependencies();
    $queryCacheAllAssetDependencies->cache();

    $queryCacheAllAssetDependents = new QueryCacheAllAssetDependents();
    $queryCacheAllAssetDependents->cache();
     
    $moFilter = $this->getFilter();
    $maFilters = array();

    $msWhereDependency = '';
    $msWhereDependent = '';
    $miAssetId = $moFilter->getAssetId();
    if ($miAssetId) {
      $msWhereDependency = 'WHERE a.fkContext = ' . $miAssetId;
      $msWhereDependent = 'WHERE a2.fkContext = ' . $miAssetId;
    }

    $this->csQuery ="SELECT
                        a.fkContext as asset_id,
                        a.sName as asset_name,
                        a.nValue as asset_value,
                        aresp.sName as asset_responsible,
                        dep.fkContext as dep_id,
                        dep.sName as dep_name,
                        dep.nValue as dep_value,
                        depresp.sName as dep_responsible,
                        ".REPORT_DEPENDENCY_TYPE." as dep_type
                      FROM
                        view_rm_asset_active a
                        JOIN view_isms_user_active aresp ON (a.fkResponsible = aresp.fkContext)
                        JOIN all_asset_dependencies_cache depfunc ON (a.fkContext = depfunc.asset)
                        JOIN view_rm_asset_active dep ON (depfunc.dependency = dep.fkContext)
                        JOIN view_isms_user_active depresp ON (dep.fkResponsible = depresp.fkContext)
                        $msWhereDependency

                      UNION

                      SELECT
                        a2.fkContext as asset_id,
                        a2.sName as asset_name,
                        a2.nValue as asset_value,
                        aresp2.sName as asset_responsible,
                        dep2.fkContext as dep_id,
                        dep2.sName as dep_name,
                        dep2.nValue as dep_value,
                        depresp2.sName as dep_responsible,
                        ".REPORT_DEPENDENT_TYPE." as dep_type
                      FROM
                        view_rm_asset_active a2
                        JOIN view_isms_user_active aresp2 ON (a2.fkResponsible = aresp2.fkContext)
                        JOIN all_asset_dependents_cache depfunc2 ON (a2.fkContext = depfunc2.asset)
                        JOIN view_rm_asset_active dep2 ON (depfunc2.dependent = dep2.fkContext)
                        JOIN view_isms_user_active depresp2 ON (dep2.fkResponsible = depresp2.fkContext)
                        $msWhereDependent

                      ORDER BY
                        asset_id, dep_type";
                        return parent::executeQuery();
  }
}
?>