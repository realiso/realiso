<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe ISMSReportControlsByAsset.
 *
 * <p>Classe que implementa o relat�rio de controles por Ativo.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportControlsByAsset extends ISMSReport {
	
	/**
	 * Inicializa o relat�rio.
	 *
	 * <p>M�todo para inicializar o relat�rio.
	 * Necess�rio para funcionar com o XML compilado.</p>
	 * @access public
	 */	
	public function init() {
		parent::init();

		$col1 = new FWDDBField("","asset_id",DB_NUMBER);
		$col1->setLabel(' ');
		$col1->setShowColumn(false);

		$col2 = new FWDDBField("","asset_name",DB_STRING);
		$col2->setLabel(FWDLanguage::getPHPStringValue('rs_asset','Ativo'));
		$col2->setShowColumn(true);

		$col3 = new FWDDBField("","control_id",DB_NUMBER);
		$col3->setLabel(' ');
		$col3->setShowColumn(false);		

		$col4 = new FWDDBField("","control_name",DB_STRING);
		$col4->setLabel(FWDLanguage::getPHPStringValue('rs_controls','Controles'));
		$col4->setShowColumn(true);		

		$col5 = new FWDDBField("","control_is_active",DB_NUMBER);
		$col5->setLabel(' ');
		$col5->setShowColumn(false);			

		$this->coDataSet->addFWDDBField($col1);		
		$this->coDataSet->addFWDDBField($col2);
		$this->coDataSet->addFWDDBField($col3);
		$this->coDataSet->addFWDDBField($col4);
		$this->coDataSet->addFWDDBField($col5);		
	}
	
	/**
	 * Executa a query do relat�rio.
	 *
	 * <p>M�todo para executar a query do relat�rio.</p>
	 * @access public
	 */
	public function makeQuery() {
		$this->csQuery = "SELECT a.fkContext AS asset_id, a.sName AS asset_name, ctrl.fkContext AS control_id, ctrl.sName AS control_name, ctrl.bIsActive AS control_is_active
							FROM 
								isms_context c
								JOIN rm_control ctrl ON (ctrl.fkContext = c.pkContext AND c.nState != 2705) 
								JOIN context_history ch ON (ch.context_id = c.pkContext) 
								JOIN view_rm_risk_control_active rc ON (rc.fkControl = ctrl.fkContext) 
								LEFT JOIN rm_risk r ON (r.fkContext = rc.fkRisk)
								LEFT JOIN rm_asset a ON (a.fkContext = r.fkAsset)
								GROUP BY ctrl.fkContext, ctrl.sName, a.fkContext, a.sName, ctrl.bIsActive
								ORDER BY a.sName, ctrl.sName";
		return parent::executeQuery();
	}
  
  	/**
	 * Desenha o cabe�alho do relat�rio.
	 *
	 * <p>M�todo para desenhar o cabe�alho do relat�rio.</p>
	 * @access public
	 */
	public function drawHeader(){
		foreach($this->caHeaders as $moHeader){
			if($moHeader->getAttrName()=='header_filter'){
				$moHeaderFilter = $moHeader;
			}elseif($moHeader->getAttrName()=='header_filter_item'){
				$moHeaderFilterItem = $moHeader;
			}elseif($moHeader->getAttrName()=='report_header_blank'){
				$moHeaderBlank = $moHeader;
			}else{
				$this->coWriter->drawLine($moHeader,array());
			}
		}
		
		if ($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());
    
	    $moFilterText = FWDWebLib::getObject('header_filter_text');
	    $moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
	    $maFilters = $this->coFilter->getSummary();
	    foreach($maFilters as $maFilter){
	    	$moFilterText->setValue($maFilter['name']);
	    	$this->coWriter->drawLine($moHeaderFilter,array());
	    	foreach($maFilter['items'] as $msFilterItem){
	    		$moFilterItemText->setValue($msFilterItem);
	    		$this->coWriter->drawLine($moHeaderFilterItem,array());
	    	}
	    }
    	$this->coWriter->drawLine($moHeaderBlank,array());
  	}
}
?>