<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe ISMSReportControlsNotMeasured.
 *
 * <p>Classe que implementa o relat�rio de controles n�o medidos.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportControlsNotMeasured extends ISMSReport {
	
	/**
	 * Inicializa o relat�rio.
	 *
	 * <p>M�todo para inicializar o relat�rio.
	 * Necess�rio para funcionar com o XML compilado.</p>
	 * @access public
	 */	
	public function init() {
		parent::init();
		
		$col1 = new FWDDBField("","control_id",DB_NUMBER);
		$col1->setLabel(' ');
		$col1->setShowColumn(false);
		
		$col2 = new FWDDBField("","control_name",DB_STRING);
		$col2->setLabel(FWDLanguage::getPHPStringValue('cb_control','Controle'));
		$col2->setShowColumn(true);		
	
		$col3 = new FWDDBField("","control_is_active",DB_NUMBER);
		$col3->setLabel(' ');
		$col3->setShowColumn(false);
		
		$col4 = new FWDDBField("","control_efficiency_limit_date",DB_DATETIME);
		$col4->setLabel(FWDLanguage::getPHPStringValue('rs_efficiency','Efici�cia'));
		$col4->setShowColumn(true);		
		
		$col5 = new FWDDBField("","control_test_limit_date",DB_DATETIME);
		$col5->setLabel(FWDLanguage::getPHPStringValue('cb_test','Teste'));
		$col5->setShowColumn(true);
		
		$this->coDataSet->addFWDDBField($col1);
		$this->coDataSet->addFWDDBField($col2);
		$this->coDataSet->addFWDDBField($col3);
		$this->coDataSet->addFWDDBField($col4);
		$this->coDataSet->addFWDDBField($col5);
					
	}
	
	/**
	 * Executa a query do relat�rio.
	 *
	 * <p>M�todo para executar a query do relat�rio.</p>
	 * @access public
	 */
	public function makeQuery() {		
		$msCurrentDate = ISMSLib::getTimestampFormat(ISMSLib::ISMSTime());
    
    $moFilter = $this->getFilter();
    $maFilters = array();
    $msWhere ="";
    $msWhereAux = "";
    
    $maClassifType = $moFilter->getControlClassifType();
    
    if(count($maClassifType)){
      if(isset($maClassifType['null'])){
        $maFilters[] = " c.fkType IS NULL ";
      }else{
        $maFilters[] = " c.fkType IN ( ".implode(',',array_keys($maClassifType)) ." ) ";
      }
    }
    if(count($maFilters)){
      $msWhereAux .= " AND ". implode(' AND ',$maFilters);
    }

    $this->csQuery = "  SELECT c.fkContext as control_id,
                               c.sName as control_name,
                               c.bIsActive as control_is_active,
                               ces.dDateLimit as control_efficiency_limit_date,
                               cts.dDateLimit as control_test_limit_date
                          FROM rm_control c
                               JOIN isms_context ctx ON (c.fkContext = ctx.pkContext AND ctx.nState != 2705)
                               LEFT JOIN wkf_control_efficiency wce ON (c.fkContext = wce.fkControlEfficiency)
                               LEFT JOIN wkf_schedule ces ON (ces.pkSchedule = wce.fkSchedule AND ces.dDateLimit <= $msCurrentDate )
                               LEFT JOIN wkf_control_test wct ON (c.fkContext = wct.fkControlTest)
                               LEFT JOIN wkf_schedule cts ON (cts.pkSchedule = wct.fkSchedule AND cts.dDateLimit <= $msCurrentDate )
                               LEFT JOIN wkf_task_schedule wts ON (c.fkContext = wts.fkContext)
                          WHERE c.dDateImplemented IS NOT NULL
                          		AND ((wts.nactivity = 2208 AND (c.bRevisionIsLate = 1 OR wce.fkControlEfficiency IS NULL)) 
										OR (wts.nactivity = 2216 AND (c.bTestIsLate = 1 OR wct.fkControlTest IS NULL))
									)
    
                          $msWhereAux
                          ORDER BY c.sName";
    
   	//echo "<pre>{$this->csQuery}</pre>";
                          
    return parent::executeQuery();
  }
  
  /**
   * Desenha o cabe�alho do relat�rio.
   *
   * <p>M�todo para desenhar o cabe�alho do relat�rio.</p>
   * @access public
   */
  public function drawHeader(){
    foreach($this->caHeaders as $moHeader){
      if($moHeader->getAttrName()=='header_filter'){
        $moHeaderFilter = $moHeader;
      }elseif($moHeader->getAttrName()=='header_filter_item'){
        $moHeaderFilterItem = $moHeader;
      }elseif($moHeader->getAttrName()=='report_header_blank'){
        $moHeaderBlank = $moHeader;
      }else{
        $this->coWriter->drawLine($moHeader,array());
      }
    }
    if ($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());
    
    $moFilterText = FWDWebLib::getObject('header_filter_text');
    $moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
    $maFilters = $this->coFilter->getSummary();
    foreach($maFilters as $maFilter){
      $moFilterText->setValue($maFilter['name']);
      $this->coWriter->drawLine($moHeaderFilter,array());
      foreach($maFilter['items'] as $msFilterItem){
        $moFilterItemText->setValue($msFilterItem);
        $this->coWriter->drawLine($moHeaderFilterItem,array());
      }
    }
    $this->coWriter->drawLine($moHeaderBlank,array());
  }
}
?>