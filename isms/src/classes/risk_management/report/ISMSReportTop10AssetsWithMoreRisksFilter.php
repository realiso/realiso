<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportTop10AssetsWithMoreRisksFilter.
 *
 * <p>Classe que implementa o filtro do relatório de top 10 ativos com mais riscos.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportTop10AssetsWithMoreRisksFilter extends FWDReportFilter {
  
  protected $cbOnlyByHighRisks = false;
  
  protected $caRiskClassifType = array();
  
  public function filterOnlyByHighRisks() {
    return $this->cbOnlyByHighRisks = true;
  }
  
  public function mustFilterOnlyByHighRisks() {
    return $this->cbOnlyByHighRisks;
  }
  
  public function getRiskClassifType(){
    return $this->caRiskClassifType;
  }

  public function setRiskClassifType($paRiskClassifType){
    $this->caRiskClassifType = $paRiskClassifType;
  }
  
  public function getSummary(){
    $maGets = array();
    $maGets[] = "RiskClassifType";
    $maLabels ["RiskClassifType"] = FWDLanguage::getPHPStringValue('lb_risk_types', "Tipos de Risco");
    $maFilters = array();

    foreach($maGets as $msGet){
      $maCtxClassif = $this->{"get".$msGet}();
      if(count($maCtxClassif)){
        $maFilters[] = array(
          'name' => $maLabels[$msGet],
          'items' => $maCtxClassif
        );
      }
    }
    return $maFilters;
  }
  
}
?>