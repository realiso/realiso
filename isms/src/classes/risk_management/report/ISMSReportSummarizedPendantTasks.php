<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportSummarizedPendantTasks.
 *
 * <p>Classe que implementa o relat�rio resumido de tarefas pendentes.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportSummarizedPendantTasks extends ISMSReport {

	/**
	 * Inicializa o relat�rio.
	 *
	 * <p>M�todo para inicializar o relat�rio.
	 * Necess�rio para funcionar com o XML compilado.</p>
	 * @access public
	 */
	public function init() {
		parent::init();

		$col1 = new FWDDBField("","user_id",DB_NUMBER);
		$col1->setLabel(' ');
		$col1->setShowColumn(false);
		
		$col2 = new FWDDBField("","user_name",DB_STRING);
		$col2->setLabel(FWDLanguage::getPHPStringValue('rs_responsible','Respons�vel'));
		$col2->setShowColumn(true);		
	
		$col3 = new FWDDBField("","task_activity",DB_NUMBER);
		$col3->setLabel(FWDLanguage::getPHPStringValue('rs_tasks','Tarefas'));
		$col3->setShowColumn(true);
		
		$col4 = new FWDDBField("","task_count",DB_NUMBER);
		$col4->setLabel(FWDLanguage::getPHPStringValue('rs_amount','Quantidade'));
		$col4->setShowColumn(true);
		
		$this->coDataSet->addFWDDBField($col1);
		$this->coDataSet->addFWDDBField($col2);
		$this->coDataSet->addFWDDBField($col3);
		$this->coDataSet->addFWDDBField($col4);
		
	}

	/**
	 * Executa a query do relat�rio.
	 *
	 * <p>M�todo para executar a query do relat�rio.</p>
	 * @access public
	 */
	public function makeQuery() {
		$moFilter = $this->getFilter();

		$queryFilter = array();

		// filtro tipo area
		$maClassifFilter = "";
		$maClassifFilter = $moFilter->getAreaClassifType();
		if(count($maClassifFilter)){
			if(isset($maClassifFilter['null'])){
				$queryFilter[] = " a.fkType IS NULL ";
			}else{
				$queryFilter[] = " a.fkType IN ( ". implode(',',array_keys($maClassifFilter)) ." ) ";
			}
		}

		// filtro tipo processo
		$maClassifFilter = "";
		$maClassifFilter = $moFilter->getProcessClassifType();
		if(count($maClassifFilter)){
			if(isset($maClassifFilter['null'])){
				$queryFilter[] = " p.fkType IS NULL ";
			}else{
				$queryFilter[] = " p.fkType IN ( ". implode(',',array_keys($maClassifFilter)) ." ) ";
			}
		}

		// filtro tipo risco
		$maClassifFilter = "";
		$maClassifFilter = $moFilter->getRiskClassifType();
		if(count($maClassifFilter)){
			if(isset($maClassifFilter['null'])){
				$queryFilter[] = " r.fkType IS NULL ";
			}else{
				$queryFilter[] = " r.fkType IN ( ". implode(',',array_keys($maClassifFilter)) ." ) ";
			}
		}

		// filtro tipo controle
		$maClassifFilter = "";
		$maClassifFilter = $moFilter->getControlClassifType();
		if(count($maClassifFilter)){
			if(isset($maClassifFilter['null'])){
				$queryFilter[] = " c.fkType IS NULL ";
			}else{
				$queryFilter[] = " c.fkType IN ( ". implode(',',array_keys($maClassifFilter)) ." ) ";
			}
		}

		// filtro tipo evento
		$msClassifFilter = "";
		$maClassifFilter = $moFilter->getEventClassifType();
		if(count($maClassifFilter)){
			if(isset($maClassifFilter['null'])){
				$queryFilter[] = " e.fkType IS NULL ";
			}else{
				$queryFilter[] = " e.fkType IN ( ". implode(',',array_keys($maClassifFilter)) ." ) ";
			}
		}

		// filtro prioridade area
		$maClassifFilter = "";
		$maClassifFilter = $moFilter->getAreaClassifPriority();
		if(count($maClassifFilter)){
			if(isset($maClassifFilter['null'])){
				$queryFilter[] = " a.fkPriority IS NULL ";
			}else{
				$queryFilter[] = " a.fkPriority IN ( ".implode(',',array_keys($maClassifFilter)) ." ) ";
			}
		}
	  
		// filtro prioridade processo
		$maClassifFilter = "";
		$maClassifFilter = $moFilter->getProcessClassifPriority();
		if(count($maClassifFilter)){
			if(isset($maClassifFilter['null'])){
				$queryFilter[] = " p.fkPriority IS NULL ";
			}else{
				$queryFilter[] = " p.fkPriority IN ( ".implode(',',array_keys($maClassifFilter)) ." ) ";
			}
		}

		$sfilter = "";
		$ljoins = "";

		if(count($queryFilter) > 0){
			$ljoins = " LEFT JOIN rm_area a ON (wt.fkContext = a.fkContext)
						LEFT JOIN rm_process p ON (wt.fkContext = p.fkContext)
						LEFT JOIN rm_risk r ON (wt.fkContext = r.fkContext)
						LEFT JOIN rm_control c ON (wt.fkContext = c.fkContext)
						LEFT JOIN rm_event e ON (wt.fkContext = e.fkContext) "; 

			$sfilter = "AND " . implode(' AND ', $queryFilter);
		}

		$moConfig = new ISMSConfig();
		$mbHasRevision = $moConfig->getConfig(GENERAL_REVISION_ENABLED);
		$mbHasTests = $moConfig->getConfig(GENERAL_TEST_ENABLED);
		$msWhere = ' WHERE wt.nActivity >= 2201 AND wt.nActivity <= 2220 ';
		$maFilterTypes = array();
		//retirado escopo e pol�tica dos relat�rios de task pendentes
		$maFilterTypes[]='2218';
		$maFilterTypes[]='2219';
	  
		if(!$mbHasRevision){
			$maFilterTypes[]= '2208';
		}
		if(!$mbHasTests){
			$maFilterTypes[] = '2216';
		}

		if(count($maFilterTypes)){
			$msWhere .= "AND wt.nActivity NOT IN (".implode(',',$maFilterTypes).") ";
		}
	  
		$this->csQuery = "SELECT u.fkContext as user_id, u.sName as user_name,
												wt.nActivity as task_activity, count(wt.nActivity) as task_count
											FROM view_isms_user_active u
											JOIN wkf_task wt ON (u.fkContext = wt.fkReceiver AND wt.bVisible = 1)
											$ljoins
											$msWhere $sfilter
	                      GROUP BY u.fkContext, u.sName, wt.nActivity
	                      ORDER BY u.fkContext";

											return parent::executeQuery();
	}

	/**
	 * Desenha o cabe�alho do relat�rio.
	 *
	 * <p>M�todo para desenhar o cabe�alho do relat�rio.</p>
	 * @access public
	 */
	public function drawHeader(){

		foreach($this->caHeaders as $moHeader){
			if($moHeader->getAttrName()=='header_filter'){
				$moHeaderFilter = $moHeader;
			}elseif($moHeader->getAttrName()=='header_filter_item'){
				$moHeaderFilterItem = $moHeader;
			}elseif($moHeader->getAttrName()=='report_header_blank'){
				$moHeaderBlank = $moHeader;
			}else{
				$this->coWriter->drawLine($moHeader,array());
			}
		}

		if ($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());

		$moFilterText = FWDWebLib::getObject('header_filter_text');
		$moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
		$maFilters = $this->coFilter->getSummary();

		foreach($maFilters as $maFilter){
			$moFilterText->setValue($maFilter['name']);
			$this->coWriter->drawLine($moHeaderFilter,array());
			foreach($maFilter['items'] as $msFilterItem){
				$moFilterItemText->setValue($msFilterItem);
				$this->coWriter->drawLine($moHeaderFilterItem,array());
			}
		}

		$this->coWriter->drawLine($moHeaderBlank,array());
	}
}
?>