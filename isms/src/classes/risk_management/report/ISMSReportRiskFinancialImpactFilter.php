<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportRiskFinancialImpactFilter.
 *
 * <p>Classe que implementa o filtro do relat�rio de impacto financeiro dos riscos.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportRiskFinancialImpactFilter extends FWDReportFilter {

  // constantes de tipo de tratamento
  const REDUCED     = 1;
  const TREATED     = 2;
  const NOT_TREATED = RISK_ACCEPT_MODE_NONE;
  const ACCEPTED    = RISK_ACCEPT_MODE_ACCEPT;
  const TRANSFERRED = RISK_ACCEPT_MODE_TRANSFER;
  const AVOIDED     = RISK_ACCEPT_MODE_AVOID;
  
  // constantes de valor de risco
  const LOW    = 1;
  const MIDDLE = 2;
  const HIGH   = 4;
  
  protected $ciTreatment = 0;

  protected $ciValues = 0;
  
  protected $cbNotTreatedOrAccepted = false;

  public function getNotTreatedOrAccepted(){
    return $this->cbNotTreatedOrAccepted;
  }

  public function setNotTreatedOrAccepted($pbNotTreatedOrAccepted){
    $this->cbNotTreatedOrAccepted = $pbNotTreatedOrAccepted;
  }

  public function setTreatment($piTreatment){
    $this->ciTreatment = $piTreatment;
  }

  public function getTreatment(){
    return $this->ciTreatment;
  }

  public function setValues($piValues){
    $this->ciValues = $piValues;
  }

  public function getValues(){
    return $this->ciValues;
  }
  
  public static function getTreatmentName($piTreatment){
    switch($piTreatment){
      case ISMSReportRiskFinancialImpactFilter::REDUCED:{
        return FWDLanguage::getPHPStringValue('mx_reduced',"Reduzido");
      }
      case ISMSReportRiskFinancialImpactFilter::TREATED:{
        return FWDLanguage::getPHPStringValue('mx_treated',"Tratado");
      }
      case ISMSReportRiskFinancialImpactFilter::NOT_TREATED:{
        return FWDLanguage::getPHPStringValue('mx_not_treated',"N�o Tratado");
      }
      case ISMSReportRiskFinancialImpactFilter::ACCEPTED:{
        return FWDLanguage::getPHPStringValue('mx_accepted',"Aceito");
      }
      case ISMSReportRiskFinancialImpactFilter::TRANSFERRED:{
        return FWDLanguage::getPHPStringValue('mx_transferred',"Transferido");
      }
      case ISMSReportRiskFinancialImpactFilter::AVOIDED:{
        return FWDLanguage::getPHPStringValue('mx_avoided',"Evitado");
      }
      default:{
        trigger_error("Invalid treatment code ($piTreatment).",E_USER_ERROR);
      }
    }
  }
  
  public function getSummary(){
    
    $maFilters = array();
    
    if($this->ciTreatment){
      $maFilters[] = array(
        'name' => FWDLanguage::getPHPStringValue('rs_treatment_cl','Tratamento:'),
        'items' => array(self::getTreatmentName($this->ciTreatment))
      );
    }
    
    if($this->ciValues){
      $maFilter = array(
        'name' => FWDLanguage::getPHPStringValue('rs_value_cl','Valores:'),
        'items' => array()
      );
      if($this->ciValues & self::LOW){
        $maFilter['items'][] = FWDLanguage::getPHPStringValue('rs_low',"Baixo");
      }
      if($this->ciValues & self::MIDDLE){
        $maFilter['items'][] = FWDLanguage::getPHPStringValue('rs_medium',"M�dio");
      }
      if($this->ciValues & self::HIGH){
        $maFilter['items'][] = FWDLanguage::getPHPStringValue('rs_high',"Alto");
      }
      if(count($maFilter['items']) < 3){
        $maFilters[] = $maFilter;
      }
    }
    
    if($this->cbNotTreatedOrAccepted){
      $maFilters[] = array(
        'name' => FWDLanguage::getPHPStringValue('rs_show_only_cl','Exibir apenas:'),
        'items' => array(FWDLanguage::getPHPStringValue('rs_not_treated_or_accepted',"N�o tratados ou aceitos"))
      );
    }
    
    return $maFilters;
  }

}

?>