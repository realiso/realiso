<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportParametersFilter.
 *
 * <p>Classe que implementa o filtro do relatório de impacto de risco / importância do ativo.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportParametersFilter extends FWDReportFilter {
  
  protected $caParameters = array();
  
  public function setParameters($paParameters) {
    $this->caParameters = $paParameters;
  }
  
  public function getParameters() {
    return $this->caParameters;
  }
  
  public function getSummary(){
    $maFilterParameter = array();
    foreach ($this->caParameters as $miParameter) {
      $moParameterName = new RMParameterName();
      $moParameterName->fetchById($miParameter);
      $maFilterParameter[] = $moParameterName->getFieldValue('parametername_name');
    }

    $maFilter = array(array(
      'name' => FWDLanguage::getPHPStringValue('rs_considered_parameters_cl', 'Parâmetros Considerados:'),
      'items' => $maFilterParameter
    ));
        
    return $maFilter;
  }
}
?>