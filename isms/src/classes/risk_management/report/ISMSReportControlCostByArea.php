<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe ISMSReportControlCostByArea.
 *
 * <p>Classe que implementa o relat�rio de custo dos controles por �rea.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportControlCostByArea extends ISMSReport {
  
  /**
   * Inicializa o relat�rio.
   *
   * <p>M�todo para inicializar o relat�rio.
   * Necess�rio para funcionar com o XML compilado.</p>
   * @access public
   */  
  public function init() {
    parent::init();
    
 	$col1 = new FWDDBField("","area_id",DB_NUMBER);
	$col1->setLabel(' ');
	$col1->setShowColumn(false);
	
	$col2 = new FWDDBField("","area_name",DB_STRING);
	$col2->setLabel(FWDLanguage::getPHPStringValue('cb_area','�rea'));
	$col2->setShowColumn(true);		

	$col3 = new FWDDBField("","area_value",DB_NUMBER);
	$col3->setLabel(' ');
	$col3->setShowColumn(false);
	
	$col4 = new FWDDBField("","control_id",DB_NUMBER);
	$col4->setLabel(' ');
	$col4->setShowColumn(false);		
	
	$col5 = new FWDDBField("","control_name",DB_STRING);
	$col5->setLabel(FWDLanguage::getPHPStringValue('cb_control','Controle'));
	$col5->setShowColumn(true);
	
	$col6 = new FWDDBField("","control_is_active",DB_NUMBER);
	$col6->setLabel(' ');
	$col6->setShowColumn(false);		
	
	$col7 = new FWDDBField("","control_cost",DB_NUMBER);
	$col7->setLabel(FWDLanguage::getPHPStringValue('gc_cost','Custo'));
	$col7->setShowColumn(true);
	
    $this->coDataSet->addFWDDBField($col1);    
    $this->coDataSet->addFWDDBField($col2);
    $this->coDataSet->addFWDDBField($col3);
    $this->coDataSet->addFWDDBField($col4);
    $this->coDataSet->addFWDDBField($col5);
    $this->coDataSet->addFWDDBField($col6);        
    $this->coDataSet->addFWDDBField($col7);
  }
  
  /**
   * Executa a query do relat�rio.
   *
   * <p>M�todo para executar a query do relat�rio.</p>
   * @access public
   */
  public function makeQuery() {
    $moFilter = $this->getFilter();
    $maFilters = array();
    $msWhere ="";

    $maClassifType = $moFilter->getControlClassifType();
    if(count($maClassifType)){
      if(isset($maClassifType['null'])){
        $maFilters[] = " c.fkType IS NULL ";
      }else{
        $maFilters[] = " c.fkType IN ( ".implode(',',array_keys($maClassifType)) ." ) ";
      }
    }

    $maClassifAreaType = $moFilter->getAreaClassifType();
    if(count($maClassifAreaType)){
      if(isset($maClassifAreaType['null'])){
        $maFilters[] = " ar.fkType IS NULL ";
      }else{
        $maFilters[] = " ar.fkType IN ( ".implode(',',array_keys($maClassifAreaType)) ." ) ";
      }
    }

    $maClassifAreaPriority = $moFilter->getAreaClassifPriority();
    if(count($maClassifAreaPriority)){
      if(isset($maClassifAreaPriority['null'])){
        $maFilters[] = " ar.fkPriority IS NULL ";
      }else{
        $maFilters[] = " ar.fkPriority IN ( ".implode(',',array_keys($maClassifAreaPriority)) ." ) ";
      }
    }

    if(count($maFilters)){
        $msWhere .= " WHERE ". implode(' AND ',$maFilters);
    }

    $this->csQuery = "SELECT ar.fkContext as area_id, ar.sName as area_name, ar.nValue as area_value,
                        c.fkContext as control_id, c.sName as control_name, c.bIsActive as control_is_active,
                        (cc.nCost1+cc.nCost2+cc.nCost3+cc.nCost4+cc.nCost5) as control_cost
                      FROM view_rm_area_active ar
                      JOIN view_rm_process_active p ON (ar.fkContext = p.fkArea)
                      JOIN view_rm_process_asset_active pa ON (p.fkContext = pa.fkProcess)
                      JOIN view_rm_asset_active a ON (pa.fkAsset = a.fkContext)
                      JOIN view_rm_risk_active r ON (a.fkContext = r.fkAsset)
                      JOIN view_rm_risk_control_active rc ON (r.fkContext = rc.fkRisk)
                      JOIN view_rm_control_active c ON (rc.fkControl = c.fkContext)
                      LEFT JOIN rm_control_cost cc ON (c.fkContext = cc.fkControl)
                      $msWhere
                      GROUP BY ar.fkContext, ar.sName, ar.nValue, c.fkContext, c.sName, c.bIsActive, (cc.nCost1+cc.nCost2+cc.nCost3+cc.nCost4+cc.nCost5)
                      ORDER BY ar.fkContext";
    
    return parent::executeQuery();
  }
  
  /**
   * Desenha o cabe�alho do relat�rio.
   *
   * <p>M�todo para desenhar o cabe�alho do relat�rio.</p>
   * @access public
   */
  public function drawHeader(){
    foreach($this->caHeaders as $moHeader){
      if($moHeader->getAttrName()=='header_filter'){
        $moHeaderFilter = $moHeader;
      }elseif($moHeader->getAttrName()=='header_filter_item'){
        $moHeaderFilterItem = $moHeader;
      }elseif($moHeader->getAttrName()=='report_header_blank'){
        $moHeaderBlank = $moHeader;
      }else{
        $this->coWriter->drawLine($moHeader,array());
      }
    }
    if ($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());
    
    $moFilterText = FWDWebLib::getObject('header_filter_text');
    $moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
    $maFilters = $this->coFilter->getSummary();
    foreach($maFilters as $maFilter){
      $moFilterText->setValue($maFilter['name']);
      $this->coWriter->drawLine($moHeaderFilter,array());
      foreach($maFilter['items'] as $msFilterItem){
        $moFilterItemText->setValue($msFilterItem);
        $this->coWriter->drawLine($moHeaderFilterItem,array());
      }
    }
    $this->coWriter->drawLine($moHeaderBlank,array());
  }

}
?>