<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe ISMSReportRiskStatusByArea.
 *
 * <p>Classe que implementa o relat�rio de status do risco por �rea.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportRiskStatusByArea extends ISMSReport {
  
  /**
   * Inicializa o relat�rio.
   *
   * <p>M�todo para inicializar o relat�rio.
   * Necess�rio para funcionar com o XML compilado.</p>
   * @access public
   */  
  public function init() {
    parent::init();

	$col1 = new FWDDBField("","area_id",DB_NUMBER);
	$col1->setLabel(' ');
	$col1->setShowColumn(false);
	
	$col2 = new FWDDBField("","area_name",DB_STRING);
	$col2->setLabel(FWDLanguage::getPHPStringValue('cb_area','�rea'));
	$col2->setShowColumn(true);		

	$col3 = new FWDDBField("","area_value",DB_NUMBER);
	$col3->setLabel(' ');
	$col3->setShowColumn(false);
	
	$col4 = new FWDDBField("","area_responsible_id",DB_NUMBER);
	$col4->setLabel(' ');
	$col4->setShowColumn(false);	  
    
	$col5 = new FWDDBField("","area_responsible_name",DB_STRING);
	$col5->setLabel(FWDLanguage::getPHPStringValue('rs_responsible','Respons�vel'));
	$col5->setShowColumn(true);	
	
    $this->coDataSet->addFWDDBField($col1);
    $this->coDataSet->addFWDDBField($col2);
    $this->coDataSet->addFWDDBField($col3);
    $this->coDataSet->addFWDDBField($col4);
    $this->coDataSet->addFWDDBField($col5);    
  }
  
  /**
   * Executa a query do relat�rio.
   *
   * <p>M�todo para executar a query do relat�rio.</p>
   * @access public
   */
  public function makeQuery() {
  
    $moFilter = $this->getFilter();
    $maFilters = array();
    $msWhere ="";

    $maClassifType = $moFilter->getAreaClassifType();
    if(count($maClassifType)){
      if(isset($maClassifType['null'])){
        $maFilters[] = " ar.fkType IS NULL ";
      }else{
        $maFilters[] = " ar.fkType IN ( ".implode(',',array_keys($maClassifType)) ." ) ";
      }
    }
    
    $maClassifPriority = $moFilter->getAreaClassifPriority();
    if(count($maClassifPriority)){
      if(isset($maClassifPriority['null'])){
        $maFilters[] = " ar.fkPriority IS NULL ";
      }else{
        $maFilters[] = " ar.fkPriority IN ( ".implode(',',array_keys($maClassifPriority)) ." ) ";
      }
    }

    if(count($maFilters)){
        $msWhere .= " WHERE ". implode(' AND ',$maFilters);
    }
    $this->csQuery = "SELECT ar.fkContext as area_id, ar.sName as area_name, ar.nValue as area_value, u.fkContext as area_responsible_id, u.sName as area_responsible_name
                      FROM view_rm_area_active ar
                      JOIN view_isms_user_active u ON (ar.fkResponsible = u.fkContext)
                      $msWhere
                      ORDER BY ar.nValue DESC";
    
    return parent::executeQuery();
  }
  
  /**
   * Desenha o cabe�alho do relat�rio.
   *
   * <p>M�todo para desenhar o cabe�alho do relat�rio.</p>
   * @access public
   */
  public function drawHeader(){
    foreach($this->caHeaders as $moHeader){
      if($moHeader->getAttrName()=='header_filter'){
        $moHeaderFilter = $moHeader;
      }elseif($moHeader->getAttrName()=='header_filter_item'){
        $moHeaderFilterItem = $moHeader;
      }elseif($moHeader->getAttrName()=='report_header_blank'){
        $moHeaderBlank = $moHeader;
      }else{
        $this->coWriter->drawLine($moHeader,array());
      }
    }
    if ($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());
    
    $moFilterText = FWDWebLib::getObject('header_filter_text');
    $moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
    $maFilters = $this->coFilter->getSummary();
    foreach($maFilters as $maFilter){
      $moFilterText->setValue($maFilter['name']);
      $this->coWriter->drawLine($moHeaderFilter,array());
      foreach($maFilter['items'] as $msFilterItem){
        $moFilterItemText->setValue($msFilterItem);
        $this->coWriter->drawLine($moHeaderFilterItem,array());
      }
    }
    $this->coWriter->drawLine($moHeaderBlank,array());
  }
}
?>