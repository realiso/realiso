<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportRisksByAsset.
 *
 * <p>Classe que implementa o relat�rio de riscos por ativo.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportRisksByAsset extends ISMSReport {

	/**
	 * Inicializa o relat�rio.
	 *
	 * <p>M�todo para inicializar o relat�rio.
	 * Necess�rio para funcionar com o XML compilado.</p>
	 * @access public
	 */
	public function init() {
		parent::init();

		$col1 = new FWDDBField("","asset_id",DB_NUMBER);
		$col1->setLabel(' ');
		$col1->setShowColumn(false);

		$col2 = new FWDDBField("","asset_name",DB_STRING);
		$col2->setLabel(FWDLanguage::getPHPStringValue('cb_asset','Ativo'));
		$col2->setShowColumn(true);

		$col3 = new FWDDBField("","asset_value",DB_NUMBER);
		$col3->setLabel(' ');
		$col3->setShowColumn(false);

		$col4 = new FWDDBField("","risk_id",DB_NUMBER);
		$col4->setLabel(' ');
		$col4->setShowColumn(false);

		$col5 = new FWDDBField("","risk_name",DB_STRING);
		$col5->setLabel(FWDLanguage::getPHPStringValue('rs_risk_and_impact','Risco / Impacto'));
		$col5->setShowColumn(true);

		$col6 = new FWDDBField("","risk_impact",DB_STRING);
		$col6->setLabel(' ');
		$col6->setShowColumn(true);

		$col7 = new FWDDBField("","risk_value",DB_NUMBER);
		$col7->setLabel(FWDLanguage::getPHPStringValue('rs_real_risk','Risco Potencial'));
		$col7->setShowColumn(true);

		$col8 = new FWDDBField("","risk_residual_value",DB_NUMBER);
		$col8->setLabel(FWDLanguage::getPHPStringValue('rs_residual_risk','Risco Residual'));
		$col8->setShowColumn(true);

		$col9 = new FWDDBField("","risk_accept_mode",DB_NUMBER);
		$col9->setLabel(' ');
		$col9->setShowColumn(false);

		$this->coDataSet->addFWDDBField($col1);
		$this->coDataSet->addFWDDBField($col2);
		$this->coDataSet->addFWDDBField($col3);
		$this->coDataSet->addFWDDBField($col4);
		$this->coDataSet->addFWDDBField($col5);
    	$this->coDataSet->addFWDDBField($col6);
		$this->coDataSet->addFWDDBField($col7);
		$this->coDataSet->addFWDDBField($col8);
		$this->coDataSet->addFWDDBField($col9);
	}

	/**
	 * Executa a query do relat�rio.
	 *
	 * <p>M�todo para executar a query do relat�rio.</p>
	 * @access public
	 */
	public function makeQuery() {
		$moFilter = $this->getFilter();
		$maFilters = array();

		$miLowRisk = ISMSLib::getConfigById(RISK_LOW);
		$miHighRisk = ISMSLib::getConfigById(RISK_HIGH);

		$msRiskType = ($moFilter->getRiskValueType() == RISK_VALUE_TYPE_REAL_VALUE) ? 'r.nValue' : 'r.nValueResidual';

		if(ISMSConfig::getConfigFromDB(RISK_LEVEL)==5){
      $miMidLowRisk = ISMSLib::getConfigById(RISK_MID_LOW);
      $miMidHighRisk = ISMSLib::getConfigById(RISK_MID_HIGH);

      if($moFilter->mustShowMidHighRisk()) {
        $maFilters[] = '(' . $msRiskType . ' >= ' . $miHighRisk . ')';
      }
      if($moFilter->mustShowHighRisk()) {
        $maFilters[] = '(' . $msRiskType . ' > ' . $miMidHighRisk . ' AND ' . $msRiskType . ' < ' . $miHighRisk . ')';
      }
      if($moFilter->mustShowMediumRisk()) {
        $maFilters[] = '(' . $msRiskType . ' > ' . $miMidLowRisk . ' AND ' . $msRiskType . ' <= ' . $miMidHighRisk . ')';
      }
      if($moFilter->mustShowLowRisk()) {
        $maFilters[] = '(' . $msRiskType . ' > ' . $miLowRisk . ' AND ' . $msRiskType . ' <= ' . $miMidLowRisk . ')';
      }
      if($moFilter->mustShowMidLowRisk()) {
        $maFilters[] = '(' . $msRiskType . '<=' . $miLowRisk . ' AND ' . $msRiskType . ' != 0)';
      }
      if ($moFilter->mustShowRiskWithNoValues()) {
        $maFilters[] = '(' . $msRiskType . ' = 0' . ')';
      }
    }else{
      if ($moFilter->mustShowHighRisk()) {
        $maFilters[] = '(' . $msRiskType . ' >= ' . $miHighRisk . ')';
      }
      if ($moFilter->mustShowMediumRisk()) {
        $maFilters[] = '(' . $msRiskType . ' > ' . $miLowRisk . ' AND ' . $msRiskType . ' < ' . $miHighRisk . ')';
      }
      if ($moFilter->mustShowLowRisk()) {
        $maFilters[] = '(' . $msRiskType . ' <= ' . $miLowRisk . ' AND ' . $msRiskType . ' != 0)';
      }
      if ($moFilter->mustShowRiskWithNoValues()) {
        $maFilters[] = '(' . $msRiskType . ' = 0' . ')';
      }
    }



		$miCategoryId = $moFilter->getCategory();
		if ($miCategoryId) $msWhere = 'WHERE (' . implode(' OR ', $maFilters) . ') AND a.fkCategory = ' . $miCategoryId;
		else $msWhere = 'WHERE' . implode(' OR ', $maFilters);




    $msClassifFilter = "";
    $maClassifFilter = $moFilter->getRiskClassifType();
    if(count($maClassifFilter)){
      if(isset($maClassifFilter['null'])){
        $msClassifFilter = " AND  r.fkType IS NULL ";
      }else{
        $msClassifFilter = " AND  r.fkType IN ( ". implode(',',array_keys($moFilter->getRiskClassifType())) ." ) ";
      }
    }
		if($msClassifFilter){
      if ($miCategoryId){
        $msWhere = 'WHERE (' . implode(' OR ', $maFilters) . ') AND a.fkCategory = ' . $miCategoryId ." ".$msClassifFilter;
      }
      else{
        $msWhere = 'WHERE ( ' . implode(' OR ', $maFilters). ' ) ' . $msClassifFilter;
      }
		}else{
      if ($miCategoryId){
        $msWhere = 'WHERE (' . implode(' OR ', $maFilters) . ') AND a.fkCategory = ' . $miCategoryId;
      }
      else{
        $msWhere = 'WHERE' . implode(' OR ', $maFilters);
      }
    }

		$this->csQuery = "SELECT a.fkContext as asset_id, a.sName as asset_name, a.nValue as asset_value,
												r.fkContext as risk_id, r.sName as risk_name, r.tImpact as risk_impact, r.nValue as risk_value, r.nValueResidual as risk_residual_value, r.nAcceptMode as risk_accept_mode
											FROM view_rm_asset_active a
                      JOIN view_rm_risk_active r ON (a.fkContext = r.fkAsset)
											$msWhere
											ORDER BY a.nValue, a.fkContext, r.nValueResidual, r.fkcontext";

		return parent::executeQuery();
	}

	/**
	 * Desenha o cabe�alho do relat�rio.
	 *
	 * <p>M�todo para desenhar o cabe�alho do relat�rio.</p>
	 * @access public
	 */
	public function drawHeader(){
    foreach($this->caHeaders as $moHeader){
      if($moHeader->getAttrName()=='header_filter'){
        $moHeaderFilter = $moHeader;
      }elseif($moHeader->getAttrName()=='header_filter_item'){
        $moHeaderFilterItem = $moHeader;
      }elseif($moHeader->getAttrName()=='report_header_blank'){
        $moHeaderBlank = $moHeader;
      }else{
        $this->coWriter->drawLine($moHeader,array());
      }
    }
    if ($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());

    $moFilterText = FWDWebLib::getObject('header_filter_text');
    $moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
    $maFilters = $this->coFilter->getSummary();
    foreach($maFilters as $maFilter){
      $moFilterText->setValue($maFilter['name']);
      $this->coWriter->drawLine($moHeaderFilter,array());
      foreach($maFilter['items'] as $msFilterItem){
        $moFilterItemText->setValue($msFilterItem);
        $this->coWriter->drawLine($moHeaderFilterItem,array());
      }
    }
    $this->coWriter->drawLine($moHeaderBlank,array());
  }
}
?>