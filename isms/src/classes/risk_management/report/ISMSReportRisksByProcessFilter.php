<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportRisksByProcessFilter.
 *
 * <p>Classe que implementa o filtro do relatório de riscos por processo.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportRisksByProcessFilter extends ISMSReportRiskValuesFilter {
  
  protected $ciProcessId = 0;
  
  protected $cbOrganizeByAsset = false;
  
  protected $csReportType = '';
  
  protected $caRiskClassifType = array();
  
  protected $caProcessClassifType = array();
  
  protected $caProcessClassifPriority = array();
  
  
  public function setProcess($pbProcessId) {
    $this->ciProcessId = $pbProcessId;
  }
  
  public function getProcess() {
    return $this->ciProcessId;
  }
  
  public function organizeByAsset() {
    return $this->cbOrganizeByAsset = true;
  }
  
  public function mustOrganizeByAsset() {
    return $this->cbOrganizeByAsset;
  }
  
  public function setReportType($psReportType) {
    $this->csReportType = $psReportType;
  }
  
  public function getReportType() {
    return $this->csReportType;
  }
  
  public function getProcessClassifType(){
    return $this->caProcessClassifType;
  }

  public function setProcessClassifType($paProcessClassifType){
    $this->caProcessClassifType = $paProcessClassifType;
  }

  public function getProcessClassifPriority(){
    return $this->caProcessClassifPriority;
  }

  public function setProcessClassifPriority($paProcessClassifPriority){
    $this->caProcessClassifPriority = $paProcessClassifPriority;
  }

  
  public function getSummary(){    
    $maFilters = parent::getSummary();
    
    $maFilterProcess = array();
    if ($this->ciProcessId){      
      $moProcess = new RMProcess();
      $moProcess->fetchById($this->ciProcessId);
      $maFilterProcess[] = $moProcess->getName();
    }else{
    	$maFilterProcess[] = FWDLanguage::getPHPStringValue('st_all', 'Todos');
    }
            
    $maFilters[] = array(
      'name' => FWDLanguage::getPHPStringValue('rs_process_cl', 'Processo:'),
      'items' => $maFilterProcess
    );
    
    $maProcessClassifType = $this->getProcessClassifType();
    if(count($maProcessClassifType)){
      $maFilters[] = array(
        'name' => FWDLanguage::getPHPStringValue('lb_process_types_cl', 'Tipos de Processo:'),
        'items' => $maProcessClassifType
      );
    }

    $maProcessClassifPriority = $this->getProcessClassifPriority();
    if(count($maProcessClassifPriority)){
      $maFilters[] = array(
        'name' => FWDLanguage::getPHPStringValue('lb_process_priorities_cl', 'Prioridades de Processo:'),
        'items' => $maProcessClassifPriority
      );
    }
    
    return $maFilters;
  }
}
?>