<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportRisksResume.
 *
 * <p>Classe que implementa o relat�rio de indicadores</p>
 * @package ISMS
 * @subpackage report
 */

class ISMSReportRisksResume extends ISMSReport {

	protected $ciUserId = 0;

	/**
	 * Inicializa o relat�rio.
	 *
	 * <p>M�todo para inicializar o relat�rio.
	 * Necess�rio para funcionar com o XML compilado.</p>
	 * @access public
	 */
	public function init() {
		parent::init();

		$dataSet = $this->coDataSet;
		
		$origin = new FWDDBField("","origin",DB_STRING);
		$origin->setLabel(' ');
		$origin->setShowColumn(true);
		
		$order = new FWDDBField("","order",DB_STRING);
		$order->setShowColumn(false);
		
		$index = new FWDDBField("","index",DB_NUMBER);
		$index->setShowColumn(false);
		
		$ordem = new FWDDBField("","ordem",DB_STRING);
		$ordem->setLabel(' ');
		$ordem->setShowColumn(true);
		
		$col1 = new FWDDBField("","col1",DB_NUMBER);
		$col1->setLabel(FWDLanguage::getPHPStringValue('cb_high','Alto'));
		$col1->setShowColumn(true);
		
		$col2 = new FWDDBField("","col2",DB_NUMBER);
		$col2->setLabel(FWDLanguage::getPHPStringValue('cb_medium','M�dio'));
		$col2->setShowColumn(true);
		
		$col3 = new FWDDBField("","col3",DB_NUMBER);
		$col3->setLabel(FWDLanguage::getPHPStringValue('cb_low','Baixo'));
		$col3->setShowColumn(true);
		
		$col4 = new FWDDBField("","col4",DB_NUMBER);
		$col4->setLabel(FWDLanguage::getPHPStringValue('cb_not_parameterized','N�o Estimados'));
		$col4->setShowColumn(true);
		
		$col5 = new FWDDBField("","col5",DB_NUMBER);
		$col5->setLabel(FWDLanguage::getPHPStringValue('lb_total','Total'));
		$col5->setShowColumn(true);
		
		$col6 = new FWDDBField("","col6",DB_NUMBER);
		$col6->setLabel(FWDLanguage::getPHPStringValue('gc_capability','Potencial'));
		$col5->setShowColumn(true);
		
		$col7 = new FWDDBField("","col7",DB_NUMBER);
		$col7->setLabel(FWDLanguage::getPHPStringValue('rb_residual','Residual'));
		$col7->setShowColumn(true);
		
		$col8 = new FWDDBField("","col8",DB_NUMBER);
		$col8->setLabel(' ');
		$col8->setShowColumn(true);
		
		$col9 = new FWDDBField("","col9",DB_NUMBER);
		$col9->setLabel(' ');
		$col9->setShowColumn(true);
		
		$dataSet->addFWDDBField($origin);
		$dataSet->addFWDDBField($order);
		$dataSet->addFWDDBField($index);
		$dataSet->addFWDDBField($ordem);
		$dataSet->addFWDDBField($col1);
		$dataSet->addFWDDBField($col2);
		$dataSet->addFWDDBField($col3);
		$dataSet->addFWDDBField($col4);
		$dataSet->addFWDDBField($col5);
		$dataSet->addFWDDBField($col6);
		$dataSet->addFWDDBField($col7);
		$dataSet->addFWDDBField($col8);
		$dataSet->addFWDDBField($col9);
	}

	public function setUserId($psUserId) {
		$this->ciUserId = $psUserId;
	}

	/**
	 * Executa a query do relat�rio.
	 *
	 * <p>M�todo para executar a query do relat�rio.</p>
	 * @access public
	 */
	public function makeQuery() {

		$moFilter = $this->getFilter();

		// Filtros de tipo e prioridade

		$filterTypePriorArea = "";
		$filterTypePriorProcess = "";
		$filterTypePriorRisk = "";
		$filterTypePriorAsset = "";

		$joinTypeProcess = "";
		$joinTypeArea = "";
		$joinTypeAsset = "";

		$maClassifFilter = "";
		$maClassifFilter = $moFilter->getAreaClassifType();

		$areaTypeFilter = count($maClassifFilter);

		if($areaTypeFilter){
			if(isset($maClassifFilter['null'])){
				$filterTypePriorArea .= " AND a.fkType IS NULL ";
			}else{
				$filterTypePriorArea .= " AND a.fkType IN ( ". implode(',',array_keys($maClassifFilter)) ." ) ";
				$filterTypePriorProcess .= " AND area.fkType IN ( ". implode(',',array_keys($maClassifFilter)) ." ) ";
				$joinTypeProcess .= " JOIN view_rm_area_active area on area.fkcontext = a.fkarea";
				$filterTypePriorAsset .= " AND area.fkType IN ( ". implode(',',array_keys($maClassifFilter)) ." ) ";
				$filterTypePriorRisk .= " AND ar.fkType IN ( ". implode(',',array_keys($maClassifFilter)) ." ) ";
			}
		}

		$maClassifFilter = "";
		$maClassifFilter = $moFilter->getProcessClassifType();
		$processTypeFilter = count($maClassifFilter);
		if($processTypeFilter){
			if(isset($maClassifFilter['null'])){
				$filterTypePriorProcess .= " AND a.fkType IS NULL ";
			}else{
				$filterTypePriorProcess .= " AND a.fkType IN ( ". implode(',',array_keys($maClassifFilter)) ." ) ";
				$joinTypeArea = " JOIN view_rm_process_active p on p.fkarea = a.fkcontext ";
				$filterTypePriorArea .= " AND p.fkType IN ( ". implode(',',array_keys($maClassifFilter)) ." ) ";
				$filterTypePriorAsset .= " AND p.fkType IN ( ". implode(',',array_keys($maClassifFilter)) ." ) ";
				$filterTypePriorRisk .= " AND p.fkType IN ( ". implode(',',array_keys($maClassifFilter)) ." ) ";
			}
		}

		$maClassifFilter = "";
		$maClassifFilter = $moFilter->getAreaClassifPriority();
		$areaTypeFilter = $areaTypeFilter || count($maClassifFilter);
		if(count($maClassifFilter)){
			if(isset($maClassifFilter['null'])){
				$filterTypePriorArea .= "  AND a.fkPriority IS NULL ";
			}else{
				$filterTypePriorArea .= "  AND a.fkPriority IN ( ".implode(',',array_keys($maClassifFilter)) ." ) ";

				$filterTypePriorProcess .= " AND area2.fkPriority IN ( ". implode(',',array_keys($maClassifFilter)) ." ) ";
				$joinTypeProcess .= " JOIN view_rm_area_active area2 on area2.fkcontext = a.fkarea";
				$filterTypePriorAsset .= " AND area.fkPriority IN ( ". implode(',',array_keys($maClassifFilter)) ." ) ";
				$filterTypePriorRisk .= " AND ar.fkPriority IN ( ". implode(',',array_keys($maClassifFilter)) ." ) ";
			}
		}

		$maClassifFilter = "";
		$maClassifFilter = $moFilter->getProcessClassifPriority();

		$processTypeFilter = $processTypeFilter || count($maClassifFilter);
		if(count($maClassifFilter)){
			if(isset($maClassifFilter['null'])){
				$filterTypePriorProcess .= " AND a.fkPriority IS NULL ";
			}else{
				$filterTypePriorProcess .= " AND a.fkPriority IN ( ".implode(',',array_keys($maClassifFilter)) ." ) ";

				$joinTypeArea = " JOIN view_rm_process_active p on p.fkarea = a.fkcontext ";
				$filterTypePriorArea .= " AND p.fkPriority IN ( ". implode(',',array_keys($maClassifFilter)) ." ) ";
				$filterTypePriorAsset .= " AND p.fkPriority IN ( ". implode(',',array_keys($maClassifFilter)) ." ) ";
				$filterTypePriorRisk .= " AND p.fkPriority IN ( ". implode(',',array_keys($maClassifFilter)) ." ) ";
			}
		}

		$maClassifFilter = "";
		$maClassifFilter = $moFilter->getRiskClassifType();
		if(count($maClassifFilter)){
			if(isset($maClassifFilter['null'])){
				$filterTypePriorRisk .= " AND a.fkType IS NULL ";
			}else{
				$filterTypePriorRisk .= " AND a.fkType IN ( ". implode(',',array_keys($maClassifFilter)) ." ) ";

				$joinTypeProcess .= " JOIN view_rm_process_asset_active pa ON (pa.fkprocess = a.fkcontext)
									JOIN view_rm_risk_active risk ON (risk.fkasset = pa.fkasset) ";

				$joinTypeArea = " 	JOIN view_rm_process_active p ON (p.fkarea = a.fkcontext)
									JOIN view_rm_process_asset_active pa ON (pa.fkprocess = p.fkcontext) 
									JOIN view_rm_risk_active risk ON (risk.fkasset = pa.fkasset)";

				$filterTypePriorArea .= " AND risk.fkType IN ( ". implode(',',array_keys($maClassifFilter)) ." ) ";
				$filterTypePriorProcess .= " AND risk.fkType IN ( ". implode(',',array_keys($maClassifFilter)) ." ) ";

				$joinTypeAsset .= " JOIN view_rm_risk_active risk on risk.fkasset = a.fkcontext";
				$filterTypePriorAsset .= " AND risk.fkType IN ( ". implode(',',array_keys($maClassifFilter)) ." ) ";
			}
		}

		// Filtros especificos

		$selectedAreaId = $moFilter->getArea();
		$selectedProcessId = $moFilter->getProcess();
		$selectedAssetId = $moFilter->getAsset();

		$filterJoinAsset = "";
		$filterWhereAsset = "";
		$filterWhereArea = "";
		$filterJoinProcess = "";
		$filterWhereProcess = "";
		$filterJoinRisk = "";
		$filterWhereRisk = "";

		if($selectedAreaId || $selectedProcessId || $selectedAssetId || $areaTypeFilter || $processTypeFilter){
			$filterJoinAsset .= " JOIN view_rm_process_asset_active pa on pa.fkasset = a.fkcontext
								  JOIN view_rm_process_active p on p.fkcontext = pa.fkprocess 
								  JOIN view_rm_area_active area on p.fkarea = area.fkcontext ";

			$filterJoinRisk = " JOIN view_rm_asset_active a2 ON (a.fkasset = a2.fkcontext)
							    JOIN view_rm_process_asset_active pa ON (pa.fkasset = a2.fkcontext)
							    JOIN view_rm_process_active p ON (p.fkcontext = pa.fkprocess)
							    JOIN view_rm_area_active ar ON (ar.fkcontext = p.fkarea) ";				
		}

		if($selectedAreaId){
			$filterWhereArea .= " AND a.fkcontext =  " . $selectedAreaId . " ";
			$filterWhereRisk = " AND ar.fkcontext = " . $selectedAreaId . " ";
			$filterWhereProcess .= " AND a.fkarea = " . $selectedAreaId . " ";
			$filterWhereAsset .= " AND area.fkcontext = " . $selectedAreaId . " ";
		}

		if($selectedProcessId){
			$filterWhereRisk .= " AND p.fkcontext = " . $selectedProcessId . " ";
			$filterWhereProcess .= " AND a.fkcontext = " . $selectedProcessId . " ";
				
			$filterWhereAsset .= " AND p.fkcontext = " . $selectedProcessId . " ";
		}

		if($selectedAssetId){
			$filterWhereAsset .= " AND a.fkcontext = " . $selectedAssetId . " ";
			$filterWhereRisk .= " AND a2.fkcontext = " . $selectedAssetId . " ";
		}

		$msUserJoin_ar = $msUserJoin_p = $msUserJoin_a = $joinRisk = '';
		if (isset($this->caUserPermissions['list_area'],$this->caUserPermissions['list_process'],$this->caUserPermissions['list_asset'],$this->caUserPermissions['list_risk'])) {
			$msUserJoin_ar = (($this->ciUserId && !$this->caUserPermissions['list_area']) ? $this->getUserFilter(): "");
			$msUserJoin_p = (($this->ciUserId && !$this->caUserPermissions['list_process']) ? $this->getUserFilter() : "");
			$msUserJoin_a = (($this->ciUserId && !$this->caUserPermissions['list_asset']) ? $this->getUserFilter() : "");
			$joinRisk = (($this->ciUserId && !$this->caUserPermissions['list_risk']) ? " JOIN rm_asset a2 ON (a.fkAsset = a2.fkContext AND a2.fkResponsible=".$this->ciUserId.")" : "");
		}

		$title_summaryResidualRiskXsummaryRisk = FWDLanguage::getPHPStringValue('tt_summary_of_parameterized_risks','Riscos Potenciais vs. Riscos Residuais');
		$title_summaryResidualRisk = FWDLanguage::getPHPStringValue('tt_risk_management_status','N�vel de Risco');
		$title_summaryRisk = FWDLanguage::getPHPStringValue('tt_general_summary_of_risks','Risco Estimado vs. N�o-Estimado');		
		
		$title_area	= FWDLanguage::getPHPStringValue('cb_area','�rea');
		$title_process = FWDLanguage::getPHPStringValue('cb_process','Processo');
		$title_asset = FWDLanguage::getPHPStringValue('cb_asset','Ativo');
		$title_risk = FWDLanguage::getPHPStringValue('cb_risk','Risco');
		$title_high = FWDLanguage::getPHPStringValue('st_high_risk','Risco Alto');
		$title_mid = FWDLanguage::getPHPStringValue('st_medium_risk','Risco M�dio');
		$title_low = FWDLanguage::getPHPStringValue('st_low_risk','Risco Baixo');
		$title_total = FWDLanguage::getPHPStringValue('lb_total','Total');
		$title_risk_np = FWDLanguage::getPHPStringValue('st_non_parameterized_risks','Riscos N�o Estimados');
		$title_risk_estimated = FWDLanguage::getPHPStringValue('st_parameterized_risks','Riscos Estimados');
		$title_risk_treated = FWDLanguage::getPHPStringValue('rb_treated','Tratados');
		$title_risk_not_treated = FWDLanguage::getPHPStringValue('rb_not_treated','N�o Tratados');
		$title_risk_total = FWDLanguage::getPHPStringValue('lb_total','Total');				
		
		$query = "SELECT
					1 as index,
					'$title_summaryResidualRisk' as origin,
					1 as order,
					'$title_area' as ordem, 
					count(distinct area_high.pkcontext)||'' as col1,
					count(distinct area_mid.pkcontext)||'' as col2,
					count(distinct area_low.pkcontext)||'' as col3,
					count(distinct area_np.pkcontext)||'' as col4,
					count(distinct area_total.pkcontext)||'' as col5,
					'' as col6,
					'' as col7,
					'' as col8,
					'' as col9
					from rm_area a
					{$joinTypeArea}
					LEFT JOIN isms_context area_high ON (a.fkContext = area_high.pkContext AND area_high.nState != ".CONTEXT_STATE_DELETED." AND a.nValue >= ".ISMSLib::getConfigById(RISK_HIGH)." )
					LEFT JOIN isms_context area_low ON (a.fkContext = area_low.pkContext AND area_low.nState != ".CONTEXT_STATE_DELETED." AND a.nValue <= ".ISMSLib::getConfigById(RISK_LOW)." AND a.nValue > 0)
					LEFT JOIN isms_context area_mid ON (a.fkContext = area_mid.pkContext AND area_mid.nState != ".CONTEXT_STATE_DELETED." AND a.nValue > ".ISMSLib::getConfigById(RISK_LOW)." AND a.nValue < ".ISMSLib::getConfigById(RISK_HIGH)." )
					LEFT JOIN isms_context area_np ON (a.fkContext = area_np.pkContext AND area_np.nState != ".CONTEXT_STATE_DELETED." AND (a.nValue = 0 OR a.nValue IS NULL))
					LEFT JOIN isms_context area_total ON (a.fkContext = area_total.pkContext AND area_total.nState != ".CONTEXT_STATE_DELETED.")
					WHERE 1=1 {$msUserJoin_ar}
					{$filterWhereArea}
					{$filterTypePriorArea}
				UNION
				SELECT 
					2,
					'$title_summaryResidualRisk' as origin,
					1 as order,
					'$title_process' as ordem, 
					count(distinct area_high.pkcontext)||'' as col1,
					count(distinct area_mid.pkcontext)||'' as col2,
					count(distinct area_low.pkcontext)||'' as col3,
					count(distinct area_np.pkcontext)||'' as col4,
					count(distinct area_total.pkcontext)||'' as col5,
					'' as col6,
					'' as col7,
					'' as col8,
					'' as col9					
					from rm_process a
					{$joinTypeProcess}
					{$filterJoinProcess}
					LEFT JOIN isms_context area_high ON (a.fkContext = area_high.pkContext AND area_high.nState != ".CONTEXT_STATE_DELETED." AND a.nValue >= ".ISMSLib::getConfigById(RISK_HIGH)." )
					LEFT JOIN isms_context area_low ON (a.fkContext = area_low.pkContext AND area_low.nState != ".CONTEXT_STATE_DELETED." AND a.nValue <= ".ISMSLib::getConfigById(RISK_LOW)." AND a.nValue > 0)
					LEFT JOIN isms_context area_mid ON (a.fkContext = area_mid.pkContext AND area_mid.nState != ".CONTEXT_STATE_DELETED." AND a.nValue > ".ISMSLib::getConfigById(RISK_LOW)." AND a.nValue < ".ISMSLib::getConfigById(RISK_HIGH)." )
					LEFT JOIN isms_context area_np ON (a.fkContext = area_np.pkContext AND area_np.nState != ".CONTEXT_STATE_DELETED." AND (a.nValue = 0 OR a.nValue IS NULL))
					LEFT JOIN isms_context area_total ON (a.fkContext = area_total.pkContext AND area_total.nState != ".CONTEXT_STATE_DELETED.")
					WHERE 1=1 {$msUserJoin_p}
					{$filterWhereProcess}
					{$filterTypePriorProcess}
				UNION
				SELECT 
					3,
					'$title_summaryResidualRisk' as origin,
					1 as order,
					'$title_asset' as ordem, 
					count(distinct area_high.pkcontext)||'' as col1,
					count(distinct area_mid.pkcontext)||'' as col2,
					count(distinct area_low.pkcontext)||'' as col3,
					count(distinct area_np.pkcontext)||'' as col4,
					count(distinct area_total.pkcontext)||'' as col5,
					'' as col6,
					'' as col7,
					'' as col8,
					'' as col9					
					from rm_asset a
					{$filterJoinAsset}
					{$joinTypeAsset}
					LEFT JOIN isms_context area_high ON (a.fkContext = area_high.pkContext AND area_high.nState != ".CONTEXT_STATE_DELETED." AND a.nValue >= ".ISMSLib::getConfigById(RISK_HIGH)." )
					LEFT JOIN isms_context area_low ON (a.fkContext = area_low.pkContext AND area_low.nState != ".CONTEXT_STATE_DELETED." AND a.nValue <= ".ISMSLib::getConfigById(RISK_LOW)." AND a.nValue > 0)
					LEFT JOIN isms_context area_mid ON (a.fkContext = area_mid.pkContext AND area_mid.nState != ".CONTEXT_STATE_DELETED." AND a.nValue > ".ISMSLib::getConfigById(RISK_LOW)." AND a.nValue < ".ISMSLib::getConfigById(RISK_HIGH)." )
					LEFT JOIN isms_context area_np ON (a.fkContext = area_np.pkContext AND area_np.nState != ".CONTEXT_STATE_DELETED." AND (a.nValue = 0 OR a.nValue IS NULL))
					LEFT JOIN isms_context area_total ON (a.fkContext = area_total.pkContext AND area_total.nState != ".CONTEXT_STATE_DELETED.")
					WHERE 1=1 {$msUserJoin_a}
					{$filterWhereAsset}
					{$filterTypePriorAsset}
				UNION
				SELECT
					4,
					'$title_summaryResidualRisk' as origin,
					1 as order,
					'$title_risk' as ordem, 
					count(distinct area_high.pkcontext)||'' as col1,
					count(distinct area_mid.pkcontext)||'' as col2,
					count(distinct area_low.pkcontext)||'' as col3,
					count(distinct area_np.pkcontext)||'' as col4,
					count(distinct area_total.pkcontext)||'' as col5,
					'' as col6,
					'' as col7,
					'' as col8,
					'' as col9					
					from rm_risk a
					{$joinRisk}
					{$filterJoinRisk}
					LEFT JOIN isms_context area_high ON (a.fkContext = area_high.pkContext AND area_high.nState != ".CONTEXT_STATE_DELETED." AND a.nValueResidual >= ".ISMSLib::getConfigById(RISK_HIGH)." )
					LEFT JOIN isms_context area_low ON (a.fkContext = area_low.pkContext AND area_low.nState != ".CONTEXT_STATE_DELETED." AND a.nValueResidual <= ".ISMSLib::getConfigById(RISK_LOW)." AND a.nValueResidual > 0)
					LEFT JOIN isms_context area_mid ON (a.fkContext = area_mid.pkContext AND area_mid.nState != ".CONTEXT_STATE_DELETED." AND a.nValueResidual > ".ISMSLib::getConfigById(RISK_LOW)." AND a.nValueResidual < ".ISMSLib::getConfigById(RISK_HIGH)." )
					LEFT JOIN isms_context area_np ON (a.fkContext = area_np.pkContext AND area_np.nState != ".CONTEXT_STATE_DELETED." AND (a.nValueResidual = 0 OR a.nValueResidual IS NULL))
					LEFT JOIN isms_context area_total ON (a.fkContext = area_total.pkContext AND area_total.nState != ".CONTEXT_STATE_DELETED.")
					WHERE 1=1 
					{$filterWhereRisk}
					{$filterTypePriorRisk}
				UNION
				SELECT 
					1,
					'$title_summaryResidualRiskXsummaryRisk' as origin,
					2 as order,
					'$title_high' as ordem, 
					'' as col1,
					'' as col2,
					'' as col3,
					'' as col4,
					'' as col5,
					count(distinct area_high.pkcontext)||'' as col6,
					count(distinct summary.pkcontext)||'' as col7,
					'' as col8,
					'' as col9					
					
					from rm_risk a
					{$filterJoinRisk}
					LEFT JOIN isms_context area_high ON (a.fkContext = area_high.pkContext AND area_high.nState != ".CONTEXT_STATE_DELETED." AND a.nValue >= ".ISMSLib::getConfigById(RISK_HIGH)." )
					LEFT JOIN isms_context summary ON (a.fkContext = summary.pkContext AND summary.nState != ".CONTEXT_STATE_DELETED." AND a.nValueResidual >= ".ISMSLib::getConfigById(RISK_HIGH)." )
					WHERE 1=1 
					{$filterWhereRisk}
					{$filterTypePriorRisk}
				UNION
				SELECT 
					2,
					'$title_summaryResidualRiskXsummaryRisk' as origin,
					2 as order,
					'$title_mid' as ordem, 
					'' as col1,
					'' as col2,
					'' as col3,
					'' as col4,
					'' as col5,					
					count(distinct area_mid.pkcontext)||'' as col6,
					count(distinct summary.pkcontext)||'' as col7,
					'' as col8,
					'' as col9
										
					from rm_risk a
					{$filterJoinRisk}
					LEFT JOIN isms_context area_mid ON (a.fkContext = area_mid.pkContext AND area_mid.nState != ".CONTEXT_STATE_DELETED." AND a.nValue > ".ISMSLib::getConfigById(RISK_LOW)." AND a.nValue < ".ISMSLib::getConfigById(RISK_HIGH)." )
					LEFT JOIN isms_context summary ON (a.fkContext = summary.pkContext AND summary.nState != ".CONTEXT_STATE_DELETED." AND a.nValueResidual > ".ISMSLib::getConfigById(RISK_LOW)." AND a.nValueResidual < ".ISMSLib::getConfigById(RISK_HIGH)." )
					WHERE 1=1 
					{$filterWhereRisk}
					{$filterTypePriorRisk}
				UNION
				SELECT 
					3,
					'$title_summaryResidualRiskXsummaryRisk' as origin,
					2 as order,
					'$title_low' as ordem, 
					'' as col1,
					'' as col2,
					'' as col3,
					'' as col4,
					'' as col5,					
					count(distinct area_low.pkcontext)||'' as col6,
					count(distinct summary.pkcontext)||'' as col7,
					'' as col8,
					'' as col9					
					
					from rm_risk a
					{$filterJoinRisk}
					LEFT JOIN isms_context area_low ON (a.fkContext = area_low.pkContext AND area_low.nState != ".CONTEXT_STATE_DELETED." AND a.nValue <= ".ISMSLib::getConfigById(RISK_LOW)." AND a.nValue > 0)
					LEFT JOIN isms_context summary ON (a.fkContext = summary.pkContext AND summary.nState != ".CONTEXT_STATE_DELETED." AND a.nValueResidual <= ".ISMSLib::getConfigById(RISK_LOW)." AND a.nValueResidual > 0)
					WHERE 1=1 
					{$filterWhereRisk}
					{$filterTypePriorRisk}
				UNION
				SELECT 
					4,
					'$title_summaryResidualRiskXsummaryRisk' as origin,
					2 as order,
					'$title_total' as ordem, 
					'' as col1,
					'' as col2,
					'' as col3,
					'' as col4,
					'' as col5,					
					count(distinct area_low.pkcontext)||'' as col6,
					count(distinct summary.pkcontext)||'' as col7,
					'' as col8,
					'' as col9					
					
					from rm_risk a
					{$filterJoinRisk}
					LEFT JOIN isms_context area_low ON (a.fkContext = area_low.pkContext AND area_low.nState != ".CONTEXT_STATE_DELETED." AND a.nValue > 0)
					LEFT JOIN isms_context summary ON (a.fkContext = summary.pkContext AND summary.nState != ".CONTEXT_STATE_DELETED." AND a.nValueResidual > 0)
					WHERE 1=1 
					{$filterWhereRisk}
					{$filterTypePriorRisk}
				UNION
				
				SELECT 1,'$title_summaryRisk' as origin, 3 as order, '$title_risk_np' as ordem, '', '','',    '','','','',    count(distinct a.fkcontext)||'' as count, 
				round(count(distinct a.fkcontext)/coalesce(nullif( ( select cast(count(distinct a.fkcontext) as numeric) from view_rm_risk_active a {$filterJoinRisk} where 1=1 {$filterWhereRisk} {$filterTypePriorRisk} ) ,0 ) , 1) *100,2)||'%'
				as total					
				FROM view_rm_risk_active a  
				{$filterJoinRisk}
				where (a.nValue = 0 OR a.nValue IS NULL) 
				{$filterWhereRisk}
				{$filterTypePriorRisk}
				UNION
				
				SELECT 2,'$title_summaryRisk' as origin, 3 as order,  '$title_risk_estimated' as ordem, '', '','',   '','','','',  count(distinct a.fkcontext)||'' as count, 
				round(count(distinct a.fkcontext)/coalesce(nullif( ( select cast(count(distinct a.fkcontext) as numeric) from view_rm_risk_active a {$filterJoinRisk} where 1=1 {$filterWhereRisk} {$filterTypePriorRisk} ) ,0 ) , 1) *100,2)||'%'
				as total				
				FROM view_rm_risk_active a
				{$filterJoinRisk}
				where ((a.nAcceptMode<>0 OR a.nValue != a.nValueResidual) or (a.nAcceptMode=0 AND a.nValue = a.nValueResidual AND a.nValue > 0)) 
				{$filterWhereRisk}
				{$filterTypePriorRisk}
				UNION
				
				SELECT 3,'$title_summaryRisk' as origin, 3 as order,  '$title_risk_treated' as ordem, '', '','',     '','','','',    count(distinct a.fkcontext)||'' as count, 
				round(count(distinct a.fkcontext)/coalesce(nullif(( select cast(count(distinct a.fkcontext) as numeric) from view_rm_risk_active a {$filterJoinRisk} where 1=1 {$filterWhereRisk} {$filterTypePriorRisk} ) ,0 ) , 1) *100,2)||'%'
				as total				
				FROM view_rm_risk_active a 
				{$filterJoinRisk}
				where (a.nAcceptMode<>0 OR a.nValue != a.nValueResidual)
				{$filterWhereRisk}
				{$filterTypePriorRisk}
				UNION 
				
				SELECT 4,'$title_summaryRisk' as origin, 3 as order,  '$title_risk_not_treated' as ordem, '', '','',    '','','','',    count(distinct a.fkcontext)||'' as count, 
				round(count(distinct a.fkcontext)/coalesce(nullif(( select cast(count(distinct a.fkcontext) as numeric) from view_rm_risk_active a {$filterJoinRisk} where 1=1 {$filterWhereRisk} {$filterTypePriorRisk} ) ,0 ) , 1) *100,2)||'%' 
				as total
				FROM view_rm_risk_active a 
				{$filterJoinRisk}
				where (a.nAcceptMode=0 AND a.nValue = a.nValueResidual AND a.nValue > 0) 
				{$filterWhereRisk}
				{$filterTypePriorRisk}
				UNION 
				
				SELECT 5,'$title_summaryRisk' as origin, 3 as order,  '$title_risk_total' as ordem, '', '','',     '','','','',    count(distinct a.fkcontext)||'' as count, '100.00%' as total 
				FROM view_rm_risk_active a 
				{$filterJoinRisk}
				where 1=1
				{$filterWhereRisk}
				{$filterTypePriorRisk}
				order by 3,1";
				$this->csQuery = $query;
				
				return parent::executeQuery();
	}

	protected function getUserFilter(){
		$userId = $this->ciUserId;
		return " AND
			area_high.fkResponsible = {$userId} AND 
			area_mid.fkResponsible = {$userId} AND 
			area_low.fkResponsible = {$userId} AND 
			area_np.fkResponsible = {$userId} AND 
			area_total.fkResponsible = {$userId}";
	}

	/**
	 * Desenha o cabe�alho do relat�rio.
	 *
	 * <p>M�todo para desenhar o cabe�alho do relat�rio.</p>
	 * @access public
	 */
	public function drawHeader(){
		foreach($this->caHeaders as $moHeader){
			if($moHeader->getAttrName()=='header_filter'){
				$moHeaderFilter = $moHeader;
			}elseif($moHeader->getAttrName()=='header_filter_item'){
				$moHeaderFilterItem = $moHeader;
			}elseif($moHeader->getAttrName()=='report_header_blank'){
				$moHeaderBlank = $moHeader;
			}else{
				$this->coWriter->drawLine($moHeader,array());
			}
		}
		
		if ($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());

		$moFilterText = FWDWebLib::getObject('header_filter_text');
		$moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
		$maFilters = $this->coFilter->getSummary();
		foreach($maFilters as $maFilter){
			$moFilterText->setValue($maFilter['name']);
			$this->coWriter->drawLine($moHeaderFilter,array());
			foreach($maFilter['items'] as $msFilterItem){
				$moFilterItemText->setValue($msFilterItem);
				$this->coWriter->drawLine($moHeaderFilterItem,array());
			}
		}
		$this->coWriter->drawLine($moHeaderBlank,array());
	}
}

?>