<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportRisksByControlFilter.
 *
 * <p>Classe que implementa o filtro do relatório de riscos por Controle.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportRisksByControlFilter extends ISMSReportRiskValuesFilter {
	
  protected $csReportType = '';
  
  protected $caControlClassifType = array();
	
  public function setReportType($psReportType) {
	$this->csReportType = $psReportType;
  }
	
  public function getReportType() {
    return $this->csReportType;
  }
  
  public function getControlClassifType(){
    return $this->caControlClassifType;
  }

  public function setControlClassifType($paControlClassifType){
    $this->caControlClassifType = $paControlClassifType;
  }

  public function getSummary(){		
	$maFilters = parent::getSummary();
		
	$maFilterControl = array();
    $maControlClassifType = $this->getControlClassifType();
    if(count($maControlClassifType)){
      $maFilters[] = array(
        'name' => FWDLanguage::getPHPStringValue('lb_control_types_cl', 'Tipos de Controle:'),
        'items' => $maControlClassifType
      );
    }
    return $maFilters;
  }
  
}
?>