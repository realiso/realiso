<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe ISMSReportConformity.
 *
 * <p>Classe que implementa o relat�rio de conformidades.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportConformity extends ISMSReport {
  
  /**
   * Inicializa o relat�rio.
   *
   * <p>M�todo para inicializar o relat�rio.
   * Necess�rio para funcionar com o XML compilado.</p>
   * @access public
   */  
  public function init() {
    parent::init();

	$col1 = new FWDDBField("","standard_id",DB_NUMBER);
	$col1->setLabel(' ');
	$col1->setShowColumn(false);
	
	$col2 = new FWDDBField("","standard_name",DB_STRING);
	$col2->setLabel(FWDLanguage::getPHPStringValue('mx_standard','Norma'));
	$col2->setShowColumn(true);		

	$col3 = new FWDDBField("","bp_applied",DB_NUMBER);
	$col3->setLabel(FWDLanguage::getPHPStringValue('rs_applied_best_practices', 'Melhores Pr�ticas Aplicadas'));
	$col3->setShowColumn(true);
	
	$col4 = new FWDDBField("","bp_id",DB_NUMBER);
	$col4->setLabel(' ');
	$col4->setShowColumn(false);		
	
	$col5 = new FWDDBField("","bp_name",DB_STRING);
	$col5->setLabel(FWDLanguage::getPHPStringValue('cb_best_practices','Melhores Pr�ticas'));
	$col5->setShowColumn(true);
	
	$col6 = new FWDDBField("","control_applied",DB_NUMBER);
	$col6->setLabel(FWDLanguage::getPHPStringValue('rs_used_controls',"Controles Utilizados"));
	$col6->setShowColumn(true);		
	
	$col7 = new FWDDBField("","control_id",DB_NUMBER);
	$col7->setLabel(' ');
	$col7->setShowColumn(false);
	
	$col8 = new FWDDBField("","control_name",DB_STRING);
	$col8->setLabel(FWDLanguage::getPHPStringValue('cb_control','Controle'));
	$col8->setShowColumn(true);	    
    
	$col9 = new FWDDBField("","control_is_active",DB_NUMBER);
	$col9->setLabel(' ');
	$col9->setShowColumn(false);
	
	$col10 = new FWDDBField("","control_responsible",DB_STRING);
	$col10->setLabel(FWDLanguage::getPHPStringValue('rs_responsible','Respons�vel'));
	$col10->setShowColumn(true);	
    
    $this->coDataSet->addFWDDBField($col1);
    $this->coDataSet->addFWDDBField($col2);
    $this->coDataSet->addFWDDBField($col3);
    $this->coDataSet->addFWDDBField($col4);
    $this->coDataSet->addFWDDBField($col5);
    $this->coDataSet->addFWDDBField($col6);
    $this->coDataSet->addFWDDBField($col7);
    $this->coDataSet->addFWDDBField($col8);
    $this->coDataSet->addFWDDBField($col9);
    $this->coDataSet->addFWDDBField($col10);
    
  }
  
  /**
   * Executa a query do relat�rio.
   *
   * <p>M�todo para executar a query do relat�rio.</p>
   * @access public
   */
  public function makeQuery() {    
    $moFilter = $this->getFilter();    
    $msWhere ="";
    $maClassifType = $moFilter->getControlClassifType();
    
    if(count($maClassifType)){
      if(isset($maClassifType['null'])){
        $msWhere = " AND c.fkType IS NULL ";
      }else{
        $msWhere = " AND c.fkType IN ( ".implode(',',array_keys($maClassifType)) ." ) ";
      }
    }
    
    $miStandardId = $moFilter->getStandard();
    if ($miStandardId) $msWhere .= " AND s.fkContext = $miStandardId";
    
    $this->csQuery = "SELECT
                        s.fkContext as standard_id,
                        s.sName as standard_name,
                        1 as bp_applied,
                        bp.fkContext as bp_id,
                        bp.sName as bp_name,
                        1 as control_applied, 
                        c.fkContext as control_id,
                        c.sName as control_name,
                        c.bIsActive as control_is_active,
                        u.sName as control_responsible
                      FROM
                        view_rm_standard_active s
                        LEFT JOIN view_rm_bp_standard_active bps ON (s.fkContext = bps.fkStandard)
                        LEFT JOIN view_rm_best_practice_active bp ON (bps.fkBestPractice = bp.fkContext)
                        LEFT JOIN view_rm_control_bp_active cbp ON (bp.fkContext = cbp.fkBestPractice)  
                        LEFT JOIN view_rm_control_active c ON (cbp.fkControl = c.fkContext)
                        LEFT JOIN view_rm_risk_control_active rc ON (c.fkContext = rc.fkControl)
                        LEFT JOIN view_isms_user_active u ON (c.fkResponsible = u.fkContext)
                      WHERE
                        c.fkContext IS NOT NULL AND rc.fkRisk IS NOT NULL $msWhere
                      
                      UNION
                      
                      SELECT
                        s.fkContext as standard_id,
                        s.sName as standard_name,
                        1 as bp_applied,
                        bp.fkContext as bp_id,
                        bp.sName as bp_name,
                        0 as control_applied, 
                        c.fkContext as control_id,
                        c.sName as control_name,
                        c.bIsActive as control_is_active,
                        u.sName as control_responsible
                      FROM
                        view_rm_standard_active s
                        LEFT JOIN view_rm_bp_standard_active bps ON (s.fkContext = bps.fkStandard)
                        LEFT JOIN view_rm_best_practice_active bp ON (bps.fkBestPractice = bp.fkContext)
                        LEFT JOIN view_rm_control_bp_active cbp ON (bp.fkContext = cbp.fkBestPractice)  
                        LEFT JOIN view_rm_control_active c ON (cbp.fkControl = c.fkContext)
                        LEFT JOIN view_rm_risk_control_active rc ON (c.fkContext = rc.fkControl)
                        LEFT JOIN view_isms_user_active u ON (c.fkResponsible = u.fkContext)
                      WHERE
                        c.fkContext IS NOT NULL AND rc.fkRisk IS NULL $msWhere
                      
                      UNION
                      
                      SELECT
                        s.fkContext as standard_id,
                        s.sName as standard_name,
                        0 as bp_applied,
                        bp.fkContext as bp_id,
                        bp.sName as bp_name,
                        0 as control_applied, 
                        c.fkContext as control_id,
                        c.sName as control_name,
                        c.bIsActive as control_is_active,
                        u.sName as control_responsible
                      FROM
                        view_rm_standard_active s
                        LEFT JOIN view_rm_bp_standard_active bps ON (s.fkContext = bps.fkStandard)
                        LEFT JOIN view_rm_best_practice_active bp ON (bps.fkBestPractice = bp.fkContext)
                        LEFT JOIN view_rm_control_bp_active cbp ON (bp.fkContext = cbp.fkBestPractice)  
                        LEFT JOIN view_rm_control_active c ON (cbp.fkControl = c.fkContext)
                        LEFT JOIN view_rm_risk_control_active rc ON (c.fkContext = rc.fkControl)
                        LEFT JOIN view_isms_user_active u ON (c.fkResponsible = u.fkContext)
                      WHERE
                        c.fkContext IS NULL $msWhere
                      
                      ORDER BY
                         standard_id,
                        bp_applied DESC,
                        --bp_id,
                        bp_name,
                        control_applied DESC,
                        --control_id,
                        control_name";
    
    return parent::executeQuery();
  }
  
  /**
   * Desenha o cabe�alho do relat�rio.
   *
   * <p>M�todo para desenhar o cabe�alho do relat�rio.</p>
   * @access public
   */
  public function drawHeader(){
    foreach($this->caHeaders as $moHeader){
      if($moHeader->getAttrName()=='header_filter'){
        $moHeaderFilter = $moHeader;
      }elseif($moHeader->getAttrName()=='header_filter_item'){
        $moHeaderFilterItem = $moHeader;
      }elseif($moHeader->getAttrName()=='report_header_blank'){
        $moHeaderBlank = $moHeader;
      }else{
        $this->coWriter->drawLine($moHeader,array());
      }
    }
    if ($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());
    
    $moFilterText = FWDWebLib::getObject('header_filter_text');
    $moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
    $maFilters = $this->coFilter->getSummary();
    foreach($maFilters as $maFilter){
      $moFilterText->setValue($maFilter['name']);
      $this->coWriter->drawLine($moHeaderFilter,array());
      foreach($maFilter['items'] as $msFilterItem){
        $moFilterItemText->setValue($msFilterItem);
        $this->coWriter->drawLine($moHeaderFilterItem,array());
      }
    }
    $this->coWriter->drawLine($moHeaderBlank,array());
  }
}
?>