<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportControlsByProcess.
 *
 * <p>Classe que implementa o relatório riscos por processo.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportControlsByProcess extends ISMSReport {

  /**
   * Inicializa o relatório.
   *
   * <p>Método para inicializar o relatório.
   * Necessário para funcionar com o XML compilado.</p>
   * @access public
   */
  public function init() {
    parent::init();

	$col1 = new FWDDBField("","process_name",DB_STRING);
	$col1->setLabel(FWDLanguage::getPHPStringValue('lb_department','Processo'));
	$col1->setShowColumn(true);

	$col2 = new FWDDBField("","asset_name",DB_STRING);
	$col2->setLabel(FWDLanguage::getPHPStringValue('lb_process','Ativo'));
	$col2->setShowColumn(true);

	$col3 = new FWDDBField("","asset_description",DB_STRING);
	$col3->setLabel(FWDLanguage::getPHPStringValue('rs_description','Descrição'));
	$col3->setShowColumn(true);

	$col4 = new FWDDBField("","asset_category",DB_STRING);
	$col4->setLabel(FWDLanguage::getPHPStringValue('gc_category','Categoria'));
	$col4->setShowColumn(true);

	$col5 = new FWDDBField("","asset_relavance",DB_STRING);
	$col5->setLabel(FWDLanguage::getPHPStringValue('lb_relavance','Relevância'));
	$col5->setShowColumn(true);

	$col6 = new FWDDBField("","risk_name",DB_STRING);
	$col6->setLabel(FWDLanguage::getPHPStringValue('cb_risk','Risco'));
	$col6->setShowColumn(true);

	$col7 = new FWDDBField("","risk_event_description",DB_STRING);
	$col7->setLabel(' ');
	$col7->setShowColumn(false);

	$col8 = new FWDDBField("","risk_description",DB_STRING);
	$col8->setLabel(FWDLanguage::getPHPStringValue('rs_description','Descrição'));
	$col8->setShowColumn(true);

	$col9 = new FWDDBField("","risk_estimation",DB_STRING);
	$col9->setLabel(FWDLanguage::getPHPStringValue('lb_estimation','Estimativa de Risco'));
	$col9->setShowColumn(true);

	$col10 = new FWDDBField("","control_name",DB_STRING);
	$col10->setLabel(FWDLanguage::getPHPStringValue('cb_control','Controle'));
	$col10->setShowColumn(true);

	$col11 = new FWDDBField("","control_description",DB_STRING);
	$col11->setLabel(FWDLanguage::getPHPStringValue('rs_description','Descrição'));
	$col11->setShowColumn(true);

	$col12 = new FWDDBField("","control_evidence",DB_STRING);
	$col12->setLabel(FWDLanguage::getPHPStringValue('lb_evidence','Evidência'));
	$col12->setShowColumn(true);

	$col13 = new FWDDBField("","control_type",DB_STRING);
	$col13->setLabel(FWDLanguage::getPHPStringValue('gc_type','Tipo'));
	$col13->setShowColumn(true);

	$col14 = new FWDDBField("","control_best_practice",DB_STRING);
	$col14->setLabel(FWDLanguage::getPHPStringValue('cb_best_practices','Melhores Práticas'));
	$col14->setShowColumn(true);

	$col15 = new FWDDBField("","process_id",DB_STRING);
	$col15->setLabel(' ');
	$col15->setShowColumn(false);

	$col16 = new FWDDBField("","asset_id",DB_STRING);
	$col16->setLabel(' ');
	$col16->setShowColumn(false);

	$col17 = new FWDDBField("","risk_id",DB_STRING);
	$col17->setLabel(' ');
	$col17->setShowColumn(false);

	$col18 = new FWDDBField("","control_id",DB_STRING);
	$col18->setLabel(' ');
	$col18->setShowColumn(false);

	$col19 = new FWDDBField("","risk_residual",DB_STRING);
	$col19->setLabel(FWDLanguage::getPHPStringValue('rs_residual_risk','Risco Residual'));
	$col19->setShowColumn(true);

    $this->coDataSet->addFWDDBField($col1);

    $this->coDataSet->addFWDDBField($col2);
    $this->coDataSet->addFWDDBField($col3);
    $this->coDataSet->addFWDDBField($col4);
    $this->coDataSet->addFWDDBField($col5);

    $this->coDataSet->addFWDDBField($col6);
    $this->coDataSet->addFWDDBField($col7);
    $this->coDataSet->addFWDDBField($col8);
    $this->coDataSet->addFWDDBField($col9);
	$this->coDataSet->addFWDDBField($col10);

    $this->coDataSet->addFWDDBField($col11);
    $this->coDataSet->addFWDDBField($col12);
    $this->coDataSet->addFWDDBField($col13);
    $this->coDataSet->addFWDDBField($col14);
    $this->coDataSet->addFWDDBField($col15);

    $this->coDataSet->addFWDDBField($col16);
    $this->coDataSet->addFWDDBField($col17);
    $this->coDataSet->addFWDDBField($col18);
    $this->coDataSet->addFWDDBField($col19);
  }

  /**
   * Executa a query do relatório.
   *
   * <p>Método para executar a query do relatório.</p>
   * @access public
   */
  public function makeQuery() {

  	$moFilter = $this->coFilter;

  	$miProcessId = $moFilter->getProcess();
  	$miAssetId = $moFilter->getAsset();

  	$showApprovedAsset = $moFilter->getShowApprovedAsset();
  	$showDeniedAsset = $moFilter->getShowDeniedAsset();
  	$showPendantAsset = $moFilter->getShowPendantAsset();

  	$filter = array();
	$assetStatusFilter = array();

  	if($miProcessId > 0)
  		$filter[] = 'p.fkContext = ' . $miProcessId;

  	if($miAssetId > 0)
  		$filter[] = 'a.fkContext = ' . $miAssetId;

  	// filtro tipo risco
	$msClassifFilter = "";
	$maClassifFilter = $moFilter->getRiskClassifType();
	if(count($maClassifFilter)){
		if(isset($maClassifFilter['null'])){
			$filter[] = " r.fkType IS NULL ";
		}else{
			$filter[] = " r.fkType IN ( ". implode(',',array_keys($moFilter->getRiskClassifType())) ." ) ";
		}
	}

	// filtro tipo controle
	$msClassifFilter = "";
	$maClassifFilter = $moFilter->getControlClassifType();
	if(count($maClassifFilter)){
		if(isset($maClassifFilter['null'])){
			$filter[] = " c.fkType IS NULL ";
		}else{
			$filter[] = " c.fkType IN ( ". implode(',',array_keys($moFilter->getControlClassifType())) ." ) ";
		}
	}

  	if($showApprovedAsset)
  		$assetStatusFilter[] = "ctx.nstate = 2702";

  	if($showDeniedAsset)
  		$assetStatusFilter[] = "ctx.nstate = 2703";

  	if($showPendantAsset)
  		$assetStatusFilter[] = "ctx.nstate = 2701";

  	$filters = implode(' AND ', $filter);
  	$assetStatusFilters = "and (" . implode(' OR ', $assetStatusFilter) . ")";

  	$query = array();

  	if(strlen($filters) > 0)
  		$query[] = implode(' AND ', $filter);

  	$where = '';
  	$whereNew = '';

  	if(count($query) > 0){
  		$where = ' WHERE ' . implode(' AND ', $query);
  		$whereNew = ' WHERE ' . implode(' AND ', $filter) . ' and ctx.nstate is null '  . ' order by p.fkcontext, a.fkcontext, r.fkcontext ';
  	}

  	$riskResidualLabel = RMRiskConfig::getRiskLabelSql("r.nValueResidual");

    $this->csQuery = "SELECT
						distinct p.sname as process_name,

						a.sname as asset_name,
						a.tdescription as asset_description,
						ca.sname as asset_category,

                      	(SELECT SUM(vn2.nValue)
                      		FROM view_rm_asset_active a2
                      			LEFT JOIN rm_asset_value av2 ON (a2.fkContext = av2.fkAsset)
                      			LEFT JOIN rm_parameter_value_name vn2 ON (av2.fkValueName = vn2.pkValueName)
                      			WHERE a.fkContext = a2.fkContext
                      			GROUP BY  a.sName) as asset_relavance,

						r.sname as risk_name,
						e.sdescription as risk_event_description,
						r.tdescription as risk_description,

						rvn.simpact || ' / ' || pvn.sriskprobability as risk_estimation,

						{$riskResidualLabel} AS risk_residual,

						c.sname as control_name,
						c.tdescription as control_description,
						c.sevidence as control_evidence,
						cc.sname as control_type,
						bp.sname as control_best_practice,

						p.fkcontext as process_id,
						a.fkcontext as asset_id,
						r.fkcontext as risk_id,
						c.fkcontext as control_id

					FROM view_rm_process_active p

						JOIN view_rm_process_asset_active pa ON (p.fkContext = pa.fkProcess)
						JOIN view_rm_asset_active a ON (pa.fkAsset = a.fkContext)
						JOIN rm_category ca ON ca.fkcontext = a.fkcategory
						JOIN rm_asset_value av ON av.fkasset = a.fkContext and av.fkparametername = 1

						JOIN view_rm_risk_active r ON a.fkContext = r.fkAsset
						LEFT JOIN rm_event e ON r.fkevent = e.fkcontext
						LEFT JOIN rm_risk_value rv ON r.fkcontext = rv.fkrisk and rv.fkparametername = 1
						LEFT JOIN rm_parameter_value_name rvn ON rv.fkvaluename = rvn.pkvaluename
						LEFT JOIN rm_parameter_value_name pvn ON r.fkprobabilityvaluename = pvn.pkvaluename
						LEFT JOIN view_rm_risk_control_active rca ON rca.fkrisk = r.fkContext
						LEFT JOIN view_rm_control_active c ON c.fkcontext = rca.fkcontrol
						LEFT JOIN isms_context_classification cc ON cc.pkclassification = c.fktype
						LEFT JOIN rm_control_best_practice cbp ON cbp.fkcontrol = c.fkcontext
						LEFT JOIN rm_best_practice bp ON bp.fkcontext = fkbestpractice
						JOIN isms_context ctx on (ctx.pkContext = r.fkContext $assetStatusFilters)
					" .
    				$where . " union " .

				 " (SELECT
						distinct p.sname as process_name,

						a.sname as asset_name,
						a.tdescription as asset_description,
						ca.sname as asset_category,

                      	(SELECT SUM(vn2.nValue)
                      		FROM view_rm_asset_active a2
                      			LEFT JOIN rm_asset_value av2 ON (a2.fkContext = av2.fkAsset)
                      			LEFT JOIN rm_parameter_value_name vn2 ON (av2.fkValueName = vn2.pkValueName)
                      			WHERE a.fkContext = a2.fkContext
                      			GROUP BY  a.sName) as asset_relavance,

						r.sname as risk_name,
						e.sdescription as risk_event_description,
						r.tdescription as risk_description,

						rvn.simpact || ' / ' || pvn.sriskprobability as risk_estimation,

						{$riskResidualLabel} AS risk_residual,


						c.sname as control_name,
						c.tdescription as control_description,
						c.sevidence as control_evidence,
						cc.sname as control_type,
						bp.sname as control_best_practice,

						p.fkcontext as process_id,
						a.fkcontext as asset_id,
						r.fkcontext as risk_id,
						c.fkcontext as control_id

					FROM view_rm_process_active p

						LEFT JOIN view_rm_process_asset_active pa ON (p.fkContext = pa.fkProcess)
						LEFT JOIN view_rm_asset_active a ON (pa.fkAsset = a.fkContext)
						LEFT JOIN rm_category ca ON ca.fkcontext = a.fkcategory
						LEFT JOIN rm_asset_value av ON av.fkasset = a.fkContext and av.fkparametername = 1
						LEFT JOIN view_rm_risk_active r ON a.fkContext = r.fkAsset
						LEFT JOIN rm_event e ON r.fkevent = e.fkcontext
						LEFT JOIN rm_risk_value rv ON r.fkcontext = rv.fkrisk and rv.fkparametername = 1
						LEFT JOIN rm_parameter_value_name rvn ON rv.fkvaluename = rvn.pkvaluename
						LEFT JOIN rm_parameter_value_name pvn ON r.fkprobabilityvaluename = pvn.pkvaluename
						LEFT JOIN view_rm_risk_control_active rca ON rca.fkrisk = r.fkContext
						LEFT JOIN view_rm_control_active c ON c.fkcontext = rca.fkcontrol
						LEFT JOIN isms_context_classification cc ON cc.pkclassification = c.fktype
						LEFT JOIN rm_control_best_practice cbp ON cbp.fkcontrol = c.fkcontext
						LEFT JOIN rm_best_practice bp ON bp.fkcontext = fkbestpractice
						LEFT JOIN isms_context ctx on (ctx.pkContext = r.fkContext)
					" .
    				$whereNew . " )";

    return parent::executeQuery();

  }

  /**
   * Desenha o cabeçalho do relatório.
   *
   * <p>Método para desenhar o cabeçalho do relatório.</p>
   * @access public
   */
  public function drawHeader(){

    foreach($this->caHeaders as $moHeader){
      if($moHeader->getAttrName()=='header_filter'){
        $moHeaderFilter = $moHeader;
      }elseif($moHeader->getAttrName()=='header_filter_item'){
        $moHeaderFilterItem = $moHeader;
      }elseif($moHeader->getAttrName()=='report_header_blank'){
        $moHeaderBlank = $moHeader;
      }else{
        $this->coWriter->drawLine($moHeader,array());
      }
    }

    if ($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());

    $moFilterText = FWDWebLib::getObject('header_filter_text');
    $moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
    $maFilters = $this->coFilter->getSummary();

    foreach($maFilters as $maFilter){
      $moFilterText->setValue($maFilter['name']);
      $this->coWriter->drawLine($moHeaderFilter,array());
      foreach($maFilter['items'] as $msFilterItem){
        $moFilterItemText->setValue($msFilterItem);
        $this->coWriter->drawLine($moHeaderFilterItem,array());
      }
    }

    $this->coWriter->drawLine($moHeaderBlank,array());
  }

}
?>