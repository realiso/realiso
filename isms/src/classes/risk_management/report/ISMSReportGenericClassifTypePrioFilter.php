<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportGenericClassifTypePrioFilter.
 *
 * <p>Classe que implementa o filtro do relat�rio de riscos por processo.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportGenericClassifTypePrioFilter extends FWDReportFilter {

  protected $caAreaClassifType = array();

  protected $caProcessClassifType = array();

  protected $caRiskClassifType = array();

  protected $caControlClassifType = array();

  protected $caEventClassifType = array();

  protected $caAreaClassifPriority = array();

  protected $caProcessClassifPriority = array();
  
  public function getAreaClassifType(){
    return $this->caAreaClassifType;
  }

  public function setAreaClassifType($paAreaClassifType){
    $this->caAreaClassifType = $paAreaClassifType;
  }

  public function getProcessClassifType(){
    return $this->caProcessClassifType;
  }

  public function setProcessClassifType($paProcessClassifType){
    $this->caProcessClassifType = $paProcessClassifType;
  }

  public function getRiskClassifType(){
    return $this->caRiskClassifType;
  }

  public function setRiskClassifType($paRiskClassifType){
    $this->caRiskClassifType = $paRiskClassifType;
  }

  public function getControlClassifType(){
    return $this->caControlClassifType;
  }

  public function setControlClassifType($paControlClassifType){
    $this->caControlClassifType = $paControlClassifType;
  }

  public function getEventClassifType(){
    return $this->caEventClassifType;
  }

  public function setEventClassifType($paEventClassifType){
    $this->caEventClassifType = $paEventClassifType;
  }

  public function getAreaClassifPriority(){
    return $this->caAreaClassifPriority;
  }

  public function setAreaClassifPriority($paAreaClassifPriority){
    $this->caAreaClassifPriority = $paAreaClassifPriority;
  }

  public function getProcessClassifPriority(){
    return $this->caProcessClassifPriority;
  }

  public function setProcessClassifPriority($paProcessClassifPriority){
    $this->caProcessClassifPriority = $paProcessClassifPriority;
  }
  
  public function getSummary(){    
    
    $maGets = array();
    $maGets[] = "AreaClassifType";
    $maGets[] = "AreaClassifPriority";
    $maGets[] = "ProcessClassifType";
    $maGets[] = "ProcessClassifPriority";
    $maGets[] = "RiskClassifType";
    $maGets[] = "ControlClassifType";
    $maGets[] = "EventClassifType";
    
    $maLabels = array();

    $maLabels ["AreaClassifType"] = FWDLanguage::getPHPStringValue('lb_area_types_cl', "Tipos de �rea:");
    $maLabels ["ProcessClassifType"] = FWDLanguage::getPHPStringValue('lb_process_types_cl', "Tipos de Processo:");
    $maLabels ["RiskClassifType"] = FWDLanguage::getPHPStringValue('lb_risk_types_cl', "Tipos de Risco:");
    $maLabels ["ControlClassifType"] = FWDLanguage::getPHPStringValue('lb_control_types_cl', "Tipos de Controle:");
    $maLabels ["EventClassifType"] = FWDLanguage::getPHPStringValue('lb_event_types_cl', "Tipos de Evento:");
    $maLabels ["AreaClassifPriority"] = FWDLanguage::getPHPStringValue('lb_area_priorities_cl', "Prioridades de �rea:");
    $maLabels ["ProcessClassifPriority"] = FWDLanguage::getPHPStringValue('lb_process_priorities_cl', "Prioridades de Processo:"); 
    
    $maFilters = array();
    
    foreach($maGets as $msGet){
      $maCtxClassif = $this->{"get".$msGet}();
      if(count($maCtxClassif)){
        $maFilters[] = array(
          'name' => $maLabels[$msGet],
          'items' => $maCtxClassif
        );
      }
    }
    return $maFilters;
  }
}
?>