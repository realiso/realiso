<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportRisksResumeFilter.
 *
 * <p>Classe que implementa o filtro do relat�rio de resumo dos riscos.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportRisksResumeFilter extends FWDReportFilter {

	protected $caAreaClassifType = array();
	protected $caProcessClassifType = array();
	protected $caRiskClassifType = array();

	protected $caAreaClassifPriority = array();
	protected $caProcessClassifPriority = array();
	
	protected $caAreaId;
	protected $caProcessId;
	protected $caAssetId;
	
	// area id
	public function setArea($paAreaId){
		$this->caAreaId = $paAreaId;
	}
	
	public function getArea(){
		return $this->caAreaId;
	}
	
	// process id
	public function setProcess($paProcessId){
		$this->caProcessId = $paProcessId;
	}
	
	public function getProcess(){
		return $this->caProcessId;
	}	
	
	// asset id
	public function setAsset($paAssetId){
		$this->caAssetId = $paAssetId;
	}
	
	public function getAsset(){
		return $this->caAssetId;
	}	

	
	// area
	public function setAreaClassifType($paAreaClassifType){
		$this->caAreaClassifType = $paAreaClassifType;
	}

	public function getAreaClassifType(){
		return $this->caAreaClassifType;
	}

	// process
	public function setProcessClassifType($paProcessClassifType){
		$this->caProcessClassifType = $paProcessClassifType;
	}

	public function getProcessClassifType(){
		return $this->caProcessClassifType;
	}

	// risco
	public function setRiskClassifType($paRiskClassifType){
		$this->caRiskClassifType = $paRiskClassifType;
	}

	public function getRiskClassifType(){
		return $this->caRiskClassifType;
	}

	// prioridade
	public function setAreaClassifPriority($paAreaClassifPriority){
		$this->caAreaClassifPriority = $paAreaClassifPriority;
	}

	public function getAreaClassifPriority(){
		return $this->caAreaClassifPriority;
	}

	public function setProcessClassifPriority($paProcessClassifPriority){
		$this->caProcessClassifPriority = $paProcessClassifPriority;
	}

	public function getProcessClassifPriority(){
		return $this->caProcessClassifPriority;
	}

	public function getSummary(){
		$maFilters = array();
		
		// Area
		if($this->caAreaId){
			$moArea = new RMArea();
			$moArea->fetchById($this->caAreaId);
			$maFilterArea[] = $moArea->getName();
		}else{
			$maFilterArea[] = FWDLanguage::getPHPStringValue('cb_all', 'Todos');
		}
		
		$maFilters[] = array(
	        'name' => FWDLanguage::getPHPStringValue('lb_area_cl', '�rea:'),
	        'items' => $maFilterArea
		);
		
		// Processo
		if($this->caProcessId){
			$moProcess = new RMProcess();
			$moProcess->fetchById($this->caProcessId);
			$maFilterProcess[] = $moProcess->getName();
		}else{
			$maFilterProcess[] = FWDLanguage::getPHPStringValue('cb_all', 'Todos');
		}
		
		$maFilters[] = array(
	        'name' => FWDLanguage::getPHPStringValue('lb_department', 'Processo:'),
	        'items' => $maFilterProcess
		);

		// Ativo
		if($this->caAssetId){
			$moAsset = new RMAsset();
			$moAsset->fetchById($this->caAssetId);
			$maFilterAsset[] = $moAsset->getName();
		}else{
			$maFilterAsset[] = FWDLanguage::getPHPStringValue('cb_all', 'Todos');
		}
		
		$maFilters[] = array(
	        'name' => FWDLanguage::getPHPStringValue('lb_asset_cl', 'Ativo:'),
	        'items' => $maFilterAsset
		);		

		//
		$maAreaClassifType = $this->getAreaClassifType();
		if(count($maAreaClassifType)){
			$maFilters[] = array(
        'name' => FWDLanguage::getPHPStringValue('lb_area_types_cl', 'Tipos de Area:'),
        'items' => $maAreaClassifType
			);
		}

		$maProcessClassifType = $this->getProcessClassifType();
		if(count($maProcessClassifType)){
			$maFilters[] = array(
        'name' => FWDLanguage::getPHPStringValue('lb_process_types_cl', 'Tipos de Processo:'),
        'items' => $maProcessClassifType
			);
		}

		$maRiskClassifType = $this->getRiskClassifType();
		if(count($maRiskClassifType)){
			$maFilters[] = array(
        'name' => FWDLanguage::getPHPStringValue('rs_risk_types_cl', 'Tipos de Risco:'),
        'items' => $maRiskClassifType
			);
		}

		$maAreaClassifPriority = $this->getAreaClassifPriority();
		if(count($maAreaClassifPriority)){
			$maFilters[] = array(
        'name' => FWDLanguage::getPHPStringValue('lb_area_priorities_cl', 'Prioridades de Area:'),
        'items' => $maAreaClassifPriority
			);
		}

		$maProcessClassifPriority = $this->getProcessClassifPriority();
		if(count($maProcessClassifPriority)){
			$maFilters[] = array(
        'name' => FWDLanguage::getPHPStringValue('lb_process_priorities_cl', 'Prioridades de Processo:'),
        'items' => $maProcessClassifPriority
			);
		}

		return $maFilters;
	}
}
?>