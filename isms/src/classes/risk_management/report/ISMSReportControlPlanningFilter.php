<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportControlPlanningFilter.
 *
 * <p>Classe que implementa o filtro do relatório de planejamento dos controles.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportControlPlanningFilter extends FWDReportFilter {
	
	protected $cbShowOnlyPlanned = false;
	protected $cbShowOnlyImplemented = false;	
  
  protected $caControlClassifType = array();
			
  public function getControlClassifType(){
    return $this->caControlClassifType;
  }

  public function setControlClassifType($psControlClassifType){
    $this->caControlClassifType = $psControlClassifType;
  }

  
  public function showOnlyPlanned() {
		$this->cbShowOnlyPlanned = true;
	}
	
	public function showOnlyImplemented() {
		$this->cbShowOnlyImplemented = true;
	}
	
	public function mustShowOnlyPlanned() {
		return $this->cbShowOnlyPlanned;
	}
	
	public function mustShowOnlyImplemented() {
		return $this->cbShowOnlyImplemented;
	}
  
  public function getSummary(){		
    $maControlClassifType = $this->getControlClassifType();
    $maFilters = array();
    if(count($maControlClassifType)){
      $maFilters[] = array(
        'name' => FWDLanguage::getPHPStringValue('rs_control_types_cl', 'Tipos de Controle:'),
        'items' => $maControlClassifType
      );
    }
    return $maFilters;
  }
  
  
}
?>