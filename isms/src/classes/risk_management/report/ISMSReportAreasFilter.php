<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportAreasFilter.
 *
 * <p>Classe que implementa o filtro do relat�rio de �reas.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportAreasFilter extends FWDReportFilter {

  protected $csValue = '';

  public function setValue($psValue){
    $this->csValue = $psValue;
  }

  public function getValue(){
    return $this->csValue;
  }

  public function getSummary(){
    
    $maFilters = array();
    
    if($this->csValue){
      switch($this->csValue){
        case 'high': $msValueLabel = FWDLanguage::getPHPStringValue('rs_high',"Alto");                  break;
        case 'mid' : $msValueLabel = FWDLanguage::getPHPStringValue('rs_medium',"M�dio");               break;
        case 'low' : $msValueLabel = FWDLanguage::getPHPStringValue('rs_low',"Baixo");                  break;
        case 'np'  : $msValueLabel = FWDLanguage::getPHPStringValue('rs_not_estimated',"N�o Estimado"); break;
      }
      $maFilters[] = array(
        'name' => FWDLanguage::getPHPStringValue('rs_value_cl','Risco:'),
        'items' => array($msValueLabel)
      );
    }
    
    return $maFilters;
  }

}

?>