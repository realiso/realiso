<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

define('CONTROL_IS_PLANNED', 2401);
define('CONTROL_IS_IMPLEMENTED', 2402);

/**
 * Classe ISMSReportControlPlanning.
 *
 * <p>Classe que implementa o relat�rio de planejamento dos controles.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportControlPlanning extends ISMSReport {
	
	/**
	 * Inicializa o relat�rio.
	 *
	 * <p>M�todo para inicializar o relat�rio.
	 * Necess�rio para funcionar com o XML compilado.</p>
	 * @access public
	 */	
	public function init() {
		parent::init();
		
		$col1 = new FWDDBField("","is_planned",DB_NUMBER);
		$col1->setLabel(FWDLanguage::getPHPStringValue('rs_planned_controls','Controles Planejados'));
		$col1->setShowColumn(true);		
		
		$col2 = new FWDDBField("","control_id",DB_NUMBER);
		$col2->setLabel(FWDLanguage::getPHPStringValue('',''));
		$col2->setShowColumn(false);				

		$col3 = new FWDDBField("","control_name",DB_STRING);
		$col3->setLabel(FWDLanguage::getPHPStringValue('rs_implemented_controls','Controles Implementados'));
		$col3->setShowColumn(true);				

		$col4 = new FWDDBField("","control_is_active",DB_NUMBER);
		$col4->setLabel(' ');
		$col4->setShowColumn(false);		

		$col5 = new FWDDBField("","control_deadline",DB_DATETIME);
		$col5->setLabel(FWDLanguage::getPHPStringValue('rs_implementation_deadline', 'Prazo de Implementa��o'));
		$col5->setShowColumn(true);				

		$col6 = new FWDDBField("","control_date_implemented",DB_DATETIME);
		$col6->setLabel(FWDLanguage::getPHPStringValue('rs_implementation_date', 'Data de Implementa��o'));
		$col6->setShowColumn(true);		
		
		$col7 = new FWDDBField("","responsible_id",DB_NUMBER);
		$col7->setLabel(' ');
		$col7->setShowColumn(false);				

		$col8 = new FWDDBField("","responsible_name",DB_STRING);
		$col8->setLabel(FWDLanguage::getPHPStringValue('rs_responsible','Respons�vel'));
		$col8->setShowColumn(true);				

		$this->coDataSet->addFWDDBField($col1);
		$this->coDataSet->addFWDDBField($col2);		
		$this->coDataSet->addFWDDBField($col3);
		$this->coDataSet->addFWDDBField($col4);
		$this->coDataSet->addFWDDBField($col5);
		$this->coDataSet->addFWDDBField($col6);
		$this->coDataSet->addFWDDBField($col7);
		$this->coDataSet->addFWDDBField($col8);
	}
	
	/**
	 * Executa a query do relat�rio.
	 *
	 * <p>M�todo para executar a query do relat�rio.</p>
	 * @access public
	 */
	public function makeQuery() {

    $moFilter = $this->getFilter();
    $maFilters = array();
    $msWhere ="";

    $maClassifType = $moFilter->getControlClassifType();
    
    if(count($maClassifType)){
      if(isset($maClassifType['null'])){
        $maFilters[] = " c.fkType IS NULL ";
      }else{
        $maFilters[] = " c.fkType IN ( ".implode(',',array_keys($maClassifType)) ." ) ";
      }
    }

    if(count($maFilters)){
        $msWhere .= " AND ". implode(' AND ',$maFilters);
    }
    
		$msSelectPlanned = "SELECT " . CONTROL_IS_PLANNED . " as is_planned,
													c.fkContext as control_id, c.sName as control_name, c.bIsActive as control_is_active,
													c.dDateDeadline as control_deadline, c.dDateImplemented as control_date_implemented,
													u.fkContext as responsible_id, u.sName as responsible_name
												FROM view_rm_control_active c
                        JOIN view_isms_user_active u ON (c.fkResponsible = u.fkContext)
												WHERE c.dDateImplemented IS NULL
                        $msWhere"; 
		
		$msSelectImplemented = "SELECT " . CONTROL_IS_IMPLEMENTED . " as is_planned,
															c.fkContext as control_id, c.sName as control_name, c.bIsActive as control_is_active,
															c.dDateDeadline as control_deadline, c.dDateImplemented as control_date_implemented,
															u.fkContext as responsible_id, u.sName as responsible_name
														FROM view_rm_control_active c
                            JOIN view_isms_user_active u ON (c.fkResponsible = u.fkContext)
														WHERE c.dDateImplemented IS NOT NULL
                            $msWhere";
		

		if ($moFilter->mustShowOnlyPlanned()) $this->csQuery = $msSelectPlanned;
		else if ($moFilter->mustShowOnlyImplemented()) $this->csQuery = $msSelectImplemented;		
		else $this->csQuery = $msSelectPlanned . ' UNION ' . $msSelectImplemented;
		
		$this->csQuery .= ' ORDER BY is_planned ASC';
		
		return parent::executeQuery();
	}
  
  /**
   * Desenha o cabe�alho do relat�rio.
   *
   * <p>M�todo para desenhar o cabe�alho do relat�rio.</p>
   * @access public
   */
  public function drawHeader(){
    foreach($this->caHeaders as $moHeader){
      if($moHeader->getAttrName()=='header_filter'){
        $moHeaderFilter = $moHeader;
      }elseif($moHeader->getAttrName()=='header_filter_item'){
        $moHeaderFilterItem = $moHeader;
      }elseif($moHeader->getAttrName()=='report_header_blank'){
        $moHeaderBlank = $moHeader;
      }else{
        $this->coWriter->drawLine($moHeader,array());
      }
    }
    if ($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());
    
    $moFilterText = FWDWebLib::getObject('header_filter_text');
    $moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
    $maFilters = $this->coFilter->getSummary();
    foreach($maFilters as $maFilter){
      $moFilterText->setValue($maFilter['name']);
      $this->coWriter->drawLine($moHeaderFilter,array());
      foreach($maFilter['items'] as $msFilterItem){
        $moFilterItemText->setValue($msFilterItem);
        $this->coWriter->drawLine($moHeaderFilterItem,array());
      }
    }
    $this->coWriter->drawLine($moHeaderBlank,array());
  }
  
}

?>