<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportControlsFilter.
 *
 * <p>Classe que implementa o filtro do relat�rio de controles.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportControlsFilter extends FWDReportFilter {

  const PLANNED_IMPLANTATION              = 1;
  const PLANNED_LATE                      = 2;
  const PLANNED_TOTAL                     = 3;
  const IMPLEMENTED_TRUSTED_AND_EFFICIENT = 4;
  const CONTROL_TEST_NOT_OK               = 5;
  const CONTROL_REVISION_NOT_OK           = 6;
  const CONTROL_NOT_MEASURED              = 7;
  const IMPLEMENTED_TOTAL                 = 8;
  
  protected $ciImplementationStatus = 0;

  public function setImplementationStatus($piImplementationStatus){
    $this->ciImplementationStatus = $piImplementationStatus;
  }

  public function getImplementationStatus(){
    return $this->ciImplementationStatus;
  }

  public function getSummary(){
    
    $maFilters = array();
    
    if($this->ciImplementationStatus){
      switch($this->ciImplementationStatus){
        //case self::: $msText = FWDLanguage::getPHPStringValue('rs_implemented_controls','Controles Implementados'); break;
        case self::PLANNED_IMPLANTATION             : $msText = FWDLanguage::getPHPStringValue('rs_under_implementation','Em processo de implanta��o'); break;
        case self::PLANNED_LATE                     : $msText = FWDLanguage::getPHPStringValue('rs_delayed','Atrasados'); break;
        case self::PLANNED_TOTAL                    : $msText = FWDLanguage::getPHPStringValue('rs_planned_controls','Controles Planejados'); break;
        case self::IMPLEMENTED_TRUSTED_AND_EFFICIENT: $msText = FWDLanguage::getPHPStringValue('rs_trusted_and_efficient_controls','Controles confi�veis e eficientes'); break;
        case self::CONTROL_TEST_NOT_OK              : $msText = FWDLanguage::getPHPStringValue('rs_not_trusted_controls','Controles n�o confi�veis'); break;
        case self::CONTROL_REVISION_NOT_OK          : $msText = FWDLanguage::getPHPStringValue('rs_inefficient_controls','Controles n�o eficientes'); break;
        case self::CONTROL_NOT_MEASURED             : $msText = FWDLanguage::getPHPStringValue('rs_not_measured_controls','Controles n�o medidos'); break;
        case self::IMPLEMENTED_TOTAL                : $msText = FWDLanguage::getPHPStringValue('rs_implemented_controls','Controles Implementados'); break;
      }
      $maFilters[] = array(
        'name' => FWDLanguage::getPHPStringValue('rs_implementationstatus_cl','Exibir Apenas:'),
        'items' => array($msText)
      );
    }
    
    return $maFilters;
  }

}

?>