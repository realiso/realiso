<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportRisksByAreaFilter.
 *
 * <p>Classe que implementa o filtro do relat�rio de riscos por �rea.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportRisksByAreaFilter extends ISMSReportRiskValuesFilter {

	protected $cbOrganizeByProcess = false;
	
	protected $cbOrganizeByAsset = false;
	
	protected $ciAreaId = 0;
	
  protected $caProcessClassifType = array();
  
  protected $caProcessClassifPriority = array();

  protected $caAreaClassifType = array();
  
  protected $caAreaClassifPriority = array();

  
	public function organizeByProcess() {
		$this->cbOrganizeByProcess = true;
	}
	
	public function mustOrganizeByProcess() {
		return $this->cbOrganizeByProcess;
	}
	
	public function organizeByAsset() {
		$this->cbOrganizeByAsset = true;
	}
	
	public function mustOrganizeByAsset() {
		return $this->cbOrganizeByAsset;
	}
	
	public function setArea($piAreaId) {
		$this->ciAreaId = $piAreaId;
	}
	
	public function getArea() {
		return $this->ciAreaId;
	}
  
  public function getProcessClassifType(){
    return $this->caProcessClassifType;
  }

  public function setProcessClassifType($paProcessClassifType){
    $this->caProcessClassifType = $paProcessClassifType;
  }

  public function getProcessClassifPriority(){
    return $this->caProcessClassifPriority;
  }

  public function setProcessClassifPriority($paProcessClassifPriority){
    $this->caProcessClassifPriority = $paProcessClassifPriority;
  }

  public function getAreaClassifType(){
    return $this->caAreaClassifType;
  }

  public function setAreaClassifType($paAreaClassifType){
    $this->caAreaClassifType = $paAreaClassifType;
  }

  public function getAreaClassifPriority(){
    return $this->caAreaClassifPriority;
  }

  public function setAreaClassifPriority($paAreaClassifPriority){
    $this->caAreaClassifPriority = $paAreaClassifPriority;
  }
  
	public function getSummary(){		
		$maFilters = parent::getSummary();
		
		$maFilterArea = array();
		if ($this->ciAreaId){      
      $moArea = new RMArea();
    	$moArea->fetchById($this->ciAreaId);
      $maFilterArea[] = $moArea->getName();
    }else{
    	$maFilterArea[] = FWDLanguage::getPHPStringValue('st_all', 'Todas');
    }
    $maFilter = array(
      'name' => FWDLanguage::getPHPStringValue('rs_area_cl', '�rea:'),
      'items' => $maFilterArea
    );
    
		   
    $maAreaClassifType = $this->getAreaClassifType();
    if(count($maAreaClassifType)){
      $maFilters[] = array(
        'name' => FWDLanguage::getPHPStringValue('lb_area_types_cl', 'Tipos de Area:'),
        'items' => $maAreaClassifType
      );
    }

    $maAreaClassifPriority = $this->getAreaClassifPriority();
    if(count($maAreaClassifPriority)){
      $maFilters[] = array(
        'name' => FWDLanguage::getPHPStringValue('lb_area_priorities_cl', 'Prioridades de Area:'),
        'items' => $maAreaClassifPriority
      );
    }

    $maProcessClassifType = $this->getProcessClassifType();
    if(count($maProcessClassifType)){
      $maFilters[] = array(
        'name' => FWDLanguage::getPHPStringValue('lb_process_types_cl', 'Tipos de Processo:'),
        'items' => $maProcessClassifType
      );
    }

    $maProcessClassifPriority = $this->getProcessClassifPriority();
    if(count($maProcessClassifPriority)){
      $maFilters[] = array(
        'name' => FWDLanguage::getPHPStringValue('lb_process_priorities_cl', 'Prioridades de Processo:'),
        'items' => $maProcessClassifPriority
      );
    }

    
    
    
    array_unshift($maFilters, $maFilter);    
    
    return $maFilters;
  }
}
?>