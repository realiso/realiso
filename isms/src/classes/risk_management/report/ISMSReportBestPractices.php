<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportBestPractices.
 *
 * <p>Classe que implementa o relatório de melhores práticas.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportBestPractices extends ISMSFilteredReport {
  
  public function __construct(){
    parent::__construct();
  }
  
 /**
  * Inicializa o relatório.
  *
  * <p>Método para inicializar o relatório.
  * Necessário para funcionar com o XML compilado.</p>
  * @access public
  */
  public function init(){
    parent::init();
    $this->coDataSet->addFWDDBField(new FWDDBField('bp.fkContext','best_practice_id'  ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('bp.sName'    ,'best_practice_name',DB_STRING));
  }
  
 /**
  * Executa a query do relatório.
  *
  * <p>Método para executar a query do relatório.</p>
  * @access public
  */
  public function makeQuery(){
    $miStandard = $this->coFilter->getStandard();
    $mbOnlyApplied = $this->coFilter->getOnlyApplied();
    
    $maFilters = array();
    
    if($miStandard){
      $maFilters[] = "bps.fkStandard = {$miStandard}";
    }
    
    if($mbOnlyApplied){
      $maFilters[] = "EXISTS ( SELECT * FROM view_rm_control_bp_active cbp WHERE cbp.fkBestPractice = bp.fkContext )";
    }
    
    if(count($maFilters)==0){
      $msWhere = '';
    }else{
      $msWhere = ' WHERE '.implode(' AND ',$maFilters);
    }
    $this->csQuery = "SELECT
                        bp.fkContext AS best_practice_id,
                        bp.sName AS best_practice_name
                      FROM
                        view_rm_best_practice_active bp
                        JOIN view_rm_bp_standard_active bps ON (bps.fkBestPractice = bp.fkContext)
                      {$msWhere}
                      ORDER BY bp.sName";
    return parent::executeQuery();
  }

}

?>