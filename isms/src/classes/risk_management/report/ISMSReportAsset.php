<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe ISMSReportAsset.
 *
 * <p>Classe que implementa o relat�rio de ativos.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportAsset extends ISMSReport {  
  
  /**
   * Inicializa o relat�rio.
   *
   * <p>M�todo para inicializar o relat�rio.
   * Necess�rio para funcionar com o XML compilado.</p>
   * @access public
   */  
  public function init() {
    parent::init();    

	$col1 = new FWDDBField("","area_id",DB_NUMBER);
	$col1->setLabel(' ');
	$col1->setShowColumn(false);
	
	$col2 = new FWDDBField("","area_name",DB_STRING);
	$col2->setLabel(FWDLanguage::getPHPStringValue('cb_area','�rea'));
	$col2->setShowColumn(true);		

	$col3 = new FWDDBField("","area_value",DB_NUMBER);
	$col3->setLabel(' ');
	$col3->setShowColumn(false);
	
	$col4 = new FWDDBField("","process_id",DB_NUMBER);
	$col4->setLabel(' ');
	$col4->setShowColumn(false);		
	
	$col5 = new FWDDBField("","process_name",DB_STRING);
	$col5->setLabel(FWDLanguage::getPHPStringValue('lb_department','Processo'));
	$col5->setShowColumn(true);
	
	$col6 = new FWDDBField("","process_value",DB_NUMBER);
	$col6->setLabel(' ');
	$col6->setShowColumn(false);		
	
	$col7 = new FWDDBField("","asset_id",DB_NUMBER);
	$col7->setLabel(' ');
	$col7->setShowColumn(false);
	
	$col8 = new FWDDBField("","asset_name",DB_STRING);
	$col8->setLabel(FWDLanguage::getPHPStringValue('rs_asset','Ativo'));
	$col8->setShowColumn(true);	    
    
	$col9 = new FWDDBField("","asset_value",DB_NUMBER);
	$col9->setLabel(' ');
	$col9->setShowColumn(false);    
    
	$col10 = new FWDDBField("","responsible_id",DB_NUMBER);
	$col10->setLabel(' ');
	$col10->setShowColumn(false);       
	
	$col11 = new FWDDBField("","responsible_name",DB_STRING);
	$col11->setLabel(FWDLanguage::getPHPStringValue('rs_responsible','Respons�vel'));
	$col11->setShowColumn(true);	

    $this->coDataSet->addFWDDBField($col1);    
    $this->coDataSet->addFWDDBField($col2);
    $this->coDataSet->addFWDDBField($col3);
    $this->coDataSet->addFWDDBField($col4);
    $this->coDataSet->addFWDDBField($col5);    
    $this->coDataSet->addFWDDBField($col6);        
    $this->coDataSet->addFWDDBField($col7);
    $this->coDataSet->addFWDDBField($col8);
    $this->coDataSet->addFWDDBField($col9);
    $this->coDataSet->addFWDDBField($col10);
    $this->coDataSet->addFWDDBField($col11);
  }
  
  /**
   * Executa a query do relat�rio.
   *
   * <p>M�todo para executar a query do relat�rio.</p>
   * @access public
   */
  public function makeQuery() {
    $moFilter = $this->getFilter();
    $msAreaType = "";
    $msProcessType = "";
    $msControlType = "";
    $msAreaPriority = "";
    $msProcessPriority = "";
    $msProcess = "";
    $msArea = "";
    $msAreaProcess = "";
    

    $maClassifType = $moFilter->getAreaClassifType();
    if(count($maClassifType)){
      if(isset($maClassifType['null'])){
        $msAreaType = " ar.fkType IS NULL ";
      }else{
        $msAreaType = " ar.fkType IN ( ".implode(',',array_keys($maClassifType)) ." ) ";
      }
    }
    $maClassifType = $moFilter->getProcessClassifType();
    if(count($maClassifType)){
      if(isset($maClassifType['null'])){
        $msProcessType = " p.fkType IS NULL ";
      }else{
        $msProcessType = " p.fkType IN ( ".implode(',',array_keys($maClassifType)) ." ) ";
      }
    }
    $maClassifType = $moFilter->getAreaClassifPriority();
    if(count($maClassifType)){
      if(isset($maClassifType['null'])){
        $msAreaPriority = " ar.fkPriority IS NULL ";
      }else{
        $msAreaPriority = " ar.fkPriority IN ( ".implode(',',array_keys($maClassifType)) ." ) ";
      }
    }
    $maClassifType = $moFilter->getProcessClassifPriority();
    if(count($maClassifType)){
      if(isset($maClassifType['null'])){
        $msProcessPriority = " p.fkPriority IS NULL ";
      }else{
        $msProcessPriority = " p.fkPriority IN ( ".implode(',',array_keys($maClassifType)) ." ) ";
      }
    }

    if($msAreaType && $msAreaPriority)
      $msArea = $msAreaType ." AND ". $msAreaPriority;
    else
      if($msAreaType || $msAreaPriority){
        $msArea =  $msAreaType . $msAreaPriority;
      }
    if($msProcessType && $msProcessPriority)
      $msProcess = $msProcessType ." AND ". $msProcessPriority;
    else{
      if($msProcessType || $msProcessPriority)
        $msProcess = $msProcessType . $msProcessPriority;
    }
    
    switch ($moFilter->getReportType()) {
      
      case 'report_asset_by_area_and_process':
        if($msArea && $msProcess){
          $msAreaProcess = $msArea . " AND ". $msProcess;
        }else{
          $msAreaProcess = $msArea . $msProcess;
        }
        $this->csQuery = "SELECT ar.fkContext as area_id, ar.sName as area_name, ar.nValue as area_value,
                            p.fkContext as process_id, p.sName as process_name, p.nValue as process_value,
                            a.fkContext as asset_id, a.sName as asset_name, a.nValue as asset_value,
                            u.fkContext as responsible_id, u.sName as responsible_name
                          FROM isms_context cont 
                          JOIN " . FWDWebLib::getFunctionCall("get_area_tree(0,0)") . " gat ON (cont.pkContext = gat.fkContext)
                          JOIN view_rm_area_active ar ON (gat.fkContext = ar.fkContext)
                          JOIN view_rm_process_active p ON (gat.fkContext = p.fkArea)
                          JOIN view_rm_process_asset_active pa ON (p.fkContext = pa.fkProcess)
                          JOIN view_rm_asset_active a ON (pa.fkAsset = a.fkContext)
                          JOIN view_isms_user_active u ON (a.fkResponsible = u.fkContext)
                          " . ((trim($msAreaProcess) != "") ? " WHERE " . $msAreaProcess : ""). "
                          ORDER BY gat.pkId, ar.fkContext, p.fkContext";
      break;
      
      case 'report_asset_by_area':
        $this->csQuery = "SELECT ar.fkContext as area_id, ar.sName as area_name, ar.nValue as area_value,
                            a.fkContext as asset_id, a.sName as asset_name, a.nValue as asset_value,
                            u.fkContext as responsible_id, u.sName as responsible_name
                          FROM isms_context cont 
                          JOIN " . FWDWebLib::getFunctionCall("get_area_tree(0,0)") . " gat ON (cont.pkContext = gat.fkContext)
                          JOIN view_rm_area_active ar ON (gat.fkContext = ar.fkContext)
                          JOIN view_rm_process_active p ON (gat.fkContext = p.fkArea)
                          JOIN view_rm_process_asset_active pa ON (p.fkContext = pa.fkProcess)
                          JOIN view_rm_asset_active a ON (pa.fkAsset = a.fkContext)
                          JOIN view_isms_user_active u ON (a.fkResponsible = u.fkContext)
                          " . ((trim($msArea) != "") ? " WHERE " . $msArea : ""). "
                          ORDER BY gat.pkId, ar.fkContext, p.fkContext";
      break;
      
      case 'report_asset_by_process':
        $this->csQuery = "SELECT p.fkContext as process_id, p.sName as process_name, p.nValue as process_value,
                            a.fkContext as asset_id, a.sName as asset_name, a.nValue as asset_value,
                            u.fkContext as responsible_id, u.sName as responsible_name
                          FROM view_rm_process_active p
                          JOIN view_rm_process_asset_active pa ON (p.fkContext = pa.fkProcess)
                          JOIN view_rm_asset_active a ON (pa.fkAsset = a.fkContext)
                          JOIN view_isms_user_active u ON (a.fkResponsible = u.fkContext)
                          " . ((trim($msProcess) != "") ? " WHERE " . $msProcess : ""). "
                          ORDER BY p.fkContext";
      break;
      
      case 'report_asset_by_asset':
        $this->csQuery = "SELECT a.fkContext as asset_id, a.sName as asset_name, a.nValue as asset_value,
                            u.fkContext as responsible_id, u.sName as responsible_name
                          FROM view_rm_asset_active a
                          JOIN isms_user u ON (a.fkResponsible = u.fkContext)";
      break;
    }

    return parent::executeQuery();
  }
  
 /**
   * Desenha o cabe�alho do relat�rio.
   *
   * <p>M�todo para desenhar o cabe�alho do relat�rio.</p>
   * @access public
   */
  public function drawHeader(){
    foreach($this->caHeaders as $moHeader){
      if($moHeader->getAttrName()=='header_filter'){
        $moHeaderFilter = $moHeader;
      }elseif($moHeader->getAttrName()=='header_filter_item'){
        $moHeaderFilterItem = $moHeader;
      }elseif($moHeader->getAttrName()=='report_header_blank'){
        $moHeaderBlank = $moHeader;
      }else{
        $this->coWriter->drawLine($moHeader,array());
      }
    }
    if ($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());
    
    $moFilterText = FWDWebLib::getObject('header_filter_text');
    $moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
    $maFilters = $this->coFilter->getSummary();
    foreach($maFilters as $maFilter){
      $moFilterText->setValue($maFilter['name']);
      $this->coWriter->drawLine($moHeaderFilter,array());
      foreach($maFilter['items'] as $msFilterItem){
        $moFilterItemText->setValue($msFilterItem);
        $this->coWriter->drawLine($moHeaderFilterItem,array());
      }
    }
    $this->coWriter->drawLine($moHeaderBlank,array());
  }
}
?>