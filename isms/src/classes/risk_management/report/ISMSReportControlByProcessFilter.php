<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportControlByProcessFilter.
 *
 * <p>Classe que implementa o filtro do relatório geral de riscos.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportControlByProcessFilter extends FWDReportFilter {
	
	protected $ciProcessId = 0;
	protected $ciAssetId = 0;
	
	protected $ciShowApprovedAsset = false;
	protected $ciShowDeniedAsset = false;
	protected $ciShowPendantAsset = false;
	
	protected $caRiskClassifType = array();	
	protected $caControlClassifType = array();	
	
	public function setProcess($pbProcessId) {
		$this->ciProcessId = $pbProcessId;
	}
	  
	public function setAsset($pbAssetId) {
		$this->ciAssetId = $pbAssetId;
	}

	public function setShowApprovedAsset($value) {
		$this->ciShowApprovedAsset = $value;
	}

	public function setShowDeniedAsset($value) {
		$this->ciShowDeniedAsset = $value;
	}

	public function setShowPendantAsset($value) {
		$this->ciShowPendantAsset = $value;
	}
	
	public function setRiskClassifType($paRiskClassifType){
		$this->caRiskClassifType = $paRiskClassifType;
	}	

	public function setControlClassifType($paControlClassifType){
		$this->caControlClassifType = $paControlClassifType;
	}		
	
	public function getProcess() {
		return $this->ciProcessId;
	}	
	
	public function getAsset() {
		return $this->ciAssetId;
	}
	
	public function getShowApprovedAsset() {
		return $this->ciShowApprovedAsset;
	}  
	
	public function getShowDeniedAsset() {
		return $this->ciShowDeniedAsset;
	}  
	  
	public function getShowPendantAsset() {
		return $this->ciShowPendantAsset;
	}  
  
	public function getRiskClassifType(){
		return $this->caRiskClassifType;
	}	

	public function getControlClassifType(){
		return $this->caControlClassifType;
	}

	public function getSummary(){		
		$maFilters = array();
		
		$maRiskClassifType = $this->getRiskClassifType();
		if(count($maRiskClassifType)){
			$maFilters[] = array(
				'name' => FWDLanguage::getPHPStringValue('rs_risk_types_cl', 'Tipos de Risco:'),
				'items' => $maRiskClassifType
			);
		}		
		
		$maControlClassifType = $this->getControlClassifType();
		if(count($maControlClassifType)){
			$maFilters[] = array(
				'name' => FWDLanguage::getPHPStringValue('rs_control_types_cl', 'Tipos de Controle:'),
				'items' => $maControlClassifType
			);
		}
		
		return $maFilters;
	}	
}
?>