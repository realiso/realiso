<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe ISMSReportAreasByType.
 *
 * <p>Classe que implementa o relat�rio de �reas por tipo.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportAreasByType extends ISMSReport {
  
  /**
   * Inicializa o relat�rio.
   *
   * <p>M�todo para inicializar o relat�rio.
   * Necess�rio para funcionar com o XML compilado.</p>
   * @access public
   */  
  public function init() {
    parent::init();
    
	$col1 = new FWDDBField("","class_id",DB_NUMBER);
	$col1->setLabel(' ');
	$col1->setShowColumn(false);
	
	$col2 = new FWDDBField("","class_name",DB_STRING);
	$col2->setLabel(FWDLanguage::getPHPStringValue('gc_type','Tipo'));
	$col2->setShowColumn(true);		

	$col3 = new FWDDBField("","area_id",DB_NUMBER);
	$col3->setLabel(' ');
	$col3->setShowColumn(false);
	
	$col4 = new FWDDBField("","area_name",DB_STRING);
	$col4->setLabel(FWDLanguage::getPHPStringValue('cb_area','�rea'));
	$col4->setShowColumn(true);		
	
	$col5 = new FWDDBField("","area_value",DB_NUMBER);
	$col5->setLabel(' ');
	$col5->setShowColumn(false);    
    
    $this->coDataSet->addFWDDBField($col1);    
    $this->coDataSet->addFWDDBField($col2);
    $this->coDataSet->addFWDDBField($col3);
    $this->coDataSet->addFWDDBField($col4);
    $this->coDataSet->addFWDDBField($col5);
        
  }
  
  /**
   * Executa a query do relat�rio.
   *
   * <p>M�todo para executar a query do relat�rio.</p>
   * @access public
   */
  public function makeQuery() {
    $moFilter = $this->getFilter();    
    $msWhere ="";
    $maClassifType = $moFilter->getAreaClassifType();
    
    $msUnion = "UNION SELECT
                  99999 as class_id,
                  '".FWDLanguage::getPHPStringValue('rs_undefined_type','Tipo Indefinido')."' as class_name,
                  a.fkContext as area_id,
                  a.sName as area_name,
                  a.nValue as area_value
                FROM
                  view_rm_area_active a
                WHERE
                  a.fkType IS NULL";
                  
    if(count($maClassifType)){
      if(isset($maClassifType['null'])){
        $msWhere = " AND a.fkType IS NULL ";
      }else{
        $msWhere = " AND a.fkType IN ( ".implode(',',array_keys($maClassifType)) ." ) ";
        $msUnion = "";
      }
    }

    $this->csQuery = "SELECT
												cl.pkClassification as class_id,
												cl.sName as class_name,
												a.fkContext as area_id,
												a.sName as area_name,
												a.nValue as area_value
											FROM
												isms_context_classification cl JOIN
												view_rm_area_active a ON (cl.pkClassification = a.fkType)
											WHERE
												cl.nContextType = " . CONTEXT_AREA . " AND
												cl.nClassificationType = " . CONTEXT_CLASSIFICATION_TYPE . "
												$msWhere
                        $msUnion
                      ORDER BY
                        class_id ASC";
    
    return parent::executeQuery();
  }
}
?>