<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe ISMSReportAssetsWithoutRisks.
 *
 * <p>Classe que implementa o relat�rio de ativos que n�o possuem riscos.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportAssetsWithoutRisks extends ISMSReport {
	
	/**
	 * Inicializa o relat�rio.
	 *
	 * <p>M�todo para inicializar o relat�rio.
	 * Necess�rio para funcionar com o XML compilado.</p>
	 * @access public
	 */	
	public function init() {
		parent::init();

		$col1 = new FWDDBField("","asset_id",DB_NUMBER);
		$col1->setLabel(' ');
		$col1->setShowColumn(false);
		
		$col2 = new FWDDBField("","asset_name",DB_STRING);
		$col2->setLabel(FWDLanguage::getPHPStringValue('rs_asset','Ativo'));
		$col2->setShowColumn(true);		
	
		$col3 = new FWDDBField("","asset_value",DB_NUMBER);
		$col3->setLabel(' ');
		$col3->setShowColumn(false);
		
		$col4 = new FWDDBField("","asset_responsible_id",DB_NUMBER);
		$col4->setLabel(' ');
		$col4->setShowColumn(false);		
		
		$col5 = new FWDDBField("","asset_responsible_name",DB_STRING);
		$col5->setLabel(FWDLanguage::getPHPStringValue('rs_responsible','Respons�vel'));
		$col5->setShowColumn(true);
		
		$col6 = new FWDDBField("","asset_sec_responsible_id",DB_NUMBER);
		$col6->setLabel(' ');
		$col6->setShowColumn(false);				
		
		$col7 = new FWDDBField("","asset_sec_responsible_name",DB_STRING);
		$col7->setLabel(FWDLanguage::getPHPStringValue('rs_security_responsible','Respons�vel pela Seguran�a'));
		$col7->setShowColumn(true);	
		
		$this->coDataSet->addFWDDBField($col1);
		$this->coDataSet->addFWDDBField($col2);
		$this->coDataSet->addFWDDBField($col3);
		$this->coDataSet->addFWDDBField($col4);
		$this->coDataSet->addFWDDBField($col5);
		$this->coDataSet->addFWDDBField($col6);
		$this->coDataSet->addFWDDBField($col7);		
	}
	
	/**
	 * Executa a query do relat�rio.
	 *
	 * <p>M�todo para executar a query do relat�rio.</p>
	 * @access public
	 */
	public function makeQuery() {
		$this->csQuery = "SELECT a.fkContext as asset_id, a.sName as asset_name, a.nValue as asset_value,
                        resp.fkContext as asset_responsible_id, resp.sName as asset_responsible_name,
                        sec.fkContext as asset_sec_responsible_id, sec.sName as asset_sec_responsible_name
                      FROM view_rm_asset_active a
                      JOIN view_isms_user_active resp ON (a.fkResponsible = resp.fkContext)
                      JOIN view_isms_user_active sec ON (a.fkSecurityResponsible = sec.fkContext)
                      WHERE a.fkContext NOT IN (SELECT fkAsset FROM view_rm_risk_active)";
		
		return parent::executeQuery();
	}
}
?>