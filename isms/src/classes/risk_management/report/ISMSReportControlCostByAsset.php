<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe ISMSReportControlCostByAsset.
 *
 * <p>Classe que implementa o relatório de custo dos controles por ativo.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportControlCostByAsset extends ISMSReport {
	
	/**
	 * Inicializa o relatório.
	 *
	 * <p>Método para inicializar o relatório.
	 * Necessário para funcionar com o XML compilado.</p>
	 * @access public
	 */	
	public function init() {
		parent::init();
		
		$col1 = new FWDDBField("","asset_id",DB_NUMBER);
		$col1->setLabel(' ');
		$col1->setShowColumn(false);
		
		$col2 = new FWDDBField("","asset_name",DB_STRING);
		$col2->setLabel(FWDLanguage::getPHPStringValue('rs_asset','Ativo'));
		$col2->setShowColumn(true);		
	
		$col3 = new FWDDBField("","asset_value",DB_NUMBER);
		$col3->setLabel(' ');
		$col3->setShowColumn(false);
		
		$col4 = new FWDDBField("","control_id",DB_NUMBER);
		$col4->setLabel(' ');
		$col4->setShowColumn(false);		
		
		$col5 = new FWDDBField("","control_name",DB_STRING);
		$col5->setLabel(FWDLanguage::getPHPStringValue('cb_control','Controle'));
		$col5->setShowColumn(true);
		
		$col6 = new FWDDBField("","control_is_active",DB_NUMBER);
		$col6->setLabel(' ');
		$col6->setShowColumn(false);		
		
		$col7 = new FWDDBField("","control_cost",DB_NUMBER);
		$col7->setLabel(FWDLanguage::getPHPStringValue('gc_cost','Custo'));
		$col7->setShowColumn(true);		
		
		$this->coDataSet->addFWDDBField($col1);		
		$this->coDataSet->addFWDDBField($col2);
		$this->coDataSet->addFWDDBField($col3);
		$this->coDataSet->addFWDDBField($col4);
		$this->coDataSet->addFWDDBField($col5);
		$this->coDataSet->addFWDDBField($col6);				
		$this->coDataSet->addFWDDBField($col7);
	}
	
	/**
	 * Executa a query do relatório.
	 *
	 * <p>Método para executar a query do relatório.</p>
	 * @access public
	 */
	public function makeQuery() {		
		$this->csQuery = "SELECT a.fkContext as asset_id, a.sName as asset_name, a.nValue as asset_value,
												c.fkContext as control_id, c.sName as control_name, c.bIsActive as control_is_active,
												(cc.nCost1+cc.nCost2+cc.nCost3+cc.nCost4+cc.nCost5) as control_cost
											FROM view_rm_asset_active a
                      JOIN view_rm_risk_active r ON (a.fkContext = r.fkAsset)
                      JOIN view_rm_risk_control_active rc ON (r.fkContext = rc.fkRisk)
                      JOIN view_rm_control_active c ON (rc.fkControl = c.fkContext)
											LEFT JOIN rm_control_cost cc ON (c.fkContext = cc.fkControl)
											ORDER BY a.fkContext";
		
		return parent::executeQuery();
	}
}
?>