<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe ISMSReportControlCost.
 *
 * <p>Classe que implementa o relat�rio dos custos dos controles.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportControlCost extends ISMSReport {
  
  /**
   * Inicializa o relat�rio.
   *
   * <p>M�todo para inicializar o relat�rio.
   * Necess�rio para funcionar com o XML compilado.</p>
   * @access public
   */  
  public function init() {
    parent::init();
    
	$col1 = new FWDDBField("","control_id",DB_NUMBER);
	$col1->setLabel(' ');
	$col1->setShowColumn(false);
	
	$col2 = new FWDDBField("","control_name",DB_STRING);
	$col2->setLabel(FWDLanguage::getPHPStringValue('cb_control','Controle'));
	$col2->setShowColumn(true);		

	$col3 = new FWDDBField("","control_is_active",DB_NUMBER);
	$col3->setLabel(' ');
	$col3->setShowColumn(false);
	
	$col4 = new FWDDBField("","control_cost_id",DB_NUMBER);
	$col4->setLabel(' ');
	$col4->setShowColumn(false);		
	
	$col5 = new FWDDBField("","control_cost",DB_NUMBER);
	$col5->setLabel(FWDLanguage::getPHPStringValue('gc_cost','Custo'));
	$col5->setShowColumn(true);
    
    $this->coDataSet->addFWDDBField($col1);
    $this->coDataSet->addFWDDBField($col2);    
    $this->coDataSet->addFWDDBField($col3);
    $this->coDataSet->addFWDDBField($col4);
    $this->coDataSet->addFWDDBField($col5);    
  }
  
  /**
   * Executa a query do relat�rio.
   *
   * <p>M�todo para executar a query do relat�rio.</p>
   * @access public
   */
  public function makeQuery() {
  
    $moFilter = $this->getFilter();
    $maFilters = array();
    $msWhere ="";

    $maClassifType = $moFilter->getControlClassifType();
    if(count($maClassifType)){
      if(isset($maClassifType['null'])){
        $maFilters[] = " c.fkType IS NULL ";
      }else{
        $maFilters[] = " c.fkType IN ( ".implode(',',array_keys($maClassifType)) ." ) ";
      }
    }

    if(count($maFilters)){
        $msWhere .= " WHERE ". implode(' AND ',$maFilters);
    }
    
    $this->csQuery = "SELECT c.fkContext as control_id, c.sName as control_name, c.bIsActive as control_is_active, " . CONTROL_COST_1 . " as control_cost_id, cc.nCost1 as control_cost
                      FROM view_rm_control_active c
                      LEFT JOIN rm_control_cost cc ON (c.fkContext = cc.fkControl)
                      $msWhere
                      
                      UNION
                      
                      SELECT c.fkContext as control_id, c.sName as control_name, c.bIsActive as control_is_active, " . CONTROL_COST_2 . " as control_cost_id, cc.nCost2 as control_cost
                      FROM view_rm_control_active c
                      LEFT JOIN rm_control_cost cc ON (c.fkContext = cc.fkControl)
                      $msWhere
                      
                      UNION
                      
                      SELECT c.fkContext as control_id, c.sName as control_name, c.bIsActive as control_is_active, " . CONTROL_COST_3 . " as control_cost_id, cc.nCost3 as control_cost
                      FROM view_rm_control_active c
                      LEFT JOIN rm_control_cost cc ON (c.fkContext = cc.fkControl)
                      $msWhere
                      
                      UNION
                      
                      SELECT c.fkContext as control_id, c.sName as control_name, c.bIsActive as control_is_active, " . CONTROL_COST_4 . " as control_cost_id, cc.nCost4 as control_cost                      
                      FROM view_rm_control_active c
                      LEFT JOIN rm_control_cost cc ON (c.fkContext = cc.fkControl)
                      $msWhere
                      
                      UNION
                      
                      SELECT c.fkContext as control_id, c.sName as control_name, c.bIsActive as control_is_active, " . CONTROL_COST_5 . " as control_cost_id, cc.nCost5 as control_cost
                      FROM view_rm_control_active c
                      LEFT JOIN rm_control_cost cc ON (c.fkContext = cc.fkControl)
                      $msWhere
                      
                      ORDER BY control_id, control_cost_id";
    
    return parent::executeQuery();
  }
  
  /**
   * Desenha o cabe�alho do relat�rio.
   *
   * <p>M�todo para desenhar o cabe�alho do relat�rio.</p>
   * @access public
   */
  public function drawHeader(){
    foreach($this->caHeaders as $moHeader){
      if($moHeader->getAttrName()=='header_filter'){
        $moHeaderFilter = $moHeader;
      }elseif($moHeader->getAttrName()=='header_filter_item'){
        $moHeaderFilterItem = $moHeader;
      }elseif($moHeader->getAttrName()=='report_header_blank'){
        $moHeaderBlank = $moHeader;
      }else{
        $this->coWriter->drawLine($moHeader,array());
      }
    }
    if ($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());
    
    $moFilterText = FWDWebLib::getObject('header_filter_text');
    $moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
    $maFilters = $this->coFilter->getSummary();
    foreach($maFilters as $maFilter){
      $moFilterText->setValue($maFilter['name']);
      $this->coWriter->drawLine($moHeaderFilter,array());
      foreach($maFilter['items'] as $msFilterItem){
        $moFilterItemText->setValue($msFilterItem);
        $this->coWriter->drawLine($moHeaderFilterItem,array());
      }
    }
    $this->coWriter->drawLine($moHeaderBlank,array());
  }

}
?>