<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportAssets.
 *
 * <p>Classe que implementa o relatório de ativos.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportAssets extends ISMSFilteredReport {
  
  public function __construct(){
    parent::__construct();
  }
  
 /**
  * Inicializa o relatório.
  *
  * <p>Método para inicializar o relatório.
  * Necessário para funcionar com o XML compilado.</p>
  * @access public
  */
  public function init(){
    parent::init();
    $this->coDataSet->addFWDDBField(new FWDDBField('a.fkcontext','asset_id'         ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('a.sname'    ,'asset_name'       ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('u.sName'    ,'asset_responsible',DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('a.nvalue'   ,'asset_value'      ,DB_NUMBER));
  }
  
 /**
  * Executa a query do relatório.
  *
  * <p>Método para executar a query do relatório.</p>
  * @access public
  */
  public function makeQuery(){
    $msValue = $this->coFilter->getValue();
    
    $maFilters = array();
    
    if($msValue){
      $miRiskLow = ISMSLib::getConfigById(RISK_LOW);
      $miRiskHigh = ISMSLib::getConfigById(RISK_HIGH);
      switch($msValue){
        case 'high':{
          $maFilters[] = "a.nValue >= {$miRiskHigh}";
          break;
        }
        case 'mid':{
          $maFilters[] = "a.nValue > {$miRiskLow}";
          $maFilters[] = "a.nValue < {$miRiskHigh}";
          break;
        }
        case 'low':{
          $maFilters[] = "a.nValue <= {$miRiskLow}";
          $maFilters[] = "a.nValue != 0";
          break;
        }
        case 'np':{
          $maFilters[] = "a.nValue = 0";
          break;
        }
      }
    }
    
    if(count($maFilters)==0){
      $msWhere = '';
    }else{
      $msWhere = ' WHERE '.implode(' AND ',$maFilters);
    }
    $this->csQuery = "SELECT
                        a.fkcontext AS asset_id,
                        a.sname AS asset_name,
                        u.sName AS asset_responsible,
                        a.nvalue AS asset_value
                      FROM
                        view_rm_asset_active a
                        JOIN isms_user u ON (u.fkContext = a.fkResponsible)
                      {$msWhere}
                      ORDER BY a.sname";
    return parent::executeQuery();
  }

}

?>