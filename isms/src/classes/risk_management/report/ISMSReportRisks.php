<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportRisks.
 *
 * <p>Classe que implementa o relatório de riscos.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportRisks extends ISMSFilteredReport {
  
  public function __construct(){
    parent::__construct();
  }
  
 /**
  * Inicializa o relatório.
  *
  * <p>Método para inicializar o relatório.
  * Necessário para funcionar com o XML compilado.</p>
  * @access public
  */
  public function init(){
    parent::init();
    $this->coDataSet->addFWDDBField(new FWDDBField('r.fkcontext'     ,'risk_id'            ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('r.sname'         ,'risk_name'          ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('r.nvalue'        ,'risk_value'         ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('r.nvalueresidual','risk_residual_value',DB_NUMBER));
  }
  
 /**
  * Executa a query do relatório.
  *
  * <p>Método para executar a query do relatório.</p>
  * @access public
  */
  public function makeQuery(){
    $msValue = $this->coFilter->getValue();
    $msResidualValue = $this->coFilter->getResidualValue();
    $miShowEstimated = $this->coFilter->getShowEstimated();
    $miShowTreated = $this->coFilter->getShowTreated();
    
    
    $riskLevel = ISMSLib::getConfigById(RISK_LEVEL);
    
    if($riskLevel == 3){ 
    
	    $miRiskLow = ISMSLib::getConfigById(RISK_LOW);
	    $miRiskHigh = ISMSLib::getConfigById(RISK_HIGH);
	    
	    $maFilters = array();
	    
	    if($msValue){
	      switch($msValue){
	        case 'high':{
	          $maFilters[] = "r.nvalue >= {$miRiskHigh}";
	          break;
	        }
	        case 'mid':{
	          $maFilters[] = "r.nvalue > {$miRiskLow}";
	          $maFilters[] = "r.nvalue < {$miRiskHigh}";
	          break;
	        }
	        case 'low':{
	          $maFilters[] = "r.nvalue <= {$miRiskLow}";
	          $maFilters[] = "r.nvalue != 0";
	          break;
	        }
	        case 'np':{
	          $maFilters[] = "r.nvalue = 0";
	          break;
	        }
	      }
	    }
	    
	    if($msResidualValue){
	      switch($msResidualValue){
	        case 'high':{
	          $maFilters[] = "r.nvalueresidual >= {$miRiskHigh}";
	          break;
	        }
	        case 'mid':{
	          $maFilters[] = "r.nvalueresidual > {$miRiskLow}";
	          $maFilters[] = "r.nvalueresidual < {$miRiskHigh}";
	          break;
	        }
	        case 'low':{
	          $maFilters[] = "r.nvalueresidual <= {$miRiskLow}";
	          $maFilters[] = "r.nvalueresidual != 0";
	          break;
	        }
	        case 'np':{
	          $maFilters[] = "r.nvalueresidual = 0";
	          break;
	        }
	      }
	    }
    
    
    } elseif($riskLevel == 5){ 
    
    	$miRiskLow = ISMSLib::getConfigById(RISK_LOW);
    	$miRiskMidLow = ISMSLib::getConfigById(RISK_MID_LOW);
    	$miRiskMidHigh = ISMSLib::getConfigById(RISK_MID_HIGH);
	    $miRiskHigh = ISMSLib::getConfigById(RISK_HIGH);
	    
	    $maFilters = array();
	    
	    if($msValue){
	      switch($msValue){
	        case 'high':{
	          $maFilters[] = "r.nvalue > {$miRiskHigh}";
	          break;
	        }
	        
	      	case 'mid_high':{
	          $maFilters[] = "r.nvalue > {$miRiskMidHigh}";
	          $maFilters[] = "r.nvalue <= {$miRiskHigh}";
	          break;
	        }		        
	        
	      	case 'mid':{
	          $maFilters[] = "r.nvalue > {$miRiskMidLow}";
	          $maFilters[] = "r.nvalue <= {$miRiskMidHigh}";
	          break;
	        }	        
	        
	        case 'mid_low':{
	          $maFilters[] = "r.nvalue > {$miRiskLow}";
	          $maFilters[] = "r.nvalue <= {$miRiskMidLow}";
	          break;
	        }
	        
	        case 'low':{
	          $maFilters[] = "r.nvalue <= {$miRiskLow}";
	          $maFilters[] = "r.nvalue != 0";
	          break;
	        }
	        case 'np':{
	          $maFilters[] = "r.nvalue = 0";
	          break;
	        }
	      }
	    }
	    
	    if($msResidualValue){
	      switch($msResidualValue){
	        case 'high':{
	          $maFilters[] = "r.nvalueresidual > {$miRiskHigh}";
	          break;
	        }
	        
	        case 'mid_high':{
	          $maFilters[] = "r.nvalueresidual > {$miRiskMidHigh}";
	          $maFilters[] = "r.nvalueresidual <= {$miRiskHigh}";
	          break;
	        }
	        
	        
	        case 'mid':{
	          $maFilters[] = "r.nvalueresidual > {$miRiskMidLow}";
	          $maFilters[] = "r.nvalueresidual <= {$miRiskMidHigh}";
	          break;
	        }

	        case 'mid_low':{
	          $maFilters[] = "r.nvalueresidual > {$miRiskLow}";
	          $maFilters[] = "r.nvalueresidual <= {$miRiskMidLow}";
	          break;
	        }
	        
	        
	        case 'low':{
	          $maFilters[] = "r.nvalueresidual <= {$miRiskLow}";
	          $maFilters[] = "r.nvalueresidual != 0";
	          break;
	        }
	        case 'np':{
	          $maFilters[] = "r.nvalueresidual = 0";
	          break;
	        }
	      }
	    }    	
    	
    	
    }
    
    
    switch($miShowEstimated){
      case ISMSReportRisksFilter::ONLY_NOT_ESTIMATED: $maFilters[] = "r.nvalue = 0";  break;
      case ISMSReportRisksFilter::ONLY_ESTIMATED    : $maFilters[] = "r.nvalue != 0"; break;
    }
    
    switch($miShowTreated){
      case ISMSReportRisksFilter::ONLY_NOT_TREATED:{
        $maFilters[] = "(r.nAcceptMode = 0 AND r.nValue = r.nValueResidual)";
        break;
      }
      case ISMSReportRisksFilter::ONLY_TREATED    :{
        $maFilters[] = "(r.nAcceptMode != 0 OR r.nValue != r.nValueResidual)";
        break;
      }
    }
    
    if(count($maFilters)==0){
      $msWhere = '';
    }else{
      $msWhere = ' WHERE '.implode(' AND ',$maFilters);
    }
    $this->csQuery = "SELECT
                        r.fkcontext AS risk_id,
                        r.sname AS risk_name,
                        r.nvalue AS risk_value,
                        r.nvalueresidual AS risk_residual_value
                      FROM view_rm_risk_active r
                      {$msWhere}
                      ORDER BY r.sname";
    return parent::executeQuery();
  }

}

?>