<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe ISMSReportAreasByPriority.
 *
 * <p>Classe que implementa o relat�rio de �reas por prioridade.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportAreasByPriority extends ISMSReport {
  
  /**
   * Inicializa o relat�rio.
   *
   * <p>M�todo para inicializar o relat�rio.
   * Necess�rio para funcionar com o XML compilado.</p>
   * @access public
   */  
  public function init() {
    parent::init();
    
    $this->coDataSet->addFWDDBField(new FWDDBField("","class_id",DB_NUMBER));    
    $this->coDataSet->addFWDDBField(new FWDDBField("","class_name",DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("","area_id",DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("","area_name",DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("","area_value",DB_NUMBER));        
  }
  
  /**
   * Executa a query do relat�rio.
   *
   * <p>M�todo para executar a query do relat�rio.</p>
   * @access public
   */
  public function makeQuery() {
    $moFilter = $this->getFilter();    
    $msWhere ="";
    $maClassifPriority = $moFilter->getAreaClassifPriority();
    
    $msUnion = "UNION SELECT
                  99999 as class_id,
                  '".FWDLanguage::getPHPStringValue('rs_undefined_priority','Prioridade Indefinida')."' as class_name,
                  a.fkContext as area_id,
                  a.sName as area_name,
                  a.nValue as area_value
                FROM
                  view_rm_area_active a
                WHERE
                  a.fkPriority IS NULL";
                  
    if(count($maClassifPriority)){
      if(isset($maClassifPriority['null'])){
        $msWhere = " AND a.fkPriority IS NULL ";
      }else{
        $msWhere = " AND a.fkPriority IN ( ".implode(',',array_keys($maClassifPriority)) ." ) ";
        $msUnion = "";
      }
    }

    $this->csQuery = "SELECT
												cl.pkClassification as class_id,
												cl.sName as class_name,
												a.fkContext as area_id,
												a.sName as area_name,
												a.nValue as area_value
											FROM
												isms_context_classification cl JOIN
												view_rm_area_active a ON (cl.pkClassification = a.fkPriority)
											WHERE
												cl.nContextType = " . CONTEXT_AREA . " AND
												cl.nClassificationType = " . CONTEXT_CLASSIFICATION_PRIORITY . "
												$msWhere
                        $msUnion
											ORDER BY
												class_id ASC";
    
    return parent::executeQuery();
  }
}
?>