<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe RMControlBestPractice.
 *
 * <p>Classe que representa a tabela de associa��o entre controles e melhores pr�ticas.</p>
 * @package ISMS
 * @subpackage classes
 */
class RMControlBestPractice extends ISMSContext {
  
 /**
  * Construtor.
  * 
  * <p>Construtor da classe RMControlBestPractice.</p>
  * @access public 
  */
  public function __construct(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    		
  	parent::__construct("rm_control_best_practice");
    $this->ciContextType = CONTEXT_CONTROL_BEST_PRACTICE;	
  	
  	$this->csAliasId = "control_best_practice_id";
  	
  	$this->coDataset->addFWDDBField(new FWDDBField("fkContext",		   "control_best_practice_id",	DB_NUMBER));  	
  	$this->coDataset->addFWDDBField(new FWDDBField("fkBestPractice", "best_practice_id",			    DB_NUMBER));  	
  	$this->coDataset->addFWDDBField(new FWDDBField("fkControl",		    "control_id",				  DB_NUMBER));  	
  }
	
 /**
  * Verifica quais as rela��es entre o controle e as melhores praticas devem ser inseridas e / ou removidas
  * 
  * <p>M�todo para verifica quais as rela��es entre o controle e as melhores praticas devem ser inseridas e / ou removidas.</p>
  * @access public
  * @param integer $piControlId Id da Melhor Pr�tica
  * @param string $psInitialBestPracticesIds Ids das melhores pr�ticas que o controle j� possuia
  * @param string $psCurrentBestPractcesIds Ids das novas melhores pr�ticas relacionadas ao controle
  */
	public function updateRelations($piControlId,$psInitialBestPractcesIds,$psCurrentBestPractcesIds){
			$maParameters = func_get_args();
    	FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    	
			$maInitial = array_unique(array_filter(explode(':',$psInitialBestPractcesIds)));
			$maCurrent = array_unique(array_filter(explode(':',$psCurrentBestPractcesIds)));
			$maDel = array_diff($maInitial,$maCurrent);
			$maInsert = array_diff($maCurrent,$maInitial);
			if(count($maInsert)){
				foreach($maInsert as $miNew){
					$this->insertRelation($piControlId,$miNew);
				}
			}
			if(count($maDel)){
				foreach($maDel as $miDel){
					$this->removeRelation($piControlId,$miDel);
				}
		  }			
	}

 /**
  * Insere rela��es entre um controle e diversas melhores pr�ticas.
  *
  * <p>M�todo para inserir rela��es entre um controle e diversas melhores pr�ticas.</p>
  * @access public
  * @param integer $piControlId Id do controle
  * @param array $paBestPractices Ids das melhores pr�ticas
  */
  public function insertBestPractices($piControlId, $paBestPractices){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $this->setFieldValue('control_id', $piControlId);
    foreach($paBestPractices as $miBestPracticeId){
      $this->setFieldValue('best_practice_id', $miBestPracticeId);
      parent::insert();
    }
  }

 /**
  * Deleta rela��es entre um controle e diversas melhores pr�ticas.
  *
  * <p>M�todo para deletar rela��es entre um controle e diversas melhores pr�ticas.</p>
  * @access public
  * @param integer $piControlId Id do controle
  * @param array $paBestPractices Ids das melhores pr�ticas
  */
  public function deleteBestPractices($piControlId, $paBestPractices){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    foreach($paBestPractices as $miBestPracticeId){
      parent::delete($this->getId($piControlId, $miBestPracticeId), true);
    }
  }

 /**
  * Deleta rela��es entre um controle e suas melhores pr�ticas.
  * 
  * <p>M�todo para deletar rela��es entre um controle e suas melhores pr�ticas.</p>
  * @access public
  * @param integer $piControlId Id do controle
  * @param integer $piBestPractceId Id da melhor pr�tica
  */
	public function removeRelation($piControlId,$piBestPractceId){
		$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
		parent::delete($this->getId($piControlId, $piBestPractceId), true); 
	}
  
 /**
  * Retorna o id do contexto da associa��o.
  * 
  * <p>M�todo para retornar o id do contexto da associa��o.</p>
  * @access public
  * @param integer $piBestPracticeId Id da Melhor Pr�tica
  * @param integer $piControlId Id do controle
  * @return integer Id da associa��o
  */
  public function getId($piControlId, $piBestPractceId) {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
  	 $moControlBP = new RMControlBestPractice();
  	 $moControlBP->createFilter($piControlId,"control_id");	
		 $moControlBP->createFilter($piBestPractceId,"best_practice_id");
	   $moControlBP->select();
	   $moControlBP->fetch();
  	 return $moControlBP->getFieldValue('control_best_practice_id');
  }
	
 /**
  * Insere uma rela��o entre um controle e uma melhor pr�tica.
  * 
  * <p>M�todo para inserir uma rela��o entre um controle e uma melhor pr�tica.</p>
  * @access public 
  * @param integer $piControlId Id do controle
  * @param integer $piBestPracticeId Id da Melhor Pr�tica
  */ 
	public function insertRelation($piControlId,$piBestPractceId){
		$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
		$this->setFieldValue('best_practice_id',$piBestPractceId);
		$this->setFieldValue('control_id',$piControlId);
		parent::insert();
	}
  
 /**
  * Retorna o nome do contexto.
  * 
  * <p>M�todo para retornar o nome do contexto.</p>
  * @access public 
  * @return string Nome do contexto
  */
  public function getName(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
  	$moControl = new RMControl();
  	$moControl->fetchById($this->getFieldValue('control_id'));  	
  	$moBestPractice = new RMBestPractice();
  	$moBestPractice->fetchById($this->getFieldValue('best_practice_id'));
  	return $moBestPractice->getName() . ' -> ' . $moControl->getName();
  }
 
 /**
  * Retorna o label do contexto.
  * 
  * <p>M�todo para retornar o label do contexto.</p>
  * @access public 
  * @return string Label do contexto
  */ 
  public function getLabel(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
  	return FWDLanguage::getPHPStringValue('mx_control_best_practice_association', "Associa��o de Melhores Praticas a Controle");
  }  
  
 /**
  * Retorna se o usu�rio logado tem permi��o para inserir o contexto.
  * 
  * <p>M�todo para retornar se o usu�rio tem permi��o para inserir o contexto no sistema.</p>
  * @access public
  * @return boolean Se o usu�ro tem permi��o para inserir o contexto no sistema
  */
  protected function userCanInsert(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    //contexto deve ser tratado, assim assume-se que a principio o usu�rio n�o pode inserir tal contexto
    $mbReturn = false;
    $miCount = 0;

    $maPermissions = array( 'M.RM.5.4','M.RM.5.6','M.RM.5.7' );
    //acls negadas o usu�rio logado no sistema
    $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
    foreach($maACLs as $miKey => $miValue){
      $maACLs[$miValue]=$miKey;
    }
    // acl1 OR acl2
    $mbAuxPermission = false;
    foreach($maPermissions as $msValue){
      if(!isset($maACLs[$msValue])){
        $mbAuxPermission = true;
      }
      $miCount++;
    }
    //caso exista pelo menos 1 acl nas permi��es do contexto e estas permi��es n�o est�o nas permi��es negadas do usu�rio
    if(($mbAuxPermission)&&($miCount)){
      $mbReturn = $mbAuxPermission;
    }
    return $mbReturn;
  }

 /**
  * Retorna se o usu�rio logado tem permi��o para editar o contexto.
  * 
  * <p>M�todo para retornar se o usu�rio tem permi��o para editar o contexto no sistema.</p>
  * @access protected
  * @return boolean Se o usu�ro tem permi��o de editar o contexto no sistema
  */
  protected function userCanEdit($piContextId,$piUserResponsible = 0){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    if(!$this->contextIsOfType($piContextId,$this->ciContextType)) return false;
    //contexto deve ser tratado, assim assume-se que a principio o usu�rio n�o pode editar esse contexto
    $mbReturn = false;
    $maPermissions = array( 'M.RM.5.4','M.RM.5.7' );
    //acls negadas do usu�rio logado no sistema
    $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
    foreach($maACLs as $miKey => $miValue){
      $maACLs[$miValue]=$miKey;
    }
    $miCount = 0;
    // acl1 OR acl2
    $mbAuxPermission = false;
    foreach($maPermissions as $msValue){
      if(!isset($maACLs[$msValue])){
        $mbAuxPermission = true;
      }
      $miCount++;
    }
    //caso exista pelo menos 1 acl nas permi��es do contexto e estas permi��es n�o est�o nas permi��es negadas do usu�rio
    if(($mbAuxPermission)&&($miCount)){
      $mbReturn = $mbAuxPermission;
      }
    return $mbReturn;
  }
 
 /**
  * Retorna se o usu�rio logado tem permi��o para excluir o contexto.
  * 
  * <p>M�todo para retornar se o usu�rio tem permi��o para excluir o contexto no sistema.</p>
  * @access protected
  * @return boolean Se o usu�ro tem permi��o de excluir o contexto no sistema
  */
  protected function userCanDelete($piContextId){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    //tester esquemas de respons�vel aq nesse teste
    $miContextType = $this->ciContextType;
    if(!$this->contextIsOfType($piContextId,$miContextType)) return false;
    //contexto deve ser tratado, assim assume-se que a principio o usu�rio n�o pode inserir tal contexto
    $mbReturn = false;
    $maPermissions = array( 'M.RM.5.4' );

    //acls negadas do usu�rio logado no sistema
    $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
    foreach($maACLs as $miKey => $miValue){
      $maACLs[$miValue]=$miKey;
    }
      $miCount = 0;
    //acl1 AND acl2
    $mbAuxPermission = true;
    foreach($maPermissions as $msValue){
      if(isset($maACLs[$msValue])){
        $mbAuxPermission = false;
      }
      $miCount++;
    }
    //caso exista pelo menos 1 acl nas permi��es do contexto e estas permi��es n�o est�o nas permi��es negadas do usu�rio
    if(($mbAuxPermission)&&($miCount)){
      $mbReturn = $mbAuxPermission;
    }
    return $mbReturn;
  }  
}
?>