<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

define("RISK_ACCEPT_MODE_NONE",72600);
define("RISK_ACCEPT_MODE_ACCEPT",72601);
define("RISK_ACCEPT_MODE_TRANSFER",72602);
define("RISK_ACCEPT_MODE_AVOID",72603);

/**
 * Classe RMAcceptRisk.
 *
 * <p>Classe que representa a tabela de ricos (para aceita��o de riscos).</p>
 * @package ISMS
 * @subpackage classes
 */
class RMAcceptRisk extends ISMSContext implements IWorkflow {
  
 /**
  * Construtor.
  *
  * <p>Construtor da classe RMAcceptRisk.</p>
  * @access public
  */
  public function __construct(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    parent::__construct('rm_risk');
    $this->ciContextType = CONTEXT_ACCEPT_RISK;
    
    $this->csAliasId = "risk_id";
    $this->coWorkflow = new RMWorkflow(ACT_ACCEPT_RISK_APPROVAL);

    $this->coDataset->addFWDDBField(new FWDDBField('fkContext',             'risk_id',                  DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('fkAsset',               'risk_asset_id',            DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('sName',                 'risk_name',                DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField('nAcceptMode',           'risk_accept_mode',         DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('nAcceptState',          'risk_accept_state',        DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('sAcceptJustification',  'risk_accept_justification',DB_STRING));
    
    $this->caSearchableFields = array('risk_name', 'risk_accept_justification');
  }

 /**
  * Retorna o id do usu�rio que deve aprovar o contexto.
  *
  * <p>M�todo para retornar id do usu�rio que deve aprovar o contexto.</p>
  * @access public
  * @return integer Id do aprovador
  */
  public function getApprover(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return ISMSLib::getConfigById(USER_CHAIRMAN);
  }

 /**
  * Retorna o id do usu�rio respons�vel pelo contexto.
  *
  * <p>M�todo para retornar id do usu�rio respons�vel pelo contexto.</p>
  * @access public
  * @return integer Id do respons�vel
  */
  public function getResponsible(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $moAsset = new RMAsset();
    $moAsset->fetchById($this->getFieldValue("risk_asset_id"));
    return $moAsset->getResponsible();
  }

 /**
  * Retorna o nome do contexto.
  * 
  * <p>M�todo para retornar o nome do contexto.</p>
  * @access public 
  * @return string Nome do contexto
  */
  public function getName() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return $this->getFieldValue('risk_name');
  }
 
 /**
  * Retorna a descri��o do contexto.
  * 
  * <p>M�todo para retornar a descri��o do contexto.</p>
  * @access public 
  * @return string Descri��o do contexto
  */ 
  public function getDescription() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return $this->getFieldValue('risk_accept_justification');
  }
  
 /**
  * Retorna o label do contexto.
  * 
  * <p>M�todo para retornar o label do contexto.</p>
  * @access public 
  * @return string Label do contexto
  */
  public function getLabel() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return FWDLanguage::getPHPStringValue('mx_risk_acceptance', "Aceita��o de Risco");
  }

 /**
  * Retorna se o usu�rio logado tem permi��o para inserir o contexto.
  * 
  * <p>M�todo para retornar se o usu�rio tem permi��o para inserir o contexto no sistema.</p>
  * @access public
  * @return boolean Se o usu�ro tem permi��o para inserir o contexto no sistema
  */
  protected function userCanInsert(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return true;
    $mbReturn = false;
    $moRisk = new RMRisk();
    if($moRisk->getApprover()==ISMSLib::getCurrentUserId()){
      $mbReturn = true;
    }
    return $mbReturn;
  }

 /**
  * Retorna se o usu�rio logado tem permi��o para editar o contexto.
  * 
  * <p>M�todo para retornar se o usu�rio tem permi��o para editar o contexto no sistema.</p>
  * @access protected
  * @return boolean Se o usu�ro tem permi��o de editar o contexto no sistema
  */
  protected function userCanEdit($piContextId,$piUserResponsible = 0){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    //contexto deve ser tratado, assim assume-se que a principio o usu�rio n�o pode editar esse contexto
    return true;
    $mbReturn = false;
    $moRisk = new RMRisk();
    $moRisk->fetchById($piContextId);
    if($moRisk->getApprover()==ISMSLib::getCurrentUserId()){
      $mbReturn = true;
    }
    return $mbReturn;
  }
 
 /**
  * Retorna se o usu�rio logado tem permi��o para excluir o contexto.
  * 
  * <p>M�todo para retornar se o usu�rio tem permi��o para excluir o contexto no sistema.</p>
  * @access protected
  * @return boolean Se o usu�ro tem permi��o de excluir o contexto no sistema
  */
  protected function userCanDelete($piContextId){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $mbReturn = false;
    $moRisk = new RMRisk();
    $moRisk->fetchById($piContextId);
    if($moRisk->getApprover()==ISMSLib::getCurrentUserId()){
      $mbReturn = true;
    }
    return $mbReturn;
  }
}
?>
