<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe RMRisk.
 *
 * <p>Classe que representa a tabela de ricos.</p>
 * @package ISMS
 * @subpackage classes
 */
class RMRisk extends ISMSContext implements IWorkflow {
  
 /**
  * Construtor.
  *
  * <p>Construtor da classe RMRisk.</p>
  * @access public
  */
  public function __construct(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    parent::__construct('rm_risk');
    $this->ciContextType = CONTEXT_RISK;
    
    $this->csAliasId = "risk_id";
    $this->csDependenceAliasId = "risk_asset_id";
    $this->coWorkflow = new RMWorkflow(ACT_RISK_APPROVAL);

    $this->coDataset->addFWDDBField(new FWDDBField('fkContext',             'risk_id',                  DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('fkEvent',               'risk_event_id',            DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('fkAsset',               'risk_asset_id',            DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('fkProbabilityValueName','risk_probability',         DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('sName',                 'risk_name',                DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField('tDescription',          'risk_description',         DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField('nValue',                'risk_value',               DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('nValueResidual',        'risk_residual_value',      DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('tJustification',        'risk_justification',       DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField('nAcceptMode',           'risk_accept_mode',         DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('nAcceptState',          'risk_accept_state',        DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('sAcceptJustification',  'risk_accept_justification',DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField('nCost',                 'risk_cost',                DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('fkType',                'risk_type',                DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('tImpact',               'risk_impact',              DB_STRING));
    
    $this->caSensitiveFields = array('risk_name', 'risk_asset_id');
    $this->caSearchableFields = array('risk_name', 'risk_description', 'risk_justification', 'risk_accept_justification', 'risk_impact');
  }
  
 /**
  * Duplica o risco mudando o ativo e retorna o id do risco inserido.
  *
  * <p>M�todo para duplicar o risco totalmente, i.e., o risco e todos os seus par�metros, mudando apenas o ativo associado. Retorna o id do risco inserido.</p>
  * @access public
  * @param integer $piAssetId Id do novo ativo
  * @return integer Id do novo risco.
  */
  public function duplicateRiskOnAsset($piAssetId){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $miRiskId = $this->getFieldValue('risk_id');
    $miEventId = $this->getFieldValue('risk_event_id');
    $miAssetId = $this->getFieldValue('risk_asset_id');
    $miProbabilityValue = $this->getFieldValue('risk_probability');
    $miRiskType = $this->getFieldValue('risk_type');
    
    $moNewRisk = new RMRisk();
    $moNewRisk->setFieldValue('risk_event_id',$miEventId?$miEventId:'null');
    $moNewRisk->setFieldValue('risk_asset_id',$piAssetId);
    $moNewRisk->setFieldValue('risk_probability',$miProbabilityValue?$miProbabilityValue:'null');
    $moNewRisk->setFieldValue('risk_name',$this->getFieldValue('risk_name'),false);
    $moNewRisk->setFieldValue('risk_description',$this->getFieldValue('risk_description'),false);
    $moNewRisk->setFieldValue('risk_justification',$this->getFieldValue('risk_justification'),false);
    $moNewRisk->setFieldValue('risk_cost',$this->getFieldValue('risk_cost'));
    $moNewRisk->setFieldValue('risk_type',$miRiskType?$miRiskType:'null');
    $moNewRisk->setFieldValue('risk_impact',$this->getFieldValue('risk_impact'),false);
    $miNewRiskId = $moNewRisk->insert(true);
    
    $moRiskValue = new RMRiskValue();
    $moRiskValue->createElementIdFilter($miRiskId);
    $moRiskValue->select();
    while($moRiskValue->fetch()) {
      $moNewRiskValue = new RMRiskValue();
      $moNewRiskValue->setFieldValue('riskvalue_value_id',$moRiskValue->getFieldValue('riskvalue_value_id'));
      $moNewRiskValue->setFieldValue('riskvalue_parameter_id',$moRiskValue->getFieldValue('riskvalue_parameter_id'));
      $moNewRiskValue->setFieldValue('riskvalue_risk_id',$miNewRiskId);
      $moNewRiskValue->setFieldValue('riskvalue_justification',$moRiskValue->getFieldValue('riskvalue_justification'),false);
      $moNewRiskValue->insert();
    }
    if(FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(INCIDENT_MODE)){
      $moSchedule = new CIRiskSchedule();
      $moSchedule->createFilter($miRiskId,'risk_id');
      $moSchedule->select();
      if($moSchedule->fetch()){
        $moNewSchedule = new CIRiskSchedule();
        $moNewSchedule->setFieldValue('risk_id',$miNewRiskId);
        $moNewSchedule->setFieldValue('schedule_period',$moSchedule->getFieldValue('schedule_period'));
        $moNewSchedule->setFieldValue('schedule_value',$moSchedule->getFieldValue('schedule_value'));
        $moNewSchedule->setFieldValue('schedule_date_last_check',$moSchedule->getFieldValue('schedule_date_last_check'));
        $moNewSchedule->insert();
      }
      
      $moRiskProbabilities = new CIRiskProbability();
      $moRiskProbabilities->createFilter($miRiskId,'risk_id');
      $moRiskProbabilities->select();
      while($moRiskProbabilities->fetch()){
        $moNewRiskProbabilities = new CIRiskProbability();
        $moNewRiskProbabilities->setFieldValue('risk_id',$miNewRiskId);
        $moNewRiskProbabilities->setFieldValue('value_name_id',$moRiskProbabilities->getFieldValue('value_name_id'));
        $moNewRiskProbabilities->setFieldValue('incident_amount',$moRiskProbabilities->getFieldValue('incident_amount'));
        $moNewRiskProbabilities->insert();
      }
    }
  }

 /**
  * Retorna o id do usu�rio que deve aprovar o contexto.
  *
  * <p>M�todo para retornar id do usu�rio que deve aprovar o contexto.</p>
  * @access public
  * @return integer Id do aprovador
  */
  public function getApprover(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $moAsset = new RMAsset();
    $moAsset->fetchById($this->getFieldValue('risk_asset_id'));
    return $moAsset->getFieldValue('asset_responsible_id');
  }

 /**
  * Retorna o id do usu�rio respons�vel pelo contexto.
  *
  * <p>M�todo para retornar id do usu�rio respons�vel pelo contexto.</p>
  * @access public
  * @return integer Id do respons�vel
  */
  public function getResponsible(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return $this->getCreator();
  }

 /**
  * Retorna o id do usu�rio respons�vel pelo risco para ser utilizado nos testes de permiss�o.
  *
  * <p>M�todo para retornar id do usu�rio respons�vel pelo risco (o respons�vel pela seguran�a do ativo).</p>
  * @access public
  * @return integer Id do respons�vel
  */
  public function getResponsibleToACLPermition($piRiskId = 0){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $miAssetId = $this->getFieldValue('risk_asset_id');
    if(!$miAssetId){
      $moRisk = new RMRisk();
      $moRisk->fetchById($piRiskId);
      $miAssetId = $moRisk->getFieldValue('risk_asset_id');
    }
    $moAsset = new RMAsset();
    $moAsset->fetchById($miAssetId);
    return $moAsset->getFieldValue('asset_security_responsible_id');
  }
  
  /**
  * Retorna o id do segundo usu�rio respons�vel pelo risco para ser utilizado nos testes de permiss�o.
  *
  * <p>M�todo para retornar id do usu�rio respons�vel pelo risco (o respons�vel pelo ativo).</p>
  * @access public
  * @return integer Id do respons�vel
  */
  public function getSecondResponsibleToACLPermition($piRiskId = 0){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $miAssetId = $this->getFieldValue('risk_asset_id');
    if(!$miAssetId){
      $moRisk = new RMRisk();
      $moRisk->fetchById($piRiskId);
      $miAssetId = $moRisk->getFieldValue('risk_asset_id');
    }
    $moAsset = new RMAsset();
    $moAsset->fetchById($miAssetId);
    return $moAsset->getFieldValue('asset_responsible_id');
  }

 /**
  * Retorna o nome do contexto.
  * 
  * <p>M�todo para retornar o nome do contexto.</p>
  * @access public 
  * @return string Nome do contexto
  */
  public function getName() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return $this->getFieldValue('risk_name');
  }
 
 /**
  * Retorna a descri��o do contexto.
  * 
  * <p>M�todo para retornar a descri��o do contexto.</p>
  * @access public 
  * @return string Descri��o do contexto
  */ 
  public function getDescription() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return $this->getFieldValue('risk_description');
  }
  
 /**
  * Retorna o �cone do contexto.
  *
  * <p>M�todo para retornar o �cone do contexto.</p>
  * @access public
  * @param integer $piContextId Id do contexto
  * @return string Nome do �cone
  */
  public function getIcon($piContextId = 0){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return 'icon-risk_gray.gif';
  }

 /**
  * Retorna o label do contexto.
  * 
  * <p>M�todo para retornar o label do contexto.</p>
  * @access public 
  * @return string Label do contexto
  */
  public function getLabel() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return FWDLanguage::getPHPStringValue('mx_risk', "Risco");
  }

 /**
  * Retorna o valor do contexto.
  * 
  * <p>M�todo para retornar o valor do contexto.</p>
  * @access public 
  * @return integer valor do contexto
  */
  public function getValue(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
  	return $this->getFieldValue('risk_residual_value');
  }

  /**
  * Retorna o caminho do contexto para a cria��o do pathscroll (utilizado quando se volta de controle).
  * 
  * <p>M�todo para retornar o caminho do contexto para a cria��o do pathscroll.</p>
  * @access public 
  * @param integer $piTab id da tab
  * @param integer $piContextType id do contexto
  * @param string $psContextId indices do filtro de ids dos links do pathscroling  
  * @param string $psAuxContextId indices auxiliares (para a recurs�o (Ativo->Controle)) do filtro de ids dos links do pathscroling
  * @return array Array contendo o Caminho do contexto
  */ 
  public function getSystemPathScrollAux($piTab,$piContextType,$psContextId,$psAuxContextId =''){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
  	$maPath=array();
		if($psContextId){
 			$maPath[] = "<a href='javascript:isms_redirect_to_mode_scroll({$piTab},".CONTEXT_RISK.",\\\"$psContextId\\\",\\\"$psAuxContextId\\\");'>";
 			$maPath[] = $this->getName();
			$maPath[]=ISMSLib::getIconCode('icon-risk_',$this->getValue());
 			$maPath[] = "</a>";
 	  }else{
			if($psContextId){
				if(!(strpos($psContextId,':')===false))
						$psContextId="\\\"$psContextId\\\"";
			}
			$psContextId = 0;
  		$maPath[] = "<a href='javascript:isms_redirect_to_mode_scroll({$piTab},".CONTEXT_RISK.",\\\"$psContextId\\\");'>";
  		$maPath[] = $this->getName();
  		$maPath[]=ISMSLib::getIconCode('icon-risk_',$this->getValue());
  		$maPath[] = "</a>";  
 	  }
    return $maPath;
  }

 /**
  * Retorna os ancestrais do contexto.
  * 
  * <p>M�todo para retornar todos contextos acima de um determinado contexto na
  * �rvore de depend�ncias.</p>
  * @access public
  * @param integer $piContextId Id do contexto
  * @param boolean $pbExecByTrash se o metodo est� sendo chamado pela lixeira
  * @return array Array de ids dos ancestrais do contexto
  * /
  public function getSuperContexts($piContextId,$pbExecByTrash = false){
    $miAssetId = $this->getFieldValue('risk_asset_id')->getValue();
    if(!$miAssetId){
      $moRisk = new RMRisk();
      $moRisk->fetchById($piContextId);
      $miAssetId = $moRisk->getFieldValue('risk_asset_id')->getValue();
    }
    return array($miAssetId);
  }*/

  /**
  * Retorna o caminho do contexto para a cria��o do pathscroll.
  * 
  * <p>M�todo para retornar o caminho do contexto para a cria��o do pathscroll.</p>
  * @access public 
  * @param integer $piTab id da tab
  * @param integer $piContextType id do contexto
  * @param string $psContextId indices do filtro de ids dos links do pathscroling  
  * @param string $psAuxContextId indices auxiliares (para a recurs�o (Ativo->Controle)) do filtro de ids dos links do pathscroling
  * @return array Array contendo o Caminho do contexto
  */ 
  public function getSystemPathScroll($piTab,$piContextType,$psContextId,$psAuxContextId = ''){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
  	$msHome = FWDLanguage::getPHPStringValue('st_home','Raiz');
  	$maPath=array();
  	$paContextId = explode(':',$psContextId);
  	$psAssetId = '';
  	if((isset($paContextId[1]))&&($paContextId[1])){
  		$psAssetId .= $paContextId[1];
  		if(isset($paContextId[2]))
  			$psAssetId .= ':'. $paContextId[2];
  		if(isset($paContextId[3]))
  			$psAssetId .= ':'. $paContextId[3];
  	}				  
		if($psAssetId){
				$moAsset = new RMAsset();
				$moAsset->fetchById($paContextId[1]);
				$maPath = $moAsset->getSystemPathScroll(RISK_MANAGEMENT_MODE,CONTEXT_ASSET,$psAssetId);
		}elseif($piContextType==CONTEXT_RISK){
				$maPath[] = "<a href='javascript:isms_redirect_to_mode_scroll(".$piTab.", ".$piContextType.",0);'>";
				$maPath[] = $msHome;
				$maPath[]=ISMSLib::getIconCode('',-1);
			  $maPath[] = "</a>";
		}
		if($psAssetId){
			if(!(strpos($psAssetId,':')===FALSE))
					$psAssetId="\\\"$psAssetId\\\"";
		}else
			$psAssetId = 0;
		if(($psAuxContextId!='')&&($psAuxContextId!=0)){
			$maPath[] = "<a href='javascript:isms_redirect_to_mode_scroll({$piTab},".CONTEXT_RISK.",$psAssetId,\\\"$psAuxContextId\\\" );'>";
		}else{
			$maPath[] = "<a href='javascript:isms_redirect_to_mode_scroll({$piTab},".CONTEXT_RISK.",$psAssetId);'>";
		}
		$maPath[] = $this->getName();
		$maPath[]=ISMSLib::getIconCode('icon-risk_',$this->getValue());  
		$maPath[] = "</a>";
    return $maPath;
  }
  
 /**
  * Retorna o caminho do contexto.
  * 
  * <p>M�todo para retornar o caminho do contexto.</p>
  * @access public 
  * @return string Caminho do contexto
  */ 
  public function getSystemPath() {  	
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
  	$miAssetId = $this->getFieldValue('risk_asset_id');
  	$moAsset = new RMAsset();
  	$moAsset->fetchById($miAssetId);
  	
  	$msPath = $moAsset->getSystemPath();  	
  	$msPath .= "&nbsp;".ISMSLib::getIconCode('icon-arrow-right.gif',-2,0).'&nbsp;' . ISMSLib::getIconCode('icon-risk_',$this->getValue(),-4) . "<a href='javascript:isms_redirect_to_mode(" . RISK_MANAGEMENT_MODE . "," . $this->ciContextType . "," . $miAssetId . ")'>" . $this->getName() . "</a>";
  	
  	return $msPath;
  }
  
 /**
  * Retorna se o usu�rio logado tem permi��o para inserir o contexto.
  * 
  * <p>M�todo para retornar se o usu�rio tem permi��o para inserir o contexto no sistema.</p>
  * @access public
  * @return boolean Se o usu�ro tem permi��o para inserir o contexto no sistema
  */
  protected function userCanInsert(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $mbReturn = false;
    if(ISMSLib::userHasACL('M.RM.4.6')){
      $mbReturn = true;
    }elseif(ISMSLib::userHasACL('M.RM.3.10')){ // ou quando estiver carregando risco da biblioteca.
      $mbReturn = true;
    }
    return $mbReturn;
  }

 /**
  * Retorna se o usu�rio logado tem permi��o para editar o contexto.
  * 
  * <p>M�todo para retornar se o usu�rio tem permi��o para editar o contexto no sistema.</p>
  * @access protected
  * @return boolean Se o usu�ro tem permi��o de editar o contexto no sistema
  */
  public function userCanEdit($piContextId,$piUserResponsible = 0){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    if(!$this->contextIsOfType($piContextId,$this->ciContextType)) return false;
    $mbReturn = false;
    
    if(ISMSLib::userHasACL('M.RM.4.4')){
      $mbReturn = true;
    }else{
      if(ISMSLib::userHasACL('M.RM.4.9') && (($this->getResponsibleToACLPermition($piContextId) == ISMSLib::getCurrentUserId()) || ($this->getSecondResponsibleToACLPermition($piContextId) == ISMSLib::getCurrentUserId()))){
        $mbReturn = true;
      }
    }
    return $mbReturn;
  }
 
 /**
  * Retorna se o usu�rio logado tem permi��o para excluir o contexto.
  * 
  * <p>M�todo para retornar se o usu�rio tem permi��o para excluir o contexto no sistema.</p>
  * @access protected
  * @return boolean Se o usu�ro tem permi��o de excluir o contexto no sistema
  */
  protected function userCanDelete($piContextId){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    if(!$this->contextIsOfType($piContextId,$this->ciContextType)) return false;
    $mbReturn = false;
    if( ISMSLib::userHasACL('M.RM.4.5') ){
      $mbReturn = true;
    }else{
      if(ISMSLib::userHasACL('M.RM.4.10') && (($this->getResponsibleToACLPermition($piContextId) == ISMSLib::getCurrentUserId()) || ($this->getSecondResponsibleToACLPermition($piContextId) == ISMSLib::getCurrentUserId()))){
        $mbReturn = true;
      }
    }
    return $mbReturn;
  }
  
  /**
  * Retorna o c�digo HTML do icone do contexto.
  * 
  * <p>M�todo para retornar o c�digo HTML do contexto</p>
  * @access public
  * @param integer $piId Id do contexto
  * @param integer $pbIsFetched id do contexto
  * @return string contendo o c�digo HTML do icone do contexto
  */ 
  public function getIconCode($piId, $pbIsFetched = false){
    if(!$pbIsFetched){
      $this->fetchById($piId);
    }
    return ISMSLib::getIconCode('icon-risk_'.RMRiskConfig::getRiskColor($this->getValue()).'.gif',-2,7);
  }

   /**
  * Retorna o caminho para abrir a popup de edi��o desse contexto.
  * 
  * <p>M�todo para retornar o caminho para abrir a popup de edi��o desse contexto.</p>
  * @param integer $piContextId id do contexto
  * @access public 
  * @return string caminho para abrir a popup de edi��o desse contexto
  */ 
  public function getVisualizeEditEvent($piContextId,$psUniqId){
    $soSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    $msPackagesRiskPath = ($soSession->getMode()==MANAGER_MODE) ? "packages/risk/" : "../../packages/risk/";
    $msPopupId = "popup_risk_edit";
    
    return "isms_open_popup('{$msPopupId}','{$msPackagesRiskPath}{$msPopupId}.php?risk=$piContextId','','true',455,775);"
           ."soPopUpManager.getPopUpById('{$msPopupId}').setCloseEvent( function close_visualize_$psUniqId() {soPopUpManager.closePopUp('popup_visualize_$psUniqId');} );";
  }

 /**
  * Deleta logicamente um contexto.
  * 
  * <p>M�todo para deletar logicamente um contexto. Deve ser sobrecarregada
  * por contextos para os quais for necess�ria.</p>
  * @access public
  * @param integer $piContextId id do contexto
  */ 
  public function logicalDelete($piContextId) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $moRel = new RMRiskControl();
    $moRel->createFilter($piContextId,'rc_risk_id');
    $moRel->select();
    while($moRel->fetch()){
      // deletar tarefas do contexto deletado
      $moTasks = new WKFTask();
      $moTasks->createFilter($moRel->getFieldValue('rc_id'), 'task_context_id');
      $moTasks->delete();
      // deletar alertas do contexto deletado
      $moAlerts = new WKFAlert();
      $moAlerts->createFilter($moRel->getFieldValue('rc_id'), 'alert_context_id');
      $moAlerts->delete();
    }
  }
  
  /**
  * Retorna o identificador do primeiro contexto da rela��o.
  * 
  * <p>M�todo para retornar o identificador do primeiro contexto da rela��o.</p>
  * @access public 
  * @return string Alias
  */
  public function getFirstContextId() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
  	return $this->getFieldValue('risk_id');
  }
 
 /**
  * Retorna o identificador do segundo contexto da rela��o.
  * 
  * <p>M�todo para retornar o identificador do segundo contexto da rela��o.</p>
  * @access public 
  * @return string Alias
  */
  public function getSecondContextId() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
  	return $this->getFieldValue('risk_asset_id');
  }

}
?>