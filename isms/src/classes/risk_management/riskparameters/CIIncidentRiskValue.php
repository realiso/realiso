<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe CIIncidentRiskValue.
 *
 * <p>Classe que representa a tabela de riscos.</p>
 * @package ISMS
 * @subpackage riskparameters
 */
class CIIncidentRiskValue extends ISMSTable implements RMIValue {
  
 /**
  * Construtor.
  * 
  * <p>Construtor da classe RMRiskValue.</p>
  * @access public 
  */
  public function __construct(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
  	parent::__construct("ci_incident_risk_value");
  	
  	$this->csAliasId = 'riskvalue_risk_id';
  	
  	$this->coDataset->addFWDDBField(new FWDDBField("fkValueName",			"riskvalue_value_id",				 DB_NUMBER));
  	$this->coDataset->addFWDDBField(new FWDDBField("fkParameterName",	"riskvalue_parameter_id",		 DB_NUMBER));
  	$this->coDataset->addFWDDBField(new FWDDBField("fkIncRisk",			  "riskvalue_incident_risk_id",DB_NUMBER));
  }
  
  public function setValueId($piValueId){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
  	$this->setFieldValue('riskvalue_value_id', $piValueId);
  }
  
  public function setParameterId($piParameterId){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
  	$this->setFieldValue('riskvalue_parameter_id', $piParameterId);
  }
  
  public function setElementId($piElementId){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
  	$this->setFieldValue('riskvalue_incident_risk_id', $piElementId);
  }
  
  public function setJustification($psJustification){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
  	return true;
  }
  
  public function createElementIdFilter($piElementId) {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
  	$this->createFilter($piElementId,'riskvalue_incident_risk_id');
  } 
  
  public function createParameterIdFilter($piParameterId) {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
  	$this->createFilter($piParameterId,'riskvalue_parameter_id');
  }
}
?>