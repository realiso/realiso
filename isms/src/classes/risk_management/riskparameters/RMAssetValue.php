<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe RMAssetValue.
 *
 * <p>Classe que representa a tabela de �reas.</p>
 * @package ISMS
 * @subpackage riskparameters
 */
class RMAssetValue extends ISMSTable implements RMIValue {	
  
 /**
  * Construtor.
  * 
  * <p>Construtor da classe RMAssetValue.</p>
  * @access public 
  */
  public function __construct(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
  	parent::__construct("rm_asset_value");
  	
  	$this->csAliasId = 'assetvalue_asset_id';
  	
  	$this->coDataset->addFWDDBField(new FWDDBField("fkValueName",			"assetvalue_value_id",			DB_NUMBER));
  	$this->coDataset->addFWDDBField(new FWDDBField("fkParameterName",	"assetvalue_parameter_id",	DB_NUMBER));
  	$this->coDataset->addFWDDBField(new FWDDBField("fkAsset",					"assetvalue_asset_id",			DB_NUMBER));
  	$this->coDataset->addFWDDBField(new FWDDBField("tJustification",	"assetvalue_justification",	DB_STRING));  	
  }
  
  public function setValueId($piValueId){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
  	$this->setFieldValue('assetvalue_value_id', $piValueId);
  }
  
  public function setParameterId($piParameterId){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
  	$this->setFieldValue('assetvalue_parameter_id', $piParameterId);
  }
  
  public function setElementId($piElementId){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
  	$this->setFieldValue('assetvalue_asset_id', $piElementId);
  }
  
  public function setJustification($psJustification){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
  	$this->setFieldValue('assetvalue_justification',$psJustification);
  }
  
  public function createElementIdFilter($piElementId) {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
  	$this->createFilter($piElementId,'assetvalue_asset_id');
  } 
  
  public function createParameterIdFilter($piParameterId) {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
  	$this->createFilter($piParameterId,'assetvalue_parameter_id');
  }  
}
?>