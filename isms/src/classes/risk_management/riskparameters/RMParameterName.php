<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe RMParameterName.
 *
 * <p>Classe que representa a tabela de nome dos parāmetros de risco.</p>
 * @package ISMS
 * @subpackage riskparameters
 */
class RMParameterName extends ISMSTable {	
  
 /**
  * Construtor.
  * 
  * <p>Construtor da classe RMParameterName.</p>
  * @access public 
  */
  public function __construct(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
  	parent::__construct("rm_parameter_name");
  	
  	$this->csAliasId = 'parametername_id';
  	
  	$this->coDataset->addFWDDBField(new FWDDBField("pkParameterName",	"parametername_id",			DB_NUMBER));
  	$this->coDataset->addFWDDBField(new FWDDBField("sName",				    "parametername_name",		DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField("nWeight",         "parametername_weight", DB_NUMBER));  	  	
  }
  
  public function getParameters() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $maParameters = array();
    $this->setOrderBy('parametername_name','+');
    $this->select();
    while($this->fetch()) {
      $maParameters[$this->getFieldValue('parametername_id')]=array($this->getFieldValue('parametername_name'),$this->getFieldValue('parametername_weight'));
    }
    return $maParameters;
  }
}
?>