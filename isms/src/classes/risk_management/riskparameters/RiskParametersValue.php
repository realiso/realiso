<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
 include_once $handlers_ref . "QueryRiskValues.php";
 include_once $handlers_ref . "QueryRiskProbability.php";
 include_once $handlers_ref . "QueryRiskControlValues.php";
 include_once $handlers_ref . "QueryRiskControlProbability.php";
 include_once $handlers_ref . "QueryRiskUpdate.php";
 include_once $handlers_ref . "QueryAssetValues.php";
 include_once $handlers_ref . "QueryAssetUpdate.php";
 include_once $handlers_ref . "QueryRCRiskUpdate.php";
 
/**
 * Classe RiskParametersValue.
 *
 * <p>Classe que representa a tabela de �reas.</p>
 * @package ISMS
 * @subpackage riskparameters
 */
class RiskParametersValue {
	
 /**
  * Tipo do elemento. RISKPARAMETER_ASSET | RISKPARAMETER_RISK | RISKPARAMETER_RISKCONTROL | RISKPARAMETER_INCIDENTRISK
  * @var string
  * @access protected
  */
  protected $csType;
  
 /**
  * Objeto que armazena todos os valores dos Parametros de Risco
  * @var RiskParameters
  * @access protected
  */
  protected $coRiskParameters = null;  		
  
 /**
  * Construtor.
  * 
  * <p>Construtor da classe RiskParametersValue.</p>
  * @access public 
  */
  public function __construct(RiskParameters $poRiskParameters, $piType){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
  	FWDWebLib::attributeParser($piType, array(RISKPARAMETER_ASSET, 
			RISKPARAMETER_RISK, RISKPARAMETER_RISKCONTROL,RISKPARAMETER_INCIDENTRISK),"piType");
  	$this->coRiskParameters = $poRiskParameters;
  	$this->csType = $piType;			
  } 
  
  public function getValues($piElementId) {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
  	$this->coRiskParameters->populate();
  	switch($this->csType) {
		case RISKPARAMETER_ASSET:
				$moValues = new QueryAssetValues(FWDWebLib::getConnection());
				break;
		case RISKPARAMETER_RISK:
				$moValues = new QueryRiskValues(FWDWebLib::getConnection());
				$moProbability = new QueryRiskProbability(FWDWebLib::getConnection());
				break;
		case RISKPARAMETER_RISKCONTROL:
				$moValues = new QueryRiskControlValues(FWDWebLib::getConnection());
				$moProbability = new QueryRiskControlProbability(FWDWebLib::getConnection());
				break;
    case RISKPARAMETER_INCIDENTRISK:
        $moValues = new QueryRiskValues(FWDWebLib::getConnection());
        break;
		default:  
				trigger_error("Element type, at Risk Parameters Values, not allowed.",E_USER_WARNING);
				break; 		
  	}
  	$moValues->setElementId($piElementId);
  	$moValues->makeQuery();
  	$moValues->executeQuery();
  	while($moValues->getDataset()->fetch()) {
  		$moValueDataset = $moValues->getDataset();
  		$miValueId = $moValueDataset->getFieldByAlias("value_id")->getValue();
		$miParameterId = $moValueDataset->getFieldByAlias("parameter_id")->getValue();
		$msJustification = $moValueDataset->getFieldByAlias("justification")->getValue();
  		$this->coRiskParameters->setParameterValue($miParameterId,$miValueId);
  		$this->coRiskParameters->setJustification($miParameterId,$msJustification);
  	}
  	if ($this->csType != RISKPARAMETER_ASSET && $this->csType != RISKPARAMETER_INCIDENTRISK)
  	{
  		$moProbability->setElementId($piElementId);
  		$moProbability->makeQuery();
  		$moProbability->executeQuery();
  		$moProbability->getDataset()->fetch();
  		$moProbabilityDataset = $moProbability->getDataset();
  		$miValueId = $moProbabilityDataset->getFieldByAlias("value_id")->getValue();
  		$msJustification = $moProbabilityDataset->getFieldByAlias("justification")->getValue();
  		$this->coRiskParameters->setParameterValue('prob',$miValueId);
  		$this->coRiskParameters->setJustification('prob',$msJustification);
  	}
  }
  
  public function setValues($piElementId) {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    switch($this->csType) {
    	case RISKPARAMETER_ASSET:
    		$moValues = new RMAssetValue();
    		$moUpdate = new QueryAssetUpdate(FWDWebLib::getConnection());
    		$moUpdate2 = new QueryAssetUpdateSubquery(FWDWebLib::getConnection());
    		break;
    	case RISKPARAMETER_RISK:
    		$moValues = new RMRiskValue();
    		$moUpdate = new QueryRiskUpdate(FWDWebLib::getConnection());
    		break;
    	case RISKPARAMETER_RISKCONTROL:
    		$moValues = new RMRiskControlValue();
    		$moUpdate = new QueryRCRiskUpdate(FWDWebLib::getConnection());
    		break;
    	case RISKPARAMETER_INCIDENTRISK:
      $moValues = new CIIncidentRiskValue();
      break;
    	default:
    		trigger_error("Element type, at Risk Parameters Values, not allowed.",E_USER_WARNING);
    		break;
    }
  	
    $moValues->createElementIdFilter($piElementId);
  	$moValues->delete();
  	
  	$maRiskParameters = $this->coRiskParameters->getValues();
  	$maRiskParametersJustifications = $this->coRiskParameters->getJustifications();
  	foreach($maRiskParameters as $msParameterId=>$miValueId) {
  		if ($msParameterId != 'prob') {
  			$moValues->setValueId($miValueId);
  			$moValues->setParameterId($msParameterId);
  			$moValues->setElementId($piElementId);
  			$moValues->setJustification($maRiskParametersJustifications[$msParameterId]);
  			$moValues->insert();
  		}
  	}
  	if (isset($moUpdate)) {
  		$moUpdate->setElementId($piElementId);
  		$moUpdate->makeQuery();
  		$moUpdate->executeQuery();
  	}
  	if (isset($moUpdate2)) {
  		$moUpdate->setElementId($piElementId);
  		$moUpdate->makeQuery();
  		$moUpdate->executeQuery();
  	}
  }
  
  public function updateJustification($piElementId) {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
  	$maRiskParameters = $this->coRiskParameters->getValues();
	$maRiskParametersJustifications = $this->coRiskParameters->getJustifications();
	foreach($maRiskParameters as $msParameterId=>$miValueId) {
	  	if ($msParameterId != 'prob') {
		  	switch($this->csType) {
		  		case RISKPARAMETER_ASSET:
					$moValues = new RMAssetValue();
					break;
				case RISKPARAMETER_RISK:
					$moValues = new RMRiskValue();
					break;
				case RISKPARAMETER_RISKCONTROL:
					$moValues = new RMRiskControlValue();
					break;
				default:  
					trigger_error("Element type, at Risk Parameters Values, not allowed.",E_USER_WARNING);
					break;
		  	}
			$moValues->createElementIdFilter($piElementId);
			$moValues->createParameterIdFilter($msParameterId);
			$moValues->setJustification($maRiskParametersJustifications[$msParameterId]);
			$moValues->update();
	  	}
	}
  }     
}
?>