<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

include_once $handlers_ref . "QueryParameterNames.php";
include_once $handlers_ref . "QueryParameterValueNames.php";
include_once $handlers_ref . "QueryProbabilityValueNames.php";

// Tipos de elementos que podem ter par�metros de risco
define("RISKPARAMETER_ASSET", 7701);
define("RISKPARAMETER_RISK", 7702);
define("RISKPARAMETER_RISKCONTROL", 7703); 
define("RISKPARAMETER_INCIDENTRISK", 7704);

/**
 * Classe RiskParameters. Classe de interface para parametriza��o de riscos.
 *
 * <p>Classe de interface para parametriza��o de riscos.</p>
 * @package ISMS
 * @subpackage handlers
 */
class RiskParameters extends FWDDrawing {

 /**
  * Box do elemento
  * @var FWDBox
  * @access protected
  */
  protected $coBox = null;

 /**
  * Nome do elemento
  * @var string
  * @access protected
  */
  protected $csName = "";

 /**
  * Classe CSS
  * @var string
  * @access protected
  */
  protected $csClass = "RiskParameters";

 /**
  * Tipo do elemento. Pode ser RISKPARAMETER_ASSET | RISKPARAMETER_RISK | RISKPARAMETER_RISKCONTROL | RISKPARAMETER_INCIDENTRISK
  * @var integer
  * @access protected
  */
  protected $ciType;
  
 /**
  * Atributo que indica se o componente deve ser desenhado na horizontal
  * @var boolean
  * @access protected
  */
  protected $cbHorizontal = false;

 /**
  * Altura das linhas
  * @var integer
  * @access protected
  */
  protected $ciLineHeight = 25;

 /**
  * Altura dos elementos (Selects e Statics)
  * @var integer
  * @access protected
  */
  protected $ciElementsHeight = 20;

 /**
  * Largura da coluna com os nomes dos par�metros
  * @var integer
  * @access protected
  */
  protected $ciParameterNameWidth = 145;

 /**
  * Largura da coluna com o select dos valores
  * @var integer
  * @access protected
  */
  protected $ciValueNameWidth = 145;

 /**
  * Dist�ncia horizontal entre os elementos de uma linha
  * @var integer
  * @access protected
  */
  protected $ciHorizontalMargin = 10;

 /**
  * Altura do memo para a justificativa
  * @var integer
  * @access protected
  */
  protected $ciMemoHeight = 70;
  
 /**
  * Margem entre o �ltimo par�metro e a justificativa
  * @var integer
  * @access protected
  */
  protected $ciJustificationMargin = 10;

 /**
  * Atributo que define a classe CSS das linhas �mpares
  * @var string
  * @access protected
  */
  protected $csOddRowClass = 'BdZ1';

 /**
  * Atributo que define a classe CSS das linhas pares
  * @var string
  * @access protected
  */
  protected $csEvenRowClass = 'BdZ0';
  
 /**
  * Indica se o elemento j� foi constru�do, ou seja, se seus sub-elementos j�
  * foram criados.
  * @var boolean
  * @access protected
  */
  protected $cbBuilt = false;

 /**
  * Array com os nomes dos par�metros
  * @var array
  * @access protected
  */
  protected $caParameters = array();

 /**
  * Array com os nomes dos valores
  * @var array
  * @access protected
  */
  protected $caValues = array();
  
 /**
  * Array com os valores que estao selecionados por padrao
  * @var array
  * @access protected
  */
  protected $caSelectedValues = array();

 /**
  * Array com os nomes dos valores de probabilidade
  * @var array
  * @access protected
  */
  protected $caProbabilityValues = array();

 /**
  * Array com os containers que formam o elemento
  * @var array
  * @access protected
  */
  protected $caContainers = array();

 /**
  * Array com os selects dos par�metros
  * @var array
  * @access protected
  */
  protected $caSelects = array();

 /**
  * Array com os memos das justificativas
  * @var array
  * @access protected
  */
  protected $caMemos = array();

 /**
  * Construtor da classe.
  *
  * <p>Construtor da classe RiskParameters.</p>
  * @access public
  */
  public function __construct(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
  }
  
 /**
  * Constr�i o elemento
  *
  * <p>Constr�i o elemento. Instancia seus sub-elementos e define seus
  * par�metros.</p>
  * @access public
  */
  public function build(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    if(!isset($this->ciType)){
      trigger_error('Parameter type must be defined.',E_USER_ERROR);
    }elseif(!$this->cbBuilt){
      if(count($this->caParameters)==0){
        trigger_error("RiskParameters element must have at least one parameter.",E_USER_ERROR);
        return false;
      }elseif(count($this->caValues)==0){
        trigger_error("RiskParameters element must have at least one value.",E_USER_ERROR);
        return false;
      }
      $this->caContainers = array();
      $this->caSelects = array();
      $this->caMemos = array();
      $maParameters = $this->caParameters;
      if($this->ciType != RISKPARAMETER_ASSET && $this->ciType != RISKPARAMETER_INCIDENTRISK){
        $maParameters['prob'] = FWDLanguage::getPHPStringValue('mx_probability','Probabilidade');
      }
      $msNamePrefix = $this->csName.'_';
      $miOffsetTop = floor(($this->ciLineHeight - $this->ciElementsHeight)/2);
      if($this->cbHorizontal){
        $miWidth = count($maParameters) * ($this->ciValueNameWidth + $this->ciHorizontalMargin);
        $miHeight = $this->ciLineHeight;
        $miViewGroupWidth = $this->ciValueNameWidth + $this->ciHorizontalMargin;
        $miSelectsLeft = ceil($this->ciHorizontalMargin/2);
      }else{
        $miStaticsLeft = floor($this->ciHorizontalMargin/2);
        $miSelectsLeft = $miStaticsLeft + $this->ciParameterNameWidth + $this->ciHorizontalMargin;
        $miWidth = $miSelectsLeft + $this->ciValueNameWidth + ceil($this->ciHorizontalMargin/2);
        $miJustificationTop = count($maParameters) * $this->ciLineHeight + $this->ciJustificationMargin;
        $miHeight = $miJustificationTop + $this->ciLineHeight + $this->ciMemoHeight;
        $miViewGroupWidth = $miWidth;
      }
      $this->coBox->setAttrWidth($miWidth);
      $this->coBox->setAttrHeight($miHeight);
      $miTop = 0;
      $miLeft = 0;
      $mbIsEven = false;
      $mfFactor = 5/count($this->caValues);
      foreach($maParameters as $miParamId=>$msParamName){
        // ViewGroup da linha
        $moViewGroup = new FWDViewGroup();
        $moViewGroup->setAttrName($msNamePrefix.'line_'.$miParamId);
        $moViewGroup->setObjFWDBox(new FWDBox($miLeft,$miTop,$this->ciLineHeight,$miViewGroupWidth));
        $this->caContainers[] = $moViewGroup;
        
        if(!$this->cbHorizontal){
          //$moViewGroup->setAttrClass($mbIsEven?$this->csEvenRowClass:$this->csOddRowClass);
          $moViewGroup->setAttrClass($this->csEvenRowClass);
          $moEvent = new FWDClientEvent();
          $moEvent->setAttrEvent('onClick');
          $moEvent->setAttrValue("gebi('{$this->csName}').object.selectParameter('$miParamId');");
          $moViewGroup->addObjFWDEvent($moEvent);
          $moEvent = new FWDClientEvent();
          $moEvent->setAttrEvent('onMouseOver');
          $moEvent->setAttrValue("gebi('{$this->csName}').object.mouseOver('$miParamId');");
          $moViewGroup->addObjFWDEvent($moEvent);
          $moEvent = new FWDClientEvent();
          $moEvent->setAttrEvent('onMouseOut');
          $moEvent->setAttrValue("gebi('{$this->csName}').object.mouseOut('$miParamId');");
          $moViewGroup->addObjFWDEvent($moEvent);
          // Static com o nome do par�metro
          $moStatic = new FWDStatic();
          $moStatic->setAttrName($msNamePrefix.'static_'.$miParamId);
          $moStatic->setAttrHorizontal('left');
          $moStatic->setObjFWDBox(new FWDBox($miStaticsLeft,$miOffsetTop,$this->ciElementsHeight,$this->ciParameterNameWidth));
          $moStatic->setValue($msParamName);
          $moViewGroup->addObjFWDView($moStatic);
        }
        
        // Select com os valores
        $moSelect = new FWDSelect();
        $moSelect->setAttrName($msNamePrefix.$miParamId);
        
        $moSelect->setObjFWDBox(new FWDBox($miSelectsLeft,$miOffsetTop,$this->ciElementsHeight,$this->ciValueNameWidth));
        if(!$this->cbHorizontal){
          $moEvent = new FWDClientEvent();
          $moEvent->setAttrEvent('onFocus');
          $moEvent->setAttrValue("gobi('{$this->csName}').selectParameter('$miParamId');");
          $moSelect->addObjFWDEvent($moEvent);
        }
        $moEvent = new FWDClientEvent();
        $moEvent->setAttrEvent('onChange');
        $moEvent->setAttrValue("gobi('{$this->csName}').setParameterValue('$miParamId',this.value);");
        $moSelect->addObjFWDEvent($moEvent);
        $this->caSelects[$miParamId] = $moSelect;
        $moViewGroup->addObjFWDView($moSelect);
        // Valores do select
        if($miParamId=='prob'){
          $maValues = &$this->caProbabilityValues;
        }else{
          $maValues = &$this->caValues;
        }
        $moItem = new FWDItem();
        $moSelect->addObjFWDItem($moItem);
        $miCount = 1;
        foreach($maValues as $miValId=>$msValName){
          $moItem = new FWDItem();
          $moItem->setAttrKey($miValId);
          $moItem->setValue($msValName);
          $moItem->setAttrClass('value_'.floor($miCount*$mfFactor));
     
          if(array_key_exists($miParamId,$this->caSelectedValues)) {
          	if($this->caSelectedValues[$miParamId] == $miValId) {
          		$moItem->setAttrCheck(true);
          	}
          }
          $moSelect->addObjFWDItem($moItem);
          $miCount++;
        }
        // Panel com a justificativa
        if($this->cbHorizontal){
          // Atualiza vari�veis de controle para a pr�xima itera��o
          $miLeft+= $miViewGroupWidth;
        }else{
          $moPanel = new FWDPanel();
          $moPanel->setAttrClass('justification_panel');
          $moPanel->setAttrName($msNamePrefix.'justif_panel_'.$miParamId);
          $moPanel->setObjFWDBox(new FWDBox(0,$miJustificationTop,$this->ciMemoHeight+$this->ciLineHeight,$miWidth));
          $this->caContainers[] = $moPanel;
          // Static da justificativa
          $moStatic = new FWDStatic();
          $moStatic->setObjFWDBox(new FWDBox(0,$miOffsetTop,$this->ciElementsHeight,$miWidth));
          $msStaticText = FWDLanguage::getPHPStringValue('mx_justifying','Justificando ').'&nbsp;';
          if($miParamId=='prob'){
            $msStaticText.= FWDLanguage::getPHPStringValue('mx_probability', 'Probabilidade');
          }else{
            $msStaticText.= $this->caParameters[$miParamId];
          }
          $moStatic->setValue("$msStaticText");
          $moPanel->addObjFWDView($moStatic);
          $moPanel->setAttrDisplay('false');
          // Memo para a justificativa
          $moMemo = new FWDMemo();
          $moMemo->setAttrName($msNamePrefix.'justif_'.$miParamId);
          $moMemo->setObjFWDBox(new FWDBox(0,$this->ciLineHeight,$this->ciMemoHeight,$miWidth));
          $this->caMemos[$miParamId] = $moMemo;
          $moPanel->addObjFWDView($moMemo);
          // Atualiza vari�veis de controle para a pr�xima itera��o
          $miTop+= $this->ciLineHeight;
          $mbIsEven = !$mbIsEven;
        }
      }
      // Panel com a justificativa
      if(!$this->cbHorizontal){
        $moPanel = new FWDPanel();
        $moPanel->setAttrName($msNamePrefix.'dummy_panel');
        $moPanel->setAttrClass('justification_panel');
        $moPanel->setObjFWDBox(new FWDBox(0,$miJustificationTop,$this->ciMemoHeight+$this->ciLineHeight,$miWidth));
        $this->caContainers[] = $moPanel;
        // Static da justificativa
        $moStatic = new FWDStatic();
        $moStatic->setObjFWDBox(new FWDBox(0,$miOffsetTop,$this->ciElementsHeight,$miWidth));
        $moStatic->setValue(FWDLanguage::getPHPStringValue('mx_justification_bl','Justificativa'));
        $moPanel->addObjFWDView($moStatic);
        // Memo para a justificativa
        $moMemo = new FWDMemo();
        $moMemo->setAttrName($msNamePrefix.'dummy_memo');
        $moMemo->setObjFWDBox(new FWDBox(0,$this->ciLineHeight,$this->ciMemoHeight,$miWidth));
        $moMemo->setAttrDisabled('true');
        $moPanel->addObjFWDView($moMemo);
      }
      // Carrega os valores recebidos por post
      $maClasses = array();
      $miCount = 1;
      foreach($this->caValues as $ciId=>$_){
        $maClasses[$ciId] = 'value_'.floor($miCount*$mfFactor);
        $miCount++;
      }
      foreach($this->caSelects as $moSelect){
        $miPostValue = FWDWebLib::getPost($moSelect->getAttrName());
        if($miPostValue){
          $moSelect->setAttrValue($miPostValue);
          $moSelect->setAttrClass($maClasses[$miPostValue]);
        }
      }
      if(!$this->cbHorizontal){
        foreach($this->caMemos as $moMemo){
          $moPostValue = FWDWebLib::getPost($moMemo->getAttrName());
          $moMemo->setAttrValue($moPostValue);
        }
      }
      
      $this->cbBuilt = true;
    }
  }
  
 /**
  * Popula o elemento a partir do banco.
  *
  * <p>Popula o elemento buscando os nomes dos par�metros e dos valores no
  * banco.</p>
  * @access public
  */
  public function populate(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    if(isset($this->ciType)){
      // Busca no banco os nomes dos par�metros
      $this->caParameters = array();
      $moHandler = new QueryParameterNames(FWDWebLib::getConnection());
      $moHandler->makeQuery();
      $moHandler->executeQuery();
      $moDataset = $moHandler->getDataset();
      while($moDataset->fetch()){
        $miId = $moDataset->getFieldByAlias('parametername_id')->getValue();
        $msName = $moDataset->getFieldByAlias('parametername_name')->getValue();
        $this->addParameter($miId,$msName);
      }
      // Busca no banco os nomes dos valores
      $this->caValues = array();
      $moHandler = new QueryParameterValueNames(FWDWebLib::getConnection());
      $moHandler->setType($this->ciType);
      $moHandler->makeQuery();
      $moHandler->executeQuery();
      $moDataset = $moHandler->getDataset();
      while($moDataset->fetch()){
        $miId = $moDataset->getFieldByAlias('value_id')->getValue();
        $msName = $moDataset->getFieldByAlias('value_name')->getValue();
        $this->addValue($miId,$msName);
      }
      if($this->ciType != RISKPARAMETER_ASSET && $this->ciType != RISKPARAMETER_INCIDENTRISK){
        // Busca no banco os nomes dos valores de probabilidade
        $moHandler = new QueryProbabilityValueNames(FWDWebLib::getConnection());
        $moHandler->setType($this->ciType);
        $moHandler->makeQuery();
        $moHandler->executeQuery();
        $moDataset = $moHandler->getDataset();
        while($moDataset->fetch()){
          $miId = $moDataset->getFieldByAlias('value_id')->getValue();
          $msName = $moDataset->getFieldByAlias('value_name')->getValue();
          $this->addProbabilityValue($miId,$msName);
        }
      }
    }else{
      trigger_error('Parameter type must be defined.',E_USER_ERROR);
    }
  }
  
 /**
  * Desenha o cabe�alho do elemento.
  *
  * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
  * as informa��es necess�rias para implementar o cabe�alho do elemento.</p>
  * @access protected
  * @return string C�digo HTML que ser� inserido na p�gina
  */
  protected function drawHeader(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $this->build();
    $msStyle = 'overflow:hidden;position:absolute;z-index:1;'.$this->coBox->draw();
    return "<div name='{$this->csName}' id='{$this->csName}' class='{$this->csClass}' style='$msStyle'>";
  }
  
 /**
  * Desenha o corpo do elemento.
  *
  * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
  * as informa��es necess�rias para implementar o corpo do elemento.</p>
  * @access public
  * @param FWDBox $poBox Box que define os limites do elemento
  * @return string C�digo HTML que ser� inserido na p�gina
  */
  protected function drawBody(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $msOut = '';
    foreach($this->caContainers as $moViewGroup){
      $msOut.= $moViewGroup->draw();
    }
    return $msOut;
  }
  
 /**
  * Desenha o rodap� do elemento.
  *
  * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
  * as informa��es necess�rias para implementar o rodap� do elemento.</p>
  * @access protected
  * @return string C�digo HTML que ser� inserido na p�gina
  */
  protected function drawFooter(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $msParamsIds = implode(':',array_keys($this->caParameters));
    if($this->ciType != RISKPARAMETER_ASSET && $this->ciType != RISKPARAMETER_INCIDENTRISK) $msParamsIds.= ':prob';
    $msOut = "</div>"
            ."<script language='javascript'>"
               ."gebi('{$this->csName}').object = new ISMSRiskParameters('{$this->csName}','$msParamsIds');"
            ."</script>";
    return $msOut;
  }

 /**
  * Adiciona um par�metro ao elemento.
  *
  * <p>Adiciona um par�metro ao elemento.</p>
  * @access public
  * @param integer $piId Chave do par�metro
  * @param string $psName Nome do par�metro
  */
  public function addParameter($piId, $psName){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $this->cbBuilt = false;
    $this->caParameters[$piId] = $psName;
  }

 /**
  * Adiciona um valor ao elemento.
  *
  * <p>Adiciona um valor ao elemento.</p>
  * @access public
  * @param integer $piId Chave do valor
  * @param string $psName Nome do valor
  */
  public function addValue($piId, $psName){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $this->cbBuilt = false;
    $this->caValues[$piId] = $psName;
  }

/**
  * Adiciona um valor selecionado padrao.
  *
  * <p>Adiciona um valor selecionado padrao.</p>
  * @access public
  * @param integer $parameterId id do paramtro
  * @param string $valueSelected id do parametro selecionado
  */  
  public function addSelectedValue($parameterId, $valueSelected) {
  	$this->caSelectedValues[$parameterId] = $valueSelected;
  }
  
 /**
  * Adiciona um valor de probabilidade ao elemento.
  *
  * <p>Adiciona um valor de probabilidade ao elemento.</p>
  * @access public
  * @param integer $piId Chave do valor
  * @param string $psName Nome do valor
  */
  public function addProbabilityValue($piId, $psName){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $this->cbBuilt = false;
    $this->caProbabilityValues[$piId] = $psName;
  }
  
 /**
  * Define o valor de um par�metro.
  *
  * <p>Define o valor de um par�metro. A probabilidade � representada por um
  * par�metro com id 'prob'.</p>
  * @access public
  * @param string $psParamId Chave do par�metro
  * @param string $psValueId Chave do valor
  */
  public function setParameterValue($psParamId,$psValueId){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $this->build();
    if(isset($this->caSelects[$psParamId])){
      $this->caSelects[$psParamId]->setAttrValue($psValueId);
      $maItems = $this->caSelects[$psParamId]->getItems();
      foreach($maItems as $msKey=>$moItem){
        if($msKey==$psValueId){
          $this->caSelects[$psParamId]->setAttrClass($moItem->getAttrClass());
          break;
        }
      }
    }else{
      trigger_error("Invalid parameter id '$psParamId'.",E_USER_ERROR);
    }
  }
  
 /**
  * Define a justificativa de um par�metro.
  *
  * <p>Define a justificativa de um par�metro. A probabilidade � representada
  * por um par�metro com id 'prob'.</p>
  * @access public
  * @param string $psParamId Chave do par�metro
  * @param string $psJustification Justificativa do par�metro
  */
  public function setJustification($psParamId,$psJustification){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $this->build();
    if(isset($this->caMemos[$psParamId])){
      $this->caMemos[$psParamId]->setAttrValue($psJustification);
    }elseif(!isset($this->caParameters[$psParamId]) && $psParamId!='prob'){
      trigger_error("Invalid parameter id '$psParamId'.",E_USER_ERROR);
    }
  }
  
 /**
  * Retorna um array contendo o valor de cada par�metro.
  *
  * <p>Retorna um array associativo contendo o valor de cada par�metro indexado
  * pelo seu identificador.</p>
  * @access public
  * @return array Valores dos par�metros
  */
  public function getValues(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $this->build();
    $maValues = array();
    foreach($this->caSelects as $msParamId=>$moSelect){
      $maValues[$msParamId] = (string)$moSelect->getValue();
    }
    return $maValues;
  }

 /**
  * Retorna um array contendo a justificativa de cada par�metro.
  *
  * <p>Retorna um array associativo contendo a justificativa de cada par�metro
  * indexado pelo seu identificador.</p>
  * @access public
  * @return array Justificativas dos par�metros
  */
  public function getJustifications(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $this->build();
    $maJustifications = array();
    foreach($this->caMemos as $msParamId=>$moMemo){
      $maJustifications[$msParamId] = $moMemo->getValue();
    }
    return $maJustifications;
  }

 /**
  * Retorna o valor do atributo name
  * 
  * <p>Retorna o valor do atributo name.</p>
  * @access public
  * @return string Valor do atributo name
  */
  public function getAttrName(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return $this->csName;
  }

 /**
  * Seta o valor do atributo name
  * 
  * <p>Seta o valor do atributo name.</p>
  * @access public
  * @param string $psName Novo valor do atributo
  */
  public function setAttrName($psName){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $this->csName = $psName;
  }

 /**
  * Retorna o valor do atributo type
  * 
  * <p>Retorna o valor do atributo type.</p>
  * @access public
  * @return string Valor do atributo type
  */
  public function getAttrType(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return $this->ciType;
  }

 /**
  * Seta o valor do atributo type
  * 
  * <p>Seta o valor do atributo type.</p>
  * @access public
  * @param string $psType Novo valor do atributo
  */
  public function setAttrType($psType){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
  	$this->ciType = FWDWebLib::attributeParser($psType, array("asset"=>RISKPARAMETER_ASSET, 
			"risk"=>RISKPARAMETER_RISK, "control"=>RISKPARAMETER_RISKCONTROL, "incident_risk"=>RISKPARAMETER_INCIDENTRISK),"psType");
	$this->cbBuilt = false;
  }


 /**
  * Retorna o valor do atributo horizontal
  * 
  * <p>Retorna o valor do atributo horizontal.</p>
  * @access public
  * @return boolean Valor do atributo horizontal
  */
  public function getAttrHorizontal(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return $this->cbHorizontal;
  }

 /**
  * Seta o valor do atributo horizontal
  * 
  * <p>Seta o valor do atributo horizontal.</p>
  * @access public
  * @param boolean $pbHorizontal Novo valor do atributo
  */
  public function setAttrHorizontal($psHorizontal){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $this->cbHorizontal = FWDWebLib::attributeParser($psHorizontal,array("true"=>true, "false"=>false),"horizontal");
  }

 /**
  * Retorna o valor do atributo valuenamewidth
  * 
  * <p>Retorna o valor do atributo valuenamewidth.</p>
  * @access public
  * @return integer Valor do atributo valuenamewidth
  */
  public function getAttrValueNameWidth(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return $this->ciValueNameWidth;
  }

 /**
  * Seta o valor do atributo valuenamewidth
  * 
  * <p>Seta o valor do atributo valuenamewidth.</p>
  * @access public
  * @param integer $piValueNameWidth Novo valor do atributo
  */
  public function setAttrValueNameWidth($piValueNameWidth){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $this->ciValueNameWidth = $piValueNameWidth;
  }

 /**
  * Retorna o valor do atributo horizontalmargin
  * 
  * <p>Retorna o valor do atributo horizontalmargin.</p>
  * @access public
  * @return integer Valor do atributo horizontalmargin
  */
  public function getAttrHorizontalMargin(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return $this->ciHorizontalMargin;
  }

 /**
  * Seta o valor do atributo horizontalmargin
  * 
  * <p>Seta o valor do atributo horizontalmargin.</p>
  * @access public
  * @param integer $piHorizontalMargin Novo valor do atributo
  */
  public function setAttrHorizontalMargin($piHorizontalMargin){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $this->ciHorizontalMargin = $piHorizontalMargin;
  }

 /**
  * Retorna o valor do atributo class
  * 
  * <p>Retorna o valor do atributo class.</p>
  * @access public
  * @return string Valor do atributo class
  */
  public function getAttrClass(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return $this->csClass;
  }

 /**
  * Seta o valor do atributo class
  * 
  * <p>Seta o valor do atributo class.</p>
  * @access public
  * @param string $psClass Novo valor do atributo
  */
  public function setAttrClass($psClass){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $this->csClass = $psClass;
  }

 /**
  * Seta a box do elemento.
  *
  * <p>Seta a box do elemento.</p>
  * @access public
  * @param FWDBox $poBox Box do elemento
  */
  public function setObjFWDBox($poBox){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $this->coBox = $poBox;
  }
  
 /**
  * Retorna a box do elemento.
  *
  * <p>Retorna a box do elemento.</p>
  * @access public
  * @return FWDBox A box do elemento
  */
  public function getObjFWDBox(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return $this->coBox;
  }
  
 /**
  * Retorna um hash do array serializado, contendo o valor de cada par�metro.
  *
  * <p>Retorna um hash md5 do array associativo, serializado, contendo o valor de cada par�metro indexado
  * pelo seu identificador.</p>
  * @access public
  * @return string Array de valores dos par�metros
  */
  public function getHash(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $maValues = serialize($this->getValues());
    return md5($maValues);
  }
}
?>