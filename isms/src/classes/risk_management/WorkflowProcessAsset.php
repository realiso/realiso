<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe WorkflowProcessAsset.
 *
 * <p>Classe que herda de Workflow para possibilitar dupla aprova��o.</p>
 * @package ISMS
 * @subpackage classes
 */
class WorkflowProcessAsset extends RMWorkflow { 
 
 /**
  * Avan�a o estado do contexto no workflow.
  * 
  * <p>M�todo para avan�ar o estado do contexto no workflow.</p>
  * @access public 
  * 
  * @param RMProcessAsset $poProcessAsset Objeto
  * @param integer $piAction Tipo da a��o
  * @param string $psJustification Justificativa
  */
  public function stateForward(IWorkflow $poProcessAsset, $piAction, $psJustification = "") {  	
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
	$miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();	
		
	switch ($poProcessAsset->getContextState()) {
		case CONTEXT_STATE_NONE:
			$moProcess = new RMProcess();
			$moProcess->fetchById($poProcessAsset->getFieldValue('process_id'));
			$poProcessAsset->setApprover($moProcess->getResponsible());			
			
			/* 
			 * Se o usu�rio atual for o mesmo que o aprovador do processo, 
			 * n�o precisa enviar tarefa de aprova��o para ele, ou seja,
			 * passa direto para a aprova��o por parte do aprovador do ativo.
			 * Caso o usu�rio seja difirente, envia uma tarefa de aprova��o da
			 * associa��o para ele.
			 */
			if ($miUserId == $moProcess->getResponsible()) {
				$moAsset = new RMAsset();
				$moAsset->fetchById($poProcessAsset->getFieldValue('asset_id'));
				
				/*
				 * Se o usu�rio tamb�m for respons�vel pelo ativo, 
				 * aprovar automaticamente a associa��o. Caso contr�rio,
				 * envia uma tarefa de aprova��o para o respons�vel pelo
				 * ativo, assim como um alerta indicando que a associa��o
				 * j� foi co-aprovada.  
				 */
				if ($miUserId == $moAsset->getResponsible()) {
					$poProcessAsset->setContextState(CONTEXT_STATE_APPROVED);	
				}
				else {
					$poProcessAsset->setContextState(CONTEXT_STATE_COAPPROVED);
					$poProcessAsset->setApprover($moAsset->getResponsible());
					$this->dispatchAlert(WKF_ALERT_COAPPROVED, $poProcessAsset, $psJustification);
					$this->dispatchTask($this->ciActivity, $poProcessAsset->getId(), $poProcessAsset->getApprover());	
				}	
			}
			else {
				$this->dispatchTask($this->ciActivity, $poProcessAsset->getId(), $poProcessAsset->getApprover());
				$poProcessAsset->setContextState(CONTEXT_STATE_PENDANT);	
			}
		break;
		
		case CONTEXT_STATE_PENDANT:			
			switch ($piAction) {				
				case WKF_ACTION_APPROVE :
					$moAsset = new RMAsset();
					$moAsset->fetchById($poProcessAsset->getFieldValue('asset_id'));
					$poProcessAsset->setApprover($moAsset->getResponsible());				
									
					/*
					 * Se o criador da associa��o tamb�m for o respons�vel pelo ativo,
					 * aprova automaticamente. Caso contr�rio, seta o estado para co-aprovado.
					 */
					if ($poProcessAsset->getCreator() == $moAsset->getResponsible()) {
						$poProcessAsset->setContextState(CONTEXT_STATE_APPROVED);
					}
					else {
						$this->dispatchAlert(WKF_ALERT_COAPPROVED, $poProcessAsset, $psJustification);
						$this->dispatchTask($this->ciActivity, $poProcessAsset->getId(), $poProcessAsset->getApprover());
						$poProcessAsset->setContextState(CONTEXT_STATE_COAPPROVED);
					}
				break;
					
				case WKF_ACTION_DENY :
					$this->dispatchAlert(WKF_ALERT_DENIED, $poProcessAsset, $psJustification);									
					$poProcessAsset->setContextState(CONTEXT_STATE_DENIED);
				break;				
			}
		break;
		
		case CONTEXT_STATE_COAPPROVED:			
			switch ($piAction) {
				case WKF_ACTION_APPROVE:
					$this->dispatchAlert(WKF_ALERT_APPROVED, $poProcessAsset, $psJustification);									
					$poProcessAsset->setContextState(CONTEXT_STATE_APPROVED);
				break;
				
				case WKF_ACTION_DENY:
					$this->dispatchAlert(WKF_ALERT_DENIED, $poProcessAsset, $psJustification);									
					$poProcessAsset->setContextState(CONTEXT_STATE_DENIED);
				break;	
			}	
		break;
	}  	
  } 
}
?>