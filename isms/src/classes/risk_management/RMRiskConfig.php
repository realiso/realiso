<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
 /**
 * Classe RMRiskConfig.
 *
 * <p>Classe que retorna as configuracoes para os riscos do sistema.</p>
 * @package ISMS
 * @subpackage classes
 */
class RMRiskConfig {
  
 /**
  * M�todo para retornar a cor do risco
  * 
  * <p>M�todo para retornar a cor do risco, escrita ou em hexadecimal</p>
  * @access public
  * @param $value valor a ser comparado com vari�veis do sistema.
  * @param $hex booleano para retornar em hexadecimal ou escrita.
  */
  static function getRiskColor($piValue, $pbHex = false){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

    $return = "";
    
    $riskLevel = ISMSLib::getConfigById(RISK_LEVEL);
    if($riskLevel == 3){    
	    
	    $miRiskLow = ISMSLib::getConfigById(RISK_LOW);
	    $miRiskHigh = ISMSLib::getConfigById(RISK_HIGH);
	    
	    if($piValue==0){
	      $return = (!$pbHex)?"blue":"#0000FF";
	    }elseif($piValue <= $miRiskLow){
	      $return = (!$pbHex)?"green":"#008000";
	    }elseif($piValue < $miRiskHigh){
	      $return = (!$pbHex)?"yellow":"#DAA520";
	    }else{
	      $return = (!$pbHex)?"red":"#990000";
	    }
    } else if ($riskLevel == 5){

    	$miRiskLow = ISMSLib::getConfigById(RISK_LOW);
    	$miRiskMidLow = ISMSLib::getConfigById(RISK_MID_LOW);
    	$miRiskMidHigh = ISMSLib::getConfigById(RISK_MID_HIGH);
	    $miRiskHigh = ISMSLib::getConfigById(RISK_HIGH);
	     
	    if($piValue==0){
	      $return = (!$pbHex)?"blue":"#0000FF";
	    }elseif($piValue <= $miRiskLow){
	      $return = (!$pbHex)?"green":"#008000";
	    }elseif( ($piValue > $miRiskLow) &&  ($piValue <= $miRiskMidLow) ) {
	      $return = (!$pbHex)?"yellow":"#DAA520";
	    }elseif( ($piValue > $miRiskMidLow) &&  ($piValue <= $miRiskMidHigh) ) {
	      $return = (!$pbHex)?"orange":"#ff6c00";
	    }elseif( ($piValue >  $miRiskMidHigh) && ($piValue <= $miRiskHigh) ){
	      $return = (!$pbHex)?"red":"#990000";
	    }else{
	      $return = (!$pbHex)?"black":"#000000";
	    } 
	    	    
    }
    
    return $return;
  }

 /**
  * M�todo para retornar a label do risco
  * 
  * <p>M�todo para retornar a label do risco</p>
  * @access public
  * @param $value valor a ser comparado com vari�veis do sistema.
  */
  static function getRiskLabel($piValue){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

    $return = "";
    
    $riskLevel = ISMSLib::getConfigById(RISK_LEVEL);
    if($riskLevel == 3){    
	    
	    $miRiskLow = ISMSLib::getConfigById(RISK_LOW);
	    $miRiskHigh = ISMSLib::getConfigById(RISK_HIGH);
	    
	    if($piValue==0){
	      $return = FWDLanguage::getPHPStringValue('rs_not_estimated',"N�o Estimado");
	    }elseif($piValue <= $miRiskLow){
	      $return = FWDLanguage::getPHPStringValue('rs_low',"Baixo");
	    }elseif($piValue < $miRiskHigh){
	      $return = FWDLanguage::getPHPStringValue('rs_medium',"M�dio");
	    }else{
	      $return = FWDLanguage::getPHPStringValue('rs_high',"Alto");
	    }
    } else if ($riskLevel == 5){

    	$miRiskLow = ISMSLib::getConfigById(RISK_LOW);
    	$miRiskMidLow = ISMSLib::getConfigById(RISK_MID_LOW);
    	$miRiskMidHigh = ISMSLib::getConfigById(RISK_MID_HIGH);
	    $miRiskHigh = ISMSLib::getConfigById(RISK_HIGH);
	     
	    if($piValue==0){
	      $return = FWDLanguage::getPHPStringValue('rs_not_estimated',"N�o Estimado");  
	    }elseif($piValue <= $miRiskLow){
	      $return = FWDLanguage::getPHPStringValue('rs_low',"Baixo");
	    }elseif( ($piValue > $miRiskLow) &&  ($piValue <= $miRiskMidLow) ) {
	      $return = FWDLanguage::getPHPStringValue('rs_mid_low',"M�dio Baixo"); 
	    }elseif( ($piValue > $miRiskMidLow) &&  ($piValue <= $miRiskMidHigh) ) {
	      $return = FWDLanguage::getPHPStringValue('rs_medium',"M�dio");
	    }elseif( ($piValue >  $miRiskMidHigh) && ($piValue <= $miRiskHigh) ){
	      $return = FWDLanguage::getPHPStringValue('rs_mid_high',"M�dio Alto");
	    }else{
	      $return = FWDLanguage::getPHPStringValue('rs_high',"Alto");
	    } 
	    	    
    }
    
    return $return;
  }  

 /**
  * M�todo para retornar a label do risco numa query.
  * 
  * <p>M�todo para retornar a label do risco numa query</p>
  * @access public
  * @param $value valor a ser comparado com vari�veis do sistema.
  */  
  static function getRiskLabelSql($columnName){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

    $return = "";
    
    $strNotEstimated = FWDLanguage::getPHPStringValue('rs_not_estimated',"N�o Estimado");
    $strLow = FWDLanguage::getPHPStringValue('rs_low',"Baixo");
    $strMidLow = FWDLanguage::getPHPStringValue('rs_mid_low',"M�dio Baixo");
    $strMedium = FWDLanguage::getPHPStringValue('rs_medium',"M�dio");
    $strMidHigh = FWDLanguage::getPHPStringValue('rs_mid_high',"M�dio Alto");
    $strHigh = FWDLanguage::getPHPStringValue('rs_high',"Alto");
    
    $riskLevel = ISMSLib::getConfigById(RISK_LEVEL);
    if($riskLevel == 3){    
	    
	    $miRiskLow = ISMSLib::getConfigById(RISK_LOW);
	    $miRiskHigh = ISMSLib::getConfigById(RISK_HIGH);
	    
	    $return = "
		    CASE WHEN {$columnName} = 0 THEN '{$strNotEstimated}'
		    	 WHEN {$columnName} <= {$miRiskLow} THEN '{$strLow}'
		    	 WHEN {$columnName} < {$miRiskHigh} THEN '{$strMedium}'
		    	 ELSE '{$strHigh}'
			END 
	    ";
	    
    } else if ($riskLevel == 5){

    	$miRiskLow = ISMSLib::getConfigById(RISK_LOW);
    	$miRiskMidLow = ISMSLib::getConfigById(RISK_MID_LOW);
    	$miRiskMidHigh = ISMSLib::getConfigById(RISK_MID_HIGH);
	    $miRiskHigh = ISMSLib::getConfigById(RISK_HIGH);
	    
	    $return = "
		    CASE WHEN {$columnName} = 0 THEN '{$strNotEstimated}'
		    	 WHEN {$columnName} <= {$miRiskLow} THEN '{$strLow}'
		    	 WHEN ({$columnName} > {$miRiskLow}) AND {$columnName} <= {$miRiskMidLow} THEN '{$strMidLow}'
		    	 WHEN ({$columnName} > {$miRiskMidLow}) AND {$columnName} <= {$miRiskMidHigh} THEN '{$strMedium}'
		    	 WHEN ({$columnName} > {$miRiskMidHigh}) AND {$columnName} <= {$miRiskHigh} THEN '{$strMidHigh}'
		    	 ELSE '{$strHigh}'
			END 
	    ";	    
    }
    
    return $return;
  }   
  
	/**
	 * M�todo para retornar o valor do risco em percentual
	 * 
	 * <p>M�todo para retornar o valor do risco em percentual, dependendo da configura��o.</p>
	 * @access public
	 * @param $piValue valor do risco que ser� comparado
	 */
	 static function getRiskValueFormat($piValue) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
	 	$miPercentualRisk = ISMSLib::getConfigById(GENERAL_PERCENTUAL_RISK);
	 	if ($miPercentualRisk) {
	 		$miRiskCountValue = ISMSLib::getConfigById(RISK_VALUE_COUNT);
	 		$miRiskMaxValue = pow($miRiskCountValue,2);
	 		$return = ($piValue/$miRiskMaxValue);
	 		$return *= 100;
	 		$return = number_format($return,2);
	 	} else
	 		$return = number_format($piValue,2);
	 	
	 	return $return;
	 }
}
?>