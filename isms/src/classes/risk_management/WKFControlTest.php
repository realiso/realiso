<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe WKFControlTest.
 *
 * <p>Classe que representa a tabela de testes do controle.</p>
 * @package ISMS
 * @subpackage classes
 */

class WKFControlTest extends ISMSTable {

 /**
  * Construtor.
  * 
  * <p>Construtor da classe RMControlTest.</p>
  * @access public 
  */
  public function __construct(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    parent::__construct('wkf_control_test');
    
    $this->csAliasId = 'control_test_id';
    $this->coDataset->addFWDDBField(new FWDDBField('fkControlTest','control_test_id',DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('tDescription', 'description',    DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField('fkSchedule',   'schedule',       DB_NUMBER));
  }

 /**
  * Remove resgistros das tabelas do sistema.
  * 
  * <p>M�todo para remover registros das tabelas do sistema.</p>
  * @access public
  * @param integer $piId Identificador 
  */
  public function delete($piId = 0){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $moSchedule = new WKFSchedule();
    $miScheduleId = $this->getFieldValue('schedule');
    if(!$miScheduleId){
      $moControlTest = new WKFControlTest();
      $moControlTest->fetchById($piId);
      $miScheduleId = $moControlTest->getFieldValue('schedule');
    }
    if($miScheduleId){
      $moSchedule->delete($miScheduleId);
    }
    parent::delete($piId);
  }
  
 /**
  * Insere um registro na tabela.
  * 
  * <p>M�todo para inserir um registro na tabela.</p>
  * @param boolean $pbGetId Verdadeiro para retornar o id do registro inserido
  * @return integer Id do �ltimo registro inserido ou falso
  * @access public 
  */
  public function insert($pbGetId = false){
    $miId = parent::insert($pbGetId);
    $moTaskSchedule = new WKFTaskSchedule();
    $moTaskSchedule->setFieldValue('task_schedule_schedule',$this->getFieldValue('schedule'));
    $moTaskSchedule->setFieldValue('task_schedule_context',$this->getFieldValue('control_test_id'));
    $moTaskSchedule->setFieldValue('task_schedule_activity',ACT_CONTROL_TEST);
    $moTaskSchedule->setFieldValue('task_schedule_alert_type',WKF_ALERT_TEST_CONTROL);
    $moTaskSchedule->setFieldValue('task_schedule_alert_sent',false);
    $moTaskSchedule->insert();
    return $miId;
  }
  
}

?>