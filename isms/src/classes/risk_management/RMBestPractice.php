<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe RMBestPractice.
 *
 * <p>Classe que representa a tabela de melhores pr�ticas.</p>
 * @package ISMS
 * @subpackage classes
 */
class RMBestPractice extends ISMSContext implements IWorkflow {
  
 /**
  * Construtor.
  * 
  * <p>Construtor da classe RMBestPractice.</p>
  * @access public 
  */
  public function __construct(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    parent::__construct("rm_best_practice");
    $this->ciContextType = CONTEXT_BEST_PRACTICE;
    $this->csAliasId = "best_practice_id";
    $this->csDependenceAliasId = "section_best_practice_id";
    $this->cbHasDocument = false;
    $this->coWorkflow = new RMWorkflow(ACT_BEST_PRACTICE_APPROVAL);
    
    $this->coDataset->addFWDDBField(new FWDDBField("fkContext",               "best_practice_id",                   DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("fkSectionBestPractice",   "section_best_practice_id",           DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("sName",                   "best_practice_name",                 DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField("tDescription",            "best_practice_description",          DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField("nControlType",            "best_practice_control_type",         DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("sClassification",         "best_practice_classification",       DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField("tImplementationGuide",    "best_practice_imple_guide",          DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField("tMetric",                 "best_practice_metric",               DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField("sDocument",               "best_practice_document",             DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField("tJustification",          "best_practice_justification",        DB_STRING));
    
    $this->caSensitiveFields = array('best_practice_name');
    $this->caSearchableFields = array('best_practice_name', 'best_practice_description', 
                                      'best_practice_imple_guide',
                                      'best_practice_metric');
  }
 
 /**
  * Limpa os campos de justificativa da tabela.
  *
  * <p>M�todo para limpar os campos de justificativa da tabela.</p>
  * @access public
  */
  public function cleanJustifications() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $this->setFieldValue('best_practice_justification','');
    $this->createFilter(null, 'best_practice_id', "notnull");
    $this->update(0,false);
  }
 
 /**
  * Retorna o id do usu�rio que deve aprovar o contexto.
  *
  * <p>M�todo para retornar id do usu�rio que deve aprovar o contexto.</p>
  * @access public
  * @return integer Id do aprovador
  */
  public function getApprover(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return ISMSLib::getConfigById(USER_LIBRARIAN);
  }

  /**
   * Retorna o respons�vel da melhor pr�tica. 
   * 
   * <p>Retorna o respons�vel da melhor pr�tica. Esta fun��o s� existe
   * por compatibilidade, usada quando da associa��o de um documento
   * com uma melhor pr�tica.</p>
   * @access public
   * @return array Array com o respons�vel da �rea.
   */
  public function getReadersAndResponsible($piBestPracticeId) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $moBestPractice = new RMBestPractice();
    $moBestPractice->fetchById($piBestPracticeId);
    $miResponsibleId = $moBestPractice->getResponsible();
    if ($miResponsibleId)
      return array($miResponsibleId);
    else 
      return array();
  }

 /**
  * Retorna o id do usu�rio respons�vel pelo contexto.
  *
  * <p>M�todo para retornar id do usu�rio respons�vel pelo contexto.</p>
  * @access public
  * @return integer Id do respons�vel
  */
  public function getResponsible(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return $this->getCreator();
  }
 
 /**
  * Retorna o nome do contexto.
  * 
  * <p>M�todo para retornar o nome do contexto.</p>
  * @access public 
  * @return string Nome do contexto
  */
  public function getName() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return $this->getFieldValue('best_practice_name');
  }
  
 /**
  * Retorna a descri��o do contexto.
  * 
  * <p>M�todo para retornar a descri��o do contexto.</p>
  * @access public 
  * @return string Descri��o do contexto
  */ 
  public function getDescription() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return $this->getFieldValue('best_practice_description');
  }
  
 /**
  * Retorna os pais do subordinante do contexto.
  * 
  * <p>M�todo para retornar os pais do subordinante do contexto..</p>
  * @access public
  * @param boolean $pbExecByTrash se o metodo est� sendo chamado pela lixeira 
  * @return array Array de ids dos pais do subordinante do contexto
  * / 
  public function getBestPractice Parents($pbExecByTrash = false) {
    $moSection = new RMSectionBestPractice($pbExecByTrash);
    $miParentId = $this->getFieldValue($this->csDependenceAliasId);
    $maParents = $moSection->getSection Parents($miParentId);
    $maParents[] = $miParentId;
    return $maParents;
  }*/
  
 /**
  * Retorna os ancestrais do contexto.
  * 
  * <p>M�todo para retornar todos contextos acima de um determinado contexto na
  * �rvore de depend�ncias.</p>
  * @access public
  * @param integer $piContextId Id do contexto
  * @param boolean $pbExecByTrash se o metodo est� sendo chamado pela lixeira
  * @return array Array de ids dos ancestrais do contexto
  * /
  public function getSuperContexts($piContextId,$pbExecByTrash = false){
    $miSectionId = $this->getFieldValue('section_best_practice_id')->getValue();
    if(!$miSectionId){
      $moBestPractice = new RMBestPractice();
      $moBestPractice->fetchById($piContextId);
      $miSectionId = $moBestPractice->getFieldValue('section_best_practice_id')->getValue();
    }
    $moSection = new RMSectionBestPractice();
    $maSuperContexts = $moSection->getSuperContexts($moSection,$pbExecByTrash);
    $maSuperContexts[] = $miSectionId;
    return $maSuperContexts;
  }*/
  
 /**
  * Indica se o contexto � delet�vel ou n�o.
  * 
  * <p>M�todo que indica se o contexto � delet�vel ou n�o.</p>
  * @access public
  * @param integer $piContextId id do contexto
  */ 
  public function isDeletable($piContextId) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    // verificar exist�ncia de associa��o desta melhor pr�tica com um controle
    $moControlBP = new RMControlBestPractice();
    $moControlBP->createFilter($piContextId,"best_practice_id");
    $moControlBP->select();
    if ($moControlBP->fetch())
      return false;
    return true;
  }
  
 /**
  * Exibe uma popup caso n�o seja poss�vel remover um contexto.
  * 
  * <p>M�todo que exibe uma popup caso n�o seja poss�vel remover um contexto.</p>
  * @access public
  * @param integer $piContextId id do contexto
  */ 
  public function showDeleteError($piContextId) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $msTitle = FWDLanguage::getPHPStringValue('tt_best_practice_remove_error','Erro ao remover Melhor Pr�tica');
    $msMessage = FWDLanguage::getPHPStringValue('st_best_practice_remove_error_message',"N�o foi poss�vel remover a Melhor Pr�tica <b>%best_practice_name%</b>: A Melhor Pr�tica est� associada a um ou mais controles. Para remover Melhores Pr�ticas que estejam associadas a um ou mais controles, ative a op��o \"Delete Cascade\" na aba \"Admin -> Configura��o\".");
    $moBestPractice = new RMBestPractice();
    $moBestPractice->fetchById($piContextId);
    $msBestPracticeName = $moBestPractice->getFieldValue('best_practice_name');
    $msMessage = str_replace("%best_practice_name%",$msBestPracticeName,$msMessage);
    ISMSLib::openOk($msTitle,$msMessage,"",80);
  }
  
 /**
  * Retorna o �cone do contexto.
  *
  * <p>M�todo para retornar o �cone do contexto.</p>
  * @access public
  * @param integer $piContextId Id do contexto
  * @return string Nome do �cone
  */
  public function getIcon($piContextId = 0){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return 'icon-standard.gif';
  }

 /**
  * Retorna o label do contexto.
  * 
  * <p>M�todo para retornar o label do contexto.</p>
  * @access public 
  * @return string Label do contexto
  */
  public function getLabel() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return FWDLanguage::getPHPStringValue('mx_best_practice', "Melhor Pr�tica");
  }
  
 /**
  * Retorna o caminho do contexto.
  * 
  * <p>M�todo para retornar o caminho do contexto.</p>
  * @access public 
  * @return string Caminho do contexto
  */ 
  public function getSystemPath() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $miSectionId = $this->getFieldValue('section_best_practice_id');
    $moSection = new RMSectionBestPractice();
    $moSection->fetchById($miSectionId);
    
    $msPath = $moSection->getSystemPath();
    $msPath .="&nbsp;".ISMSLib::getIconCode('icon-arrow-right.gif',-2,0).'&nbsp;' . ISMSLib::getIconCode('icon-best_practice.gif',-2,-4). "<a href='javascript:isms_redirect_to_mode(" . LIBRARIES_MODE . "," . CONTEXT_SECTION_BEST_PRACTICE . "," . $miSectionId . ")'>" . $this->getName() . "</a>";
    return $msPath;
  }
  
 /**
  * Retorna se o usu�rio logado tem permi��o para inserir o contexto.
  * 
  * <p>M�todo para retornar se o usu�rio tem permi��o para inserir o contexto no sistema.</p>
  * @access public
  * @return boolean Se o usu�ro tem permi��o para inserir o contexto no sistema
  */
  protected function userCanInsert(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    //contexto deve ser tratado, assim assume-se que a principio o usu�rio n�o pode inserir tal contexto
    $mbReturn = false;
    $miCount = 0;

    $maPermissions = array( 'M.L.2.8' );
    //acls negadas o usu�rio logado no sistema
    $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
    foreach($maACLs as $miKey => $miValue){
      $maACLs[$miValue]=$miKey;
    }
    //acl1 AND acl2
    $mbAuxPermission = true;
    foreach($maPermissions as $msValue){
      if(isset($maACLs[$msValue])){
        $mbAuxPermission = false;
      }
      $miCount++;
    }
    //caso exista pelo menos 1 acl nas permi��es do contexto e estas permi��es n�o est�o nas permi��es negadas do usu�rio
    if(($mbAuxPermission)&&($miCount)){
      $mbReturn = $mbAuxPermission;
    }
    return $mbReturn;
  }

 /**
  * Retorna se o usu�rio logado tem permi��o para editar o contexto.
  * 
  * <p>M�todo para retornar se o usu�rio tem permi��o para editar o contexto no sistema.</p>
  * @access protected
  * @return boolean Se o usu�ro tem permi��o de editar o contexto no sistema
  */
  public function userCanEdit($piContextId,$piUserResponsible = 0){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    if(!$this->contextIsOfType($piContextId,$this->ciContextType)) return false;
    //contexto deve ser tratado, assim assume-se que a principio o usu�rio n�o pode editar esse contexto
    $mbReturn = false;
    $maPermissions = array( 'M.L.2.5' );
    //acls negadas do usu�rio logado no sistema
    $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
    foreach($maACLs as $miKey => $miValue){
      $maACLs[$miValue]=$miKey;
    }
    $miCount = 0;
    //acl1 AND acl2
    $mbAuxPermission = true;
    foreach($maPermissions as $msValue){
      if(isset($maACLs[$msValue])){
        $mbAuxPermission = false;
      }
      $miCount++;
    }
    //caso exista pelo menos 1 acl nas permi��es do contexto e estas permi��es n�o est�o nas permi��es negadas do usu�rio
    if(($mbAuxPermission)&&($miCount)){
      $mbReturn = $mbAuxPermission;
      }
    return $mbReturn;
  }
 
 /**
  * Retorna se o usu�rio logado tem permi��o para excluir o contexto.
  * 
  * <p>M�todo para retornar se o usu�rio tem permi��o para excluir o contexto no sistema.</p>
  * @access protected
  * @return boolean Se o usu�ro tem permi��o de excluir o contexto no sistema
  */
  protected function userCanDelete($piContextId){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $miContextType = $this->ciContextType;
    if(!$this->contextIsOfType($piContextId,$miContextType)) return false;
    //contexto deve ser tratado, assim assume-se que a principio o usu�rio n�o pode inserir tal contexto
    $mbReturn = false;
    $maPermissions = array( 'M.L.2.6' );

    //acls negadas do usu�rio logado no sistema
    $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
    foreach($maACLs as $miKey => $miValue){
      $maACLs[$miValue]=$miKey;
    }
    $miCount = 0;
    //acl1 AND acl2
    $mbAuxPermission = true;
    foreach($maPermissions as $msValue){
      if(isset($maACLs[$msValue])){
        $mbAuxPermission = false;
      }
      $miCount++;
    }
    //caso exista pelo menos 1 acl nas permi��es do contexto e estas permi��es n�o est�o nas permi��es negadas do usu�rio
    if(($mbAuxPermission)&&($miCount)){
      $mbReturn = $mbAuxPermission;
    }
    return $mbReturn;
  }
  
  /**
  * Retorna o c�digo HTML do icone do contexto.
  * 
  * <p>M�todo para retornar o c�digo HTML do contexto</p>
  * @access public
  * @param integer $piId Id do contexto
  * @param integer $pbIsFetched id do contexto
  * @return string contendo o c�digo HTML do icone do contexto
  */ 
  public function getIconCode($piId, $pbIsFetched = false){
  	return ISMSLib::getIconCode('icon-best_practice.gif', -2, 7);
  }

 /**
  * Retorna o caminho para abrir a popup de edi��o desse contexto.
  * 
  * <p>M�todo para retornar o caminho para abrir a popup de edi��o desse contexto.</p>
  * @param integer $piContextId id do contexto
  * @access public 
  * @return string caminho para abrir a popup de edi��o desse contexto
  */ 
  public function getVisualizeEditEvent($piContextId,$psUniqId){
    $soSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    $msPackagesLibrariesPath = ($soSession->getMode()==MANAGER_MODE) ? "packages/libraries/" : "../../packages/libraries/";
    $msPopupId = "popup_best_practice_edit";
    
    return "isms_open_popup('{$msPopupId}','{$msPackagesLibrariesPath}{$msPopupId}.php?best_practice=$piContextId','','true',502,500);"
           ."soPopUpManager.getPopUpById('{$msPopupId}').setCloseEvent( function close_visualize_$psUniqId() {soPopUpManager.closePopUp('popup_visualize_$psUniqId');} );";
  }

}
?>