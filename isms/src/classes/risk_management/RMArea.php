<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

include_once $handlers_ref . "QueryAreaParents.php";
include_once $handlers_ref . "QuerySubAreas.php";
include_once $handlers_ref . "QueryProcessesFromArea.php";
include_once $handlers_ref . "QueryAreaAutoReaders.php";

/**
 * Classe RMArea.
 *
 * <p>Classe que representa a tabela de �reas.</p>
 * @package ISMS
 * @subpackage classes
 */
class RMArea extends ISMSContext implements IWorkflow {
  
 /**
  * Construtor.
  * 
  * <p>Construtor da classe RMArea.</p>
  * @access public 
  */
  public function __construct(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    parent::__construct("rm_area");
    $this->ciContextType = CONTEXT_AREA;
    
    $this->csAliasId = "area_id";
    $this->csDependenceAliasId = "area_parent_id";
    $this->csSpecialDeleteSP = "delete_area";
    $this->cbHasDocument = true;
    $this->coWorkflow = new RMWorkflow(ACT_AREA_APPROVAL);

    $this->coDataset->addFWDDBField(new FWDDBField("fkContext",     "area_id",              DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("fkResponsible", "area_responsible_id",  DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("fkParent",      "area_parent_id",       DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("sName",         "area_name",            DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField("tDescription",  "area_description",     DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField("sDocument",     "area_document",        DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField("nValue",        "area_value",           DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("fkType",        "area_type",            DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("fkPriority",    "area_priority",        DB_NUMBER));

    $this->caSensitiveFields = array('area_name', 'area_responsible_id');
    $this->caSearchableFields = array('area_name', 'area_description');
  }

  /**
  * Insere uma area.
  * 
  * <p>M�todo para inserir uma �rea e retorna seu identificador.</p>
  * @access public
  * 
  * @param boolean $pbReturnId Indica se deve retornar o id do contexto inserido
  * @return integer Id do contexto inserido
  */ 
  public function insert($pbReturnId = false){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
  	// pega o nome do usu�rio atual
  	$maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
		//acl de inser��o de �rea de primeiro n�vel!
    if( !isset($maACLs['M.RM.1.7']) ){
    	return parent::insert($pbReturnId);
    }else{
		  if($this->getFieldValue('area_parent_id')==''){
				//usu�rio n�o pode inserir �rea de primeiro n�vel e nao est� setada o id da area_parent
				trigger_error('The area could not be inserted!', E_USER_ERROR);  
		  }else{
		  	//inser��o de uma �rea filha
		  	return parent::insert($pbReturnId);
		  }
    } 
  }
 
 /**
  * Retorna o id do usu�rio que deve aprovar o contexto.
  * 
  * <p>M�todo para retornar id do usu�rio que deve aprovar o contexto.</p>
  * @access public 
  * @return integer Id do aprovador
  */
  public function getApprover() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    if ($miParentId = $this->getFieldValue('area_parent_id')) {
      $moParent = new RMArea();
      $moParent->fetchById($miParentId);
      return $moParent->getFieldValue('area_responsible_id');
    }
    else return ISMSLib::getConfigById(USER_CHAIRMAN);
  }
  
 /**
  * Retorna o id do usu�rio respons�vel pelo contexto.
  * 
  * <p>M�todo para retornar id do usu�rio respons�vel pelo contexto.</p>
  * @access public 
  * @return integer Id do respons�vel
  */
  public function getResponsible() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return $this->getFieldValue('area_responsible_id');
  }
  
 /**
  * Retorna o valor do contexto.
  * 
  * <p>M�todo para retornar o valor do contexto.</p>
  * @access public 
  * @return integer valor do contexto
  */
  public function getValue(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
  	return $this->getFieldValue('area_value');
  }

 /**
  * Retorna o nome do contexto.
  * 
  * <p>M�todo para retornar o nome do contexto.</p>
  * @access public 
  * @return string Nome do contexto
  */
  public function getName() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return $this->getFieldValue('area_name');
  }

 /**
  * Retorna a descri��o do contexto.
  * 
  * <p>M�todo para retornar a descri��o do contexto.</p>
  * @access public 
  * @return string Descri��o do contexto
  */ 
  public function getDescription() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return $this->getFieldValue('area_description');
  }
  
 /**
  * Retorna os pais de uma �rea.
  * 
  * <p>M�todo para retornar os pais de uma �rea.</p>
  * @access public 
  * @param integer $piAreaId Id da �rea
  * @param boolean $pbExecByTrash se o metodo est� sendo chamado pela lixeira
  * @return array Array de ids das �reas pais
  */ 
  public function getAreaParents($piAreaId,$pbExecByTrash = false) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $moQuery = new QueryAreaParents(FWDWebLib::getConnection(),$pbExecByTrash);
    $moQuery->setArea($piAreaId);
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    return $moQuery->getParentAreas();
  }
  
 /**
  * Retorna os ancestrais do contexto.
  * 
  * <p>M�todo para retornar todos contextos acima de um determinado contexto na
  * �rvore de depend�ncias.</p>
  * @access public
  * @param integer $piContextId Id do contexto
  * @param boolean $pbExecByTrash se o metodo est� sendo chamado pela lixeira
  * @return array Array de ids dos ancestrais do contexto
  */
  public function getSuperContexts($piContextId,$pbExecByTrash = false){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return $this->getAreaParents($piContextId,$pbExecByTrash);
  }

 /**
  * Retorna as sub-�reas de uma �rea.
  * 
  * <p>M�todo para retornar as sub-�reas de uma �rea.</p>
  * @access public 
  * @param integer $piAreaId Id da �rea
  * @return array Array de ids das sub-�reas
  */ 
  public function getSubAreas($piAreaId) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $moQuery = new QuerySubAreas(FWDWebLib::getConnection());
    $moQuery->setArea($piAreaId);
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    return $moQuery->getSubAreas();
  }
  
 /**
  * Retorna os processos de uma �rea (e de suas sub-�reas, se existirem).
  * 
  * <p>M�todo para retornar os processos de uma �rea (e de suas sub-�reas,
  * se existirem).</p>
  * @access public 
  * @param integer $piAreaId Id da �rea
  * @return array Array de ids dos processos
  */ 
  public function getProcessesFromArea($piAreaId) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $moQuery = new QueryProcessesFromArea(FWDWebLib::getConnection());
    $moQuery->setArea($piAreaId);
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    return $moQuery->getProcesses();
  }
  
 /**
  * Retorna os sub-contextos de uma �rea (sub-�reas/processos).
  * 
  * <p>M�todo para retornar os sub-contextos de uma �rea (sub-�reas/processos).</p>
  * @access public 
  * @param integer $piAreaId Id da �rea
  * @return array Array de ids dos sub-contextos desta �rea
  */ 
  public function getSubContexts($piAreaId) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return array_merge($this->getSubAreas($piAreaId),$this->getProcessesFromArea($piAreaId));
  }
  
 /**
  * Indica se o contexto � delet�vel ou n�o.
  * 
  * <p>M�todo que indica se o contexto � delet�vel ou n�o.</p>
  * @access public
  * @param integer $piContextId id do contexto
  */ 
  public function isDeletable($piContextId) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    if (!ISMSLib::getConfigById(GENERAL_CASCADE_ON)) {
      // cascade off: verificar exist�ncia de subcontextos
      if ($this->getSubContexts($piContextId))
        return false;
    }
    return true;
  }
  
 /**
  * Exibe uma popup caso n�o seja poss�vel remover um contexto.
  * 
  * <p>M�todo que exibe uma popup caso n�o seja poss�vel remover um contexto.</p>
  * @access public
  * @param integer $piContextId id do contexto
  */ 
  public function showDeleteError($piContextId) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $msTitle = FWDLanguage::getPHPStringValue('tt_area_remove_erorr','Erro ao remover �rea de Neg�cio');
    $msMessage = FWDLanguage::getPHPStringValue('st_area_remove_error_message',"N�o foi poss�vel remover a �rea de Neg�cio <b>%area_name%</b>: A �rea cont�m Sub-�reas e/ou Processos. Para remover �reas de Neg�cio que contenham Sub-�reas ou Processos, ative a op��o \"Delete Cascade\" na aba \"Admin -> Configura��o\".");
    $moArea = new RMArea();
    $moArea->fetchById($piContextId);
    $msAreaName = $moArea->getFieldValue('area_name');
    $msMessage = str_replace("%area_name%",$msAreaName,$msMessage);
    ISMSLib::openOk($msTitle,$msMessage,"",80);
  }
  
 /**
  * Deleta logicamente um contexto.
  * 
  * <p>M�todo para deletar logicamente um contexto.</p>
  * @access public
  * @param integer $piContextId id do contexto
  */ 
  public function logicalDelete($piContextId) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $maProcesses = array_reverse($this->getProcessesFromArea($piContextId));
    $moProcess = new RMProcess();
    foreach ($maProcesses as $miProcessId) {
      $moProcess->delete($miProcessId);
    }
    unset($moProcess);
    
    // remove logicamente todas as sub-�reas desta area
    $maSubAreas = array_reverse($this->getSubAreas($piContextId));
    $moArea = new RMArea();
    foreach ($maSubAreas as $miSubAreaId) {
      $moArea->delete($miSubAreaId);
    }
    unset($moArea);
  }
  
 /**
  * Retorna o �cone do contexto.
  *
  * <p>M�todo para retornar o �cone do contexto.</p>
  * @access public
  * @param integer $piContextId Id do contexto
  * @return string Nome do �cone
  */
  public function getIcon($piContextId = 0){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return 'icon-area_gray.gif';
  }

 /**
  * Retorna o label do contexto.
  * 
  * <p>M�todo para retornar o label do contexto.</p>
  * @access public 
  * @return string Label do contexto
  */
  public function getLabel() {  	
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
  	return FWDLanguage::getPHPStringValue('mx_area', "�rea");  	
  }
  
 /**
  * Retorna o caminho do contexto para a cria��o do pathscroll.
  * 
  * <p>M�todo para retornar o caminho do contexto para a cria��o do pathscroll.</p>
  * @access public
  * @param integer $piTab id da tab
  * @param integer $piContextType id do contexto
  * @param string $psContextFunc fun��o que deve ser utilizada para gerar o c�digo do link  
  * @return array Array contendo o Caminho do contexto
  */ 
  public function getSystemPathScroll($piTab,$piContextType,$psContextFunc){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
 		$msHome = FWDLanguage::getPHPStringValue('st_home','Raiz');
  	$maPath = array();  	
    if($psContextFunc){
    	$maPath[]="<a href='javascript:{$psContextFunc}(0);'>";
    }else{
    	$maPath[]="<a href='javascript:isms_redirect_to_mode_scroll(" . $piTab . ",".CONTEXT_AREA.",0)'>";
    }
   	$maPath[]=$msHome;
   	$maPath[]=ISMSLib::getIconCode('',-1);
   	$maPath[]="</a>";
    $miAreaId = $this->getFieldValue('area_id');
    $maAreaParents = array_reverse($this->getAreaParents($miAreaId));    
    foreach ($maAreaParents as $miAreaParentId) {
    	$moAreaParent = new RMArea();
    	$moAreaParent->fetchById($miAreaParentId);
    	if($psContextFunc)
    		$maPath[] = "<a href='javascript:{$psContextFunc}({$miAreaParentId});'>";
    	else
				$maPath[] =  "<a href='javascript:isms_redirect_to_mode_scroll(" . $piTab . "," . CONTEXT_AREA . ",$miAreaParentId)'>";
		
			$maPath[]=$moAreaParent->getName();
			$maPath[]=ISMSLib::getIconCode('icon-area_',$moAreaParent->getValue());
   		$maPath[]="</a>";
    	unset($moAreaParent);	
    }
    
		if($psContextFunc)
    	$maPath[] = "<a href='javascript:{$psContextFunc}({$miAreaId});'>";
		else
    	$maPath[] = "<a href='javascript:isms_redirect_to_mode_scroll(" . $piTab . "," . $piContextType . ",$miAreaId)'>";
  	    
		$maPath[]=$this->getName();
		$maPath[]=ISMSLib::getIconCode('icon-area_',$this->getValue());
		$maPath[]="</a>";
    
  	$miIContPath = count($maPath);
	  
    for($miI=$miIContPath-1;$miI>0;$miI--){
			if((isset($maPath[$miI-7]))&&($maPath[$miI-7]!=''))
				$maPath[$miI-3] = $maPath[$miI-7];
			$miI=$miI-3;
    }
    return $maPath;
  }

 /**
  * Retorna o caminho do contexto.
  * 
  * <p>M�todo para retornar o caminho do contexto.</p>
  * @access public 
  * @return string Caminho do contexto
  */ 
  public function getSystemPath() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
  	$maPath = array();  	
    $maPath[] = "<a href='javascript:isms_redirect_to_mode(" . RISK_MANAGEMENT_MODE . ",0,0)'>".FWDLanguage::getPHPStringValue('st_rm_module_name','Gest�o de Riscos')."</a>";
        
    $miAreaId = $this->getFieldValue('area_id');
    $maAreaParents = array_reverse($this->getAreaParents($miAreaId));    
    foreach ($maAreaParents as $miAreaParentId) {
    	$moAreaParent = new RMArea();
    	$moAreaParent->fetchById($miAreaParentId);
    	$maPath[] = ISMSLib::getIconCode('icon-area_',$moAreaParent->getValue(),-4) . "<a href='javascript:isms_redirect_to_mode(" . RISK_MANAGEMENT_MODE . "," . $this->ciContextType . "," . $miAreaParentId . ")'>" . $moAreaParent->getName() . "</a>";
    	unset($moAreaParent);	
    }
    $maPath[] = ISMSLib::getIconCode('icon-area_',$this->getValue(),-4) . "<a href='javascript:isms_redirect_to_mode(" . RISK_MANAGEMENT_MODE . "," . $this->ciContextType . "," . $miAreaId . ")'>" . $this->getName() . "</a>";
    
    return implode("&nbsp;".ISMSLib::getIconCode('icon-arrow-right.gif',-2,0).'&nbsp;', $maPath);
  }
  
  /**
   * Retorna os leitores e o respons�vel da �rea.
   * 
   * <p>Retorna os leitores e o respons�vel da �rea.</p>
   * @access public
   * @return array Array com leitores + respons�vel da �rea.
   */
  public function getReadersAndResponsible($piAreaId) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $moArea = new RMArea();
    $moArea->fetchById($piAreaId);
    $miResponsibleId = $moArea->getResponsible();
    
    $moQuery = new QueryAreaAutoReaders(FWDWebLib::getConnection());
    $moQuery->setArea($piAreaId);
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    $maUsers = $moQuery->getReaders();
    
    if (!in_array($miResponsibleId,$maUsers)) $maUsers[] = $miResponsibleId;
    return $maUsers;
  }
  
 /**
  * Atualiza os leitores do documento da �rea.
  * 
  * <p>M�todo para atualizar os leitores do documento da �rea.</p>
  * @access public 
  * @param integer $piAreaId Id da �rea
  */
  public function updateReaders($piAreaId) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
  	/*
  	 * Busca, a partir dos usu�rios dos processos, todos os usu�rios
  	 * que est�o indiretamente relacionados com a �rea (e sub�res).
  	 * �rea -> [Sub�reas] -> Processos
  	 */
  	$moQuery = new QueryAreaAutoReaders(FWDWebLib::getConnection());
    $moQuery->setArea($piAreaId);
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    $maUsers = $moQuery->getReaders();
		
		/*
  	 * Para cada documento da �rea, atualiza seus leitores
  	 */
		$moDocContext = new PMDocContext();
  	$moDocContext->createFilter($piAreaId, 'context_id');
  	$moDocContext->select();  	
  	while($moDocContext->fetch()) {
  		$miDocId = $moDocContext->getFieldValue('document_id');  		
  		$moDocReaders = new PMDocumentReader();
  		$moDocReaders->updateUsers($miDocId, $maUsers);
  	}
  }

 /**
  * Retorna se o usu�rio logado tem permi��o para inserir o contexto.
  * 
  * <p>M�todo para retornar se o usu�rio tem permi��o para inserir o contexto no sistema.</p>
  * @access public
  * @return boolean Se o usu�ro tem permi��o para inserir o contexto no sistema
  */
  protected function userCanInsert(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $mbReturn = false;
    if(ISMSLib::userHasACL('M.RM.1.7') ||  ISMSLib::userHasACL('M.RM.1.6') ){
       $mbReturn = true;
    }
    return $mbReturn;
  }

 /**
  * Retorna se o usu�rio logado tem permi��o para editar o contexto.
  * 
  * <p>M�todo para retornar se o usu�rio tem permi��o para editar o contexto no sistema.</p>
  * @access protected
  * @return boolean Se o usu�ro tem permi��o de editar o contexto no sistema
  */
  public function userCanEdit($piContextId,$piUserResponsible = 0){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    if(!$this->contextIsOfType($piContextId,$this->ciContextType)) return false;
    $mbReturn = false;
    $miResponsibleId = $piUserResponsible;
    if(!$miResponsibleId){
      $miResponsibleId = $this->getFieldValue('area_responsible_id');
    }
    if(ISMSLib::userHasACL('M.RM.1.4')){
      $mbReturn = true;
    }elseif( ISMSLib::userHasACL('M.RM.1.8') && $miResponsibleId == ISMSLib::getCurrentUserId() ){
      $mbReturn = true;
    }
    return $mbReturn;
  }

 /**
  * Retorna se o usu�rio logado tem permi��o para excluir o contexto.
  * 
  * <p>M�todo para retornar se o usu�rio tem permi��o para excluir o contexto no sistema.</p>
  * @access protected
  * @return boolean Se o usu�ro tem permi��o de excluir o contexto no sistema
  */
  protected function userCanDelete($piContextId){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    if(!$this->contextIsOfType($piContextId,$this->ciContextType)) return false;
    $mbReturn = false;
    if(ISMSLib::userHasACL('M.RM.1.5')){
      $mbReturn = true;
    }else{
      $moContext = new RMArea();
      $moContext->fetchById($piContextId);
      if( ISMSLib::userHasACL('M.RM.1.9') && $moContext->getFieldValue('area_responsible_id') == ISMSLib::getCurrentUserId() ){
        $mbReturn = true;
      }
    }
    return $mbReturn;
  }

  /**
  * Retorna o c�digo HTML do icone do contexto.
  * 
  * <p>M�todo para retornar o c�digo HTML do contexto</p>
  * @access public
  * @param integer $piId Id do contexto
  * @param integer $pbIsFetched id do contexto
  * @return string contendo o c�digo HTML do icone do contexto
  */ 
  public function getIconCode($piId, $pbIsFetched = false){
    if(!$pbIsFetched){
       $this->fetchById($piId);
    }
    return ISMSLib::getIconCode('icon-area_'.RMRiskConfig::getRiskColor($this->getValue()).'.gif',-2,7);
  }
  
  /**
  * Retorna o caminho para abrir a popup de edi��o desse contexto.
  * 
  * <p>M�todo para retornar o caminho para abrir a popup de edi��o desse contexto.</p>
  * @param integer $piContextId id do contexto
  * @access public 
  * @return string caminho para abrir a popup de edi��o desse contexto
  */ 
  public function getVisualizeEditEvent($piContextId,$psUniqId){
    $soSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    $msPackagesRiskPath = ($soSession->getMode()==MANAGER_MODE) ? "packages/risk/" : "../../packages/risk/";
    $msPopupId = "popup_area_edit";
    
    return "isms_open_popup('{$msPopupId}','{$msPackagesRiskPath}{$msPopupId}.php?area=$piContextId','','true',297,440);"
          ."soPopUpManager.getPopUpById('{$msPopupId}').setCloseEvent( function close_visualize_$psUniqId() {soPopUpManager.closePopUp('popup_visualize_$psUniqId');} );";
  }

 }
?>