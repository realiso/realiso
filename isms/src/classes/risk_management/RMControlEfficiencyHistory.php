<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe RMControlEfficiencyHistory.
 *
 * <p>Classe que representa a tabela de histórico de eficiencia dos controles.</p>
 * @package ISMS
 * @subpackage classes
 */

class RMControlEfficiencyHistory extends ismsTable 
{
 /**
  * Construtor.
  * 
  * <p>Construtor da classe RMControl.</p>
  * @access public 
  */
  public function __construct(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    parent::__construct("rm_control_efficiency_history");
    
    $this->csAliasId = "control_date_todo";
    
    $this->coDataset->addFWDDBField(new FWDDBField("dDateTodo",            "control_date_todo",       DB_DATETIME));
    $this->coDataset->addFWDDBField(new FWDDBField("fkControl",            "control_id",              DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("dDateAccomplishment",  "control_date_realized",   DB_DATETIME));
    $this->coDataset->addFWDDBField(new FWDDBField("nRealEfficiency",      "control_real_eff",        DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField("nExpectedEfficiency",  "control_expected_eff",    DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField("nExpectedEfficiency",  "control_expected_eff",    DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("bIncident",            "is_by_induction",           DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("tJustification",            "justification",           DB_STRING));
  }
}  
?>