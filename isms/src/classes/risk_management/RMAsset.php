<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

include_once $handlers_ref . "QueryRisksFromAsset.php";
include_once $handlers_ref . "QueryAssetAutoReaders.php";
include_once $handlers_ref . "QueryControlsByAsset.php";

/**
 * Classe RMAsset.
 *
 * <p>Classe que representa a tabela de ativos.</p>
 * @package ISMS
 * @subpackage classes
 */
class RMAsset extends ISMSContext implements IWorkflow {
  
  
 /**
  * Construtor.
  *
  * <p>Construtor da classe RMAsset.</p>
  * @access public
  */
  public function __construct(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    parent::__construct('rm_asset');
    $this->ciContextType = CONTEXT_ASSET;

    $this->csAliasId = "asset_id";
    $this->cbHasDocument = true;
    $this->coWorkflow = new RMWorkflow(ACT_ASSET_APPROVAL);
    
    $this->coDataset->addFWDDBField(new FWDDBField('fkContext',            'asset_id',                     DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('fkSecurityResponsible','asset_security_responsible_id',DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('fkResponsible',        'asset_responsible_id',         DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('fkCategory',           'asset_category_id',            DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('sName',                'asset_name',                   DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField('tDescription',         'asset_description',            DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField('sDocument',            'asset_document',               DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField('nCost',                'asset_cost',                   DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('nValue',               'asset_value',                  DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('bLegality',            'asset_legality',               DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('tJustification',       'asset_justification',          DB_STRING));
    
    $this->caSensitiveFields = array('asset_name', 'asset_category_id', 'asset_responsible_id', 'asset_security_responsible_id');
    $this->caSearchableFields = array('asset_name', 'asset_description', 'asset_justification');
  }

 /**
  * Retorna o id do usu�rio que deve aprovar o contexto.
  *
  * <p>M�todo para retornar id do usu�rio que deve aprovar o contexto.</p>
  * @access public
  * @return integer Id do aprovador
  */
  public function getApprover(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return ISMSLib::getConfigById(USER_ASSET_CONTROLLER);
  }

 /**
  * Retorna o id do usu�rio respons�vel pelo contexto.
  *
  * <p>M�todo para retornar id do usu�rio respons�vel pelo contexto.</p>
  * @access public
  * @return integer Id do respons�vel
  */
  public function getResponsible(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return $this->getFieldValue('asset_responsible_id');
  }

 /**
  * Retorna o nome do contexto.
  *
  * <p>M�todo para retornar o nome do contexto.</p>
  * @access public
  * @return string Nome do contexto
  */
  public function getName(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return $this->getFieldValue('asset_name');
  }
  
 /**
  * Retorna a descri��o do contexto.
  * 
  * <p>M�todo para retornar a descri��o do contexto.</p>
  * @access public 
  * @return string Descri��o do contexto
  */ 
  public function getDescription() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return $this->getFieldValue('asset_description');
  }
  
 /**
  * Retorna os riscos associados a um ativo.
  * 
  * <p>M�todo para retornar os riscos associados a um ativo.</p>
  * @access public 
  * @param integer $piAssetId 
  * @return array Array de ids dos riscos
  */ 
  public function getRisksFromAsset($piAssetId) {$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $moQuery = new QueryRisksFromAsset(FWDWebLib::getConnection());
    $moQuery->setAsset($piAssetId);
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    return $moQuery->getRisks();
  }
  
 /**
  * Retorna os sub-contextos de um ativo (riscos).
  * 
  * <p>M�todo para retornar os sub-contextos de um ativo (riscos).</p>
  * @access public 
  * @param integer $piAssetId Id do ativo
  * @return array Array de ids dos sub-contextos desta ativo
  */ 
  public function getSubContexts($piAssetId) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return $this->getRisksFromAsset($piAssetId);
  }
  
 /**
  * Indica se o contexto � delet�vel ou n�o.
  * 
  * <p>M�todo que indica se o contexto � delet�vel ou n�o.</p>
  * @access public
  * @param integer $piContextId id do contexto
  */ 
  public function isDeletable($piContextId) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    if (!ISMSLib::getConfigById(GENERAL_CASCADE_ON)) {
      // cascade off: verificar exist�ncia de subcontextos
      if ($this->getSubContexts($piContextId))
        return false;
    }
    return true;
  }
  
 /**
  * Exibe uma popup caso n�o seja poss�vel remover um contexto.
  * 
  * <p>M�todo que exibe uma popup caso n�o seja poss�vel remover um contexto.</p>
  * @access public
  * @param integer $piContextId id do contexto
  */ 
  public function showDeleteError($piContextId) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $msTitle = FWDLanguage::getPHPStringValue('tt_asset_remove_error','Erro ao remover Ativo');
    $msMessage = FWDLanguage::getPHPStringValue('st_asset_remove_error_message',"N�o foi poss�vel remover o Ativo <b>%asset_name%</b>: Existem Riscos associados ao Ativo. Para remover Ativos para o qual existem Riscos associados, ative a op��o \"Delete Cascade\" na aba \"Admin -> Configura��o\".");
    $moAsset = new RMAsset();
    $moAsset->fetchById($piContextId);
    $msAssetName = $moAsset->getFieldValue('asset_name');
    $msMessage = str_replace("%asset_name%",$msAssetName,$msMessage);
    ISMSLib::openOk($msTitle,$msMessage,"",80);
  }
  
 /**
  * Deleta logicamente um contexto.
  * 
  * <p>M�todo para deletar logicamente um contexto.</p>
  * @access public
  * @param integer $piContextId id do contexto
  */ 
  public function logicalDelete($piContextId) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $maRisks = array_reverse($this->getRisksFromAsset($piContextId));
    $moRisk = new RMRisk();
    foreach ($maRisks as $miRiskId) {
      $moRisk->delete($miRiskId);
    }
    unset($moRisk);
    //delete tarefas e tasks da rela��o entre ativo X processo
    $moRel = new RMProcessAsset();
    $moRel->createFilter($piContextId,'asset_id');
    $moRel->select();
    while($moRel->fetch()){
      // deletar tarefas do contexto deletado
      $moTasks = new WKFTask();
      $moTasks->createFilter($moRel->getFieldValue('process_asset_id'), 'task_context_id');
      $moTasks->delete();
      // deletar alertas do contexto deletado
      $moAlerts = new WKFAlert();
      $moAlerts->createFilter($moRel->getFieldValue('process_asset_id'), 'alert_context_id');
      $moAlerts->delete();
    }
  }
  
 /**
  * Retorna o �cone do contexto.
  *
  * <p>M�todo para retornar o �cone do contexto.</p>
  * @access public
  * @param integer $piContextId Id do contexto
  * @return string Nome do �cone
  */
  public function getIcon($piContextId = 0){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return 'icon-asset_gray.gif';
  }

 /**
  * Retorna o label do contexto.
  * 
  * <p>M�todo para retornar o label do contexto.</p>
  * @access public 
  * @return string Label do contexto
  */
  public function getLabel() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return FWDLanguage::getPHPStringValue('mx_asset', "Ativo");
  }

 /**
  * Retorna o valor do contexto.
  * 
  * <p>M�todo para retornar o valor do contexto.</p>
  * @access public 
  * @return integer valor do contexto
  */
  public function getValue(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
  	return $this->getFieldValue('asset_value');
  }

 /**
  * Retorna o caminho do contexto para a cria��o do pathscroll.
  * 
  * <p>M�todo para retornar o caminho do contexto para a cria��o do pathscroll.</p>
  * @access public 
  * @param integer $piTab id da tab
  * @param integer $piContextType id do contexto
  * @param string $psContextId indices do filtro de ids dos links do pathscroling  
  * @return array Array contendo o Caminho do contexto
  */ 
  public function getSystemPathScroll($piTab,$piContextType,$psContextId){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
  	$msHome = FWDLanguage::getPHPStringValue('st_home','Raiz');
  	$maPath=array();
  	$paContextId = explode(':',$psContextId);
  	$psProcessId = '';
  	if((isset($paContextId[1]))&&($paContextId[1]))
  		$psProcessId .= $paContextId[1];
  		if(isset($paContextId[2]))
  			$psProcessId .= ':'. $paContextId[2];			  
		if($psProcessId){
				$moProcess = new RMProcess();
				$moProcess->fetchById($paContextId[1]);
				$maPath = $moProcess->getSystemPathScroll(RISK_MANAGEMENT_MODE,CONTEXT_PROCESS,$psProcessId);
		}else{
				$maPath[] = "<a href='javascript:isms_redirect_to_mode_scroll(".$piTab.", ".$piContextType.",0);'>";
				$maPath[] = $msHome;
				$maPath[]=ISMSLib::getIconCode('icon-asset_',-1);
				$maPath[] = "</a>";
		}
		if($psProcessId){
			if(!(strpos($psProcessId,':')===false))
					$psProcessId="\\\"$psProcessId\\\"";
		}else
			$psProcessId = 0;
 		$maPath[]= "<a href='javascript:isms_redirect_to_mode_scroll({$piTab},{$piContextType},$psProcessId);'>";
 		$maPath[]= $this->getName();
 		$maPath[]=ISMSLib::getIconCode('icon-asset_',$this->getValue());
 		$maPath[]= "</a>";
    return $maPath;
  }
  
 /**
  * Retorna o caminho do contexto.
  * 
  * <p>M�todo para retornar o caminho do contexto.</p>
  * @access public 
  * @return string Caminho do contexto
  */ 
  public function getSystemPath() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $msRMPath = "<a href='javascript:isms_redirect_to_mode(" . RISK_MANAGEMENT_MODE . ",0,0)'>".FWDLanguage::getPHPStringValue('st_rm_module_name','Gest�o de Riscos')."</a>";
    $msContextPath = ISMSLib::getIconCode('icon-asset_',$this->getValue(),-4). "<a href='javascript:isms_redirect_to_mode(" . RISK_MANAGEMENT_MODE . "," . CONTEXT_RISK . "," . $this->getFieldValue($this->getAliasId()) . ")'>" . $this->getName() . "</a>";
    return $msRMPath ."&nbsp;".ISMSLib::getIconCode('icon-arrow-right.gif',-2,0).'&nbsp;' . $msContextPath;
  }  
 
  /**
   * Retorna os leitores e o respons�vel do ativo.
   * 
   * <p>Retorna os leitores e o respons�vel do ativo.</p>
   * @access public
   * @return array Array com leitores + respons�vel do ativo.
   */
  public function getReadersAndResponsible($piAssetId) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $moAsset = new RMAsset();
    $moAsset->fetchById($piAssetId);
    $miResponsibleId = $moAsset->getResponsible();
    
    $moQuery = new QueryAssetAutoReaders(FWDWebLib::getConnection());
    $moQuery->setAsset($piAssetId);
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    $maUsers = $moQuery->getReaders();
    
    if (!in_array($miResponsibleId,$maUsers)) $maUsers[] = $miResponsibleId;
    return $maUsers;
  }
 
 /**
  * Atualiza os leitores do documento do ativo.
  * 
  * <p>M�todo para atualizar os leitores do documento do ativo.</p>
  * @access public 
  * @param integer $piAssetId Id do ativo
  */
  public function updateReaders($piAssetId) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
  	/*
  	 * Busca, a partir dos usu�rios dos processos, todos os usu�rios
  	 * que est�o indiretamente relacionados com o ativo.
  	 * Ativo -> Processo -> Usu�rios
  	 */
  	$moQuery = new QueryAssetAutoReaders(FWDWebLib::getConnection());
    $moQuery->setAsset($piAssetId);
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    $maUsers = $moQuery->getReaders();		
		
		/*
  	 * Para cada documento do ativo, atualiza os leitores dos documentos
  	 */
		$moDocContext = new PMDocContext();
    $moDocContext->createFilter($piAssetId, 'context_id');
    $moDocContext->select();    
    while($moDocContext->fetch()) {
      $miDocId = $moDocContext->getFieldValue('document_id');
  		$moDocReaders = new PMDocumentReader();
  		$moDocReaders->updateUsers($miDocId, $maUsers);
  	}
  	
  	/*
  	 * Atualiza os leitores dos documentos dos controles que est�o
  	 * associados com os riscos do ativo
  	 */  	
  	$moQuery = new QueryControlsByAsset(FWDWebLib::getConnection());
    $moQuery->setAsset($piAssetId);
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    $maControls = $moQuery->getControls();
  	
  	$moControl = new RMControl();
  	foreach ($maControls as $miControlId) $moControl->updateReaders($miControlId);  	
  }
  
 /**
  * Deleta um ativo.
  * 
  * <p>M�todo para deletar um ativo.
  * Necess�rio para caso seja uma dele��o de banco (n�o l�gica),
  * excluir antes da tabela de depend�ncias de ativos.</p>
  * @access public
  * @param integer $piContextId Id do contexto
  * @param boolean $pbDeleteDB Remove do banco?
  */ 
  public function delete($piContextId, $pbDeleteDB = false) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
  	if ($pbDeleteDB) {
  		$moAssetAsset = new RMAssetAsset();
  		$moAssetAsset->createFilter($piContextId, 'asset_id');  		
  		$moAssetAsset->delete();
  		$moAssetAsset->removeAllFilters();	  		
  		$moAssetAsset->createFilter($piContextId, 'dependent_id');
  		$moAssetAsset->delete();
  	}
 
  	parent::delete($piContextId,$pbDeleteDB,$pbDeleteDB);
  }
  
 /**
  * Retorna se o usu�rio logado tem permi��o para inserir o contexto.
  * 
  * <p>M�todo para retornar se o usu�rio tem permi��o para inserir o contexto no sistema.</p>
  * @access public
  * @return boolean Se o usu�ro tem permi��o para inserir o contexto no sistema
  */
  protected function userCanInsert(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $mbReturn = false;
    if(ISMSLib::userHasACL('M.RM.3.7')){
      $mbReturn = true;
    }
    return $mbReturn;
  }

 /**
  * Retorna se o usu�rio logado tem permi��o para editar o contexto.
  * 
  * <p>M�todo para retornar se o usu�rio tem permi��o para editar o contexto no sistema.</p>
  * @access protected
  * @return boolean Se o usu�ro tem permi��o de editar o contexto no sistema
  */
  public function userCanEdit($piContextId,$piUserResponsible = 0){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    if(!$this->contextIsOfType($piContextId,$this->ciContextType)) return false;
    $mbReturn = false;
    $miResponsibleId = $piUserResponsible;
    if(!$miResponsibleId){
      $miResponsibleId = $this->getFieldValue('asset_responsible_id');
    }
    if(ISMSLib::userHasACL('M.RM.3.5')){
      $mbReturn = true;
    }elseif( ISMSLib::userHasACL('M.RM.3.8') && $miResponsibleId == ISMSLib::getCurrentUserId() ){
      $mbReturn = true;
    }elseif(ISMSLib::userHasACL('M.RM.3.10')){ // ou quando estiver carregando risco da biblioteca.
      $mbReturn = true;
    }
    return $mbReturn;
  }
  
 /**
  * Retorna se o usu�rio logado tem permi��o para excluir o contexto.
  * 
  * <p>M�todo para retornar se o usu�rio tem permi��o para excluir o contexto no sistema.</p>
  * @access protected
  * @return boolean Se o usu�ro tem permi��o de excluir o contexto no sistema
  */
  protected function userCanDelete($piContextId){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    if(!$this->contextIsOfType($piContextId,$this->ciContextType)) return false;
    $mbReturn = false;
    if(ISMSLib::userHasACL('M.RM.3.6')){
      $mbReturn = true;
    }else{
      $moContext = new RMAsset();
      $moContext->fetchById($piContextId);
      if( ISMSLib::userHasACL('M.RM.3.9') && $moContext->getFieldValue('asset_responsible_id') == ISMSLib::getCurrentUserId() ){
        $mbReturn = true;
      }
    }
    return $mbReturn;
  }

  /**
  * Retorna o c�digo HTML do icone do contexto.
  * 
  * <p>M�todo para retornar o c�digo HTML do contexto</p>
  * @access public
  * @param integer $piId Id do contexto
  * @param integer $pbIsFetched id do contexto
  * @return string contendo o c�digo HTML do icone do contexto
  */ 
  public function getIconCode($piId, $pbIsFetched = false){
    if(!$pbIsFetched){
      $this->fetchById($piId);
    }
    return ISMSLib::getIconCode('icon-asset_'.RMRiskConfig::getRiskColor($this->getValue()).'.gif',-2,7);
  }

   /**
  * Retorna o caminho para abrir a popup de edi��o desse contexto.
  * 
  * <p>M�todo para retornar o caminho para abrir a popup de edi��o desse contexto.</p>
  * @param integer $piContextId id do contexto
  * @access public 
  * @return string caminho para abrir a popup de edi��o desse contexto
  */ 
  public function getVisualizeEditEvent($piContextId,$psUniqId){
    $soSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    $msPackagesRiskPath = ($soSession->getMode()==MANAGER_MODE) ? "packages/risk/" : "../../packages/risk/";
    $msPopupId = "popup_asset_edit";
    
    return "isms_open_popup('{$msPopupId}','{$msPackagesRiskPath}{$msPopupId}.php?asset=$piContextId','','true',472,855);"
           ."soPopUpManager.getPopUpById('{$msPopupId}').setCloseEvent( function close_visualize_$psUniqId() {soPopUpManager.closePopUp('popup_visualize_$psUniqId');} );";
  }

}
?>