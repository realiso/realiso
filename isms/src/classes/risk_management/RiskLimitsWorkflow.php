<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe RiskLimitsWorkflow.
 *
 * <p>Classe que manipula os estados do workflow de RiskLimits.</p>
 * @package ISMS
 * @subpackage classes
 */
class RiskLimitsWorkflow extends Workflow {

 /**
  * Construtor.
  *
  * <p>Construtor da classe Workflow.</p>
  * @access public
  */
  public function __construct(){}

 /**
  * Avan�a o estado do contexto no workflow.
  * 
  * <p>M�todo para avan�ar o estado do contexto no workflow.</p>
  * @access public 
  * 
  * @param IWorkflow $poDocument Objeto do documento
  * @param integer $piAction Tipo da a��o
  * @param string $psJustification Justificativa
  */
  public function stateForward(IWorkflow $poContext, $piAction, $psJustification = ''){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $moContext = new RMRiskLimits();
    $moContext->fetchById($poContext->getId());
    $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    $miApproverId = $moContext->getApprover();
    $mbUserEqualApprover = ($miUserId == $miApproverId);
    
    switch($moContext->getContextState()){
      case CONTEXT_STATE_NONE:{
        if($mbUserEqualApprover){
          $moContext->setContextState(CONTEXT_STATE_APPROVED);
        }else{
          $moContext->setContextState(CONTEXT_STATE_PENDANT);
          $this->dispatchTask(ACT_RISK_LIMITS_APPROVAL, $moContext->getId(), $miApproverId);
        }
        return;
      }
      case CONTEXT_STATE_PENDANT:{
        switch($piAction){
          case WKF_ACTION_APPROVE: $moContext->setContextState(CONTEXT_STATE_APPROVED); return;
          case WKF_ACTION_DENY:    $moContext->setContextState(CONTEXT_STATE_DENIED);   return;
        } 
      }
    }
    trigger_error("The action '$piAction' at state '{$moContext->getContextState()}' is not implemented.",E_USER_ERROR);
  }

}
?>