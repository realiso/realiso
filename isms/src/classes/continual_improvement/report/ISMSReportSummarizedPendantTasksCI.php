<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe ISMSReportSummarizedPendantTasksCI.
 *
 * <p>Classe que implementa o relat�rio resumido de tarefas pendentes do m�dulo
 * de gest�o de incidentes.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportSummarizedPendantTasksCI extends ISMSReport {
  
  /**
   * Inicializa o relat�rio.
   *
   * <p>M�todo para inicializar o relat�rio.
   * Necess�rio para funcionar com o XML compilado.</p>
   * @access public
   */  
  public function init() {
    parent::init();
        
    $this->coDataSet->addFWDDBField(new FWDDBField("","user_id",DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("","user_name",DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("","task_activity",DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("","task_count",DB_NUMBER));
  }
  
  /**
   * Executa a query do relat�rio.
   *
   * <p>M�todo para executar a query do relat�rio.</p>
   * @access public
   */
  public function makeQuery() {
    $moConfig = new ISMSConfig();
    $msWhere = '';
    $maFilterTypes = array(
            ACT_NON_CONFORMITY_APPROVAL,
            ACT_ACTION_PLAN_FINISH_CONFIRM,
            ACT_ACTION_PLAN_REVISION,
            ACT_OCCURRENCE_APPROVAL,
            ACT_INC_DISPOSAL_APPROVAL,
            ACT_INC_SOLUTION_APPROVAL,
            ACT_INC_CONTROL_INDUCTION,
            ACT_AP_APPROVAL
    );

    if(count($maFilterTypes)){
      $msWhere .= "AND wt.nActivity IN (".implode(',',$maFilterTypes).") ";
    }
    
    $this->csQuery = "SELECT u.fkContext as user_id, u.sName as user_name,  
                        wt.nActivity as task_activity, count(wt.nActivity) as task_count
                      FROM view_isms_user_active u
                      JOIN wkf_task wt ON (u.fkContext = wt.fkReceiver AND wt.bVisible = 1)
                      $msWhere
                      GROUP BY u.fkContext, u.sName, wt.nActivity
                      ORDER BY u.fkContext";
    
    return parent::executeQuery();
  }
}
?>