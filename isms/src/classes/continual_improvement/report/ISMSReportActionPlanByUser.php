<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportActionPlanByUser.
 *
 * <p>Classe que implementa o relat�rio de plano de a��o por respons�vel.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportActionPlanByUser extends ISMSReport {

  public function __construct(){
    parent::__construct();
  }

 /**
  * Inicializa o relat�rio.
  *
  * <p>M�todo para inicializar o relat�rio.
  * Necess�rio para funcionar com o XML compilado.</p>
  * @access public
  */
  public function init(){
    parent::init();

	$col1 = new FWDDBField('ap.fkResponsible','responsible_id'  ,DB_NUMBER);
	$col1->setLabel(' ');
	$col1->setShowColumn(false);

	$col2 = new FWDDBField('u.sName'         ,'responsible_name',DB_STRING);
	$col2->setLabel(FWDLanguage::getPHPStringValue('rs_responsible','Respons�vel'));
	$col2->setShowColumn(true);

	$col3 = new FWDDBField('ap.fkContext'    ,'ap_id'           ,DB_NUMBER);
	$col3->setLabel(' ');
	$col3->setShowColumn(false);

	$col4 = new FWDDBField('ap.sName'        ,'ap_name'         ,DB_STRING);
	$col4->setLabel(FWDLanguage::getPHPStringValue('mx_action_plan','Plano de A��o'));
	$col4->setShowColumn(true);

	$col5 = new FWDDBField('ctx.nState'      ,'ap_state'        ,DB_NUMBER);
	$col5->setLabel(FWDLanguage::getPHPStringValue('gc_state','Estado'));
	$col5->setShowColumn(true);

  $col6 = new FWDDBField('ap.trevisionjustification'      ,'ap_justification'        ,DB_STRING);
  $col6->setLabel(FWDLanguage::getPHPStringValue('gc_justification','Justificativa'));
  $col6->setShowColumn(true);

  //teste
  $col7 = new FWDDBField('ap.ddateefficiencyrevision'      ,'ap_date'        ,DB_DATETIME);
  $col7->setLabel(FWDLanguage::getPHPStringValue('gc_justification','Data'));
  $col7->setShowColumn(true);

    $this->coDataSet->addFWDDBField($col1);
    $this->coDataSet->addFWDDBField($col2);
    $this->coDataSet->addFWDDBField($col3);
    $this->coDataSet->addFWDDBField($col4);
    $this->coDataSet->addFWDDBField($col5);
    $this->coDataSet->addFWDDBField($col6);
    $this->coDataSet->addFWDDBField($col7);

  }

 /**
  * Executa a query do relat�rio.
  *
  * <p>M�todo para executar a query do relat�rio.</p>
  * @access public
  */
  public function makeQuery(){

    $where = "";

    if($this->coFilter->getActionType() > 0){
      $actionType = $this->coFilter->getActionType();
      $where = "WHERE ap.nactiontype = {$actionType}";
    }

    $this->csQuery = "SELECT
                      	ap.fkResponsible as responsible_id,
                      	u.sName as responsible_name,
                      	ap.fkContext as ap_id,
                      	ap.sName as ap_name,
                      	ctx.nState as ap_state,
                        ap.trevisionjustification as ap_justification,
                        ap.ddateefficiencyrevision as ap_date
                      FROM
                      	view_ci_action_plan_active ap
                      	JOIN view_isms_user_active u ON (ap.fkResponsible = u.fkContext)
                      	JOIN view_isms_context_active ctx ON (ap.fkContext = ctx.pkContext)

                      {$where}

                      ORDER BY u.sName";

    return parent::executeQuery();
  }

 /**
  * Desenha o cabe�alho do relat�rio.
  *
  * <p>M�todo para desenhar o cabe�alho do relat�rio.</p>
  * @access public
  */
  public function drawHeader(){
    foreach($this->caHeaders as $moHeader){
      if($moHeader->getAttrName()=='header_filter'){
        $moHeaderFilter = $moHeader;
      }elseif($moHeader->getAttrName()=='header_filter_item'){
        $moHeaderFilterItem = $moHeader;
      }elseif($moHeader->getAttrName()=='report_header_blank'){
        $moHeaderBlank = $moHeader;
      }else{
        $this->coWriter->drawLine($moHeader,array());
      }
    }
    if($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());

    $moFilterText = FWDWebLib::getObject('header_filter_text');
    $moFilterItemText = FWDWebLib::getObject('header_filter_item_text');

    $maFilters = $this->coFilter->getSummary();

    foreach($maFilters as $maFilter){
      $moFilterText->setValue($maFilter['name']);
      $this->coWriter->drawLine($moHeaderFilter,array());

      $moFilterItemText->setValue($maFilter['items']);
      $this->coWriter->drawLine($moHeaderFilterItem,array());
    }
    $this->coWriter->drawLine($moHeaderBlank,array());
  }
}

?>