<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportIncidentFilter.
 *
 * <p>Classe que implementa o filtro de relat�rios de incidente.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportActionPlanFilter extends FWDReportFilter {

  protected $ciActionType = 0;
  
  public function setActionType($type){
    $this->ciActionType = $type;
  }
  
  public function getActionType(){
    return $this->ciActionType;
  }
  
  public function getSummary(){
  	$maFilters = array();
    $actionType = "";
  	
  	if($this->ciActionType == 0){
      $actionType = FWDLanguage::getPHPStringValue('mx_both','Ambas');  	  
  	} else if ($this->ciActionType == ACTION_PLAN_CORRECTIVE_TYPE){
      $actionType = FWDLanguage::getPHPStringValue('mx_corrective','Corretiva');
  	} else if ($this->ciActionType == ACTION_PLAN_PREVENTIVE_TYPE){
      $actionType = FWDLanguage::getPHPStringValue('mx_preventive','Preventiva');
  	}
  	
  	$maFilters[] = array(
  		'name' => FWDLanguage::getPHPStringValue('lb_action_type', 'Tipo de A��o:'),
  		'items' => $actionType
  	);
  
  	return $maFilters;
  }
}
?>