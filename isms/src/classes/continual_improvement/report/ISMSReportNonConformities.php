<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportNonConformities.
 *
 * <p>Classe que implementa o relat�rio de n�o conformidades.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportNonConformities extends ISMSFilteredReport {
  
  public function __construct(){
    parent::__construct();
  }
  
 /**
  * Inicializa o relat�rio.
  *
  * <p>M�todo para inicializar o relat�rio.
  * Necess�rio para funcionar com o XML compilado.</p>
  * @access public
  */
  public function init(){
    parent::init();
    $this->coDataSet->addFWDDBField(new FWDDBField('nc.fkContext','nc_id'  ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('nc.sName'    ,'nc_name',DB_STRING));
  }
  
 /**
  * Executa a query do relat�rio.
  *
  * <p>M�todo para executar a query do relat�rio.</p>
  * @access public
  */
  public function makeQuery(){
    $miStatus = $this->coFilter->getStatus();
    $miProcess = $this->coFilter->getProcess();
    
    $maFilters = array();
    
    if($miStatus){
      $maFilters[] = "EXISTS (
                        SELECT *
                        FROM isms_context c
                        WHERE
                          c.pkContext = nc.fkContext
                          AND c.nState = {$miStatus}
                      )";
    }
    
    if($miProcess){
      $maFilters[] = "EXISTS (
                        SELECT *
                        FROM view_ci_nc_process_active ncp
                        WHERE
                          ncp.fkNC = nc.fkContext
                          AND ncp.fkProcess = {$miProcess}
                      )";
    }
    
    if(count($maFilters)==0){
      $msWhere = '';
    }else{
      $msWhere = ' WHERE '.implode(' AND ',$maFilters);
    }
    $this->csQuery = "SELECT
                        nc.fkContext AS nc_id,
                        nc.sName AS nc_name
                      FROM
                        view_ci_nc_active nc
                      {$msWhere}
                      ORDER BY nc_name";
    return parent::executeQuery();
  }

}

?>