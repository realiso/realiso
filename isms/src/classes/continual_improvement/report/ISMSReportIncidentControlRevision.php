<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe ISMSReportIncidentControlRevision.
 *
 * <p>Classe que implementa o relat�rio revis�o de controles geradas por incidentes.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportIncidentControlRevision extends ISMSReport {
	
	/**
	 * Inicializa o relat�rio.
	 *
	 * <p>M�todo para inicializar o relat�rio.
	 * Necess�rio para funcionar com o XML compilado.</p>
	 * @access public
	 */	
	public function init() {
		parent::init();
		
		$col1 = new FWDDBField("","control_id",DB_NUMBER);
		$col1->setLabel(' ');
		$col1->setShowColumn(false);
		
		$col2 = new FWDDBField("","control_name",DB_STRING);
		$col2->setLabel(FWDLanguage::getPHPStringValue('cb_control','Controle'));
		$col2->setShowColumn(true);		
	
		$col3 = new FWDDBField("","control_is_active",DB_NUMBER);
		$col3->setLabel(' ');
		$col3->setShowColumn(false);
		
		$col4 = new FWDDBField("","control_history_date_todo",DB_DATETIME);
		$col4->setLabel(FWDLanguage::getPHPStringValue('rs_date_todo','Data Prevista'));
		$col4->setShowColumn(true);		
	
		$col5 = new FWDDBField("","control_hist_date_accomplish",DB_DATETIME);
		$col5->setLabel(FWDLanguage::getPHPStringValue('rs_date_accomplishment','Data Realiza��o'));
		$col5->setShowColumn(true);
		
		$col6 = new FWDDBField("","control_real_efficiency",DB_NUMBER);
		$col6->setLabel(FWDLanguage::getPHPStringValue('rs_er','ER'));
		$col6->setShowColumn(true);     
	    
		$col7 = new FWDDBField("","control_expected_efficiency",DB_NUMBER);
		$col7->setLabel(FWDLanguage::getPHPStringValue('rs_ee','EE'));
		$col7->setShowColumn(true);		
		
		$this->coDataSet->addFWDDBField($col1);
		$this->coDataSet->addFWDDBField($col2);		
		$this->coDataSet->addFWDDBField($col3);
		$this->coDataSet->addFWDDBField($col4);
		$this->coDataSet->addFWDDBField($col5);
		$this->coDataSet->addFWDDBField($col6);
		$this->coDataSet->addFWDDBField($col7);
		
	}
	
	/**
	 * Executa a query do relat�rio.
	 *
	 * <p>M�todo para executar a query do relat�rio.</p>
	 * @access public
	 */
	public function makeQuery() {
		$this->csQuery = "SELECT c.fkContext as control_id, c.sName as control_name, c.bIsActive as control_is_active,
												ceh.dDateTodo as control_history_date_todo, ceh.dDateAccomplishment as control_hist_date_accomplish,
												ceh.nRealEfficiency as control_real_efficiency, ceh.nExpectedEfficiency as control_expected_efficiency
											FROM view_rm_control_active c
											JOIN rm_control_efficiency_history ceh ON (c.fkContext = ceh.fkControl)
											WHERE ceh.bIncident = 1
											ORDER BY c.fkContext, ceh.dDateTodo DESC";
		
		return parent::executeQuery();
	}
}
?>