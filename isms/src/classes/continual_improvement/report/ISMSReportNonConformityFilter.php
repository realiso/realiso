<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportNonConformityFilter.
 *
 * <p>Classe que implementa o filtro de n�o conformidade.</p>
 * @package ISMS
 * @subpackage report
 */

include_once $handlers_ref . "select/QuerySelectNonConformityType.php";

class ISMSReportNonConformityFilter extends FWDReportFilter {

	protected $ciStatus = 0;
  protected $ciCapability = 0;
  protected $ciClassificationType = 0;
  protected $ciEstimatedConclusionStart = 0;
  protected $ciEstimatedConclusionFinish = 0;
  protected $ciConclusionStart = 0;
  protected $ciConclusionFinish = 0;

  public function setStatus($piStatus){
    $this->ciStatus = $piStatus;
  }

  public function getStatus(){
    return $this->ciStatus;
  }

  public function setCapability($piCapability){
    $this->ciCapability = $piCapability;
  }

  public function getCapability(){
    return $this->ciCapability;
  }

  public function setClassificationType($piClassificationType){
    $this->ciClassificationType = $piClassificationType;
  }

  public function getClassificationType(){
    return $this->ciClassificationType;
  }

  public function setEstimatedConclusionDateStart($piDate){
    $this->ciEstimatedConclusionStart = $piDate;
  }

  public function setEstimatedConclusionDateFinish($piDate){
    $this->ciEstimatedConclusionFinish = $piDate;
  }

  public function setConclusionDateStart($piDate){
    $this->ciConclusionStart = $piDate;
  }

  public function setConclusionDateFinish($piDate){
    $this->ciConclusionFinish = $piDate;
  }

  public function getEstimatedConclusionDateStart() {
  	return $this->ciEstimatedConclusionStart;
  }

  public function getEstimatedConclusionDateFinish() {
  	return $this->ciEstimatedConclusionFinish;
  }

  public function getConclusionDateStart() {
  	return $this->ciConclusionStart;
  }

  public function getConclusionDateFinish() {
  	return $this->ciConclusionFinish;
  }


  public function getSummary(){
    $maFilters = array();

    if($this->ciCapability){
      if ($this->ciCapability == NC_NO_CAPABILITY) $msCapability = FWDLanguage::getPHPStringValue('mx_no','N�o');
      else $msCapability = FWDLanguage::getPHPStringValue('mx_yes','Sim');
    }
    else $msCapability = FWDLanguage::getPHPStringValue('mx_all','Todos');
    $maFilters[] = array(
      	'name' => FWDLanguage::getPHPStringValue('lb_potential_cl','Potencial:'),
      	'items' => array($msCapability)
    );

    if($this->ciClassificationType){
      $moSelectNonConformityType = new QuerySelectNonConformityType();
	  $moSelectNonConformityType->setTypeId($this->ciClassificationType);
      $msClassificationType = $moSelectNonConformityType->getTypeName();
      
	  /*
      if ($this->ciClassificationType == NC_ACTION_CLASSIFICATION_INTERNAL_AUDIT) $msClassificationType = FWDLanguage::getPHPStringValue('mx_internal_audit','Auditoria Interna');
      elseif ($this->ciClassificationType == NC_ACTION_CLASSIFICATION_EXTERNAL_AUDIT) $msClassificationType = FWDLanguage::getPHPStringValue('mx_external_audit','Auditoria Externa');
      else $msClassificationType = FWDLanguage::getPHPStringValue('mx_security_control','Controle de Seguran�a');
      */
    }
    else $msClassificationType = FWDLanguage::getPHPStringValue('mx_all','Todas');
    $maFilters[] = array(
      	'name' => FWDLanguage::getPHPStringValue('lb_classification_cl','Classifica��o:'),
      	'items' => array($msClassificationType)
    );

    if($this->ciStatus){
      $moContext = new ISMSContextObject();
      $msStatus = $moContext->getContextStateAsString($this->ciStatus);
    }
    else $msStatus = FWDLanguage::getPHPStringValue('mx_all','Todos');
    $maFilters[] = array(
      	'name' => FWDLanguage::getPHPStringValue('lb_status_cl','Status:'),
      	'items' => array($msStatus)
    );

    $msInitialDate = ($this->ciEstimatedConclusionStart?ISMSLib::getISMSShortDate($this->ciEstimatedConclusionStart,true):'');
    $msFinalDate = ($this->ciEstimatedConclusionFinish?ISMSLib::getISMSShortDate($this->ciEstimatedConclusionFinish,true):'');
    if($msInitialDate || $msFinalDate){
      if($msInitialDate){
        if($msFinalDate){
          $msDateFilter = FWDLanguage::getPHPStringValue('mx_between_initialdate_finaldate','Entre %initialDate% e %finalDate%');
        }else{
          $msDateFilter = FWDLanguage::getPHPStringValue('mx_after_initialdate','Depois de %initialDate%');
        }
      }else{
        $msDateFilter = FWDLanguage::getPHPStringValue('mx_before_finaldate','Antes de %finalDate%');
      }
      $msDateFilter = str_replace(array('%initialDate%','%finalDate%'),array($msInitialDate,$msFinalDate),$msDateFilter);
      $maFilters[] = array(
        'name' => FWDLanguage::getPHPStringValue('lb_deadline_cl','Prazo:'),
        'items' => array($msDateFilter)
      );
    }

    $msInitialDate = ($this->ciConclusionStart?ISMSLib::getISMSShortDate($this->ciConclusionStart,true):'');
    $msFinalDate = ($this->ciConclusionFinish?ISMSLib::getISMSShortDate($this->ciConclusionFinish,true):'');
    if($msInitialDate || $msFinalDate){
      if($msInitialDate){
        if($msFinalDate){
          $msDateFilter = FWDLanguage::getPHPStringValue('mx_between_initialdate_finaldate','Entre %initialDate% e %finalDate%');
        }else{
          $msDateFilter = FWDLanguage::getPHPStringValue('mx_after_initialdate','Depois de %initialDate%');
        }
      }else{
        $msDateFilter = FWDLanguage::getPHPStringValue('mx_before_finaldate','Antes de %finalDate%');
      }
      $msDateFilter = str_replace(array('%initialDate%','%finalDate%'),array($msInitialDate,$msFinalDate),$msDateFilter);
      $maFilters[] = array(
        'name' => FWDLanguage::getPHPStringValue('lb_conclusion_date_cl','Data de Conclus�o:'),
        'items' => array($msDateFilter)
      );
    }    
    return $maFilters;
  }
}
?>