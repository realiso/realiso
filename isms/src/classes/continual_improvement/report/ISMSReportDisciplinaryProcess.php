<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportDisciplinaryProcess.
 *
 * <p>Classe que implementa o relatório de processos disciplinares.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportDisciplinaryProcess extends ISMSFilteredReport {
  
  public function __construct(){
    parent::__construct();
  }
  
 /**
  * Inicializa o relatório.
  *
  * <p>Método para inicializar o relatório.
  * Necessário para funcionar com o XML compilado.</p>
  * @access public
  */
  public function init(){
    parent::init();
    $this->coDataSet->addFWDDBField(new FWDDBField('iu.fkIncident','dp_id'  ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('i.sName'      ,'dp_name',DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('iu.fkUser'    ,'dp_user',DB_NUMBER));
  }
  
 /**
  * Executa a query do relatório.
  *
  * <p>Método para executar a query do relatório.</p>
  * @access public
  */
  public function makeQuery(){
    $miUser = $this->coFilter->getUser();
    
    $maFilters = array();
    
    if($miUser){
      $maFilters[] = "iu.fkUser = {$miUser}";
    }
    
    if(count($maFilters)==0){
      $msWhere = '';
    }else{
      $msWhere = ' WHERE '.implode(' AND ',$maFilters);
    }
    $this->csQuery = "SELECT
                        iu.fkIncident AS dp_id,
                        i.sName as dp_name,
                        iu.fkUser AS dp_user
                      FROM
                        view_ci_incident_user_active iu
                        JOIN view_ci_incident_active i ON (i.fkContext = iu.fkIncident)
                      {$msWhere}
                      ORDER BY iu.tDescription";
    return parent::executeQuery();
  }

}

?>