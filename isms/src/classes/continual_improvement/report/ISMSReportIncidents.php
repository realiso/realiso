<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportIncidents.
 *
 * <p>Classe que implementa o relatório de incidentes.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportIncidents extends ISMSFilteredReport {
  
  public function __construct(){
    parent::__construct();
  }
  
 /**
  * Inicializa o relatório.
  *
  * <p>Método para inicializar o relatório.
  * Necessário para funcionar com o XML compilado.</p>
  * @access public
  */
  public function init(){
    parent::init();
    $this->coDataSet->addFWDDBField(new FWDDBField('i.fkContext','incident_id'  ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('i.sName'    ,'incident_name',DB_STRING));
  }
  
 /**
  * Executa a query do relatório.
  *
  * <p>Método para executar a query do relatório.</p>
  * @access public
  */
  public function makeQuery(){
    $miStatus = $this->coFilter->getStatus();
    $miAsset = $this->coFilter->getAsset();
    $miProcess = $this->coFilter->getProcess();
    
    $maFilters = array();
    
    if($miStatus){
      $maFilters[] = "EXISTS (
                        SELECT *
                        FROM isms_context c
                        WHERE
                          c.pkContext = i.fkContext
                          AND c.nState = {$miStatus}
                      )";
    }
    
    if($miAsset){
      $maFilters[] = "EXISTS (
                        SELECT *
                        FROM
                          view_ci_incident_risk_active ir
                          JOIN view_rm_risk_active r ON (r.fkContext = ir.fkRisk)
                        WHERE
                          ir.fkIncident = i.fkContext
                          AND r.fkAsset = {$miAsset}
                      )";
    }
    
    if($miProcess){
      $maFilters[] = "EXISTS (
                        SELECT *
                        FROM view_ci_inc_process_active ip
                        WHERE
                          ip.fkIncident = i.fkContext
                          AND ip.fkProcess = {$miProcess}
                      )";
    }
    
    if(count($maFilters)==0){
      $msWhere = '';
    }else{
      $msWhere = ' WHERE '.implode(' AND ',$maFilters);
    }
    $this->csQuery = "SELECT
                        i.fkContext AS incident_id,
                        i.sName AS incident_name
                      FROM view_ci_incident_active i
                      {$msWhere}
                      ORDER BY incident_name";
    return parent::executeQuery();
  }

}

?>