<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe IncidentWorkflow.
 *
 * <p>Classe que manipula os estados do workflow.
 * O workflow � respons�vel por disparar as devidas tarefas.
 * � ele que sabe quais e quantas tarefas ser�o disparadas, de acordo
 * com o estado do contexto utilizado.</p>
 * @package ISMS
 * @subpackage classes
 */
class IncidentWorkflow extends Workflow {

 /**
  * Construtor.
  *
  * <p>Construtor da classe Workflow.</p>
  * @access public
  */
  public function __construct(){}

 /**
  * Avan�a o estado do contexto no workflow.
  * 
  * <p>M�todo para avan�ar o estado do contexto no workflow.</p>
  * @access public 
  * 
  * @param IWorkflow $poDocument Objeto do documento
  * @param integer $piAction Tipo da a��o
  * @param string $psJustification Justificativa
  */
  public function stateForward(IWorkflow $poContext, $piAction, $psJustification = ''){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $moContext = new CIIncident();
    $moContext->fetchById($poContext->getId());
    $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    $miApproverId = $moContext->getApprover();//incident manager
    $miResponsibleId = $moContext->getResponsible();// respons�vel setado pelo usu�rio
    $mbUserEqualApprover = ($miUserId == $miApproverId);
    $mbUserEqualResponsible = ($miUserId == $miResponsibleId);
    $mbDeletedResponsible = ($miResponsibleId == 0);
    
    if($piAction==WKF_ACTION_DELETE){
      $moContext->setContextState(CONTEXT_STATE_DELETED);
      return;
    }

    switch($moContext->getContextState()){
      case CONTEXT_STATE_NONE:{
        if($mbUserEqualApprover){
          if($mbUserEqualResponsible){
            $this->dispatchAlert(WKF_ALERT_INC_SENT_TO_RESPONSIBLE,$moContext,$psJustification);
          	$moContext->setContextState(CONTEXT_STATE_INC_DIRECTED);
          }else{
            $moContext->setContextState(CONTEXT_STATE_INC_OPEN);
          }
        }else{
          $this->dispatchTask(ACT_INC_APPROVAL, $moContext->getId(), $miApproverId);
          $moContext->setContextState(CONTEXT_STATE_PENDANT);
        }
        return;
      }
      case CONTEXT_STATE_PENDANT:{
        if($mbUserEqualApprover){
          switch($piAction){
            case WKF_ACTION_APPROVE:{
              if($mbUserEqualResponsible){
                $moContext->setContextState(CONTEXT_STATE_INC_DIRECTED);
              }else{
                $this->dispatchAlert(WKF_ALERT_APPROVED,$moContext,$psJustification);
                $moContext->setContextState(CONTEXT_STATE_INC_OPEN);
              }
              break;
            }
            CASE WKF_ACTION_DENY:{
              // enviar alerta para o responsavel com coment�rio do porque o incidente foi negado
              $this->dispatchAlert(WKF_ALERT_DENIED,$moContext,$psJustification);
              $moContext->setContextState(CONTEXT_STATE_DENIED);
              break;
            }
          }
        }
        return;
      }
      case CONTEXT_STATE_DENIED:{
        if($mbUserEqualResponsible && WKF_ACTION_SEND_TO_APPROVAL){
          if(!$mbUserEqualApprover){
            $this->dispatchTask(ACT_INC_APPROVAL, $moContext->getId(), $miApproverId);
            $moContext->setContextState(CONTEXT_STATE_PENDANT);
          }else{
            $moContext->setContextState(CONTEXT_STATE_INC_DIRECTED);
          }
        }
        return;
      }
      case CONTEXT_STATE_INC_OPEN:{
        switch($piAction) {
          case WKF_ACTION_INC_SEND_TO_RESPONSIBLE:
            if(!$mbUserEqualResponsible) $this->dispatchAlert(WKF_ALERT_INC_SENT_TO_RESPONSIBLE,$moContext,$psJustification);
            $moContext->setContextState(CONTEXT_STATE_INC_DIRECTED);
            return;
          case WKF_ACTION_INC_SEND_TO_APP_DISPOSAL:
            $this->dispatchAlert(WKF_ALERT_INC_DISPOSAL_APP,$moContext,$psJustification);
            $moContext->setContextState(CONTEXT_STATE_INC_WAITING_SOLUTION);
            return;
        } 
      }
      case CONTEXT_STATE_INC_DIRECTED:{
        if($piAction==WKF_ACTION_INC_SEND_TO_APP_DISPOSAL){
          $moContext->setContextState(CONTEXT_STATE_INC_PENDANT_DISPOSAL);
          if($mbUserEqualApprover){
            $this->stateForward($poContext,WKF_ACTION_INC_APP_DISPOSAL);
          }else{
            $this->dispatchTask(ACT_INC_DISPOSAL_APPROVAL, $moContext->getId(), $miApproverId);
          }
          return;
        }
      }
      case CONTEXT_STATE_INC_PENDANT_DISPOSAL:{
        if($piAction==WKF_ACTION_INC_APP_DISPOSAL || $piAction == WKF_ACTION_APPROVE){
          $this->dispatchAlert(WKF_ALERT_INC_DISPOSAL_APP,$moContext,$psJustification);
          $moContext->setContextState(CONTEXT_STATE_INC_WAITING_SOLUTION);
          return;
        }elseif($piAction==WKF_ACTION_INC_DENY_DISPOSAL || $piAction == WKF_ACTION_DENY){
          $this->dispatchAlert(WKF_ALERT_INC_DISPOSAL_DENIED,$moContext,$psJustification);
          $moContext->setContextState(CONTEXT_STATE_INC_DIRECTED);
          return;
        }
      }
      case CONTEXT_STATE_INC_WAITING_SOLUTION:{
        if($piAction==WKF_ACTION_INC_SEND_TO_APP_SOLUTION){
          $moContext->setContextState(CONTEXT_STATE_INC_PENDANT_SOLUTION);
          if($mbUserEqualApprover){
            $this->stateForward($poContext,WKF_ACTION_INC_APP_SOLUTION);
          }else{
            $this->dispatchTask(ACT_INC_SOLUTION_APPROVAL, $moContext->getId(), $miApproverId);
          }
          return;
        }
      }
      case CONTEXT_STATE_INC_PENDANT_SOLUTION:{
        if($piAction==WKF_ACTION_INC_APP_SOLUTION || $piAction == WKF_ACTION_APPROVE){
          $this->dispatchAlert(WKF_ALERT_INC_SOLUTION_APP,$moContext,$psJustification);
          $moContext->setContextState(CONTEXT_STATE_INC_SOLVED);
          return;
        }elseif($piAction==WKF_ACTION_INC_DENY_SOLUTION || $piAction == WKF_ACTION_DENY){
          $this->dispatchAlert(WKF_ALERT_INC_SOLUTION_DENIED,$moContext,$psJustification);
          $moContext->setContextState(CONTEXT_STATE_INC_WAITING_SOLUTION);
          return;
        }
      }
      case CONTEXT_STATE_DELETED:{
        if($piAction == WKF_ACTION_UNDELETE){
          $moContext->setContextState(CONTEXT_STATE_INC_OPEN);
        }
        return;
      }
    }
    trigger_error("The action '$piAction' at state '{$moContext->getContextState()}' is not implemented.",E_USER_ERROR);
  }

}
?>