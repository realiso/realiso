<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

define("ACTION_PLAN_CORRECTIVE_TYPE", 4780);
define("ACTION_PLAN_PREVENTIVE_TYPE", 4781);


class CIActionPlan extends ISMSContext implements IWorkflow {

 /**
  * Construtor.
  *
  * <p>Construtor da classe CIActionPlan.</p>
  * @access public
  */
  public function __construct(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    parent::__construct('ci_action_plan');

    $this->ciContextType = CONTEXT_CI_ACTION_PLAN;

    $this->csAliasId = 'ap_id';
    $this->coDataset->addFWDDBField(new FWDDBField('fkcontext'              ,'ap_id'                    ,DB_NUMBER  ));
    $this->coDataset->addFWDDBField(new FWDDBField('fkresponsible'          ,'ap_responsible_id'        ,DB_NUMBER  ));
    $this->coDataset->addFWDDBField(new FWDDBField('sname'                  ,'ap_name'                  ,DB_STRING  ));
    $this->coDataset->addFWDDBField(new FWDDBField('tactionplan'            ,'ap_actionplan'            ,DB_STRING  ));
    $this->coDataset->addFWDDBField(new FWDDBField('nactiontype'            ,'ap_actiontype'            ,DB_NUMBER  ));
    $this->coDataset->addFWDDBField(new FWDDBField('ddatedeadline'          ,'ap_datedeadline'          ,DB_DATETIME));
    $this->coDataset->addFWDDBField(new FWDDBField('ddateconclusion'        ,'ap_dateconclusion'        ,DB_DATETIME));
    $this->coDataset->addFWDDBField(new FWDDBField('bisefficient'           ,'ap_isefficient'           ,DB_NUMBER  ));
    $this->coDataset->addFWDDBField(new FWDDBField('ddateefficiencyrevision','ap_dateefficiencyrevision',DB_DATETIME));
    $this->coDataset->addFWDDBField(new FWDDBField('ddateefficiencymeasured','ap_dateefficiencymeasured',DB_DATETIME));
    $this->coDataset->addFWDDBField(new FWDDBField('ndaysbefore'            ,'ap_daysbefore'            ,DB_NUMBER  ));
    $this->coDataset->addFWDDBField(new FWDDBField('bflagrevisionalert'     ,'ap_flagrevisionalert'     ,DB_NUMBER  ));
    $this->coDataset->addFWDDBField(new FWDDBField('bflagrevisionalertlate' ,'ap_flagrevisionalertlate' ,DB_NUMBER  ));
    $this->coDataset->addFWDDBField(new FWDDBField('sDocument'              ,'ap_document'              ,DB_STRING  ));
    $this->coDataset->addFWDDBField(new FWDDBField('tRevisionJustification' ,'ap_revisionjustification' ,DB_STRING  ));
    
    $this->caSearchableFields = array('ap_name', 'ap_actionplan','ap_document');

    $this->coWorkflow = new ActionPlanWorkflow();
  }

 /**
  * Retorna o label do contexto.
  *
  * <p>M�todo para retornar o label do contexto.</p>
  * @access public
  * @return string Label do contexto
  */
  public function getLabel(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

    return FWDLanguage::getPHPStringValue('mx_action_plan','Plano de A��o');
  }

 /**
  * Retorna o nome do(a) plano de a��o.
  *
  * <p>Retorna o nome do(a) plano de a��o.</p>
  * @access public
  * @return string Nome do(a) plano de a��o
  */
  public function getName(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

    return $this->getFieldValue('ap_name');
  }

 /**
  * Retorna a descri��o do(a) plano de a��o.
  *
  * <p>Retorna a descri��o do(a) plano de a��o.</p>
  * @access public
  * @return string Descri��o do(a) plano de a��o
  */
  public function getDescription(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

    return $this->getFieldValue('ap_actionplan');
  }

 /**
  * Retorna o id do usu�rio respons�vel pelo(a) plano de a��o.
  *
  * <p>Retorna o id do usu�rio respons�vel pelo(a) plano de a��o.</p>
  * @access public
  * @return integer Id do respons�vel
  */
  public function getResponsible(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return $this->getFieldValue('ap_responsible_id');
  }

 /**
  * Retorna o id do usu�rio respons�vel pelo(a) plano de a��o.
  *
  * <p>Retorna o id do usu�rio respons�vel pelo(a) plano de a��o.</p>
  * @access public
  * @return integer Id do respons�vel
  */
  public function getApprover(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return $this->getFieldValue('ap_responsible_id');
  }

 /**
  * Retorna o id do usu�rio criador do plano de a��o.
  *
  * <p>Retorna o id do usu�rio criador do plano de a��o.</p>
  * @access public
  * @return integer Id do criador
  */
  public function getCreator(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

    $moCtx = new ISMSContextDate();
    $moCtx->createFilter($this->getFieldValue('ap_id'),'context_id');
    $moCtx->select();
    $miCreator = 0;
    while($moCtx->fetch()){
      if($moCtx->getFieldValue('context_id')==ACTION_CREATE){
        $miCreator = $moCtx->getFieldValue('user_id');
      }
    }
    return $miCreator;
  }

 /**
  * Verifica se o usu�rio pode inserir um(a) plano de a��o.
  *
  * <p>M�todo para verificar se o usu�rio pode inserir um(a) plano de a��o.</p>
  * @access public
  * @return boolean True, sse pode inserir
  */
  public function userCanInsert(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
    if(!in_array('M.CI.7.2',$maACLs)){
      return true;
    }else{
      return false;
    }
  }

 /**
  * Verifica se o usu�rio pode editar o(a) plano de a��o.
  *
  * <p>M�todo para verificar se o usu�rio pode editar o(a) plano de a��o.</p>
  * @access public
  * @param integer $piContextId Id do(a) plano de a��o
  * @return boolean True, sse pode editar
  */
  public function userCanEdit($piContextId,$piUserResponsible = 0){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    if($this->getResponsible() == ISMSLib::getCurrentUserId())
      return true;
    else{
      if(ISMSLib::userHasACL('M.CI.7.3')){
        return true;
      }else{
        return false;
      }
    }
  }

 /**
  * Verifica se o usu�rio pode deletar o(a) plano de a��o.
  *
  * <p>M�todo para verificar se o usu�rio pode deletar o(a) plano de a��o.</p>
  * @access public
  * @param integer $piContextId Id do(a) plano de a��o
  * @return boolean True, sse pode deletar
  */
  public function userCanDelete($piContextId){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    if($this->getResponsible() == ISMSLib::getCurrentUserId()){
      return true;
    }else{
      $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
      if(!in_array('M.CI.7.4',$maACLs)){
        return true;
      }else{
        return false;
      }
    }
  }

 /**
  * Indica se o contexto � delet�vel ou n�o.
  *
  * <p>M�todo que indica se o contexto � delet�vel ou n�o. Deve ser sobrecarregada
  * por contextos para os quais for necess�ria.</p>
  * @access public
  * @param integer $piContextId id do contexto
  */
  public function isDeletable($piContextId) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $moNcAc = new CINcActionPlan();
    $moNcAc->createFilter($this->getFieldValue('ap_id'),'ap_id');
    $moNcAc->select();
    //verifica se existe pelo menos uma associa��o deste AP com alguma melhor pr�tica
    if($moNcAc->fetch())
      return false;
    else
      return true;
  }

  /**
  * Exibe uma popup caso n�o seja poss�vel remover um contexto.
  *
  * <p>M�todo que exibe uma popup caso n�o seja poss�vel remover um contexto.</p>
  * @access public
  * @param integer $piContextId id do contexto
  */
  public function showDeleteError() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

    $msTitle = FWDLanguage::getPHPStringValue('mx_ap_remove_error_title','Erro ao remover Plano de A��o');
    $msMessage = FWDLanguage::getPHPStringValue('mx_ap_remove_error_message',"N�o foi poss�vel remover o Plano de A��o, pois ele est� associado a pelo menos uma N�o Conformidade.");
    ISMSLib::openOk($msTitle,$msMessage,"",60);
  }

 /**
  * Retorna o c�digo JavaScript para abrir a popup de edi��o de plano de a��o.
  *
  * <p>Retorna o c�digo JavaScript para abrir a popup de edi��o de plano de a��o.</p>
  * @param integer $piContextId Id do(a) plano de a��o
  * @param string $psUniqId Id da popup de visualiza��o
  * @access public
  * @return string C�digo JavaScript para abrir a popup de edi��o
  */
  public function getVisualizeEditEvent($piContextId,$psUniqId){
    $soSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    $msPackagePath = (($soSession->getMode()==MANAGER_MODE) ? 'packages/improvement/' : '../../packages/improvement/');
    $msPopupId = 'popup_action_plan_edit';

    return "isms_open_popup('{$msPopupId}','{$msPackagePath}{$msPopupId}.php?action_plan={$piContextId}','','true');"
           ."soPopUpManager.getPopUpById('{$msPopupId}').setCloseEvent(function(){soPopUpManager.closePopUp('popup_visualize_{$psUniqId}');});";
  }

 /**
  * Retorna o c�digo HTML do �cone do contexto.
  *
  * <p>M�todo para retornar o c�digo HTML do contexto</p>
  * @access public
  * @param integer $piId Id do contexto
  * @param integer $pbIsFetched True sse o contexto esta "fetchado"
  * @param integer $piTop Top do �cone
  * @return string C�digo HTML do �cone do contexto
  */
  public function getIconCode($piId, $pbIsFetched = false, $piTop = 7){
    return ISMSLib::getIconCode('icon-ci_action_plan.gif', -2, $piTop);
  }

  /**
  * Retorna o �cone do contexto.
  *
  * <p>M�todo para retornar o �cone do contexto.</p>
  * @access public
  * @param integer $piContextId Id do contexto
  * @return string Nome do �cone
  */
  public function getIcon($piContextId = 0){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return 'icon-ci_action_plan.gif';
  }
  
 /**
  * Retorna o caminho do contexto.
  *
  * <p>M�todo para retornar o caminho do contexto.</p>
  * @access public
  * @return string Caminho do contexto
  */
  public function getSystemPath(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

    $miModuleId = INCIDENT_MODE;
    $msModuleName = FWDLanguage::getPHPStringValue('st_incident_module_name','Melhoria Cont�nua');
    $miId = $this->getFieldValue($this->getAliasId());
    $msName = $this->getName();
    $msModulePath = "<a href='javascript:isms_redirect_to_mode($miModuleId,0,0)'>$msModuleName</a>";
    $msContextPath = $this->getIconCode($miId,true,-4)
                    ."<a href='javascript:isms_redirect_to_mode($miModuleId,".CONTEXT_CI_ACTION_PLAN.",$miId)'>"
                    .$msName
                    ."</a>";
    return $msModulePath .'&nbsp;'.ISMSLib::getIconCode('icon-arrow-right.gif',-2,0).'&nbsp;' . $msContextPath;
  }

  /**
   * Retorna a pr�xima a��o para mudan�a de estado.
   *
   * <p>Retorna a pr�xima a��o para mudan�a de estado.</p>
   *
   * @access public
   * @param integer $piAPId Id do Plano de A��o; Se n�o for passado nenhum, assume o atual.
   * @return integer Id do Plano de A��o para mudan�a de estado ou 0 se n�o houver a��o dispon�vel.
   */
  public function getStateForwardAction($piAPId='') {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

    if ($piAPId) {
      $moAP = new CIActionPlan();
      $moAP->fetchById($piAPId);
    } else {
      $moAP = $this;
      $piAPId = $moAP->getFieldValue('ap_id');
    }

    $maAction = array();
    $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();

    $miResponsibleId = $moAP->getFieldValue('ap_responsible_id');
    $msDateDeadline = $moAP->getFieldValue('ap_datedeadline');
    $msDateConclusion = $moAP->getFieldValue('ap_dateconclusion');

    $mbUserEqualResponsible = ($miUserId == $miResponsibleId);
    $mbUserEqualCreator = ($miUserId == $moAP->getCreator());

    $mbUserIsManager = $mbUserEqualResponsible;
    $mbUserEqualGestor = ISMSLib::getGestorPermission();

    switch($moAP->getContextState()){
      case CONTEXT_STATE_NONE:{ //Se n�o possui estado, � porque acabou de ser criado.
        if($mbUserEqualResponsible){ //Se o usuario que inseriu o contexto for o respons�vel, aprova automaticamente passando para o estado de aguardando solu��o.
          $maAction['direct'] = WKF_ACTION_AP_SEND_TO_WAINTING_CONCLUSION;
        }else{ //Se o usu�rio n�o for o RESPONS�VEL, gera tarefa de aprova��o para o respos�vel e estado � Pendente.
          $maAction['direct'] = WKF_ACTION_APPROVE;
        }
        break;
      }
      case CONTEXT_STATE_AP_PENDANT:{ //Estado Pendente
        if($mbUserEqualResponsible){
          $maAction['direct'] = WKF_ACTION_AP_SEND_TO_WAINTING_CONCLUSION;
        }
        break;
      }
      case CONTEXT_STATE_AP_WAITING_CONCLUSION:{ //Estado Aguardando Conclus�o
        if($mbUserEqualResponsible || $mbUserEqualGestor){
          if($moAP->getFieldValue('ap_dateconclusion')){
            $maAction['direct'] = WKF_ACTION_AP_SEND_TO_WAINTING_MEASUREMENT;
          }
        }
        break;
      }
      case CONTEXT_STATE_AP_WAITING_MEASUREMENT:{ //esperando medi��o
        if($mbUserEqualResponsible){
          if($moAP->getFieldValue('ap_dateefficiencymeasured')){
             $maAction['direct'] = WKF_ACTION_AP_SEND_TO_WAINTING_MEASUREMENT;
          }
        }
        break;
      }
    }

    return $maAction;
  }

  /**
   * Exibe tela de confirma��o da mudan�a de estado, ou tela de finaliza��o do plano de a��o.
   *
   * <p>Exibe tela de confirma��o da mudan�a de estado, ou tela de finaliza��o do plano de a��o.</p>
   *
   * @access public
   * @param integer $piAPId Id da N�o Conformidade; Se n�o for passado nenhum, assume o atual.
   * @return boolean True em caso de haver uma a��o. Falso se n�o houver possibilidade de altera��o de estado.
   */
  public function getStateForwardConfirmation($piAPId='') {
    if ($piAPId) {
      $moAP = new CIActionPlan();
      $moAP->fetchById($piAPId);
    } else {
      $moAP = $this;
      $piAPId = $moAP->getFieldValue('nc_id');
    }

    $maAction = $this->getStateForwardAction($piAPId);
    $miDirectAction = isset($maAction['direct'])?$maAction['direct']:0;

    if ($miDirectAction) {
      $moAP->stateForward($miDirectAction);
    }
  }
}

?>