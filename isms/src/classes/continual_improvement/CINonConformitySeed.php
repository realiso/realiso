<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

include $handlers_ref.'QueryControlProcesses.php';

define('NC_SEED_IMPLEMENTATION_IS_LATE', 1);
define('NC_SEED_EFFICIENCY_NOT_OK'     , 2);
define('NC_SEED_REVISION_IS_LATE'      , 4);
define('NC_SEED_TEST_NOT_OK'           , 8);
define('NC_SEED_TEST_IS_LATE'          ,16);

class CINonConformitySeed extends ISMSTable {

 /**
  * Construtor.
  *
  * <p>Construtor da classe CINonConformitySeed.</p>
  * @access public
  */
  public function __construct(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    parent::__construct('ci_nc_seed');
    $this->coDataset->addFWDDBField(new FWDDBField('fkControl'          ,'nc_seed_control_id'         ,DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('nDeactivationReason','nc_seed_deactivation_reason',DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('dDateSent'          ,'nc_seed_date_sent'          ,DB_NUMBER));
  }

  protected function getControlName(){
    $moControl = new RMControl();
    $moControl->fetchById($this->getFieldValue('nc_seed_control_id'));
    return $moControl->getName();
  }

  protected function getDeactivationReasonAsString(){
    switch($this->getFieldValue('nc_seed_deactivation_reason')){
      case NC_SEED_IMPLEMENTATION_IS_LATE:{
        return FWDLanguage::getPHPStringValue('mx_late_implementation', "Implementa��o Atrasada");
      }
      case NC_SEED_EFFICIENCY_NOT_OK:{
        return FWDLanguage::getPHPStringValue('mx_inefficient', "Ineficiente");
      }
      case NC_SEED_REVISION_IS_LATE:{
        return FWDLanguage::getPHPStringValue('mx_late_efficiency_revision', "Revis�o de Efici�ncia Atrasada");
      }
      case NC_SEED_TEST_NOT_OK:{
        return FWDLanguage::getPHPStringValue('mx_test_failure', "Falha no Teste");
      }
      case NC_SEED_TEST_IS_LATE:{
        return FWDLanguage::getPHPStringValue('mx_late_test', "Teste Atrasado");
      }
    }
  }

  protected function getDeactivationReasonDescription(){
    switch($this->getFieldValue('nc_seed_deactivation_reason')){
      case NC_SEED_IMPLEMENTATION_IS_LATE:{
        return FWDLanguage::getPHPStringValue('mx_late_implementation', "Implementa��o Atrasada");
      }
      case NC_SEED_EFFICIENCY_NOT_OK:{
        return FWDLanguage::getPHPStringValue('mx_inefficient', "Ineficiente");
      }
      case NC_SEED_REVISION_IS_LATE:{
        return FWDLanguage::getPHPStringValue('mx_late_efficiency_revision', "Revis�o de Efici�ncia Atrasada");
      }
      case NC_SEED_TEST_NOT_OK:{
        return FWDLanguage::getPHPStringValue('mx_test_failure', "Falha no Teste");
      }
      case NC_SEED_TEST_IS_LATE:{
        return FWDLanguage::getPHPStringValue('mx_late_test', "Teste Atrasado");
      }
    }
  }

  protected function getProcesses(){
    $moHandler = new QueryControlProcesses();
    $moHandler->setControlId($this->getFieldValue('nc_seed_control_id'));
    $moHandler->makeQuery();
    $moHandler->executeQuery();
    $maProcesses = array();
    while($moHandler->fetch()){
      $maProcesses[] = $moHandler->getFieldValue('process_id');
    }
    return $maProcesses;
  }

  public function createNonConformity(){
    $msName = $this->getControlName().' - '.$this->getDeactivationReasonAsString();
    $msDescription = $this->getDeactivationReasonDescription();
    $msDateSent = $this->getFieldValue('nc_seed_date_sent');
    $miControlId = $this->getFieldValue('nc_seed_control_id');

    $miChairmanId = ISMSLib::getConfigById(USER_CHAIRMAN);

    $moNonConformity = new CINonConformity();
    $moNonConformity->setFieldValue('nc_name',$msName,false);
    $moNonConformity->setFieldValue('nc_description',$msDescription,false);
    $moNonConformity->setFieldValue('nc_datesent',$msDateSent);
    $moNonConformity->setFieldValue('nc_sender_id',$miChairmanId);
    $moNonConformity->setFieldValue('nc_control_id',$miControlId);
    $moNonConformity->setFieldValue('nc_classification',NC_ACTION_CLASSIFICATION_SECURITY_CONTROL);
    //o segundo parametro � passado como true para indicar que � o sistema que est� executando
    //esse insert, assim o contexto n�o testar� a permiss�o de insert do usu�rio logado no sistema
    $miNonConformityId = $moNonConformity->insert(true,true);

    $maProcesses = $this->getProcesses();
    foreach($maProcesses as $miProcessId){
      $moNonConformityProcess = new CINonConformityProcess();
      $moNonConformityProcess->setFieldValue('nc_id',$miNonConformityId);
      $moNonConformityProcess->setFieldValue('process_id',$miProcessId);
      $moNonConformityProcess->insert();
    }

    $this->createFilter($miControlId,'nc_seed_control_id');
    $this->createFilter($this->getFieldValue('nc_seed_deactivation_reason'),'nc_seed_deactivation_reason');
    $this->delete();
  }

}

?>