<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */


/**
 * Classe CICategory
 *
 * <p>Classe que representa a tabela de categorias.</p>
 * @package ISMS
 * @subpackage classes
 */
class CICategory extends ISMSContext {
 /**
  * Construtor.
  *
  * <p>Construtor da classe CICategory.</p>
  * @access public
  */
  public function __construct() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

    parent::__construct("ci_category");
    $this->csAliasId = "category_id";
    $this->ciContextType = CONTEXT_CI_CATEGORY;

    $this->coDataset->addFWDDBField(new FWDDBField("fkContext",  "category_id",    DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("sName",      "category_name",  DB_STRING));

	$this->caSearchableFields = array('category_name');
  }

  public function getResponsible(){

  }

  /**
  * Retorna o label do contexto.
  *
  * <p>M�todo para retornar o label do contexto.</p>
  * @access public
  * @return string Label do contexto
  */
  public function getLabel(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

    return FWDLanguage::getPHPStringValue('mx_category', "Categoria");
  }

  /**
  * Retorna o nome do contexto.
  *
  * <p>M�todo para retornar o nome do contexto.</p>
  * @access public
  * @return string Nome do contexto
  */
  public function getName(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

    return $this->getFieldValue('category_name');
  }


  /**
   * Verifica se o usu�rio pode deletar a categoria.
   *
   * <p>M�todo para verificar se o usu�rio pode deletar a categoria.</p>
   * @access public
   * @param integer piOccurrenceId Id da categoria
   * @return boolean True|False
   */
  public function userCanDelete($piCategoryId){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

  	$maDeniedACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
  	if(in_array('M.L.6.3',$maDeniedACLs)){
  		return false;
  	}else{
  		return true;
  	}
  }

   /**
   * Verifica se o usu�rio pode editar a categoria.
   *
   * <p>M�todo para verificar se o usu�rio pode editar a categoria.</p>
   * @access public
   * @param integer piOccurrenceId Id do categoria
   * @return boolean True|False
   */
  public function userCanEdit($piCategoryId,$piUserResponsible = 0){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
	$maDeniedACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
	if(in_array('M.L.6.2 ',$maDeniedACLs)){
		return false;
	}else{
		return true;
	}
  }

  /**
   * Verifica se o usu�rio pode inserir uma ocorr�ncia.
   *
   * <p>M�todo para verificar se o usu�rio pode inserir uma ocorr�ncia.</p>
   * @access public
   * @return boolean True|False
   */
  public function userCanInsert(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
	$maDeniedACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
	if(in_array('M.L.6.1',$maDeniedACLs)){
		return false;
	}else{
		return true;
	}
  }

 /**
  * Retorna o caminho do contexto.
  *
  * <p>M�todo para retornar o caminho do contexto.</p>
  * @access public
  * @return string Caminho do contexto
  */
  public function getSystemPath() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $msRMPath = "<a href='javascript:isms_redirect_to_mode(" . LIBRARIES_MODE . ",0,0)'>".FWDLanguage::getPHPStringValue('st_libraries_module_name','Bibliotecas')."</a>";
    $msContextPath = ISMSLib::getIconCode('icon-ci_category.gif',-2,-4). "<a href='javascript:isms_redirect_to_mode(" . LIBRARIES_MODE . "," . CONTEXT_CI_CATEGORY . "," . $this->getFieldValue($this->getAliasId()) . ")'>" . $this->getName() . "</a>";
    return $msRMPath ."&nbsp;".ISMSLib::getIconCode('icon-arrow-right.gif',-2,0).'&nbsp;' . $msContextPath;
  }

  /**
  * Retorna o caminho do contexto para a cria��o do pathscroll.
  *
  * <p>M�todo para retornar o caminho do contexto para a cria��o do pathscroll.</p>
  * @access public
  * @param integer $piTab id da tab
  * @param integer $piContextType id do contexto
  * @param string $psContextFunc fun��o que deve ser utilizada para gerar o c�digo do link
  * @return array Array contendo o Caminho do contexto
  */
  public function getSystemPathScroll($piTab,$piContextType,$psContextFunc){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $msHome = FWDLanguage::getPHPStringValue('mx_root','Raiz');
    $maPath = array();
    if($psContextFunc){
      $maPath[]="<a href='javascript:{$psContextFunc}(0);'>";
    }else{
      $maPath[]="<a href='javascript:isms_redirect_to_mode_scroll(" . $piTab . ",".$piContextType.",0)'>";
    }
    $maPath[]=$msHome;
    $maPath[]=ISMSLib::getIconCode('',-1);
    $maPath[]="</a>";
    $miCategoryId = $this->getFieldValue('category_id');
    if($psContextFunc)
      $maPath[] = "<a href='javascript:{$psContextFunc}({$miCategoryId});'>";
    else
      $maPath[] = "<a href='javascript:isms_redirect_to_mode_scroll(" . $piTab . "," . $piContextType . ",$miCategoryId)'>";
    $maPath[]=$this->getName();
    $maPath[]=ISMSLib::getIconCode('icon-ci_category.gif',-2);
    $maPath[]="</a>";

    $miIContPath = count($maPath);
    for($miI=$miIContPath-1;$miI>0;$miI--){
      if((isset($maPath[$miI-7]))&&($maPath[$miI-7]!=''))
        $maPath[$miI-3] = $maPath[$miI-7];
      $miI=$miI-3;
    }
    return $maPath;
  }

/**
  * Retorna o c�digo HTML do icone do contexto.
  *
  * <p>M�todo para retornar o c�digo HTML do contexto</p>
  * @access public
  * @param integer $piId Id do contexto
  * @param integer $pbIsFetched id do contexto
  * @return string contendo o c�digo HTML do icone do contexto
  */
 public function getIconCode($piId, $pbIsFetched = false){
   return ISMSLib::getIconCode('icon-ci_category.gif', -2, 7);
 }

  /**
  * Retorna o �cone do contexto.
  *
  * <p>M�todo para retornar o �cone do contexto.</p>
  * @access public
  * @param integer $piContextId Id do contexto
  * @return string Nome do �cone
  */
  public function getIcon($piContextId = 0){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return 'icon-ci_category.gif';
  }
  
 /**
  * Retorna o caminho para abrir a popup de edi��o desse contexto.
  *
  * <p>M�todo para retornar o caminho para abrir a popup de edi��o desse contexto.</p>
  * @param integer $piContextId id do contexto
  * @access public
  * @return string caminho para abrir a popup de edi��o desse contexto
  */
  public function getVisualizeEditEvent($piContextId,$psUniqId){
    $soSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    $msPackagesIncidentPath = ($soSession->getMode()==MANAGER_MODE) ? "packages/libraries/" : "../../packages/libraries/";
    $msPopupId = "popup_incident_category_edit";

    return "isms_open_popup('{$msPopupId}','{$msPackagesIncidentPath}{$msPopupId}.php?category=$piContextId','','true',107,420);"
           ."soPopUpManager.getPopUpById('{$msPopupId}').setCloseEvent( function close_visualize_$psUniqId() {soPopUpManager.closePopUp('popup_visualize_$psUniqId');} );";
  }

  /**
  * Deleta logicamente um contexto.
  *
  * <p>M�todo para deletar logicamente um contexto.</p>
  * @access public
  * @param integer $piContextId id do contexto
  */
  public function logicalDelete($piContextId) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

    // remove logicamente todas as solu��es desta categoria
    $maSolutions = $this->getSolutions($piContextId);
    $moSolution = new CISolution();
    foreach ($maSolutions as $miSolutionId) {
      $moSolution->delete($miSolutionId);
    }
    unset($moSolution);
  }

  /**
  * Retorna as solu��es de uma categoria.
  *
  * <p>M�todo para retornar as solu��es de uma categoria.</p>
  * @access public
  * @param integer $piAreaId Id da �rea
  * @return array Array de ids das sub-�reas
  */
  public function getSolutions($piCategoryId) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $maSolutions = array();
    $moSolution = new CISolution();
    $moSolution->createFilter($piCategoryId,'category_id');
    $moSolution->select();
    while($moSolution->fetch()) {
      $maSolutions[] = $moSolution->getFieldValue('solution_id');
    }
    return $maSolutions;
  }

}
?>