<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
 
/**
 * Classe CIIncidentProcess
 *
 * <p>Classe que representa a tabela de rela��o entre incidente e processo.</p>
 * @package ISMS
 * @subpackage classes
 */
class CIIncidentProcess extends ISMSContext {
 /**
  * Construtor.
  * 
  * <p>Construtor da classe CIIncidentProcess.</p>
  * @access public 
  */
  public function __construct() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    parent::__construct("ci_incident_process");
    $this->csAliasId = "context_id";
    $this->ciContextType = CONTEXT_CI_INCIDENT_PROCESS;
    $this->coDataset->addFWDDBField(new FWDDBField("fkContext"   ,"context_id"  ,DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("fkIncident"  ,"incident_id" ,DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("fkProcess"   ,"process_id"  ,DB_NUMBER));
  }
  
    /**
  * Retorna o label do contexto.
  *
  * <p>M�todo para retornar o label do contexto.</p>
  * @access public
  * @return string Label do contexto
  */
  public function getLabel(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return FWDLanguage::getPHPStringValue('mx_incident_process_association', "Associa��o de Incidente a Processo");
  }
  
/**
  * Retorna o nome do contexto.
  * 
  * <p>M�todo para retornar o nome do contexto.</p>
  * @access public 
  * @return string Nome do contexto
  */
  public function getName() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $moProcess = new RMProcess();
    $moProcess->fetchById($this->getFieldValue('process_id'));
    $moIncident = new CIIncident();
    $moIncident->fetchById($this->getFieldValue('incident_id'));
    return $moIncident->getName() . ' -> ' . $moProcess->getName();
  }  
  /**
   * Verifica se o usu�rio pode deletar o incidente.
   * 
   * <p>M�todo para verificar se o usu�rio pode deletar o incidente.</p>
   * @access public
   * @param integer piOccurrenceId Id do incidente.
   * @return boolean True|False
   */
  protected function userCanDelete($piOccurrenceId){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    //?????????????????????????????????????
    return true;
  }
  
   /**
   * Verifica se o usu�rio pode editar o incidente.
   * 
   * <p>M�todo para verificar se o usu�rio pode editar o incidente.</p>
   * @access public
   * @param integer piOccurrenceId Id do incidente.
   * @return boolean True|False
   */
  protected function userCanEdit($piOccurrenceId,$piUserResponsible = 0){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    //????????????????????????????????
    return true;
  }
  
  /**
   * Verifica se o usu�rio pode inserir o incidente.
   * 
   * <p>M�todo para verificar se o usu�rio pode inserir o incidente.</p>
   * @access public
   * @return boolean True|False
   */
  protected function userCanInsert(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    //?????????????????????????
    return true;
  }

}
?>