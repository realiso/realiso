<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe CIIncident
 *
 * <p>Classe que representa um incidente</p>
 * @package ISMS
 * @subpackage classes
 */

define("INCIDENT_LOSS_TYPE_DIRECT", 4580);
define("INCIDENT_LOSS_TYPE_INDIRECT", 4581);

class CIIncident extends ISMSContext implements IWorkflow {

	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe CIIncident.</p>
	 * @access public
	 */
	public function __construct() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		parent::__construct("ci_incident");

		$this->csAliasId = "incident_id";
		$this->ciContextType = CONTEXT_CI_INCIDENT;

		$this->coDataset->addFWDDBField(new FWDDBField("fkContext",                    "incident_id",                            DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkCategory",                   "incident_category_id",                   DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkResponsible",                "incident_responsible_id",                DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("sName",                        "incident_name",                          DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("tAccountsPlan",                "incident_accounts_plan",                 DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("tEvidences",                   "incident_evidences",                     DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("nLossType",                    "incident_loss_type",                     DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("tEvidenceRequirementComment",  "incident_evidence_req_comment",          DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("dDateLimit",                   "incident_date_limit",                    DB_DATETIME));
		$this->coDataset->addFWDDBField(new FWDDBField("dDateFinish",                  "incident_date_finish",                   DB_DATETIME));
		$this->coDataset->addFWDDBField(new FWDDBField("tDisposalDescription",         "incident_disposal_description",          DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("tSolutionDescription",         "incident_solution_description",          DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("tProductService",              "incident_product_service",               DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("bNotEmailDP",                  "incident_not_email_dp",                  DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("bEmailDPSent",                 "incident_email_dp_sent",                 DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("dDate",                  			 "incident_date",                   			 DB_DATETIME));

		$this->caSearchableFields = array('incident_name', 'incident_evidences', 'incident_evidence_req_comment','incident_disposal_description','incident_solution_description','incident_product_service');

		$this->coWorkflow = new IncidentWorkflow();
	}


	/**
	 * Retorna o label do contexto.
	 *
	 * <p>M�todo para retornar o label do contexto.</p>
	 * @access public
	 * @return string Label do contexto
	 */
	public function getLabel(){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		return FWDLanguage::getPHPStringValue('mx_incident', "Incidente");
	}

	/**
	 * Retorna o nome do contexto.
	 *
	 * <p>M�todo para retornar o nome do contexto.</p>
	 * @access public
	 * @return string Nome do contexto
	 */
	public function getName(){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		return $this->getFieldValue('incident_name');
	}

	
	/**
	 * Deleta um contexto.
	 *
	 * <p></p>
	 * @access public
	 * @param integer $piContextId Id do contexto
	 * @param boolean $pbDeleteDB Remove do banco
	 * @param boolean $pbExecByTrash Delete chamado pela Lixeira do sistema
	 */
	public function delete($piContextId, $pbDeleteDB = false,$pbExecByTrash = false,$pbExecBySystem = false){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		if($pbExecByTrash){
    		$moIncidentRiskParameter = new CIIncidentRiskParameter();
    		$moIncidentRiskParameter->createFilter($piContextId, 'incident_id');
    		$moIncidentRiskParameter->select();
    		while($moIncidentRiskParameter->fetch()){
    			$moIncidentDel = new CIIncidentRiskParameter();
    			$moIncidentDel->delete($moIncidentRiskParameter->getFieldValue('incident_risk_parameter_id'), true, false, false);
    		}          
              
    	}		
		
		parent::delete($piContextId, $pbDeleteDB,$pbExecByTrash, $pbExecBySystem);
	}	
	
	
	/**
	 * Deleta logicamente um contexto.
	 *
	 * <p>M�todo para deletar logicamente um contexto.</p>
	 * @access public
	 * @param integer $piContextId id do contexto
	 */
	public function logicalDelete($piContextId) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		$moOccurrence = new CIOccurrence();
		$moOccurrence->createFilter($piContextId, 'incident_id');
		$moOccurrence->select();
		while($moOccurrence->fetch()){
			$moOccurrenceDel = new CIOccurrence();
			//1 false = deleteBD, 2 false = execByTrash, 3 true -> execBySystem
			$moOccurrenceDel->delete($moOccurrence->getFieldValue('occurrence_id'),false,false,true);
		}

		$moIncidentRiskParameter = new CIIncidentRiskParameter();
		$moIncidentRiskParameter->createFilter($piContextId, 'incident_id');
		$moIncidentRiskParameter->select();
		while($moIncidentRiskParameter->fetch()){
			$moIncidentDel = new CIIncidentRiskParameter();
			$moIncidentDel->delete($moIncidentRiskParameter->getFieldValue('incident_risk_parameter_id'), false, false, true);
		}

		$maAlertAndTaskIds = array();
		//pega os ids para excluir os alertas e tasks referentes a incident_risk
		$moCtx = new CIIncidentRisk();
		$moCtx->createFilter($piContextId,'incident_id');
		$moCtx->select();
		while($moCtx->fetch()){
			$maAlertAndTaskIds[] = $moCtx->getFieldValue('incident_risk_id');
		}
		//pega os ids para excluir os alertas e tasks referentes a incident_process
		$moCtx = new CIIncidentProcess();
		$moCtx->createFilter($piContextId,'incident_id');
		$moCtx->select();
		while($moCtx->fetch()){
			$maAlertAndTaskIds[] = $moCtx->getFieldValue('context_id');
		}
		//pega os ids para excluir os alertas e tasks referentes a incident_control
		$moCtx = new CIIncidentControl();
		$moCtx->createFilter($piContextId,'incident_id');
		$moCtx->select();
		while($moCtx->fetch()){
			$maAlertAndTaskIds[] = $moCtx->getFieldValue('incident_control_id');
		}

		//deleta todas as tasks e alertas referentes ao incidente sendo enviado para a lixeira
		foreach($maAlertAndTaskIds as $miValue){
			// deletar tarefas do contexto deletado
			$moTasks = new WKFTask();
			$moTasks->createFilter($miValue, 'task_context_id');
			$moTasks->delete();
			// deletar alertas do contexto deletado
			$moAlerts = new WKFAlert();
			$moAlerts->createFilter($miValue, 'alert_context_id');
			$moAlerts->delete();
		}
	}



	/**
	 * Verifica se o usu�rio pode deletar o incidente.
	 *
	 * <p>M�todo para verificar se o usu�rio pode deletar o incidente.</p>
	 * @access public
	 * @param integer piIncidentId Id do incidente.
	 * @return boolean True|False
	 */
	public function userCanDelete($piIncidentId){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		$maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
		$miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
		if((ISMSLib::getConfigById(USER_INCIDENT_MANAGER) == $miUserId)||(!in_array('M.CI.2.4',$maACLs))){
			return true;
		}else{
			return false;
		}
	}

	/**
	 * Verifica se o usu�rio pode editar o incidente.
	 *
	 * <p>M�todo para verificar se o usu�rio pode editar o incidente.</p>
	 * @access public
	 * @param integer piIncidentId Id do incidente.
	 * @return boolean True|False
	 */
	public function userCanEdit($piIncidentId,$piUserResponsible = 0){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		$maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
		$miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
		$moIncident = new CIIncident();
		$moIncident->fetchById($piIncidentId);
		$miState = $moIncident->getContextState();
		if ( $miState == CONTEXT_STATE_INC_SOLVED){
			return false;
		}
		if((ISMSLib::getConfigById(USER_INCIDENT_MANAGER) == $miUserId)||(!in_array('M.CI.2.3',$maACLs))){
			return true;
		}else{
			if($miUserId == $moIncident->getFieldValue('incident_responsible_id')){
				if($miState==CONTEXT_STATE_INC_DIRECTED || $miState==CONTEXT_STATE_INC_WAITING_SOLUTION){
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Verifica se o usu�rio pode inserir o incidente.
	 *
	 * <p>M�todo para verificar se o usu�rio pode inserir o incidente.</p>
	 * @access public
	 * @return boolean True|False
	 */
	public function userCanInsert(){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		$miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
		$maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();

		return ((ISMSLib::getConfigById(USER_INCIDENT_MANAGER) == $miUserId)||(!in_array('M.CI.2.2',$maACLs)));
	}

	/**
	 * Retorna o id do usu�rio que deve aprovar o contexto.
	 *
	 * <p>M�todo para retornar id do usu�rio que deve aprovar o contexto.</p>
	 * @access public
	 * @return integer Id do aprovador
	 */
	public function getApprover(){
		return ISMSLib::getConfigById(USER_INCIDENT_MANAGER);
	}

	/**
	 * Retorna o id do usu�rio respons�vel pelo contexto.
	 *
	 * <p>M�todo para retornar id do usu�rio respons�vel pelo contexto.</p>
	 * @access public
	 * @return integer Id do respons�vel
	 */
	public function getResponsible(){
		return $this->getFieldValue('incident_responsible_id');
	}

	/**
	 * Retorna a pr�xima a��o para mudan�a de estado.
	 *
	 * <p>Retorna a pr�xima a��o para mudan�a de estado.</p>
	 *
	 * @access public
	 * @param integer $piIncidentId Id do Incidente; Se n�o for passado nenhum, assume o atual.
	 * @return integer Id da a��o para mudan�a de estado ou 0 se n�o houver a��o dispon�vel.
	 */
	public function getStateForwardAction($piIncidentId='') {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		if ($piIncidentId) {
			$moIncident = new CIIncident();
			$moIncident->fetchById($piIncidentId);
		} else $moIncident = $this;

		$maAction = array();

		$miUserId = ISMSLib::getCurrentUserId();
		$mbIsResponsible = ($miUserId == $moIncident->getResponsible());
		$mbIsApprover = ($miUserId == $moIncident->getApprover());

		$mbResponsibleIsSet = (bool)$moIncident->getFieldValue('incident_responsible_id');
		$mbDisposalIsSet = (bool)trim($moIncident->getFieldValue('incident_disposal_description'));
		$mbSolutionIsSet = (bool)trim($moIncident->getFieldValue('incident_solution_description'));
		$mbDateLimitIsSet = (bool)$moIncident->getFieldValue('incident_date_limit');
		$mbDateFinishIsSet = (bool)$moIncident->getFieldValue('incident_date_finish');

		switch ($moIncident->getContextState()){
			case CONTEXT_STATE_DENIED:{
				if($mbIsResponsible){
					$maAction['confirm'] = WKF_ACTION_SEND_TO_APPROVAL;
				}
				break;
			}
			case CONTEXT_STATE_PENDANT:{
				if($mbIsApprover){
					$maAction['confirm'] = WKF_ACTION_APPROVE;
				}
				break;
			}
			case CONTEXT_STATE_INC_OPEN:{
				if($mbIsResponsible){
					$maAction['direct'] = WKF_ACTION_INC_SEND_TO_RESPONSIBLE;
					if($mbDisposalIsSet && $mbDateLimitIsSet){
						$maAction['direct'] = WKF_ACTION_INC_SEND_TO_APP_DISPOSAL;
						if($mbSolutionIsSet && $mbDateFinishIsSet){
							$maAction['confirm'] = WKF_ACTION_INC_SEND_TO_APP_SOLUTION;
						}
					}
				}elseif($mbResponsibleIsSet && $mbDateLimitIsSet){
					$maAction['confirm'] = WKF_ACTION_INC_SEND_TO_RESPONSIBLE;
				}
				break;
			}
			case CONTEXT_STATE_INC_DIRECTED:{
				if($mbDisposalIsSet && $mbDateLimitIsSet){
					if($mbIsApprover){
						$maAction['direct'] = WKF_ACTION_INC_SEND_TO_APP_DISPOSAL;
						if($mbSolutionIsSet && $mbDateFinishIsSet){
							$maAction['confirm'] = WKF_ACTION_INC_SEND_TO_APP_SOLUTION;
						}
					}else{
						$maAction['confirm'] = WKF_ACTION_INC_SEND_TO_APP_DISPOSAL;
					}
				}
				break;
			}
			case CONTEXT_STATE_INC_WAITING_SOLUTION:{
				if($mbSolutionIsSet && $mbDateFinishIsSet){
					$maAction['confirm'] = WKF_ACTION_INC_SEND_TO_APP_SOLUTION;
				}
				break;
			}
		}
		return $maAction;
	}

	/**
	 * Exibe tela de confirma��o da mudan�a de estado do incidente.
	 *
	 * <p>Exibe tela de confirma��o da mudan�a de estado do incidente.</p>
	 *
	 * @access public
	 * @param integer $piIncidentId Id do Incidente; Se n�o for passado nenhum, assume o atual.
	 * @return boolean True em caso de haver uma a��o. Falso se n�o houver possibilidade de altera��o de estado.
	 */
	public function getStateForwardConfirmation($piIncidentId='') {
		if ($piIncidentId) {
			$moIncident = new CIIncident();
			$moIncident->fetchById($piIncidentId);
		} else {
			$moIncident = $this;
			$piIncidentId = $moIncident->getFieldValue('incident_id');
		}

		$maAction = $this->getStateForwardAction($piIncidentId);

		$miDirectAction = isset($maAction['direct'])?$maAction['direct']:0;
		$miConfirmAction = isset($maAction['confirm'])?$maAction['confirm']:0;
		if ($miDirectAction) {
			$moIncident->stateForward($miDirectAction);
		}

		if ($miConfirmAction) {
			if($miConfirmAction == WKF_ACTION_APPROVE){
				$moTask = new WKFTask();
				$moTask->createFilter($piIncidentId,'task_context_id');
				$moTask->select();
				$moTask->fetch();
				echo "isms_open_popup('popup_incident_approval_task', 'packages/improvement/popup_incident_approval_task.php?task=" . $moTask->getFieldValue('task_id') . "', '', 'true');";
				echo "soWindow = soPopUpManager.getPopUpById('popup_incident_approval_task');
              soWindow.setCloseEvent(
                function(){ 
                  if(soPopUpManager.getPopUpById('popup_incident_edit')){
                    soWindowNav = soPopUpManager.getPopUpById('popup_incident_edit').getOpener();
                    if(soWindowNav.refresh_grid) soWindowNav.refresh_grid();
                    soPopUpManager.closePopUp('popup_incident_edit');
                    soWindowNav = null;
                  }
                }
              );";
				return true;
			}else{
				echo "isms_open_popup('popup_incident_state_forward','packages/improvement/popup_incident_state_forward.php?incident={$piIncidentId}','','true',122,350);";
				echo "soWindow = soPopUpManager.getPopUpById('popup_incident_state_forward');
              soWindow.setCloseEvent(
                function(){ 
                  if(soPopUpManager.getPopUpById('popup_incident_edit')){
                    soWindowNav = soPopUpManager.getPopUpById('popup_incident_edit').getOpener();
                    if(soWindowNav.refresh_grid) soWindowNav.refresh_grid();
                    soPopUpManager.closePopUp('popup_incident_edit');
                    soWindowNav = null;
                  }else{
                    soWindowNav = soPopUpManager.getPopUpById('popup_incident_state_forward').getOpener();
                    if(soWindowNav.refresh_grid) soWindowNav.refresh_grid();
                    soWindowNav = null;
                  }
               }
              );";
				return true;
			}
		}else{
			return false;
		}
	}

	/**
	 * For�a a restaura��o das ocorr�nicas relacionadas ao incidente sendo restaurado da lixeira.
	 *
	 * <p>M�todo for�ar a restaura��o das ocorr�nicas relacionadas ao incidente sendo restaurado da lixeira.
	 * @access public
	 * @param integer $piContextId Identificador do contexto
	 */
	public function restore($piContextId) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		// necessita pegar os dados do objeto para criar o log

		$moOccurrence = new CIOccurrence();
		$moOccurrence->createFilter($piContextId, 'incident_id');
		$moOccurrence->select();
		while($moOccurrence->fetch()){
			$moContext = new ISMSContextObject();
			$moContext->fetchById($moOccurrence->getFieldValue('occurrence_id'));
			if($moContext->getFieldValue('context_state') == CONTEXT_STATE_DELETED){
				$moOccurrenceRestore = new CIOccurrence();
				$moOccurrenceRestore->restore($moOccurrence->getFieldValue('occurrence_id'));
			}
		}
		parent::restore($piContextId);
	}

	/**
	 * Retorna o caminho do contexto.
	 *
	 * <p>M�todo para retornar o caminho do contexto.</p>
	 * @access public
	 * @return string Caminho do contexto
	 */
	public function getSystemPath() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		$msRMPath = "<a href='javascript:isms_redirect_to_mode(" . INCIDENT_MODE . ",0,0)'> " . FWDLanguage::getPHPStringValue('st_incident_module_name','Melhoria Cont�nua') . "</a>";
		$msContextPath = ISMSLib::getIconCode('icon-ci_incident.gif',-2,-4). "<a href='javascript:isms_redirect_to_mode(" . INCIDENT_MODE . "," . CONTEXT_CI_INCIDENT . "," . $this->getFieldValue($this->getAliasId()) . ")'>" . $this->getName() . "</a>";
		return $msRMPath ."&nbsp;".ISMSLib::getIconCode('icon-arrow-right.gif',-2,0).'&nbsp;' . $msContextPath;
	}

	/**
	 * Retorna o c�digo HTML do icone do contexto.
	 *
	 * <p>M�todo para retornar o c�digo HTML do contexto</p>
	 * @access public
	 * @param integer $piId Id do contexto
	 * @param integer $pbIsFetched id do contexto
	 * @return string contendo o c�digo HTML do icone do contexto
	 */
	public function getIconCode($piId, $pbIsFetched = false){
		return ISMSLib::getIconCode('icon-ci_incident.gif', -2, 7);
	}

	/**
	 * Retorna o caminho para abrir a popup de edi��o desse contexto.
	 *
	 * <p>M�todo para retornar o caminho para abrir a popup de edi��o desse contexto.</p>
	 * @param integer $piContextId id do contexto
	 * @access public
	 * @return string caminho para abrir a popup de edi��o desse contexto
	 */
	public function getVisualizeEditEvent($piContextId,$psUniqId){
		$soSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
		$msPackagesIncidentPath = ($soSession->getMode()==MANAGER_MODE) ? "packages/improvement/" : "../../packages/improvement/";
		$msPopupId = "popup_incident_edit";

		return "isms_open_popup('{$msPopupId}','{$msPackagesIncidentPath}{$msPopupId}.php?incident=$piContextId','','true');"
		."soPopUpManager.getPopUpById('{$msPopupId}').setCloseEvent( function close_visualize_$psUniqId() {soPopUpManager.closePopUp('popup_visualize_$psUniqId');} );";
	}

	/**
	 * Retorna o �cone do contexto.
	 *
	 * <p>M�todo para retornar o �cone do contexto.</p>
	 * @access public
	 * @param integer $piContextId Id do contexto
	 * @return string Nome do �cone
	 */
	public function getIcon($piContextId = 0){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		return 'icon-ci_incident.gif';
	}

}

?>
