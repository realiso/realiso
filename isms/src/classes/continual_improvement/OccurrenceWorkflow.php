<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe OccurrenceWorkflow.
 *
 * <p>Classe que manipula os estados do workflow das ocorr�ncias.
 * O workflow � respons�vel por disparar as devidas tarefas.
 * � ele que sabe quais e quantas tarefas ser�o disparadas, de acordo
 * com o estado do contexto utilizado.</p>
 * @package ISMS
 * @subpackage classes
 */
class OccurrenceWorkflow extends Workflow {

 /**
  * Avan�a o estado do contexto no workflow.
  * 
  * <p>M�todo para avan�ar o estado do contexto no workflow.</p>
  * @access public 
  * 
  * @param IWorkflow $poDocument Objeto do documento
  * @param integer $piAction Tipo da a��o
  * @param string $psJustification Justificativa
  */
  public function stateForward(IWorkflow $poContext, $piAction, $psJustification = ''){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    $miApproverId = $poContext->getApprover();
    $miResponsibleId = $poContext->getResponsible();
    $mbUserEqualApprover = ($miUserId == $miApproverId);
    $mbUserEqualResponsible = ($miUserId == $miResponsibleId);
    $mbDeletedResponsible = ($miResponsibleId == 0);
    
    if($piAction==WKF_ACTION_DELETE){
      $poContext->setContextState(CONTEXT_STATE_DELETED);
      return;
    }
    
    switch($poContext->getContextState()){
      case CONTEXT_STATE_NONE:{ //Se n�o possui estado, � porque acabou de ser criado. 
        if($mbUserEqualApprover){ //Se o usuario que inseriu o contexto for o gestor, aprova automaticamente.
            $poContext->setContextState(CONTEXT_STATE_APPROVED);
        }else{ //Se o usu�rio n�o for o gestor, gera tarefa de aprova��o para o gestor e estado � Pendente.
          $this->dispatchTask($this->ciActivity, $poContext->getId(), $miApproverId);
          $poContext->setContextState(CONTEXT_STATE_PENDANT);
        }
        break;
      }
      case CONTEXT_STATE_PENDANT:{
        switch($piAction){
          case WKF_ACTION_APPROVE:{ //Quando aprovado, emite alerta para o respons�vel, se este n�o for o mesmo que est� aprovando.
            if(!$mbDeletedResponsible && !$mbUserEqualResponsible) 
              $this->dispatchAlert(WKF_ALERT_APPROVED, $poContext, $psJustification);
            $poContext->setContextState(CONTEXT_STATE_APPROVED); //Outros: estado Aprovado
            break;
          }
          case WKF_ACTION_DENY:{ //Ap�s negado, n�o h� como voltar para estado anterior.
            if(!$mbDeletedResponsible && !$mbUserEqualResponsible) 
              $this->dispatchAlert(WKF_ALERT_DENIED, $poContext, $psJustification);
            $poContext->setContextState(CONTEXT_STATE_DENIED);
            break;
          }
           if($piAction == WKF_ACTION_UNDELETE){
              // pode ocorrer esse estado caso o incidente e suas ocorr�ncias forem selecionados para serem restaurados
              //na lixeira e o incidente for o primeiro a ser restaudado da lixeira. O incidente j� restaura suas ocurr�ncias,
              //assim quando a lixeira for restaurar essa ocorrencia ela j� estar� no estado de aprovado ou pendente, dependendo
              //de quem restaurou a ocorr�ncia
              break;
           }
        }
        break;
      }
      case CONTEXT_STATE_DELETED:{
        if($piAction == WKF_ACTION_UNDELETE){
          if($mbUserEqualApprover){ //Se o usuario que restaurou o contexto for o gestor, aprova automaticamente.
              $poContext->setContextState(CONTEXT_STATE_APPROVED);
          }else{ //Se o usu�rio n�o for o gestor, gera tarefa de aprova��o para o gestor e estado � Pendente.
            $this->dispatchTask($this->ciActivity, $poContext->getId(), $miApproverId);
            $poContext->setContextState(CONTEXT_STATE_PENDANT);
          }
        }
        break;
      }
      case CONTEXT_STATE_APPROVED:{
        
         if($piAction == WKF_ACTION_EDIT){
           if(!$mbUserEqualApprover){
             $this->dispatchTask($this->ciActivity, $poContext->getId(), $miApproverId);
             $poContext->setContextState(CONTEXT_STATE_PENDANT);
           }
           
           break;
         }
        
         if($piAction == WKF_ACTION_UNDELETE){
            // pode ocorrer esse estado caso o incidente e suas ocorr�ncias forem selecionados para serem restaurados
            //na lixeira e o incidente for o primeiro a ser restaudado da lixeira. O incidente j� restaura suas ocurr�ncias,
            //assim quando a lixeira for restaurar essa ocorrencia ela j� estar� no estado de aprovado ou pendente, dependendo
            //de quem restaurou a ocorr�ncia
            break;
         }
      }
      default:{
        trigger_error("The action '$piAction' at state '{$poContext->getContextState()}' is not implemented.",E_USER_ERROR);  
      break;
      }
    }
  }
}
?>