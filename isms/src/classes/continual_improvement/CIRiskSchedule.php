<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

include_once $handlers_ref."QueryScheduledRisks.php";
include_once $handlers_ref."QueryTooManyIncidents.php";
 
/**
 * Classe CIRiskSchedule
 *
 * <p>Classe que representa a tabela de revis�o (schedule) de risco.</p>
 * @package ISMS
 * @subpackage classes
 */
class CIRiskSchedule extends ISMSTable {
 /**
  * Construtor.
  * 
  * <p>Construtor da classe CIRiskSchedule.</p>
  * @access public 
  */
  public function __construct() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    parent::__construct("ci_risk_schedule");
    
    $this->csAliasId = 'risk_id';
    
    $this->coDataset->addFWDDBField(new FWDDBField("fkRisk",          "risk_id",                   DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("nPeriod",         "schedule_period",           DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("nValue",          "schedule_value",            DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("dDateLastCheck",  "schedule_date_last_check",  DB_DATETIME));
  }
  
  public function checkSchedule(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    $miCreatorId = ISMSLib::getConfigById(USER_CHAIRMAN);
    $moRisksHandler = new QueryScheduledRisks(FWDWebLib::getConnection());

    if(!$miUserId)
    	return;    
    
    $moRisksHandler->makeQuery();
    $moRisksHandler->executeQuery();
    $miNow = ISMSLib::ISMSTime();
    
    while($moRisksHandler->fetch()){
      $miRiskId = $moRisksHandler->getFieldValue('risk_id');
      $miPeriodUnit = $moRisksHandler->getFieldValue('period_unit');
      $miPeriodSize = $moRisksHandler->getFieldValue('period_size');
      $miPeriodStart = ISMSLib::subtractPeriodFromTime($miPeriodSize,$miPeriodUnit,$miNow);
      
      $moTooManyIncidentsHandler = new QueryTooManyIncidents(FWDWebLib::getConnection());
      $moTooManyIncidentsHandler->setRiskId($miRiskId);
      $moTooManyIncidentsHandler->setInitialTime($miPeriodStart);
      $moTooManyIncidentsHandler->setFinalTime($miNow);
      $moTooManyIncidentsHandler->makeQuery();
      $moTooManyIncidentsHandler->executeQuery();
      $moTooManyIncidentsHandler->fetch();
      $moIncidentsCountCmp = $moTooManyIncidentsHandler->getFieldValue('incidents_count_cmp');
      if($moIncidentsCountCmp!=0){
        // Envia alerta
        $miAlertType = ($moIncidentsCountCmp>0?WKF_ALERT_PROBABILITY_UPDATE_UP:WKF_ALERT_PROBABILITY_UPDATE_DOWN);
        $moAlert = new WKFAlert();
        $moAlert->setFieldValue('alert_receiver_id',$miUserId);
        $moAlert->setFieldValue('alert_date',$miNow);
        $moAlert->setFieldValue('alert_type',$miAlertType);
        $moAlert->setFieldValue('alert_justification','');
        $moAlert->setFieldValue('alert_creator_id',$miCreatorId);
        $moAlert->setFieldValue('alert_context_id',$miRiskId);
        $moAlert->insert();
      }
      // Atualiza data da �ltima verifica��o
      $moRiskSchedule = new CIRiskSchedule();
      $moRiskSchedule->setFieldValue('schedule_date_last_check',$miNow);
      $moRiskSchedule->update($miRiskId);
    }
  }

}
?>