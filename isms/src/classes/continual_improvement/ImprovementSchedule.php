<?php
include_once $handlers_ref . "QueryActionPlanEfficiencyRevisionAlert.php";
include_once $handlers_ref . "QueryActionPlanEfficiencyRevisionExpired.php";

/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ImprovementSchedule.
 *
 * <p>Classe que manipula a revis�o (schedule) da gest�o de incidente.</p>
 * @package ISMS
 * @subpackage classes
 */
class ImprovementSchedule extends ISMSTable {

 private $ciUserId = 0;

 /**
  * Construtor.
  *
  * <p>Construtor da classe ImprovementSchedule.</p>
  * @access public
  */
  public function __construct($piUserId){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

    $this->ciUserId = $piUserId;
  }

  /**
  * Verifica os schedules, disparando alertas e tasks deles
  *
  * <p>Verifica os schedules, disparando alertas e tasks deles
  * @access public
  */
  public function checkSchedule(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $moConfig = new ISMSConfig();
    $moHandle = new QueryActionPlanEfficiencyRevisionAlert(FWDWebLib::getConnection());
    $moHandle->makeQuery();
    $moHandle->executeQuery();
    $moDataset = $moHandle->getDataset();
    while($moDataset->fetch()){
      $miTimestamp = ISMSLib::ISMSTime();
      $miAPId = $moDataset->getFieldByAlias('ap_id')->getValue();
      $miRevision = ISMSLib::getTimestamp($moDataset->getFieldByAlias('ap_efficiency_revision_date')->getValue());
      $miDaysBefore = intval($moDataset->getFieldByAlias('ap_days_before')->getValue());
      
      $miReceiverId = $moDataset->getFieldByAlias('ap_responsible_id')->getValue();
      if(($miRevision + 86400) <= ($miTimestamp + ($miDaysBefore*86400))){
        $moAP = new CIActionPlan();
        $moAP->setFieldValue('ap_flagrevisionalert',true);
        $moAP->update($miAPId);
        $this->createTask($miAPId,$moConfig->getConfig(USER_CHAIRMAN),$miReceiverId,ACT_ACTION_PLAN_REVISION);
        if (($miRevision + 86400) > $miTimestamp) {
          $this->createAlert($miAPId,$moConfig->getConfig(USER_CHAIRMAN),$miReceiverId,WKF_ALERT_AP_EFFICIENCY_REVISION_NEAR);
        }
      }
    }
    
    $moHandle = new QueryActionPlanEfficiencyRevisionExpired(FWDWebLib::getConnection());
    $moHandle->makeQuery();
    $moHandle->executeQuery();
    $moDataset = $moHandle->getDataset();
    while($moDataset->fetch()){
      $miTimestamp = ISMSLib::ISMSTime();
      $miAPId = $moDataset->getFieldByAlias('ap_id')->getValue();
      $miRevision = ISMSLib::getTimestamp($moDataset->getFieldByAlias('ap_efficiency_revision_date')->getValue());
      $miApproverId = $moDataset->getFieldByAlias('ap_responsible')->getValue();
      if(($miRevision + 86400) < $miTimestamp ){
        $moAP = new CIActionPlan();
        $moAP->setFieldValue('ap_flagrevisionalertlate',true);
        $moAP->update($miAPId);
        $this->createAlert($miAPId,$moConfig->getConfig(USER_CHAIRMAN),$miApproverId,WKF_ALERT_AP_EFFICIENCY_REVISION_LATE);
      }
    }
    
    return true;
  }

  /**
  * Cria alertas.
  *
  * <p>M�todo para criar alertas.</p>
  * @access private
  * @param Integer $piContextId Identificador do contexto
  * @param Integer $piCreatorId Identificador do criador do alerta
  * @param Integer $piReceiverId Identificador do receptor do alerta
  * @param Integer $piType Tipo de alerta
  */
  private function createAlert($piContextId,$piCreatorId,$piReceiverId,$piType) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

    $moAlert = new WKFAlert();
    $moAlert->setFieldValue('alert_receiver_id',$piReceiverId);
    $moAlert->setFieldValue('alert_date',ISMSLib::ISMSTime());
    $moAlert->setFieldValue('alert_type',$piType);
    $moAlert->setFieldValue('alert_justification',' ');
    $moAlert->setFieldValue('alert_creator_id',$piCreatorId);
    $moAlert->setFieldValue('alert_context_id',$piContextId);
    $moAlert->insert();
  }


  /**
  * Cria tarefas.
  *
  * <p>M�todo para criar tarefas.</p>
  * @param Integer $piDocumentId Identificador do documento
  * @param Integer $piCreatorId Identificador do criador do alerta
  * @param Integer $piReceiverId Identificador do receptor do alerta
  * @param Integer $piType Tipo de tarefa
  * @access private
  */
  private function createTask($piContextId,$piCreatorId,$piReceiverId,$piType){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

    $moTask = new WKFTask();
    $moConfig = new ISMSConfig();
    $moTask->setFieldValue('task_activity',$piType);
    $moTask->setFieldValue('task_context_id',$piContextId);
    $moTask->setFieldValue('task_creator_id',$piCreatorId);
    $moTask->setFieldValue('task_receiver_id',$piReceiverId);
    $moTask->setFieldValue('task_date_created',ISMSLib::ISMSTime());
    $moTask->setFieldValue('task_is_visible',true);
    $moTask->insert();
  }
}
?>