<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */


/**
 * Classe CINonConformity
 *
 * <p>Classe que representa a tabela de n�o-conformidades.</p>
 * @package ISMS
 * @subpackage classes
 */
/*indica n�o potencial*/
//define("NC_ACTION_TYPE_CORRECTIVE",62281);
define("NC_NO_CAPABILITY",62281);
/*indica potencial - que n�o ocorreu ainda*/
//define("NC_ACTION_TYPE_PREVENTIVE",62282);
define("NC_CAPABILITY",62282);

define("NC_ACTION_CLASSIFICATION_INTERNAL_AUDIT",62251);
define("NC_ACTION_CLASSIFICATION_EXTERNAL_AUDIT",62252);
define("NC_ACTION_CLASSIFICATION_SECURITY_CONTROL",62253);

class CINonConformity extends ISMSContext implements IWorkflow {
 /**
  * Construtor.
  *
  * <p>Construtor da classe CINonConformity.</p>
  * @access public
  */



 /**
  * Construtor.
  *
  * <p>Construtor da classe CINonConformity.</p>
  * @access public
  */
  public function __construct(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    parent::__construct('ci_nc');

    $this->ciContextType = CONTEXT_CI_NON_CONFORMITY;

    $this->csAliasId = 'nc_id';
    $this->coDataset->addFWDDBField(new FWDDBField('fkcontext'           ,'nc_id'                 ,DB_NUMBER  ));
    $this->coDataset->addFWDDBField(new FWDDBField('fkcontrol'           ,'nc_control_id'         ,DB_NUMBER  ));
    $this->coDataset->addFWDDBField(new FWDDBField('fkresponsible'       ,'nc_responsible_id'     ,DB_NUMBER  ));
    $this->coDataset->addFWDDBField(new FWDDBField('fksender'            ,'nc_sender_id'          ,DB_NUMBER  ));
    $this->coDataset->addFWDDBField(new FWDDBField('sname'               ,'nc_name'               ,DB_STRING  ));
    $this->coDataset->addFWDDBField(new FWDDBField('nseqnumber'          ,'nc_seqnumber'          ,DB_NUMBER  ));
    $this->coDataset->addFWDDBField(new FWDDBField('tdescription'        ,'nc_description'        ,DB_STRING  ));
    $this->coDataset->addFWDDBField(new FWDDBField('tcause'              ,'nc_cause'              ,DB_STRING  ));
    $this->coDataset->addFWDDBField(new FWDDBField('tdenialjustification','nc_denialjustification',DB_STRING  ));
    $this->coDataset->addFWDDBField(new FWDDBField('nclassification'     ,'nc_classification'     ,DB_NUMBER  ));
    $this->coDataset->addFWDDBField(new FWDDBField('ncapability'         ,'nc_capability'         ,DB_NUMBER  ));
    $this->coDataset->addFWDDBField(new FWDDBField('ddatesent'           ,'nc_datesent'           ,DB_DATETIME));

    $this->caSensitiveFields = array('nc_name', 'nc_description');
    $this->caSearchableFields = array('nc_name', 'nc_description', 'nc_cause', 'nc_denialjustification');

    $this->coWorkflow = new NCWorkflow(ACT_NON_CONFORMITY_APPROVAL);
  }

  /**
  * Retorna o label do contexto.
  *
  * <p>M�todo para retornar o label do contexto.</p>
  * @access public
  * @return string Label do contexto
  */
  public function getLabel(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

    return FWDLanguage::getPHPStringValue('mx_non_conformity', "N�o Conformidade");
  }

  /**
  * Retorna o nome do contexto.
  *
  * <p>M�todo para retornar o nome do contexto.</p>
  * @access public
  * @return string Nome do contexto
  */
  public function getName(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

    return $this->getFieldValue('nc_name');
  }

 /**
  * Retorna a descri��o do contexto.
  * 
  * <p>M�todo para retornar a descri��o do contexto.
  * Deve ser sobreescrito nas classes filhas.</p>
  * @access public 
  * @return string Descri��o do contexto
  */ 
  public function getDescription() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return $this->getFieldValue('nc_description');
  }

  /**
   * Verifica se o usu�rio pode deletar a n�o conformidade.
   *
   * <p>M�todo para verificar se o usu�rio pode deletar a n�o conformidade.</p>
   * @access public
   * @param integer piNCId Id da n�o conformidade.
   * @return boolean True|False
   */
  public function userCanDelete($piNCId){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

    $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
    if(!in_array('M.CI.3.4',$maACLs)){
      return true;
    }else{
      $moNonConformity = new CINonConformity();
      if ($moNonConformity->getApprover() == ISMSLib::getCurrentUserId()) {
        return true;
      }
      return false;
    }
  }



 /**
  * Deleta logicamente um contexto.
  *
  * <p>M�todo para deletar logicamente um contexto.</p>
  * @access public
  * @param integer $piContextId id do contexto
  */
  public function logicalDelete($piContextId) {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    /* deleta as rela��es entre os planos de a��o e a n�o conformidade deletada */
    $moNc = new CINcActionPlan();
    $moNc->createFilter($piContextId, 'nc_id');
    $moNc->delete();
  }

   /**
   * Verifica se o usu�rio pode editar a n�o conformidade.
   *
   * <p>M�todo para verificar se o usu�rio pode editar a n�o conformidade.</p>
   * @access public
   * @param integer piNCId Id da n�o conformidade.
   * @return boolean True|False
   */
  public function userCanEdit($piNCId,$piUserResponsible = 0){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

    $moNonConformity = new CINonConformity();
    $moNonConformity->fetchById($piNCId);
    $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    $miState = $moNonConformity->getContextState();
    $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();

    $maStatesGNC = array(CONTEXT_STATE_DENIED, CONTEXT_STATE_CI_OPEN, CONTEXT_STATE_CI_SENT, CONTEXT_STATE_CI_NC_PENDANT,CONTEXT_STATE_CI_WAITING_CONCLUSION,CONTEXT_STATE_CI_FINISHED,CONTEXT_STATE_CI_CLOSED);
    //print "OOOOOOO";
    //if ($moNonConformity->getContextState()==CONTEXT_STATE_DENIED) return false;
    //print "A";
    if ($miUserId == $moNonConformity->getApprover() && in_array($miState, $maStatesGNC)) return true;
    //print "B";
    if ($miUserId == $moNonConformity->getNCResponsible() && $miState == CONTEXT_STATE_CI_DIRECTED) return true;
    if ($moNonConformity->getCreator() == $miUserId && $miState == CONTEXT_STATE_PENDANT) return true;
    //permiss�o para quem tem 'editar sem restri��o' � a mesma do gestor de NC
    if ( (in_array($miState,$maStatesGNC) || $miState == CONTEXT_STATE_CI_DIRECTED) && (!in_array('M.CI.3.3',$maACLs))) return true;
    //return false;
    //print "C";

    return false;
  }

  /**
   * Verifica se o usu�rio pode inserir a n�o conformidade.
   *
   * <p>M�todo para verificar se o usu�rio pode inserir a n�o conformidade.</p>
   * @access public
   * @return boolean True|False
   */
  public function userCanInsert(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
    if(in_array('M.CI.3.2',$maACLs)){
      return false;
    }else{
      return true;
    }
  }

  /**
   * Retorna o aprovador do contexto.
   *
   * <p>Retorna o aprovador do contexto.</p>
   * @access public
   * @return integer Id do aprovador do contexto.
   */
  public function getApprover(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

    return ISMSLib::getConfigById(USER_NON_CONFORMITY_MANAGER);
  }

  /**
   * Retorna o respons�vel do contexto.
   *
   * <p>Retorna o respons�vel do contexto.</p>
   * @access public
   * @return integer Id do respons�vel do contexto.
   */
  public function getResponsible(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

    return $this->getCreator();
  }

  /**
   * Retorna o respons�vel da n�o conformidade.
   *
   * <p>Retorna o respons�vel da n�o conformidade.</p>
   * @access public
   * @return integer Id do respons�vel da n�o conformidade.
   */
  public function getNCResponsible(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

    return $this->getFieldValue('nc_responsible_id');
  }

  /**
   * Retorna os ids dos planos de a��o relacionados a n�o conformidade.
   *
   * <p>Retorna os ids dos planos de a��o relacionados a n�o conformidade</p>
   * @access public
   * @return array Ids dos planos de a��o relacionados a n�o conformidade.
   */
  public function getCIAps($piNcId){
    $moNcAP = new CINcActionPlan();
    $moNcAP->createFilter($piNcId,'nc_id');
    $moNcAP->select();
    $maIds = array();
    while($moNcAP->fetch()){
      $maIds[] = $moNcAP->getFieldValue('ap_id');
    }
    return $maIds;
  }

  /**
   * Retorna a pr�xima a��o para mudan�a de estado.
   *
   * <p>Retorna a pr�xima a��o para mudan�a de estado.</p>
   *
   * @access public
   * @param integer $piNCId Id da N�o Conformidade; Se n�o for passado nenhum, assume o atual.
   * @return integer Id da a��o para mudan�a de estado ou 0 se n�o houver a��o dispon�vel.
   */
  public function getStateForwardAction($piNCId='') {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    if ($piNCId) {
      $moNonConformity = new CINonConformity();
      $moNonConformity->fetchById($piNCId);
    } else {
      $moNonConformity = $this;
      $piNCId = $moNonConformity->getFieldValue('nc_id');
    }

    $miUserId = ISMSLib::getCurrentUserId();
    $miResponsibleId = $moNonConformity->getFieldValue('nc_responsible_id');
    $mbUserIsManager = ($miUserId == $moNonConformity->getApprover());
    $mbUserIsResponsible = ($miUserId == $moNonConformity->getNCResponsible());

    $maAction = array();
    switch($moNonConformity->getContextState()){
      case CONTEXT_STATE_CI_SENT:{/*aguardando confirma��o*//*com confirma��o*/
        if( $mbUserIsManager ){
          //aprova��o pelo gestor de n�o conformidade
          $maAction['confirm'] = WKF_ACTION_APPROVE;
        }
        break;
      }
      case CONTEXT_STATE_CI_OPEN:{/*aberto*//*com confirma��o*/
        if ( $miResponsibleId && $mbUserIsManager && $moNonConformity->getFieldValue('nc_capability') && $moNonConformity->getFieldValue('nc_classification')) {
          //ESTADO: Aberto; CAMPOS: respons�vel, potencial, classifica��o; USU�RIO: GNC
          $maAction['confirm'] = WKF_ACTION_CI_NC_SEND_TO_RESPONSIBLE;
        }
        break;
      }
      case CONTEXT_STATE_CI_DIRECTED:{/*encaminhado*//*com confirma��o*/

        if( (trim($moNonConformity->getFieldValue('nc_cause'))!='') && count($this->getCIAps($moNonConformity->getFieldValue('nc_id'))) && $mbUserIsResponsible){
          /*se j� possui causa e planos de a��o e se o usu�rio for o respons�vel, a nc est� apta a passar para
          o estado de "aceite do plano de a��o pendente" */
          $maAction['confirm'] = WKF_ACTION_SEND_NC_TO_APPROVE;
        }
        break;
      }

      case CONTEXT_STATE_CI_AP_PENDANT:{/*aceite do plano de a��o pendente*/
      /*esta mudan�a de estado depende somente das aprova��es dos planos de a��o relacionados a esta n�o conformidade*/
      break;
      }

      case CONTEXT_STATE_CI_NC_PENDANT:{/*tratamento da n�o conformidade pendente*/
        if($mbUserIsManager){
          $maAction['confirm'] = WKF_ACTION_CI_NC_SEND_TO_WAITING_CONCLUSION;
          //$maAction['confirm'] = WKF_ACTION_APPROVE;
        }
        break;
      }

      case CONTEXT_STATE_CI_WAITING_CONCLUSION:{/*aguardando conclus�o*/
        /*passa para o estado de concluido quanto todos os planos de a��o relacionados com a NC estiverem concluidos*/
        break;
      }

      case CONTEXT_STATE_CI_FINISHED:{/*concluido*/
        /*aguarda medi��o de efic�cia do PA para passar para o estado fechado*/
        break;
      }

      case CONTEXT_STATE_CI_CLOSED:{/*finalizado*/
        /* fechado, n�o existe mais nada a ser feito! */
        break;
      }

      default:{
       break;
      }

    }

    return $maAction;
  }

  /**
   * Exibe tela de confirma��o da mudan�a de estado, ou tela de finaliza��o da n�o conformidade.
   *
   * <p>Exibe tela de confirma��o da mudan�a de estado, ou tela de finaliza��o da n�o conformidade.</p>
   *
   * @access public
   * @param integer $piNCId Id da N�o Conformidade; Se n�o for passado nenhum, assume o atual.
   * @return boolean True em caso de haver uma a��o. Falso se n�o houver possibilidade de altera��o de estado.
   */
  public function getStateForwardConfirmation($piNCId='') {
    if ($piNCId) {
      $moNonConformity = new CINonConformity();
      $moNonConformity->fetchById($piNCId);
    } else {
      $moNonConformity = $this;
      $piNCId = $moNonConformity->getFieldValue('nc_id');
    }

    $maAction = $moNonConformity->getStateForwardAction($piNCId);
    $miDirectAction = isset($maAction['direct'])?$maAction['direct']:0;
    $miConfirmAction = isset($maAction['confirm'])?$maAction['confirm']:0;
    if ($miDirectAction) {
      $moNonConformity->stateForward($miDirectAction);
      echo "soWindow = self.getOpener();'
          .'if(soWindow.refresh_grid) soWindow.refresh_grid();";
    }

    switch($miConfirmAction) {
      case WKF_ACTION_CI_NC_SEND_TO_RESPONSIBLE:
      case WKF_ACTION_SEND_NC_TO_APPROVE:
        echo "isms_open_popup('popup_non_conformity_state_forward','packages/improvement/popup_non_conformity_state_forward.php?nc={$piNCId}','','true',122,350);";
        return true;
      break;
      case WKF_ACTION_CI_NC_SEND_TO_WAITING_CONCLUSION:
      case WKF_ACTION_APPROVE:
        $moTask = new WKFTask();
        $moTask->createFilter($piNCId,'task_context_id');
        $moTask->createFilter(ISMSLib::getCurrentUserId(),'task_receiver_id');
        $moTask->createFilter(1,'task_is_visible');
        $moTask->select();
        $moTask->fetch();
        $miTaskId = $moTask->getFieldValue('task_id');
        echo "isms_open_popup('popup_non_conformity_approval_task', 'packages/improvement/popup_non_conformity_approval_task.php?task={$miTaskId}', '', 'true');";
        return true;
      break;
      default:
        return false;
      break;
    }
  }

/**
  * Retorna o caminho do contexto.
  *
  * <p>M�todo para retornar o caminho do contexto.</p>
  * @access public
  * @return string Caminho do contexto
  */
  public function getSystemPath() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $msRMPath = "<a href='javascript:isms_redirect_to_mode(" . INCIDENT_MODE . ",0,0)'>".FWDLanguage::getPHPStringValue('st_incident_module_name','Melhoria Cont�nua')."</a>";
    $msContextPath = ISMSLib::getIconCode('icon-ci_nonconformity.gif',-2,-4). "<a href='javascript:isms_redirect_to_mode(" . INCIDENT_MODE . "," . CONTEXT_CI_NON_CONFORMITY . "," . $this->getFieldValue($this->getAliasId()) . ")'>" . $this->getName() . "</a>";
    return $msRMPath ."&nbsp;".ISMSLib::getIconCode('icon-arrow-right.gif',-2,0).'&nbsp;' . $msContextPath;
  }

  /**
  * Retorna o c�digo HTML do icone do contexto.
  *
  * <p>M�todo para retornar o c�digo HTML do contexto</p>
  * @access public
  * @param integer $piId Id do contexto
  * @param integer $pbIsFetched id do contexto
  * @return string contendo o c�digo HTML do icone do contexto
  */
  public function getIconCode($piId, $pbIsFetched = false){
    return ISMSLib::getIconCode('icon-ci_nonconformity.gif', -2, 7);
  }

 /**
  * Retorna o caminho para abrir a popup de edi��o desse contexto.
  *
  * <p>M�todo para retornar o caminho para abrir a popup de edi��o desse contexto.</p>
  * @param integer $piContextId id do contexto
  * @access public
  * @return string caminho para abrir a popup de edi��o desse contexto
  */
  public function getVisualizeEditEvent($piContextId,$psUniqId){
    $soSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    $msPackagesIncidentPath = ($soSession->getMode()==MANAGER_MODE) ? "packages/improvement/" : "../../packages/improvement/";
    $msPopupId = "popup_non_conformity_edit";

    return "isms_open_popup('{$msPopupId}','{$msPackagesIncidentPath}{$msPopupId}.php?nc=$piContextId','','true');"
           ."soPopUpManager.getPopUpById('{$msPopupId}').setCloseEvent( function close_visualize_$psUniqId() {soPopUpManager.closePopUp('popup_visualize_$psUniqId');} );";
  }
  
  /**
  * Retorna o �cone do contexto.
  *
  * <p>M�todo para retornar o �cone do contexto.</p>
  * @access public
  * @param integer $piContextId Id do contexto
  * @return string Nome do �cone
  */
  public function getIcon($piContextId = 0){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return 'icon-ci_nonconformity.gif';
  }

}
?>