<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

// Estados de um contexto
define("CONTEXT_STATE_NONE", 2700);
define("CONTEXT_STATE_PENDANT", 2701);
define("CONTEXT_STATE_APPROVED", 2702);
define("CONTEXT_STATE_DENIED", 2703);
define("CONTEXT_STATE_COAPPROVED", 2704);
define("CONTEXT_STATE_DELETED", 2705);

define("CONTEXT_STATE_DOC_DEVELOPING", 2751);
define("CONTEXT_STATE_DOC_APPROVED", 2752);
define("CONTEXT_STATE_DOC_PENDANT", 2753);
define("CONTEXT_STATE_DOC_REVISION", 2754);
define("CONTEXT_STATE_DOC_OBSOLETE", 2755);
define("CONTEXT_STATE_DOC_TO_BE_PUBLISHED", 2756);
define("CONTEXT_STATE_DOC_APPROVAL", CONTEXT_STATE_DOC_PENDANT);
define("CONTEXT_STATE_DOC_PUBLISHED", CONTEXT_STATE_DOC_APPROVED);

define("CONTEXT_STATE_CI_SENT",2770);
define("CONTEXT_STATE_CI_OPEN", 2771);
define("CONTEXT_STATE_CI_DIRECTED", 2772);
define("CONTEXT_STATE_CI_NC_PENDANT", 2773);
define("CONTEXT_STATE_CI_AP_PENDANT", 2774);
define("CONTEXT_STATE_CI_WAITING_CONCLUSION", 2775);
define("CONTEXT_STATE_CI_FINISHED", 2776);
define("CONTEXT_STATE_CI_CLOSED", 2777);

define("CONTEXT_STATE_INC_OPEN", 2779);
define("CONTEXT_STATE_INC_DIRECTED", 2780);
define("CONTEXT_STATE_INC_PENDANT_DISPOSAL", 2781);
define("CONTEXT_STATE_INC_WAITING_SOLUTION", 2782);
define("CONTEXT_STATE_INC_PENDANT_SOLUTION", 2783);
define("CONTEXT_STATE_INC_SOLVED", 2784);

define("CONTEXT_STATE_AP_PENDANT", 2785);
define("CONTEXT_STATE_AP_DENIED", 2786);
define("CONTEXT_STATE_AP_WAITING_CONCLUSION", 2787);
define("CONTEXT_STATE_AP_WAITING_MEASUREMENT", 2788);
define("CONTEXT_STATE_AP_MEASURED", 2789);

define("CONTEXT_STATE_CI_WAITING_DEADLINE",2790);

// Pendente, Aprovado, Negado, Atrasado, Conclu�do
include_once $handlers_ref . "QuerySearchContext.php";
include_once $handlers_ref . "QueryContextsByDocumentId.php";
include_once $handlers_ref . "QueryDocumentsByContextId.php";

/**
 * Classe ISMSContext.
 *
 * <p>Classe abstrata que representa um contexto do sistema (�rea, processo, ativo, ...).</p>
 * @package ISMS
 * @subpackage classes
 */
abstract class ISMSContext extends ISMSTable {
  
 /**
  * C�digo do tipo do contexto
  * @var integer
  * @access protected
  */
  protected $ciContextType = CONTEXT_NONE;
  
 /**
  * Objeto de Workflow
  * @var RMWorkflow
  * @access protected
  */
  protected $coWorkflow = NULL;
 
 /**
  * Id do contexto
  * @var integer
  * @access protected
  */ 
  protected $ciContextId = 0;
  
 /**
  * Array de Campos Sens�veis 
  * @var array
  * @access protected
  */ 
  protected $caSensitiveFields = array();
 
 /**
  * Hash com os valores dos campos sens�veis 
  * @var string
  * @access protected
  */ 
  protected $csHash = '';
  
 /**
  * Array de campos que podem ser pesquisados na busca geral do sistema 
  * @var array
  * @access protected
  */ 
  protected $caSearchableFields = array();
  
 /**
  * Cont�m o alias do id do subordinante do contexto
  * @var string
  * @access protected
  */ 
  protected $csDependenceAliasId = "";
  
 /**
  * Stored Procedure para remo��o recursiva de estruturas (�rea, categoria, se��o)
  * @var string
  * @access protected
  */ 
  protected $csSpecialDeleteSP = "";
  
 /**
  * Indica se o contexto possui um documento associado
  * @var boolean
  * @access protected
  */ 
  protected $cbHasDocument = false;
  
 /**
  * Busca informa��es de um context atrav�s de seu identificador.
  * 
  * <p>M�todo para buscar informa��es de um registro atrav�s de seu identificador.</p>
  * @access public 
  * @param integer $piId Identificador 
  */ 
  public function fetchById($piId) {
    $this->ciContextId = $piId;
    return parent::fetchById($piId, $this->csAliasId);
  }
  
 /**
  * Insere um contexto.
  * 
  * <p>M�todo para inserir um context e retornar seu identificador.</p>
  * @access public
  * 
  * @param boolean $pbReturnId Indica se deve retornar o id do contexto inserido
  * @param boolean $pbExecBySystem indica que o insert est� sendo executado pelo sistema, assim n�o � nescess�rio testar se o usu�rio logado tem permiss�o de insert para o contexto
  * @return integer Id do contexto inserido
  */ 
  public function insert($pbReturnId = false,$pbExecBySystem = false) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

    if(!$pbExecBySystem){
		if(!$this->userCanInsert()){
	      trigger_error("Unauthorized user trying to insert a ".strtolower($this->getLabel()).".",E_USER_ERROR);
	    }
	}
    $miContextId = 0;
    // insere na tabela de contexto e pega o id
    $moContextObject = new ISMSContextObject();
    $moContextObject->setFieldValue('context_type', $this->ciContextType);
    $moContextObject->setFieldValue('context_state', CONTEXT_STATE_NONE);
    $miContextId = $moContextObject->insert(true);
    // guarda o id do contexto inserido
    $this->ciContextId = $miContextId;
    // grava a data e por quem foi criado o contexto
    $moContextDate = new ISMSContextDate();
    $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    $moContextDate->insert($miContextId, $moSession->getUserId(), $moSession->getUserName(true));
    // insere o contexto em sua respectiva tabela
    $this->setFieldValue($this->csAliasId, $miContextId);
    parent::insert();
    // se possuir workflow, avan�a para o pr�ximo estado
    if(isset($this->coWorkflow)) $this->coWorkflow->stateForward($this, WKF_ACTION_NONE);
    // gera o log
    $this->createLog(ADT_ACTION_CREATE);
    /*
     * Se possuir o modo de doumenta��o e a cria��o autom�tica
     * de documentos estiver habilitada, e o contexto possuir
     * um documento, deve-se criar um documento automaticamente
     * para associar com o contexto em quest�o.
     */
    $moConfig = new ISMSConfig();
    if(FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(POLICY_MODE)){
      if($this->cbHasDocument && $moConfig->getConfig(GENERAL_AUTOMATIC_DOCUMENTS)){
        $this->createDocument();
      }
    }
    return $miContextId;
  }

 /**
  * Cria o documento do contexto.
  * 
  * <p>M�todo para criar o documento do contexto.</p>
  * @access private
  * 
  */
  private function createDocument() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
     $moDocument = new PMDocument();
     $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();           
    $msDocName = FWDLanguage::getPHPStringValue('db_default_document_name', '%label_doc% %label_context% %context_name%');
    $msDocName = str_replace( array('%label_doc%', '%label_context%', '%context_name%'), array($moDocument->getLabel(), $this->getLabel(), $this->getName() ), $msDocName);
    /*bug no tamanho do documento que pode ser maior que 256 caracteres*/
    //$msDocName = FWDWebLib::convertToISO($msDocName, false);
    if(mb_strlen($msDocName, mb_detect_encoding($msDocName,"UTF-8, ISO-8859-1"))>255){
      $msDocName = ISMSLib::truncateString($msDocName,253);
    }
    $moDocument->setFieldValue('document_name', $msDocName, true);
    $moDocument->setFieldValue('document_description', $msDocName, true);
    $moDocument->setFieldValue('document_type', $this->ciContextType);
    $moDocument->setFieldValue('document_author', $miUserId);
    $moDocument->setFieldValue('document_main_approver', $miUserId);
    
    /*
     * Se for um controle, a data de implementa��o do
     * documento deve ser a mesma de seu respectivo controle.
     */
    if ($this->ciContextType == CONTEXT_CONTROL) 
      $moDocument->setFieldValue('document_deadline', $this->getFieldValue('control_date_deadline'));
      
    $miDocumentId = $moDocument->insert(true);
    
    $moDocContext = new PMDocContext();
    $moDocContext->setFieldValue('context_id',$this->ciContextId);
    $moDocContext->setFieldValue('document_id',$miDocumentId);
    $moDocContext->insert();
  }
 
 /**
  * Atualiza um contexto.
  * 
  * <p>M�todo para atualizar um contexto.</p>
  * @access public
  * @param integer $piContextId Identificador do contexto
  * @param boolean $pbLog Indica se deve logar o update
  * @param boolean $pbHasSensitiveChanges Indica se um dos campos sens�veis foi modificado
  */  
  public function update($piContextId, $pbLog = true, $pbHasSensitiveChanges = false) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    // guarda o id do contexto atualizado
    $this->ciContextId = $piContextId;
    // atualiza o contexto
    parent::update($piContextId);
    //gera o log e grava a data e por quem o contexto foi modificado
    if ($pbLog) {
			$moContextDate = new ISMSContextDate();
			$moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
			$moContextDate->update($piContextId, $moSession->getUserId(), $moSession->getUserName(true));
			$this->createLog(ADT_ACTION_EDIT);
    }    
    // avan�a o estado do workflow somente se um dos campos sens�veis for modificado
    if(isset($this->coWorkflow) && $pbHasSensitiveChanges){
      $this->coWorkflow->stateForward($this, WKF_ACTION_EDIT);
    }    
  }

 /**
  * Insere ou atualiza um contexto.
  * 
  * <p>M�todo para inserir ou editar um contexto. Se recebe um id n�o vazio e
  * diferente de zero, tenta dar update, sen�o insere. Sempre retorna o id.</p>
  * @access public
  * @param integer $piContextId Identificador do contexto
  * @param boolean $pbLog Indica se deve logar o update
  * @param boolean $pbHasSensitiveChanges Indica se um dos campos sens�veis foi modificado
  * @return integer Id do contexto
  */  
  public function insertOrUpdate($piContextId, $pbLog = true, $pbHasSensitiveChanges = false){
    if($piContextId){
      $this->testPermissionToEdit($piContextId);
      $this->update($piContextId,$pbLog,$pbHasSensitiveChanges);
      return $piContextId;
    }else{
      $this->testPermissionToInsert();
      return $this->insert(true);
    }
  }

 /**
  * Retorna o identificador do contexto subordinante a este contexto.
  * 
  * <p>M�todo que retorna o identificador do contexto subordinante deste contexto.</p>
  * @access public
  * @param integer $piContextId id do contexto (subordinado)
  * @return integer Id do contexto subordinante
  */ 
  public function getDependenceId($piContextId) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
  }
  
 /**
  * Retorna os ancestrais do contexto.
  * 
  * <p>M�todo para retornar todos contextos acima de um determinado contexto na
  * �rvore de depend�ncias.</p>
  * @access public
  * @param integer $piContextId Id do contexto
  * @param boolean $pbExecByTrash se o metodo est� sendo chamado pela lixeira
  * @return array Array de ids dos ancestrais do contexto
  */
  public function getSuperContexts($piContextId,$pbExecByTrash = false){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $maSuperContexts = array();
    if($this->csDependenceAliasId){
      $miParentId = $this->getFieldValue($this->csDependenceAliasId);
      $moContextObject = new ISMSContextObject();
      if(!$miParentId){
        $moObject = $moContextObject->getContextObjectByContextId($piContextId);
        $moObject->fetchById($piContextId);
        $miParentId = $moObject->getFieldValue($this->csDependenceAliasId);
      }
      if($miParentId){
        $moParent = $moContextObject->getContextObjectByContextId($miParentId);
        $moParent->fetchById($miParentId);
        $maSuperContexts = $moParent->getSuperContexts($miParentId,$pbExecByTrash);
        $maSuperContexts[] = $miParentId;
      }
    }
    return $maSuperContexts;
  }

 /**
  * Retorna os sub-contextos de um contexto.
  * 
  * <p>M�todo para retornar os sub-contextos de um contexto.</p>
  * @access public
  * @param integer $piContextId Id do contexto
  * @return array Array de ids dos sub-contextos
  */ 
  public function getSubContexts($piContextId){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return array();
  }

 /**
  * Retorna o �cone do contexto.
  *
  * <p>M�todo para retornar o �cone do contexto.</p>
  * @access public
  * @param integer $piContextId Id do contexto
  * @return string Nome do �cone
  */
  public function getIcon($piContextId = 0){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return 'icon-wrong.gif';
  }

 /**
  * Indica se o contexto � delet�vel ou n�o.
  * 
  * <p>M�todo que indica se o contexto � delet�vel ou n�o. Deve ser sobrecarregada
  * por contextos para os quais for necess�ria.</p>
  * @access public
  * @param integer $piContextId id do contexto
  */ 
  public function isDeletable($piContextId) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return true;
  }
  
 /**
  * Exibe uma popup caso n�o seja poss�vel remover um contexto.
  * 
  * <p>M�todo que exibe uma popup caso n�o seja poss�vel remover um contexto.
  * Deve ser sobrecarregada por contextos para os quais for necess�ria.</p>
  * @access public
  * @param integer $piContextId id do contexto
  */ 
  public function showDeleteError($piContextId) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
  }
  
 /**
  * Deleta logicamente um contexto.
  * 
  * <p>M�todo para deletar logicamente um contexto. Deve ser sobrecarregada
  * por contextos para os quais for necess�ria.</p>
  * @access public
  * @param integer $piContextId id do contexto
  */ 
  public function logicalDelete($piContextId) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
  }

 /**
  * deleta um documento do contexto.
  * 
  * <p>M�todo para deletar um documento do contexto.</p>
  * @access private
  * 
  */
  private function deleteDocument($pbDeleteDB = false,$pbExecByTrash = false,$pbExecBySystem = false) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    //pega todos documentos relacionados ao contexto sendo deletado
    $maAllDocs = array();
    $maContCtxByDocs = array();
    $moQueryAllDocs = new QueryDocumentsByContextId(FWDWebLib::getConnection());
    $moQueryAllDocs->setExecByTrash($pbExecByTrash);
    $moQueryAllDocs->setId($this->ciContextId);
    $maAllDocs = $moQueryAllDocs->getValue();
    //para cada documento relacionado ao contexto, verificar se ele est� relacionado com mais de um contextos
    //se estiver relacionado somente com o contexto sendo deletado, deletar tamb�m o documentos
    foreach($maAllDocs as $miValue){
      $moQueryAllCtx = new QueryContextsByDocumentId(FWDWebLib::getConnection());
      $moQueryAllCtx->setExecByTrash($pbExecByTrash);
      $moQueryAllCtx->setId($miValue);
      $maAllCtxByDoc = $moQueryAllCtx->getValue();
      $maContCtxByDocs[$miValue] = count($maAllCtxByDoc);
    }
    //delete os documentos que possuem somente rela��o com o  contexto sendo deletado
    foreach($maContCtxByDocs as $miKey=>$miValue){
      if($miValue <= 1){
        $moDocument = new PMDocument();
        $moDocument->delete($miKey,$pbDeleteDB,$pbExecByTrash,$pbExecBySystem);
      }
    }
  }

 /**
  * Deleta um contexto.
  * 
  * <p>M�todo para deletar um contexto.
  * N�o deleta do banco. Somente troca seu estado para deletado.</p>
  * @access public
  * @param integer $piContextId Id do contexto
  * @param boolean $pbDeleteDB Remove do banco
  * @param boolean $pbExecByTrash Delete chamado pela Lixeira do sistema 
  * @param boolean $pbExecBySystem delete logico executado pelo sistema
  */
  public function delete($piContextId, $pbDeleteDB = false,$pbExecByTrash = false,$pbExecBySystem = false){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    /*
    o atributo $pbExecBySystem foi criado para evitar erros de permiss�o de dele��o l�gica quando o usu�rio que 
    est� logado tem permiss�o para deletar um contexto do tipo X mas n�o tem permiss�o para deletar um contexto
    do tipo Y e o tipo X � pai do tipo Y, ou seja, ao deletar um tipo X, os elementos do tipo Y que est�o relacionados
    ao elemento do tipo X que est� sendo deletado tambem devem ser deletados.
    */
    // necessita pegar os dados do objeto para criar o log
    if($this->fetchById($piContextId)){
      if($pbDeleteDB){

        //delete fisico (se n�o for chamado pela lixeira, testar se o usu�rio tem permiss�o)
        if(!$pbExecByTrash){
          if(!$this->userCanDelete($piContextId)){
            trigger_error("Unauthorized user trying to delete a ".strtolower($this->getLabel()).".",E_USER_ERROR);
          }
        }//else{/* evento de dele��o permanente utilizado pela lixeira, n�o � necess�rio testar as permiss�es para dele��o*/}

        // gera o log
        $this->createLog(ADT_ACTION_GARBAGE_REMOVE);
        // deleta da tabela respectiva ao contexto
        if($this->csSpecialDeleteSP){
          switch(FWDWebLib::getDatabaseType()){
            case DB_MSSQL:{
              $this->execute("EXEC ".FWDWebLib::getFunctionUserDB()."{$this->csSpecialDeleteSP} $piContextId");
              break;
            }
            case DB_POSTGRES:{
              $this->execute("SELECT ".FWDWebLib::getFunctionUserDB()."{$this->csSpecialDeleteSP}($piContextId)");
              break;
            }
            case DB_ORACLE:{
              $this->execute("BEGIN ".FWDWebLib::getFunctionUserDB()."{$this->csSpecialDeleteSP}($piContextId); END;");
              break;
            }
            default:{
              trigger_error("Delete not implemented for database type '".FWDWebLib::getDatabaseType()."'.",E_USER_ERROR);
            }
          }
        }else{
           /*
           * Se possuir o modo de doumenta��o e o contexto possuir
           * um documento, deve-se deletar o documento e este � o ultimo contexto relacionado a ele
           */
          $moConfig = new ISMSConfig();
          if(FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(POLICY_MODE)){
            if($this->cbHasDocument){
              $this->deleteDocument($pbDeleteDB, $pbExecByTrash);
            }
          }
          
          parent::delete($piContextId);
        }
        // deleta da tabela de contexto
        $moContextObject = new ISMSContextObject();
        $moContextObject->delete($piContextId);
      }else{
        //delete l�gico - sempre executado pelo sistema
        if(!$pbExecBySystem){
          //delete l�gico - sempre executado pelo sistema
          if(!$this->userCanDelete($piContextId)){
            trigger_error("Unauthorized user trying to delete a ".strtolower($this->getLabel()).".",E_USER_ERROR);
          }
        }
        // guarda o id do contexto deletado
        $this->ciContextId = $piContextId;
        
        if(($pbExecBySystem ) || ($this->isDeletable($piContextId))){
           /*
           * Se possuir o modo de doumenta��o e o contexto possuir
           * um documento, deve-se deletar o documento e este � o ultimo contexto relacionado a ele
           */
          $moConfig = new ISMSConfig();
          if(FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->hasModule(POLICY_MODE)){
            if($this->cbHasDocument){
              /*
              independente do usu�rio logado, pela hierarquia de objetos, em alguns casos h� a 
              nescecidade de deletar outros elementos relacionados ao contexto sendo deletado
              ent�o foi criado o execBySystem que for�a a dele��o de elementos relacionados ao contexto
              sendo deletado.
            */
              $mbExecBySystem = true;
              $this->deleteDocument($pbDeleteDB, $pbExecByTrash,$mbExecBySystem);
            }
          }
          $this->logicalDelete($piContextId,false,false,$pbExecBySystem);
          // gera o log
          $this->createLog(ADT_ACTION_SYSTEM_REMOVE);
          // se possuir workflow, avan�a para o pr�ximo estado
          if(isset($this->coWorkflow)) $this->coWorkflow->stateForward($this, WKF_ACTION_DELETE);
          else $this->setContextState(CONTEXT_STATE_DELETED);
          // deletar tarefas do contexto deletado
          $moTasks = new WKFTask();
          $moTasks->createFilter($this->ciContextId, 'task_context_id');
          $moTasks->delete();
          // deletar alertas do contexto deletado
          $moAlerts = new WKFAlert();
          $moAlerts->createFilter($this->ciContextId, 'alert_context_id');
          $moAlerts->delete();
        }else{
          $this->showDeleteError($piContextId);
        }
      }
    }
  }
 
 /**
  * Restaura um contexto.
  * 
  * <p>M�todo para restaurar um contexto.
  * @access public
  * @param integer $piContextId Identificador do contexto
  */ 
  public function restore($piContextId) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    // necessita pegar os dados do objeto para criar o log
    $this->fetchById($piContextId);   
    $this->createLog(ADT_ACTION_RESTORE);
    
    // guarda o id do contexto a ser restaurado
    $this->ciContextId = $piContextId;
    
    // se possuir workflow, avan�a para o pr�ximo estado
    if(isset($this->coWorkflow)) $this->coWorkflow->stateForward($this, WKF_ACTION_UNDELETE);
    else $this->setContextState(CONTEXT_STATE_NONE);
  }
 
 /**
  * Seta o estado do contexto.
  * 
  * <p>M�todo para setar o estado de um contexto.
  * Se n�o for passado como par�metro o id do contexto,
  * pega o id do objeto atual.</p>
  * @access public
  * @param integer $piState Estado do contexto
  * @param integer $piContextId Id do contexto
  */ 
  public function setContextState($piState, $piContextId = 0) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $miId = $piContextId ? $piContextId : $this->ciContextId;
    $moContext = new ISMSContextObject();
    $moContext->setFieldValue('context_state', $piState);
    $moContext->update($miId);
  }
  
 /**
  * Retorna o estado do contexto.
  * 
  * <p>M�todo para retornar o estado de um contexto.
  * Se n�o for passado como par�metro o id do contexto,
  * pega o id do objeto atual.</p>
  * @access public
  * @param integer $piContextId Id do contexto
  * @return integer Estado do contexto
  */
  public function getContextState($piContextId = 0) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $moContext = new ISMSContextObject();
    if($piContextId){
      $moContext->fetchById($piContextId);
    }else{
      $moContext->fetchById($this->ciContextId);
    }
    return $moContext->getFieldValue('context_state');
  }
  
 /**
  * Avan�a o estado do contexto no workflow.
  * 
  * <p>M�todo para avan�ar o estado do contexto no workflow.</p>
  * @access public
  * 
  * @param integer $piAction Tipo da a��o
  * @param string $psJustification Justificativa
  */
  public function stateForward($piAction, $psJustification = "") {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $this->coWorkflow->stateForward($this, $piAction, $psJustification);
  } 
 
 /**
  * Calcula o hash dos campos sens�veis.
  * 
  * <p>M�todo para calcular o hash dos campos sens�veis.</p>
  * @access public 
  * @param string $psHash Hash
  */
  public function setHash($psHash) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return $this->csHash = $psHash;
  }
 
 /**
  * Retorna o hash dos campos sens�veis.
  * 
  * <p>M�todo para retornar o hash dos campos sens�veis.</p>
  * @access public
  * @param boolean $pbConvert Indica se deve for�ar a convers�o para ISO
  * @return string Hash
  */
  public function getHash($pbConvert = false) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $maValues = array();
    foreach($this->caSensitiveFields as $msFieldAlias){
      $moField = $this->getFieldByAlias($msFieldAlias);
      if($moField->getType()==DB_DATETIME && !is_numeric($moField->getValue())){
        $maValues[$msFieldAlias] = $pbConvert ? FWDWebLib::convertToISO((string)strtotime($moField->getValue()), true) : (string)strtotime($moField->getValue());
      }else{
        $maValues[$msFieldAlias] = $pbConvert ? FWDWebLib::convertToISO((string)$moField->getValue(), true) : (string)$moField->getValue();
      }
    }
    return md5(serialize($maValues));
  }

 /**
  * Verifica se um dos campos sens�veis foi modificado.
  * 
  * <p>M�todo para verificar se um dos campos sens�veis foi modificado.</p>
  * @access public 
  * @return boolean Verdadeiro ou Falso
  */
  public function hasSensitiveChanges(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return $this->csHash != $this->getHash(true);
  }
 
 /**
  * Retorna o id do contexto.
  * 
  * <p>M�todo para retornar id do contexto.</p>
  * @access public 
  * @return integer Id do contexto
  */
  public function getId() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return $this->ciContextId;
  }
  
 /**
  * Retorna o nome do contexto.
  * 
  * <p>M�todo para retornar o nome do contexto.</p>
  * @access public 
  * @return string Nome do contexto
  */
  abstract public function getName(); 
 
 /**
  * Retorna o label do contexto.
  * 
  * <p>M�todo para retornar o label do contexto.</p>
  * @access public 
  * @return string Label do contexto
  */ 
  abstract public function getLabel();
  
 /**
  * Retorna a descri��o do contexto.
  * 
  * <p>M�todo para retornar a descri��o do contexto.
  * Deve ser sobreescrito nas classes filhas.</p>
  * @access public 
  * @return string Descri��o do contexto
  */ 
  public function getDescription() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return "";
  }
  
 /**
  * Retorna o caminho do contexto.
  * 
  * <p>M�todo para retornar o caminho do contexto.</p>
  * @access public 
  * @return string Caminho do contexto
  */ 
  public function getSystemPath() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return "";
  }
  
 /**
  * Retorna o id do usu�rio que criou o contexto.
  * 
  * <p>M�todo para retornar o id do usu�rio que criou o contexto.</p>
  * @access public
  * @return interger Id do usu�rio
  */ 
  public function getCreator() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $moContextDate = new ISMSContextDate();
    $moContextDate->createFilter($this->getFieldValue($this->csAliasId), 'context_id');
    $moContextDate->createFilter(ACTION_EDIT, 'action');
    $moContextDate->select();
    $moContextDate->fetch();
    return $moContextDate->getFieldValue('user_id');
  }
 
 /**
  * Retorna o alias do identificador do contexto.
  * 
  * <p>M�todo para retornar o alias do identificador do contexto.</p>
  * @access public
  * @return string Alias
  */ 
  public function getAliasId() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return $this->csAliasId;
  }
  
 /**
  * Retorna o alias do identificador do subordinante do contexto.
  * 
  * <p>M�todo para retornar o alias do identificador do subordinante do contexto.</p>
  * @access public
  * @return string Alias
  */ 
  public function getDependenceAliasId() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return $this->csDependenceAliasId;
  }
 
 /**
  * Retorna o tipo do contexto.
  * 
  * <p>M�todo para retornar o tipo do contexto.</p>
  * @access public
  * @return interger Tipo do contexto
  */ 
  public function getContextType() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return $this->ciContextType;
  }
  
 /**
  * Retorna os campos pesquis�veis.
  * 
  * <p>M�todo para retornar os campos pesquis�veis.</p>
  * @access public
  * @return array Arrays dos campos pesquis�veis
  */ 
  public function getSearchableFields() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return $this->caSearchableFields;
  }
  
 /**
  * Verifica se o contexto possui um documento associado.
  * 
  * <p>M�todo para verifica se o contexto possui um documento associado.</p>
  * @access public
  * @return boolean Verdadeiro ou falso
  */ 
  public function hasDocument() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return $this->cbHasDocument;
  }
  
  /**
   * Retorna os leitores e o respons�vel do processo.
   * 
   * <p>Retorna os leitores e o respons�vel do processo.</p>
   * @access public
   * @return array Array com leitores + respons�vel do processo.
   */
  public function getReadersAndResponsible($piProcessId) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    if (!$this->cbHasDocument) trigger_error("Cannot get readers! This context type ({$this->ciContextType}) cannot have documents.", E_USER_WARNING);
  }
  
 /**
  * Atualiza os leitores do documento do contexto.
  * 
  * <p>M�todo para atualizar os leitores do documento do contexto.
  * Atualiza tamb�m os contextos dependentes.
  * Deve ser sobreescrito pelos contextos que possuem documento.</p>
  * @access public 
  * @param integer $piContextId Id do contexto
  */
  public function updateReaders($piContextId) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    if (!$this->cbHasDocument) trigger_error("Cannot update readers! This context type ({$this->ciContextType}) cannot have documents.", E_USER_WARNING);
  }
   
 /**
  * Registra no log.
  * 
  * <p>M�todo para registrar no log.</p>
  * @access public
  * @param integer $piAction Id da a��o
  */ 
  public function createLog($piAction) {  
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);  
    // pega o nome do usu�rio atual
    $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    $moUser = new ISMSUser();
    $moUser->fetchById($miUserId);
    $msUserName = $moUser->getFieldValue('user_name');

    // pega o nome do contexto
    if (!$msContextName = $this->getName()) { //Associacao-consulta
      $moContextObject = new ISMSContextObject();
      $moObject = $moContextObject->getContextObjectByContextId($this->ciContextId);
      $moObject->fetchById($this->ciContextId);
      $msContextName = $moObject->getName();
    }
    
    // se existir, pega o nome do procurador
    $msSolicitorName = '';
    $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    if ($moSession->getSolicitor())
      $msSolicitorName = $moSession->getSolicitorName();
    
    // monta mensagem de quem foi o respons�vel pela gera��o do log
    $msResponsibleMsg = '';
    if ($msSolicitorName)
      $msResponsibleMsg = ' ' . $msSolicitorName . ' ' . FWDLanguage::getPHPStringValue('ad_on_behalf_of', 'em nome de') . ' ' . $msUserName;
    else 
      $msResponsibleMsg = ' ' . $msUserName;
    
    if(in_array($this->ciContextType,array(CONTEXT_CONTROL_BEST_PRACTICE,
                                           CONTEXT_BEST_PRACTICE_EVENT,
                                           CONTEXT_BEST_PRACTICE_STANDARD,
                                           CONTEXT_PROCESS_ASSET,
                                           CONTEXT_RISK_CONTROL,
                                           CONTEXT_DOCUMENT_INSTANCE,
                                           CONTEXT_DOCUMENT_INSTANCE_WITH_CONTENT
                                          ))){
     // $msContextName = FWDWebLib::convertToISO($msContextName);
    }else{
     // $msContextName = FWDWebLib::convertToISO($msContextName,true);
    }

    if(mb_strlen($msContextName, mb_detect_encoding($msContextName,"UTF-8,ISO-8859-1"))>255){
    	$msContextName = ISMSLib::truncateString($msContextName,253);
    }
    
    // insere na tabela de log
    $moLog = new ISMSAuditLog();
    $moLog->setFieldValue('log_date', ISMSLib::ISMSTime());
    $moLog->setFieldValue('log_user', $msUserName, false);
    $moLog->setFieldValue('log_action', $piAction);
    
    $moLog->setFieldValue('log_element', $msContextName, false);
    
    $msLog = FWDLanguage::getPHPStringValue('ad_log_message', '%action% %label% <b>%context_name%</b> por %user_name%.');
    
    $msDescription = str_replace(array('%action%', '%label%', '%context_name%', '%user_name%'), array($moLog->getAction($piAction), $this->getLabel(), $msContextName, $msResponsibleMsg), $msLog);        
    $moLog->setFieldValue('log_description', $msDescription, false);   

    $moLog->insert();
  }
  
  
 /**
  * Retorna se o usu�rio logado tem permi��o para inserir um contexto.
  * 
  * <p>M�todo para retornar se o usu�rio tem permi��o para inserir um contexto no sistema.</p>
  * @access public
  * @return boolean Se o usu�ro tem permi��o do usu�rio inserir um contexto no sistema
  */
  abstract protected function userCanInsert();

 /**
  * Retorna se o usu�rio logado tem permi��o para editar o contexto.
  * 
  * <p>M�todo para retornar se o usu�rio tem permi��o para editar o contexto no sistema.</p>
  * @access protected
  * @return boolean Se o usu�ro tem permi��o de editar o contexto no sistema
  */
  abstract protected function userCanEdit($piContextId,$piUserResponsible = 0);


 /**
  * Retorna se o usu�rio logado tem permi��o para editar o contexto.
  * 
  * <p>M�todo para retornar se o usu�rio tem permi��o para editar o contexto no sistema.</p>
  * @access protected
  * @return boolean Se o usu�ro tem permi��o de editar o contexto no sistema
  */
  protected function userCanRead($piContextId,$piUserResponsible){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $miContextType = $this->ciContextType;
    if(!$this->contextIsOfType($piContextId,$miContextType)) return false;
    return true; //ToDo: verificar as ACLs do usu�rio
  }

 /**
  * Retorna se o usu�rio logado tem permi��o para excluir o contexto.
  * 
  * <p>M�todo para retornar se o usu�rio tem permi��o para excluir o contexto no sistema.</p>
  * @access protected
  * @return boolean Se o usu�ro tem permi��o de excluir o contexto no sistema
  */
  abstract protected function userCanDelete($piContextId);

  /**
  * Retorna se a instancia do contexto � do tipo passado por parametro.
  * 
  * <p>M�todo para retornar se a instancia do contexto referente ao id passado
  * � do mesmo tipo do tipo passado por par�metro.
  * </p>
  * @access protected
  * @param integer $piContextId Id do context
  * @param integer $piContextType Tipo do contexto
  * @return boolean Se o usu�ro tem permi��o de editar o contexto no sistema
  */
  protected function contextIsOfType($piContextId,$piContextType){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    if($piContextId){
      $moObjContext = new ISMSContextObject();
      $moObj = $moObjContext->getContextObjectByContextId($piContextId);
      if($moObj){
        if($moObj->fetchById($piContextId)){
          if($moObj->getContextType()==$piContextType){
            return true;
          }
        }
      }
    }
    return false;
  }
  
  /**
  * Retorna se o usu�rio pode executar a task.
  * 
  * <p>M�todo para retornar se o usu�rio pode executar a task 
  * </p>
  * @access protected
  * @param integer $piTaskId Id de tasks
  * @return boolean Se o usu�ro tem permi��o executar a task
  */
  protected function userCanExecuteTask($piTaskId){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    
    /**
     * ALTERADO TEMPORARIAMENTE PARA RETORNAR SEMPRE TRUE
   * Ao transferir o usuario, o novo usuario nao poderia executar as tasks transferidas.
     */
    return true;
    
    
    $mbReturn = false;
    $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    $moTask = new WKFTask();
    $moTask->fetchById($piTaskId);
    $miActivity = $moTask->getFieldValue('task_activity');
    $miContextId = $moTask->getFieldValue('task_context_id');
    $moContextObject = new ISMSContextObject();
    $moContext = $moContextObject->getContextObjectByContextId($miContextId);
    $moContext->fetchById($miContextId);
    
    if($moContext->getApprover()==$miUserId){
      $moContextObject2 = new ISMSContextObject();
      $moContextObject2->fetchById($miContextId);
      if(($moContextObject2->getFieldValue('context_state')==CONTEXT_STATE_PENDANT)|| ($moContextObject2->getFieldValue('context_state')==CONTEXT_STATE_COAPPROVED) ){
        $mbReturn = true;
      }
    }
    return $mbReturn;
  }
  
  

  /**
  * Retorna mensagem de erro -> usu�rio n�o pode executar a task.
  * 
  * <p>M�todo para retornar mensagem de erro -> usu�rio n�o pode executar a
  * task.
  * </p>
  * @access protected
  * @return string retorna mensagem de erro -> usu�rio n�o pode executar a task.
  */
  protected function getDeniedPermissionToExecuteTaskMessage(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return FWDLanguage::getPHPStringValue('st_denied_permission_to_execute','Voc� n�o tem permiss�o para executar a tarefa').' ';
  }
  
  /**
  * Retorna mensagem de erro -> usu�rio n�o pode inserir a instancia do contexto
  * 
  * <p>M�todo para retornar mensagem de erro -> usu�rio n�o pode inserir a
  * instancia do contexto.
  * </p>
  * @access protected
  * @return string retorna mensagem de erro -> usu�rio n�o pode inserir
  * instancia do contexto
  */
  protected function getDeniedPermissionToInsertMessage(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return FWDLanguage::getPHPStringValue('st_denied_permission_to_insert','Voc� n�o tem permiss�o para inserir um(a)').' '.strtolower($this->getLabel()).'.';
  }

  /**
  * Retorna mensagem de erro -> usu�rio n�o pode editar a instancia do contexto
  * 
  * <p>M�todo para retornar mensagem de erro -> usu�rio n�o pode editar a
  * instancia do contexto.
  * </p>
  * @access protected
  * @return string retorna mensagem de erro -> usu�rio n�o pode editar a
  * instancia do contexto
  */
  protected function getDeniedPermissionToEditMessage(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return FWDLanguage::getPHPStringValue('st_denied_permission_to_edit','Voc� n�o tem permiss�o para editar o(a)').' '.strtolower($this->getLabel()).'.';
  }

  /**
  * Retorna mensagem de erro -> usu�rio n�o pode deletar a instancia do contexto
  * 
  * <p>M�todo para retornar mensagem de erro -> usu�rio n�o pode deletar a
  * instancia do contexto.
  * </p>
  * @access protected
  * @return string retorna mensagem de erro -> usu�rio n�o pode deletar a
  * instancia do contexto
  */
  protected function getDeniedPermissionToDeleteMessage(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return FWDLanguage::getPHPStringValue('st_denied_permission_to_delete','Voc� n�o tem permiss�o para remover o(a)').' '.strtolower($this->getLabel()).'.';
  }

  /**
  * Retorna mensagem de erro -> usu�rio n�o pode ler a instancia do contexto
  * 
  * <p>M�todo para retornar mensagem de erro -> usu�rio n�o pode ler a instancia
  * do contexto.
  * </p>
  * @access protected
  * @return string retorna mensagem de erro -> usu�rio n�o pode ler a instancia
  * do contexto
  */
  protected function getDeniedPermissionToReadMessage(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return FWDLanguage::getPHPStringValue('st_denied_permission_to_read','Voc� n�o tem permiss�o para ver o(a)').' '.strtolower($this->getLabel()).'.';
  }

  /**
  * Retorna mensagem de erro -> usu�rio n�o pode aprovar a instancia do contexto
  * 
  * <p>M�todo para retornar mensagem de erro -> usu�rio n�o pode aprovar a
  * instancia do contexto.
  * </p>
  * @access protected
  * @return string retorna mensagem de erro -> usu�rio n�o pode aprovar a
  * instancia do contexto
  */
  protected function getDeniedPermissionToApproveMessage(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return FWDLanguage::getPHPStringValue('st_denied_permission_to_approve','Voc� n�o tem permiss�o para aprovar o(a)').' '.strtolower($this->getLabel()).'.';
  }

  /**
  * Dispara as fun��es testPermissionTo(alguma coisa).
  * 
  * <p>M�todo para dispara as fun��es testPermissionTo(alguma coisa).
  * </p>
  * @access public
  * @param string $psMethodName nome da fun��o chamada
  * @param array $paArguments argumentos a ser passado para a fun�ao
  */
  public function __call($psMethodName,$paArguments){
    if(substr($psMethodName,0,16)=='testPermissionTo'){
      $msAction = substr($psMethodName,16);
      $msTestMethod = "userCan$msAction";
      $msMessageMethod = "getDeniedPermissionTo{$msAction}Message";
      if(!method_exists($this,$msTestMethod)){
        trigger_error("Calling method '$psMethodName' without defining method '$msTestMethod'.",E_USER_ERROR);
      }elseif(!method_exists($this,$msMessageMethod)){
        trigger_error("Calling method '$psMethodName' without defining method '$msMessageMethod'.",E_USER_ERROR);
      }elseif(!call_user_func_array(array(&$this,$msTestMethod),$paArguments)){
        trigger_error(call_user_func(array(&$this,$msMessageMethod)), E_USER_ERROR);
      }
    }else{
      trigger_error("Method '$psMethodName' is not defined.",E_USER_ERROR);
    }
  }

  public function getSearchHandler(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $moHandler = new QuerySearchContext(FWDWebLib::getConnection());
    $moHandler->setType($this->getContextType());
    return $moHandler;
  }
  
  /**
  * Retorna o c�digo javascript espec�fico para a janela de visualiza��o.
  * 
  * <p>M�todo para retornar o c�digo javascript espec�fico para a janela de
  * visualiza��o.
  * </p>
  * @access public
  * @return string C�digo javascript espec�fico por contexto para a janela de
  * visualiza��o
  */
  public function getVisualizeJavascript() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return "";
  }

}
?>
