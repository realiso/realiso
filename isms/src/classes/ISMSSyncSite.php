<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

define('ACTION_INSERT', 1);
define('ACTION_UPDATE', 2);
define('ACTION_DELETE', 3);

/**
 * Classe ISMSSyncSiteLogin.
 */
class ISMSSyncSite {

	public function __construct(){}

	public function insertLogin($id, $login, $email){
		$this->sync($id, $login, $email, ACTION_INSERT);
	}

	public function updateLogin($id, $login, $email){
		$this->sync($id, $login, $email, ACTION_UPDATE);
	}

	public function deleteLogin($id, $login = "", $email = ""){
		$this->sync($id, $login, $email, ACTION_DELETE);
	}

	private function sync($id, $login, $email, $action){
		$instanceName = ISMSSaaS::getConfig(ISMSSaaS::LICENSE_CLIENT_NAME);		
		$licenseType = ISMSSaaS::getConfig(ISMSSaaS::LICENSE_TYPE);

		$data = array(
			'id' => $id,
			'email' => $email,
			'login' => $login,
			'instanceName' => $instanceName,
			'licenseType' => $licenseType,
			'action' => $action
		);

		$output = $this->encrypt(json_encode($data));
		
		$fields = "data=" . urlencode(base64_encode($output));

		try {
			$this->post($fields);
		} catch (Exception $e) {
			$this->stackUp($data);
		}
	}

	private function encrypt($input){
		$key = "+asd7f6a8711110xxz=";

		$module = mcrypt_module_open(MCRYPT_3DES, '', 'ecb', '');
		$iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($module), MCRYPT_RAND);
		mcrypt_generic_init($module, $key, $iv);

		$output = mcrypt_generic($module, $input);

		mcrypt_generic_deinit($module);
		mcrypt_module_close($module);

		return $output;
	}

	private function post($fields){
		$curl = curl_init("http://www.realiso.com/asaas/sync_login.php");

		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($curl, CURLOPT_HEADER, 0); // sem header no retorno
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 2);

		$response = curl_exec($curl);

		$httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		if($httpCode != 200) {
			throw new Exception('curl - resposta http invalida!');
		}

		if(curl_errno($curl))
			throw new Exception('curl - erro!');
		else
			debug($response);
	}

	private function stackUp($data){
		$db = new FWDDB(DB_POSTGRES, "newisms_admin", "saas", "Fgvt*98Tas3$", "localhost");
		$db->connect();

		$query = "insert into syncfail_user(idcontext, email, login, instancename, licensetype, action) values (" . 
	
		$data['id'] . ", '" .
		$data['email'] . "','" .
		$data['login'] . "','" .
		$data['instanceName'] . "','" .
		$data['licenseType'] . "', " .
		$data['action']
		. ");";



		$db->execute($query);
	}
}

function debug($str){
	file_put_contents('/tmp/debug_sync_site.txt', $str."\n\n--\n\n", FILE_APPEND);
}
?>
