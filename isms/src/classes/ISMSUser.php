<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

include_once $handlers_ref . "QueryUserIsDeletable.php";

/**
 * Classe ISMSUser.
 *
 * <p>Classe que representa a tabela de usu�rios.</p>
 * @package ISMS
 * @subpackage classes
 */
class ISMSUser extends ISMSContext {

  const LOGIN_ERROR_USER_BLOCKED=5301;
  const LOGIN_ERROR_CHANGE_PASSWORD=5302;
  const LOGIN_ERROR_PASSWORD_EXPIRED=5303;

 /**
  * Construtor.
  * 
  * <p>Construtor da classe ISMSUser.</p>
  * @access public 
  */
  public function __construct(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    parent::__construct("isms_user");

    $this->csAliasId = 'user_id';
    $this->ciContextType = CONTEXT_USER;
    /*utilizado pela lixeira para descobrir a depenc�ncia na hora de restaurar o contexto*/
    $this->csDependenceAliasId = 'user_profile_id';

    $this->coDataset->addFWDDBField(new FWDDBField("fkContext",             "user_id",                    DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("fkProfile",             "user_profile_id",            DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("sName",                 "user_name",                  DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField("sLogin",                "user_login",                 DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField("sPassword",             "user_password",              DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField("sEmail",                "user_email",                 DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField("nIP",                   "user_ip",                    DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("nLanguage",             "user_language",              DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("dLastLogin",            "user_last_login",            DB_DATETIME));
    $this->coDataset->addFWDDBField(new FWDDBField("nWrongLogonAttempts",   "user_wrong_logon_attempts",  DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("bIsBlocked",            "user_is_blocked",            DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("bMustChangePassword",   "user_must_change_password",  DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("sRequestPassword",      "user_request_password",      DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField("sSession",              "user_session_id",            DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField("bAnsweredSurvey",       "user_answered_survey",       DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("bShowHelp",             "show_help",                  DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("bisAD",                 "user_is_ad",                 DB_NUMBER));
    
    $this->caSearchableFields = array('user_name', 'user_login');
  }
  
 /**
  * Busca informa��es de um usu�rio atrav�s de seu login/senha.
  * 
  * <p>M�todo para buscar informa��es de um usu�rio atrav�s de login/senha.</p>
  * @access public 
  * @param string $psLogin Login 
  * @param string $psPassword Senha
  */ 
  public function fetchByLoginPass($psLogin, $psPassword) {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $pbLogin = false;

    $this->createFilter(FWDWebLib::ConvertToIso($psLogin,true), 'user_login');
    $this->createFilter(FWDWebLib::ConvertToIso($psPassword,true), 'user_password');
    $this->select();
    if ($this->fetch()) { //Login + Senha
      $pbLogin = true;
      $pbChangePassword = false;
    }
    if (!$pbLogin) {
      $this->removeFilters('user_login');
      $this->createFilter(FWDWebLib::ConvertToIso($psLogin,true), 'user_email');
      $this->select();
      if ($this->fetch()) { //Email + Senha
        $pbLogin = true;
        $pbChangePassword = false;
      }
    }
    if (!$pbLogin) {
      $this->removeFilters('user_password');
      $this->createFilter($psPassword,'user_request_password');
      $this->select();
      if ($this->fetch()) { //Email + Request
        $pbLogin = true;
        $pbChangePassword = true;
      }
    }
    if (!$pbLogin) {
      $this->removeFilters('user_login');
      $this->removeFilters('user_email');
      $this->createFilter(FWDWebLib::ConvertToIso($psLogin,true), 'user_login');
      $this->select();
      if ($this->fetch()) { //Login + Request
        $pbLogin = true;
        $pbChangePassword = true;
      }
    }
    if ($pbLogin) {
      if ($pbChangePassword) { //Logon com senha request        
        $this->getFieldByAlias('user_login')->setUpdatable(false);
        $this->getFieldByAlias('user_name')->setUpdatable(false);
        if ($psPassword) $this->setFieldValue('user_password',$psPassword);
        $this->setFieldValue('user_request_password',null);
        $this->setFieldValue('user_must_change_password', true);
        $this->update($this->getFieldValue('user_id'),false);
        return true;
      } else { //Logon com senha original
        $this->getFieldByAlias('user_login')->setUpdatable(false);
        $this->getFieldByAlias('user_name')->setUpdatable(false);
        $this->setFieldValue('user_request_password',null);
        $this->update($this->getFieldValue('user_id'),false);        
        return true;
      }
    } else {
      return false;
    }
  }
  
 /**
  * Busca informa��es do usu�rio se o login passar pelo AD.
  * 
  * <p>M�todo para buscar informa��es do usu�rio se o login passar pelo AD.</p>
  * @access public 
  * @param string $psLogin Login 
  * @param string $psPassword Senha
  */
  public function fetchByLoginActiveDirectory($psLogin, $psPassword) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
 
    $user = new ISMSUser();
    $user->createFilter(FWDWebLib::ConvertToIso($psLogin, true), 'user_login');
    $user->createFilter(true, 'user_is_ad');
    $user->select();
    
    if(!$user->fetch()){
      return false;
    }

    $ismsAd = new ISMSActiveDirectory();

    if($ismsAd->fetchByLoginPass($psLogin, $psPassword)){
      $this->createFilter(FWDWebLib::ConvertToIso($psLogin, true), 'user_login');
      $this->createFilter(true, 'user_is_ad');
      $this->select();
      $this->fetch();

      // mantemos atualizados nome de usu�rio e e-mail vindo do AD.
      $this->setFieldValue('user_email', $ismsAd->getMail());
      $this->setFieldValue('user_name', $ismsAd->getUserName());
      $this->setFieldValue('user_is_blocked', false);
      $this->update($this->getFieldValue('user_id'), false);  

      return true;      
    } else {
      $user->setFieldValue('user_is_blocked', true);
      $user->update($user->getFieldValue('user_id'), false); 

      return false;
    }
  }

 /**
  * Busca informa��es de um usu�rio atrav�s de seu login.
  * 
  * <p>M�todo para buscar informa��es de um usu�rio atrav�s de login.</p>
  * @access public 
  * @param string $psLogin Login
  */ 
  public function fetchByLogin($psLogin) {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $this->createFilter($psLogin, 'user_email');
    $this->select();
    if (!$this->fetch()) {
      $this->removeFilters('user_email');
      $this->createFilter($psLogin, 'user_login');
      $this->select();
      return $this->fetch();
    } else return true;
  }
  
 /**
  * Busca lista de usu�rios que o usu�rio � procurador.
  * 
  * <p>M�todo para buscar a lista de usu�rios que o usu�rio � procurador.</p>
  * @access public
  * 
  * @return array Ids dos usu�rios representados
  */
  public function getRepresentedUsersList() {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $moSolicitor = new ISMSSolicitor();
    $moSolicitor->createFilter($this->getFieldValue($this->csAliasId), 'solicitor_id');
    $moSolicitor->createFilter(ISMSLib::ISMSTime(), 'date_begin', '<=');
    $moSolicitor->createFilter(ISMSLib::ISMSTime(), 'date_finish', '>=');
    $moSolicitor->select();
    $maUsers = array();
    while ($moSolicitor->fetch())
      $maUsers[] = $moSolicitor->getFieldValue('user_id');
    return $maUsers;
  }
  
 /**
  * Verifica se o usu�rio est� sendo representado por outro.
  * 
  * <p>M�todo para verificar se o usu�rio est� sendo representado por outro.</p>
  * @access public
  * 
  * @return boolean Verdadeiro se o usu�rio possuir um procurador
  */
  public function hasSolicitor() {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $moSolicitor = new ISMSSolicitor();
    $moSolicitor->createFilter($this->getFieldValue($this->csAliasId), 'user_id');
    $moSolicitor->createFilter(ISMSLib::ISMSTime(), 'date_begin', '<=');
    $moSolicitor->createFilter(ISMSLib::ISMSTime(), 'date_finish', '>=');
    $moSolicitor->select();
    if ($moSolicitor->fetch()) return $moSolicitor->getFieldValue('solicitor_id');
    else{
        $moSolicitor = new ISMSSolicitor();
        $moSolicitor->createFilter($this->getFieldValue($this->csAliasId), 'user_id');
        $moSolicitor->select();
        $moSolicitor->delete();
        return false;
    } 
  }
  
 /**
  * Retorna o identificador do procurador.
  * 
  * <p>M�todo para retornar o identificador do procurador.</p>
  * @access public
  * 
  * @return string Id do procurador
  */
  public function getSolicitor() {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return $this->hasSolicitor();
  }
  
 /**
  * Retorna o nome do contexto.
  * 
  * <p>M�todo para retornar o nome do contexto.</p>
  * @access public 
  * @return string Nome do contexto
  */
  public function getName() {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return $this->getFieldValue('user_name');
  }
  
 /**
  * Retorna o �cone do contexto.
  *
  * <p>M�todo para retornar o �cone do contexto.</p>
  * @access public
  * @param integer $piContextId Id do contexto
  * @return string Nome do �cone
  */
  public function getIcon($piContextId = 0){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return 'icon-user.gif';
  }

 /**
  * Retorna o label do contexto.
  * 
  * <p>M�todo para retornar o label do contexto.</p>
  * @access public 
  * @return string Label do contexto
  */
  public function getLabel() {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return FWDLanguage::getPHPStringValue('mx_user', "Usu�rio");
  }
  
 /**
  * Indica se o contexto � delet�vel ou n�o.
  * 
  * <p>M�todo que indica se o contexto � delet�vel ou n�o.</p>
  * @access public
  * @param integer $piContextId id do contexto
  */ 
  public function isDeletable($piContextId) {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $moQuery = new QueryUserIsDeletable(FWDWebLib::getConnection());
    $moQuery->setUserId($piContextId);
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    return $moQuery->getIsDeletable();
  }
  
 /**
  * Exibe uma popup caso n�o seja poss�vel remover um contexto.
  * 
  * <p>M�todo que exibe uma popup caso n�o seja poss�vel remover um contexto.</p>
  * @access public
  * @param integer $piContextId id do contexto
  */ 
  public function showDeleteError($piContextId) {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $msTitle = FWDLanguage::getPHPStringValue('tt_user_remove_error','Erro ao remover Usu�rio');
    $msMessage = FWDLanguage::getPHPStringValue('st_user_remove_error',"N�o foi poss�vel remover o usu�rio <b>%user_name%</b>: O usu�rio � respons�vel por um ou mais elementos do sistema ou � um usu�rio especial.");
    $moUser = new ISMSUser();
    $moUser->fetchById($piContextId);
    $msUserName = $moUser->getFieldValue('user_name');
    $msMessage = str_replace("%user_name%",$msUserName,$msMessage);
    ISMSLib::openOk($msTitle,$msMessage,"",60);
  }
  
 /**
  * Retorna o caminho do contexto.
  * 
  * <p>M�todo para retornar o caminho do contexto.</p>
  * @access public 
  * @return string Caminho do contexto
  */ 
  public function getSystemPath() {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    /*
     * No caso do usu�rio n�o � necess�rio passar para a fun��o isms_redirect_to_mode(...)
     * o seu identificador pois ele apenas deve redirecionar para a aba de usu�rios. Caso fosse
     * passado o id, toda vez que um usu�rio diferente fosse clicado, a aba de usu�rios seria
     * recarregada desnecessariamente. Desse modo, ela s� � carregada a primeira vez.
     */
    $msRMPath = "<a href='javascript:isms_redirect_to_mode(" . ADMIN_MODE . ",0,0)'>".FWDLanguage::getPHPStringValue('st_admim_module_name','Admin')."</a>";
    $msContextPath = ISMSLib::getIconCode('icon-user.gif',-2,-4) . "<a href='javascript:isms_redirect_to_mode(" . ADMIN_MODE . "," . $this->ciContextType . ",0)'>" . $this->getName() . "</a>";
    return $msRMPath . "&nbsp;".ISMSLib::getIconCode('icon-arrow-right.gif',-2,0).'&nbsp;' . $msContextPath;
  }
  
  
 /**
  * Retorna se o usu�rio logado tem permi��o para inserir o contexto.
  * 
  * <p>M�todo para retornar se o usu�rio tem permi��o para inserir o contexto no sistema.</p>
  * @access public
  * @return boolean Se o usu�ro tem permi��o para inserir o contexto no sistema
  */
  protected function userCanInsert(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $miContextType = $this->ciContextType;
    //contexto deve ser tratado, assim assume-se que a principio o usu�rio n�o pode inserir tal contexto
    $mbReturn = false;
    $miCount = 0;

    $maPermissions = array( 'A.U.3' );
    //acls negadas o usu�rio logado no sistema
    $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
    foreach($maACLs as $miKey => $miValue){
      $maACLs[$miValue]=$miKey;
    }
    //acl1 AND acl2
    $mbAuxPermission = true;
    foreach($maPermissions as $msValue){
      if(isset($maACLs[$msValue])){
        $mbAuxPermission = false;
      }
      $miCount++;
    }

    //caso exista pelo menos 1 acl nas permi��es do contexto e estas permi��es n�o est�o nas permi��es negadas do usu�rio
    if(($mbAuxPermission)&&($miCount)){
      $mbReturn = $mbAuxPermission;
    }
    return $mbReturn;
  }

 /**
  * Retorna se o usu�rio logado tem permi��o para editar o contexto.
  * 
  * <p>M�todo para retornar se o usu�rio tem permi��o para editar o contexto no sistema.</p>
  * @access protected
  * @return boolean Se o usu�ro tem permi��o de editar o contexto no sistema
  */
  protected function userCanEdit($piContextId,$piUserResponsible = 0){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    if(!$this->contextIsOfType($piContextId,$this->ciContextType)) return false;
    //contexto deve ser tratado, assim assume-se que a principio o usu�rio n�o pode editar esse contexto
    $mbReturn = false;
    $maPermissions = array('A.U.1');
    //acls negadas do usu�rio logado no sistema
    $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
    foreach($maACLs as $miKey => $miValue){
      $maACLs[$miValue]=$miKey;
    }
    $miCount = 0;
    //acl1 AND acl2
    $mbAuxPermission = true;
    foreach($maPermissions as $msValue){
      if(isset($maACLs[$msValue])){
        $mbAuxPermission = false;
      }
      $miCount++;
    }
    //caso exista pelo menos 1 acl nas permi��es do contexto e estas permi��es n�o est�o nas permi��es negadas do usu�rio
    if(($mbAuxPermission)&&($miCount)){
      $mbReturn = $mbAuxPermission;
      }
    return $mbReturn;
  }
 
 /**
  * Retorna se o usu�rio logado tem permi��o para excluir o contexto.
  * 
  * <p>M�todo para retornar se o usu�rio tem permi��o para excluir o contexto no sistema.</p>
  * @access protected
  * @return boolean Se o usu�ro tem permi��o de excluir o contexto no sistema
  */
  protected function userCanDelete($piContextId){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    //tester esquemas de respons�vel aq nesse teste
    $miContextType = $this->ciContextType;
    if(!$this->contextIsOfType($piContextId,$miContextType)) return false;
    //contexto deve ser tratado, assim assume-se que a principio o usu�rio n�o pode inserir tal contexto
    $mbReturn = false;
    $maPermissions = array( 'A.U.2' );

    //acls negadas do usu�rio logado no sistema
    $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
    foreach($maACLs as $miKey => $miValue){
      $maACLs[$miValue]=$miKey;
    }
      $miCount = 0;
    //acl1 AND acl2
    $mbAuxPermission = true;
    foreach($maPermissions as $msValue){
      if(isset($maACLs[$msValue])){
        $mbAuxPermission = false;
      }
      $miCount++;
    }
    //caso exista pelo menos 1 acl nas permi��es do contexto e estas permi��es n�o est�o nas permi��es negadas do usu�rio
    if(($mbAuxPermission)&&($miCount)){
      $mbReturn = $mbAuxPermission;
    }
    return $mbReturn;
  }
  
  /**
   * Retorna valor booleano indicando se o usu�rio est� bloqueado.
   * 
   * <p>Retorna valor booleano indicando se o usu�rio est� bloqueado.</p>
   * @access public
   * @return boolean Estado bloqueado ou n�o
   */  
  public function isBlocked() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return $this->getFieldValue('user_is_blocked')?true:false;
  }

  /**
   * Retorna valor booleano indicando se o usu�rio � importado do Active Directory.
   * 
   * <p>Retorna valor booleano indicando se o usu�rio � importado do Active Directory.</p>
   * @access public
   * @return boolean Estado importado ou n�o
   */  
  public function isAD() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return $this->getFieldValue('user_is_ad')?true:false;
  }  
  
  /**
   * Acrescenta 1 no campos de tentativas de logon inv�lidas.
   * 
   * <p>Acrescenta 1 no campos de tentativas de logon inv�lidas.
   * Se o n�mero ultrapassar o estipulado nas configura��es do sistema,
   * bloqueia o usu�rio.</p>
   * @access public
   */
  public function addWrongLogonAttempt() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $miUserProfile = $this->getFieldValue('user_profile_id');
    $moProfile = new ISMSProfile();
    $moProfile->fetchById($miUserProfile);
    $miUserProfileId = $moProfile->getFieldValue('profile_identifier');
    
    $miWrongLogonAttempts = $this->getFieldValue('user_wrong_logon_attempts') + 1;
    $this->setFieldValue('user_wrong_logon_attempts',$miWrongLogonAttempts);
    if ($miWrongLogonAttempts >= ISMSLib::getConfigById(PASSWORD_BLOCK_TRIES) && $miUserProfileId != PROFILE_SECURITY_OFFICER) {
      $this->setFieldValue('user_is_blocked',true);
    }
    parent::update($this->getFieldValue('user_id'),false);
  }
  
  /**
   * Verifica se o usu�rio precisa trocar a senha no primeiro login.
   * 
   * <p>Verifica se o usu�rio precisa trocar a senha no primeiro login.</p>
   * @access public
   * @return True/False
   */
  public function changePasswordOnLogin() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    // Se for usu�rio AD n�o precisa mudar senha.
    if($this->isAD())
      return false;

    return $this->getFieldValue('user_must_change_password')?true:false;
  }
  
  /**
  * Retorna o c�digo HTML do icone do contexto.
  * 
  * <p>M�todo para retornar o c�digo HTML do contexto</p>
  * @access public
  * @param integer $piId Id do contexto
  * @param integer $pbIsFetched id do contexto
  * @return string contendo o c�digo HTML do icone do contexto
  */ 
  public function getIconCode($piId, $pbIsFetched = false){
  	return ISMSLib::getIconCode('icon-user.gif', -2, 7);
  }
  
  /**
   * Retorna o motivo pelo qual o usu�rio n�o pode efetuar login no sistema.
   * 
   * <p>Retorna o motivo pelo qual o usu�rio n�o pode efetuar login no sistema.</p>
   * @access public
   * @return integer Indicador de erro do login
   */
  public function getLoginError() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    if ($this->isBlocked()) return self::LOGIN_ERROR_USER_BLOCKED;
    if ($this->changePasswordOnLogin()) return self::LOGIN_ERROR_CHANGE_PASSWORD;
    $moUserPasswordHistory = new ISMSUserPasswordHistory();
    if ($moUserPasswordHistory->isPasswordExpired($this->getFieldValue('user_id'))) return self::LOGIN_ERROR_PASSWORD_EXPIRED;
    return 0;
  }
  
}
?>