<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSContextObject.
 *
 * <p>Classe que representa um objeto de contexto.</p>
 * @package ISMS
 * @subpackage classes
 */
class ISMSContextObject extends ISMSTable {
	
 /**
  * Construtor.
  * 
  * <p>Construtor da classe ISMSContextObject.</p>
  * @access public 
  */
  public function __construct(){		
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
  	parent::__construct("isms_context");  	
  	
  	$this->csAliasId = 'context_id';
  	
  	$this->coDataset->addFWDDBField(new FWDDBField("pkContext",		"context_id",		DB_NUMBER));  	
  	$this->coDataset->addFWDDBField(new FWDDBField("nType",			"context_type",		DB_NUMBER));
  	$this->coDataset->addFWDDBField(new FWDDBField("nState",		"context_state",	DB_NUMBER));  	
  }
  
 /**
  * Retorna um objeto de um dado contexto.
  * 
  * <p>M�todo para retornar um objeto, dado um contexto.</p>
  * @access public
  * @param integer $piContextType Tipo do contexto
  * @return Object Objeto do contexto
  */
  public static function getContextObject($piContextType = 0,$pbExecByTrash = false) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return ISMSLib::getContextObject($piContextType);
  }
 
 /**
  * Retorna um objeto de um dado contexto atrav�s de seu id.
  * 
  * <p>M�todo para retornar um objeto de um dado contexto atrav�s de seu id.</p>
  * @access public
  * @param integer $piContextId Id do contexto
  * @return Object Objeto do contexto
  */
  public function getContextObjectByContextId($piContextId,$pbExecByTrash = false) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $this->removeFilters('context_id');
  	$this->createFilter($piContextId, 'context_id');
  	$this->select();
  	$this->fetch();
  	return $this->getContextObject($this->getFieldValue('context_type'),$pbExecByTrash);
  }
  
 /**
  * Retorna o estado do contexto em formato string.
  * 
  * <p>M�todo para retornar o estado do contexto em formato string.</p>
  * @access public
  * @param integer $piContextState Id do estado
  * @return string Estado no forma de string
  */
  public static function getContextStateAsString($piContextState) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    switch($piContextState){
      case CONTEXT_STATE_NONE:{
        return "";
      }case CONTEXT_STATE_PENDANT:{
        return FWDLanguage::getPHPStringValue('mx_pendant', "Pendente");
      }case CONTEXT_STATE_APPROVED:{
        return FWDLanguage::getPHPStringValue('mx_approved', "Aprovado(a)");
      }case CONTEXT_STATE_DENIED:{
        return FWDLanguage::getPHPStringValue('mx_denied', "Negado(a)");
      }case CONTEXT_STATE_COAPPROVED:{
        return FWDLanguage::getPHPStringValue('mx_co_approved', "Co-aprovado(a)");
      }case CONTEXT_STATE_DELETED:{
        return FWDLanguage::getPHPStringValue('mx_deleted', "Deletado(a)");
      }case CONTEXT_STATE_DOC_DEVELOPING:{
        return FWDLanguage::getPHPStringValue('mx_under_development', "Em Desenvolvimento");
      }case CONTEXT_STATE_DOC_APPROVED:{
        return FWDLanguage::getPHPStringValue('mx_published', "Publicado");
      }case CONTEXT_STATE_DOC_PENDANT:{
        return FWDLanguage::getPHPStringValue('mx_approval', "Em Aprova��o");
      }case CONTEXT_STATE_DOC_REVISION:{
        return FWDLanguage::getPHPStringValue('mx_revision', "Em Revis�o");
      }case CONTEXT_STATE_DOC_OBSOLETE:{
        return FWDLanguage::getPHPStringValue('mx_obsolete', "Obsoleto");
      }case CONTEXT_STATE_DOC_TO_BE_PUBLISHED:{
        return FWDLanguage::getPHPStringValue('mx_to_be_published', "A Publicar");
      }case CONTEXT_STATE_CI_SENT:{
        return FWDLanguage::getPHPStringValue('mx_sent', "Emitido");
      }case CONTEXT_STATE_CI_OPEN:{
        return FWDLanguage::getPHPStringValue('mx_open', "Aberto");
      }case CONTEXT_STATE_CI_DIRECTED:{
        return FWDLanguage::getPHPStringValue('mx_directed', "Encaminhado");
      }case CONTEXT_STATE_CI_NC_PENDANT:{
        return FWDLanguage::getPHPStringValue('mx_nc_treatment_pendant', "Tratamento de N�o Conformidade Pendente");
      }case CONTEXT_STATE_CI_AP_PENDANT:{
        return FWDLanguage::getPHPStringValue('mx_ap_acceptance_pendant', "Aceita��o do Plano de A��o Pendente");
      }case CONTEXT_STATE_CI_WAITING_DEADLINE:{
        return FWDLanguage::getPHPStringValue('mx_waiting_deadline', "Previs�o");
      }case CONTEXT_STATE_CI_WAITING_CONCLUSION:{
        return FWDLanguage::getPHPStringValue('mx_waiting_conclusion', "Aguardando Conclus�o");
      }case CONTEXT_STATE_CI_FINISHED: {
        return FWDLanguage::getPHPStringValue('mx_finished', "Conclu�do");
      }case CONTEXT_STATE_CI_CLOSED:{
        return FWDLanguage::getPHPStringValue('mx_closed', "Fechado");
      }case CONTEXT_STATE_INC_OPEN:{
        return FWDLanguage::getPHPStringValue('mx_open', "Aberto");
      }case CONTEXT_STATE_INC_DIRECTED:{
        return FWDLanguage::getPHPStringValue('mx_directed', "Encaminhado");
      }case CONTEXT_STATE_INC_PENDANT_DISPOSAL:{
        return FWDLanguage::getPHPStringValue('mx_pendant_solution', "Pendente - Solu��o");
      }case CONTEXT_STATE_INC_WAITING_SOLUTION:{
        return FWDLanguage::getPHPStringValue('mx_waiting_solution', "Aguardando Solu��o");
      }case CONTEXT_STATE_INC_PENDANT_SOLUTION:{
        return FWDLanguage::getPHPStringValue('mx_pendant_solved', "Pendente - Solucionado");
      }case CONTEXT_STATE_INC_SOLVED:{
        return FWDLanguage::getPHPStringValue('mx_solved', "Solucionado");
      }case CONTEXT_STATE_AP_PENDANT:{
        return FWDLanguage::getPHPStringValue('mx_pendant', "Pendente");
      }case CONTEXT_STATE_AP_WAITING_CONCLUSION:{
        return FWDLanguage::getPHPStringValue('mx_waiting_conclusion', "Aguardando Conclus�o");
      }case CONTEXT_STATE_AP_WAITING_MEASUREMENT:{
        return FWDLanguage::getPHPStringValue('mx_waiting_measurement', "Aguardando Medi��o");
      }case CONTEXT_STATE_AP_MEASURED:{
        return FWDLanguage::getPHPStringValue('mx_measured', "Medido");
      }
    }
  }
}
?>