<?
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

define('TIMEOUT_LIMIT', 3600); // 1 hora - Piero

/**
 * Classe ISMSSession.
 *
 * Classe que representa uma sess�o do ISMS.
 *
 * @package ISMS
 * @subpackage classes
 */
class ISMSSession extends FWDSession {
 /**
  * Array de tags
  * @var array
  * @access protected
  */
  protected $caUserDeniedACLs = array();

 /**
  * Id do usu�rio que est� logado como procurador
  * @var array
  * @access protected
  */
  protected $ciSolicitorId = 0;

 /**
  * IP do �ltimo acesso
  * @var string
  * @access protected
  */
  protected $csLastIP = "";

 /**
  * Timestamp do �ltimo acesso
  * @var integer
  * @access protected
  */
  protected $ciLastAccessDate = 0;

 /**
  * Configura��es do ISMS
  * @var ISMSConfig
  * @access protected
  */
  protected $coConfig = NULL;

 /**
  * Modo atual do sistema (ADMIN_MODE ou MANAGER_MODE)
  * @var integer
  * @access protected
  */
  protected $ciMode = MANAGER_MODE;

  /**
   * Atributo que indica se o sistema est� em per�odo de ativa��o
   * @var boolean
   * @access private
   */
  protected $cbActivationPeriod = false;

 /**
  * Construtor.
  *
  * <p>Construtor da classe ISMSSession.</p>
  *
  * @access public
  */
  public function __construct($psId){
    $maParameters = func_get_args();
    $this->setTimeout(TIMEOUT_LIMIT);
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    parent::__construct($psId);
  }

 /**
  * M�todo para autenticar o usu�rio
  *
  * <p>M�todo para autenticar o usu�rio.</p>
  * @access public
  * @param string $psUser Login do usu�rio
  * @param string $psPassword Senha do usu�rio
  * @return boolean Indica se o usu�rio foi autenticado ou n�o
  */
  public function login($psUser,$psPassword = "") {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
  	$moUser = new ISMSUser();

  	if ($psPassword){

      $moConfig = new ISMSConfig();
      
      if($moConfig->getConfigFromDB(AD_SERVER)){

        if(!$moUser->fetchByLoginActiveDirectory($psUser, $psPassword)){
          $moUser->fetchByLoginPass($psUser, md5($psPassword));
        }
        
      } else {
        $moUser->fetchByLoginPass($psUser, md5($psPassword));
      }

  	} else {
      $moUser->fetchByLogin($psUser);
    }

    $this->deleteAttribute("loginErrorId");
  	if ($miUserId = $moUser->getFieldValue('user_id')) {
      if ($miErrorId = $moUser->getLoginError()) {
        if (!$this->attributeExists("loginErrorId"))
          $this->addAttribute("loginErrorId");
        $this->setAttrLoginErrorId($miErrorId);
        return false;
      }

      $moLicense = new ISMSLicense();

  		$moProfile = new ISMSProfile();
  		$moProfile->setFieldValue('profile_id', $moUser->getFieldValue('user_profile_id'));
			$this->caUserProfile = $moProfile->getDeniedAcls();

      $this->setUserId($miUserId);
      if ($miIP = $moUser->getFieldValue('user_ip'))
        $this->csLastIP = long2ip($miIP);
      $this->ciLastAccessDate = $moUser->getFieldValue('user_last_login');

			if(getenv("REMOTE_ADDR")) {
				$miIP = getenv("REMOTE_ADDR");
			}

			$miIP = ip2long($miIP);
      
      $moUpdateUser = new ISMSUser();
			$moUpdateUser->setFieldValue('user_ip', $miIP);
			$moUpdateUser->setFieldValue('user_last_login', ISMSLib::ISMSTime());
      $moUpdateUser->setFieldValue('user_wrong_logon_attempts', 0);
      $moUpdateUser->setFieldValue('user_session_id', 'sess_'.session_id());
			$moUpdateUser->update($miUserId, false);

  		// loga evento de login
      if( ISMSLib::getGestorPermission() || ISMSLib::getAdminPermission()){
        $this->createLog(ADT_ACTION_LOGIN, $moUser);
      }else{
        $this->createLog(ADT_ACTION_LOGIN_FAILED, $moUser);
        $this->logout();
        return false;
      }

      $miUserId = $this->getUserId(true);
      $moUser = new ISMSUser();
      $mbUserIsActive = false;
      if($moUser->fetchById($miUserId)){
        if($moUser->getContextState()!=CONTEXT_STATE_DELETED){
          $mbUserIsActive = true;
        }
      }

      if($mbUserIsActive){
       /*
        * Esse trecho pega a l�ngua do usu�rio e carrega as respectivas tradu��es.
        * O 'IF' � necess�rio pois na tela de login ainda n�o existe identificador
        * de usu�rio.
        */
        if($moUser->getFieldValue('user_language')){
          FWDLanguage::selectLanguage($moUser->getFieldValue('user_language'));
        }
      }

      // Cria tarefas agendadas e envia alertas de tarefas atrasadas.
      WKFSchedule::checkSchedules();

      // Envia os alertas referentes � implementa��o dos controles
      $moControlSchedule = new ControlSchedule($this->getUserId());
      $moControlSchedule->checkImplementation();

      // For�a um update do isActive dos controles com implementa��o ou tarefas pendentes
      $moControlSchedule->updateControlsIsActive();

      if(ISMSLib::hasModule(INCIDENT_MODE)){
        // Cria as n�o-conformidades que forem necess�rias
        $moControlSchedule->checkNonConformities();
      }else{
        // Deleta as sementes de n�o-conformidades
        $moNonConformitySeed = new CINonConformitySeed();
        $moNonConformitySeed->createFilter(0,'nc_seed_deactivation_reason','>');
        $moNonConformitySeed->delete();
      }

      // Envia os alertas de deadline dos documentos
      if(ISMSLib::hasModule(POLICY_MODE)){
        $moDocumentSchedule = new DocumentSchedule($this->getUserId());
        $moDocumentSchedule->checkDeadline();
      }

      // Executa o schedule da Gest�o de Incidente
      if(ISMSLib::hasModule(INCIDENT_MODE)){
        $moImprovementSchedule = new ImprovementSchedule($this->getUserId());
        $moImprovementSchedule->checkSchedule();
      }

      // Executa o schedule de risco (c�lculo autom�tico de probabilidade)
      if(ISMSLib::hasModule(INCIDENT_MODE)){
        $moRiskSchedule = new CIRiskSchedule();
        $moRiskSchedule->checkSchedule();
      }

      return true;
    }else{
      return false;
    }
  }

 /**
  * M�todo para deslogar o usu�rio do sistema
  *
  * <p>M�todo para deslogar o usu�rio do sistema.</p>
  * @access public
  * @return boolean Indica se o usu�rio foi deslogado ou n�o
  */
  public function logout() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
  	$moUser = new ISMSUser();
  	$moUser->fetchById($this->getUserId());
		// loga evento de logout
		$this->createLog(ADT_ACTION_LOGOUT, $moUser);
    
    if ($this->getUserId() && substr($moUser->getFieldValue('user_session_id'),5)==session_id()) {
      $moUpdateUser = new ISMSUser();
      $moUpdateUser->setFieldValue('user_session_id','');
      $moUpdateUser->update($this->getUserId());
    }
		parent::logout();

		return true;
  }


 /**
  * Retorna o usu�rio que est� logado no sitema.
  *
  * <p>M�todo para retornar o usu�rio que est� logado no sitema.
  * Se o par�metro for verdadeiro e o usu�rio logado est� sendo representado
  * por outro usu�rio, retorna o id do usu�rio procurador.</p>
  * @access public
  * @param boolean $pbGetSolicitorIfSet Retorna o id do procurador se este existir
  */
  public function getUserId($pbGetSolicitorIfSet = false) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
  	if ($pbGetSolicitorIfSet && $this->ciSolicitorId) return $this->ciSolicitorId;
  	else return parent::getUserId();
  }

 /**
  * Retorna o nome do usu�rio que est� logado no sitema.
  *
  * <p>M�todo para retornar o nome do usu�rio que est� logado no sitema.
  * Se o par�metro for verdadeiro e o usu�rio logado est� sendo representado
  * por outro usu�rio, retorna o nome do usu�rio procurador tamb�m.</p>
  * @access public
  * @param boolean $pbGetSolicitorNameIfSet Retorna o nome do procurador se este existir
  */
  public function getUserName($pbGetSolicitorNameIfSet = false) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $miUserId = $this->getUserId($pbGetSolicitorNameIfSet);
    $moUser = new ISMSUser();
    $moUser->fetchById($miUserId);
    $msName = $moUser->getFieldValue('user_name');

    if ($this->getSolicitor()) {
    	$miUserId = $this->getUserId();
    	$moUser = new ISMSUser();
    	$moUser->fetchById($miUserId);
    	$msName .= " (" . $moUser->getFieldValue('user_name') . ")";
    }
    return $msName;
  }

 /**
  * Seta as tags dos elementos que o usu�rio n�o possui acesso.
  *
  * <p>M�todo para setar as tags dos elementos que o usu�rio n�o possui acesso.</p>
  * @access public
  * @param string $paUserDeniedACLs Array de tags
  * @return boolean Indica se o usu�rio foi autenticado ou n�o
  */
  public function setUserDeniedACLs($paUserDeniedACLs) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
  	$this->caUserDeniedACLs = $paUserDeniedACLs;
  }

 /**
  * Retorna as tags dos elementos que o usu�rio n�o possui acesso.
  *
  * <p>M�todo para retornar as tags dos elementos que o usu�rio n�o possui acesso.</p>
  * @access public
  * @return string Array de tags
  */
  public function getUserDeniedACLs() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
  	return $this->caUserDeniedACLs;
  }

 /**
  * Seta o id do usu�rio que est� logado como procurador.
  *
  * <p>M�todo para setar o id do usu�rio que est� logado como procurador.</p>
  * @access public
  * @param integer $piSolicitorId Id do procurador
  */
  public function setSolicitor($piSolicitorId) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
  	$this->ciSolicitorId = $piSolicitorId;
  }

 /**
  * Retorna o id do usu�rio que est� logado como procurador.
  *
  * <p>M�todo para retornar o id do usu�rio que est� logado como procurador.</p>
  * @access public
  * @return integer Id do procurador
  */
  public function getSolicitor() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
  	return $this->ciSolicitorId;
  }

 /**
  * Retorna o nome do usu�rio que est� logado como procurador.
  *
  * <p>M�todo para retornar o nome do usu�rio que est� logado como procurador.</p>
  * @access public
  * @return string Nome do procurador
  */
  public function getSolicitorName() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
  	if ($this->ciSolicitorId) {
	  	$moUser = new ISMSUser();
	  	$moUser->fetchById($this->ciSolicitorId);
	  	return $moUser->getFieldValue('user_name');
  	}
  	else trigger_error('There is no solicitor in this session!', E_USER_WARNING);
  }

 /**
  * Seta uma determinada configura��o.
  *
  * <p>M�todo para setar uma determinada configura��o.</p>
  * @access public
  * @param integer $piConfigId Id da configura��o
  * @param string $psConfigValue Valor da configura��o
  */
  public function setConfig($piConfigId, $psConfigValue) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
  	$this->coConfig->setConfig($piConfigId, $psConfigValue);
  }

 /**
  * Retorna o valor de uma determinada configura��o.
  *
  * <p>M�todo para retornar o valor de uma determinada configura��o.</p>
  * @access public
  * @param integer $piConfigId Id da configura��o
  * @return string Valor da configura��o
  */
  public function getConfig($piConfigId) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
  	return $this->coConfig->getConfig($piConfigId);
  }

 /**
  * Seta o modo atual do sistema.
  *
  * <p>M�todo para setar o modo atual do sistema.</p>
  * @access public
  * @param integer $piMode Modo
  */
  public function setMode($piMode) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $this->ciMode = $piMode;
  }

 /**
  * Retorna o modo atual do sistema.
  *
  * <p>M�todo para retornar o modo atual do sistema.</p>
  * @access public
  * @return integer Modo atual do sistema
  */
  public function getMode() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return $this->ciMode;
  }

 /**
  * Verifica se possui determinado m�dulo.
  *
  * <p>M�todo para verificar se possui determinado m�dulo.</p>
  * @access public
  * @param integer $piAction Id da a��o
  * @return boolean Verdadeiro se possui o m�dulo. Falso caso contr�rio.
  */
  public function hasModule($piModule) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $moLicense = new ISMSLicense();
    if(in_array($piModule, $moLicense->getModules())){
      $maDeniedACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
      switch($piModule){
        case RISK_MANAGEMENT_MODE:
          if(in_array('M.RM', $maDeniedACLs))
            return false;
          else
            return true;
        break;
        case POLICY_MODE:
          if(in_array('M.PM', $maDeniedACLs))
            return false;
          else
            return true;
        break;
        case INCIDENT_MODE:
          if(in_array('M.CI',$maDeniedACLs))
            return false;
          else
            return true;
        default:
          return true;
        break;
      }
    }else{
      return false;
    }
  }

  /**
  * Registra no log a entrada do usu�rio no sistema.
  *
  * <p>M�todo para registrar no log a entrada do usu�rio no sistema.</p>
  * @access private
  * @param integer $piAction Id da a��o
  * @param ISMSUser $poUser Objeto que representa o usu�rio
  */
  private function createLog($piAction, $poUser) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
	 	// pega o nome do usu�rio
	 	$msUserName = $poUser->getFieldValue('user_name');

 	  // se o usu�rio for um procurador, mostra o nome de quem ele est� representando
 	  $moSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    $msLabelUser = $moSession->getUserName(true);

  	// insere na tabela de log
  	$moLog = new ISMSAuditLog();
  	$moLog->setFieldValue('log_date', ISMSLib::ISMSTime());
  	$moLog->setFieldValue('log_user', $msUserName, false);
  	$moLog->setFieldValue('log_action', $piAction);
  	$moLog->setFieldValue('log_element', $msUserName, false);

  	$msLogLogin = FWDLanguage::getPHPStringValue('ad_login_message', '%label_user% <b>%user_name%</b> %action%.');
  	$msDescription = str_replace(array('%label_user%', '%user_name%', '%action%'), array($poUser->getLabel(), $msLabelUser, $moLog->getAction($piAction)), $msLogLogin);
  	$moLog->setFieldValue('log_description', $msDescription, false);

  	$moLog->insert();
  }

  /**
   * Retorna o ip do �ltimo login efetuado pelo usu�rio logado.
   *
   * <p>Retorna o ip do �ltimo login efetuado pelo usu�rio logado.</p>
   * @access public
   * @return string IP do �ltimo login
   */
  public function getLastIP() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return $this->csLastIP;
  }

  /**
   * Retorna a data do �ltimo login efetuado pelo usu�rio logado.
   *
   * <p>Retorna a data do �ltimo login efetuado pelo usu�rio logado.</p>
   * @access public
   * @return string Data do �ltimo login
   */
  public function getLastLogin() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return $this->ciLastAccessDate?ISMSLib::getISMSDate($this->ciLastAccessDate):"";
  }

  public function setActivationPeriod($pbActivationPeriod) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $this->cbActivationPeriod = $pbActivationPeriod;
  }

  public function getActivationPeriod() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    return $this->cbActivationPeriod;
  }
}
?>