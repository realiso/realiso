<?php
class WKFTaskMetadata extends ISMSTable {  
  
 /**
  * Construtor.
  * 
  * <p>Construtor da classe WKFTaskMetadata.</p>
  * @access public 
  */
  public function __construct(){    
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    parent::__construct("wkf_task_metadata");
    //$this->csAliasId = 'task_metadata_id';
    $this->coDataset->addFWDDBField(new FWDDBField("pktaskmetadata",           "task_metadata_id",               DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("fkContext",                "metadata_context_id",            DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("fktask",                   "task_id",                        DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("ntype",                    "metadata_type",                  DB_NUMBER));
    
  }
  
};

?>