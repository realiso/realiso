<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSActiveDirectory.
 *
 * <p>Implementa��o do suporte ao Active Directory</p>
 * @package ISMS
 * @subpackage classes
 */
class ISMSActiveDirectory {

  protected $userName;
  protected $login;
  protected $mail;

  /**
   * Retorna o nome de usu�rio
   */
  public function getUserName(){
    if(empty($this->$userName))
      return "Real User";

    return $this->userName;
  }

  /*
   * Retorna o login do usu�rio
   */
  public function getLogin(){
    return $this->login;
  }

  /*
   * Retorna o e-mail do usu�rio
   */
  public function getMail(){
    if(empty($this->mail))
      return $this->login . "@change.your.email.com";

    return $this->mail;
  }

 /**
  * Facilita a conex�o com o servidor do AD
  * 
  * <p>M�todo para facilitar a conex�o com o servidor AD</p>
  * @access private 
  * @param string $psLogin Login 
  * @param string $psPassword Senha
  */
  private function connectServer($login, $password){
    $moConfig = new ISMSConfig();
    $host = $moConfig->getConfig(AD_HOST);
    $port = $moConfig->getConfig(AD_PORT);
    $ssl = $moConfig->getConfig(AD_SSL);
    $bindingDn = $moConfig->getConfig(AD_SEARCH_BINDING_DN);
    $loginAndDomain = str_replace("%user%", $login, $bindingDn); 

    $fwdAd = new FWDActiveDirectory();
    $connected = $fwdAd->connect($host, $loginAndDomain, $password, $port, $ssl);

    if(!$connected)
      return null;
    else
      return $fwdAd;
  }
  
 /**
  * Verifica se um usu�rio existe no AD e guarda algumas informa��es para uso.
  * 
  * <p>M�todo para verificar se um usu�rio existe no AD e guarda algumas informa��es para uso.</p>
  * @access public 
  * @param string $psLogin Login 
  * @param string $psPassword Senha
  */ 
  public function fetchByLoginPass($psLogin, $psPassword) {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

    $fwdAd = $this->connectServer($psLogin, $psPassword);

    if(!$fwdAd) {
      return false;
    }

    $moConfig = new ISMSConfig();
    $baseDn = $moConfig->getConfig(AD_SEARCH_BASE_DN);

    $filter = "(&(objectClass=user)(samaccountname=$psLogin))";
    $fields = array("samaccountname", "givenname", "sn", "mail");

    if(!$fwdAd->search($baseDn, $filter, $fields)) {
      return false;
    }

    $result = $fwdAd->getResults();

    $this->userName = $result[0]["givenname"][0] . " " . $result[0]["sn"][0];  
    $this->login = $result[0]["samaccountname"][0];
    $this->mail = $result[0]["mail"][0];

    return true;
  }
  
 /**
  * Busca todos os usu�rios do AD.
  * 
  * <p>M�todo para buscar todos os usu�rios do AD</p>
  * @access public 
  */ 
  public function getAllUsers() {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

    $moConfig = new ISMSConfig();
    $login = $moConfig->getConfig(AD_SEARCH_USERNAME);
    $password = $moConfig->getConfig(AD_SEARCH_PASSWORD); 

    $fwdAd = $this->connectServer($login, $password);

    if(!$fwdAd) {
      return null;
    }

    $baseDn = $moConfig->getConfig(AD_SEARCH_BASE_DN);

    $filter = "(&(objectClass=user)(givenname=*))";
    $fields = array("samaccountname", "givenname", "sn", "mail");

    if(!$fwdAd->search($baseDn, $filter, $fields)) {
      return null;
    }

    $results = $fwdAd->getResults();

    return $results;
  }
}
?>