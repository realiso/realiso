<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe PMDocumentReader
 *
 * <p>Classe que representa a tabela que relaciona documentos e usu�rios.</p>
 * @package ISMS
 * @subpackage classes
 */
class PMDocumentReader extends ISMSTable {

 /**
  * Construtor.
  *
  * <p>Construtor da classe PMDocumentReader.</p>
  * @access public
  */
  public function __construct(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    parent::__construct('pm_doc_readers');
    $this->coDataset->addFWDDBField(new FWDDBField('fkDocument','document_id',DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('fkUser',    'user_id',    DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('bHasRead', 	'user_has_read_document', DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('bDenied', 	'denied', DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('bManual', 	'manual', DB_NUMBER));
  }

 /**
  * Atualiza rela��es entre um documento e diversos usu�rios baseado
  * nos usu�rios correntes de um processo.
  *
  * <p>M�todo para atualizar rela��es entre um documento e diversos
  * usu�rios baseado nos usu�rios correntes de um processo.</p>
  * @access public
  * @param integer $piDocumentId Id do documento
  * @param array $paUsersIds Usu�rios correntes do processo
  */
  public function updateUsers($piDocumentId, $paUsers){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
  	$moDocReaderInsert = new PMDocumentReader();  	   	 
  	$moDocReaderInsert->setFieldValue('document_id',$piDocumentId);    
  	 
  	/*
  	 * Usu�rios que n�o possuem permiss�o de leitura n�o devem ser inseridos
  	 */
  	$maDeniedUsers = $moDocReaderInsert->getDeniedUsers($piDocumentId);
  	
  	/*
  	 * Usu�rios que j� possuem permiss�o de leitura n�o devem ser inseridos
  	 */
  	$maAllowedUsers = $moDocReaderInsert->getAllowedUsers($piDocumentId);
  	
  	/*
  	 * Insere novos usu�rios
  	 */
  	$maInsertUsers = array_diff($paUsers, array_merge($maDeniedUsers, $maAllowedUsers));
  	foreach($maInsertUsers as $piUserId){
      $moDocReaderInsert->setFieldValue('user_id',$piUserId);
      $moDocReaderInsert->insert();
    }    
    
    /*
     * Dele��o de usu�rios. Os usu�rios que devem ser deletados s�o
     * aqueles que n�o foram negados ou inseridos explicitamente
     * pelo usu�rio, ou seja, somente os que foram inseridos
     * automaticamente.
     */
    $moDocReader2 = new PMDocumentReader();
    $maDeleteUsers = array_diff($this->getAutoAllowedUsers($piDocumentId), $paUsers);
    if (count($maDeleteUsers)) {
	    $moDocReader2->createFilter($piDocumentId,'document_id');
	    foreach($maDeleteUsers as $piUserId){
	      $moDocReader2->createFilter($piUserId,'user_id','in');
	    }
	    $moDocReader2->delete();
  	}
  }
 
 /**
  * Insere rela��es entre um documento e diversos usu�rios.
  *
  * <p>M�todo para inserir rela��es entre um documento e diversos usu�rios.</p>
  * @access public
  * @param integer $piDocumentId Id do documento
  * @param array $paUsersIds Ids dos usu�rios
  * @param boolean $pbManualInsert Indica se os usu�rios foram inseridos manualmente
  */
  public function insertUsers($piDocumentId, $paUsers){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
  	$moDocReader = new PMDocumentReader();  	   	 
  	$moDocReader->setFieldValue('document_id',$piDocumentId);
    $moDocReader->setFieldValue('manual',1);
  	 
  	/*
  	 * Pega usu�rios que n�o possuem permiss�o de leitura
  	 */  	
  	$maDeniedUsers = $moDocReader->getDeniedUsers($piDocumentId);
  	  	
  	/*
  	 * Usu�rios que devem ser inseridos
  	 */
  	$maInsertUsers = array_diff($paUsers, $maDeniedUsers);
  	foreach($maInsertUsers as $piUserId){
      $moDocReader->setFieldValue('user_id',$piUserId);
      $moDocReader->insert();
    }
  	
  	/*
  	 * Usu�rios que devem apenas receber permiss�o de leitura
  	 * pois j� se encontram na tabela.
  	 */  	 		
  	$maUpdateUsers = array_intersect($paUsers, $maDeniedUsers);
  	if (count($maUpdateUsers)) {
  		$moDocReader = new PMDocumentReader();  		
  		$moDocReader->setFieldValue('denied', 0);
  		$moDocReader->setFieldValue('manual', 1);
  		$moDocReader->createFilter($piDocumentId,'document_id');
	  	foreach($maUpdateUsers as $piUserId){
	      $moDocReader->createFilter($piUserId,'user_id','in');      
	    }
	    $moDocReader->update();
  	}  	
  }
  
 /**
  * Nega a leitura de um documento para diversos usu�rios.
  *
  * <p>M�todo para negar a leitura de um documento para diversos usu�rios.</p>
  * @access public
  * @param integer $piDocumentId Id do documento
  * @param integer $paUsersIds Ids dos usu�rios
  */
  public function denyUsers($piDocumentId, $paUsersIds){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
  	if (count($paUsersIds)) {  		
  		$moDocReader = new PMDocumentReader();  		
	    $moDocReader->createFilter($piDocumentId,'document_id');
	    foreach($paUsersIds as $piUserId){
	      $moDocReader->createFilter($piUserId,'user_id','in');
	    }
	    $moDocReader->setFieldValue('denied',1);
	    $moDocReader->update();	    
  	}
  }

 /**
  * Retorna os ids de todos os usu�rios associados a um documento.
  *
  * <p>Retorna os ids de todos os usu�rios associados a um documento.</p>
  * @access public
  * @param integer $piDocumentId Id do documento
  * @return array Ids dos usu�rios
  */
  public function getAllUsers($piDocumentId){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
  	$moDocReader = new PMDocumentReader();
    $moDocReader->createFilter($piDocumentId,'document_id');    
    $moDocReader->select();
    $maUsers = array();
    while($moDocReader->fetch()){
      $maUsers[] = $moDocReader->getFieldValue('user_id');
    }    
    return $maUsers;
  }
  
 /**
  * Retorna os ids dos usu�rios associados a um documento.
  *
  * <p>Retorna os ids dos usu�rios associados a um documento.</p>
  * @access public
  * @param integer $piDocumentId Id do documento
  * @return array Ids dos usu�rios
  */
  public function getAllowedUsers($piDocumentId){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
  	$moDocReader = new PMDocumentReader();    
    $moDocReader->createFilter($piDocumentId,'document_id');
    $moDocReader->createFilter(1,'denied','<>');    
    $moDocReader->select();
    $maUsers = array();
    while($moDocReader->fetch()){
      $maUsers[] = $moDocReader->getFieldValue('user_id');
    }    
    return $maUsers;
  }
  
 /**
  * Retorna os ids dos usu�rios associados a um documento automaticamente.
  *
  * <p>Retorna os ids dos usu�rios associados a um documento automaticamente.</p>
  * @access public
  * @param integer $piDocumentId Id do documento
  * @return array Ids dos usu�rios
  */
  public function getAutoAllowedUsers($piDocumentId) {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
  	$moDocReader = new PMDocumentReader();
    $moDocReader->createFilter($piDocumentId,'document_id');
    $moDocReader->createFilter(1,'denied','<>');
    $moDocReader->createFilter(1,'manual','<>');
    $moDocReader->select();
    $maUsers = array();
    while($moDocReader->fetch()){
      $maUsers[] = $moDocReader->getFieldValue('user_id');
    }    
    return $maUsers;
  } 
  
 /**
  * Retorna os ids dos usu�rios removidos da leitura de um documento.
  *
  * <p>Retorna os ids dos usu�rios removidos da leitura de documento.</p>
  * @access public
  * @param integer $piDocumentId Id do documento
  * @return array Ids dos usu�rios
  */
  public function getDeniedUsers($piDocumentId){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
  	$moDocReader = new PMDocumentReader();
    $moDocReader->createFilter($piDocumentId,'document_id');
    $moDocReader->createFilter(1,'denied');    
    $moDocReader->select();
    $maUsers = array();
    while($moDocReader->fetch()){
      $maUsers[] = $moDocReader->getFieldValue('user_id');
    }
    return $maUsers;
  }
  
 /**
  * Retorna os ids dos usu�rios inseridos manualmente para leitura de um documento.
  *
  * <p>Retorna os ids dos usu�rios inseridos manualmente para leitura de um documento.</p>
  * @access public
  * @param integer $piDocumentId Id do documento
  * @return array Ids dos usu�rios
  */
  public function getManualInsertedUsers($piDocumentId){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
  	$moDocReader = new PMDocumentReader();
    $moDocReader->createFilter($piDocumentId,'document_id');
    $moDocReader->createFilter(1,'manual');    
    $moDocReader->select();
    $maUsers = array();
    while($moDocReader->fetch()){
      $maUsers[] = $moDocReader->getFieldValue('user_id');
    }
    return $maUsers;
  }
}
?>