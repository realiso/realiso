<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe PMTemplateBestPractice
 *
 * <p>Classe que representa a tabela de relacionamento entre templates de documentos 
 * e melhores praticas</p>
 * @package ISMS
 * @subpackage classes
 */
class PMTemplateBestPractice extends ISMSTable {

 /**
  * Construtor.
  *
  * <p>Construtor da classe PMTemplateBestPractice</p>
  * @access public
  */
  public function __construct(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    parent::__construct('pm_template_best_practice');
    $this->csAliasId = '';


    $this->coDataset->addFWDDBField(new FWDDBField('fkTemplate'     ,'template_id'     ,DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('fkBestPractice' ,'best_practice_id' ,DB_NUMBER));
  }
  
  /**
  * Insere rela��es entre um template e diversas melhores pr�ticas.
  *
  * <p>M�todo para inserir rela��es entre um template e diversas melhores pr�ticas.</p>
  * @access public
  * @param integer $piTemplateId Id do template
  * @param array $paBestPractices Ids das melhores pr�ticas
  */
  public function insertRelations($piTemplate, $paBestPractices){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $this->setFieldValue('template_id', $piTemplate);
    foreach($paBestPractices as $miBestPracticeId){
      $this->setFieldValue('best_practice_id', $miBestPracticeId);
      parent::insert();
    }
  }

 /**
  * Deleta rela��es entre um template e diversas melhores pr�ticas.
  *
  * <p>M�todo para deletar rela��es entre um template e diversas melhores pr�ticas.</p>
  * @access public
  * @param integer $piTemplateId Id de melhor pratica
  * @param array $paBestPractices Ids das melhores pr�ticas
  */
  public function deleteRelations($piTemplateId, $paBestPractices){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    foreach($paBestPractices as $miBestPracticeId){
      $moTemplateBP = new PMTemplateBestPractice();
      $moTemplateBP->createFilter($piTemplateId,'template_id');
      $moTemplateBP->createFilter($miBestPracticeId,'best_practice_id');
      $moTemplateBP->delete();
    }
  }

 
}
?>