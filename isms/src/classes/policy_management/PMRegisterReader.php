<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe PMRegisterReader
 *
 * <p>Classe que representa a tabela que relaciona registros e usu�rios.</p>
 * @package ISMS
 * @subpackage classes
 */
class PMRegisterReader extends ISMSTable {

 /**
  * Construtor.
  *
  * <p>Construtor da classe PMRegisterReader.</p>
  * @access public
  */
  public function __construct(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    parent::__construct('pm_register_readers');
    $this->coDataset->addFWDDBField(new FWDDBField('fkRegister','register_id',DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('fkUser',    'user_id',    DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('bDenied', 	'denied',     DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('bManual', 	'manual',     DB_NUMBER));
  }

 /**
  * Atualiza rela��es entre um registro e diversos usu�rios.
  *
  * <p>M�todo para atualizar rela��es entre um registro e diversos usu�rios.</p>
  * @access public
  * @param integer $piRegisterId Id do registro
  * @param array $paUsersIds Usu�rios correntes do processo
  */
  public function updateUsers($piRegisterId, $paUsers){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
  	$moRegisterReaderInsert = new PMRegisterReader();  	   	 
  	$moRegisterReaderInsert->setFieldValue('register_id',$piRegisterId);    
  	 
  	/*
  	 * Usu�rios que n�o possuem permiss�o de leitura n�o devem ser inseridos
  	 */
  	$maDeniedUsers = $moRegisterReaderInsert->getDeniedUsers($piRegisterId);
  	
  	/*
  	 * Usu�rios que j� possuem permiss�o de leitura n�o devem ser inseridos
  	 */
  	$maAllowedUsers = $moRegisterReaderInsert->getAllowedUsers($piRegisterId);
  	
  	/*
  	 * Insere novos usu�rios
  	 */
  	$maInsertUsers = array_diff($paUsers, array_merge($maDeniedUsers, $maAllowedUsers));
  	foreach($maInsertUsers as $piUserId){
      $moRegisterReaderInsert->setFieldValue('user_id',$piUserId);
      $moRegisterReaderInsert->insert();
    }    
    
    /*
     * Dele��o de usu�rios. Os usu�rios que devem ser deletados s�o
     * aqueles que n�o foram negados ou inseridos explicitamente
     * pelo usu�rio, ou seja, somente os que foram inseridos
     * automaticamente.
     */
    $moRegisterReader2 = new PMRegisterReader();
    $maDeleteUsers = array_diff($this->getAutoAllowedUsers($piRegisterId), $paUsers);
    if (count($maDeleteUsers)) {
	    $moRegisterReader2->createFilter($piRegisterId,'register_id');
	    foreach($maDeleteUsers as $piUserId){
	      $moRegisterReader2->createFilter($piUserId,'user_id','in');
	    }
	    $moRegisterReader2->delete();
  	}
  }
 
 /**
  * Insere rela��es entre um registro e diversos usu�rios.
  *
  * <p>M�todo para inserir rela��es entre um registro e diversos usu�rios.</p>
  * @access public
  * @param integer $piRegisterId Id do registro
  * @param array $paUsersIds Ids dos usu�rios
  */
  public function insertUsers($piRegisterId, $paUsers){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
  	$moRegisterReader = new PMRegisterReader();  	   	 
  	$moRegisterReader->setFieldValue('register_id',$piRegisterId);
    $moRegisterReader->setFieldValue('manual',1);
  	 
  	/*
  	 * Pega usu�rios que n�o possuem permiss�o de leitura
  	 */  	
  	$maDeniedUsers = $moRegisterReader->getDeniedUsers($piRegisterId);
  	  	
  	/*
  	 * Usu�rios que devem ser inseridos
  	 */
  	$maInsertUsers = array_diff($paUsers, $maDeniedUsers);
  	foreach($maInsertUsers as $piUserId){
      $moRegisterReader->setFieldValue('user_id',$piUserId);
      $moRegisterReader->insert();
    }
  	
  	/*
  	 * Usu�rios que devem apenas receber permiss�o de leitura
  	 * pois j� se encontram na tabela.
  	 */  	 		
  	$maUpdateUsers = array_intersect($paUsers, $maDeniedUsers);
  	if (count($maUpdateUsers)) {
  		$moRegisterReader = new PMRegisterReader();  		
  		$moRegisterReader->setFieldValue('denied', 0);
  		$moRegisterReader->setFieldValue('manual', 1);
  		$moRegisterReader->createFilter($piRegisterId,'register_id');
	  	foreach($maUpdateUsers as $piUserId){
	      $moRegisterReader->createFilter($piUserId,'user_id','in');      
	    }
	    $moRegisterReader->update();
  	}  	
  }
  
 /**
  * Nega a leitura de um registro para diversos usu�rios.
  *
  * <p>M�todo para negar a leitura de um registro para diversos usu�rios.</p>
  * @access public
  * @param integer $piRegisterId Id do registro
  * @param integer $paUsersIds Ids dos usu�rios
  */
  public function denyUsers($piRegisterId, $paUsersIds){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
  	if (count($paUsersIds)) {
      foreach($paUsersIds as $miUserId){
        $moRegisterReader = new PMRegisterReader();
        $moRegisterReader->createFilter($piRegisterId,'register_id');
        $moRegisterReader->createFilter($miUserId,'user_id');
        $moRegisterReader->select();
        if ($moRegisterReader->fetch()) {
          $moRegisterReader->setFieldValue('manual',1);
          $moRegisterReader->setFieldValue('denied',1);
          $moRegisterReader->update();
        } else {
          $moRegisterReader->removeAllFilters();
          $moRegisterReader->setFieldValue('register_id',$piRegisterId);
          $moRegisterReader->setFieldValue('user_id',$miUserId);
          $moRegisterReader->setFieldValue('manual',1);
          $moRegisterReader->setFieldValue('denied',1);
          $moRegisterReader->insert();
        }
      }
  	}
  }

 /**
  * Retorna os ids de todos os usu�rios associados a um registro.
  *
  * <p>Retorna os ids de todos os usu�rios associados a um registro.</p>
  * @access public
  * @param integer $piRegisterId Id do registro
  * @return array Ids dos usu�rios
  */
  public function getAllUsers($piRegisterId){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
  	$moRegisterReader = new PMRegisterReader();
    $moRegisterReader->createFilter($piRegisterId,'register_id');    
    $moRegisterReader->select();
    $maUsers = array();
    while($moRegisterReader->fetch()){
      $maUsers[] = $moRegisterReader->getFieldValue('user_id');
    }    
    return $maUsers;
  }
  
 /**
  * Retorna os ids dos usu�rios associados a um registro.
  *
  * <p>Retorna os ids dos usu�rios associados a um registro.</p>
  * @access public
  * @param integer $piRegisterId Id do registro
  * @return array Ids dos usu�rios
  */
  public function getAllowedUsers($piRegisterId){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
  	$moRegisterReader = new PMRegisterReader();    
    $moRegisterReader->createFilter($piRegisterId,'register_id');
    $moRegisterReader->createFilter(1,'denied','<>');    
    $moRegisterReader->select();
    $maUsers = array();
    while($moRegisterReader->fetch()){
      $maUsers[] = $moRegisterReader->getFieldValue('user_id');
    }
    $maDocumentUsers = $moRegisterReader->getDocumentAllowedUsers($piRegisterId);
    $maUsers = array_merge($maUsers,$maDocumentUsers);
    return array_unique($maUsers);
  }
  
 /**
  * Retorna os ids dos usu�rios associados a um registro automaticamente.
  *
  * <p>Retorna os ids dos usu�rios associados a um registro automaticamente.</p>
  * @access public
  * @param integer $piRegisterId Id do registro
  * @return array Ids dos usu�rios
  */
  public function getAutoAllowedUsers($piRegisterId) {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
  	$moRegisterReader = new PMRegisterReader();
    $moRegisterReader->createFilter($piRegisterId,'register_id');
    $moRegisterReader->createFilter(1,'denied','<>');
    $moRegisterReader->createFilter(1,'manual','<>');
    $moRegisterReader->select();
    $maUsers = array();
    while($moRegisterReader->fetch()){
      $maUsers[] = $moRegisterReader->getFieldValue('user_id');
    }
    return $maUsers;
  } 
  
 /**
  * Retorna os ids dos usu�rios removidos da leitura de um registro.
  *
  * <p>Retorna os ids dos usu�rios removidos da leitura de registro.</p>
  * @access public
  * @param integer $piRegisterId Id do registro
  * @return array Ids dos usu�rios
  */
  public function getDeniedUsers($piRegisterId){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
  	$moRegisterReader = new PMRegisterReader();
    $moRegisterReader->createFilter($piRegisterId,'register_id');
    $moRegisterReader->createFilter(1,'denied');    
    $moRegisterReader->select();
    $maUsers = array();
    while($moRegisterReader->fetch()){
      $maUsers[] = $moRegisterReader->getFieldValue('user_id');
    }
    return $maUsers;
  }
  
 /**
  * Retorna os ids dos usu�rios inseridos manualmente para leitura de um registro.
  *
  * <p>Retorna os ids dos usu�rios inseridos manualmente para leitura de um registro.</p>
  * @access public
  * @param integer $piRegisterId Id do registro
  * @return array Ids dos usu�rios
  */
  public function getManualInsertedUsers($piRegisterId){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
  	$moRegisterReader = new PMRegisterReader();
    $moRegisterReader->createFilter($piRegisterId,'register_id');
    $moRegisterReader->createFilter(1,'manual');    
    $moRegisterReader->select();
    $maUsers = array();
    while($moRegisterReader->fetch()){
      $maUsers[] = $moRegisterReader->getFieldValue('user_id');
    }
    return $maUsers;
  }
  
  /**
  * Retorna os ids dos leitores que podem ler o registro e os documentos associados a ele.
  *
  * <p>Retorna os ids dos leitores que podem ler o registro e os documentos associados a ele.</p>
  * @access public
  * @param integer $piRegisterId Id do registro
  * @return array Ids dos usu�rios
  */
  public function getDocumentAllowedUsers($piRegisterId){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $maUsers = array();
    $moDocRegisters = new PMDocRegisters();
    $moDocRegisters->createFilter($piRegisterId,'register_id');
    $moDocRegisters->select();
    while($moDocRegisters->fetch()) {
      $miDocumentId = $moDocRegisters->getFieldValue('document_id');
      $moDocument = new PMDocument();
      $moDocument->fetchById($miDocumentId);
      if ($moDocInstance = $moDocument->getPublishedInstance()) {
        if ($moDocInstance->getFieldValue('doc_instance_major_version')>0) {
          $moDocumentReader = new PMDocumentReader();
          $maDocumentReaders = $moDocumentReader->getAllowedUsers($miDocumentId);
          $maUsers = array_merge($maUsers,$maDocumentReaders);
        }
      }
    }
    $maUsers = array_unique($maUsers);
    
    $moRegisterReader = new PMRegisterReader();
    $maDeniedUsers = $moRegisterReader->getDeniedUsers($piRegisterId);
    return array_diff($maUsers,$maDeniedUsers);
  }
}
?>