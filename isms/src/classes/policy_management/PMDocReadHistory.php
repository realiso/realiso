<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe PMDocReadHistory
 *
 * <p>Classe que representa a tabela de hist�rico de quem leu e quando leu um documento.</p>
 * @package ISMS
 * @subpackage classes
 */
class PMDocReadHistory extends ISMSTable {

 /**
  * Construtor.
  *
  * <p>Construtor da classe PMDocReadHistory.</p>
  * @access public
  */
  public function __construct(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    parent::__construct('pm_doc_read_history');
    $this->coDataset->addFWDDBField(new FWDDBField('fkinstance',  'document_id',  DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('fkUser',      'user_id',      DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('dDate', 	    'read_date',		DB_DATETIME));
  }
  
  public function insert(){
    //insert do isms_context sobrescrito para arrumar o bug #0001185: Erro de chave duplicada na tabela pm_doc_read_history
    //abaixo � testado se jah existe o valor  na tabela, caso exista n�o insere o novo valor
    $moPMDocReadHistoryTst = new PMDocReadHistory();
    $moPMDocReadHistoryTst->createFilter($this->getFieldValue('user_id') ,'user_id');
    $moPMDocReadHistoryTst->createFilter($this->getFieldValue('document_id') ,'document_id');
    $moPMDocReadHistoryTst->createFilter($this->getFieldValue('read_date'),'read_date');
    $moPMDocReadHistoryTst->select();
    if(!$moPMDocReadHistoryTst->fetch()){
      parent::insert();
    }
  }
}
?>