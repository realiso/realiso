<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

include_once $handlers_ref . "QueryDocumentCanRead.php";
include_once $handlers_ref . "QuerySubDocuments.php";
include_once $handlers_ref . "QueryDocumentModelRegisters.php";
include_once $handlers_ref . "QueryDocumentParents.php";
include_once $handlers_ref . "QuerySearchDocument.php";
include_once $handlers_ref . "grid/policy/QueryGridDocuments.php";


define("DOCUMENT_TYPE_MANAGEMENT",3801);
define("DOCUMENT_TYPE_OTHERS",3802);

/**
 * Classe PMDocument
 *
 * <p>Classe que insere um documento </p>
 * @package ISMS
 * @subpackage classes
 */
class PMDocument extends ISMSContext implements IWorkflow {

	protected static $caAllowedCtxTypes = array(
	CONTEXT_AREA,
	CONTEXT_PROCESS,
	CONTEXT_ASSET,
	CONTEXT_CONTROL,
	CONTEXT_CI_ACTION_PLAN
	);

	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe PMDocument.</p>
	 * @access public
	 */
	public function __construct() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		parent::__construct("pm_document");
		$this->csAliasId = "document_id";
		$this->csSpecialDeleteSP = "delete_document";
		$this->csDependenceAliasId = "document_parent";
		$this->ciContextType = CONTEXT_DOCUMENT;

		$this->coWorkflow = new PMWorkflow(ACT_DOCUMENT_APPROVAL);

		$this->coDataset->addFWDDBField(new FWDDBField("fkContext",           "document_id",             DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkClassification",    "document_classification", DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkParent",            "document_parent",         DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkAuthor",            "document_author",         DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkMainApprover",      "document_main_approver",  DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("bHasApproved",        "document_has_approved",   DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("sName",               "document_name",           DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("tDescription",        "document_description",    DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("fkCurrentVersion",    "document_current_version",DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("nType",               "document_type",           DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("sKeywords",           "document_keywords",       DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("dDateProduction",     "document_date_production",DB_DATETIME));
		$this->coDataset->addFWDDBField(new FWDDBField("dDeadline",           "document_deadline",       DB_DATETIME));
		$this->coDataset->addFWDDBField(new FWDDBField("bFlagDeadlineAlert",  "flag_deadline_near",      DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("bFlagDeadlineExpired","flag_deadline_late",      DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("nDaysBefore",         "days_before",             DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkSchedule",          "document_schedule",       DB_NUMBER));

		$this->caSearchableFields = array('document_name', 'document_description', 'document_keywords');
	}

	/**
	 * Retorna o nome do contexto.
	 *
	 * <p>M�todo para retornar o nome do contexto.</p>
	 * @access public
	 * @return string Nome do contexto
	 */
	public function getName(){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		return $this->getFieldValue('document_name');
	}

	/**
	 * Retorna o label do contexto.
	 *
	 * <p>M�todo para retornar o label do contexto.</p>
	 * @access public
	 * @return string Label do contexto
	 */
	public function getLabel(){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		return FWDLanguage::getPHPStringValue('mx_document', "Documento");
	}

	/**
	 * Retorna os ids dos usu�rios que devem aprovar o documento.
	 *
	 * <p>Retorna os ids dos usu�rios que devem aprovar o documento.</p>
	 * @access public
	 * @return array Ids dos aprovadores
	 */
	public function getApprovers(){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		$moDocumentApprover = new PMDocumentApprover();
		$maApprovers = $moDocumentApprover->getUsers($this->getFieldValue('document_id'),true);
		$maApprovers[] = $this->getFieldValue('document_main_approver');
		return $maApprovers;
	}

	/**
	 * Testa se um usu�rio � aprovador de um documento.
	 *
	 * <p>Testa se um usu�rio � aprovador de um documento.</p>
	 * @access public
	 * @param integer $psUserId Id do usu�rio
	 * @return boolean Indica se o usu�rio � aprovador do documento
	 */
	public function isApprover($psUserId){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		if($psUserId==$this->getFieldValue('document_main_approver')) return true;
		$moDocumentApprover = new PMDocumentApprover();
		$moDocumentApprover->createFilter($this->getFieldValue('document_id'),'document_id');
		$moDocumentApprover->createFilter($psUserId,'user_id');
		$moDocumentApprover->select();
		return $moDocumentApprover->fetch();
	}

	/**
	 * Testa se um aprovador j� aprovou o documento.
	 *
	 * <p>Testa se um aprovador j� aprovou o documento.</p>
	 * @access public
	 * @param integer $psUserId Id do usu�rio
	 * @return boolean Indica se o usu�rio j� aprovou o documento
	 */
	public function hasApproved($psUserId){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		if($psUserId==$this->getFieldValue('document_main_approver')){
			return ($this->getFieldValue('document_has_approved')==1?true:false);
		}else{
			$moDocumentApprover = new PMDocumentApprover();
			$miDocumentId = $this->getFieldValue('document_id');
			$moDocumentApprover->createFilter($miDocumentId,'document_id');
			$moDocumentApprover->createFilter($psUserId,'user_id');
			$moDocumentApprover->select();
			if($moDocumentApprover->fetch()){
				return ($moDocumentApprover->getFieldValue('has_approved')==1?true:false);
			}else{
				trigger_error("User '$psUserId' is not approver of document '$miDocumentId'.",E_USER_WARNING);
			}
		}
	}

	/**
	 * Retorna os ancestrais do contexto.
	 *
	 * <p>M�todo para retornar todos contextos acima de um determinado contexto na
	 * �rvore de depend�ncias.</p>
	 * @access public
	 * @param integer $piContextId Id do contexto
	 * @param boolean $pbExecByTrash se o metodo est� sendo chamado pela lixeira
	 * @return array Array de ids dos ancestrais do contexto
	 */
	public function getSuperContexts($piContextId,$pbExecByTrash = false){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		$moQuery = new QueryDocumentParents(FWDWebLib::getConnection(),$pbExecByTrash);
		$moQuery->setDocumentId($piContextId);
		$moQuery->makeQuery();
		$moQuery->executeQuery();
		$maSuperContexts = array();
		while($moQuery->fetch()){
			$maSuperContexts[] = $moQuery->getFieldValue('document_id');
		}
		return $maSuperContexts;
	}

	/**
	 * Retorna os sub-contextos de um documento.
	 *
	 * <p>M�todo para retornar os sub-contextos de um contexto.</p>
	 * @access public
	 * @param integer $piDocumentId Id do documento
	 * @return array Array de ids dos sub-contextos
	 */
	public function getSubContexts($piDocumentId){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		$moQuery = new QuerySubDocuments(FWDWebLib::getConnection());
		$moQuery->setDocumentId($piDocumentId);
		$moQuery->makeQuery();
		$moQuery->executeQuery();
		$maSubContexts = array();
		while($moQuery->fetch()){
			$maSubContexts[] = $moQuery->getFieldValue('document_id');
		}

		$moQuery = new QueryDocumentModelRegisters(FWDWebLib::getConnection(), $piDocumentId);
		$moQuery->makeQuery();
		$moQuery->executeQuery();
		while($moQuery->fetch()){
			$maSubContexts[] = $moQuery->getFieldValue('reg_id');
		}

		return $maSubContexts;
	}

	/**
	 * Indica se o contexto � delet�vel ou n�o.
	 *
	 * <p>M�todo que indica se o contexto � delet�vel ou n�o.</p>
	 * @access public
	 * @param integer $piContextId id do contexto
	 */
	public function isDeletable($piContextId){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		if(!ISMSLib::getConfigById(GENERAL_CASCADE_ON)){
			// cascade off: verificar exist�ncia de subcontextos
			if($this->getSubContexts($piContextId)){
				return false;
			}
			
    		$pmDocRegisters = new PMDocRegisters();
    		$pmDocRegisters->createFilter($piContextId,'document_id');
    		$pmDocRegisters->select();
    		if($pmDocRegisters->fetch()){
    		    return false;
    		}			
			
		}
		return true;
	}

	/**
	 * Exibe uma popup caso n�o seja poss�vel remover um contexto.
	 *
	 * <p>M�todo que exibe uma popup caso n�o seja poss�vel remover um contexto.</p>
	 * @access public
	 * @param integer $piContextId id do contexto
	 */	
	public function showDeleteError($piContextId) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		$msTitle = FWDLanguage::getPHPStringValue('tt_document_remove_error','Erro ao remover documento');
		$msMessage = FWDLanguage::getPHPStringValue('st_document_remove_error',"N�o foi poss�vel remover o documento <b>%name%</b>. O documento cont�m subdocumentos ou registros associados. Para remover documentos que contenham subdocumentos ou registros associados, ative a op��o \"Delete Cascade\" na aba \"Admin -> Configura��o\".");
		$moDocument = new PMDocument();
		$moDocument->fetchById($piContextId);
		$msName = $moDocument->getFieldValue('document_name');
		$msMessage = str_replace("%name%",$msName,$msMessage);
		ISMSLib::openOk($msTitle,$msMessage,'',80);
	}

	/**
	 * Deleta logicamente um contexto.
	 *
	 * <p>M�todo para deletar logicamente um contexto.</p>
	 * @access public
	 * @param integer $piContextId id do contexto
	 */
	public function logicalDelete($piContextId,$pbDeleteDB = false,$pbExecByTrash = false,$pbExecBySystem = false){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		$moDocument = new PMDocument();
		$maSubDocuments = $this->getSubContexts($piContextId);
		foreach($maSubDocuments as $miSubDocument){
			$moDocument->delete($miSubDocument, false, false, $pbExecBySystem);
		}
	}

	/**
	 * Deleta um contexto.
	 *
	 * <p>M�todo para deletar um contexto.
	 * verifica se deve deletar os documentos fisicos ou somente trocar seu estado para deletado.</p>
	 * @access public
	 * @param integer $piContextId Id do contexto
	 * @param boolean $pbDeleteDB Remove do banco
	 * @param boolean $pbExecByTrash Delete chamado pela Lixeira do sistema
	 */
	public function delete($piContextId, $pbDeleteDB = false,$pbExecByTrash = false,$pbExecBySystem = false){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		// necessita pegar os dados do objeto para criar o log
		if($this->fetchById($piContextId)){
			if($pbDeleteDB && $pbExecByTrash && $piContextId){
				$moPMDocInstance = new PMDocInstance();
				$moPMDocInstance->createFilter($piContextId,'document_id');
				$moPMDocInstance->select();
				while($moPMDocInstance->fetch()) {
					global $files_ref;
					$msPath = (ISMSLib::isCompiled() ? $GLOBALS['files_ref'] : "../../packages/policy/");
					$msPath .= $moPMDocInstance->getFieldValue('doc_instance_path');
					if ($msPath && is_file($msPath)){
						unlink($msPath);
					}
				}
			}
		}
/*
        // verifica e desassocia registros do documento.
		if($pbDeleteDB){
          $pmRegister = new PMRegister();
          $pmRegister->createFilter($piContextId, 'document_id');
          $pmRegister->select();

          while($pmRegister->fetch()){
            $registerId = $pmRegister->getFieldValue('register_id');
            
            $pmRegisterTmp = new PMRegister();
            $pmRegisterTmp->setFieldValue('document_id', 'NULL');
            $pmRegisterTmp->update($registerId);
          }
		}		
	*/	
		parent::delete($piContextId, $pbDeleteDB, $pbExecByTrash, $pbExecBySystem);
	}
	/*

	*/
	/**
	 * Retorna o id do usu�rio que deve aprovar o contexto.
	 *
	 * <p>M�todo para retornar id do usu�rio que deve aprovar o contexto.</p>
	 * @access public
	 * @return integer Id do aprovador
	 */
	public function getApprover(){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		trigger_error("Documents have N approvers, use 'getApprovers' instead.",E_USER_ERROR);
	}

	/**
	 * Retorna o id do usu�rio respons�vel pelo contexto.
	 *
	 * <p>M�todo para retornar id do usu�rio respons�vel pelo contexto.</p>
	 * @access public
	 * @return integer Id do respons�vel
	 */
	public function getResponsible(){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		return $this->getFieldValue('document_author');
	}

	/**
	 * Retorna o caminho do contexto.
	 *
	 * <p>M�todo para retornar o caminho do contexto.</p>
	 * @access public
	 * @return string Caminho do contexto
	 */
	public function getSystemPath() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		$maPath = array();
		$maPathAux = array();
		$maPath[] =     $msPMPath = "<a href='javascript:isms_redirect_to_mode(".POLICY_MODE.",3,0)'>".FWDLanguage::getPHPStringValue('st_policy_management','Gest�o das Pol�ticas')."</a>";

		if($this->getFieldValue('document_type')==CONTEXT_REGISTER){
			$miSubTab = CONTEXT_REGISTER;
		}else{
			$miSubTab = CONTEXT_DOCUMENT;
		}
		$maPath[] = ISMSLib::getIconCode($this->getIcon($this->getFieldValue('document_id'),true),-2,-4). " <a href='javascript:isms_redirect_to_mode(" . POLICY_MODE . ",{$miSubTab},0)'>" . $this->getName() . "</a>";
		return implode("&nbsp;".ISMSLib::getIconCode('icon-arrow-right.gif',-2,0).'&nbsp;', $maPath);
	}


	/**
	 * Retorna o �cone da inst�ncia atual do documento.
	 *
	 * <p>M�todo para retornar o �cone da inst�ncia atual do documento.</p>
	 * @access public
	 * @param integer $piContextId Id do contexto
	 * @return string Nome do �cone
	 */
	public function getIcon($piContextId = 0,$pbForceBublished = false){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		$miInstanceId = 0;
		if($piContextId){
			$moDocument = new PMDocument();
			$moDocument->fetchById($piContextId);
		}else{
			$moDocument = $this;
		}
		$moQueryDoc = new QueryGridDocuments(FWDWebLib::getConnection());
		$moQueryDoc->setDocumentId($moDocument->getId());
		$moQueryDoc->setTypesToHide(CONTEXT_REGISTER);
		$moQueryDoc->makeQuery();
		$moQueryDoc->executeQuery();
		$moQueryDoc->fetch();
		if ($moDocument->getContextState()==CONTEXT_STATE_DOC_DEVELOPING && $moQueryDoc->getFieldValue('document_is_released') && !$pbForceBublished) {
			// modificado para n�o causar erro explica��es no BugTrack #0001193: problema no metodo getIcon da classe PMDocument

			//$miInstanceId = $moQueryDoc->getFieldValue('rev_instance_id');
		} else {
			$miInstanceId = $moDocument->getFieldValue('document_current_version');
		}
		$msFileName = '';
		$mbIsLink = false;
		$moPMDocInstance = new PMDocInstance();
		if($miInstanceId && $moPMDocInstance->fetchById($miInstanceId)){
			if($moPMDocInstance->getFieldValue('doc_instance_is_link')){
				$mbIsLink = true;
			}else{
				$msFileName = $moPMDocInstance->getFieldValue('doc_instance_file_name');
			}
		}else{
			// modificado para n�o causar erro explica��es no BugTrack #0001193: problema no metodo getIcon da classe PMDocument
			return 'icon-document.gif';
		}

		return self::getDocumentIcon($msFileName,$mbIsLink);
	}

	/**
	 * Retorna o �cone de um documento
	 *
	 * <p>Retorna o �cone de um documento sem a necessidade de uma nova consulta
	 * ao banco.</p>
	 * @access public
	 * @param string $psFileName Nome do arquivo do documento
	 * @param boolean $pbIsLink Indica se o documento tem um link ao inv�s de um arquivo
	 * @return string Nome do �cone
	 */
	public static function getDocumentIcon($psFileName,$pbIsLink){
		if($pbIsLink){
			return 'icon-link.gif';
		}elseif($psFileName){
			return ISMSLib::getIcon($psFileName);
		}else{
			return 'icon-wrong.gif';
		}
	}

	/**
	 * Seta o estado do contexto.
	 *
	 * <p>M�todo para setar o estado de um contexto.
	 * Se n�o for passado como par�metro o id do contexto,
	 * pega o id do objeto atual.</p>
	 * @access public
	 * @param integer $piState Estado do contexto
	 * @param integer $piContextId Id do contexto
	 */
	public function setContextState($piState, $piContextId = 0){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		$miId = ($piContextId?$piContextId:$this->ciContextId);
		if($piState==CONTEXT_STATE_DOC_PENDANT){
			$moDocumentApprover = new PMDocumentApprover();
			$moDocumentApprover->clearApprovals($miId);
			$moDocument = new PMDocument();
			$moDocument->setFieldValue('document_has_approved',false);
			$moDocument->update($miId);
		}
		parent::setContextState($piState,$miId);
	}

	/**
	 * Retorna a inst�ncia em desenvolvimento do documento.
	 *
	 * <p>Retorna a inst�ncia em desenvolvimento do documento.</p>
	 * @access public
	 * @param $piDocumentId Id do documento. Por default pega do pr�prio objeto.
	 * @return PMDocInstance A inst�ncia em desenvolvimento do documento
	 */
	public function getDeveloppingInstance($piDocumentId=0){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		if($piDocumentId){
			$miDocumentId = $piDocumentId;
			$moDocument = new PMDocument();
			$moDocument->fetchById($piDocumentId);
			$miCurrentVersion = $moDocument->getFieldValue('document_current_version');
		}else{
			$miDocumentId = $this->getFieldValue('document_id');
			$miCurrentVersion = $this->getFieldValue('document_current_version');
		}
		$moDocInstance = null;
		if($miCurrentVersion){
			// Testa se o documento tem uma vers�o em revis�o.
			$moDocInstance = new QueryDocumentRevision(FWDWebLib::getConnection());
			$moDocInstance->setDocument($miDocumentId);
			$moDocInstance->makeQuery();
			$moDocInstance->executeQuery();
			if($moDocInstance->fetch()){
				$miInstanceId = $moDocInstance->getFieldValue('doc_instance_id');
			}else{ // N�o tem vers�o em revis�o, � a primeira vers�o.
				$miInstanceId = $miCurrentVersion;
			}
			$moDocInstance = new PMDocInstance();
			$moDocInstance->fetchById($miInstanceId);
		}
		return $moDocInstance;
	}

	/**
	 * Retorna a inst�ncia publicada do documento.
	 *
	 * <p>Retorna a inst�ncia publicada do documento (a mais atual entre as que
	 * foram aprovadas).</p>
	 * @access public
	 * @return PMDocInstance A inst�ncia publicada do documento
	 */
	public function getPublishedInstance(){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		$moDocInstance = null;
		$miCurrentVersion = $this->getFieldValue('document_current_version');
		if($miCurrentVersion){
			$moDocInstance = new PMDocInstance();
			$moDocInstance->fetchById($miCurrentVersion);
		}
		return $moDocInstance;
	}

	public function userCanEdit($piDocumentId,$piUserResponsible = 0){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		$miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
		$moDocument = new PMDocument();
		$moDocument->fetchById($piDocumentId);
		//adicionado o mainApprover no teste abaixo porque se o documento estiver em desenvolvimento e muda-se o author do documento
		//o mainApprover que fica com o menu de publicar o documento e n�o o author, assim dava erro de permiss�o ao tentar pulbicar
		//um documento em que o author e o mainApprover s�o diferentes
		if( ($miUserId == $moDocument->getFieldValue('document_author')) || ($miUserId == $moDocument->getFieldValue('document_main_approver')) ){
			if($moDocument->getContextState() == CONTEXT_STATE_DOC_DEVELOPING){
				return true;
			}
		}elseif($moDocument->getContextState() == CONTEXT_STATE_DOC_PENDANT){
			if($moDocument->isApprover($miUserId)) return true;
		}
		return false;
	}

	protected function userCanDelete($piDocumentId){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		$miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
		$moDocument = new PMDocument();
		$moDocument->fetchById($piDocumentId);
		if($miUserId == $moDocument->getFieldValue('document_main_approver')){
			return true;
		}else{
			return false;
		}
	}

	public function userCanRead($piDocumentId){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		$moHandler = new QueryDocumentCanRead(FWDWebLib::getConnection());
		$moHandler->setDocumentId($piDocumentId);
		$moHandler->makeQuery();
		$moHandler->executeQuery();
		return $moHandler->fetch();
	}

	protected function userCanManage($piDocumentId){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		return $this->userCanDelete($piDocumentId);
	}

	protected function getDeniedPermissionToManageMessage(){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		return FWDLanguage::getPHPStringValue('st_denied_permission_to_manage_document','Voc� n�o tem permiss�o para gerenciar o documento.');
	}

	protected function userCanInsert(){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		$miParentId = $this->getFieldValue('document_parent');
		if($miParentId){
			return $this->userCanEdit($miParentId);
		}else{
			// Testar ACLs aqui
			return true;
		}
	}

	protected function getDeniedPermissionToExecuteTaskMessage(){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		return FWDLanguage::getPHPStringValue('st_denied_permission_to_execute_task','Voc� n�o tem permiss�o para executar essa tarefa.');
	}

	public function testPermissionToExecuteTask($piTaskId){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		$miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
		$moTask = new WKFTask();
		$moTask->fetchById($piTaskId);
		$miActivity = $moTask->getFieldValue('task_activity');
		$miDocumentId = $moTask->getFieldValue('task_context_id');
		$moDocument = new PMDocument();
		$moDocument->fetchById($miDocumentId);
		switch($miActivity){
			case ACT_DOCUMENT_REVISION:{
				if($moDocument->getFieldValue('document_author')==$miUserId){
					if($moDocument->getContextState()==CONTEXT_STATE_DOC_APPROVED){
						return;
					}
				}
				break;
			}
			case ACT_DOCUMENT_APPROVAL:{
				if($moDocument->getContextState()==CONTEXT_STATE_DOC_PENDANT){
					if($moDocument->isApprover($miUserId)){
						if(!$moDocument->hasApproved($miUserId)){
							return;
						}
					}
				}
				break;
			}
		}
		trigger_error($this->getDeniedPermissionToExecuteTaskMessage(), E_USER_ERROR);
	}

	public function getSearchHandler(){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		$moHandler = new QuerySearchDocument(FWDWebLib::getConnection());
		return $moHandler;
	}

	/**
	 * Retorna o c�digo HTML do icone do contexto.
	 *
	 * <p>M�todo para retornar o c�digo HTML do contexto</p>
	 * @access public
	 * @param integer $piId Id do contexto
	 * @param boolean $pbIsFetched se o documento est� fetchiado
	 * @param boolean $pbForceBublished indica se � para for�ar a retornar o icone da inst�ncia publicada
	 * @return string contendo o c�digo HTML do icone do contexto
	 */
	public function getIconCode($piId, $pbIsFetched = false, $pbForceBublished = false){
		if(!$pbIsFetched){
			$this->fetchById($piId);
		}
		return ISMSLib::getIconCode($this->getIcon(0,true),-2,10);
	}

	/**
	 * Retorna o caminho para abrir a popup de edi��o desse contexto.
	 *
	 * <p>M�todo para retornar o caminho para abrir a popup de edi��o desse contexto.</p>
	 * @param integer $piContextId id do contexto
	 * @access public
	 * @return string caminho para abrir a popup de edi��o desse contexto
	 */
	public function getVisualizeEditEvent($piContextId,$psUniqId){
		$soSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
		$msPackagesPolicyPath = ($soSession->getMode()==MANAGER_MODE) ? "packages/policy/" : "../../packages/policy/";
		$msPopupId = "popup_document_edit";

		return "isms_open_popup('{$msPopupId}','{$msPackagesPolicyPath}{$msPopupId}.php?document=$piContextId','','true');"
		."soPopUpManager.getPopUpById('{$msPopupId}').setCloseEvent( function close_visualize_$psUniqId() {soPopUpManager.closePopUp('popup_visualize_$psUniqId');} );";
	}

	//
	/**
	* Retorna a descri��o do contexto.
	*
	* <p>M�todo para retornar a descri��o do contexto.</p>
	* @access public
	* @return string Descri��o do contexto
	*/
	public function getDescription() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
		return $this->getFieldValue('document_description');
	}

	public static function getPermissions(
	$piDocStatus,
	$piInstanceStatus,
	$pbIsMainApprover,
	$pbIsAuthor,
	$pbIsApprover,
	$pbIsReader,
	$pbHasApproved,
	$piCountApprovers,
	$pbIsLink,
	$psFilePath,
	$psLink,
	$piType
	){

		// Se n�o puder nem ver o documento, n�o tem permiss�o pra a��o nenhuma
		if(!(
		$pbIsMainApprover
		|| $pbIsAuthor
		|| (
		($pbIsApprover || $pbIsReader)
		&& ($piInstanceStatus==CONTEXT_STATE_DOC_PUBLISHED || $piInstanceStatus==CONTEXT_STATE_DOC_OBSOLETE)
		)
		|| (
		$pbIsApprover
		&& ($piInstanceStatus==CONTEXT_STATE_DOC_APPROVAL || $piInstanceStatus==CONTEXT_STATE_DOC_TO_BE_PUBLISHED)
		)
		)
		){
			return array();
		}

		$maAllowed = array();

		if($pbIsMainApprover){
			$maAllowed[] = 'delete';
			if($piInstanceStatus==CONTEXT_STATE_DOC_DEVELOPING || $piInstanceStatus==CONTEXT_STATE_DOC_REVISION){
				$maAllowed[] = 'edit_author';
				$maAllowed[] = 'edit_readers';
				$maAllowed[] = 'copy_readers';
				$maAllowed[] = 'edit_approvers';
				if(!$pbIsAuthor){
					$maAllowed[] = 'view_details';
					$maAllowed[] = 'view_references';
					$maAllowed[] = 'view_subdocs';
				}
				if(in_array($piType,self::$caAllowedCtxTypes)){
					$maAllowed[] = 'associate_components';
				}
			}
		}else{
			$maAllowed[] = 'view_readers';
			$maAllowed[] = 'view_approvers';
			$maAllowed[] = 'view_components';
		}

		if($pbIsAuthor && ($piInstanceStatus==CONTEXT_STATE_DOC_DEVELOPING || $piInstanceStatus==CONTEXT_STATE_DOC_REVISION)){
			$maAllowed[] = 'edit';
			$maAllowed[] = 'manage_subdocs';
			$maAllowed[] = 'previous_versions';
			$maAllowed[] = 'registers';
			$maAllowed[] = 'references';
			if(!$pbIsMainApprover){
				$maAllowed[] = 'view_readers';
				$maAllowed[] = 'view_approvers';
				$maAllowed[] = 'view_components';
			}
			if(($pbIsLink && $psLink) || (!$pbIsLink && is_file($psFilePath))){
				if($piCountApprovers==0 && $pbIsMainApprover){
					$maAllowed[] = 'publish';
				}else{
					$maAllowed[] = 'approval';
				}
			}
		}

		if($pbIsMainApprover || $pbIsApprover){
			if($piInstanceStatus==CONTEXT_STATE_DOC_PUBLISHED || $piInstanceStatus==CONTEXT_STATE_DOC_TO_BE_PUBLISHED){
				if($piDocStatus==CONTEXT_STATE_DOC_PUBLISHED){
					$maAllowed[] = 'revision';
				}
			}elseif($piInstanceStatus==CONTEXT_STATE_DOC_APPROVAL && !$pbHasApproved){
				$maAllowed[] = 'approve';
			}
		}

		if($piInstanceStatus==CONTEXT_STATE_DOC_PUBLISHED || $piInstanceStatus==CONTEXT_STATE_DOC_OBSOLETE){
			$maAllowed[] = 'comments';
		}

		if($piInstanceStatus!=CONTEXT_STATE_DOC_DEVELOPING && $piInstanceStatus!=CONTEXT_STATE_DOC_REVISION){
			$maAllowed[] = 'view_details';
			$maAllowed[] = 'view_references';
			$maAllowed[] = 'view_subdocs';
			$maAllowed[] = 'edit_readers';
			$maAllowed[] = 'view_readers';
			$maAllowed[] = 'view_approvers';
			$maAllowed[] = 'view_components';
		}

		/*
		 if(????????????){
		 $maAllowed[] = 'template_edit';
		 }

		 if(????????????){
		 $maAllowed[] = 'template_read';
		 }

		 // leitura dos templates de documentos relacionados ao tipo do documento
		 //if (in_array($miType,$this->caCtxWithTemplate)){
		 //  if((($miState==CONTEXT_STATE_DOC_DEVELOPING)||(($miState==CONTEXT_STATE_DOC_PENDANT&& $mbIsApprover)))&&($this->csGridName != 'grid_publish')){
		 //    $maAllowed[] = 'template_edit';
		 //  }
		 //}
		 /*/
		$maAllowed[] = 'template_edit';
		$maAllowed[] = 'template_read';
		//*/
		return array_unique($maAllowed);

	}

	/**
	 * Esse m�todo deve ser removido ou modificado para que os testes de permiss�o
	 * fiquem coerentes com o metodo getPermissions. O problema � que o
	 * getPermissions � por inst�ncia e esse, a princ�pio, � por documento.
	 */
	public function __call($psMethodName,$paArguments){
		if(substr($psMethodName,0,16)=='testPermissionTo'){
			return true;
		}else{
			trigger_error("Method '$psMethodName' is not defined.",E_USER_ERROR);
		}
	}

}

?>