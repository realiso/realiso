<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */


/**
 * Classe PMProcessUser
 *
 * <p>Classe que representa a tabela que relaciona usu�rios e processos.</p>
 * @package ISMS
 * @subpackage classes
 */
class PMProcessUser extends ISMSTable {
	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe PMProcessUser.</p>
	 * @access public
	 */
	public function __construct() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		parent::__construct("pm_process_user");

		$this->coDataset->addFWDDBField(new FWDDBField("fkProcess", 	"process_id", DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("fkUser", 			"user_id", DB_NUMBER));
	}

	/**
	 * Insere rela��es entre um processo e diversos usu�rios.
	 *
	 * <p>M�todo para inserir rela��es entre um processo e diversos usu�rios.</p>
	 * @access public
	 * @param integer $piProcessId Id do processo
	 * @param array $paUsersIds Ids dos usu�rios
	 */
	public function insertUsers($piProcessId, $paUsers) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		$this->setFieldValue('process_id', $piProcessId);
		foreach ($paUsers as $piUserId) {
			$this->setFieldValue('user_id', $piUserId);
			$this->insert();
		}
	}

	/**
	 * Deleta rela��es entre um processo e diversos usu�rios.
	 *
	 * <p>M�todo para deletar rela��es entre um processo e diversos usu�rios.</p>
	 * @access public
	 * @param integer $piProcessId Id do processo
	 * @param integer $paUsers Ids dos usu�rios
	 */
	public function deleteUsers($piProcessId, $paUsers) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		if (count($paUsers)) {
			$this->createFilter($piProcessId, 'process_id');
			foreach ($paUsers as $piUserId)
			$this->createFilter($piUserId, 'user_id', 'in');
			$this->delete();
		}
	}

	/**
	 * Retorna os usu�rios de um processo.
	 *
	 * <p>M�todo para retornar os usu�rios de um processo.</p>
	 * @access public
	 * @param integer $piProcessId Id do processo
	 * @return array $paUsers Ids dos usu�rios
	 */
	public function getUsersFromProcess($piProcessId) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

		$this->createFilter($piProcessId,'process_id');
		$this->select();
		$maUsers = array();
		while ($this->fetch()) $maUsers[] = $this->getFieldValue('user_id');
		return $maUsers;
	}
}
?>