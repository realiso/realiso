<?php

include_once $handlers_ref . "QueryDocumentDeadlineAlert.php";
include_once $handlers_ref . "QueryDocumentDeadlineExpired.php";
include_once $handlers_ref . "QueryDocumentApprovers.php";

/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe DocumentSchedule.
 *
 * <p>Classe que manipula a revis�o (schedule) dos documentos.</p>
 * @package ISMS
 * @subpackage classes
 */
class DocumentSchedule extends ISMSTable {	
 
 private $ciUserId = 0;
 
 /**
  * Construtor.
  * 
  * <p>Construtor da classe ControlSchedule.</p>
  * @access public 
  */
  public function __construct($piUserId){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
		$this->ciUserId = $piUserId;
	}

 /**
  * Verifica o deadline dos documentos, disparando alertas quando for necess�rio.
  * 
  * <p>Verifica o deadline dos documentos, disparando alertas quando for necess�rio.</p>
  * @access public
  */
  public function checkDeadline(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    // verifica se o deadline est� pr�ximo e envia alerta
    $this->checkAlerts(new QueryDocumentDeadlineAlert(FWDWebLib::getConnection(),$this->ciUserId),WKF_ALERT_DOCUMENT_DEADLINE_NEAR);
    // verifica se o deadline est� atrasado e envia alerta
    $this->checkAlerts(new QueryDocumentDeadlineExpired(FWDWebLib::getConnection(),$this->ciUserId),WKF_ALERT_DOCUMENT_DEADLINE_LATE);
  }

	/**
  * Dispara Alertas de revis�o atrasada 
  * 
  * <p>M�todo que dispara Alertas de revis�o atrasada
  * @access private 
  * @param FWDQueryHandler $poQuery Identificador do documento
  * @param Integer $piStatus tipo do alerta que deve ser gerado
  */
	private function checkAlerts($poQuery,$piStatus){
		$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    		
		$poQuery->makeQuery();
		$moConfig = new ISMSConfig();
		$miTimestamp = ISMSLib::ISMSTime();
    $maApprovers=array();
		if($poQuery->executeQuery()){
			$moDataset = $poQuery->getDataset();
			while($moDataset->fetch()){
				$mbSendAlert = false;					
				switch($piStatus){
					case WKF_ALERT_DOCUMENT_DEADLINE_NEAR:
            $miDaysBefore = intval($moDataset->getFieldByAlias('days_before')->getValue());
            $miDeadline = ISMSLib::getTimestamp($moDataset->getFieldByAlias('deadline')->getValue());
            if(($miDeadline + 86400) <= ($miTimestamp + ($miDaysBefore*86400))){
  						//update do campo de envio de alerta de deadline pr�ximo
  						$moObj = new PMDocument();
  						$moObj->setFieldValue('flag_deadline_near',true);
  						$moObj->update($moDataset->getFieldByAlias('id')->getValue(),false);
  						//Somente envia alerta se o deadline ainda nao expirou. De qualquer forma, seta a flag.
              if ((ISMSLib::getTimestamp($moDataset->getFieldByAlias('deadline')->getValue()) + 86400) > $miTimestamp) {
                $moQueryApprovers = new QueryDocumentApprovers(FWDWebLib::getConnection(),$moDataset->getFieldByAlias('id')->getValue());
                $moQueryApprovers->makeQuery();
                $moQueryApprovers->executeQuery();
                $maApprovers = $moQueryApprovers->getApprovers();
                $mbSendAlert = true;
              }
  					}
					break;
          case WKF_ALERT_DOCUMENT_DEADLINE_LATE:
            //teste se o deadline est� atrasado.
            $miDeadline = ISMSLib::getTimestamp($moDataset->getFieldByAlias('deadline')->getValue());
            if(($miDeadline + 86400) < $miTimestamp ){
              //update do campo de envio de alerta de deadline atrasado
              $moObj = new PMDocument();
              $moObj->setFieldValue('flag_deadline_late',true);
              $moObj->update($moDataset->getFieldByAlias('id')->getValue(),false);
              $moQueryApprovers = new QueryDocumentApprovers(FWDWebLib::getConnection(),$moDataset->getFieldByAlias('id')->getValue());
              $moQueryApprovers->makeQuery();
              $moQueryApprovers->executeQuery();
              $maApprovers = $moQueryApprovers->getApprovers();
              $mbSendAlert = true;
            }
          break;
					default:
					break;	
				}
				if($mbSendAlert==true){
					foreach($maApprovers as $miApproverId) {
            $moAlert = new WKFAlert();
  					$moAlert->setFieldValue('alert_receiver_id',$miApproverId);				
  					$moAlert->setFieldValue('alert_date',ISMSLib::ISMSTime());
  					$moAlert->setFieldValue('alert_type',$piStatus);
  					$moAlert->setFieldValue('alert_justification',' ');
  					$moAlert->setFieldValue('alert_creator_id',$moConfig->getConfig(USER_CHAIRMAN));
  					$moAlert->setFieldValue('alert_context_id',$moDataset->getFieldByAlias('id')->getValue());
  					$moAlert->insert();
          }
				}
			}
		}
	}

}
?>