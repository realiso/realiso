<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

include_once $handlers_ref.'QueryDocumentRevision.php';

/**
 * Classe RMWorkflow.
 *
 * <p>Classe que manipula os estados do workflow.
 * O workflow � respons�vel por disparar as devidas tarefas.
 * � ele que sabe quais e quantas tarefas ser�o disparadas, de acordo
 * com o estado do contexto utilizado.</p>
 * @package ISMS
 * @subpackage classes
 */
class PMWorkflow extends Workflow {

 /**
  * Dispara uma mesma tarefa para v�rios usu�rios.
  *
  * <p>Dispara uma mesma tarefa para v�rios usu�rios.</p>
  * @access public
  * @param integer $piActivity Id da atividade
  * @param integer $piContextId Id do contexto
  * @param array $paReceivers Ids dos receptores da tarefa
  */
  public function dispatchTasks($piActivity, $piContextId, $paReceivers){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

    $miCreatorId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    $moTask = new WKFTask();
    $moTask->setFieldValue('task_context_id', $piContextId);
    $moTask->setFieldValue('task_creator_id', $miCreatorId);
    $moTask->setFieldValue('task_activity', $piActivity);
    $moTask->setFieldValue('task_date_created', ISMSLib::ISMSTime());
    $moTask->setFieldValue('task_is_visible', 1);
    foreach($paReceivers as $miReceiver){
      $moTask->setFieldValue('task_receiver_id',$miReceiver);
      $moTask->insert();
    }
  }

 /**
  * Dispara um mesmo alerta para v�rios usu�rios.
  *
  * <p>Dispara um mesmo alerta para v�rios usu�rios.</p>
  * @access public
  * @param integer $piAlertType Tipo de alerta
  * @param IWorkflow $poContext Objeto de contexto
  * @param string $psJustification Justificativa
  * @param integer $paReceivers Ids dos destinat�rios do alerta
  */
  public function dispatchAlerts($piAlertType, IWorkflow $poContext, $psJustification = '', $paReceivers,$miDate = 0){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

    $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    $moAlert = new WKFAlert();
    $moAlert->setFieldValue('alert_type', $piAlertType);
    $moAlert->setFieldValue('alert_context_id', $poContext->getId());
    $moAlert->setFieldValue('alert_creator_id', $miUserId);
    $moAlert->setFieldValue('alert_justification', $psJustification);

    if($piAlertType == WKF_ALERT_DOCUMENT_NEW_VERSION || $piAlertType == WKF_ALERT_DOCUMENT_PUBLISHED){
      $moAlert->setFieldValue('alert_date', $miDate);
    }else{
      $moAlert->setFieldValue('alert_date', ISMSLib::ISMSTime());
    }

    foreach($paReceivers as $miReceiver){
      $moAlert->setFieldValue('alert_receiver_id', $miReceiver);
      $moAlert->insert();
    }
  }

 /**
  * Avan�a o estado do contexto no workflow.
  *
  * <p>M�todo para avan�ar o estado do contexto no workflow.</p>
  * @access public
  *
  * @param IWorkflow $poDocument Objeto do documento
  * @param integer $piAction Tipo da a��o
  * @param string $psJustification Justificativa
  */
  public function stateForward(IWorkflow $poDocument, $piAction, $psJustification = ''){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);

    $miUserId = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserId();
    $miResponsibleId = $poDocument->getResponsible();
    $mbUserIsApprover = $poDocument->isApprover($miUserId);
    $mbUserEqualResponsible = ($miUserId == $miResponsibleId);
    $mbDeletedResponsible = ($miResponsibleId == 0);

    if($piAction==WKF_ACTION_DELETE){
      $poDocument->setContextState(CONTEXT_STATE_DELETED);
      return;
    }

    switch($poDocument->getContextState()){
      case CONTEXT_STATE_NONE:{
        $poDocument->setContextState(CONTEXT_STATE_DOC_DEVELOPING);
        return;
      }case CONTEXT_STATE_DOC_DEVELOPING:{
        if($piAction==WKF_ACTION_SEND_DOC_TO_APPROVE){
          $miId = $poDocument->getId();
          // vers�o+= 0.1
          $moInstance = $poDocument->getDeveloppingInstance();

          $moInstanceUp = new PMDocInstance();
          $moInstanceUp->setFieldValue('doc_instance_revision_version',$moInstance->getFieldValue('doc_instance_revision_version')+1);
          $moInstanceUp->setFieldValue('doc_instance_modify_comment',$psJustification);
          /*adicionado o campo abaixo somente para que o audit_log do sistema registre as informa��es corretas j� que a inst�nica nao est� fetchiada*/
          $moInstanceUp->setFieldValue('document_id',$moInstance->getFieldValue('document_id'));

          //setar a data de indo para aprova��o da instanica do documento
          $moInstanceUp->setFieldValue('doc_instance_begin_approver',ISMSLib::ISMSTime());


          $moInstanceUp->update($moInstance->getFieldValue('doc_instance_id'));

          // marca como n�o aprovado por ningu�m
          $moDocumentApprover = new PMDocumentApprover();
          $moDocumentApprover->clearApprovals($miId);
          $moDocument = new PMDocument();
          $moDocument->setFieldValue('document_has_approved',false);
          $moDocument->update($miId);
          // muda o estado
          $poDocument->setContextState(CONTEXT_STATE_DOC_PENDANT);
          // envia tarefa de aprova��o para os aprovadores
          $maApprovers = $poDocument->getApprovers();
          $maApprovers = array_diff($poDocument->getApprovers(),array($miUserId));
          $this->dispatchTasks($this->ciActivity,$miId,$maApprovers);
          $this->dispatchAlerts(WKF_ALERT_DOCUMENT_CREATION_COMMENT, $poDocument, $psJustification, $maApprovers, ISMSLib::ISMSTime());
        }
        return;
      }case CONTEXT_STATE_DOC_PENDANT:{
        if($piAction==WKF_ACTION_APPROVE){
          $moDocumentApprover = new PMDocumentApprover();
          $miDocumentId = $poDocument->getFieldValue('document_id');
          $moDocumentApprover->createFilter($miDocumentId,'document_id');
          $moDocumentApprover->select();
          $miNotApproved = 0;
          $mbIsApprover = false;
          while($moDocumentApprover->fetch()){
            if(!$moDocumentApprover->getFieldValue('has_approved')){
              $miNotApproved++;
              if($moDocumentApprover->getFieldValue('user_id')==$miUserId) $mbIsApprover = true;
            }
          }
          if(!$poDocument->getFieldValue('document_has_approved')) $miNotApproved++;
          if($miUserId==$poDocument->getFieldValue('document_main_approver')){
            $moDocument = new PMDocument();
            $moDocument->createFilter($miDocumentId,'document_id');
            $moDocument->setFieldValue('document_has_approved',true);
            $moDocument->update($miDocumentId);
            $miNotApproved--;
          }elseif($mbIsApprover){
            $moDocumentApprover = new PMDocumentApprover();
            $moDocumentApprover->createFilter($miUserId,'user_id');
            $moDocumentApprover->createFilter($miDocumentId,'document_id');
            $moDocumentApprover->setFieldValue('has_approved',true);
            $moDocumentApprover->update();
            $miNotApproved--;
          }
          if($miNotApproved==0){
            // vers�o = floor(vers�o+1)
            $moDevInstance = $poDocument->getDeveloppingInstance();
            $msComment = $moDevInstance->getFieldValue('doc_instance_modify_comment');
            $miMajorVersion = $moDevInstance->getFieldValue('doc_instance_major_version');
            $miInstanceId = $moDevInstance->getFieldValue('doc_instance_id');

            $moPubInstance = new PMDocInstance();

            if(ISMSLib::getConfigById(GENERAL_MANUAL_DATA_CONTROL)){
              /*caso a data de publica��o seja uma data no passado, utilizar ela como a data de inicio de produ��o da inst�ncia do documento*/
              $moDoc = new PMDocument();
              $moDoc->fetchById($miDocumentId);
              $miDocPublish = ISMSLib::getTimestamp($moDoc->getFieldValue('document_date_production'));
              $miNow = ISMSLib::ISMSTime();
              $moPubInstance->setFieldValue('doc_instance_begin_production',min($miNow,$miDocPublish));
            }else{
              $moPubInstance->setFieldValue('doc_instance_begin_production',ISMSLib::ISMSTime());
            }

            $moPubInstance->setFieldValue('doc_instance_major_version',$miMajorVersion+1);
            $moPubInstance->setFieldValue('doc_instance_revision_version',0);
            /*adicionado a linha abaixo somente para que o audit_log obtenha registre as informa��es corretas, j� que a inst�ncia do doc n�o esta fetchiada*/
            $moPubInstance->setFieldValue('document_id',$moDevInstance->getFieldValue('document_id'));
            $moPubInstance->update($miInstanceId);

            $moDocument = new PMDocument();
            $moDocument->setFieldValue('document_current_version',$miInstanceId);
            $moDocument->update($miDocumentId);

            // Manda alerta pros leitores
            $moDocumentReaders = new PMDocumentReader();
            $maReaders = $moDocumentReaders->getAllowedUsers($miDocumentId);
            $moDocDate = new PMDocument();
            $moDocDate->fetchById($miDocumentId);

            /*quando o documento ter� publica��o futura, utilizar a data de publica��o futura como data do alerta enviado*/
            $miDate = max(ISMSLib::ISMSTime(),ISMSLib::getTimestamp($moDocDate->getFieldValue('document_date_production')));
            if(!$miMajorVersion){
              $this->dispatchAlerts(WKF_ALERT_DOCUMENT_PUBLISHED,$poDocument,$msComment,$maReaders,$miDate);
            }else{
              $this->dispatchAlerts(WKF_ALERT_DOCUMENT_NEW_VERSION,$poDocument,$msComment,$maReaders,$miDate);
            }

            $moDocument = new PMDocument();
            $moDocument->fetchById($miDocumentId);
            // Cria um task_schedule, para "ativar" o schedule de revis�o, caso ele exista
            if($moDocument->getFieldValue('document_schedule')){
              $moTaskSchedule = new WKFTaskSchedule();
              $moTaskSchedule->setFieldValue('task_schedule_schedule',$poDocument->getFieldValue('document_schedule'));
              $moTaskSchedule->setFieldValue('task_schedule_context',$poDocument->getFieldValue('document_id'));
              $moTaskSchedule->setFieldValue('task_schedule_activity',ACT_DOCUMENT_REVISION);
              $moTaskSchedule->setFieldValue('task_schedule_alert_type',WKF_ALERT_DOCUMENT_REVISION_LATE);
              $moTaskSchedule->setFieldValue('task_schedule_alert_sent',false);
              $moTaskSchedule->insert();
            }
            // muda o estado do documento
            $poDocument->setContextState(CONTEXT_STATE_DOC_APPROVED);
          }

        }elseif($piAction==WKF_ACTION_DENY){
          // envia alerta de nega��o pro elaborador
          if(!$mbDeletedResponsible && !$mbUserEqualResponsible){
            $this->dispatchAlert(WKF_ALERT_DENIED,$poDocument,$psJustification);
          }
          // deleta tarefas de aprova��o do documento
          $miDocumentId = $poDocument->getFieldValue('document_id');
          $moTask = new WKFTask();
          $moTask->createFilter($miDocumentId,'task_context_id');
          $moTask->createFilter(null,'task_date_accomplished','null');
          $moTask->delete();
          $poDocument->setContextState(CONTEXT_STATE_DOC_DEVELOPING);
        }
        return;
      }case CONTEXT_STATE_DOC_APPROVED:{
        if($piAction==WKF_ACTION_DOC_REVISION){
          /*DELETA TASKS N�O REALIZADAS REFERENTES A ESTE CONTEXTO - nescess�rio pois quando existe task de revis�o de documento
          e o documento � enviado para revis�o pelo menu de contexto na nav_publish, devemos deletar essas tasks pois o documento n�o
          esta mais no estado de publicado, onde se executa as tasks de revis�o do documento*/
          $moTask = new WKFTask();
          $moTask->createFilter($poDocument->getId(),'task_context_id');
          $moTask->createFilter(ACT_DOCUMENT_REVISION,'task_activity');
          $moTask->createFilter(1,'task_is_visible');
          $moTask->delete();

          // Deleta o task_schedule para "desativar" a cria��o de tarefas de revis�o quando o documento n�o est� publicado
          $moTaskSchedule = new WKFTaskSchedule();
          $moTaskSchedule->createFilter($poDocument->getId(),'task_schedule_context');
          $moTaskSchedule->createFilter(ACT_DOCUMENT_REVISION,'task_schedule_activity');
          $moTaskSchedule->delete();

          $documentId = $poDocument->getId();
          $documentScheduleId = $poDocument->getFieldValue('document_schedule');
          
          $pmDoc = new PMDocument();
          $pmDoc->setFieldValue('document_schedule', 'null');
          $pmDoc->update($documentId);

          if($documentScheduleId){
            $wkfSched = new WKFSchedule();
            $wkfSched->delete($documentScheduleId);
          }
          
          // Envia alerta para o elaborador revisar o documento com justificativa de pq revisar
          $this->dispatchAlert(WKF_ALERT_DOCUMENT_REVISION,$poDocument,$psJustification);
          $miCurrentVersion = $poDocument->getFieldValue('document_current_version');

          $moCurrentVersion = new PMDocInstance();
          $moCurrentVersion->fetchById($miCurrentVersion);
          
          $msTime = ISMSLib::ISMSTime();
          $moCurrentVersionUp = new PMDocInstance();
          $moCurrentVersionUp->setFieldValue('doc_instance_end_production',$msTime);
          $moCurrentVersionUp->setFieldValue('doc_instance_revision_justif',$psJustification);
          /*adicionado os 3 campos abaixo somente para que o audit_log do sistema registre as informa��es corretas j� que a inst�nica nao est� fetchiada*/
          $moCurrentVersionUp->setFieldValue('doc_instance_major_version',$moCurrentVersion->getFieldValue('doc_instance_major_version'));
          $moCurrentVersionUp->setFieldValue('doc_instance_revision_version',$moCurrentVersion->getFieldValue('doc_instance_revision_version'));
          $moCurrentVersionUp->setFieldValue('document_id',$moCurrentVersion->getFieldValue('document_id'));

          $moCurrentVersionUp->update($miCurrentVersion);

          // Cria nova inst�ncia pra ser revisada. A antiga continua em produ��o at� a nova ser aprovada.
          $moRevisionVersion = new PMDocInstance();
          $moRevisionVersion->setFieldValue('document_id',$moCurrentVersion->getFieldValue('document_id'));
          $moRevisionVersion->setFieldValue('doc_instance_major_version',$moCurrentVersion->getFieldValue('doc_instance_major_version'));
          $moRevisionVersion->setFieldValue('doc_instance_revision_version',1);
          $moRevisionVersion->setFieldValue('doc_instance_begin_revision', $msTime);
          
          //atualiza a data de publica��o da nova isntancia
          //$moCurrentVersion->setFieldValue('doc_instance_begin_production',ISMSLib::ISMSTime());
          $moRevisionVersion->insert();
          $poDocument->setContextState(CONTEXT_STATE_DOC_DEVELOPING);
        }
        return;
      }case CONTEXT_STATE_DELETED:{
        if($piAction==WKF_ACTION_UNDELETE){
          $poDocument->setContextState(CONTEXT_STATE_DOC_DEVELOPING);
        }
        return;
      }
    }
    trigger_error("The action '$piAction' at state '{$poDocument->getContextState()}' is not implemented.",E_USER_ERROR);
  }
}
?>