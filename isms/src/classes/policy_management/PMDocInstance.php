<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe PMDocInstance
 *
 * <p>Classe que insere uma inst�ncia de documento </p>
 * @package ISMS
 * @subpackage classes
 */
class PMDocInstance extends ISMSContext {

  protected $cbGetContent;
  /**
  * Construtor.
  *
  * <p>Construtor da classe PMDocInstance.</p>
  * @access public
  */
  public function __construct($pbGetContent=false,$pbExecByTrash = false){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $this->cbGetContent = $pbGetContent;
    if ($pbGetContent){
      //criado o execByTrash pois a view activeWithContent filtra os deletados,
      //assim n�o era possivel deletar um docInstance da lixeira
      if($pbExecByTrash)
        parent::__construct('view_pm_di_with_content');
      else{
        parent::__construct('view_pm_di_active_with_content');
      }
    }else{
      parent::__construct('pm_doc_instance');
    }  
    $this->csAliasId = 'doc_instance_id';
    
    if ($pbGetContent)
      $this->ciContextType = CONTEXT_DOCUMENT_INSTANCE_WITH_CONTENT;
    else
      $this->ciContextType = CONTEXT_DOCUMENT_INSTANCE;

    $this->coDataset->addFWDDBField(new FWDDBField('fkContext',             'doc_instance_id',              DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('fkDocument',            'document_id',                  DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('nMajorVersion',         'doc_instance_major_version',   DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('nRevisionVersion',      'doc_instance_revision_version',DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('tRevisionJustification','doc_instance_revision_justif', DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField('sPath',                 'doc_instance_path',            DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField('tModifyComment',        'doc_instance_modify_comment',  DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField('dBeginProduction',      'doc_instance_begin_production',DB_DATETIME));
    $this->coDataset->addFWDDBField(new FWDDBField('dEndProduction',        'doc_instance_end_production',  DB_DATETIME));
    $this->coDataset->addFWDDBField(new FWDDBField('sFileName',             'doc_instance_file_name',       DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField('bIsLink',               'doc_instance_is_link',         DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('sLink',                 'doc_instance_link',            DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField('dBeginApprover',        'doc_instance_begin_approver',  DB_DATETIME));//usado
    $this->coDataset->addFWDDBField(new FWDDBField('dBeginRevision',        'doc_instance_begin_revision',  DB_DATETIME));
    $this->coDataset->addFWDDBField(new FWDDBField('nFileSize',             'doc_instance_file_size',       DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('sManualVersion',        'doc_instance_manual_version',      DB_STRING));
    
    if ($pbGetContent) 
      $this->coDataset->addFWDDBField(new FWDDBField('tContent',            'doc_instance_content',         DB_STRING));

    $this->caSearchableFields = array('doc_instance_revision_justif', 'doc_instance_modify_comment', 'doc_instance_file_name');
    if ($pbGetContent) 
      $this->caSearchableFields[] =  'doc_instance_content';
  }
  
  /**
  * Retorna a vers�o do documento.
  *
  * <p>M�todo para retornar a vers�o do documento.</p>
  * @access public
  * @return string Vers�o do documento
  */
  public function getVersion() {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    $msVersion="";
    if (ISMSLib::getConfigById(GENERAL_DOCUMENTS_MANUAL_VERSIONING)) {
      $msVersion = $this->getFieldValue('doc_instance_manual_version');
    } 
    if ($msVersion)
      return $msVersion;
    else {
      $miMainVerion = intval($this->getFieldValue('doc_instance_major_version'));
      $miSubVersion = intval($this->getFieldValue('doc_instance_revision_version'));
      if(!$miMainVerion)
        $miMainVerion = '0';
      if(!$miSubVersion)
        $miSubVersion = '0';
  
      return $miMainVerion.".".$miSubVersion;
    }
  }

  /**
  * Retorna o nome do contexto.
  *
  * <p>M�todo para retornar o nome do contexto.</p>
  * @access public
  * @return string Nome do contexto
  */
  public function getName($pbWithVersion=true){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $moPMDocument = new PMDocument();
    $moPMDocument->fetchById($this->getFieldValue('document_id'));
    $msName = $moPMDocument->getFieldValue('document_name');
    if ($pbWithVersion)
      $msName .= " (".$this->getVersion().")"; 
    return $msName;
  }

  /**
  * Retorna o label do contexto.
  *
  * <p>M�todo para retornar o label do contexto.</p>
  * @access public
  * @return string Label do contexto
  */
  public function getLabel(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return FWDLanguage::getPHPStringValue('mx_document_instance', 'Inst�ncia de Documento');
  }
  
  /**
   * N�o permite a inser��o quando usado em conjunto da leitura do conte�do do arquivo.
   */
  public function insert($pbReturnId = false) {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    //inicializa a data de publica��o da inst�ncia como o time atual na hora da inser��o da inst�ncia
    //$this->setFieldValue('doc_instance_begin_published',ISMSLib::ISMSTime());
    
    if ($this->cbGetContent) {
      trigger_error('Insert an instance, while using its contents, is not allowed.',E_USER_ERROR);
    } else {
      return parent::insert($pbReturnId);
    }
  }
  
  /**
  * Retorna o id do usu�rio respons�vel pelo contexto.
  *
  * <p>M�todo para retornar id do usu�rio respons�vel pelo contexto.</p>
  * @access public
  * @return integer Id do respons�vel
  */
  public function getResponsible(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return $this->getCreator();
  }

  protected function userCanInsert(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return true;
  }
  
  protected function userCanEdit($piInstanceId, $piResponsible = 0){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $moInstance = new PMDocInstance();
    $moInstance->fetchById($piInstanceId);
    $moDocument = new PMDocument();
    return $moDocument->userCanEdit($moInstance->getFieldValue('document_id'));
  }

  protected function userCanDelete($piInstanceId){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return $this->userCanEdit($piInstanceId);
  }

  protected function userCanRead($piInstanceId){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $moInstance = new PMDocInstance();
    $moInstance->fetchById($piInstanceId);
    $moDocument = new PMDocument();
    return $moDocument->userCanRead($moInstance->getFieldValue('document_id'));
  }
}
?>