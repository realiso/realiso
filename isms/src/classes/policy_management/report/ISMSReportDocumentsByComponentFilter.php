<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportDocumentsByComponentFilter.
 *
 * <p>Classe que implementa o filtro do relatório de documentos por componente do SGSI.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportDocumentsByComponentFilter extends FWDReportFilter {

  protected $caDocType = array();

  public function setDocType($paDocType){
    $this->caDocType = $paDocType;
  }

  public function getDocType(){
    return $this->caDocType;
  }

  public function getSummary(){
    
    $maFilters = array();
    
    $maFilter = array(
      'name' => FWDLanguage::getPHPStringValue('report_filter_doctype_cl','Tipo de Documento:'),
      'items' => array()
    );
    if(count($this->caDocType)){
      foreach($this->caDocType as $miId){
        $maFilter['items'][] = $msLabel = ISMSContextObject::getContextObject($miId)->getLabel();
      }
    }else{
      $maFilter['items'][] = FWDLanguage::getPHPStringValue('report_filter_all_doctype','Todos');
    }
    $maFilters[] = $maFilter;
    
    return $maFilters;
  }

}

?>