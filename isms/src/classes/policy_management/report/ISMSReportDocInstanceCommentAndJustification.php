<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportDocInstanceCommentAndJustification.
 *
 * <p>Classe que implementa o relat�rio de coment�rios de aprova��o e
 * justificativas de revis�o de inst�ncias de documentos.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportDocInstanceCommentAndJustification extends ISMSFilteredReport {
  
  public function __construct(){
    parent::__construct();
  }
  
 /**
  * Inicializa o relat�rio.
  *
  * <p>M�todo para inicializar o relat�rio.
  * Necess�rio para funcionar com o XML compilado.</p>
  * @access public
  */
  public function init(){
    parent::init();
    
	$col1 = new FWDDBField('d.fkContext'              ,'document_id'               ,DB_NUMBER);
	$col1->setLabel(' ');
	$col1->setShowColumn(false);
	
	$col2 = new FWDDBField('d.sName'                  ,'document_name'             ,DB_STRING);
	$col2->setLabel(FWDLanguage::getPHPStringValue('mx_document','Documento'));
	$col2->setShowColumn(true);		

	$col3 = new FWDDBField('di.fkContext'             ,'instance_id'               ,DB_NUMBER);
	$col3->setLabel(' ');
	$col3->setShowColumn(false);
	
	$col4 = new FWDDBField('di.nMajorVersion'         ,'instance_major_version'    ,DB_NUMBER);
	$col4->setLabel(FWDLanguage::getPHPStringValue('rs_version','Vers�o'));
	$col4->setShowColumn(true);		

	$col5 = new FWDDBField('di.nRevisionVersion'      ,'instance_revision_version' ,DB_NUMBER);
	$col5->setLabel(FWDLanguage::getPHPStringValue('cb_revision','Revis�o'));
	$col5->setShowColumn(true);
	
	$col6 = new FWDDBField('di.sManualVersion'        ,'instance_manual_version'   ,DB_STRING);
	$col6->setLabel(FWDLanguage::getPHPStringValue('rs_manual_version','Vers�o manual'));
	$col6->setShowColumn(true);     
    
	$col7 = new FWDDBField('di.sFileName'             ,'instance_file_name'        ,DB_STRING);
	$col7->setLabel(' ');
	$col7->setShowColumn(false);   

	$col8 = new FWDDBField('di.bIsLink'               ,'instance_is_link'          ,DB_NUMBER);
	$col8->setLabel(' ');
	$col8->setShowColumn(false);   

	$col9 = new FWDDBField('di.tModifyComment'        ,'instance_modify_comment'   ,DB_STRING);
	$col9->setLabel(FWDLanguage::getPHPStringValue('rs_header_instance_modify_comment','Coment�rio de Modifica��o'));
	$col9->setShowColumn(true);       
    
	$col10 = new FWDDBField('di.tRevisionJustification','instance_rev_justification',DB_STRING);
	$col10->setLabel(FWDLanguage::getPHPStringValue('rs_header_instance_rev_justification','Justificativa de Revis�o'));
	$col10->setShowColumn(true);     	
	
    $this->coDataSet->addFWDDBField($col1);
    $this->coDataSet->addFWDDBField($col2);
    $this->coDataSet->addFWDDBField($col3);
    $this->coDataSet->addFWDDBField($col4);
    $this->coDataSet->addFWDDBField($col5);
    $this->coDataSet->addFWDDBField($col6);
    $this->coDataSet->addFWDDBField($col7);
    $this->coDataSet->addFWDDBField($col8);
    $this->coDataSet->addFWDDBField($col9);
    $this->coDataSet->addFWDDBField($col10);
    
  }
  
 /**
  * Executa a query do relat�rio.
  *
  * <p>M�todo para executar a query do relat�rio.</p>
  * @access public
  */
  public function makeQuery(){
    $maDocuments = $this->coFilter->getDocuments();
    
    $maFilters = array();
    
    $maFilters[] = 'di.dBeginProduction IS NOT NULL';
    
    if(count($maDocuments)>0){
      $maFilters[] = 'd.fkContext IN ('.implode(',',$maDocuments).')';
    }
    
    if(count($maFilters)==0){
      $msWhere = '';
    }else{
      $msWhere = ' WHERE '.implode(' AND ',$maFilters);
    }
    $this->csQuery = "SELECT
                        d.fkContext AS document_id,
                        d.sName AS document_name,
                        di.fkContext AS instance_id,
                        di.nMajorVersion AS instance_major_version,
                        di.nRevisionVersion AS instance_revision_version,
                        di.sManualVersion AS instance_manual_version,
                        di.sFileName AS instance_file_name,
                        di.bIsLink AS instance_is_link,
                        di.tModifyComment AS instance_modify_comment,
                        di.tRevisionJustification AS instance_rev_justification
                      FROM
                        view_pm_document_active d
                        JOIN pm_doc_instance di ON (di.fkdocument = d.fkcontext)
                      {$msWhere}
                      ORDER BY d.sName, di.fkContext";
    return parent::executeQuery();
  }

}

?>