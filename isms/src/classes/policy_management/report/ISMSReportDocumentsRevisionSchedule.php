<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportDocumentsRevisionSchedule.
 *
 * <p>Classe que implementa o relat�rio de agendamentos de revis�o dos documentos.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportDocumentsRevisionSchedule extends ISMSReport {
  
  public function __construct(){
    parent::__construct();
  }
  
 /**
  * Inicializa o relat�rio.
  *
  * <p>M�todo para inicializar o relat�rio.
  * Necess�rio para funcionar com o XML compilado.</p>
  * @access public
  */
  public function init(){
    parent::init();
    
	$col1 = new FWDDBField('d.fkContext' ,'document_id'         ,DB_NUMBER);
	$col1->setLabel(' ');
	$col1->setShowColumn(false);
	
	$col2 = new FWDDBField('d.sName'     ,'document_name'       ,DB_STRING);
	$col2->setLabel(FWDLanguage::getPHPStringValue('mx_document','Documento'));
	$col2->setShowColumn(true);		

	$col3 = new FWDDBField('di.sFileName','document_file_name'  ,DB_STRING);
	$col3->setLabel(' ');
	$col3->setShowColumn(false);
	
	$col4 = new FWDDBField('di.bIsLink'  ,'document_is_link'    ,DB_NUMBER);
	$col4->setLabel(' ');
	$col4->setShowColumn(false);		

	$col5 = new FWDDBField('d.fkSchedule','document_schedule_id',DB_NUMBER);
	$col5->setLabel(' ');
	$col5->setShowColumn(true);
    
    $this->coDataSet->addFWDDBField($col1);
    $this->coDataSet->addFWDDBField($col2);
    $this->coDataSet->addFWDDBField($col3);
    $this->coDataSet->addFWDDBField($col4);
    $this->coDataSet->addFWDDBField($col5);
    
  }
  
 /**
  * Executa a query do relat�rio.
  *
  * <p>M�todo para executar a query do relat�rio.</p>
  * @access public
  */
  public function makeQuery(){
    $this->csQuery = "SELECT
                        d.fkContext AS document_id,
                        d.sName AS document_name,
                        di.sFileName AS document_file_name,
                        di.bIsLink AS document_is_link,
                        d.fkSchedule AS document_schedule_id
                      FROM
                        view_pm_published_docs d
                        JOIN pm_doc_instance di ON (di.fkContext = d.fkCurrentVersion)
                      WHERE d.fkSchedule IS NOT NULL
                      ORDER BY d.sName";
    return parent::executeQuery();
  }

}

?>