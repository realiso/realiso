<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportDocumentsByStateFilter.
 *
 * <p>Classe que implementa o filtro do relat�rio de documentos por status.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportDocumentsByStateFilter extends FWDReportFilter {

  protected $ciStatus = 0;

  protected $cbShowRegisterDocs = false;

  public function setStatus($piStatus){
    $this->ciStatus = $piStatus;
  }

  public function getStatus(){
    return $this->ciStatus;
  }

  public function setShowRegisterDocs($piShowRegisterDocs){
    $this->cbShowRegisterDocs = $piShowRegisterDocs;
  }

  public function getShowRegisterDocs(){
    return $this->cbShowRegisterDocs;
  }

  public function getSummary(){    
    $maFilters = array();
    
    $maFilters[] = array(
      'name' => FWDLanguage::getPHPStringValue('report_filter_status_cl','Status dos Documentos:'),
      'items' => array(($this->ciStatus ? ISMSContextObject::getContextStateAsString($this->ciStatus) : FWDLanguage::getPHPStringValue('rs_all_status','Todos')))
    );
    
    $maFilters[] = array(
      'name' => FWDLanguage::getPHPStringValue('report_filter_showregisterdocs_cl','Mostrar Documentos de Registro:'),
      'items' => array(($this->cbShowRegisterDocs ? FWDLanguage::getPHPStringValue('rs_yes','Sim') : FWDLanguage::getPHPStringValue('rs_no','N�o')))
    );
    
    return $maFilters;
  }

}

?>