<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe ISMSReportDocumentsByArea.
 *
 * <p>Classe que implementa o relat�rio de quantidade de documentos que cada �rea precisa ter acesso, identando as �reas.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportDocumentsByArea extends ISMSReport {
  
 /**
  * Inicializa o relat�rio.
  *
  * <p>M�todo para inicializar o relat�rio.
  * Necess�rio para funcionar com o XML compilado.</p>
  * @access public
  */
  public function init(){
    parent::init();
    
	$col1 = new FWDDBField('a.fkContext' ,'area_id'               ,DB_NUMBER);
	$col1->setLabel(' ');
	$col1->setShowColumn(false);
	
	$col2 = new FWDDBField('t.nLevel'    ,'area_level'            ,DB_NUMBER);
	$col2->setLabel(' ');
	$col2->setShowColumn(false);		

	$col3 = new FWDDBField('a.sName'     ,'area_name'             ,DB_STRING);
	$col3->setLabel(' ');
	$col3->setShowColumn(true);
	
	$col4 = new FWDDBField(''            ,'doc_count'             ,DB_NUMBER);
	$col4->setLabel(FWDLanguage::getPHPStringValue('mx_document_total','Quantidade de Documentos'));
	$col4->setShowColumn(true);		

	$col5 = new FWDDBField('a.nValue'    ,'area_value'            ,DB_NUMBER);
	$col5->setLabel(' ');
	$col5->setShowColumn(false);
	
	$col6 = new FWDDBField('d.fkContext' ,'document_id'           ,DB_NUMBER);
	$col6->setLabel(' ');
	$col6->setShowColumn(false);     
    
	$col7 = new FWDDBField('d.sName'     ,'document_name'         ,DB_STRING);
	$col7->setLabel(FWDLanguage::getPHPStringValue('mx_document','Documento'));
	$col7->setShowColumn(true);   

	$col8 = new FWDDBField('d.nType'     ,'document_type'         ,DB_NUMBER);
	$col8->setLabel(' ');
	$col8->setShowColumn(false);   

	$col9 = new FWDDBField('di.sFileName','doc_instance_file_name',DB_STRING);
	$col9->setLabel(' ');
	$col9->setShowColumn(false);     
    
	$col10 = new FWDDBField('di.bIsLink'  ,'doc_instance_is_link'  ,DB_NUMBER);
	$col10->setLabel(' ');
	$col10->setShowColumn(false);  	

	$col11 = new FWDDBField('t.pkId'      ,'tree_order'            ,DB_NUMBER);
	$col11->setLabel(' ');
	$col11->setShowColumn(false);  
		
    $this->coDataSet->addFWDDBField($col1);
    $this->coDataSet->addFWDDBField($col2);
    $this->coDataSet->addFWDDBField($col3);
    $this->coDataSet->addFWDDBField($col4);
    $this->coDataSet->addFWDDBField($col5);
    $this->coDataSet->addFWDDBField($col6);
    $this->coDataSet->addFWDDBField($col7);
    $this->coDataSet->addFWDDBField($col8);
    $this->coDataSet->addFWDDBField($col9);
    $this->coDataSet->addFWDDBField($col10);
    $this->coDataSet->addFWDDBField($col11);
    
  }
  
  /**
   * Executa a query do relat�rio.
   *
   * <p>M�todo para executar a query do relat�rio.</p>
   * @access public
   */
  public function makeQuery(){
    if($this->coFilter->getExpand()){
      $this->csQuery = "SELECT
                          a.fkContext as area_id,                          t.nLevel as area_level,                          a.sName as area_name,                          ac.doc_count as doc_count,                          a.nValue as area_value,                          d.fkContext as document_id,                          d.sName as document_name,                          d.nType as document_type,                          di.sFileName as doc_instance_file_name,                          di.bIsLink as doc_instance_is_link,
                          t.pkId as tree_order                        FROM (
                          SELECT
                            area_id,
                            count(doc_id) as doc_count
                          FROM (
                              SELECT                                a.fkContext as area_id,                                dc.fkDocument as doc_id                              FROM                                view_rm_area_active a                                LEFT JOIN view_pm_doc_context_active dc ON (dc.fkContext = a.fkContext)
                              
                              UNION
                              
                              SELECT
                                p.fkArea as area_id,
                                dc.fkDocument as doc_id
                              FROM
                                view_rm_process_active p
                                JOIN view_pm_doc_context_active dc ON (dc.fkContext = p.fkContext)
                              
                              UNION
                              
                              SELECT
                                p.fkArea as area_id,
                                dc.fkDocument as doc_id
                              FROM
                                view_rm_process_active p
                                JOIN view_rm_process_asset_active pa ON (pa.fkProcess = p.fkContext)
                                JOIN view_pm_doc_context_active dc ON (dc.fkContext = pa.fkAsset)
                              
                              UNION
                              
                              SELECT
                                p.fkArea as area_id,
                                dc.fkDocument as doc_id
                              FROM
                                view_rm_process_active p
                                JOIN view_rm_process_asset_active pa ON (pa.fkProcess = p.fkContext)
                                JOIN view_rm_risk_active r ON (r.fkAsset = pa.fkAsset)
                                JOIN view_rm_risk_control_active rc ON (rc.fkRisk = r.fkContext)
                                JOIN view_pm_doc_context_active dc ON (dc.fkContext = rc.fkControl)
                            ) t
                          GROUP BY area_id
                        ) ac LEFT JOIN (
                          SELECT                            a.fkContext as area_id,                            dc.fkDocument as doc_id                          FROM                            view_rm_area_active a                            JOIN view_pm_doc_context_active dc ON (dc.fkContext = a.fkContext)
                          
                          UNION
                          
                          SELECT
                            p.fkArea as area_id,
                            dc.fkDocument as doc_id
                          FROM
                            view_rm_process_active p
                            JOIN view_pm_doc_context_active dc ON (dc.fkContext = p.fkContext)
                          
                          UNION
                          
                          SELECT
                            p.fkArea as area_id,
                            dc.fkDocument as doc_id
                          FROM
                            view_rm_process_active p
                            JOIN view_rm_process_asset_active pa ON (pa.fkProcess = p.fkContext)
                            JOIN view_pm_doc_context_active dc ON (dc.fkContext = pa.fkAsset)
                          
                          UNION
                          
                          SELECT
                            p.fkArea as area_id,
                            dc.fkDocument as doc_id
                          FROM
                            view_rm_process_active p
                            JOIN view_rm_process_asset_active pa ON (pa.fkProcess = p.fkContext)
                            JOIN view_rm_risk_active r ON (r.fkAsset = pa.fkAsset)
                            JOIN view_rm_risk_control_active rc ON (rc.fkRisk = r.fkContext)
                            JOIN view_pm_doc_context_active dc ON (dc.fkContext = rc.fkControl)
                        ) ad ON (ad.area_id = ac.area_id)
                        JOIN rm_area a ON (a.fkContext = ac.area_id)
                        JOIN ".FWDWebLib::getFunctionCall('get_area_tree(0,0)')." t ON (t.fkContext = a.fkContext)
                        LEFT JOIN pm_document d ON (d.fkContext = ad.doc_id)
                        LEFT JOIN view_pm_doc_instance_active di ON (di.fkContext = d.fkCurrentVersion)                        ORDER BY t.pkId, area_name, document_name";
    }else{
      $this->csQuery = "SELECT
                          a.fkContext as area_id,                          t.nLevel as area_level,                          a.sName as area_name,                          ac.doc_count as doc_count,                          a.nValue as area_value,
                          t.pkId as tree_order                        FROM (
                          SELECT
                            area_id,
                            count(doc_id) as doc_count
                          FROM (
                              SELECT                                a.fkContext as area_id,                                dc.fkDocument as doc_id                              FROM                                view_rm_area_active a                                LEFT JOIN view_pm_doc_context_active dc ON (dc.fkContext = a.fkContext)
                              
                              UNION
                              
                              SELECT
                                p.fkArea as area_id,
                                dc.fkDocument as doc_id
                              FROM
                                view_rm_process_active p
                                JOIN view_pm_doc_context_active dc ON (dc.fkContext = p.fkContext)
                              
                              UNION
                              
                              SELECT
                                p.fkArea as area_id,
                                dc.fkDocument as doc_id
                              FROM
                                view_rm_process_active p
                                JOIN view_rm_process_asset_active pa ON (pa.fkProcess = p.fkContext)
                                JOIN view_pm_doc_context_active dc ON (dc.fkContext = pa.fkAsset)
                              
                              UNION
                              
                              SELECT
                                p.fkArea as area_id,
                                dc.fkDocument as doc_id
                              FROM
                                view_rm_process_active p
                                JOIN view_rm_process_asset_active pa ON (pa.fkProcess = p.fkContext)
                                JOIN view_rm_risk_active r ON (r.fkAsset = pa.fkAsset)
                                JOIN view_rm_risk_control_active rc ON (rc.fkRisk = r.fkContext)
                                JOIN view_pm_doc_context_active dc ON (dc.fkContext = rc.fkControl)
                            ) t
                          GROUP BY area_id
                        ) ac
                        JOIN rm_area a ON (a.fkContext = ac.area_id)
                        JOIN ".FWDWebLib::getFunctionCall('get_area_tree(0,0)')." t ON (t.fkContext = a.fkContext)
                        ORDER BY t.pkId, area_name";
    }
    return parent::executeQuery();
  }
  
 /**
  * Desenha o cabe�alho do relat�rio.
  *
  * <p>M�todo para desenhar o cabe�alho do relat�rio.</p>
  * @access public
  */
  public function drawHeader(){
    foreach($this->caHeaders as $moHeader){
      if($moHeader->getAttrName()=='header_filter'){
        $moHeaderFilter = $moHeader;
      }elseif($moHeader->getAttrName()=='header_filter_item'){
        $moHeaderFilterItem = $moHeader;
      }elseif($moHeader->getAttrName()=='report_header_blank'){
        $moHeaderBlank = $moHeader;
      }else{
        $this->coWriter->drawLine($moHeader,array());
      }
    }
    if($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());
    
    $moFilterText = FWDWebLib::getObject('header_filter_text');
    $moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
    $maFilters = $this->coFilter->getSummary();
    foreach($maFilters as $maFilter){
      $moFilterText->setValue($maFilter['name']);
      $this->coWriter->drawLine($moHeaderFilter,array());
      foreach($maFilter['items'] as $msFilterItem){
        $moFilterItemText->setValue($msFilterItem);
        $this->coWriter->drawLine($moHeaderFilterItem,array());
      }
    }
    $this->coWriter->drawLine($moHeaderBlank,array());
  }

}
?>