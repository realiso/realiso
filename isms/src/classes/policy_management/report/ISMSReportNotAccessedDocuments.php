<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe ISMSReportNotAccessedDocuments.
 *
 * <p>Classe que implementa o relat�rio de documentos que ainda n�o foram acessados por todas as pessoas que precisam acessar.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportNotAccessedDocuments extends ISMSReport {
  
 /**
  * Inicializa o relat�rio.
  *
  * <p>M�todo para inicializar o relat�rio.
  * Necess�rio para funcionar com o XML compilado.</p>
  * @access public
  */
  public function init(){
    parent::init();
    
	$col1 = new FWDDBField('d.fkContext' ,'doc_id'                ,DB_NUMBER);
	$col1->setLabel(' ');
	$col1->setShowColumn(false);
	
	$col2 = new FWDDBField('d.sName'     ,'doc_name'              ,DB_STRING);
	$col2->setLabel(FWDLanguage::getPHPStringValue('mx_document','Documento'));
	$col2->setShowColumn(true);		

	$col3 = new FWDDBField('di.sFileName','doc_instance_file_name',DB_STRING);
	$col3->setLabel(' ');
	$col3->setShowColumn(false);
	
	$col4 = new FWDDBField('di.bIsLink'  ,'doc_instance_is_link'  ,DB_NUMBER);
	$col4->setLabel(' ');
	$col4->setShowColumn(false);		

	$col5 = new FWDDBField('da.fkUser'   ,'doc_reader_id'         ,DB_NUMBER);
	$col5->setLabel(' ');
	$col5->setShowColumn(false);
	
	$col6 = new FWDDBField('u.sName'     ,'doc_reader_name'       ,DB_STRING);
	$col6->setLabel(FWDLanguage::getPHPStringValue('rs_user','Usu�rio'));
	$col6->setShowColumn(true);         
    
    $this->coDataSet->addFWDDBField($col1);
    $this->coDataSet->addFWDDBField($col2);
    $this->coDataSet->addFWDDBField($col3);
    $this->coDataSet->addFWDDBField($col4);
    $this->coDataSet->addFWDDBField($col5);
    $this->coDataSet->addFWDDBField($col6);
    
  }
  
  /**
   * Executa a query do relat�rio.
   *
   * <p>M�todo para executar a query do relat�rio.</p>
   * @access public
   */
  public function makeQuery(){
    
    $maFilters = array();

    if(count($maFilters)){
      $msWhere = ' AND '.implode(' AND ',$maFilters);
    }else{
      $msWhere = '';
    }

    $this->csQuery = "
    SELECT d.fkContext as doc_id,
       d.sName as doc_name,
       di.sFileName as doc_instance_file_name,
       di.bIsLink as doc_instance_is_link,
       reader.isms_user as doc_reader_id,
       u.sName as doc_reader_name
   FROM view_pm_published_docs d
   JOIN view_pm_doc_instance_active di ON (di.fkContext = d.fkCurrentVersion)
   JOIN ( 
      SELECT d.fkContext as doc, d.fkAuthor as isms_user FROM view_pm_document_active d
      UNION
      SELECT d.fkContext as doc, d.fkMainApprover as isms_user FROM view_pm_document_active d
      UNION
      SELECT d.fkDocument as doc, d.fkUser as isms_user FROM view_pm_doc_readers_active d
  ) reader ON (reader.doc = d.fkContext)
  JOIN view_isms_user_active u ON (u.fkContext = reader.isms_user)
   WHERE d.fkContext NOT IN (
     SELECT d.fkContext FROM view_pm_document_active d
     JOIN view_pm_doc_instance_active di ON (di.fkContext = d.fkCurrentVersion)
     JOIN pm_doc_read_history drh ON (drh.fkInstance = di.fkContext)
   )
   $msWhere
   ORDER BY d.sName, d.fkContext, u.sName
   ";

    return parent::executeQuery();
  }
  
 /**
  * Desenha o cabe�alho do relat�rio.
  *
  * <p>M�todo para desenhar o cabe�alho do relat�rio.</p>
  * @access public
  */
  public function drawHeader(){
    foreach($this->caHeaders as $moHeader){
      if($moHeader->getAttrName()=='header_filter'){
        $moHeaderFilter = $moHeader;
      }elseif($moHeader->getAttrName()=='header_filter_item'){
        $moHeaderFilterItem = $moHeader;
      }elseif($moHeader->getAttrName()=='report_header_blank'){
        $moHeaderBlank = $moHeader;
      }else{
        $this->coWriter->drawLine($moHeader,array());
      }
    }
    if($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());
    
    $moFilterText = FWDWebLib::getObject('header_filter_text');
    $moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
    $maFilters = $this->coFilter->getSummary();
    foreach($maFilters as $maFilter){
      $moFilterText->setValue($maFilter['name']);
      $this->coWriter->drawLine($moHeaderFilter,array());
      foreach($maFilter['items'] as $msFilterItem){
        $moFilterItemText->setValue($msFilterItem);
        $this->coWriter->drawLine($moHeaderFilterItem,array());
      }
    }
    $this->coWriter->drawLine($moHeaderBlank,array());
  }

}
?>