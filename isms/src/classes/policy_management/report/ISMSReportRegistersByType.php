<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe ISMSReportRegistersByType.
 *
 * <p>Classe que implementa o relatório de registros por tipo.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportRegistersByType extends ISMSReport {
  
  /**
   * Inicializa o relatório.
   *
   * <p>Método para inicializar o relatório.
   * Necessário para funcionar com o XML compilado.</p>
   * @access public
   */  
  public function init() {
    parent::init();
    
    $this->coDataSet->addFWDDBField(new FWDDBField("","class_id",DB_NUMBER));    
    $this->coDataSet->addFWDDBField(new FWDDBField("","class_name",DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("","reg_id",DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("","reg_name",DB_STRING));
  }
  
  /**
   * Executa a query do relatório.
   *
   * <p>Método para executar a query do relatório.</p>
   * @access public
   */
  public function makeQuery() {
    $moFilter = $this->getFilter();    
    $msWhere ="";
    $maClassifType = $moFilter->getRegistersType();
    
    $msUnion = "UNION SELECT
                  99999 as class_id,
                  '".FWDLanguage::getPHPStringValue('rs_undefined_type','Tipo Indefinido')."' as class_name,
                  r.fkContext as reg_id,
                  r.sName as reg_name 
                FROM
                  view_pm_register_active r
                WHERE
                  r.fkClassification IS NULL";
    
    if(count($maClassifType)){
      if(isset($maClassifType['null'])){
        $msWhere = " AND r.fkClassification IS NULL ";
      }else{
        $msWhere = " AND r.fkClassification IN ( ".implode(',',$maClassifType) ." ) ";
        $msUnion = "";
      }
    }

    $this->csQuery = "SELECT
												cl.pkClassification as class_id,
												cl.sName as class_name,
												r.fkContext as reg_id,
												r.sName as reg_name	
											FROM
												isms_context_classification cl JOIN
												view_pm_register_active r ON (cl.pkClassification = r.fkClassification)
											WHERE
												cl.nContextType = " . CONTEXT_REGISTER . " AND
												cl.nClassificationType = " . CONTEXT_CLASSIFICATION_TYPE . "
												$msWhere
                        $msUnion
											ORDER BY
												class_id ASC";
    
    return parent::executeQuery();
  }
}
?>