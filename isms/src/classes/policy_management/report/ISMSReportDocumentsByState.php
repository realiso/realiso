<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportDocumentsByState.
 *
 * <p>Classe que implementa o relatório de documentos por status.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportDocumentsByState extends ISMSFilteredReport {
  
  public function __construct(){
    parent::__construct();
  }
  
 /**
  * Inicializa o relatório.
  *
  * <p>Método para inicializar o relatório.
  * Necessário para funcionar com o XML compilado.</p>
  * @access public
  */
  public function init(){
    parent::init();
    
	$col1 = new FWDDBField(''            ,'doc_id'      ,DB_NUMBER);
	$col1->setLabel(' ');
	$col1->setShowColumn(false);
	
	$col2 = new FWDDBField('d.sName'     ,'doc_name'    ,DB_STRING);
	$col2->setLabel(FWDLanguage::getPHPStringValue('mx_document','Documento'));
	$col2->setShowColumn(true);		

	$col3 = new FWDDBField(''            ,'doc_state'   ,DB_NUMBER);
	$col3->setLabel(FWDLanguage::getPHPStringValue('gc_state','Estado'));
	$col3->setShowColumn(true);
	
	$col4 = new FWDDBField('di.sFileName','doc_filename',DB_STRING);
	$col4->setLabel(' ');
	$col4->setShowColumn(false);		
	
	$col5 = new FWDDBField('di.bIsLink'  ,'doc_is_link' ,DB_NUMBER);
	$col5->setLabel(' ');
	$col5->setShowColumn(false);     
       
    $this->coDataSet->addFWDDBField($col1);
    $this->coDataSet->addFWDDBField($col2);
    $this->coDataSet->addFWDDBField($col3);
    $this->coDataSet->addFWDDBField($col4);
    $this->coDataSet->addFWDDBField($col5);
  }
  
 /**
  * Executa a query do relatório.
  *
  * <p>Método para executar a query do relatório.</p>
  * @access public
  */
  public function makeQuery(){
    $miStatus = $this->coFilter->getStatus();
    $mbShowRegisterDocs = $this->coFilter->getShowRegisterDocs();
    
    $maFilters = array();
    
    if($miStatus){
      $maFilters[] = "dis.doc_instance_status = {$miStatus}";
    }
    
    if(!$mbShowRegisterDocs){
      $maFilters[] = "d.nType != ".CONTEXT_REGISTER;
    }
    
    if(count($maFilters)==0){
      $msWhere = '';
    }else{
      $msWhere = ' WHERE '.implode(' AND ',$maFilters);
    }
    $this->csQuery = "SELECT
                        CASE
                          WHEN dis.doc_instance_id IS NULL THEN d.fkContext || '|'
                          ELSE d.fkContext || '|' || dis.doc_instance_id
                        END AS doc_id,
                        d.sName AS doc_name,
                        dis.doc_instance_status AS doc_state,
                        di.sFileName AS doc_filename,
                        di.bIsLink AS doc_is_link
                      FROM
                        view_pm_document_active d
                        LEFT JOIN view_pm_doc_instance_status dis ON (dis.document_id = d.fkContext)
                        LEFT JOIN pm_doc_instance di ON (di.fkContext = dis.doc_instance_id)
                      {$msWhere}
                      ORDER BY doc_state, doc_name";
    return parent::executeQuery();
  }

}

?>