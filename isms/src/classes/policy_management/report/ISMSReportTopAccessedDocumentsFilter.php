<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportTopAccessedDocumentsFilter.
 *
 * <p>Classe que implementa o filtro do relatório de documentos mais acessados, por ordem de quantidade de acessos.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportTopAccessedDocumentsFilter extends FWDReportFilter {

  protected $ciTruncateNumber = 0;

  public function getTruncateNumber(){
    return $this->ciTruncateNumber;
  }

  public function setTruncateNumber($piTruncateNumber){
    $this->ciTruncateNumber = $piTruncateNumber;
  }

  public function getSummary(){
    $maFilters = array();
    if($this->ciTruncateNumber){
      $maFilters[] = array(
        'name' => FWDLanguage::getPHPStringValue('rs_show_only_first_cl','Exibir somente os primeiros:'),
        'items' => array($this->ciTruncateNumber)
      );
    }
    return $maFilters;
  }

}
?>