<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe ISMSReportAccessedDocuments.
 *
 * <p>Classe que implementa o relat�rio de documentos acessados por um usu�rio.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportAccessedDocuments extends ISMSReport {
  
 /**
  * Inicializa o relat�rio.
  *
  * <p>M�todo para inicializar o relat�rio.
  * Necess�rio para funcionar com o XML compilado.</p>
  * @access public
  */
  public function init(){
    parent::init();
    
	$col1 = new FWDDBField('u.fkContext' ,'user_id'               ,DB_NUMBER);
	$col1->setLabel(' ');
	$col1->setShowColumn(false);
	
	$col2 = new FWDDBField('u.sName'     ,'user_name'             ,DB_STRING);
	$col2->setLabel(FWDLanguage::getPHPStringValue('rs_user','Usu�rio'));
	$col2->setShowColumn(true);		

	$col3 = new FWDDBField('rh.dDate'    ,'access_date'           ,DB_DATETIME);
	$col3->setLabel(FWDLanguage::getPHPStringValue('rs_date','Data') . '/' . FWDLanguage::getPHPStringValue('rs_time','Hora'));
	$col3->setShowColumn(true);
	
	$col4 = new FWDDBField('d.fkContext' ,'document_id'           ,DB_NUMBER);
	$col4->setLabel(' ');
	$col4->setShowColumn(false);		

	$col5 = new FWDDBField('d.sName'     ,'document_name'         ,DB_STRING);
	$col5->setLabel(FWDLanguage::getPHPStringValue('mx_document','Documento'));
	$col5->setShowColumn(true);
	
	$col6 = new FWDDBField('d.nType'     ,'document_type'         ,DB_NUMBER);
	$col6->setLabel(FWDLanguage::getPHPStringValue('gc_type','Tipo'));
	$col6->setShowColumn(true);     
    
	$col7 = new FWDDBField('di.sFileName','doc_instance_file_name',DB_STRING);
	$col7->setLabel(' ');
	$col7->setShowColumn(false);   

	$col8 = new FWDDBField('di.bIsLink'  ,'doc_instance_is_link'  ,DB_NUMBER);
	$col8->setLabel(' ');
	$col8->setShowColumn(false);        
    
    $this->coDataSet->addFWDDBField($col1);
    $this->coDataSet->addFWDDBField($col2);
    $this->coDataSet->addFWDDBField($col3);
    $this->coDataSet->addFWDDBField($col4);
    $this->coDataSet->addFWDDBField($col5);
    $this->coDataSet->addFWDDBField($col6);
    $this->coDataSet->addFWDDBField($col7);
    $this->coDataSet->addFWDDBField($col8);
    
  }
  
  /**
   * Executa a query do relat�rio.
   *
   * <p>M�todo para executar a query do relat�rio.</p>
   * @access public
   */
  public function makeQuery(){
    $mbShowAllReads = $this->coFilter->getShowAllReads();
    $maUsers = $this->coFilter->getUsers();
    $miInitialDate = $this->coFilter->getInitialDate();
    $miFinalDate = $this->coFilter->getFinalDate();
    
    $maFilters = array();
    
    if($miInitialDate){
      $maFilters[] = "rh.dDate >= ".ISMSLib::getTimestampFormat($miInitialDate);
    }
    
    if($miFinalDate){
      $msFinalDate = ISMSLib::getTimestampFormat($miFinalDate);
      $maFilters[] = "rh.dDate <= $msFinalDate";
    }else{
      $msFinalDate = '';
    }
    
    if(!$mbShowAllReads){
      $maFilters[] = "NOT EXISTS (
                        SELECT *
                        FROM pm_doc_read_history rh2
                          JOIN pm_doc_instance di2 ON (di2.fkContext = rh2.fkInstance)
                        WHERE di2.fkDocument = di.fkDocument
                          AND rh2.fkUser = rh.fkUser
                          AND rh2.dDate > rh.dDate
                          ".($msFinalDate?"AND rh2.dDate <= $msFinalDate":'')."
                      )";
    }
    
    if(count($maUsers)){
      $maFilters[] = "u.fkContext IN (".implode(',',$maUsers).")";
    }
    
    if(count($maFilters)){
      $msWhere = ' WHERE '.implode(' AND ',$maFilters);
    }else{
      $msWhere = '';
    }
    $this->csQuery = "SELECT
                        u.fkContext as user_id,
                        u.sName as user_name,
                        rh.dDate as access_date,
                        d.fkContext as document_id,
                        d.sName as document_name,
                        d.nType as document_type,
                        di.sFileName as doc_instance_file_name,
                        di.bIsLink as doc_instance_is_link
                      FROM
                        view_isms_user_active u
                        JOIN pm_doc_read_history rh ON (rh.fkUser = u.fkContext)
                        JOIN view_pm_doc_instance_active di ON (di.fkContext = rh.fkInstance)
                        JOIN view_pm_document_active d ON (d.fkContext = di.fkDocument)
                      $msWhere
                      ORDER BY user_name, access_date";
    return parent::executeQuery();
  }
  
 /**
  * Desenha o cabe�alho do relat�rio.
  *
  * <p>M�todo para desenhar o cabe�alho do relat�rio.</p>
  * @access public
  */
  public function drawHeader(){
    foreach($this->caHeaders as $moHeader){
      if($moHeader->getAttrName()=='header_filter'){
        $moHeaderFilter = $moHeader;
      }elseif($moHeader->getAttrName()=='header_filter_item'){
        $moHeaderFilterItem = $moHeader;
      }elseif($moHeader->getAttrName()=='report_header_blank'){
        $moHeaderBlank = $moHeader;
      }else{
        $this->coWriter->drawLine($moHeader,array());
      }
    }
    if($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());
    
    $moFilterText = FWDWebLib::getObject('header_filter_text');
    $moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
    $maFilters = $this->coFilter->getSummary();
    foreach($maFilters as $maFilter){
      $moFilterText->setValue($maFilter['name']);
      $this->coWriter->drawLine($moHeaderFilter,array());
      foreach($maFilter['items'] as $msFilterItem){
        $moFilterItemText->setValue($msFilterItem);
        $this->coWriter->drawLine($moHeaderFilterItem,array());
      }
    }
    $this->coWriter->drawLine($moHeaderBlank,array());
  }

}
?>