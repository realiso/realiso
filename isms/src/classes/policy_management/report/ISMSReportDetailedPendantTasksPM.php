<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe ISMSReportDetailedPendantTasksPM.
 *
 * <p>Classe que implementa o relat�rio detalhado de tarefas pendentes da gest�o de pol�ticas.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportDetailedPendantTasksPM extends ISMSReport {
  
  /**
   * Inicializa o relat�rio.
   *
   * <p>M�todo para inicializar o relat�rio.
   * Necess�rio para funcionar com o XML compilado.</p>
   * @access public
   */  
  public function init() {
    parent::init();

    $dataSet = $this->coDataSet;
    
    $userId = new FWDDBField("","user_id",DB_NUMBER);
    $userId->setShowColumn(false);
    
    $userName = new FWDDBField("","user_name",DB_STRING);
    $userName->setLabel(FWDLanguage::getPHPStringValue('rs_responsible','Respons�vel'));
    $userName->setShowColumn(true);
    
    $contextId = new FWDDBField("","context_id",DB_NUMBER);
    $contextId->setShowColumn(false);
    
    $contextName = new FWDDBField("","context_name",DB_STRING);
    $contextName->setLabel(FWDLanguage::getPHPStringValue('rs_tasks','Tarefas'));
    $contextName->setShowColumn(true);
    
    $taskId = new FWDDBField("","task_id",DB_NUMBER);
    $taskId->setShowColumn(false);
    
    $taskActivity = new FWDDBField("","task_activity",DB_NUMBER);
    $taskActivity->setShowColumn(false);
    
    $dataSet->addFWDDBField($userId);
    $dataSet->addFWDDBField($userName);
    $dataSet->addFWDDBField($contextId);
    $dataSet->addFWDDBField($contextName);
    $dataSet->addFWDDBField($taskId);
    $dataSet->addFWDDBField($taskActivity);
  }
  
  /**
   * Executa a query do relat�rio.
   *
   * <p>M�todo para executar a query do relat�rio.</p>
   * @access public
   */
  public function makeQuery() {
    $moConfig = new ISMSConfig();
    $msWhere = '';
    $maFilterTypes = array(
              ACT_DOCUMENT_REVISION,
              ACT_DOCUMENT_APPROVAL,
              ACT_DOCUMENT_TEMPLATE_APPROVAL,
              
              // + todas as tarefas pendentes de melhoria continua.
              ACT_OCCURRENCE_APPROVAL,
              
              ACT_INC_APPROVAL,
              ACT_INC_DISPOSAL_APPROVAL,
              ACT_INC_SOLUTION_APPROVAL,
              ACT_INC_CONTROL_INDUCTION,
              
              ACT_NON_CONFORMITY_APPROVAL,
              
              ACT_AP_APPROVAL 
              
    );

    if(count($maFilterTypes)){
      $msWhere .= "AND wt.nActivity IN (".implode(',',$maFilterTypes).") ";
    }
    
    $this->csQuery = "SELECT u.fkContext as user_id, u.sName as user_name,
                             cn.context_id as context_id, cn.context_name as context_name,
                             wt.pkTask as task_id, wt.nActivity as task_activity
                         FROM view_isms_user_active u
                         JOIN wkf_task wt ON (u.fkContext = wt.fkReceiver AND wt.bVisible = 1)
                         JOIN context_names cn ON (wt.fkContext = cn.context_id)
                            $msWhere
                         ORDER BY u.fkContext, cn.context_type";

    return parent::executeQuery();
  }
}
?>