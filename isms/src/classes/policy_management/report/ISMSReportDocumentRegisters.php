<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe ISMSReportDocumentRegisters.
 *
 * <p>Classe que implementa o relat�rio de registros por documento.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportDocumentRegisters extends ISMSReport {
  
 /**
  * Inicializa o relat�rio.
  *
  * <p>M�todo para inicializar o relat�rio.
  * Necess�rio para funcionar com o XML compilado.</p>
  * @access public
  */
  public function init(){
    parent::init();
    
	$col1 = new FWDDBField('d.fkContext' ,'document_id'           ,DB_NUMBER);
	$col1->setLabel(' ');
	$col1->setShowColumn(false);
	
	$col2 = new FWDDBField('d.sName'     ,'document_name'         ,DB_STRING);
	$col2->setLabel(FWDLanguage::getPHPStringValue('mx_document','Documento'));
	$col2->setShowColumn(true);		

	$col3 = new FWDDBField('di.sFileName','doc_instance_file_name',DB_STRING);
	$col3->setLabel(' ');
	$col3->setShowColumn(false);
	
	$col4 = new FWDDBField('di.bIsLink'  ,'doc_instance_is_link'  ,DB_NUMBER);
	$col4->setLabel(' ');
	$col4->setShowColumn(false);		

	$col5 = new FWDDBField('r.fkContext' ,'register_id'           ,DB_NUMBER);
	$col5->setLabel(' ');
	$col5->setShowColumn(false);
	
	$col6 = new FWDDBField('r.sName'     ,'register_name'         ,DB_STRING);
	$col6->setLabel(FWDLanguage::getPHPStringValue('rs_name','Nome'));
	$col6->setShowColumn(true);     
    
	$col7 = new FWDDBField('r.nPeriod'   ,'register_period'       ,DB_NUMBER);
	$col7->setLabel(FWDLanguage::getPHPStringValue('rs_retention_time','Tempo de Reten��o'));
	$col7->setShowColumn(true);   

	$col8 = new FWDDBField('r.nValue'    ,'register_value'       ,DB_NUMBER);
	$col8->setLabel(' ');
	$col8->setShowColumn(false);          
    
    $this->coDataSet->addFWDDBField($col1);
    $this->coDataSet->addFWDDBField($col2);
    $this->coDataSet->addFWDDBField($col3);
    $this->coDataSet->addFWDDBField($col4);
    $this->coDataSet->addFWDDBField($col5);
    $this->coDataSet->addFWDDBField($col6);
    $this->coDataSet->addFWDDBField($col7);
    $this->coDataSet->addFWDDBField($col8);
    
  }
  
  /**
   * Executa a query do relat�rio.
   *
   * <p>M�todo para executar a query do relat�rio.</p>
   * @access public
   */
  public function makeQuery(){
    $maDocuments = $this->coFilter->getDocuments();
    
    $maFilters = array();
    
    if(count($maDocuments)){
      $maFilters[] = "d.fkContext IN (".implode(',',$maDocuments).")";
    }
    
    if(count($maFilters)){
      $msWhere = ' WHERE '.implode(' AND ',$maFilters);
    }else{
      $msWhere = '';
    }
    $this->csQuery = "SELECT
                        d.fkContext as document_id,
                        d.sName as document_name,
                        di.sFileName as doc_instance_file_name,
                        di.bIsLink as doc_instance_is_link,
                        r.fkContext as register_id,
                        r.sName as register_name,
                        r.nPeriod as register_period,
                        r.nValue as register_value
                      FROM
                        view_pm_document_active d
                        JOIN view_pm_doc_instance_active di ON (di.fkDocument = d.fkContext AND di.fkContext = d.fkCurrentVersion)
                        JOIN view_pm_doc_registers_active dr ON (d.fkContext = dr.fkDocument)
                        JOIN view_pm_register_active r ON (dr.fkRegister = r.fkContext)
                      $msWhere
                      ORDER BY d.sName, r.sName";
    return parent::executeQuery();
  }
  
 /**
  * Desenha o cabe�alho do relat�rio.
  *
  * <p>M�todo para desenhar o cabe�alho do relat�rio.</p>
  * @access public
  */
  public function drawHeader(){
    foreach($this->caHeaders as $moHeader){
      if($moHeader->getAttrName()=='header_filter'){
        $moHeaderFilter = $moHeader;
      }elseif($moHeader->getAttrName()=='header_filter_item'){
        $moHeaderFilterItem = $moHeader;
      }elseif($moHeader->getAttrName()=='report_header_blank'){
        $moHeaderBlank = $moHeader;
      }else{
        $this->coWriter->drawLine($moHeader,array());
      }
    }
    if($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());
    
    $moFilterText = FWDWebLib::getObject('header_filter_text');
    $moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
    $maFilters = $this->coFilter->getSummary();
    foreach($maFilters as $maFilter){
      $moFilterText->setValue($maFilter['name']);
      $this->coWriter->drawLine($moHeaderFilter,array());
      foreach($maFilter['items'] as $msFilterItem){
        $moFilterItemText->setValue($msFilterItem);
        $this->coWriter->drawLine($moHeaderFilterItem,array());
      }
    }
    $this->coWriter->drawLine($moHeaderBlank,array());
  }

}
?>