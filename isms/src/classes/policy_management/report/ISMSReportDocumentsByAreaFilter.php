<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe ISMSReportDocumentsByAreaFilter.
 *
 * <p>Classe que implementa o filtro do relat�rio de quantidade de documentos que cada �rea precisa ter acesso, identando as �reas.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportDocumentsByAreaFilter extends FWDReportFilter {

  protected $cbExpand = false;

  public function getExpand(){
    return $this->cbExpand;
  }

  public function setExpand($pbExpand){
    $this->cbExpand = $pbExpand;
  }

  public function getSummary(){
    $maFilters = array();
    
    if($this->cbExpand){
      $msExpandReaders = FWDLanguage::getPHPStringValue('rs_yes','sim');
    }else{
      $msExpandReaders = FWDLanguage::getPHPStringValue('rs_no','n�o');
    }
    $maFilters[] = array(
      'name' => FWDLanguage::getPHPStringValue('rs_expand_documents','Expandir documentos'),
      'items' => array($msExpandReaders)
    );
    
    return $maFilters;
  }

}
?>