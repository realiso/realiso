<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe ISMSReportDocumentsByType.
 *
 * <p>Classe que implementa o relatório de documentos por tipo.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportDocumentsByType extends ISMSReport {
  
  /**
   * Inicializa o relatório.
   *
   * <p>Método para inicializar o relatório.
   * Necessário para funcionar com o XML compilado.</p>
   * @access public
   */  
  public function init() {
    parent::init();
    
	$col1 = new FWDDBField("","class_id",DB_NUMBER);
	$col1->setLabel(' ');
	$col1->setShowColumn(false);
	
	$col2 = new FWDDBField("","class_name",DB_STRING);
	$col2->setLabel(FWDLanguage::getPHPStringValue('gc_type','Tipo'));
	$col2->setShowColumn(true);		

	$col3 = new FWDDBField("","doc_id",DB_NUMBER);
	$col3->setLabel(' ');
	$col3->setShowColumn(false);
	
	$col4 = new FWDDBField("","doc_name",DB_STRING);
	$col4->setLabel(FWDLanguage::getPHPStringValue('mx_document','Documento'));
	$col4->setShowColumn(true);		

	$col5 = new FWDDBField("","doc_version_name",DB_STRING);
	$col5->setLabel(' ');
	$col5->setShowColumn(false);
	
	$col6 = new FWDDBField("","doc_is_link",DB_NUMBER);
	$col6->setLabel(' ');
	$col6->setShowColumn(false);     
    
	$col7 = new FWDDBField("","doc_link",DB_STRING);
	$col7->setLabel(' ');
	$col7->setShowColumn(false);      
    
    $this->coDataSet->addFWDDBField($col1);    
    $this->coDataSet->addFWDDBField($col2);
    $this->coDataSet->addFWDDBField($col3);
    $this->coDataSet->addFWDDBField($col4);
    $this->coDataSet->addFWDDBField($col5);
    $this->coDataSet->addFWDDBField($col6);
    $this->coDataSet->addFWDDBField($col7);
    
  }
  
  /**
   * Executa a query do relatório.
   *
   * <p>Método para executar a query do relatório.</p>
   * @access public
   */
  public function makeQuery() {
    $moFilter = $this->getFilter();    
    $msWhere ="";
    $maClassifType = $moFilter->getDocumentsType();
    
    $msUnion = "UNION SELECT
                  99999 as class_id,
                  '".FWDLanguage::getPHPStringValue('rs_undefined_type','Tipo Indefinido')."' as class_name,
                  d.fkContext as doc_id,
                  d.sName as doc_name,
                  i.sFilename as doc_version_name,
                  i.bIsLink as doc_is_link,
                  i.sLink as doc_link
                FROM
                  view_pm_published_docs d
                  JOIN view_pm_doc_instance_active i ON (d.fkCurrentVersion = i.fkContext)
                WHERE
                  d.fkClassification IS NULL";
    
    if(count($maClassifType)){
      if(isset($maClassifType['null'])){
        $msWhere = " AND d.fkClassification IS NULL ";
      }else{
        $msWhere = " AND d.fkClassification IN ( ".implode(',',$maClassifType) ." ) ";
        $msUnion = "";
      }
    }

    $this->csQuery = "SELECT
												cl.pkClassification as class_id,
												cl.sName as class_name,
												d.fkContext as doc_id,
												d.sName as doc_name,
												i.sFilename as doc_version_name,
												i.bIsLink as doc_is_link,
												i.sLink as doc_link
											FROM
												isms_context_classification cl JOIN
												view_pm_published_docs d ON (cl.pkClassification = d.fkClassification)
												JOIN view_pm_doc_instance_active i ON (d.fkCurrentVersion = i.fkContext)
											WHERE
												cl.nContextType = " . CONTEXT_DOCUMENT . " AND
												cl.nClassificationType = " . CONTEXT_CLASSIFICATION_TYPE . "
												$msWhere
                        $msUnion
											ORDER BY
												class_id ASC";
    
    return parent::executeQuery();
  }
}
?>