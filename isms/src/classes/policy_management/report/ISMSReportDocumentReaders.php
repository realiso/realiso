<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
/**
 * Classe ISMSReportDocumentReaders.
 *
 * <p>Classe que implementa o relat�rio de documentos, seus respons�veis e leitores.</p>
 * @package ISMS
 * @subpackage report
 */
class ISMSReportDocumentReaders extends ISMSReport {
  
 /**
  * Inicializa o relat�rio.
  *
  * <p>M�todo para inicializar o relat�rio.
  * Necess�rio para funcionar com o XML compilado.</p>
  * @access public
  */
  public function init(){
    parent::init();
    
	$col1 = new FWDDBField('d.fkContext'     ,'doc_id'                ,DB_NUMBER);
	$col1->setLabel(' ');
	$col1->setShowColumn(false);
	
	$col2 = new FWDDBField('d.sName'         ,'doc_name'              ,DB_STRING);
	$col2->setLabel(FWDLanguage::getPHPStringValue('mx_document','Documento'));
	$col2->setShowColumn(true);		

	$col3 = new FWDDBField('di.sFileName'    ,'doc_instance_file_name',DB_STRING);
	$col3->setLabel(' ');
	$col3->setShowColumn(false);
	
	$col4 = new FWDDBField('di.bIsLink'      ,'doc_instance_is_link'  ,DB_NUMBER);
	$col4->setLabel(' ');
	$col4->setShowColumn(false);		

	$col5 = new FWDDBField('d.fkAuthor'      ,'doc_author_id'         ,DB_NUMBER);
	$col5->setLabel(' ');
	$col5->setShowColumn(false);
	
	$col6 = new FWDDBField('u.sName'         ,'doc_author_name'       ,DB_STRING);
	$col6->setLabel(FWDLanguage::getPHPStringValue('rs_author','Elaborador'));
	$col6->setShowColumn(true);     
    
	$col7 = new FWDDBField('d.fkMainApprover','doc_main_approver_id'  ,DB_NUMBER);
	$col7->setLabel(' ');
	$col7->setShowColumn(false);   

	$col8 = new FWDDBField('u2.sName'        ,'doc_main_approver_name',DB_STRING);
	$col8->setLabel(FWDLanguage::getPHPStringValue('rs_responsible','Respons�vel'));
	$col8->setShowColumn(true);   

	$col9 = new FWDDBField('dr.fkUser'       ,'doc_reader_id'         ,DB_NUMBER);
	$col9->setLabel(' ');
	$col9->setShowColumn(false);       
    
	$col10 = new FWDDBField('u3.sName'        ,'doc_reader_name'       ,DB_STRING);
	$col10->setLabel(FWDLanguage::getPHPStringValue('rs_reader','Leitor'));
	$col10->setShowColumn(true);     	
	
    $this->coDataSet->addFWDDBField($col1);
    $this->coDataSet->addFWDDBField($col2);
    $this->coDataSet->addFWDDBField($col3);
    $this->coDataSet->addFWDDBField($col4);
    $this->coDataSet->addFWDDBField($col5);
    $this->coDataSet->addFWDDBField($col6);
    $this->coDataSet->addFWDDBField($col7);
    $this->coDataSet->addFWDDBField($col8);
    $this->coDataSet->addFWDDBField($col9);
    $this->coDataSet->addFWDDBField($col10);
    
  }
  
  /**
   * Executa a query do relat�rio.
   *
   * <p>M�todo para executar a query do relat�rio.</p>
   * @access public
   */
  public function makeQuery(){
    $maDocuments = $this->coFilter->getDocuments();
    
    $maFilters = array();
    
    if(count($maDocuments)){
      $maFilters[] = "d.fkContext IN (".implode(',',$maDocuments).")";
    }
    
    if(count($maFilters)){
      $msWhere = ' WHERE '.implode(' AND ',$maFilters);
    }else{
      $msWhere = '';
    }
    $this->csQuery = "SELECT                        d.fkContext as doc_id,                        d.sName as doc_name,                         di.sFileName as doc_instance_file_name,                        di.bIsLink as doc_instance_is_link,                        d.fkAuthor as doc_author_id,                        u.sName as doc_author_name,                         d.fkMainApprover as doc_main_approver_id,                        u2.sName as doc_main_approver_name,                         dr.fkUser as doc_reader_id,                        u3.sName as doc_reader_name                      FROM                        view_pm_document_active d                        JOIN view_pm_doc_instance_active di ON (di.fkContext = d.fkCurrentVersion)                        LEFT JOIN pm_doc_readers dr ON (d.fkContext = dr.fkDocument AND dr.bDenied!=1)                        LEFT JOIN view_isms_user_active u ON (d.fkAuthor = u.fkContext)                        LEFT JOIN view_isms_user_active u2 ON (d.fkMainApprover = u2.fkContext)                        LEFT JOIN view_isms_user_active u3 ON (dr.fkUser = u3.fkContext)
                      $msWhere                      ORDER BY d.sName, d.fkContext, u3.sName";
    return parent::executeQuery();
  }
  
 /**
  * Desenha o cabe�alho do relat�rio.
  *
  * <p>M�todo para desenhar o cabe�alho do relat�rio.</p>
  * @access public
  */
  public function drawHeader(){
    foreach($this->caHeaders as $moHeader){
      if($moHeader->getAttrName()=='header_filter'){
        $moHeaderFilter = $moHeader;
      }elseif($moHeader->getAttrName()=='header_filter_item'){
        $moHeaderFilterItem = $moHeader;
      }elseif($moHeader->getAttrName()=='report_header_blank'){
        $moHeaderBlank = $moHeader;
      }else{
        $this->coWriter->drawLine($moHeader,array());
      }
    }
    if($this->coFilter->getComment()) $this->coWriter->drawLine($moHeaderBlank,array());
    
    $moFilterText = FWDWebLib::getObject('header_filter_text');
    $moFilterItemText = FWDWebLib::getObject('header_filter_item_text');
    $maFilters = $this->coFilter->getSummary();
    foreach($maFilters as $maFilter){
      $moFilterText->setValue($maFilter['name']);
      $this->coWriter->drawLine($moHeaderFilter,array());
      foreach($maFilter['items'] as $msFilterItem){
        $moFilterItemText->setValue($msFilterItem);
        $this->coWriter->drawLine($moHeaderFilterItem,array());
      }
    }
    $this->coWriter->drawLine($moHeaderBlank,array());
  }

}
?>