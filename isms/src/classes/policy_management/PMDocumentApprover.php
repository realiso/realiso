<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe PMDocumentApprover
 *
 * <p>Classe que representa a tabela que relaciona documentos e aprovadores.</p>
 * @package ISMS
 * @subpackage classes
 */
class PMDocumentApprover extends ISMSTable {

 /**
  * Construtor.
  *
  * <p>Construtor da classe PMDocumentApprover.</p>
  * @access public
  */
  public function __construct(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    parent::__construct('pm_doc_approvers');
    $this->coDataset->addFWDDBField(new FWDDBField('fkDocument',      'document_id',        DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('fkUser',          'user_id',            DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('bHasApproved',    'has_approved',       DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('bManual',         'manual',             DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('bContextApprover','is_context_approver',DB_NUMBER));
  }

 /**
  * Insere rela��es entre um documento e diversos usu�rios.
  *
  * <p>M�todo para inserir rela��es entre um documento e diversos usu�rios.</p>
  * @access public
  * @param integer $piDocumentId Id do documento
  * @param array $paUsersIds Ids dos usu�rios
  */
  public function insertUsers($piDocumentId, $paUsers){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $this->setFieldValue('document_id',$piDocumentId);
    foreach($paUsers as $piUserId){
      $this->setFieldValue('user_id',$piUserId);
      $this->setFieldValue('manual',1);
      $this->insert();
    }
  }

 /**
  * Deleta rela��es entre um documento e diversos usu�rios.
  *
  * <p>M�todo para deletar rela��es entre um documento e diversos usu�rios.</p>
  * @access public
  * @param integer $piDocumentId Id do documento
  * @param integer $paUsersIds Ids dos usu�rios
  */
  public function deleteUsers($piDocumentId, $paUsersIds){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
  	if (count($paUsersIds)) {
	    $this->createFilter($piDocumentId,'document_id');
	    foreach($paUsersIds as $piUserId){
	      $this->createFilter($piUserId,'user_id','in');
	    }
	    $this->delete();
  	}
  }

 /**
  * Retorna os ids dos usu�rios associados a um documento.
  *
  * <p>Retorna os ids dos usu�rios associados a um documento.</p>
  * @access public
  * @param integer $piDocumentId Id do documento
  * @param boolean $pbGetContextApprovers Determina se deve ou n�o retornar usu�rios que s�o aprovadores autom�ticos (de contexto).
  * @return array Ids dos usu�rios
  */
  public function getUsers($piDocumentId,$pbGetContextApprovers=false){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $this->createFilter($piDocumentId,'document_id');
    if (!$pbGetContextApprovers) $this->createFilter(false,'is_context_approver');
    $this->select();
    $maUsers = array();
    while($this->fetch()){
      $maUsers[] = $this->getFieldValue('user_id');
    }
    return $maUsers;
  }

 /**
  * Retira a aprova��o de todos aprovadores.
  *
  * <p>Retira a aprova��o de todos aprovadores.</p>
  * @access public
  * @param integer $piDocumentId Id do documento
  */
  public function clearApprovals($piDocumentId){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $this->createFilter($piDocumentId,'document_id');
    $this->setFieldValue('has_approved',false);
    $this->update();
  }
  
 /**
  * Insere ou atualiza rela��es entre um documento e diversos usu�rios inseridos pelo sistema.
  *
  * <p>M�todo para inserir ou atualizar rela��es entre um documento e diversos usu�rios inseridos pelo sistema.</p>
  * @access public
  * @param integer $piDocumentId Id do documento
  * @param array $paUsersIds Ids dos usu�rios
  */
  public function autoInsertOrUpdateUsers($piDocumentId,$paUsers) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $moDocument = new PMDocument();
    $moDocument->fetchById($piDocumentId);
    $miMainApproverId = $moDocument->getFieldValue('document_main_approver');
    
    foreach($paUsers as $miUserId) {
      if ($miUserId == $miMainApproverId) continue;
      $moDocumentApprover = new PMDocumentApprover();
      $moDocumentApprover->createFilter($piDocumentId,'document_id');
      $moDocumentApprover->createFilter($miUserId,'user_id');
      $moDocumentApprover->select();
      if ($moDocumentApprover->fetch()) {
        $moDocumentApprover->setFieldValue('is_context_approver',true);
        $moDocumentApprover->update();
      } else {
        $moDocumentApprover = new PMDocumentApprover();
        $moDocumentApprover->setFieldValue('document_id',$piDocumentId);
        $moDocumentApprover->setFieldValue('user_id',$miUserId);
        $moDocumentApprover->setFieldValue('manual',false);
        $moDocumentApprover->setFieldValue('is_context_approver',true);
        $moDocumentApprover->insert();
      }
    }
  }
}
?>