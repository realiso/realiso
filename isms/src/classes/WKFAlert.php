<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

include_once $handlers_ref . "QueryDisciplinaryProcessUsers.php";

// Identificadores dos alertas
define("WKF_ALERT_APPROVED", 9201);
define("WKF_ALERT_DENIED", 9202);
define("WKF_ALERT_INFORMATION", 9203);
define("WKF_ALERT_DELEGATION", 9204);
define("WKF_ALERT_COAPPROVED", 9205);
define("WKF_ALERT_REAL_EFFICIENCY", 9206);
define("WKF_ALERT_IMPLEMENTATION_ALERT", 9207);
define("WKF_ALERT_IMPLEMENTATION_EXPIRED", 9208);
define("WKF_ALERT_TEST_CONTROL", 9209);
define("WKF_ALERT_SUGGESTED_EVENT_CREATED", 9210);
define("WKF_ALERT_DOCUMENT_COMMENT_CREATED", 9211);
define("WKF_ALERT_DOCUMENT_DEADLINE_NEAR", 9212);
define("WKF_ALERT_DOCUMENT_DEADLINE_LATE", 9213);
define("WKF_ALERT_DOCUMENT_REVISION", 9214);
define("WKF_ALERT_DOCUMENT_PUBLISHED", 9215);
define("WKF_ALERT_DOCUMENT_NEW_VERSION", 9216);
define("WKF_ALERT_NON_CONFORMITY_WAITING_DEADLINE", 9217);
define("WKF_ALERT_NON_CONFORMITY_WAITING_CONCLUSION", 9218);
define("WKF_ALERT_AP_EFFICIENCY_REVISION_NEAR", 9219);
define("WKF_ALERT_AP_EFFICIENCY_REVISION_LATE", 9220);
define("WKF_ALERT_PROBABILITY_UPDATE_UP", 9221);
define("WKF_ALERT_PROBABILITY_UPDATE_DOWN", 9222);

define("WKF_ALERT_INC_SENT_TO_RESPONSIBLE",9223);
define("WKF_ALERT_INC_DISPOSAL_APP",       9224);
define("WKF_ALERT_INC_DISPOSAL_DENIED",    9225);
define("WKF_ALERT_INC_SOLUTION_APP",       9226);
define("WKF_ALERT_INC_SOLUTION_DENIED",    9227);

define("WKF_ALERT_INC_ASSET",              9228);
define("WKF_ALERT_INC_CONTROL",            9229);
define("WKF_ALERT_INCIDENT",               9230);

define("WKF_ALERT_RISK_CREATED",           9231);

define("WKF_ALERT_INC_EVIDENCE_REQUIRED",  9232);

define("WKF_ALERT_AP_CONCLUSION",          9233);

define("WKF_ALERT_DISCIPLINARY_PROCESS",   9234);

define("WKF_ALERT_INCIDENT_RISK_PARAMETRIZATION",   9235);

define("WKF_ALERT_DOCUMENT_REVISION_LATE", 9236);

define("WKF_ALERT_DOCUMENT_CREATION_COMMENT", 9237);

/**
 * Classe WKFAlert.
 *
 * <p>Classe que manipula os alertas do sistema.</p>
 * @package ISMS
 * @subpackage classes
 */
class WKFAlert extends ISMSTable {
 
 /**
  * Array de alertas
  * @var array
  * @access protected
  */
  protected $caAlert = array();
 
 /**
  * Construtor.
  * 
  * <p>Construtor da classe WKFAlert.</p>
  * @access public 
  */
  public function __construct(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    parent::__construct("wkf_alert");
    
    $this->csAliasId = 'alert_id';
    
    $this->coDataset->addFWDDBField(new FWDDBField("pkAlert",         "alert_id",           DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("fkContext",       "alert_context_id",   DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("fkCreator",       "alert_creator_id",   DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("fkReceiver",      "alert_receiver_id",  DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("tJustification",  "alert_justification",DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField("nType",           "alert_type",         DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("bEmailSent",      "alert_email_sent",   DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField("dDate",           "alert_date",         DB_DATETIME));
    
    $this->caAlert = array (
      WKF_ALERT_APPROVED => FWDLanguage::getPHPStringValue('wk_approved',"foi aprovado(a)."),
      WKF_ALERT_DENIED => FWDLanguage::getPHPStringValue('wk_denied',"foi negado(a)."),
      WKF_ALERT_INFORMATION => FWDLanguage::getPHPStringValue('wk_information',""),
      WKF_ALERT_DELEGATION => FWDLanguage::getPHPStringValue('wk_delegated',"foi delegado(a) para voc�."),
      WKF_ALERT_COAPPROVED => FWDLanguage::getPHPStringValue('wk_pre_approved',"foi pr�-aprovado(a)."),
      WKF_ALERT_REAL_EFFICIENCY => FWDLanguage::getPHPStringValue('wk_real_efficiency_late', "Acompanhamento de Efic�cia Real do Controle '%name%' est� atrasado."),
      WKF_ALERT_IMPLEMENTATION_ALERT => FWDLanguage::getPHPStringValue('wk_control_implementation',"O Controle '%name%' deve ser implementado."),
      WKF_ALERT_IMPLEMENTATION_EXPIRED => FWDLanguage::getPHPStringValue('wk_implementation_expired', "A Implementa��o do Controle '%name%' expirou."),
      WKF_ALERT_TEST_CONTROL => FWDLanguage::getPHPStringValue('wk_control_test_late', "Acompanhamento de Teste do Controle '%name%' est� atrasado."),
      WKF_ALERT_SUGGESTED_EVENT_CREATED => FWDLanguage::getPHPStringValue('wk_asset_np_risk_association', "Risco n�o estimado foi associado ao ativo '%asset%' a partir do evento sugerido '%event%'."),
      WKF_ALERT_DOCUMENT_COMMENT_CREATED => FWDLanguage::getPHPStringValue('wk_document_comment', "O Documento '%name%' recebeu um coment�rio."),
      WKF_ALERT_DOCUMENT_PUBLISHED => FWDLanguage::getPHPStringValue('wk_document_publication', "O documento '%name%' foi enviado para publica��o."),
      WKF_ALERT_DOCUMENT_REVISION => FWDLanguage::getPHPStringValue('wk_document_revision', "O documento '%name%' foi enviado para revis�o."),
      WKF_ALERT_DOCUMENT_CREATION_COMMENT => FWDLanguage::getPHPStringValue('wk_document_creation_comment', "O documento '%name%' recebeu um coment�rio de cria��o."),
      WKF_ALERT_DOCUMENT_DEADLINE_NEAR => FWDLanguage::getPHPStringValue('wk_deadline_close', "O deadline para aprova��o do documento '%name%' est� pr�ximo."),
      WKF_ALERT_DOCUMENT_DEADLINE_LATE => FWDLanguage::getPHPStringValue('wk_deadline_expired', "O deadline para aprova��o do documento '%name%' expirou."),
      WKF_ALERT_DOCUMENT_PUBLISHED => FWDLanguage::getPHPStringValue('wk_document_is_now_published', "O documento '%name%' foi publicado."),
      WKF_ALERT_DOCUMENT_NEW_VERSION => FWDLanguage::getPHPStringValue('wk_document_new_version', "O documento '%name%' tem uma nova vers�o."),
      WKF_ALERT_NON_CONFORMITY_WAITING_DEADLINE => FWDLanguage::getPHPStringValue('wk_non_conformity_waiting_deadline', "N�o Conformidade %name% aguardando previs�o de conclus�o."),
      WKF_ALERT_NON_CONFORMITY_WAITING_CONCLUSION => FWDLanguage::getPHPStringValue('wk_non_conformity_waiting_conclusion', "N�o Conformidade %name% aguardando conclus�o."),
      WKF_ALERT_AP_EFFICIENCY_REVISION_NEAR => FWDLanguage::getPHPStringValue('wk_ap_efficiency_revision_near', "A data de revis�o da efici�ncia do Plano de A��o '%name%' est� pr�xima."), 
      WKF_ALERT_AP_EFFICIENCY_REVISION_LATE => FWDLanguage::getPHPStringValue('wk_ap_efficiency_revision_late', "A data de revis�o da efici�ncia do Plano de A��o '%name%' expirou."),
      WKF_ALERT_PROBABILITY_UPDATE_UP => FWDLanguage::getPHPStringValue('wk_probability_update_up', "A freq��ncia de incidentes associados ao risco \'%name%\', no �ltimo per�odo, sugere que sua probabilidade � maior do que a estimada."),
      WKF_ALERT_PROBABILITY_UPDATE_DOWN => FWDLanguage::getPHPStringValue('wk_probability_update_down', "A freq��ncia de incidentes associados ao risco \'%name%\', no �ltimo per�odo, sugere que sua probabilidade � menor do que a estimada"),
      
      WKF_ALERT_INC_SENT_TO_RESPONSIBLE => FWDLanguage::getPHPStringValue('wk_inc_sent_to_responsible',"O incidente '%name%' foi encaminhado para voc�."),
      WKF_ALERT_INC_DISPOSAL_APP => FWDLanguage::getPHPStringValue('wk_inc_disposal_app',"A disposi��o imediata para o incidente '%name%' foi aprovada."),
      WKF_ALERT_INC_DISPOSAL_DENIED => FWDLanguage::getPHPStringValue('wk_inc_disposal_denied',"A disposi��o imediata para o incidente '%name%' foi negada."),
      WKF_ALERT_INC_SOLUTION_APP => FWDLanguage::getPHPStringValue('wk_inc_solution_app',"A solu��o para o incidente '%name%' foi aprovada."),
      WKF_ALERT_INC_SOLUTION_DENIED => FWDLanguage::getPHPStringValue('wk_inc_solution_denied',"A solu��o para o incidente '%name%' foi negada."),
      
      WKF_ALERT_INC_ASSET => FWDLanguage::getPHPStringValue('wk_inc_asset',"O incidente '%incident%' foi relacionado ao ativo '%asset%'."),
      WKF_ALERT_INC_CONTROL => FWDLanguage::getPHPStringValue('wk_inc_control',"O incidente '%incident%' foi relacionado ao controle '%control%'."),
      WKF_ALERT_INCIDENT => FWDLanguage::getPHPStringValue('wk_incident',"Criado o incidente '%name%'."),
      
      WKF_ALERT_RISK_CREATED => FWDLanguage::getPHPStringValue('wk_risk_created', "O risco n�o estimado '%risk%' foi associado ao ativo '%asset%'."),
      
      WKF_ALERT_INC_EVIDENCE_REQUIRED => FWDLanguage::getPHPStringValue('wk_inc_evidence_required', "Processo de coleta de evid�ncias requerido para o incidente '%name%'."),
      
      WKF_ALERT_AP_CONCLUSION => FWDLanguage::getPHPStringValue('wk_ap_conclusion', 'O plano de a��o %name% foi concluido, ou seja, ele foi medido'),
      
      WKF_ALERT_DISCIPLINARY_PROCESS => FWDLanguage::getPHPStringValue('wk_disciplinary_process', "O(s) usu�rio(s) %users% foi/foram adicionado(s) ao processo disciplinar do incidente %name%."),
      
      WKF_ALERT_INCIDENT_RISK_PARAMETRIZATION => FWDLanguage::getPHPStringValue('wk_incident_risk_parametrization', "O risco %risk_name% foi associado ao ativo %asset_name%, pelo qual voc� � respons�vel. Os par�metros de risco s�o diferentes do risco original."),
      
      WKF_ALERT_DOCUMENT_REVISION_LATE => FWDLanguage::getPHPStringValue('wk_document_revision_late', "A revis�o do documento '%name%' est� atrasada."),
    );
  }

 /**
  * Retorna a descri��o do alerta.
  * 
  * <p>M�todo para retornar a descri��o do alerta.</p>
  * @access public
  * @param integer $piStatus Status do alerta
  * @param integer $piContextType Tipo do contexto
  * @param string $piContextName Nome do contexto
  * @param integer $piContextId Id do contexto
  * @return string Descri��o do alerta
  */		
  public function getDescription($piStatus, $piContextType, $psContextName, $piContextId) {
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
	if (isset($this->caAlert[$piStatus])) {
		$msDescription = "";
		switch ($piStatus) {
			case WKF_ALERT_REAL_EFFICIENCY:
			case WKF_ALERT_IMPLEMENTATION_ALERT:
			case WKF_ALERT_IMPLEMENTATION_EXPIRED:
			case WKF_ALERT_TEST_CONTROL:
      case WKF_ALERT_DOCUMENT_COMMENT_CREATED:
      case WKF_ALERT_DOCUMENT_PUBLISHED:
      case WKF_ALERT_DOCUMENT_REVISION:
      case WKF_ALERT_DOCUMENT_CREATION_COMMENT:
      case WKF_ALERT_DOCUMENT_REVISION_LATE:
      case WKF_ALERT_DOCUMENT_DEADLINE_NEAR:
      case WKF_ALERT_DOCUMENT_DEADLINE_LATE:
      case WKF_ALERT_DOCUMENT_NEW_VERSION:
      case WKF_ALERT_NON_CONFORMITY_WAITING_DEADLINE:
      case WKF_ALERT_NON_CONFORMITY_WAITING_CONCLUSION:
      case WKF_ALERT_AP_EFFICIENCY_REVISION_NEAR:
      case WKF_ALERT_AP_EFFICIENCY_REVISION_LATE:
      case WKF_ALERT_PROBABILITY_UPDATE_UP:
      case WKF_ALERT_PROBABILITY_UPDATE_DOWN:
      
      case WKF_ALERT_INC_SENT_TO_RESPONSIBLE:
      case WKF_ALERT_INC_DISPOSAL_APP:
      case WKF_ALERT_INC_DISPOSAL_DENIED:
      case WKF_ALERT_INC_SOLUTION_APP:
      case WKF_ALERT_INC_SOLUTION_DENIED:
      case WKF_ALERT_INCIDENT:
      case WKF_ALERT_INC_EVIDENCE_REQUIRED:
      case WKF_ALERT_AP_CONCLUSION:
				$msDescription = str_replace("%name%",$psContextName,$this->caAlert[$piStatus]);
			break;
			
			case WKF_ALERT_SUGGESTED_EVENT_CREATED:
				$moRisk = new RMRisk();
				$moRisk->fetchById($piContextId);
				$moAsset = new RMAsset();
				$moAsset->fetchById($moRisk->getFieldValue("risk_asset_id"));
				$msDescription = str_replace(array("%asset%","%event%"),array($moAsset->getName(),$psContextName),$this->caAlert[$piStatus]);
			break;
			
      case WKF_ALERT_INC_ASSET:{
        $moIncidentRisk = new CIIncidentRisk();
        $moIncidentRisk->fetchById($piContextId);
        $moIncident = new CIIncident();
        $moIncident->fetchById($moIncidentRisk->getFieldValue('incident_id'));
        $moRisk = new RMRisk();
        $moRisk->fetchById($moIncidentRisk->getFieldValue('risk_id'));
        $moAsset = new RMAsset();
        $moAsset->fetchById($moRisk->getFieldValue('risk_asset_id'));
        $msDescription = str_replace(array("%asset%","%incident%"),array($moAsset->getName(),$moIncident->getName()),$this->caAlert[$piStatus]);
        break;
      }
      case WKF_ALERT_INC_CONTROL:{
        $moIncidentControl = new CIIncidentControl();
        $moIncidentControl->fetchById($piContextId);
        $moIncident = new CIIncident();
        $moIncident->fetchById($moIncidentControl->getFieldValue('incident_id'));
        $moControl = new RMControl();
        $moControl->fetchById($moIncidentControl->getFieldValue('control_id'));
        $msDescription = str_replace(array("%control%","%incident%"),array($moControl->getName(),$moIncident->getName()),$this->caAlert[$piStatus]);
        break;
      }
      case WKF_ALERT_RISK_CREATED:{
        $moRisk = new RMRisk();
        $moRisk->fetchById($piContextId);
        $moAsset = new RMAsset();
        $moAsset->fetchById($moRisk->getFieldValue('risk_asset_id'));
        $msDescription = str_replace(array('%asset%','%risk%'),array($moAsset->getName(),$psContextName),$this->caAlert[$piStatus]);
        break;
      }
      case WKF_ALERT_DISCIPLINARY_PROCESS:{
        $moIncident = new CIIncident();
        $moIncident->fetchById($piContextId);
        
        $moHandler = new QueryDisciplinaryProcessUsers(FWDWebLib::getConnection());
        $moHandler->setIncident($piContextId);
        $moHandler->makeQuery();
        $moHandler->executeQuery();
        $maUsers = $moHandler->getUsers();
        if($maUsers){
        	$msUsers = implode(', ', $maUsers);        
        	$msDescription = str_replace(array('%users%','%name%'),array($msUsers, $moIncident->getFieldValue('incident_name')),$this->caAlert[$piStatus]);
        }
        break;
      }
      
      case WKF_ALERT_INCIDENT_RISK_PARAMETRIZATION:{
        $moRisk = new RMRisk();
        $moRisk->fetchById($piContextId);
        $moAsset = new RMAsset();
        $moAsset->fetchById($moRisk->getFieldValue('risk_asset_id'));                
        $msDescription = str_replace(array('%risk_name%','%asset_name%'),array($moRisk->getFieldValue('risk_name'), $moAsset->getFieldValue('asset_name')),$this->caAlert[$piStatus]);
        break;
      }
      
			default:
				$moContextObject = new ISMSContextObject();
				$moContext = $moContextObject->getContextObject($piContextType);
				$msDescription = $moContext->getLabel() . ' ' . $psContextName . ' ' . $this->caAlert[$piStatus]; 
			break;
		}		
		
		return $msDescription;	
	}
	else trigger_error("Alert status id not found: $piStatus", E_USER_WARNING);
  }

 /**
  * Insere um registro na tabela.
  * 
  * <p>M�todo para inserir um registro na tabela.</p>
  * @param boolean $pbGetId Verdadeiro para retornar o id do registro inserido
  * @access public 
  */
  public function insert($pbGetId = false, $pbSendEmail = true){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
          
    $mbToBeSent = true;
    if(ISMSLib::getConfigById(GENERAL_EMAIL_ENABLED) && $pbSendEmail){
      $miReceiverId = $this->getFieldValue('alert_receiver_id');
      $moEmailPreferences = new ISMSEmailPreferences();
      if($moEmailPreferences->getDigestType($miReceiverId)=='none'){
        $miContextId = $this->getFieldValue('alert_context_id');
        $moContextObject = new ISMSContextObject();
        $moContext = $moContextObject->getContextObjectByContextId($miContextId);
        $miContextType = $moContext->getContextType();
        if($moEmailPreferences->isMessageTypeSet($miContextType,$miReceiverId)){
          $moReceiver = new ISMSUser();
          $moReceiver->fetchById($miReceiverId);
          if($moReceiver->getContextState()!=CONTEXT_STATE_DELETED){
            $msReceiverEmail = $moReceiver->getFieldValue('user_email');
            
            $moContext->fetchById($miContextId);
            $miStatus = $this->getFieldValue('alert_type');
            $msContextName = $moContext->getName();
            $msDescription = $this->getDescription($miStatus,$miContextType,$msContextName,$miContextId);
            $msJustification = $this->getFieldValue('alert_justification');
            $msDate = ISMSLib::getISMSDate($this->getFieldValue('alert_date'),true);
            
            $miSenderId = $this->getFieldValue('alert_creator_id');
            $moSender = new ISMSUser();
            $moSender->fetchById($miSenderId);
            $msSender = $moSender->getName();
            
            $moEmailPreferences = new ISMSEmailPreferences();
            $msFormat = $moEmailPreferences->getEmailFormat($miReceiverId);
            
            $msSystemName = ISMSLib::getSystemName();
            
            $moMailer = new ISMSMailer();
            $moMailer->setFormat($msFormat);
            $moMailer->setTo($msReceiverEmail);
            $moMailer->setSubject("[$msSystemName] " . date('d/m/Y') . ' - '.FWDLanguage::getPHPStringValue('wk_warning','Alerta'));
            if($msJustification){
              $moMailer->addContent('alertWithJustification');
              $moMailer->setParameter('justification',FWDWebLib::convertToISO($msJustification, true));
            }else{
              $moMailer->addContent('alert');
            }
            $moMailer->setParameter('description',$msDescription);
            $moMailer->setParameter('sender',$msSender);
            $moMailer->setParameter('date',$msDate);
            $moMailer->setFooterMessage(FWDLanguage::getPHPStringValue('em_alert_check_list','<b>Para visualizar o alerta acesse o sistema e verifique sua lista de atividades.</b><br>Para maiores informa��es sobre este alerta entre em contato com o security officer.'));
            $moMailer->send();
            
            $this->setFieldValue('alert_email_sent',true);
          }
        }
        $mbToBeSent = false; // ou foi enviado, ou o usu�rio n�o quer receber mesmo
      }
    }else{
      $mbToBeSent = false;
    }
    if(!$mbToBeSent) $this->setFieldValue('alert_email_sent',true);
    parent::insert($pbGetId);
  }

}
?>