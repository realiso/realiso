<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */
 
include_once $handlers_ref . "QueryProfileIsDeletable.php";

/*
 * Constantes que identificam os perfis que
 * s�o manipuladoas apenas pelo sistema.
 */
define('PROFILE_SECURITY_OFFICER', 8801);
define('PROFILE_MANAGER'         , 8802);
define('PROFILE_ADMINISTRATOR'   , 8803);
define('PROFILE_NO_PERMISSION'   , 8804);
define('PROFILE_DOCUMENT_READER' , 8805);

/**
 * Classe ISMSProfile.
 *
 * <p>Classe que representa a tabela de perfis.</p>
 * @package ISMS
 * @subpackage classes
 */
class ISMSProfile extends ISMSContext {

 /**
  * Construtor.
  * 
  * <p>Construtor da classe ISMSProfile.</p>
  * @access public 
  */
  public function __construct(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    parent::__construct('isms_profile');
    
    $this->csAliasId = 'profile_id';
    $this->ciContextType = CONTEXT_PROFILE;
    
    $this->coDataset->addFWDDBField(new FWDDBField('fkContext','profile_id'        ,DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('sName'    ,'profile_name'      ,DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField('nId'      ,'profile_identifier',DB_NUMBER));
  }
  
 /**
  * Retorna um array com todas as ACLs negadas para o perfil.
  * 
  * <p>M�todo para retornar um array com todas as ACLs negadas para o perfil.
  * S� faz a consulta na primeira vez, ou seja, guarda o resultado na sess�o do usu�rio.</p>
  * @access public 
  * @param boolean $pbForce For�ar a cria��o do array de ACLs negadas mesmo que ja exista na sess�o
  * @return array Array de ACLs negadas
  */
  public function getDeniedAcls($pbForce = false){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $moGlobalSession = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId());
    if((count($moGlobalSession->getUserDeniedACLs()) == 0) || $pbForce){
      $maDeniedAcls = array_diff($this->getAllAcls(),$this->getAcls());
      $moGlobalSession->setUserDeniedACLs($maDeniedAcls);
    }
    return $moGlobalSession->getUserDeniedACLs();
  }
  
 /**
  * Retorna o nome do contexto.
  * 
  * <p>M�todo para retornar o nome do contexto.</p>
  * @access public 
  * @return string Nome do contexto
  */
  public function getName() {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return $this->getFieldValue('profile_name');
  }
  
 /**
  * Retorna o label do contexto.
  * 
  * <p>M�todo para retornar o label do contexto.</p>
  * @access public 
  * @return string Label do contexto
  */
  public function getLabel() {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    return FWDLanguage::getPHPStringValue('mx_profile', "Perfil");
  }

 /**
  * Retorna todas acls.
  * 
  * <p>Retorna em um Array todas acls.</p>
  * @access public
  * @return array Lista de todas tags de ACL do sistema
  */ 
  public function getAllAcls(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    include_once('acl_tags.php');
    return $laTags;
  }

 /**
  * Retorna uma lista com as ACLs que o profie tem permiss�o.
  * 
  * <p>Retorna uma lista com as ACLs que o profie tem permiss�o.</p>
  * @access public
  * @return array Array das Tags de ACL do perfil
  */
  public function getAcls(){
    $maProfileAcls = array();
    $moProfileAcl = new ISMSProfileAcl();
    $moProfileAcl->createFilter($this->getFieldValue('profile_id'),'profile_id');
    $moProfileAcl->select();
    while($moProfileAcl->fetch()){
      $maProfileAcls[] = $moProfileAcl->getFieldValue('acl_tag');
    }
    return $maProfileAcls;
  }

 /**
  * Insere as associa��es de um profile com suas ACLs no banco.
  * 
  * <p>Insere as associa��es de um profile com suas ACLs no banco.</p>
  * @access public
  * @param integer $piProfileId Identificador do profile
  * @param string $psAllowedACLs Tags a serem associadas separadas por ':'
  */
  public function insertAcls($piProfileId,$psAllowedACLs){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    if($piProfileId){
      $maCheckedTags = explode(':', $psAllowedACLs);
      $msCheckedTags = ":{$psAllowedACLs}:";
      $maTags = array();
      // Insere no array $maTags somente as tags que t�m todos ancestrais marcados
      // (as ra�zes s�o consideradas sempre marcadas)
      foreach($maCheckedTags as $msTag){
        $mbAllow = true;
        $maLevels = explode('.',$msTag);
        array_pop($maLevels);
        while(count($maLevels)>1){
          if(strpos($msCheckedTags,':'.implode('.',$maLevels).':')===false){
            $mbAllow = false;
            break;
          }
          array_pop($maLevels);
        }
        //Coloquei a verifica��o de $msTag abaixo para corrigir bug que tentava inserir uma tag vazia...
        if($msTag && $mbAllow){
          $maTags[] = $msTag;
        }
      }
      
      $moProfile = new ISMSProfile();
      $moProfile->setFieldValue('profile_id',$piProfileId);
      $moProfile->updateRelation(new ISMSProfileAcl(),'acl_tag',$maTags,true);
    }else{
      trigger_error("Invalid profile id.",E_USER_ERROR);
    }
  }

 /**
  * Insere um profile na tabela isms_profiles.
  * 
  * <p>Insere um profile na tabela isms_profiles e retorna o identificador do novo profile</p>
  * @access public 
  * @param string $psName Nome do profile
  * @return int Identificador do novo profile 
  */ 
  public function insert($psName){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $this->coDataset->getFieldByAlias('profile_name')->setValue($psName);
    return parent::insert(true);
  } 

 /**
  * Indica se o contexto � delet�vel ou n�o.
  * 
  * <p>M�todo que indica se o contexto � delet�vel ou n�o.</p>
  * @access public
  * @param integer $piContextId id do contexto
  */ 
  public function isDeletable($piContextId) {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $moQuery = new QueryProfileIsDeletable(FWDWebLib::getConnection());
    $moQuery->setProfileId($piContextId);
    $moQuery->makeQuery();
    $moQuery->executeQuery();
    return $moQuery->getIsDeletable();
  }
  
 /**
  * Exibe uma popup caso n�o seja poss�vel remover um contexto.
  * 
  * <p>M�todo que exibe uma popup caso n�o seja poss�vel remover um contexto.</p>
  * @access public
  * @param integer $piContextId id do contexto
  */ 
  public function showDeleteError($piContextId) {
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $msTitle = FWDLanguage::getPHPStringValue('tt_profile_remove_error','Erro ao remover Perfil');
    $msMessage = FWDLanguage::getPHPStringValue('st_profile_remove_error',"N�o foi poss�vel remover o perfil <b>%profile_name%</b>: Existem usu�rios associados a este perfil.");
    $moProfile = new ISMSProfile();
    $moProfile->fetchById($piContextId);
    $msProfileName = $moProfile->getFieldValue('profile_name');
    $msMessage = str_replace("%profile_name%",$msProfileName,$msMessage);
    ISMSLib::openOk($msTitle,$msMessage,"",60);
  }
  
  
 /**
  * Retorna se o usu�rio logado tem permi��o para inserir o contexto.
  * 
  * <p>M�todo para retornar se o usu�rio tem permi��o para inserir o contexto no sistema.</p>
  * @access public
  * @return boolean Se o usu�ro tem permi��o para inserir o contexto no sistema
  */
  protected function userCanInsert(){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $miContextType = $this->ciContextType;
    //contexto deve ser tratado, assim assume-se que a principio o usu�rio n�o pode inserir tal contexto
    $mbReturn = false;
    $miCount = 0;

    $maPermissions = array('A.P.3' );
    //acls negadas o usu�rio logado no sistema
    $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
    foreach($maACLs as $miKey => $miValue){
      $maACLs[$miValue]=$miKey;
    }
    //acl1 AND acl2
    $mbAuxPermission = true;
    foreach($maPermissions as $msValue){
      if(isset($maACLs[$msValue])){
        $mbAuxPermission = false;
      }
      $miCount++;
    }
    //caso exista pelo menos 1 acl nas permi��es do contexto e estas permi��es n�o est�o nas permi��es negadas do usu�rio
    if(($mbAuxPermission)&&($miCount)){
      $mbReturn = $mbAuxPermission;
    }
    return $mbReturn;
  }

 /**
  * Retorna se o usu�rio logado tem permi��o para editar o contexto.
  * 
  * <p>M�todo para retornar se o usu�rio tem permi��o para editar o contexto no sistema.</p>
  * @access protected
  * @return boolean Se o usu�ro tem permi��o de editar o contexto no sistema
  */
  protected function userCanEdit($piContextId,$piUserResponsible = 0){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    $moProfile = new ISMSProfile();
    $moProfile->fetchById($piContextId);
    if($moProfile->getFieldValue('profile_identifier') > 0){
      return false;
    }
    
    if(!$this->contextIsOfType($piContextId,$this->ciContextType)) return false;
    //contexto deve ser tratado, assim assume-se que a principio o usu�rio n�o pode editar esse contexto
    $mbReturn = false;
    $maPermissions = array('A.P.1' );
    //acls negadas do usu�rio logado no sistema
    $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
    foreach($maACLs as $miKey => $miValue){
      $maACLs[$miValue]=$miKey;
    }
    $miCount = 0;
    //acl1 AND acl2
    $mbAuxPermission = true;
    foreach($maPermissions as $msValue){
      if(isset($maACLs[$msValue])){
        $mbAuxPermission = false;
      }
      $miCount++;
    }
    //caso exista pelo menos 1 acl nas permi��es do contexto e estas permi��es n�o est�o nas permi��es negadas do usu�rio
    if(($mbAuxPermission)&&($miCount)){
      $mbReturn = $mbAuxPermission;
      }
    return $mbReturn;
  }
 
 /**
  * Retorna se o usu�rio logado tem permi��o para excluir o contexto.
  * 
  * <p>M�todo para retornar se o usu�rio tem permi��o para excluir o contexto no sistema.</p>
  * @access protected
  * @return boolean Se o usu�ro tem permi��o de excluir o contexto no sistema
  */
  protected function userCanDelete($piContextId){
  	$maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    //tester esquemas de respons�vel aq nesse teste
    $miContextType = $this->ciContextType;
    if(!$this->contextIsOfType($piContextId,$miContextType)) return false;
    //contexto deve ser tratado, assim assume-se que a principio o usu�rio n�o pode inserir tal contexto
    $mbReturn = false;
    $maPermissions = array( 'A.P.2' );

    //acls negadas do usu�rio logado no sistema
    $maACLs = FWDWebLib::getInstance()->getSessionById(ISMSLib::getCurrentSessionId())->getUserDeniedACLs();
    foreach($maACLs as $miKey => $miValue){
      $maACLs[$miValue]=$miKey;
    }
    $miCount = 0;
    //acl1 AND acl2
    $mbAuxPermission = true;
    foreach($maPermissions as $msValue){
      if(isset($maACLs[$msValue])){
        $mbAuxPermission = false;
      }
      $miCount++;
    }
    //caso exista pelo menos 1 acl nas permi��es do contexto e estas permi��es n�o est�o nas permi��es negadas do usu�rio
    if(($mbAuxPermission)&&($miCount)){
      $mbReturn = $mbAuxPermission;
    }
    return $mbReturn;
  }  
}
?>