<?php
/**
 * ISMS - INTERNET SECURITY MANAGEMENT SYSTEM
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe WKFTaskSchedule
 *
 * <p>Classe que representa um agendamento de tarefa.</p>
 * @package ISMS
 * @subpackage classes
 */

class WKFTaskSchedule extends ISMSTable {

 /**
  * Construtor.
  *
  * <p>Construtor da classe WKFTaskSchedule.</p>
  * @access public
  */
  public function __construct(){
    $maParameters = func_get_args();
    FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_INFO,__FILE__,__LINE__);
    
    parent::__construct('wkf_task_schedule');
    
    $this->coDataset->addFWDDBField(new FWDDBField('fkSchedule','task_schedule_schedule'  ,DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('fkContext' ,'task_schedule_context'   ,DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('nActivity' ,'task_schedule_activity'  ,DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('nAlertType','task_schedule_alert_type',DB_NUMBER));
    $this->coDataset->addFWDDBField(new FWDDBField('bAlertSent','task_schedule_alert_sent',DB_NUMBER));
  }

}

?>