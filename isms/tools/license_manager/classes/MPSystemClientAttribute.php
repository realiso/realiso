<?php
/**
 * MINI PROJECTS
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe MPSystemClientAttribute.
 *
 * <p>Classe que representa a tabela de atributos associados a um sistema associado
 * a um cliente.</p>
 * @package MINI_PROJECTS
 * @subpackage classes
 */
class MPSystemClientAttribute extends MPTable {
	
	/**
	 * Construtor.
	 * 
	 * <p>Construtor da classe MPSystemClientAttribute.</p>
	 * @access public 
	 */
	public function __construct(){		
		parent::__construct("lic_system_client_attribute");
		$this->coDataset->addFWDDBField(new FWDDBField("skSystem",			"system_id",							DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("skAttribute",		"attribute_id",						DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("skClient",			"client_id",							DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("zValue", 				"attribute_value",				DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField("skLicense",     "license_id",             DB_NUMBER));
	}
  
	/**
	 * Busca informa��es de atributo de um sistema de um cliente.
	 * 
	 * <p>M�todo para buscar informa��es de atributo de um sistema de um cliente
	 * atrav�s das chaves prim�rias do sistema, do atributo e do cliente.</p>
	 * @access public
	 * @param integer $piSystemId Identificador do sistema
	 * @param integer $piAttributeId Identificador do atributo
	 * @param integer $piClientId Identificador do atributo
	 */
	public function fetch($piSystemId,$piAttributeId,$piClientId) {
		$this->createFilter($piSystemId, "system_id");
		$this->createFilter($piAttributeId, "attribute_id");
		$this->createFilter($piClientId, "client_id");
		$this->coDataset->select();
		return $this->fetch();
  }
}
?>