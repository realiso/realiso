<?php
/**
 * MINI PROJECTS
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe MPLicenseActivation.
 *
 * <p>Classe que representa a tabela de desativações de licenças.</p>
 * @package MINI_PROJECTS
 * @subpackage classes
 */
class MPLicenseDeactivation extends MPTable {
	
	/**
	 * Construtor.
	 * 
	 * <p>Construtor da classe MPLicenseDeactivation.</p>
	 * @access public 
	 */
	public function __construct(){		
		parent::__construct("lic_license_deactivation");
		$this->coDataset->addFWDDBField(new FWDDBField("skLicense",			    "license_id",							DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("dDatetime", 		    "date_time",				      DB_STRING));
	}
}
?>