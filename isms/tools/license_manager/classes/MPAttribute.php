<?php
/**
 * MINI PROJECTS
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe MPAttribute.
 *
 * <p>Classe que representa a tabela de atributos.</p>
 * @package MINI PROJECTS
 * @subpackage classes
 */
class MPAttribute extends MPTable {
	
	/**
	 * Construtor.
	 * 
	 * <p>Construtor da classe MPAttribute.</p>
	 * @access public
	 */
	public function __construct() {
		parent::__construct("lic_attribute");
		
    $this->csAliasId = 'attribute_id';
		$this->coDataset->addFWDDBField(new FWDDBField("skAttribute",		"attribute_id",						DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("skSystem",			"system_id",							DB_NUMBER));
		$this->coDataset->addFWDDBField(new FWDDBField("zName",					"attribute_name",					DB_STRING));
		$this->coDataset->addFWDDBField(new FWDDBField("zDescription",	"attribute_description",	DB_STRING));
	}
	
	/**
	 * Busca informa��es de um cliente atrav�s de seu identificador.
	 * 
	 * <p>M�todo para buscar informa��es de um cliente atrav�s de seu identificador.</p>
	 * @access public
	 * @param integer $piId Identificador
	 */
	public function fetchById($piId) {
		return parent::fetchById($piId, "attribute_id");
	}
}
?>