<?php

include_once "include.php";

class InitEvent extends FWDRunnable {
  public function run() {
    
    /** Lista de sistemas dispon�veis para associa��o **/
    $maAvailableSystems = array(22=>"ISMS 2.0",23=>"AMS 2.0");
        
    $miClientId = FWDWebLib::getObject("param_client_id")->getValue();
    
    $moSystemClient = new MPSystemClient();
    $moSystemClient->createFilter($miClientId,'client_id');
    $moSystemClient->select();
    $maClientSystems = array();
    while($moSystemClient->fetch()) {
      unset($maAvailableSystems[$moSystemClient->getFieldValue('system_id')]);
    }
    if (count($maAvailableSystems)) {
      $maDisplaySystems = array();
      foreach($maAvailableSystems as $miSystemId=>$msSystemName) {
        $maDisplaySystems[] = array($miSystemId,$msSystemName);
      }
    } else {
      $maDisplaySystems[] = array('',FWDLanguage::getPHPStringValue('no_system_available','N�o h� sistemas dispon�veis para associa��o'));
    }
    $msDisplaySystems = serialize($maDisplaySystems);
    echo "js_populate_select('select_system', '$msDisplaySystems');";
  }
}

class SaveAssociationEvent extends FWDRunnable {
	public function run() {
		$miClientId = FWDWebLib::getObject('param_client_id')->getValue();
    $miSystemId = FWDWebLib::getObject('select_system')->getValue();
    
    if ($miClientId && $miSystemId) {
      $moSystemClient = new MPSystemClient();
      $moSystemClient->setFieldValue('client_id',$miClientId);
      $moSystemClient->setFieldValue('system_id',$miSystemId);
      $moSystemClient->insert();
    }
    
    echo "soPopUp = soPopUpManager.getPopUpById('new_license_manager_system_client_association');
          soWindow = soPopUp.getOpener();
          soWindow.trigger_event('init_event',3);
          soPopUpManager.closePopUp('new_license_manager_system_client_association');";
	}
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));
		$moStartEvent->addAjaxEvent(new SaveAssociationEvent("save_association"));
    $moStartEvent->addAjaxEvent(new InitEvent("init_event"));
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
    $miClientId = FWDWebLib::getObject("param_client_id")->getValue();
    if ($miClientId) {
      $moClient = new MPClient();
      $moClient->fetchById($miClientId);
      FWDWebLib::getObject("client_name")->setValue($moClient->getFieldValue("client_name"));
    }
    
		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
      <script language="javascript">
        trigger_event('init_event',3);
      </script>
    <?
	}

}
$soWebLib = FWDWebLib::getInstance();

$soStartEvent = FWDStartEvent::getInstance();
$soStartEvent->addBeforeAjax(new ScreenBeforeEvent(""));

$soWebLib->xml_load("new_license_manager_system_client_association.xml");

?>