<?php

include_once "include.php";

include_once $handlers_ref."QueryGridLicenseReportCompleteLicense.php";

class GridLicenseReport extends FWDDrawGrid {
  public function DrawItem(){
    switch($this->ciColumnIndex){
      case 3:
        if ($this->coCellBox->getValue() != 0)
          $this->coCellBox->setValue(date("d/m/Y",strtotime($this->coCellBox->getValue())));
        else 
          $this->coCellBox->setValue("");
        return parent::drawItem();
      case 4:
        switch($this->caData[7]) {
          case 1:$msPeriodType = "dia(s)";break;
          case 2:$msPeriodType = "semana(s)";break;
          case 3:$msPeriodType = "m�s(es)";break;
          case 4:$msPeriodType = "ano(s)";break;
          default:$msPeriodType = "m�s(es)";break;
        }
        $this->coCellBox->setValue($this->coCellBox->getValue()." {$msPeriodType}");
        return parent::drawItem();
      case 5:
        if ($this->caData[2]=='ISMS 2.0') {
          if ($this->coCellBox->getValue() == '99999')
            $this->coCellBox->setValue("Usu�rios ilimitados");
          else
            $this->coCellBox->setValue($this->coCellBox->getValue()." usu�rios");
        } elseif ($this->caData[2]=='AMS 2.0') {
          $this->coCellBox->setValue($this->coCellBox->getValue()." GB");
        }
        return parent::drawItem();
      case 6:
        if ($this->coCellBox->getValue() != 0)
          $this->coCellBox->setValue(date("d/m/Y",$this->coCellBox->getValue()));
        else 
          $this->coCellBox->setValue("");
        return parent::drawItem();
      default:
        return parent::drawItem();
    }
  }
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));
		
		$moGridLicenseReport = FWDWebLib::getObject("grid_license_report");
    $moHandler = new QueryGridLicenseReportCompleteLicense(FWDWebLib::getConnection());
    $moGridLicenseReport->setObjFwdDrawGrid(new GridLicenseReport());
    $moGridLicenseReport->setQueryHandler($moHandler);
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
	}

}

$soWebLib = FWDWebLib::getInstance();

$soStartEvent = FWDStartEvent::getInstance();
$soStartEvent->addBeforeAjax(new ScreenBeforeEvent(""));

$soWebLib->xml_load("license_manager_report_complete_license_list_with_data.xml");

?>