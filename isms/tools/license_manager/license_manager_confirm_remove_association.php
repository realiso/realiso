<?php
include_once "include.php";

class RemoveAssociationEvent extends FWDRunnable {
  public function run(){    
    $msClientSystemId = FWDWebLib::getObject('element_id')->getValue();
    list($miClientId,$miSystemId) = split(":",$msClientSystemId);
    
    $moSystemClient = new MPSystemClient();
    $moSystemClient->createFilter($miSystemId,"system_id");
    $moSystemClient->createFilter($miClientId,"client_id");
    $moSystemClient->delete();
    
    echo "soPopUp = soPopUpManager.getPopUpById('popup_confirm');
          soWindow = soPopUp.getOpener();
          soWindow.trigger_event('init_event',3);
          soPopUpManager.closePopUp('popup_confirm');
         ";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new RemoveAssociationEvent("remove_association"));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $msClientSystemId = FWDWebLib::getObject('element_id')->getValue();
    list($miClientId,$miSystemId) = split(":",$msClientSystemId);
    
    $msTitle = FWDLanguage::getPHPStringValue('association_remove_title','Remover Associa��o');
    $msMessage = FWDLanguage::getPHPStringValue('license_remove_message',"Voc� tem certeza que deseja excluir a associa��o de <b>%client_name%</b> com <b>%system_name%</b>?");
    $msIconSrc = 'gfx/'.'icon-exclamation.gif';
    
    $moClient = new MPClient();
    $moClient->fetchById($miClientId);
    $msClientName = $moClient->getFieldValue('client_name');
    
    $moSystem = new MPSystem();
    $moSystem->fetchById($miSystemId);
    $msSystemName = $moSystem->getFieldValue('system_name');
    
    $msMessage = str_replace("%client_name%",$msClientName,$msMessage);
    $msMessage = str_replace("%system_name%",$msSystemName,$msMessage);
    
    //FWDWebLib::getObject('confirm_title')->setValue($msTitle);
    FWDWebLib::getObject('confirm_message')->setValue($msMessage);
    FWDWebLib::getObject('confirm_icon')->setAttrSrc($msIconSrc);
    
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
    <script language="javascript">
      function remove_element() {
        trigger_event("remove_association",3);
      }
    </script>
    <?
  }
}

$soWebLib = FWDWebLib::getInstance();

$soStartEvent = FWDStartEvent::getInstance();
$soStartEvent->addBeforeAjax(new ScreenBeforeEvent(""));

$soWebLib->xml_load("confirm.xml");
?>
