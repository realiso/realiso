<?php
include_once "include.php";
include_once $handlers_ref."/QueryAttributesInfoByLicenseId.php";

class StaticVariables {
  static $ssPublicKey = "-----BEGIN CERTIFICATE-----
MIIEFzCCA4CgAwIBAgIBATANBgkqhkiG9w0BAQQFADCBujELMAkGA1UEBhMCQlIx
GjAYBgNVBAgTEVJpbyBHcmFuZGUgZG8gU3VsMRUwEwYDVQQHEwxQb3J0byBBbGVn
cmUxIjAgBgNVBAoTGUF4dXIgSW5mb3JtYXRpb24gU2VjdXJpdHkxFjAUBgNVBAsT
DWxpY2Vuc2UgZGVwdC4xHDAaBgNVBAMTE0F4dXIgTGljZW5zZSBTeXN0ZW0xHjAc
BgkqhkiG9w0BCQEWD2Fsc0BheHVyLmNvbS5icjAeFw0wNDA2MDkxNzU0NTBaFw0w
OTA2MTAxNzU0NTBaMIG6MQswCQYDVQQGEwJCUjEaMBgGA1UECBMRUmlvIEdyYW5k
ZSBkbyBTdWwxFTATBgNVBAcTDFBvcnRvIEFsZWdyZTEiMCAGA1UEChMZQXh1ciBJ
bmZvcm1hdGlvbiBTZWN1cml0eTEWMBQGA1UECxMNbGljZW5zZSBkZXB0LjEcMBoG
A1UEAxMTQXh1ciBNb25pdG9yIFN5c3RlbTEeMBwGCSqGSIb3DQEJARYPYW1zQGF4
dXIuY29tLmJyMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCzKZ+FEPy6DbvF
IOQ6hp6pCnth0kr5FFSh39pMsq0ytv4RRAxnWxjPvyfx0w7vpnKbARF8zDaStN5K
JIxocA7SYmYM2ItYZdAu+6JU1m9/EYT8eb7w110cYdEIJJr+CPrx2iivXVYBbl9J
VkKlf/1xFnkwhIlZ6CYAwFfnxdIswwIDAQABo4IBKTCCASUwCQYDVR0TBAIwADAP
BglghkgBhvhCAQ0EAhYAMB0GA1UdDgQWBBTAEH8Y7ZVtOpNxtuO5djrXmDp59zCB
5wYDVR0jBIHfMIHcgBTIsD9pVTsIJx25scfn1t6xQmdPZaGBwKSBvTCBujELMAkG
A1UEBhMCQlIxGjAYBgNVBAgTEVJpbyBHcmFuZGUgZG8gU3VsMRUwEwYDVQQHEwxQ
b3J0byBBbGVncmUxIjAgBgNVBAoTGUF4dXIgSW5mb3JtYXRpb24gU2VjdXJpdHkx
FjAUBgNVBAsTDWxpY2Vuc2UgZGVwdC4xHDAaBgNVBAMTE0F4dXIgTGljZW5zZSBT
eXN0ZW0xHjAcBgkqhkiG9w0BCQEWD2Fsc0BheHVyLmNvbS5icoIBADANBgkqhkiG
9w0BAQQFAAOBgQAabwBzSL+Ee3up3MxfE0gz4UAmjtaJMTlmclz+fGX22CVVwMXX
1WhH4QacRA9UnZp5tYEAwPMXpOGrlgR0tAkI3O5SaLaki6qc07h5mqfczN2L2BMp
LvUwF7gTTmNITm+ooJRP96Q+16G/UhUp4FNQvJyG2OmovKnH/8ZKuGgwlQ==
-----END CERTIFICATE----- ";
  static $ssPrivateKey = "-----BEGIN RSA PRIVATE KEY-----
MIICXAIBAAKBgQCzKZ+FEPy6DbvFIOQ6hp6pCnth0kr5FFSh39pMsq0ytv4RRAxn
WxjPvyfx0w7vpnKbARF8zDaStN5KJIxocA7SYmYM2ItYZdAu+6JU1m9/EYT8eb7w
110cYdEIJJr+CPrx2iivXVYBbl9JVkKlf/1xFnkwhIlZ6CYAwFfnxdIswwIDAQAB
AoGAZmVTlmmvB2bx+ihiSFDIPTSQb8gQsWq9NAcMbOlxs5qCeru5mxilhZZq7fsC
flVTCuQBaqIMTqywnw9kcAwba42j8v1rTfM95tXqLSUigQO5QvEQ6YqKOG9wDIz9
4C52w9mbHzwBUQbKrvZ8tL/4rtVuFsVhJJQ0s8FAqm1j+wECQQDWklvP5BHLLkEW
cPAbXw/8ZYJD1GqQ9YsRIBl+EqWsgYy5aPZ4RCrh0c3CaZ/sHwFOE9CKNlJb6I36
yWRPerBZAkEA1cEYzWUJ0AaviW/sceh6MbQnk7diHA74Qva6ZikdxaBPQSAA6Tit
7ESA7YxlgS/OD0dwB4CP/Hvx6S0blpzCewJBALuZnQInAlOlbizDs3oK5DwlN/47
8qwoslzXttIeVZF8duAIYC2IVAG54G8Q0EyuUwLDmjmtAtbufWv9tmEzAIkCQCmv
r9OWqdQ9CYzHHBiW2wXIeZNwRxzrunTswytbR2gcPHiZ8jOJjzmtnms1XzJTjV8j
cnC0HOCDl4j++AtmZPMCQBZswfRiZYlHLrgzm+SRfwDfNzeLR9qmMPgsdPO2dx/y
pSQ9YXv6lnY2yTQ1Qtt+ePB4IYOFM9N6XCXDvxdZio0=
-----END RSA PRIVATE KEY----- ";  
}


function readLicenseData($piLicense) {
  $moLicense = new FWDLicense(StaticVariables::$ssPublicKey);
  $moLicense->setPrivateKey(StaticVariables::$ssPrivateKey);
  $maAllowedAttributes = array(76,77,81,82,85,86,92,145);
  $moAttrQuery = new QueryAttributesInfoByLicenseId(FWDWebLib::getConnection());
  $moAttrQuery->setLicenseId($piLicense);
  $moAttrQuery->makeQuery();
  $moAttrQuery->executeQuery();
  $mbCanGenerateLicense = true;
  foreach ($moAttrQuery->getAttributes() as $miAttrId => $maAttrInfo) {
    if (!in_array($miAttrId,$maAllowedAttributes)) continue;
    $moLicense->setAttribute($maAttrInfo["name"], $maAttrInfo["value"]);
    if (trim($maAttrInfo["value"])=="") $mbCanGenerateLicense = false;
  }
  if ($mbCanGenerateLicense) {
    $msLicense = $moLicense->generateLicenseKey();
    $msLicense = str_replace("--------------------- BEGIN LICENSE KEY ------------------------\n","",$msLicense);
    $msLicense = str_replace("---------------------- END LICENSE KEY -------------------------\n","",$msLicense);
    return trim($msLicense);
  } else {
    return "";
  }
}

class DeactivateEvent extends FWDRunnable {
  public function run() {
    $msDeactivationHash = FWDWebLib::getObject('deactivation_hash')->getValue();
    $msDeactivationHash = base64_decode($msDeactivationHash);
    
    $moCrypt = new FWDCrypt();
    $moCrypt->setIV(substr($msDeactivationHash,0,strlen($moCrypt->getIV())));
    $msDeactivationHash = substr($msDeactivationHash,strlen($moCrypt->getIV()));
    $msDeactivationHash = $moCrypt->decrypt($msDeactivationHash);
    
    $maParts = explode('|',$msDeactivationHash);
    
    $msCodedDeactivationHash = @gzuncompress(@base64_decode($maParts[0])); //SIGNED HASH
    $msCodedToken = @gzuncompress(@base64_decode($maParts[1])); //DEACTIVATED
    $msTimeStamp = gzuncompress(base64_decode($maParts[2]));
    
    $msDeactivationHash = base64_decode($msCodedDeactivationHash);
    $maDeactivationHash = explode('|',$msDeactivationHash);
    $msLicenseHash = $maDeactivationHash[1];

    $miSystemClientId = 0;
    if(FWDWebLib::getObject('advanced_deactivation')->getValue() == 1) {
      $moLicense = new MPLicense();
      $moLicense->select();
      while($moLicense->fetch()) {
        $miLicenseId=$moLicense->getFieldValue('license_id');
        $msReadLicenseHash = readLicenseData($miLicenseId);
         for($miI=0;$miI<10;$miI++) {
          $msCompareLicenseHash = $msReadLicenseHash.str_repeat(PHP_EOL,$miI);
          if (md5($msCompareLicenseHash) == $msLicenseHash) {
            $miSystemClientId = $moLicense->getFieldValue('system_client_id');
            break 2;
          }
        }
      }
    } else {
      $moLicense = new MPLicense();
      $moLicense->createFilter($msLicenseHash,'license_hash');
      $moLicense->select();
      if ($moLicense->fetch()) {
        $miLicenseId=$moLicense->getFieldValue('license_id');
        $miSystemClientId = $moLicense->getFieldValue('system_client_id');
      }
    }
    if ($miSystemClientId) {
      $moLicenseDeactivation = new MPLicenseDeactivation();
      $moLicenseDeactivation->setFieldValue('license_id',$miLicenseId);
      $moLicenseDeactivation->setFieldValue('date_time',date('Y-m-d H:i:s'));
      $moLicenseDeactivation->insert();
      
      $moLicense = new MPLicense();
      $moLicense->createFilter($miLicenseId,'license_id');
      $moLicense->setFieldValue('system_activated',0);
      $moLicense->update();
      
      $moSystemClient = new MPSystemClient();
      $moSystemClient->createFilter($miSystemClientId,'system_client_id');
      $moSystemClient->select();
      if ($moSystemClient->fetch()) {
        $miClientId = $moSystemClient->getFieldValue('client_id');
        $moClient = new MPClient();
        if ($moClient->fetchById($miClientId)) {
          echo "gobi('client_name').setValue('".$moClient->getFieldValue('client_name')."');";
        }
      }
    } else {
      //nao encontrou nenhum cliente
      echo "alert('Cliente n�o encontrado!');";
    }
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new DeactivateEvent("deactivate"));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
  }
}
$soWebLib = FWDWebLib::getInstance();
$soStartEvent = FWDStartEvent::getInstance();
$soStartEvent->addBeforeAjax(new ScreenBeforeEvent(""));
$soWebLib->xml_load("deactivation.xml");
?>