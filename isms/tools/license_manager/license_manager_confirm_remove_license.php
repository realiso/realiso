<?php
include_once "include.php";

class RemoveLicenseEvent extends FWDRunnable {
  public function run(){    
    $miLicenseId = FWDWebLib::getObject('element_id')->getValue();
    
    $moMPLicense = new MPLicense();
    $moMPLicense->createFilter($miLicenseId,"license_id");
    $moMPLicense->delete();
    
    echo "soPopUp = soPopUpManager.getPopUpById('popup_confirm');
          soWindow = soPopUp.getOpener();
          soWindow.trigger_event('change_system',3);
          soPopUpManager.closePopUp('popup_confirm');
         ";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(""));
    $moStartEvent->addAjaxEvent(new RemoveLicenseEvent("remove_license"));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $miLicenseId = FWDWebLib::getObject("element_id")->getValue();
    $msTitle = FWDLanguage::getPHPStringValue('license_remove_title','Remover Licen�a');
    $msMessage = FWDLanguage::getPHPStringValue('license_remove_message',"Voc� tem certeza que deseja excluir a licen�a de <b>%license_date%</b>?");
    $msIconSrc = 'gfx/'.'icon-exclamation.gif';
    
    $moMPLicense = new MPLicense();
    $moMPLicense->fetchById($miLicenseId);
    $msLicenseDate = date('d/m/Y',strtotime($moMPLicense->getFieldValue('date_time')));
    $msMessage = str_replace("%license_date%",$msLicenseDate,$msMessage);
    
    //FWDWebLib::getObject('confirm_title')->setValue($msTitle);
    FWDWebLib::getObject('confirm_message')->setValue($msMessage);
    FWDWebLib::getObject('confirm_icon')->setAttrSrc($msIconSrc);
    
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
    <script language="javascript">
      function remove_element() {
        trigger_event("remove_license",3);
      }
    </script>
    <?
  }
}

$soWebLib = FWDWebLib::getInstance();

$soStartEvent = FWDStartEvent::getInstance();
$soStartEvent->addBeforeAjax(new ScreenBeforeEvent(""));

$soWebLib->xml_load("confirm.xml");
?>
