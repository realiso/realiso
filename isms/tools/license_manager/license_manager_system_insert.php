<?php

include_once "include.php";

class SaveSystemNameEvent extends FWDRunnable {

	public function run() {
		$msSystemName = FWDWebLib::getObject('var_system_name')->getValue();
		$moSystem = new MPSystem();
		$moSystem->setFieldValue("system_name",$msSystemName);
		$miSystemId = $moSystem->insert(true);
		
		/*** CAMPOS OBRIGATÓRIOS PARA TODOS OS SISTEMAS ***********************************/
		$moAttribute = new MPAttribute();
		// expiracy (timestamp)
		$moAttribute->setFieldValue("system_id",$miSystemId);
		$moAttribute->setFieldValue("attribute_name","expiracy");
		$moAttribute->setFieldValue("attribute_description","Data de expiracao (timestamp)");
		$moAttribute->insert();
		// expiracy (string)
		$moAttribute->setFieldValue("system_id",$miSystemId);
		$moAttribute->setFieldValue("attribute_name","expiracy_str");
		$moAttribute->setFieldValue("attribute_description","Data de expiracao (string)");
		$moAttribute->insert();
		/**********************************************************************************/
		echo "soPopUp = soPopUpManager.getPopUpById('license_manager_system_insert');
          soWindow = soPopUp.getOpener();
          soWindow.update_selects('all');
		      soPopUpManager.closePopUp('license_manager_system_insert');";
	}

}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent(""));
		
		$moStartEvent->addAjaxEvent(new SaveSystemNameEvent("save_system_name_event"));
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
	}

}

$soWebLib = FWDWebLib::getInstance();

$soStartEvent = FWDStartEvent::getInstance();
$soStartEvent->addBeforeAjax(new ScreenBeforeEvent(""));

$soWebLib->xml_load("license_manager_system_insert.xml");

?>