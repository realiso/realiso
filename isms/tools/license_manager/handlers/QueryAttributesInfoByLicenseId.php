<?php
/**
 * MINI PROJECTS
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryAttributesInfoByLicenseId.
 *
 * <p>Classe que busca informa��es de atributos (nome, descricao e valor,
 * indexados pelo id do atributo) associados a uma licen�a de um sitema de um cliente,
 * atrav�s do id da licen�a, do id do sistema e do id do cliente.</p>
 * @package MINI PROJECTS
 * @subpackage handlers
 */
class QueryAttributesInfoByLicenseId extends FWDDBQueryHandler {

  protected $ciLicenseId = 0;
	
	protected $caAttributes = array();

  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField('a.skAttribute' ,'attr_id'         ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('a.zName'       ,'attr_name'       ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('sca.zValue'    ,'attr_value'      ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('a.zDescription','attr_description',DB_STRING));
  }
	
	public function makeQuery() {
		if (!$this->ciLicenseId)
			return false;
		
		$this->csSQL = "
SELECT a.skAttribute as attr_id, 
       a.zName as attr_name, 
       sca.zValue as attr_value, 
       a.zDescription as attr_description
	FROM lic_attribute a, 
       lic_license_attribute sca
	WHERE a.skAttribute = sca.skAttribute 
    AND sca.skLicense = {$this->ciLicenseId}
	ORDER BY attr_name
";
	}
	
	public function executeQuery() {
		parent::executeQuery();		
		while ($this->coDataSet->fetch()) {
			$miAttributeId = $this->coDataSet->getFieldByAlias("attr_id")->getValue();
			$msAttributeName = $this->coDataSet->getFieldByAlias("attr_name")->getValue();
			$msAttributeDescription = $this->coDataSet->getFieldByAlias("attr_description")->getValue();
			$msAttributeValue = $this->coDataSet->getFieldByAlias("attr_value")->getValue();
			
      $this->caAttributes[$miAttributeId] = array("name" => $msAttributeName,
																									"description" => $msAttributeDescription,
																									"value" => $msAttributeValue);
		}
	}
	
  public function setLicenseId($piLicenseId) {
    $this->ciLicenseId = $piLicenseId;
  }
  
	public function getAttributes() {
		return $this->caAttributes;
	}
}
?>