<?php
/**
 * MINI PROJECTS
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QuerySystemsByClientId.
 *
 * <p>Classe que busca informa��es de todos os sistemas que est�o associados a
 * um cliente, atrav�s do id deste cliente.</p>
 * @package MINI PROJECTS
 * @subpackage handlers
 */
class QuerySystemsByClientId extends FWDDBQueryHandler {
	
	protected $caSystems = array();
	protected $ciClientId = 0;
	protected $cbNotIn = false;

  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField('s.skSystem','system_id'  ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('s.zName'   ,'system_name',DB_STRING));
  }
	
	public function makeQuery() {
		$this->csSQL = "
SELECT s.skSystem as system_id, 
       s.zName as system_name
	FROM lic_system s
	WHERE s.skSystem ".( $this->cbNotIn? "NOT IN":"IN" )."
			(
  			SELECT skSystem
    			FROM lic_system_client
    			WHERE skClient = {$this->ciClientId}
			)
	ORDER BY system_name
";
	}
	
	public function executeQuery() {
		parent::executeQuery();		
		while ($this->coDataSet->fetch()) {
			$miSystemId = $this->coDataSet->getFieldByAlias("system_id")->getValue();
			$this->caSystems[$miSystemId] = $this->coDataSet->getFieldByAlias("system_name")->getValue();
		}
	}
	
	public function setClientId($piClientId) {
		$this->ciClientId = $piClientId;
	}
	
	public function setNotIn($pbNotIn) {
		$this->cbNotIn = $pbNotIn;
	}
  
  public function getSystems() {
		return $this->caSystems;
	}
}
?>