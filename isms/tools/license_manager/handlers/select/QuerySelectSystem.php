<?php
/**
 * MINI PROJECTS
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QuerySelectSystem.
 *
 * <p>Classe que busca informações de sistemas.</p>
 * @package MINI PROJECTS
 * @subpackage handlers/select
 */
class QuerySelectSystem extends FWDDBQueryHandler {
  protected $caSystems=array();
  protected $caSystemsSelect=array();
  protected $ciClientId;
  
  public function __construct($poDB) {
    parent::__construct($poDB);
    
    $this->coDataSet->addFWDDBField(new FWDDBField('s.skSystem','select_id'   ,DB_UNKNOWN));
    $this->coDataSet->addFWDDBField(new FWDDBField('s.zName'   ,'select_value',DB_UNKNOWN));
  }

  public function setClient($piClientId) {
    $this->ciClientId = $piClientId;
  }
  
  public function makeQuery() { 
    $msClient = "";
    if ($this->ciClientId) {
      $msClient = "JOIN lic_system_client_attribute sca ON (s.skSystem = sca.skSystem AND sca.skClient={$this->ciClientId})";
    }
      
    $this->csSQL = "
SELECT s.skSystem as select_id, 
       s.zName as select_value
  FROM lic_system s
    $msClient
  ORDER BY select_value
";        
  }
  
  public function executeQuery() {
    parent::executeQuery();   
    while ($this->coDataSet->fetch()) {
      $miSystemId = $this->coDataSet->getFieldByAlias("select_id")->getValue();
      $this->caSystems[$miSystemId] = $this->coDataSet->getFieldByAlias("select_value")->getValue();
    }
  }
  
  public function getSystems() {
    return $this->caSystems;
  }
  
  /**
   * Retorna um array pronto para ser populado em um select.
   */
  public function getSelect() {
    foreach($this->caSystems as $miSystemId=>$msSystemName) {
      $this->caSystemsSelect[] = array($miSystemId,$msSystemName);
    }
    return serialize($this->caSystemsSelect);
  }
}
?>