<?php

include_once "include.php";
include_once $handlers_ref."QueryAttributesInfoBySystemIdAndClientId.php";

class CloneLicenseEvent extends FWDRunnable {
  public function run() {
    $miSystemId = FWDWebLib::getObject('system_id')->getValue();
    $miClientId = FWDWebLib::getObject('client_id')->getValue();
    $msNewClientName = FWDWebLib::getObject('client_name')->getValue();
    $msNewExpiracy = FWDWebLib::getObject('expiracy_date')->getTimestamp();

    $moClient = new MPClient();
    $moClient->setFieldValue('client_name',$msNewClientName);
    $miNewClientId = $moClient->insert(true);

    $moAttrQuery = new QueryAttributesInfoBySystemIdAndClientId(FWDWebLib::getConnection());
    $moAttrQuery->setSystemId($miSystemId);
    $moAttrQuery->setClientId($miClientId);
    $moAttrQuery->makeQuery();
    $moAttrQuery->executeQuery();
    foreach ($moAttrQuery->getAttributes() as $miAttrId => $maAttrInfo) {
      $moNewSystemClientAttribute = new MPSystemClientAttribute();
      $moNewSystemClientAttribute->setFieldValue('system_id',$miSystemId);
      $moNewSystemClientAttribute->setFieldValue('client_id',$miNewClientId);
      $moNewSystemClientAttribute->setFieldValue('attribute_id',$miAttrId);
      if ($maAttrInfo["name"]=="client") {
        $moNewSystemClientAttribute->setFieldValue('attribute_value',$msNewClientName);
      } elseif ($maAttrInfo["name"]=="expiracy") {
        $moNewSystemClientAttribute->setFieldValue('attribute_value',$msNewExpiracy);
      } elseif ($maAttrInfo["name"]=="expiracy_str") {
        $moNewSystemClientAttribute->setFieldValue('attribute_value',date('d/m/Y',$msNewExpiracy));
      } else {
        $moNewSystemClientAttribute->setFieldValue('attribute_value',$maAttrInfo["value"]);
      }
      $moNewSystemClientAttribute->insert();
    }
    
    echo "
          soPopUp = soPopUpManager.getPopUpById('license_manager_clone');
          soWindow = soPopUp.getOpener();
          soWindow.populate_client();
          soPopUpManager.closePopUp('license_manager_clone');
        ";
  }
}

class ScreenBeforeEvent extends FWDRunnable {
	public function run(){
		$moStartEvent = FWDStartEvent::getInstance();
		$moStartEvent->setScreenEvent(new ScreenEvent("screen_event"));
    $moStartEvent->addAjaxEvent(new CloneLicenseEvent("clone_license"));
	}
}

class ScreenEvent extends FWDRunnable {
	public function run(){
    $miSystemId = FWDWebLib::getObject('param_sys_id')->getValue();
    FWDWebLib::getObject('system_id')->setValue($miSystemId);
    
    $miClientId = FWDWebLib::getObject('param_cli_id')->getValue();
    FWDWebLib::getObject('client_id')->setValue($miClientId);
         
		FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    ?>
  		<script language="javascript">
  		</script>
    <?
	}
}

$soWebLib = FWDWebLib::getInstance();

$soStartEvent = FWDStartEvent::getInstance();
$soStartEvent->addBeforeAjax(new ScreenBeforeEvent(""));

$soWebLib->xml_load("license_manager_clone.xml");

?>