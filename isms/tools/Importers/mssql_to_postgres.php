<?php

set_time_limit(0);
// DEBUG - define um limite pra testar a importan��o mais facil
// n�o remover, s� mudar para 0
define("LIMIT", 0);

// configura��es
$nameDataSource = "boreal";
$nameTarget = "boreal_import";
$dbHost = '127.0.0.1:5432';
$dbUser = 'isms';
$dbPass = 'isms';
$userMail = '';
$instanceConfigPath = '';


// incluir arquivos necess�rios
$lib_ref = "lib/";
require_once($lib_ref . "adodb/adodb.inc.php");
require_once($lib_ref . "fpdf/fpdf.php");
require_once($lib_ref . 'fwd0.php');
require_once($lib_ref . 'fwd1.php');
require_once($lib_ref . 'fwd2.php');
require_once($lib_ref . 'fwd3.php');

require('import_functions.php');

// criar conex�es com o banco
echo "conectando ao banco... ";
$source = new FWDDB(DB_POSTGRES, $nameDataSource, $dbUser, $dbPass, $dbHost);
$source->connect();
$target = new FWDDB(DB_POSTGRES, $nameTarget, $dbUser, $dbPass, $dbHost);
$target->connect();
echo "OK\n<br/>";

/**
 * PROCEDIMENTO:
 * 1 - configurar tudo - DONE
 * 2 - o vamos precisar de duas c�pias do banco - uma para ser moldada pelo sql, outra para ser fonte de dados
 * 3 - executar o sql principal
 * 4 - corrigir e adaptar o que for preciso de dados
 * 5 - criar types, views, functions e restraints
 */
// 3
echo "executando SQL principal...";
flush();
$target->execute(utf8_decode(file_get_contents('main_import.sql')), 0, 0, true);
echo "OK\n<br/>";

//4
echo "importando profiles...\n<br/>";
importProfiles($source, $target); // troca os profiles dos usu�rios para os padr�es
echo "executando ACL...\n<br/>";
importACL($target); //isms_profile_acl -> drop, restaura a partir do arquivo padr�o.
updateUsers($target); // preenche campos default
echo "setando campos default...\n<br/>";
$target->execute('update pm_doc_approvers set bcontextapprover = 0, bmanual = 0;'); //preenche campos default
$target->execute('update rm_control_efficiency_history set bincident = 0;'); //preenche campos default
$target->execute('update rm_parameter_name set nweight = 1;'); //preenche campos default
echo "importando controles de eficiencia...\n<br/>";
importControlEfficiency($source,$target);
echo "setando configura��es...\n<br/>";
setConfig($target, $userMail, $instanceConfigPath);

// isms_saas

// 5
echo "criando views...\n<br/>";
$target->execute(file_get_contents('isms_postgres_views_create.sql'), 0, 0, true);
echo "criando tipos...\n<br/>";
$target->execute(file_get_contents('isms_postgres_types_create.sql'), 0, 0, true);
echo "criando fun��es...\n<br/>";
$target->execute(file_get_contents('isms_postgres_functions_create.sql'), 0, 0, true);
echo "criando triggers...\n<br/>";
$target->execute(file_get_contents('isms_postgres_triggers_create.sql'), 0, 0, true);
// $target->execute(file_get_contents($sqlsDir.'isms_postgres_constraints_create.sql'), 0, 0, true); // creio que j� criaram-se as constraints
echo "ajustando sequ�ncias...\n<br/>";
$target->execute(file_get_contents('adjust_sequences.sql'), 0, 0, true);
echo "desfazendo loops em rm_asset_asset...\n<br/>";
$count = desfazNosAssetAsset($target);
echo "removidos $count loops\n<br/>";
echo "DONE."
?>