DROP TABLE "public"."wkf_doc_revision";

DROP TABLE "public"."view_rm_standard_active";

DROP TABLE "public"."view_rm_sec_bp_active";

DROP TABLE "public"."view_rm_risk_control_active";

DROP TABLE "public"."view_rm_risk_active";

DROP TABLE "public"."view_rm_process_asset_active";

DROP TABLE "public"."view_rm_process_active";

DROP TABLE "public"."view_rm_event_active";

DROP TABLE "public"."view_rm_control_bp_active";

DROP TABLE "public"."view_rm_control_active";

DROP TABLE "public"."view_rm_category_active";

DROP TABLE "public"."view_rm_bp_standard_active";

DROP TABLE "public"."view_rm_bp_event_active";

DROP TABLE "public"."view_rm_best_practice_active";

DROP TABLE "public"."view_rm_asset_asset_active";

DROP TABLE "public"."view_rm_asset_active";

DROP TABLE "public"."view_rm_area_active";

DROP TABLE "public"."view_pm_tp_active_with_content";

DROP TABLE "public"."view_pm_template_bp_active";

DROP TABLE "public"."view_pm_template_active";

DROP TABLE "public"."view_pm_register_active";

DROP TABLE "public"."view_pm_published_docs";

DROP TABLE "public"."view_pm_process_user_active";

DROP TABLE "public"."view_pm_document_active";

DROP TABLE "public"."view_pm_doc_registers_active";

DROP TABLE "public"."view_pm_doc_readers_active";

DROP TABLE "public"."view_pm_doc_instance_active";

DROP TABLE "public"."view_pm_doc_context_active";

DROP TABLE "public"."view_pm_doc_approvers_active";

DROP TABLE "public"."view_pm_di_with_content";

DROP TABLE "public"."view_pm_di_active_with_content";

DROP TABLE "public"."view_isms_user_active";

DROP TABLE "public"."view_isms_scope_active";

DROP TABLE "public"."view_isms_profile_active";

DROP TABLE "public"."view_isms_policy_active";

DROP TABLE "public"."view_isms_context_hist_active";

DROP TABLE "public"."view_isms_context_active";

DROP TABLE "public"."isms_acl";

DROP TABLE "public"."context_names";

DROP TABLE "public"."context_history";

ALTER TABLE "public"."wkf_control_test"
  DROP COLUMN "bflagalert";

ALTER TABLE "public"."wkf_control_test"
  DROP COLUMN "nperiod";

ALTER TABLE "public"."wkf_control_test"
  DROP COLUMN "nvalue";

ALTER TABLE "public"."wkf_control_test"
  DROP COLUMN "ddatelimit";

ALTER TABLE "public"."wkf_control_test"
  DROP COLUMN "ddatenext";

ALTER TABLE "public"."wkf_control_efficiency"
  DROP COLUMN "bflagalert";

ALTER TABLE "public"."wkf_control_efficiency"
  DROP COLUMN "nperiod";

ALTER TABLE "public"."wkf_control_efficiency"
  DROP COLUMN "nvalue";

ALTER TABLE "public"."wkf_control_efficiency"
  DROP COLUMN "ddatelimit";

ALTER TABLE "public"."wkf_control_efficiency"
  DROP COLUMN "ddatenext";

ALTER TABLE "public"."wkf_control_efficiency"
  DROP COLUMN "smetric5";

ALTER TABLE "public"."wkf_control_efficiency"
  DROP COLUMN "smetric4";

ALTER TABLE "public"."wkf_control_efficiency"
  DROP COLUMN "smetric3";

ALTER TABLE "public"."wkf_control_efficiency"
  DROP COLUMN "smetric2";

ALTER TABLE "public"."wkf_control_efficiency"
  DROP COLUMN "smetric1";

ALTER TABLE "public"."isms_profile_acl"
  DROP COLUMN "fkacl";

CREATE SEQUENCE "public"."isms_context_history_pkid_seq"
    INCREMENT 1  MINVALUE 1
    MAXVALUE 9223372036854775807  START 1106
    CACHE 1;

CREATE SEQUENCE "public"."isms_context_classification_pkclassification_seq"
    INCREMENT 1  MINVALUE 1
    MAXVALUE 9223372036854775807  START 200
    CACHE 1;

CREATE SEQUENCE "public"."ci_nc_nseqnumber_seq"
    INCREMENT 1  MINVALUE 1
    MAXVALUE 9223372036854775807  START 1
    CACHE 1;

CREATE SEQUENCE "public"."rm_parameter_name_pkparametername_seq"
    INCREMENT 1  MINVALUE 1
    MAXVALUE 9223372036854775807  START 10
    CACHE 1;

CREATE SEQUENCE "public"."isms_saas_pkconfig_seq"
    INCREMENT 1  MINVALUE 1
    MAXVALUE 9223372036854775807  START 1
    CACHE 1;

CREATE SEQUENCE "public"."isms_context_pkcontext_seq"
    INCREMENT 1  MINVALUE 1
    MAXVALUE 9223372036854775807  START 15274
    CACHE 1;

CREATE SEQUENCE "public"."rm_parameter_value_name_pkvaluename_seq"
    INCREMENT 1  MINVALUE 1
    MAXVALUE 9223372036854775807  START 10
    CACHE 1;

CREATE SEQUENCE "public"."wkf_alert_pkalert_seq"
    INCREMENT 1  MINVALUE 1
    MAXVALUE 9223372036854775807  START 6
    CACHE 1;

CREATE SEQUENCE "public"."wkf_task_pktask_seq"
    INCREMENT 1  MINVALUE 1
    MAXVALUE 9223372036854775807  START 1
    CACHE 1;

CREATE SEQUENCE "public"."pm_instance_comment_pkcomment_seq"
    INCREMENT 1  MINVALUE 1
    MAXVALUE 9223372036854775807  START 1
    CACHE 1;

CREATE SEQUENCE "public"."pm_doc_reference_pkreference_seq"
    INCREMENT 1  MINVALUE 1
    MAXVALUE 9223372036854775807  START 1
    CACHE 1;

CREATE SEQUENCE "public"."wkf_schedule_pkschedule_seq"
    INCREMENT 1  MINVALUE 1
    MAXVALUE 9223372036854775807  START 3
    CACHE 1;

CREATE SEQUENCE "public"."rm_rc_parameter_value_name_pkrcvaluename_seq"
    INCREMENT 1  MINVALUE 1
    MAXVALUE 9223372036854775807  START 10
    CACHE 1;

ALTER TABLE "public"."isms_context_classification"
  ALTER COLUMN "pkclassification" SET DEFAULT nextval('isms_context_classification_pkclassification_seq'::regclass);

ALTER TABLE "public"."isms_context_classification"
  ALTER COLUMN "pkclassification" TYPE INTEGER;

ALTER TABLE "public"."isms_context_history"
  ALTER COLUMN "pkid" SET DEFAULT nextval('isms_context_history_pkid_seq'::regclass);

ALTER TABLE "public"."isms_context_history"
  ALTER COLUMN "pkid" TYPE INTEGER;

ALTER TABLE "public"."isms_context"
  ALTER COLUMN "pkcontext" SET DEFAULT nextval('isms_context_pkcontext_seq'::regclass);

ALTER TABLE "public"."isms_context"
  ALTER COLUMN "pkcontext" TYPE INTEGER;

ALTER TABLE "public"."pm_doc_reference"
  ALTER COLUMN "pkreference" SET DEFAULT nextval('pm_doc_reference_pkreference_seq'::regclass);

ALTER TABLE "public"."pm_doc_reference"
  ALTER COLUMN "pkreference" TYPE INTEGER;

ALTER TABLE "public"."pm_instance_comment"
  ALTER COLUMN "pkcomment" SET DEFAULT nextval('pm_instance_comment_pkcomment_seq'::regclass);

ALTER TABLE "public"."pm_instance_comment"
  ALTER COLUMN "pkcomment" TYPE INTEGER;

ALTER TABLE "public"."rm_parameter_name"
  ALTER COLUMN "pkparametername" SET DEFAULT nextval('rm_parameter_name_pkparametername_seq'::regclass);

ALTER TABLE "public"."rm_parameter_name"
  ALTER COLUMN "pkparametername" TYPE INTEGER;

ALTER TABLE "public"."rm_parameter_value_name"
  ALTER COLUMN "pkvaluename" SET DEFAULT nextval('rm_parameter_value_name_pkvaluename_seq'::regclass);

ALTER TABLE "public"."rm_parameter_value_name"
  ALTER COLUMN "pkvaluename" TYPE INTEGER;

ALTER TABLE "public"."rm_rc_parameter_value_name"
  ALTER COLUMN "pkrcvaluename" SET DEFAULT nextval('rm_rc_parameter_value_name_pkrcvaluename_seq'::regclass);

ALTER TABLE "public"."rm_rc_parameter_value_name"
  ALTER COLUMN "pkrcvaluename" TYPE INTEGER;

ALTER TABLE "public"."wkf_alert"
  ALTER COLUMN "pkalert" SET DEFAULT nextval('wkf_alert_pkalert_seq'::regclass);

ALTER TABLE "public"."wkf_alert"
  ALTER COLUMN "pkalert" TYPE INTEGER;

ALTER TABLE "public"."wkf_task"
  ALTER COLUMN "pktask" SET DEFAULT nextval('wkf_task_pktask_seq'::regclass);

ALTER TABLE "public"."wkf_task"
  ALTER COLUMN "pktask" TYPE INTEGER;

ALTER TABLE "public"."isms_context"
  ADD CONSTRAINT "pk_isms_context" 
  PRIMARY KEY ("pkcontext");

ALTER TABLE "public"."isms_user"
  ALTER COLUMN "fkcontext" TYPE INTEGER;

ALTER TABLE "public"."isms_user"
  ADD CONSTRAINT "pk_isms_user" 
  PRIMARY KEY ("fkcontext");

CREATE TABLE "public"."ci_action_plan" (
  "fkcontext" INTEGER NOT NULL PRIMARY KEY, 
  "fkresponsible" INTEGER, 
  "sname" VARCHAR(256) NOT NULL, 
  "tactionplan" TEXT, 
  "nactiontype" INTEGER DEFAULT 0, 
  "ddatedeadline" TIMESTAMP WITHOUT TIME ZONE, 
  "ddateconclusion" TIMESTAMP WITHOUT TIME ZONE, 
  "bisefficient" INTEGER DEFAULT 0, 
  "ddateefficiencyrevision" TIMESTAMP WITHOUT TIME ZONE, 
  "ddateefficiencymeasured" TIMESTAMP WITHOUT TIME ZONE, 
  "ndaysbefore" INTEGER DEFAULT 0, 
  "bflagrevisionalert" INTEGER DEFAULT 0, 
  "bflagrevisionalertlate" INTEGER DEFAULT 0, 
  "sdocument" VARCHAR(256)
) WITH OIDS;

ALTER TABLE "public"."ci_action_plan"
  OWNER TO "isms";

CREATE TABLE "public"."ci_category" (
  "fkcontext" INTEGER NOT NULL PRIMARY KEY, 
  "sname" VARCHAR(256) NOT NULL
) WITH OIDS;

ALTER TABLE "public"."ci_category"
  OWNER TO "isms";

COMMENT ON TABLE "public"."ci_category"
IS 'Categoria de incidentes.';

CREATE TABLE "public"."ci_incident" (
  "fkcontext" INTEGER NOT NULL PRIMARY KEY, 
  "fkcategory" INTEGER, 
  "fkresponsible" INTEGER, 
  "sname" VARCHAR(256) NOT NULL, 
  "taccountsplan" TEXT, 
  "tevidences" TEXT, 
  "nlosstype" INTEGER DEFAULT 0, 
  "tevidencerequirementcomment" TEXT, 
  "ddatelimit" TIMESTAMP WITHOUT TIME ZONE, 
  "ddatefinish" TIMESTAMP WITHOUT TIME ZONE, 
  "tdisposaldescription" TEXT, 
  "tsolutiondescription" TEXT, 
  "tproductservice" TEXT, 
  "bnotemaildp" INTEGER DEFAULT 0, 
  "bemaildpsent" INTEGER DEFAULT 0, 
  "ddate" TIMESTAMP WITHOUT TIME ZONE NOT NULL
) WITH OIDS;

ALTER TABLE "public"."ci_incident"
  OWNER TO "isms";

COMMENT ON TABLE "public"."ci_incident"
IS 'Tabela de incidentes.';

ALTER TABLE "public"."rm_control"
  ALTER COLUMN "fkcontext" TYPE INTEGER;

ALTER TABLE "public"."rm_control"
  ADD CONSTRAINT "pk_rm_control" 
  PRIMARY KEY ("fkcontext");

CREATE TABLE "public"."ci_incident_control" (
  "fkincident" INTEGER NOT NULL, 
  "fkcontrol" INTEGER NOT NULL, 
  "fkcontext" INTEGER NOT NULL
) WITH OIDS;

ALTER TABLE "public"."ci_incident_control"
  OWNER TO "isms";

COMMENT ON TABLE "public"."ci_incident_control"
IS 'Tabela que relaciona incidentes e controles.';

ALTER TABLE "public"."isms_context_classification"
  ADD CONSTRAINT "pk_isms_context_classification" 
  PRIMARY KEY ("pkclassification");

CREATE TABLE "public"."ci_incident_financial_impact" (
  "fkincident" INTEGER NOT NULL, 
  "fkclassification" INTEGER NOT NULL, 
  "nvalue" DOUBLE PRECISION DEFAULT (0)::double precision
) WITH OIDS;

ALTER TABLE "public"."ci_incident_financial_impact"
  OWNER TO "isms";

COMMENT ON TABLE "public"."ci_incident_financial_impact"
IS 'Tabela de impacto financeiro de um incidente.';

ALTER TABLE "public"."rm_process"
  ALTER COLUMN "fkcontext" TYPE INTEGER;

ALTER TABLE "public"."rm_process"
  ADD CONSTRAINT "pk_rm_process" 
  PRIMARY KEY ("fkcontext");

CREATE TABLE "public"."ci_incident_process" (
  "fkprocess" INTEGER NOT NULL, 
  "fkincident" INTEGER NOT NULL, 
  "fkcontext" INTEGER NOT NULL
) WITH OIDS;

ALTER TABLE "public"."ci_incident_process"
  OWNER TO "isms";

COMMENT ON TABLE "public"."ci_incident_process"
IS 'Tabela que relaciona incidentes e processos.';

ALTER TABLE "public"."rm_risk"
  ALTER COLUMN "fkcontext" TYPE INTEGER;

ALTER TABLE "public"."rm_risk"
  ADD CONSTRAINT "pk_rm_risk" 
  PRIMARY KEY ("fkcontext");

CREATE TABLE "public"."ci_incident_risk" (
  "fkcontext" INTEGER NOT NULL PRIMARY KEY, 
  "fkrisk" INTEGER NOT NULL, 
  "fkincident" INTEGER NOT NULL
) WITH OIDS;

ALTER TABLE "public"."ci_incident_risk"
  OWNER TO "isms";

ALTER TABLE "public"."rm_parameter_name"
  ADD CONSTRAINT "pk_rm_parameter_name" 
  PRIMARY KEY ("pkparametername");

ALTER TABLE "public"."rm_parameter_value_name"
  ADD CONSTRAINT "pk_rm_parameter_value_name" 
  PRIMARY KEY ("pkvaluename");

CREATE TABLE "public"."ci_incident_risk_value" (
  "fkparametername" INTEGER NOT NULL, 
  "fkvaluename" INTEGER NOT NULL, 
  "fkincrisk" INTEGER NOT NULL
) WITH OIDS;

ALTER TABLE "public"."ci_incident_risk_value"
  OWNER TO "isms";

COMMENT ON TABLE "public"."ci_incident_risk_value"
IS 'Tabela que relaciona incidentes e riscos.';

CREATE TABLE "public"."ci_incident_user" (
  "fkuser" INTEGER NOT NULL, 
  "fkincident" INTEGER NOT NULL, 
  "tdescription" TEXT NOT NULL, 
  "tactiontaken" TEXT
) WITH OIDS;

ALTER TABLE "public"."ci_incident_user"
  OWNER TO "isms";

COMMENT ON TABLE "public"."ci_incident_user"
IS 'Tabela de relacionamento entre  incidentes e usu�rios.';

CREATE TABLE "public"."ci_nc" (
  "fkcontext" INTEGER NOT NULL PRIMARY KEY, 
  "fkcontrol" INTEGER, 
  "fkresponsible" INTEGER, 
  "fksender" INTEGER NOT NULL, 
  "sname" VARCHAR(256) NOT NULL, 
  "nseqnumber" INTEGER DEFAULT nextval('ci_nc_nseqnumber_seq'::regclass) NOT NULL, 
  "tdescription" TEXT, 
  "tcause" TEXT, 
  "tdenialjustification" TEXT, 
  "nclassification" INTEGER DEFAULT 0, 
  "ddatesent" TIMESTAMP WITHOUT TIME ZONE, 
  "ncapability" INTEGER DEFAULT 0
) WITH OIDS;

ALTER TABLE "public"."ci_nc"
  OWNER TO "isms";

CREATE TABLE "public"."ci_nc_action_plan" (
  "fknc" INTEGER NOT NULL, 
  "fkactionplan" INTEGER NOT NULL
) WITH OIDS;

ALTER TABLE "public"."ci_nc_action_plan"
  OWNER TO "isms";

COMMENT ON TABLE "public"."ci_nc_action_plan"
IS 'Relaciona n�o conformidades com planos de a��o.';

CREATE TABLE "public"."ci_nc_process" (
  "fknc" INTEGER NOT NULL, 
  "fkprocess" INTEGER NOT NULL
) WITH OIDS;

ALTER TABLE "public"."ci_nc_process"
  OWNER TO "isms";

COMMENT ON TABLE "public"."ci_nc_process"
IS 'Tabela que relaciona n�o conformidades com processos.';

CREATE TABLE "public"."ci_nc_seed" (
  "fkcontrol" INTEGER NOT NULL, 
  "ndeactivationreason" INTEGER DEFAULT 0 NOT NULL, 
  "ddatesent" TIMESTAMP WITHOUT TIME ZONE
) WITH OIDS;

ALTER TABLE "public"."ci_nc_seed"
  OWNER TO "isms";

CREATE TABLE "public"."ci_occurrence" (
  "fkcontext" INTEGER NOT NULL PRIMARY KEY, 
  "fkincident" INTEGER, 
  "tdescription" TEXT NOT NULL, 
  "tdenialjustification" TEXT, 
  "ddate" TIMESTAMP WITHOUT TIME ZONE NOT NULL
) WITH OIDS;

ALTER TABLE "public"."ci_occurrence"
  OWNER TO "isms";

COMMENT ON TABLE "public"."ci_occurrence"
IS 'Tabela de ocorr�ncias de incidentes.';

CREATE TABLE "public"."ci_risk_probability" (
  "fkrisk" INTEGER NOT NULL, 
  "fkvaluename" INTEGER NOT NULL, 
  "nincidentamount" INTEGER DEFAULT 0 NOT NULL
) WITH OIDS;

ALTER TABLE "public"."ci_risk_probability"
  OWNER TO "isms";

CREATE TABLE "public"."ci_risk_schedule" (
  "fkrisk" INTEGER NOT NULL PRIMARY KEY, 
  "nperiod" INTEGER DEFAULT 0 NOT NULL, 
  "nvalue" INTEGER DEFAULT 0 NOT NULL, 
  "ddatelastcheck" TIMESTAMP WITHOUT TIME ZONE
) WITH OIDS;

ALTER TABLE "public"."ci_risk_schedule"
  OWNER TO "isms";

CREATE TABLE "public"."ci_solution" (
  "fkcontext" INTEGER NOT NULL PRIMARY KEY, 
  "fkcategory" INTEGER, 
  "tproblem" TEXT, 
  "tsolution" TEXT, 
  "tkeywords" TEXT
) WITH OIDS;

ALTER TABLE "public"."ci_solution"
  OWNER TO "isms";

COMMENT ON TABLE "public"."ci_solution"
IS 'Tabela de solu��es para incidentes.';

ALTER TABLE "public"."isms_audit_log"
  OWNER TO "isms";

ALTER TABLE "public"."isms_audit_log"
  ALTER COLUMN "naction" SET DEFAULT 0;

ALTER TABLE "public"."isms_config"
  OWNER TO "isms";

CREATE UNIQUE INDEX "isms_config_pk" ON "public"."isms_config"
  USING btree ("pkconfig");

ALTER TABLE "public"."isms_config"
  ADD CONSTRAINT "pk_isms_config" 
  PRIMARY KEY ("pkconfig");

ALTER TABLE "public"."isms_context"
  OWNER TO "isms";

ALTER TABLE "public"."isms_context"
  ALTER COLUMN "ntype" SET DEFAULT 0;

ALTER TABLE "public"."isms_context"
  ALTER COLUMN "nstate" SET DEFAULT 0;

CREATE UNIQUE INDEX "isms_context_pk" ON "public"."isms_context"
  USING btree ("pkcontext");

ALTER TABLE "public"."isms_context_classification"
  OWNER TO "isms";

ALTER TABLE "public"."isms_context_classification"
  ALTER COLUMN "ncontexttype" SET DEFAULT 0;

ALTER TABLE "public"."isms_context_classification"
  ALTER COLUMN "nclassificationtype" SET DEFAULT 0;

CREATE UNIQUE INDEX "isms_context_classification_pk" ON "public"."isms_context_classification"
  USING btree ("pkclassification");

ALTER TABLE "public"."isms_context_date"
  OWNER TO "isms";

ALTER TABLE "public"."isms_context_date"
  ALTER COLUMN "fkcontext" TYPE INTEGER;

ALTER TABLE "public"."isms_context_date"
  ALTER COLUMN "naction" SET DEFAULT 0;

ALTER TABLE "public"."isms_context_date"
  ADD CONSTRAINT "fk_isms_con_fkcontext_isms_con" FOREIGN KEY ("fkcontext")
    REFERENCES "public"."isms_context"("pkcontext")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

CREATE INDEX "fkcontext_fk3" ON "public"."isms_context_date"
  USING btree ("fkcontext");

CREATE UNIQUE INDEX "isms_context_date_pk" ON "public"."isms_context_date"
  USING btree ("fkcontext", "naction");

ALTER TABLE "public"."isms_context_date"
  ADD CONSTRAINT "pk_isms_context_date" 
  PRIMARY KEY ("fkcontext", "naction");

ALTER TABLE "public"."isms_context_history"
  OWNER TO "isms";

ALTER TABLE "public"."isms_context_history"
  ALTER COLUMN "fkcontext" TYPE INTEGER;

ALTER TABLE "public"."isms_context_history"
  ALTER COLUMN "nvalue" SET DEFAULT (0)::double precision;

ALTER TABLE "public"."isms_context_history"
  ADD CONSTRAINT "fk_isms_con_fkcontext_isms_con2" FOREIGN KEY ("fkcontext")
    REFERENCES "public"."isms_context"("pkcontext")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

CREATE INDEX "fkcontext_fk5" ON "public"."isms_context_history"
  USING btree ("fkcontext");

CREATE UNIQUE INDEX "isms_context_history_pk" ON "public"."isms_context_history"
  USING btree ("pkid");

ALTER TABLE "public"."isms_context_history"
  ADD CONSTRAINT "pk_isms_context_history" 
  PRIMARY KEY ("pkid");

ALTER TABLE "public"."isms_policy"
  OWNER TO "isms";

ALTER TABLE "public"."isms_policy"
  ALTER COLUMN "fkcontext" TYPE INTEGER;

ALTER TABLE "public"."isms_policy"
  ADD CONSTRAINT "fk_isms_pol_fkcontext_isms_con" FOREIGN KEY ("fkcontext")
    REFERENCES "public"."isms_context"("pkcontext")
    MATCH FULL
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

CREATE UNIQUE INDEX "isms_policy_pk" ON "public"."isms_policy"
  USING btree ("fkcontext");

ALTER TABLE "public"."isms_policy"
  ADD CONSTRAINT "pk_isms_policy" 
  PRIMARY KEY ("fkcontext");

ALTER TABLE "public"."isms_profile"
  OWNER TO "isms";

ALTER TABLE "public"."isms_profile"
  ALTER COLUMN "fkcontext" TYPE INTEGER;

ALTER TABLE "public"."isms_profile"
  ADD COLUMN "nid" INTEGER;

ALTER TABLE "public"."isms_profile"
  ALTER COLUMN "nid" SET DEFAULT 0;

ALTER TABLE "public"."isms_profile"
  ADD CONSTRAINT "fk_isms_pro_fkcontext_isms_con" FOREIGN KEY ("fkcontext")
    REFERENCES "public"."isms_context"("pkcontext")
    MATCH FULL
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

CREATE UNIQUE INDEX "isms_profile_pk" ON "public"."isms_profile"
  USING btree ("fkcontext");

ALTER TABLE "public"."isms_profile"
  ADD CONSTRAINT "pk_isms_profile" 
  PRIMARY KEY ("fkcontext");

ALTER TABLE "public"."isms_profile_acl"
  OWNER TO "isms";

ALTER TABLE "public"."isms_profile_acl"
  ALTER COLUMN "fkprofile" TYPE INTEGER;

ALTER TABLE "public"."isms_profile_acl"
  ADD COLUMN "stag" VARCHAR(256);



CREATE TABLE "public"."isms_saas" (
  "pkconfig" INTEGER DEFAULT nextval('isms_saas_pkconfig_seq'::regclass) NOT NULL PRIMARY KEY, 
  "svalue" VARCHAR(256) NOT NULL
) WITH OIDS;

ALTER TABLE "public"."isms_scope"
  OWNER TO "isms";

ALTER TABLE "public"."isms_scope"
  ALTER COLUMN "fkcontext" TYPE INTEGER;

ALTER TABLE "public"."isms_scope"
  ADD CONSTRAINT "fk_isms_sco_fkcontext_isms_con" FOREIGN KEY ("fkcontext")
    REFERENCES "public"."isms_context"("pkcontext")
    MATCH FULL
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

CREATE UNIQUE INDEX "isms_scope_pk" ON "public"."isms_scope"
  USING btree ("fkcontext");

ALTER TABLE "public"."isms_scope"
  ADD CONSTRAINT "pk_isms_scope" 
  PRIMARY KEY ("fkcontext");

ALTER TABLE "public"."isms_solicitor"
  OWNER TO "isms";

ALTER TABLE "public"."isms_solicitor"
  ALTER COLUMN "fkuser" TYPE INTEGER;

ALTER TABLE "public"."isms_solicitor"
  ALTER COLUMN "fksolicitor" TYPE INTEGER;

ALTER TABLE "public"."isms_solicitor"
  ADD CONSTRAINT "fk_isms_sol_fksolicit_isms_use" FOREIGN KEY ("fksolicitor")
    REFERENCES "public"."isms_user"("fkcontext")
    MATCH FULL
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."isms_solicitor"
  ADD CONSTRAINT "fk_isms_sol_fkuser_isms_use" FOREIGN KEY ("fkuser")
    REFERENCES "public"."isms_user"("fkcontext")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

CREATE INDEX "fksolicitor_fk" ON "public"."isms_solicitor"
  USING btree ("fksolicitor");

CREATE INDEX "fkuser_fk2" ON "public"."isms_solicitor"
  USING btree ("fkuser");

CREATE UNIQUE INDEX "isms_solicitor_pk" ON "public"."isms_solicitor"
  USING btree ("fkuser", "fksolicitor");

ALTER TABLE "public"."isms_solicitor"
  ADD CONSTRAINT "pk_isms_solicitor" 
  PRIMARY KEY ("fkuser", "fksolicitor");

ALTER TABLE "public"."isms_user"
  OWNER TO "isms";

ALTER TABLE "public"."isms_user"
  ADD COLUMN "bisblocked" INTEGER;

ALTER TABLE "public"."isms_user"
  ALTER COLUMN "bisblocked" SET DEFAULT 0;

ALTER TABLE "public"."isms_user"
  ADD CONSTRAINT "ckc_bisblocked_isms_use" CHECK ((bisblocked IS NULL) OR ((bisblocked >= 0) AND (bisblocked <= 1)));

ALTER TABLE "public"."isms_user"
  ADD COLUMN "bmustchangepassword" INTEGER;

ALTER TABLE "public"."isms_user"
  ALTER COLUMN "bmustchangepassword" SET DEFAULT 0;

ALTER TABLE "public"."isms_user"
  ADD CONSTRAINT "ckc_bmustchangepasswo_isms_use" CHECK ((bmustchangepassword IS NULL) OR ((bmustchangepassword >= 0) AND (bmustchangepassword <= 1)));

ALTER TABLE "public"."isms_user"
  ALTER COLUMN "fkprofile" TYPE INTEGER;

ALTER TABLE "public"."isms_user"
  ALTER COLUMN "nip" SET DEFAULT 0;

ALTER TABLE "public"."isms_user"
  ALTER COLUMN "nlanguage" SET DEFAULT 0;

ALTER TABLE "public"."isms_user"
  ADD COLUMN "nwronglogonattempts" INTEGER;

ALTER TABLE "public"."isms_user"
  ALTER COLUMN "nwronglogonattempts" SET DEFAULT 0;

ALTER TABLE "public"."isms_user"
  ADD COLUMN "srequestpassword" VARCHAR(256);

ALTER TABLE "public"."isms_user"
  ADD COLUMN "ssession" VARCHAR(256);

ALTER TABLE "public"."isms_user"
  ADD COLUMN "bansweredsurvey" INTEGER;

ALTER TABLE "public"."isms_user"
  ALTER COLUMN "bansweredsurvey" SET DEFAULT 0;

ALTER TABLE "public"."isms_user"
  ADD COLUMN "bshowhelp" INTEGER;

ALTER TABLE "public"."isms_user"
  ALTER COLUMN "bshowhelp" SET DEFAULT 1;

ALTER TABLE "public"."isms_user"
  ADD CONSTRAINT "fk_isms_use_fkcontext_isms_con" FOREIGN KEY ("fkcontext")
    REFERENCES "public"."isms_context"("pkcontext")
    MATCH FULL
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."isms_user"
  ADD CONSTRAINT "fk_isms_use_fkprofile_isms_pro" FOREIGN KEY ("fkprofile")
    REFERENCES "public"."isms_profile"("fkcontext")
    MATCH FULL
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

CREATE INDEX "fkprofile_fk" ON "public"."isms_user"
  USING btree ("fkprofile");

CREATE UNIQUE INDEX "isms_user_pk" ON "public"."isms_user"
  USING btree ("fkcontext");

CREATE TABLE "public"."isms_user_password_history" (
  "fkuser" INTEGER, 
  "spassword" VARCHAR(256) NOT NULL, 
  "ddatepasswordchanged" TIMESTAMP WITHOUT TIME ZONE NOT NULL
) WITH OIDS;

ALTER TABLE "public"."isms_user_password_history"
  OWNER TO "isms";

ALTER TABLE "public"."isms_user_preference"
  OWNER TO "isms";

ALTER TABLE "public"."isms_user_preference"
  ALTER COLUMN "fkuser" TYPE INTEGER;

ALTER TABLE "public"."isms_user_preference"
  ADD CONSTRAINT "fk_isms_use_fkuser_isms_use" FOREIGN KEY ("fkuser")
    REFERENCES "public"."isms_user"("fkcontext")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

CREATE INDEX "fkuser_fk" ON "public"."isms_user_preference"
  USING btree ("fkuser");

CREATE UNIQUE INDEX "isms_user_preference_pk" ON "public"."isms_user_preference"
  USING btree ("fkuser", "spreference");

ALTER TABLE "public"."isms_user_preference"
  ADD CONSTRAINT "pk_isms_user_preference" 
  PRIMARY KEY ("fkuser", "spreference");

ALTER TABLE "public"."pm_document"
  ALTER COLUMN "fkcontext" TYPE INTEGER;

ALTER TABLE "public"."pm_document"
  ADD CONSTRAINT "pk_pm_document" 
  PRIMARY KEY ("fkcontext");

COMMENT ON TABLE "public"."pm_doc_approvers"
IS 'Tabela que relaciona os documentos com seus respectivos aprovadores.';

ALTER TABLE "public"."pm_doc_approvers"
  OWNER TO "isms";

ALTER TABLE "public"."pm_doc_approvers"
  ADD COLUMN "bcontextapprover" INTEGER;

ALTER TABLE "public"."pm_doc_approvers"
  ALTER COLUMN "bcontextapprover" SET DEFAULT 0;

ALTER TABLE "public"."pm_doc_approvers"
  ADD CONSTRAINT "ckc_bcontextapprover_pm_doc_a" CHECK ((bcontextapprover IS NULL) OR ((bcontextapprover >= 0) AND (bcontextapprover <= 1)));

COMMENT ON COLUMN "public"."pm_doc_approvers"."bhasapproved"
IS 'Indica se o usu�io aprovou o documento.';

ALTER TABLE "public"."pm_doc_approvers"
  ALTER COLUMN "bhasapproved" SET DEFAULT 0;

ALTER TABLE "public"."pm_doc_approvers"
  ADD CONSTRAINT "ckc_bhasapproved_pm_doc_a" CHECK ((bhasapproved IS NULL) OR ((bhasapproved >= 0) AND (bhasapproved <= 1)));

ALTER TABLE "public"."pm_doc_approvers"
  ADD COLUMN "bmanual" INTEGER;

ALTER TABLE "public"."pm_doc_approvers"
  ALTER COLUMN "bmanual" SET DEFAULT 0;

ALTER TABLE "public"."pm_doc_approvers"
  ADD CONSTRAINT "ckc_bmanual_pm_doc_a" CHECK ((bmanual IS NULL) OR ((bmanual >= 0) AND (bmanual <= 1)));

ALTER TABLE "public"."pm_doc_approvers"
  ALTER COLUMN "fkdocument" TYPE INTEGER;

ALTER TABLE "public"."pm_doc_approvers"
  ALTER COLUMN "fkuser" TYPE INTEGER;

ALTER TABLE "public"."pm_doc_approvers"
  ADD CONSTRAINT "fk_pm_doc_a_fkdocumen_pm_docum" FOREIGN KEY ("fkdocument")
    REFERENCES "public"."pm_document"("fkcontext")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."pm_doc_approvers"
  ADD CONSTRAINT "fk_pm_doc_a_fkuser_isms_use" FOREIGN KEY ("fkuser")
    REFERENCES "public"."isms_user"("fkcontext")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

CREATE INDEX "fkdocument_fk" ON "public"."pm_doc_approvers"
  USING btree ("fkdocument");

CREATE INDEX "fkuser_fk3" ON "public"."pm_doc_approvers"
  USING btree ("fkuser");

ALTER TABLE "public"."pm_doc_approvers"
  ADD CONSTRAINT "pk_pm_doc_approvers" 
  PRIMARY KEY ("fkdocument", "fkuser");

ALTER TABLE "public"."pm_doc_context"
  OWNER TO "isms";

ALTER TABLE "public"."pm_doc_context"
  ALTER COLUMN "fkcontext" TYPE INTEGER;

ALTER TABLE "public"."pm_doc_context"
  ALTER COLUMN "fkdocument" TYPE INTEGER;

ALTER TABLE "public"."pm_doc_context"
  ADD CONSTRAINT "fk_pm_doc_c_fkcontext_isms_con" FOREIGN KEY ("fkcontext")
    REFERENCES "public"."isms_context"("pkcontext")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."pm_doc_context"
  ADD CONSTRAINT "fk_pm_doc_c_fkdocumen_pm_docum" FOREIGN KEY ("fkdocument")
    REFERENCES "public"."pm_document"("fkcontext")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

CREATE INDEX "fkcontext_fk4" ON "public"."pm_doc_context"
  USING btree ("fkcontext");

CREATE INDEX "fkdocument_fk8" ON "public"."pm_doc_context"
  USING btree ("fkdocument");

ALTER TABLE "public"."pm_doc_context"
  ADD CONSTRAINT "pk_pm_doc_context" 
  PRIMARY KEY ("fkcontext", "fkdocument");

COMMENT ON TABLE "public"."pm_doc_instance"
IS 'Tabela com o hist�rico das diferentes vers�es de um documento.';

ALTER TABLE "public"."pm_doc_instance"
  OWNER TO "isms";

COMMENT ON COLUMN "public"."pm_doc_instance"."bislink"
IS 'Indica se o documento � um link ao inv�s de um documento f�sico.';

ALTER TABLE "public"."pm_doc_instance"
  ALTER COLUMN "bislink" SET DEFAULT 0;

ALTER TABLE "public"."pm_doc_instance"
  ADD CONSTRAINT "ckc_bislink_pm_doc_i" CHECK ((bislink IS NULL) OR ((bislink >= 0) AND (bislink <= 1)));

ALTER TABLE "public"."pm_doc_instance"
  ALTER COLUMN "fkcontext" TYPE INTEGER;

ALTER TABLE "public"."pm_doc_instance"
  ALTER COLUMN "fkdocument" TYPE INTEGER;

COMMENT ON COLUMN "public"."pm_doc_instance"."nmajorversion"
IS 'Vers�o da inst�ncia.';

ALTER TABLE "public"."pm_doc_instance"
  ALTER COLUMN "nmajorversion" SET DEFAULT 0;

ALTER TABLE "public"."pm_doc_instance"
  ALTER COLUMN "nrevisionversion" SET DEFAULT 0;

COMMENT ON COLUMN "public"."pm_doc_instance"."trevisionjustification"
IS 'Motivo pelo qual o documento entrou em revis�o.';

COMMENT ON COLUMN "public"."pm_doc_instance"."spath"
IS 'Path do arquivo no servidor de arquivos.';

COMMENT ON COLUMN "public"."pm_doc_instance"."tmodifycomment"
IS 'Coment�rio sobre o que foi modificado no documento.';

COMMENT ON COLUMN "public"."pm_doc_instance"."dbeginproduction"
IS 'Data em que o documento entrou em produ��o.';

COMMENT ON COLUMN "public"."pm_doc_instance"."dendproduction"
IS 'Data em que o documento saiu de produ��o.';

COMMENT ON COLUMN "public"."pm_doc_instance"."sfilename"
IS 'Nome do arquivo.';

COMMENT ON COLUMN "public"."pm_doc_instance"."slink"
IS 'Link para o documento.';

ALTER TABLE "public"."pm_doc_instance"
  ADD COLUMN "nfilesize" INTEGER;

ALTER TABLE "public"."pm_doc_instance"
  ALTER COLUMN "nfilesize" SET DEFAULT 0;

ALTER TABLE "public"."pm_doc_instance"
  ADD COLUMN "smanualversion" VARCHAR(256);

ALTER TABLE "public"."pm_doc_instance"
  ADD CONSTRAINT "fk_pm_doc_i_fkcontext_isms_con" FOREIGN KEY ("fkcontext")
    REFERENCES "public"."isms_context"("pkcontext")
    MATCH FULL
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."pm_doc_instance"
  ADD CONSTRAINT "fk_pm_doc_i_fkdocumen_pm_docum" FOREIGN KEY ("fkdocument")
    REFERENCES "public"."pm_document"("fkcontext")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

CREATE INDEX "fkdocument_fk2" ON "public"."pm_doc_instance"
  USING btree ("fkdocument");

ALTER TABLE "public"."pm_doc_instance"
  ADD CONSTRAINT "pk_pm_doc_instance" 
  PRIMARY KEY ("fkcontext");

CREATE UNIQUE INDEX "pm_doc_instance_pk" ON "public"."pm_doc_instance"
  USING btree ("fkcontext");

ALTER TABLE "public"."pm_doc_read_history"
  OWNER TO "isms";

ALTER TABLE "public"."pm_doc_read_history"
  ALTER COLUMN "fkinstance" TYPE INTEGER;

ALTER TABLE "public"."pm_doc_read_history"
  ALTER COLUMN "fkuser" TYPE INTEGER;

ALTER TABLE "public"."pm_doc_read_history"
  ADD CONSTRAINT "fk_pm_doc_r_fkinstanc_pm_doc_i" FOREIGN KEY ("fkinstance")
    REFERENCES "public"."pm_doc_instance"("fkcontext")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."pm_doc_read_history"
  ADD CONSTRAINT "fk_pm_doc_r_fkuser_isms_use2" FOREIGN KEY ("fkuser")
    REFERENCES "public"."isms_user"("fkcontext")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

CREATE INDEX "fkuser_fk7" ON "public"."pm_doc_read_history"
  USING btree ("fkuser");

ALTER TABLE "public"."pm_doc_read_history"
  ADD CONSTRAINT "pk_pm_doc_read_history" 
  PRIMARY KEY ("ddate", "fkinstance", "fkuser");

CREATE UNIQUE INDEX "pm_doc_read_history_pk" ON "public"."pm_doc_read_history"
  USING btree ("ddate", "fkinstance", "fkuser");

ALTER TABLE "public"."pm_doc_readers"
  OWNER TO "isms";

COMMENT ON COLUMN "public"."pm_doc_readers"."bdenied"
IS 'Campo para indicar que foi negado o acesso do usu�rio ao documento.';

ALTER TABLE "public"."pm_doc_readers"
  ALTER COLUMN "bdenied" SET DEFAULT 0;

ALTER TABLE "public"."pm_doc_readers"
  ADD CONSTRAINT "ckc_bdenied_pm_doc_r" CHECK ((bdenied IS NULL) OR ((bdenied >= 0) AND (bdenied <= 1)));

ALTER TABLE "public"."pm_doc_readers"
  ALTER COLUMN "bhasread" SET DEFAULT 0;

ALTER TABLE "public"."pm_doc_readers"
  ADD CONSTRAINT "ckc_bhasread_pm_doc_r" CHECK ((bhasread >= 0) AND (bhasread <= 1));

COMMENT ON COLUMN "public"."pm_doc_readers"."bmanual"
IS 'Campo para indicar se o usu�rio foi inserido manualmente, ou seja,
n�o foi inserido de forma autom�tica pelo sistema.';

ALTER TABLE "public"."pm_doc_readers"
  ALTER COLUMN "bmanual" SET DEFAULT 0;

ALTER TABLE "public"."pm_doc_readers"
  ADD CONSTRAINT "ckc_bmanual_pm_doc_r" CHECK ((bmanual IS NULL) OR ((bmanual >= 0) AND (bmanual <= 1)));

ALTER TABLE "public"."pm_doc_readers"
  ALTER COLUMN "fkdocument" TYPE INTEGER;

ALTER TABLE "public"."pm_doc_readers"
  ALTER COLUMN "fkuser" TYPE INTEGER;

ALTER TABLE "public"."pm_doc_readers"
  ADD CONSTRAINT "fk_pm_doc_r_fkdocumen_pm_docum4" FOREIGN KEY ("fkdocument")
    REFERENCES "public"."pm_document"("fkcontext")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."pm_doc_readers"
  ADD CONSTRAINT "fk_pm_doc_r_fkuser_isms_use" FOREIGN KEY ("fkuser")
    REFERENCES "public"."isms_user"("fkcontext")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

CREATE INDEX "fkdocument_fk4" ON "public"."pm_doc_readers"
  USING btree ("fkdocument");

CREATE INDEX "fkuser_fk5" ON "public"."pm_doc_readers"
  USING btree ("fkuser");

ALTER TABLE "public"."pm_doc_readers"
  ADD CONSTRAINT "pk_pm_doc_readers" 
  PRIMARY KEY ("fkdocument", "fkuser");

ALTER TABLE "public"."pm_doc_reference"
  OWNER TO "isms";

ALTER TABLE "public"."pm_doc_reference"
  ALTER COLUMN "fkdocument" TYPE INTEGER;

COMMENT ON COLUMN "public"."pm_doc_reference"."sname"
IS 'Nome da refer�ncia.';

COMMENT ON COLUMN "public"."pm_doc_reference"."slink"
IS 'Link para a refer�ncia.';

COMMENT ON COLUMN "public"."pm_doc_reference"."dcreationdate"
IS 'Data de inser��o.';

ALTER TABLE "public"."pm_doc_reference"
  ADD CONSTRAINT "fk_pm_doc_r_fkdocumen_pm_docum2" FOREIGN KEY ("fkdocument")
    REFERENCES "public"."pm_document"("fkcontext")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

CREATE INDEX "fkdocument_fk3" ON "public"."pm_doc_reference"
  USING btree ("fkdocument");

ALTER TABLE "public"."pm_doc_reference"
  ADD CONSTRAINT "pk_pm_doc_reference" 
  PRIMARY KEY ("pkreference");

CREATE UNIQUE INDEX "pm_doc_reference_pk" ON "public"."pm_doc_reference"
  USING btree ("pkreference");

ALTER TABLE "public"."pm_register"
  ALTER COLUMN "fkcontext" TYPE INTEGER;

ALTER TABLE "public"."pm_register"
  ADD CONSTRAINT "pk_pm_register" 
  PRIMARY KEY ("fkcontext");

ALTER TABLE "public"."pm_doc_registers"
  OWNER TO "isms";

ALTER TABLE "public"."pm_doc_registers"
  ALTER COLUMN "fkregister" TYPE INTEGER;

ALTER TABLE "public"."pm_doc_registers"
  ALTER COLUMN "fkdocument" TYPE INTEGER;

ALTER TABLE "public"."pm_doc_registers"
  ADD CONSTRAINT "fk_pm_doc_r_fkdocumen_pm_docum3" FOREIGN KEY ("fkdocument")
    REFERENCES "public"."pm_document"("fkcontext")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."pm_doc_registers"
  ADD CONSTRAINT "fk_pm_doc_r_fkregiste_pm_regis" FOREIGN KEY ("fkregister")
    REFERENCES "public"."pm_register"("fkcontext")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

CREATE INDEX "fkdocument_fk6" ON "public"."pm_doc_registers"
  USING btree ("fkdocument");

CREATE INDEX "fkregister_fk" ON "public"."pm_doc_registers"
  USING btree ("fkregister");

ALTER TABLE "public"."pm_doc_registers"
  ADD CONSTRAINT "pk_pm_doc_registers" 
  PRIMARY KEY ("fkregister", "fkdocument");

COMMENT ON TABLE "public"."pm_document"
IS 'Tabela de documentos.';

ALTER TABLE "public"."pm_document"
  OWNER TO "isms";

COMMENT ON COLUMN "public"."pm_document"."bflagdeadlinealert"
IS 'Indica se j� enviou alerta que est� chegando perto do prazo.';

ALTER TABLE "public"."pm_document"
  ALTER COLUMN "bflagdeadlinealert" SET DEFAULT 0;

ALTER TABLE "public"."pm_document"
  ADD CONSTRAINT "ckc_bflagdeadlinealer_pm_docum" CHECK ((bflagdeadlinealert IS NULL) OR ((bflagdeadlinealert >= 0) AND (bflagdeadlinealert <= 1)));

COMMENT ON COLUMN "public"."pm_document"."bflagdeadlineexpired"
IS 'Indica se j� enviou o alerta que o prazo encerrou.';

ALTER TABLE "public"."pm_document"
  ALTER COLUMN "bflagdeadlineexpired" SET DEFAULT 0;

ALTER TABLE "public"."pm_document"
  ADD CONSTRAINT "ckc_bflagdeadlineexpi_pm_docum" CHECK ((bflagdeadlineexpired IS NULL) OR ((bflagdeadlineexpired >= 0) AND (bflagdeadlineexpired <= 1)));

COMMENT ON COLUMN "public"."pm_document"."bhasapproved"
IS 'Indica se j� foi aprovado pelo aprovador principal.';

ALTER TABLE "public"."pm_document"
  ALTER COLUMN "bhasapproved" SET DEFAULT 0;

ALTER TABLE "public"."pm_document"
  ADD CONSTRAINT "ckc_bhasapproved_pm_docum" CHECK ((bhasapproved IS NULL) OR ((bhasapproved >= 0) AND (bhasapproved <= 1)));

ALTER TABLE "public"."pm_document"
  ADD COLUMN "fkschedule" INTEGER;

ALTER TABLE "public"."pm_document"
  ALTER COLUMN "fkclassification" TYPE INTEGER;

ALTER TABLE "public"."pm_document"
  ALTER COLUMN "fkcurrentversion" TYPE INTEGER;

ALTER TABLE "public"."pm_document"
  ALTER COLUMN "fkparent" TYPE INTEGER;

ALTER TABLE "public"."pm_document"
  ALTER COLUMN "fkauthor" TYPE INTEGER;

ALTER TABLE "public"."pm_document"
  ALTER COLUMN "fkmainapprover" TYPE INTEGER;

COMMENT ON COLUMN "public"."pm_document"."sname"
IS 'Nome do documento.';

COMMENT ON COLUMN "public"."pm_document"."tdescription"
IS 'Descri��o do documento.';

COMMENT ON COLUMN "public"."pm_document"."ntype"
IS 'Tipo do documento (template, registro, ...).';

ALTER TABLE "public"."pm_document"
  ALTER COLUMN "ntype" SET DEFAULT 0;

COMMENT ON COLUMN "public"."pm_document"."skeywords"
IS 'Palavras para fazer uma busca r�pida por documentos.';

COMMENT ON COLUMN "public"."pm_document"."ddateproduction"
IS 'Data em que o documento foi para produ��o.';

COMMENT ON COLUMN "public"."pm_document"."ddeadline"
IS 'Prazo para que o documento v� para produ��o.';

COMMENT ON COLUMN "public"."pm_document"."ndaysbefore"
IS 'Quantos dias antes deve ser enviado alerta que proximadade do prazo final.';

ALTER TABLE "public"."pm_document"
  ALTER COLUMN "ndaysbefore" SET DEFAULT 0;

ALTER TABLE "public"."pm_document"
  ADD CONSTRAINT "fk_pm_docum_fkauthor_isms_use" FOREIGN KEY ("fkauthor")
    REFERENCES "public"."isms_user"("fkcontext")
    MATCH FULL
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."pm_document"
  ADD CONSTRAINT "fk_pm_docum_fkclassif_isms_con" FOREIGN KEY ("fkclassification")
    REFERENCES "public"."isms_context_classification"("pkclassification")
    MATCH FULL
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."pm_document"
  ADD CONSTRAINT "fk_pm_docum_fkcontext_isms_con" FOREIGN KEY ("fkcontext")
    REFERENCES "public"."isms_context"("pkcontext")
    MATCH FULL
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."pm_document"
  ADD CONSTRAINT "fk_pm_docum_fkcurrent_pm_doc_i" FOREIGN KEY ("fkcurrentversion")
    REFERENCES "public"."pm_doc_instance"("fkcontext")
    MATCH FULL
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."pm_document"
  ADD CONSTRAINT "fk_pm_docum_fkmainapp_isms_use" FOREIGN KEY ("fkmainapprover")
    REFERENCES "public"."isms_user"("fkcontext")
    MATCH FULL
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."pm_document"
  ADD CONSTRAINT "fk_pm_docum_fkparent_pm_docum" FOREIGN KEY ("fkparent")
    REFERENCES "public"."pm_document"("fkcontext")
    MATCH FULL
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE;


CREATE TABLE "public"."wkf_schedule" (
  "pkschedule" INTEGER DEFAULT nextval('wkf_schedule_pkschedule_seq'::regclass) NOT NULL PRIMARY KEY, 
  "dstart" TIMESTAMP WITHOUT TIME ZONE NOT NULL, 
  "dend" TIMESTAMP WITHOUT TIME ZONE, 
  "ntype" INTEGER DEFAULT 0 NOT NULL, 
  "nperiodicity" INTEGER DEFAULT 0, 
  "nbitmap" INTEGER DEFAULT 0, 
  "nweek" INTEGER DEFAULT 0, 
  "nday" INTEGER DEFAULT 0, 
  "dnextoccurrence" TIMESTAMP WITHOUT TIME ZONE, 
  "ndaystofinish" INTEGER DEFAULT 0, 
  "ddatelimit" TIMESTAMP WITHOUT TIME ZONE
) WITH OIDS;

ALTER TABLE "public"."pm_document"
  ADD CONSTRAINT "fk_pm_docum_fkschedul_wkf_sche" FOREIGN KEY ("fkschedule")
    REFERENCES "public"."wkf_schedule"("pkschedule")
    MATCH FULL
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

CREATE INDEX "fkauthor_fk" ON "public"."pm_document"
  USING btree ("fkauthor");

CREATE INDEX "fkclassification_fk" ON "public"."pm_document"
  USING btree ("fkclassification");

CREATE INDEX "fkcurrentversion_fk" ON "public"."pm_document"
  USING btree ("fkcurrentversion");

CREATE INDEX "fkmainappover_fk" ON "public"."pm_document"
  USING btree ("fkmainapprover");

CREATE INDEX "fkparent_fk5" ON "public"."pm_document"
  USING btree ("fkparent");

CREATE INDEX "fkschedule_fk4" ON "public"."pm_document"
  USING btree ((fkschedule));

CREATE UNIQUE INDEX "pm_document_pk" ON "public"."pm_document"
  USING btree ("fkcontext");

COMMENT ON TABLE "public"."pm_document_revision_history"
IS 'Armazena o hist�rico de revis�o do documento.';

ALTER TABLE "public"."pm_document_revision_history"
  OWNER TO "isms";

ALTER TABLE "public"."pm_document_revision_history"
  ALTER COLUMN "bnewversion" SET DEFAULT 0;

ALTER TABLE "public"."pm_document_revision_history"
  ADD CONSTRAINT "ckc_bnewversion_pm_docum" CHECK ((bnewversion IS NULL) OR ((bnewversion >= 0) AND (bnewversion <= 1)));

ALTER TABLE "public"."pm_document_revision_history"
  ALTER COLUMN "fkdocument" TYPE INTEGER;

COMMENT ON COLUMN "public"."pm_document_revision_history"."ddaterevision"
IS 'Data em que ocorreu a revis�o.';

COMMENT ON COLUMN "public"."pm_document_revision_history"."sjustification"
IS 'Justificativa para a revis�o.';

ALTER TABLE "public"."pm_document_revision_history"
  ADD CONSTRAINT "fk_pm_docum_fkdocumen_pm_docum" FOREIGN KEY ("fkdocument")
    REFERENCES "public"."pm_document"("fkcontext")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

CREATE INDEX "fkdocument_fk5" ON "public"."pm_document_revision_history"
  USING btree ("fkdocument");

ALTER TABLE "public"."pm_document_revision_history"
  ADD CONSTRAINT "pk_pm_document_revision_histor" 
  PRIMARY KEY ("fkdocument", "ddaterevision");

CREATE UNIQUE INDEX "pm_document_revision_history_pk" ON "public"."pm_document_revision_history"
  USING btree ("fkdocument", "ddaterevision");

ALTER TABLE "public"."pm_instance_comment"
  OWNER TO "isms";

ALTER TABLE "public"."pm_instance_comment"
  ALTER COLUMN "fkinstance" TYPE INTEGER;

ALTER TABLE "public"."pm_instance_comment"
  ALTER COLUMN "fkuser" TYPE INTEGER;

COMMENT ON COLUMN "public"."pm_instance_comment"."ddate"
IS 'Data em que foi feito o coment�rio.';

ALTER TABLE "public"."pm_instance_comment"
  ADD CONSTRAINT "fk_pm_insta_fkinstanc_pm_doc_i" FOREIGN KEY ("fkinstance")
    REFERENCES "public"."pm_doc_instance"("fkcontext")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."pm_instance_comment"
  ADD CONSTRAINT "fk_pm_insta_fkuser_isms_use" FOREIGN KEY ("fkuser")
    REFERENCES "public"."isms_user"("fkcontext")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

CREATE INDEX "fkinstance_fk" ON "public"."pm_instance_comment"
  USING btree ("fkinstance");

CREATE INDEX "fkuser_fk4" ON "public"."pm_instance_comment"
  USING btree ("fkuser");

ALTER TABLE "public"."pm_instance_comment"
  ADD CONSTRAINT "pk_pm_instance_comment" 
  PRIMARY KEY ("pkcomment");

CREATE UNIQUE INDEX "pm_instance_comment_pk" ON "public"."pm_instance_comment"
  USING btree ("pkcomment");

ALTER TABLE "public"."pm_instance_content"
  OWNER TO "isms";

ALTER TABLE "public"."pm_instance_content"
  ALTER COLUMN "fkinstance" TYPE INTEGER;

COMMENT ON COLUMN "public"."pm_instance_content"."tcontent"
IS 'Conte�do do arquivo f�sico.';

ALTER TABLE "public"."pm_instance_content"
  ADD CONSTRAINT "fk_pm_insta_fkinstanc_pm_doc_i2" FOREIGN KEY ("fkinstance")
    REFERENCES "public"."pm_doc_instance"("fkcontext")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."pm_instance_content"
  ADD CONSTRAINT "pk_pm_instance_content" 
  PRIMARY KEY ("fkinstance");

CREATE UNIQUE INDEX "pm_instance_content_pk" ON "public"."pm_instance_content"
  USING btree ("fkinstance");

ALTER TABLE "public"."pm_process_user"
  OWNER TO "isms";

ALTER TABLE "public"."pm_process_user"
  ALTER COLUMN "fkprocess" TYPE INTEGER;

ALTER TABLE "public"."pm_process_user"
  ALTER COLUMN "fkuser" TYPE INTEGER;

ALTER TABLE "public"."pm_process_user"
  ADD CONSTRAINT "fk_pm_proce_fkprocess_rm_proce" FOREIGN KEY ("fkprocess")
    REFERENCES "public"."rm_process"("fkcontext")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."pm_process_user"
  ADD CONSTRAINT "fk_pm_proce_fkuser_isms_use" FOREIGN KEY ("fkuser")
    REFERENCES "public"."isms_user"("fkcontext")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

CREATE INDEX "fkprocess_fk2" ON "public"."pm_process_user"
  USING btree ("fkprocess");

CREATE INDEX "fkuser_fk6" ON "public"."pm_process_user"
  USING btree ("fkuser");

ALTER TABLE "public"."pm_process_user"
  ADD CONSTRAINT "pk_pm_process_user" 
  PRIMARY KEY ("fkprocess", "fkuser");

ALTER TABLE "public"."pm_register"
  OWNER TO "isms";

ALTER TABLE "public"."pm_register"
  ALTER COLUMN "fkclassification" TYPE INTEGER;

ALTER TABLE "public"."pm_register"
  ALTER COLUMN "fkdocument" TYPE INTEGER;

ALTER TABLE "public"."pm_register"
  ALTER COLUMN "fkresponsible" TYPE INTEGER;

COMMENT ON COLUMN "public"."pm_register"."sname"
IS 'Nome do registro.';

COMMENT ON COLUMN "public"."pm_register"."nperiod"
IS 'Ano, m�s, dia.';

ALTER TABLE "public"."pm_register"
  ALTER COLUMN "nperiod" SET DEFAULT 0;

COMMENT ON COLUMN "public"."pm_register"."nvalue"
IS 'Valor. Ex: 5 (nValue) meses (nPeriod).';

ALTER TABLE "public"."pm_register"
  ALTER COLUMN "nvalue" SET DEFAULT 0;

ALTER TABLE "public"."pm_register"
  ADD CONSTRAINT "fk_pm_regis_fkclassif_isms_con" FOREIGN KEY ("fkclassification")
    REFERENCES "public"."isms_context_classification"("pkclassification")
    MATCH FULL
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."pm_register"
  ADD CONSTRAINT "fk_pm_regis_fkcontext_isms_con" FOREIGN KEY ("fkcontext")
    REFERENCES "public"."isms_context"("pkcontext")
    MATCH FULL
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."pm_register"
  ADD CONSTRAINT "fk_pm_regis_fkdocumen_pm_docum" FOREIGN KEY ("fkdocument")
    REFERENCES "public"."pm_document"("fkcontext")
    MATCH FULL
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."pm_register"
  ADD CONSTRAINT "fk_pm_regis_fkrespons_isms_use" FOREIGN KEY ("fkresponsible")
    REFERENCES "public"."isms_user"("fkcontext")
    MATCH FULL
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

CREATE INDEX "fkclassification_fk2" ON "public"."pm_register"
  USING btree ("fkclassification");

CREATE INDEX "fkdocument_fk7" ON "public"."pm_register"
  USING btree ("fkdocument");

CREATE INDEX "fkresponsible_fk5" ON "public"."pm_register"
  USING btree ("fkresponsible");

CREATE UNIQUE INDEX "pm_register_pk" ON "public"."pm_register"
  USING btree ("fkcontext");

CREATE TABLE "public"."pm_register_readers" (
  "fkuser" INTEGER NOT NULL, 
  "fkregister" INTEGER NOT NULL, 
  "bmanual" INTEGER DEFAULT 0, 
  "bdenied" INTEGER DEFAULT 0
) WITH OIDS;

ALTER TABLE "public"."pm_register_readers"
  OWNER TO "isms";

ALTER TABLE "public"."pm_template"
  OWNER TO "isms";

ALTER TABLE "public"."pm_template"
  ALTER COLUMN "fkcontext" TYPE INTEGER;

ALTER TABLE "public"."pm_template"
  ALTER COLUMN "ncontexttype" SET DEFAULT 0;

ALTER TABLE "public"."pm_template"
  ADD COLUMN "nfilesize" INTEGER;

ALTER TABLE "public"."pm_template"
  ALTER COLUMN "nfilesize" SET DEFAULT 0;

ALTER TABLE "public"."pm_template"
  ADD CONSTRAINT "fk_pm_templ_fkcontext_isms_con" FOREIGN KEY ("fkcontext")
    REFERENCES "public"."isms_context"("pkcontext")
    MATCH FULL
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."pm_template"
  ADD CONSTRAINT "pk_pm_template" 
  PRIMARY KEY ("fkcontext");

CREATE UNIQUE INDEX "pm_template_pk" ON "public"."pm_template"
  USING btree ("fkcontext");

ALTER TABLE "public"."rm_best_practice"
  ALTER COLUMN "fkcontext" TYPE INTEGER;

ALTER TABLE "public"."rm_best_practice"
  ADD CONSTRAINT "pk_rm_best_practice" 
  PRIMARY KEY ("fkcontext");

ALTER TABLE "public"."pm_template_best_practice"
  OWNER TO "isms";

ALTER TABLE "public"."pm_template_best_practice"
  ALTER COLUMN "fktemplate" TYPE INTEGER;

ALTER TABLE "public"."pm_template_best_practice"
  ALTER COLUMN "fkbestpractice" TYPE INTEGER;

ALTER TABLE "public"."pm_template_best_practice"
  ADD CONSTRAINT "fk_pm_templ_fkbestpra_rm_best_" FOREIGN KEY ("fkbestpractice")
    REFERENCES "public"."rm_best_practice"("fkcontext")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."pm_template_best_practice"
  ADD CONSTRAINT "fk_pm_templ_fktemplat_pm_templ" FOREIGN KEY ("fktemplate")
    REFERENCES "public"."pm_template"("fkcontext")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

CREATE INDEX "fkbestpractice_fk4" ON "public"."pm_template_best_practice"
  USING btree ("fkbestpractice");

CREATE INDEX "fktemplate_fk" ON "public"."pm_template_best_practice"
  USING btree ("fktemplate");

ALTER TABLE "public"."pm_template_best_practice"
  ADD CONSTRAINT "pk_pm_template_best_practice" 
  PRIMARY KEY ("fktemplate", "fkbestpractice");

CREATE UNIQUE INDEX "pm_template_best_practice_pk" ON "public"."pm_template_best_practice"
  USING btree ("fktemplate", "fkbestpractice");

ALTER TABLE "public"."pm_template_content"
  OWNER TO "isms";

ALTER TABLE "public"."pm_template_content"
  ALTER COLUMN "fkcontext" TYPE INTEGER;

ALTER TABLE "public"."pm_template_content"
  ADD CONSTRAINT "fk_pm_templ_fkcontext_pm_templ" FOREIGN KEY ("fkcontext")
    REFERENCES "public"."pm_template"("fkcontext")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."pm_template_content"
  ADD CONSTRAINT "pk_pm_template_content" 
  PRIMARY KEY ("fkcontext");

CREATE UNIQUE INDEX "pm_template_content_pk" ON "public"."pm_template_content"
  USING btree ("fkcontext");

ALTER TABLE "public"."rm_area"
  OWNER TO "isms";

ALTER TABLE "public"."rm_area"
  ALTER COLUMN "fkcontext" TYPE INTEGER;

ALTER TABLE "public"."rm_area"
  ALTER COLUMN "fkpriority" TYPE INTEGER;

ALTER TABLE "public"."rm_area"
  ALTER COLUMN "fktype" TYPE INTEGER;

ALTER TABLE "public"."rm_area"
  ALTER COLUMN "fkparent" TYPE INTEGER;

ALTER TABLE "public"."rm_area"
  ALTER COLUMN "fkresponsible" TYPE INTEGER;

ALTER TABLE "public"."rm_area"
  ALTER COLUMN "nvalue" SET DEFAULT (0)::double precision;

ALTER TABLE "public"."rm_area"
  ADD CONSTRAINT "fk_rm_area_fkcontext_isms_con" FOREIGN KEY ("fkcontext")
    REFERENCES "public"."isms_context"("pkcontext")
    MATCH FULL
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."rm_area"
  ADD CONSTRAINT "pk_rm_area" 
  PRIMARY KEY ("fkcontext");

ALTER TABLE "public"."rm_area"
  ADD CONSTRAINT "fk_rm_area_fkparent_rm_area" FOREIGN KEY ("fkparent")
    REFERENCES "public"."rm_area"("fkcontext")
    MATCH FULL
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."rm_area"
  ADD CONSTRAINT "fk_rm_area_fkpriorit_isms_con" FOREIGN KEY ("fkpriority")
    REFERENCES "public"."isms_context_classification"("pkclassification")
    MATCH FULL
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."rm_area"
  ADD CONSTRAINT "fk_rm_area_fkrespons_isms_use" FOREIGN KEY ("fkresponsible")
    REFERENCES "public"."isms_user"("fkcontext")
    MATCH FULL
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."rm_area"
  ADD CONSTRAINT "fk_rm_area_fktype_isms_con" FOREIGN KEY ("fktype")
    REFERENCES "public"."isms_context_classification"("pkclassification")
    MATCH FULL
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

CREATE INDEX "fkparent_fk" ON "public"."rm_area"
  USING btree ("fkparent");

CREATE INDEX "fkpriority_fk" ON "public"."rm_area"
  USING btree ("fkpriority");

CREATE INDEX "fkresponsible_fk" ON "public"."rm_area"
  USING btree ("fkresponsible");

CREATE INDEX "fktype_fk" ON "public"."rm_area"
  USING btree ("fktype");

CREATE UNIQUE INDEX "rm_area_pk" ON "public"."rm_area"
  USING btree ("fkcontext");

ALTER TABLE "public"."rm_category"
  ALTER COLUMN "fkcontext" TYPE INTEGER;

ALTER TABLE "public"."rm_category"
  ADD CONSTRAINT "pk_rm_category" 
  PRIMARY KEY ("fkcontext");

ALTER TABLE "public"."rm_asset"
  OWNER TO "isms";

ALTER TABLE "public"."rm_asset"
  ALTER COLUMN "blegality" SET DEFAULT 0;

ALTER TABLE "public"."rm_asset"
  ADD CONSTRAINT "ckc_blegality_rm_asset" CHECK ((blegality IS NULL) OR ((blegality >= 0) AND (blegality <= 1)));

ALTER TABLE "public"."rm_asset"
  ALTER COLUMN "fkcontext" TYPE INTEGER;

ALTER TABLE "public"."rm_asset"
  ALTER COLUMN "fkcategory" TYPE INTEGER;

ALTER TABLE "public"."rm_asset"
  ALTER COLUMN "fkresponsible" TYPE INTEGER;

ALTER TABLE "public"."rm_asset"
  ALTER COLUMN "fksecurityresponsible" TYPE INTEGER;

ALTER TABLE "public"."rm_asset"
  ALTER COLUMN "ncost" SET DEFAULT (0)::double precision;

ALTER TABLE "public"."rm_asset"
  ALTER COLUMN "nvalue" SET DEFAULT (0)::double precision;

ALTER TABLE "public"."rm_asset"
  ADD CONSTRAINT "fk_rm_asset_fkcategor_rm_categ" FOREIGN KEY ("fkcategory")
    REFERENCES "public"."rm_category"("fkcontext")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."rm_asset"
  ADD CONSTRAINT "fk_rm_asset_fkcontext_isms_con" FOREIGN KEY ("fkcontext")
    REFERENCES "public"."isms_context"("pkcontext")
    MATCH FULL
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."rm_asset"
  ADD CONSTRAINT "fk_rm_asset_fkrespons_isms_use" FOREIGN KEY ("fkresponsible")
    REFERENCES "public"."isms_user"("fkcontext")
    MATCH FULL
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."rm_asset"
  ADD CONSTRAINT "fk_rm_asset_fksecurit_isms_use" FOREIGN KEY ("fksecurityresponsible")
    REFERENCES "public"."isms_user"("fkcontext")
    MATCH FULL
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

CREATE INDEX "fkcategory_fk" ON "public"."rm_asset"
  USING btree ("fkcategory");

CREATE INDEX "fkresponsible_fk4" ON "public"."rm_asset"
  USING btree ("fkresponsible");

CREATE INDEX "fksecurityresponsible_fk" ON "public"."rm_asset"
  USING btree ("fksecurityresponsible");

ALTER TABLE "public"."rm_asset"
  ADD CONSTRAINT "pk_rm_asset" 
  PRIMARY KEY ("fkcontext");

CREATE UNIQUE INDEX "rm_asset_pk" ON "public"."rm_asset"
  USING btree ("fkcontext");

ALTER TABLE "public"."rm_asset_asset"
  OWNER TO "isms";

ALTER TABLE "public"."rm_asset_asset"
  ALTER COLUMN "fkasset" TYPE INTEGER;

ALTER TABLE "public"."rm_asset_asset"
  ALTER COLUMN "fkdependent" TYPE INTEGER;

ALTER TABLE "public"."rm_asset_asset"
  ADD CONSTRAINT "fk_rm_asset_fkasset_rm_asset" FOREIGN KEY ("fkasset")
    REFERENCES "public"."rm_asset"("fkcontext")
    MATCH FULL
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."rm_asset_asset"
  ADD CONSTRAINT "fk_rm_asset_fkdepende_rm_asset" FOREIGN KEY ("fkdependent")
    REFERENCES "public"."rm_asset"("fkcontext")
    MATCH FULL
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

CREATE INDEX "fkasset_fk4" ON "public"."rm_asset_asset"
  USING btree ("fkasset");

CREATE INDEX "fkdependent_fk" ON "public"."rm_asset_asset"
  USING btree ("fkdependent");

ALTER TABLE "public"."rm_asset_asset"
  ADD CONSTRAINT "pk_rm_asset_asset" 
  PRIMARY KEY ("fkasset", "fkdependent");

CREATE UNIQUE INDEX "rm_asset_asset_pk" ON "public"."rm_asset_asset"
  USING btree ("fkasset", "fkdependent");

ALTER TABLE "public"."rm_asset_value"
  OWNER TO "isms";

ALTER TABLE "public"."rm_asset_value"
  ALTER COLUMN "fkasset" TYPE INTEGER;

ALTER TABLE "public"."rm_asset_value"
  ALTER COLUMN "fkvaluename" TYPE INTEGER;

ALTER TABLE "public"."rm_asset_value"
  ALTER COLUMN "fkparametername" TYPE INTEGER;

ALTER TABLE "public"."rm_asset_value"
  ADD CONSTRAINT "fk_rm_asset_fkasset_rm_asset2" FOREIGN KEY ("fkasset")
    REFERENCES "public"."rm_asset"("fkcontext")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."rm_asset_value"
  ADD CONSTRAINT "fk_rm_asset_fkparamet_rm_param" FOREIGN KEY ("fkparametername")
    REFERENCES "public"."rm_parameter_name"("pkparametername")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."rm_asset_value"
  ADD CONSTRAINT "fk_rm_asset_fkvaluena_rm_param" FOREIGN KEY ("fkvaluename")
    REFERENCES "public"."rm_parameter_value_name"("pkvaluename")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

CREATE INDEX "fkasset_fk3" ON "public"."rm_asset_value"
  USING btree ("fkasset");

CREATE INDEX "fkparametername_fk" ON "public"."rm_asset_value"
  USING btree ("fkparametername");

CREATE INDEX "fkvaluename_fk" ON "public"."rm_asset_value"
  USING btree ("fkvaluename");

ALTER TABLE "public"."rm_asset_value"
  ADD CONSTRAINT "pk_rm_asset_value" 
  PRIMARY KEY ("fkasset", "fkvaluename", "fkparametername");

CREATE UNIQUE INDEX "rm_asset_value_pk" ON "public"."rm_asset_value"
  USING btree ("fkasset", "fkvaluename", "fkparametername");

ALTER TABLE "public"."rm_section_best_practice"
  ALTER COLUMN "fkcontext" TYPE INTEGER;

ALTER TABLE "public"."rm_section_best_practice"
  ADD CONSTRAINT "pk_rm_section_best_practice" 
  PRIMARY KEY ("fkcontext");

ALTER TABLE "public"."rm_best_practice"
  OWNER TO "isms";

ALTER TABLE "public"."rm_best_practice"
  ALTER COLUMN "fksectionbestpractice" TYPE INTEGER;

ALTER TABLE "public"."rm_best_practice"
  ALTER COLUMN "ncontroltype" SET DEFAULT 0;

ALTER TABLE "public"."rm_best_practice"
  ADD CONSTRAINT "fk_rm_best__fkcontext_isms_con3" FOREIGN KEY ("fkcontext")
    REFERENCES "public"."isms_context"("pkcontext")
    MATCH FULL
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."rm_best_practice"
  ADD CONSTRAINT "fk_rm_best__fksection_rm_secti" FOREIGN KEY ("fksectionbestpractice")
    REFERENCES "public"."rm_section_best_practice"("fkcontext")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

CREATE INDEX "fksectionbestpractice_fk" ON "public"."rm_best_practice"
  USING btree ("fksectionbestpractice");

CREATE UNIQUE INDEX "rm_best_practice_pk" ON "public"."rm_best_practice"
  USING btree ("fkcontext");

ALTER TABLE "public"."rm_event"
  ALTER COLUMN "fkcontext" TYPE INTEGER;

ALTER TABLE "public"."rm_event"
  ADD CONSTRAINT "pk_rm_event" 
  PRIMARY KEY ("fkcontext");

ALTER TABLE "public"."rm_best_practice_event"
  OWNER TO "isms";

ALTER TABLE "public"."rm_best_practice_event"
  ALTER COLUMN "fkcontext" TYPE INTEGER;

ALTER TABLE "public"."rm_best_practice_event"
  ALTER COLUMN "fkbestpractice" TYPE INTEGER;

ALTER TABLE "public"."rm_best_practice_event"
  ALTER COLUMN "fkevent" TYPE INTEGER;

ALTER TABLE "public"."rm_best_practice_event"
  ADD CONSTRAINT "fk_rm_best__fkbestpra_rm_best_2" FOREIGN KEY ("fkbestpractice")
    REFERENCES "public"."rm_best_practice"("fkcontext")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."rm_best_practice_event"
  ADD CONSTRAINT "fk_rm_best__fkcontext_isms_con2" FOREIGN KEY ("fkcontext")
    REFERENCES "public"."isms_context"("pkcontext")
    MATCH FULL
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."rm_best_practice_event"
  ADD CONSTRAINT "fk_rm_best__fkevent_rm_event" FOREIGN KEY ("fkevent")
    REFERENCES "public"."rm_event"("fkcontext")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

CREATE INDEX "fkbestpractice_fk3" ON "public"."rm_best_practice_event"
  USING btree ("fkbestpractice");

CREATE INDEX "fkevent_fk2" ON "public"."rm_best_practice_event"
  USING btree ("fkevent");

ALTER TABLE "public"."rm_best_practice_event"
  ADD CONSTRAINT "pk_rm_best_practice_event" 
  PRIMARY KEY ("fkcontext");

CREATE UNIQUE INDEX "rm_best_practice_event_pk" ON "public"."rm_best_practice_event"
  USING btree ("fkcontext");

ALTER TABLE "public"."rm_standard"
  ALTER COLUMN "fkcontext" TYPE INTEGER;

ALTER TABLE "public"."rm_standard"
  ADD CONSTRAINT "pk_rm_standard" 
  PRIMARY KEY ("fkcontext");

ALTER TABLE "public"."rm_best_practice_standard"
  OWNER TO "isms";

ALTER TABLE "public"."rm_best_practice_standard"
  ALTER COLUMN "fkcontext" TYPE INTEGER;

ALTER TABLE "public"."rm_best_practice_standard"
  ALTER COLUMN "fkstandard" TYPE INTEGER;

ALTER TABLE "public"."rm_best_practice_standard"
  ALTER COLUMN "fkbestpractice" TYPE INTEGER;

ALTER TABLE "public"."rm_best_practice_standard"
  ADD CONSTRAINT "fk_rm_best__fkbestpra_rm_best_" FOREIGN KEY ("fkbestpractice")
    REFERENCES "public"."rm_best_practice"("fkcontext")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."rm_best_practice_standard"
  ADD CONSTRAINT "fk_rm_best__fkcontext_isms_con" FOREIGN KEY ("fkcontext")
    REFERENCES "public"."isms_context"("pkcontext")
    MATCH FULL
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."rm_best_practice_standard"
  ADD CONSTRAINT "fk_rm_best__fkstandar_rm_stand" FOREIGN KEY ("fkstandard")
    REFERENCES "public"."rm_standard"("fkcontext")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

CREATE INDEX "fkbestpractice_fk2" ON "public"."rm_best_practice_standard"
  USING btree ("fkbestpractice");

CREATE INDEX "fkstandard_fk" ON "public"."rm_best_practice_standard"
  USING btree ("fkstandard");

ALTER TABLE "public"."rm_best_practice_standard"
  ADD CONSTRAINT "pk_rm_best_practice_standard" 
  PRIMARY KEY ("fkcontext");

CREATE UNIQUE INDEX "rm_best_practice_standard_pk" ON "public"."rm_best_practice_standard"
  USING btree ("fkcontext");

ALTER TABLE "public"."rm_category"
  OWNER TO "isms";

ALTER TABLE "public"."rm_category"
  ALTER COLUMN "fkparent" TYPE INTEGER;

ALTER TABLE "public"."rm_category"
  ADD CONSTRAINT "fk_rm_categ_fkcontext_isms_con" FOREIGN KEY ("fkcontext")
    REFERENCES "public"."isms_context"("pkcontext")
    MATCH FULL
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."rm_category"
  ADD CONSTRAINT "fk_rm_categ_fkparent_rm_categ" FOREIGN KEY ("fkparent")
    REFERENCES "public"."rm_category"("fkcontext")
    MATCH FULL
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

CREATE INDEX "fkparent_fk2" ON "public"."rm_category"
  USING btree ("fkparent");

CREATE UNIQUE INDEX "rm_category_pk" ON "public"."rm_category"
  USING btree ("fkcontext");

ALTER TABLE "public"."rm_control"
  OWNER TO "isms";

ALTER TABLE "public"."rm_control"
  ADD COLUMN "befficiencynotok" INTEGER;

ALTER TABLE "public"."rm_control"
  ALTER COLUMN "befficiencynotok" SET DEFAULT 0;

ALTER TABLE "public"."rm_control"
  ADD CONSTRAINT "ckc_befficiencynotok_rm_contr" CHECK ((befficiencynotok IS NULL) OR ((befficiencynotok >= 0) AND (befficiencynotok <= 1)));

ALTER TABLE "public"."rm_control"
  ALTER COLUMN "bflagimplalert" SET DEFAULT 0;

ALTER TABLE "public"."rm_control"
  ADD CONSTRAINT "ckc_bflagimplalert_rm_contr" CHECK ((bflagimplalert IS NULL) OR ((bflagimplalert >= 0) AND (bflagimplalert <= 1)));

ALTER TABLE "public"."rm_control"
  ALTER COLUMN "bflagimplexpired" SET DEFAULT 0;

ALTER TABLE "public"."rm_control"
  ADD CONSTRAINT "ckc_bflagimplexpired_rm_contr" CHECK ((bflagimplexpired IS NULL) OR ((bflagimplexpired >= 0) AND (bflagimplexpired <= 1)));

ALTER TABLE "public"."rm_control"
  ADD COLUMN "bimplementationislate" INTEGER;

ALTER TABLE "public"."rm_control"
  ALTER COLUMN "bimplementationislate" SET DEFAULT 0;

ALTER TABLE "public"."rm_control"
  ADD CONSTRAINT "ckc_bimplementationis_rm_contr" CHECK ((bimplementationislate IS NULL) OR ((bimplementationislate >= 0) AND (bimplementationislate <= 1)));

ALTER TABLE "public"."rm_control"
  ALTER COLUMN "bisactive" SET DEFAULT 0;

ALTER TABLE "public"."rm_control"
  ADD CONSTRAINT "ckc_bisactive_rm_contr" CHECK ((bisactive IS NULL) OR ((bisactive >= 0) AND (bisactive <= 1)));

ALTER TABLE "public"."rm_control"
  ADD COLUMN "brevisionislate" INTEGER;

ALTER TABLE "public"."rm_control"
  ALTER COLUMN "brevisionislate" SET DEFAULT 0;

ALTER TABLE "public"."rm_control"
  ADD CONSTRAINT "ckc_brevisionislate_rm_contr" CHECK ((brevisionislate IS NULL) OR ((brevisionislate >= 0) AND (brevisionislate <= 1)));

ALTER TABLE "public"."rm_control"
  ADD COLUMN "btestislate" INTEGER;

ALTER TABLE "public"."rm_control"
  ALTER COLUMN "btestislate" SET DEFAULT 0;

ALTER TABLE "public"."rm_control"
  ADD CONSTRAINT "ckc_btestislate_rm_contr" CHECK ((btestislate IS NULL) OR ((btestislate >= 0) AND (btestislate <= 1)));

ALTER TABLE "public"."rm_control"
  ADD COLUMN "btestnotok" INTEGER;

ALTER TABLE "public"."rm_control"
  ALTER COLUMN "btestnotok" SET DEFAULT 0;

ALTER TABLE "public"."rm_control"
  ADD CONSTRAINT "ckc_btestnotok_rm_contr" CHECK ((btestnotok IS NULL) OR ((btestnotok >= 0) AND (btestnotok <= 1)));

ALTER TABLE "public"."rm_control"
  ALTER COLUMN "fktype" TYPE INTEGER;

ALTER TABLE "public"."rm_control"
  ALTER COLUMN "fkresponsible" TYPE INTEGER;

ALTER TABLE "public"."rm_control"
  ALTER COLUMN "ndaysbefore" SET DEFAULT 0;

ALTER TABLE "public"."rm_control"
  ALTER COLUMN "nimplementationstate" SET DEFAULT 0;

ALTER TABLE "public"."rm_control"
  ADD CONSTRAINT "fk_rm_contr_fkcontext_isms_con2" FOREIGN KEY ("fkcontext")
    REFERENCES "public"."isms_context"("pkcontext")
    MATCH FULL
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."rm_control"
  ADD CONSTRAINT "fk_rm_contr_fkrespons_isms_use" FOREIGN KEY ("fkresponsible")
    REFERENCES "public"."isms_user"("fkcontext")
    MATCH FULL
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."rm_control"
  ADD CONSTRAINT "fk_rm_contr_fktype_isms_con" FOREIGN KEY ("fktype")
    REFERENCES "public"."isms_context_classification"("pkclassification")
    MATCH FULL
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

CREATE INDEX "fkresponsible_fk3" ON "public"."rm_control"
  USING btree ("fkresponsible");

CREATE INDEX "fktype_fk3" ON "public"."rm_control"
  USING btree ("fktype");

CREATE UNIQUE INDEX "rm_control_pk" ON "public"."rm_control"
  USING btree ("fkcontext");

ALTER TABLE "public"."rm_control_best_practice"
  OWNER TO "isms";

ALTER TABLE "public"."rm_control_best_practice"
  ALTER COLUMN "fkcontext" TYPE INTEGER;

ALTER TABLE "public"."rm_control_best_practice"
  ALTER COLUMN "fkbestpractice" TYPE INTEGER;

ALTER TABLE "public"."rm_control_best_practice"
  ALTER COLUMN "fkcontrol" TYPE INTEGER;

ALTER TABLE "public"."rm_control_best_practice"
  ADD CONSTRAINT "fk_rm_contr_fkbestpra_rm_best_" FOREIGN KEY ("fkbestpractice")
    REFERENCES "public"."rm_best_practice"("fkcontext")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."rm_control_best_practice"
  ADD CONSTRAINT "fk_rm_contr_fkcontext_isms_con" FOREIGN KEY ("fkcontext")
    REFERENCES "public"."isms_context"("pkcontext")
    MATCH FULL
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."rm_control_best_practice"
  ADD CONSTRAINT "fk_rm_contr_fkcontrol_rm_contr" FOREIGN KEY ("fkcontrol")
    REFERENCES "public"."rm_control"("fkcontext")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

CREATE INDEX "fkbestpractice_fk" ON "public"."rm_control_best_practice"
  USING btree ("fkbestpractice");

CREATE INDEX "fkcontrol_fk2" ON "public"."rm_control_best_practice"
  USING btree ("fkcontrol");

ALTER TABLE "public"."rm_control_best_practice"
  ADD CONSTRAINT "pk_rm_control_best_practice" 
  PRIMARY KEY ("fkcontext");

CREATE UNIQUE INDEX "rm_control_best_practice_pk" ON "public"."rm_control_best_practice"
  USING btree ("fkcontext");

ALTER TABLE "public"."rm_control_cost"
  OWNER TO "isms";

ALTER TABLE "public"."rm_control_cost"
  ALTER COLUMN "fkcontrol" TYPE INTEGER;

ALTER TABLE "public"."rm_control_cost"
  ALTER COLUMN "ncost1" SET DEFAULT (0)::double precision;

ALTER TABLE "public"."rm_control_cost"
  ALTER COLUMN "ncost2" SET DEFAULT (0)::double precision;

ALTER TABLE "public"."rm_control_cost"
  ALTER COLUMN "ncost3" SET DEFAULT (0)::double precision;

ALTER TABLE "public"."rm_control_cost"
  ALTER COLUMN "ncost4" SET DEFAULT (0)::double precision;

ALTER TABLE "public"."rm_control_cost"
  ALTER COLUMN "ncost5" SET DEFAULT (0)::double precision;

ALTER TABLE "public"."rm_control_cost"
  ADD CONSTRAINT "fk_rm_contr_fkcontrol_rm_contr4" FOREIGN KEY ("fkcontrol")
    REFERENCES "public"."rm_control"("fkcontext")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."rm_control_cost"
  ADD CONSTRAINT "pk_rm_control_cost" 
  PRIMARY KEY ("fkcontrol");

CREATE UNIQUE INDEX "rm_control_cost_pk" ON "public"."rm_control_cost"
  USING btree ("fkcontrol");

ALTER TABLE "public"."rm_control_efficiency_history"
  OWNER TO "isms";

ALTER TABLE "public"."rm_control_efficiency_history"
  ADD COLUMN "bincident" INTEGER;

ALTER TABLE "public"."rm_control_efficiency_history"
  ALTER COLUMN "bincident" SET DEFAULT 0;

ALTER TABLE "public"."rm_control_efficiency_history"
  ADD CONSTRAINT "ckc_bincident_rm_contr" CHECK ((bincident IS NULL) OR ((bincident >= 0) AND (bincident <= 1)));

ALTER TABLE "public"."rm_control_efficiency_history"
  ALTER COLUMN "fkcontrol" TYPE INTEGER;

ALTER TABLE "public"."rm_control_efficiency_history"
  ALTER COLUMN "nrealefficiency" SET DEFAULT 0;

ALTER TABLE "public"."rm_control_efficiency_history"
  ALTER COLUMN "nexpectedefficiency" SET DEFAULT 0;

ALTER TABLE "public"."rm_control_efficiency_history"
  ADD CONSTRAINT "fk_rm_contr_fkcontrol_rm_contr2" FOREIGN KEY ("fkcontrol")
    REFERENCES "public"."rm_control"("fkcontext")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

CREATE INDEX "fkcontrol_fk3" ON "public"."rm_control_efficiency_history"
  USING btree ("fkcontrol");

ALTER TABLE "public"."rm_control_efficiency_history"
  ADD CONSTRAINT "pk_rm_control_efficiency_histo" 
  PRIMARY KEY ("fkcontrol", "ddatetodo");

CREATE UNIQUE INDEX "rm_control_efficiency_history_p" ON "public"."rm_control_efficiency_history"
  USING btree ("fkcontrol", "ddatetodo");

ALTER TABLE "public"."rm_control_test_history"
  OWNER TO "isms";

ALTER TABLE "public"."rm_control_test_history"
  ALTER COLUMN "btestedvalue" SET DEFAULT 0;

ALTER TABLE "public"."rm_control_test_history"
  ADD CONSTRAINT "ckc_btestedvalue_rm_contr" CHECK ((btestedvalue IS NULL) OR ((btestedvalue >= 0) AND (btestedvalue <= 1)));

ALTER TABLE "public"."rm_control_test_history"
  ALTER COLUMN "fkcontrol" TYPE INTEGER;

ALTER TABLE "public"."rm_control_test_history"
  ADD CONSTRAINT "fk_rm_contr_fkcontrol_rm_contr3" FOREIGN KEY ("fkcontrol")
    REFERENCES "public"."rm_control"("fkcontext")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

CREATE INDEX "fkcontrol_fk4" ON "public"."rm_control_test_history"
  USING btree ("fkcontrol");

ALTER TABLE "public"."rm_control_test_history"
  ADD CONSTRAINT "pk_rm_control_test_history" 
  PRIMARY KEY ("fkcontrol", "ddatetodo");

CREATE UNIQUE INDEX "rm_control_test_history_pk" ON "public"."rm_control_test_history"
  USING btree ("fkcontrol", "ddatetodo");

ALTER TABLE "public"."rm_event"
  OWNER TO "isms";

ALTER TABLE "public"."rm_event"
  ALTER COLUMN "bpropagate" SET DEFAULT 0;

ALTER TABLE "public"."rm_event"
  ADD CONSTRAINT "ckc_bpropagate_rm_event" CHECK ((bpropagate IS NULL) OR ((bpropagate >= 0) AND (bpropagate <= 1)));

ALTER TABLE "public"."rm_event"
  ALTER COLUMN "fktype" TYPE INTEGER;

ALTER TABLE "public"."rm_event"
  ALTER COLUMN "fkcategory" TYPE INTEGER;

ALTER TABLE "public"."rm_event"
  ADD COLUMN "timpact" TEXT;

ALTER TABLE "public"."rm_event"
  ADD CONSTRAINT "fk_rm_event_fkcategor_rm_categ" FOREIGN KEY ("fkcategory")
    REFERENCES "public"."rm_category"("fkcontext")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."rm_event"
  ADD CONSTRAINT "fk_rm_event_fkcontext_isms_con" FOREIGN KEY ("fkcontext")
    REFERENCES "public"."isms_context"("pkcontext")
    MATCH FULL
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."rm_event"
  ADD CONSTRAINT "fk_rm_event_fktype_isms_con" FOREIGN KEY ("fktype")
    REFERENCES "public"."isms_context_classification"("pkclassification")
    MATCH FULL
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

CREATE INDEX "fkcategory_fk2" ON "public"."rm_event"
  USING btree ("fkcategory");

CREATE INDEX "fktype_fk5" ON "public"."rm_event"
  USING btree ("fktype");

CREATE UNIQUE INDEX "rm_event_pk" ON "public"."rm_event"
  USING btree ("fkcontext");

ALTER TABLE "public"."rm_parameter_name"
  OWNER TO "isms";

ALTER TABLE "public"."rm_parameter_name"
  ADD COLUMN "nweight" INTEGER;

ALTER TABLE "public"."rm_parameter_name"
  ALTER COLUMN "nweight" SET DEFAULT 1;

CREATE UNIQUE INDEX "rm_parameter_name_pk" ON "public"."rm_parameter_name"
  USING btree ("pkparametername");

ALTER TABLE "public"."rm_parameter_value_name"
  OWNER TO "isms";

ALTER TABLE "public"."rm_parameter_value_name"
  ALTER COLUMN "nvalue" SET DEFAULT 1;

ALTER TABLE "public"."rm_parameter_value_name"
  ADD CONSTRAINT "ckc_nvalue_rm_param" CHECK ((nvalue IS NULL) OR ((nvalue >= 1) AND (nvalue <= 5)));

CREATE UNIQUE INDEX "rm_parameter_value_name_pk" ON "public"."rm_parameter_value_name"
  USING btree ("pkvaluename");

ALTER TABLE "public"."rm_process"
  OWNER TO "isms";

ALTER TABLE "public"."rm_process"
  ALTER COLUMN "fkpriority" TYPE INTEGER;

ALTER TABLE "public"."rm_process"
  ALTER COLUMN "fktype" TYPE INTEGER;

ALTER TABLE "public"."rm_process"
  ALTER COLUMN "fkarea" TYPE INTEGER;

ALTER TABLE "public"."rm_process"
  ALTER COLUMN "fkresponsible" TYPE INTEGER;

ALTER TABLE "public"."rm_process"
  ALTER COLUMN "nvalue" SET DEFAULT (0)::double precision;

ALTER TABLE "public"."rm_process"
  ADD CONSTRAINT "fk_rm_proce_fkarea_rm_area" FOREIGN KEY ("fkarea")
    REFERENCES "public"."rm_area"("fkcontext")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."rm_process"
  ADD CONSTRAINT "fk_rm_proce_fkcontext_isms_con2" FOREIGN KEY ("fkcontext")
    REFERENCES "public"."isms_context"("pkcontext")
    MATCH FULL
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."rm_process"
  ADD CONSTRAINT "fk_rm_proce_fkpriorit_isms_con" FOREIGN KEY ("fkpriority")
    REFERENCES "public"."isms_context_classification"("pkclassification")
    MATCH FULL
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."rm_process"
  ADD CONSTRAINT "fk_rm_proce_fkrespons_isms_use" FOREIGN KEY ("fkresponsible")
    REFERENCES "public"."isms_user"("fkcontext")
    MATCH FULL
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."rm_process"
  ADD CONSTRAINT "fk_rm_proce_fktype_isms_con" FOREIGN KEY ("fktype")
    REFERENCES "public"."isms_context_classification"("pkclassification")
    MATCH FULL
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

CREATE INDEX "fkarea_fk" ON "public"."rm_process"
  USING btree ("fkarea");

CREATE INDEX "fkpriority_fk2" ON "public"."rm_process"
  USING btree ("fkpriority");

CREATE INDEX "fkresponsible_fk2" ON "public"."rm_process"
  USING btree ("fkresponsible");

CREATE INDEX "fktype_fk2" ON "public"."rm_process"
  USING btree ("fktype");

CREATE UNIQUE INDEX "rm_process_pk" ON "public"."rm_process"
  USING btree ("fkcontext");

ALTER TABLE "public"."rm_process_asset"
  OWNER TO "isms";

ALTER TABLE "public"."rm_process_asset"
  ALTER COLUMN "fkcontext" TYPE INTEGER;

ALTER TABLE "public"."rm_process_asset"
  ALTER COLUMN "fkprocess" TYPE INTEGER;

ALTER TABLE "public"."rm_process_asset"
  ALTER COLUMN "fkasset" TYPE INTEGER;

ALTER TABLE "public"."rm_process_asset"
  ADD CONSTRAINT "fk_rm_proce_fkasset_rm_asset" FOREIGN KEY ("fkasset")
    REFERENCES "public"."rm_asset"("fkcontext")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."rm_process_asset"
  ADD CONSTRAINT "fk_rm_proce_fkcontext_isms_con" FOREIGN KEY ("fkcontext")
    REFERENCES "public"."isms_context"("pkcontext")
    MATCH FULL
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."rm_process_asset"
  ADD CONSTRAINT "fk_rm_proce_fkprocess_rm_proce" FOREIGN KEY ("fkprocess")
    REFERENCES "public"."rm_process"("fkcontext")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

CREATE INDEX "fkasset_fk" ON "public"."rm_process_asset"
  USING btree ("fkasset");

CREATE INDEX "fkprocess_fk" ON "public"."rm_process_asset"
  USING btree ("fkprocess");

ALTER TABLE "public"."rm_process_asset"
  ADD CONSTRAINT "pk_rm_process_asset" 
  PRIMARY KEY ("fkcontext");

CREATE UNIQUE INDEX "rm_process_asset_pk" ON "public"."rm_process_asset"
  USING btree ("fkcontext");

ALTER TABLE "public"."rm_rc_parameter_value_name"
  OWNER TO "isms";

ALTER TABLE "public"."rm_rc_parameter_value_name"
  ALTER COLUMN "nvalue" SET DEFAULT 0;

ALTER TABLE "public"."rm_rc_parameter_value_name"
  ADD CONSTRAINT "pk_rm_rc_parameter_value_name" 
  PRIMARY KEY ("pkrcvaluename");

CREATE UNIQUE INDEX "rm_rc_parameter_value_name_pk" ON "public"."rm_rc_parameter_value_name"
  USING btree ("pkrcvaluename");

ALTER TABLE "public"."rm_risk"
  OWNER TO "isms";

ALTER TABLE "public"."rm_risk"
  ALTER COLUMN "fkprobabilityvaluename" TYPE INTEGER;

ALTER TABLE "public"."rm_risk"
  ALTER COLUMN "fktype" TYPE INTEGER;

ALTER TABLE "public"."rm_risk"
  ALTER COLUMN "fkevent" TYPE INTEGER;

ALTER TABLE "public"."rm_risk"
  ALTER COLUMN "fkasset" TYPE INTEGER;

ALTER TABLE "public"."rm_risk"
  ALTER COLUMN "nvalue" SET DEFAULT (0)::double precision;

ALTER TABLE "public"."rm_risk"
  ALTER COLUMN "nvalueresidual" SET DEFAULT (0)::double precision;

ALTER TABLE "public"."rm_risk"
  ALTER COLUMN "nacceptmode" SET DEFAULT 0;

ALTER TABLE "public"."rm_risk"
  ALTER COLUMN "nacceptstate" SET DEFAULT 0;

ALTER TABLE "public"."rm_risk"
  ALTER COLUMN "ncost" SET DEFAULT (0)::double precision;

ALTER TABLE "public"."rm_risk"
  ADD COLUMN "timpact" TEXT;

ALTER TABLE "public"."rm_risk"
  ADD CONSTRAINT "fk_rm_risk_fkasset_rm_asset" FOREIGN KEY ("fkasset")
    REFERENCES "public"."rm_asset"("fkcontext")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."rm_risk"
  ADD CONSTRAINT "fk_rm_risk_fkcontext_isms_con" FOREIGN KEY ("fkcontext")
    REFERENCES "public"."isms_context"("pkcontext")
    MATCH FULL
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."rm_risk"
  ADD CONSTRAINT "fk_rm_risk_fkevent_rm_event" FOREIGN KEY ("fkevent")
    REFERENCES "public"."rm_event"("fkcontext")
    MATCH FULL
    ON DELETE SET NULL
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."rm_risk"
  ADD CONSTRAINT "fk_rm_risk_fkprobabi_rm_param" FOREIGN KEY ("fkprobabilityvaluename")
    REFERENCES "public"."rm_parameter_value_name"("pkvaluename")
    MATCH FULL
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."rm_risk"
  ADD CONSTRAINT "fk_rm_risk_fktype_isms_con" FOREIGN KEY ("fktype")
    REFERENCES "public"."isms_context_classification"("pkclassification")
    MATCH FULL
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

CREATE INDEX "fkasset_fk2" ON "public"."rm_risk"
  USING btree ("fkasset");

CREATE INDEX "fkevent_fk" ON "public"."rm_risk"
  USING btree ("fkevent");

CREATE INDEX "fkprobabilityvaluename_fk" ON "public"."rm_risk"
  USING btree ("fkprobabilityvaluename");

CREATE INDEX "fktype_fk4" ON "public"."rm_risk"
  USING btree ("fktype");

CREATE UNIQUE INDEX "rm_risk_pk" ON "public"."rm_risk"
  USING btree ("fkcontext");

ALTER TABLE "public"."rm_risk_control"
  OWNER TO "isms";

ALTER TABLE "public"."rm_risk_control"
  ALTER COLUMN "fkcontext" TYPE INTEGER;

ALTER TABLE "public"."rm_risk_control"
  ALTER COLUMN "fkprobabilityvaluename" TYPE INTEGER;

ALTER TABLE "public"."rm_risk_control"
  ALTER COLUMN "fkrisk" TYPE INTEGER;

ALTER TABLE "public"."rm_risk_control"
  ALTER COLUMN "fkcontrol" TYPE INTEGER;

ALTER TABLE "public"."rm_risk_control"
  ADD CONSTRAINT "fk_rm_risk__fkcontext_isms_con" FOREIGN KEY ("fkcontext")
    REFERENCES "public"."isms_context"("pkcontext")
    MATCH FULL
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."rm_risk_control"
  ADD CONSTRAINT "fk_rm_risk__fkcontrol_rm_contr" FOREIGN KEY ("fkcontrol")
    REFERENCES "public"."rm_control"("fkcontext")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."rm_risk_control"
  ADD CONSTRAINT "fk_rm_risk__fkprobabi_rm_rc_pa" FOREIGN KEY ("fkprobabilityvaluename")
    REFERENCES "public"."rm_rc_parameter_value_name"("pkrcvaluename")
    MATCH FULL
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."rm_risk_control"
  ADD CONSTRAINT "fk_rm_risk__fkrisk_rm_risk" FOREIGN KEY ("fkrisk")
    REFERENCES "public"."rm_risk"("fkcontext")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

CREATE INDEX "fkcontrol_fk" ON "public"."rm_risk_control"
  USING btree ("fkcontrol");

CREATE INDEX "fkprobabilityvaluename_fk2" ON "public"."rm_risk_control"
  USING btree ("fkprobabilityvaluename");

CREATE INDEX "fkrisk_fk" ON "public"."rm_risk_control"
  USING btree ("fkrisk");

ALTER TABLE "public"."rm_risk_control"
  ADD CONSTRAINT "pk_rm_risk_control" 
  PRIMARY KEY ("fkcontext");

CREATE UNIQUE INDEX "rm_risk_control_pk" ON "public"."rm_risk_control"
  USING btree ("fkcontext");

ALTER TABLE "public"."rm_risk_control_value"
  OWNER TO "isms";

ALTER TABLE "public"."rm_risk_control_value"
  ALTER COLUMN "fkriskcontrol" TYPE INTEGER;

ALTER TABLE "public"."rm_risk_control_value"
  ALTER COLUMN "fkparametername" TYPE INTEGER;

ALTER TABLE "public"."rm_risk_control_value"
  ALTER COLUMN "fkrcvaluename" TYPE INTEGER;

ALTER TABLE "public"."rm_risk_control_value"
  ADD CONSTRAINT "fk_rm_risk__fkparamet_rm_param" FOREIGN KEY ("fkparametername")
    REFERENCES "public"."rm_parameter_name"("pkparametername")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."rm_risk_control_value"
  ADD CONSTRAINT "fk_rm_risk__fkrcvalue_rm_rc_pa" FOREIGN KEY ("fkrcvaluename")
    REFERENCES "public"."rm_rc_parameter_value_name"("pkrcvaluename")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."rm_risk_control_value"
  ADD CONSTRAINT "fk_rm_risk__fkriskcon_rm_risk_" FOREIGN KEY ("fkriskcontrol")
    REFERENCES "public"."rm_risk_control"("fkcontext")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

CREATE INDEX "fkparametername_fk3" ON "public"."rm_risk_control_value"
  USING btree ("fkparametername");

CREATE INDEX "fkrcvaluename_fk" ON "public"."rm_risk_control_value"
  USING btree ("fkrcvaluename");

CREATE INDEX "fkriskcontrol_fk" ON "public"."rm_risk_control_value"
  USING btree ("fkriskcontrol");

ALTER TABLE "public"."rm_risk_control_value"
  ADD CONSTRAINT "pk_rm_risk_control_value" 
  PRIMARY KEY ("fkriskcontrol", "fkparametername", "fkrcvaluename");

CREATE UNIQUE INDEX "rm_risk_control_value_pk" ON "public"."rm_risk_control_value"
  USING btree ("fkriskcontrol", "fkparametername", "fkrcvaluename");

ALTER TABLE "public"."rm_risk_limits"
  OWNER TO "isms";

ALTER TABLE "public"."rm_risk_limits"
  ALTER COLUMN "fkcontext" TYPE INTEGER;

ALTER TABLE "public"."rm_risk_limits"
  ADD CONSTRAINT "fk_rm_risk__fkcontext_isms_con2" FOREIGN KEY ("fkcontext")
    REFERENCES "public"."isms_context"("pkcontext")
    MATCH FULL
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."rm_risk_limits"
  ADD CONSTRAINT "pk_rm_risk_limits" 
  PRIMARY KEY ("fkcontext");

CREATE UNIQUE INDEX "rm_risk_limits_pk" ON "public"."rm_risk_limits"
  USING btree ("fkcontext");

ALTER TABLE "public"."rm_risk_value"
  OWNER TO "isms";

ALTER TABLE "public"."rm_risk_value"
  ALTER COLUMN "fkrisk" TYPE INTEGER;

ALTER TABLE "public"."rm_risk_value"
  ALTER COLUMN "fkvaluename" TYPE INTEGER;

ALTER TABLE "public"."rm_risk_value"
  ALTER COLUMN "fkparametername" TYPE INTEGER;

ALTER TABLE "public"."rm_risk_value"
  ADD CONSTRAINT "fk_rm_risk__fkparamet_rm_param2" FOREIGN KEY ("fkparametername")
    REFERENCES "public"."rm_parameter_name"("pkparametername")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."rm_risk_value"
  ADD CONSTRAINT "fk_rm_risk__fkrisk_rm_risk2" FOREIGN KEY ("fkrisk")
    REFERENCES "public"."rm_risk"("fkcontext")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."rm_risk_value"
  ADD CONSTRAINT "fk_rm_risk__fkvaluena_rm_param" FOREIGN KEY ("fkvaluename")
    REFERENCES "public"."rm_parameter_value_name"("pkvaluename")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

CREATE INDEX "fkparametername_fk2" ON "public"."rm_risk_value"
  USING btree ("fkparametername");

CREATE INDEX "fkrisk_fk2" ON "public"."rm_risk_value"
  USING btree ("fkrisk");

CREATE INDEX "fkvaluename_fk2" ON "public"."rm_risk_value"
  USING btree ("fkvaluename");

ALTER TABLE "public"."rm_risk_value"
  ADD CONSTRAINT "pk_rm_risk_value" 
  PRIMARY KEY ("fkrisk", "fkvaluename", "fkparametername");

CREATE UNIQUE INDEX "rm_risk_value_pk" ON "public"."rm_risk_value"
  USING btree ("fkrisk", "fkvaluename", "fkparametername");

ALTER TABLE "public"."rm_section_best_practice"
  OWNER TO "isms";

ALTER TABLE "public"."rm_section_best_practice"
  ALTER COLUMN "fkparent" TYPE INTEGER;

ALTER TABLE "public"."rm_section_best_practice"
  ADD CONSTRAINT "fk_rm_secti_fkcontext_isms_con" FOREIGN KEY ("fkcontext")
    REFERENCES "public"."isms_context"("pkcontext")
    MATCH FULL
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."rm_section_best_practice"
  ADD CONSTRAINT "fk_rm_secti_fkparent_rm_secti" FOREIGN KEY ("fkparent")
    REFERENCES "public"."rm_section_best_practice"("fkcontext")
    MATCH FULL
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

CREATE INDEX "fkparent_fk3" ON "public"."rm_section_best_practice"
  USING btree ("fkparent");

CREATE UNIQUE INDEX "rm_section_best_practice_pk" ON "public"."rm_section_best_practice"
  USING btree ("fkcontext");

ALTER TABLE "public"."rm_standard"
  OWNER TO "isms";

ALTER TABLE "public"."rm_standard"
  ADD CONSTRAINT "fk_rm_stand_fkcontext_isms_con" FOREIGN KEY ("fkcontext")
    REFERENCES "public"."isms_context"("pkcontext")
    MATCH FULL
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

CREATE UNIQUE INDEX "rm_standard_pk" ON "public"."rm_standard"
  USING btree ("fkcontext");

ALTER TABLE "public"."wkf_alert"
  OWNER TO "isms";

ALTER TABLE "public"."wkf_alert"
  ALTER COLUMN "bemailsent" SET DEFAULT 0;

ALTER TABLE "public"."wkf_alert"
  ADD CONSTRAINT "ckc_bemailsent_wkf_aler" CHECK ((bemailsent IS NULL) OR ((bemailsent >= 0) AND (bemailsent <= 1)));

ALTER TABLE "public"."wkf_alert"
  ALTER COLUMN "fkcontext" TYPE INTEGER;

ALTER TABLE "public"."wkf_alert"
  ALTER COLUMN "fkreceiver" TYPE INTEGER;

ALTER TABLE "public"."wkf_alert"
  ALTER COLUMN "fkcreator" TYPE INTEGER;

ALTER TABLE "public"."wkf_alert"
  ALTER COLUMN "ntype" SET DEFAULT 0;

ALTER TABLE "public"."wkf_alert"
  ADD CONSTRAINT "fk_wkf_aler_fkcontext_isms_con" FOREIGN KEY ("fkcontext")
    REFERENCES "public"."isms_context"("pkcontext")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."wkf_alert"
  ADD CONSTRAINT "fk_wkf_aler_fkcreator_isms_use" FOREIGN KEY ("fkcreator")
    REFERENCES "public"."isms_user"("fkcontext")
    MATCH FULL
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."wkf_alert"
  ADD CONSTRAINT "fk_wkf_aler_fkreceive_isms_use" FOREIGN KEY ("fkreceiver")
    REFERENCES "public"."isms_user"("fkcontext")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

CREATE INDEX "fkcontext_fk2" ON "public"."wkf_alert"
  USING btree ("fkcontext");

CREATE INDEX "fkcreator_fk2" ON "public"."wkf_alert"
  USING btree ("fkcreator");

CREATE INDEX "fkreceiver_fk2" ON "public"."wkf_alert"
  USING btree ("fkreceiver");

ALTER TABLE "public"."wkf_alert"
  ADD CONSTRAINT "pk_wkf_alert" 
  PRIMARY KEY ("pkalert");

CREATE UNIQUE INDEX "wkf_alert_pk" ON "public"."wkf_alert"
  USING btree ("pkalert");

ALTER TABLE "public"."wkf_control_efficiency"
  OWNER TO "isms";

ALTER TABLE "public"."wkf_control_efficiency"
  ALTER COLUMN "fkcontrolefficiency" TYPE INTEGER;

ALTER TABLE "public"."wkf_control_efficiency"
  ADD COLUMN "fkschedule" INTEGER;

ALTER TABLE "public"."wkf_control_efficiency"
  ALTER COLUMN "nrealefficiency" SET DEFAULT 0;

ALTER TABLE "public"."wkf_control_efficiency"
  ALTER COLUMN "nexpectedefficiency" SET DEFAULT 0;

ALTER TABLE "public"."wkf_control_efficiency"
  ADD COLUMN "svalue1" VARCHAR(256);

ALTER TABLE "public"."wkf_control_efficiency"
  ADD COLUMN "svalue2" VARCHAR(256);

ALTER TABLE "public"."wkf_control_efficiency"
  ADD COLUMN "svalue3" VARCHAR(256);

ALTER TABLE "public"."wkf_control_efficiency"
  ADD COLUMN "svalue4" VARCHAR(256);

ALTER TABLE "public"."wkf_control_efficiency"
  ADD COLUMN "svalue5" VARCHAR(256);

ALTER TABLE "public"."wkf_control_efficiency"
  ADD COLUMN "tmetric" TEXT;

ALTER TABLE "public"."wkf_control_efficiency"
  ADD CONSTRAINT "fk_wkf_cont_fkcontrol_rm_contr2" FOREIGN KEY ("fkcontrolefficiency")
    REFERENCES "public"."rm_control"("fkcontext")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."wkf_control_efficiency"
  ADD CONSTRAINT "fk_wkf_cont_fkschedul_wkf_sche" FOREIGN KEY ("fkschedule")
    REFERENCES "public"."wkf_schedule"("pkschedule")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

CREATE INDEX "fkschedule_fk" ON "public"."wkf_control_efficiency"
  USING btree ((fkschedule));

ALTER TABLE "public"."wkf_control_efficiency"
  ADD CONSTRAINT "pk_wkf_control_efficiency" 
  PRIMARY KEY ("fkcontrolefficiency");

CREATE UNIQUE INDEX "wkf_control_efficiency_pk" ON "public"."wkf_control_efficiency"
  USING btree ("fkcontrolefficiency");

ALTER TABLE "public"."wkf_control_test"
  OWNER TO "isms";

ALTER TABLE "public"."wkf_control_test"
  ALTER COLUMN "fkcontroltest" TYPE INTEGER;

ALTER TABLE "public"."wkf_control_test"
  ADD COLUMN "fkschedule" INTEGER;

ALTER TABLE "public"."wkf_control_test"
  ADD CONSTRAINT "fk_wkf_cont_fkcontrol_rm_contr" FOREIGN KEY ("fkcontroltest")
    REFERENCES "public"."rm_control"("fkcontext")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."wkf_control_test"
  ADD CONSTRAINT "fk_wkf_cont_fkschedul_wkf_sch2" FOREIGN KEY ("fkschedule")
    REFERENCES "public"."wkf_schedule"("pkschedule")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

CREATE INDEX "fkschedule_fk2" ON "public"."wkf_control_test"
  USING btree ((fkschedule));

ALTER TABLE "public"."wkf_control_test"
  ADD CONSTRAINT "pk_wkf_control_test" 
  PRIMARY KEY ("fkcontroltest");

CREATE UNIQUE INDEX "wkf_control_test_pk" ON "public"."wkf_control_test"
  USING btree ("fkcontroltest");

ALTER TABLE "public"."wkf_schedule"
  OWNER TO "isms";

ALTER TABLE "public"."wkf_task"
  OWNER TO "isms";

ALTER TABLE "public"."wkf_task"
  ALTER COLUMN "bemailsent" SET DEFAULT 0;

ALTER TABLE "public"."wkf_task"
  ADD CONSTRAINT "ckc_bemailsent_wkf_task" CHECK ((bemailsent IS NULL) OR ((bemailsent >= 0) AND (bemailsent <= 1)));

ALTER TABLE "public"."wkf_task"
  ALTER COLUMN "fkcontext" TYPE INTEGER;

ALTER TABLE "public"."wkf_task"
  ALTER COLUMN "fkcontext" SET NOT NULL;

ALTER TABLE "public"."wkf_task"
  ALTER COLUMN "fkcreator" TYPE INTEGER;

ALTER TABLE "public"."wkf_task"
  ALTER COLUMN "fkreceiver" TYPE INTEGER;

ALTER TABLE "public"."wkf_task"
  ALTER COLUMN "nactivity" SET DEFAULT 0;

ALTER TABLE "public"."wkf_task"
  ADD CONSTRAINT "fk_wkf_task_fkcontext_isms_con" FOREIGN KEY ("fkcontext")
    REFERENCES "public"."isms_context"("pkcontext")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."wkf_task"
  ADD CONSTRAINT "fk_wkf_task_fkcreator_isms_use" FOREIGN KEY ("fkcreator")
    REFERENCES "public"."isms_user"("fkcontext")
    MATCH FULL
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

ALTER TABLE "public"."wkf_task"
  ADD CONSTRAINT "fk_wkf_task_fkreceive_isms_use" FOREIGN KEY ("fkreceiver")
    REFERENCES "public"."isms_user"("fkcontext")
    MATCH FULL
    ON DELETE CASCADE
    ON UPDATE RESTRICT
    NOT DEFERRABLE;

CREATE INDEX "fkcontext_fk" ON "public"."wkf_task"
  USING btree ("fkcontext");

CREATE INDEX "fkcreator_fk" ON "public"."wkf_task"
  USING btree ("fkcreator");

CREATE INDEX "fkreceiver_fk" ON "public"."wkf_task"
  USING btree ("fkreceiver");

ALTER TABLE "public"."wkf_task"
  ADD CONSTRAINT "pk_wkf_task" 
  PRIMARY KEY ("pktask");

CREATE UNIQUE INDEX "wkf_task_pk" ON "public"."wkf_task"
  USING btree ("pktask");

CREATE TABLE "public"."wkf_task_schedule" (
  "fkcontext" INTEGER NOT NULL, 
  "nactivity" INTEGER DEFAULT 0 NOT NULL, 
  "fkschedule" INTEGER, 
  "balertsent" INTEGER DEFAULT 0 NOT NULL, 
  "nalerttype" INTEGER DEFAULT 0
) WITH OIDS;

ALTER TABLE "public"."wkf_task_schedule"
  OWNER TO "isms";
