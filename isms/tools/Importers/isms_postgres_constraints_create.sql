alter table CI_ACTION_PLAN
   add constraint FK_CI_ACTIO_FKCONTEXT_ISMS_CON foreign key (FKCONTEXT)
      references ISMS_CONTEXT (PKCONTEXT)
      on delete restrict on update restrict;

alter table CI_ACTION_PLAN
   add constraint FK_CI_ACTIO_FKRESPONS_ISMS_USE foreign key (FKRESPONSIBLE)
      references ISMS_USER (FKCONTEXT)
      on delete restrict on update restrict;

alter table CI_CATEGORY
   add constraint FK_CI_CATEG_FKCONTEXT_ISMS_CON foreign key (FKCONTEXT)
      references ISMS_CONTEXT (PKCONTEXT)
      on delete restrict on update restrict;

alter table CI_INCIDENT
   add constraint FK_CI_INCID_FKCATEGOR_CI_CATEG foreign key (FKCATEGORY)
      references CI_CATEGORY (FKCONTEXT)
      on delete restrict on update restrict;

alter table CI_INCIDENT
   add constraint FK_CI_INCID_FKCONTEXT_ISMS_CON2 foreign key (FKCONTEXT)
      references ISMS_CONTEXT (PKCONTEXT)
      on delete restrict on update restrict;

alter table CI_INCIDENT
   add constraint FK_CI_INCID_FKRESPONS_ISMS_USE foreign key (FKRESPONSIBLE)
      references ISMS_USER (FKCONTEXT)
      on delete restrict on update restrict;

alter table CI_INCIDENT_CONTROL
   add constraint FK_CI_INCID_FKCONTEXT_ISMS_CON8 foreign key (FKCONTEXT)
      references ISMS_CONTEXT (PKCONTEXT)
      on delete restrict on update restrict;

alter table CI_INCIDENT_CONTROL
   add constraint FK_CI_INCID_FKCONTROL_RM_CONTR foreign key (FKCONTROL)
      references RM_CONTROL (FKCONTEXT)
      on delete cascade on update restrict;

alter table CI_INCIDENT_CONTROL
   add constraint FK_CI_INCID_FKINCIDEN_CI_INCID2 foreign key (FKINCIDENT)
      references CI_INCIDENT (FKCONTEXT)
      on delete cascade on update restrict;

alter table CI_INCIDENT_FINANCIAL_IMPACT
   add constraint FK_CI_INCID_FKCLASSIF_ISMS_CON foreign key (FKCLASSIFICATION)
      references ISMS_CONTEXT_CLASSIFICATION (PKCLASSIFICATION)
      on delete restrict on update restrict;

alter table CI_INCIDENT_FINANCIAL_IMPACT
   add constraint FK_CI_INCID_FKINCIDEN_CI_INCID3 foreign key (FKINCIDENT)
      references CI_INCIDENT (FKCONTEXT)
      on delete cascade on update restrict;

alter table CI_INCIDENT_PROCESS
   add constraint FK_CI_INCID_FKCONTEXT_ISMS_CON foreign key (FKCONTEXT)
      references ISMS_CONTEXT (PKCONTEXT)
      on delete restrict on update restrict;

alter table CI_INCIDENT_PROCESS
   add constraint FK_CI_INCID_FKINCIDEN_CI_INCID4 foreign key (FKINCIDENT)
      references CI_INCIDENT (FKCONTEXT)
      on delete cascade on update restrict;

alter table CI_INCIDENT_PROCESS
   add constraint FK_CI_INCID_FKPROCESS_RM_PROCE foreign key (FKPROCESS)
      references RM_PROCESS (FKCONTEXT)
      on delete cascade on update restrict;

alter table CI_INCIDENT_RISK
   add constraint FK_CI_INCID_FKCONTEXT_ISMS_CON7 foreign key (FKCONTEXT)
      references ISMS_CONTEXT (PKCONTEXT)
      on delete restrict on update restrict;

alter table CI_INCIDENT_RISK
   add constraint FK_CI_INCID_FKINCIDEN_CI_INCID5 foreign key (FKINCIDENT)
      references CI_INCIDENT (FKCONTEXT)
      on delete cascade on update restrict;

alter table CI_INCIDENT_RISK
   add constraint FK_CI_INCID_FKRISK_RM_RISK foreign key (FKRISK)
      references RM_RISK (FKCONTEXT)
      on delete cascade on update restrict;

alter table CI_INCIDENT_RISK_VALUE
   add constraint FK_CI_INCID_FKINCRISK_CI_INCID foreign key (FKINCRISK)
      references CI_INCIDENT_RISK (FKCONTEXT)
      on delete cascade on update restrict;

alter table CI_INCIDENT_RISK_VALUE
   add constraint FK_CI_INCID_FKPARAMET_RM_PARAM foreign key (FKPARAMETERNAME)
      references RM_PARAMETER_NAME (PKPARAMETERNAME)
      on delete cascade on update restrict;

alter table CI_INCIDENT_RISK_VALUE
   add constraint FK_CI_INCID_FKVALUENA_RM_PARAM foreign key (FKVALUENAME)
      references RM_PARAMETER_VALUE_NAME (PKVALUENAME)
      on delete cascade on update restrict;

alter table CI_INCIDENT_USER
   add constraint FK_CI_INCID_FKINCIDEN_CI_INCID foreign key (FKINCIDENT)
      references CI_INCIDENT (FKCONTEXT)
      on delete cascade on update restrict;

alter table CI_INCIDENT_USER
   add constraint FK_CI_INCID_FKUSER_ISMS_USE foreign key (FKUSER)
      references ISMS_USER (FKCONTEXT)
      on delete cascade on update restrict;

alter table CI_NC
   add constraint FK_CI_NC_FKCONTEXT_ISMS_CON foreign key (FKCONTEXT)
      references ISMS_CONTEXT (PKCONTEXT)
      on delete restrict on update restrict;

alter table CI_NC
   add constraint FK_CI_NC_FKCONTROL_RM_CONTR foreign key (FKCONTROL)
      references RM_CONTROL (FKCONTEXT)
      on delete cascade on update restrict;

alter table CI_NC
   add constraint FK_CI_NC_FKRESPONS_ISMS_USE foreign key (FKRESPONSIBLE)
      references ISMS_USER (FKCONTEXT)
      on delete restrict on update restrict;

alter table CI_NC
   add constraint FK_CI_NC_FKSENDER_ISMS_USE foreign key (FKSENDER)
      references ISMS_USER (FKCONTEXT)
      on delete restrict on update restrict;

alter table CI_NC_ACTION_PLAN
   add constraint FK_CI_NC_AC_FKACTIONP_CI_ACTIO foreign key (FKACTIONPLAN)
      references CI_ACTION_PLAN (FKCONTEXT)
      on delete cascade on update restrict;

alter table CI_NC_ACTION_PLAN
   add constraint FK_CI_NC_AC_FKNC_CI_NC foreign key (FKNC)
      references CI_NC (FKCONTEXT)
      on delete cascade on update restrict;

alter table CI_NC_PROCESS
   add constraint FK_CI_NC_PR_FKNC_CI_NC foreign key (FKNC)
      references CI_NC (FKCONTEXT)
      on delete cascade on update restrict;

alter table CI_NC_PROCESS
   add constraint FK_CI_NC_PR_FKPROCESS_RM_PROCE foreign key (FKPROCESS)
      references RM_PROCESS (FKCONTEXT)
      on delete cascade on update restrict;

alter table CI_NC_SEED
   add constraint FK_CI_NC_SE_FKCONTROL_RM_CONTR foreign key (FKCONTROL)
      references RM_CONTROL (FKCONTEXT)
      on delete cascade on update restrict;

alter table CI_OCCURRENCE
   add constraint FK_CI_OCCUR_FKCONTEXT_ISMS_CON foreign key (FKCONTEXT)
      references ISMS_CONTEXT (PKCONTEXT)
      on delete restrict on update restrict;

alter table CI_OCCURRENCE
   add constraint FK_CI_OCCUR_FKINCIDEN_CI_INCID foreign key (FKINCIDENT)
      references CI_INCIDENT (FKCONTEXT)
      on delete cascade on update restrict;

alter table CI_RISK_PROBABILITY
   add constraint FK_CI_RISK__FKRISK_CI_RISK_ foreign key (FKRISK)
      references CI_RISK_SCHEDULE (FKRISK)
      on delete cascade on update restrict;

alter table CI_RISK_PROBABILITY
   add constraint FK_CI_RISK__FKVALUENA_RM_PARAM foreign key (FKVALUENAME)
      references RM_PARAMETER_VALUE_NAME (PKVALUENAME)
      on delete cascade on update restrict;

alter table CI_RISK_SCHEDULE
   add constraint FK_CI_RISK__FKRISK_RM_RISK foreign key (FKRISK)
      references RM_RISK (FKCONTEXT)
      on delete cascade on update restrict;

alter table CI_SOLUTION
   add constraint FK_CI_SOLUT_FKCATEGOR_CI_CATEG foreign key (FKCATEGORY)
      references CI_CATEGORY (FKCONTEXT)
      on delete cascade on update restrict;

alter table CI_SOLUTION
   add constraint FK_CI_SOLUT_FKCONTEXT_ISMS_CON foreign key (FKCONTEXT)
      references ISMS_CONTEXT (PKCONTEXT)
      on delete restrict on update restrict;

alter table ISMS_CONTEXT_DATE
   add constraint FK_ISMS_CON_FKCONTEXT_ISMS_CON foreign key (FKCONTEXT)
      references ISMS_CONTEXT (PKCONTEXT)
      on delete cascade on update restrict;

alter table ISMS_CONTEXT_HISTORY
   add constraint FK_ISMS_CON_FKCONTEXT_ISMS_CON2 foreign key (FKCONTEXT)
      references ISMS_CONTEXT (PKCONTEXT)
      on delete cascade on update restrict;

alter table ISMS_POLICY
   add constraint FK_ISMS_POL_FKCONTEXT_ISMS_CON foreign key (FKCONTEXT)
      references ISMS_CONTEXT (PKCONTEXT)
      on delete restrict on update restrict;

alter table ISMS_PROFILE
   add constraint FK_ISMS_PRO_FKCONTEXT_ISMS_CON foreign key (FKCONTEXT)
      references ISMS_CONTEXT (PKCONTEXT)
      on delete restrict on update restrict;

alter table ISMS_PROFILE_ACL
   add constraint FK_ISMS_PRO_FKPROFILE_ISMS_PRO foreign key (FKPROFILE)
      references ISMS_PROFILE (FKCONTEXT)
      on delete cascade on update restrict;

alter table ISMS_SCOPE
   add constraint FK_ISMS_SCO_FKCONTEXT_ISMS_CON foreign key (FKCONTEXT)
      references ISMS_CONTEXT (PKCONTEXT)
      on delete restrict on update restrict;

alter table ISMS_SOLICITOR
   add constraint FK_ISMS_SOL_FKSOLICIT_ISMS_USE foreign key (FKSOLICITOR)
      references ISMS_USER (FKCONTEXT)
      on delete restrict on update restrict;

alter table ISMS_SOLICITOR
   add constraint FK_ISMS_SOL_FKUSER_ISMS_USE foreign key (FKUSER)
      references ISMS_USER (FKCONTEXT)
      on delete cascade on update restrict;

alter table ISMS_USER
   add constraint FK_ISMS_USE_FKCONTEXT_ISMS_CON foreign key (FKCONTEXT)
      references ISMS_CONTEXT (PKCONTEXT)
      on delete restrict on update restrict;

alter table ISMS_USER
   add constraint FK_ISMS_USE_FKPROFILE_ISMS_PRO foreign key (FKPROFILE)
      references ISMS_PROFILE (FKCONTEXT)
      on delete restrict on update restrict;

alter table ISMS_USER_PASSWORD_HISTORY
   add constraint FK_ISMS_USE_FKUSER_ISMS_USE3 foreign key (FKUSER)
      references ISMS_USER (FKCONTEXT)
      on delete cascade on update restrict;

alter table ISMS_USER_PREFERENCE
   add constraint FK_ISMS_USE_FKUSER_ISMS_USE foreign key (FKUSER)
      references ISMS_USER (FKCONTEXT)
      on delete cascade on update restrict;

alter table PM_DOCUMENT
   add constraint FK_PM_DOCUM_FKAUTHOR_ISMS_USE foreign key (FKAUTHOR)
      references ISMS_USER (FKCONTEXT)
      on delete restrict on update restrict;

alter table PM_DOCUMENT
   add constraint FK_PM_DOCUM_FKCLASSIF_ISMS_CON foreign key (FKCLASSIFICATION)
      references ISMS_CONTEXT_CLASSIFICATION (PKCLASSIFICATION)
      on delete restrict on update restrict;

alter table PM_DOCUMENT
   add constraint FK_PM_DOCUM_FKCONTEXT_ISMS_CON foreign key (FKCONTEXT)
      references ISMS_CONTEXT (PKCONTEXT)
      on delete restrict on update restrict;

alter table PM_DOCUMENT
   add constraint FK_PM_DOCUM_FKPARENT_PM_DOCUM foreign key (FKPARENT)
      references PM_DOCUMENT (FKCONTEXT)
      on delete restrict on update restrict;

alter table PM_DOCUMENT
   add constraint FK_PM_DOCUM_FKCURRENT_PM_DOC_I foreign key (FKCURRENTVERSION)
      references PM_DOC_INSTANCE (FKCONTEXT)
      on delete restrict on update restrict;

alter table PM_DOCUMENT
   add constraint FK_PM_DOCUM_FKMAINAPP_ISMS_USE foreign key (FKMAINAPPROVER)
      references ISMS_USER (FKCONTEXT)
      on delete restrict on update restrict;

alter table PM_DOCUMENT
   add constraint FK_PM_DOCUM_FKSCHEDUL_WKF_SCHE foreign key (FKSCHEDULE)
      references WKF_SCHEDULE (PKSCHEDULE)
      on delete restrict on update restrict;

alter table PM_DOCUMENT_REVISION_HISTORY
   add constraint FK_PM_DOCUM_FKDOCUMEN_PM_DOCUM foreign key (FKDOCUMENT)
      references PM_DOCUMENT (FKCONTEXT)
      on delete cascade on update restrict;

alter table PM_DOC_APPROVERS
   add constraint FK_PM_DOC_A_FKDOCUMEN_PM_DOCUM foreign key (FKDOCUMENT)
      references PM_DOCUMENT (FKCONTEXT)
      on delete cascade on update restrict;

alter table PM_DOC_APPROVERS
   add constraint FK_PM_DOC_A_FKUSER_ISMS_USE foreign key (FKUSER)
      references ISMS_USER (FKCONTEXT)
      on delete cascade on update restrict;

alter table PM_DOC_CONTEXT
   add constraint FK_PM_DOC_C_FKCONTEXT_ISMS_CON foreign key (FKCONTEXT)
      references ISMS_CONTEXT (PKCONTEXT)
      on delete cascade on update restrict;

alter table PM_DOC_CONTEXT
   add constraint FK_PM_DOC_C_FKDOCUMEN_PM_DOCUM foreign key (FKDOCUMENT)
      references PM_DOCUMENT (FKCONTEXT)
      on delete cascade on update restrict;

alter table PM_DOC_INSTANCE
   add constraint FK_PM_DOC_I_FKCONTEXT_ISMS_CON foreign key (FKCONTEXT)
      references ISMS_CONTEXT (PKCONTEXT)
      on delete restrict on update restrict;

alter table PM_DOC_INSTANCE
   add constraint FK_PM_DOC_I_FKDOCUMEN_PM_DOCUM foreign key (FKDOCUMENT)
      references PM_DOCUMENT (FKCONTEXT)
      on delete cascade on update restrict;

alter table PM_DOC_READERS
   add constraint FK_PM_DOC_R_FKDOCUMEN_PM_DOCUM4 foreign key (FKDOCUMENT)
      references PM_DOCUMENT (FKCONTEXT)
      on delete cascade on update restrict;

alter table PM_DOC_READERS
   add constraint FK_PM_DOC_R_FKUSER_ISMS_USE foreign key (FKUSER)
      references ISMS_USER (FKCONTEXT)
      on delete cascade on update restrict;

alter table PM_DOC_READ_HISTORY
   add constraint FK_PM_DOC_R_FKINSTANC_PM_DOC_I foreign key (FKINSTANCE)
      references PM_DOC_INSTANCE (FKCONTEXT)
      on delete cascade on update restrict;

alter table PM_DOC_READ_HISTORY
   add constraint FK_PM_DOC_R_FKUSER_ISMS_USE2 foreign key (FKUSER)
      references ISMS_USER (FKCONTEXT)
      on delete cascade on update restrict;

alter table PM_DOC_REFERENCE
   add constraint FK_PM_DOC_R_FKDOCUMEN_PM_DOCUM2 foreign key (FKDOCUMENT)
      references PM_DOCUMENT (FKCONTEXT)
      on delete cascade on update restrict;

alter table PM_DOC_REGISTERS
   add constraint FK_PM_DOC_R_FKDOCUMEN_PM_DOCUM3 foreign key (FKDOCUMENT)
      references PM_DOCUMENT (FKCONTEXT)
      on delete cascade on update restrict;

alter table PM_DOC_REGISTERS
   add constraint FK_PM_DOC_R_FKREGISTE_PM_REGIS foreign key (FKREGISTER)
      references PM_REGISTER (FKCONTEXT)
      on delete cascade on update restrict;

alter table PM_INSTANCE_COMMENT
   add constraint FK_PM_INSTA_FKINSTANC_PM_DOC_I foreign key (FKINSTANCE)
      references PM_DOC_INSTANCE (FKCONTEXT)
      on delete cascade on update restrict;

alter table PM_INSTANCE_COMMENT
   add constraint FK_PM_INSTA_FKUSER_ISMS_USE foreign key (FKUSER)
      references ISMS_USER (FKCONTEXT)
      on delete cascade on update restrict;

alter table PM_INSTANCE_CONTENT
   add constraint FK_PM_INSTA_FKINSTANC_PM_DOC_I2 foreign key (FKINSTANCE)
      references PM_DOC_INSTANCE (FKCONTEXT)
      on delete cascade on update restrict;

alter table PM_PROCESS_USER
   add constraint FK_PM_PROCE_FKPROCESS_RM_PROCE foreign key (FKPROCESS)
      references RM_PROCESS (FKCONTEXT)
      on delete cascade on update restrict;

alter table PM_PROCESS_USER
   add constraint FK_PM_PROCE_FKUSER_ISMS_USE foreign key (FKUSER)
      references ISMS_USER (FKCONTEXT)
      on delete cascade on update restrict;

alter table PM_REGISTER
   add constraint FK_PM_REGIS_FKCLASSIF_ISMS_CON foreign key (FKCLASSIFICATION)
      references ISMS_CONTEXT_CLASSIFICATION (PKCLASSIFICATION)
      on delete restrict on update restrict;

alter table PM_REGISTER
   add constraint FK_PM_REGIS_FKCONTEXT_ISMS_CON foreign key (FKCONTEXT)
      references ISMS_CONTEXT (PKCONTEXT)
      on delete restrict on update restrict;

alter table PM_REGISTER
   add constraint FK_PM_REGIS_FKDOCUMEN_PM_DOCUM foreign key (FKDOCUMENT)
      references PM_DOCUMENT (FKCONTEXT)
      on delete restrict on update restrict;

alter table PM_REGISTER
   add constraint FK_PM_REGIS_FKRESPONS_ISMS_USE foreign key (FKRESPONSIBLE)
      references ISMS_USER (FKCONTEXT)
      on delete restrict on update restrict;

alter table PM_REGISTER_READERS
   add constraint FK_PM_REGIS_FKREGISTE_PM_REGIS foreign key (FKREGISTER)
      references PM_REGISTER (FKCONTEXT)
      on delete cascade on update restrict;

alter table PM_REGISTER_READERS
   add constraint FK_PM_REGIS_FKUSER_ISMS_USE foreign key (FKUSER)
      references ISMS_USER (FKCONTEXT)
      on delete cascade on update restrict;

alter table PM_TEMPLATE
   add constraint FK_PM_TEMPL_FKCONTEXT_ISMS_CON foreign key (FKCONTEXT)
      references ISMS_CONTEXT (PKCONTEXT)
      on delete restrict on update restrict;

alter table PM_TEMPLATE_BEST_PRACTICE
   add constraint FK_PM_TEMPL_FKBESTPRA_RM_BEST_ foreign key (FKBESTPRACTICE)
      references RM_BEST_PRACTICE (FKCONTEXT)
      on delete cascade on update restrict;

alter table PM_TEMPLATE_BEST_PRACTICE
   add constraint FK_PM_TEMPL_FKTEMPLAT_PM_TEMPL foreign key (FKTEMPLATE)
      references PM_TEMPLATE (FKCONTEXT)
      on delete cascade on update restrict;

alter table PM_TEMPLATE_CONTENT
   add constraint FK_PM_TEMPL_FKCONTEXT_PM_TEMPL foreign key (FKCONTEXT)
      references PM_TEMPLATE (FKCONTEXT)
      on delete cascade on update restrict;

alter table RM_AREA
   add constraint FK_RM_AREA_FKCONTEXT_ISMS_CON foreign key (FKCONTEXT)
      references ISMS_CONTEXT (PKCONTEXT)
      on delete restrict on update restrict;

alter table RM_AREA
   add constraint FK_RM_AREA_FKPARENT_RM_AREA foreign key (FKPARENT)
      references RM_AREA (FKCONTEXT)
      on delete restrict on update restrict;

alter table RM_AREA
   add constraint FK_RM_AREA_FKPRIORIT_ISMS_CON foreign key (FKPRIORITY)
      references ISMS_CONTEXT_CLASSIFICATION (PKCLASSIFICATION)
      on delete restrict on update restrict;

alter table RM_AREA
   add constraint FK_RM_AREA_FKRESPONS_ISMS_USE foreign key (FKRESPONSIBLE)
      references ISMS_USER (FKCONTEXT)
      on delete restrict on update restrict;

alter table RM_AREA
   add constraint FK_RM_AREA_FKTYPE_ISMS_CON foreign key (FKTYPE)
      references ISMS_CONTEXT_CLASSIFICATION (PKCLASSIFICATION)
      on delete restrict on update restrict;

alter table RM_ASSET
   add constraint FK_RM_ASSET_FKCATEGOR_RM_CATEG foreign key (FKCATEGORY)
      references RM_CATEGORY (FKCONTEXT)
      on delete cascade on update restrict;

alter table RM_ASSET
   add constraint FK_RM_ASSET_FKCONTEXT_ISMS_CON foreign key (FKCONTEXT)
      references ISMS_CONTEXT (PKCONTEXT)
      on delete restrict on update restrict;

alter table RM_ASSET
   add constraint FK_RM_ASSET_FKRESPONS_ISMS_USE foreign key (FKRESPONSIBLE)
      references ISMS_USER (FKCONTEXT)
      on delete restrict on update restrict;

alter table RM_ASSET
   add constraint FK_RM_ASSET_FKSECURIT_ISMS_USE foreign key (FKSECURITYRESPONSIBLE)
      references ISMS_USER (FKCONTEXT)
      on delete restrict on update restrict;

alter table RM_ASSET_ASSET
   add constraint FK_RM_ASSET_FKASSET_RM_ASSET foreign key (FKASSET)
      references RM_ASSET (FKCONTEXT)
      on delete restrict on update restrict;

alter table RM_ASSET_ASSET
   add constraint FK_RM_ASSET_FKDEPENDE_RM_ASSET foreign key (FKDEPENDENT)
      references RM_ASSET (FKCONTEXT)
      on delete restrict on update restrict;

alter table RM_ASSET_VALUE
   add constraint FK_RM_ASSET_FKASSET_RM_ASSET2 foreign key (FKASSET)
      references RM_ASSET (FKCONTEXT)
      on delete cascade on update restrict;

alter table RM_ASSET_VALUE
   add constraint FK_RM_ASSET_FKPARAMET_RM_PARAM foreign key (FKPARAMETERNAME)
      references RM_PARAMETER_NAME (PKPARAMETERNAME)
      on delete cascade on update restrict;

alter table RM_ASSET_VALUE
   add constraint FK_RM_ASSET_FKVALUENA_RM_PARAM foreign key (FKVALUENAME)
      references RM_PARAMETER_VALUE_NAME (PKVALUENAME)
      on delete cascade on update restrict;

alter table RM_BEST_PRACTICE
   add constraint FK_RM_BEST__FKCONTEXT_ISMS_CON3 foreign key (FKCONTEXT)
      references ISMS_CONTEXT (PKCONTEXT)
      on delete restrict on update restrict;

alter table RM_BEST_PRACTICE
   add constraint FK_RM_BEST__FKSECTION_RM_SECTI foreign key (FKSECTIONBESTPRACTICE)
      references RM_SECTION_BEST_PRACTICE (FKCONTEXT)
      on delete cascade on update restrict;

alter table RM_BEST_PRACTICE_EVENT
   add constraint FK_RM_BEST__FKBESTPRA_RM_BEST_2 foreign key (FKBESTPRACTICE)
      references RM_BEST_PRACTICE (FKCONTEXT)
      on delete cascade on update restrict;

alter table RM_BEST_PRACTICE_EVENT
   add constraint FK_RM_BEST__FKCONTEXT_ISMS_CON2 foreign key (FKCONTEXT)
      references ISMS_CONTEXT (PKCONTEXT)
      on delete restrict on update restrict;

alter table RM_BEST_PRACTICE_EVENT
   add constraint FK_RM_BEST__FKEVENT_RM_EVENT foreign key (FKEVENT)
      references RM_EVENT (FKCONTEXT)
      on delete cascade on update restrict;

alter table RM_BEST_PRACTICE_STANDARD
   add constraint FK_RM_BEST__FKBESTPRA_RM_BEST_ foreign key (FKBESTPRACTICE)
      references RM_BEST_PRACTICE (FKCONTEXT)
      on delete cascade on update restrict;

alter table RM_BEST_PRACTICE_STANDARD
   add constraint FK_RM_BEST__FKCONTEXT_ISMS_CON foreign key (FKCONTEXT)
      references ISMS_CONTEXT (PKCONTEXT)
      on delete restrict on update restrict;

alter table RM_BEST_PRACTICE_STANDARD
   add constraint FK_RM_BEST__FKSTANDAR_RM_STAND foreign key (FKSTANDARD)
      references RM_STANDARD (FKCONTEXT)
      on delete cascade on update restrict;

alter table RM_CATEGORY
   add constraint FK_RM_CATEG_FKCONTEXT_ISMS_CON foreign key (FKCONTEXT)
      references ISMS_CONTEXT (PKCONTEXT)
      on delete restrict on update restrict;

alter table RM_CATEGORY
   add constraint FK_RM_CATEG_FKPARENT_RM_CATEG foreign key (FKPARENT)
      references RM_CATEGORY (FKCONTEXT)
      on delete restrict on update restrict;

alter table RM_CONTROL
   add constraint FK_RM_CONTR_FKCONTEXT_ISMS_CON2 foreign key (FKCONTEXT)
      references ISMS_CONTEXT (PKCONTEXT)
      on delete restrict on update restrict;

alter table RM_CONTROL
   add constraint FK_RM_CONTR_FKRESPONS_ISMS_USE foreign key (FKRESPONSIBLE)
      references ISMS_USER (FKCONTEXT)
      on delete restrict on update restrict;

alter table RM_CONTROL
   add constraint FK_RM_CONTR_FKTYPE_ISMS_CON foreign key (FKTYPE)
      references ISMS_CONTEXT_CLASSIFICATION (PKCLASSIFICATION)
      on delete restrict on update restrict;

alter table RM_CONTROL_BEST_PRACTICE
   add constraint FK_RM_CONTR_FKBESTPRA_RM_BEST_ foreign key (FKBESTPRACTICE)
      references RM_BEST_PRACTICE (FKCONTEXT)
      on delete cascade on update restrict;

alter table RM_CONTROL_BEST_PRACTICE
   add constraint FK_RM_CONTR_FKCONTEXT_ISMS_CON foreign key (FKCONTEXT)
      references ISMS_CONTEXT (PKCONTEXT)
      on delete restrict on update restrict;

alter table RM_CONTROL_BEST_PRACTICE
   add constraint FK_RM_CONTR_FKCONTROL_RM_CONTR foreign key (FKCONTROL)
      references RM_CONTROL (FKCONTEXT)
      on delete cascade on update restrict;

alter table RM_CONTROL_COST
   add constraint FK_RM_CONTR_FKCONTROL_RM_CONTR4 foreign key (FKCONTROL)
      references RM_CONTROL (FKCONTEXT)
      on delete cascade on update restrict;

alter table RM_CONTROL_EFFICIENCY_HISTORY
   add constraint FK_RM_CONTR_FKCONTROL_RM_CONTR2 foreign key (FKCONTROL)
      references RM_CONTROL (FKCONTEXT)
      on delete cascade on update restrict;

alter table RM_CONTROL_TEST_HISTORY
   add constraint FK_RM_CONTR_FKCONTROL_RM_CONTR3 foreign key (FKCONTROL)
      references RM_CONTROL (FKCONTEXT)
      on delete cascade on update restrict;

alter table RM_EVENT
   add constraint FK_RM_EVENT_FKCATEGOR_RM_CATEG foreign key (FKCATEGORY)
      references RM_CATEGORY (FKCONTEXT)
      on delete cascade on update restrict;

alter table RM_EVENT
   add constraint FK_RM_EVENT_FKCONTEXT_ISMS_CON foreign key (FKCONTEXT)
      references ISMS_CONTEXT (PKCONTEXT)
      on delete restrict on update restrict;

alter table RM_EVENT
   add constraint FK_RM_EVENT_FKTYPE_ISMS_CON foreign key (FKTYPE)
      references ISMS_CONTEXT_CLASSIFICATION (PKCLASSIFICATION)
      on delete restrict on update restrict;

alter table RM_PROCESS
   add constraint FK_RM_PROCE_FKAREA_RM_AREA foreign key (FKAREA)
      references RM_AREA (FKCONTEXT)
      on delete cascade on update restrict;

alter table RM_PROCESS
   add constraint FK_RM_PROCE_FKCONTEXT_ISMS_CON2 foreign key (FKCONTEXT)
      references ISMS_CONTEXT (PKCONTEXT)
      on delete restrict on update restrict;

alter table RM_PROCESS
   add constraint FK_RM_PROCE_FKPRIORIT_ISMS_CON foreign key (FKPRIORITY)
      references ISMS_CONTEXT_CLASSIFICATION (PKCLASSIFICATION)
      on delete restrict on update restrict;

alter table RM_PROCESS
   add constraint FK_RM_PROCE_FKRESPONS_ISMS_USE foreign key (FKRESPONSIBLE)
      references ISMS_USER (FKCONTEXT)
      on delete restrict on update restrict;

alter table RM_PROCESS
   add constraint FK_RM_PROCE_FKTYPE_ISMS_CON foreign key (FKTYPE)
      references ISMS_CONTEXT_CLASSIFICATION (PKCLASSIFICATION)
      on delete restrict on update restrict;

alter table RM_PROCESS_ASSET
   add constraint FK_RM_PROCE_FKASSET_RM_ASSET foreign key (FKASSET)
      references RM_ASSET (FKCONTEXT)
      on delete cascade on update restrict;

alter table RM_PROCESS_ASSET
   add constraint FK_RM_PROCE_FKCONTEXT_ISMS_CON foreign key (FKCONTEXT)
      references ISMS_CONTEXT (PKCONTEXT)
      on delete restrict on update restrict;

alter table RM_PROCESS_ASSET
   add constraint FK_RM_PROCE_FKPROCESS_RM_PROCE foreign key (FKPROCESS)
      references RM_PROCESS (FKCONTEXT)
      on delete cascade on update restrict;

alter table RM_RISK
   add constraint FK_RM_RISK_FKASSET_RM_ASSET foreign key (FKASSET)
      references RM_ASSET (FKCONTEXT)
      on delete cascade on update restrict;

alter table RM_RISK
   add constraint FK_RM_RISK_FKCONTEXT_ISMS_CON foreign key (FKCONTEXT)
      references ISMS_CONTEXT (PKCONTEXT)
      on delete restrict on update restrict;

alter table RM_RISK
   add constraint FK_RM_RISK_FKEVENT_RM_EVENT foreign key (FKEVENT)
      references RM_EVENT (FKCONTEXT)
      on delete set null on update restrict;

alter table RM_RISK
   add constraint FK_RM_RISK_FKPROBABI_RM_PARAM foreign key (FKPROBABILITYVALUENAME)
      references RM_PARAMETER_VALUE_NAME (PKVALUENAME)
      on delete restrict on update restrict;

alter table RM_RISK
   add constraint FK_RM_RISK_FKTYPE_ISMS_CON foreign key (FKTYPE)
      references ISMS_CONTEXT_CLASSIFICATION (PKCLASSIFICATION)
      on delete restrict on update restrict;

alter table RM_RISK_CONTROL
   add constraint FK_RM_RISK__FKCONTEXT_ISMS_CON foreign key (FKCONTEXT)
      references ISMS_CONTEXT (PKCONTEXT)
      on delete restrict on update restrict;

alter table RM_RISK_CONTROL
   add constraint FK_RM_RISK__FKCONTROL_RM_CONTR foreign key (FKCONTROL)
      references RM_CONTROL (FKCONTEXT)
      on delete cascade on update restrict;

alter table RM_RISK_CONTROL
   add constraint FK_RM_RISK__FKPROBABI_RM_RC_PA foreign key (FKPROBABILITYVALUENAME)
      references RM_RC_PARAMETER_VALUE_NAME (PKRCVALUENAME)
      on delete restrict on update restrict;

alter table RM_RISK_CONTROL
   add constraint FK_RM_RISK__FKRISK_RM_RISK foreign key (FKRISK)
      references RM_RISK (FKCONTEXT)
      on delete cascade on update restrict;

alter table RM_RISK_CONTROL_VALUE
   add constraint FK_RM_RISK__FKPARAMET_RM_PARAM foreign key (FKPARAMETERNAME)
      references RM_PARAMETER_NAME (PKPARAMETERNAME)
      on delete cascade on update restrict;

alter table RM_RISK_CONTROL_VALUE
   add constraint FK_RM_RISK__FKRCVALUE_RM_RC_PA foreign key (FKRCVALUENAME)
      references RM_RC_PARAMETER_VALUE_NAME (PKRCVALUENAME)
      on delete cascade on update restrict;

alter table RM_RISK_CONTROL_VALUE
   add constraint FK_RM_RISK__FKRISKCON_RM_RISK_ foreign key (FKRISKCONTROL)
      references RM_RISK_CONTROL (FKCONTEXT)
      on delete cascade on update restrict;

alter table RM_RISK_LIMITS
   add constraint FK_RM_RISK__FKCONTEXT_ISMS_CON2 foreign key (FKCONTEXT)
      references ISMS_CONTEXT (PKCONTEXT)
      on delete restrict on update restrict;

alter table RM_RISK_VALUE
   add constraint FK_RM_RISK__FKPARAMET_RM_PARAM2 foreign key (FKPARAMETERNAME)
      references RM_PARAMETER_NAME (PKPARAMETERNAME)
      on delete cascade on update restrict;

alter table RM_RISK_VALUE
   add constraint FK_RM_RISK__FKRISK_RM_RISK2 foreign key (FKRISK)
      references RM_RISK (FKCONTEXT)
      on delete cascade on update restrict;

alter table RM_RISK_VALUE
   add constraint FK_RM_RISK__FKVALUENA_RM_PARAM foreign key (FKVALUENAME)
      references RM_PARAMETER_VALUE_NAME (PKVALUENAME)
      on delete cascade on update restrict;

alter table RM_SECTION_BEST_PRACTICE
   add constraint FK_RM_SECTI_FKCONTEXT_ISMS_CON foreign key (FKCONTEXT)
      references ISMS_CONTEXT (PKCONTEXT)
      on delete restrict on update restrict;

alter table RM_SECTION_BEST_PRACTICE
   add constraint FK_RM_SECTI_FKPARENT_RM_SECTI foreign key (FKPARENT)
      references RM_SECTION_BEST_PRACTICE (FKCONTEXT)
      on delete restrict on update restrict;

alter table RM_STANDARD
   add constraint FK_RM_STAND_FKCONTEXT_ISMS_CON foreign key (FKCONTEXT)
      references ISMS_CONTEXT (PKCONTEXT)
      on delete restrict on update restrict;

alter table WKF_ALERT
   add constraint FK_WKF_ALER_FKCONTEXT_ISMS_CON foreign key (FKCONTEXT)
      references ISMS_CONTEXT (PKCONTEXT)
      on delete cascade on update restrict;

alter table WKF_ALERT
   add constraint FK_WKF_ALER_FKCREATOR_ISMS_USE foreign key (FKCREATOR)
      references ISMS_USER (FKCONTEXT)
      on delete restrict on update restrict;

alter table WKF_ALERT
   add constraint FK_WKF_ALER_FKRECEIVE_ISMS_USE foreign key (FKRECEIVER)
      references ISMS_USER (FKCONTEXT)
      on delete cascade on update restrict;

alter table WKF_CONTROL_EFFICIENCY
   add constraint FK_WKF_CONT_FKCONTROL_RM_CONTR2 foreign key (FKCONTROLEFFICIENCY)
      references RM_CONTROL (FKCONTEXT)
      on delete cascade on update restrict;

alter table WKF_CONTROL_EFFICIENCY
   add constraint FK_WKF_CONT_FKSCHEDUL_WKF_SCHE foreign key (FKSCHEDULE)
      references WKF_SCHEDULE (PKSCHEDULE)
      on delete cascade on update restrict;

alter table WKF_CONTROL_TEST
   add constraint FK_WKF_CONT_FKCONTROL_RM_CONTR foreign key (FKCONTROLTEST)
      references RM_CONTROL (FKCONTEXT)
      on delete cascade on update restrict;

alter table WKF_CONTROL_TEST
   add constraint FK_WKF_CONT_FKSCHEDUL_WKF_SCH2 foreign key (FKSCHEDULE)
      references WKF_SCHEDULE (PKSCHEDULE)
      on delete cascade on update restrict;

alter table WKF_TASK
   add constraint FK_WKF_TASK_FKCONTEXT_ISMS_CON foreign key (FKCONTEXT)
      references ISMS_CONTEXT (PKCONTEXT)
      on delete cascade on update restrict;

alter table WKF_TASK
   add constraint FK_WKF_TASK_FKCREATOR_ISMS_USE foreign key (FKCREATOR)
      references ISMS_USER (FKCONTEXT)
      on delete restrict on update restrict;

alter table WKF_TASK
   add constraint FK_WKF_TASK_FKRECEIVE_ISMS_USE foreign key (FKRECEIVER)
      references ISMS_USER (FKCONTEXT)
      on delete cascade on update restrict;

alter table WKF_TASK_SCHEDULE
   add constraint FK_WKF_TASK_FKCONTEXT_ISMS_CO2 foreign key (FKCONTEXT)
      references ISMS_CONTEXT (PKCONTEXT)
      on delete cascade on update restrict;

alter table WKF_TASK_SCHEDULE
   add constraint FK_WKF_TASK_FKSCHEDUL_WKF_SCHE foreign key (FKSCHEDULE)
      references WKF_SCHEDULE (PKSCHEDULE)
      on delete cascade on update restrict;