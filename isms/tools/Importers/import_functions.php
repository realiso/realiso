<?php
/**
 * 
 * @param $dbFrom - db origem
 * @param $dbTo - db destino
 * @param $table - tabela a importar
 * @param $order - ordem dos campos - array contendo i para numero e s para string ou data - necess�rio para saber se usa aspas
 * @param $limit - limite de resultados a importar
 * @return unknown_type
 */
function importWithNoChange($dbFrom, $dbTo, $table, $order){
	$sql = "select * from $table;";
	$dbFrom->execute($sql, LIMIT);
	$results = $dbFrom->fetchAll();
	$sql = '';
	foreach($results as $result){
		$values = '';
		for ($x = 0; isset($result[$x]); $x++){
			if ($order[$x] == 's') $values .= "'";
			$values .= $result[$x];
			if ($order[$x] == 's') $values .= "'";
			if (isset($result[$x+1])) $values .= ',';
		}  
		$sql .= "insert into $table values ($values);";
	}
	print_r($sql);
	$dbTo->execute($sql, 0, 0, true);
}

/**
 * 
 * @param $dbFrom - db origem
 * @param $dbTo - db destino
 * @param $query - query a ser executada no select
 * @param $table - tabela a importar
 * @param $order - ordem dos campos - array contendo i para numero e s para string ou data - necess�rio para saber se usa aspas
 * @param $limit - limite de resultados a importar
 * @return unknown_type
 */
function importByQuery($dbFrom, $dbTo, $query, $table, $order){
	echo "importando pela query";
	$sql = $query;
	$dbFrom->execute($sql, LIMIT);
	$results = $dbFrom->fetchAll();
	echo "tenho" . count($results) . " resultados";
	$sql = '';
	foreach($results as $result){
		$values = '';
		for ($x = 0; isset($result[$x]); $x++){
			if ($order[$x] == 's') $values .= "'";
			$values .= $result[$x];
			if ($order[$x] == 's') $values .= "'";
			if (isset($result[$x+1])) $values .= ',';
		}  
		$sql .= "insert into $table values ($values);";
	}
	if ($sql) $dbTo->execute($sql, 0, 0, true);
}

function importACL($target){
	$query = "DROP TABLE isms_profile_acl;

CREATE TABLE isms_profile_acl
(
  fkprofile integer NOT NULL,
  stag character varying(256) NOT NULL,
  CONSTRAINT pk_isms_profile_acl PRIMARY KEY (fkprofile, stag),
  CONSTRAINT fk_isms_pro_fkprofile_isms_pro FOREIGN KEY (fkprofile)
      REFERENCES isms_profile (fkcontext) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE CASCADE
)
WITH (OIDS=TRUE);
ALTER TABLE isms_profile_acl OWNER TO \"isms\";

CREATE INDEX fkprofile_fk2
  ON isms_profile_acl
  USING btree
  (fkprofile);

CREATE UNIQUE INDEX isms_profile_acl_pk
  ON isms_profile_acl
  USING btree
  (fkprofile, stag);
";
	$target->execute($query, 0, 0, true);
	$target->execute(file_get_contents('isms_postgres_profile_acl.sql'), 0, 0, true);
}


function importProfiles($source, $target){
	// fucapi 
	/*
	$query = 
	"
	UPDATE isms_profile set nid = 8801 where fkcontext = 1;
	UPDATE isms_context set nstate = 2700 where pkcontext = 1;
	INSERT INTO isms_context values (2,2817,2700);
	INSERT INTO isms_context values (3,2817,2700);
	INSERT INTO isms_context values (4,2817,2700);
	INSERT INTO isms_context values (5,2817,2700);
	INSERT INTO isms_profile values (2,'Manager',8802);
	INSERT INTO isms_profile values (3,'Administrator',8803);
	INSERT INTO isms_profile values (4,'Without Permissions',8804);
	INSERT INTO isms_profile values (5,'Document Reader',8805);
	UPDATE isms_user set fkprofile = 4 where fkprofile = 10;
	UPDATE isms_user set fkprofile = 3 where fkprofile = 15;
	UPDATE isms_user set fkprofile = 2 where fkprofile = 22;
	DELETE from isms_profile where fkcontext > 5;
	";  */
	// boreal
	$query = "
	UPDATE isms_profile set nid = 8801 where fkcontext = 1;
	UPDATE isms_profile set nid = 8802 where fkcontext = 2;
	UPDATE isms_profile set nid = 8803 where fkcontext = 3;
	UPDATE isms_profile set nid = 8804 where fkcontext = 4;
	UPDATE isms_profile set nid = 8805 where fkcontext = 5;
	";
	$target->execute($query, 0, 0, true);
}

function updateUsers($target){
	$sql = "update isms_user set bisblocked = 0, 
		bmustchangepassword = 0,
  		nwronglogonattempts = 0,
  		srequestpassword = '',
  		ssession = '',
  		bansweredsurvey = 0,
  		bshowhelp = 1,
  		nlanguage = 3301;";
	$target->execute($sql, 0, 0, true);
}

function importControlEfficiency($source,$target){
	$target->execute("TRUNCATE TABLE wkf_control_efficiency;");
	$query = "SELECT fkcontrolefficiency, nrealefficiency, nexpectedefficiency, 'null', smetric1, smetric2, smetric3, smetric4, smetric5, null" .
	 " from wkf_control_efficiency ORDER BY fkcontrolefficiency;";
	importByQuery($source, $target, $query, 'wkf_control_efficiency', array('i','i','i','i','s','s','s','s','s','s'));
}

function setConfig($target, $mail, $configPath){
	$query = "insert into isms_config values(426,'1');
insert into isms_config values(427,'');
insert into isms_config values(428,'');
insert into isms_config values(429,'1');
insert into isms_config values(430,'Realiso Team');
insert into isms_config values(431,'CURR_REAL');
insert into isms_config values(806,'13');
insert into isms_config values(807,'13');
insert into isms_config values(808,'13');
insert into isms_config values(809,'13');
INSERT INTO isms_config (pkConfig, sValue) VALUES (5701, '8301');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7205, '3');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7206, '365');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7207, '5');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7208, '7');
insert into isms_config values(7209,'support@realiso.com');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7404, '0');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7500, '0');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7501, '');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7502, '');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7503, '');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7504, '');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7510, '');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7511, '');
INSERT INTO isms_config (pkconfig, svalue) VALUES (7512, '');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711701, 'support@realiso.com');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711703, 'fucapi');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711704, 'RM|PM|CI');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711705, '1');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711706, '0');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711707, '1');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711708, '2601323a346352ccb455be7a36657825');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711709, '1');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711710, '5802');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711712, '1231513150');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711713, '$mail');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711714, '$configPath');
INSERT INTO isms_saas (pkconfig, svalue) VALUES (711715, '5242880');";
	$target->execute($query, 0, 0, true);
}
$GLOBALS['master'] = Array();
$GLOBALS['removeCount'] = 0;
function desfazNosAssetAsset($target){
	$query = 'select * from view_rm_asset_asset_active;';
	$target->execute($query);
	$results = $target->fetchAll();
	foreach($results as $result){
		searchLinks($target, $result[0]);
		$GLOBALS['master'] = Array();
	}
	return $GLOBALS['removeCount'];
}
function searchLinks($target, $fkAsset){
	$GLOBALS['master'][] = $fkAsset;
	$query = "select * from view_rm_asset_asset_active where fkdependent = " . $fkAsset .";";
	$target->execute($query);
	$rs = $target->fetchAll();
	foreach($rs as $r){
		if (in_array($r[0], $GLOBALS['master'])){
			$query = "delete from rm_asset_asset where fkasset = ". $r[0] . " and fkdependent = " . $r[1] . ";";
			$target->execute($query);
			echo "loop no asset fkasset: " . $r[0] . "\n<BR>Imprimindo stack:<BR />\n";
			print_r($GLOBALS['master']);
			echo "\n<BR />";
			$GLOBALS['removeCount']++;
		} else {
			searchLinks($target, $r[0]);
		}
	}
	array_pop($GLOBALS['master']);
}

function fucapi_fix_risk_values (){
	$query = "  Update rm_risk_value SET fkvaluename = 1 where fkvaluename = 6;
				Update rm_risk_value SET fkvaluename = 2 where fkvaluename = 7;
				Update rm_risk_value SET fkvaluename = 3 where fkvaluename = 8;

				Update rm_asset_value SET fkvaluename = 1 where fkvaluename = 6;
				Update rm_asset_value SET fkvaluename = 2 where fkvaluename = 7;
				Update rm_asset_value SET fkvaluename = 3 where fkvaluename = 8;
				
				Update rm_risk SET fkProbabilityValueName = 1 where fkProbabilityValueName = 6;
				Update rm_risk SET fkProbabilityValueName = 2 where fkProbabilityValueName = 7;
				Update rm_risk SET fkProbabilityValueName = 3 where fkProbabilityValueName = 8;
				
				Update rm_risk_control SET fkProbabilityValueName = 1 where fkProbabilityValueName = 6;
				Update rm_risk_control SET fkProbabilityValueName = 2 where fkProbabilityValueName = 7;
				Update rm_risk_control SET fkProbabilityValueName = 3 where fkProbabilityValueName = 8;
				
				update rm_parameter_value_name set simportance = 'Baixa', simpact = 'Baixo', sriskprobability = 'Improv�vel' where pkvaluename = 1;
				update rm_parameter_value_name set simportance = 'M�dia', simpact = 'M�dio', sriskprobability = 'Poss�vel' where pkvaluename = 2;  
				update rm_parameter_value_name set simportance = 'Alta', simpact = 'Alto', sriskprobability = 'Prov�vel' where pkvaluename = 3;
				
				delete from rm_parameter_value_name where pkvaluename > 3;
				";
}
?>