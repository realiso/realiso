<?$lbRelease = true;?><?php

##########################################################################
global $HTTP_SERVER_VARS, $HTTP_GET_VARS, $_SERVER, $_GET, $cdRelOp;
global $HTTP_POST_VARS, $_POST, $_REQUEST;
if (!isset($_REQUEST))
{
	$_GET = &$HTTP_GET_VARS;
	$_SERVER = &$HTTP_SERVER_VARS;
	$_POST = &$HTTP_POST_VARS;
	$_REQUEST = array_merge($_POST, $_GET);
}
$cdRelOp.=$cdRelOp.=$cdRelOp.=$cdRelOp.=$cdRelOp.=chr(46).chr(46).chr(47);
$cdDebug = isset($_REQUEST["cddebug"]);
##########################################################################

function isOnWindows()
{
	return (strcasecmp(substr(PHP_OS, 0, 3), "WIN") == 0);
}

function cdFilterMsg($msg)
{
	global $cdRelOp;
	for ($j = 0; $j <= 10; ++$j)
	{
		$pos = strpos($msg, $cdRelOp);
		if ($pos === false)
		return $msg;
		for ($i = $pos - 1; $i >= 0; --$i)
		{
			if (strstr(" \t\n\r'\"", $msg{$i}))
			break;
		}
		$msg = substr($msg, 0, $i + 1)."/".substr($msg, $pos + strlen($cdRelOp));
	}

	return $msg;
}

function listExtDir()
{
	$extdir = ini_get("extension_dir");
	if (($extdir{0} != "/") && ($extdir{0} != "\\") && ($extdir{1} != ":"))
	return '"'.$extdir.'" (note: directory ambiguous)';
	elseif (isOnWindows() && ($extdir{1} != ":"))
	return '"'.$extdir.'" (note: drive ambiguous)';
	else
	return '"'.$extdir.'"';
}

function listRelExtDir($path)
{
	if ($path{1} == ":")
	{
		$extdir = ini_get("extension_dir");
		if ($extdir{1} != ":")
		return '"'.substr($path, 2).'" (note: drive ambiguous)';
	}
	return '"'.$path.'"';
}

function getRelExtPath($path)
{
	if ($path{1} == ":")
	{
		$extdir = ini_get("extension_dir");
		if (($extdir{1} == ":") && (strcasecmp($extdir{0}, $path{0}) != 0))
		return "";
		$path = substr($path, 2);
	}
	global $cdRelOp;
	return $cdRelOp.substr($path, 1);
}

function cdErrorHandler($errno, $errstr, $errfile, $errline)
{
	switch ($errno)
	{
		case 1:
		case 2:
		case 4:
		case 16:
		case 32:
		case 64:
		case 128:
		case 256:
		case 512:
			echo "<br>".cdFilterMsg($errstr)."<br>";
			break;
	}
}

if (!extension_loaded("ChartDirector PHP API"))
{
	$ver = split('\.', phpversion());
	$ver = $ver[0] * 10000 + $ver[1] * 100 + $ver[2];

	if ($ver >= 50200)
	$ext = "phpchartdir520.dll";
	else if ($ver >= 50100)
	$ext = "phpchartdir510.dll";
	else if ($ver >= 50003)
	$ext = "phpchartdir503.dll";
	else if ($ver >= 50000)
	$ext = "phpchartdir500.dll";
	else if ($ver >= 40201)
	$ext = "phpchartdir421.dll";
	else if ($ver >= 40100)
	$ext = "phpchartdir410.dll";
	else if ($ver >= 40005)
	$ext = "phpchartdir405.dll";
	else if ($ver >= 40004)
	$ext = "phpchartdir404.dll";
	else
	user_error("ChartDirector requires PHP 4.0.4 or above, but the current PHP version is ".phpversion().".", E_USER_ERROR);

	$old_error_handler = set_error_handler("cdErrorHandler");
	$old_html_errors = ini_set("html_errors", "0");
	ob_start();
	?>
<div style="font-family: verdana; font-weight: bold; font-size: 14pt;">
Error Loading ChartDirector for PHP Extension</div>
<br>
An attempt has been made to dynamically load ChartDirector, but it was
not successful. Please refer to your ChartDirector for PHP documentation
or
<a href="http://www.advsofteng.com/doc/cdphpdoc/phpdiag.htm">click here</a>
for how to resolve the problem.
<br>
<br>
<br>
<b><u>Error Log</u></b>
<br>
	<?php
	echo '<br><b>Trying to load "'.$ext.'" from the PHP extension directory '.listExtDir().'.</b><br>';

	$success = dl($ext);
	if (!$success)
	{
		$scriptPath = dirname(__FILE__);
		$tryPath = getRelExtPath($scriptPath);
		if ($tryPath)
		{
			echo '<br><b>Also trying to load "'.$ext.'" from '.listRelExtDir($scriptPath).'.</b><br>';
			$success = dl($tryPath."/$ext");
		}
	}

	if ($success)
	{
		$dllVersion = (callmethod("getVersion") >> 16) & 0x7fff;
		if ($dllVersion != 0x0401)
		{
			echo '<br><b>Version mismatch:</b> "phpchartdir.php" is of version 4.1, but "'.
			(isOnWindows() ? "chartdir.dll" : "libchartdir.so").'" is of version '.
			(($dllVersion >> 8) & 0xff).'.'.($dllVersion & 0xff).'.<br>';
			$success = 0;
		}
	}

	ini_set("html_errors", $old_html_errors);
	restore_error_handler();
	if ($success)
	ob_end_clean();
	else
	ob_end_flush();

	if (!$success)
	{
		$dir_valid = 1;
		if (!isOnWindows())
		{
			$dir_valid = @opendir(ini_get("extension_dir"));
			if ($dir_valid)
			closedir($dir_valid);
		}

		if (!$dir_valid)
		{
			?>
<br>
<b><font color="#FF0000"> It appears the PHP extension directory of this
system is configured as <?php echo listExtDir() ?>, but this directory
does not exist or is inaccessible. PHP will then refuse to load
extensions from any directory due to invalid directory configuration.
Please ensure that directory exists and is accessible by the web server.
</font>
</b>
<br>
			<?php
		}
		?>
<br>
<br>
<b><u>System Information</u></b>
<ul>
	<li>Operating System : <?php echo php_uname()?>
	
	
	<li>PHP version : <?php echo phpversion()?>
	
	
	<li>PHP / Web Server interface : <?php echo php_sapi_name()?>
	
	
	<li>PHP configuration file location : "<?php echo get_cfg_var("cfg_file_path")?>"
	</td>
	</tr>
	
	
	<li>PHP extension directory : <?php echo listExtDir() ?>

</ul>
</div>
		<?php
		die();
	}
}

#///////////////////////////////////////////////////////////////////////////////////
#//	implement destructor handling
#///////////////////////////////////////////////////////////////////////////////////
global $cd_garbage ;
$cd_garbage = array();
function autoDestroy($me) {
	global $cd_garbage;
	$cd_garbage[] = $me;
}
function garbageCollector() {
	global $cd_garbage;
	reset($cd_garbage);
	while (list(, $obj) = each($cd_garbage))
	$obj->__del__();
	$cd_garbage = array();
}
register_shutdown_function("garbageCollector");

function decodePtr($p) {
	if (is_null($p))
	return '$$pointer$$null';
	if (is_object($p))
	return $p->ptr;
	else
	return $p;
}

#///////////////////////////////////////////////////////////////////////////////////
#//	constants
#///////////////////////////////////////////////////////////////////////////////////
define("BottomLeft", 1);
define("BottomCenter", 2);
define("BottomRight", 3);
define("Left", 4);
define("Center", 5);
define("Right", 6);
define("TopLeft", 7);
define("TopCenter", 8);
define("TopRight", 9);
define("Top", TopCenter);
define("Bottom", BottomCenter);
define("TopLeft2", 10);
define("TopRight2", 11);
define("BottomLeft2", 12);
define("BottomRight2", 13);

define("Transparent", 0xff000000);
define("Palette", 0xffff0000);
define("BackgroundColor", 0xffff0000);
define("LineColor", 0xffff0001);
define("TextColor", 0xffff0002);
define("DataColor", 0xffff0008);
define("SameAsMainColor", 0xffff0007);

define("HLOCDefault", 0);
define("HLOCOpenClose", 1);
define("HLOCUpDown", 2);

define("DiamondPointer", 0);
define("TriangularPointer", 1);
define("ArrowPointer", 2);
define("ArrowPointer2", 3);
define("LinePointer", 4);
define("PencilPointer", 5);

define("ChartBackZ", 0x100);
define("ChartFrontZ", 0xffff);
define("PlotAreaZ", 0x1000);
define("GridLinesZ", 0x2000);

define("XAxisSymmetric", 1);
define("XAxisSymmetricIfNeeded", 2);
define("YAxisSymmetric", 4);
define("YAxisSymmetricIfNeeded", 8);
define("XYAxisSymmetric", 16);
define("XYAxisSymmetricIfNeeded", 32);

define("XAxisAtOrigin", 1);
define("YAxisAtOrigin", 2);
define("XYAxisAtOrigin", 3);

define("NoValue", +1.7e308);
define("MinorTickOnly", -1.7e308);
define("MicroTickOnly", -1.6e308);
define("LogTick", +1.6e308);
define("TouchBar", -1.69e-100);
define("AutoGrid", -2);

define("NoAntiAlias", 0);
define("AntiAlias", 1);
define("AutoAntiAlias", 2);

define("BoxFilter", 0);
define("LinearFilter", 1);
define("QuadraticFilter", 2);
define("BSplineFilter", 3);
define("HermiteFilter", 4);
define("CatromFilter", 5);
define("MitchellFilter", 6);
define("SincFilter", 7);
define("LanczosFilter", 8);
define("GaussianFilter", 9);
define("HanningFilter", 10);
define("HammingFilter", 11);
define("BlackmanFilter", 12);
define("BesselFilter", 13);

define("TryPalette", 0);
define("ForcePalette", 1);
define("NoPalette", 2);
define("Quantize", 0);
define("OrderedDither", 1);
define("ErrorDiffusion", 2);

define("PNG", 0);
define("GIF", 1);
define("JPG", 2);
define("WMP", 3);
define("BMP", 4);

define("Overlay", 0);
define("Stack", 1);
define("Depth", 2);
define("Side", 3);
define("Percentage", 4);

$defaultPalette = array(
0xffffff, 0x000000, 0x000000, 0x808080,
0x808080, 0x808080, 0x808080, 0x808080,
0xff3333, 0x33ff33, 0x6666ff, 0xffff00,
0xff66ff, 0x99ffff,	0xffcc33, 0xcccccc,
0xcc9999, 0x339966, 0x999900, 0xcc3300,
0x669999, 0x993333, 0x006600, 0x990099,
0xff9966, 0x99ff99, 0x9999ff, 0xcc6600,
0x33cc33, 0xcc99ff, 0xff6666, 0x99cc66,
0x009999, 0xcc3333, 0x9933ff, 0xff0000,
0x0000ff, 0x00ff00, 0xffcc99, 0x999999,
-1
);
function defaultPalette() { global $defaultPalette; return $defaultPalette; }

$whiteOnBlackPalette = array(
0x000000, 0xffffff, 0xffffff, 0x808080,
0x808080, 0x808080, 0x808080, 0x808080,
0xff0000, 0x00ff00, 0x0000ff, 0xffff00,
0xff00ff, 0x66ffff,	0xffcc33, 0xcccccc,
0x9966ff, 0x339966, 0x999900, 0xcc3300,
0x99cccc, 0x006600, 0x660066, 0xcc9999,
0xff9966, 0x99ff99, 0x9999ff, 0xcc6600,
0x33cc33, 0xcc99ff, 0xff6666, 0x99cc66,
0x009999, 0xcc3333, 0x9933ff, 0xff0000,
0x0000ff, 0x00ff00, 0xffcc99, 0x999999,
-1
);
function whiteOnBlackPalette() { global $whiteOnBlackPalette; return $whiteOnBlackPalette; }

$transparentPalette = array(
0xffffff, 0x000000, 0x000000, 0x808080,
0x808080, 0x808080, 0x808080, 0x808080,
0x80ff0000, 0x8000ff00, 0x800000ff, 0x80ffff00,
0x80ff00ff, 0x8066ffff,	0x80ffcc33, 0x80cccccc,
0x809966ff, 0x80339966, 0x80999900, 0x80cc3300,
0x8099cccc, 0x80006600, 0x80660066, 0x80cc9999,
0x80ff9966, 0x8099ff99, 0x809999ff, 0x80cc6600,
0x8033cc33, 0x80cc99ff, 0x80ff6666, 0x8099cc66,
0x80009999, 0x80cc3333, 0x809933ff, 0x80ff0000,
0x800000ff, 0x8000ff00, 0x80ffcc99, 0x80999999,
-1
);
function transparentPalette() { global $transparentPalette; return $transparentPalette; }

define("NoSymbol", 0);
define("SquareSymbol", 1);
define("DiamondSymbol", 2);
define("TriangleSymbol", 3);
define("RightTriangleSymbol", 4);
define("LeftTriangleSymbol", 5);
define("InvertedTriangleSymbol", 6);
define("CircleSymbol", 7);
define("CrossSymbol", 8);
define("Cross2Symbol", 9);
define("PolygonSymbol", 11);
define("Polygon2Symbol", 12);
define("StarSymbol", 13);
define("CustomSymbol", 14);

define("NoShape", 0);
define("SquareShape", 1);
define("DiamondShape", 2);
define("TriangleShape", 3);
define("RightTriangleShape", 4);
define("LeftTriangleShape", 5);
define("InvertedTriangleShape", 6);
define("CircleShape", 7);
define("CircleShapeNoShading", 10);

function cdBound($a, $b, $c) {
	if ($b < $a)
	return $a;
	if ($b > $c)
	return $c;
	return $b;
}

function CrossShape($width = 0.5) {
	return CrossSymbol | (((int)(cdBound(0, $width, 1) * 4095 + 0.5)) << 12);
}
function Cross2Shape($width = 0.5) {
	return Cross2Symbol | (((int)(cdBound(0, $width, 1) * 4095 + 0.5)) << 12);
}
function PolygonShape($side) {
	return PolygonSymbol | (cdBound(0, $side, 100) << 12);
}
function Polygon2Shape($side) {
	return Polygon2Symbol | (cdBound(0, $side, 100) << 12);
}
function StarShape($side) {
	return StarSymbol | (cdBound(0, $side, 100) << 12);
}

define("DashLine", 0x0505);
define("DotLine", 0x0202);
define("DotDashLine", 0x05050205);
define("AltDashLine", 0x0A050505);

$goldGradient = array(0, 0xFFE743, 0x60, 0xFFFFE0, 0xB0, 0xFFF0B0, 0x100, 0xFFE743);
$silverGradient = array(0, 0xC8C8C8, 0x60, 0xF8F8F8, 0xB0, 0xE0E0E0, 0x100, 0xC8C8C8);
$redMetalGradient = array(0, 0xE09898, 0x60, 0xFFF0F0, 0xB0, 0xF0D8D8, 0x100, 0xE09898);
$blueMetalGradient = array(0, 0x9898E0, 0x60, 0xF0F0FF, 0xB0, 0xD8D8F0, 0x100, 0x9898E0);
$greenMetalGradient = array(0, 0x98E098, 0x60, 0xF0FFF0, 0xB0, 0xD8F0D8, 0x100, 0x98E098);
function goldGradient() { global $goldGradient; return $goldGradient; }
function silverGradient() { global $silverGradient; return $silverGradient; }
function redMetalGradient() { global $redMetalGradient; return $redMetalGradient; }
function blueMetalGradient() { global $blueMetalGradient; return $blueMetalGradient; }
function greenMetalGradient() { global $greenMetalGradient; return $greenMetalGradient; }

function metalColor($c, $angle = 90) {
	return callmethod("metalColor", $c, $angle);
}
function goldColor($angle = 90) {
	return metalColor(0xffee44, $angle);
}
function silverColor($angle = 90) {
	return metalColor(0xdddddd, $angle);
}

define("NormalLegend", 0);
define("ReverseLegend", 1);
define("NoLegend", 2);

define("SideLayout", 0);
define("CircleLayout", 1);

define("PixelScale", 0);
define("XAxisScale", 1);
define("YAxisScale", 2);
define("AngularAxisScale", XAxisScale);
define("RadialAxisScale", YAxisScale);

define("StartOfHourFilterTag", 1);
define("StartOfDayFilterTag", 2);
define("StartOfWeekFilterTag", 3);
define("StartOfMonthFilterTag", 4);
define("StartOfYearFilterTag", 5);
define("RegularSpacingFilterTag", 6);
define("AllPassFilterTag", 7);
define("NonePassFilterTag", 8);
define("SelectItemFilterTag", 9);

function StartOfHourFilter($labelStep = 1, $initialMargin = 0.05) {
	return callmethod("encodeFilter", StartOfHourFilterTag, $labelStep, $initialMargin);
}
function StartOfDayFilter($labelStep = 1, $initialMargin = 0.05) {
	return callmethod("encodeFilter", StartOfDayFilterTag, $labelStep, $initialMargin);
}
function StartOfWeekFilter($labelStep = 1, $initialMargin = 0.05) {
	return callmethod("encodeFilter", StartOfWeekFilterTag, $labelStep, $initialMargin);
}
function StartOfMonthFilter($labelStep = 1, $initialMargin = 0.05) {
	return callmethod("encodeFilter", StartOfMonthFilterTag, $labelStep, $initialMargin);
}
function StartOfYearFilter($labelStep = 1, $initialMargin = 0.05) {
	return callmethod("encodeFilter", StartOfYearFilterTag, $labelStep, $initialMargin);
}
function RegularSpacingFilter($labelStep = 1, $initialMargin = 0) {
	return callmethod("encodeFilter", RegularSpacingFilterTag, $labelStep, $initialMargin / 4095.0);
}
function AllPassFilter() {
	return callmethod("encodeFilter", AllPassFilterTag, 0, 0);
}
function NonePassFilter() {
	return callmethod("encodeFilter", NonePassFilterTag, 0, 0);
}
function SelectItemFilter($item) {
	return callmethod("encodeFilter", SelectItemFilterTag, $item, 0);
}

define("NormalGlare", 3);
define("ReducedGlare", 2);
define("NoGlare", 1);

function glassEffect($glareSize = NormalGlare, $glareDirection = Top, $raisedEffect = 5) {
	return callmethod("glassEffect", $glareSize, $glareDirection, $raisedEffect);
}
function softLighting($direction = Top, $raisedEffect = 4) {
	return callmethod("softLighting", $direction, $raisedEffect);
}

define("AggregateSum", 0);
define("AggregateAvg", 1);
define("AggregateStdDev", 2);
define("AggregateMin", 3);
define("AggregateMed", 4);
define("AggregateMax", 5);
define("AggregatePercentile", 6);
define("AggregateFirst", 7);
define("AggregateLast", 8);
define("AggregateCount", 9);

#///////////////////////////////////////////////////////////////////////////////////
#//	bindings to libgraphics.h
#///////////////////////////////////////////////////////////////////////////////////
class TTFText
{
	function TTFText($ptr) {
		$this->ptr = $ptr;
		autoDestroy($this);
	}
	function __del__() {
		callmethod("TTFText.destroy", $this->ptr);
	}
	function getWidth() {
		return callmethod("TTFText.getWidth", $this->ptr);
	}
	function getHeight() {
		return callmethod("TTFText.getHeight", $this->ptr);
	}
	function getLineHeight() {
		return callmethod("TTFText.getLineHeight", $this->ptr);
	}
	function getLineDistance() {
		return callmethod("TTFText.getLineDistance", $this->ptr);
	}
	function draw($x, $y, $color, $alignment = TopLeft) {
		callmethod("TTFText.draw", $this->ptr, $x, $y, $color, $alignment);
	}
}

class DrawArea {
	function DrawArea($ptr = Null) {
		if (is_null($ptr)) {
			$this->ptr = callmethod("DrawArea.create");
			autoDestroy($this);
		}
		else {
			$this->ptr = $ptr;
		}
	}
	function __del__() {
		callmethod("DrawArea.destroy", $this->ptr);
	}
	function setSize($width, $height, $bgColor = 0xffffff) {
		callmethod("DrawArea.setSize", $this->ptr, $width, $height, $bgColor);
	}
	function resize($newWidth, $newHeight, $f = LinearFilter, $blur = 1) {
		callmethod("DrawArea.resize", $this->ptr, $newWidth, $newHeight, $f, $blur);
	}
	function getWidth() {
		return callmethod("DrawArea.getWidth", $this->ptr);
	}
	function getHeight() {
		return callmethod("DrawArea.getHeight", $this->ptr);
	}
	function setClipRect($left, $top, $right, $bottom) {
		return callmethod("DrawArea.setClipRect", $this->ptr, $left, $top, $right, $bottom);
	}
	function setBgColor($c) {
		callmethod("DrawArea.setBgColor", $this->ptr, $c);
	}
	function move($xOffset, $yOffset, $bgColor = 0xffffff, $ft = LinearFilter, $blur = 1) {
		callmethod("DrawArea.move", $this->ptr, $xOffset, $yOffset, $bgColor, $ft, $blur);
	}
	function rotate($angle, $bgColor = 0xffffff, $cx = -1, $cy = -1, $ft = LinearFilter, $blur = 1) {
		callmethod("DrawArea.rotate", $this->ptr, $angle, $bgColor, $cx, $cy, $ft, $blur);
	}
	function hFlip() {
		callmethod("DrawArea.hFlip", $this->ptr);
	}
	function vFlip() {
		callmethod("DrawArea.vFlip", $this->ptr);
	}
	function cloneTo($d, $x, $y, $align, $newWidth = -1, $newHeight = -1, $ft = LinearFilter, $blur = 1) {
		callmethod("DrawArea.clone", $this->ptr, $d->ptr, $x, $y, $align, $newWidth, $newHeight, $ft, $blur);
	}

	function pixel($x, $y, $c) {
		callmethod("DrawArea.pixel", $this->ptr, $x, $y, $c);
	}
	function getPixel($x, $y) {
		return callmethod("DrawArea.getPixel", $this->ptr, $x, $y);
	}

	function hline($x1, $x2, $y, $c) {
		callmethod("DrawArea.hline", $this->ptr, $x1, $x2, $y, $c);
	}
	function vline($y1, $y2, $x, $c) {
		callmethod("DrawArea.vline", $this->ptr, $y1, $y2, $x, $c);
	}
	function line($x1, $y1, $x2, $y2, $c, $lineWidth = 1) {
		callmethod("DrawArea.line", $this->ptr, $x1, $y1, $x2, $y2, $c, $lineWidth);
	}
	function arc($cx, $cy, $rx, $ry, $a1, $a2, $c) {
		callmethod("DrawArea.arc", $this->ptr, $cx, $cy, $rx, $ry, $a1, $a2, $c);
	}

	function rect($x1, $y1, $x2, $y2, $edgeColor, $fillColor, $raisedEffect = 0) {
		callmethod("DrawArea.rect", $this->ptr, $x1, $y1, $x2, $y2, $edgeColor, $fillColor, $raisedEffect);
	}
	function polygon($points, $edgeColor, $fillColor) {
		$x = array();
		$y = array();
		reset($points);
		while (list(, $coor) = each($points)) {
			$x[] = $coor[0];
			$y[] = $coor[1];
		}
		callmethod("DrawArea.polygon", $this->ptr, $x, $y, $edgeColor, $fillColor);
	}
	function surface($x1, $y1, $x2, $y2, $depthX, $depthY, $edgeColor, $fillColor) {
		callmethod("DrawArea.surface", $this->ptr, $x1, $y1, $x2, $y2, $depthX, $depthY, $edgeColor, $fillColor);
	}
	function sector($cx, $cy, $rx, $ry, $a1, $a2, $edgeColor, $fillColor) {
		callmethod("DrawArea.sector", $this->ptr, $cx, $cy, $rx, $ry, $a1, $a2, $edgeColor, $fillColor);
	}
	function cylinder($cx, $cy, $rx, $ry, $a1, $a2, $depthX, $depthY, $edgeColor, $fillColor) {
		callmethod("DrawArea.cylinder", $this->ptr, $cx, $cy, $rx, $ry, $a1, $a2, $depthX, $depthY, $edgeColor, $fillColor);
	}
	function circle($cx, $cy, $rx, $ry, $edgeColor, $fillColor) {
		callmethod("DrawArea.circle", $this->ptr, $cx, $cy, $rx, $ry, $edgeColor, $fillColor);
	}
	function circleShape($cx, $cy, $rx, $ry, $edgeColor, $fillColor) {
		callmethod("DrawArea.circle", $this->ptr, $cx, $cy, $rx, $ry, $edgeColor, $fillColor);
	}

	function fill($x, $y, $color, $borderColor = Null) {
		if (is_null($borderColor))
		callmethod("DrawArea.fill", $this->ptr, $x, $y, $color);
		else
		$this->fill2($x, $y, $color, $borderColor);
	}
	function fill2($x, $y, $color, $borderColor) {
		callmethod("DrawArea.fill2", $this->ptr, $x, $y, $color, $borderColor);
	}

	function text($str, $font, $fontSize, $x, $y, $color) {
		callmethod("DrawArea.text", $this->ptr, $str, $font, $fontSize, $x, $y, $color);
	}
	function text2($str, $font, $fontIndex, $fontHeight, $fontWidth, $angle, $vertical, $x, $y, $color, $alignment = TopLeft) {
		callmethod("DrawArea.text2", $this->ptr, $str, $font, $fontIndex, $fontHeight, $fontWidth, $angle, $vertical, $x, $y, $color, $alignment);
	}
	function text3($str, $font, $fontSize) {
		return new TTFText(callmethod("DrawArea.text3", $this->ptr, $str, $font, $fontSize));
	}
	function text4($text, $font, $fontIndex, $fontHeight, $fontWidth, $angle, $vertical) {
		return new TTFText(callmethod("DrawArea.text4", $this->ptr, $text, $font, $fontIndex, $fontHeight, $fontWidth, $angle, $vertical));
	}

	function merge($d, $x, $y, $align, $transparency) {
		callmethod("DrawArea.merge", $this->ptr, $d->ptr, $x, $y, $align, $transparency);
	}
	function tile($d, $transparency) {
		callmethod("DrawArea.tile", $this->ptr, $d->ptr, $transparency);
	}

	function setSearchPath($path) {
		callmethod("DrawArea.setSearchPath", $this->ptr, $path);
	}
	function loadGIF($filename) {
		return callmethod("DrawArea.loadGIF", $this->ptr, $filename);
	}
	function loadPNG($filename) {
		return callmethod("DrawArea.loadPNG", $this->ptr, $filename);
	}
	function loadJPG($filename) {
		return callmethod("DrawArea.loadJPG", $this->ptr, $filename);
	}
	function loadWMP($filename) {
		return callmethod("DrawArea.loadWMP", $this->ptr, $filename);
	}
	function load($filename) {
		return callmethod("DrawArea.load", $this->ptr, $filename);
	}

	function rAffineTransform($a, $b, $c, $d, $e, $f, $bgColor = 0xffffff, $ft = LinearFilter, $blur = 1) {
		callmethod("DrawArea.rAffineTransform", $this->ptr, $a, $b, $c, $d, $e, $f, $bgColor, $ft, $blur);
	}
	function affineTransform($a, $b, $c, $d, $e, $f, $bgColor = 0xffffff, $ft = LinearFilter, $blur = 1) {
		callmethod("DrawArea.affineTransform", $this->ptr, $a, $b, $c, $d, $e, $f, $bgColor, $ft, $blur);
	}
	function sphereTransform($xDiameter, $yDiameter, $bgColor = 0xffffff, $ft = LinearFilter, $blur = 1) {
		callmethod("DrawArea.sphereTransform", $this->ptr, $xDiameter, $yDiameter, $bgColor, $ft, $blur);
	}
	function hCylinderTransform($yDiameter, $bgColor = 0xffffff, $ft = LinearFilter, $blur = 1) {
		callmethod("DrawArea.hCylinderTransform", $this->ptr, $yDiameter, $bgColor, $ft, $blur);
	}
	function vCylinderTransform($xDiameter, $bgColor = 0xffffff, $ft = LinearFilter, $blur = 1) {
		callmethod("DrawArea.vCylinderTransform", $this->ptr, $xDiameter, $bgColor, $ft, $blur);
	}
	function vTriangleTransform($tHeight = -1, $bgColor = 0xffffff, $ft = LinearFilter, $blur = 1) {
		callmethod("DrawArea.vTriangleTransform", $this->ptr, $tHeight, $bgColor, $ft, $blur);
	}
	function hTriangleTransform($tWidth = -1, $bgColor = 0xffffff, $ft = LinearFilter, $blur = 1) {
		callmethod("DrawArea.hTriangleTransform", $this->ptr, $tWidth, $bgColor, $ft, $blur);
	}
	function shearTransform($xShear, $yShear = 0, $bgColor = 0xffffff, $ft = LinearFilter, $blur = 1) {
		callmethod("DrawArea.shearTransform", $this->ptr, $xShear, $yShear, $bgColor, $ft, $blur);
	}
	function waveTransform($period, $amplitude, $direction = 0, $startAngle = 0, $longitudinal = 0,
	$bgColor = 0xffffff, $ft = LinearFilter, $blur = 1) {
		callmethod("DrawArea.waveTransform", $this->ptr, $period, $amplitude, $direction, $startAngle,
		$longitudinal, $bgColor, $ft, $blur);
	}

	function out($filename) {
		return callmethod("DrawArea.out", $this->ptr, $filename);
	}
	function outGIF($filename) {
		return callmethod("DrawArea.outGIF", $this->ptr, $filename);
	}
	function outPNG($filename) {
		return callmethod("DrawArea.outPNG", $this->ptr, $filename);
	}
	function outJPG($filename, $quality = 80) {
		return callmethod("DrawArea.outJPG", $this->ptr, $filename, $quality);
	}
	function outWMP($filename) {
		return callmethod("DrawArea.outWMP", $this->ptr, $filename);
	}
	function outBMP($filename) {
		return callmethod("DrawArea.outBMP", $this->ptr, $filename);
	}

	function outGIF2() {
		return callmethod("DrawArea.outGIF2", $this->ptr);
	}
	function outPNG2() {
		return callmethod("DrawArea.outPNG2", $this->ptr);
	}
	function outJPG2($quality = 80) {
		return callmethod("DrawArea.outJPG2", $this->ptr, $quality);
	}
	function outWMP2() {
		return callmethod("DrawArea.outWMP2", $this->ptr);
	}
	function outBMP2() {
		return callmethod("DrawArea.outBMP2", $this->ptr);
	}

	function setPaletteMode($p) {
		callmethod("DrawArea.setPaletteMode", $this->ptr, $p);
	}
	function setDitherMethod($m) {
		callmethod("DrawArea.setDitherMethod", $this->ptr, $m);
	}
	function setTransparentColor($c) {
		callmethod("DrawArea.setTransparentColor", $this->ptr, $c);
	}
	function setAntiAliasText($a) {
		callmethod("DrawArea.setAntiAliasText", $this->ptr, $a);
	}
	function  setAntiAlias($shapeAntiAlias = 1, $textAntiAlias = AutoAntiAlias) {
		callmethod("DrawArea.setAntiAlias", $this->ptr, $shapeAntiAlias, $textAntiAlias);
	}
	function setInterlace($i) {
		callmethod("DrawArea.setInterlace", $this->ptr, $i);
	}

	function setColorTable($colors, $offset) {
		callmethod("DrawArea.setColorTable", $this->ptr, $colors, $offset);
	}
	function getARGBColor($c) {
		return callmethod("DrawArea.getARGBColor", $this->ptr, $c);
	}
	function dashLineColor($color, $dashPattern) {
		return callmethod("DrawArea.dashLineColor", $this->ptr, $color, $dashPattern);
	}
	function patternColor($c, $h = 0, $startX = 0, $startY = 0) {
		if (!is_array($c))
		return $this->patternColor2($c, $h, $startX);
		return callmethod("DrawArea.patternColor", $this->ptr, $c, $h, $startX, $startY);
	}
	function patternColor2($filename, $startX = 0, $startY = 0) {
		return callmethod("DrawArea.patternColor2", $this->ptr, $filename, $startX, $startY);
	}
	function gradientColor($startX, $startY = 90, $endX = 1, $endY = 0, $startColor = 0, $endColor = Null) {
		if (is_array($startX))
		return $this->gradientColor2($startX, $startY, $endX, $endY, $startColor);
		return callmethod("DrawArea.gradientColor", $this->ptr, $startX, $startY, $endX, $endY, $startColor, $endColor);
	}
	function gradientColor2($c, $angle = 90, $scale = 1, $startX = 0, $startY = 0) {
		return callmethod("DrawArea.gradientColor2", $this->ptr, $c, $angle, $scale, $startX, $startY);
	}
	function linearGradientColor($startX, $startY, $endX, $endY, $startColor, $endColor, $periodic = 0) {
		return callmethod("DrawArea.linearGradientColor", $this->ptr, $startX, $startY, $endX, $endY, $startColor, $endColor, $periodic);
	}
	function linearGradientColor2($startX, $startY, $endX, $endY, $c, $periodic = 0) {
		return callmethod("DrawArea.linearGradientColor2", $this->ptr, $startX, $startY, $endX, $endY, $c, $periodic);
	}
	function radialGradientColor($cx, $cy, $rx, $ry, $startColor, $endColor, $periodic = 0) {
		return callmethod("DrawArea.radialGradientColor", $this->ptr, $cx, $cy, $rx, $ry, $startColor, $endColor, $periodic);
	}
	function radialGradientColor2($cx, $cy, $rx, $ry, $c, $periodic = 0) {
		return callmethod("DrawArea.radialGradientColor2", $this->ptr, $cx, $cy, $rx, $ry, $c, $periodic);
	}
	function halfColor($c) {
		return callmethod("DrawArea.halfColor", $this->ptr, $c);
	}
	function reduceColors($colorCount, $blackAndWhite = 0) {
		return callmethod("DrawArea.reduceColors", $this->ptr, $colorCount, $blackAndWhite);
	}

	function setDefaultFonts($normal, $bold = "", $italic = "", $boldItalic = "") {
		callmethod("DrawArea.setDefaultFonts", $this->ptr, $normal, $bold, $italic, $boldItalic);
	}
	function setFontTable($index, $font) {
		callmethod("DrawArea.setFontTable", $this->ptr, $index, $font);
	}
}


#///////////////////////////////////////////////////////////////////////////////////
#//	bindings to drawobj.h
#///////////////////////////////////////////////////////////////////////////////////
class Box {
	function Box($ptr) {
		$this->ptr = $ptr;
	}
	function setPos($x, $y) {
		callmethod("Box.setPos", $this->ptr, $x, $y);
	}
	function setSize($w, $h) {
		callmethod("Box.setSize", $this->ptr, $w, $h);
	}
	function getWidth() {
		return callmethod("Box.getWidth", $this->ptr);
	}
	function getHeight() {
		return callmethod("Box.getHeight", $this->ptr);
	}
	function setBackground($color, $edgeColor = -1, $raisedEffect = 0) {
		callmethod("Box.setBackground", $this->ptr, $color, $edgeColor, $raisedEffect);
	}
	function setRoundedCorners($r1 = 10, $r2 = -1, $r3 = -1, $r4 = -1) {
		callmethod("Box.setRoundedCorners", $this->ptr, $r1, $r2, $r3, $r4);
	}
	function getImageCoor($offsetX = 0, $offsetY = 0) {
		return callmethod("Box.getImageCoor", $this->ptr, $offsetX, $offsetY);
	}
}

class TextBox extends Box {
	function TextBox($ptr) {
		$this->ptr = $ptr;
	}
	function setText($text) {
		callmethod("TextBox.setText", $this->ptr, $text);
	}
	function setAlignment($a) {
		callmethod("TextBox.setAlignment", $this->ptr, $a);
	}
	function setFontStyle($font, $fontIndex = 0) {
		callmethod("TextBox.setFontStyle", $this->ptr, $font, $fontIndex);
	}
	function setFontSize($fontHeight, $fontWidth = 0) {
		callmethod("TextBox.setFontSize", $this->ptr, $fontHeight, $fontWidth);
	}
	function setFontAngle($angle, $vertical = 0) {
		callmethod("TextBox.setFontAngle", $this->ptr, $angle, $vertical);
	}
	function setFontColor($color) {
		callmethod("TextBox.setFontColor", $this->ptr, $color);
	}
	function setMargin2($leftMargin, $rightMargin, $topMargin, $bottomMargin) {
		callmethod("TextBox.setMargin2", $this->ptr,
		$leftMargin, $rightMargin, $topMargin, $bottomMargin);
	}
	function setMargin($m) {
		callmethod("TextBox.setMargin", $this->ptr, $m);
	}
	function setWidth($width) {
		callmethod("TextBox.setWidth", $this->ptr, $width);
	}
	function setHeight($height) {
		callmethod("TextBox.setHeight", $this->ptr, $height);
	}
	function setMaxWidth($maxWidth) {
		callmethod("TextBox.setMaxWidth", $this->ptr, $maxWidth);
	}
	function setZOrder($z) {
		callmethod("TextBox.setZOrder", $this->ptr, $z);
	}
	function setTruncate($maxWidth, $maxLines = 1) {
		callmethod("TextBox.setTruncate", $this->ptr, $maxWidth, $maxLines);
	}
}

class Line {
	function Line($ptr) {
		$this->ptr = $ptr;
	}
	function setPos($x1, $y1, $x2, $y2) {
		callmethod("Line.setPos", $this->ptr, $x1, $y1, $x2, $y2);
	}
	function setColor($c) {
		callmethod("Line.setColor", $this->ptr, $c);
	}
	function setWidth($w) {
		callmethod("Line.setWidth", $this->ptr, $w);
	}
	function setZOrder($z) {
		callmethod("Line.setZOrder", $this->ptr, $z);
	}
}

#///////////////////////////////////////////////////////////////////////////////////
#//	bindings to basechart.h
#///////////////////////////////////////////////////////////////////////////////////
class LegendBox extends TextBox {
	function LegendBox($ptr) {
		$this->ptr = $ptr;
	}
	function setCols($noOfCols) {
		callmethod("LegendBox.setCols", $this->ptr, $noOfCols);
	}
	function setReverse($b = 1) {
		callmethod("LegendBox.setReverse", $this->ptr, $b);
	}
	function addKey($text, $color, $lineWidth = 0, $drawarea = Null) {
		callmethod("LegendBox.addKey", $this->ptr, $text, $color, $lineWidth, decodePtr($drawarea));
	}
	function addKey2($pos, $text, $color, $lineWidth = 0, $drawarea = Null) {
		callmethod("LegendBox.addKey2", $this->ptr, $pos, $text, $color, $lineWidth, decodePtr($drawarea));
	}
	function setKeySize($width, $height = -1, $gap = -1) {
		callmethod("LegendBox.setKeySize", $this->ptr, $width, $height, $gap);
	}
	function setKeySpacing($keySpacing, $lineSpacing = -1) {
		callmethod("LegendBox.setKeySpacing", $this->ptr, $keySpacing, $lineSpacing);
	}
	function setKeyBorder($edgeColor, $raisedEffect = 0) {
		callmethod("LegendBox.setKeyBorder", $this->ptr, $edgeColor, $raisedEffect);
	}
	function getImageCoor2($dataItem, $offsetX = 0, $offsetY = 0) {
		return callmethod("LegendBox.getImageCoor", $this->ptr, $dataItem, $offsetX, $offsetY);
	}
	function getHTMLImageMap($url, $queryFormat = "", $extraAttr = "", $offsetX = 0, $offsetY = 0) {
		return callmethod("LegendBox.getHTMLImageMap", $this->ptr, $url, $queryFormat, $extraAttr, $offsetX, $offsetY);
	}
}

class BaseChart {
	function __del__() {
		callmethod("BaseChart.destroy", $this->ptr);
	}
	#//////////////////////////////////////////////////////////////////////////////////////
	#//	set overall chart
	#//////////////////////////////////////////////////////////////////////////////////////
	function setSize($width, $height) {
		callmethod("BaseChart.setSize", $this->ptr, $width, $height);
	}
	function setBorder($color) {
		callmethod("BaseChart.setBorder", $this->ptr, $color);
	}
	function setRoundedFrame($extColor = 0xffffff, $r1 = 10, $r2 = -1, $r3 = -1, $r4 = -1) {
		callmethod("BaseChart.setRoundedFrame", $this->ptr, $extColor, $r1, $r2, $r3, $r4);
	}
	function setBackground($bgColor, $edgeColor = -1, $raisedEffect = 0) {
		callmethod("BaseChart.setBackground", $this->ptr, $bgColor, $edgeColor, $raisedEffect);
	}
	function setWallpaper($img) {
		callmethod("BaseChart.setWallpaper", $this->ptr, $img);
	}
	function setBgImage($img, $align = Center) {
		callmethod("BaseChart.setBgImage", $this->ptr, $img, $align);
	}
	function setTransparentColor($c) {
		callmethod("BaseChart.setTransparentColor", $this->ptr, $c);
	}
	function setAntiAlias($antiAliasShape = 1, $antiAliasText = AutoAntiAlias) {
		callmethod("BaseChart.setAntiAlias", $this->ptr, $antiAliasShape, $antiAliasText);
	}
	function setSearchPath($path) {
		callmethod("BaseChart.setSearchPath", $this->ptr, $path);
	}

	function addTitle2($alignment, $text, $font = "", $fontSize = 12, $fontColor = TextColor,
	$bgColor = Transparent, $edgeColor = Transparent) {
		return new TextBox(callmethod("BaseChart.addTitle2", $this->ptr,
		$alignment, $text, $font, $fontSize, $fontColor, $bgColor, $edgeColor));
	}
	function addTitle($text, $font = "", $fontSize = 12, $fontColor = TextColor,
	$bgColor = Transparent, $edgeColor = Transparent) {
		return new TextBox(callmethod("BaseChart.addTitle", $this->ptr,
		$text, $font, $fontSize, $fontColor, $bgColor, $edgeColor));
	}
	function addLegend($x, $y, $vertical = 1, $font = "", $fontSize = 10) {
		return new LegendBox(callmethod("BaseChart.addLegend", $this->ptr,
		$x, $y, $vertical, $font, $fontSize));
	}
	function addLegend2($x, $y, $noOfCols, $font = "", $fontSize = 10) {
		return new LegendBox(callmethod("BaseChart.addLegend2", $this->ptr,
		$x, $y, $noOfCols, $font, $fontSize));
	}
	function getLegend() {
		return new LegendBox(callmethod("BaseChart.getLegend", $this->ptr));
	}
	#//////////////////////////////////////////////////////////////////////////////////////
	#//	drawing primitives
	#//////////////////////////////////////////////////////////////////////////////////////
	function getDrawArea() {
		return new DrawArea(callmethod("BaseChart.getDrawArea", $this->ptr));
	}
	function addDrawObj($obj) {
		callmethod("BaseChart.addDrawObj", $obj->ptr);
		return $obj;
	}
	function addText($x, $y, $text, $font = "", $fontSize = 8, $fontColor = TextColor,
	$alignment = TopLeft, $angle = 0, $vertical = 0) {
		return new TextBox(callmethod("BaseChart.addText", $this->ptr,
		$x, $y, $text, $font, $fontSize, $fontColor, $alignment, $angle, $vertical));
	}
	function addLine($x1, $y1, $x2, $y2, $color = LineColor, $lineWidth = 1) {
		return new Line(callmethod("BaseChart.addLine", $this->ptr,
		$x1, $y1, $x2, $y2, $color, $lineWidth));
	}
	function addExtraField($texts) {
		callmethod("BaseChart.addExtraField", $this->ptr, $texts);
	}
	function addExtraField2($numbers) {
		callmethod("BaseChart.addExtraField2", $this->ptr, $numbers);
	}

	#//////////////////////////////////////////////////////////////////////////////////////
	#//	$color management methods
	#//////////////////////////////////////////////////////////////////////////////////////
	function setColor($paletteEntry, $color) {
		callmethod("BaseChart.setColor", $this->ptr, $paletteEntry, $color);
	}
	function setColors($colors) {
		if (count($colors) <= 0 or $colors[count($colors) - 1] != -1)
		$colors[] = -1;
		callmethod("BaseChart.setColors", $this->ptr, $colors);
	}
	function setColors2($paletteEntry, $colors) {
		if (count($colors) <= 0 or $colors[count($colors) - 1] != -1 )
		$colors[] = -1;
		callmethod("BaseChart.setColors2", $this->ptr, $paletteEntry, $colors);
	}
	function getColor($paletteEntry) {
		return callmethod("BaseChart.getColor", $this->ptr, $paletteEntry);
	}
	function dashLineColor($color, $dashPattern) {
		return callmethod("BaseChart.dashLineColor", $this->ptr, $color, $dashPattern);
	}
	function patternColor($c, $h = 0, $startX = 0, $startY = 0) {
		if (!is_array($c))
		return $this->patternColor2($c, $h, $startX);
		return callmethod("BaseChart.patternColor", $this->ptr, $c, $h, $startX, $startY);
	}
	function patternColor2($filename, $startX = 0, $startY = 0) {
		return callmethod("BaseChart.patternColor2", $this->ptr, $filename, $startX, $startY);
	}
	function gradientColor($startX, $startY = 90, $endX = 1, $endY = 0, $startColor = 0, $endColor = Null) {
		if (is_array($startX))
		return $this->gradientColor2($startX, $startY, $endX, $endY, $startColor);
		return callmethod("BaseChart.gradientColor", $this->ptr, $startX, $startY, $endX, $endY, $startColor, $endColor);
	}
	function gradientColor2($c, $angle = 90, $scale = 1, $startX = 0, $startY = 0) {
		return callmethod("BaseChart.gradientColor2", $this->ptr, $c, $angle, $scale, $startX, $startY);
	}
	function linearGradientColor($startX, $startY, $endX, $endY, $startColor, $endColor, $periodic = 0) {
		return callmethod("BaseChart.linearGradientColor", $this->ptr, $startX, $startY, $endX, $endY, $startColor, $endColor, $periodic);
	}
	function linearGradientColor2($startX, $startY, $endX, $endY, $c, $periodic = 0) {
		return callmethod("BaseChart.linearGradientColor2", $this->ptr, $startX, $startY, $endX, $endY, $c, $periodic);
	}
	function radialGradientColor($cx, $cy, $rx, $ry, $startColor, $endColor, $periodic = 0) {
		return callmethod("BaseChart.radialGradientColor", $this->ptr, $cx, $cy, $rx, $ry, $startColor, $endColor, $periodic);
	}
	function radialGradientColor2($cx, $cy, $rx, $ry, $c, $periodic = 0) {
		return callmethod("BaseChart.radialGradientColor2", $this->ptr, $cx, $cy, $rx, $ry, $c, $periodic);
	}

	#//////////////////////////////////////////////////////////////////////////////////////
	#//	locale support
	#//////////////////////////////////////////////////////////////////////////////////////
	function setDefaultFonts($normal, $bold = "", $italic = "", $boldItalic = "") {
		callmethod("BaseChart.setDefaultFonts", $this->ptr, $normal, $bold, $italic, $boldItalic);
	}
	function setFontTable($index, $font) {
		callmethod("BaseChart.setFontTable", $this->ptr, $index, $font);
	}
	function setNumberFormat($thousandSeparator = '~', $decimalPointChar = '.', $signChar = '-') {
		callmethod("BaseChart.setNumberFormat", $this->ptr, $thousandSeparator , $decimalPointChar, $signChar);
	}
	function setMonthNames($names) {
		callmethod("BaseChart.setMonthNames", $this->ptr, $names);
	}
	function setWeekDayNames($names) {
		callmethod("BaseChart.setWeekDayNames", $this->ptr, $names);
	}
	function setAMPM($AM, $PM) {
		callmethod("BaseChart.setAMPM", $this->ptr, $AM, $PM);
	}
	function formatValue($value, $formatString) {
		return callmethod("BaseChart.formatValue", $this->ptr, $value, $formatString);
	}

	#//////////////////////////////////////////////////////////////////////////////////////
	#//	chart creation methods
	#//////////////////////////////////////////////////////////////////////////////////////
	function layoutLegend() {
		return new LegendBox(callmethod("BaseChart.layoutLegend", $this->ptr));
	}
	function layout() {
		callmethod("BaseChart.layout", $this->ptr);
	}
	function makeChart($filename) {
		return callmethod("BaseChart.makeChart", $this->ptr, $filename);
	}
	function makeChart2($format) {
		return callmethod("BaseChart.makeChart2", $this->ptr, $format);
	}
	function makeChart3() {
		return new DrawArea(callmethod("BaseChart.makeChart3", $this->ptr));
	}
	function makeSession($id, $format = PNG) {
		session_register($id);
		global $HTTP_SESSION_VARS;
		if (isset($HTTP_SESSION_VARS))
		$HTTP_SESSION_VARS[$id] = $GLOBALS[$id] = $this->makeChart2($format);
		else
		$_SESSION[$id] = $GLOBALS[$id] = $this->makeChart2($format);
		return "img=".$id."&id=".uniqid(session_id())."&".SID;
	}
	function getHTMLImageMap($url, $queryFormat = "", $extraAttr = "", $offsetX = 0, $offsetY = 0) {
		return callmethod("BaseChart.getHTMLImageMap", $this->ptr, $url, $queryFormat, $extraAttr, $offsetX, $offsetY);
	}
	function halfColor($c) {
		return callmethod("BaseChart.halfColor", $this->ptr, $c);
	}
	function autoColor() {
		return callmethod("BaseChart.autoColor", $this->ptr);
	}
	function getChartMetrics() {
		return callmethod("BaseChart.getChartMetrics", $this->ptr);
	}
}

class MultiChart extends BaseChart {
	function MultiChart($width, $height, $bgColor = BackgroundColor, $edgeColor = Transparent, $raisedEffect = 0) {
		$this->ptr = callmethod("MultiChart.create", $width, $height, $bgColor, $edgeColor, $raisedEffect);
		autoDestroy($this);
	}
	function addChart($x, $y, $c) {
		callmethod("MultiChart.addChart", $this->ptr, $x, $y, $c->ptr);
		$this->dependencies[] = $c;
	}
	function setMainChart($c) {
		callmethod("MultiChart.setMainChart", $this->ptr, $c->ptr);
	}
}

#///////////////////////////////////////////////////////////////////////////////////
#//	bindings to piechart.h
#///////////////////////////////////////////////////////////////////////////////////
class Sector {
	function Sector($ptr) {
		$this->ptr = $ptr;
	}
	function setExplode($distance = -1) {
		callmethod("Sector.setExplode", $this->ptr, $distance);
	}
	function setLabelFormat($formatString) {
		callmethod("Sector.setLabelFormat", $this->ptr, $formatString);
	}
	function setLabelStyle($font = "", $fontSize = 8, $fontColor = TextColor) {
		return new TextBox(callmethod("Sector.setLabelStyle", $this->ptr, $font, $fontSize, $fontColor));
	}
	function setLabelPos($pos, $joinLineColor = -1) {
		callmethod("Sector.setLabelPos", $this->ptr, $pos, $joinLineColor);
	}
	function setJoinLine($joinLineColor, $joinLineWidth = 1) {
		callmethod("Sector.setJoinLine", $this->ptr, $joinLineColor, $joinLineWidth);
	}
	function setColor($color, $edgeColor = -1, $joinLineColor = -1) {
		callmethod("Sector.setColor", $this->ptr, $color, $edgeColor, $joinLineColor);
	}
	function getImageCoor($offsetX = 0, $offsetY = 0) {
		return callmethod("Sector.getImageCoor", $this->ptr, $offsetX, $offsetY);
	}
	function getLabelCoor($offsetX = 0, $offsetY = 0) {
		return callmethod("Sector.getLabelCoor", $this->ptr, $offsetX, $offsetY);
	}
	function setLabelLayout($layoutMethod, $pos = -1) {
		callmethod("Sector.setLabelLayout", $this->ptr, $layoutMethod, $pos);
	}
}

class PieChart extends BaseChart {
	function PieChart($width, $height, $bgColor = BackgroundColor, $edgeColor = Transparent, $raisedEffect = 0) {
		$this->ptr = callmethod("PieChart.create", $width, $height, $bgColor, $edgeColor, $raisedEffect);
		autoDestroy($this);
	}
	function setPieSize($x, $y, $r) {
		callmethod("PieChart.setPieSize", $this->ptr, $x, $y, $r);
	}
	function setDonutSize($x, $y, $r, $r2) {
		callmethod("PieChart.setDonutSize", $this->ptr, $x, $y, $r, $r2);
	}
	function set3D($depth = -1, $angle = -1, $shadowMode = 0) {
		if (is_array($depth))
		$this->set3D2($depth, $angle, $shadowMode);
		else
		callmethod("PieChart.set3D", $this->ptr, $depth, $angle, $shadowMode);
	}
	function set3D2($depths, $angle = 45, $shadowMode = 0) {
		callmethod("PieChart.set3D2", $this->ptr, $depths, $angle, $shadowMode);
	}
	function setStartAngle($startAngle, $clockWise = 1) {
		callmethod("PieChart.setStartAngle", $this->ptr, $startAngle, $clockWise);
	}
	function setExplode($sectorNo, $distance = -1) {
		callmethod("PieChart.setExplode", $this->ptr, $sectorNo, $distance);
	}
	function setExplodeGroup($startSector, $endSector, $distance = -1) {
		callmethod("PieChart.setExplodeGroup", $this->ptr, $startSector, $endSector, $distance);
	}

	function setLabelFormat($formatString) {
		callmethod("PieChart.setLabelFormat", $this->ptr, $formatString);
	}
	function setLabelStyle($font = "", $fontSize = 8, $fontColor = TextColor) {
		return new TextBox(callmethod("PieChart.setLabelStyle", $this->ptr, $font,
		$fontSize, $fontColor));
	}
	function setLabelPos($pos, $joinLineColor = -1) {
		callmethod("PieChart.setLabelPos", $this->ptr, $pos, $joinLineColor);
	}
	function setLabelLayout($layoutMethod, $pos = -1, $topBound = -1, $bottomBound = -1) {
		callmethod("PieChart.setLabelLayout", $this->ptr, $layoutMethod, $pos, $topBound, $bottomBound);
	}
	function setJoinLine($joinLineColor, $joinLineWidth = 1) {
		callmethod("PieChart.setJoinLine", $this->ptr, $joinLineColor, $joinLineWidth);
	}
	function setLineColor($edgeColor, $joinLineColor = -1) {
		callmethod("PieChart.setLineColor", $this->ptr, $edgeColor, $joinLineColor);
	}

	function setData($data, $labels = Null) {
		callmethod("PieChart.setData", $this->ptr, $data, $labels);
	}
	function sector($sectorNo) {
		return new Sector(callmethod("PieChart.sector", $this->ptr, $sectorNo));
	}
}

#///////////////////////////////////////////////////////////////////////////////////
#//	bindings to axis.h
#///////////////////////////////////////////////////////////////////////////////////
class Mark extends TextBox {
	function Mark($ptr) {
		$this->ptr = $ptr;
	}
	function setValue($value) {
		callmethod("Mark.setValue", $this->ptr, $value);
	}
	function setMarkColor($lineColor, $textColor = -1, $tickColor = -1) {
		callmethod("Mark.setMarkColor", $this->ptr, $lineColor, $textColor, $tickColor);
	}
	function setLineWidth($w) {
		callmethod("Mark.setLineWidth", $this->ptr, $w);
	}
	function setDrawOnTop($b) {
		callmethod("Mark.setDrawOnTop", $this->ptr, $b);
	}
	function getLine() {
		return callmethod("Mark.getLine", $this->ptr);
	}
}

class Axis {
	function Axis($ptr) {
		$this->ptr = $ptr;
	}
	function setLabelStyle($font = "", $fontSize = 8, $fontColor = TextColor, $fontAngle = 0) {
		return new TextBox(callmethod("Axis.setLabelStyle", $this->ptr, $font, $fontSize, $fontColor, $fontAngle));
	}
	function setLabelFormat($formatString) {
		callmethod("Axis.setLabelFormat", $this->ptr, $formatString);
	}
	function setLabelGap($d) {
		callmethod("Axis.setLabelGap", $this->ptr, $d);
	}
	function setMultiFormat($filter1, $format1, $filter2 = 1, $format2 = Null, $labelSpan = 1, $promoteFirst = 1) {
		if (is_null($format2))
		$this->setMultiFormat2($filter1, $format1, $filter2, 1);
		else
		callmethod("Axis.setMultiFormat", $this->ptr, $filter1, $format1, $filter2, $format2, $labelSpan, $promoteFirst);
	}
	function setMultiFormat2($filterId, $formatString, $labelSpan = 1, $promoteFirst = 1) {
		callmethod("Axis.setMultiFormat2", $this->ptr, $filterId, $formatString, $labelSpan, $promoteFirst);
	}
	function setFormatCondition($condition, $operand = 0) {
		callmethod("Axis.setFormatCondition", $this->ptr, $condition, $operand);
	}

	function setTitle($text, $font = "", $fontSize = 8, $fontColor = TextColor) {
		return new TextBox(callmethod("Axis.setTitle", $this->ptr, $text, $font, $fontSize, $fontColor));
	}
	function setTitlePos($alignment, $titleGap = 3) {
		callmethod("Axis.setTitlePos", $this->ptr, $alignment, $titleGap);
	}
	function setColors($axisColor, $labelColor = TextColor, $titleColor = -1, $tickColor = -1) {
		callmethod("Axis.setColors", $this->ptr, $axisColor, $labelColor, $titleColor, $tickColor);
	}

	function setTickLength($majorTickLen, $minorTickLen = Null) {
		if (is_null($minorTickLen))
		callmethod("Axis.setTickLength", $this->ptr, $majorTickLen);
		else
		$this->setTickLength2($majorTickLen, $minorTickLen);
	}
	function setTickLength2($majorTickLen, $minorTickLen) {
		callmethod("Axis.setTickLength2", $this->ptr, $majorTickLen, $minorTickLen);
	}
	function setTickWidth($majorTickWidth, $minorTickWidth = -1) {
		callmethod("Axis.setTickWidth", $this->ptr, $majorTickWidth, $minorTickWidth);
	}
	function setTickColor($majorTickColor, $minorTickColor = -1) {
		callmethod("Axis.setTickColor", $this->ptr, $majorTickColor, $minorTickColor);
	}

	function setWidth($width) {
		callmethod("Axis.setWidth", $this->ptr, $width);
	}
	function setLength($length) {
		callmethod("Axis.setLength", $this->ptr, $length);
	}
	function setPos($x, $y, $align = Center) {
		callmethod("Axis.setPos", $this->ptr, $x, $y, $align);
	}
	function setTopMargin($topMargin) {
		$this->setMargin($topMargin);
	}
	function setMargin($topMargin, $bottomMargin = 0) {
		callmethod("Axis.setMargin", $this->ptr, $topMargin, $bottomMargin);
	}
	function setIndent($indent) {
		callmethod("Axis.setIndent", $this->ptr, $indent);
	}
	function setTickOffset($offset) {
		callmethod("Axis.setTickOffset", $this->ptr, $offset);
	}
	function setLabelOffset($offset) {
		callmethod("Axis.setLabelOffset", $this->ptr, $offset);
	}

	function setAutoScale($topExtension = 0.1, $bottomExtension = 0.1, $zeroAffinity = 0.8) {
		callmethod("Axis.setAutoScale", $this->ptr, $topExtension, $bottomExtension, $zeroAffinity);
	}
	function setRounding($roundMin, $roundMax) {
		callmethod("Axis.setRounding", $this->ptr, $roundMin, $roundMax);
	}
	function setTickDensity($majorTickDensity, $minorTickSpacing = -1) {
		callmethod("Axis.setTickDensity", $this->ptr, $majorTickDensity, $minorTickSpacing);
	}
	function setReverse($b = 1) {
		callmethod("Axis.setReverse", $this->ptr, $b);
	}
	function setMinTickInc($inc) {
		callmethod("Axis.setMinTickInc", $this->ptr, $inc);
	}

	function setLabels($labels, $formatString = Null) {
		if (is_null($formatString))
		return new TextBox(callmethod("Axis.setLabels", $this->ptr, $labels));
		else
		return $this->setLabels2($labels, $formatString);
	}
	function setLabels2($labels, $formatString = "") {
		return new TextBox(callmethod("Axis.setLabels2", $this->ptr, $labels, $formatString));
	}

	function setLabelStep($majorTickStep, $minorTickStep = 0, $majorTickOffset = 0, $minorTickOffset = -0x7fffffff) {
		callmethod("Axis.setLabelStep", $this->ptr, $majorTickStep, $minorTickStep, $majorTickOffset, $minorTickOffset);
	}

	function setLinearScale($lowerLimit = Null, $upperLimit = Null, $majorTickInc = 0, $minorTickInc = 0) {
		if (is_null($lowerLimit))
		$this->setLinearScale3();
		else if (is_null($upperLimit))
		$this->setLinearScale3($lowerLimit);
		else if (is_array($majorTickInc))
		$this->setLinearScale2($lowerLimit, $upperLimit, $majorTickInc);
		else
		callmethod("Axis.setLinearScale", $this->ptr, $lowerLimit, $upperLimit, $majorTickInc, $minorTickInc);
	}
	function setLinearScale2($lowerLimit, $upperLimit, $labels) {
		callmethod("Axis.setLinearScale2", $this->ptr, $lowerLimit, $upperLimit, $labels);
	}
	function setLinearScale3($formatString = "") {
		callmethod("Axis.setLinearScale3", $this->ptr, $formatString);
	}

	function setLogScale($lowerLimit = Null, $upperLimit = Null, $majorTickInc = 0, $minorTickInc = 0) {
		if (is_null($lowerLimit))
		$this->setLogScale3();
		else if (is_null($upperLimit))
		$this->setLogScale3($lowerLimit);
		else if (is_array($majorTickInc))
		$this->setLogScale2($lowerLimit, $upperLimit, $majorTickInc);
		else
		callmethod("Axis.setLogScale", $this->ptr, $lowerLimit, $upperLimit, $majorTickInc, $minorTickInc);
	}
	function setLogScale2($lowerLimit, $upperLimit, $labels = 0) {
		if (is_array($labels))
		callmethod("Axis.setLogScale2", $this->ptr, $lowerLimit, $upperLimit, $labels);
		else
		#compatibility with ChartDirector Ver 2.5
		$this->setLogScale($lowerLimit, $upperLimit, $labels);
	}
	function setLogScale3($formatString = "") {
		if (!is_string($formatString)) {
			#compatibility with ChartDirector Ver 2.5
			if ($formatString)
			$this->setLogScale3();
			else
			$this->setLinearScale3();
		}
		else
		callmethod("Axis.setLogScale3", $this->ptr, $formatString);
	}

	function setDateScale($lowerLimit = Null, $upperLimit = Null, $majorTickInc = 0, $minorTickInc = 0) {
		if (is_null($lowerLimit))
		$this->setDateScale3();
		else if (is_null($upperLimit))
		$this->setDateScale3($lowerLimit);
		else if (is_array($majorTickInc))
		$this->setDateScale2($lowerLimit, $upperLimit, $majorTickInc);
		else
		callmethod("Axis.setDateScale", $this->ptr, $lowerLimit, $upperLimit, $majorTickInc, $minorTickInc);
	}
	function setDateScale2($lowerLimit, $upperLimit, $labels) {
		callmethod("Axis.setDateScale2", $this->ptr, $lowerLimit, $upperLimit, $labels);
	}
	function setDateScale3($formatString = "") {
		callmethod("Axis.setDateScale3", $this->ptr, $formatString);
	}

	function syncAxis($axis, $slope = 1, $intercept = 0) {
		callmethod("Axis.syncAxis", $this->ptr, $axis->ptr, $slope, $intercept);
	}
	function copyAxis($axis) {
		callmethod("Axis.copyAxis", $this->ptr, $axis->ptr);
	}

	function addLabel($pos, $label) {
		callmethod("Axis.addLabel", $this->ptr, $pos, $label);
	}
	function addMark($lineColor, $value, $text = "", $font = "", $fontSize = 8) {
		return new Mark(callmethod("Axis.addMark", $this->ptr, $lineColor, $value, $text, $font, $fontSize));
	}
	function addZone($startValue, $endValue, $color) {
		callmethod("Axis.addZone", $this->ptr, $startValue, $endValue, $color);
	}

	function getCoor($v) {
		return callmethod("Axis.getCoor", $this->ptr, $v);
	}
	function getLength() {
		return callmethod("Axis.getLength", $this->ptr);
	}
	function getMinValue() {
		return callmethod("Axis.getMinValue", $this->ptr);
	}
	function getMaxValue() {
		return callmethod("Axis.getMaxValue", $this->ptr);
	}
	function getScaleType() {
		return callmethod("Axis.getScaleType", $this->ptr);
	}

	function getTicks() {
		return callmethod("Axis.getTicks", $this->ptr);
	}
	function getLabel($i) {
		return callmethod("Axis.getLabel", $this->ptr, $i);
	}

	function getAxisImageMap($noOfSegments, $mapWidth, $url, $queryFormat = "", $extraAttr = "", $offsetX = 0, $offsetY = 0) {
		return callmethod("Axis.getAxisImageMap", $this->ptr, $noOfSegments, $mapWidth, $url, $queryFormat, $extraAttr, $offsetX, $offsetY);
	}
	function getHTMLImageMap($url, $queryFormat = "", $extraAttr = "", $offsetX = 0, $offsetY = 0) {
		return callmethod("Axis.getHTMLImageMap", $this->ptr, $url, $queryFormat, $extraAttr, $offsetX, $offsetY);
	}
}

class AngularAxis {
	function AngularAxis($ptr) {
		$this->ptr = $ptr;
	}
	function setLabelStyle($font = "bold", $fontSize = 10, $fontColor = TextColor, $fontAngle = 0) {
		return new TextBox(callmethod("AngularAxis.setLabelStyle", $this->ptr, $font, $fontSize, $fontColor, $fontAngle));
	}
	function setLabelGap($d) {
		callmethod("AngularAxis.setLabelGap", $this->ptr, $d);
	}

	function setLabels($labels, $formatString = Null) {
		if (is_null($formatString))
		return new TextBox(callmethod("AngularAxis.setLabels", $this->ptr, $labels));
		else
		return $this->setLabels2($labels, $formatString);
	}
	function setLabels2($labels, $formatString = "") {
		return new TextBox(callmethod("AngularAxis.setLabels2", $this->ptr, $labels, $formatString));
	}
	function addLabel($pos, $label) {
		callmethod("AngularAxis.addLabel", $this->ptr, $pos, $label);
	}

	function setLinearScale($lowerLimit, $upperLimit, $majorTickInc = 0, $minorTickInc = 0) {
		if (is_array($majorTickInc))
		$this->setLinearScale2($lowerLimit, $upperLimit, $majorTickInc);
		else
		callmethod("AngularAxis.setLinearScale", $this->ptr, $lowerLimit, $upperLimit, $majorTickInc, $minorTickInc);
	}
	function setLinearScale2($lowerLimit, $upperLimit, $labels) {
		callmethod("AngularAxis.setLinearScale2", $this->ptr, $lowerLimit, $upperLimit, $labels);
	}

	function addZone($startValue, $endValue, $startRadius, $endRadius = -1, $fillColor = Null, $edgeColor = -1) {
		if (is_null($fillColor))
		$this->addZone2($startValue, $endValue, $startRadius, $endRadius);
		else
		callmethod("AngularAxis.addZone", $this->ptr, $startValue, $endValue, $startRadius, $endRadius, $fillColor, $edgeColor);
	}
	function addZone2($startValue, $endValue, $fillColor, $edgeColor = -1) {
		callmethod("AngularAxis.addZone2", $this->ptr, $startValue, $endValue, $fillColor, $edgeColor);
	}

	function getCoor($v) {
		return callmethod("AngularAxis.getCoor", $this->ptr, $v);
	}
	function getTicks() {
		return callmethod("AngularAxis.getTicks", $this->ptr);
	}
	function getLabel($i) {
		return callmethod("AngularAxis.getLabel", $this->ptr, $i);
	}

	function getAxisImageMap($noOfSegments, $mapWidth, $url, $queryFormat = "", $extraAttr = "", $offsetX = 0, $offsetY = 0) {
		return callmethod("AngularAxis.getAxisImageMap", $this->ptr, $noOfSegments, $mapWidth, $url, $queryFormat, $extraAttr, $offsetX, $offsetY);
	}
	function getHTMLImageMap($url, $queryFormat = "", $extraAttr = "", $offsetX = 0, $offsetY = 0) {
		return callmethod("AngularAxis.getHTMLImageMap", $this->ptr, $url, $queryFormat, $extraAttr, $offsetX, $offsetY);
	}
}

#///////////////////////////////////////////////////////////////////////////////////
#//	bindings to layer.h
#///////////////////////////////////////////////////////////////////////////////////
class DataSet {
	function DataSet($ptr) {
		$this->ptr = $ptr;
	}
	function setData($data) {
		callmethod("DataSet.setData", $this->ptr, $data);
	}
	function setDataName($name) {
		callmethod("DataSet.setDataName", $this->ptr, $name);
	}
	function setDataColor($dataColor, $edgeColor = -1, $shadowColor = -1, $shadowEdgeColor = -1) {
		callmethod("DataSet.setDataColor", $this->ptr, $dataColor, $edgeColor, $shadowColor, $shadowEdgeColor);
	}
	function setUseYAxis2($b = 1) {
		callmethod("DataSet.setUseYAxis2", $this->ptr, $b);
	}
	function setUseYAxis($a) {
		callmethod("DataSet.setUseYAxis", $this->ptr, $a->ptr);
	}
	function setLineWidth($w) {
		callmethod("DataSet.setLineWidth", $this->ptr, $w);
	}

	function setDataLabelFormat($formatString) {
		callmethod("DataSet.setDataLabelFormat", $this->ptr, $formatString);
	}
	function setDataLabelStyle($font = "", $fontSize = 8, $fontColor = TextColor, $fontAngle = 0) {
		return new TextBox(callmethod("DataSet.setDataLabelStyle", $this->ptr, $font, $fontSize, $fontColor, $fontAngle));
	}

	function setDataSymbol($symbol, $size = Null, $fillColor = -1, $edgeColor = -1, $lineWidth = 1) {
		if (is_array($symbol)) {
			if (is_null($size))
			$size = 11;
			$this->setDataSymbol4($symbol, $size, $fillColor, $edgeColor);
			return;
		}
		if (!is_numeric($symbol))
		return $this->setDataSymbol2($symbol);
		if (is_null($size))
		$size = 5;
		callmethod("DataSet.setDataSymbol", $this->ptr, $symbol, $size, $fillColor, $edgeColor, $lineWidth);
	}
	function setDataSymbol2($image) {
		if (!is_string($image))
		return $this->setDataSymbol3($image);
		callmethod("DataSet.setDataSymbol2", $this->ptr, $image);
	}
	function setDataSymbol3($image) {
		callmethod("DataSet.setDataSymbol3", $this->ptr, $image->ptr);
	}
	function setDataSymbol4($polygon, $size = 11, $fillColor = -1, $edgeColor = -1) {
		callmethod("DataSet.setDataSymbol4", $this->ptr, $polygon, $size, $fillColor, $edgeColor);
	}
}

class Layer {
	function Layer($ptr) {
		$this->ptr = $ptr;
	}
	function setSize($x, $y, $w, $h, $swapXY = 0) {
		callmethod("Layer.setSize", $this->ptr, $x, $y, $w, $h, $swapXY);
	}
	function setBorderColor($color, $raisedEffect = 0) {
		callmethod("Layer.setBorderColor", $this->ptr, $color, $raisedEffect);
	}
	function set3D($d = -1, $zGap = 0) {
		callmethod("Layer.set3D", $this->ptr, $d, $zGap);
	}
	function set3D2($xDepth, $yDepth, $xGap, $yGap) {
		callmethod("Layer.set3D2", $this->ptr, $xDepth, $yDepth, $xGap, $yGap);
	}
	function setLineWidth($w) {
		callmethod("Layer.setLineWidth", $this->ptr, $w);
	}
	function setLegend($m) {
		callmethod("Layer.setLegend", $this->ptr, $m);
	}
	function setLegendOrder($dataSetOrder, $layerOrder = -1) {
		callmethod("Layer.setLegendOrder", $this->ptr, $dataSetOrder, $layerOrder);
	}
	function setDataCombineMethod($m) {
		callmethod("Layer.setDataCombineMethod", $this->ptr, $m);
	}
	function addDataSet($data, $color = -1, $name = "") {
		return new DataSet(callmethod("Layer.addDataSet", $this->ptr, $data, $color, $name));
	}
	function addDataGroup($name = "") {
		callmethod("Layer.addDataGroup", $this->ptr, $name);
	}
	function addExtraField($texts) {
		callmethod("Layer.addExtraField", $this->ptr, $texts);
	}
	function addExtraField2($numbers) {
		callmethod("Layer.addExtraField2", $this->ptr, $numbers);
	}
	function getDataSet($dataSet) {
		return new DataSet(callmethod("Layer.getDataSet", $this->ptr, $dataSet));
	}
	function setUseYAxis2($b = 1) {
		callmethod("Layer.setUseYAxis2", $this->ptr, $b);
	}
	function setUseYAxis($a) {
		callmethod("Layer.setUseYAxis", $this->ptr, $a->ptr);
	}

	function setXData($xData, $maxValue = Null) {
		if (is_null($maxValue))
		callmethod("Layer.setXData", $this->ptr, $xData);
		else
		$this->setXData2($xData, $maxValue);
	}
	function setXData2($minValue, $maxValue) {
		callmethod("Layer.setXData2", $this->ptr, $minValue, $maxValue);
	}
	function alignLayer($layer, $dataSet) {
		callmethod("Layer.alignLayer", $this->ptr, $layer->ptr, $dataSet);
	}

	function getMinX() {
		return callmethod("Layer.getMinX", $this->ptr);
	}
	function getMaxX() {
		return callmethod("Layer.getMaxX", $this->ptr);
	}
	function getMaxY($yAxis = 1) {
		return callmethod("Layer.getMaxY", $this->ptr, $yAxis);
	}
	function getMinY($yAxis = 1) {
		return callmethod("Layer.getMinY", $this->ptr, $yAxis);
	}
	function getDepthX() {
		return callmethod("Layer.getDepthX", $this->ptr);
	}
	function getDepthY() {
		return callmethod("Layer.getDepthY", $this->ptr);
	}
	function getXCoor($v) {
		return callmethod("Layer.getXCoor", $this->ptr, $v);
	}
	function getYCoor($v, $yAxis = 1) {
		if (is_object($yAxis))
		return callmethod("Layer.getYCoor2", $this->ptr, $v, $yAxis->ptr);
		else
		return callmethod("Layer.getYCoor", $this->ptr, $v, $yAxis);
	}
	function xZoneColor($threshold, $belowColor, $aboveColor) {
		return callmethod("Layer.xZoneColor", $this->ptr, $threshold, $belowColor, $aboveColor);
	}
	function yZoneColor($threshold, $belowColor, $aboveColor, $yAxis = 1) {
		if (is_object($yAxis))
		return callmethod("Layer.yZoneColor2", $this->ptr, $threshold, $belowColor, $aboveColor, $yAxis->ptr);
		else
		return callmethod("Layer.yZoneColor", $this->ptr, $threshold, $belowColor, $aboveColor, $yAxis);
	}

	function setDataLabelFormat($formatString) {
		callmethod("Layer.setDataLabelFormat", $this->ptr, $formatString);
	}
	function setDataLabelStyle($font = "", $fontSize = 8, $fontColor = TextColor, $fontAngle = 0) {
		return new TextBox(callmethod("Layer.setDataLabelStyle", $this->ptr, $font, $fontSize, $fontColor, $fontAngle));
	}
	function setAggregateLabelFormat($formatString) {
		callmethod("Layer.setAggregateLabelFormat", $this->ptr, $formatString);
	}
	function setAggregateLabelStyle($font = "", $fontSize = 8, $fontColor = TextColor, $fontAngle = 0) {
		return new TextBox(callmethod("Layer.setAggregateLabelStyle", $this->ptr, $font, $fontSize, $fontColor, $fontAngle));
	}
	function addCustomDataLabel($dataSet, $dataItem, $label, $font = "", $fontSize = 8, $fontColor = TextColor, $fontAngle = 0) {
		return new TextBox(callmethod("Layer.addCustomDataLabel", $this->ptr, $dataSet, $dataItem, $label, $font, $fontSize, $fontColor, $fontAngle));
	}
	function addCustomAggregateLabel($dataItem, $label, $font = "", $fontSize = 8, $fontColor = TextColor, $fontAngle = 0) {
		return new TextBox(callmethod("Layer.addCustomAggregateLabel", $this->ptr, $dataItem, $label, $font, $fontSize, $fontColor, $fontAngle));
	}
	function addCustomGroupLabel($dataGroup, $dataItem, $label, $font = "", $fontSize = 8, $fontColor = TextColor, $fontAngle = 0) {
		return new TextBox(callmethod("Layer.addCustomGroupLabel", $this->ptr, $dataGroup, $dataItem, $label, $font, $fontSize, $fontColor, $fontAngle));
	}

	function getImageCoor($dataSet, $dataItem = Null, $offsetX = 0, $offsetY = 0) {
		if (is_null($dataItem))
		return $this->getImageCoor2($dataSet, $offsetX, $offsetY);
		return callmethod("Layer.getImageCoor", $this->ptr, $dataSet, $dataItem, $offsetX, $offsetY);
	}
	function getImageCoor2($dataItem, $offsetX = 0, $offsetY = 0) {
		return callmethod("Layer.getImageCoor2", $this->ptr, $dataItem, $offsetX, $offsetY);
	}
	function getHTMLImageMap($url, $queryFormat = "", $extraAttr = "", $offsetX = 0, $offsetY = 0) {
		return callmethod("Layer.getHTMLImageMap", $this->ptr, $url, $queryFormat, $extraAttr, $offsetX, $offsetY);
	}
	function setHTMLImageMap($url, $queryFormat = "", $extraAttr = "") {
		return callmethod("Layer.setHTMLImageMap", $this->ptr, $url, $queryFormat, $extraAttr);
	}
}

#///////////////////////////////////////////////////////////////////////////////////
#//	bindings to barlayer.h
#///////////////////////////////////////////////////////////////////////////////////
class BarLayer extends Layer {
	function BarLayer($ptr) {
		$this->ptr = $ptr;
	}
	function setBarGap($barGap, $subBarGap = 0.2) {
		callmethod("BarLayer.setBarGap", $this->ptr, $barGap, $subBarGap);
	}
	function setBarWidth($barWidth, $subBarWidth = -1) {
		callmethod("BarLayer.setBarWidth", $this->ptr, $barWidth, $subBarWidth);
	}
	function setMinLabelSize($s) {
		callmethod("BarLayer.setMinLabelSize", $this->ptr, $s);
	}
	function setBarShape($shape, $dataGroup = -1, $dataItem = -1) {
		if (is_array($shape))
		$this->setBarShape2($shape, $dataGroup, $dataItem);
		else
		callmethod("BarLayer.setBarShape", $this->ptr, $shape, $dataGroup, $dataItem);
	}
	function setBarShape2($shape, $dataGroup = -1, $dataItem = -1) {
		callmethod("BarLayer.setBarShape2", $this->ptr, $shape, $dataGroup, $dataItem);
	}
	function setIconSize($height, $width = -1) {
		callmethod("BarLayer.setIconSize", $this->ptr, $height, $width);
	}
	function setOverlapRatio($overlapRatio, $firstOnTop = 1) {
		callmethod("BarLayer.setOverlapRatio", $this->ptr, $overlapRatio, $firstOnTop);
	}
}

#///////////////////////////////////////////////////////////////////////////////////
#//	bindings to linelayer.h
#///////////////////////////////////////////////////////////////////////////////////
class LineLayer extends Layer {
	function LineLayer($ptr) {
		$this->ptr = $ptr;
	}
	function setSymbolScale($zDataX, $scaleTypeX = PixelScale, $zDataY = Null, $scaleTypeY = PixelScale) {
		callmethod("LineLayer.setSymbolScale", $this->ptr, $zDataX, $scaleTypeX, $zDataY, $scaleTypeY);
	}
	function setGapColor($lineColor, $lineWidth = -1) {
		callmethod("LineLayer.setGapColor", $this->ptr, $lineColor, $lineWidth);
	}
	function setImageMapWidth($width) {
		callmethod("LineLayer.setImageMapWidth", $this->ptr, $width);
	}
	function getLine($dataSet = 0) {
		return callmethod("LineLayer.getLine", $this->ptr, $dataSet);
	}
}

class ScatterLayer extends LineLayer {
	function ScatterLayer($ptr) {
		$this->ptr = $ptr;
	}
}

class InterLineLayer extends LineLayer {
	function InterLineLayer($ptr) {
		$this->ptr = $ptr;
	}
	function setGapColor($gapColor12, $gapColor21 = -1) {
		return callmethod("InterLineLayer.setGapColor", $this->ptr, $gapColor12, $gapColor21);
	}
}

class SplineLayer extends LineLayer {
	function SplineLayer($ptr) {
		$this->ptr = $ptr;
	}
	function setTension($tension) {
		return callmethod("SplineLayer.setTension", $this->ptr, $tension);
	}
}

class StepLineLayer extends LineLayer {
	function StepLineLayer($ptr) {
		$this->ptr = $ptr;
	}
	function setAlignment($a) {
		return callmethod("StepLineLayer.getLine", $this->ptr, $a);
	}
}

#///////////////////////////////////////////////////////////////////////////////////
#//	bindings to arealayer.h
#///////////////////////////////////////////////////////////////////////////////////
class AreaLayer extends Layer {
	function AreaLayer($ptr) {
		$this->ptr = $ptr;
	}
	function setMinLabelSize($s) {
		callmethod("AreaLayer.setMinLabelSize", $this->ptr, $s);
	}
	function setGapColor($fillColor) {
		callmethod("AreaLayer.setGapColor", $this->ptr, $fillColor);
	}
}

#///////////////////////////////////////////////////////////////////////////////////
#//	bindings to trendlayer.h
#///////////////////////////////////////////////////////////////////////////////////
class TrendLayer extends Layer {
	function TrendLayer($ptr) {
		$this->ptr = $ptr;
	}
	function setImageMapWidth($width) {
		callmethod("TrendLayer.setImageMapWidth", $this->ptr, $width);
	}
	function getLine() {
		return callmethod("TrendLayer.getLine", $this->ptr);
	}
	function addConfidenceBand($confidence, $upperFillColor, $upperEdgeColor = Transparent, $upperLineWidth = 1,
	$lowerFillColor = -1, $lowerEdgeColor = -1, $lowerLineWidth = -1) {
		callmethod("TrendLayer.addConfidenceBand", $this->ptr, $confidence, $upperFillColor, $upperEdgeColor, $upperLineWidth,
		$lowerFillColor, $lowerEdgeColor, $lowerLineWidth);
	}
	function addPredictionBand($confidence, $upperFillColor, $upperEdgeColor = Transparent, $upperLineWidth = 1,
	$lowerFillColor = -1, $lowerEdgeColor = -1, $lowerLineWidth = -1) {
		callmethod("TrendLayer.addPredictionBand", $this->ptr, $confidence, $upperFillColor, $upperEdgeColor, $upperLineWidth,
		$lowerFillColor, $lowerEdgeColor, $lowerLineWidth);
	}
	function getSlope() {
		return callmethod("TrendLayer.getSlope", $this->ptr);
	}
	function getIntercept() {
		return callmethod("TrendLayer.getIntercept", $this->ptr);
	}
	function getCorrelation() {
		return callmethod("TrendLayer.getCorrelation", $this->ptr);
	}
	function getStdError() {
		return callmethod("TrendLayer.getStdError", $this->ptr);
	}
}

#///////////////////////////////////////////////////////////////////////////////////
#//	bindings to hloclayer.h
#///////////////////////////////////////////////////////////////////////////////////
class BaseBoxLayer extends Layer
{
	function BaseBoxLayer($ptr) {
		$this->ptr = $ptr;
	}
	function setDataGap($gap) {
		callmethod("BaseBoxLayer.setDataGap", $this->ptr, $gap);
	}
	function setDataWidth($width) {
		callmethod("BaseBoxLayer.setDataWidth", $this->ptr, $width);
	}
}

class HLOCLayer extends BaseBoxLayer {
	function HLOCLayer($ptr) {
		$this->ptr = $ptr;
	}
	function setColorMethod($colorMethod, $riseColor, $fallColor = -1, $leadValue = -1.7E308) {
		callmethod("HLOCLayer.setColorMethod", $this->ptr, $colorMethod, $riseColor, $fallColor, $leadValue);
	}
}

class CandleStickLayer extends BaseBoxLayer {
	function CandleStickLayer($ptr) {
		$this->ptr = $ptr;
	}
}

class BoxWhiskerLayer extends BaseBoxLayer {
	function BoxWhiskerLayer($ptr) {
		$this->ptr = $ptr;
	}
	function setBoxColors($colors, $names = Null) {
		callmethod("BoxWhiskerLayer.setBoxColors", $this->ptr, $colors, $names);
	}
	function setBoxColor($item, $boxColor) {
		callmethod("BoxWhiskerLayer.setBoxColor", $this->ptr, $item, $boxColor);
	}
	function setWhiskerBrightness($whiskerBrightness) {
		callmethod("BoxWhiskerLayer.setWhiskerBrightness", $this->ptr, $whiskerBrightness);
	}
}

class VectorLayer extends Layer
{
	function VectorLayer($ptr) {
		$this->ptr = $ptr;
	}
	function setVector($lengths, $directions, $lengthScale = PixelScale) {
		callmethod("VectorLayer.setVector", $this->ptr, $lengths, $directions, $lengthScale);
	}
	function setArrowHead($width, $height = 0) {
		if (is_array($width))
		$this->setArrowHead2($width);
		else
		callmethod("VectorLayer.setArrowHead", $this->ptr, $width, $height);
	}
	function setArrowHead2($polygon) {
		callmethod("VectorLayer.setArrowHead2", $this->ptr, $polygon);
	}
	function setArrowStem($polygon) {
		callmethod("VectorLayer.setArrowStem", $this->ptr, $polygon);
	}
	function setArrowAlignment($alignment) {
		callmethod("VectorLayer.setArrowAlignment", $this->ptr, $alignment);
	}
	function setIconSize($height, $width = 0) {
		callmethod("VectorLayer.setIconSize", $this->ptr, $height, $width);
	}
}

#///////////////////////////////////////////////////////////////////////////////////
#//	bindings to xychart.h
#///////////////////////////////////////////////////////////////////////////////////
class PlotArea {
	function PlotArea($ptr) {
		$this->ptr = $ptr;
	}
	function setBackground($color, $altBgColor = -1, $edgeColor = -1) {
		callmethod("PlotArea.setBackground", $this->ptr, $color, $altBgColor, $edgeColor);
	}
	function setBackground2($img, $align = Center) {
		callmethod("PlotArea.setBackground2", $this->ptr, $img, $align);
	}
	function set4QBgColor($Q1Color, $Q2Color, $Q3Color, $Q4Color, $edgeColor = -1) {
		callmethod("PlotArea.set4QBgColor", $this->ptr, $Q1Color, $Q2Color, $Q3Color, $Q4Color, $edgeColor);
	}
	function setAltBgColor($horizontal, $color1, $color2, $edgeColor = -1) {
		callmethod("PlotArea.setAltBgColor", $this->ptr, $horizontal, $color1, $color2, $edgeColor);
	}
	function setGridColor($hGridColor, $vGridColor = Transparent, $minorHGridColor = -1, $minorVGridColor = -1) {
		callmethod("PlotArea.setGridColor", $this->ptr, $hGridColor, $vGridColor, $minorHGridColor, $minorVGridColor);
	}
	function setGridWidth($hGridWidth, $vGridWidth = -1, $minorHGridWidth = -1, $minorVGridWidth = -1) {
		callmethod("PlotArea.setGridWidth", $this->ptr, $hGridWidth, $vGridWidth, $minorHGridWidth, $minorVGridWidth);
	}
}

class XYChart extends BaseChart {
	function XYChart($width, $height, $bgColor = BackgroundColor, $edgeColor = Transparent, $raisedEffect = 0) {
		$this->ptr = callmethod("XYChart.create", $width, $height, $bgColor, $edgeColor, $raisedEffect);
		$this->xAxis = new Axis(callmethod("XYChart.xAxis", $this->ptr));
		$this->xAxis2 = new Axis(callmethod("XYChart.xAxis2", $this->ptr));
		$this->yAxis = new Axis(callmethod("XYChart.yAxis", $this->ptr));
		$this->yAxis2 = new Axis(callmethod("XYChart.yAxis2", $this->ptr));
		autoDestroy($this);
	}
	function addAxis($align, $offset) {
		return new Axis(callmethod("XYChart.addAxis", $this->ptr, $align, $offset));
	}
	function yAxis() {
		return new Axis(callmethod("XYChart.yAxis", $this->ptr));
	}
	function yAxis2() {
		return new Axis(callmethod("XYChart.yAxis2", $this->ptr));
	}
	function syncYAxis($slope = 1, $intercept = 0) {
		callmethod("XYChart.syncYAxis", $this->ptr, $slope, $intercept);
	}
	function setYAxisOnRight($b = 1) {
		callmethod("XYChart.setYAxisOnRight", $this->ptr, $b);
	}
	function xAxis() {
		return new Axis(callmethod("XYChart.xAxis", $this->ptr));
	}
	function xAxis2() {
		return new Axis(callmethod("XYChart.xAxis2", $this->ptr));
	}
	function setXAxisOnTop($b = 1) {
		callmethod("XYChart.setXAxisOnTop", $this->ptr, $b);
	}
	function swapXY($b = 1) {
		callmethod("XYChart.swapXY", $this->ptr, $b);
	}
	function setAxisAtOrigin($originMode = XYAxisAtOrigin, $symmetryMode = 0) {
		callmethod("XYChart.setAxisAtOrigin", $this->ptr, $originMode, $symmetryMode);
	}

	function getXCoor($v) {
		return callmethod("XYChart.getXCoor", $this->ptr, $v);
	}
	function getYCoor($v, $yAxis = Null) {
		return callmethod("XYChart.getYCoor", $this->ptr, $v, decodePtr($yAxis));
	}
	function xZoneColor($threshold, $belowColor, $aboveColor) {
		return callmethod("XYChart.xZoneColor", $this->ptr, $threshold, $belowColor, $aboveColor);
	}
	function yZoneColor($threshold, $belowColor, $aboveColor, $axis = Null) {
		return callmethod("XYChart.yZoneColor", $this->ptr, $threshold, $belowColor, $aboveColor, decodePtr($axis));
	}

	function setPlotArea($x, $y, $width, $height, $bgColor = Transparent, $altBgColor = -1,
	$edgeColor = -1, $hGridColor = 0xc0c0c0, $vGridColor = Transparent) {
		return new PlotArea(callmethod("XYChart.setPlotArea", $this->ptr,
		$x, $y, $width, $height, $bgColor, $altBgColor, $edgeColor, $hGridColor, $vGridColor));
	}
	function setClipping($margin = 0) {
		callmethod("XYChart.setClipping", $this->ptr, $margin);
	}
	function setTrimData($startPos, $len = 0x7fffffff) {
		callmethod("XYChart.setTrimData", $this->ptr, $startPos, $len);
	}

	function addBarLayer($data = Null, $color = -1, $name = "", $depth = 0) {
		if ($data != Null)
		return new BarLayer(callmethod("XYChart.addBarLayer", $this->ptr, $data, $color, $name, $depth));
		else
		return $this->addBarLayer2();
	}
	function addBarLayer2($dataCombineMethod = Side, $depth = 0) {
		return new BarLayer(callmethod("XYChart.addBarLayer2", $this->ptr, $dataCombineMethod, $depth));
	}
	function addBarLayer3($data, $colors = Null, $names = Null, $depth = 0) {
		return new BarLayer(callmethod("XYChart.addBarLayer3", $this->ptr, $data, $colors, $names, $depth));
	}
	function addLineLayer($data = Null, $color = -1, $name = "", $depth = 0) {
		if ($data != Null)
		return new LineLayer(callmethod("XYChart.addLineLayer", $this->ptr, $data, $color, $name, $depth));
		else
		return $this->addLineLayer2();
	}
	function addLineLayer2($dataCombineMethod = Overlay, $depth = 0) {
		return new LineLayer(callmethod("XYChart.addLineLayer2", $this->ptr, $dataCombineMethod, $depth));
	}
	function addAreaLayer($data = Null, $color = -1, $name = "", $depth = 0) {
		if ($data != Null)
		return new AreaLayer(callmethod("XYChart.addAreaLayer", $this->ptr, $data, $color, $name, $depth));
		else
		return $this->addAreaLayer2();
	}
	function addAreaLayer2($dataCombineMethod = Stack, $depth = 0) {
		return new AreaLayer(callmethod("XYChart.addAreaLayer2", $this->ptr, $dataCombineMethod, $depth));
	}
	function addHLOCLayer($highData = Null, $lowData = Null, $openData = Null, $closeData = Null, $color = -1) {
		if ($highData != Null)
		return $this->addHLOCLayer3($highData, $lowData, $openData, $closeData, $color, $color);
		else
		return $this->addHLOCLayer2();
	}
	function addHLOCLayer2() {
		return new HLOCLayer(callmethod("XYChart.addHLOCLayer2", $this->ptr));
	}
	function addHLOCLayer3($highData, $lowData, $openData, $closeData, $upColor, $downColor, $colorMode = -1, $leadValue = -1.7E308) {
		return new HLOCLayer(callmethod("XYChart.addHLOCLayer3", $this->ptr, $highData, $lowData, $openData, $closeData, $upColor, $downColor, $colorMode, $leadValue));
	}
	function addScatterLayer($xData, $yData, $name = "", $symbol = SquareSymbol, $symbolSize = 5, $fillColor = -1, $edgeColor = -1) {
		return new ScatterLayer(callmethod("XYChart.addScatterLayer", $this->ptr, $xData, $yData, $name, $symbol, $symbolSize, $fillColor, $edgeColor));
	}
	function addCandleStickLayer($highData, $lowData, $openData, $closeData, $riseColor = 0xffffff, $fallColor = 0x0, $edgeColor = LineColor) {
		return new CandleStickLayer(callmethod("XYChart.addCandleStickLayer", $this->ptr, $highData, $lowData, $openData, $closeData, $riseColor, $fallColor, $edgeColor));
	}
	function addBoxWhiskerLayer($boxTop, $boxBottom, $maxData = Null, $minData = Null, $midData = Null, $fillColor = -1, $whiskerColor = LineColor, $edgeColor = LineColor) {
		return new BoxWhiskerLayer(callmethod("XYChart.addBoxWhiskerLayer", $this->ptr, $boxTop, $boxBottom, $maxData, $minData, $midData, $fillColor, $whiskerColor, $edgeColor));
	}
	function addBoxWhiskerLayer2($boxTop, $boxBottom, $maxData = Null, $minData = Null, $midData = Null, $fillColors = Null, $whiskerBrightness = 0.5, $names = Null) {
		return new BoxWhiskerLayer(callmethod("XYChart.addBoxWhiskerLayer2", $this->ptr, $boxTop, $boxBottom, $maxData, $minData, $midData, $fillColors, $whiskerBrightness, $names));
	}
	function addBoxLayer($boxTop, $boxBottom, $color = -1, $name = "") {
		return new BoxWhiskerLayer(callmethod("XYChart.addBoxLayer", $this->ptr, $boxTop, $boxBottom, $color, $name));
	}
	function addTrendLayer($data, $color = -1, $name = "", $depth = 0) {
		return new TrendLayer(callmethod("XYChart.addTrendLayer", $this->ptr, $data, $color, $name, $depth));
	}
	function addTrendLayer2($xData, $yData, $color = -1, $name = "", $depth = 0) {
		return new TrendLayer(callmethod("XYChart.addTrendLayer2", $this->ptr, $xData, $yData, $color, $name, $depth));
	}
	function addSplineLayer($data = Null, $color = -1, $name = "") {
		return new SplineLayer(callmethod("XYChart.addSplineLayer", $this->ptr, $data, $color, $name));
	}
	function addStepLineLayer($data = Null, $color = -1, $name = "") {
		return new StepLineLayer(callmethod("XYChart.addStepLineLayer", $this->ptr, $data, $color, $name));
	}
	function addInterLineLayer($line1, $line2, $color12, $color21 = -1) {
		return new InterLineLayer(callmethod("XYChart.addInterLineLayer", $this->ptr, $line1, $line2, $color12, $color21));
	}
	function addVectorLayer($xData, $yData, $lengths, $directions, $lengthScale = PixelScale, $color = -1, $name = "") {
		return new VectorLayer(callmethod("XYChart.addVectorLayer", $this->ptr, $xData, $yData, $lengths, $directions, $lengthScale, $color, $name));
	}
}

#///////////////////////////////////////////////////////////////////////////////////
#//	bindings to polarchart.h
#///////////////////////////////////////////////////////////////////////////////////
class PolarLayer
{
	function PolarLayer($ptr) {
		$this->ptr = $ptr;
	}
	function setData($data, $color = -1, $name = "") {
		callmethod("PolarLayer.setData", $this->ptr, $data, $color, $name);
	}
	function setAngles($angles) {
		callmethod("PolarLayer.setAngles", $this->ptr, $angles);
	}

	function setBorderColor($edgeColor) {
		callmethod("PolarLayer.setBorderColor", $this->ptr, $edgeColor);
	}
	function setLineWidth($w) {
		callmethod("PolarLayer.setLineWidth", $this->ptr, $w);
	}

	function setDataSymbol($symbol, $size = Null, $fillColor = -1, $edgeColor = -1, $lineWidth = 1) {
		if (is_array($symbol)) {
			if (is_null($size))
			$size = 11;
			$this->setDataSymbol4($symbol, $size, $fillColor, $edgeColor);
			return;
		}
		if (!is_numeric($symbol))
		return $this->setDataSymbol2($symbol);
		if (is_null($size))
		$size = 7;
		callmethod("PolarLayer.setDataSymbol", $this->ptr, $symbol, $size, $fillColor, $edgeColor, $lineWidth);
	}
	function setDataSymbol2($image) {
		if (!is_string($image))
		return $this->setDataSymbol3($image);
		callmethod("PolarLayer.setDataSymbol2", $this->ptr, $image);
	}
	function setDataSymbol3($image) {
		callmethod("PolarLayer.setDataSymbol3", $this->ptr, $image->ptr);
	}
	function setDataSymbol4($polygon, $size = 11, $fillColor = -1, $edgeColor = -1) {
		callmethod("PolarLayer.setDataSymbol4", $this->ptr, $polygon, $size, $fillColor, $edgeColor);
	}
	function setSymbolScale($zData, $scaleType = PixelScale) {
		callmethod("PolarLayer.setSymbolScale", $this->ptr, $zData, $scaleType);
	}

	function setImageMapWidth($width) {
		callmethod("PolarLayer.setImageMapWidth", $this->ptr, $width);
	}
	function getImageCoor($dataItem, $offsetX = 0, $offsetY = 0) {
		return callmethod("PolarLayer.getImageCoor", $this->ptr, $dataItem, $offsetX, $offsetY);
	}
	function getHTMLImageMap($url, $queryFormat = "", $extraAttr = "", $offsetX = 0, $offsetY = 0) {
		return callmethod("PolarLayer.getHTMLImageMap", $this->ptr, $url, $queryFormat, $extraAttr, $offsetX, $offsetY);
	}
	function setHTMLImageMap($url, $queryFormat = "", $extraAttr = "") {
		callmethod("PolarLayer.setHTMLImageMap", $this->ptr, $url, $queryFormat, $extraAttr);
	}
	function setDataLabelFormat($formatString) {
		callmethod("PolarLayer.setDataLabelFormat", $this->ptr, $formatString);
	}
	function setDataLabelStyle($font = "", $fontSize = 8, $fontColor = TextColor, $fontAngle = 0) {
		return new TextBox(callmethod("PolarLayer.setDataLabelStyle", $this->ptr, $font, $fontSize, $fontColor, $fontAngle));
	}
	function addCustomDataLabel($i, $label, $font = "", $fontSize = 8, $fontColor = TextColor, $fontAngle = 0) {
		return new TextBox(callmethod("PolarLayer.addCustomDataLabel", $this->ptr, $i, $label, $font, $fontSize, $fontColor, $fontAngle));
	}
}

class PolarAreaLayer extends PolarLayer {
	function PolarAreaLayer($ptr) {
		$this->ptr = $ptr;
	}
}

class PolarLineLayer extends PolarLayer {
	function PolarLineLayer($ptr) {
		$this->ptr = $ptr;
	}
	function setCloseLoop($b) {
		callmethod("PolarLineLayer.setCloseLoop", $this->ptr, $b);
	}
	function setGapColor($lineColor, $lineWidth = -1) {
		callmethod("PolarLineLayer.setGapColor", $this->ptr, $lineColor, $lineWidth);
	}
}

class PolarSplineLineLayer extends PolarLineLayer {
	function PolarSplineLineLayer($ptr) {
		$this->ptr = $ptr;
	}
	function setTension($tension) {
		callmethod("PolarSplineLineLayer.setTension", $this->ptr, $tension);
	}
}

class PolarSplineAreaLayer extends PolarAreaLayer {
	function PolarSplineAreaLayer($ptr) {
		$this->ptr = $ptr;
	}
	function setTension($tension) {
		callmethod("PolarSplineAreaLayer.setTension", $this->ptr, $tension);
	}
}

class PolarVectorLayer extends PolarLayer
{
	function PolarVectorLayer($ptr) {
		$this->ptr = $ptr;
	}
	function setVector($lengths, $directions, $lengthScale = PixelScale) {
		callmethod("PolarVectorLayer.setVector", $this->ptr, $lengths, $directions, $lengthScale);
	}
	function setArrowHead($width, $height = 0) {
		if (is_array($width))
		$this->setArrowHead2($width);
		else
		callmethod("PolarVectorLayer.setArrowHead", $this->ptr, $width, $height);
	}
	function setArrowHead2($polygon) {
		callmethod("PolarVectorLayer.setArrowHead2", $this->ptr, $polygon);
	}
	function setArrowStem($polygon) {
		callmethod("PolarVectorLayer.setArrowStem", $this->ptr, $polygon);
	}
	function setArrowAlignment($alignment) {
		callmethod("PolarVectorLayer.setArrowAlignment", $this->ptr, $alignment);
	}
	function setIconSize($height, $width = 0) {
		callmethod("PolarVectorLayer.setIconSize", $this->ptr, $height, $width);
	}
}

class PolarChart extends BaseChart
{
	function PolarChart($width, $height, $bgColor = BackgroundColor, $edgeColor = Transparent, $raisedEffect = 0) {
		$this->ptr = callmethod("PolarChart.create", $width, $height, $bgColor, $edgeColor, $raisedEffect);
		$this->angularAxis = new AngularAxis(callmethod("PolarChart.angularAxis", $this->ptr));
		$this->radialAxis = new Axis(callmethod("PolarChart.radialAxis", $this->ptr));
		autoDestroy($this);
	}
	function setPlotArea($x, $y, $r, $bgColor = Transparent, $edgeColor = Transparent, $edgeWidth = 1) {
		callmethod("PolarChart.setPlotArea", $this->ptr, $x, $y, $r, $bgColor, $edgeColor, $edgeWidth);
	}
	function setPlotAreaBg($bgColor1, $bgColor2 = -1, $altRings = 1) {
		callmethod("PolarChart.setPlotAreaBg", $this->ptr, $bgColor1, $bgColor2, $altRings);
	}
	function setGridColor($rGridColor = 0x80000000, $rGridWidth = 1, $aGridColor = 0x80000000, $aGridWidth = 1) {
		callmethod("PolarChart.setGridColor", $this->ptr, $rGridColor, $rGridWidth, $aGridColor, $aGridWidth);
	}
	function setGridStyle($polygonGrid, $gridOnTop = 1) {
		callmethod("PolarChart.setGridStyle", $this->ptr, $polygonGrid, $gridOnTop);
	}
	function setStartAngle($startAngle, $clockwise = 1) {
		callmethod("PolarChart.setStartAngle", $this->ptr, $startAngle, $clockwise);
	}

	function angularAxis() {
		return new AngularAxis(callmethod("PolarChart.angularAxis", $this->ptr));
	}
	function radialAxis() {
		return new Axis(callmethod("PolarChart.radialAxis", $this->ptr));
	}
	function getXCoor($r, $a) {
		return callmethod("PolarChart.getXCoor", $this->ptr, $r, $a);
	}
	function getYCoor($r, $a) {
		return callmethod("PolarChart.getYCoor", $this->ptr, $r, $a);
	}

	function addAreaLayer($data, $color = -1, $name = "") {
		return new PolarAreaLayer(callmethod("PolarChart.addAreaLayer", $this->ptr, $data, $color, $name));
	}
	function addLineLayer($data, $color = -1, $name = "") {
		return new PolarLineLayer(callmethod("PolarChart.addLineLayer", $this->ptr, $data, $color, $name));
	}
	function addSplineLineLayer($data, $color = -1, $name = "") {
		return new PolarSplineLineLayer(callmethod("PolarChart.addSplineLineLayer", $this->ptr, $data, $color, $name));
	}
	function addSplineAreaLayer($data, $color = -1, $name = "") {
		return new PolarSplineAreaLayer(callmethod("PolarChart.addSplineAreaLayer", $this->ptr, $data, $color, $name));
	}
	function addVectorLayer($rData, $aData, $lengths, $directions, $lengthScale = PixelScale, $color = -1, $name = "") {
		return new PolarVectorLayer(callmethod("PolarChart.addVectorLayer", $this->ptr, $rData, $aData, $lengths, $directions, $lengthScale, $color, $name));
	}
}

class MeterPointer
{
	function MeterPointer($ptr) {
		$this->ptr = $ptr;
	}
	function setColor($fillColor, $edgeColor = -1) {
		callmethod("MeterPointer.setColor", $this->ptr, $fillColor, $edgeColor);
	}
	function setPos($value) {
		callmethod("MeterPointer.setPos", $this->ptr, $value);
	}
	function setShape($pointerType, $lengthRatio = NoValue, $widthRatio = NoValue) {
		if (is_array($pointerType))
		$this->setShape2($pointerType, $lengthRatio, $widthRatio);
		else
		callmethod("MeterPointer.setShape", $this->ptr, $pointerType, $lengthRatio, $widthRatio);
	}
	function setShape2($pointerCoor, $lengthRatio = NoValue, $widthRatio = NoValue) {
		callmethod("MeterPointer.setShape2", $this->ptr, $pointerCoor, $lengthRatio, $widthRatio);
	}
	function setZOrder($z) {
		callmethod("MeterPointer.setZOrder", $this->ptr, $z);
	}
}

class BaseMeter extends BaseChart
{
	function addPointer($value, $fillColor = LineColor, $edgeColor = -1) {
		return new MeterPointer(callmethod("BaseMeter.addPointer", $this->ptr, $value, $fillColor, $edgeColor));
	}
	function setScale($lowerLimit, $upperLimit, $majorTickInc = 0, $minorTickInc = 0, $microTickInc = 0) {
		if (is_array($majorTickInc)) {
			if ($minorTickInc != 0)
			$this->setScale3($lowerLimit, $upperLimit, $majorTickInc, $minorTickInc);
			else
			$this->setScale2($lowerLimit, $upperLimit, $majorTickInc);
		} else
		callmethod("BaseMeter.setScale", $this->ptr, $lowerLimit, $upperLimit, $majorTickInc, $minorTickInc, $microTickInc);
	}
	function setScale2($lowerLimit, $upperLimit, $labels) {
		callmethod("BaseMeter.setScale2", $this->ptr, $lowerLimit, $upperLimit, $labels);
	}
	function setScale3($lowerLimit, $upperLimit, $labels, $formatString = "") {
		callmethod("BaseMeter.setScale3", $this->ptr, $lowerLimit, $upperLimit, $labels, $formatString);
	}
	function addLabel($pos, $label) {
		callmethod("BaseMeter.addLabel", $this->ptr, $pos, $label);
	}
	function getLabel($i) {
		return callmethod("BaseMeter.getLabel", $this->ptr, $i);
	}
	function getTicks() {
		return callmethod("BaseMeter.getTicks", $this->ptr);
	}
	function setLabelStyle($font = "bold", $fontSize = -1, $fontColor = TextColor, $fontAngle = 0) {
		return new TextBox(callmethod("BaseMeter.setLabelStyle", $this->ptr, $font, $fontSize, $fontColor, $fontAngle));
	}
	function setLabelPos($labelInside, $labelOffset = 0) {
		callmethod("BaseMeter.setLabelPos", $this->ptr, $labelInside, $labelOffset);
	}
	function setLabelFormat($formatString) {
		callmethod("BaseMeter.setLabelFormat", $this->ptr, $formatString);
	}
	function setTickLength($majorLen, $minorLen = -0x7fffffff, $microLen = -0x7fffffff) {
		callmethod("BaseMeter.setTickLength", $this->ptr, $majorLen, $minorLen, $microLen);
	}
	function setLineWidth($axisWidth, $majorTickWidth = 1, $minorTickWidth = 1, $microTickWidth = 1) {
		callmethod("BaseMeter.setLineWidth", $this->ptr, $axisWidth, $majorTickWidth, $minorTickWidth, $microTickWidth);
	}
	function setMeterColors($axisColor, $labelColor = -1, $tickColor = -1) {
		callmethod("BaseMeter.setMeterColors", $this->ptr, $axisColor, $labelColor, $tickColor);
	}
	function getCoor($v) {
		return callmethod("BaseMeter.getCoor", $this->ptr, $v);
	}
}

class AngularMeter extends BaseMeter
{
	function AngularMeter($width, $height, $bgColor = BackgroundColor, $edgeColor = Transparent, $raisedEffect = 0) {
		$this->ptr = callmethod("AngularMeter.create", $width, $height, $bgColor, $edgeColor, $raisedEffect);
		autoDestroy($this);
	}
	function addRing($startRadius, $endRadius, $fillColor, $edgeColor = -1) {
		callmethod("AngularMeter.addRing", $this->ptr, $startRadius, $endRadius, $fillColor, $edgeColor);
	}
	function addRingSector($startRadius, $endRadius, $a1, $a2, $fillColor, $edgeColor = -1) {
		callmethod("AngularMeter.addRingSector", $this->ptr, $startRadius, $endRadius, $a1, $a2, $fillColor, $edgeColor);
	}
	function setCap($radius, $fillColor, $edgeColor = LineColor) {
		callmethod("AngularMeter.setCap", $this->ptr, $radius, $fillColor, $edgeColor);
	}
	function setMeter($cx, $cy, $radius, $startAngle, $endAngle) {
		callmethod("AngularMeter.setMeter", $this->ptr, $cx, $cy, $radius, $startAngle, $endAngle);
	}
	function addZone($startValue, $endValue, $startRadius, $endRadius = -1, $fillColor = Null, $edgeColor = -1) {
		if (is_null($fillColor))
		$this->addZone2($startValue, $endValue, $startRadius, $endRadius);
		else
		callmethod("AngularMeter.addZone", $this->ptr, $startValue, $endValue, $startRadius, $endRadius, $fillColor, $edgeColor);
	}
	function addZone2($startValue, $endValue, $fillColor, $edgeColor = -1) {
		callmethod("AngularMeter.addZone2", $this->ptr, $startValue, $endValue, $fillColor, $edgeColor);
	}
}

class LinearMeter extends BaseMeter
{
	function LinearMeter($width, $height, $bgColor = BackgroundColor, $edgeColor = Transparent, $raisedEffect = 0) {
		$this->ptr = callmethod("LinearMeter.create", $width, $height, $bgColor, $edgeColor, $raisedEffect);
		autoDestroy($this);
	}
	function setMeter($leftX, $topY, $width, $height, $axisPos = Left, $isReversed = 0) {
		callmethod("LinearMeter.setMeter", $this->ptr, $leftX, $topY, $width, $height, $axisPos, $isReversed);
	}
	function setRail($railColor, $railWidth = 2, $railOffset = 6) {
		callmethod("LinearMeter.setRail", $this->ptr, $railColor, $railWidth, $railOffset);
	}
	function addZone($startValue, $endValue, $color, $label = "") {
		return new TextBox(callmethod("LinearMeter.addZone", $this->ptr, $startValue, $endValue, $color, $label));
	}
}

#///////////////////////////////////////////////////////////////////////////////////
#//	bindings to chartdir.h
#///////////////////////////////////////////////////////////////////////////////////
function getCopyright() {
	return callmethod("getCopyright");
}

function getVersion() {
	return callmethod("getVersion");
}

function getDescription() {
	return cdFilterMsg(callmethod("getDescription"));
}

function getBootLog() {
	return cdFilterMsg(callmethod("getBootLog"));
}

function libgTTFTest($font = "", $fontIndex = 0, $fontHeight = 8, $fontWidth = 8, $angle = 0) {
	return cdFilterMsg(callmethod("testFont", $font, $fontIndex, $fontHeight, $fontWidth, $angle));
}

function testFont($font = "", $fontIndex = 0, $fontHeight = 8, $fontWidth = 8, $angle = 0) {
	return cdFilterMsg(callmethod("testFont", $font, $fontIndex, $fontHeight, $fontWidth, $angle));
}

function setLicenseCode($licCode) {
	return callmethod("setLicenseCode", $licCode);
}

function chartTime($y, $m = Null, $d = 1, $h = 0, $n = 0, $s = 0) {
	if (is_null($m))
	return chartTime2($y);
	else
	return callmethod("chartTime", $y, $m, $d, $h, $n, $s);
}

function chartTime2($t) {
	return callmethod("chartTime2", $t);
}

function getChartYMD($t) {
	return callmethod("getChartYMD", $t);
}

function getChartWeekDay($t) {
	return ((int)($t / 86400 + 1)) % 7;
}

#///////////////////////////////////////////////////////////////////////////////////
#//	bindings to rantable.h
#///////////////////////////////////////////////////////////////////////////////////
class RanTable
{
	function RanTable($seed, $noOfCols, $noOfRows) {
		$this->ptr = callmethod("RanTable.create", $seed, $noOfCols, $noOfRows);
		autoDestroy($this);
	}
	function __del__() {
		callmethod("RanTable.destroy", $this->ptr);
	}

	function setCol($colNo, $minValue, $maxValue, $p4 = Null, $p5 = -1E+308, $p6 = 1E+308) {
		if (is_null($p4))
		callmethod("RanTable.setCol", $this->ptr, $colNo, $minValue, $maxValue);
		else
		$this->setCol2($colNo, $minValue, $maxValue, $p4, $p5, $p6);
	}
	function setCol2($colNo, $startValue, $minDelta, $maxDelta, $lowerLimit = -1E+308, $upperLimit = 1E+308) {
		callmethod("RanTable.setCol2", $this->ptr, $colNo, $startValue, $minDelta, $maxDelta, $lowerLimit, $upperLimit);
	}
	function setDateCol($i, $startTime, $tickInc, $weekDayOnly = 0) {
		callmethod("RanTable.setDateCol", $this->ptr, $i, $startTime, $tickInc, $weekDayOnly);
	}
	function setHLOCCols($i, $startValue, $minDelta, $maxDelta,	$lowerLimit = 0, $upperLimit = 1E+308) {
		callmethod("RanTable.setHLOCCols", $this->ptr, $i, $startValue, $minDelta, $maxDelta, $lowerLimit, $upperLimit);
	}
	function selectDate($colNo, $minDate, $maxDate)	{
		return callmethod("RanTable.selectDate", $this->ptr, $colNo, $minDate, $maxDate);
	}
	function getCol($i) {
		return callmethod("RanTable.getCol", $this->ptr, $i);
	}
}

class FinanceSimulator
{
	function FinanceSimulator($seed, $startTime, $endTime, $resolution) {
		$this->ptr = callmethod("FinanceSimulator.create", $seed, $startTime, $endTime, $resolution);
		autoDestroy($this);
	}
	function __del__() {
		callmethod("FinanceSimulator.destroy", $this->ptr);
	}
	function getTimeStamps() {
		return callmethod("FinanceSimulator.getTimeStamps", $this->ptr);
	}
	function getHighData() {
		return callmethod("FinanceSimulator.getHighData", $this->ptr);
	}
	function getLowData() {
		return callmethod("FinanceSimulator.getLowData", $this->ptr);
	}
	function getOpenData() {
		return callmethod("FinanceSimulator.getOpenData", $this->ptr);
	}
	function getCloseData() {
		return callmethod("FinanceSimulator.getCloseData", $this->ptr);
	}
	function getVolData() {
		return callmethod("FinanceSimulator.getVolData", $this->ptr);
	}
}

#///////////////////////////////////////////////////////////////////////////////////
#//	bindings to datafilter.h
#///////////////////////////////////////////////////////////////////////////////////
class ArrayMath
{
	function ArrayMath($a) {
		$this->ptr = callmethod("ArrayMath.create", $a);
		autoDestroy($this);
	}
	function __del__() {
		callmethod("ArrayMath.destroy", $this->ptr);
	}

	function add($b) {
		if (!is_array($b))
		$this->add2($b);
		else
		callmethod("ArrayMath.add", $this->ptr, $b);
		return $this;
	}
	function add2($b) {
		callmethod("ArrayMath.add2", $this->ptr, $b);
		return $this;
	}
	function sub($b) {
		if (!is_array($b))
		$this->sub2($b);
		else
		callmethod("ArrayMath.sub", $this->ptr, $b);
		return $this;
	}
	function sub2($b) {
		callmethod("ArrayMath.sub2", $this->ptr, $b);
		return $this;
	}
	function mul($b) {
		if (!is_array($b))
		$this->mul2($b);
		else
		callmethod("ArrayMath.mul", $this->ptr, $b);
		return $this;
	}
	function mul2($b) {
		callmethod("ArrayMath.mul2", $this->ptr, $b);
		return $this;
	}
	function div($b) {
		if (!is_array($b))
		$this->div2($b);
		else
		callmethod("ArrayMath.div", $this->ptr, $b);
		return $this;
	}
	function div2($b) {
		callmethod("ArrayMath.div2", $this->ptr, $b);
		return $this;
	}
	function financeDiv($b, $zeroByZeroValue) {
		callmethod("ArrayMath.financeDiv", $this->ptr, $b, $zeroByZeroValue);
		return $this;
	}
	function shift($offset = 1, $fillValue = NoValue) {
		callmethod("ArrayMath.shift", $this->ptr, $offset, $fillValue);
		return $this;
	}
	function delta($offset = 1) {
		callmethod("ArrayMath.delta", $this->ptr, $offset);
		return $this;
	}
	function rate($offset = 1) {
		callmethod("ArrayMath.rate", $this->ptr, $offset);
		return $this;
	}
	function abs() {
		callmethod("ArrayMath.abs", $this->ptr);
		return $this;
	}
	function acc() {
		callmethod("ArrayMath.acc", $this->ptr);
		return $this;
	}

	function selectGTZ($b = Null, $fillValue = 0) { callmethod("ArrayMath.selectGTZ", $this->ptr, $b, $fillValue); return $this; }
	function selectGEZ($b = Null, $fillValue = 0) { callmethod("ArrayMath.selectGEZ", $this->ptr, $b, $fillValue); return $this; }
	function selectLTZ($b = Null, $fillValue = 0) { callmethod("ArrayMath.selectLTZ", $this->ptr, $b, $fillValue); return $this; }
	function selectLEZ($b = Null, $fillValue = 0) { callmethod("ArrayMath.selectLEZ", $this->ptr, $b, $fillValue); return $this; }
	function selectEQZ($b = Null, $fillValue = 0) { callmethod("ArrayMath.selectEQZ", $this->ptr, $b, $fillValue); return $this; }
	function selectNEZ($b = Null, $fillValue = 0) { callmethod("ArrayMath.selectNEZ", $this->ptr, $b, $fillValue); return $this; }

	function selectStartOfHour($majorTickStep = 1, $initialMargin = 300) {
		callmethod("ArrayMath.selectStartOfHour", $this->ptr, $majorTickStep, $initialMargin);
		return $this;
	}
	function selectStartOfDay($majorTickStep = 1, $initialMargin = 10800) {
		callmethod("ArrayMath.selectStartOfDay", $this->ptr, $majorTickStep, $initialMargin);
		return $this;
	}
	function selectStartOfWeek($majorTickStep = 1, $initialMargin = 172800) {
		callmethod("ArrayMath.selectStartOfWeek", $this->ptr, $majorTickStep, $initialMargin);
		return $this;
	}
	function selectStartOfMonth($majorTickStep = 1, $initialMargin = 432000) {
		callmethod("ArrayMath.selectStartOfMonth", $this->ptr, $majorTickStep, $initialMargin);
		return $this;
	}
	function selectStartOfYear($majorTickStep = 1, $initialMargin = 5184000) {
		callmethod("ArrayMath.selectStartOfYear", $this->ptr, $majorTickStep, $initialMargin);
		return $this;
	}
	function selectRegularSpacing($majorTickStep, $minorTickStep = 0, $initialMargin = 0) {
		callmethod("ArrayMath.selectRegularSpacing", $this->ptr, $majorTickStep, $minorTickStep, $initialMargin);
		return $this;
	}
		
	function trim($startIndex = 0, $len = -1) {
		callmethod("ArrayMath.trim", $this->ptr, $startIndex, $len);
		return $this;
	}
	function insert($a, $insertPoint = -1) {
		callmethod("ArrayMath.insert", $this->ptr, $a, $insertPoint);
		return $this;
	}
	function insert2($c, $len, $insertPoint= -1) {
		callmethod("ArrayMath.insert2", $this->ptr, $c, $len, $insertPoint);
		return $this;
	}
	function replace($a, $b) {
		callmethod("ArrayMath.replace", $this->ptr, $a, $b);
		return $this;
	}

	function movAvg($interval) {
		callmethod("ArrayMath.movAvg", $this->ptr, $interval);
		return $this;
	}
	function expAvg($smoothingFactor) {
		callmethod("ArrayMath.expAvg", $this->ptr, $smoothingFactor);
		return $this;
	}
	function movMed($interval) {
		callmethod("ArrayMath.movMed", $this->ptr, $interval);
		return $this;
	}
	function movPercentile($interval, $percentile) {
		callmethod("ArrayMath.movPercentile", $this->ptr, $interval, $percentile);
		return $this;
	}
	function movMax($interval) {
		callmethod("ArrayMath.movMax", $this->ptr, $interval);
		return $this;
	}
	function movMin($interval) {
		callmethod("ArrayMath.movMin", $this->ptr, $interval);
		return $this;
	}
	function movStdDev($interval) {
		callmethod("ArrayMath.movStdDev", $this->ptr, $interval);
		return $this;
	}
	function movCorr($interval, $b = Null) {
		callmethod("ArrayMath.movCorr", $this->ptr, $interval, $b);
		return $this;
	}
	function lowess($smoothness = 0.25, $iteration = 0) {
		callmethod("ArrayMath.lowess", $this->ptr, $smoothness, $iteration);
		return $this;
	}
	function lowess2($b, $smoothness = 0.25, $iteration = 0) {
		callmethod("ArrayMath.lowess2", $this->ptr, $b, $smoothness, $iteration);
		return $this;
	}

	function result() {
		return callmethod("ArrayMath.result", $this->ptr);
	}
	function max() {
		return callmethod("ArrayMath.max", $this->ptr);
	}
	function min() {
		return callmethod("ArrayMath.min", $this->ptr);
	}
	function avg() {
		return callmethod("ArrayMath.avg", $this->ptr);
	}
	function sum() {
		return callmethod("ArrayMath.sum", $this->ptr);
	}
	function stdDev() {
		return callmethod("ArrayMath.stdDev", $this->ptr);
	}
	function med() {
		return callmethod("ArrayMath.med", $this->ptr);
	}
	function percentile($p) {
		return callmethod("ArrayMath.percentile", $this->ptr, $p);
	}
	function maxIndex() {
		return callmethod("ArrayMath.maxIndex", $this->ptr);
	}
	function minIndex() {
		return callmethod("ArrayMath.minIndex", $this->ptr);
	}

	function aggregate($srcArray, $aggregateMethod, $param = 50) {
		return callmethod("ArrayMath.aggregate", $this->ptr, $srcArray, $aggregateMethod, $param);
	}
}

#///////////////////////////////////////////////////////////////////////////////////
#//	WebChartViewer implementation
#///////////////////////////////////////////////////////////////////////////////////
define("MouseUsageDefault", 0);
define("MouseUsageScroll", 2);
define("MouseUsageZoomIn", 3);
define("MouseUsageZoomOut", 4);

define("DirectionHorizontal", 0);
define("DirectionVertical", 1);
define("DirectionHorizontalVertical", 2);

class WebChartViewer
{
	function WebChartViewer($id) {
		global $_REQUEST;
		$this->ptr = callmethod("WebChartViewer.create");
		autoDestroy($this);
		$this->putAttrS(":id", $id);
		$s = $id."_JsChartViewerState";
		if (isset($_REQUEST[$s]))
		$this->putAttrS(":state", $_REQUEST[$s]);
	}
	function __del__() {
		callmethod("WebChartViewer.destroy", $this->ptr);
	}

	function getId() { return $this->getAttrS(":id"); }

	function setImageUrl($url) { $this->putAttrS(":url", $url); }
	function getImageUrl() { return $this->getAttrS(":url"); }

	function setImageMap($imageMap) { $this->putAttrS(":map", $imageMap); }
	function getImageMap() { return $this->getAttrS(":map"); }

	function setChartMetrics($metrics) { $this->putAttrS(":metrics", $metrics); }
	function getChartMetrics() { return $this->getAttrS(":metrics"); }

	function makeDelayedMap($imageMap, $compress = 0) {
		global $HTTP_SESSION_VARS, $_SERVER;
		if ($compress) {
			if (!isset($_SERVER['HTTP_ACCEPT_ENCODING']) || !strstr($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip'))
			$compress = 0;
		}

		$mapId = $this->getId()."_map";
		session_register($mapId);

		$b = "<body><!--CD_MAP $imageMap CD_MAP--></body>";
		if ($compress)
		$b = callmethod("WebChartViewer.compressMap", $this->ptr, $b, 4);

		if (isset($HTTP_SESSION_VARS))
		$HTTP_SESSION_VARS[$mapId] = $GLOBALS[$mapId] = $b;
		else
		$_SESSION[$mapId] = $GLOBALS[$mapId] = $b;

		return "img=".$mapId."&isMap=1&id=".uniqid(session_id())."&".SID;
	}

	function renderHTML($extraAttrs = null) {
		global $_SERVER;
		$url = isset($_SERVER["SCRIPT_NAME"]) ? $_SERVER["SCRIPT_NAME"] : "";
		$query = isset($_SERVER["QUERY_STRING"]) ? $_SERVER["QUERY_STRING"] : "";
		return callmethod("WebChartViewer.renderHTML", $this->ptr, $url, $query, $extraAttrs);
	}
	function partialUpdateChart($msg = null, $timeout = 0) {
		header("Content-type: text/html; charset=utf-8");
		return callmethod("WebChartViewer.partialUpdateChart", $this->ptr, $msg, $timeout);
	}
	function isPartialUpdateRequest() {	global $_REQUEST; return isset($_REQUEST["cdPartialUpdate"]); }
	function isFullUpdateRequest() {
		if ($this->isPartialUpdateRequest())
		return 0;
		global $_REQUEST;
		$s = "_JsChartViewerState";
		foreach($_REQUEST as $k => $v) {
			if (substr($k, -strlen($s)) == $s)
			return 1;
		}
		return 0;
	}
	function isStreamRequest() { global $_REQUEST; return isset($_REQUEST["cdDirectStream"]); }
	function isViewPortChangedEvent() {	return $this->getAttrF(25, 0) != 0; }
	function getSenderClientId() {
		global $_REQUEST;
		if ($this->isPartialUpdateRequest())
		return $_REQUEST["cdPartialUpdate"];
		elseif ($this->isStreamRequest())
		return $_REQUEST["cdDirectStream"];
		else
		return null;
	}

	function getAttrS($attr, $defaultValue = "") {
		return callmethod("WebChartViewer.getAttrS", $this->ptr, $attr, $defaultValue);
	}
	function getAttrF($attr, $defaultValue = 0) {
		return callmethod("WebChartViewer.getAttrF", $this->ptr, $attr, $defaultValue);
	}
	function putAttrF($attr, $value) {
		callmethod("WebChartViewer.putAttrF", $this->ptr, $attr, $value);
	}
	function putAttrS($attr, $value) {
		callmethod("WebChartViewer.putAttrS", $this->ptr, $attr, $value);
	}

	function getViewPortLeft() { return $this->getAttrF(4, 0); }
	function setViewPortLeft($left) { $this->putAttrF(4, $left); }

	function getViewPortTop() { return $this->getAttrF(5, 0); }
	function setViewPortTop($top) { $this->putAttrF(5, $top); }

	function getViewPortWidth() { return $this->getAttrF(6, 1); }
	function setViewPortWidth($width) { $this->putAttrF(6, $width); }

	function getViewPortHeight() { return $this->getAttrF(7, 1); }
	function setViewPortHeight($height) { $this->putAttrF(7, $height); }

	function getSelectionBorderWidth() { return (int)($this->getAttrF(8, 2)); }
	function setSelectionBorderWidth($lineWidth) { $this->putAttrF(8, $lineWidth); }

	function getSelectionBorderColor() { return $this->getAttrS(9, "Black"); }
	function setSelectionBorderColor($color) { $this->putAttrS(9, $color); }

	function getMouseUsage() { return (int)($this->getAttrF(10, MouseUsageDefault)); }
	function setMouseUsage($usage) { $this->putAttrF(10, $usage); }

	function getScrollDirection() { return (int)($this->getAttrF(11, DirectionHorizontal)); }
	function setScrollDirection($direction) { $this->putAttrF(11, $direction); }

	function getZoomDirection() { return (int)($this->getAttrF(12, DirectionHorizontal)); }
	function setZoomDirection($direction) { $this->putAttrF(12, $direction); }

	function getZoomInRatio() { return $this->getAttrF(13, 2); }
	function setZoomInRatio($ratio) { if ($ratio > 0) $this->putAttrF(13, $ratio); }

	function getZoomOutRatio() { return $this->getAttrF(14, 0.5); }
	function setZoomOutRatio($ratio) { if ($ratio > 0) $this->putAttrF(14, $ratio); }

	function getZoomInWidthLimit() { return $this->getAttrF(15, 0.01); }
	function setZoomInWidthLimit($limit) { $this->putAttrF(15, $limit); }

	function getZoomOutWidthLimit() { return $this->getAttrF(16, 1); }
	function setZoomOutWidthLimit($limit) { $this->putAttrF(16, $limit); }

	function getZoomInHeightLimit() { return $this->getAttrF(17, 0.01); }
	function setZoomInHeightLimit($limit) { $this->putAttrF(17, $limit); }

	function getZoomOutHeightLimit() { return $this->getAttrF(18, 1); }
	function setZoomOutHeightLimit($limit) { $this->putAttrF(18, $limit); }

	function getMinimumDrag() { return (int)($this->getAttrF(19, 5)); }
	function setMinimumDrag($offset) { $this->putAttrF(19, $offset); }

	function getZoomInCursor() { return $this->getAttrS(20, ""); }
	function setZoomInCursor($cursor) { $this->putAttrS(20, $cursor); }

	function getZoomOutCursor() { return $this->getAttrS(21, ""); }
	function setZoomOutCursor($cursor) { $this->putAttrS(21, $cursor); }

	function getScrollCursor() { return $this->getAttrS(22, ""); }
	function setScrollCursor($cursor) { $this->putAttrS(22, $cursor); }

	function getNoZoomCursor() { return $this->getAttrS(26, ""); }
	function setNoZoomCursor($cursor) { $this->putAttrS(26, $cursor); }

	function getCustomAttr($key) { return $this->getAttrS($key, ""); }
	function setCustomAttr($key, $value) { $this->putAttrS($key, $value); }
}

?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

$goRGBTable = array(
	    "aqua",
		"lime",
		"teal",
		"whitesmoke",	
		"gainsboro",
		"oldlace",
		"linen",
		"antiquewhite",
		"papayawhip",
		"blanchedalmond",
		"lightgray","gray8",
	    "bisque",
		"peachpuff",	
		"navajowhite",
		"moccasin",
		"cornsilk",
		"ivory",
		"lemonchiffon",	
		"seashell",
		"mintcream",
		"azure",
		"aliceblue",
		"lavender",
		"lavenderblush",
		"mistyrose",
		"white",
		"black",	
		"darkslategray",
		"dimgray",
		"slategray",
	    "lightslategray",
		"gray",
		"midnightblue",
		"navy",
		"cornflowerblue",
		"darkslateblue",
		"slateblue",
		"mediumslateblue",
	    "lightslateblue",
		"mediumblue",
		"royalblue",
		"blue",
		"dodgerblue",
		"deepskyblue",
		"skyblue",
		"lightskyblue",
		"steelblue",
	    "lightred",
		"lightsteelblue",
		"lightblue",
		"powderblue",
		"paleturquoise",
		"darkturquoise",
		"mediumturquoise",
		"turquoise",
	    "cyan",
		"lightcyan",
		"cadetblue",
		"mediumaquamarine",
		"aquamarine",
		"darkgreen",
		"darkolivegreen",
		"darkseagreen",
		"seagreen",
	    "mediumseagreen",
		"lightseagreen",
		"palegreen",
		"springgreen",
		"lawngreen",
		"green",
		"chartreuse",
		"mediumspringgreen",
		"greenyellow",
		"limegreen",
		"yellowgreen",
		"forestgreen",
		"olivedrab",
		"darkkhaki",
		"khaki",
		"palegoldenrod",
		"lightgoldenrodyellow",
	    "lightyellow",
		"yellow",
		"gold",
		"lightgoldenrod",
		"goldenrod",
		"darkgoldenrod",
		"rosybrown",
		"indianred",
		"saddlebrown",
	    "sienna",
		"peru",
		"burlywood",
		"beige",
		"wheat",
		"sandybrown",
		"tan",	
		"chocolate",
		"firebrick",
		"brown",
		"darksalmon",
	    "salmon",
		"lightsalmon",
		"orange",
		"darkorange",
		"coral",
		"lightcoral",
	    "tomato",
	    "orangered",
	    "red",
	    "hotpink",
	    "deeppink",
	    "pink",
	    "lightpink",
	    "palevioletred",
	    "maroon",
	    "mediumvioletred",
	    "violetred",
	    "magenta",
	    "violet",
	    "plum",
	    "orchid",
	    "mediumorchid",
	    "darkorchid",
	    "darkviolet",
	    "blueviolet",
	    "purple",
	    "mediumpurple",
	    "thistle",
	    "snow",
	    "seashell",
	    "AntiqueWhite",
	    "bisque",
	    "peachPuff",
	    "navajowhite",
	    "lemonchiffon",
	    "ivory",
	    "honeydew",
	    "lavenderblush",
	    "mistyrose",
	    "azure",
	    "slateblue",
	    "royalblue",
	    "dodgerblue",
	    "steelblue",
	    "deepskyblue",
	    "skyblue",
	    "lightskyblue",
	    "slategray",
	    "lightsteelblue",
	    "lightblue",
	    "lightcyan",
	    "paleturquoise",
	    "cadetblue",
	    "turquoise",
	    "cyan",
	    "darkslategray",
	    "aquamarine",
	    "darkseagreen",
	    "seagreen",
	    "palegreen",
	    "springgreen",
	    "chartreuse",
	    "olivedrab",
	    "darkolivegreen",
	    "khaki",
	    "lightgoldenrod",
	    "yellow",
	    "gold",
	    "goldenrod",
	    "darkgoldenrod",
	    "rosybrown",
	    "indianred",
	    "sienna",
	    "burlywood",
	    "wheat",
	    "tan",
	    "chocolate",
	    "firebrick",
	    "brown",
	    "salmon",
	    "lightsalmon",
	    "orange",
	    "darkorange",
	    "coral",
	    "tomato",
	    "orangered",
	    "deeppink",
	    "hotpink",
	    "pink",
	    "lightpink",
	    "palevioletred",
	    "maroon",
	    "violetred",
	    "magenta",
	    "mediumred",
	    "orchid",
	    "plum",
	    "mediumorchid",
	    "darkorchid",
	    "purple",
	    "mediumpurple",
	    "thistle",
	    "gray",
	    "darkgray",
	    "darkblue",
	    "darkcyan",
	    "darkmagenta",
	    "darkred",
	    "silver",
	    "eggplant",
	    "lightgreen",)

?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDActivationHandler. Classe para controle do arquivo de ativa��es.
 *
 * <p>Classe para controle do arquivo de ativa��es.</p>
 * @package FWD5
 * @subpackage base
 */

class FWDActivationHandler {

	/**
	 * Posi��o do primeiro byte onde ser� lido/gravado o hash no arquivo criptografado.
	 * Se este valor for modificado, os sistemas de clientes que forem atualizados
	 * precisar�o de nova ativa��o.
	 * O arquivo criptografado deve conter, seguindo essa posi��o, dois bytes cujo valor
	 * seja maior que o tamanho at� o final do arquivo. Isto �, se o arquivo possuir 1024 bytes,
	 * os dois bytes devem possuir valor maior que 1006 (0x03EE).
	 * @var constant integer
	 */
	const BYTE_TO_WRITE = 18;

	/**
	 * Ponteiro para arquivo criptografado, que cont�m ativa��es/desativa��o.
	 * @var object
	 * @access private
	 */
	private $coFile;

	/**
	 * Apontador para a posi��o atual de leitura do arquivo.
	 * @var integer
	 * @access private
	 */
	private $ciCurrPos;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDActivationHandler.</p>
	 * @access public
	 * @param string $psFile Arquivo criptogrado, ao qual ser�o gravadas as ativa��es e a desativa��o.
	 */
	public function __construct($psLicense) {
		if(!file_exists($psLicense) || !is_writable($psLicense)) return false;
		FWDWebLib::addKnownError('error_permission_denied',array('fopen'));
		$this->coFile = fopen($psLicense,'rb+'); //Carrega o arquivo e salva o ponteiro.
		FWDWebLib::removeKnownError('error_permission_denied');
		$this->ciCurrPos = self::BYTE_TO_WRITE; //Define o primeiro byte de escrita como posi��o atual.
	}

	/**
	 * Destrutor da classe.
	 *
	 * <p>Destrutor da classe FWDActivationHandler.
	 * "Fecha" o ponteiro para o arquivo.</p>
	 * @access public
	 */
	public function __destruct() {
		if ($this->coFile) fclose($this->coFile);
	}

	/**
	 * Pega o hash da posi��o atual e seta o apontador para a pr�xima posi��o de hash.
	 *
	 * <p>Pega o hash da posi��o atual e seta o apontador para a pr�xima posi��o de hash.</p>
	 * @access public
	 * @return string Hash da posi��o atual
	 */
	public function fetch() {
		$msHash = $this->getHash();
		$this->ciCurrPos += $this->getHashSize() + 2; // + 2 bytes indicadores de tamanho.
		return $msHash;
	}

	/**
	 * Pega o hash da posi��o atual.
	 *
	 * <p>Pega o hash da posi��o atual.</p>
	 * @access public
	 * @return string Hash da posi��o atual.
	 */
	public function getHash() {
		FWDWebLib::addKnownError('error_corrupt_activation_hash',array('gzuncompress'));
		$miHashSize = $this->getHashSize();
		if(!$miHashSize){
			FWDWebLib::removeKnownError('error_corrupt_activation_hash');
			return "";
		}
		fseek($this->coFile,$this->ciCurrPos+2);
		$msHash = fread($this->coFile,$miHashSize);
		$msHash = gzuncompress($msHash);
		FWDWebLib::removeKnownError('error_corrupt_activation_hash');
		return $msHash;
	}

	/**
	 * Pega o tamanho do hash da posi��o atual.
	 *
	 * <p>Pega o tamanho do hash da posi��o atual. Se o tamanho do hash
	 * for maior que o tamanho desta posi��o at� o final do arquivo,
	 * retorna 0, pois n�o h� hash nessa posi��o.</p>
	 * @access public
	 * @return integer Tamanho do hash da posi��o atual.
	 */
	public function getHashSize() {
		$maFileStat = fstat($this->coFile);
		fseek($this->coFile,$this->ciCurrPos);
		$miFirstByte = ord(fread($this->coFile,1)) <<8;
		$miSecondByte = ord(fread($this->coFile,1));
		$miLen = $miFirstByte+$miSecondByte;
		if ($miLen < $maFileStat['size'] - $this->ciCurrPos)
		return $miLen;
		else
		return 0;
	}

	/**
	 * Volta o apontador de hash atual para a primeira posi��o de hash.
	 *
	 * <p>Volta o apontador de hash atual para a primeira posi��o de hash.</p>
	 * @access public
	 */
	public function resetCurrPos() {
		$this->ciCurrPos = self::BYTE_TO_WRITE;
	}

	/**
	 * Insere um hash no arquivo criptografado.
	 *
	 * <p>Insere um hash no arquivo criptografado. Os hash s�o inseridos
	 * no in�cio do arquivo, na posi��o definida por BYTE_TO_WRITE.</p>
	 * @access public
	 * @param string psHash Hash que ser� inserido
	 * @return boolean True em caso de sucesso.
	 */
	public function insertHash($psHash) {
		$this->resetCurrPos();
		$psHash = gzcompress($psHash);
		$miHashLen = strlen($psHash);
		$miMSB = (0xFF00 & $miHashLen)>>8;
		$miLSB = 0x00FF & $miHashLen;
		$msBinaryLen = chr($miMSB).chr($miLSB);
		fseek($this->coFile,$this->ciCurrPos);
		$msPostBuffer = "";
		while(!feof($this->coFile)) {
			$msPostBuffer .= fread($this->coFile,8192);
		}
		fseek($this->coFile,$this->ciCurrPos);
		fwrite($this->coFile,$msBinaryLen);
		fwrite($this->coFile,$psHash);
		fwrite($this->coFile,$msPostBuffer);
		return true;
	}

	/**
	 * Limpa todos os registros de ativa��es/desativa��o do arquivo criptografado.
	 *
	 * <p>Limpa todos os registros de ativa��es/desativa��o do arquivo criptografado.
	 * O arquivo criptografado fica como se nunca houvesse sido feita alguma ativa��o.</p>
	 * @access public
	 */
	public function clearFile() {
		$miTotal=0;
		$this->resetCurrPos();
		do {
			if ($miLen = $this->getHashSize()) {
				$this->ciCurrPos += $miLen + 2;
			}
		} while($miLen);
		rewind($this->coFile);
		$msPrevBuffer = fread($this->coFile,self::BYTE_TO_WRITE);
		fseek($this->coFile,$this->ciCurrPos);
		$msPostBuffer = "";
		while(!feof($this->coFile)) {
			$msPostBuffer .= fread($this->coFile,8192);
		}
		ftruncate($this->coFile,0);
		rewind($this->coFile);
		fwrite($this->coFile,$msPrevBuffer);
		fwrite($this->coFile,$msPostBuffer);
	}
}
?>
<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDXmlLoad. Singleton. Transforma arquivos XML em objetos PHP.
 *
 * <p>Classe singleton que transforma arquivos XML em objetos PHP. Garante a
 * exist�ncia de uma e apenas uma inst�ncia da classe FWDXmlLoad.</p>
 * @package FWD5
 * @subpackage base
 */
class FWDXmlLoad {

	/**
	 * Inst�ncia �nica da classe FWDXmlLoad
	 * @staticvar FWDXmlLoad
	 * @access private
	 */
	private static $coXmlLoad = NULL;

	/**
	 * Contador para gerar ids �nicos
	 * @staticvar integer
	 * @access private
	 */
	private static $ciLastId = 0;

	/**
	 * Array de Views (usa ciXmlDepth para guardar informa��es de aninhamento/identa��o)
	 * @var array
	 * @access private
	 */
	private $caXmlRoot;

	/**
	 * Alvo da View
	 * @var array
	 * @access private
	 */
	private $caXmlTarget;

	/**
	 * Array de Views (todas as views da dialog)
	 * @var array
	 */
	private $caXmlObjects;

	/**
	 * Profundidade das tags xml (aninhamento/identa��o)
	 * @var integer
	 * @access private
	 */
	private $ciXmlDepth;

	/**
	 * Array de tags html
	 * @var array
	 * @access private
	 */
	private $caHtmlTags;

	/**
	 * Lista das classes que podem ter conte�do de texto
	 * @var array
	 * @access private
	 */
	private $caTextContainers;

	/**
	 * Vari�vel para verificar se o XMLLOAD est� sendo usado para compila��o
	 * @var integer
	 * @access private
	 */
	private $cbIsCompiling = false;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDXmlLoad.</p>
	 * @access private
	 */
	private function __construct() {
		$this->caXmlRoot = array();
		$this->caXmlTarget = array();
		$this->caXmlObjects = array();
		$this->caObjects = array();
		$this->ciXmlDepth = 0;
		$this->caHtmlTags = array( "img", "b", "br", "p", "div", "font", "i", "u", "a");
		$this->caTextContainers = array('FWDString','FWDClientEvent');
	}

	/**
	 * Retorna a inst�ncia �nica da classe FWDXmlLoad (singleton).
	 *
	 * <p>Retorna a inst�ncia �nica da classe FWDXmlLoad. Se a inst�ncia
	 * ainda n�o existir, cria a �nica inst�ncia. Se a inst�ncia j� existir,
	 * retorna a �nica inst�ncia (singleton).</p>
	 * @access public
	 * @return FWDXmlLoad Inst�ncia �nica da classe FWDXmlLoad
	 * @static
	 */
	public static function getInstance() {
		if (self::$coXmlLoad == NULL) {
			self::$coXmlLoad = new FWDXmlLoad();
		}
		else {}	// inst�ncia �nica j� existe, retorne-a
		return self::$coXmlLoad;
	}

	/**
	 * Limpa a inst�ncia �nica da classe FWDXmlLoad (singleton).
	 *
	 * <p>Limpa a inst�ncia �nica da classe FWDXmlLoad. Reseta as vari�veis
	 * do singleton FWDXmlLoad para os valores padr�o.</p>
	 * @access public
	 * @static
	 */
	public function cleanInstance() {
		$this->caXmlRoot = array();
		$this->caXmlTarget = array();
		$this->caXmlObjects = array();
		$this->ciXmlDepth = 0;
	}

	/**
	 * Exibe o conte�do da inst�ncia �nica FWDXmlLoad.
	 *
	 * <p>Exibe na tela o conte�do de todos os atributos da �nica inst�ncia
	 * da classe FWDXmlLoad.</p>
	 * @access public
	 */
	public function dump_instance() {
		echo "<br/><br/><br/><br/>--------------[ FWDXmlLoad Singleton Dump <strong>BEGIN</strong> ]--------------<br/>";
		echo "<h3>coXmlLoad:</h3>";
		var_dump(self::$coXmlLoad);
		echo "<br/><h3>caXmlRoot:</h3>";
		var_dump($this->caXmlRoot);
		echo "<br/><h3>caXmlTarget:</h3>";
		var_dump($this->caXmlTarget);
		echo "<br/><h3>caXmlObjects:</h3>";
		var_dump($this->caXmlObjects);
		echo "<br/><h3>ciXmlDepth:</h3>";
		var_dump($this->ciXmlDepth);
		echo "<br/><h3>caHtmlTags:</h3>";
		var_dump($this->caHtmlTags);
		echo "<h3></h3>--------------[ FWDXmlLoad Singleton Dump <strong>END</strong> ]--------------<br/><br/><br/><br/>";
	}

	// NAO USADA
	/**
	 * Seta os valores dos atributos necess�rios da view.
	 *
	 * <p>Seta os valores dos atributos necess�rios da view.</p>
	 * @access private
	 * @param object $poView View
	 * @param array $paAttribs Array de atributos cujos valores ser�o setados (se necess�rio)
	 * @param array $paNeeded Array de atributos necess�rios
	 */
	private function xml_decode_attribute_array($poView,$paAttribs,$paNeeded) {
		foreach( $paAttribs as $key => $value ) {
			$key = strtolower($key);
			if (in_array($key,$paNeeded))
			$poView->{$key} = $value;
		}
	}

	/**
	 * Abre uma tag que n�o � HTML.
	 *
	 * <p>Abre uma tag que n�o � HTML.</p>
	 * @access private
	 * @param string $psClass_Name Nome da classe (tag)
	 * @param array $paAttribs Atributos da tag
	 * @return boolean Indica se conseguiu abrir a tag adequadamente
	 */
	private function generic_start($psClass_Name,$paAttribs) {
		$psClass_Name_Orig = strtolower($psClass_Name);
		if(substr($psClass_Name,0,3)!='fwd'){
			$psClass_Name = "FWD".$psClass_Name_Orig;
		}
		if(!class_exists($psClass_Name)){
			if(class_exists($psClass_Name_Orig)) {
				$psClass_Name = $psClass_Name_Orig;
			}
			else {
				return false;
			}
		}

		$view = new $psClass_Name;

		$name = "";
		$this->caXmlRoot[$this->ciXmlDepth] = $view;

		$target = "";
		if (isset($paAttribs['TARGET']))
		$target = strtolower($paAttribs['TARGET']);
		$this->caXmlTarget[$this->ciXmlDepth] = $target;

		// check if it has a "name" attribute
		if (isset($paAttribs['NAME'])) {
			$name = $paAttribs['NAME'];
		}
		else {
			$name = $this->generateName();
		}

		if(method_exists($view,"setAttrName")) {
			$view->setAttrName($name);
			FWDWebLib::addObject($name,$view);
			//$this->caXmlObjects[$name] = $view;
		}

		if (!$this->ciXmlDepth) {
			if ($name)
			$this->caXmlObjects[$name] = $view;
			else
			$this->caXmlObjects[] = $view;
		}

		// decode attributes;
		foreach( $paAttribs as $key => $value ) {
			$methodName = "setAttr" . strtolower($key);
			if (method_exists($view,$methodName)) {
				$view->{$methodName}($value);
			}elseif($key!='TARGET' && $key!='LABEL'){
				trigger_error("Parameter '$key' not alowed in class '$psClass_Name'.",E_USER_WARNING);
			}
		}

		$this->ciXmlDepth++;
		return true;
	}

	/**
	 * Abre uma tag HTML.
	 *
	 * <p>Abre uma tag HTML.</p>
	 * @access private
	 * @param string $psName Nome da tag a ser aberta
	 * @param array $paAttribs Array de atributos da tag
	 * @return boolean Indica se conseguiu abrir a tag ou n�o
	 */
	private function tag_html_start($psName,$paAttribs) {
		if( $this->ciXmlDepth == 0)
		return false;
		$msView = $this->caXmlRoot[$this->ciXmlDepth-1];
		// just add the html tag to the view's value
		// :TRICK: the parent view must be a static type, otherwise it wont work.
		$msOut = "";

		$psName = strtolower($psName);
		if (in_array($psName,$this->caHtmlTags)) {
			$msOut .= "<" . $psName;
			foreach( $paAttribs as $key => $value ) {
				$msOut .= ' ' . $key . '="' . $value . '"';
			}
			if (strcasecmp($psName,"BR")==0)
			$msOut.="/";
			$msOut .= '>';
			$msView->setValue($msOut);
			return true;
		}
		return false;
	}

	/**
	 * Start element Handler.
	 *
	 * <p>Start element Handler.</p>
	 * @access private
	 * @param resource $prParser Handler do analisador XML
	 * @param string $psName Nome do elemento para o qual o handler � chamado
	 * @param array $paAttribs Atributos do elemento
	 */
	private function xml_start_element($prParser,$psName,$paAttribs) {
		if (!$this->generic_start($psName,$paAttribs)) {
			if (!$this->tag_html_start($psName,$paAttribs)) {
				//                    	$this->caXmlRoot[ $this->ciXmlDepth++] = new fwdNone();
			}
		}
	}

	/**
	 * Adiciona objetos no pai do objeto corrente.
	 *
	 * <p>Adiciona o objeto corrente no seu pai. Executa os m�todos de setObj e
	 * addObj do pai do objeto corrente. Executa o m�todo 'execute' caso exista na classe.</p>
	 * @access private
	 * @param object $poView View a ter seus m�todos executados
	 * @param object $poParent Pai da View
	 * @param string $psView_Class Nome da classe que foi adicionada (setObj, addObj)
	 * @param string $psTarget Alvo
	 * @return boolean Indica se conseguiu executar m�todos
	 */
	private function execute_view($poView,$poParent,$psView_Class,$psTarget) {

		$parent_functions = array(
			"addObj" . $psView_Class,
			"setObj" . $psView_Class);
			
		$view_functions = array ("execute");
			
		$msOut = false;
		if ($poParent) {
			foreach($parent_functions as $funct_name) {
				if ( method_exists($poParent,$funct_name) ) {
					$poParent->{$funct_name}($poView,$psTarget);
					$msOut = true;
				}
			}
		}
		return $msOut;
	}

	/**
	 * Fecha uma tag que n�o � HTML.
	 *
	 * <p>Fecha uma tag que n�o � HTML.</p>
	 * @access private
	 * @param string $psName Nome da tag
	 * @return boolean Indica se conseguiu fechar a tag adequadamente
	 */
	private function generic_end($psName) {
		if ($this->ciXmlDepth < 1)
		return false;

		$view = $this->caXmlRoot[$this->ciXmlDepth-1];
		$view_class = get_class($view);

		if (method_exists($view,"CloseXml"))
		$view->CloseXml();
	  
		if ( isset($this->caXmlRoot[$this->ciXmlDepth-2]) )
		$parent_view = $this->caXmlRoot[$this->ciXmlDepth-2];
		else
		$parent_view = 0;

		$target = $this->caXmlTarget[$this->ciXmlDepth-1];
	  
		// scan the inheritance of each object
		$out = true;
		$msErrorMessage = "Element '$view_class' is not expected in '".get_class($parent_view)."'.";
		if(!$this->execute_view($view,$parent_view,$view_class,$target)){
			if($parent_view){
				$mbFound = false;
				while(($parent_class = get_parent_class($view_class))){
					if($this->execute_view($view,$parent_view,$parent_class,$target)){
						$mbFound = true;
						break;
					}
					$view_class = $parent_class;
				}
				if(!$mbFound){
					trigger_error($msErrorMessage,E_USER_ERROR);
				}
			}else{
				// no parent view...
				$out = false;     // onde � usado ?
			}
		}

		//execute das fun��es foi retirado da fun�ao {$this->execute_view} pois nessa fun��o a fun��o
		//executeView era chamada mais de uma vez por causa da recur��o para chamar os metodos da classe
		//mae - caso esta altera��o venha a ocasionar algum erro - � s� copiar o c�digo abaixo para
		// a fun��o execute_view modificando o nome da vari�vel $view para $poView
		$executeFunction="execute";
		if (!$this->cbIsCompiling)
		if ( method_exists($view,$executeFunction) )
		$view->{$executeFunction}();

		$this->ciXmlDepth--;
		unset($this->caXmlRoot[$this->ciXmlDepth]);
		unset($this->caXmlTarget[$this->ciXmlDepth]);

		return true;
	}

	/**
	 * Fecha uma tag HTML.
	 *
	 * <p>Fecha uma tag HTML.</p>
	 * @access private
	 * @param string $psName Nome da tag a ser fechada
	 * @return boolean Indica se conseguiu fechar a tag ou n�o
	 */
	private function tag_html_end($psName) {
		if( $this->ciXmlDepth == 0)
		return false;
		$msView = $this->caXmlRoot[$this->ciXmlDepth-1];
		$psName = strtolower($psName);
		if (strcasecmp($psName,"BR")==0)
		return true;
		if (in_array($psName,$this->caHtmlTags)) {
			$msView->setValue("</" . $psName . ">");
			return true;
		}
		return false;
	}

	/**
	 * End element Handler.
	 *
	 * <p>End element Handler.</p>
	 * @access private
	 * @param resource $prParser Handler do analisador XML
	 * @param string $psName Nome do elemento para o qual o handler � chamado
	 */
	private function xml_end_element($prParser,$psName) {
		if (!$this->tag_html_end($psName)) {
			$this->generic_end($psName);
		}
	}

	/**
	 * Insere texto do xml que nao esteja dentro de uma tag.
	 *
	 * <p>Insere texto do xml que nao esteja dentro de uma tag.</p>
	 * @access private
	 * @param resource $prParser Handler do analisador XML
	 * @param string $psData Texto a ser inserido
	 */
	private function xml_data($prParser,$psData){
		if($msTmp = trim($psData)){
			// se trim(psData) = nulo, entao matem o psData, senao desconta os espa�os em branco
			$psData = str_replace("\\n","<BR/>",$psData);
		}else{
			//$psData = $msTmp;
		}
		if($this->ciXmlDepth == 0) return false;
		$moView = $this->caXmlRoot[$this->ciXmlDepth-1];
		if(is_object($moView)
		&& method_exists($moView,"setValue")
		&& !empty($psData)
		&& in_array(get_class($moView),$this->caTextContainers)
		) {
			$miSelectedLanguage = FWDLanguage::getSelectedLanguage();
			$maDefaultLanguages = FWDLanguage::getDefaultLanguages();
			if(!empty($miSelectedLanguage) && $moView instanceof FWDString
			&& !in_array($miSelectedLanguage,$maDefaultLanguages)
			) {
				$moView->setValue(FWDWebLib::convertToISO(""),false);
			}
			else {
				$moView->setValue(FWDWebLib::convertToISO($psData),false);
			}
		}
	}

	/**
	 * Transforma arquivos XML em objetos PHP.
	 *
	 * <p>Transforma arquivos XML em objetos PHP.</p>
	 * @access public
	 * @param string $psFilename Nome do arquivo XML a ser transformado
	 * @return boolean Indica se conseguiu transformar o arquivo XML em objetos PHP
	 */
	public function xml_load($psFilename) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD1,__FILE__,__LINE__);
		$this->cleanInstance();

		$fp = fopen($psFilename,"r");
		if (!$fp)
		return false;

		//$xml_parser = xml_parser_create('ISO-8859-1');
		$xml_parser = xml_parser_create('UTF-8');

		xml_set_object ($xml_parser,$this);

		xml_set_element_handler($xml_parser,"xml_start_element","xml_end_element");
		xml_set_character_data_handler($xml_parser,"xml_data");


		$data = fread ($fp,filesize($psFilename));
		fclose ($fp);
		if (!xml_parse($xml_parser,$data,true)) {
			trigger_error(sprintf("xml_load error: %s at line %d",
			xml_error_string(xml_get_error_code($xml_parser)),
			xml_get_current_line_number($xml_parser)),E_USER_ERROR);
		}

		xml_parser_free($xml_parser);

		$xml_count = count($this->caXmlObjects);

		if (!isset($_POST))		// ZendEncoder 1.2 bugfix
		global $_POST;
		// Atribui valor do post nos objetos, e executa os eventos
		if(!empty($_POST)) {
			$moStartEvent = FWDStartEvent::getInstance();
			foreach ($this->caXmlObjects as $moObject) {
				if(method_exists($moObject, "collect")) {
						
					$maViews = $moObject->collect();
						
					foreach ($maViews as $moView) {
						if($moView instanceof FWDPostValue) {
							isset($_POST[$moView->getAttrName()]) ? $moView->setAttrValue( FWDWebLib::convertToISO($_POST[$moView->getAttrName()]) ) : "";
						}
						if (get_class($moView) == "FWDAjaxEvent")
						$moStartEvent->addAjaxEvent($moView);
					}
				}
				else {
					if($moObject instanceof FWDPostValue) {
						isset($_POST[$moObject->getAttrName()]) ? $moObject->setAttrValue( FWDWebLib::convertToISO($_POST[$moObject->getAttrName()]) ) : "";
					}
					if(get_class($moObject) == "FWDAjaxEvent") {
						$moStartEvent->addAjaxEvent($moObject);
					}
				}
			}
		}

		return true;
	}

	/**
	 * Retorna um nome gerado automaticamente.
	 *
	 * <p>Retorna um nome gerado automaticamente.</p>
	 * @access private
	 * @return string Nome �nico gerado automaticamente
	 */
	private function generateName(){
		return 'FWDID'.++self::$ciLastId;
	}

	/**
	 * Retorna o array de Views (todas as views da dialog).
	 *
	 * <p>Retorna o array de Views (todas as views da dialog).</p>
	 * @access public
	 * @return array Array de Views
	 */
	public function getAttrXmlObjects() {
		return $this->caXmlObjects;
	}

	/**
	 * Indica que o XMLLOAD est� sendo usado para compila��o ou n�o.
	 *
	 * <p>M�todo para indicar que o XMLLOAD est� sendo usado para compila��o ou n�o.</p>
	 * @access public
	 * @param boolean Verdadeiro ou falso
	 */
	public function setCompiling($pbCompiling) {
		$this->cbIsCompiling = $pbCompiling;
	}

	/**
	 * Verifica se o XMLLOAD est� sendo usado para compila��o ou n�o.
	 *
	 * <p>M�todo para verificar se o XMLLOAD est� sendo usado para compila��o ou n�o.</p>
	 * @access public
	 * @return array Array de Views
	 */
	public function isCompiling() {
		return $this->cbIsCompiling;
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDXmlStatic. Singleton. Carrega para mem�ria os objetos PHP
 * que est�o armazenados nos arquivos XMC (XML Compilado).
 *
 * <p>Classe singleton que carrega para a mem�ria os objetos PHP que est�o
 * armazenados nos arquivos XMC (XML Compilado). Garante a exist�ncia de
 * uma e apenas uma inst�ncia da classe FWDXmlStatic.</p>
 * @package FWD5
 * @subpackage base
 */
class FWDXmlStatic {

	/**
	 * Inst�ncia �nica da classe FWDXmlStatic
	 * @staticvar FWDXmlStatic
	 * @access private
	 */
	private static $coXmlStatic = NULL;

	/**
	 * Array de Views (todas as views da dialog)
	 * @var array
	 * @access private
	 */
	private $caXmlObjects;

	/**
	 * Nome do arquivo TAR que ser� pelo xml_load
	 * @var string
	 * @access private
	 */
	private $csTarFileName;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDXmlStatic.</p>
	 * @access private
	 */
	private function __construct() {
		$this->caXmlObjects = array();
		$this->csTarFileName = "forms.xmc";
	}

	/**
	 * Retorna a inst�ncia �nica da classe FWDXmlStatic (singleton).
	 *
	 * <p>Retorna a inst�ncia �nica da classe FWDXmlStatic. Se a inst�ncia
	 * ainda n�o existe, cria a �nica inst�ncia. Se a inst�ncia j� existe,
	 * retorna a �nica inst�ncia (singleton).</p>
	 * @access public
	 * @return FWDXmlStatic Inst�ncia �nica da classe FWDXmlStatic
	 * @static
	 */
	public static function getInstance() {
		if (self::$coXmlStatic == NULL) {
			self::$coXmlStatic = new FWDXmlStatic();
		}
		else {}	// inst�ncia �nica j� existe, retorne-a
		return self::$coXmlStatic;
	}

	/**
	 * Exibe o conte�do da inst�ncia �nica FWDXmlStatic.
	 *
	 * <p>Exibe na tela o conte�do de todos os atributos da �nica inst�ncia
	 * da classe FWDXmlStatic.</p>
	 * @access public
	 */
	public function dump_instance() {
		echo "<br/><br/><br/><br/>--------------[ FWDXmlStatic Singleton Dump <strong>BEGIN</strong> ]--------------<br/>";
		echo "<h3>coXmlStatic:</h3>";
		var_dump(self::$coXmlStatic);
		echo "<br/><h3>caXmlRoot:</h3>";
		var_dump($this->caXmlObjects);
		echo "<br/><h3>csTarFileName:</h3>";
		var_dump($this->csTarFileName);
		echo "<h3></h3>--------------[ FWDXmlStatic Singleton Dump <strong>END</strong> ]--------------<br/><br/><br/><br/>";
	}

	/**
	 * Seta o nome do arquivo TAR.
	 *
	 * <p>Seta o nome do arquivo TAR.</p>
	 * @access public
	 * @param string $psTarFileName Nome do arquivo TAR
	 */
	public function setAttrTarFileName($psTarFileName) {
		$this->csTarFileName = $psTarFileName;
	}

	/**
	 * L� os dados de um arquivo dentro de um pacote TAR.
	 *
	 * <p>L� os dados de um arquivo dentro de um pacote TAR.</p>
	 * @access private
	 * @param string $psTarFile Arquivo TAR a ser lido
	 * @param string $psFileItem Arquivo a ser extra�do de dentro do TAR
	 * @return string Conte�do do arquivo lido
	 */
	private function readTarFile($psTarFile,$psFileItem) {
		if ( !($tarFileStream = fopen($psTarFile,"rb")) )
		trigger_error("Error openning file $psTarFile.",E_USER_ERROR);
		else {
			while(!feof($tarFileStream)) {
				$msBlock = fread($tarFileStream,512);
				if (strlen($msBlock)<512)
				trigger_error("File $psFileItem was not found in $psTarFile.",E_USER_ERROR);

				$maData = unpack("a100name/a8mode/a8uid/a8gid/a12size/a12mtime/a8checksum/a1type/a100symlink/a6magic/a2temp/a32temp/a32temp/a8temp/a8temp/a155prefix/a12temp", $msBlock);

				if (!strcmp(trim($maData['name']),$psFileItem)) {
					if ($maData['checksum'] == 0x00000000) break;
					$checksum = 0;
					$msBlock = substr_replace($msBlock, "        ", 148, 8);
					for ($i = 0; $i < 512; $i++)
					$checksum += ord(substr($msBlock, $i, 1));
					if (octdec($maData['checksum']) != $checksum)
					trigger_error("Could not extract $psFileItem from $psTarFile, $psFileItem's header is corrupted.",E_USER_ERROR);
						
					$msBuffer = fread($tarFileStream,octdec($maData['size']));
						
					fclose($tarFileStream);
					if (strlen($msBuffer) == octdec($maData['size']))
					return $msBuffer;
					else
					trigger_error("Could not extract $psFileItem from $psTarFile, it is corrupted.",E_USER_ERROR);
				}
			}
		}
	}

	/**
	 * Transforma os dados em objetos PHP.
	 *
	 * <p>Transforma os dados em objetos PHP. Adiciona-os na WebLib e no
	 * array caXmlObjects.</p>
	 * @access private
	 * @param string $psData Dados a serem carregados
	 */
	private function xmlStatic($psData) {

		if (!$psData) return;

		$moObject = FWDWebLib::unserializeString(gzuncompress(base64_decode($psData)), false);

		if (method_exists($moObject,"getAttrName") && $moObject->getAttrName()) {
			$msKey = $moObject->getAttrName();
		}
		else {
			$msKey = "";
		}

		if ($msKey) {
			FWDWebLib::addObject($msKey,$moObject);
			$this->caXmlObjects[$msKey] = $moObject;
		}
		else {
			$this->caXmlObjects[] = $moObject;
		}

		if (method_exists($moObject, "execute")) {
			$moObject->execute();
		}

		if (!method_exists($moObject, "collect"))
		return true;

		$maViews = $moObject->collect();
		$moStartEvent = FWDStartEvent::getInstance();
		$moWebLib = FWDWebLib::getInstance();

		foreach ($maViews as $moView) {
			if (method_exists($moView, "execute")) {
				$moView->execute();
			}
			if (method_exists($moView,"getAttrName") && $moView->getAttrName()) {
				$exists = FWDWebLib::getObject($moView->getAttrName());
				if (!isset($exists))
				FWDWebLib::addObject($moView->getAttrName(),$moView);
			}

			// Para funcionar a tradu��o.
			$miSelectedLanguage = FWDLanguage::getSelectedLanguage();
			$maDefaultLanguages = FWDLanguage::getDefaultLanguages();
			if(!empty($miSelectedLanguage)
			&& !in_array($miSelectedLanguage,$maDefaultLanguages)
			) {
				if(!($moView instanceof FWDSelect)
				&& !($moView instanceof FWDController)
				&& !($moView instanceof FWDVariable)
				&& method_exists($moView,"setObjFWDString")
				&& method_exists($moView,"setValue"))
				$moView->setValue(FWDWebLib::convertToISO(""),true);

				if($moView instanceof FWDSelect) {
					$moItens = $moView->getItems();
					foreach ($moItens as $moItem)
					$moItem->setValue(FWDWebLib::convertToISO(""),true);
				}

				if($moView instanceof FWDMenu) {
					$moItens = $moView->getAttrItems();
					foreach ($moItens as $moItem)
					if(get_class($moItem) == "FWDMenuItem")
					$moItem->setValue(FWDWebLib::convertToISO(""),true);
				}
			}

			if ($moView instanceof FWDPostValue) {
				isset($_POST[$moView->getAttrName()]) ? $moView->setAttrValue( $_POST[$moView->getAttrName()] ) : "";
			}
			if (get_class($moView) == "FWDAjaxEvent")
			$moStartEvent->addAjaxEvent($moView);
		}
		return true;
	}

	/**
	 * Transforma arquivos XML em objetos PHP.
	 *
	 * <p>Transforma arquivos XML em objetos PHP.</p>
	 * @access public
	 * @param string $psFilename Nome do arquivo XML a ser transformado
	 * @return boolean Indica se conseguiu transformar o arquivo XML em objetos PHP
	 */
	public function xml_load($psFilename) {
		$maParameters = func_get_args();		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD1,__FILE__,__LINE__);
		$this->caXmlObjects = array();

		/*
		 * Esse trecho trata os casos em que o xml do arquivo
		 * n�o est� no forms.xmc do diret�rio corrente.
		 */
		$miPos = strrpos($psFilename, "/");
		if ($miPos===false) {
			$msRef = '';
			$msFile = $psFilename;
		}
		else {
			$msRef = substr($psFilename, 0, $miPos+1);
			$msFile = substr($psFilename, $miPos+1, strlen($psFilename)-$miPos);
		}

		$msInstDir = dirname( $_SERVER[ "SCRIPT_FILENAME" ] ) . '/' . $msRef;
		$msData = $this->readTarFile($msInstDir.$this->csTarFileName,$msFile);
		if (!isset($msData))
		trigger_error("Error loading XML file. Please contact the administrator.", E_USER_ERROR);

		$maData = explode("\n", $msData);

		foreach($maData as $msItem) {
			$this->xmlStatic($msItem);
		}

		if (!isset($_POST))		// ZendEncoder 1.2 bugfix
		global $_POST;
		// Atribui valor do post nos objetos, e executa os eventos
		if (!empty($_POST)) {
			$moStartEvent = FWDStartEvent::getInstance();
			foreach ($this->caXmlObjects as $moObject) {
				if ($moObject instanceof FWDPostValue) {
					isset($_POST[$moObject->getAttrName()]) ? $moObject->setAttrValue( $_POST[$moObject->getAttrName()] ) : "";
				}
				if (get_class($moObject) == "FWDAjaxEvent") {
					$moStartEvent->addAjaxEvent($moObject);
				}
			}
		}
	}
}
?>
<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDException. Realiza a adequa��o dos atributos de uma exce��o para a FWD5.
 *
 * <p>Classe para corre��o dos valores dos atributos 'file' e 'line' da classe
 * nativa Exception. Seta os valores segundo o arquivo que disparou o erro, e n�o
 * segundo o arquivo que levantou a exce��o (este �ltimo � sempre a classe
 * singleton FWDWebLib).</p>
 * @package FWD5
 * @subpackage base
 */
class FWDException extends Exception {

	/**
	 * Seta o nome do arquivo que disparou o erro.
	 *
	 * <p>M�todo que seta o atributo 'file' da classe nativa Exception para o
	 * valor do nome do arquivo em que foi realizada a chamada da fun��o
	 * 'trigger_error()'.</p>
	 * @access public
	 * @param string $psFile Nome do arquivo que disparou o erro
	 */
	public function setFile($psFile) {
		$this->file = $psFile;
	}

	/**
	 * Seta a linha do arquivo que disparou o erro.
	 *
	 * <p>M�todo que seta o atributo 'line' da classe nativa Exception para o
	 * valor da linha do arquivo em que foi realizada a chamada da fun��o
	 * 'trigger_error()'.</p>
	 * @access public
	 * @param string $psLine Linha do arquivo em que o erro foi disparado
	 */
	public function setLine($psLine) {
		$this->line = $psLine;
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

define('LANGUAGE_DEFAULT', 3300);

/**
 * Classe FWDLanguage. Classe que faz a tradu��o das strings.
 *
 * <p>Classe est�tica que faz a tradu��o das strings, tanto do XML, quanto do
 * PHP.</p>
 * @package FWD5
 * @subpackage base
 */
class FWDLanguage {

	/**
	 * Array contendo os caminhos dos arquivos de linguagem
	 * @var array
	 * @access private
	 */
	private static $caLanguages = array();

	/**
	 * Array contendo os identificadores das linguagem default
	 * Linguagem na qual as string que est�o no PHP e no XML s�o escritas
	 * Utilizado para saber quando o xml_load deve usar o dicion�rio e quando
	 * ele deve utilizar o valor original (dos PHPs e XMLs)
	 * @var array
	 * @access private
	 */
	private static $caDefaultLanguages = array(LANGUAGE_DEFAULT);

	/**
	 * Array contendo os nomes das linguagens
	 * @var array
	 * @access private
	 */
	private static $caLanguagesName = array();

	/**
	 * Array contendo os valores traduzidos das strings
	 * @var array
	 * @access private
	 */
	private static $caStrings = array();

	/**
	 * Array contendo os valores default de algumas strings
	 * Utilizado principalmente pelas string de KnownErrors
	 * @var array
	 * @access private
	 */
	private static $caStringsDefault = array();

	/**
	 * Identificador da linguagem selecionada
	 * @var integer
	 * @access private
	 */
	private static $ciSelectedLanguage = null;
	 
	/**
	 * Construtor. N�o � usado.
	 *
	 * <p>Construtor. N�o � usado, serve s� para tornar a classe est�tica, pois o
	 * seu acesso � private.</p>
	 * @access private
	 */
	private function __construct(){
	}

	/**
	 * Adiciona uma linguagem ao array de linguagens
	 *
	 * <p>Adiciona uma linguagem ao array de linguagens.</p>
	 * @access public
	 * @param integer piLanguageId Identificador da linguagem
	 * @param string piLanguageName Nome da linguagem
	 * @param string psStringFile Caminho do arquivo contendo as strings
	 */
	public static function addLanguage($piLanguageId,$psLanguageName,$psStringFile){
		self::$caLanguages[$piLanguageId] = $psStringFile;
		self::$caLanguagesName[$piLanguageId] = $psLanguageName;
	}

	/**
	 * Seta o nome da linguagem.
	 *
	 * <p>M�todo para setar o nome da linguagem.</p>
	 * @access public
	 * @param integer piLanguageId Identificador da linguagem
	 * @param string piLanguageName Nome da linguagem
	 */
	public static function setLanguageName($piLanguageId,$psLanguageName){
		self::$caLanguagesName[$piLanguageId] = $psLanguageName;
	}

	/**
	 * Adiciona uma linguagem default ao array de linguagens default
	 * Linguagem na qual as string que est�o no PHP e no XML s�o escritas
	 *
	 * <p>Adiciona uma linguagem default ao array de linguagens default</p>
	 * @access public
	 * @param integer piLanguageId Identificador da linguagem
	 */
	public static function addDefaultLanguage($piLanguageId){
		self::$caDefaultLanguages[] = $piLanguageId;
	}

	/**
	 * Seleciona uma linguagem do array de linguagens
	 *
	 * <p>Seleciona uma linguagem do array de linguagens.</p>
	 * @access public
	 * @param integer piLanguageId Identificador da linguagem
	 * @return boolean True indica sucesso
	 */
	public static function selectLanguage($piLanguageId){
		if ($piLanguageId == LANGUAGE_DEFAULT) return false;
		elseif(!isset(self::$caLanguages[$piLanguageId])){
			trigger_error("Unknown language id: $piLanguageId",E_USER_ERROR);
			return false;
		}

		self::$ciSelectedLanguage = $piLanguageId;
		$msStringFile = self::$caLanguages[$piLanguageId];

		if(!file_exists($msStringFile) || !is_readable($msStringFile)){
			trigger_error("Could not read file '$msStringFile'.",E_USER_ERROR);
			return false;
		}

		require($msStringFile);

		if(isset($gaStrings)){
			if(empty(self::$caStringsDefault)) {
				self::$caStringsDefault = self::$caStrings;
			}
			self::$caStrings = array_merge(self::$caStringsDefault,$gaStrings);
			unset($gaStrings);
		}else{
			trigger_error("File '$msStringFile' does not contain \$gaStrings array.",E_USER_ERROR);
			return false;
		}
		return true;
	}

	/**
	 * Substitui uma string no c�digo PHP pela sua tradu��o.
	 *
	 * <p>Substitui uma string no c�digo PHP pela sua tradu��o.</p>
	 * @access public
	 * @param string psId Id da string a ser traduzida
	 * @param string psValue String a ser traduzida
	 * @param boolean pbForce For�a o novo valor mesmo que a string j� exista.
	 * @return string String traduzida
	 */
	public static function getPHPStringValue($psId,$psValue="",$pbForce=false){
		if($pbForce || !isset(self::$caStrings[$psId]) || empty(self::$caStrings[$psId]))
		self::$caStrings[$psId] = utf8_encode(FWDWebLib::convertToISO($psValue));
		return self::$caStrings[$psId];
	}

	/**
	 * Substitui uma string no c�digo XML pela sua tradu��o.
	 *
	 * <p>Se o id da string existir no array de tradu��o, retorna o valor
	 * traduzido, sen�o retorna o pr�prio valor recebido como par�metro.</p>
	 * @access public
	 * @param string psId Id da string a ser traduzida
	 * @param string psValue String a ser traduzida
	 * @return string String traduzida
	 */
	public static function getXMLStringValue($psId,$psValue){
		if(isset(self::$caStrings[$psId]) && !empty(self::$caStrings[$psId])){
			return self::$caStrings[$psId];
		}else{
			return $psValue;
		}
	}

	/**
	 * Obtem o identificados da linguagem selecionada
	 *
	 * <p>Obtem o identificados da linguagem selecionada.</p>
	 * @access public
	 * @return integer Identificador
	 */
	public static function getSelectedLanguage(){
		return self::$ciSelectedLanguage;
	}

	/**
	 * Retorna os identificadores das linguagens default
	 *
	 * <p>Retorna os identificadores das linguagens default</p>
	 * @access public
	 * @return array Identificadores
	 */
	public static function getDefaultLanguages(){
		return self::$caDefaultLanguages;
	}

	/**
	 * Retorna as linguagens do sistema.
	 *
	 * <p>M�todo para retornar as linguagens do sistema.</p>
	 * @access public
	 * @param boolean $pbDefault Return Default language in results
	 * @return array Array[LanguageId] => LanguageName
	 */
	public static function getLanguages($pbDefault=false){
		if ($pbDefault) {
			$maReturn = self::$caLanguagesName;
			$maReturn[LANGUAGE_DEFAULT] = FWDLanguage::getPHPStringValue('default_language', 'Portugu�s');
			return $maReturn;
		} else {
			if (count(self::$caLanguagesName)) return self::$caLanguagesName;
			else return array(LANGUAGE_DEFAULT => FWDLanguage::getPHPStringValue('default_language', 'Portugu�s'));
		}
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDDefaultErrorHandler. Manipulador de erros default da FWD.
 *
 * <p>Manipulador de erros default da FWD.</p>
 * @package FWD5
 * @subpackage base
 */
class FWDDefaultErrorHandler implements FWDErrorHandler {

	/**
	 * Indica se o pr�ximo erro a ser encontrado ser� o primeiro
	 * @var boolean
	 * @access protected
	 */
	protected static $cbFirstError = true;

	/**
	 * Array associativo de erros conhecidos. Cada elemento � um array de
	 * strings que, se alguma delas for encontrada dentro de uma mensagem de erro,
	 * o handler de erros vai tratar o erro como um erro conhecido identificado
	 * pela chave desse elemento neste array.
	 * @var array
	 * @access protected
	 */
	protected static $caKnownErrors = array();

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDErrorHandler.</p>
	 * @access public
	 */
	public function __construct(){
		FWDLanguage::getPHPStringValue('error_db_connection',"Database connection error.");
		FWDLanguage::getPHPStringValue('error_configuration',"The configuration file is corrupt.");
		FWDLanguage::getPHPStringValue('error_smtp',"Failed to connect to mailserver.");
		FWDLanguage::getPHPStringValue('error_db_invalid_object_name',"Invalid database object name. Probably using wrong DB user.");
		FWDLanguage::getPHPStringValue('error_session_permission_denied',"Permission denied in session file.");
		FWDLanguage::getPHPStringValue('error_debug_permission_denied',"Permission denied in debug file.");
		FWDLanguage::getPHPStringValue('error_config_corrupt',"The configuration file is corrupt.");

		self::addKnownError(
      'error_db_connection',
		array(
        'function.mssql-connect',
        'function.mssql-select-db',
        'function.pg-connect',
        'function.ocilogon',
		)
		);
		self::addKnownError('error_configuration',array('The configuration file is corrupt'));
		self::addKnownError('error_smtp',array('function.mail'));
		self::addKnownError('error_session_permission_denied',array('function.session-start'));
		self::addKnownError('error_db_invalid_object_name',array('Invalid object name'));
		self::addKnownError('error_debug_permission_denied',array('debug.txt'));
		self::addKnownError('error_config_corrupt',array('error_config_corrupt'));
	}

	/**
	 * Error Handler da FWD5.
	 *
	 * <p>Error Handler da FWD5. Identifica o tipo de erro e levanta uma
	 * exce��o que ir� efetuar seu tratamento. Aborta se necess�rio.</p>
	 * @access public
	 * @param integer $piErrNo N�mero do erro
	 * @param string $psErrStr Mensagem de erro
	 * @param string $psErrFile Nome do arquivo que gerou o erro
	 * @param integer $piErrLine N�mero da linha em que ocorreu o erro
	 */
	public function handleError($piErrNo, $psErrStr, $psErrFile, $piErrLine){
		try{
			$exception = new FWDException($psErrStr,$piErrNo);
			$exception->setFile($psErrFile);
			$exception->setLine($piErrLine);
			throw $exception;
		}catch(FWDException $moException){
			self::handleException($moException);
		}
		if($piErrNo==E_USER_ERROR){
			$msOut = '';
			$msOut.= "<b>FWD Fatal error</b> in <strong>$psErrFile</strong> on line <strong>$piErrLine</strong>";
			$msOut.= ", PHP " . PHP_VERSION . " (" . PHP_OS . ")<br/>\n";
			$msOut.= "Aborting...<br/>\n";
			echo $msOut;
			die(1);
		}
	}

	/**
	 * Exception Handler da FWD5.
	 *
	 * <p>Exception Handler da FWD5. Captura a exce��o e gera um trace dos erros.</p>
	 * @access public
	 * @param FWDException $poFWDException Exce��o
	 * @static
	 */
	public static function handleException($poFWDException){
		switch($poFWDException->getCode()){
			case E_USER_ERROR:   $msError = "FWD ERROR";              break;
			case E_USER_WARNING: $msError = "FWD WARNING";            break;
			case E_USER_NOTICE:  $msError = "FWD NOTICE";             break;
			case E_STRICT:       return;                              break;
			default:             $msError = "FWD Unknown error type"; break;
		}

		$msExceptionMessage = $poFWDException->getMessage();

		foreach(self::$caKnownErrors as $msErrorId=>$maErrorSubstrs){
			foreach($maErrorSubstrs as $msErrorSubstr){
				if(strpos($msExceptionMessage,$msErrorSubstr)!==false){
					$msMessage = $msErrorId;
					echo "[FWD_ERROR]: $msMessage";
					exit();
				}
			}
		}

		if(self::$cbFirstError){
			echo self::dumpPost()."\n";
			self::$cbFirstError = false;
		}

		$msErrorMsg = "<b>FWD Exception Caught:</b> {$msError} [{$poFWDException->getCode()}]<br/>"
		."<br/>&nbsp;&nbsp;&nbsp; Message: {$poFWDException->getMessage()} <br/>"
		."<br/>&nbsp;&nbsp;&nbsp; File: {$poFWDException->getFile()} on line {$poFWDException->getLine()}<br/>";

		FWDWebLib::getInstance()->writeStringDebug($msErrorMsg,FWD_DEBUG_ERROR);

		echo $msErrorMsg;
		echo "<br/>TraceAsString:<br/><pre>";
		echo $poFWDException->getTraceAsString();
		echo "</pre><br/><br/>";
		return 1;
	}

	/**
	 * Adiciona um novo erro conhecido.
	 *
	 * <p>Adiciona um novo erro conhecido ao array de erros conhecidos.</p>
	 * @access public
	 * @param string $psErrorStringId Id do erro
	 * @param array $paMsgSubstrings Array de strings que caracterizam o erro
	 * @static
	 */
	public static function addKnownError($psErrorStringId,$paMsgSubstrings){
		self::$caKnownErrors[$psErrorStringId] = $paMsgSubstrings;
	}

	/**
	 * Remove um erro conhecido.
	 *
	 * <p>Remove um erro conhecido do array de erros conhecidos.</p>
	 * @access public
	 * @param string $psErrorStringId Id do erro
	 * @static
	 */
	public static function removeKnownError($psErrorStringId){
		unset(self::$caKnownErrors[$psErrorStringId]);
	}

	/**
	 * Retorna o conte�do do array $_POST codificado na forma de string
	 *
	 * <p>Retorna o conte�do do array $_POST codificado na forma de string</p>
	 * @access protected
	 * @return string Conte�do do array $_POST codificado na forma de string
	 */
	protected static function dumpPost(){
		return base64_encode(serialize($_POST));
	}

}

?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDDoNothingErrorHandler. Error Handler para mascarar erros.
 *
 * <p>Error Handler para mascarar erros.</p>
 * @package FWD5
 * @subpackage base
 */
class FWDDoNothingErrorHandler implements FWDErrorHandler {

	protected $cbErrorCaptured = false;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDDoNothingErrorHandler.</p>
	 * @access public
	 */
	public function __construct(){
	}

	/**
	 * Error Handler para fazer nada.
	 *
	 * <p>Error Handler para fazer nada.</p>
	 * @access public
	 * @param integer $piErrNo N�mero do erro
	 * @param string $psErrStr Mensagem de erro
	 * @param string $psErrFile Nome do arquivo que gerou o erro
	 * @param integer $piErrLine N�mero da linha em que ocorreu o erro
	 */
	public function handleError($piErrNo, $psErrStr, $psErrFile, $piErrLine){
		$this->cbErrorCaptured = true;
	}

	/**
	 * Exception Handler para fazer nada.
	 *
	 * <p>Exception Handler para fazer nada.</p>
	 * @access public
	 * @param FWDException $poFWDException Exce��o
	 * @static
	 */
	public static function handleException($poFWDException){
	}

	/**
	 * Indica se o error handler capturou algum erro.
	 *
	 * <p>Indica se o error handler capturou algum erro.</p>
	 * @access public
	 * @return boolean True sse o error handler capturou algum erro
	 */
	public function errorCaptured(){
		return $this->cbErrorCaptured;
	}

}

?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

define("XML_LOAD", 2);
define("XML_STATIC", 4);

define("FWD_DEBUG_NONE",        0);
define("FWD_DEBUG_DB",          1);
define("FWD_DEBUG_INFO",        2);
define("FWD_DEBUG_ERROR",       4);
define("FWD_DEBUG_FWD1",        8);
define("FWD_DEBUG_FWD2",       16);
define("FWD_DEBUG_ALL",0xFFFFFFFF);

$GLOBALS['load_method'] = ((isset($lbRelease) && $lbRelease)?XML_STATIC:XML_LOAD);

/**
 * Classe FWDWebLib. Singleton. Implementa vari�veis e m�todos globais na FWD5.
 *
 * <p>Classe singleton que implementa vari�veis e m�todos globais na Framework FWD5.
 * Garante a exist�ncia de uma e apenas uma inst�ncia da classe FWDWebLib.</p>
 * @package FWD5
 * @subpackage base
 */
class FWDWebLib {

	/**
	 * Atributo que controla a vers�o da FWD
	 * @var string
	 * @access private
	 */
	private $csVersion = '$Id: FWDWebLib.php 1147 2006-12-18 17:17:51Z eschein $';

	/**
	 * Inst�ncia �nica da classe FWDWebLib
	 * @staticvar FWDWebLib
	 * @access private
	 */
	private static $coWebLib = NULL;

	/**
	 * Inst�ncia �nica da classe FWDXmlLoad
	 * @staticvar FWDXmlLoad
	 * @access private
	 */
	private static $coXmlLoad = NULL;

	/**
	 * Define se os objetos PHP vem de um XmlLoad ou de um XmlStatic
	 * @var integer
	 * @access private
	 */
	private $ciXmlLoadMethod;

	/**
	 * Inst�ncia �nica de conex�o com o banco de dados
	 * @staticvar FWDDB
	 * @access private
	 */
	private static $coDBConnection = NULL;

	/**
	 * Refer�ncia base do sistema (url, pode ser relativa ou absoluta)
	 * @var string
	 * @access private
	 */
	private  $csSysRef;

	/**
	 * Refer�ncia para base (../base/)
	 * @var string
	 * @access private
	 */
	private $csLibRef;

	/**
	 * Refer�ncia para os arquivos javascript (../base/javascript)
	 * @var string
	 * @access private
	 */
	private $csJsRef;

	private $csSysJsRef;

	/**
	 * Refer�ncia para o sys_ref baseado no tab_main
	 * @var string
	 * @access private
	 */
	private $csSysRefBasedOnTabMain;

	/**
	 * Refer�ncia para os arquivos CSS do projeto
	 * @var string
	 * @access private
	 */
	private $csCSSRef;

	/**
	 * Refer�ncia para os arquivos de imagens (../gfx/)
	 * @var string
	 * @access private
	 */
	private $csGfxRef;

	/**
	 * Indica se a fun��o session_start() j� foi chamada.
	 * @var boolean
	 * @access private
	 */
	private $cbSessionStarted = false;

	/**
	 * Indica se o dump_html j� foi iniciado. �til, pois algumas opera��es n�o podem ser feitas depois de iniciar o dump.
	 * @var boolean
	 * @access private
	 */
	private $cbDumpStarted = false;

	/**
	 * Indica se a FWD est� dentro da fun��o xml_load . �til, pois algumas opera��es n�o podem ser feitas durante o xml_load1.
	 * @var boolean
	 * @access private
	 */
	private $cbXmlLoadRunning = false;

	/**
	 * Objeto que manipula os erros e as exce��es
	 * @var FWDErrorHandler
	 * @access private
	 */
	private static $coErrorHandler = null;

	/**
	 * Pilha de error handlers
	 * @var array
	 * @access private
	 */
	private static $caErrorHandlerStack = array();

	/**
	 * Indica se o sistema est� em modo debug
	 * @var boolean
	 * @access private
	 */
	private static $cbDebugMode = false;

	/**
	 * Indica o tipo de debug gravado
	 * @var integer
	 * @access private
	 */
	private static $ciDebugType = FWD_DEBUG_NONE;

	/**
	 * Indica se o debug gravado ser� encriptado ou n�o
	 * @var integer
	 * @access private
	 */
	private static $cbDebugEncrypted = true;

	/**
	 * Indica o caminho do arquivo de debug
	 * @var integer
	 * @access private
	 */
	private static $csDebugFilePath = "./lib/debug.txt";

	/**
	 * Titulo da janela do browser
	 * @var string
	 * @access private
	 */
	private $csWindowTitle = 'FWD5 - Fast Web Development';

	/**
	 * Meta Keywords da p�gina
	 * @var string
	 * @access private
	 */
	private $csMetaKeywords = '';

	/**
	 * FavIcon
	 * @var string
	 * @access private
	 */
	private $csFavIcon = '';

	/**
	 * Avoid Google
	 * @var boolean
	 * @access private
	 */
	private $cbAvoidGoogle = false;

	/**
	 * Tracker code (ex. Google Analytics)
	 * @var string
	 * @access private
	 */
	private $csTracker = '';

	/**
	 * CSS extra
	 * @var string
	 * @access private
	 */
	private $csExtraCSS = '';

	/**
	 * Primeiro arquivo XML carregado. � o que representa a tela e desencadeia os eventos.
	 * @var string
	 * @access private
	 */
	private $csXmlName = '';

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDWebLib. Inicializa as vari�veis globais
	 * com os valores padr�o.</p>
	 * @access private
	 * @param string $psBaseDefault Refer�ncia base padr�o
	 */
	private function __construct($psBaseDefault = "./"){
		ob_start();
		$this->csLibRef = $psBaseDefault."base/";
		$this->csJsRef = $this->csLibRef."javascript/nonauto/";
		$this->csSysJsRef = '';
		$this->csGfxRef = $psBaseDefault."gfx/";
		$this->ciXmlLoadMethod = $GLOBALS['load_method'];
		switch($this->ciXmlLoadMethod){
			case XML_LOAD  : self::$coXmlLoad = FWDXmlLoad::getInstance(); break;
			case XML_STATIC: self::$coXmlLoad = FWDXmlStatic::getInstance(); break;
			default        : trigger_error("Invalid XmlLoad Method. Must be either 'XML_LOAD' or 'XML_STATIC'.",E_USER_ERROR); break;
		}

		self::setErrorHandler(new FWDDefaultErrorHandler());
	}

	/**
	 * Seta o objeto que manipula os erros e as exce��es.
	 *
	 * <p>Seta o objeto que manipula os erros e as exce��es.</p>
	 * @access public
	 * @param FWDErrorHandler $poErrorHandler Objeto que manipula os erros e as exce��es
	 * @static
	 */
	public static function setErrorHandler(FWDErrorHandler $poErrorHandler){
		if(self::$coErrorHandler){
			array_push(self::$caErrorHandlerStack,self::$coErrorHandler);
		}
		self::$coErrorHandler = $poErrorHandler;
		set_error_handler(array(self::$coErrorHandler,'handleError'));
		set_exception_handler(array(self::$coErrorHandler,'handleException'));
	}

	/**
	 * Restaura o error handler anterior, se houver.
	 *
	 * <p>Restaura o error handler anterior, se houver.</p>
	 * <p>� poss�vel aninhar chamadas de setErrorHandler e restoreLastErrorHandler,
	 * mas n�o � poss�vel ficar alternando entre dois error handlers com sucesivas
	 * chamadas de restoreLastErrorHandler.</p>
	 * @access public
	 * @return boolean Indica se havia um error handler anterior
	 * @static
	 */
	public static function restoreLastErrorHandler(){
		if(count(self::$caErrorHandlerStack)){
			self::$coErrorHandler = array_pop(self::$caErrorHandlerStack);
			set_error_handler(array(self::$coErrorHandler,'handleError'));
			set_exception_handler(array(self::$coErrorHandler,'handleException'));
			return true;
		}else{
			return false;
		}
	}

	/**
	 * Retorna a inst�ncia �nica da classe FWDWebLib (singleton).
	 *
	 * <p>Retorna a inst�ncia �nica da classe FWDWebLib. Se a inst�ncia
	 * ainda n�o existir, cria a �nica inst�ncia. Se a inst�ncia j� existir,
	 * retorna a �nica inst�ncia (singleton).</p>
	 * @access public
	 * @param string $psBaseDefault Refer�ncia base padr�o
	 * @return FWDWebLib Inst�ncia �nica da classe FWDWebLib
	 * @static
	 */
	public static function getInstance($psBaseDefault = "./") {
		if (self::$coWebLib == NULL) {
			self::$coWebLib = new FWDWebLib($psBaseDefault);
		}
		else {}  // inst�ncia �nica j� existe, retorne-a
		return self::$coWebLib;
	}

	/**
	 * Seta a inst�ncia �nica de conex�o com o banco de dados (singleton).
	 *
	 * <p>Seta a inst�ncia �nica de conex�o com o banco de dados. Se a inst�ncia
	 * ainda n�o existir, cria a �nica inst�ncia. Se a inst�ncia j� existir,
	 * n�o faz nada (singleton).</p>
	 * @access public
	 * @param FWDDB $poDB Objeto de conex�o com o banco de dados
	 * @static
	 */
	public static function setConnection(FWDDB $poDB) {
		if (self::$coDBConnection == NULL) self::$coDBConnection = $poDB;
	}

	/**
	 * Retorna a inst�ncia �nica da classe FWDWebLib (singleton).
	 *
	 * <p>Retorna a inst�ncia �nica da classe de conex�o do banco de dados.</p>
	 * @access public
	 * @return FWDDB Inst�ncia �nica de conex�o do banco de dados
	 * @static
	 */
	public static function getConnection() {
		if (self::$coDBConnection == NULL)
		trigger_error("No database connection available",E_USER_ERROR);
		return clone self::$coDBConnection;
	}

	/**
	 * Adiciona um objeto global.
	 *
	 * <p>Insere no array $GLOBALS um objeto definido pelo usu�rio.</p>
	 * @access public
	 * @param string $psName Nome da vari�vel global a ser retornada
	 * @param object $poObject Objeto
	 * @static
	 */
	public static function addObject($psName, $poObject) {
		if(!isset($GLOBALS[$psName]))
		$GLOBALS[$psName] = $poObject;
		else
		trigger_error("An object with the same name ($psName) already exists", E_USER_WARNING);
	}

	/**
	 * Retorna uma vari�vel global gen�rica.
	 *
	 * <p>Retorna uma vari�vel global gen�rica. Procura por uma ocorr�ncia
	 * da string passada como par�metro no array $GLOBALS[]. Caso encontre,
	 * retorna o conte�do desta posi��o do array. Caso contr�rio, retorna NULL.</p>
	 * @access public
	 * @param string $psName Nome da vari�vel global a ser retornada
	 * @return object Vari�vel global
	 * @static
	 */
	public static function getObject($psName = "") {
		$moObject = NULL;
		if ( isset($GLOBALS[$psName]) ) {
			$moObject = $GLOBALS[$psName];
		}
		else{}  // objeto n�o existe, retorne NULL
		return $moObject;
	}

	/**
	 * Limpa o array $GLOBALS.
	 *
	 * <p>Limpa o array $GLOBALS.</p>
	 * @access public
	 * @static
	 */
	public static function cleanObjects() {
		$GLOBALS = array();
	}

	/**
	 * Retorna uma vari�vel do POST.
	 *
	 * <p>Retorna uma vari�vel do POST. Procura por uma ocorr�ncia
	 * da string passada como par�metro no array $_POST[]. Caso encontre,
	 * retorna o conte�do desta posi��o do array. Caso contr�rio, retorna NULL.</p>
	 * @access public
	 * @param string $psName Nome da vari�vel do POST a ser retornada
	 * @return object Vari�vel do POST
	 * @static
	 */
	public static function getPOST($psName = "") {
		$moObject = NULL;
		if ( isset($_POST[$psName]) ) {
			$moObject = $_POST[$psName];
		}
		else{}  // objeto n�o existe, retorne NULL
		return $moObject;
	}

	/**
	 * Indica se o browser utilizado � IE ou n�o.
	 *
	 * <p>Retorna o valor booleano 'true' caso o browser utilizado seja
	 * o Internet Explorer. Retorna 'false' caso contr�rio.</p>
	 * @access public
	 * @return boolean Indica se o browser � IE ou n�o
	 * @static
	 */
	public static function browserIsIE() {
		return (strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), "MSIE")) ? true:false;
	}

	/**
	 * Exibe o conte�do da inst�ncia �nica FWDWebLib.
	 *
	 * <p>Exibe na tela o conte�do de todos os atributos da �nica inst�ncia
	 * da classe FWDWebLib.</p>
	 * @access public
	 */
	public function dump_instance() {
		echo "<br/><br/><br/><br/>--------------[ FWDWebLib Singleton Dump <strong>BEGIN</strong> ]--------------<br/>";
		echo "<h3>coWebLib:</h3>";
		var_dump(self::$coWebLib);
		echo "<br/><h3>coXmlLoad:</h3>";
		var_dump(self::$coXmlLoad);
		echo "<br/><h3>csSysRef:</h3>";
		var_dump($this->csSysRef);
		echo "<br/><h3>csLibRef:</h3>";
		var_dump($this->csLibRef);
		echo "<br/><h3>csJsRef:</h3>";
		var_dump($this->csJsRef);
		echo "<br/><h3>csSysJsRef:</h3>";
		var_dump($this->csSysJsRef);
		echo "<br/><h3>csGfxRef:</h3>";
		var_dump($this->csGfxRef);
		echo "<h3></h3>--------------[ FWDWebLib Singleton Dump <strong>END</strong> ]--------------<br/><br/><br/><br/>";
	}

	/**
	 * Traduz um valor, caso este esteja presente no array.
	 *
	 * <p>Se o array passado como par�metro for uma lista de valores, por exemplo
	 * 'array("a","b","c",...)', retorna o pr�prio valor caso encontre uma ocorr�ncia
	 * deste no array. Caso n�o encontre, retorna NULL.
	 * Se o array passado como par�metro for um array do tipo
	 * 'array("a"=>x, "b"=>y, "c"=>z,...)', procura por uma ocorr�ncia do valor a
	 * ser traduzido (par�metro 1) no lado esquerdo da seta. Caso encontre, retorna
	 * o valor existente do lado direito da seta. Caso n�o encontre, retorna NULL.</p>
	 * @access public
	 * @param string $psValue Valor a ser traduzido
	 * @param array $paSet Array de valores OU array do tipo valor=>tradu��o
	 * @param string $psAttribute Nome do atributo
	 * @return string Depende dos par�metros passados para a fun��o
	 */
	public static function attributeParser($psValue,$paSet,$psAttribute) {
		$miMatched = 0;
		foreach($paSet as $msKey => $msKeyValue) {
			if ( (is_int($msKey)?$msKeyValue:$msKey) == $psValue ) {
				$miMatched++;
				break;
			}
			else{}
		}

		if (!$miMatched) {
			trigger_error("Value '{$psValue}' does not belong to the set of possible values in attribute '{$psAttribute}'",E_USER_WARNING);
			return NULL;
		}
		else {
			return $msKeyValue;
		}
	}

	/**
	 * Transforma arquivos XML em objetos PHP.
	 *
	 * <p>Transforma arquivos XML em objetos PHP.</p>
	 * @access public
	 * @param string $psFilename Nome do arquivo XML a ser transformado
	 */
	public function xml_load($psFilename, $pbStartEvent = TRUE) {
		if($this->csXmlName=='') $this->csXmlName = $psFilename;
		$this->cbXmlLoadRunning = true;
		self::$coXmlLoad->xml_load($psFilename, $pbStartEvent);
		$this->cbXmlLoadRunning = false;
		//Dispara os eventos definidos pelo usu�rio
		if($pbStartEvent) {
			$StartEvent_instance = FWDStartEvent::getInstance();
			$StartEvent_instance->start();
		}
	}

	/**
	 * Seta o t�tulo da janela do browser.
	 *
	 * <p>Seta o t�tulo da janela do browser.</p>
	 * @access public
	 * @param string $psWindowTitle T�tulo da janela
	 */
	public function setWindowTitle($psWindowTitle) {
		$this->csWindowTitle = $psWindowTitle;
	}

	/**
	 * Seta o meta keywords da p�gina.
	 *
	 * <p>Seta o meta keywords da p�gina.</p>
	 * @access public
	 * @param string $psMetaKeywords Palavras-chave da p�gina
	 */
	public function setMetaKeywords($psMetaKeywords) {
		$this->csMetaKeywords = $psMetaKeywords;
	}

	/**
	 * Seta o FavIcon p�gina.
	 *
	 * <p>Seta o FavIcon da p�gina.</p>
	 * @access public
	 * @param string $psUrl Url do FavIcon
	 */
	public function setFavIcon($psUrl) {
		$this->csFavIcon = $psUrl;
	}

	/**
	 * Seta o Tracker da p�gina.
	 *
	 * <p>Seta o Tracker da p�gina.</p>
	 * @access public
	 * @param string $psTracker C�digo do Tracker
	 */
	public function setTracker($psTracker) {
		$this->csTracker = $psTracker;
	}

	/**
	 * Indica que o sistema n�o deve ser indexado pelo google.
	 *
	 * <p>Indica que o sistema n�o deve ser indexado pelo google.</p>
	 * @access public
	 * @param boolean $pbAvoidGoogle True or false
	 */
	public function avoidGoogle($pbAvoidGoogle) {
		$this->cbAvoidGoogle = $pbAvoidGoogle;
	}

	/**
	 * Adiciona um css extra no sistema.
	 *
	 * <p>M�todo para adicionar um css extra no sistema. Utilizado para
	 * diferenciar entre os diferentes tipos de licen�a.</p>
	 * @access public
	 * @param string $psExtraCSS Nome do arquivo css
	 */
	public function setExtraCSS($psExtraCSS) {
		$this->csExtraCSS = $psExtraCSS;
	}

	/**
	 * Cria o c�digo HTML de uma p�gina.
	 *
	 * <p>Cria o c�digo HTML de uma p�gina. Escreve o cabe�alho, desenha o dialog
	 * (escreve o corpo da p�gina) e fecha as tags abertas no cabe�alho.</p>
	 * @access public
	 * @param FWDDialog $poDialog Corpo da p�gina
	 */
	public function dump_html(FWDDialog $poDialog){
		$this->cbDumpStarted = true;
		// Para resolver o problema com a FPDF, somente adiciona-se o gzhandler no
		// dump_html, e para que funcione � necess�rio armazenar o que se encontra no
		// buffer e dar o flush depois de ter setado o hzhandler, ob_end_flush n�o funciona.
		$msOutput = ob_get_contents();
		ob_end_clean();
		ob_start("ob_gzhandler");
		echo $msOutput;

		$msPopUpId = substr($this->csXmlName,0,strrpos($this->csXmlName,'.'));

		$msRenderLoad = $poDialog->renderLoad();
		?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<? if ($this->csMetaKeywords) echo "<meta name='keywords' content='".$this->csMetaKeywords."'>"; ?>
		<? if ($this->cbAvoidGoogle) echo "<meta name='robots' content='noindex, nofollow'>"; ?>
		<?=($this->csFavIcon)?'<link rel="shortcut icon" href="'.$this->csFavIcon.'">':''?>
<link rel="StyleSheet" href="<?=$this->getCSSRef()?>css.php"
	type="text/css" />
		<? if ($this->csExtraCSS) {?>
<link rel="StyleSheet"
	href="<?=$this->getCSSRef()?>css/nonauto/<?=$this->csExtraCSS?>"
	type="text/css" />
		<? } ?>
<script src='<?=$this->getLibRef()?>js.php' type='text/javascript'
	language='javascript'></script>
		<?
		if($this->csSysJsRef!=''){
			?>
<script src="<?=$this->csSysJsRef?>js.php" type="text/javascript"
	language="javascript"></script>
			<?
		}
		?>
<script type='text/javascript' language='javascript'>
  if (soPopUpManager.getPopUpById('<?=$msPopUpId?>')) {
    eval('var self = soPopUpManager.getPopUpById(\'<?=$msPopUpId?>\');');
  }
</script>
<title><?=$this->csWindowTitle?></title>
</head>
<body topmargin=0 leftmargin=0 <?=$msRenderLoad?>
	oncontextmenu='return false'>
<form name="form_fwd" id="form_fwd" enctype="multipart/form-data"
	onsubmit="return false;" method="post"><span id="FWDLoading"
	class="FWDLoading" style="display: none;"><?=FWDLanguage::getPHPStringValue('FWDLoading','Carregando')?><img
	src="<?=$this->csGfxRef?>loading.gif" /></span> <?=$poDialog->draw()?>
<div id='tooltip_code_tt_init' name='tooltip_code_tt_init'></div>
<script src="<?=$this->getJsRef()?>wz_tooltip.js" type="text/javascript"
	language="javascript"></script> <iframe
	name="force_report_download_iframe" id="force_report_download_iframe"
	style="display: none"></iframe> <iframe name="fwd_iframe_upload"
	id="fwd_iframe_upload" style="display: none"></iframe> <input
	type="hidden" value="" /> <script type='text/javascript'
	language='javascript'>if(self && self.finishLoad)self.finishLoad();</script>
</form>
		<?=$this->csTracker?>
</body>
</html>
		<?
	}
	/*criado o input hidden acima (sem nome e sem valor) para que a fun��o js_submit funcione em uma tela em que n�o tem elementos de form
	 assim o bug da tela document_read_details seja corrigido (a fun��o js_submit � utilizada nessa tela para fazer o download do documento)*/

	/**
	 * Adiciona um novo erro conhecido.
	 *
	 * <p>Adiciona um novo erro conhecido ao array de erros conhecidos.</p>
	 * @access public
	 * @param string $psErrorStringId Id do erro
	 * @param array $paMsgSubstrings Array de strings que caracterizam o erro
	 * @static
	 */
	public static function addKnownError($psErrorStringId,$paMsgSubstrings){
		self::$coErrorHandler->addKnownError($psErrorStringId,$paMsgSubstrings);
	}

	/**
	 * Remove um erro conhecido.
	 *
	 * <p>Remove um erro conhecido do array de erros conhecidos.</p>
	 * @access public
	 * @param string $psErrorStringId Id do erro
	 * @static
	 */
	public static function removeKnownError($psErrorStringId){
		self::$coErrorHandler->removeKnownError($psErrorStringId);
	}

	/**
	 * Seta o modo de debug do sistema.
	 *
	 * <p>Seta o modo de debug do sistema.</p>
	 * @access public
	 * @param boolean $pbMode True ou Flase
	 */
	public static function setDebugMode($pbMode,$piType,$pbEncrypted=false) {
		self::$cbDebugMode = $pbMode;
		self::$ciDebugType = $piType;
		self::$cbDebugEncrypted=$GLOBALS['load_method']==XML_STATIC?false:$pbEncrypted; //quando compilado, sempre encripta o debug.txt
	}

	/**
	 * Retorna se o debug est� sendo criptografado ou n�o.
	 *
	 * <p>Retorna se o debug est� sendo criptografado ou n�o.</p>
	 * @access public
	 * @return boolean True ou False
	 */
	public static function isDebugEncrypted() {
		return self::$cbDebugEncrypted;
	}

	/**
	 * Retorna o modo de debug do sistema.
	 *
	 * <p>Retorna o modo de debug do sistema.</p>
	 * @access public
	 * @return boolean True ou Flase
	 */
	public function getDebugMode() {
		return self::$cbDebugMode;
	}

	/**
	 * Retorna o modo de debug do sistema.
	 *
	 * <p>Retorna o modo de debug do sistema.</p>
	 * @access public
	 * @return boolean True ou Flase
	 */
	public static function getDebugType() {
		return self::$ciDebugType;
	}

	/**
	 * Retorna o caminho para o arquivo de debug.
	 *
	 * <p>Retorna o caminho para o arquivo de debug.</p>
	 * @access public
	 * @return string Caminho para o arquivo
	 */
	public static function getDebugFilePath() {
		return self::$csDebugFilePath;
	}

	/**
	 * Seta o caminho para o arquivo de debug.
	 *
	 * <p>Seta o caminho para o arquivo de debug.</p>
	 * @access public
	 * @return string Caminho para o arquivo
	 */
	public static function setDebugFilePath($psDebugFilePath){
		self::$csDebugFilePath = $psDebugFilePath;
	}

	/**
	 * Seta o valor do atributo SysRef.
	 *
	 * <p>Seta o valor do atributo SysRef (url absoluta ou relativa).</p>
	 * @access public
	 * @param string $psSysRef Valor de base (url)
	 */
	public function setSysRef($psSysRef) {
		$this->csSysRef = $psSysRef;
	}

	/**
	 * Retorna o valor do atributo SysRef.
	 *
	 * <p>Retorna o valor do atributo SysRef (url absoluta ou relativa).</p>
	 * @access public
	 * @return string Valor de base (url)
	 */
	public function getSysRef() {
		return $this->csSysRef;
	}

	/**
	 * Seta o valor do atributo LibRef.
	 *
	 * <p>Seta o valor do atributo LibRef (refer�ncia para base).</p>
	 * @access public
	 * @param string $psLibRef Refer�ncia para base (url)
	 */
	public function setLibRef($psLibRef) {
		$this->csLibRef = $psLibRef;
	}

	/**
	 * Retorna o valor do atributo LibRef.
	 *
	 * <p>Retorna o valor do atributo LibRef (refer�ncia para base).</p>
	 * @access public
	 * @return string Refer�ncia para base (url)
	 */
	public function getLibRef() {
		return $this->csLibRef;
	}

	/**
	 * Seta o valor do atributo JsRef.
	 *
	 * <p>Seta o valor do atributo JsRef (refer�ncia para arquivos javascript).</p>
	 * @access public
	 * @param string $psJsRef Refer�ncia para arquivos javascript (url)
	 */
	public function setJsRef($psJsRef) {
		$this->csJsRef = $psJsRef;
	}

	/**
	 * Retorna o valor do atributo JsRef.
	 *
	 * <p>Retorna o valor do atributo JsRef (refer�ncia para arquivos javascript).</p>
	 * @access public
	 * @return string Refer�ncia para arquivos javascript (url)
	 */
	public function getJsRef() {
		return $this->csJsRef;
	}

	/**
	 * Seta o valor do atributo SysJsRef.
	 *
	 * <p>Seta o valor do atributo SysJsRef (refer�ncia para arquivos javascript
	 * do sistema).</p>
	 * @access public
	 * @param string $psSysJsRef Refer�ncia para arquivos javascript do sistema
	 */
	public function setSysJsRef($psSysJsRef){
		$this->csSysJsRef = $psSysJsRef;
	}

	/**
	 * Retorna o valor do atributo SysJsRef.
	 *
	 * <p>Retorna o valor do atributo SysJsRef (refer�ncia para arquivos javascript
	 * do sistema).</p>
	 * @access public
	 * @return string Refer�ncia para arquivos javascript do sistema
	 */
	public function getSysJsRef(){
		return $this->csSysJsRef;
	}

	/**
	 * Seta o valor do atributo GfxRef.
	 *
	 * <p>Seta o valor do atributo GfxRef (refer�ncia para arquivos de imagens).</p>
	 * @access public
	 * @param string $psGfxRef Refer�ncia para arquivos de imagens (url)
	 */
	public function setGfxRef($psGfxRef) {
		$this->csGfxRef = $psGfxRef;
	}

	/**
	 * Retorna o valor do atributo GfxRef.
	 *
	 * <p>Retorna o valor do atributo GfxRef (refer�ncia para arquivos de imagens).</p>
	 * @access public
	 * @return string Refer�ncia para arquivos de imagens (url)
	 */
	public function getGfxRef() {
		return $this->csGfxRef;
	}

	/**
	 * Seta o valor do atributo CSSRef.
	 *
	 * <p>Seta o valor do atributo CSSRef (refer�ncia para arquivos de CSS).</p>
	 * @access public
	 * @param string $psCSSRef Refer�ncia para arquivos de CSS
	 */
	public function setCSSRef($psCSSRef) {
		$this->csCSSRef = $psCSSRef;
	}

	/**
	 * Retorna o valor do atributo CSSRef.
	 *
	 * <p>Retorna o valor do atributo CSSRef (refer�ncia para arquivos de css).</p>
	 * @access public
	 * @return string Refer�ncia para arquivos de imagens CSS
	 */
	public function getCSSRef() {
		return $this->csCSSRef;
	}

	/**
	 * Seta o valor do atributo SysRefBasedOnTabMain.
	 *
	 * <p>Seta o valor do atributo SysRefBasedOnTabMain (refer�ncia para sys_ref baseado
	 * no diret�rio onde encontra-se o arquivo tab_main.php).</p>
	 * @access public
	 * @param string $psSysRefBasedOnTabMain Refer�ncia para sys_ref baseado no tab_main
	 */
	public function setSysRefBasedOnTabMain($psSysRefBasedOnTabMain) {
		$this->csSysRefBasedOnTabMain = $psSysRefBasedOnTabMain;
	}

	/**
	 * Retorna o valor do atributo SysRefBasedOnTabMain.
	 *
	 * <p>Retorna o valor do atributo SysRefBasedOnTabMain (refer�ncia para sys_ref baseado
	 * no diret�rio onde encontra-se o arquivo tab_main.php).</p>
	 * @access public
	 * @return string Refer�ncia para sys_ref baseado no tab_main
	 */
	public function getSysRefBasedOnTabMain() {
		return $this->csSysRefBasedOnTabMain;
	}

	/**
	 * Retorna o valor do atributo cbDumpStarted.
	 *
	 * <p>Retorna o valor do atributo cbDumpStarted (determina se o dump_html foi iniciado).</p>
	 * @access public
	 * @return boolean True ou False
	 */
	public function isDumpStarted() {
		return $this->cbDumpStarted;
	}

	/**
	 * Retorna o valor do atributo cbXmlLoadRunning.
	 *
	 * <p>Retorna o valor do atributo cbXmlLoadRunning (determina se o xml_load est� rodando).</p>
	 * @access public
	 * @return boolean True ou False
	 */
	public function isXmlLoadRunning() {
		return $this->cbXmlLoadRunning;
	}

	/**
	 * Obt�m uma sess�o armazenada
	 *
	 * <p>Recebe um objeto sess�o e, se ele j� existe armazenado, retorna a vers�o
	 * armazenada, sen�o retorna ele mesmo.</p>
	 * @access public
	 * @param FWDSession $poSession Sess�o
	 * @return FWDSession Sess�o
	 */
	public function getSession($poSession) {
		$this->checkSession();
		$msId = $poSession->getId();
		if (!isset($_SESSION[$msId])) {
			$_SESSION[$msId] = $poSession;
		}
		return $_SESSION[$msId];
	}

	/**
	 * Obt�m uma sess�o a partir de seu id.
	 *
	 * <p>M�todo para obter uma sess�o a partir de seu identificador.</p>
	 * @access public
	 * @param string $psSessionId Id da sess�o
	 * @return FWDSession Sess�o
	 */
	public function getSessionById($psSessionId) {
		$this->checkSession();
		if (!isset($_SESSION[$psSessionId]))
		trigger_error("There is no session with the id '{$psSessionId}'!", E_USER_ERROR);
		return $_SESSION[$psSessionId];
	}

	/**
	 * Inicia a sess�o caso ela ainda n�o tenha sido iniciada.
	 *
	 * <p>M�todo para iniciar a sess�o caso ela ainda n�o tenha sido iniciada.</p>
	 * @access private
	 */
	public function checkSession() {
		if (!$this->cbSessionStarted) {
			session_start();
			$this->cbSessionStarted = true;
		}
	}

	/**
	 * Destr�i uma sess�o
	 *
	 * <p>Destr�i uma sess�o.</p>
	 * @access public
	 * @param string $psSessionId Id da sess�o a ser destru�da
	 */
	public function destroySession($psSessionId) {
		unset($_SESSION[$psSessionId]);
	}

	/**
	 * Retorna o metodo a ser utilizado para carregar os objetos PHP
	 *
	 * <p>Retorna o metodo a ser utilizado para carregar os objetos PHP.</p>
	 * @access public
	 * @return integer Constante que indica o metodo a ser utilizado para carregar os objetos PHP
	 */
	public function getXmlLoadMethod() {
		return $this->ciXmlLoadMethod;
	}

	/**
	 * Retorna a URL atual
	 *
	 * <p>Retorna a URL atual (sem a query string).</p>
	 * @access public
	 * @return string URL atual
	 */
	public static function getCurrentURL(){
		if(isset($_SERVER['HTTPS']))
		$msProtocol = ($_SERVER['HTTPS']=='on'?'https':'http');
		else
		$msProtocol = 'http';

		return "$msProtocol://${_SERVER['HTTP_HOST']}${_SERVER['PHP_SELF']}";
  }

  /**
   * Converte a string para ISO.
   *
   * <p>M�todo para converter a string para ISO.</p>
   * @access public
   * @return string String no formato ISO
   */
  public static function convertToISO($psValue, $pbForce = false){return $psValue;
  /*VERIFICAR EM QUE SITUA��O � UM ARRAY*/
  if (!is_array($psValue)) {
  	$msValue = $psValue;
  	if (!$pbForce) {
  		$msEncoding = mb_detect_encoding($msValue,"ISO-8859-1,UTF-8");
  		if ($msEncoding == 'UTF-8') $msValue = mb_convert_encoding($psValue,"ISO-8859-1", "UTF-8");
  	}
  	else $msValue = mb_convert_encoding($msValue,"ISO-8859-1", "UTF-8");
  	return $msValue;
  }
  else return $psValue;
  }

  /**
   * Envia os headers para for�ar download
   *
   * <p>Envia os headers para for�ar download</p>
   * @access public
   * @param string $psFilename Nome do arquivo (opcional)
   */
  public static function forceDownload($psFilename=''){
  	header("Cache-Control: ");// leave blank to avoid IE errors
  	header("Pragma: ");// leave blank to avoid IE errors
  	header("Content-type: application/octet-stream");
  	if($psFilename){
  		header("Content-Disposition: attachment; filename=\"$psFilename\"");
  	}
  }

  /**
   * Retorna o Usu�rio que deve ser utilizado como prefixo das functions do banco
   *
   * <p>Usu�rio que deve ser utilizado como prefixo das functions do banco</p>
   * @access public
   * @return string Usu�rio da conex�o com o banco que deve ser utilizado antes do nome das functions
   */
  public static function getFunctionUserDB(){
  	return self::$coDBConnection->getFunctionUserDB();
  }

  /**
   * Retorna a chamada da fun��o de acordo com o banco de dados.
   *
   * <p>M�todo para retornar a chamada da fun��o de acordo com o banco de dados.</p>
   * @access public
   * @param string $psFunction Fun��o que deve ser chamada
   * @return string Chamada da fun��o
   */
  public static function getFunctionCall($psFunction){
  	return self::$coDBConnection->getFunctionCall($psFunction);
  }

  /**
   * Retorna uma express�o de "like" case insensitive entre uma coluna e uma string constante.
   *
   * <p>Retorna uma express�o de "like" case insensitive entre uma coluna e uma
   * string constante de acordo com o banco sendo usado.</p>
   * @access public
   * @param string $psColumn Nome/express�o da coluna
   * @param string $psString A string constante
   * @return string Uma string contendo a express�o
   */
  public static function getCaseInsensitiveLike($psColumn,$psString){
  	if(self::$coDBConnection->getDatabaseType()==DB_MSSQL){
  		return "{$psColumn} LIKE '%{$psString}%'";
  	}else{
  		//return "UPPER({$psColumn}) LIKE '%".strtoupper($psString)."%'";
  		return "UPPER({$psColumn}) LIKE UPPER('%".$psString."%')";
  	}
  }

  /**
   * Retorna o banco de dados que est� sendo usado pelo sistema.
   *
   * <p>M�todo para retornar o banco de dados que est� sendo usado pelo sistema.</p>
   * @access public
   * @return integer Constante que identifica qual o banco que est� sendo usado.
   */
  public static function getDatabaseType(){
  	return self::$coDBConnection->getDatabaseType();
  }

  /**
   * Retorna um valor constante do banco de dados.
   *
   * <p>M�todo para retornar um valor constante do banco de dados. Necess�rio
   * pois no Oracle n�o s�o aceitas consultas no formato 'select 1' e sim
   * no formato 'select 1 from dual'.</p>
   * @access public
   * @param string $psValue Valores [Ex: '1 as first, 2 as second, ...]
   * @return string Select para retornar uma constante
   */
  public function selectConstant($psValues) {
  	return self::$coDBConnection->selectConstant($psValues);
  }

  /**
   * Abstra��o da fun��o array_diff_key que s� existe a partir do php 5.1.0
   *
   * <p>abstrai a existencia da fun��o array_diff_key do php, para que funcione no php 5.0.x</p>
   * @access public
   * @param Array $maPar1 array cujas keys nao devem estar no $maPar2 para essa key e seu valor irem para o resultado
   * @param Array $maPar2 array alvo da compara��o
   * @return Array array associativo com o resultado do array_diff_key
   */
  public static function fwd_array_diff_key($maPar1,$maPar2){
  	if(function_exists('array_diff_key')){
  		return array_diff_key($maPar1,$maPar2);
  	}
  	else
  	{
  		$maReturn = array();
  		foreach($maPar1 as $miKey => $miValue){
  			if(!isset($maPar2[$miKey]))
  			$maReturn[$miKey] = $miValue;
  		}
  		return $maReturn;
  	}
  }

  /**
   * Retorna a hora local corrigindo erro de GMT.
   *
   * <p>O PHP est� considerando o timezone do sistema operacional para calcular a hora
   * Esta fun��o retorna a hora local corrigindo este erro.</p>
   * @access public
   * @return timestamp
   */
  public static function getTime(){
  	return time();
  }

  /**
   * Retorna um timestamp baseado em uma data no formato YYYY-MM-DD HH-NN-SS.
   *
   * <p>M�todo para retornar um timestamp baseado em uma data no formato YYYY-MM-DD HH-NN-SS.</p>
   * @access public
   * @param string $psDate Data
   * @return integer Timestamp
   */
  public static function getTimestamp($psDate){
  	return self::getInstance()->getConnection()->getTimestamp($psDate);
  }

  /**
   * Retorna um timestamp baseado em uma data em um dos formatos aceitos.
   *
   * <p>Retorna um timestamp baseado em uma data em um dos seguintes formatos:</p>
   * <p>timestamp</p>
   * <p>YYYY-MM-DD</p>
   * <p>YYYY-MM-DD HH-NN-SS</p>
   * <p>mx_shortdate_format</p>
   * @access public
   * @param string $pmDate Data
   * @return integer Timestamp
   */
  public static function dateToTimestamp($pmDate){
  	if($pmDate){
  		if(is_int($pmDate)){
  			return $pmDate;
  		}elseif(is_string($pmDate)){
  			if(preg_match('/^\d{4}-\d{2}-\d{2}/',$pmDate)){
  				return strtotime($pmDate);
  			}elseif(preg_match('|^\d{1,2}/\d{1,2}/\d{1,4}$|',$pmDate)){
  				$msDateFormat = FWDLanguage::getPHPStringValue('mx_shortdate_format','');
  				$maDate = explode('/',$pmDate);
  				if($msDateFormat=='%d/%m/%Y'){
  					return mktime(0, 0, 0, $maDate[1], $maDate[0], $maDate[2]);
  				}elseif($msDateFormat=='%m/%d/%Y'){
  					return mktime(0, 0, 0, $maDate[0], $maDate[1], $maDate[2]);
  				}
  			}
  		}
  	}else{
  		return 0;
  	}
  	trigger_error("Invalid date format.",E_USER_ERROR);
  }

  /**
   * Retorna uma data no formato do banco.
   *
   * <p>M�todo para retornar uma data no formato do banco.</p>
   * @access public
   * @param string $piTimestamp Timestamp
   * @return string Data
   */
  public static function timestampToDBFormat($piTimestamp){
  	return self::getInstance()->getConnection()->timestampFormat($piTimestamp);
  }

  /**
   * Retorna uma data formatada para exibi��o.
   *
   * <p>M�todo para retornar uma data (com horas e minutos) formatada para exibi��o.</p>
   * @access public
   * @param mixed $pmDate Timestamp ou string de data
   * @param boolean $pbIsTimestamp Indica se o primeiro par�metro � um timestamp ou uma string de data
   * @return string Data formatada
   */
  public static function getDate($pmDate = '', $pbIsTimestamp = false){
  	if(!$pbIsTimestamp){
  		if($pmDate) $miTimestamp = self::getTimestamp($pmDate);
  		else $miTimestamp = self::getTime();
  	}else{
  		$miTimestamp = $pmDate;
  	}

  	$msDateFormat = FWDLanguage::getPHPStringValue('mx_shortdate_format','');
  	if($msDateFormat == "%m/%d/%Y"){
  		return date("m/d/Y h:i a", $miTimestamp);
  	}elseif($msDateFormat == "%d/%m/%Y"){
  		return date("d/m/Y H:i", $miTimestamp);
  	}else{
  		trigger_error("Invalid date format!", E_USER_WARNING);
  	}
  }

  /**
   * Retorna uma data "curta" formatada para exibi��o.
   *
   * <p>M�todo para retornar uma data "curta" (dia, m�s e ano) formatada para exibi��o.</p>
   * @access public
   * @param mixed $pmDate Timestamp ou string de data
   * @param boolean $pbIsTimestamp Indica se o primeiro par�metro � um timestamp ou uma string de data
   * @return string Data formatada
   */
  public static function getShortDate($pmDate = '', $pbIsTimestamp = false){
  	if(!$pbIsTimestamp){
  		if($pmDate) $miTimestamp = self::getTimestamp($pmDate);
  		else $miTimestamp = self::getTime();
  	}else{
  		$miTimestamp = $pmDate;
  	}

  	$msDateFormat = FWDLanguage::getPHPStringValue('mx_shortdate_format','');
  	if($msDateFormat == "%m/%d/%Y"){
  		return date("m/d/Y", $miTimestamp);
  	}elseif($msDateFormat == "%d/%m/%Y"){
  		return date("d/m/Y", $miTimestamp);
  	}else{
  		trigger_error("Invalid date format!", E_USER_WARNING);
  	}
  }

  /**
   * M�todo para deserializar uma string.
   *
   * <p>M�todo para deserializar uma string. Criado para resolver
   * erros que ocorriam com strings codificadas em UTF-8</p>
   * @access public
   * @param string $psString String
   * @return mixed Valor deserializado
   */
  public static function unserializeString($psString, $pbReplace=true) {
  	if ($pbReplace) return unserialize(preg_replace('!s:(\d+):"(.*?)";!se', "'s:'.strlen('$2').':\"$2\";'", $psString));
  	else return unserialize($psString);
  }

  /**
   * M�todo para para abstrair a fun��o utf8_decode do php.
   *
   * <p>M�todo para para abstrair a fun��o utf8_decode do php.</p>
   * @access public
   * @param string $psString String
   * @return string String decodificada
   */
  public static function decodeUTF8($psString) {
  	return utf8_decode($psString);
  }

  /**
   * Escreve uma string no arquivo de debug.
   *
   * <p>Escreve uma string no arquivo de debug.</p>
   * @access public
   * @param string psString String a escrever
   */
  public function writeStringDebug($psString="",$piType=FWD_DEBUG_INFO) {
  	if ($this->getDebugMode()) {
  		if ($piType & self::$ciDebugType) {
  			$msSessionId = session_id()?session_id():'none';
  			switch($piType) {
  				case FWD_DEBUG_DB: $msDebugType='DB'; break;
  				case FWD_DEBUG_INFO: $msDebugType='INFO'; break;
  				case FWD_DEBUG_ERROR: $msDebugType='ERROR'; break;
  				case FWD_DEBUG_FWD1: $msDebugType='FWD1'; break;
  				case FWD_DEBUG_FWD2: $msDebugType='FWD2'; break;
  				default: $msDebugType='DEBUG'; break;
  			}
  			$msString = "($msDebugType) ".date('Y-m-d H:i:s')." [Session Id: {$msSessionId}] {$psString}";
  			$moCrypt = new FWDCrypt();
  			if (self::$cbDebugEncrypted) $msString = $moCrypt->getIV().$moCrypt->encrypt($msString);
  			$msString = PHP_EOL."#%#".(self::$cbDebugEncrypted?"D":"d").$msString."\n#%#".PHP_EOL;
  			$msDebugFile = self::getInstance()->getSysRef().self::getDebugFilePath();
  			$moHandle = fopen($msDebugFile, "a+");
  			fwrite($moHandle, $msString);
  		}
  	}
  	return;
  }

  public function arrayToString(&$item,$key) {
  	if (is_array($item)) {
  		$temp_array = $item;
  		array_walk($temp_array, array($this,'arrayToString'));
  		$item = "[array] {".implode(",",$temp_array)."}";
  	} elseif(is_object($item) && !method_exists($item,'__toString')){
  		$item = "[".get_class($item)." object]";
  	}
  }

  public function writeFunction2Debug($psClassName,$psFunctionName,$paAttributes,$piType=FWD_DEBUG_INFO,$psFileName='',$piLineNumber='') {
  	if ($this->getDebugMode()) {
  		if ($piType & self::$ciDebugType) {
  			foreach($paAttributes as $miIndex=>$mmAttribute){
  				if(is_object($mmAttribute) && !method_exists($mmAttribute,'__toString')){
  					$paAttributes[$miIndex] = "[".get_class($mmAttribute)." object]";
  				} elseif(is_array($mmAttribute)) {
  					array_walk($mmAttribute,array($this,'arrayToString'));
  					$paAttributes[$miIndex] = "[array] { ".implode(',',$mmAttribute)." }";
  				}
  			}
  			$msString = ($psFileName?"[$psFileName: $piLineNumber] ":"").$psClassName.": ".$psFunctionName."(".implode(",",$paAttributes).")";
  			$this->writeStringDebug($msString,$piType);
  		}
  	}
  }

}

?>
<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDLicense. Classe de licen�a para os sistemas
 *
 * <p>Classe de licen�a para os sistemas.</p>
 * @package FWD5
 * @subpackage base
 */
 
class FWDLicense {

	/**
	 * Chave p�blica
	 * @var string
	 * @access protected
	 */
	protected $csPublicKey = '';

	/**
	 * Chave privada
	 * @var string
	 * @access protected
	 */
	protected $csPrivateKey = '';

	/**
	 * Array de atributos
	 * @var array
	 * @access protected
	 */
	protected $caAttributes = array();

	/**
	 * Array de Ativa��o
	 * @var array
	 * @access protected
	 */
	protected $caActivation = array();

	/**
	 * Sistema Operacional
	 * @var string
	 * @access protected
	 */
	protected $csOS;

	/**
	 * Arquivo de Licen�a
	 * @var string
	 * @access protected
	 */
	protected $csLicenseFile;

	/**
	 * Arquivo de Licen�a Encriptado
	 * @var string
	 * @access protected
	 */
	protected $csEncryptedLicenseFile;

	/**
	 * Indica erro na leitura de dados com gzuncompress
	 * @var string
	 * @access protected
	 */
	protected $cbGZUncompressError=false;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDLicense.</p>
	 * @access public
	 * @param string $psPublicKey Chave p�blica
	 * @param string $psLicenseFile Arquivo de licen�a
	 * @param string $psEncryptedLicenseFile Arquivo que cont�m o hash de ativa��o do sistema
	 */
	public function __construct($psPublicKey,$psLicenseFile="",$psEncryptedLicenseFile=""){
		$this->csPublicKey = $psPublicKey;
		$this->caActivation['WIN']['COMMAND'] = "ipconfig /all";
		$this->caActivation['WIN']['HWSTRING'] = "Physical Address. . . . . . . . . : ";
		$this->caActivation['WIN']['CHAR'] = "\r\n";
		$this->caActivation['LINUX']['COMMAND'] = "ifconfig";
		$this->caActivation['LINUX']['HWSTRING'] = "HWaddr ";
		$this->caActivation['LINUX']['CHAR'] = " ";
		if (substr(PHP_OS, 0, 3) == "WIN") {
			$this->csOS = "WIN";
		} else {
			$this->csOS = "LINUX";
		}
		if (stripos($_SERVER['SERVER_SOFTWARE'],'iis')!==false) { //Server: IIS
			$this->caActivation[$this->csOS]['SERVER_IP'] = "LOCAL_ADDR";
		} else { //Server: Apache, etc.
			$this->caActivation[$this->csOS]['SERVER_IP'] = "SERVER_ADDR";
		}

		$this->csLicenseFile = $psLicenseFile;
		$this->csEncryptedLicenseFile = $psEncryptedLicenseFile;
		FWDLanguage::getPHPStringValue('error_corrupt_license',"License is corrupted.");
		FWDLanguage::getPHPStringValue('error_corrupt_hash',"Hash is corrupted.");
		FWDLanguage::getPHPStringValue('error_permission_denied',"Permission denied in activation file.");
	}

	/**
	 * Calcula a assinatura para os dados especificados
	 *
	 * <p>Calcula a assinatura para os dados especificados usando a chave privada.</p>
	 * @access public
	 * @param string $psPrivateKey Chave privada
	 * @param string $psData Dados a serem assinados
	 */
	public function sign($psPrivateKey, $psData){
		$mrKeyId = openssl_get_privatekey($psPrivateKey);
		$msSignature="";
		openssl_sign($psData,$msSignature,$mrKeyId);
		openssl_free_key($mrKeyId);
		return $msSignature;
	}

	/**
	 * Verifica a assinatura para os dados especificados
	 *
	 * <p>Verifica se a assinatura para os dados especificados est� correta usando
	 * a chave p�blica.</p>
	 * @access public
	 * @param string $psSignature Assinatura
	 * @param string $psData Dados a partir dos quais a assinatura foi gerada
	 * @return boolean True, se a assinatura est� correta
	 */
	public function verify($psSignature, $psData){
		$mrKeyId = openssl_get_publickey($this->csPublicKey);
		$mbOk = openssl_verify($psData,$psSignature,$mrKeyId);
		openssl_free_key($mrKeyId);
		return ($mbOk?true:false);
	}

	/**
	 * Seta um atributo da licen�a
	 *
	 * <p>Seta um atributo da licen�a.</p>
	 * @access public
	 * @param string $psName Nome do atributo
	 * @param string $psValue Valor do atributo
	 */
	public function setAttribute($psName, $psValue){
		$this->caAttributes[strtolower($psName)] = $psValue;
	}

	/**
	 * Retorna um atributo da licen�a
	 *
	 * <p>Retorna um atributo da licen�a.</p>
	 * @access public
	 * @param string $psName Nome do atributo
	 * @return string Valor do atributo
	 */
	public function getAttribute($psName){
		$psName = strtolower($psName);
		if(isset($this->caAttributes[$psName])) return $this->caAttributes[$psName];
		else return false;
	}

	/**
	 * Seta a chave privada
	 *
	 * <p>Seta a chave privada.</p>
	 * @access public
	 * @param string $psPrivateKey Chave privada
	 */
	public function setPrivateKey($psPrivateKey){
		$this->csPrivateKey = $psPrivateKey;
	}

	/**
	 * Retorna uma string com os atributos da licen�a
	 *
	 * <p>Retorna uma string contendo uma lista dos atributos da licen�a, com seus
	 * valores.</p>
	 * @access public
	 * @return string String com os atributos da licen�a
	 */
	public function summarizeLicense(){
		$msRet = "; &lt;?php exit(); ?&gt;\n";
		foreach($this->caAttributes as $msKey => $msValue){
			$msRet.= "; $msKey: $msValue \n";
		}
		return $msRet;
	}

	/**
	 * Gera uma chave de licen�a
	 *
	 * <p>Gera uma chave de licen�a.</p>
	 * @access public
	 * @return string Chave de licen�a
	 */
	public function generateLicenseKey(){
		$msSerializedAttrs = serialize($this->caAttributes);
		$msCodedAtrrs = base64_encode(gzcompress($msSerializedAttrs));
		$msSignature = $this->Sign($this->csPrivateKey,$msSerializedAttrs);
		$msCodedSignature = base64_encode(gzcompress($msSignature));
		$msLicense = "--------------------- BEGIN LICENSE KEY ------------------------\n"
		.chunk_split("$msCodedAtrrs|$msCodedSignature",64)
		."---------------------- END LICENSE KEY -------------------------\n";
		return $msLicense;
	}

	/**
	 * Gera uma licen�a
	 *
	 * <p>Gera uma licen�a (atributos + chave).</p>
	 * @access public
	 * @return string licen�a
	 */
	public function generateLicense(){
		return $this->summarizeLicense().$this->generateLicenseKey();
	}

	/**
	 * Carrega e parseia a licen�a de um arquivo
	 *
	 * <p>Carrega e parseia a licen�a de um arquivo. Retorna um array com dois
	 * elementos, uma string contendo os atributos codificados e uma string
	 * contendo a assinatura codificada. Se n�o conseguir ler o arquivo, retorna
	 * false.</p>
	 * @access protected
	 * @param string $psFileName Nome do arquivo de licen�a
	 * @return array Array contendo os atributos e a assinatura codificados
	 */
	protected function loadLicenseFromFile($psFileName){
		if(!file_exists($psFileName) || !is_readable($psFileName)) return false;
		$msContent = implode('',file($psFileName));
		$maLines = explode("\n",$msContent);
		$miI = 0;
		while($maLines[$miI][0]==';') $miI++;
		$maArr = array();
		$miLines = count($maLines);
		for($miI++;$miI<$miLines;$miI++){
			if(strpos($maLines[$miI],'-')!==false) break;
			$maArr[] = trim($maLines[$miI]);
		}
		$maParts = explode('|',implode('',$maArr));
		return $maParts;
	}

	/**
	 * Carrega de um arquivo e verifica a licen�a
	 *
	 * <p>Carrega de um arquivo e verifica a licen�a. Se a licen�a for inv�lida, gera
	 * um erro.</p>
	 * @access public
	 * @param string $psFileName Nome do arquivo de licen�a
	 * @return boolean Indica se a licen�a � v�lida ou n�o
	 */
	public function loadLicense($psFileName){
		$mbReturn = false;
		FWDWebLib::addKnownError('error_corrupt_license',array('gzuncompress','base64_decode','unserialize','Uninitialized string offset'));
		$maParts = $this->loadLicenseFromFile($psFileName);
		if($maParts!==false){
			$msSerializedAttrs = gzuncompress(base64_decode($maParts[0]));
			$msSignature = gzuncompress(base64_decode($maParts[1]));
			if($this->verify($msSignature,$msSerializedAttrs)){
				$maAttributes = FWDWebLib::unserializeString($msSerializedAttrs);
				foreach($maAttributes as $msName => $msValue){
					$this->setAttribute($msName,$msValue);
				}
				$mbReturn = true;
			}
		}
		FWDWebLib::removeKnownError('error_corrupt_license');
		return $mbReturn;
	}

	/**
	 * Retorna o MAC Address da m�quina
	 *
	 * <p>Retorna o MAC Address da m�quina</p>
	 * @access private
	 * @return string MAC Address
	 */
	private function getMACAddress() {
		$msMACAddress = shell_exec($this->caActivation[$this->csOS]['COMMAND']);
		$miIPPos = strpos($msMACAddress,$_SERVER[$this->caActivation[$this->csOS]['SERVER_IP']]);
		$msMACAddress = substr($msMACAddress,0,$miIPPos);
		$msHWString = $this->caActivation[$this->csOS]['HWSTRING'];
		$miHWStringPos = strrpos($msMACAddress,$msHWString);
		$msMACAddress = substr($msMACAddress,$miHWStringPos + strlen($msHWString));
		$miSpacePos = strpos($msMACAddress,$this->caActivation[$this->csOS]['CHAR']);
		$msMACAddress = substr($msMACAddress,0,$miSpacePos);
		return $msMACAddress;
	}

	/**
	 * Retorna o Hostname da m�quina
	 *
	 * <p>Retorna o Hostname da m�quina</p>
	 * @access private
	 * @return string Hostname
	 */
	private function getHostname() {
		$msHostname = shell_exec('hostname');
		$miSpacePos = strpos($msHostname,"\n");
		$msHostname = substr($msHostname,0,$miSpacePos);
		return $msHostname;
	}

	/**
	 * Retorna o hash md5 do MACAddress|Hostname|Licen�a
	 *
	 * <p>Retorna o hash md5 do MACAddress|Hostname|Licen�a</p>
	 * @access public
	 * @return string Hash md5
	 */
	public function getSystemHash() {
		if (!$this->csLicenseFile || !file_exists($this->csLicenseFile) || !is_readable($this->csLicenseFile)) {
			return false;
		}
		$msLicenseContent = "";
		$maLicenseContent = file($this->csLicenseFile);
		foreach($maLicenseContent as $msLine) {
			if ($msLine[0]==";" || $msLine[0]=="-") {
				continue;
			}
			$msLicenseContent.=$msLine;
		}
		$msMD5SystemHash = md5($this->getMACAddress()."|".$this->getHostname())."|".md5(trim($msLicenseContent));
		return chunk_split(base64_encode($msMD5SystemHash),64);
	}

	/**
	 * Trata erros da fun��o gzuncompress.
	 *
	 * <p>Trata erros da fun��o gzuncompress.</p>
	 * @access private
	 */
	private function activationGZUncompressError() {
		$this->cbGZUncompressError = true;
	}

	/**
	 * Retira a criptografia de um hash.
	 *
	 * <p>Retira a criptografia de um hash obtido do arquivo criptografado
	 * ou da gera��o de um hash de ativa��o.</p>
	 * @access public
	 * @param string psSource String em base64 contendo o hash criptografado.
	 * @return string Hash descriptografado.
	 */
	public function decryptHash($psSource) {
		FWDWebLib::addKnownError('error_corrupt_hash',array('base64_decode','mdecrypt_generic'));
		$msActivationHash = base64_decode($psSource);
		$moCrypt = new FWDCrypt();
		$moCrypt->setIV(substr($msActivationHash,0,strlen($moCrypt->getIV())));
		$msActivationHash = substr($msActivationHash,strlen($moCrypt->getIV()));
		$msActivationHash = $moCrypt->decrypt($msActivationHash);
		FWDWebLib::removeKnownError('error_corrupt_hash');
		return $msActivationHash;
	}

	/**
	 * Gera um hash de ativa��o
	 *
	 * <p>Gera um hash de ativa��o.</p>
	 * @access public
	 * @param string psSystemHash Hash md5 do sistema, obtido atrav�s de getSystemHash() pelo cliente
	 * @return string Hash de ativa��o
	 */
	public function generateActivationHash($psSystemHash){
		$msCodedHash = base64_encode(gzcompress($psSystemHash));
		$msSignature = $this->sign($this->csPrivateKey,$psSystemHash);
		$msCodedSignature = base64_encode(gzcompress($msSignature));
		$msCodedTimeStamp = base64_encode(gzcompress(time()));
		$msActivationHash = "$msCodedHash|$msCodedSignature|$msCodedTimeStamp";
		$moCrypt = new FWDCrypt();
		return chunk_split(base64_encode($moCrypt->getIV().$moCrypt->encrypt($msActivationHash)),64);
	}

	/**
	 * Desativa o sistema, gerando um hash de desativa��o.
	 *
	 * <p>Desativa o sistema, gerando um hash de desativa��o.
	 * Limpa o arquivo de ativa��o criptografado e escreve
	 * o hash de desativa��o nele.</p>
	 * @access public
	 */
	public function generateDeactivationHash(){
		$moActivationHandler = new FWDActivationHandler($this->csEncryptedLicenseFile);
		$miMaxTimeStamp = 0;
		while($msHash = $moActivationHandler->fetch()) {
			$msHash = $this->decryptHash($msHash);
			$maParts = explode('|',$msHash);
			if (array_key_exists(2,$maParts)) {
				$miTimestamp = gzuncompress(base64_decode($maParts[2]));
				$miMaxTimeStamp = $miTimestamp>$miMaxTimeStamp?$miTimestamp:$miMaxTimeStamp;
			}
		}
		$msSystemHash = $this->getSystemHash();
		$msCodedSystemHash = base64_encode(gzcompress($msSystemHash));
		$msCodedToken = base64_encode(gzcompress("DEACTIVATED"));
		$msCodedTimeStamp = base64_encode(gzcompress($miMaxTimeStamp));
		$msDeactivationHash = "$msCodedSystemHash|$msCodedToken|$msCodedTimeStamp";
		$moCrypt = new FWDCrypt();
		$msDeactivationHash = chunk_split(base64_encode($moCrypt->getIV().$moCrypt->encrypt($msDeactivationHash)),64);
		$moActivationHandler->clearFile();
		return $moActivationHandler->insertHash($msDeactivationHash);
	}

	/**
	 * Escreve o hash no arquivo de ativa��o criptografado.
	 *
	 * <p>Escreve o hash no arquivo de ativa��o criptografado.
	 * Se o arquivo possuir um hash de desativa��o, verifica se o timestamp
	 * da ativa��o � maior que o da desativa��o, para prevenir que o usu�rio
	 * insira um ativa��o gerada antes da desativa��o. Ent�o limpa o arquivo
	 * antes de escrever o novo hash.</p>
	 * @access public
	 * @param string psHash Hash que ser� escrito no arquivo.
	 */
	public function writeHashToFile($psHash) {
		$moActivationHandler = new FWDActivationHandler($this->csEncryptedLicenseFile);
		if ($msHash = $moActivationHandler->getHash()) {
			$msHash = $this->decryptHash($msHash);
			$maParts = explode('|',$msHash);
			if(array_key_exists(1,$maParts)) {
				FWDWebLib::addKnownError('error_corrupt_activation_hash',array('gzuncompress','base64_decode','mdecrypt_generic'));
				$msToken = gzuncompress(base64_decode($maParts[1]));
				if ($msToken=='DEACTIVATED') {
					$miDeactivationTimestamp = gzuncompress(base64_decode($maParts[2]));
					$msHash = $this->decryptHash($psHash);
					$maHash = explode('|',$msHash);
					$miTimestamp = gzuncompress(base64_decode($maHash[2]));
					if ($miTimestamp <= $miDeactivationTimestamp) return false;
					$moActivationHandler->clearFile();
				}
				FWDWebLib::removeKnownError('error_corrupt_activation_hash');
			}
		}
		return $moActivationHandler->insertHash($psHash);
	}

	/**
	 * Verifica a validade de um hash.
	 *
	 * <p>Verifica a validade de um hash.</p>
	 * @access public
	 * @param string psHash Hash
	 * @return V�lido(true) / Inv�lido(false)
	 */
	public function checkHash($psHash) {
		$msHash = $this->decryptHash($psHash);
		set_error_handler(array($this,'activationGZUncompressError'));
		$maParts = explode('|',$msHash);
		$msSystemHash = gzuncompress(base64_decode($maParts[0]));
		$msSignature = gzuncompress(base64_decode($maParts[1]));
		restore_error_handler();
		if ($this->cbGZUncompressError) {
			return false;
		}
		if ($msSignature=='DEACTIVATED') {
			return false;
		}
		if ($this->verify($msSignature,$msSystemHash)) {
			$msMD5Hash = base64_decode($msSystemHash);
			$maMD5Parts = explode('|',$msMD5Hash);
			$msCurrentSystemHash = base64_decode($this->getSystemHash());
			$maCurrentMD5Parts = explode('|',$msCurrentSystemHash);
			if ($maCurrentMD5Parts[0] != $maMD5Parts[0]) return false;
			if ($maCurrentMD5Parts[1] != $maMD5Parts[1]) return false;
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Verifica se o sistema est� ativado.
	 *
	 * <p>Verifica se o sistem est� ativado, passando por cada registro de ativa��o no
	 * arquivo de ativa��es criptografado.</p>
	 * @access public
	 * @return Ativado(true) / Desativado(false)
	 */
	public function isActive() {
		$moActivationHandler = new FWDActivationHandler($this->csEncryptedLicenseFile);
		while($msHash = $moActivationHandler->fetch()) {
			if ($this->checkHash($msHash))
			return true;
		}
		return false;
	}

	/**
	 * Verifica se o sistema est� dentro do per�odo de ativa��o, e retorna um
	 * array com os dados do per�odo de ativa��o
	 *
	 * <p>Verifica se o sistema est� dentro do per�odo de ativa��o, e retorna um
	 * array com os dados do per�odo de ativa��o.</p>
	 * @access public
	 * @return array(isActivationPeriod, newActivationPeriodCounter,
	 * newActivationPeriodTime, newActivationPeriodHash)
	 */
	public function getActivationPeriodData($piActivationPeriodCounter,$psActivationPeriodTime,$psActivationPeriodHash) {
		$mbReturn = false;
		$miReturnCounter = 0;
		$msReturnTime = 0;
		$msReturnHash = "";
		if ($piActivationPeriodCounter<intval($this->getAttribute("activation_period"))) { //Verifica se o contador eh menor que o periodo para ativacao
			if (md5("SYSTEM_ACTIVATION_PERIOD_TIME:$psActivationPeriodTime;SYSTEM_ACTIVATION_PERIOD_COUNTER:$piActivationPeriodCounter")==$psActivationPeriodHash) { //Verifica se o hash confirma os dados passados
				$mbReturn = true;
				if (date('Y-m-d',strtotime($psActivationPeriodTime)) != date('Y-m-d')) { //se o time passado for diferente da data atual
					$msReturnTime = date('Y-m-d H:i:s');
					$miReturnCounter = $piActivationPeriodCounter + floor(abs((strtotime($psActivationPeriodTime)-strtotime($msReturnTime))/86400)); //Contador = Contador + diferenca entre a data de hoje e ultima utilizacao
					$msReturnHash = md5("SYSTEM_ACTIVATION_PERIOD_TIME:$msReturnTime;SYSTEM_ACTIVATION_PERIOD_COUNTER:$miReturnCounter");
				}
			}
		}
		return array($mbReturn,$miReturnCounter,$msReturnTime,$msReturnHash);
	}

	/**
	 * Retorna o hash de desativa��o, caso o sistema esteja desativado.
	 *
	 * <p>Retorna o hash de desativa��o, caso o sistema esteja desativado.
	 * Se o sistema n�o estiver desativado, retorna string vazia.</p>
	 * @return string Hash de desativa��o
	 */
	public function getDeactivationHash() {
		$moActivationHandler = new FWDActivationHandler($this->csEncryptedLicenseFile);
		if ($msHash = $moActivationHandler->getHash()) {
			$msDeactivationHash = $this->decryptHash($msHash);
			$maParts = explode("|",$msDeactivationHash);
			set_error_handler(array($this,'activationGZUncompressError'));
			$msToken = gzuncompress(base64_decode($maParts[1]));
			restore_error_handler();
			if ($msToken == "DEACTIVATED") {
				return $msHash;
			}
		}
		return '';
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDXmlCompile. Singleton. Compila os arquivos XML da FWD5.
 *
 * <p>Classe singleton que compila os arquivos XML da FWD5. Percorre
 * recursivamente toda a estrutura de diret�rios da Framework FWD5 em busca
 * de arquivos com a extens�o '.xml'. Cria uma lista com todos os arquivos XML
 * de um dado diret�rio, e, para cada um, executa o m�todo xml_load para
 * obter os objetos PHP, serializa-os, comprime-os e grava os dados em um
 * arquivo TAR. Garante a exist�ncia de uma e apenas uma inst�ncia da classe
 * FWDXmlCompile.</p>
 * @package FWD5
 * @subpackage base
 */
class FWDXmlCompile {

	/**
	 * Inst�ncia �nica da classe FWDXmlCompile
	 * @staticvar FWDXmlCompile
	 * @access private
	 */
	private static $coXmlCompile = NULL;

	/**
	 * Nome do arquivo TAR onde ser�o armazenados os arquivos XMC
	 * @var string
	 * @access private
	 */
	private $csTarFileName;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDXmlCompile.</p>
	 * @access private
	 */
	private function __construct() {
		$this->csTarFileName = "forms.xmc";
	}

	/**
	 * Retorna a inst�ncia �nica da classe FWDXmlCompile (singleton).
	 *
	 * <p>Retorna a inst�ncia �nica da classe FWDXmlCompile. Se a inst�ncia
	 * ainda n�o existe, cria a �nica inst�ncia. Se a inst�ncia j� existe,
	 * retorna a �nica inst�ncia (singleton).</p>
	 * @access public
	 * @return FWDXmlCompile Inst�ncia �nica da classe FWDXmlCompile
	 * @static
	 */
	public static function getInstance() {
		if (self::$coXmlCompile == NULL) {
			self::$coXmlCompile = new FWDXmlCompile();
		}
		else {}	// inst�ncia �nica j� existe, retorne-a
		return self::$coXmlCompile;
	}

	/**
	 * Seta o nome do arquivo TAR.
	 *
	 * <p>Seta o nome do arquivo TAR onde ser�o armazenados os arquivos XMC.</p>
	 * @access public
	 * @param string $psTarFileName Nome do arquivo TAR
	 */
	public function setAttrTarFileName($psTarFileName) {
		$this->csTarFileName = $psTarFileName;
	}

	/**
	 * Percorre a estrutura de diret�rios da Framework FWD5.
	 *
	 * <p>Percorre recursivamente (a partir do diret�rio passado como par�metro,
	 * percorrendo seus subdiret�rios) toda a estrutura de diret�rios da
	 * Framework em busca de arquivos com a extens�o '.xml'. Uma vez que o
	 * m�todo termine de percorrer os arquivos do diret�rio, antes de fech�-lo
	 * e continuar as chamadas recursivas, � realizada uma chamada do m�todo
	 * createTar(), o qual � o respons�vel por carregar os objetos PHP,
	 * serializ�-los, comprimi-los e grav�-los em um arquivo TAR (estas
	 * opera��es s�o realizadas para cada arquivo com a extens�o '.xml'
	 * encontrados naquele diret�rio).</p>
	 *
	 * @access public
	 * @param string $psDir Refer�ncia para o diret�rio root da estrutura de diret�rios que cont�m os arquivos '.xml'
	 * @param string $psRecursive Indica se a compila��o deve entrar nos subdiret�rios
	 */
	public function xmlCompile($psDir, $pbRecursive = false) {
		if (is_dir($psDir)) {
			echo "Entering directory '".$psDir."'<br/>";
			if ($dirHandler = opendir($psDir)) {
				while ((($file = readdir($dirHandler)) !== false) && $pbRecursive) {
					if( ($file[0] == '.') || (is_file($file)) )
					continue;
					$this->xmlCompile($psDir."/".$file);
				}
				$this->createTar($psDir);
				closedir($dirHandler);
			}
		}
	}

	/**
	 * 'Compila' todos os arquivos XML de um diret�rio e grava-os em um arquivo TAR.
	 *
	 * <p>Cria um array com os nomes de todos os arquivos com a extens�o '.xml'
	 * encontrados no diret�rio passado como par�metro. Para cada um, executa
	 * o m�todo xml_load() da classe singleton FWDXmlLoad, o qual ir� gerar
	 * os objetos PHP, os quais ser�o serializados e comprimidos. Uma vez que
	 * todos os arquivos XML do diret�rio tenha passado por este procedimento,
	 * � realizada a grava��o destes em um arquivo com a extens�o '.tar'.</p>
	 *
	 * @access private
	 * @param string $psDir Refer�ncia para um diret�rio onde encontram-se os arquivos '.xml'
	 */
	private function createTar($psDir) {
		$maFiles = scandir($psDir,1);
		$msPath = FWDWebLib::getObject('Base_Ref').$psDir."/";
		$maXMLFiles = array();
		foreach($maFiles as $msFileName)
		if(strlen($msFileName) > 4 && (strrpos($msFileName, ".xml") == strlen($msFileName)-4))
		$maXMLFiles[] = $msFileName;

		echo "<br/><hr><h3>XML FILES FROM DIRECTORY '". $psDir ."' [Total: ".count($maXMLFiles)."]</h3>";
		foreach($maXMLFiles as $msFileName)
		echo $msFileName."<br/>";

		if (count($maXMLFiles)>0) {
			echo "<br/><br/>Encoding files...<br/>";
				
			$moXmlLoad = FWDXmlLoad::getInstance();
			$moXmlLoad->setCompiling(true);
				
			$maXMCFiles = array();
			foreach($maXMLFiles as $xmlFile) {
				FWDWebLib::cleanObjects();

				$moXmlLoad->xml_load($msPath.$xmlFile);

				$msData = "";
				foreach ($moXmlLoad->getAttrXmlObjects() as $moObjectName => $moObject) {
					$msData .= base64_encode(gzcompress(serialize($moObject)))."\n";
				}
				$maXMCFiles[$xmlFile] = $msData;
				echo "<br/><b>[ File <font color='blue'>".$xmlFile."</font> encoded ]</b>";
			}
			$moXmlLoad->setCompiling(false);
				
			echo "<br/><br/>Creating TAR archive ({$this->csTarFileName}) ...<br/>";
			$msArchive = "";
			$miStoredFiles = 0;
			foreach ($maXMCFiles as $msCurrentName => $msCurrentData) {
				if (strlen($msCurrentName) > 99) {
					trigger_error("Could not add {$msPath}{$msCurrentName} to tar archive: Filename is too long.",E_USER_WARNING);
					continue;
				}
				$maCurrentStat = stat($msPath.$msCurrentName);

				$msBlock = pack("a100a8a8a8a12a12a8a1a100a6a2a32a32a8a8a155a12",
				$msCurrentName,	// filename
				sprintf("%07o", $maCurrentStat[2]),		// mode
				sprintf("%07o", $maCurrentStat[4]),		// uid
				sprintf("%07o", $maCurrentStat[5]),		// gid
				sprintf("%011o", strlen($msCurrentData)),	// filesize
				sprintf("%011o", $maCurrentStat[9]),		// mtime
					"        ",		// checksum
				0,				// filetype (always 0=>file)
					"",				// filename (if simbolic link [not implemented])
					"ustar ",		// magic
					" ",			// version
					"Unknown",		// uname
					"Unknown",		// gname
					"",				// devmajor
					"",				// devminor
					"",				// just to fill 512
					""				// just to fill 512
				);
				$miChecksum = 0;
				for ($i = 0; $i < 512; $i++)
				$miChecksum += ord(substr($msBlock, $i, 1));
				$miChecksum = pack("a8", sprintf("%07o", $miChecksum));

				$msBlock = substr_replace($msBlock, $miChecksum, 148, 8);
				if (is_link($msCurrentName) || $maCurrentStat[7] == 0)
				$msArchive .= $msBlock;
				else {
					$msCurrentData .= "\n";
					$delta = (strlen($msCurrentData) % 512) > 0 ? (512 - (strlen($msCurrentData) % 512)) : 0;
					for ($i=0; $i<$delta; $i++)
					$msCurrentData .= "\0";
					$msArchive .= $msBlock.$msCurrentData;
				}
				$miStoredFiles++;
				echo "#$miStoredFiles";
			}
				
			$msArchive .= pack("a1024", "");
				
			if ( !($tarFileStream = fopen($msPath.$this->csTarFileName, "wb+")) )
			trigger_error("Error creating file {$msPath}{$this->csTarFileName}",E_USER_ERROR);
			elseif (!fwrite($tarFileStream,$msArchive))
			trigger_error("Error writing data to file {$msPath}{$this->csTarFileName}",E_USER_ERROR);
			elseif (!fclose($tarFileStream))
			trigger_error("Error closing file {$msPath}{$this->csTarFileName}",E_USER_ERROR);
			else
			echo "<center><h4><font color='green'>FILE {$msPath}{$this->csTarFileName} SUCCESSFULLY CREATED!</font></h4></center>";
		}
		else
		echo "<br/>No XML files found in this directory.<br/>";
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe para convers�o de c�digos de cores
 *
 * <p>Classe para convers�o de c�digos de cores. Converte de nomes pra hexa ou
 * RGB e entre RGB e hexa.</p>
 * @package FWD5
 * @subpackage report
 */
class FWDColor {

	/**
	 * Tabela de cores pr�-definidas
	 * @var array
	 * @access protected
	 */
	protected static $caTable = array(
    'aliceblue' => 'f0f8ff',
    'antiquewhite' => 'faebd7',
    'aqua' => '00ffff',
    'aquamarine' => '7fffd4',
    'azure' => 'f0ffff',
    'beige' => 'f5f5dc',
    'bisque' => 'ffe4c4',
    'black' => '000000',
    'blanchedalmond' => 'ffebcd',
    'blue' => '0000ff',
    'blueviolet' => '8a2be2',
    'brown' => 'a52a2a',
    'burlywood' => 'deb887',
    'cadetblue' => '5f9ea0',
    'chartreuse' => '7fff00',
    'chocolate' => 'd2691e',
    'coral' => 'ff7f50',
    'cornflowerblue' => '6495ed',
    'cornsilk' => 'fff8dc',
    'crimson' => 'dc143c',
    'cyan' => '00ffff',
    'darkblue' => '00008b',
    'darkcyan' => '008b8b',
    'darkgoldenrod' => 'b8860b',
    'darkgray' => 'a9a9a9',
    'darkgreen' => '006400',
    'darkkhaki' => 'bdb76b',
    'darkmagenta' => '8b008b',
    'darkolivegreen' => '556b2f',
    'darkorange' => 'ff8c00',
    'darkorchid' => '9932cc',
    'darkred' => '8b0000',
    'darksalmon' => 'e9967a',
    'darkseagreen' => '8fbc8f',
    'darkslateblue' => '483d8b',
    'darkslategray' => '2f4f4f',
    'darkturquoise' => '00ced1',
    'darkviolet' => '9400d3',
    'deeppink' => 'ff1493',
    'deepskyblue' => '00bfff',
    'dimgray' => '696969',
    'dodgerblue' => '1e90ff',
    'feldspar' => 'd19275',
    'firebrick' => 'b22222',
    'floralwhite' => 'fffaf0',
    'forestgreen' => '228b22',
    'fuchsia' => 'ff00ff',
    'gainsboro' => 'dcdcdc',
    'ghostwhite' => 'f8f8ff',
    'gold' => 'ffd700',
    'goldenrod' => 'daa520',
    'gray' => '808080',
    'green' => '008000',
    'greenyellow' => 'adff2f',
    'honeydew' => 'f0fff0',
    'hotpink' => 'ff69b4',
    'indianred' => 'cd5c5c',
    'indigo' => '4b0082',
    'ivory' => 'fffff0',
    'khaki' => 'f0e68c',
    'lavender' => 'e6e6fa',
    'lavenderblush' => 'fff0f5',
    'lawngreen' => '7cfc00',
    'lemonchiffon' => 'fffacd',
    'lightblue' => 'add8e6',
    'lightcoral' => 'f08080',
    'lightcyan' => 'e0ffff',
    'lightgoldenrodyellow' => 'fafad2',
    'lightgrey' => 'd3d3d3',
    'lightgreen' => '90ee90',
    'lightpink' => 'ffb6c1',
    'lightsalmon' => 'ffa07a',
    'lightseagreen' => '20b2aa',
    'lightskyblue' => '87cefa',
    'lightslateblue' => '8470ff',
    'lightslategray' => '778899',
    'lightsteelblue' => 'b0c4de',
    'lightyellow' => 'ffffe0',
    'lime' => '00ff00',
    'limegreen' => '32cd32',
    'linen' => 'faf0e6',
    'magenta' => 'ff00ff',
    'maroon' => '800000',
    'mediumaquamarine' => '66cdaa',
    'mediumblue' => '0000cd',
    'mediumorchid' => 'ba55d3',
    'mediumpurple' => '9370d8',
    'mediumseagreen' => '3cb371',
    'mediumslateblue' => '7b68ee',
    'mediumspringgreen' => '00fa9a',
    'mediumturquoise' => '48d1cc',
    'mediumvioletred' => 'c71585',
    'midnightblue' => '191970',
    'mintcream' => 'f5fffa',
    'mistyrose' => 'ffe4e1',
    'moccasin' => 'ffe4b5',
    'navajowhite' => 'ffdead',
    'navy' => '000080',
    'oldlace' => 'fdf5e6',
    'olive' => '808000',
    'olivedrab' => '6b8e23',
    'orange' => 'ffa500',
    'orangered' => 'ff4500',
    'orchid' => 'da70d6',
    'palegoldenrod' => 'eee8aa',
    'palegreen' => '98fb98',
    'paleturquoise' => 'afeeee',
    'palevioletred' => 'd87093',
    'papayawhip' => 'ffefd5',
    'peachpuff' => 'ffdab9',
    'peru' => 'cd853f',
    'pink' => 'ffc0cb',
    'plum' => 'dda0dd',
    'powderblue' => 'b0e0e6',
    'purple' => '800080',
    'red' => 'ff0000',
    'rosybrown' => 'bc8f8f',
    'royalblue' => '4169e1',
    'saddlebrown' => '8b4513',
    'salmon' => 'fa8072',
    'sandybrown' => 'f4a460',
    'seagreen' => '2e8b57',
    'seashell' => 'fff5ee',
    'sienna' => 'a0522d',
    'silver' => 'c0c0c0',
    'skyblue' => '87ceeb',
    'slateblue' => '6a5acd',
    'slategray' => '708090',
    'snow' => 'fffafa',
    'springgreen' => '00ff7f',
    'steelblue' => '4682b4',
    'tan' => 'd2b48c',
    'teal' => '008080',
    'thistle' => 'd8bfd8',
    'tomato' => 'ff6347',
    'turquoise' => '40e0d0',
    'violet' => 'ee82ee',
    'violetred' => 'd02090',
    'wheat' => 'f5deb3',
    'white' => 'ffffff',
    'whitesmoke' => 'f5f5f5',
    'yellow' => 'ffff00',
    'yellowgreen' => '9acd32'
    );

    /**
     * Testa se uma string � uma cor v�lida
     *
     * <p>Testa se uma string � uma cor v�lida, ou seja, se � o nome ou o c�digo
     * hexadecimal de uma cor.</p>
     * @access public
     * @param string $psColor String que identifica a cor
     * @return boolean True indica que a string � uma cor v�lida
     */
    public static function isValid($psColor){
    	$psColor = strtolower($psColor);
    	if(isset(self::$caTable[$psColor])) return true;
    	else return preg_match('/[0-9a-f]{6}/',$psColor);
    }

    /**
     * Obt�m os valores de R, G e B de uma cor a partir de seu c�digo hexadecimal
     *
     * <p>Obt�m os valores de R, G e B de uma cor a partir de seu c�digo hexadecimal.
     * Caso o par�metro passado n�o seja uma cor v�lida, retorna false.</p>
     * @access public
     * @param string $psHexCode C�digo hexadecimal da cor
     * @return array Array de inteiros contendo os valores de R, G e B da cor
     */
    public static function hexToRgb($psHexCode){
    	$psHexCode = strtolower($psHexCode);
    	if(!preg_match('/[0-9a-f]{6}/',$psHexCode)) return false;
    	$maRgb = array_map('hexdec',explode('|',chunk_split($psHexCode,2,'|')));
    	unset($maRgb[3]);
    	return $maRgb;
    }

    /**
     * Obt�m o c�digo hexadecimal de uma cor a partir de seus valores R, G e B
     *
     * <p>Obt�m o c�digo hexadecimal de uma cor a partir de seus valores R, G e B.
     * </p>
     * @access public
     * @param integer $piR Valor do componente R da cor
     * @param integer $piG Valor do componente G da cor
     * @param integer $piB Valor do componente B da cor
     * @return string C�digo hexadecimal da cor
     */
    public static function rgbToHex($piR,$piG,$piB){
    	return implode('',array_map(create_function('$n','$h=dechex($n);return (strlen($h)>1?$h:"0$h");'),array($piR,$piG,$piB)));
    }

    /**
     * Obt�m o c�digo hexadecimal de uma cor a partir de seu nome
     *
     * <p>Obt�m o c�digo hexadecimal de uma cor a partir de seu nome. Caso a cor
     * n�o conste na lista de nomes, retorna false.</p>
     * @access public
     * @param string $psName Nome da cor
     * @return string C�digo hexadecimal da cor
     */
    public static function nameToHex($psName){
    	$psName = strtolower($psName);
    	if(isset(self::$caTable[$psName])) return self::$caTable[$psName];
    	else return false;
    }

    /**
     * Obt�m os valores de R, G e B de uma cor a partir de seu nome
     *
     * <p>Obt�m os valores de R, G e B de uma cor a partir de seu nome. Caso a cor
     * n�o conste na lista de nomes, retorna false.</p>
     * @access public
     * @param string $psName Nome da cor
     * @return array Array de inteiros contendo os valores de R, G e B da cor
     */
    public static function nameToRgb($psName){
    	if(isset(self::$caTable[$psName])) return self::hexToRgb(self::$caTable[$psName]);
    	else return false;
    }

}

?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDACLSecurity. Singleton. Implementa a Access Control List da FWD5.
 *
 * <p>Classe singleton que implementa a Access Control List da FWD5. Percorre
 * todos os elementos da tela, desabilitando os elementos para os quais o
 * usu�rio n�o possui permiss�o. Garante a exist�ncia de uma e apenas uma
 * inst�ncia da classe FWDACLSecurity.</p>
 * @package FWD5
 * @subpackage base
 */
class FWDACLSecurity {

	/**
	 * Inst�ncia �nica da classe FWDACLSecurity
	 * @staticvar FWDACLSecurity
	 * @access private
	 */
	private static $coACLSecurity = NULL;

	/**
	 * Array de tags que identificam elementos os quais n�o devem ser desenhados
	 * @var array
	 * @access private
	 */
	private $caNotAllowed = array();

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDACLSecurity.</p>
	 * @access private
	 */
	private function __construct() {
	}

	/**
	 * Retorna a inst�ncia �nica da classe FWDACLSecurity (singleton).
	 *
	 * <p>Retorna a inst�ncia �nica da classe FWDACLSecurity. Se a inst�ncia
	 * ainda n�o existe, cria a �nica inst�ncia. Se a inst�ncia j� existe,
	 * retorna a �nica inst�ncia (singleton).</p>
	 * @access public
	 * @return FWDACLSecurity Inst�ncia �nica da classe FWDACLSecurity
	 * @static
	 */
	public static function getInstance() {
		if (self::$coACLSecurity == NULL) {
			self::$coACLSecurity = new FWDACLSecurity();
		}
		else {}	// inst�ncia �nica j� existe, retorne-a
		return self::$coACLSecurity;
	}

	/**
	 * Seta o array de tags de elementos que n�o devem ser desenhados.
	 *
	 * <p>M�todo que seta o array de tags de elementos que n�o devem ser
	 * desenhados.</p>
	 * @access public
	 * @param array $paNotAllowed Array de tags
	 */
	public function setNotAllowed($paNotAllowed) {
		$this->caNotAllowed = $paNotAllowed;
	}

	/**
	 * Desabilita subelementos da view, se necess�rio.
	 *
	 * <p>Desabilita subelementos da view, se necess�rio. Percorre todos os
	 * subelementos da view, desabilitando aqueles cuja tag estiver presente
	 * no array de tags passado como par�metro.</p>
	 *
	 * @access public
	 * @param FWDView $poView Elemento no qual ser�o desabilitados subelementos (se necess�rio)
	 * @param array $paNotAllowed Array com as Tags que identificam elementos que n�o devem ser desenhados
	 */
	public function installSecurity($poView,$paNotAllowed = array()) {
		if ($paNotAllowed)
		$this->caNotAllowed = $paNotAllowed;
		$this->matchACL($poView);
	}

	/**
	 * Desabilita subelementos da view, se necess�rio.
	 *
	 * <p>Desabilita subelementos da view, se necess�rio. Percorre todos os
	 * subelementos da view, desabilitando aqueles cuja tag estiver presente
	 * no array de tags para as quais o usu�rio n�o possui permiss�o.</p>
	 *
	 * @access private
	 * @param FWDView $poView Elemento no qual ser�o desabilitados subelementos (se necess�rio)
	 */
	private function matchACL(FWDView $poView) {
		$maViews = $poView->collect();
		foreach ($maViews as $moObject) {
			if(method_exists($moObject,"getAttrTag")){
				if($moObject->getAttrTag()){
					$maTags = explode(':',$moObject->getAttrTag());
					foreach($maTags as $msTag){
						if (($msTag) && in_array($msTag,$this->caNotAllowed)){
							$moObject->setShouldDraw(false);
						}
					}
				}
			}
		}
	}

}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDMenuACLSecurity. Singleton. Aplica ACL nos FWDMenuItems de um FWDMenu.
 *
 * <p>Classe que aplica ACL nos FWDMenuItems de um FWDMenu. Percorre todos os
 * elementos do menu, desabilitando aqueles para os quais o usu�rio n�o possui
 * permiss�o. Garante a exist�ncia de uma e apenas uma inst�ncia da classe
 * FWDMenuACLSecurity.</p>
 * @package FWD5
 * @subpackage base
 */
class FWDMenuACLSecurity {

	/**
	 * Inst�ncia �nica da classe FWDMenuACLSecurity ou da classe FWDGridMenuACLSecurity
	 * @staticvar FWDMenuACLSecurity ou FWDGridMenuACLSecurity
	 * @access private
	 */
	private static $coMenuACLSecurity = NULL;

	/**
	 * N�mero �nico identificador de menus de contexto com itens desabilitados
	 * @staticvar integer
	 * @access private
	 */
	private static $ciNumMenus=0;

	/**
	 * Array de hash de Menus em que ja foram aplicadas ACLs
	 * @var array
	 * @access private
	 */
	private $caMenuHash = array();

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDMenuACLSecurity.</p>
	 * @access private
	 */
	private function __construct() {
	}

	/**
	 * Retorna a inst�ncia �nica da classe FWDMenuACLSecurity (singleton).
	 *
	 * <p>Retorna a inst�ncia �nica da classe FWDMenuACLSecurity. Se a inst�ncia
	 * ainda n�o existe, cria a �nica inst�ncia. Se a inst�ncia j� existe,
	 * retorna a �nica inst�ncia (singleton).</p>
	 * @access public
	 * @return FWDMenuACLSecurity Inst�ncia �nica da classe FWDMenuACLSecurity
	 * @static
	 */
	public static function getInstance() {
		if (self::$coMenuACLSecurity == NULL) {
			self::$coMenuACLSecurity = new FWDMenuACLSecurity();
		}
		else {}	// inst�ncia �nica j� existe, retorne-a
		return self::$coMenuACLSecurity;
	}

	/**
	 * Desabilita itens do menu, se necess�rio.
	 *
	 * <p>Desabilita itens do menu, se necess�rio. Percorre todos os itens
	 * do menu, desabilitando aqueles cuja tag estiver presente no array de
	 * tags para as quais o usu�rio n�o possui permiss�o.</p>
	 *
	 * @access public
	 * @param FWDMenu $poMenu FWDMenu no qual os itens ser�o desabilitados (se necess�rio)
	 * @param FWDACLSecurity $poACLSecurity ACL que cont�m as informa��es necess�rias para desabilitar os itens
	 * @return FWDMenu Menu com itens possivelmente desabilitados
	 */
	public function installMenuACL(FWDMenu $poMenu, FWDACLSecurity $poACLSecurity) {
		$poACLSecurity->installSecurity($poMenu);
		$msMD5 = "";
		foreach ($poMenu->getAttrItems() as $moItem) {
			$msMD5 .= $moItem->getValue();
			$msMD5 .= $moItem->getShouldDraw() ? "true" : "false";
		}
		$msMD5 = md5($msMD5);
		if (array_key_exists($msMD5,$this->caMenuHash)) {
			unset($poMenu);
		}
		else {
			$poMenu->setAttrName($poMenu->getAttrName() . self::$ciNumMenus++);
			$this->caMenuHash[$msMD5] = $poMenu;
		}
		return $this->caMenuHash[$msMD5];
	}

}
?>
<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDGridMenuACLSecurity. Aplica ACL a um Menu e adiciona este na Grid.
 *
 * <p>Classe que aplica ACL a um Menu e adiciona este na Grid.</p>
 * @package FWD5
 * @subpackage base
 */
class FWDGridMenuACLSecurity extends FWDMenuACLSecurity {

	/**
	 * Inst�ncia �nica da classe FWDGridMenuACLSecurity
	 * @staticvar FWDGridMenuACLSecurity
	 * @access private
	 */
	private static $coGridMenuACLSecurity = NULL;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDGridMenuACLSecurity.</p>
	 * @access private
	 */
	private function __construct() {
	}

	/**
	 * Retorna a inst�ncia �nica da classe FWDGridMenuACLSecurity (singleton).
	 *
	 * <p>Retorna a inst�ncia �nica da classe FWDGridMenuACLSecurity. Se a
	 * inst�ncia ainda n�o existe, cria a �nica inst�ncia. Se a inst�ncia j�
	 * existe, retorna a �nica inst�ncia (singleton).</p>
	 * @access public
	 * @return FWDGridMenuACLSecurity Inst�ncia �nica da classe FWDGridMenuACLSecurity
	 * @static
	 */
	public static function getInstance() {
		if (self::$coGridMenuACLSecurity == NULL) {
			self::$coGridMenuACLSecurity = new FWDGridMenuACLSecurity();
		}
		else {}	// inst�ncia �nica j� existe, retorne-a
		return self::$coGridMenuACLSecurity;
	}

	/**
	 * Desabilita itens do menu, se necess�rio.
	 *
	 * <p>Desabilita itens do menu, se necess�rio. Percorre todos os itens
	 * do menu, desabilitando aqueles cuja tag estiver presente no array de
	 * tags para as quais o usu�rio n�o possui permiss�o.</p>
	 *
	 * @access public
	 * @param FWDMenu $poMenu FWDMenu no qual os itens ser�o desabilitados (se necess�rio)
	 * @param FWDACLSecurity $poACLSecurity ACL que cont�m as informa��es necess�rias para desabilitar os itens
	 * @param FWDGrid $poGrid Grid onde o menu ser� utilizado
	 * @param string $psLine Linha da Grid onde o menu ser� chamado
	 */
	public function installGridMenuACL(FWDMenu $poMenu, FWDACLSecurity $poACLSecurity, FWDGrid $poGrid, $psLine) {
		$moNewMenu = clone $poMenu;
		$moMenu = parent::installMenuACL($moNewMenu,$poACLSecurity);
		$moMenu->addElements($psLine.":");
		$poGrid->addMenu($moMenu);
	}

}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDParameter.
 *
 * <p>Classe utilizada para passar par�metros por refer�ncia.</p>
 * @package FWD5
 * @subpackage base
 */
class FWDParameter {

	/**
	 * Valor do par�metro
	 * @var mixed
	 * @access protected
	 */
	protected $cmValue;

	/**
	 * Seta o valor do par�metro
	 *
	 * <p>Seta o valor do par�metro.</p>
	 * @access public
	 * @param mixed $pmValue Valor a ser atribu�do
	 */
	public function setValue($pmValue){
		$this->cmValue = $pmValue;
	}

	/**
	 * Retorna o valor do par�metro
	 *
	 * <p>Retorna o valor do par�metro.</p>
	 *
	 * @access public
	 * @return mixed Valor do par�metro
	 */
	public function getValue(){
		return $this->cmValue;
	}

}
?><?php

/*********************************************************************
 *
 *             $HeadURL: syslog.php $
 * $LastChangedRevision: 1.1 $
 *             Language: PHP 4.x or higher
 *            Copyright: SysCo syst�mes de communication sa
 *         CreationDate: 2005-11-05
 *            CreatedBy: SysCo/al
 *     $LastChangedDate: 2005-12-24 $
 *       $LastChangedBy: SysCo/al $
 *              WebSite: http://developer.sysco.ch/php/
 *                Email: developer@sysco.ch
 *
 *
 * Description
 *
 *   The Syslog class is a syslog device implementation in PHP
 *   following the RFC 3164 rules.
 *
 *
 *   Severity values:
 *     0 Emergency: system is unusable
 *     1 Alert: action must be taken immediately
 *     2 Critical: critical conditions
 *     3 Error: error conditions
 *     4 Warning: warning conditions
 *     5 Notice: normal but significant condition (default value)
 *     6 Informational: informational messages
 *     7 Debug: debug-level messages
 *
 *
 * Usage
 *
 *   require_once('FWDSyslog.php');
 *   $syslog = new FWDSyslog($facility = 16, $severity = 5, $hostname = "", $fqdn= "", $ip_from = "", $process="", $content = "");
 *   $syslog->send($server = "", $content = "", $timeout = 0);
 *
 *
 * Examples
 *
 *   Example 1
 *     <?php
 *         require_once('FWDSyslog.php');
 *         $syslog = new FWDSyslog();
 *         $syslog->send('192.168.0.12', 'My first PHP syslog message');
 *     ?>
 *
 *   Example 2
 *     <?php
 *         require_once('FWDSyslog.php');
 *         $syslog = new FWDSyslog(23, 7, 'MYSERVER', 'myserver.mydomain.net', '192.168.0.1', 'webautomation');
 *         $syslog->send('192.168.0.12', 'My second PHP syslog message');
 *     ?>
 *
 *   Example 3
 *     <?php
 *         require_once('FWDSyslog.php');
 *         $syslog = new FWDSyslog();
 *         $syslog->setFacility(23);
 *         $syslog->setSeverity(7);
 *         $syslog->setHostname('MYSERVER');
 *         $syslog->setFqdn('myserver.mydomain.net');
 *         $syslog->setIpFrom('192.168.0.1');
 *         $syslog->setProcess('webautomation');
 *         $syslog->setContent('My third PHP syslog message');
 *         $syslog->setServer('192.168.0.12');
 *         $syslog->send();
 *     ?>
 *
 *   Example 4
 *     <?php
 *         // Do not follow the conventions of the RFC
 *         // and send a customized MSG part instead of
 *         // the recommanded format "process fqdn ip content"
 *         require_once('FWDSyslog.php');
 *         $syslog = new FWDSyslog();
 *         $syslog->setFacility(23);
 *         $syslog->setSeverity(7);
 *         $syslog->setHostname('MYSERVER');
 *         $syslog->setMsg('My customized MSG PHP syslog message');
 *         $syslog->setServer('192.168.0.12');
 *         $syslog->send();
 *     ?>
 *
 *
 * External file needed
 *
 *   none.
 *
 *
 * External file created
 *
 *   none.
 *
 *
 * Special issues
 *
 *   - Sockets support must be enabled.
 *     * In Linux and *nix environments, the extension is enabled at
 *       compile time using the --enable-sockets configure option
 *     * In Windows, PHP Sockets can be activated by un-commenting
 *       extension=php_sockets.dll in php.ini
 *
 *
 * Licence
 *
 *   Copyright (c) 2005, SysCo syst�mes de communication sa
 *   SysCo (tm) is a trademark of SysCo syst�mes de communication sa
 *   All rights reserved.
 *
 *   Redistribution and use in source and binary forms, with or without
 *   modification, are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *   - Neither the name of SysCo syst�mes de communication sa nor the names of its
 *     contributors may be used to endorse or promote products derived from this
 *     software without specific prior written permission.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 *   EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *   OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 *   SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 *   OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 *   TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *   EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * Change Log
 *
 *   2005-12-24 1.1 SysCo/al Generic release and documentation
 *   2005-11-05 1.0 SysCo/al Initial release
 *
 *********************************************************************/

class FWDSyslog
{

	/**
	 * Aplica��o do remetente do log
	 * Facility values:
	 * 0 kernel messages
	 * 1 user-level messages
	 * 2 mail system
	 * 3 system daemons
	 * 4 security/authorization messages
	 * 5 messages generated internally by syslogd
	 * 6 line printer subsystem
	 * 7 network news subsystem
	 * 8 UUCP subsystem
	 * 9 clock daemon
	 * 10 security/authorization messages
	 * 11 FTP daemon
	 * 12 NTP subsystem
	 * 13 log audit
	 * 14 log alert
	 * 15 clock daemon
	 * 16 local user 0 (local0) (valor padr�o)
	 * 17 local user 1 (local1)
	 * 18 local user 2 (local2)
	 * 19 local user 3 (local3)
	 * 20 local user 4 (local4)
	 * 21 local user 5 (local5)
	 * 22 local user 6 (local6)
	 * 23 local user 7 (local7)
	 * @var integer
	 * @access protected
	 */
	protected $ciFacility;

	/**
	 * Severidade do log
	 * Severity values:
	 * 0 Emergency: system is unusable
	 * 1 Alert: action must be taken immediately
	 * 2 Critical: critical conditions
	 * 3 Error: error conditions
	 * 4 Warning: warning conditions
	 * 5 Notice: normal but significant condition (valor padr�o)
	 * 6 Informational: informational messages
	 * 7 Debug: debug-level messages
	 * @var integer
	 * @access protected
	 */
	protected $ciSeverity;

	/**
	 * Hostname da aplica��o remetente, ex: myhost
	 * @var string
	 * @access protected
	 */
	protected $csHostname;

	/**
	 * Fully Qualified Domain Name, ex: myhost.mydomain.com
	 * @var string
	 * @access protected
	 */
	protected $csFQDN;

	/**
	 * Endere�o IP do Remetente
	 * @var string
	 * @access protected
	 */
	protected $csIpFrom;

	/**
	 * Nome do processo remetente
	 * @var string
	 * @access protected
	 */
	protected $csProcess;

	/**
	 * Conte�do da mensagem de log
	 * @var string
	 * @access protected
	 */
	protected $csContent;

	/**
	 * Definir mensagem completa a ser enviada, n�o obedecendo a RFC
	 * Se n�o definida ser�: "processo: FQDN IP conteudo"
	 * @var string
	 * @access protected
	 */
	protected $csMsg;

	/**
	 * IP do servidor syslog, valor padr�o: 127.0.0.1
	 * @var string
	 * @access protected
	 */
	protected $csServer;

	/**
	 * Porta do servidor syslog, valor padr�o: 514
	 * @var string
	 * @access protected
	 */
	protected $ciPort;

	/**
	 * Timeout da conex�o UDP (em segundos) , valor padr�o: 10
	 * @var string
	 * @access protected
	 */
	protected $ciTimeout;

	 

	function __construct($piFacility = 16, $piSeverity = 5, $psHostname = "", $psFQDN= "", $psIpFrom = "", $psProcess="", $psContent = "")
	{
		$this->csMsg      = '';
		$this->csServer   = '127.0.0.1';
		$this->ciPort     = 514;
		$this->ciTimeout  = 10;

		$this->ciFacility = $piFacility;

		$this->ciSeverity = $piSeverity;

		$this->csHostname = $psHostname;
		if ($this->csHostname == "")
		{
			if (isset($_ENV["COMPUTERNAME"]))
			{
				$this->csHostname = $_ENV["COMPUTERNAME"];
			}
			elseif (isset($_ENV["HOSTNAME"]))
			{
				$this->csHostname = $_ENV["HOSTNAME"];
			}
			else
			{
				$this->csHostname = "WEBSERVER";
			}
		}
		$this->csHostname = substr($this->csHostname, 0, strpos($this->csHostname.".", "."));

		$this->csFQDN = $psFQDN;
		if ($this->csFQDN == "")
		{
			if (isset($_SERVER["SERVER_NAME"]))
			{
				$this->csFQDN = $_SERVER["SERVER_NAME"];
			}
		}

		$this->csIpFrom = $psIpFrom;
		if ($this->csIpFrom == "")
		{
			if (isset($_SERVER["SERVER_ADDR"]))
			{
				$this->csIpFrom = $_SERVER["SERVER_ADDR"];
			}
		}

		$this->csProcess = $psProcess;
		if ($this->csProcess == "")
		{
			$this->csProcess = "FWD5";
		}

		$this->csContent = $psContent;
		if ($this->csContent == "")
		{
			$this->csContent = "FWD5 generated message";
		}

		FWDLanguage::getPHPStringValue('error_connection_syslog',"Could not connect to syslog server.");
	}

	function setFacility($piFacility)
	{
		$this->ciFacility = $piFacility;
	}


	function setSeverity($piSeverity)
	{
		$this->ciSeverity = $piSeverity;
	}


	function setHostname($psHostname)
	{
		$this->csHostname = $psHostname;
	}


	function setFqdn($psFQDN)
	{
		$this->csFQDN = $psFQDN;
	}


	function setIpFrom($psIpFrom)
	{
		$this->csIpFrom = $psIpFrom;
	}


	function setProcess($csProcess)
	{
		$this->csProcess = $csProcess;
	}


	function setContent($csContent)
	{
		$this->csContent = $csContent;
	}


	function setMsg($csMsg)
	{
		$this->csMsg = $csMsg;
	}


	function setServer($psServer)
	{
		$this->csServer = $psServer;
	}


	function setPort($piPort)
	{
		if ((intval($piPort) > 0) && (intval($piPort) < 65536))
		{
			$this->ciPort = intval($piPort);
		}
	}


	function setTimeout($piTimeout)
	{
		if (intval($piTimeout) > 0)
		{
			$this->ciTimeout = intval($piTimeout);
		}
	}


	function send($psServer = "", $psContent = "", $piTimeout = 0)
	{
		if ($psServer != "")
		{
			$this->csServer = $psServer;
		}

		if ($psContent != "")
		{
			$this->csContent = $psContent;
		}

		if (intval($piTimeout) > 0)
		{
			$this->ciTimeout = intval($piTimeout);
		}

		if ($this->ciFacility <  0) { $this->ciFacility =  0; }
		if ($this->ciFacility > 23) { $this->ciFacility = 23; }
		if ($this->ciSeverity <  0) { $this->ciSeverity =  0; }
		if ($this->ciSeverity >  7) { $this->ciSeverity =  7; }

		$this->csProcess = substr($this->csProcess, 0, 32);

		$miActualtime = time();
		$miMonth      = date("M", $miActualtime);
		$miDay        = substr("  ".date("j", $miActualtime), -2);
		$msHHMMSS     = date("H:i:s", $miActualtime);
		$miTimestamp  = $miMonth." ".$miDay." ".$msHHMMSS;

		$msPri    = "<".($this->ciFacility*8 + $this->ciSeverity).">";
		$msHeader = $miTimestamp." ".$this->csHostname;

		if ($this->csMsg != "")
		{
			$msMsg = $this->csMsg;
		}
		else
		{
			$msMsg = $this->csProcess.": ".$this->csFQDN." ".$this->csIpFrom." ".$this->csContent;
		}

		$msMessage = substr($msPri.$msHeader." ".$msMsg, 0, 1024);

		FWDWebLib::addKnownError('error_connection_syslog',array('fsockopen'));
		$fp = fsockopen("udp://".$this->csServer, $this->ciPort, $miErrno, $msErrstr);
		FWDWebLib::removeKnownError('error_connection_syslog');
		if($fp){
			fwrite($fp, $msMessage);
			fclose($fp);
			$msResult = $msMessage;
		}else{
			$msResult = "ERROR: $miErrno - $msErrstr";
		}
		return $msResult;
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe para criptografar e decriptografar.
 *
 * <p>Classe para criptografar e decriptografar.</p>
 * @package FWD5
 * @subpackage base
 */

class FWDCrypt {
	protected $csPublicKey = "-----BEGIN CERTIFICATE-----
MIIEFzCCA4CgAwIBAgIBATANBgkqhkiG9w0BAQQFADCBujELMAkGA1UEBhMCQlIx
GjAYBgNVBAgTEVJpbyBHcmFuZGUgZG8gU3VsMRUwEwYDVQQHEwxQb3J0byBBbGVn
cmUxIjAgBgNVBAoTGUF4dXIgSW5mb3JtYXRpb24gU2VjdXJpdHkxFjAUBgNVBAsT
DWxpY2Vuc2UgZGVwdC4xHDAaBgNVBAMTE0F4dXIgTGljZW5zZSBTeXN0ZW0xHjAc
BgkqhkiG9w0BCQEWD2Fsc0BheHVyLmNvbS5icjAeFw0wNDA2MDkxNzU0NTBaFw0w
OTA2MTAxNzU0NTBaMIG6MQswCQYDVQQGEwJCUjEaMBgGA1UECBMRUmlvIEdyYW5k
ZSBkbyBTdWwxFTATBgNVBAcTDFBvcnRvIEFsZWdyZTEiMCAGA1UEChMZQXh1ciBJ
bmZvcm1hdGlvbiBTZWN1cml0eTEWMBQGA1UECxMNbGljZW5zZSBkZXB0LjEcMBoG
A1UEAxMTQXh1ciBNb25pdG9yIFN5c3RlbTEeMBwGCSqGSIb3DQEJARYPYW1zQGF4
dXIuY29tLmJyMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCzKZ+FEPy6DbvF
IOQ6hp6pCnth0kr5FFSh39pMsq0ytv4RRAxnWxjPvyfx0w7vpnKbARF8zDaStN5K
JIxocA7SYmYM2ItYZdAu+6JU1m9/EYT8eb7w110cYdEIJJr+CPrx2iivXVYBbl9J
VkKlf/1xFnkwhIlZ6CYAwFfnxdIswwIDAQABo4IBKTCCASUwCQYDVR0TBAIwADAP
BglghkgBhvhCAQ0EAhYAMB0GA1UdDgQWBBTAEH8Y7ZVtOpNxtuO5djrXmDp59zCB
5wYDVR0jBIHfMIHcgBTIsD9pVTsIJx25scfn1t6xQmdPZaGBwKSBvTCBujELMAkG
A1UEBhMCQlIxGjAYBgNVBAgTEVJpbyBHcmFuZGUgZG8gU3VsMRUwEwYDVQQHEwxQ
b3J0byBBbGVncmUxIjAgBgNVBAoTGUF4dXIgSW5mb3JtYXRpb24gU2VjdXJpdHkx
FjAUBgNVBAsTDWxpY2Vuc2UgZGVwdC4xHDAaBgNVBAMTE0F4dXIgTGljZW5zZSBT
eXN0ZW0xHjAcBgkqhkiG9w0BCQEWD2Fsc0BheHVyLmNvbS5icoIBADANBgkqhkiG
9w0BAQQFAAOBgQAabwBzSL+Ee3up3MxfE0gz4UAmjtaJMTlmclz+fGX22CVVwMXX
1WhH4QacRA9UnZp5tYEAwPMXpOGrlgR0tAkI3O5SaLaki6qc07h5mqfczN2L2BMp
LvUwF7gTTmNITm+ooJRP96Q+16G/UhUp4FNQvJyG2OmovKnH/8ZKuGgwlQ==
-----END CERTIFICATE----- ";

	protected $csAlgorithm;
	protected $csMode;
	protected $coIV;
	protected $csKey;

	public function __construct() {
		$this->csAlgorithm=MCRYPT_BLOWFISH;
		$this->csMode=MCRYPT_MODE_CBC;
		$moModule = mcrypt_module_open($this->csAlgorithm,'',$this->csMode,'');
		$this->coIV = mcrypt_create_iv(mcrypt_enc_get_iv_size($moModule),MCRYPT_RAND);
		/*if (strpos($this->csPublicKey,"\r\n")===false) {
		 $this->csKey = substr(md5(str_replace("\n","\r\n",$this->csPublicKey)),0,mcrypt_enc_get_key_size($moModule));
		 } else {
		 $this->csKey = substr(md5($this->csPublicKey),0,mcrypt_enc_get_key_size($moModule));
		 }*/
		//O md5 abaixo � o md5 da chave p�blica acima, pois n�o faz diferen�a usar diretamente o c�digo md5 ou faz�-lo no momento em que � rodado o script, podendo gerar os bugs de codifica��o que ocorriam antes.
		$this->csKey = substr('22a8ceb57d73e0ec7e78fcc3d23fb480',0,mcrypt_enc_get_key_size($moModule));
		mcrypt_module_close($moModule);
	}

	public function setIV($psSource) {
		$moModule = mcrypt_module_open($this->csAlgorithm,'',$this->csMode,'');
		if (strlen($psSource) == mcrypt_enc_get_iv_size($moModule))
		$this->coIV = $psSource;
		mcrypt_module_close($moModule);
	}

	public function getIV() {
		return $this->coIV;
	}

	public function encrypt($psSource) {
		if (!trim($psSource))
		return '';
		$moModule = mcrypt_module_open($this->csAlgorithm,'',$this->csMode,'');
		mcrypt_generic_init($moModule, $this->csKey, $this->coIV);
		$enc = mcrypt_generic($moModule, base64_encode($psSource));
		mcrypt_generic_deinit($moModule);
		mcrypt_module_close($moModule);
		return base64_encode($enc)."\n";
	}

	public function decrypt($psSource) {
		$moModule = mcrypt_module_open($this->csAlgorithm,'',$this->csMode,'');
		if (!$this->coIV)
		return '';
		if (!trim($psSource))
		return '';
		mcrypt_generic_init($moModule, $this->csKey, $this->coIV);
		$dec = rtrim(mdecrypt_generic($moModule, base64_decode($psSource)),"\0");
		mcrypt_generic_deinit($moModule);
		mcrypt_module_close($moModule);
		return base64_decode($dec);
	}

	public function encryptNoBase64($psSource) {
		if (!trim($psSource))
		return '';
		$moModule = mcrypt_module_open($this->csAlgorithm,'',$this->csMode,'');
		mcrypt_generic_init($moModule, $this->csKey, $this->coIV);
		$enc = mcrypt_generic($moModule, $psSource);
		mcrypt_generic_deinit($moModule);
		mcrypt_module_close($moModule);
		return $enc;
	}

	public function decryptNoBase64($psSource) {
		if (!trim($psSource))
		return '';
		$moModule = mcrypt_module_open($this->csAlgorithm,'',$this->csMode,'');
		if (!$this->coIV)
		return '';
		mcrypt_generic_init($moModule, $this->csKey, $this->coIV);
		$dec = mdecrypt_generic($moModule, $psSource);
		mcrypt_generic_deinit($moModule);
		mcrypt_module_close($moModule);
		return $dec;
	}

	public function decryptEmbeddedIV($psSource) {
		$this->setIV(substr($psSource,0,strlen($this->getIV())));
		return $this->decrypt(substr($psSource,strlen($this->getIV())));
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDEmail. Classe para enviar emails.
 *
 * <p>Classe para enviar emails de texto puro ou HTML. Suporta anexos e imagens
 * embutidas no HTML.</p>
 * @package FWD5
 * @subpackage base
 */

include 'xpm/SMTP.php';

class FWDEmail {

	/**
	 * Remetente do email
	 * @var string
	 * @access protected
	 */
	protected $csFrom = '';

	/**
	 * Nome do remetente do email
	 * @var string
	 * @access protected
	 */
	protected $csSenderName = '';

	/**
	 * Destinat�rio do email
	 * @var string
	 * @access protected
	 */
	protected $csTo = '';

	/**
	 * Destinat�rios que receber�o c�pia
	 * @var string
	 * @access protected
	 */
	protected $csCc = '';

	/**
	 * Destinat�rios que receber�o c�pia oculta
	 * @var string
	 * @access protected
	 */
	protected $csBcc = '';

	/**
	 * Endere�o para onde devem ser enviadas respostas para o email
	 * @var string
	 * @access protected
	 */
	protected $csReplyTo = '';

	/**
	 * Assunto do email
	 * @var string
	 * @access protected
	 */
	protected $csSubject = '';

	/**
	 * Conte�do da mensagem. Pode ser HTML ou texto puro.
	 * @var string
	 * @access protected
	 */
	protected $csMessage = '';

	/**
	 * Lista de anexos j� no formato em que ser�o enviados
	 * @var array
	 * @access protected
	 */
	protected $caAttachments = array();

	/**
	 * Lista de anexos embutidos j� no formato em que ser�o enviados
	 * @var array
	 * @access protected
	 */
	protected $caEmbeddedAttachments = array();

	/**
	 * Indica se o corpo da mensagem est� em HTML
	 * @var boolean
	 * @access protected
	 */
	protected $cbHtml = true;

	/**
	 * Indica que o sistema usa email em servidor externo
	 * @var boolean
	 */
	protected $cbExternal = false;

	/**
	 * Indica o host de email, caso seja externo
	 * @var String
	 */
	protected $csHost = null;

	/**
	 * Indica a porta de smtp do servidor de email externo
	 * @var integer
	 */
	protected $ciPort = null;

	/**
	 * Nome de usu�rio do servidor externo
	 * @var String
	 */
	protected $csUser = null;

	/**
	 * senha do usu�rio do servidor externo
	 * @var String
	 */
	protected $csPassword = null;

	/**
	 * tipo de encripta��o usada para o email
	 * pode ser tls, ssl, sslv2 or sslv3
	 * @var String
	 */
	protected $csEncryption = null;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDEmail.</p>
	 * @access public
	 */
	public function __construct($external = false, $host = null, $port = null, $user = null, $password = null, $enc = null){
		if ($external){
			if ($host && $port && $user && $password){
				$this->cbExternal = $external;
				$this->csHost = $host;
				$this->ciPort = $port;
				$this->csUser = $user;
				$this->csPassword = $password;
				$this->csEncryption = $enc;
			}
			else {
				trigger_error("Mail options not configured", E_USER_ERROR);
			}
		}
	}

	/**
	 * Seta o remetente do email.
	 *
	 * <p>Seta o remetente do email.</p>
	 * @access public
	 * @param string $psFrom Remetente do email
	 */
	public function setFrom($psFrom){
		$this->csFrom = $psFrom;
	}

	/**
	 * Seta o nome do remetente do email.
	 *
	 * <p>Seta o remetente do remetente do email.</p>
	 * @access public
	 * @param string $psSenderName Nome do remetente do email
	 */
	public function setSenderName($psSenderName){
		$this->csSenderName = $psSenderName;
	}

	/**
	 * Seta o destinat�rio do email.
	 *
	 * <p>Seta o destinat�rio do email.</p>
	 * @access public
	 * @param string $psTo Destinat�rio do email.
	 */
	public function setTo($psTo){
		$this->csTo = $psTo;
	}

	/**
	 * Seta os destinat�rios que receber�o c�pia.
	 *
	 * <p>Seta os destinat�rios que receber�o c�pia.</p>
	 * @access public
	 * @param string $psCc Destinat�rios que receber�o c�pia
	 */
	public function setCc($psCc){
		$this->csCc = $psCc;
	}

	/**
	 * Seta os destinat�rios que receber�o c�pia oculta.
	 *
	 * <p>Seta os destinat�rios que receber�o c�pia oculta.</p>
	 * @access public
	 * @param string $psBcc Destinat�rios que receber�o c�pia oculta
	 */
	public function setBcc($psBcc){
		$this->csBcc = $psBcc;
	}

	/**
	 * Seta o endere�o para onde devem ser enviadas respostas para o email.
	 *
	 * <p>Seta o endere�o para onde devem ser enviadas respostas para o email.</p>
	 * @access public
	 * @param string $psReplyTo Endere�o para onde devem ser enviadas respostas para o email.
	 */
	public function setReplyTo($psReplyTo){
		$this->csReplyTo = $psReplyTo;
	}

	/**
	 * Seta o assunto do email.
	 *
	 * <p>Seta o assunto do email.</p>
	 * @access public
	 * @param string $psSubject Assunto do email
	 */
	public function setSubject($psSubject){
		$this->csSubject = $psSubject;
	}

	/**
	 * Seta o formato do email.
	 *
	 * <p>Seta o formato do email.</p>
	 * @access public
	 * @param boolean $pbHtml true = html, false = text
	 */
	public function setHtml($pbHtml){
		$this->cbHtml = $pbHtml;
	}

	/**
	 * Verifica se o formato do email � html.
	 *
	 * <p>Verifica se o formato do email � html.</p>
	 * @access public
	 * @return boolean true = html, false = text
	 */
	public function isHtml(){
		return $this->cbHtml;
	}

	/**
	 * Seta o conte�do do corpo do email.
	 *
	 * <p>Seta o conte�do do corpo do email. Se o email for HTML e contiver
	 * imagens, elas s�o anexadas.</p>
	 * @access public
	 * @param string $psMessage Conte�do do corpo do email
	 * @param boolean $psHtml Indica se o conte�do deve ser interpretado como HTML
	 */
	public function setMessage($psMessage){
		$this->caEmbeddedAttachments = array();
		if($this->cbHtml){
			$this->csMessage = '';
			$msGfxRef = FWDWebLib::getInstance()->getGfxRef();
			$msSearch = "/[ ]+(src|background)[ ]*=[ ]*(('([^']*)')|(\"([^\"]*)\"))/";
			preg_match_all($msSearch,$psMessage,$maMatches,PREG_OFFSET_CAPTURE|PREG_SET_ORDER);
			$i = 0;
			foreach($maMatches as $maMatch){
				if($maMatch[4][1]==-1){
					$msSrc = $maMatch[6][0];
				}else{
					$msSrc = $maMatch[4][0];
				}
				$miStart = $maMatch[0][1];
				$msFileName = substr($msSrc,strrpos($msSrc,'/')+1);
				$msExt = strtolower(substr($msFileName,strrpos($msFileName,'.')+1));
				$msId = "$msFileName@0123.456";
				$msContentType = ($msExt=='jpg'?'image/jpeg':"image/$msExt");
				$this->addAttachment($msSrc,$msContentType,$msId,true);
				$this->csMessage.= substr($psMessage,$i,$miStart-$i)." ".$maMatch[1][0]."='cid:".$msId."'";
				$i = $miStart+strlen($maMatch[0][0]);
			}
			$this->csMessage.= substr($psMessage,$i);
		}else{
			$this->csMessage = $psMessage;
		}
	}

	/**
	 * Seta o conte�do do corpo do email a partir de uma View.
	 *
	 * <p>Seta o conte�do do corpo do email a partir de uma View. Se a view ou seus
	 * componentes possu�rem imagens, elas s�o anexadas.</p>
	 * @access public
	 * @param FWDView $poView View que representa o conte�do do corpo do email
	 */
	public function setMessageView(FWDView $poView){
		$msMessage = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">"
		."<html><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8'><title></title></head>"
		."<body topmargin=0 leftmargin=0>{$poView->draw()}</body></html>";
		$this->setMessage($msMessage);
	}

	/**
	 * Anexa um arquivo ao email.
	 *
	 * <p>Anexa um arquivo ao email.</p>
	 * @access public
	 * @param string $psFilePath Caminho do arquivo
	 * @param string $psContentType Tipo do conte�do do arquivo (Ex: 'image/jpeg')
	 * @param string $psContentId Identificador do conte�do (s� � necess�rio para embutidos)
	 * @param boolean $pbEmbedded Indica se o anexo � embutido (falso por default)
	 * @return boolean True indica sucesso ao anexar o arquivo
	 */
	public function addAttachment($psFilePath,$psContentType,$psContentId='',$pbEmbedded=false){
		if(!file_exists($psFilePath) || !is_readable($psFilePath) || !is_file($psFilePath)){
			trigger_error("Could not read file '$psFilePath'.",E_USER_WARNING);
			return false;
		}elseif($pbEmbedded && isset($this->caEmbeddedAttachments[$psContentId])){ // j� foi adicionado
			return true;
		}else{
			$psFileName = substr(strrchr($psFilePath,'/'),1);
			$msContent = $this->encode(file_get_contents($psFilePath));
			$msAttachmentCode = "Content-Type: $psContentType; name=\"$psFileName\"\n"
			."Content-Transfer-Encoding: base64\n"
			.($psContentId!=''?"Content-ID: <$psContentId>\n":'')
			."\n$msContent";
			if($pbEmbedded){
				$this->caEmbeddedAttachments[$psContentId] = $msAttachmentCode;
				unset($this->caAttachments[$psContentId]);
			}else{
				$this->caAttachments[$psContentId] = $msAttachmentCode;
				unset($this->caEmbeddedAttachments[$psContentId]);
			}
			return true;
		}
	}

	/**
	 * Retorna o cabe�alho do email.
	 *
	 * <p>Retorna o cabe�alho do email.</p>
	 * @access public
	 * @param string $psBoundary Separador do 1� n�vel do email
	 * @return string Header do email
	 */
	protected function drawHeader($psBoundary){
		$msSenderName = $this->csSenderName ? $this->csSenderName : 'noreply';
		$msHeader = "MIME-Version: 1.0\n"
		."Content-Type: multipart/mixed; boundary=\"$psBoundary\"\n"
		."X-Mailer: PHP/".phpversion()."\n"
		."To: {$this->csTo} <{$this->csTo}>\n"
		."From: {$msSenderName} <{$this->csFrom}>\n";
		if($this->csReplyTo!='') $msHeader.= "ReplyTo: {$this->csReplyTo}\n";
		if($this->csCc!='') $msHeader.= "Cc: {$this->csCc}\n";
		if($this->csBcc!='') $msHeader.= "Bcc: {$this->csBcc}\n";
		$msHeader .= "\n";
		return $msHeader;
	}

	/**
	 * Retorna o trecho do email que cont�m os anexos.
	 *
	 * <p>Retorna o trecho do email que cont�m os anexos.</p>
	 * @access public
	 * @param string $psBoundary Separador a ser usado entre os anexos
	 * @param boolean $pbEmbedded Indica se � para retornar o c�digo dos anexos embutidos ou dos anexos normais
	 * @return string Trecho do email que cont�m os anexos
	 */
	protected function drawAttachments($psBoundary,$pbEmbedded){
		$msOutput = '';
		if($pbEmbedded){
			$maAttachments = &$this->caEmbeddedAttachments;
		}else{
			$maAttachments = &$this->caAttachments;
		}
		foreach($maAttachments as $msAttachmentCode){
			$msOutput.= "--$psBoundary\n$msAttachmentCode\n\n";
		}
		return $msOutput;
	}

	/**
	 * Codifica uma string em base64 e quebra em linhas de tamanho fixo.
	 *
	 * <p>Codifica uma string em base64 e quebra em linhas de tamanho fixo.</p>
	 * @access public
	 * @param string $psString String a ser codificada
	 * @param integer $piLineLength Tamanho em que as linhas devem ser quebradas
	 * @return string String codificada
	 */
	protected function encode($psString,$piLineLength=76){
		return chunk_split(base64_encode($psString),$piLineLength,"\n");
	}

	/**
	 * Envia o email.
	 *
	 * <p>Envia o email.</p>
	 * @access public
	 * @return boolean True or False.
	 */
	public function send(){
		$msBoundary = md5(uniqid(time()));
		$msLv1Boundary = "_1_$msBoundary";
		$msLv2Boundary = "_2_$msBoundary";
		$msLv3Boundary = "_3_$msBoundary";
		$msHeader = $this->drawHeader($msLv1Boundary);
		if($this->cbHtml){
			$msHTMLMessage = $this->csMessage;
			$msTextMessage = strip_tags($this->csMessage);
			$msMessage = "This is a multipart message in MIME format.\n\n"
			."--$msLv1Boundary\n"
			."Content-Type: multipart/related; boundary=\"$msLv2Boundary\"\n\n"
			."--$msLv2Boundary\n"
			."Content-Type: multipart/alternative; boundary=\"$msLv3Boundary\"\n\n"
			."--$msLv3Boundary\n"
			."Content-Type: text/plain; charset=\"utf-8\"\n"
			."Content-Transfer-Encoding: base64\n"
			."\n".$this->encode($msTextMessage)."\n\n"
			."--$msLv3Boundary\n"
			."Content-Type: text/html; charset=\"utf-8\"\n"
			."Content-Transfer-Encoding: base64\n"
			."\n".$this->encode($msHTMLMessage)."\n\n"
			."--$msLv3Boundary--\n\n"
			.$this->drawAttachments($msLv2Boundary,true)
			."--$msLv2Boundary--\n\n"
			.$this->drawAttachments($msLv1Boundary,false)
			."--$msLv1Boundary--\n\n";
		}else{
			$msTextMessage = $this->csMessage;
			$msMessage = "This is a multipart message in MIME format.\n\n"
			."--$msLv1Boundary\n"
			."Content-Type: text/plain; charset=\"utf-8\"\n"
			."Content-Transfer-Encoding: base64\n"
			."\n".$this->encode($msTextMessage)."\n\n"
			."--$msLv1Boundary--\n\n";
		}

		if(strtoupper(substr(php_uname('s'),0,3))==='WIN'){
			if ($this->cbExternal){
				return $this->sendSmtp($msMessage);
			} else {
				ini_set('sendmail_from',$this->csFrom);
				return mail($this->csTo,$this->csSubject,$msMessage,$msHeader);
			}
		}else{
			if ($this->cbExternal){
				return $this->sendSmtp($msMessage);
			} else {
				return mail($this->csTo,$this->csSubject,$msMessage,$msHeader," -f {$this->csFrom}");
			}
		}
	}

	function sendSmtp($msMessage){
		$c = SMTP::connect($this->csHost, (int) $this->ciPort, $this->csUser, $this->csPassword, $this->csEncryption, 10);
		if (!$c) trigger_error("Error connection to mail server", E_USER_ERROR);
		$s = SMTP::Send($c, array($this->csTo), $msMessage, $this->csFrom);
		if (!$s) {
			$error = $_RESULT;
			trigger_error($error, E_USER_ERROR);
		}
		SMTP::Disconnect($c);
		return $s;
	}

}

?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDConfig. Transforma a estrutura XML de conex�o (config.php) em objeto PHP.
 *
 * <p>Classe que transforma Transforma a estrutura XML de conex�o (config.php) em objeto PHP.</p>
 * @package FWD5
 * @subpackage base
 */
class FWDConfig {
	protected $crParser;
	protected $csErrorMsg;

	protected $csFile="";
	protected $ciLevel=0;
	protected $cbGetData=false;
	protected $csBase="";
	protected $csVar="";

	protected $caFields = array();
	protected $ciDebugType=FWD_DEBUG_NONE;

	/**
	 * Construtor.
	 *
	 * <p>Construtor. Recebe o caminho para o arquivo de configura��o.</p>
	 * @access public
	 * @param string psFile Caminho para o arquivo de configura��o do banco de dados.
	 */
	public function __construct($psFile) {
		$this->csFile = $psFile;
	}

	/**
	 * M�todo para configurar o xml parser.
	 *
	 * <p>M�todo para configurar o xml parser. Seta as fun��es
	 * que ser�o executadas na abertura e fechamento de tags
	 * e a fun��o que ser� usada para coletar os dados.</p>
	 * @access protected
	 */
	protected function setupParser() {
		$this->crParser = xml_parser_create('ISO-8859-1');
		xml_set_object($this->crParser, $this);
		xml_set_element_handler($this->crParser,'startHandler','endHandler');
		xml_set_character_data_handler($this->crParser,'dataHandler');
	}

	/**
	 * M�todo para liberar o xml parser.
	 *
	 * <p>M�todo para liberar o xml parser.</p>
	 * @access protected
	 */
	protected function freeParser(){
		xml_parser_free($this->crParser);
	}

	/**
	 * M�todo para parsear uma string de conte�do xml.
	 *
	 * <p>M�todo para parsear uma string de conte�do xml.</p>
	 * @access public
	 * @param string psData String com conte�do xml.
	 * @return True se conseguiu parsear; False caso contr�rio.
	 */
	public function parseString($psData){
		$this->setupParser(); //Define os m�todos que parseiam a string.
		if (!$psData)
		return false;
		$mbParse = xml_parse($this->crParser,$psData);
		if(!$mbParse){
			$this->csErrorMsg = sprintf(
        "XML error: %s at line %d",
			xml_error_string(xml_get_error_code($this->crParser)),
			xml_get_current_line_number($this->crParser)
			);
			$this->freeParser();
			return false;
		}
		$this->freeParser();
		return true;
	}

	/**
	 * Retorna o erro gerado durante parseamento, se houver algum.
	 *
	 * <p>Retorna o erro gerado durante parseamento, se houver algum.</p>
	 * @access public
	 * @return string Mensagem contendo o erro de parseamento.
	 */
	public function getErrorMsg() {
		return $this->csErrorMsg;
	}

	/**
	 * M�todo para realizar a conex�o com o banco de dados.
	 *
	 * <p>M�todo para realizar a conex�o com o banco de dados.
	 * O arquivo indicado no construtor � lido, parseado, e a
	 * conex�o � feita.</p>
	 * @access public
	 */
	public function load() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD1,__FILE__,__LINE__);

		$msConfig = file_get_contents($this->csFile); //Pega o conte�do do arquivo passado no construtor.
		$this->parseString($msConfig); //Parseia o conte�do.

		$maRequiredTags = array('DB_MSSQL'=>array('databaseType','databaseDatabase','databaseSchema','databasePassword','databaseHost'),
                            'DB_POSTGRES'=>array('databaseType','databaseDatabase','databaseSchema','databasePassword','databaseHost','databasePort'),
                            'DB_ORACLE'=>array('databaseType','databaseService','databaseSchema','databasePassword')
		);
		if (!isset($this->caFields['databaseType']) || !defined($this->caFields['databaseType'])) {
			return false;
		}
		foreach($maRequiredTags[$this->caFields['databaseType']] as $msField) {
			if (!isset($this->caFields[$msField])) {
				return false;
			}
		}

		//Decripta a senha, quando estiver compilado.
		if ($GLOBALS['load_method']==XML_STATIC) {
			$moModule = mcrypt_module_open(MCRYPT_BLOWFISH,'',MCRYPT_MODE_ECB,'');
			$msKey = "b306737041cb40b1492e0debcfe45433";
			mcrypt_generic_init($moModule, $msKey, "12345678");
			$msPassword = mdecrypt_generic($moModule, base64_decode($this->caFields['databasePassword']));
			$msPadding = substr($msPassword,-1);
			$maPaddings = array(chr(0),chr(1),chr(2),chr(3),chr(4),chr(5),chr(6),chr(7),chr(8));
			if (in_array($msPadding,$maPaddings)) $msPassword = rtrim($msPassword,$msPadding);
			$this->caFields['databasePassword'] = $msPassword;
		} else {
			$msPassword = $this->caFields['databasePassword'];
		}

		switch($this->caFields['databaseType']) {
			case 'DB_MSSQL':
				$msDatabase = $this->caFields['databaseDatabase'];
				$msHost = $this->caFields['databaseHost'];
				FWDWebLib::setConnection(new FWDDB(constant($this->caFields['databaseType']),$msDatabase,$this->caFields['databaseSchema'],$msPassword,$msHost));
				break;
			case 'DB_POSTGRES':
				$msDatabase = $this->caFields['databaseDatabase'];
				$msHost = $this->caFields['databaseHost'];
				$msPort = $this->caFields['databasePort'];
				FWDWebLib::setConnection(new FWDDB(constant($this->caFields['databaseType']),$msDatabase,$this->caFields['databaseSchema'],$msPassword,$msHost.($msPort?":".$msPort:"")));
				break;
			case 'DB_ORACLE':
				$msDatabase = $this->caFields['databaseService'];
				$msHost = "";
				FWDWebLib::setConnection(new FWDDB(constant($this->caFields['databaseType']),$msDatabase,$this->caFields['databaseSchema'],$msPassword,$msHost));
				break;
			default:
				trigger_error('Database type is invalid: '.$this->caFields['databaseType'],E_USER_ERROR);
				break;
		}

		//Seta debug
		if (isset($this->ciDebugType) && $this->ciDebugType != 0) {
			FWDWebLib::setDebugMode(true,$this->ciDebugType);
		} else {
			FWDWebLib::setDebugMode(false,FWD_DEBUG_NONE);
		}

		//seta o path do arquivo de debug
		if (isset($this->caFields['debugFile'])){
			FWDWebLib::setDebugFilePath($this->caFields['debugFile']);
		}
		return true;
	}

	/**
	 * Retorna o endere�o do servidor RPC.
	 *
	 * <p>Retorna o endere�o do servidor RPC, se houver. (AMS)</p>
	 * @access public
	 * @return string Endere�o do servidor RPC.
	 */
	public function getRPCServerAddress() {
		if (isset($this->caFields['xmlrpcHost']) && isset($this->caFields['xmlrpcPort']))
		return "http://{$this->caFields['xmlrpcHost']}:{$this->caFields['xmlrpcPort']}/";
		else
		return "";
	}

	/**
	 * M�todo executado durante parseamento na abertura de tags.
	 *
	 * <p>M�todo executado durante parseamento na abertura de tags.</p>
	 * @access protected
	 */
	protected function startHandler($prParser, $psTagName, $paAttributes){
		$this->cbGetData = false; //N�o pegar os dados entre tags
		if ($this->ciLevel == 1) {
			$this->csBase = strtolower($psTagName);
		}
		if ($this->ciLevel == 2) {
			$this->csVar = ucfirst(strtolower($psTagName));
			$this->cbGetData=true; //Pegar os dados entre tags de nivel 2
		}
		$this->ciLevel++;
	}

	/**
	 * M�todo executado durante parseamento para pegar os dados entre tags.
	 *
	 * <p>M�todo executado durante parseamento para pegar os dados entre tags.</p>
	 * @access protected
	 */
	protected function dataHandler($prParser, $psData){
		if ($this->cbGetData && $this->ciLevel==3) { //Pegar os dados quando as tags forem de nivel 2. Verifica-se o nivel 3, pois foi incrementado antes de entrar neste m�todo.
			if ($this->csBase=='debug') { //Se a tag for 'debug', n�o colocar no array de tags. Setar o debugType.
				if (defined($psData)) {
					$this->ciDebugType = $this->ciDebugType | constant($psData); //Concatena os modos de debug.
				} else {
					if($this->csVar = 'File'){
						$this->caFields[$this->csBase.$this->csVar]=$psData;
					}else{
						trigger_error('Debug type is invalid: '.$psData,E_USER_ERROR);
					}
				}
			} else {
				$this->caFields[$this->csBase.$this->csVar]=$psData;
			}
		}
	}

	/**
	 * M�todo executado durante parseamento no fechamento de tags.
	 *
	 * <p>M�todo executado durante parseamento no fechamento de tags.</p>
	 * @access protected
	 */
	protected function endHandler($prParser, $psTagName){
		$this->ciLevel--; //Quando fecha uma tag, volta um n�vel.
	}

	/**
	 * Retorna os campos extra�dos do XML.
	 *
	 * <p>Retorna os campos extra�dos do XML.</p>
	 * @access public
	 * @return array Array associativo com os valores dos campos indexados por seus nomes
	 */
	public function getFields(){
		return $this->caFields;
	}

}
?><?php
/**
 * Classe FWDSMTPConnectionTest. Classe que teste a conec��o do protocolo SMTP.
 *
 * <p>Classe utilizada para testar se a conec��o com o servidor de email esta funcionando
 * PHP.</p>
 * @package FWD5
 * @subpackage base
 */

class FWDSMTPConnectionTest
{

	/**
	 * nome da maquina servidora de emails
	 * @var string
	 * @access protected
	 */
	protected $csHostName="";

	/**
	 * porta do servi�o de email
	 * @var integer
	 * @access protected
	 */
	protected $ciHostPort=25;

	/**
	 * mensagem de erro retornado quando a conec��o com o servidor for mal sucedida
	 * @var string
	 * @access protected
	 */
	protected $csError="";

	/**
	 * tempo de espera de conec��o com o servidor de email
	 * @var integer
	 * @access protected
	 */
	protected $ciTimeout=0;

	/**
	 * status da conec��o
	 * @var string
	 * @access private
	 */
	private $csState="Disconnected";

	/**
	 * handle da cone��o com o servidor de email
	 * @var integer
	 * @access private
	 */
	private $ciConnection=0;

	/**
	 * seta o nome (ip) do servidor de smpt.
	 *
	 * <p>seta o nome (ip) do servidor de smpt.</p>
	 * @access public
	 * @param integer nome do servidor de smtp
	 */
	public function setHostName($psHostName){
		$this->csHostName = $psHostName;
	}

	/**
	 * seta a porta do host do servidor de smpt.
	 *
	 * <p>seta a porta do host do servidor de smpt.</p>
	 * @access public
	 * @param integer porta do host do servidor de email
	 */
	public function setHostPort($piHostPort){
		$this->ciHostPort = $piHostPort;
	}

	/**
	 * retorna os erros ocorridos na tentativa de connec��o
	 *
	 * <p>retorna os erros ocorridos na tentativa de connec��o ao servidor de smtp.</p>
	 * @access public
	 * @return string contento os erros ocorridos na tentativa de conec��o ao servidor de smtp
	 */
	public function getError(){
		return $this->csError;
	}

	/**
	 * seta o timeout da tentativa de conec��o ao servidor de smtp
	 *
	 * <p>seta o timeout da tentativa de conec��o ao servidor de smtp.</p>
	 * @access public
	 * @param integer timeout de conec��ok
	 */
	public function setTimeout($piTimeout){
		$this->ciTimeout = $piTimeout;
	}

	/**
	 * escreve as mensagens do teste de smtp no debug do sistema
	 *
	 * <p>escreve as mensagens do teste de smtp no debug do sistema.</p>
	 * @access protected
	 * @param string $psMessage string que deve ser escrita no debug do sistema
	 */
	protected function outputDebug($psMessage)
	{
		FWDWebLib::getInstance()->writeStringDebug($psMessage);
	}

	/**
	 * executa a conec��o com o servidor de smtp
	 *
	 * <p>executa a conec��o com o servidor de smtp.</p>
	 * @access protected
	 * @param string $psDomain nome (ip) do servidor de email
	 * @param string $piPort porta do servi�o smtp
	 * @param string $psResolveMessage string q informa a mensagem que vai ser escrita no debug
	 * @return string $msMessage retorna mensagem de erro caso tenha acontecido algum erro
	 */
	protected function connectToHost($psDomain, $piPort,$psResolveMessage)
	{
		$this->outputDebug($psResolveMessage);
		$msIP=gethostbyname($psDomain);
		if($msIP!=$psDomain){
			$msMessage = "Could not resolve host '%domain_name%'";
			$msMessage = str_replace('%domain_name%', $psDomain,$msMessage);
			return($msMessage);
		}
		$msErrno="";
		$this->outputDebug("Connecting to host address '".$msIP."' port ".$piPort."...");

		FWDWebLib::setErrorHandler(new FWDDoNothingErrorHandler());

		if($this->ciConnection=( $this->ciTimeout ? fsockopen($msIP,$piPort,$msErrno,$msError,$this->ciTimeout)
		: fsockopen($msIP,$piPort)
		)
		){
			FWDWebLib::restoreLastErrorHandler();
			return("");
		}
		FWDWebLib::restoreLastErrorHandler();

		$msError =($this->ciTimeout ? strval($msError) : "??");
		$msMessage ="";
		switch($msError)
		{
			case "-3":
				$msMessage = "-3 socket could not be created";
				break;
			case "-4":
				$msMessage = "-4 dns lookup on hostname '%domain_name%' failed";
				$msMessage = str_replace("%domain_name%",$this->csHostName,$msMessage);
				break;
			case "-5":
				$msMassage = "-5 Connection refused or timed out";
				break;
			case "-6":
				$msMassage = "-6 fdopen() call failed";
				break;
			case "-7":
				$msMassage = "-7 setvbuf() call failed";
				break;
			default:
				$msMessage = "Could not connect to the host '%domain_name%' ";
				$msMessage = str_replace("%domain_name%",$this->csHostName,$msMessage);
				$msMessage .= $msError;
				break;
		}
		return $msMessage;
	}

	/**
	 * conecta ao servidor smtp indicado nas variaveis do objeto
	 *
	 * <p>conecta ao servidor smtp indicado nas variaveis do objeto utilizando os parametros previamente setados no objeto.</p>
	 * @access public
	 * @return boolean 1 se a cone��o com o servidor de email funcionou, 0 caso contr�rio
	 */
	public function connect()
	{
		if(strcmp($this->csState,"Disconnected"))
		{
			$this->csError="ciConnection is already established";
			return(0);
		}
		$this->csError="";
		$msError="";



		if(!$this->csHostName)
		{
			$this->csError="Could not determine the SMTP to connect";
			return(0);
		}
		$msMessage = "Resolving SMTP server domain '%domain_name%'...";
		$msMassage = str_replace("%domain_name%",$this->csHostName,$msMessage);
		$msError = $this->connectToHost($this->csHostName, $this->ciHostPort, $msMassage);

		if(strlen($msError))
		{
			$this->csError=$msError;
			$this->ciConnection=0;
			return(0);
		}else{
			return (1);
		}

	}
};

?><?php
/**
 * Classe FWDSyslogConnectionTest. Classe que teste a conec��o do protocolo syslog.
 *
 * <p>Classe utilizada para testar se a conec��o com o servidor de email esta funcionando
 * PHP.</p>
 * @package FWD5
 * @subpackage base
 */

class FWDSYSLogConnectionTest
{

	/**
	 * nome da maquina servidora de syslog
	 * @var string
	 * @access protected
	 */
	protected $csHostName="";

	/**
	 * porta do servi�o de syslog
	 * @var integer
	 * @access protected
	 */
	protected $ciHostPort=514;

	/**
	 * mensagem de erro retornado quando a conec��o com o servidor for mal sucedida
	 * @var string
	 * @access protected
	 */
	protected $csError="";

	/**
	 * tempo de espera de conec��o com o servidor de syslog
	 * @var integer
	 * @access protected
	 */
	protected $ciTimeout=0;

	/**
	 * status da conec��o
	 * @var string
	 * @access private
	 */
	private $csState="Disconnected";

	/**
	 * handle da cone��o com o servidor de syslog
	 * @var integer
	 * @access private
	 */
	private $ciConnection=0;

	/**
	 * seta o nome (ip) do servidor de syslog.
	 *
	 * <p>seta o nome (ip) do servidor de syslog.</p>
	 * @access public
	 * @param integer nome do servidor de syslog
	 */
	public function setHostName($psHostName){
		$this->csHostName = $psHostName;
	}

	/**
	 * seta a porta do host do servidor de syslog.
	 *
	 * <p>seta a porta do host do servidor de smpt.</p>
	 * @access public
	 * @param integer porta do host do servidor de email
	 */
	public function setHostPort($piHostPort){
		$this->ciHostPort = $piHostPort;
	}

	/**
	 * retorna os erros ocorridos na tentativa de connec��o
	 *
	 * <p>retorna os erros ocorridos na tentativa de connec��o ao servidor de syslog.</p>
	 * @access public
	 * @return string contento os erros ocorridos na tentativa de conec��o ao servidor de syslog
	 */
	public function getError(){
		return $this->csError;
	}

	/**
	 * seta o timeout da tentativa de conec��o ao servidor de syslog
	 *
	 * <p>seta o timeout da tentativa de conec��o ao servidor de syslog.</p>
	 * @access public
	 * @param integer timeout de conec��o
	 */
	public function setTimeout($piTimeout){
		$this->ciTimeout = $piTimeout;
	}

	/**
	 * escreve as mensagens do teste de syslog no debug do sistema
	 *
	 * <p>escreve as mensagens do teste de syslog no debug do sistema.</p>
	 * @access protected
	 * @param string $psMessage string que deve ser escrita no debug do sistema
	 */
	protected function outputDebug($psMessage)
	{
		FWDWebLib::getInstance()->writeStringDebug($psMessage);
	}

	/**
	 * executa a conec��o com o servidor de syslog
	 *
	 * <p>executa a conec��o com o servidor de syslog.</p>
	 * @access protected
	 * @param string $psDomain nome (ip) do servidor de email
	 * @param string $piPort porta do servi�o syslog
	 * @param string $psResolveMessage string q informa a mensagem que vai ser escrita no debug
	 * @return string $msMessage retorna mensagem de erro caso tenha acontecido algum erro
	 */
	protected function connectToHost($psDomain, $piPort,$psResolveMessage)
	{
		$this->outputDebug($psResolveMessage);
		$msIP=gethostbyname($psDomain);
		if($msIP!=$psDomain){
			$msMessage = "Could not resolve host '%domain_name%'";
			$msMessage = str_replace('%domain_name%', $psDomain,$msMessage);
			return($msMessage);
		}
		$msErrno="";
		$this->outputDebug("Connecting to host address '".$msIP."' port ".$piPort."...");

		FWDWebLib::setErrorHandler(new FWDDoNothingErrorHandler());

		if($this->ciConnection=( $this->ciTimeout ? fsockopen("udp://".$msIP,$piPort,$msErrno,$msError,$this->ciTimeout)
		: fsockopen("udp://".$msIP,$piPort)
		)
		){
			FWDWebLib::restoreLastErrorHandler();
			return("");
		}
		FWDWebLib::restoreLastErrorHandler();

		$msError =($this->ciTimeout ? strval($msError) : "??");
		$msMessage ="";
		switch($msError)
		{
			case "-3":
				$msMessage = "-3 socket could not be created";
				break;
			case "-4":
				$msMessage = "-4 dns lookup on hostname '%domain_name%' failed";
				$msMessage = str_replace("%domain_name%",$this->csHostName,$msMessage);
				break;
			case "-5":
				$msMassage = "-5 Connection refused or timed out";
				break;
			case "-6":
				$msMassage = "-6 fdopen() call failed";
				break;
			case "-7":
				$msMassage = "-7 setvbuf() call failed";
				break;
			default:
				$msMessage = "Could not connect to the host '%domain_name%' ";
				$msMessage = str_replace("%domain_name%",$this->csHostName,$msMessage);
				$msMessage .= $msError;
				break;
		}
		return $msMessage;
	}

	/**
	 * conecta ao servidor syslog indicado nas variaveis do objeto
	 *
	 * <p>conecta ao servidor syslog indicado nas variaveis do objeto utilizando os parametros previamente setados no objeto.</p>
	 * @access public
	 * @return boolean 1 se a cone��o com o servidor de email funcionou, 0 caso contr�rio
	 */
	public function connect()
	{
		if(strcmp($this->csState,"Disconnected"))
		{
			$this->csError="ciConnection is already established";
			return(0);
		}
		$this->csError="";
		$msError="";

		if(!$this->csHostName)
		{
			$this->csError="Could not determine the syslog to connect";
			return(0);
		}
		$msMessage = "Resolving syslog server domain '%domain_name%'...";
		$msMassage = str_replace("%domain_name%",$this->csHostName,$msMessage);
		$msError = $this->connectToHost($this->csHostName, $this->ciHostPort, $msMassage);

		if(strlen($msError))
		{
			$this->csError=$msError;
			$this->ciConnection=0;
			return(0);
		}else{
			$this->outputDebug("Connection to server established sucessfully!");
			return (1);
		}

	}
};

?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Interface FWDPostValue. Interface para objetos que recebem valor via POST.
 *
 * <p>Interface para objetos que recebem valor via POST. Esta interface foi
 * criada para verificar os objetos que devem ser setados no XML_LOAD. Para
 * que isto ocorra, o objeto deve ter implementado os m�todos desta interface.</p>
 * @package FWD5
 * @subpackage interface
 */
interface FWDPostValue {

	/**
	 * Atribui valor � vari�vel value.
	 *
	 * <p>Atribui valor � vari�vel value, devendo retirar espa�os em branco
	 * no inicio e/ou no fim da string.</p>
	 * @access public
	 * @param string $psValue Valor a ser atribu�do
	 */
	public function setAttrValue($psValue);

	/**
	 * Atribui valor, condicionalmente, � vari�vel value.
	 *
	 * <p>Atribui valor � vari�vel value, condicionando atrav�s do segundo par�metro.
	 * Se pbForce for FALSE, n�o atribui o valor se a vari�vel tiver conte�do;
	 * se TRUE atribui mesmo que tenha conte�do.</p>
	 * @access public
	 * @param string $psValue Valor a ser atribu�do
	 * @param boolean $pbForce For�ar o valor mesmo que j� tenha conte�do
	 */
	public function setValue($psValue, $pbForce = true);

	/**
	 * Atribui nome ao objeto.
	 *
	 * @access public
	 * @param string $psName Nome do objeto
	 */
	public function setAttrName($psName);

	/**
	 * Retorna o valor da vari�vel value.
	 *
	 * @access public
	 * @return string Valor do objeto
	 */
	public function getValue();

	/**
	 * Retorna o nome do objeto.
	 *
	 * @access public
	 * @return string Nome do objeto
	 */
	public function getAttrName();
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

define("DB_NONE", "");
define("DB_POSTGRES", "pgsql");
define("DB_ORACLE", "oci8");
define("DB_MSSQL", "mssql");
define("DB_MYSQL", "mysql");

/**
 * Interface FWDIDB.
 *
 * <p>Interface para abstra��o do banco de dados.</p>
 * @package FWD5
 * @subpackage interface
 */
interface FWDIDB {

	/**
	 * Contrutor.
	 *
	 * <p>Construtor da interface de acesso ao banco de dados.</p>
	 * @access public
	 * @param string $psType Identificador do BD {DB_NONE | DB_POSTGRES | DB_ORACLE | DB_MSSQL}
	 * @param string $psDatabase Nome da base de dados
	 * @param string $psUser Nome do usu�rio
	 * @param string $psPassword Senha do usu�rio
	 * @param string $psHost Nome do host
	 */
	public function __construct($psType, $psDatabase, $psUser, $psPassword, $psHost);

	/**
	 * Clona o objeto de conex�o com o banco de dados.
	 *
	 * <p>M�todo para clonar o objeto de conex�o com o banco de dados.</p>
	 * @access public
	 * @return FWDDB Objeto de conex�o clonado
	 */
	public function __clone();

	/**
	 * Conecta no banco.
	 *
	 * <p>M�todo para conectar no banco.</p>
	 * @access public
	 * @return boolean Verdadeiro ou Falso
	 */
	public function connect();

	/**
	 * Verifica se est� conectado ao banco.
	 *
	 * <p>M�todo para verificar se est� conectado no banco.</p>
	 * @access public
	 * @return boolean Verdadeiro ou Falso
	 */
	public function isConnected();

	/**
	 * Fecha conex�o com o banco de dados.
	 *
	 * <p>M�todo para fechar conex�o com o banco de dados.</p>
	 * @access public
	 */
	public function disconnect();

	/**
	 * Executa um sql no banco.
	 *
	 * <p>M�todo para executar um sql no banco.</p>
	 * @access public
	 * @param string $psQuery Query que vai ser executada
	 * @param string $piLimit N�mero de registros que deve ser retornado
	 * @param string $psOffset A partir de qual posi��o deve come�ar a retornar registros
	 * @return boolean Verdadeiro (sucesso) ou Falso (falha)
	 */
	public function execute($psQuery, $piLimit = 0, $piOffset = 0);

	/**
	 * Gera SQL para INSERT automaticamente.
	 *
	 * <p>M�todo para gerar o SQL para INSERT automaticamente.</p>
	 * @access public
	 * @param string $psTable Nome da tabela
	 * @param array $paFieldValues Array com os valores dos campos (Ex: array['zName'] = "Fulano")
	 * @return boolean Verdadeiro ou Falso
	 */
	public function autoInsert($psTable, $paFieldValues);

	/**
	 * Gera SQL para UPDATE automaticamente.
	 *
	 * <p>M�todo para gerar o SQL para UPDATE automaticamente.</p>
	 * @access public
	 * @param string $psTable Nome da tabela
	 * @param array $paFieldValues Array com os valores dos campos (Ex: array['zName'] = "Fulano")
	 * @param string $psWhere Cl�usula "where" para restringir o UPDATE
	 * @return boolean Verdadeiro ou Falso
	 */
	public function autoUpdate($psTable, $paFieldValues, $psWhere);

	/**
	 * Gera DELETE automaticamente.
	 *
	 * <p>M�todo para gerar o SQL DELETE automaticamente.</p>
	 * @access public
	 * @param string $psTable Nome da tabela
	 * @param string $psWhere Cl�usula "where" para restringir o UPDATE
	 * @return boolean Verdadeiro ou Falso
	 */
	public function autoDelete($psTable, $psWhere);

	/**
	 * Retorna um registro do resultado.
	 *
	 * <p>M�todo para retornar um registro do resultado.</p>
	 * @access public
	 * @return array Array contendo a informa��o ou falso caso atinja o fim
	 */
	public function fetch();

	/**
	 * Retorna um array com todos os registros retornados pelo consulta.
	 *
	 * <p>M�todo para retornar um array com todos os registros retornados pelo consulta.</p>
	 * @access public
	 * @return array Array contendo todos os registros
	 */
	public function fetchAll();

	/**
	 * Retorna o n�mero de registros retornado em um SELECT.
	 *
	 * <p>M�todo para retornar o n�mero de registros retornado em um SELECT.</p>
	 * @access public
	 * @return integer N�mero de registros
	 */
	public function getRowCount();

	/**
	 * Retorna o n�mero de registros afetados por DELETE/UPDATE.
	 *
	 * <p>M�todo para retornar o n�mero de registros afetados por DELETE/UPDATE.</p>
	 * @access public
	 * @return integer N�mero de registros
	 */
	public function getAffectedRows();

	/**
	 * Retorna o id do �ltimo registro inserido.
	 *
	 * <p>M�todo para retornar o id do �ltimo registro inserido.</p>
	 * @access public
	 * @return integer Id do �ltimo registro inserido
	 */
	public function getLastId();

	/**
	 * Retorna o n�mero de campos retornados por uma consulta.
	 *
	 * <p>M�todo para retornar o n�mero de campos retornados por uma consulta.</p>
	 * @access public
	 * @return integer N�mero de campos
	 */
	public function getFieldCount();

	/**
	 * Seta o modo de debug.
	 *
	 * <p>M�todo para setar o m�todo de debug.</p>
	 * @access public
	 * @param boolean $pbDebug Verdadeiro ou Falso
	 */
	public function debugMode($pbDebug);

	/**
	 * Retorna a �ltima mensagem de erro.
	 *
	 * <p>M�todo para retornar a �ltima mensagem de erro.</p>
	 * @access public
	 * @return string Mensagem de erro
	 */
	public function getErrorMessage();

	/**
	 * Libera da mem�ria o resultado da consulta.
	 *
	 * <p>M�todo para liberar da mem�ria o resultado da consulta.</p>
	 * @access public
	 */
	public function freeResult();

	/**
	 * Retorna a data no formato do banco.
	 *
	 * <p>M�todo para retornar a data no formato aceito pelo banco.</p>
	 * @access public
	 * @param integer $piTimestamp Timestamp
	 * @return string Data
	 */
	public function timestampFormat($piTimestamp);

	/**
	 * Retorna um timestamp baseado em uma data no formato YYYY-MM-DD HH-NN-SS.
	 *
	 * <p>M�todo para retornar um timestamp baseado em uma data no formato YYYY-MM-DD HH-NN-SS.</p>
	 * @access public
	 * @param integer $psDate Data
	 * @return integer Timestamp
	 */
	public function getTimestamp($psDate);

	/**
	 * Retorna o tipo do banco de dados
	 *
	 * <p>Retorna o tipo do banco de dados.</p>
	 * @access public
	 * @return string Tipo do banco de dados
	 */
	public function getDatabaseType();

	/**
	 * Retorna o nome da base de dados.
	 *
	 * <p>M�todo para retornar o nome da base de dados.</p>
	 * @access public
	 * @return string Nome da base de dados
	 */
	public function getDatabaseName();

	/**
	 * Retorna o host da base de dados.
	 *
	 * <p>M�todo para retornar o host da base de dados.</p>
	 * @access public
	 * @return string Host da base de dados
	 */
	public function getHost();

	/**
	 * Retorna o usu�rio que deve ser utilizado como prefixo das functions do banco
	 *
	 * <p>Usu�rio que deve ser utilizado como prefixo das functions do banco</p>
	 * @access public
	 * @return string Usu�rio da conex�o com o banco que deve ser utilizado antes do nome das functions
	 */
	public function getFunctionUserDB();

	/**
	 * Retorna a chamada da fun��o de acordo com o banco de dados.
	 *
	 * <p>M�todo para retornar a chamada da fun��o de acordo com o banco de dados.</p>
	 * @access public
	 * @param string $psFunction Fun��o que deve ser chamada
	 * @return string Chamada da fun��o
	 */
	public function getFunctionCall($psFunction);

	/**
	 * Retorna um valor constante do banco de dados.
	 *
	 * <p>M�todo para retornar um valor constante do banco de dados. Necess�rio
	 * pois no Oracle n�o s�o aceitas consultas no formato 'select 1' e sim
	 * no formato 'select 1 from dual'.</p>
	 * @access public
	 * @param string $psValue Valores [Ex: '1 as first, 2 as second, ...]
	 * @return string Select para retornar uma constante
	 */
	public function selectConstant($psValues);

	/**
	 * Retorna o nome do usu�rio do banco.
	 *
	 * <p>M�todo para retornar o nome do usu�rio do banco.</p>
	 * @access public
	 * @return string Login do usu�rio do banco
	 */
	public function getUser();

	/**
	 * Retorna a string escapada de acordo com o banco.
	 *
	 * <p>M�todo para retornar a string escapada de acordo com o banco.</p>
	 * @access public
	 * @param string $psString String
	 * @return string String escapada
	 */
	public function quote($psString);

	/**
	 * Retorna as strings concatenadas.
	 *
	 * <p>M�todo para retornar as strings concatenadas.</p>
	 * @access public
	 * @param string $psString1 String
	 * @param string $psString2 String
	 * @return string Par�metros concatenados
	 */
	public function concat($psString1, $psString2);

	/**
	 * Inicia uma transa��o.
	 *
	 * <p>M�todo para iniciar uma transa��o.</p>
	 * @access public
	 */
	public function startTransaction();

	/**
	 * Completa uma transa��o.
	 *
	 * <p>M�todo para completar uma transa��o.</p>
	 * @access public
	 * @return Verdadeiro se obtiver sucesso, falso caso contr�rio
	 */
	public function completeTransaction();

	/**
	 * For�a a falha de uma transa��o.
	 *
	 * <p>M�todo para for�ar a falha em um transa��o.</p>
	 * @access public
	 */
	public function rollbackTransaction();
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Interface FWDCollectable. Interface para elementos que podem listar seus sub-elementos
 *
 * <p>Interface para elementos que possuem sub-elementos e t�m a habilidade
 * de list�-los recursivamente.</p>
 *
 * @package FWD5
 * @subpackage interface
 */
interface FWDCollectable {

	/**
	 * Retorna um array com todos sub-elementos do elemento
	 *
	 * <p>Retorna um array contendo todos elementos contidos no elemento ou em
	 * seus descendentes.</p>
	 * @access public
	 * @return array Array de sub-elementos
	 */
	public function collect();
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Interface FWDErrorHandler. Interface para manipuladores de erros e exce��es.
 *
 * <p>Interface para manipuladores de erros e exce��es.</p>
 *
 * @package FWD5
 * @subpackage interface
 */
interface FWDErrorHandler {

	/**
	 * M�todo que manipula erros.
	 *
	 * <p>M�todo que manipula erros.</p>
	 * @access public
	 * @param integer $piErrNo N�mero do erro
	 * @param string $psErrStr Mensagem de erro
	 * @param string $psErrFile Nome do arquivo que gerou o erro
	 * @param integer $piErrLine N�mero da linha em que ocorreu o erro
	 */
	public function handleError($piErrNo, $psErrStr, $psErrFile, $piErrLine);

	/**
	 * M�todo que manipula exce��es.
	 *
	 * <p>M�todo que manipula exce��es.</p>
	 * @access public
	 * @param FWDException $poFWDException Exce��o
	 * @static
	 */
	public static function handleException($poFWDException);

}

?>