<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe abstrata FWDDrawing.
 *
 * <p>Classe abstrata para objetos que possam ser desenhados em tela.</p>
 * @package FWD5
 * @subpackage abstracts
 */
abstract class FWDDrawing {

	/**
	 * Define se o elemento deve ser desenhado ou n�o
	 * @var boolean
	 * @access protected
	 */
	protected $cbShouldDraw = true;

	/**
	 * Seta o valor do atributo ShouldDraw.
	 *
	 * <p>Seta o valor do atributo ShouldDraw.</p>
	 * @access public
	 * @param boolean $pbShouldDraw Valor a ser atribu�do
	 */
	public function setShouldDraw($pbShouldDraw) {
		$this->cbShouldDraw = $pbShouldDraw;
	}

	/**
	 * Retorna o valor booleano do atributo ShouldDraw.
	 *
	 * <p>Retorna o valor booleano do atributo ShouldDraw.</p>
	 * @access public
	 * @return boolean Valor booleano do atributo ShouldDraw
	 */
	public function getShouldDraw() {
		return $this->cbShouldDraw;
	}

	/**
	 * Desenha o cabe�alho do objeto.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o cabe�alho do objeto.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do objeto
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	protected function drawHeader($poBox = null) {}

	/**
	 * Desenha o corpo do objeto.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o corpo do objeto.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do objeto
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	protected function drawBody($poBox = null) {}

	/**
	 * Desenha o rodap� do objeto.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o rodap� do objeto.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do objeto
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	protected function drawFooter($poBox = null) {}

	/**
	 * Desenha em tela.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML/javascript,
	 * todas as informa��es necess�rias para exibir em tela o objeto.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do objeto
	 * @return string C�digo HTML/javascript que ser� inserido na p�gina
	 */
	public function draw($poBox = null) {
		$msOut = "";
		if ($this->cbShouldDraw) {
			$msOut .= $this->drawHeader($poBox);
			$msOut .= $this->drawBody($poBox);
			$msOut .= $this->drawFooter($poBox);
		}
		return $msOut;
	}
}
?>
<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe abstrata FWDReportDrawing.
 *
 * <p>Classe abstrata para objetos que possam ser desenhados em relat�rios.</p>
 * @package FWD5
 * @subpackage abstracts
 */
abstract class FWDReportDrawing {

	/**
	 * Objeto que escreve o relat�rio
	 * @var FWDReportWriter
	 * @access protected
	 */
	protected $coWriter = null;

	/**
	 * Desenha o cabe�alho do objeto.
	 *
	 * <p>M�todo para desenhar o cabe�alho do objeto.</p>
	 * @access public
	 */
	protected function drawHeader() {}

	/**
	 * Desenha o corpo do objeto.
	 *
	 * <p>M�todo para desenhar o corpo do objeto.</p>
	 * @access public
	 */
	protected function drawBody() {}

	/**
	 * Desenha o rodap� do objeto.
	 *
	 * <p>M�todo para desenhar o rodap� do objeto.</p>
	 * @access public
	 */
	protected function drawFooter() {}

	/**
	 * Desenha o relat�rio.
	 *
	 * <p>M�todo para desenhar o relat�rio.</p>
	 * @access public
	 */
	public function draw() {
		$this->drawHeader();
		$this->drawBody();
		$this->drawFooter();
		$this->coWriter->generateReport();
	}
}
?>
<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe abstrata FWDView. Classe abstrata com os atributos b�sicos de um componente.
 *
 * <p>Classe abstrata que cont�m os atributos b�sicos de um componente.</p>
 * @abstract
 * @package FWD5
 * @subpackage abstracts
 */
abstract class FWDView extends FWDDrawing implements FWDCollectable {

	/**
	 * Alinhamento relativo ao pai
	 * @var string
	 * @access protected
	 */
	protected $csAlign = "";

	/**
	 * Nome do elemento
	 * @var string
	 * @access protected
	 */
	protected $csName = "";

	/**
	 * Css
	 * @var string
	 * @access protected
	 */
	protected $csClass = "";

	/**
	 * �ndice do elemento
	 * @var integer
	 * @access protected
	 */
	protected $ciTabIndex = 0;

	/**
	 * Atalho para acessar o elemento
	 * @var string
	 * @access protected
	 */
	protected $csAccessKey = "";

	/**
	 * Tag do elemento (permiss�es: define se o elemento deve ser desenhado ou n�o)
	 * @var string
	 * @access protected
	 */
	protected $csTag = "";

	/**
	 * Z-index para o elemento
	 * @var integer
	 * @access protected
	 */
	protected $ciZIndex = 1;

	/**
	 * Define se o elemento � redimension�vel
	 * @var boolean
	 * @access protected
	 */
	protected $cbResize = false;

	/**
	 * Define cor do contorno do elemento (utilizado para debug)
	 * @var string
	 * @access protected
	 */
	protected $csDebug = "";

	/**
	 * Define se o elemento deve ser exibido ou n�o
	 * @var boolean
	 * @access protected
	 */
	protected $cbDisplay = true;

	/**
	 * Define se o posicionamento do elemento � absoluto ou relativo
	 * @var string
	 * @access protected
	 */
	protected $csPosition = 'absolute';

	/**
	 * Objeto FWDBox que define o tamanho e a posi��o do elemento
	 * @var FWDBox
	 * @access protected
	 */
	protected $coBox = null;
		
	/**
	 * Valor do elemento
	 * @var FWDString
	 * @access protected
	 */
	protected $coString = null;

	/**
	 * Array de FWDEvent do elemento
	 * @var array
	 * @access protected
	 */
	protected $caEvent = array();

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDView.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites da �rea do elemento
	 */
	public function __construct($poBox = null) {
		$this->coBox = new FWDBox();
		$this->coString = new FWDString();
		$this->setAttrClass(get_class($this));
		if ($poBox) {
			$this->coBox->setAttrLeft($poBox->getAttrLeft());
			$this->coBox->setAttrTop($poBox->getAttrTop());
			$this->coBox->setAttrHeight($poBox->getAttrHeight());
			$this->coBox->setAttrWidth($poBox->getAttrWidth());
		}
	}

	/**
	 * Atribui valor, condicionalmente.
	 *
	 * <p>Atribui valor � view, condicionando atrav�s do segundo par�metro.
	 * Se force for FALSE, concatena o valor se a vari�vel tiver conte�do;
	 * se TRUE atribui mesmo que tenha conte�do (sobrescreve).</p>
	 * @access public
	 * @param string $psValue Valor a ser atribu�do
	 * @param boolean $pbForce For�ar o valor mesmo que j� tenha conte�do
	 */
	public function setValue($psValue, $pbForce = true) {
		$this->coString->setValue($psValue, $pbForce);
	}

	/**
	 * Seta o nome do elemento.
	 *
	 * <p>M�todo para setar o nome do elemento.</p>
	 * @access public
	 * @param string $psName Nome do elemento
	 */
	final public function setAttrName($psName) {
		$this->csName = $psName;
	}

	/**
	 * Seta a classe de CSS.
	 *
	 * <p>M�todo para setar a classe de CSS.</p>
	 * @access public
	 * @param string $psClass Classe
	 */
	final public function setAttrClass($psClass) {
		$this->csClass = $psClass;
	}

	/**
	 * Seta o alinhamento do elemento.
	 *
	 * <p>M�todo para setar o alinhamento do elemento.</p>
	 * @access public
	 * @param string $psAlign Alinhamento do elemento
	 */
	final public function setAttrAlign($psAlign) {
		$this->csAlign = $psAlign;
	}

	/**
	 * Seta o modo de debug do elemento.
	 *
	 * <p>M�todo para setar o modo de debug do elemento.</p>
	 * @access public
	 * @param string $psDebug Seta o modo de debug
	 */
	final public function setAttrDebug($psDebug) {
		$this->csDebug = (($psDebug=="false")?"":$psDebug);
	}

	/**
	 * Seta o atalho para acessar do elemento.
	 *
	 * <p>M�todo para setar o atalho para acessar do elemento.</p>
	 * @access public
	 * @param string $psAccessKey Seta o atalho do elemento
	 */
	final public function setAttrAccessKey($psAccessKey) {
		$this->csAccessKey = $psAccessKey;
	}

	/**
	 * Seta a tag do elemento.
	 *
	 * <p>M�todo para setar a tag do elemento.</p>
	 * @access public
	 * @param string $psTag Tag do elemento
	 */
	final public function setAttrTag($psTag) {
		$this->csTag = $psTag;
	}

	/**
	 * Seta o ordenamento do elemento.
	 *
	 * <p>M�todo para setar o ordenamento do elemento.</p>
	 * @access public
	 * @param integer $piTabIndex Seta o ordenamento do elemento
	 */
	final public function setAttrTabIndex($piTabIndex) {
		if (is_numeric($piTabIndex) && $piTabIndex >= 0)
		$this->ciTabIndex = $piTabIndex;
		else
		trigger_error("Invalid index value: '{$piTabIndex}'",E_USER_WARNING);
	}

	/**
	 * Seta o valor do elemento.
	 *
	 * <p>M�todo para setar o valor do elemento.</p>
	 * @access public
	 * @param FWDString $poString Seta o valor do elemento
	 */
	final public function setObjFWDString(FWDString $poString) {
		if (isset($this->coString)) {
			$this->coString->setValue($poString->getAttrString());
			$this->coString->setAttrId($poString->getAttrId());
		}
		else {
			$this->coString = $poString;
		}
	}

	/**
	 * Seta a �rea do elemento.
	 *
	 * <p>M�todo para setar a �rea do elemento.</p>
	 * @access public
	 * @param FWDBox $poBox Seta a �rea do elemento
	 */
	final public function setObjFWDBox(FWDBox $poBox) {
		$this->coBox = $poBox;
	}

	/**
	 * Seta um evento para o elemento.
	 *
	 * <p>M�todo para setar um evento para o elemento.</p>
	 * @access public
	 * @param FWDEvent $poEvent Evento
	 */
	final public function addObjFWDEvent(FWDEvent $poEvent) {
		$this->addEvent($poEvent);
	}


	/**
	 * Seta um evento para o elemento.
	 *
	 * <p>M�todo para setar um evento para o elemento.</p>
	 * @access public
	 * @param FWDEvent $poEvent Evento
	 */
	final public function setAttrEvent(FWDEvent $poEvent) {
		$this->caEvent[] = $poEvent;
	}


	/**
	 * Seta um evento para o elemento.
	 *
	 * <p>M�todo para setar um evento para o elemento.</p>
	 * @access public
	 * @param FWDEvent $poEvent Evento
	 */
	final public function addEvent(FWDEvent $poEvent) {
		$mskey = strtolower($poEvent->getAttrEvent());
		$msArrayKey = explode(":",$mskey);
		foreach($msArrayKey as $msKey){
			if(!isset($this->caEvent[$msKey]))
			$this->caEvent[$msKey] = new FWDEventHandler($msKey,$this->getAttrName());
			$this->caEvent[$msKey]->setAttrContent($poEvent);
		}
	}

	/**
	 * Seta o valor booleano do atributo de display.
	 *
	 * <p>Seta o valor booleano do atributo que define se o elemento
	 * deve ser exibido. Para o elemento ser exibido, o atributo deve
	 * conter o valor booleano TRUE. Para n�o ser exibido, o atributo deve
	 * conter o valor booleano FALSE.</p>
	 * @access public
	 * @param string $psDisplay Define se o elemento deve ser exibido ou n�o
	 */
	public function setAttrDisplay($psDisplay) {
		$this->cbDisplay = FWDWebLib::attributeParser($psDisplay,array("true"=>true, "false"=>false),"psDisplay");
	}

	/**
	 * Retorna o valor booleano do atributo de display.
	 *
	 * <p>Retorna o valor booleano TRUE se o elemento deve ser exibido.
	 * Retorna FALSE caso contr�rio.</p>
	 * @access public
	 * @return boolean Indica se o elemento deve ser exibido ou n�o
	 */
	final public function getAttrDisplay() {
		return $this->cbDisplay;
	}

	/**
	 * Seta o posicionamento do elemento
	 *
	 * <p>Seta o valor do atributo que define se o posicionamento do elemento �
	 * relativo ou absoluto.</p>
	 *
	 * @access public
	 * @param string $psPosition Define o posicionamento do elemento
	 */
	final public function setAttrPosition($psPosition){
		$this->csPosition = $psPosition;
	}

	/**
	 * Retorna o valor do atributo de position.
	 *
	 * <p>Retorna o valor do atributo position.</p>
	 *
	 * @access public
	 * @return string Valor do atributo position
	 */
	final public function getAttrPosition(){
		return $this->csPosition;
	}

	/**
	 * Seta o valor booleano do atributo de resize.
	 *
	 * <p>Seta o valor booleano do atributo que define se o elemento
	 * pode ser redimensionado. Para o elemento poder ser redimensionado, o atributo deve
	 * conter o valor booleano TRUE. Caso contr�rio, o atributo deve
	 * conter o valor booleano FALSE.</p>
	 * @access public
	 * @param string $psResize Define se o elemento pode ser redimensionado ou n�o
	 */
	final public function setAttrResize($psResize) {
		$this->cbResize = FWDWebLib::attributeParser($psResize,array("true"=>true, "false"=>false),"psResize");
	}

	/**
	 * Retorna o valor booleano do atributo de resize.
	 *
	 * <p>Retorna o valor booleano TRUE se o elemento pode ser redimensionado.
	 * Retorna FALSE caso contr�rio.</p>
	 * @access public
	 * @return boolean Indica se o elemento pode ser redimensionado ou n�o
	 */
	final public function getAttrResize() {
		return $this->cbResize;
	}

	/**
	 * Retorna o nome do elemento.
	 *
	 * <p>M�todo para retornar o nome do elemento.</p>
	 * @access public
	 * @return string Nome do elemento
	 */
	final public function getAttrName() {
		return $this->csName;
	}

	/**
	 * Retorna a classe de CSS.
	 *
	 * <p>M�todo para retornar a classe de CSS.</p>
	 * @access public
	 * @return string Classe de CSS
	 */
	final public function getAttrClass() {
		return $this->csClass;
	}

	/**
	 * Retorna os eventos do elemento.
	 *
	 * <p>M�todo para retornar os eventos do elemento.</p>
	 * @access public
	 * @return array Array de FWDEvent
	 */
	final public function getObjFWDEvent() {
		return $this->caEvent;
	}

	/**
	 * Retorna a Box do elemento.
	 *
	 * <p>M�todo para retornar a Box do elemento.</p>
	 * @access public
	 * @return FWDBox �rea do elemento
	 */
	final public function getObjFWDBox() {
		return $this->coBox;
	}

	/**
	 * Retorna o objeto de string do elemento.
	 *
	 * <p>M�todo para retornar o objeto de string do elemento.</p>
	 * @access public
	 * @return FWDString String do elemento
	 */
	final public function getObjFWDString() {
		return $this->coString;
	}

	/**
	 * Retorna o valor do elemento.
	 *
	 * <p>M�todo para retornar o valor do elemento.</p>
	 * @access public
	 * @return string Valor do elemento
	 */
	public function getValue() {
		return $this->coString->getAttrString();
	}

	/**
	 * Retorna o valor do debug do elemento.
	 *
	 * <p>M�todo para retornar o valor do debug do elemento.</p>
	 * @access public
	 * @return string Valor do debug do elemento
	 */
	final public function getAttrDebug() {
		return $this->csDebug;
	}

	/**
	 * Retorna o atalho para acessar o elemento.
	 *
	 * <p>M�todo para retornar o atalho para acessar o elemento.</p>
	 * @access public
	 * @return string Atalho do elemento
	 */
	final public function getAttrAccessKey() {
		return $this->csAccessKey;
	}

	/**
	 * Retorna a tag do elemento.
	 *
	 * <p>M�todo para retornar a tag do elemento.</p>
	 * @access public
	 * @return string Tag do elemento
	 */
	final public function getAttrTag() {
		return $this->csTag;
	}

	/**
	 * Retorna o ordenamento do elemento.
	 *
	 * <p>M�todo para retornar o ordenamento do elemento.</p>
	 * @access public
	 * @return integer Ordenamento do elemento
	 */
	final public function getAttrTabIndex() {
		return $this->ciTabIndex;
	}

	/**
	 * Retorna o alinhamento do elemento.
	 *
	 * <p>M�todo para retornar o alinhamento do elemento.</p>
	 *
	 * @access public
	 * @return string Alinhamento do elemento
	 */
	final public function getAttrAlign() {
		return $this->csAlign;
	}

	/**
	 * Alinha o elemeto.
	 *
	 * <p>M�todo para alinhar o elemento.</p>
	 * @access public
	 * @param string $psValue Tipo de alinhamento
	 * @param FWDBox $poBox Box
	 */
	final public function alignView($psValue, $poBox) {
		switch ($psValue) {
			case "left":
				$miX = 0;
				$miY = 0;
				$miHeight = $poBox->getAttrHeight() - 2;
				$miWidth = $this->coBox->getAttrWidth();
				break;
			case "right":
				$miX = $poBox->getAttrWidth() - $this->coBox->getAttrWidth();
				$miY = 0;
				$miHeight = $poBox->getAttrHeight() - 2;
				$miWidth = $this->coBox->getAttrWidth() - 2;
				break;
			case "top":
				$miX = 0;
				$miY = 0;
				$miHeight = $this->coBox->getAttrHeight();
				$miWidth = $poBox->getAttrWidth() - 2;
				break;
			case "bottom":
				$miX = 0;
				$miY = $poBox->getAttrHeight() - $this->coBox->getAttrHeight();
				$miHeight = $this->coBox->getAttrHeight() - 2;
				$miWidth = $poBox->getAttrWidth() - 2;
				break;
			case "parent":
				$miX = 0;
				$miY = 0;
				$miHeight = $poBox->getAttrHeight() - 2;
				$miWidth = $poBox->getAttrWidth() - 2;
				break;
			default:
				return;
		}
		$this->coBox->setAttrLeft($miX);
		$this->coBox->setAttrTop($miY);
		$this->coBox->setAttrHeight($miHeight);
		$this->coBox->setAttrWidth($miWidth);
	}

	/**
	 * Retorna as propriedades globais do elemento.
	 *
	 * <p>M�todo para retornar as propriedades globais do elemento.</p>
	 * @access public
	 * @return string Propriedades globais do elemento
	 */
	public function getGlobalProperty() {
		$msTabIndex = (($this->ciTabIndex)?"tabindex='{$this->ciTabIndex}'":"");
		$msAccessKey = (($this->csAccessKey)?"accesskey='{$this->csAccessKey}'":"");
		$msResize = (($this->cbResize)?"resize='true'":"");
		$msClass = (($this->csClass)?"class='{$this->csClass}'":"");
		$msId = $this->csName;

		$msValue = "name='{$msId}' id='{$msId}' {$msTabIndex} {$msAccessKey} {$msResize} {$msClass}";
		return $msValue;
	}

	/** Seta o valor do zIndex do elemento
	 *
	 * <p>Seta o valor do atributo zIndex do elemento</p>
	 * @access public
	 * @param integer $piZIndex Define o valor do zIndex do elemento
	 */
	final public function setAttrZIndex($piZIndex){
		$this->ciZIndex = $piZIndex;
	}

	/**
	 * Retorna o ZIndex do elemento.
	 *
	 * <p>M�todo para retornar o zIndex do elemento.</p>
	 * @access public
	 * @return Integer Propriedade zIndex do elemento
	 */
	final public function getAttrZIndex(){
		return $this->ciZIndex;
	}

	/**
	 * Retorna o estilo do elemento.
	 *
	 * <p>M�todo para retornar o estilo do elemento.</p>
	 * @access public
	 * @return string Estilo do elemento
	 */
	final public function getStyle(){
		$msStyle = '';
		$msStyle.= $this->coBox->draw();
		$msStyle.= "position:{$this->getAttrPosition()};z-index:{$this->getAttrZIndex()};";
		$msStyle.= $this->cbDisplay ? '' : 'display:none;';
		$msStyle.= $this->csDebug ? $this->debugView() : '';
		return $msStyle;
	}

	/**
	 * Retorna os eventos do elemento.
	 *
	 * <p>M�todo para retornar os eventos do elemento.</p>
	 * @access public
	 * @return string Eventos
	 */
	final public function getEvents() {
		$msEvents = "";
		foreach ($this->getObjFWDEvent() as $moEvent) {
			$msEvents .= $moEvent->render();
		}
		return $msEvents;
	}

	/**
	 * Retorna o array de eventos do elemento e o pr�prio elemento.
	 *
	 * <p>M�todo para retornar o array de eventos do elemento e o pr�prio elemento.</p>
	 * @access public
	 * @return array Eventos do elemento e elemento
	 */
	public function collect() {
		$maOut = array($this);
		if ($this->caEvent)
		foreach ($this->caEvent as $event)
		$maOut = array_merge($maOut,$event->getAttrContent());
		return $maOut;
	}

	/**
	 * Retorna o estilo para o modo de debug.
	 *
	 * <p>M�todo para retornar o estilo para o modo de debug.</p>
	 * @access private
	 * @return string Estilo do debug
	 */
	private function debugView() {
		$moRGBTable = FWDWebLib::getObject("goRGBTable");

		$msDebug = $this->getAttrDebug();
		$msColor = ((in_array($msDebug,$moRGBTable)||($msDebug[0]=="#"&&strlen($msDebug)==7))?$msDebug:"red");

		return "border-color:{$msColor};border-style:dashed;border-width:1px;";
	}

	/**
	 * Retorna uma representa��o da view como string para debug.
	 *
	 * <p>Retorna uma representa��o da view como string para debug.</p>
	 * @access public
	 * @return string representa��o da view como string
	 */
	public function __toString(){
		return "[".get_class($this)." ({$this->csName}): '{$this->getValue()}']";
	}

}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe abstrata FWDContainer.
 *
 * <p>Classe abstrata que possui um conjunto de FWDView.</p>
 * @package FWD5
 * @subpackage abstracts
 */
abstract class FWDContainer extends FWDView implements FWDCollectable {

	/**
	 * Array com as views que est�o dentro do container
	 * @var array
	 * @access protected
	 */
	protected $caContent = array();


	/**
	 * Adiciona um Drawing.
	 *
	 * <p>Adiciona um Drawing no container.</p>
	 * @access public
	 * @param FWDDrawing $poDrawing View
	 */
	public function addObjFWDDrawing(FWDDrawing $poDrawing){
		$this->caContent[] = $poDrawing;
	}

	/**
	 * Adiciona uma view.
	 *
	 * <p>Adiciona uma view no container.</p>
	 * @access public
	 * @param FWDView $poView View
	 * @param string $psTarget Alvo do evento
	 */
	public function addObjFWDView($poView,$psTarget = ""){
		if(isset($psTarget) && $psTarget=="movedor"){
			$moEvent = new FWDClientEvent('OnMouseDown','movedor',$this->getAttrName());
			$poView->addEvent($moEvent);
			$moEvent = new FWDClientEvent('OnMouseOver','changecursor',$poView->getAttrName(),'pointer');
			$poView->addEvent($moEvent);
		}
		$this->caContent[] = $poView;
	}

	/**
	 * Retorna todas as views do container.
	 *
	 * <p>M�todo para retornar todas as views do container.</p>
	 * @access public
	 * @return array Views
	 */
	public function getContent(){
		return $this->caContent;
	}

	/**
	 * Retorna um array com todos sub-elementos do container
	 *
	 * <p>Retorna um array contendo todos elementos contidos no container ou em
	 * seus descendentes.
	 * OBS: O array pode conter elementos repetidos, caso haja algum ItemController
	 * que perten�a ao Container e a um Controller simultaneamente.
	 * </p>
	 * @access public
	 * @return array Array de sub-elementos
	 */
	public function collect() {
		$maOut = array();
		$maOut = array_merge($maOut,parent::collect());
		foreach ($this->caContent as $moView) {
			if ($moView instanceof FWDCollectable) {
				$maItems = $moView->collect();
				if (count($maItems)>0)
				$maOut = array_merge($maOut,$maItems);
			}
			else {
				$maOut[] = $moView;
			}
		}
		return $maOut;
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe abstrata FWDItemController. Implementa um Controlador de Itens.
 *
 * <p>Classe abstrata que implementa um Controlador de Itens (radioboxes, checkboxes).</p>
 * @package FWD5
 * @subpackage abstracts
 */
abstract class FWDItemController extends FWDView {

	/**
	 * Define o n�mero de items que fazem parte do agrupamento
	 * @var integer
	 * @access private
	 */
	private $ciItems = 0;

	/**
	 * Define se o Item est� marcado ou n�o
	 * @var boolean
	 * @access protected
	 */
	protected $cbCheck = false;

	/**
	 * Valor chave do Item
	 * @var string
	 * @access protected
	 */
	protected $csKey = "";

	/**
	 * Nome do Controlador de Itens
	 * @var string
	 * @access protected
	 */
	protected $csController = "";

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDItemController.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do Controlador de Itens
	 */
	public function __construct($poBox = null){
		parent::__construct($poBox);
	}

	/**
	 * Seta o valor chave do Item.
	 *
	 * <p>Seta o valor chave do Item.</p>
	 * @access public
	 * @param string $psValue Valor chave do Item
	 */
	public function setAttrKey($psValue){
		$this->csKey = $psValue;
	}

	/**
	 * Seta o valor booleano do atributo Check.
	 *
	 * <p>Seta o valor booleano do atributo Check.</p>
	 * @access public
	 * @param string $psCheck Define se o item est� marcado ou n�o
	 */
	public function setAttrCheck($psCheck){
		$this->cbCheck = FWDWebLib::attributeParser($psCheck,array("true"=>true, "false"=>false),"psCheck");
	}

	/**
	 * Seta o valor do Controlador.
	 *
	 * <p>Seta o valor do Controlador.</p>
	 * @access public
	 * @param string $psController Valor do Controlador
	 */
	public function setAttrController($psController){
		$this->csController = $psController;
	}

	/**
	 * Seta o n�mero de items que fazem parte do agrupamento.
	 *
	 * <p>M�todo para setar o n�mero de itens que fazem parte do agrupamento.</p>
	 * @access public
	 * @param integer $psItems N�mero de itens
	 */
	public function setItems($psItems){
		$this->ciItems = $psItems;
	}

	/**
	 * Retorna o valor chave do item.
	 *
	 * <p>Retorna o valor chave do item.</p>
	 * @access public
	 * @return string Valor chave do item
	 */
	public function getAttrKey(){
		return $this->csKey;
	}

	/**
	 * Retorna o n�mero de items que fazem parte do agrupamento.
	 *
	 * <p>M�todo para retornar o n�mero de items que fazem parte do agrupamento.</p>
	 * @access public
	 * @return integer N�mero de itens.
	 */
	public function getItems(){
		return $this->ciItems;
	}

	/**
	 * Retorna o valor do Controlador de Itens.
	 *
	 * <p>Retorna o valor do Controlador de Itens.</p>
	 * @access public
	 * @return string Valor do Controlador de Itens.
	 */
	public function getAttrController(){
		return $this->csController;
	}

	/**
	 * Retorna o valor booleano do atributo Check.
	 *
	 * <p>Retorna o valor booleano do atributo Check.</p>
	 * @access public
	 * @return boolean Indica se o item est� marcado ou n�o
	 */
	public function getAttrCheck(){
		return $this->cbCheck;
	}

	public function execute() {
		if(($moController = FWDWebLib::getObject($this->csController)))
		$moController->addObjFWDItemController($this);
		else
		trigger_error("Invalid controller name: '{$this->csController}'",E_USER_WARNING);
	}
}
?><?
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDSession. Controla sess�o e timeout.
 *
 * <p>Classe que controla sess�o e timeout.</p>
 *
 * @package FWD5
 * @subpackage abstracts
 */
abstract class FWDSession{

	/**
	 * Id do usu�rio logado
	 * @var string
	 * @access protected
	 */
	protected $csUserId = '';

	/**
	 * Endere�o do usu�rio logado
	 * @var string
	 * @access protected
	 */
	protected $csRemoteAddress = '';

	/**
	 * Browser do usu�rio logado
	 * @var string
	 * @access protected
	 */
	protected $csUserAgent = '';

	/**
	 * Segundos para expirar a sess�o. Zero significa que n�o expira.
	 * @var integer
	 * @access protected
	 */
	protected $ciTimeout = 0;

	/**
	 * Tempo inicial, no formato de timestamp do Unix, para checar timeout.
	 * @var integer
	 * @access protected
	 */
	protected $ciStartTimestamp;

	/**
	 * Indica se o usu�rio est� logado
	 * @var boolean
	 * @access protected
	 */
	protected $cbLoggedIn = false;

	/**
	 * URL da p�gina de login
	 * @var string
	 * @access protected
	 */
	protected $csLoginUrl = '';

	/**
	 * Id da sess�o
	 * @var string
	 * @access protected
	 */
	protected $csId = '';

	/**
	 * Array de atributos da sess�o
	 * @var array
	 * @access protected
	 */
	protected $caAttributes = array();

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDSession.</p>
	 * @access public
	 * @param string $psId Id da sess�o
	 */
	public function __construct($psId){
		$this->csId = $psId;
		$this->csRemoteAddress = $this->getRemoteAddress();
		$this->csUserAgent = $this->getUserAgent();
		$this->resetTimeout();
	}

	/**
	 * Seta o id do usu�rio
	 *
	 * <p>Seta o id do usu�rio.</p>
	 * @access public
	 * @param string $psUserId Id do usu�rio
	 */
	public function setUserId($psUserId){
		$this->csUserId = $psUserId;
	}

	/**
	 * Retorna o id do usu�rio
	 *
	 * <p>Retorna o id do usu�rio.</p>
	 * @access public
	 * @return string Id do usu�rio
	 */
	public function getUserId(){
		return $this->csUserId;
	}

	/**
	 * Reseta o tempo inicial para timeout
	 *
	 * <p>Reseta o tempo inicial para timeout. Deve ser chamado a cada refresh.</p>
	 * @access public
	 */
	public function resetTimeout(){
		$this->ciStartTimestamp = time();
	}

	/**
	 * M�todo para autenticar o usu�rio
	 *
	 * <p>M�todo para autenticar o usu�rio.</p>
	 * @access public
	 * @param string $psUser Login do usu�rio
	 * @param string $psPassword Senha do usu�rio
	 * @return boolean Indica se o usu�rio foi autenticado ou n�o
	 */
	abstract public function login($psUser,$psPassword = "");

	/**
	 * Destr�i a sess�o
	 *
	 * <p>Destr�i a sess�o</p>
	 * @access public
	 */
	public function destroySession(){
		$weblib_instance = FWDWebLib::getInstance();
		$weblib_instance->destroySession($this->getId());
	}

	/**
	 * Desloga o usu�rio.
	 *
	 * <p>Destr�i a sess�o e redireciona para a p�gina de login.</p>
	 * @access public
	 */
	public function logout(){
		$this->cbLoggedIn = false;
		$this->destroySession();
		if($this->csLoginUrl) header("Location: {$this->csLoginUrl}");
	}

	/**
	 * Checa se o usu�rio est� logado
	 *
	 * <p>Checa se o usu�rio est� logado.</p>
	 * @access public
	 * @return boolean Indica se o usu�rio est� logado
	 */
	public function isLoggedIn(){
		return ($this->cbLoggedIn && $this->checkSession());
	}

	/**
	 * Seta o atributo loggedIn
	 *
	 * <p>Seta o atributo loggedIn, que indica se o usu�rio est� logado.</p>
	 * @access public
	 * @param boolean $pbLoggedIn Indica se o usu�rio est� logado
	 */
	public function setLoggedIn($pbLoggedIn){
		$this->cbLoggedIn = $pbLoggedIn;
	}

	/**
	 * Verifica se a sess�o expirou
	 *
	 * <p>Verifica se a sess�o expirou.</p>
	 * @access public
	 * @return boolean True se a sess�o expirou, falso caso contr�rio
	 */
	public function timeout(){
		if($this->ciTimeout > 0){
			$miElapsedTime = time() - $this->ciStartTimestamp;
			return $miElapsedTime > $this->ciTimeout;
		}else{
			return false;
		}
	}

	/**
	 * Seta a URL da p�gina de login
	 *
	 * <p>Seta a URL da p�gina de login.</p>
	 * @access public
	 * @param string $psLoginUrl URL da p�gina de login
	 */
	public function setLoginUrl($psLoginUrl){
		$this->csLoginUrl = $psLoginUrl;
	}

	/**
	 * Seta o tempo para expirar a sess�o
	 *
	 * <p>Seta o tempo para expirar a sess�o.</p>
	 * @access public
	 * @param integer $piTimeout Tempo para expirar a sess�o em segundos
	 */
	public function setTimeout($piTimeout){
		$this->ciTimeout = $piTimeout;
	}

	/**
	 * Retorna o id da sess�o
	 *
	 * <p>Retorna o id da sess�o.</p>
	 * @access public
	 * @return string Id da sess�o
	 */
	public function getId(){
		return $this->csId;
	}

	/**
	 * Retorna o endere�o remoto atual
	 *
	 * <p>Retorna o endere�o remoto atual.</p>
	 * @access public
	 * @return string Endere�o remoto atual
	 */
	public function getRemoteAddress(){
		$msIp = '';
		if(getenv("HTTP_X_FORWARDED_FOR")) $msIp.= getenv( "HTTP_X_FORWARDED_FOR")." ";
		if(getenv( "HTTP_CLIENT_IP"))      $msIp.= getenv( "HTTP_CLIENT_IP")." ";
		if(getenv( "REMOTE_ADDR"))         $msIp.= getenv( "REMOTE_ADDR");
		return $msIp;
	}

	/**
	 * Retorna o browser atual
	 *
	 * <p>Retorna o browser atual.</p>
	 * @access protected
	 * @return string Browser atual
	 */
	protected function getUserAgent(){
		$msUserAgent = getenv("HTTP_USER_AGENT");
		if($msUserAgent) return $msUserAgent;
		else return '';
	}

	/**
	 * Verifica se o usu�rio atual � o mesmo do momento da cria��o do objeto
	 *
	 * <p>Verifica se o usu�rio atual � o mesmo do momento da cria��o do objeto.</p>
	 * @access public
	 * @return boolean Indica se o usu�rio � o mesmo
	 */
	public function verifyUser(){
		if($this->csRemoteAddress==$this->getRemoteAddress() && $this->csUserAgent==$this->getUserAgent()){
			return true;
		}else{
			return false;
		}
	}

	/**
	 * Checa se a sess�o est� ok
	 *
	 * <p>Checa se a sess�o est� ok, ou seja, se ela n�o expirou e o usu�rio
	 * continua o mesmo.</p>
	 * @access public
	 */
	public function checkSession(){
		return ($this->verifyUser() && !$this->timeout());
	}

	/**
	 * Adiciona um atributo � sess�o
	 *
	 * <p>Adiciona um atributo � sess�o. Os nomes dos atributos adicionados dessa
	 * maneira s�o case insensitive.</p>
	 * @access public
	 * @param string $psName Nome do atributo
	 */
	public function addAttribute($psName){
		$psName = strtolower($psName);
		if(isset($this->caAttributes[$psName])){
			trigger_error("Cannot add session attribute. Name '$psName' already in use.",E_USER_WARNING);
		}else{
			$this->caAttributes[$psName] = '';
		}
	}

	/**
	 * Verifica se um atributo existe.
	 *
	 * <p>M�todo para verificar se um atributo existe.</p>
	 * @access public
	 * @param string $psName Nome do atributo
	 * @return boolean Verdairo ou falso
	 */
	public function attributeExists($psName) {
		$psName=strtolower($psName);
		if(isset($this->caAttributes[$psName]))
		return true;
		else
		return false;
	}

	/**
	 * Limpa um atributo da sess�o.
	 *
	 * <p>M�todo para limpar um atributo da sess�o.</p>
	 * @access public
	 * @param string $psAttribute Nome do atributo
	 */
	public function cleanAttribute($psAttribute) {
		$psAttribute = strtolower($psAttribute);
		$this->caAttributes[$psAttribute] = "";
	}

	/**
	 * Apaga um atributo da sess�o.
	 *
	 * <p>M�todo para apagar um atributo da sess�o.</p>
	 * @access public
	 * @param string $psAttribute Nome do atributo
	 */
	public function deleteAttribute($psAttribute) {
		$psAttribute = strtolower($psAttribute);
		unset($this->caAttributes[$psAttribute]);
	}

	/**
	 * Atualiza a sess�o fechando ela e reabrindo.
	 *
	 * <p>M�todo para for�ar a atualiza��o dos dados da sess�o.</p>
	 * @access public
	 */
	public function commit(){
		session_write_close();
		session_start();
	}

	/**
	 * M�todo que sobrecarrega m�todos setAttr e getAttr
	 *
	 * <p>M�todo de sobrecarga de m�todos. Esse m�todo � chamado pelo PHP quando
	 * um m�todo n�o definido � chamado. Usado para que atributos adicionados com
	 * o m�todo addAttribute() possam ser acessados por m�todos setAttr<atributo>()
	 * e getAttr<atributo>().</p>
	 * @access public
	 * @param string $psMethodName Nome do m�todo
	 * @param array paArguments Array com os par�metros passados para o m�todo
	 */
	public function __call($psMethodName,$paArguments){
		$msPrefix = substr($psMethodName,0,7);
		$msSuffix = strtolower(substr($psMethodName,7));
		if($msPrefix=='setAttr' || $msPrefix=='getAttr'){
			if(isset($this->caAttributes[$msSuffix])){
				if($msPrefix=='setAttr'){
					$this->caAttributes[$msSuffix] = $paArguments[0];
				}else{
					return $this->caAttributes[$msSuffix];
				}
			}else{
				trigger_error("Trying to ".substr($psMethodName,0,3)." undefined session attribute '$msSuffix'.",E_USER_WARNING);
			}
		}else{
			trigger_error("Calling undefined method '$psMethodName'.",E_USER_WARNING);
		}
	}

	/**
	 * M�todo para 'serializar' a classe de sess�o
	 *
	 * <p>Este m�todo � necess�rio para suportar um bug no PHP 5.0.x que n�o serializa
	 * automaticamente os objetos que est�o sendo armazenados na sess�o. Est� fun��o retorna
	 * para o PHP os atributos que ele deve considerar na serializa��o de um objeto desta classe</p>
	 * @access public
	 * @return array Nome dos atributos a serem considerados na serializa��o
	 */
	public function __sleep() {
		$maAttributes = (array)$this;
		return array_keys($maAttributes);
	}

	/**
	 * M�todo para 'deserializar' a classe de sess�o
	 *
	 * <p>Este m�todo � necess�rio para suportar um bug no PHP 5.0.x. Se esta fun��o
	 * n�o est� declarada o m�todo __call retona erro, pois o PHP tenta executar um
	 * m�todo com o nome __wakeup e ele n�o existe.</p>
	 * @access public
	 */
	public function __wakeup() {}

}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDStatic. Implementa texto est�tico.
 *
 * <p>Classe que implementa texto est�tico (html tag 'span').</p>
 * @package FWD5
 * @subpackage abstract
 */
abstract class FWDViewGrid extends FWDDrawing  implements FWDCollectable{

	/**
	 * Define o nome do static
	 * @var string
	 * @access protected
	 */
	protected $csName = '';

	/**
	 * Css
	 * @var string
	 * @access protected
	 */
	protected $csClass = "";

	/**
	 * Objeto FWDBox que define o tamanho e a posi��o do elemento
	 * @var FWDBox
	 * @access protected
	 */
	protected $coBox = null;

	/**
	 * Array de FWDEvent do elemento
	 * @var array
	 * @access protected
	 */
	protected $caEvent = array();

	/**
	 * Define se o elemento deve ser exibido ou n�o
	 * @var boolean
	 * @access protected
	 */
	protected $cbDisplay = true;

	/**
	 * Tag do elemento (permiss�es: define se o elemento deve ser desenhado ou n�o)
	 * @var string
	 * @access protected
	 */
	protected $csTag = '';

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDView.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites da �rea do elemento
	 */
	public function __construct($poBox = null) {
		$this->coBox = new FWDBox();
		$this->csClass = get_class($this);
		if ($poBox) {
			$this->coBox->setAttrLeft($poBox->getAttrLeft());
			$this->coBox->setAttrTop($poBox->getAttrTop());
			$this->coBox->setAttrHeight($poBox->getAttrHeight());
			$this->coBox->setAttrWidth($poBox->getAttrWidth());
		}
	}

	/**
	 * Seta o nome do elemento.
	 *
	 * <p>M�todo para setar o nome do elemento.</p>
	 * @access public
	 * @param string $psName Nome do elemento
	 */
	final public function setAttrName($psName) {
		$this->csName = $psName;
	}

	/**
	 * Seta a classe de CSS.
	 *
	 * <p>M�todo para setar a classe de CSS.</p>
	 * @access public
	 * @param string $psClass Classe
	 */
	final public function setAttrClass($psClass) {
		$this->csClass = $psClass;
	}

	/**
	 * Seta a �rea do elemento.
	 *
	 * <p>M�todo para setar a �rea do elemento.</p>
	 * @access public
	 * @param FWDBox $poBox Seta a �rea do elemento
	 */
	final public function setObjFWDBox(FWDBox $poBox) {
		$this->coBox = $poBox;
	}

	/**
	 * Retorna o nome do elemento.
	 *
	 * <p>M�todo para retornar o nome do elemento.</p>
	 * @access public
	 * @return string Nome do elemento
	 */
	final public function getAttrName() {
		return $this->csName;
	}

	/**
	 * Retorna a classe de CSS.
	 *
	 * <p>M�todo para retornar a classe de CSS.</p>
	 * @access public
	 * @return string Classe de CSS
	 */
	final public function getAttrClass() {
		return $this->csClass;
	}

	/**
	 * Retorna a Box do elemento.
	 *
	 * <p>M�todo para retornar a Box do elemento.</p>
	 * @access public
	 * @return FWDBox �rea do elemento
	 */
	final public function getObjFWDBox() {
		return $this->coBox;
	}

	/**
	 * Seta o valor booleano do atributo de display.
	 *
	 * <p>Seta o valor booleano do atributo que define se o elemento
	 * deve ser exibido. Para o elemento ser exibido, o atributo deve
	 * conter o valor booleano TRUE. Para n�o ser exibido, o atributo deve
	 * conter o valor booleano FALSE.</p>
	 * @access public
	 * @param string $psDisplay Define se o elemento deve ser exibido ou n�o
	 */
	final public function setAttrDisplay($psDisplay) {
		$this->cbDisplay = FWDWebLib::attributeParser($psDisplay,array("true"=>true, "false"=>false),"psDisplay");
	}

	/**
	 * Retorna o valor booleano do atributo de display.
	 *
	 * <p>Retorna o valor booleano TRUE se o elemento deve ser exibido.
	 * Retorna FALSE caso contr�rio.</p>
	 * @access public
	 * @return boolean Indica se o elemento deve ser exibido ou n�o
	 */
	final public function getAttrDisplay() {
		return $this->cbDisplay;
	}

	/**
	 * Retorna o valor do atributo tag
	 *
	 * <p>Retorna o valor do atributo tag.</p>
	 * @access public
	 * @return string Valor do atributo tag
	 */
	public function getAttrTag(){
		return $this->csTag;
	}

	/**
	 * Seta o valor do atributo tag
	 *
	 * <p>Seta o valor do atributo tag.</p>
	 * @access public
	 * @param string $psTag Novo valor do atributo
	 */
	public function setAttrTag($psTag){
		$this->csTag = $psTag;
	}

	/**
	 * Retorna os eventos do elemento.
	 *
	 * <p>M�todo para retornar os eventos do elemento.</p>
	 * @access public
	 * @return string Eventos
	 */
	final public function getEvents() {
		$msEvents = "";
		foreach ($this->caEvent as $moEvent) {
			$msEvents .= $moEvent->render();
		}
		return $msEvents;
	}

	/**
	 * Seta um evento para o elemento.
	 *
	 * <p>M�todo para setar um evento para o elemento.</p>
	 * @access public
	 * @param FWDEvent $poEvent Evento
	 */
	final public function addObjFWDEvent(FWDEvent $poEvent) {
		$mskey = $poEvent->getAttrEvent();
		$msArrayKey = explode(":",$mskey);
		foreach($msArrayKey as $msKey){
			if(!isset($this->caEvent[$msKey]))
			$this->caEvent[$msKey] = new FWDEventHandler($msKey,$this->getAttrName());
			$this->caEvent[$msKey]->setAttrContent($poEvent);
		}
	}

	/**
	 * Retorna o array de eventos do elemento e o pr�prio elemento.
	 *
	 * <p>M�todo para retornar o array de eventos do elemento e o pr�prio elemento.</p>
	 * @access public
	 * @return array Eventos do elemento e elemento
	 */
	public function collect() {
		$maOut = array($this);
		if ($this->caEvent)
		foreach ($this->caEvent as $moEvent)
		$maOut = array_merge($maOut,$moEvent->getAttrContent());
		return $maOut;
	}

}
?><?
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These codedf instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

define( "GRID_SEL_NONE", 0);
define( "GRID_SEL_SINGLE", 1);
define( "GRID_SEL_MULTIPLE", 2);

/**
 * Classe abstrata FWDGrid. Classe abstrata com os atributos b�sicos da Grid.
 *
 * <p>Classe abstrata que cont�m os atributos b�sicos da Grid.</p>
 * @abstract
 * @package FWD5
 * @subpackage abstracts
 */

class FWDGridAbstract extends FWDViewGrid
{

	protected $csCSSClass = '';
	/**
	 * Array de FWDColumn que cont�m os objetos das colunas da grid
	 * @var array
	 * @access protected
	 */
	public $caColumns = array();

	/**
	 * Array de linhas (que s�o arrays) que cont�m os arrays de cada uma das linhas da grid.
	 * @var array
	 * @access protected
	 */
	protected $caRows = array();

	/**
	 * Array de FWDViewGroup contem os objetos de cada uma das linhas da grid
	 * @var array
	 * @access protected
	 */
	protected $caRowsObj = array();

	/**
	 * Armazena o numero total de linhas da grid
	 * @var integer
	 * @access protected
	 */
	protected $ciRowsCount = 0;

	/**
	 * Armazena o numero total de linhas da grid
	 * @var integer
	 * @access protected
	 */
	protected $ciColumnsCount = 0;

	/**
	 * Armazena a largura da grid
	 * @var integer
	 * @access protected
	 */
	protected $ciGridWidth = 0;

	/**
	 * Array de FWDMenu que cont�m os menus de contextos definidos dentro da grid.
	 * @var array
	 * @access protected
	 */
	protected $caMenus = array();

	/**
	 * Array de FWDMenu que s�o criados como modifica��o de um menu "esqueleto" definido no xml atravez das permi��es
	 * @var array
	 * @access protected
	 */
	protected $caMenusACL = array();

	/**
	 * Armazena o tipo de cele��o de linhas da grid
	 * @var integer
	 * @access protected
	 */
	protected $ciSelectType = GRID_SEL_NONE;

	/**
	 * Armazena as colunas q devem ser usadas como ID da linhas da grid em consultas
	 * @var string
	 * @access protected
	 */
	protected $csSelectColumn = 0;

	/**
	 * Objeto respons�vel por desenhar cada uma das c�lulas da grid.
	 * @var FWDDrawGrid
	 * @access protected
	 */
	protected $coDrawGrid;

	/**
	 * Armazena a p�gina atual da grid
	 * @var integer
	 * @access protected
	 */
	protected $ciCurrentPage=1;

	/**
	 * Armazena o n�mero total de p�ginas da grid
	 * @var integer
	 * @access protected
	 */
	protected $ciLastPage=1;

	/**
	 * Armazena o n�mero de linhas por p�gina da grid
	 * @var integer
	 * @access protected
	 */
	protected $ciRowsPerPage = 50;

	/**
	 * Define o tipo de preenchimento da grid. se � por Ajax(true) ou preencimento est�tico(false)
	 * @var boolean
	 * @access protected
	 */
	protected $cbDinamicFill = false;

	/**
	 * Define A altura de cada uma das linhas da grid.
	 * @var integer
	 * @access protected
	 */
	protected $ciRowHeight =22;

	/**
	 * Define a largura da barra de rolagem utilizada pela grid.
	 * @var integer
	 * @access protected
	 */
	protected $ciRollerBarWidth=16;
	/**
	 * Array Inteiros (0 ou 1) que armazenam se cada coluna deve ser exibida ou n�o
	 * @var array
	 * @access protected
	 */
	protected	$caColumnDisplay = array();

	/**
	 * Array Inteiros (0 ou 1) que armazenam se os eventos devem ou n�o ser renderizados em cada uma das colunas
	 * @var array
	 * @access protected
	 */
	protected	$caEventBlock = array();

	/**
	 * Array Inteiros (0 ou 1) que armazenam se cada coluna deve possuir tooltip ou n�o
	 * @var array
	 * @access protected
	 */
	protected	$caToolTip = array();


	/**
	 * Array Source de tooltip das colunas da grid
	 * @var array
	 * @access protected
	 */
	protected $caToolTipSource = array();
	/**
	 * Define se a grid deve ser populada ou n�o
	 * @var boolean
	 * @access protected
	 */
	protected $cbPopulate = true;

	/**
	 * Define se a grid deve ser populada via xml
	 * @var boolean
	 * @access protected
	 */
	protected $cbPopulateXML = true;

	/**
	 * Define A altura da barra de titulos da grid.
	 * @var integer
	 * @access protected
	 */
	protected $ciTitleHeight = 22;

	/**
	 * Define A altura da barra dos headers das colunas da grid
	 * @var integer
	 * @access protected
	 */
	protected $ciColumnHeight= 22 ;

	/**
	 * Objeto respons�vel por conter o t�tulo da grid e o botao de refresh da grid.
	 * @var FWDViewGroup
	 * @access protected
	 */
	protected $coGridTitle = null;

	/**
	 * @var integer Define A altura do bot�o de refresh da grid.
	 * @access protected
	 */
	protected $ciButtonRefreshHeight = 16;

	/**
	 * Define A largura da barra de bot�es de navega��o da grid.
	 * @var integer
	 * @access protected
	 */
	protected $ciButtonRefreshWidth = 13;

	/**
	 * Define a altura do bot�o de exporta��o para xls da grid.
	 * @var integer
	 * @access protected
	 */
	protected $ciButtonExcelHeight = 16;

	/**
	 * Define a largura do bot�o de exporta��o para xls da grid.
	 * @var integer
	 * @access protected
	 */
	protected $ciButtonExcelWidth = 16;

	/**
	 * Define A largura dos bot�es de navega��o da grid.
	 * @var integer
	 * @access protected
	 */
	protected $ciButtonChangeWidth = 12;


	/**
	 * Define A altura dos bot�es de navega��o da grid.
	 * @var integer
	 * @access protected
	 */
	protected $ciButtonChangeHeight = 12;

	/**
	 * Define A altura do bot�o de edi��o da grid.
	 * @var integer
	 * @access protected
	 */
	protected $ciButtonEditHeight = 16;

	/**
	 * Define A largura do bot�o de edi��o da grid
	 * @var integer
	 * @access protected
	 */
	protected $ciButtonEditWidth = 16;
	/**
	 * @var integer Margem esquerda do �cone ascentende e descendente
	 * @access protected
	 */
	protected $ciIconColumnOrderMarginLeft = 2;

	/**
	 * Margem do �cone at� o Static do �cone ascentende e descendente
	 * @var integer
	 * @access protected
	 */
	protected $ciIconColumnOrderMarginRight = 1;

	/**
	 * Exibe uma div dentro da grid para modificar as colunas que devem ser exibidas na grid
	 * @var integer
	 * antes do valor numerico da coluna indica ordenamento (ascendente, descendente)
	 * @access protected
	 */
	protected $cbShowEdit = false;

	/**
	 * Handle do objeto respons�vel por tratar os dados da popup de edi��o das preferencias da grid
	 * @var string
	 * @access protected
	 */
	protected $csPreferencesHandle = '';

	/**
	 * Colunas que devem ser ordenadas (separadas com ':' ) sinais de (+ e -)
	 * @var integer
	 * antes do valor numerico da coluna indica ordenamento (ascendente, descendente)
	 * @access protected
	 */
	protected $csOrder = "";

	/**
	 * Utilizado para exibir o bot�o de refresh da grid
	 * @var boolean
	 * @access protected
	 */
	protected $cbShowRefresh = true;

	/**
	 * Utilizado para exibir o bot�o de exporta��o da grid
	 * @var boolean
	 * @access protected
	 */
	protected $cbShowExcel = false;

	/**
	 * Ids da selectColumn das linhas que est�o selecionadas na grid
	 * @var array
	 * @access protected
	 */
	protected $caGridValue=array();

	/**
	 * Ids das linhas e dos selectColumn das linhas que est�o selecionadas na grid
	 * formato (array de {nomedalinha;valorSelectColumn1:valorSelectColumn2:...})
	 * @var String
	 * @access protected
	 */
	protected $caGridRowsSelected=array();

	/**
	 * Armazena se est� sendo utilizando o Internet Explorer
	 * @var string
	 * @access protected
	 */
	protected $cbIsIE = false;

	/**
	 * Indica que o requiredCheck testa, n�o se a grid n�o est� vazia, mas se existe
	 * pelo menos uma linha selecionada.
	 * @var boolean
	 * @access protected
	 */
	protected $cbSelectionRequired = false;


	/**
	 * Define cor do contorno do elemento (utilizado para debug)
	 * @var string
	 * @access protected
	 */
	protected $csDebug = "";

	/**
	 * Define A ordem em que as colunas devem ser exibidas na grid
	 * @var array
	 * @access protected
	 */
	protected $caOrderColumnShow = array();

	/**
	 * Ids dos selectColuns que devem ser selecionados pela grid (hakeamento da sele��o!)
	 * @var array
	 * @access protected
	 */
	protected $caHackSelectRows = array();

	/**
	 * Se a sele��o anterior a sele��o hakeada deve ser mantida
	 * @var boolean
	 * @access protected
	 */
	protected $cbHackCleanSelect = false;

	/**
	 * Define se o hakeamento da sele��o de linhas da grid deve ser executada
	 * @var boolean
	 * @access protected
	 */
	protected $cbHackExecHackSelect = false;

	/**
	 * Define a viewgroup contendo o que deve ser mostrado caso a grid tenha Zero Linhas exibidas
	 * @var Object
	 * @access protected
	 */
	protected $coNoResultAlert = null;

	/**
	 * Armazena a classe Zebra0 das linhas da grid
	 * @var string
	 * @access protected
	 */
	protected $csClassBodyZebra0 = "BdZ0";

	/**
	 * Armazena a classe Zebra1 das linhas da grid
	 * @var string
	 * @access protected
	 */
	protected $csClassBodyZebra1 = "BdZ1";

	/**
	 * Armazena a classe ZebraOver (quando o mouse est� sobre uma linha da grid) das linhas da grid
	 * @var string
	 * @access protected
	 */
	protected $csClassBodyZebraOver = "BdZOver";

	/**
	 * Armazena a classe BodyRowSelected (quando uma ou mais linha da grid s�o selecionadas)
	 * @var string
	 *  das linhas da grid
	 * @access protected
	 */
	protected $csClassBodyRowSelected = "BdRSel";

	/**
	 * Armazena a classe HeaderTextEdit (edit que indica a p�gina atual da grid)
	 * @var string
	 * @access protected
	 */
	protected $csClassHeaderTextEdit = "HdTextEdit";

	/**
	 * Armazena a classe da DIV do T�tulo da grid (que engloba o t�tulo e o bot�o de refresh)
	 * @var string
	 * @access protected
	 */
	protected $csClassDivHeaderTitle = "DvHdTitle";

	/**
	 * Armazena a classe do bot�o de refresh da grid.
	 * @var string
	 * @access protected
	 */
	protected $csClassButtonRefresh = "BtRefresh";

	/**
	 * Armazena a classe do bot�o de exporta��o da grid.
	 * @var string
	 * @access protected
	 */
	protected $csClassButtonExcel = "BtExcel";

	/**
	 * Armazena a classe da DivPageButton (div da barra de bot�es de navega��o)
	 * @var string
	 * @access protected
	 */
	protected $csClassDivPageButton = "DvPgBt";

	/**
	 * Armazena a classe  da DIV dos cabe�alhos das colunas da grid
	 * @var string
	 * @access protected
	 */
	protected $csClassDivHeaderColumns = "DvHdCol";

	/**
	 * Armazena a classe DIV do body da grid
	 * @var string
	 * @access protected
	 */
	protected $csClassDivBody = "DvBd";

	/**
	 * Armazena a classe dos Headers das colunas da grid
	 * @var string
	 * @access protected
	 */
	protected $csClassColumnHeader = "ColHd";

	/**
	 * Armazena a classe das C�ludas do corpo da grid
	 * @var string
	 * @access protected
	 */
	protected $csClassBodyCellUnit = "BdUt";

	/**
	 * Armazena a classe da viewGroup do HeaderTitle da grid
	 * @var string
	 * @access protected
	 */
	protected $csClassHeaderViewGroup = "HdViewGroup";

	/**
	 * Armazena a classe dos statics das c�lulas do body da grid
	 * @var string
	 * @access protected
	 */
	protected $csClassStaticBodyCellUnit = "StBdUt";

	/**
	 * Armazena a classe dos statics das c�lulas do body da grid com sem nowrap
	 * @var string
	 * @access protected
	 */
	protected $csClassStaticBodyCellUnitNFix = "StBdUtNFix";

	/**
	 * Armazena a classe dos statics dos headers das colunas da grid
	 * @var string
	 * @access protected
	 */
	protected $csClassStaticColumnHeaders = "StColHd";

	/**
	 * Armazena a classe do static do T�tulo da grid
	 * @var string
	 * @access protected
	 */
	protected $csClassStaticGridTitle = "StGridTitle";

	/**
	 * Armazena a classe do static do HeaderPageButton ("p�gina # de #") da grid
	 * @var string
	 * @access protected
	 */
	protected $csClassStaticHeaderPageButton = "StHdPgBt";

	/**
	 * Armazena a classe do button FirstPage da grid
	 * @var string
	 * @access protected
	 */
	protected $csClassButtonFirstPage = "BtFirstPg";

	/**
	 * Armazena a classe do button NextPage da grid
	 * @var string
	 * @access protected
	 */
	protected $csClassButtonNextPage = "BtNextPg";

	/**
	 * Armazena a classe do button BackPage da grid
	 * @var string
	 * @access protected
	 */
	protected $csClassButtonBackPage = "BtBackPg";

	/**
	 * Armazena a classe do button LastPage da grid
	 * @var string
	 * @access protected
	 */
	protected $csClassButtonLastPage = "BtLastPg";

	/**
	 * Armazena a classe da DIV do refreshButton
	 * @var string
	 * @access protected
	 */
	protected $csClassDivRefreshButton = "DvRefreshBt";

	/**
	 * Armazena a classe da coluna ordenada ascendentemente
	 * @var string
	 * @access protected
	 */
	protected $csClassColumnHeaderOrderASC = "ColHdOrderASC";

	/**
	 * Armazena a classe das colunas que podem ser ordenadas
	 * @var string
	 * @access protected
	 */
	protected $csClassColumnHeaderCanOrder = "ColHdCanOrder";

	/**
	 * Armazena a classe da coluna ordenada descendentemente
	 * @var string
	 * @access protected
	 */
	protected $csClassColumnHeaderOrderDESC= "ColHdOrderDESC";

	/**
	 * Armazena a classe do Static da DIV de troca das colunas que ser�o exibidas na grid
	 * @var string
	 * @access protected
	 */
	protected $csClassTitlePanel = 'StTitlePanel';

	/**
	 * Classe css do bot�o de edi��o da grid
	 * @var string
	 * @access protected
	 */
	protected $csClassButtonEditGrid = 'BtEditPreference';

	/**
	 * Classe css dos bot�es da popup de edi��o de prefer�ncias da grid
	 * @var string
	 * @access protected
	 */
	protected $csClassViewButtonPopupEdit = 'ViewBt';

	/**
	 * Classe css dos statics dos bot�es da popup de edi��o de prefer�ncias da grid
	 * @var string
	 * @access protected
	 */
	protected $csClassStaticPopupPreferences = 'StBtPopupPreference';

	/**
	 * Classe css dos statics da popup de edi��o de prefer�ncias da grid
	 * @var string
	 * @access protected
	 */
	protected $csClassStPopupPreferences = 'StPref';

	/**
	 * Classe css dos Panel da popup de edi��o de prefer�ncias da grid
	 * @var string
	 * @access protected
	 */
	protected $csClassPanelPopupPreferences = 'PanelPref';

	/**
	 * Classe css do bot�o de inserir da popup de edi��o de prefer�ncias da grid
	 * @var string
	 * @access protected
	 */
	protected $csClassVBRight = 'VBPrefRight';

	/**
	 * Classe css do bot�o de remover da popup de edi��o de prefer�ncias da grid
	 * @var string
	 * @access protected
	 */
	protected $csClassVBLeft = 'VBPrefLeft';

	/**
	 * Classe css do bot�o de mover p/ cima da popup de edi��o de prefer�ncias da grid
	 * @var string
	 * @access protected
	 */
	protected $csClassVBTop = 'VBPrefTop';

	/**
	 * Classe css do bot�o de mover p/ baixo da popup de edi��o de prefer�ncias da grid
	 * @var string
	 * @access protected
	 */
	protected $csClassVBBotton = 'VBPrefBotton';

	/**
	 * Classe css da div principal da popup de edi��o de prefer�ncias da grid
	 * @var string
	 * @access protected
	 */
	protected $csClassDialogPopup = 'DialogPopup';


	/**
	 * Indica se ao popular / dar refresh na grid, ela deve voltar para a primeira p�gina
	 * @var boolean
	 * @access protected
	 */
	protected $cbRefreshOnCurrentPage = false;


	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDGridAbstract.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites da Grid
	 */
	public function __construct($poBox = null){
		parent::__construct($poBox);
		$this->coDrawGrid = new FWDDrawGrid();
		$this->cbIsIE = FWDWebLib::browserIsIE();
	}

	/**
	 * Adiciona uma coluna da grid.
	 *
	 * <p>M�todo para adiconar uma coluna na grid.</p>
	 * @access public
	 * @param FWDColumn $poColumn Coluna
	 */
	public function addObjFWDColumn(FWDColumn $poColumn)
	{
		$miIndex = count($this->caColumns);
		//seta o nome da grid na coluna para ser usado como identificador unico nos
		//eventos de ordemamento das colunas
		$poColumn->setGridName($this->csName);
		$this->caColumns[$miIndex] = $poColumn;
		$this->ciColumnsCount = count($this->caColumns);
	}

	/**
	 * Adiciona um menu de contexto na grid.
	 *
	 * <p>M�todo para adiconar um menu de contexto na grid.</p>
	 * @access public
	 * @param FWDMenu $poMenu Menu
	 */
	public function addObjFWDMenu(FWDMenu $poMenu){
		$this->caMenus[] = $poMenu;
		$moEvent = new FWDClientEvent('OnClick','hidemenu',$poMenu->getAttrName());
		$this->addObjFWDEvent($moEvent);
	}

	/**
	 * Adiciona um menu no array de menus da grid, indexando pelo nome de menu.
	 *
	 * <p>Adiciona um menu no array de menus da grid, indexando pelo nome de menu.</p>
	 * @access public
	 * @param FWDMenu $poMenu Menu
	 */
	public function addMenu(FWDMenu $poMenu){
		if (!array_key_exists($poMenu->getAttrName(),$this->caMenusACL)) {
			$this->caMenusACL[$poMenu->getAttrName()] = $poMenu;
			$moEvent = new FWDClientEvent('OnClick','hidemenu',$poMenu->getAttrName());
			$this->addObjFWDEvent($moEvent);
		}
		else{};	// Menu ja foi adicionado na grid, nao precisa adicion�-lo novamente
	}

	/**
	 * Adiciona Uma viewGroup na Grid
	 *
	 * <p>M�todo para adiconar uma ViewGroup na grid que contem o Titulo
	 * e o Bot�o de refresh da grid se o target for o default. se o target for setado,
	 * a viewgroup ser� inserida no atributo do alerta que ser� mostrado quando a grid
	 * nao tem linhas desenhadas</p>
	 *
	 * @access public
	 * @param FWDViewGroup $poViewGroup GridTitle
	 */
	public function addObjFWDViewGroup(FWDViewGroup $poViewGroup, $psTarget = '')
	{
		if(!$psTarget){
			$this->coGridTitle = $poViewGroup;
		}else{
			$this->coNoResultAlert = $poViewGroup;
		}
	}

	/**
	 * seta o objeto respons�vel pelo Draw de cada uma das c�lulas do corpo da Grid.
	 *
	 * <p>Seta o objeto respons�vel pelo Draw de cada uma das c�lulas do corpo da Grid..</p>
	 * @access public
	 * @param FWDDrawGrid $poDrawGrid DrawGrid
	 */
	public function setObjFWDDrawGrid(FWDDrawGrid $poDrawGrid)
	{
		$this->coDrawGrid = $poDrawGrid;
	}

	/**
	 * Define o n�mero de linhas.
	 *
	 * <p>Define o n�mero de linhas por p�gina do corpo da grid.</p>
	 * @access public
	 * @param integer $piRowsPerPage N�mero de linhas por p�gina
	 */
	public function setAttrRowsPerPage($piRowsPerPage)
	{
		if($piRowsPerPage>0)
		$this->ciRowsPerPage=$piRowsPerPage;
		else
		trigger_error("Grid attribute RowsPerPage must be a positive integer",E_USER_ERROR);
	}

	/**
	 * Retorna o n�mero de linhas por p�ginas da grid.
	 *
	 * <p>retorna o n�mero de linhas por p�gina da grid.</p>
	 * @access public
	 * @return integer n�mero de linhas por p�gina da grid.
	 */
	public function getAttrRowsPerPage(){
		return $this->ciRowsPerPage;
	}

	/**
	 * Define o n�mero total de linhas.
	 *
	 * <p>Define o n�mero total de linhas da grid.</p>
	 * @access public
	 * @param integer $piRowsCount N�mero total de linhas
	 */
	public function setAttrRowsCount($piRowsCount)
	{
		if($piRowsCount>=0)
		$this->ciRowsCount=$piRowsCount;
		else
		trigger_error("Grid attribute RowsCount must be a positive integer",E_USER_ERROR);
	}

	/**
	 * Retorna o n�mero de linhas por p�ginas da grid.
	 *
	 * <p>define o comportamento do evento de refresh/populate. se o evento executar� na p�gina atual da grid ou na primeira p�gina.</p>
	 * @access public
	 * @return boolean se o refresh/populate executar� o evento na p�gina atual da grid
	 */
	public function getAttrRefreshOnCurrentPage(){
		return $this->cbRefreshOnCurrentPage;
	}

	/**
	 * Define se o refresh/populate retornar� para a p�gina inicial da grid.
	 *
	 * <p>Define se o refresh/populate retornar� para a p�gina inicial da grid.</p>
	 * @access public
	 * @param boolean $pbRefreshOnCurrentPage Refresh/populate retorna para a p�gina inicial da grid
	 */
	public function setAttrRefreshOnCurrentPage($pbRefreshOnCurrentPage){
		return $this->cbRefreshOnCurrentPage = FWDWebLib::attributeParser($pbRefreshOnCurrentPage,array("true"=>true, "false"=>false),"pbRefreshOnCurrentPage");
	}

	/**
	 * Define a p�gina atual da grid.
	 *
	 * <p>Define p�gina atual a ser exibida pela grid.</p>
	 * @access public
	 * @param integer $piCurrentPage P�gina atual da grid
	 */
	public function setCurrentPage($piCurrentPage)
	{
		if($piCurrentPage > 0)
		$this->ciCurrentPage = $piCurrentPage;
		else
		trigger_error("Grid attribute CurrentPage must be a positive integer",E_USER_ERROR);
	}

	/**
	 * Define se a grid tem preenchimento est�tico ou din�mico.
	 *
	 * <p>M�todo para definir se a grid tem preenchimento est�tico ou din�mico
	 * (via Ajax).</p>
	 * @access public
	 * @param boolean $piDinamicFill "true" ou "false"
	 */
	public function setAttrDinamicFill($pbDinamicFill)
	{
		$this->cbDinamicFill = FWDWebLib::attributeParser($pbDinamicFill,array("true"=>true, "false"=>false),"pbDinamicFill");

		if($this->cbDinamicFill == true)
		{
			$this->cbPopulate=false;
		}
	}

	/**
	 * Seta se deve aparecer a barra de rolagem no corpo da grid.
	 *
	 * <p>M�todo para setar se deve aparecer a barra de rolagem no corpo da grid.</p>
	 * @access public
	 * @param boolean $pbRollerBar valor que indica se deve aparecer a barra de rolagem no corpo da grid.
	 */
	public function setAttrShowRollerBar($pbRollerBar){
		$mbAuxValue = FWDWebLib::attributeParser($pbRollerBar,array("true"=>true, "false"=>false),"pbRollerBar");
		if($mbAuxValue)
		$this->csClassDivBody ="DvBd";
		else{
			$this->csClassDivBody ="DvBdHidden";
			if($this->cbIsIE)
			$this->ciRollerBarWidth=-1;
			else
			$this->ciRollerBarWidth=-2;
		}
	}

	/**
	 * Seta o valor da ultima p�gina da grid.
	 *
	 * <p>M�todo para setar o valor (inteiro) da ultima p�gina da grid.</p>
	 * @access public
	 * @param integer $piLastPage valor da ultima p�gina da grid
	 */
	public function setLastPage($piLastPage)
	{
		if($piLastPage > 0)
		{
			$this->ciLastPage=$piLastPage;
		}
		else
		{
			$this->ciLastPage=1;
		}
	}

	/**
	 * Seta a largura da barra de rolagem na grid.
	 *
	 * <p>M�todo para setar a largura da barra de rolagem na grid.</p>
	 * @access public
	 * @param integer $piWidth Valor para alterar a largura da barra de rolagem na grid
	 */
	public function setAttrRollerBarWidth($piWidth){
		$this->ciRollerBarWidth=$piWidth;
	}

	/**
	 * Seta a altura de cada linha do body da grid.
	 *
	 * <p>M�todo para setar a altura de cada linha do body da grid.</p>
	 * @access public
	 * @param integer $piRowHeight Valor para alterar a altura default das linhas do corpo da grid
	 */
	public function setAttrRowHeight($piRowHeight)
	{
		if($piRowHeight > 0)
		$this->ciRowHeight = $piRowHeight;
		else
		trigger_error("Grid attribute GridLineHeight must be a positive integer",E_USER_ERROR);
	}

	/**
	 * Seta a altura da barra de t�tulo da grid.
	 *
	 * <p>M�todo para setar a altura da barra de t�tulo da grid.</p>
	 * @access public
	 * @param integer $piTitleHeight Valor para alterar a altura default da barra de t�tulo da grid
	 */
	public function setAttrTitleHeight($piTitleHeight)
	{
		if($piTitleHeight > 0)
		$this->ciTitleHeight = $piTitleHeight;
		else
		trigger_error("Grid attribute GridTitleHeight must be a positive integer",E_USER_ERROR);
	}

	/**
	 * Seta a altura da barra do header das colunas da grid
	 *
	 * <p>M�todo para setar a altura do header das colunas da grid.</p>
	 * @access public
	 * @param integer $piColumnHeight Valor para alterar a altura default da barra de header das colunas
	 */
	public function setAttrColumnHeight($piColumnHeight)
	{
		if($piColumnHeight > 0)
		$this->ciColumnHeight = $piColumnHeight;
		else
		trigger_error("Grid attribute GridColumnHeight must be a positive integer",E_USER_ERROR);
	}

	/**
	 * Adiciona um dado na tabela da grid.
	 *
	 * <p>Adiciona um dado (valor de uma c�lula) na tabela de conte�do da grid.</p>
	 * @access public
	 * @param integer $piColumnIndex �ndice da coluna da tabela
	 * @param integer $piRowIndex �ndice da linha da tabela
	 * @param integer $psData informa��o a ser inserida na tabela
	 */
	public function setItem($piColumnIndex, $piRowIndex, $psData)
	{

		if($piColumnIndex<=$this->ciColumnsCount)
		{
			if ($this->ciRowsCount < $piRowIndex)
			{
				$this->ciRowsCount = $piRowIndex;
			}
			else{ }	//$row_index � menor q o contador de linhas , assim n�o eh necess�rio atualiza-lo
				
			if (!isset( $this->caRows[ $piRowIndex]))
			{
				$maRow = array();
				$maRow[ $piColumnIndex] = $psData;
				$this->caRows[ $piRowIndex] = $maRow;
			}
			else
			{
				$this->caRows[$piRowIndex][$piColumnIndex] = $psData;
			}
		}
		else
		{
			trigger_error("Trying to set a value in a inexistent column ({$piColumnIndex}).",E_USER_ERROR);
		}
	}

	/**
	 * Seta o tipo de sele��o das linhas da grid.
	 *
	 * <p>M�todo para setar o tipo de sele��o (none, single, multiple) da grid.</p>
	 * @access public
	 * @param string $psSelectType  tipo de sele��o (none, single, multiple) da grid
	 */
	public function setAttrSelectType($psSelectType)
	{
		$this->ciSelectType = FWDWebLib::attributeParser($psSelectType,array("single"=>GRID_SEL_SINGLE,
								"multiple"=>GRID_SEL_MULTIPLE,"none" =>GRID_SEL_NONE),"psSelectType");
	}

	/**
	 * Seta as colunas da grid que ser�o selecionadas.
	 *
	 * <p>M�todo para setar as colunas que ser�o selecionadas (cujos Ids ser�o utilizados para a sele��o das linhas
	 * da grid e para passar para o menu de contexto da grid) da grid.</p>
	 * @access public
	 * @param string $psSelectColumn  colunas da grid que ser�o utilizadas para a sele��o da grid e para o
	 * menu de contexto
	 */
	public function setAttrSelectColumn($psSelectColumn)
	{
		$this->csSelectColumn = $psSelectColumn;
	}

	/**
	 * Adiciona uma linha na grid.
	 *
	 * <p>M�todo para adiconar uma linha ao corpo da grid.</p>
	 * @access protected
	 * @param FWDViewGroup poRowsObj ViewGroup
	 * @param integer $piIndex �ndice da linha
	 */
	protected function setObjRow(FWDViewGroup $poRowsObj, $piIndex)
	{
		$this->caRowsObj[$piIndex] = $poRowsObj;
	}

	/**
	 * Seta via XML se a grid deve ser populada ou n�o.
	 *
	 * <p>M�todo para setar via XML se a grid grid deve ser populada (deve ser desenhado o
	 * body da grid).</p>
	 * @access public
	 * @param boolean $pbPopulateXML valor "true" "false" para popular / nao popular a grid
	 */
	public function setAttrPopulate($pbPopulateXML)
	{
		$this->cbPopulateXML = FWDWebLib::attributeParser($pbPopulateXML,array("true"=>true, "false"=>false),"pbPopulateXML");
	}

	/**
	 * Retorna se a grid deve ser populada ou n�o.
	 *
	 * <p>M�todo para retornar se a grid deve ser populada ou n�o (NUNCA usar o metodo setPopulate
	 * e SIM o m�todo setAttrPopulate()).</p>
	 * @access public
	 * @return integer p�gina atual da grid
	 */
	public function getAttrPopulate(){
		return $this->cbPopulateXML;
	}

	/**
	 * Seta se a grid deve exibir o bot�o de refresh.
	 *
	 * <p>M�todo para setar se a grid grid deve exibir o bot�o de refresh.</p>
	 * @access public
	 * @param boolean pbShowRefresh valor "true" "false" para mostrar / nao mostrar o bot�o de refresh da grid
	 */
	public function setAttrShowRefresh($pbShowRefresh)
	{
		$this->cbShowRefresh = FWDWebLib::attributeParser($pbShowRefresh,array("true"=>true, "false"=>false),"pbShowRefresh");
	}

	/**
	 * Retorna se deve ser exibido o bot�o de refresh da grid.
	 *
	 * <p>M�todo para retornar o valor do bot�o de refresh da grid.</p>
	 * @access public
	 * @return boolean Se o bot�o de refresh da grid deve ser exibido ou n�o
	 */
	public function getAttrShowRefresh(){
		return $this->sbShowRefresh;
	}

	/**
	 * Seta se a grid deve exibir o bot�o de exporta��o para Excel.
	 *
	 * <p>M�todo para setar se a grid deve exibir o bot�o de exporta��o para Excel.</p>
	 * @access public
	 * @param boolean pbShow True indica que o bot�o deve ser exibido
	 */
	public function setAttrShowExcel($pbShow){
		$this->cbShowExcel = FWDWebLib::attributeParser($pbShow,array("true"=>true, "false"=>false),"showexcel");
	}

	/**
	 * Seta a largura interna da grid.
	 *
	 * <p>M�todo para setar a largura interna da grid com base na soma das larguras das
	 * colunas </p>
	 * @access protected
	 * @param integer $piGridWidth valor da largura da coluna
	 */
	protected function setAttrGridWidth($piGridWidth)
	{
		if($piGridWidth > 0)
		$this->ciGridWidth = $piGridWidth;
		else
		trigger_error("Grid attribute GridWidth must be a positive integer",E_USER_ERROR);
	}

	/**
	 * Retorna o valor da p�gina atual da grid.
	 *
	 * <p>M�todo para retornar o valor da p�gina atual da grid.</p>
	 * @access public
	 * @return integer p�gina atual da grid
	 */
	public function getCurrentPage()
	{
		return $this->ciCurrentPage;
	}

	/**
	 * Retorna o valor da ultima p�gina da grid.
	 *
	 * <p>M�todo para retornar o valor (inteiro) correspondente a ultima p�gina da
	 * grid.</p>
	 * @access public
	 * @return integer �ltima p�gina da grid
	 */
	public function getLastPage()
	{
		return $this->ciLastPage;
	}

	/**
	 * Retorna o array de linhas da grid.
	 *
	 * <p>M�todo para retornar o array de linhas da grid.</p>
	 * @access public
	 * @return array Array de linhas da grid
	 */
	public function getRows()
	{
		return $this->caRows;
	}

	/**
	 * Retorna o conte�do de uma c�lula da grid.
	 *
	 * <p>Retorna o conte�do de uma c�lula da grid.</p>
	 * @access public
	 * @return string O conte�do da c�lula ou false caso ela n�o exista.
	 */
	public function getCellValue($piRow,$piCol){
		if(isset($this->caRows[$piRow][$piCol])){
			return $this->caRows[$piRow][$piCol];
		}else{
			return false;
		}
	}

	/**
	 * Retorna o array de colunas da grid.
	 *
	 * <p>M�todo para retornar o array de colunas da grid.</p>
	 * @access public
	 * @return array Array de colunas da grid
	 */
	public function getColumns()
	{
		return $this->caColumns;
	}

	/**
	 * Retorna o total de linhas da grid.
	 *
	 * <p>M�todo para retornar o total de linhas da grid.</p>
	 * @access public
	 * @return integer Total de linhas da grid
	 */
	public function getAttrRowsCount()
	{
		return $this->ciRowsCount;
	}

	/**
	 * Retorna a altura das linhas da grid.
	 *
	 * <p>M�todo para retornar a altura das linhas da grid.</p>
	 * @access public
	 * @return integer Altura das linhas da grid
	 */
	public function getAttrRowHeight()
	{
		return $this->ciRowHeight;
	}

	/**
	 * Retorna se a grid � populada estaticamente o dinamicamente.
	 *
	 * <p>M�todo utilizado para obter o tipo de preenchimento da grid
	 * se � est�tico ou din�mico.</p>
	 * @access public
	 * @return booblean "true" / "false" preenchimento din�mico / est�tico
	 **/
	public function getAttrDinamicFill()
	{
		return $this->cbDinamicFill;
	}

	/**
	 * Retorna o tipo de sele��o das linhas da grid.
	 *
	 * <p>M�todo utilizado para obter o tipo de sele��o das linhas da grid (none, single, multiple).</p>
	 * @access public
	 * @return integer (0, 1, 2) tipo de sele��o da grid
	 **/
	public function getAttrSelectType()
	{
		return $this->ciSelectType;
	}

	/**
	 * Retorna o n�mero da coluna que deve ser utilizada para sele��o.
	 *
	 * <p>M�todo utilizado para obter o n�mero da coluna que ser� utilizado para sele��o das linhas da grid e
	 * do menu de contexto.</p>
	 * @access public
	 * @return string n�mero da coluna que ser� utlilizada para sele��o das linhas da grid e do menu de contexto
	 **/
	public function getAttrSelectColumn()
	{
		return $this->csSelectColumn;
	}

	/**
	 * Retorna o array de menus de contexto definidos dentro da grid.
	 *
	 * <p>M�todo utilizado para obter o array de menus de contexto definidos dentro da grid.</p>
	 * @access public
	 * @return array array dos menus de contexto definidos dentro da grid.
	 **/
	public function getArrayObjFWDMenus()
	{
		return $this->caMenus;
	}

	/**
	 * Seta o valor da Margem a esquerda do �cone de ordenamento.
	 *
	 * <p>Seta o valor da margem � esquerda do �cone de ordenamento do header das colunas da grid</p>
	 * @access public
	 * @param string $piIconMarginLeft Margem a esquerda do �cone de ordenamento da coluna
	 */
	public function setAttrIconColumnOrderMarginLeft($piIconMarginLeft){
		$this->ciIconColumnOrderMarginLeft = $piIconMarginLeft;
	}

	/**
	 * Seta a margem � direita do �cone de ordenamento.
	 *
	 * <p>Seta o valor da margem � direita (entre o �cone e o Static) do �cone de ordenamento do header das colunas da grid</p>
	 * @access public
	 * @param string $piIconMarginLeft Margem a direita do �cone de ordenamento da coluna
	 */
	public function setAttrIconColumnOrderMarginRight($piIconMarginRight){
		$this->ciIconColumnOrderMarginRight = $piIconMarginRight;
	}

	/**
	 * Retorna A margem � esquerda do Icone de Ordenamento do header das colunas da grid.
	 *
	 * <p>Retorna A margem � a esquerda do Icone de Ordenamento do header das colunas da grid..</p>
	 * @access public
	 * @return integer Margem � esquerda e o Static do header da coluna
	 */
	public function getAttrIconColumnOrderMarginLeft(){
		return $this->ciIconColumnOrderMarginLeft;
	}

	/**
	 * Retorna A margem � direita do Icone de Ordenamento do header das colunas da grid.
	 *
	 * <p>Retorna A margem � direita do Icone de Ordenamento do header das colunas da grid..</p>
	 * @access public
	 * @return integer Margem a direita do icone de ordenamento e o Static do header da coluna
	 */
	public function getAttrIconColumnOrderMarginRight(){
		return $this->ciIconColumnOrderMarginRight;
	}

	/**
	 * Seta as colunas que v�o ordenar a grid.
	 *
	 * <p>Seta as colunas que v�o ordenar a grid (separadas por ':'). Os sinais de (+,-)
	 * antes do n�mero da coluna inticam ordenamento (ascendente, descendente).</p>
	 * @access public
	 * @param string $psOrder Colunas que v�o ordenar a grid.
	 */
	public function setAttrOrder($psOrder){
		$this->csOrder = $psOrder;
	}

	/**
	 * Retorna as colunas que devem ordenar a grid.
	 *
	 * <p>Retorna as colunas que devem ordenar a grid (separadas por ':'). Os sinais de (+,-)
	 * antes do n�mero da coluna inticam ordenamento (ascendente, descendente).</p>
	 * @access public
	 * @return String Colunas que devem ordenar a grid
	 */
	public function getAttrOrder(){
		return $this->csOrder;
	}

	/**
	 * Atribui o valor dos ids das colunas selecionadas da grid.
	 *
	 * <p>Atribui o valor dos ids das colunas selecionadas na grid</p>
	 * @access public
	 * @param string $paGridValue Ids da linhas selecionadas na grid
	 */
	public function setSelectValue($paGridValue) {
		if($paGridValue =='')
		$this->caGridValue = array();
		else
		$this->caGridValue = $paGridValue;
	}

	/**
	 * Retorna os Ids(ids dos selectColumn) das linhas selecionadas da grid.
	 *
	 * <p>M�todo para retornar os Ids da linhas selecionadas da grid.</p>
	 * @access public
	 * @return string Ids das linhas selecionadas da grid
	 */
	public function getSelectValue() {
		return $this->caGridValue;
	}

	/**
	 * Seta o nome do objeto que ir� tratar as preferencias do usu�rio para a grid
	 *
	 * <p>Seta o nome do objeto que ir� tratar as preferencias do usu�rio para a grid, permitindo a
	 * configura��o de v�rios itens da grid.</p>
	 * @access public
	 * @param boolean $psPreferencesHandle Nome do objeto que tratar� os dados de configura��o da grid
	 */
	public function setAttrPreferencesHandle($psPreferencesHandle){
		$this->csPreferencesHandle = $psPreferencesHandle;
	}

	/**
	 * Retorna o nome do objeto que trata das preferencias do usu�rio
	 *
	 * <p>M�todo para retornar o nome do objeto que tratar� as preferencias de usu�rio da grid.</p>
	 * @access public
	 * @return string Nome do objeto que trata das preferencias do usu�rio
	 */
	public function getAttrPreferencesHandle(){
		return $this->csPreferencesHandle;
	}
	/**
	 * Retorna os Ids(ids dos selectColumn) das linhas selecionadas da grid.
	 *
	 * <p>M�todo para retornar os Ids da linhas selecionadas da grid.</p>
	 * @access public
	 * @return array Ids das linhas selecionadas da grid
	 */
	public function getValue(){
		$maReturn = array();
		$maColumnIndex = explode(':',$this->csSelectColumn);
		if(count($maColumnIndex)>1){
			foreach ($this->caGridValue as $csValue){
				$caValue = explode(':',$csValue);
				$miCont=0;
				$maRow = array();
				foreach ($maColumnIndex as $csColumn){
					if(isset($caValue[$miCont]))
					$maRow[$csColumn] = $caValue[$miCont];
					else
					$maRow[$csColumn] ='';
						
					$miCont++;
				}
				$maReturn[]=$maRow;
				unset($maRow);
			}
		}else
		foreach ($this->caGridValue as $csValue)
		$maReturn[]=$csValue;

		return $maReturn;
	}

	/**
	 * Retorna os Ids(ids dos selectColumn) das linhas selecionadas da grid.
	 *
	 * <p>M�todo para retornar os Ids da linhas selecionadas da grid.</p>
	 * @access public
	 * @param integer $piColumnIndex index da coluna dos selectColumns q devem ser retornados
	 * @return array valores de selectColumns das linhas referentes ao columnIndex desejahdo
	 */
	public function getArraySelectColumns($piColumnIndex){
		$maValue = $this->getValue();
		$maReturn = array();
		$maColumnIndex = explode(':',$this->csSelectColumn);
		if(count($maColumnIndex)>1)
		foreach($maValue as $maValueRow){
			if(isset($maValueRow[$piColumnIndex]))
			$maReturn[]= $maValueRow[$piColumnIndex];
		}
		else
		if($piColumnIndex == $maColumnIndex[0])
		foreach($maValue as $maValueRow){
			$maReturn[]= $maValueRow;
		}
		return $maReturn;
	}

	/**
	 * Retorna os Ids (ids da linha;id do selectColumn)das linhas selecionadas da grid.
	 *
	 * <p>M�todo para retornar os Ids da linhas selecionadas da grid.</p>
	 * @access public
	 * @return string Ids das linhas selecionadas da grid
	 */
	public function getRowsSelected(){
		return $this->caGridRowsSelected;
	}

	/**
	 * Atribui o valor dos ids (ids da linha;id do selectColumn)das colunas selecionadas da grid.
	 *
	 * <p>Atribui o valor dos ids (ids da linha;id do selectColumn) das colunas selecionadas na grid</p>
	 * @access public
	 * @param string $paRowsSelected Ids (ids da linha;id do selectColumn) da linhas selecionadas na grid
	 */
	public function setRowsSelected($paRowsSelected){
		if($paRowsSelected=='')
		$this->caGridRowsSelected = array();
		else
		$this->caGridRowsSelected = $paRowsSelected;
	}

	/**
	 * Seta se deve ser exibido o bot�o de edi��o de preferencias da grid
	 *
	 * <p>Seta se deve ser exibido o bot�o de edi��o de preferencias da grid, assim � permitido
	 * configurar as op��o de cada usu�rio em uma grid.</p>
	 * @access public
	 * @param boolean $pbShowEdit Se vai ser exibido o o bot�o de edi��o de preferencias da grid.
	 */
	public function setAttrShowEdit($pbShowEdit){
		$this->cbShowEdit = FWDWebLib::attributeParser($pbShowEdit,array("true"=>true, "false"=>false),"pbShowEdit");
	}

	/**
	 * Seta o Prefixo da Classe CSS dos elementos da grid
	 *
	 * <p>Seta o prefixo da classe css da grid , assim � permitido alterar a familia de classes css da grid.</p>
	 * @access public
	 * @param string $psCSSClass Seta o Prefixo da Classe CSS dos elementos da grid.
	 */
	public function setAttrCSSClass($psCSSClass){
		$this->csCSSClass = $psCSSClass;
	}

	/**
	 * Retorna o prefixo das classes css da grid.
	 *
	 * <p>Metodo para retornar o prefixo das classes css da grid.</p>
	 * @access public
	 * @return String Prefixo das classes css da grid.
	 */
	public function getAttrCSSClass(){
		$this->csCSSClass;
	}

	/**
	 * Retorna a classe css do titulo do panel de preferencias da grid
	 *
	 * <p>Metodo para retornar a classe css do titulo do panel de preferencias da grid.</p>
	 * @access public
	 * @return String Classe CSS do titulo do panel da grid.
	 */
	public function getCssClassTitlePanel(){
		return $this->csClassTitlePanel;
	}

	/**
	 * Redefine as dimens�es das c�lulas do header da Grid
	 *
	 * <p>M�todo para  redefinir as dimens�es da box dos statics dentro da coluna para
	 * ter as mesmas dimen��es da box de cada coluna</p>
	 * @access protected
	 */
	protected function defineBoxStatic($piLastColumnWidth)
	{
		//redefine a box do static dentro da coluna p/ ter as mesmas dimen��es da box da coluna]
		if(count($this->caOrderColumnShow)>0){
			$miContColumn = $this->caOrderColumnShow[count($this->caOrderColumnShow)-1];
			foreach($this->caOrderColumnShow as $miI)
			{
				$moBox= $this->caColumns[$miI]->getObjFWDBox();
				if ($this->cbIsIE)
				$moBoxStatic = new FWDBox(-1,-1,$moBox->getAttrHeight()+2,$moBox->getAttrWidth()+2);
				else
				$moBoxStatic = new FWDBox(-1,-1,$moBox->getAttrHeight()+2,$moBox->getAttrWidth()+2);
				foreach($this->caColumns[$miI]->getContent() as $moColStatic)
				if(!($moColStatic instanceof FWDIcon))
				$moColStatic->setObjFWDBox($moBoxStatic);
			}
		}
	}

	/**
	 * Redefine as dimens�es das boxes de cada uma das colunas definidas na grid
	 *
	 * <p>M�todo para  redefinir as dimens�es da box das colunas da grid e para setar
	 * algumas v�riaveis da grid que ser�o utilizadas em outros m�todos da grid como:
	 * a largura interna da grid (soma das larguras de todas as colunas definidas na grid)
	 * um vetor para sinalizar quais colunas estao ativas (devem ser exibidas na tela)
	 * um vetor para sinalizar quais colunas nao devem ter eventos
	 * </p>
	 * @access protected
	 */
	public function arrangeAllDimensions()
	{
		//obtem dados q ser�o usados frequentemente
		// largura das colunas
		// se a coluna tah ativa ou n�o
		// se os eventos est�o bloqueados ou n�o para cada coluna
		// n�menro total de colunas da grid

		$miI = 1;
		$miContColumnWidth=0;//largura acumulada das colunas da grid
		$miColWithoutWidth=0;//contador de colunas que n�o tem largura definida
		$miMaxColumnHeight=0;

		$mbHasColumnOrderToShow = count($this->caOrderColumnShow);

		foreach($this->caColumns as $moColumn)
		{
			$moBox=$moColumn->getObjFWDBox();
			$this->caColumnDisplay[$miI] =(($moColumn->getAttrDisplay() == true)?1:0);
			$this->caEventBlock[$miI] =  (($moColumn->getAttrEventBlock() == true)?1:0);
			$this->caToolTip[$miI] =(($moColumn->getAttrToolTip() == true)?1:0);
			$this->caToolTipSource[$miI] = ($moColumn->getSourceToolTip()?$moColumn->getSourceToolTip():$miI);

			if(!$mbHasColumnOrderToShow){
				if($moColumn->getAttrDisplay()){
					$this->caOrderColumnShow[]=$miI -1;
				}
			}
			$miI++;

			if($moColumn->getAttrDisplay())
			{
				if($miMaxColumnHeight < $moBox->getAttrHeight())
				$miMaxColumnHeight = $moBox->getAttrHeight();
				if($moBox->getAttrWidth()<=0)
				$miColWithoutWidth++;
				else
				$miContColumnWidth+=$moBox->getAttrWidth();
			}
			//else { }//a coluna n�o vai ser exibida, ou seja, n�o eh necess�rio calcular a largura acumulada da grid
		}

		/*descobre qual a ultima coluna que vai ser exibida*/
		$miLastColumn = count($this->caOrderColumnShow)-1;

		//verifica se foi definido no xml a altura da barra do header das colunas
		//se nao foi definido, usar a maior altura das box dos header das colunas
		if($this->ciColumnHeight<=0)
		$this->ciColumnHeight = $miMaxColumnHeight;
		//else { }//foi definido pelo xml a altura dos headers das colunas

		if($miColWithoutWidth<=0)
		//apenas para evitar dividir por zero
		$miColWithoutWidth=1;
		//else {}//uma ou mais colunas foram definidas sem a largura, assim ser� necess�rio dividir a largura restante
		//da grid entre essas colunas.
		//calcula largura para as colunas em rela��o a largura da grid, j� considerando a largura da barra de rolagem
		$miContColumnWidth = $this->coBox->getAttrWidth() - $miContColumnWidth - $this->ciRollerBarWidth;
		//calcula a largura para de cada uma das colunas que n�o tem largura definida
		$miContColumnWidth = floor($miContColumnWidth / $miColWithoutWidth);
		$miSumWidth=0;
		$miContColumn=0;
		//redefine as dimen�oes das colunas com base nos calculos executados acima
		$miLastWidth = $this->coBox->getAttrWidth() - $this->ciRollerBarWidth -4;

		foreach($this->caOrderColumnShow as $miI){
			$moBox=$this->caColumns[$miI]->getObjFWDBox();
			$moBox->setAttrLeft($miSumWidth-1);
			$moBox->setAttrTop(-1);
			$moBox->setAttrHeight($this->ciColumnHeight -1);
			//caso a box da coluna n�o foi definida, ser� utilizada a largura calculada da sobra da lar
			//gura da grid que foi calculada anteriormente
			if($moBox->getAttrWidth()<=0)
			$moBox->setAttrWidth($miContColumnWidth);
			//else { }//a coluna tem sua largura definida assim n�o � necessario utilizar a largura calculada.
			$miSumWidth += $moBox->getAttrWidth();
			if($miLastColumn == $miContColumn){
				if ($this->cbIsIE)
				$miLastWidth = $miLastWidth - $moBox->getAttrLeft()+2;
				else
				$miLastWidth = $miLastWidth - $moBox->getAttrLeft();
				$moBox->setAttrWidth($miLastWidth);
			}
			else
			if ($this->cbIsIE)
			$moBox->setAttrWidth($moBox->getAttrWidth() +1);
			else
			$moBox->setAttrWidth($moBox->getAttrWidth() -1);
			$this->caColumns[$miI]->setObjFWDBox($moBox);
			unset($moBox);
			$miContColumn++;
		}//end foreach
		$this->setAttrGridWidth($this->coBox->getAttrWidth());
		$this->defineBoxStatic($miLastWidth);
	}

	/**
	 * Seta o modo de debug do elemento.
	 *
	 * <p>M�todo para setar o modo de debug do elemento.</p>
	 * @access public
	 * @param string $psDebug Seta o modo de debug
	 */
	final public function setAttrDebug($psDebug) {
		$this->csDebug = (($psDebug=="false")?"":$psDebug);
	}

	/**
	 * Seta o valor do atributo selectionrequired
	 *
	 * <p>Seta o valor do atributo selectionrequired.</p>
	 * @access public
	 * @param boolean $pbSelectionRequired Novo valor do atributo
	 */
	public final function setAttrSelectionRequired($psSelectionRequired){
		$this->cbSelectionRequired = FWDWebLib::attributeParser($psSelectionRequired,array("true"=>true, "false"=>false),"selectionrequired");
	}

	/**
	 * Retorna o estilo para o modo de debug.
	 *
	 * <p>M�todo para retornar o estilo para o modo de debug.</p>
	 * @access private
	 * @return string Estilo do debug
	 */
	public function debugView() {
		$moRGBTable = FWDWebLib::getObject("goRGBTable");
		$msDebug = $this->csDebug;
		$msColor = ((in_array($msDebug,$moRGBTable)||($msDebug[0]=="#"&&strlen($msDebug)==7))?$msDebug:"red");
		return "border-color:{$msColor};border-style:dashed;border-width:1px;";
	}

	/**
	 * Retorna um array com todos sub-elementos da grid
	 *
	 * <p>Retorna um array contendo todos elementos contidos na grid ou em
	 * seus descendentes.</p>
	 * @access public
	 * @return array Array de sub-elementos
	 */
	public function collect() {
		$maOut = parent::collect();
		if($this->coGridTitle)
		$maOut = array_merge($maOut,$this->coGridTitle->collect());
		if($this->caMenus)
		foreach ($this->caMenus as $moMenu)
		$maOut = array_merge($maOut,$moMenu->collect());
		if($this->caMenusACL)
		foreach ($this->caMenusACL as $moMenuACL)
		$maOut = array_merge($maOut,$moMenuACL->collect());
		if($this->caColumns)
		foreach ($this->caColumns as $moColumn)
		$maOut = array_merge($maOut,$moColumn->collect());
		return $maOut;
	}


	/**
	 * Retorna o nome da coluna passada por parametro
	 *
	 * <p>Cria um nome para a coluna passada por parametro caso ela n�o tenha nome e retorna esse nome.</p>
	 * @access public
	 * @return string Nome para a coluna
	 */
	private function getColumnName($poColumn,$piIndex){
		$msReturn = '';
		$msReturn = $poColumn->getValue();
		if($msReturn==''){
			$msReturn = $poColumn->getAttrAlias();
			if($msReturn=='')
			$msReturn = FWDLanguage::getPHPStringValue('fwdgrid_pref_column_name', 'Coluna').$piIndex;
		}
		return $msReturn;
	}

	/**
	 * Cria os objetos do panel de edi��o das propriedades da grid
	 *
	 * <p>Cria os objetos do panel de edi��o das propriedades da grid.</p>
	 * @access public
	 * @return Objeto FWDPanel
	 */
	private function getCodePanelGridEdit(){
		//panel de propriedades da grid
		$moPanel = new FWDPanel(new FWDBox(410,10,150,300));
		$moPanel->setAttrClass($this->csCSSClass . $this->csClassPanelPopupPreferences);
		//static do titulu do panel
		$moStP = new FWDStatic(new FWDBox(0,0,20,300));
		$moStP->setAttrValue(FWDLanguage::getPHPStringValue('fwdgrid_pref_grid_properties', 'Edi��o das Propriedades da Grid'));
		$moStP->setAttrClass($this->csCSSClass . $this->csClassTitlePanel);
		$moStP->setAttrVertical('middle');
		$moStP->setAttrMarginLeft(10);
		$moPanel->addObjFWDView($moStP,'title');
		//st linhas por p�gina
		$moStRowPage = new FWDStaticGrid(new FWDBox(10,30,20,120));
		$moStRowPage->setValue(FWDLanguage::getPHPStringValue('fwdgrid_pref_rows_per_page', 'Linhas por P�gina:'));
		$moStRowPage->setAttrClass($this->csCSSClass . $this->csClassStPopupPreferences);
		$moStRowPage->setAttrHorizontal('right');
		$moPanel->addObjFWDView($moStRowPage);
		//text row page
		$moTextRowPage = new FWDText(new FWDBox(140,30,20,90));
		$moTextRowPage->setAttrName("{$this->csName}_popupRowsPage");
		$moTextRowPage->setValue($this->ciRowsPerPage);
		$moMask = new FWDMaskAutoComplete();
		$moMask->setAttrType('digit');
		$moTextRowPage->addObjFWDMask($moMask);
		$moPanel->addObjFWDView($moTextRowPage);
		//st altura das linhas
		$moStRowHeight = new FWDStaticGrid(new FWDBox(10,60,20,120));
		$moStRowHeight->setValue(FWDLanguage::getPHPStringValue('fwdgrid_pref_rows_height', 'Altura das Linhas:'));
		$moStRowHeight->setAttrClass($this->csCSSClass . $this->csClassStPopupPreferences);
		$moStRowHeight->setAttrHorizontal('right');
		$moPanel->addObjFWDView($moStRowHeight);
		//text altura das linhas
		$moTextRowHeight = new FWDText(new FWDBox(140,60,20,90));
		$moTextRowHeight->setAttrName("{$this->csName}_popupRowsHeight");
		$moTextRowHeight->setValue($this->ciRowHeight);
		$moMaskHeight = new FWDMaskAutoComplete();
		$moMaskHeight->setAttrType('digit');
		$moTextRowHeight->addObjFWDMask($moMaskHeight);
		$moPanel->addObjFWDView($moTextRowHeight);

		//st tooltip conte�do
		$moStRowTT = new FWDStaticGrid(new FWDBox(10,90,20,120));
		$moStRowTT->setValue(FWDLanguage::getPHPStringValue('fwdgrid_pref_tooltip_label', 'ToolTip:'));
		$moStRowTT->setAttrClass($this->csCSSClass . $this->csClassStPopupPreferences);
		$moStRowTT->setAttrHorizontal('right');
		$moPanel->addObjFWDView($moStRowTT);
		//select da coluna que ser� o conte�do da tooltip
		$moSelectTT = new FWDSelect(new FWDBox(140,90,20,150));
		$moSelectTT->setAttrName("{$this->csName}_sel_tt");
		//select da coluna que receber� o tooltip
		$moSelectTTtarget = new FWDSelect(new FWDBox(140,120,20,150));
		$moSelectTTtarget->setAttrName("{$this->csName}_sel_tt_target");
		//itens auxiliares do select para explica��o ao usu�rio
		$moItem = new FWDItem();
		$moItem->setValue(FWDLanguage::getPHPStringValue('fwdgrid_pref_select_source','Selecione a Origem'));
		$moSelectTT->addObjFWDItem($moItem);
		$moItem = new FWDItem();
		$moItem->setValue(FWDLanguage::getPHPStringValue('fwdgrid_pref_select_target', 'Selecione o Alvo'));
		$moSelectTTtarget->addObjFWDItem($moItem);
		$miSourceTT = -1;
		for($miI=0;$miI<$this->ciColumnsCount;$miI++){
			//item do select p/ cada coluna
			if($this->caColumns[$miI]->getAttrEdit()){
				$moItemTgt = new FWDItem();
				$moItemTgt->setAttrKey($miI+1);
				$moItemTgt->setValue($this->getColumnName($this->caColumns[$miI],$miI));
				if($this->caColumns[$miI]->getAttrToolTip()){
					$moItemTgt->setAttrCheck('true');
					$miSourceTT = $this->caColumns[$miI]->getSourceToolTip();
					if(!$miSourceTT)
					$miSourceTT = $miI+1;
				}
				$moSelectTTtarget->addObjFWDItem($moItemTgt);
				unset($moItem);
			}
		}
		for($miI=0;$miI<$this->ciColumnsCount;$miI++){
			//item do select p/ cada coluna
			if($this->caColumns[$miI]->getAttrEdit()){
				$moItemSrc = new FWDItem();
				$moItemSrc->setAttrKey($miI+1);
				$moItemSrc->setValue($this->getColumnName($this->caColumns[$miI],$miI));
				if($miI == ($miSourceTT -1))
				$moItemSrc->setAttrCheck('true');
				$moSelectTT->addObjFWDItem($moItemSrc);
				unset($moItem);
			}
		}
		$moPanel->addObjFWDView($moSelectTT);
		$moPanel->addObjFWDView($moSelectTTtarget);
		//st tooltip target
		$moStRowTTtarget = new FWDStaticGrid(new FWDBox(10,120,20,120));
		$moStRowTTtarget->setValue(FWDLanguage::getPHPStringValue('fwdgrid_pref_target', 'Alvo:'));
		$moStRowTTtarget->setAttrClass($this->csCSSClass . $this->csClassStPopupPreferences);
		$moStRowTTtarget->setAttrHorizontal('right');
		$moPanel->addObjFWDView($moStRowTTtarget);
		return $moPanel;
	}

	/**
	 * Cria os objetos do panel de edi��o das propriedades das colunas da grid
	 *
	 * <p>Cria os objetos do panel de edi��o das propriedades das colunas da grid.</p>
	 * @access public
	 * @return Objeto FWDPanel
	 */
	public function getCodePanelColumnEdit(){
		$msVarCode = '';
		$moPanel = new FWDPanel(new FWDBox(410,170,150,300));
		$moPanel->setAttrClass($this->csCSSClass . $this->csClassPanelPopupPreferences);
		//static do titulu do panel
		$moStP = new FWDStatic(new FWDBox(0,0,20,300));
		$moStP->setAttrValue(FWDLanguage::getPHPStringValue('fwdgrid_pref_column_properties','Propriedades da Coluna'));
		$moStP->setAttrClass($this->csCSSClass . $this->csClassTitlePanel);
		$moStP->setAttrVertical('middle');
		$moStP->setAttrMarginLeft(10);
		$moPanel->addObjFWDView($moStP,'title');
		//st tooltip conte�do
		$moStCol = new FWDStaticGrid(new FWDBox(10,30,20,120));
		$moStCol->setValue(FWDLanguage::getPHPStringValue('fwdgrid_pref_column_label', 'Coluna:'));
		$moStCol->setAttrClass($this->csCSSClass . $this->csClassStPopupPreferences);
		$moStCol->setAttrHorizontal('right');
		$moPanel->addObjFWDView($moStCol);
		//select que exibe as colunas
		$moSelectColProp = new FWDSelect(new FWDBox(140,30,20,150));
		$moSelectColProp->setAttrName("{$this->csName}_sel_col_prop");
		//evento onclick do select
		$moSelColEvent = new FWDClientEvent();
		$moSelColEvent->setAttrValue('objManagerGrifPref.propColClick();');
		$moSelColEvent->setAttrEvent('onClick');
		$moSelectColProp->addObjFWDEvent($moSelColEvent);

		$moItem = new FWDItem();
		$moSelectColProp->addObjFWDItem($moItem);
		$msAuxSeparate = '';
		for($miI=0;$miI<$this->ciColumnsCount;$miI++){
			//item do select p/ cada coluna
			if(($this->caColumns[$miI]->getAttrEdit())&&($this->caColumns[$miI]->getAttrDisplay())){
				$moItem = new FWDItem();
				$moItem->setAttrKey($miI);
				$moItem->setValue($this->getColumnName($this->caColumns[$miI],$miI));
			}
			if($this->caColumns[$miI]->getAttrEdit()){
				$msNoWrap ='true';
				if($this->caColumns[$miI]->getAttrColNoWrap()==true)
				$msNoWrap ='false';
				$msVarCode .= $msAuxSeparate . $miI .':'. $this->caColumns[$miI]->getObjFWDBox()->getAttrWidth() .':'.$msNoWrap ;
				$msAuxSeparate = ';';
			}
		}
		$moPanel->addObjFWDView($moSelectColProp);
		//st linhas por p�gina
		$moStColWidth = new FWDStaticGrid(new FWDBox(10,60,20,120));
		$moStColWidth->setValue(FWDLanguage::getPHPStringValue('fwdgrid_pref_column_width','Largura*:'));
		$moStColWidth->setAttrClass($this->csCSSClass . $this->csClassStPopupPreferences);
		$moStColWidth->setAttrHorizontal('right');
		$moPanel->addObjFWDView($moStColWidth);
		//text Col page
		$moTextColWidth = new FWDText(new FWDBox(140,60,20,70));
		$moTextColWidth->setAttrName("{$this->csName}_popupColWidth");
		$moMask = new FWDMaskAutoComplete();
		$moMask->setAttrType('digit');
		$moTextColWidth->addObjFWDMask($moMask);
		$moPanel->addObjFWDView($moTextColWidth);
		//st tooltip conte�do
		$moStnoWrap = new FWDStaticGrid(new FWDBox(10,90,20,120));
		$moStnoWrap->setValue('Quebra de linha:');
		$moStnoWrap->setAttrClass($this->csCSSClass . $this->csClassStPopupPreferences);
		$moStnoWrap->setAttrHorizontal('right');
		$moPanel->addObjFWDView($moStnoWrap);
		//select do noWrap da coluna selecionada
		$moSelectnoWrap = new FWDSelect(new FWDBox(140,90,20,70));
		$moSelectnoWrap->setAttrName("{$this->csName}_sel_no_wrap");
		$moItem = new FWDItem();
		$moSelectnoWrap->addObjFWDItem($moItem);
		$moItemT = new FWDItem();
		$moItemT->setAttrKey('true');
		$moItemT->setValue(FWDLanguage::getPHPStringValue('fwdgrid_pref_true_value', 'Sim'));
		$moSelectnoWrap->addObjFWDItem($moItemT);
		$moItemF = new FWDItem();
		$moItemF->setAttrKey('false');
		$moItemF->setValue(FWDLanguage::getPHPStringValue('fwdgrid_pref_false_value', 'N�o'));
		$moSelectnoWrap->addObjFWDItem($moItemF);
		$moPanel->addObjFWDView($moSelectnoWrap);
		$moColumnsPropVar = new FWDVariable();
		$moColumnsPropVar->setAttrName("{$this->csName}_columns_prop_var");
		$moColumnsPropVar->setAttrValue($msVarCode);
		$moPanel->addObjFWDView($moColumnsPropVar);
		//st tooltip conte�do
		if($this->cbIsIE)
		$moStObs = new FWDStaticGrid(new FWDBox(135,117,130,120));
		else
		$moStObs = new FWDStaticGrid(new FWDBox(135,70,130,120));

		$moStObs->setValue(FWDLanguage::getPHPStringValue('fwdgrid_pref_width_obs', "* '0' = largura autom�tica"));
		$moStObs->setAttrClass($this->csCSSClass . $this->csClassStPopupPreferences);
		$moPanel->addObjFWDView($moStObs);


		return $moPanel;
	}

	/**
	 * Cria os objetos do panel de edi��o das propriedades das colunas da grid
	 *
	 * <p>Cria os objetos do panel de edi��o das propriedades das colunas da grid.</p>
	 * @access public
	 * @return Objeto FWDPanel
	 */
	public function getCodePanelColumnShow(){
		$moPanel = new FWDPanel(new FWDBox(10,10,150,390));
		$moPanel->setAttrClass($this->csCSSClass . $this->csClassPanelPopupPreferences);
		//st do titulo do panel
		$moStP = new FWDStatic(new FWDBox(0,0,20,390));
		$moStP->setAttrValue(FWDLanguage::getPHPStringValue('fwdgrid_pref_column_show_title','Colunas Exibidas e Ordem de Exibi��o entre elas'));
		$moStP->setAttrClass($this->csCSSClass . $this->csClassTitlePanel);
		$moStP->setAttrVertical('middle');
		$moStP->setAttrMarginLeft(10);
		$moPanel->addObjFWDView($moStP,'title');
		//select das colunas n�o exibidas
		$moSelectCol = new FWDSelect(new FWDBox(10,50,90,150));
		$moSelectCol->setAttrName("{$this->csName}_sel_col_notshow");
		$moSelectCol->setAttrSize(5);
		$moSelColEvent = new FWDClientEvent('onClick');
		$moSelColEvent->setAttrValue("objManagerGrifPref.selShowOnClick(\"{$this->csName}_sel_col_notshow\"); ");
		$moSelectCol->addObjFWDEvent($moSelColEvent);

		//select das colunas que ser�o exibidas
		$moSelectColShow = new FWDSelect(new FWDBox(200,50,90,150));
		$moSelectColShow->setAttrSize(5);
		$moSelectColShow->setAttrName("{$this->csName}_sel_col_show");
		$moSelColShowEvent = new FWDClientEvent('onClick');
		$moSelColShowEvent->setAttrValue("objManagerGrifPref.selShowOnClick(\"{$this->csName}_sel_col_show\"); ");
		$moSelectColShow->addObjFWDEvent($moSelColShowEvent);
		$moSelColShowEvent2 = new FWDClientEvent('onchange');
		$moSelColShowEvent2->setAttrValue("objManagerGrifPref.selShowOnClick(\"{$this->csName}_sel_col_show\"); ");
		$moSelectColShow->addObjFWDEvent($moSelColShowEvent2);

		$maAuxDisplayCol = array();
		$msAuxDisplayColEdit = '';
		foreach($this->caOrderColumnShow as $miI){
			$moItem = new FWDItem();
			$moItem->setAttrKey($miI);
			$moItem->setValue($this->getColumnName($this->caColumns[$miI],$miI));
			$moSelectColShow->addObjFWDItem($moItem);
			$maAuxDisplayCol[$miI]=$miI;
		}
		for($miI=0;$miI<count($this->caColumns);$miI++){
			if(!(isset($maAuxDisplayCol[$miI])))
			if(($this->caColumns[$miI]->getAttrDisplay()==false)&&($this->caColumns[$miI]->getAttrEdit())){
				$moItem = new FWDItem();
				$moItem->setAttrKey($miI);
				$moItem->setValue($this->getColumnName($this->caColumns[$miI],$miI));
				$moSelectCol->addObjFWDItem($moItem);
			}
			if($this->caColumns[$miI]->getAttrEdit()==true)
			$msAux = $miI . ':1';
			else
			$msAux = $miI . ':2';
			if($msAuxDisplayColEdit == '')
			$msAuxDisplayColEdit .= $msAux;
			else
			$msAuxDisplayColEdit .= ';'.$msAux;
		}
		$moPanel->addObjFWDView($moSelectCol);
		$moPanel->addObjFWDView($moSelectColShow);
		//variable que ir� guardar o c�digo de quais colunas podem ser edit�veis ou n�o
		$moColEditVar = new FWDVariable();
		$moColEditVar->setAttrName("{$this->csName}_var_col_editable");
		$moColEditVar->setValue($msAuxDisplayColEdit);
		$moPanel->addObjFWDView($moColEditVar);
		//st colunas da grid
		$moStColGrid = new FWDStaticGrid(new FWDBox(10,30,20,150));
		$moStColGrid->setValue(FWDLanguage::getPHPStringValue('fwdgrid_pref_grid_columns_title','Colunas da grid:'));
		$moStColGrid->setAttrClass($this->csCSSClass . $this->csClassStPopupPreferences);
		$moPanel->addObjFWDView($moStColGrid);
		//st linhas por p�gina
		$moStColShow = new FWDStaticGrid(new FWDBox(200,30,20,150));
		$moStColShow->setValue(FWDLanguage::getPHPStringValue('fwdgrid_pref_columns_show','Colunas exibidas:'));
		$moStColShow->setAttrClass($this->csCSSClass . $this->csClassStPopupPreferences);
		$moPanel->addObjFWDView($moStColShow);
		//VB adicionar
		$moVBRight = new FWDViewButton(new FWDBox(170,70,20,20));
		$moVBRight->setAttrName($this->csName ."btRightShow");
		$moVBRight->setAttrClass($this->csClassVBRight);
		$moRightEvent = new FWDClientEvent('onClick');
		$moRightEvent->setAttrValue("objManagerGrifPref.moveValueSelMod(\"{$this->csName}_sel_col_notshow\",\"{$this->csName}_sel_col_show\");");
		$moVBRight->addObjFWDEvent($moRightEvent);
		$moPanel->addObjFWDView($moVBRight);
		//VB remover
		$moVBLeft = new FWDViewButton(new FWDBox(170,95,20,20));
		$moVBLeft->setAttrName($this->csName . "btLeftShow");
		$moVBLeft->setAttrClass($this->csClassVBLeft);
		$moLeftEvent = new FWDClientEvent('onClick');
		$moLeftEvent->setAttrValue("objManagerGrifPref.moveValueSelMod(\"{$this->csName}_sel_col_show\",\"{$this->csName}_sel_col_notshow\");");
		$moVBLeft->addObjFWDEvent($moLeftEvent);
		$moPanel->addObjFWDView($moVBLeft);
		//VB mover p/ cima
		$moVBTop = new FWDViewButton(new FWDBox(355,70,20,20));
		$moVBTop->setAttrClass($this->csClassVBTop);
		$moUpEvent = new FWDClientEvent('onClick');
		$moUpEvent->setAttrValue("objManagerGrifPref.moveUp('{$this->csName}_sel_col_show');");
		$moVBTop->addObjFWDEvent($moUpEvent);
		$moPanel->addObjFWDView($moVBTop);
		//VB mover p/ cima
		$moVBBotton = new FWDViewButton(new FWDBox(355,95,20,20));
		$moVBBotton->setAttrClass($this->csClassVBBotton);
		$moDownEvent = new FWDClientEvent('onClick');
		$moDownEvent->setAttrValue("objManagerGrifPref.moveDown('{$this->csName}_sel_col_show');");
		$moVBBotton->addObjFWDEvent($moDownEvent);
		$moPanel->addObjFWDView($moVBBotton);
		return $moPanel;
	}

	/**
	 * Cria os objetos do panel de ordena��o das colunas da grid
	 *
	 * <p>Cria os objetos do panel de ordena��o das colunas da grid.</p>
	 * @access public
	 * @return Objeto FWDPanel
	 */
	public function getCodePanelColumnOrder(){
		$moPanel = new FWDPanel(new FWDBox(10,170,150,390));
		$moPanel->setAttrClass($this->csCSSClass . $this->csClassPanelPopupPreferences);
		//st do titulo do panel
		$moStP = new FWDStatic(new FWDBox(0,0,20,390));
		$moStP->setAttrValue(FWDLanguage::getPHPStringValue('fwdgrid_pref_column_order_title','Ordenamento das colunas da grid'));
		$moStP->setAttrClass($this->csCSSClass . $this->csClassTitlePanel);
		$moStP->setAttrVertical('middle');
		$moStP->setAttrMarginLeft(10);
		$moPanel->addObjFWDView($moStP,'title');
		//select das colunas n�o exibidas
		$moSelectCol = new FWDSelect(new FWDBox(10,50,60,150));
		$moSelectCol->setAttrName("{$this->csName}_sel_col_notorder");
		$moSelectCol->setAttrSize(5);
		//select das colunas que ser�o exibidas
		$moSelectColShow = new FWDSelect(new FWDBox(200,50,60,150));
		$moSelectColShow->setAttrSize(5);
		$moSelectColShow->setAttrName("{$this->csName}_sel_col_order");

		$mbAchou = false;
		$maOrder = explode(":",$this->csOrder);
		if(($maOrder))
		foreach($maOrder as $msColOrder){
			$msColOrder=trim($msColOrder);
			if($msColOrder!=""){
				$miColumn="";
				$msChar = substr(trim($msColOrder),0,1);
				if(($msChar=="+")||($msChar=="-"))// ascendente
				$miColumn = substr(trim($msColOrder),1);
				else//assumindo que soh veio um n�mero , ordena��o ascendente
				$miColumn = trim($msColOrder);
				if($this->caColumns[$miColumn-1]->getOrderBy()!=''){
					$mbAchou=true;
					$moItem = new FWDItem();
					$moItem->setAttrKey($miColumn);
					$moItem->setValue($this->getColumnName($this->caColumns[$miColumn-1],$miColumn-1));
					$moSelectColShow->addObjFWDItem($moItem);
				}
			}
		}

		$msOrderColumnData = '';
		for($miI=0;$miI<$this->ciColumnsCount;$miI++){
			//item do select p/ cada coluna
			if(($this->caColumns[$miI]->getAttrEdit())&&($this->caColumns[$miI]->getAttrCanOrder())){
				$miColumnId = $miI+1;
				$moItem = new FWDItem();
				$moItem->setAttrKey($miColumnId);
				$moItem->setValue($this->getColumnName($this->caColumns[$miI],$miColumnId));
				if($this->caColumns[$miI]->getOrderBy()!=''){
					$msOrderBy = $this->caColumns[$miI]->getOrderBy()=='-'?'-':'+';
					if($msOrderColumnData=='')
					$msOrderColumnData .= $miColumnId .':'. $msOrderBy;
					else
					$msOrderColumnData .= ';' . $miColumnId .':'. $msOrderBy;
					if(!$mbAchou)
					$moSelectColShow->addObjFWDItem($moItem);
				}else
				$moSelectCol->addObjFWDItem($moItem);
				unset($moItem);
			}
		}
		//evento que trata dos valores de ordenamento das colunas da grid
		$moOrderEvent = new FWDClientEvent('onClick');
		$moOrderEvent->setAttrValue("objManagerGrifPref.orderColClick();");
		$moSelectColShow->addObjFWDEvent($moOrderEvent);

		$moPanel->addObjFWDView($moSelectCol);
		$moPanel->addObjFWDView($moSelectColShow);
		//st colunas da grid
		$moStColGrid = new FWDStaticGrid(new FWDBox(10,30,20,150));
		$moStColGrid->setValue(FWDLanguage::getPHPStringValue('fwdgrid_pref_columns_grid','Colunas da grid:'));
		$moStColGrid->setAttrClass($this->csCSSClass . $this->csClassStPopupPreferences);
		$moPanel->addObjFWDView($moStColGrid);
		//st linhas por p�gina
		$moStColShow = new FWDStaticGrid(new FWDBox(200,30,20,150));
		$moStColShow->setValue(FWDLanguage::getPHPStringValue('fwdgrid_pref_column_order','Ordenamento:'));
		$moStColShow->setAttrClass($this->csCSSClass . $this->csClassStPopupPreferences);
		$moPanel->addObjFWDView($moStColShow);
		//VB adicionar
		$moVBRight = new FWDViewButton(new FWDBox(170,55,20,20));
		$moVBRight->setAttrClass($this->csClassVBRight);
		$moRightEvent = new FWDClientEvent('onClick');
		$moRightEvent->setAttrValue("objManagerGrifPref.orderColClickAdd();objManagerGrifPref.moveValueSel(\"{$this->csName}_sel_col_notorder\",\"{$this->csName}_sel_col_order\");");
		$moVBRight->addObjFWDEvent($moRightEvent);
		$moPanel->addObjFWDView($moVBRight);
		//VB remover
		$moVBLeft = new FWDViewButton(new FWDBox(170,80,20,20));
		$moVBLeft->setAttrClass($this->csClassVBLeft);
		$moLeftEvent = new FWDClientEvent('onClick');
		$moLeftEvent->setAttrValue("objManagerGrifPref.orderColClickRemove();objManagerGrifPref.moveValueSel(\"{$this->csName}_sel_col_order\",\"{$this->csName}_sel_col_notorder\");");
		$moVBLeft->addObjFWDEvent($moLeftEvent);
		$moPanel->addObjFWDView($moVBLeft);
		//VB mover p/ cima
		$moVBTop = new FWDViewButton(new FWDBox(355,55,20,20));
		$moVBTop->setAttrClass($this->csClassVBTop);
		$moUpEvent = new FWDClientEvent('onClick');
		$moUpEvent->setAttrValue("objManagerGrifPref.moveUp('{$this->csName}_sel_col_order');");
		$moVBTop->addObjFWDEvent($moUpEvent);
		$moPanel->addObjFWDView($moVBTop);
		//VB mover p/ cima
		$moVBBotton = new FWDViewButton(new FWDBox(355,80,20,20));
		$moVBBotton->setAttrClass($this->csClassVBBotton);
		$moDownEvent = new FWDClientEvent('onClick');
		$moDownEvent->setAttrValue("objManagerGrifPref.moveDown('{$this->csName}_sel_col_order');");
		$moVBBotton->addObjFWDEvent($moDownEvent);
		$moPanel->addObjFWDView($moVBBotton);
		//st ordenamento da coluna
		$moStColOrder = new FWDStatic(new FWDBox(170,120,20,65));
		$moStColOrder->setValue('Ordem:');
		$moStColOrder->setAttrName("{$this->csName}_st_order_by");
		$moStColOrder->setAttrDisplay('false');
		$moStColOrder->setAttrClass($this->csCSSClass . $this->csClassStPopupPreferences);
		$moStColOrder->setAttrHorizontal('right');
		$moPanel->addObjFWDView($moStColOrder);
		//select do ColOrder da coluna selecionada
		$moSelectColOrder = new FWDSelect(new FWDBox(240,120,20,110));
		$moSelectColOrder->setAttrName("{$this->csName}_sel_order_column_by");
		$moSelectColOrder->setAttrDisplay('false');
		$moItemASC = new FWDItem();
		$moItemASC->setAttrKey('+');
		$moItemASC->setValue('Ascendente');
		$moSelectColOrder->addObjFWDItem($moItemASC);
		$moItemDESC = new FWDItem();
		$moItemDESC->setAttrKey('-');
		$moItemDESC->setValue('Descendente');
		$moSelectColOrder->addObjFWDItem($moItemDESC);
		$moPanel->addObjFWDView($moSelectColOrder);
		//variable que ir� guardar o c�digo do ordenamento da grid
		$moOrderColumnCode = new FWDVariable();
		$moOrderColumnCode->setAttrName("{$this->csName}_sel_order_column_data");
		$moOrderColumnCode->setValue($msOrderColumnData);
		$moPanel->addObjFWDView($moOrderColumnCode);
		return $moPanel;
	}

	/**
	 * Cria a popup de edi��o das preferencias da grid.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar a popup de edi��o das preferencias da grid.</p>
	 * @access public
	 * @return string C�digo HTML da popup de edi��o de preferencias da grid
	 */
	public function createHTMLPopup(){
		//dimens�es da div
		$miLeft = 0;
		$miHeight = 360;
		$miWidth = 720;
		$miTop = 21;

		$moBox = new FWDBox(20,50);
		//c�digo que � gerado por intera��o
		$msReturn ="";
		//panel principal da popup
		$moPanel = new FWDViewGroup(new FWDBox(0,-1,350,720));

		//objetos do panel de edi��o das colunas que ser�o exibidas
		$moPanel->addObjFWDView($this->getCodePanelColumnShow());
		//objetos do panel de edi��o da grid
		$moPanel->addObjFWDView($this->getCodePanelGridEdit());
		//objetos do panel de edi��o da ordena��o da grid pelas colunas
		$moPanel->addObjFWDView($this->getCodePanelColumnOrder());
		//objetos do panel de edi��o da coluna
		$moPanel->addObjFWDView($this->getCodePanelColumnEdit());

		$moCleanEvent = new FWDClientEvent('onUnLoad');
		$moCleanEvent->setAttrValue("objManagerGrifPref.resetConfig();");
		$moPanel->addObjFWDEvent($moCleanEvent);

		//bot�o para fechar a janela
		$moBtClose = new FWDViewButton(new FWDBox(470,330,20,70));
		$moBtClose->setAttrClass($this->csCSSClass . $this->csClassViewButtonPopupEdit);
		$moCloseEvent = new FWDClientEvent('onClick');
		$moCloseEvent->setAttrValue("soPopUpManager.closePopUp('{$this->csName}_preferences');");
		$moBtClose->addObjFWDEvent($moCloseEvent);
		$moStaticClose = new FWDStaticGrid();
		$moStaticClose->setValue('Fechar');
		$moStaticClose->setObjFWDBox(new FWDBox(0,0,20,70));
		$moStaticClose->setAttrHorizontal('center');
		$moStaticClose->setAttrVertical('middle');
		$moStaticClose->setAttrClass($this->csCSSClass . $this->csClassStaticPopupPreferences);
		$moBtClose->addObjFWDView($moStaticClose);
		$moPanel->AddObjFWDView($moBtClose);
		//bot�o para salvar as modifica��es
		$moBtSave = new FWDViewButton(new FWDBox(555,330,20,70));
		$moBtSave->setAttrClass($this->csCSSClass . $this->csClassViewButtonPopupEdit);
		$moSaveEvent = new FWDClientEvent('onClick');
		$moSaveEvent->setAttrValue(" objManagerGrifPref.savePreferencesCodeInGrid();");
		$moBtSave->addObjFWDEvent($moSaveEvent);
		$moStaticSave = new FWDStaticGrid();
		$moStaticSave->setValue('Salvar');
		$moStaticSave->setObjFWDBox(new FWDBox(0,0,20,70));
		$moStaticSave->setAttrHorizontal('center');
		$moStaticSave->setAttrVertical('middle');
		$moStaticSave->setAttrClass($this->csCSSClass . $this->csClassStaticPopupPreferences);
		$moBtSave->addObjFWDView($moStaticSave);
		$moPanel->AddObjFWDView($moBtSave);
		//bot�o para resetar as modifica��es do usu�rio
		$moBtReset = new FWDViewButton(new FWDBox(640,330,20,70));
		$moBtReset->setAttrClass($this->csCSSClass . $this->csClassViewButtonPopupEdit);
		$moResetEvent = new FWDClientEvent('onClick');
		$moResetEvent->setAttrValue("objManagerGrifPref.resetConfig();");
		$moBtReset->addObjFWDEvent($moResetEvent);
		$moStaticReset = new FWDStaticGrid();
		$moStaticReset->setValue('Resetar');
		$moStaticReset->setObjFWDBox(new FWDBox(0,0,20,70));
		$moStaticReset->setAttrHorizontal('center');
		$moStaticReset->setAttrVertical('middle');
		$moStaticReset->setAttrClass($this->csCSSClass . $this->csClassStaticPopupPreferences);
		$moBtReset->addObjFWDView($moStaticReset);
		$moPanel->AddObjFWDView($moBtReset);

		return $this->scapeStringToAjax("<div name='edit_preferences' id='edit_preferences' class='{$this->csCSSClass}{$this->csClassDialogPopup}' style='top:$miTop;left:$miLeft;height:$miHeight;width:$miWidth;display:block;'>{$moPanel->draw()}</div>");
	}

	public function setAttrClassDivHeaderTitle($psClassDivHeaderTitle) {
		$this->csClassDivHeaderTitle = $psClassDivHeaderTitle;
	}

	public function setAttrClassStaticGridTitle($psClassStaticGridTitle) {
		$this->csClassStaticGridTitle = $psClassStaticGridTitle;
	}

	public function setAttrZebra0($psClassName) {
		$this->csClassBodyZebra0 = $psClassName;
	}

	public function setAttrZebra1($psClassName) {
		$this->csClassBodyZebra1 = $psClassName;
	}

	public function setAttrZebraOver($psClassName) {
		$this->csClassBodyZebraOver = $psClassName;
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDIcon. Implementa �cones (imagens).
 *
 * <p>Classe simples para manipula��o de �cones (imagens, html tag 'img scr').</p>
 * @package FWD5
 * @subpackage abstracts
 */
abstract class FWDGridParameter {

	/**
	 * Armazena as colunas que ir�o ordenar a grid. formato:{+ ou -)numeroColuna}*
	 * (qdo existir mais de uma ocorr�ncia, elas ser�o separadas por ':') a ordem da string � importante
	 * @var string
	 * @access protected
	 */
	private $csOrderColumnsData;

	/**
	 * Armazena as colunas que ser�o exibidas na grid e a ordem de exibi��o delas na grid
	 * s�o separadas por ':' e a ordem do n�mero da coluna � importante (ser� a ordem de exibi��o da coluna na grid)
	 * @var string
	 * @access protected
	 */
	private $csOrderColumn = '';

	/**
	 * Armazena as propriedades da cada coluna da grid.
	 * formato: {numCol:width:noWrap;}*
	 * @var string
	 * @access protected
	 */
	private $csColumnsProp = '';

	/**
	 * Armazena o identificador do usu�rio que est� logado no sistema.
	 * @var string
	 * @access protected
	 */
	private $csUserId = '';

	/**
	 * Armazena a string serializada das preferencias do usu�rio
	 * formato: {numCol:width:noWrap;}*
	 * @var string
	 * @access protected
	 */
	private $csPreferenceValue = '';

	/**
	 * Armazena o n�mero de linhas por p�gina da grid
	 * @var integer
	 * @access protected
	 */
	private $ciRowsPerPage = '';

	/**
	 * Armazena a altura das linhas da grid
	 * @var integer
	 * @access protected
	 */
	private $ciRowsHeight = '';

	/**
	 * Armazena a coluna fonte de dados e a coluna alvo do tooltip da grid
	 * @var integer
	 * @access protected
	 */
	private $csToolTipCol = '';

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDGridParameter.</p>
	 * @access public
	 * @param string $psUserId Id do usu�rio logado no sistema
	 */
	public function __construct($psUserId) {
		$this->csUserId = $psUserId;
	}

	/**
	 * Carrega no objeto de preferencias a preferencia do usu�rio para a grid passada por par�metro.
	 *
	 * <p>Carrega no objeto de preferencias a preferencia do usu�rio para a grid passada por par�metro.
	 * A fonte dos dados depende de cada sistema, podendo ser BD, cookies, etc</p>
	 * @access protected
	 * @param string $psObjId Id da grid
	 */
	abstract protected function getPreference($psObjId);

	/**
	 * Salva os dados armazenados no objeto de preferencias do usu�rio.
	 *
	 * <p>Salva os dados armazenados no objeto de preferencias do usu�rio no destino definido no sistema.
	 * O destino dos dados depende de cada sistema, podendo ser BD, cookies, etc</p>
	 * @access protected
	 * @param string $psObjId Id da grid
	 * @param string $psData string serializada contendo as preferencias do usu�rio da grid
	 */
	abstract protected function updatePreference($psObjId,$psData);

	/**
	 * Deleta as preferencias do usu�rio para o objeto definido em $psObjId.
	 *
	 * <p>deleta as preferencias do usu�rio para o objeto definido em $psObjId do sistema de armazenamento definido no sistema.
	 * O meio de armazenamento dos dados depende de cada sistema, podendo ser BD, cookies, etc</p>
	 * @param string $psObjId Id da grid
	 * @access protected
	 */
	abstract protected function deletePreference($psObjId);

	/**
	 * Salva os dados armazenados no objeto de preferencias do usu�rio.
	 *
	 * <p>Salva os dados armazenados no objeto de preferencias do usu�rio no destino definido no sistema.
	 * O destino dos dados depende de cada sistema, podendo ser BD, cookies, etc</p>
	 * @access public
	 * @param string $psObjId Id da grid
	 */
	public function savePreferences($psObjId){
		$maAuxValue = array();
		$maAuxValue[] = $this->csOrderColumnsData;
		$maAuxValue[] = $this->csOrderColumn;
		$maAuxValue[] = $this->csColumnsProp;
		$maAuxValue[] = $this->ciRowsPerPage;
		$maAuxValue[] = $this->ciRowsHeight;
		$maAuxValue[] = $this->csToolTipCol;

		$this->UpdatePreference($psObjId, serialize($maAuxValue));
	}

	/**
	 * Carrega no objeto de preferencias a preferencia do usu�rio para a grid passada por par�metro.
	 *
	 * <p>Carrega no objeto de preferencias a preferencia do usu�rio para a grid passada por par�metro.
	 * A fonte dos dados depende de cada sistema, podendo ser BD, cookies, etc</p>
	 * @access protected
	 * @param string $psObjId Id da grid
	 */
	public function loadPrefereces($psObjId){
		$maAuxValue = FWDWebLib::unserializeString($this->getPreference($psObjId));
		$this->csOrderColumnsData = $maAuxValue[0];
		$this->csOrderColumn = $maAuxValue[1];
		$this->csColumnsProp = $maAuxValue[2];
		$this->ciRowsPerPage = $maAuxValue[3];
		$this->ciRowsHeight = $maAuxValue[4];
		$this->csToolTipCol = $maAuxValue[5];
	}

	/**
	 * Deleta as preferencias do usu�rio para o objeto definido em $psObjId.
	 *
	 * <p>deleta as preferencias do usu�rio para o objeto definido em $psObjId do sistema de armazenamento definido no sistema.
	 * O meio de armazenamento dos dados depende de cada sistema, podendo ser BD, cookies, etc</p>
	 * @param string $psObjId Id da grid
	 * @access protected
	 */
	public function resetPreferenses($psObjId){
		$this->deletePreference($psObjId);
	}

	/**
	 * Seta a ordem e quais colunas devem ser exibidas na grid.
	 *
	 * <p>Seta a ordem e quais colunas devem ser exibidas na grid.</p>
	 * @access public
	 * @param string $psOrderColumn ordem e quais colunas devem ser exibidas na grid.
	 */
	public function setOrderColumn($psOrderColumn){
		$this->csOrderColumn = $psOrderColumn;
	}

	/**
	 * Retorna a ordem de exibi��o das colunas da grid.
	 *
	 * <p>Retorna a ordem de exibi��o das colunas da grid</p>
	 * @access public
	 * @return string ordem de exibi��o  das colunas da grid
	 */
	public function getOrderColumn(){
		return $this->csOrderColumn;
	}

	/**
	 * Seta as propriedades das colunas da grid.
	 *
	 * <p>Seta as propriedades das colunas da grid.</p>
	 * @access public
	 * @param string $psColumnsProp propriedades das colunas da grid..
	 */
	public function setColumnsProp($psColumnsProp){
		$this->csColumnsProp = $psColumnsProp;
	}

	/**
	 * Retorna as propriedades das colunas da grid.
	 *
	 * <p>Retorna as propriedades das colunas da grid</p>
	 * @access public
	 * @return string propriedades das colunas da grid
	 */
	public function getColumnsProp(){
		return $this->csColumnsProp;
	}

	/**
	 * Seta as colunas q devem ordenar a grid e o tipo de ordenamento dos dados das colunas da grid.
	 *
	 * <p>Seta as colunas q devem ordenar a grid e o tipo de ordenamento dos dados das colunas da grid.</p>
	 * @access public
	 * @param string $psColumnOrder colunas q devem ordenar a grid e o tipo de ordenamento dos dados das colunas da grid
	 */
	public function setOrderColumnsData($psColumnOrder){
		$this->csOrderColumnsData = $psColumnOrder;
	}

	/**
	 * Retorna as colunas q devem ordenar a grid e o tipo de ordenamento dos dados das colunas da grid.
	 *
	 * <p>Retorna as colunas q devem ordenar a grid e o tipo de ordenamento dos dados das colunas da grid</p>
	 * @access public
	 * @return string colunas q devem ordenar a grid e o tipo de ordenamento dos dados das colunas da grid.
	 */
	public function getOrderColumnsData(){
		return $this->csOrderColumnsData;
	}

	/**
	 * Seta o n�mero de linhas por p�gina da grid.
	 *
	 * <p>Seta o n�mero de linhas por p�gina da grid.</p>
	 * @access public
	 * @param integer $piRowsPerPage n�mero de linhas por p�gina da grid
	 */
	public function setRowsPerPage($piRowsPerPage){
		$this->ciRowsPerPage = $piRowsPerPage;
	}

	/**
	 * Retorna o n�mero de linhas por p�gina da grid.
	 *
	 * <p>Retorna o n�mero de linhas por p�gina da grid.</p>
	 * @access public
	 * @return integer n�mero de linhas por p�gina da grid.
	 */
	public function getRowsPerPage(){
		return $this->ciRowsPerPage;
	}

	/**
	 * Seta a altura das linhas da grid.
	 *
	 * <p>Seta a altura das linhas da grid.</p>
	 * @access public
	 * @param integer $piRowsHeight altura das linhas da grid.
	 */
	public function setRowsHeight($piRowsHeight){
		$this->ciRowsHeight = $piRowsHeight;
	}

	/**
	 * Retorna a altura das linhas da grid.
	 *
	 * <p>Retorna a altura das linhas da grid..</p>
	 * @access public
	 * @return string altura das linhas da grid.
	 */
	public function getRowsHeight(){
		return $this->ciRowsHeight;
	}

	/**
	 * Seta as informa��es de tooltip da grid.
	 *
	 * <p>Seta as informa��es de tooltip da grid.</p>
	 * @access public
	 * @param integer $psToolTipCol informa��es de tooltip da grid.
	 */
	public function setToolTipCol($psToolTipCol){
		$this->csToolTipCol = $psToolTipCol;
	}

	/**
	 * Retorna as informa��es de tooltip da grid.
	 *
	 * <p>Retorna as informa��es de tooltip da grid.</p>
	 * @access public
	 * @return string informa��es de tooltip da grid.
	 */
	public function getToolTipCol(){
		return $this->csToolTipCol;
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDDBQueryHandler.
 *
 * <p>Classe abstrata que representa uma consulta SQL que deve ser armazenada em um FWDDBDataSet.</p>
 * @package FWD5
 * @subpackage db
 */
abstract class FWDDBQueryHandler {
		
	/**
	 * Objeto FWDDBDataSet
	 * @var FWDDBDataSet
	 * @access protected
	 */
	protected $coDataSet = NULL;

	/**
	 * Cosulta SQL
	 * @var string
	 * @access protected
	 */
	protected $csSQL = "";

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDDBQueryHandler.</p>
	 * @access public
	 * @param FWDDB $poDB Objeto de conex�o com o banco
	 */
	public function __construct($poDB = null){
		if($poDB){
			$moDB = $poDB;
		}else{
			$moDB = FWDWebLib::getConnection();
		}
		$this->coDataSet = new FWDDBDataSet($moDB);
	}

	/**
	 * Retorna o dataset com os resultados da consulta.
	 *
	 * <p>M�todo para retornar o dataset com os resultados da consulta.</p>
	 * @access public
	 * @return FWDDBDataSet Dataset
	 */
	public function getDataset() {
		return $this->coDataSet;
	}

	/**
	 * Retorna o SQL da consulta.
	 *
	 * <p>M�todo para retornar o SQL da consulta.</p>
	 * @access public
	 * @return string SQL
	 */
	public function getSQL() {
		return $this->csSQL;
	}

	/**
	 * Constr�i a consulta.
	 *
	 * <p>M�todo para construir a consulta.</p>
	 * @access protected
	 */
	abstract protected function makeQuery();

	/**
	 * Executa a consulta.
	 *
	 * <p>M�todo para executar a consulta.</p>
	 * @access public
	 * @return integer N�mero de registros retornados pela consulta
	 */
	public function executeQuery($piLimit = 0, $piOffset = 0) {
		if(isset($this->coDataSet, $this->csSQL)) {
			$this->coDataSet->setQuery($this->csSQL);
			if ($this->coDataSet->execute($piLimit,$piOffset))
			return $this->coDataSet->getRowCount();
			else
			trigger_error("Invalid SQL statement", E_USER_WARNING);
		}
		else
		trigger_error("Dataset and/or query not initialized", E_USER_WARNING);
	}

	/**
	 * Busca um registro do resultado e avan�a para o pr�ximo.
	 *
	 * <p>M�todo para buscar um registro do resultado e avan�ar para o pr�ximo.</p>
	 * @access public
	 * @return boolean Verdadeiro (sucesso) ou Falso (falha)
	 */
	public function fetch(){
		return $this->coDataSet->fetch();
	}

	/**
	 * Retorna o valor de uma campo do dataset.
	 *
	 * <p>M�todo para retornar o valor de um campo do dataset.</p>
	 * @access public
	 * @param string $psFieldAlias Alias do campo
	 * @return string Valor do campo
	 */
	public function getFieldValue($psFieldAlias){
		$moField = $this->coDataSet->getFieldByAlias($psFieldAlias);
		if($moField){
			return $moField->getValue();
		}else{
			trigger_error("There is no field with the alias \"{$psFieldAlias}\"", E_USER_WARNING);
		}
	}

	/**
	 * Retorna um array associativo com os valores dos campos.
	 *
	 * <p>Retorna um array associativo com os valores dos campos, no formato alias=>valor.</p>
	 * @access public
	 * @return array Array associativo com os valores dos campos
	 */
	public function getFieldValues(){
		return $this->coDataSet->getFieldValues();
	}

}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDEvent. Implementa eventos.
 *
 * <p>Classe que implementa eventos dentro da Framework FWD5.</p>
 * @package FWD5
 * @subpackage event
 * @abstract
 */
abstract class FWDEvent extends FWDRunnable {

	/**
	 * Nome do evento
	 * @var string
	 * @access protected
	 */
	protected $csName = "";

	/**
	 * Fonte do evento
	 * @var string
	 * @access protected
	 */
	protected $csSource = "this";

	/**
	 * Alvo do evento
	 * @var string
	 * @access protected
	 */
	protected $csEvent = "";

	/**
	 * Fun��o do evento (pode ser uma constante num�rica ou uma string)
	 * @var integer
	 * @access protected
	 */
	protected $csFunction = "";

	/**
	 * Elementos sobre os quais o evento atuar�
	 * @var string
	 * @access protected
	 */
	protected $csElements = "";

	/**
	 * Par�metros necess�rios a alguns eventos
	 * @var string
	 * @access protected
	 */
	protected $csParameters = "";

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDEvent. Seta os atributos de alvo,
	 * fun��o, elementos e par�metros do evento.</p>
	 *
	 * @access public
	 *
	 * @param string $psEvent Alvo do evento.
	 * @param string $psFunction Fun��o do evento.
	 * @param string $psElements Elemento sobre o qual o evento atuar�.
	 * @param string $psParameters Par�metros necess�rios a alguns eventos.
	 */
	public function __construct($psEvent,$psFunction,$psElements,$psParameters) {
		$this->csEvent = $psEvent;
		$this->csFunction = $this->translateStringToConstant($psFunction);
		$this->csElements = $psElements;
		$this->csParameters = $psParameters;
		parent::__construct($psFunction);
	}

	/**
	 * Renderiza evento.
	 *
	 * <p>M�todo abstrato para renderiza��o do evento (Deve ser implementado
	 * nas classes que estenderem a FWDEvent).</p>
	 * @access public
	 * @return string Evento renderizado
	 * @abstract
	 */
	abstract public function render();

	/**
	 * Cria um objeto de evento javascript n�o definido pelo usu�rio.
	 *
	 * <p>Cria um objeto de evento, caso o evento n�o seja um evento criado pelo
	 * usu�rio (i.e, caso o evento seja um evento javascript "num�rico").
	 * OBS.: O Client Event ja possui cria tal objeto. Deve existir um m�todo
	 * run() que n�o fa�a nada, apenas sobreescreva este na classe FWDClientEvent.</p>
	 * @access public
	 */
	public function run() {
		$js = new FWDJsEvent($this->getAttrFunction(),$this->getAttrElements(),$this->getAttrParameters());
		echo $js->render();
	}

	/**
	 * Seta o nome do evento.
	 *
	 * <p>Seta o nome do evento.</p>
	 * @access public
	 * @param string $psName Nome do evento
	 */
	public function setAttrName($psName) {
		$this->csName = $psName;
		$this->setAttrIdentifier($psName);
	}

	/**
	 * Seta a fonte do evento.
	 *
	 * <p>Seta a fonte do evento.</p>
	 * @access public
	 * @param string $psSource Fonte do evento
	 */
	public function setAttrSource($psSource) {
		$this->csSource = $psSource;
	}

	/**
	 * Seta o alvo do evento.
	 *
	 * <p>Seta o alvo do evento.</p>
	 * @access public
	 * @param string $psEvent Alvo do evento
	 */
	public function setAttrEvent($psEvent) {
		$this->csEvent = $psEvent;
	}

	/**
	 * Seta a fun��o do evento.
	 *
	 * <p>Seta o atributo de fun��o do evento (traduz a fun��o passada
	 * como par�metro de string para sua respectiva constante). Seta o
	 * identificador do evento do usu�rio.</p>
	 * @access public
	 * @param string $psFunction Fun��o do evento
	 */
	public function setAttrFunction($psFunction) {
		$this->csFunction = $this->translateStringToConstant($psFunction);
	}

	/**
	 * Seta os elementos sobre os quais o evento atuar�.
	 *
	 * <p>Seta os elementos sobre os quais o evento atuar�.</p>
	 * @access public
	 * @param string $psElements Elementos sobre os quais o evento atuar�
	 */
	public function setAttrElements($psElements) {
		$this->csElements = $psElements;
	}

	/**
	 * Seta os par�metros necess�rios a alguns eventos.
	 *
	 * <p>Seta os par�metros necess�rios a alguns eventos.</p>
	 * @access public
	 * @param string $psParameters Par�metros necess�rios a alguns eventos
	 */
	public function setAttrParameters($psParameters) {
		$this->csParameters = $psParameters;
	}

	/**
	 * Retorna o nome do evento.
	 *
	 * <p>Retorna o nome do evento.</p>
	 * @access public
	 * @return string Nome do evento
	 */
	public function getAttrName() {
		$FWDXmlLoad = FWDXmlLoad::getInstance();
		$msReturn = "";
		if($this->csName)
		$msReturn = $this->csName;
		else
		$msReturn = $this->csFunction;
		return $msReturn;
	}

	/**
	 * Retorna a fonte do evento.
	 *
	 * <p>Retorna a fonte do evento.</p>
	 * @access public
	 * @return string Fonte do evento
	 */
	public function getAttrSource() {
		return $this->csSource;
	}

	/**
	 * Retorna o alvo do evento.
	 *
	 * <p>Retorna o alvo do evento.</p>
	 * @access public
	 * @return string Alvo do evento
	 */
	public function getAttrEvent() {
		return $this->csEvent;
	}

	/**
	 * Retorna a fun��o do evento.
	 *
	 * <p>Retorna a fun��o do evento.</p>
	 * @access public
	 * @return string Fun��o do evento (pode ser uma constante num�rica)
	 */
	public function getAttrFunction() {
		return $this->csFunction;
	}

	/**
	 * Retorna os elementos do evento.
	 *
	 * <p>Retorna os elementos do evento.</p>
	 * @access public
	 * @return string Elementos do evento
	 */
	public function getAttrElements() {
		return $this->csElements;
	}

	/**
	 * Retorna os par�metros do evento
	 *
	 * <p>Retorna os par�metros do evento.</p>
	 * @access public
	 * @return string Par�metros necess�rios a alguns eventos
	 */
	public function getAttrParameters() {
		return $this->csParameters;
	}

	/**
	 * Traduz a fun��o do evento de string para sua respectiva constante.
	 *
	 * <p>Traduz a string referente a fun��o do evento passada como par�metro
	 * em sua respectiva constante. **Caso a string passada como par�metro n�o
	 * se encaixe em nenhuma op��o do switch, a pr�pria string � retornada.**</p>
	 * @access public
	 * @param string $psString Fun��o do evento (string)
	 * @return integer Fun��o do evento (pode ser uma string)
	 */
	public function translateStringToConstant($psString) {
		$msValue = strtolower($psString);
		switch($msValue){
			case "erasefield":
				return JS_ERASE;
			case "colorbackground":
				return JS_COLOR_BACKGROUND;
			case "colorfont":
				return JS_COLOR_FONT;
			case "checkvalue":
				return JS_CHECK_VALUE;
			case "resize":
				return JS_RESIZE;
			case "setcontent":
				return JS_SET_CONTENT;
			case "show":
				return JS_SHOW;
			case "hide":
				return JS_HIDE;
			case "showmessage":
				return JS_SHOW;
			case "hidemessage":
				return JS_SHOW;
			case "createelem":
				return JS_CREATE_ELEM;
			case "exampletemp":
				return JS_EXAMPLE_TEMP;
			case "showmenu":
				return JS_SHOW_MENU;
			case "hidemenu":
				return JS_HIDE_MENU;
			case "reposition":
				return JS_REPOSITION;
			case "changecursor":
				return JS_CHANGE_CURSOR;
			case "movedor":
				return JS_MOVEDOR;
			case "changetab":
				return JS_CHANGE_TAB;
			case "pageload":
				return JS_PAGE_LOAD;
			case "showloading":
				return JS_SHOW_LOADING;
				// tempor�rio
			case "resizecol":
				return JS_RESIZE_COL;
			case "activatetab":
				return JS_ACTIVATE_TAB;
			case "open":
				return JS_OPEN;
			case "tooltip":
				return JS_TOOLTIP;
			case "submit":
				return JS_SUBMIT;
			case "inputhiddengrid":
				return JS_INPUT_HIDDEN_VALUE;
			case "menuactiontarget":
				return JS_MENU_ACTION_TARGET;
			case "changeclass":
				return JS_CHANGE_CLASS;
			case "inverttabitemclass":
				return JS_INVERT_TABITEM_CLASS;
			case "gridcorrectselectedrows":
				return JS_GRID_CORRECT_SELECTED_ROWS;
			case "elementincontroller":
				return JS_ELEMENT_IN_CONTROLLER;
			case "haspopupblocker":
				return JS_HAS_POPUP_BLOCKER;
			case "clearselect":
				return JS_CLEAR_SELECT;
			case "populateselect":
				return JS_POPULATE_SELECT;
			case "haschanged":
				return JS_HAS_CHANGED;
			case "replace":
				return JS_REPLACE;
			case "download":
				return JS_DOWNLOAD;
			default:
				return $psString;
		}
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDReportWriter.
 *
 * <p>Classe abstrata que define um objeto capaz de gerar um relat�rio.</p>
 * @package FWD5
 * @subpackage report
 */
abstract class FWDReportWriter {

	/**
	 * Desenha uma linha do relat�rio
	 *
	 * <p>Desenha uma linha do relat�rio (representada por um objeto
	 * FWDReportLine).</p>
	 * @param FWDReportLine $poLine Linha a ser desenhada
	 * @param array $paOffsets Array de offsets dos n�veis superiores
	 * @access public
	 */
	abstract public function drawLine(FWDReportLine $poLine, $paOffsets);

	/**
	 * Gera o relat�rio.
	 *
	 * <p>M�todo para gerar o relat�rio.</p>
	 * @access public
	 */
	abstract public function generateReport();

}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe para iteragir com um n�vel do relat�rio.
 *
 * <p>Classe para iteragir com um n�vel do relat�rio.</p>
 * @package FWD5
 * @subpackage report
 */
abstract class FWDReportLevelIterator {

	/**
	 * @var string Nome do campo respons�vel pela itera��o
	 * @access protected
	 */
	protected $csFieldIterator = "";

	/**
	 * @var string �ltimo valor do iterador
	 * @access protected
	 */
	protected $csLastValue = "";

	/**
	 * @var integer N�mero de que foram percorridos
	 * @access protected
	 */
	protected $ciElements = 0;

	/**
	 * Contrutor.
	 *
	 * <p>Construtor da classe FWDReportLevelIterator.</p>
	 * @access public
	 * @param string psFieldIterator Nome do campo respons�vel pela itera��o (opcional)
	 */
	public function __construct($psFieldIterator = ""){
		$this->csFieldIterator = $psFieldIterator;
	}

	/**
	 * Incrementa o n�mero de elementos percorridos at� o momento.
	 *
	 * <p>M�todo para incrementar o n�mero de elementos percorridos at� o momento.</p>
	 * @access public
	 */
	public function incElements(){
		$this->ciElements++;
	}

	/**
	 * Retorna o n�mero de elementos percorridos at� o momento.
	 *
	 * <p>M�todo para retornar o n�mero de elementos percorridos at� o momento.</p>
	 * @access public
	 */
	public function getElements(){
		return $this->ciElements;
	}

	/**
	 * Avan�a o iterador.
	 *
	 * <p>M�todo para avan�ar o iterador.</p>
	 * @access public
	 */
	public function advance(FWDDBDataSet $poDataSet){
		$moField = $poDataSet->getFieldByAlias($this->csFieldIterator);
		if(!$moField) trigger_error("Field not set", E_USER_WARNING);
		$this->csLastValue = $moField->getValue();
	}

	/**
	 * M�todo abstrato que busca os valores referentes ao n�vel.
	 *
	 * <p>M�todo abstrato para buscar os valores referentes ao n�vel.</p>
	 * @access public
	 */
	abstract public function fetch(FWDDBDataSet $poDataSet);

	/**
	 * M�todo chamado antes de desenhar uma linha do n�vel
	 *
	 * <p>M�todo chamado antes de desenhar uma linha do n�vel. Sobrescreva-a caso
	 * seja necess�rio algum pr�-processamento antes de desenhar o n�vel.</p>
	 * @access public
	 */
	public function prepare(FWDDBDataSet $poDataSet){}

	/**
	 * Verifica se mudou o n�vel.
	 *
	 * <p>M�todo para verificar se houve mudan�a de n�vel.</p>
	 * @access public
	 * @return boolean Verdadeiro ou falso
	 */
	public function changedLevel(FWDDBDataSet $poDataSet){
		$moField = $poDataSet->getFieldByAlias($this->csFieldIterator);
		if($moField){
			return ($moField->getValue() != $this->csLastValue);
		}else{
			trigger_error("Field '{$this->csFieldIterator}' not set", E_USER_WARNING);
			return false;
		}
	}

	/**
	 * Reseta o iterador.
	 *
	 * <p>M�todo para resetar o iterador.</p>
	 * @access public
	 */
	public function clean(){
		$this->csLastValue = "";
	}

	/**
	 * Verifica se o iterador j� possui algum valor.
	 *
	 * <p>M�todo para verificar se o iterador j� possui algum valor. �til
	 * para verificar se � a primeira itera��o ou n�o.</p>
	 * @access public
	 * @return boolean Verdadeiro ou falso
	 */
	public function hasValue(){
		return $this->csLastValue ? true : false;
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe abstrata que define uma c�lula de uma linha de um relat�rio
 *
 * <p>Classe abstrata que define uma c�lula de uma linha de um relat�rio.</p>
 * @package FWD5
 * @subpackage report
 */
abstract class FWDReportCell {

	/**
	 * Nome da c�lula
	 * @var string
	 * @access protected
	 */
	protected $csName = "";

	/**
	 * Largura da c�lula
	 * @var integer
	 * @access protected
	 */
	protected $ciWidth = 100;

	/**
	 * Largura da margem esquerda da c�lula (na verdade, � um padding com nome de margem para manter o padr�o da FWD)
	 * @var integer
	 * @access protected
	 */
	protected $ciMarginLeft = 0;

	/**
	 * Largura da margem direita da c�lula (na verdade, � um padding com nome de margem para manter o padr�o da FWD)
	 * @var integer
	 * @access protected
	 */
	protected $ciMarginRight = 0;

	/**
	 * Retorna o valor do atributo name
	 *
	 * <p>Retorna o valor do atributo name.</p>
	 * @access public
	 * @return string Valor do atributo name
	 */
	public function getAttrName(){
		return $this->csName;
	}

	/**
	 * Seta o valor do atributo name
	 *
	 * <p>Seta o valor do atributo name.</p>
	 * @access public
	 * @param string $psName Novo valor do atributo
	 */
	public function setAttrName($psName){
		$this->csName = $psName;
	}

	/**
	 * Retorna o valor do atributo width
	 *
	 * <p>Retorna o valor do atributo width.</p>
	 * @access public
	 * @return integer Valor do atributo width
	 */
	public function getAttrWidth(){
		return $this->ciWidth;
	}

	/**
	 * Seta o valor do atributo width
	 *
	 * <p>Seta o valor do atributo width.</p>
	 * @access public
	 * @param integer $piWidth Novo valor do atributo
	 */
	public function setAttrWidth($piWidth){
		$this->ciWidth = $piWidth;
	}

	/**
	 * Retorna o valor do atributo marginleft
	 *
	 * <p>Retorna o valor do atributo marginleft.</p>
	 * @access public
	 * @return integer Valor do atributo marginleft
	 */
	public function getAttrMarginLeft(){
		return $this->ciMarginLeft;
	}

	/**
	 * Seta o valor do atributo marginleft
	 *
	 * <p>Seta o valor do atributo marginleft.</p>
	 * @access public
	 * @param integer $piMarginLeft Novo valor do atributo
	 */
	public function setAttrMarginLeft($piMarginLeft){
		$this->ciMarginLeft = $piMarginLeft;
	}

	/**
	 * Retorna o valor do atributo marginright
	 *
	 * <p>Retorna o valor do atributo marginright.</p>
	 * @access public
	 * @return integer Valor do atributo marginright
	 */
	public function getAttrMarginRight(){
		return $this->ciMarginRight;
	}

	/**
	 * Seta o valor do atributo marginright
	 *
	 * <p>Seta o valor do atributo marginright.</p>
	 * @access public
	 * @param integer $piMarginRight Novo valor do atributo
	 */
	public function setAttrMarginRight($piMarginRight){
		$this->ciMarginRight = $piMarginRight;
	}

}

?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe abstrata que representa um objeto do word.
 *
 * <p>Classe abstrata que representa um objeto que possua valor de um documento word.</p>
 *
 * @package FWD5
 * @subpackage formats-word
 */
abstract class FWDWordObject {}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe abstrata FWDXMLParser. Classe base para parsers de XML.
 *
 * <p>Classe base para parsers de XML.</p>
 *
 * @package FWD5
 * @subpackage translation
 */
abstract class FWDXMLParser {

	/**
	 * Parser retornado pela fun��o xml_parser_create
	 * @var resource
	 * @access protected
	 */
	protected $crParser;

	/**
	 * Inicializa o parser para parsear um arquivo
	 *
	 * <p>Inicializa o parser para parsear um arquivo.</p>
	 * @access protected
	 */
	protected function setupParser(){
		$this->crParser = xml_parser_create('iso-8859-1');//no XML funciona sem o iso
		xml_set_object($this->crParser, $this);
		xml_set_element_handler($this->crParser,'startHandler','endHandler');
		xml_set_character_data_handler($this->crParser,'dataHandler');
	}

	/**
	 * Libera o parser ao terminar de parsear um arquivo
	 *
	 * <p>Libera o parser ao terminar de parsear um arquivo.</p>
	 * @access protected
	 */
	protected function freeParser(){
		xml_parser_free($this->crParser);
	}

	/**
	 * Parseia um arquivo XML
	 *
	 * <p>Parseia um arquivo XML.</p>
	 * @access public
	 * @param string $psFileName Arquivo XML a ser parseado
	 * @param boolean $pbShowErrors Indica que, se ocorrerem erros de parse, eles devem ser exibidos
	 * @return boolean Indica se o arquivo foi parseado com sucesso
	 */
	public function parseFile($psFileName,$pbShowErrors=true){
		if(!file_exists($psFileName) || !is_readable($psFileName)){
			trigger_error("Could not open file '$psFileName'.",E_USER_WARNING);
			return false;
		}else{
			$mrFile = fopen($psFileName,'r');
		}
		$this->setupParser();
		while($msData = fread($mrFile,512)){
			$mbParse = xml_parse($this->crParser,$msData,feof($mrFile));
			if(!$mbParse){
				if($pbShowErrors){
					$msErrorMsg = sprintf(
            "XML error: %s in file $psFileName at line %d",
					xml_error_string(xml_get_error_code($this->crParser)),
					xml_get_current_line_number($this->crParser)
					);
					trigger_error($msErrorMsg,E_USER_WARNING);
				}
				$this->csFileBeingParsed = '';
				$this->freeParser();
				return false;
			}
		}
		fclose($mrFile);
		$this->freeParser();
		return true;
	}

	/**
	 * Handler chamado quando o parser encontra uma tag de in�cio de elemento
	 *
	 * <p>Handler chamado quando o parser encontra uma tag de in�cio de elemento.</p>
	 * @access protected
	 * @param resource $prParser Parser
	 * @param string $psTagName Nome da tag
	 * @param array $paAttributes Array associativo contendo os atributos do elemento
	 */
	abstract protected function startHandler($prParser, $psTagName, $paAttributes);

	/**
	 * Handler chamado quando o parser encontra texto dentro de um elemento
	 *
	 * <p>Handler chamado quando o parser encontra texto dentro de um elemento.</p>
	 * @access protected
	 * @param resource $prParser Parser
	 * @param string $psData Conte�do de texto do elemento
	 */
	abstract protected function dataHandler($prParser, $psData);

	/**
	 * Handler chamado quando o parser encontra uma tag de fim de elemento
	 *
	 * <p>Handler chamado quando o parser encontra uma tag de fim de elemento.</p>
	 * @access protected
	 * @param resource $prParser Parser
	 * @param string $psTagName Nome da tag
	 */
	abstract protected function endHandler($prParser, $psTagName);

}

?>