<?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe para acesso ao banco de dados.
 *
 * <p>Classe para acesso ao banco de dados. Utiliza a biblioteca ADODB.</p>
 * @package FWD5
 * @subpackage db
 */
class FWDDB implements FWDIDB {

	/**
	 * Nome do host
	 * @var string
	 * @access protected
	 */
	protected $csHost = "";

	/**
	 * Nome do usu�rio
	 * @var string
	 * @access protected
	 */
	protected $csUser = "";

	/**
	 * Senha do usu�rio
	 * @var string
	 * @access protected
	 */
	protected $csPassword = "";

	/**
	 * Nome da base de dados
	 * @var string
	 * @access protected
	 */
	protected $csDatabase = "";

	/**
	 * Tipo do banco de dados
	 * @var string
	 * @access protected
	 */
	protected $csType = DB_NONE;

	/**
	 * Objeto de conex�o do banco
	 * @var object
	 * @access private
	 */
	private $coConnection = null;

	/**
	 * Objeto que representa o conjunto de registros retornados em um SELECT
	 * @var object
	 * @access private
	 */
	private $coRecordSet = null;

	/**
	 * Contrutor.
	 *
	 * <p>Construtor da classe FWDDB.</p>
	 * @access public
	 * @param string $psType Identificador do BD {DB_NONE | DB_POSTGRES | DB_ORACLE | DB_MSSQL}
	 * @param string $psDatabase Nome da base de dados
	 * @param string $psUser Nome do usu�rio
	 * @param string $psPassword Senha do usu�rio
	 * @param string $psHost Nome do host
	 */
	public function __construct($psType, $psDatabase, $psUser, $psPassword, $psHost) {
		$this->csType 		= $psType;
		$this->csDatabase 	= $psDatabase;
		$this->csUser 	 	= $psUser;
		$this->csPassword 	= $psPassword;
		$this->csHost	 	= $psHost;
		$this->coConnection = ADONewConnection($this->csType);
	}

	/**
	 * Retorna o nome do usu�rio do banco.
	 *
	 * <p>M�todo para retornar o nome do usu�rio do banco.</p>
	 * @access public
	 * @return string Login do usu�rio do banco
	 */
	public function getUser() {
		return $this->csUser;
	}

	/**
	 * Clona o objeto de conex�o com o banco de dados.
	 *
	 * <p>M�todo para clonar o objeto de conex�o com o banco de dados.</p>
	 * @access public
	 * @return FWDDB Objeto de conex�o clonado
	 */
	public function __clone() {
		$moDB = new FWDDB($this->csType, $this->csDatabase, $this->csUser, $this->csPassword, $this->csHost);
		return $moDB;
	}

	/**
	 * Conecta no banco.
	 *
	 * <p>M�todo para conectar no banco.</p>
	 * @access public
	 * @return boolean Verdadeiro ou Falso
	 */
	public function connect() {
		return $this->coConnection->Connect($this->csHost,$this->csUser,$this->csPassword,$this->csDatabase);
	}

	/**
	 * Verifica se est� conectado ao banco.
	 *
	 * <p>M�todo para verificar se est� conectado no banco.</p>
	 * @access public
	 * @return boolean Verdadeiro ou Falso
	 */
	public function isConnected() {
		return $this->coConnection->IsConnected();
	}

	/**
	 * Fecha conex�o com o banco de dados.
	 *
	 * <p>M�todo para fechar conex�o com o banco de dados.</p>
	 * @access public
	 */
	public function disconnect() {
		$this->coConnection->Close();
	}

	/**
	 * Executa um sql no banco.
	 *
	 * <p>M�todo para executar um sql no banco.</p>
	 * @access public
	 * @param string $psQuery Query que vai ser executada
	 * @param string $piLimit N�mero de registros que deve ser retornado
	 * @param string $psOffset A partir de qual registro deve come�ar a retornar
	 * @param boolean $skipInjection Se for true pula a verifica��o de injection
	 * @return boolean Verdadeiro (sucesso) ou Falso (falha)
	 */
	public function execute($psQuery, $piLimit = 0, $piOffset = 0, $skipInjection = false) {
		if (FWDWebLib::getInstance()->getDebugType() & FWD_DEBUG_DB) {
			$this->debugMode(true);
		}
		unset($this->coRecordSet);
		if (!$skipInjection && !$this->checkInjection($psQuery)) {
			trigger_error("injection technique" . "\nQuery: " . $psQuery, E_USER_ERROR);
			return false;
		}
		if($piLimit) $this->coRecordSet = $this->coConnection->SelectLimit($psQuery, $piLimit, $piOffset);
		else $this->coRecordSet = $this->coConnection->Execute($psQuery);
		if(!$this->coRecordSet) trigger_error($this->coConnection->ErrorMsg() . "\nQuery: " . $psQuery, E_USER_ERROR);
		else return true;
		return false;
	}
	 
	/**
	 * <p>M�todo para garantir que n�o h� SQL Injection.</p>
	 * @access public
	 * @param $psQuery
	 * @return boolean Verdadeiro (tudo ok) ou Falso (h� injection)
	 */
	 
	public function checkInjection($psQuery){
		$psQuery = utf8_decode($psQuery);
		$msString = $this->stripStrings($psQuery);
		if(preg_match("/((;\s*\S+)|(')|(-{2,}))/i", $msString)){
			$moMatchArray1 = Array();
			$moMatchArray1 = preg_match_all("/(;\s*\S+|'|--)/s", $msString, $bla, PREG_OFFSET_CAPTURE);
			return false;
		}
		return true;
	}
	 
	/**
	 * <p>M�todo que retira strings de dentro de uma query string</p>
	 * @param $psString
	 * @return string sem as strings internas
	 */
	public function stripStrings($psString){
		$str1 = preg_replace("/\\\'/s", '', $psString);
		return preg_replace("/\'.*\'/sU", '', $str1);

	}

	/**
	 * Retorna o n�mero de campos retornados por uma consulta.
	 *
	 * <p>M�todo para retornar o n�mero de campos retornados por uma consulta.</p>
	 * @access public
	 * @return integer N�mero de campos
	 */
	public function getFieldCount() {
		if ($this->coRecordSet) return $this->coRecordSet->FieldCount();
		else trigger_error("RecordSet not initialized yet", E_USER_WARNING);
	}

	/**
	 * Gera SQL para INSERT automaticamente.
	 *
	 * <p>M�todo para gerar o SQL para INSERT automaticamente.</p>
	 * @access public
	 * @param string $psTable Nome da tabela
	 * @param array $paFieldsInfo Array com as informa��es dos campos
	 * @return boolean Verdadeiro ou Falso
	 */
	public function autoInsert($psTable, $paFieldsInfo) {
		$msInsert = '';
		$msFieldNames = "";
		$msFieldValues = "";
		$miI = 0;
		$miCount = count($paFieldsInfo);
		if ($this->csType == DB_ORACLE) {
			$msInsert = "BEGIN EXECUTE IMMEDIATE 'INSERT INTO " . $psTable . " (";
			$maValues = array();
			foreach ($paFieldsInfo as $maFieldInfo) {
				$msFieldNames .= ($miI != ($miCount-1)) ? $maFieldInfo['name'] . ", " : $maFieldInfo['name'];
				$maValues[] = FWDDBType::translate($maFieldInfo['value'], $maFieldInfo['type']);
				$msFieldValues .= ($miI != ($miCount-1)) ? ':'.$miI . ", " : ':'.$miI;
				$miI++;
			}
			$msInsert .= $msFieldNames . ") VALUES (" . $msFieldValues . ")' USING " . implode(',', $maValues) . "; END;";
		}
		else {
			$msInsert = "INSERT INTO " . $psTable . " (";
			foreach ($paFieldsInfo as $maFieldInfo) {
				$msFieldNames .= ($miI != ($miCount-1)) ? $maFieldInfo['name'] . ", " : $maFieldInfo['name'];
				$msFieldValue = FWDDBType::translate($maFieldInfo['value'], $maFieldInfo['type']);
				$msFieldValues .= ($miI != ($miCount-1)) ? $msFieldValue . ", " : $msFieldValue;
				$miI++;
			}
			$msInsert .= $msFieldNames . ") VALUES (" . $msFieldValues . ")";
		}
		return $this->execute($msInsert);
	}

	/**
	 * Gera SQL para UPDATE automaticamente.
	 *
	 * <p>M�todo para gerar o SQL para UPDATE automaticamente.</p>
	 * @access public
	 * @param string $psTable Nome da tabela
	 * @param array $paFieldsInfo Array com as informa��es dos campos
	 * @param string $psWhere Cl�usula "where" para restringir o UPDATE
	 * @return boolean Verdadeiro ou Falso
	 */
	public function autoUpdate($psTable, $paFieldsInfo, $psWhere) {
		$msUpdate = '';
		$miI = 0;
		$miCount = count($paFieldsInfo);
		$msNewValues = "";
		if ($this->csType == DB_ORACLE) {
			$msUpdate = "BEGIN EXECUTE IMMEDIATE 'UPDATE " . $psTable . " SET ";
			$maValues = array();
			foreach ($paFieldsInfo as $maFieldInfo) {
				$maValues[] = FWDDBType::translate($maFieldInfo['value'], $maFieldInfo['type']);
				$msNewValues .= $maFieldInfo['name'] . " = " . ':'.$miI;
				$msNewValues .= ($miI != ($miCount-1)) ? ", " : "";
				$miI++;
			}
			$msUpdate .= $msNewValues . " WHERE " . str_replace("'", "''", $psWhere) . "' USING " . implode(',', $maValues) . "; END;";
		}
		else {
			$msUpdate = "UPDATE " . $psTable . " SET ";
			foreach ($paFieldsInfo as $maFieldInfo) {
				$msNewValues .= $maFieldInfo['name'] . " = " . FWDDBType::translate($maFieldInfo['value'], $maFieldInfo['type']);
				$msNewValues .= ($miI != ($miCount-1)) ? ", " : "";
				$miI++;
			}
			$msUpdate .= $msNewValues . " WHERE " . $psWhere;
		}
		return $this->execute($msUpdate);
	}

	/**
	 * Gera DELETE automaticamente.
	 *
	 * <p>M�todo para gerar o SQL DELETE automaticamente.</p>
	 * @access public
	 * @param string $psTable Nome da tabela
	 * @param string $psWhere Cl�usula "where" para restringir o UPDATE
	 * @return boolean Verdadeiro ou Falso
	 */
	public function autoDelete($psTable, $psWhere) {
		if (!$psWhere)
		trigger_error("Invalid parameter: autoDelete method must have a \"where\" clause (for security reasons)", E_USER_ERROR);
		$msDelete = "DELETE FROM " . $psTable . " WHERE " . $psWhere;
		return $this->execute($msDelete);
	}

	/**
	 * Atualiza um CLOB no bnco de dados.
	 *
	 * <p>M�todo para atualizar um clob no banco de dados.</p>
	 * @access public
	 * @param string $psTable Nome da tabela
	 * @param string $psCollumn Nome da coluna
	 * @param string $psValue Valor do clob
	 * @param string $psWhere Cl�usula where
	 */
	public function updateClob($psTable, $psCollumn, $psValue, $psWhere) {
		if ($this->csType == DB_ORACLE) $this->coConnection->UpdateClob($psTable, $psCollumn, $psValue, $psWhere);
		else trigger_error('The method updateClob() is only for oracle!', E_USER_WARNING);
	}

	/**
	 * Seta o modo de debug.
	 *
	 * <p>M�todo para setar o m�todo de debug.</p>
	 * @access public
	 * @param boolean $pbDebug Verdadeiro ou Falso
	 */
	public function debugMode($pbDebug) {
		$this->coConnection->debug = $pbDebug;
		$this->coConnection->setDebugFilePath(FWDWebLib::getInstance()->getSysRef().FWDWebLib::getDebugFilePath());
	}

	/**
	 * Retorna a �ltima mensagem de erro.
	 *
	 * <p>M�todo para retornar a �ltima mensagem de erro.</p>
	 * @access public
	 * @return string Mensagem de erro
	 */
	public function getErrorMessage() {
		return $this->coConnection->ErrorMsg();
	}

	/**
	 * Libera da mem�ria o resultado da consulta.
	 *
	 * <p>M�todo para liberar da mem�ria o resultado da consulta.</p>
	 * @access public
	 */
	public function freeResult() {
		$this->coRecordSet->Close();
	}

	/**
	 * Retorna o n�mero de registros retornado em um SELECT.
	 *
	 * <p>M�todo para retornar o n�mero de registros retornado em um SELECT.</p>
	 * @access public
	 * @return integer N�mero de registros
	 */
	public function getRowCount() {
		return $this->coRecordSet->RowCount();
	}

	/**
	 * Retorna o n�mero de registros afetados por DELETE/UPDATE.
	 *
	 * <p>M�todo para retornar o n�mero de registros afetados por DELETE/UPDATE.</p>
	 * @access public
	 * @return integer N�mero de registros
	 */
	public function getAffectedRows() {
		return $this->coConnection->Affected_Rows();
	}

	/**
	 * Retorna o id do �ltimo registro inserido.
	 *
	 * <p>M�todo para retornar o id do �ltimo registro inserido.</p>
	 * @access public
	 * @param string $psTable Nome da tabela
	 * @param string $psKey Nome da coluna chave prim�ria
	 * @return integer Id do �ltimo registro inserido
	 */
	public function getLastId($psTable = "", $psKey = "") {
		if ($this->csType != DB_ORACLE) return $this->coConnection->Insert_ID($psTable, $psKey);
		else {
			$this->execute("SELECT S_" . $psTable . ".CURRVAL as pk FROM dual");
			$maPK = $this->fetch();
			return $maPK["PK"];
		}
	}

	/**
	 * Retorna um registro do resultado.
	 *
	 * <p>M�todo para retornar um registro do resultado.</p>
	 * @access public
	 * @return array Array contendo a informa��o ou falso caso atinja o fim
	 */
	public function fetch() {
		if (!$this->coRecordSet->EOF) {
			$maData = $this->coRecordSet->GetRowAssoc(2);
			$this->coRecordSet->MoveNext();
			return $maData;
		} else return false;
	}

	/**
	 * Retorna um array com todos os registros retornados pelo consulta.
	 *
	 * <p>M�todo para retornar um array com todos os registros retornados pelo consulta.</p>
	 * @access public
	 * @return array Array contendo todos os registros
	 */
	public function fetchAll() {
		return $this->coRecordSet->GetAll();
	}

	/**
	 * Inicia uma transa��o.
	 *
	 * <p>M�todo para iniciar uma transa��o.</p>
	 * @access public
	 */
	public function startTransaction() {
		$this->coConnection->StartTrans();
	}

	/**
	 * Completa uma transa��o.
	 *
	 * <p>M�todo para completar uma transa��o.</p>
	 * @access public
	 * @return boolean Verdadeiro se obtiver sucesso, falso caso contr�rio
	 */
	public function completeTransaction() {
		$this->coConnection->CompleteTrans();
	}

	/**
	 * For�a a falha de uma transa��o.
	 *
	 * <p>M�todo para for�ar a falha em um transa��o.</p>
	 * @access public
	 */
	public function rollbackTransaction() {
		$this->coConnection->FailTrans();
	}

	/**
	 * Retorna a data no formato do banco.
	 *
	 * <p>M�todo para retornar a data no formato aceito pelo banco.</p>
	 * @access public
	 * @param integer $piTimestamp Timestamp
	 * @return string Data
	 */
	public function timestampFormat($piTimestamp) {
		return $this->coConnection->DBTimeStamp($piTimestamp);
	}

	/**
	 * Retorna um timestamp baseado em uma data no formato YYYY-MM-DD HH-NN-SS.
	 *
	 * <p>M�todo para retornar um timestamp baseado em uma data no formato YYYY-MM-DD HH-NN-SS.</p>
	 * @access public
	 * @param integer $psDate Data
	 * @return integer Timestamp
	 */
	public function getTimestamp($psDate) {
		return $this->coConnection->UnixTimestamp($psDate);
	}

	/**
	 * Retorna o tipo do banco de dados
	 *
	 * <p>Retorna o tipo do banco de dados.</p>
	 * @access public
	 * @return string Tipo do banco de dados
	 */
	public function getDatabaseType(){
		return $this->csType;
	}

	/**
	 * Retorna o nome da base de dados.
	 *
	 * <p>M�todo para retornar o nome da base de dados.</p>
	 * @access public
	 * @return string Nome da base de dados
	 */
	public function getDatabaseName() {
		return $this->csDatabase;
	}

	/**
	 * Retorna o host da base de dados.
	 *
	 * <p>M�todo para retornar o host da base de dados.</p>
	 * @access public
	 * @return string Host da base de dados
	 */
	public function getHost() {
		return $this->csHost;
	}

	/**
	 * Retorna o Usu�rio que deve ser utilizado como prefixo das functions do banco
	 *
	 * <p>Usu�rio que deve ser utilizado como prefixo das functions do banco</p>
	 * @access public
	 * @return string Usu�rio da conex�o com o banco que deve ser utilizado antes do nome das functions
	 */
	public function getFunctionUserDB(){
		$msUserDB = "";
		switch($this->getDatabaseType()){
			case DB_POSTGRES:
			case DB_ORACLE:
				$msUserDB = '';
				break;
			default:
				$msUserDB = $this->getUser() . ".";
				break;
		}
		return $msUserDB;
	}

	/**
	 * Retorna a chamada da fun��o de acordo com o banco de dados.
	 *
	 * <p>M�todo para retornar a chamada da fun��o de acordo com o banco de dados.</p>
	 * @access public
	 * @param string $psFunction Fun��o que deve ser chamada
	 * @return string Chamada da fun��o
	 */
	public function getFunctionCall($psFunction){
		$msFunctionCall = "";
		switch($this->getDatabaseType()){
			case DB_ORACLE:
				$msFunctionCall = 'TABLE(' . $this->getFunctionUserDB() . $psFunction . ')';
				break;
			default:
				$msFunctionCall = $this->getFunctionUserDB() . $psFunction;
				break;
		}
		return $msFunctionCall;
	}

	/**
	 * Retorna um valor constante do banco de dados.
	 *
	 * <p>M�todo para retornar um valor constante do banco de dados. Necess�rio
	 * pois no Oracle n�o s�o aceitas consultas no formato 'select 1' e sim
	 * no formato 'select 1 from dual'.</p>
	 * @access public
	 * @param string $psValue Valores [Ex: '1 as first, 2 as second, ...]
	 * @return string Select para retornar uma constante
	 */
	public function selectConstant($psValues) {
		$msSelect = '';
		switch($this->getDatabaseType()){
			case DB_ORACLE:
				$msSelect = 'SELECT ' . $psValues . ' FROM DUAL';
				break;
			default:
				$msSelect = 'SELECT ' . $psValues;
				break;
		}
		return $msSelect;
	}

	/**
	 * Retorna uma express�o de "like" case insensitive entre uma coluna e uma string constante.
	 *
	 * <p>Retorna uma express�o de "like" case insensitive entre uma coluna e uma string constante.</p>
	 * @access public
	 * @param string $psColumn Nome/express�o da coluna
	 * @param string $psString A string constante
	 * @return string Uma string contendo a express�o
	 */
	public function getCaseInsensitiveLike($psColumn,$psString){
		if($this->csType==DB_MSSQL){
			return "{$psColumn} LIKE '%{$psString}%'";
		}else{
			//return "UPPER({$psColumn}) LIKE '%".strtoupper($psString)."%'";
			return "UPPER({$psColumn}) LIKE UPPER('%".$psString."%')";
		}
	}

	/**
	 * Retorna a string escapada de acordo com o banco.
	 *
	 * <p>M�todo para retornar a string escapada de acordo com o banco.</p>
	 * @access public
	 * @param string $psString String
	 * @return string String escapada
	 */
	public function quote($psString) {
		return $this->coConnection->Quote($psString);
	}

	/**
	 * Retorna as strings concatenadas.
	 *
	 * <p>M�todo para retornar as strings concatenadas.</p>
	 * @access public
	 * @param string $psString1 String
	 * @param string $psString2 String
	 * @return string Par�metros concatenados
	 */
	public function concat($psString1, $psString2) {
		return $this->coConnection->concat($psString1, $psString2);
	}

	/*************************************ADODB ONLY****************************************/

	/**
	 * Loga as consultas feitas no banco.
	 *
	 * <p>M�todo para logar as consultas feitas no banco.</p>
	 * @access public
	 * @param boolean $pbLog True ou False
	 */
	public function logSQL($pbLog) {
		$this->coConnection->LogSQL($pbLog);
	}

	/**
	 * Mostra os resultados do log.
	 *
	 * <p>M�todo para mostrar os resultados do log.</p>
	 * @access public
	 */
	public function showLog() {
		$perf = NewPerfMonitor($this->coConnection);
		echo $perf->SuspiciousSQL();
		echo $perf->ExpensiveSQL();
		echo $perf->InvalidSQL();
	}

}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe AMSTable. Manipula informa��es b�sicas de uma tabela qualquer.
 *
 * <p>Classe abstrata que manipula informa��es b�sicas de uma tabela qualquer.</p>
 * @package FWD5
 * @subpackage db
 */
abstract class FWDDBTable {

	/**
	 * Alias da chave prim�ria da tabela
	 * @var string
	 * @access protected
	 */
	protected $csAliasId = "";

	/**
	 * Objeto para conex�o com o banco de dados
	 * @var FWDDBDataset
	 * @access protected
	 */
	protected $coDataset = null;

	/**
	 * Array com os filtros da tabela
	 * @var array
	 * @access private
	 */
	private $caFilters = array();

	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDDBTable.</p>
	 * @access public
	 * @param string $psTable Nome da tabela
	 */
	public function __construct($psTable){
		$this->coDataset = new FWDDBDataset(FWDWebLib::getConnection(), $psTable);
	}

	/**
	 * Retorna o nome da tabela.
	 *
	 * <p>M�todo para retornar o nome da tabela.</p>
	 * @access public
	 * @return string Nome da tabela
	 */
	public function getTable() {
		return $this->coDataset->getTable();
	}

	/**
	 * Retorna o nome de um campo.
	 *
	 * <p>M�todo para retornar o nome de um campo.</p>
	 * @access public
	 * @param string $psFieldAlias Alias do campo
	 * @return string Nome do campo
	 */
	public function getFieldName($psFieldAlias) {
		return $this->coDataset->getFieldByAlias($psFieldAlias)->getName();
	}

	/**
	 * Seta o valor de uma campo da tabela.
	 *
	 * <p>M�todo para setar o valor de um campo da tabela.</p>
	 * @access public
	 * @param string $psFieldAlias Alias do campo
	 * @param string $psValue Valor do campo
	 * @param string $pbDecode Decodificar o campo antes de inserir no banco?
	 */
	public function setFieldValue($psFieldAlias, $psValue, $pbDecode = true) {
		$moField = $this->coDataset->getFieldByAlias($psFieldAlias);
		if($moField) $moField->setValue($psValue, true, $pbDecode);
		else trigger_error("There is no field with the alias \"{$psFieldAlias}\"", E_USER_WARNING);
	}

	/**
	 * Retorna o valor de uma campo da tabela.
	 *
	 * <p>M�todo para retornar o valor de um campo da tabela.</p>
	 * @access public
	 * @return string Valor do campo
	 */
	public function getFieldValue($psFieldAlias) {
		$moField = $this->coDataset->getFieldByAlias($psFieldAlias);
		if($moField) return $moField->getValue();
		else trigger_error("There is no field with the alias \"{$psFieldAlias}\"", E_USER_WARNING);
	}

	/**
	 * Retorna o objeto FWDDBField relativo ao alias passado.
	 *
	 * <p>M�todo para retornar o objeto FWDDBField relativo ao alias passado.</p>
	 * @access public
	 * @param string $psAlias Alias do campo
	 * @return FWDDBField Campo
	 */
	public function getFieldByAlias($psAlias) {
		return $this->coDataset->getFieldByAlias($psAlias);
	}

	/**
	 * Busca um registro atrav�s da chave prim�ria da tabela.
	 *
	 * <p>M�todo para buscar um registro atrav�s da chave prim�ria da tabela.</p>
	 * @access protected
	 * @param string $psId Identificador
	 * @param string $psFieldAlias Alias do campo
	 */
	protected function fetchById($psId, $psFieldAlias) {
		if(($psId=='')||($psId==0)||($psId===false)||($psId==null))
		return false;
		else{
			$this->createFilter($psId, $psFieldAlias);
			$this->coDataset->select();
			return $this->fetch();
		}
	}

	/**
	 * Remove todos os filtros do campo.
	 *
	 * <p>Remove todos os filtros do campo.</p>
	 * @access public
	 * @param string $psAlias Alias do campo
	 */
	public function removeFilters($psAlias){
		unset($this->caFilters[$psAlias]);
		$this->coDataset->getFieldByAlias($psAlias)->clearFilters();
	}

	/**
	 * Remove todos os filtros de todos os campos.
	 *
	 * <p>Remove todos os filtros de todos os campo.</p>
	 * @access public
	 */
	public function removeAllFilters(){
		$maFields = $this->coDataset->getFields();
		unset($this->caFilters);
		foreach($maFields as $moField) $moField->clearFilters();
	}

	/**
	 * Cria um filtro.
	 *
	 * <p>M�todo para criar um filtro.</p>
	 * @access public
	 * @param string $psValue Valor
	 * @param string $psFieldAlias Alias do campo
	 * @param string $psOperator Operador
	 */
	public function createFilter($psValue, $psFieldAlias, $psOperator = "=") {
		if (isset($this->caFilters[$psFieldAlias])) {
			$this->caFilters[$psFieldAlias]->addValue($psValue);
		}
		else {
			$moFilter = new FWDDBFilter($psOperator, $psValue);
			$moField = $this->coDataset->getFieldByAlias($psFieldAlias);
			if($moField){
				$moField->addFilter($moFilter);
			}else{
				trigger_error("Unknown field alias '$psFieldAlias'.",E_USER_ERROR);
			}
			$this->caFilters[$psFieldAlias] = $moFilter;
		}
	}

	/**
	 * Busca um registro do resultado.
	 *
	 * <p>M�todo para buscar um registro do resultado.</p>
	 * @access public
	 * @return boolean Verdadeiro ou falso
	 */
	public function fetch() {
		return $this->coDataset->fetch();
	}

	/**
	 * Busca informa��es de uma tabela.
	 *
	 * <p>M�todo para buscar informa��es de uma tabela.</p>
	 * @access public
	 * @return integer N�mero de registros retornados
	 */
	public function select() {
		return $this->coDataset->select();
	}

	/**
	 * Atualiza a tabela.
	 *
	 * <p>M�todo para atualizar a tabela.</p>
	 * @access public
	 * @return integer N�mero de registros afetados pelo update ou falso em caso de falha
	 */
	public function update() {
		return $this->coDataset->update();
	}

	/**
	 * Remove um registro da tabela.
	 *
	 * <p>M�todo para remover um registro da tabela.</p>
	 * @access public
	 */
	public function delete() {
		return $this->coDataset->delete();
	}

	/**
	 * Insere um registro aa tabela.
	 *
	 * <p>M�todo para inserir um registro na tabela.</p>
	 * @param boolean $pbGetId Verdadeiro para retornar o id do registro inserido
	 * @return integer Id do �ltimo registro inserido ou falso
	 * @access public
	 */
	public function insert($pbGetId = false) {
		if ($pbGetId) {
			if (FWDWebLib::getConnection()->getDatabaseType() != DB_POSTGRES) {
				return $this->coDataset->insert(true);
			}
			else {
				/*
				 * No caso do POSTGRES � necess�rio que se especifique a chave prim�ria
				 * da tabela para que seja poss�vel retornar o id do �ltimo registro inserido.
				 */
				if ($this->csAliasId) {
					return $this->coDataset->insert($this->coDataset->getFieldByAlias($this->csAliasId)->getName());
				}
				else {
					trigger_error('Error while getting the last inserted id: $this->csAliasId not set (You must specify the primary to get the last inserted id when using POSTGRES)!', E_USER_WARNING);
					return false;
				}
			}
		}
		else return $this->coDataset->insert();
	}

	/**
	 * Executa um sql qualquer no banco.
	 *
	 * <p>M�todo para executar um sql qualquer no banco.</p>
	 * @access public
	 * @param string $psQuery SQL Query
	 * @param string $piLimit N�mero de registros que deve ser retornado
	 * @param string $psOffset A partir de qual registro deve come�ar a retornar
	 * @return boolean Verdadeiro (sucesso) ou Falso (falha)
	 */
	public function execute($psQuery = "", $piLimit = 0, $piOffset = 0) {
		$this->coDataset->setQuery($psQuery);
		return $this->coDataset->execute($piLimit, $piOffset);
	}

	/**
	 * Ordena o resultado da consulta a partir do campo especificado.
	 *
	 * <p>M�todo para ordenar o resultado da consulta a partir do campo especificado.</p>
	 * @access public
	 * @param integer $psAlias Alias do campo
	 * @param integer $psOrder Ordem crescente ("+") ou decrescente ("-")
	 */
	public function setOrderBy($psAlias, $psOrder){
		$this->coDataset->setOrderBy($psAlias,$psOrder);
	}

	/**
	 * Preenche um campo a partir de um objeto de interface.
	 *
	 * <p>Preenche um campo a partir de um objeto de interface.</p>
	 * @access public
	 * @param string $psFieldAlias Alias do campo
	 * @param string psObjectName Nome do objeto de interface
	 * @return boolean True sse o campo foi preenchido com sucesso
	 */
	public function fillFieldFromObject($psFieldAlias,$psObjectName){
		return $this->coDataset->fillFieldFromObject($psFieldAlias,$psObjectName);
	}

	/**
	 * Preenche os campos a partir dos objetos de interface.
	 *
	 * <p>Preenche os campos a partir dos objetos de interface, quando houver
	 * objetos de interface com nomes iguais aos aliases dos campos.</p>
	 * @access public
	 * @return boolean True sse todos campos foram preenchidos com sucesso
	 */
	public function fillFieldsFromForm(){
		$mbReturn = true;
		foreach($this->coDataset->getFields() as $msAlias=>$moField){
			$mbReturn&= $this->fillFieldFromObject($msAlias,$msAlias);
		}
		return $mbReturn;
	}

	/**
	 * Preenche os campos a partir de um array associativo.
	 *
	 * <p>Preenche os campos a partir de um array associativo no formato alias=>valor.</p>
	 * @access public
	 * @return boolean True sse todos campos foram preenchidos com sucesso
	 */
	public function fillFieldsFromArray($paFields){
		return $this->coDataset->fillFieldsFromArray($paFields);
	}

	/**
	 * Preenche um objeto de interface a partir de um campo da tabela.
	 *
	 * <p>Preenche um objeto de interface a partir de um campo da tabela.</p>
	 * @access public
	 * @param string psObjectName Nome do objeto de interface
	 * @param string $psFieldAlias Alias do campo
	 * @return boolean True sse o objeto foi preenchido com sucesso
	 */
	public function fillObjectFromField($psObjectName,$psFieldAlias){
		return $this->coDataset->fillObjectFromField($psObjectName,$psFieldAlias);
	}

	/**
	 * Preenche os objetos de interface a partir dos campos da tabela.
	 *
	 * <p>Preenche os objetos de interface a partir dos campos da tabela, quando
	 * houver objetos de interface com nomes iguais aos aliases dos campos.</p>
	 * @access public
	 * @return boolean True sse todos objetos foram preenchidos com sucesso
	 */
	public function fillFormFromFields(){
		$mbReturn = true;
		foreach($this->coDataset->getFields() as $msAlias=>$moField){
			$mbReturn&= $this->fillObjectFromField($msAlias,$msAlias);
		}
		return $mbReturn;
	}

	/**
	 * Atualiza a rela��o entre o registro e registros de outra tabela.
	 *
	 * <p>Atualiza a rela��o entre o registro e registros de outra tabela.</p>
	 * @access public
	 * @param FWDDBTable $poRelationTable Inst�ncia de registro da tabela de rela��o
	 * @param string $psForeignKey Alias da chave da outra tabela
	 * @param array $paIds Ids dos registros da outra tabela a serem relacionados
	 * @param boolean $pbDelete Sse for true, deleta rela��es com registros n�o inclu�dos em $paIds
	 */
	public function updateRelation(FWDDBTable $poRelationTable,$psForeignKey,$paIds,$pbDelete=true){
		$msAliasId = $this->csAliasId;

		if(!$this->getFieldByAlias($msAliasId)){
			trigger_error("You must specify the alias of the primary key.",E_USER_ERROR);
			return;
		}elseif(!$poRelationTable->getFieldByAlias($msAliasId) || !$poRelationTable->getFieldByAlias($psForeignKey)){
			trigger_error("The aliases and the relation table don't match.",E_USER_ERROR);
			return;
		}elseif(!$this->getFieldValue($msAliasId)){
			trigger_error("You must define the id.",E_USER_ERROR);
			return;
		}

		$miId = $this->getFieldValue($msAliasId);
		$msRelationClass = get_class($poRelationTable);

		$moRelation = new $msRelationClass;
		$moRelation->createFilter($miId,$msAliasId);
		$moRelation->select();
		$maCurrentIds = array();
		while($moRelation->fetch()){
			$maCurrentIds[] = $moRelation->getFieldValue($psForeignKey);
		}

		$maToInsert = array_diff($paIds,$maCurrentIds);
		$moRelation = new $msRelationClass;
		$moRelation->setFieldValue($msAliasId,$miId);
		foreach($maToInsert as $miForeignKey){
			$moRelation->setFieldValue($psForeignKey,$miForeignKey);
			$moRelation->insert();
		}

		if($pbDelete){
			$maToDelete = array_diff($maCurrentIds,$paIds);
			if(count($maToDelete)>0){
				$moRelation = new $msRelationClass;
				$moRelation->createFilter($miId,$msAliasId);
				$moRelation->getFieldByAlias($psForeignKey)->addFilter(new FWDDBFilter('in',$maToDelete));
				$msRelationAliasId = $moRelation->csAliasId;
				if($msRelationAliasId){
					$moRelation->select();
					$moDeleteRelation = new $msRelationClass;
					while($moRelation->fetch()){
						$moDeleteRelation->delete($moRelation->getFieldValue($msRelationAliasId),true);
					}
				}else{
					$moRelation->delete();
				}
			}
		}
	}

	/**
	 * Retorna um array associativo com os valores dos campos.
	 *
	 * <p>Retorna um array associativo com os valores dos campos, no formato alias=>valor.</p>
	 * @access public
	 * @return array Array associativo com os valores dos campos
	 */
	public function getFieldValues(){
		return $this->coDataset->getFieldValues();
	}

}

?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDDBFilter.
 *
 * <p>Armazena os filtros que ser�o aplicados no campo.</p>
 * @package FWD5
 * @subpackage db
 */
class FWDDBType {

	/**
	 * Converte o tipo de acordo com o banco de dados.
	 *
	 * <p>M�todo para converter o tipo de acordo com o banco de dados.</p>
	 * @access public
	 * @param string $psValue Valor
	 * @param integer $piType Tipo
	 * @return string SQL para ser inserido no banco
	 */
	static public function translate($psValue, $piType) {
		$moConn = FWDWebLib::getConnection();
		$msValue = "";

		switch ($piType) {
			case DB_UNKNOWN:
				trigger_error("Undefined field type", E_USER_WARNING);
				break;
					
			case DB_NUMBER:
				if (($moConn->getDatabaseType() == DB_ORACLE) && ($psValue == 'null')) return "''";
				else if ($moConn->getDatabaseType() == DB_POSTGRES 
					&& strtoupper($psValue) == 'NULL') return "null";
				else if ($psValue) $msValue = 0 + $psValue;
				else $msValue = 0;
				break;
					
			case DB_STRING:
				$msValue = $moConn->quote($psValue);
				break;
					
			case DB_DATETIME:
				if($psValue!='null')
				$msValue = $moConn->timestampFormat($psValue);
				else if ($moConn->getDatabaseType() != DB_ORACLE)
				return 'null';
				else
				return "''";
				break;
		}
		return $msValue;
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDDBEntity.
 *
 * <p>Classe que representa uma entidade do banco de dados.</p>
 * @package FWD5
 * @subpackage db
 */
class FWDDBEntity {
		
	/**
	 * Objeto FWDDBDataSet
	 * @var FWDDBDataSet
	 * @access protected
	 */
	protected $coDataSet = NULL;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDDBEntity.</p>
	 * @access public
	 * @param FWDDB $poDB Objeto de conex�o com o banco
	 * @param string $psTable Nome da tabela (opcional)
	 */
	public function __construct(FWDDB $poDB, $psTable = "") {
		$this->coDataSet = new FWDDBDataSet($poDB, $psTable);
	}

	/**
	 * Adiciona um campo � entidade.
	 *
	 * <p>M�todo para adicionar um campo � entidade.</p>
	 * @access public
	 * @param FWDDBField $poField Campo
	 */
	public function addField(FWDDBField $poField) {
		$this->coDataSet->addFWDDBField($poField);
	}

	/**
	 * Executa uma query.
	 *
	 * <p>M�todo para executar uma query.</p>
	 * @access public
	 * @param object $psQuery Consulta que deve ser executada
	 */
	public function execute($psQuery) {
		$this->coDataSet->setQuery($psQuery);
		$this->coDataSet->execute();
	}

	/**
	 * Executa a consulta para buscar os dados da entidade.
	 *
	 * <p>M�todo para executa a consulta para buscar os dados da entidade.</p>
	 * @access public
	 * @return FWDDBDataSet DataSet
	 */
	public function select() {
		$this->coDataSet->select();
		return $this->coDataSet;
	}

	/**
	 * Insere os dados referentes � entidade no banco de dados.
	 *
	 * <p>M�todo para inserir os dados referentes � entidade no banco de dados.</p>
	 * @access public
	 * @return integer Id do registro inserido ou falso
	 */
	public function insert() {
		return $this->coDataSet->insert();
	}

	/**
	 * Atualiza dados referentes � entidade no banco de dados.
	 *
	 * <p>M�todo para atualizar dados referentes � entidade no banco de dados.</p>
	 * @access public
	 * @return integer N�mero de registros afetados pelo update ou falso em caso de falha
	 */
	public function update() {
		return $this->coDataSet->update();
	}

	/**
	 * Deleta do banco baseado nos valores da view.
	 *
	 * <p>M�todo para deletar do banco baseado nos valores da view.</p>
	 * @access public
	 * @return integer N�mero de registros afetados pelo delete ou falso em caso de falha
	 */
	public function delete() {
		return $this->coDataSet->delete();
	}
	/**
	 * Seta um filtro para a consulta.
	 *
	 * <p>M�todo para setar um filtro para consulta (normalmente o id do objeto buscado).</p>
	 * @access public
	 * @param string $psKeyAlias Alias do campo chave
	 * @param string $psValue Valor do campo
	 * @return booleam Verdadeiro ou falso
	 */
	public function setKey($psKeyAlias, $psValue) {
		$moField = $this->coDataSet->GetFieldByAlias($psKeyAlias);
		if(isset($moField)){
			$moFilter = new FWDDBFilter("=", $psValue);
			$moField->addFilter($moFilter);
			return true;
		}
		return false;
	}

	/**
	 * Seta o valor de um campo da entidade.
	 *
	 * <p>M�todo para setar o valor de um campo da entidiade.</p>
	 * @access public
	 * @param string $psAlias Alias do campo
	 * @param string $psValue Valor do campo
	 * @return boolean Verdadeiro ou Falso
	 */
	public function setValue($psAlias, $psValue) {
		$moField = $this->coDataSet->getFieldByAlias($psAlias);
		if(isset($moField)) {
			$moField->setValue($psValue);
			return true;
		} else
		return false;
	}

	/**
	 * Retorna o valor de um campo da entidade.
	 *
	 * <p>M�todo para retornar o valor de um campo da entidiade.</p>
	 * @access public
	 * @param string $psAlias Alias do campo
	 * @return string Valor do campo ou falso
	 */
	public function getValue($psAlias) {
		$moField = $this->coDataSet->GetFieldByAlias($psAlias);
		if(isset($moField))
		return $moField->GetValue();
		else return false;
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDDBDataBinder.
 *
 * <p>Classe utilizada para associar uma View com um campo do banco de dados.</p>
 * @package FWD5
 * @subpackage db
 */
class FWDDBDataBinder {

	/**
	 * Array de Views (indexado pelo nome da View)
	 * @var FWDView
	 * @access protected
	 */
	protected $caViews = array();

	/**
	 * Entidade de banco de dados
	 * @var FWDEntity
	 * @access protected
	 */
	protected $coEntity = NULL;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDDBDataBinder.</p>
	 * @access public
	 * @param FWDDBEntity $poEntity Entidade
	 */
	public function __construct(FWDDBEntity $poEntity) {
		$this->coEntity = $poEntity;
	}

	/**
	 * Adiciona uma view.
	 *
	 * <p>M�todo para adicionar uma view.</p>
	 * @access public
	 * @param FWDView $poView View
	 * @param string $psAlias Alias do campo ao qual a View est� associada
	 */
	public function addView(FWDView $poView, $psAlias) {
		$this->caViews[$psAlias] = $poView;
	}

	/**
	 * Insere os valores das views no banco de dados.
	 *
	 * <p>M�todo para inserir os valores das views no banco de dados.</p>
	 * @access public
	 * @return integer Id do registro inserido ou falso
	 */
	public function insert() {
		$this->setEntityValues();
		return $this->coEntity->insert();
	}

	/**
	 * Atualiza os valores das views no banco de dados.
	 *
	 * <p>M�todo para atualizar os valores das views no banco de dados.</p>
	 * @access public
	 * @return integer N�mero de registros afetados pelo update ou falso em caso de falha
	 */
	public function update() {
		$this->setEntityValues();
		return $this->coEntity->update();
	}

	/**
	 * Deleta do banco baseado nos valores da view.
	 *
	 * <p>M�todo para deletar do banco baseado nos valores da view.</p>
	 * @access public
	 * @return integer N�mero de registros afetados pelo delete ou falso em caso de falha
	 */
	public function delete() {
		$this->setEntityValues();
		return $this->coEntity->delete();
	}

	/**
	 * Popula as views.
	 *
	 * <p>M�todo para popular as views.</p>
	 * @access public
	 */
	public function populate() {
		$moDataset = $this->coEntity->select();
		while ($moDataset->fetch()) {
			foreach($this->caViews as $msAlias => $moView)
			$moView->setValue($this->coEntity->getValue($msAlias));
		}
	}

	/**
	 * Seta os valores dos campos da entidade de acordo com os valores das views.
	 *
	 * <p>M�todo para setar os valores dos campos da entidade de acordo com os valores das views.</p>
	 * @access public
	 */
	function setEntityValues() {
		foreach($this->caViews as $msAlias => $moView) {
			$this->coEntity->setValue($msAlias, $moView->getValue());
		}
	}

	/**
	 * Seta um filtro para a entidade.
	 *
	 * <p>M�todo para setar um filtro para a entidade.</p>
	 * @access public
	 */
	public function setKey($psKeyAlias, $psValue) {
		if (!$this->coEntity->setKey($psKeyAlias, $psValue))
		trigger_error("Invalid entity field alias: $psKeyAlias", E_USER_WARNING);
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

define("DB_UNKNOWN", 0);
define("DB_NUMBER", 1);
define("DB_STRING", 2);
define("DB_DATETIME", 3);

/**
 * Classe FWDDBField. Representa um campo do banco de dados.
 *
 * <p>Classe que representa um campo do banco de dados.</p>
 * @package FWD5
 * @subpackage db
 */
class FWDDBField {

	/**
	 * Nome do campo
	 * @var string
	 * @access protected
	 */
	protected $csName = "";

	/**
	 * Alias do campo
	 * @var string
	 * @access protected
	 */
	protected $csAlias = "";

	/**
	 * Tipo do campo
	 * @var integer
	 * @access protected
	 */
	protected $ciType = DB_UNKNOWN;

	/**
	 * Valor do campo
	 * @var string
	 * @access protected
	 */
	protected $csValue = "";

	/**
	 * Array de FWDDBFilter
	 * @var array
	 * @access protected
	 */
	protected $caFilters = array();

	/**
	 * Define se o campo pode ser atualizado
	 * @var boolean
	 * @access private
	 */
	private $cbUpdatable = false;

	/**
	 * Define se o campo deve ser decodificado antes de ir para o banco
	 * @var boolean
	 * @access private
	 */
	private $cbDecode = true;

	/**
	 * Define se deve separar os filtros por AND (=true) ou por OR (=false)
	 * @var boolean
	 * @access protected
	 */
	protected $cbAnd = true;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDDBField.</p>
	 * @access public
	 * @param string $psName Nome
	 * @param string $psAlias Alias
	 * @param integer $piType Tipo
	 * @param string $psValue Valor
	 * @param boolean $pbUpdatable Pode ser atualizado ou n�o
	 */
	public function __construct($psName, $psAlias, $piType, $psValue = "", $pbUpdatable = false) {
		$this->csName = $psName;
		$this->csAlias = $psAlias;
		$this->ciType = $piType;
		if (!($psValue === "")) {
			$this->csValue = $psValue;
			$this->cbUpdatable = true;
		}
		else $this->cbUpdatable = $pbUpdatable;
	}

	/**
	 * Seta o nome do campo.
	 *
	 * <p>M�todo para setar o nome do campo.</p>
	 * @access public
	 * @param string $psName Nome do campo
	 */
	public function setName($psName) {
		$this->csName = $psName;
	}

	/**
	 * Retorna o nome do campo.
	 *
	 * <p>M�todo para retornar o nome do campo.</p>
	 * @access public
	 * @return string Nome do campo
	 */
	public function getName() {
		return $this->csName;
	}

	/**
	 * Define se o campo � atualiz�vel ou n�o.
	 *
	 * <p>M�todo para definir se o campo � atualiz�vel ou n�o.</p>
	 * @access public
	 * @param boolean $pbUpdatable Verdadeiro ou Falso
	 */
	public function setUpdatable($pbUpdatable) {
		$this->cbUpdatable = $pbUpdatable;
	}

	/**
	 * Verifica se o campo � atualiz�vel.
	 *
	 * <p>M�todo para verificar se o campo � atualiz�vel.</p>
	 * @access public
	 * @return boolean Verdadeiro ou Falso
	 */
	public function isUpdatable() {
		return $this->cbUpdatable;
	}

	/**
	 * Define se o campo � atualiz�vel ou n�o.
	 *
	 * <p>M�todo para definir se o campo � atualiz�vel ou n�o.</p>
	 * @access public
	 * @param boolean $pbUpdatable Verdadeiro ou Falso
	 */
	public function setDecode($pbUpdatable) {
		$this->cbDecode = $pbUpdatable;
	}

	/**
	 * Verifica se o campo � atualiz�vel.
	 *
	 * <p>M�todo para verificar se o campo � atualiz�vel.</p>
	 * @access public
	 * @return boolean Verdadeiro ou Falso
	 */
	public function mustDecode() {
		return $this->cbDecode;
	}

	/**
	 * Define se deve separar os filtros por AND (=true) ou OR (=false).
	 *
	 * <p>M�todo para definir se deve separar os filtros por AND (=true) ou OR (=false).</p>
	 * @access public
	 * @param boolean $pbAnd Verdadeiro (and) ou Falso (or)
	 */
	public function setFilterSeparator($pbAnd) {
		$this->cbAnd = $pbAnd;
	}

	/**
	 * Retorna o separador de filtros.
	 *
	 * <p>M�todo para retornar o separador de filtros (true = and | false = or).</p>
	 * @access public
	 * @return boolean Verdadeiro ou Falso
	 */
	public function getFilterSeparator() {
		return $this->cbAnd;
	}

	/**
	 * Seta o alias do campo.
	 *
	 * <p>M�todo para setar o alias do campo.</p>
	 * @access public
	 * @param string $psAlias Alias do campo
	 */
	public function setAlias($psAlias) {
		$this->csAlias = $psAlias;
	}

	/**
	 * Retorna o alias do campo.
	 *
	 * <p>M�todo para retornar o alias do campo.</p>
	 * @access public
	 * @return string Alias do campo
	 */
	public function getAlias() {
		return $this->csAlias;
	}

	/**
	 * Seta o tipo do campo.
	 *
	 * <p>M�todo para setar o tipo do campo.</p>
	 * @access public
	 * @param integer $piType Tipo do campo
	 */
	public function setType($piType) {
		$this->ciType = $piType;
	}

	/**
	 * Retorna o tipo do campo.
	 *
	 * <p>M�todo para retornar o tipo do campo.</p>
	 * @access public
	 * @return integer Tipo do campo
	 */
	public function getType() {
		return $this->ciType;
	}

	/**
	 * Seta o valor do campo.
	 *
	 * <p>M�todo para setar o valor do campo.</p>
	 * @access public
	 * @param string $psName Valor do campo
	 * @param string $pbDecode Decodificar o campo antes de inserir no banco?
	 */
	public function setValue($psValue, $pbUpdatable = true, $pbDecode = true) {
		$this->setUpdatable($pbUpdatable);
		$this->setDecode($pbDecode);
		$this->csValue = $this->ciType == DB_STRING ? FWDWebLib::convertToISO($psValue) : $psValue;
	}

	/**
	 * Retorna o valor do campo.
	 *
	 * <p>M�todo para retornar o valor do campo.</p>
	 * @access public
	 * @return string Valor do campo
	 */
	public function getValue() {
		$msValue = '';
		if ($this->ciType == DB_STRING) $msValue = FWDWebLib::convertToISO($this->csValue);
		elseif ($this->ciType == DB_DATETIME){
			if($this->csValue==='null'){
				$msValue = 'null';
			}else{
				$msValue = (is_numeric($this->csValue) || !$this->csValue) ? $this->csValue : date('Y-m-d H:i:s', strtotime($this->csValue));
			}
		}else $msValue = $this->csValue;
		return $msValue;
	}

	/**
	 * Adiciona um filtro no campo.
	 *
	 * <p>M�todo para adicionar um filtro no campo.</p>
	 * @access public
	 * @param FWDDBFilter $poFilter Filtro
	 */
	public function addFilter(FWDDBFilter $poFilter) {
		$poFilter->setField($this->getName());
		$poFilter->setFieldType($this->getType());
		$this->caFilters[] = $poFilter;
	}

	/**
	 * Remove todos os filtros do campo.
	 *
	 * <p>Remove todos os filtros do campo.</p>
	 * @access public
	 */
	public function clearFilters(){
		$this->caFilters = array();
	}

	/**
	 * Retorna todos os filtros do campo.
	 *
	 * <p>M�todo para retornar todos os filtros do campo.</p>
	 * @access public
	 * @return array Array de filtros
	 */
	public function getFilters() {
		return $this->caFilters;
	}

	/**
	 * Retorna o SQL referente aos filtros.
	 *
	 * <p>M�todo para retornar o SQL referente ao filtros.</p>
	 * @access public
	 * @return string SQL do filtro
	 */
	public function getFiltersSQL() {
		$msFilter = "";
		$miCount = count($this->caFilters);
		$miI = 0;
		foreach($this->caFilters as $moFilter) {
			$msFilter .= $moFilter->getSQL();
			if ($miI != $miCount - 1) {
				$msFilter .=  $this->getFilterSeparator() ? " AND " : " OR ";
			}
			$miI++;
		}
		return $msFilter;
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDDBFilter.
 *
 * <p>Armazena os filtros que ser�o aplicados no campo.</p>
 * @package FWD5
 * @subpackage db
 */
class FWDDBFilter {

	/**
	 * Array com os poss�veis valores de operadores
	 * @var array
	 * @access protected
	 */
	private $caOperators = array('=', '!=', '<>', '>', '<', '>=', '<=', 'like', 'in', 'null', 'notnull');

	/**
	 * Array com os valores para restri��o do campo
	 * @var array
	 * @access protected
	 */
	protected $caValues = array();

	/**
	 * Operador {=, !=, <>, >, <, >=, <=, like, in, null, notnull}
	 * @var string
	 * @access protected
	 */
	protected $csOperator = "";

	/**
	 * Nome do campo ao qual o filtro pertence
	 * @var string
	 * @access protected
	 */
	protected $csField = "";

	/**
	 * Tipo do campo ao qual o filtro pertence
	 * @var integer
	 * @access protected
	 */
	protected $ciType = DB_UNKNOWN;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDDBFilter.</p>
	 * @access public
	 * @param string $psField Nome do campo
	 * @param string $psOperator Operador
	 * @param string $psValue Valor para compara��o
	 */
	public function __construct($psOperator, $psValue = null){
		$this->setOperator($psOperator);
		if($psValue!==null){
			if($psOperator=='null' || $psOperator=='notnull'){
				trigger_error("Operator '$psOperator' does not take values.", E_USER_WARNING);
			}else{
				if(is_array($psValue)) $this->caValues = $psValue;
				else $this->addValue($psValue);
			}
		}
	}

	/**
	 * Seta o operador.
	 *
	 * <p>M�todo para setar o operador.</p>
	 * @access public
	 * @param string $psOperator Operador
	 */
	public function setOperator($psOperator){
		if (in_array($psOperator, $this->caOperators)) $this->csOperator = $psOperator;
		else trigger_error("Invalid SQL Operator: $psOperator", E_USER_WARNING);
	}

	/**
	 * Retorna o operador.
	 *
	 * <p>M�todo para retornar o operador.</p>
	 * @access public
	 * @return string Operador
	 */
	public function getOperator(){
		return $this->csOperator;
	}

	/**
	 * Seta o nome do campo ao qual o filtro se refere.
	 *
	 * <p>M�todo para setar o nome do campo ao qual o filtro se refere.</p>
	 * @access public
	 * @param string $psField Campo
	 */
	public function setField($psField){
		$this->csField = $psField;
	}

	/**
	 * Retorna o nome do campo ao qual o filtro se refere.
	 *
	 * <p>M�todo para retornar o nome do campo ao qual o filtro se refere.</p>
	 * @access public
	 * @return string Campo
	 */
	public function getField(){
		return $this->csField;
	}

	/**
	 * Seta o tipo do campo ao qual o filtro se refere.
	 *
	 * <p>M�todo para setar o tipo do campo ao qual o filtro se refere.</p>
	 * @access public
	 * @param integer $piType Campo
	 */
	public function setFieldType($piType){
		$this->ciType = $piType;
	}

	/**
	 * Retorna o tipo do campo ao qual o filtro se refere.
	 *
	 * <p>M�todo para retornar o tipo do campo ao qual o filtro se refere.</p>
	 * @access public
	 * @return integer Tipo
	 */
	public function getFieldType(){
		return $this->ciType;
	}

	/**
	 * Adiciona um valor ao filtro.
	 *
	 * <p>M�todo para adicionar valores no filtro.</p>
	 * @access public
	 * @param string $psValue Valor
	 */
	public function addValue($psValue){
		if($this->csOperator=='null' || $this->csOperator=='notnull'){
			trigger_error("Operator '{$this->csOperator}' does not take values.", E_USER_WARNING);
		}else{
			$this->caValues[] = $psValue;
		}
	}

	/**
	 * Retorna o valor do campo.
	 *
	 * <p>M�todo para retornar o valor do campo.</p>
	 * @access public
	 * @return string Valor do campo
	 */
	public function getValue(){
		return $this->caValues;
	}

	/**
	 * Retorna o SQL referente ao filtro.
	 *
	 * <p>M�todo para retornar o SQL referente ao filtro.</p>
	 * @access public
	 * @return string SQL do filtro
	 */
	public function getSQL(){
		$msSQL = "";
		$miCount = count($this->caValues);

		switch($this->csOperator){
			case "=":
				$miI = 0;
				foreach($this->caValues as $msValue){
					if ($miI) $msSQL .= " OR ";
					$msSQL .= $this->csField . " = " . FWDDBType::translate($msValue, $this->getFieldType());
					$miI++;
				}
				if ($miCount > 1) $msSQL = "(" . $msSQL . ")";
				break;

			case "!=": case "<>":
				$miI = 0;
				foreach($this->caValues as $msValue){
					if ($miI) $msSQL .= " AND ";
					$msSQL .= $this->csField . " != " . FWDDBType::translate($msValue, $this->getFieldType());
					$miI++;
				}
				if ($miCount > 1) $msSQL = "(" . $msSQL . ")";
				break;

			case ">": case ">=": case "<": case "<=":
				$miI = 0;
				foreach($this->caValues as $msValue){
					if ($miI) $msSQL .= " AND ";
					$msSQL .= $this->csField . " {$this->csOperator} " . FWDDBType::translate($msValue, $this->getFieldType());
					$miI++;
				}
				if (count($this->caValues) != 1)
				trigger_error("SQL could be better written: $msSQL", E_USER_WARNING);
				break;

			case "in":
				$msSQL = $this->csField . " in (";
				if ($miCount){
					$miI = 0;
					foreach($this->caValues as $msValue){
						$msSQL .= FWDDBType::translate($msValue, $this->getFieldType());
						if ($miI != $miCount - 1) $msSQL .= ", ";
						$miI++;
					}
					$msSQL .= ")";
				} else trigger_error("SQL \"in\" operator must have at least one value: $msSQL", E_USER_WARNING);
				break;

			case "like":
				$miI = 0;
				foreach($this->caValues as $msValue){
					if ($miI) $msSQL .= " OR ";
					$msDBType = FWDWebLib::getConnection()->getDatabaseType();
					if($msDBType==DB_MSSQL){
						$msValue = str_replace('[','[[]',$msValue);
					}
					$msSQL .= $this->csField . " like " . FWDDBType::translate($msValue, $this->getFieldType());
					$miI++;
				}
				if ($miCount > 1) $msSQL = "(" . $msSQL . ")";
				break;

			case 'null':
				$msSQL = $this->csField.' is NULL';
				break;

			case 'notnull':
				$msSQL = $this->csField.' is not NULL';
				break;
		}
		return $msSQL;
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDDBDataSet.
 *
 * <p>Classe que representa uma abstra��o de uma tabela do banco de dados.</p>
 * @package FWD5
 * @subpackage db
 */
class FWDDBDataSet {

	/**
	 * Objeto de conex�o com o banco de dados
	 * @var FWDDB
	 * @access protected
	 */
	protected $coDatabase = null;

	/**
	 * Array de FWDDBField (indexado pelo alias do campo)
	 * @var array
	 * @access protected
	 */
	protected $caFields = array();

	/**
	 * Array com os campos que devem ordenar o resultado (ex: array['rm_area'] = '+')
	 * @var array
	 * @access protected
	 */
	protected $caOrderBy = array();

	/**
	 * Nome da tabela
	 * @var string
	 * @access protected
	 */
	protected $csTable = "";

	/**
	 * Query que vai ser executada
	 * @var string
	 * @access protected
	 */
	protected $csQuery = "";

	/**
	 * N�mero m�ximo de linhas que devem ser retornadas
	 * @var integer
	 * @access protected
	 */
	protected $ciLimit;

	/**
	 * N�mero da linha corrente durante o retorno de uma consulta
	 * @var integer
	 * @access protected
	 */
	protected $ciCurrentRow;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDDBDataSet.</p>
	 * @access public
	 * @param FWDDB $poDB Objeto de conex�o
	 * @param string $psTable Tabela (opcional)
	 */
	public function __construct(FWDDB $poDB, $psTable = "") {
		$this->coDatabase = $poDB;
		$this->csTable = $psTable;
		if (!$this->coDatabase->connect())
		trigger_error("Error while connecting to database", E_USER_ERROR);
	}

	/**
	 * Limpa o dataset.
	 *
	 * <p>M�todo para limpar o dataset.</p>
	 * @access public
	 */
	public function clean() {
		$this->coDatabase = NULL;
		$this->caFields = array();
		$this->caOrderBy = array();
		$this->csTable = "";
		$this->csQuery = "";
	}

	/**
	 * Seta o objeto objeto de conex�o com o banco.
	 *
	 * <p>M�todo para setar o objeto de conex�o com o banco.</p>
	 * @access public
	 * @param FWDDB $poDB Objeto de conex�o
	 */
	public function setDatabase(FWDDB $poDB) {
		$this->coDatabase = $poDB;
	}

	/**
	 * Retorna o objeto objeto de conex�o com o banco.
	 *
	 * <p>M�todo para retornar o objeto objeto de conex�o com o banco.</p>
	 * @access public
	 * @return FWDDB Objeto de conex�o
	 */
	public function getDatabase() {
		return $this->coDatabase;
	}

	/**
	 * Seta o nome da tabela.
	 *
	 * <p>M�todo para setar o nome da tabela.</p>
	 * @access public
	 * @param string $psTable Tabela
	 */
	public function setTable($psTable) {
		$this->csTable = $psTable;
	}

	/**
	 * Retorna o nome da tabela.
	 *
	 * <p>M�todo para retornar o nome da tabela.</p>
	 * @access public
	 * @return string Tabela
	 */
	public function getTable() {
		return $this->csTable;
	}

	/**
	 * Adiciona um campo na tabela.
	 *
	 * <p>M�todo para adicionar um campo na tabela.</p>
	 * @access public
	 * @param FWDDBField $poField Campo
	 */
	public function addFWDDBField(FWDDBField $poField) {
		$this->caFields[strtolower($poField->getAlias())] = $poField;
	}

	/**
	 * Retorna o objeto FWDDBField relativo ao alias passado.
	 *
	 * <p>M�todo para retornar o objeto FWDDBField relativo ao alias passado.</p>
	 * @access public
	 * @param string $psAlias Alias do campo
	 * @return FWDDBField Campo
	 */
	public function getFieldByAlias($psAlias) {
		$psAlias = strtolower($psAlias);
		if (isset($this->caFields[$psAlias])) return $this->caFields[$psAlias];
		else return NULL;
	}

	/**
	 * Retorna o n�mero total de campos.
	 *
	 * <p>M�todo para retornar o n�mero total de campos.</p>
	 * @access public
	 * @return integer N�mero total de campos
	 */
	public function getFieldCount() {
		if (count($this->caFields)) return count($this->caFields);
		else return $this->coDatabase->getFieldCount();
	}

	/**
	 * Retorna um array com os campos do DataSet.
	 *
	 * <p>Retorna um array com os campos (objetos FWDDBField) do DataSet.</p>
	 * @access public
	 * @return array Array de campos
	 */
	public function getFields(){
		return $this->caFields;
	}

	/**
	 * Executa um sql qualquer no banco.
	 *
	 * <p>M�todo para executar um sql qualquer no banco.</p>
	 * @access public
	 * @param string $piLimit N�mero de registros que deve ser retornado
	 * @param string $psOffset A partir de qual registro deve come�ar a retornar
	 * @return boolean Verdadeiro (sucesso) ou Falso (falha)
	 */
	public function execute($piLimit = 0, $piOffset = 0) {
		if($this->csQuery){
			$msOrder = $this->getOrderBy();
			$this->ciCurrentRow = 0;
			$this->ciLimit = $piLimit;
			$miLimit = ($piLimit?$piLimit+1:0);
			return $this->coDatabase->execute($this->csQuery . $msOrder, $miLimit, $piOffset);
		}else{
			return false;
		}
	}

	/**
	 * Retorna o n�mero total de linhas.
	 *
	 * <p>M�todo para retornar o n�mero total de linhas.</p>
	 * @access public
	 * @return integer N�mero total de linhas
	 */
	public function getRowCount(){
		$miRowCount = $this->coDatabase->getRowCount();
		if($this->ciLimit){
			return min($miRowCount,$this->ciLimit);
		}else{
			return $miRowCount;
		}
	}

	/**
	 * Seta a query que ser� executada.
	 *
	 * <p>M�todo para setar a query que ser� executada.</p>
	 * @access public
	 * @param string $psQuery Query que vai ser executada
	 */
	public function setQuery($psQuery) {
		$this->csQuery = $psQuery;
	}

	/**
	 * Retorna a query que ser� executada.
	 *
	 * <p>M�todo para retornar a query que ser� executada.</p>
	 * @access public
	 * @return string Query que vai ser executada
	 */
	public function getQuery() {
		return $this->csQuery;
	}

	/**
	 * Busca um registro do resultado e avan�a para o pr�ximo.
	 *
	 * <p>M�todo para buscar um registro do resultado e avan�ar para o pr�ximo.</p>
	 * @access public
	 * @return boolean Verdadeiro (sucesso) ou Falso (falha)
	 */
	public function fetch(){
		$maData = $this->coDatabase->fetch();
		if($maData && ($this->ciLimit==0 || $this->ciCurrentRow < $this->ciLimit)){
			$this->ciCurrentRow++;
			foreach($maData as $msAlias => $msData){
				$moField = $this->getFieldByAlias($msAlias);
				if($moField){
					$moField->setValue($msData);
				}else{
					trigger_error("QueryHandler without a dbfield with alias = '{$msAlias}'", E_USER_WARNING);
				}
			}
			return true;
		}else{
			return false;
		}
	}

	/**
	 * Ordena o resultado da consulta a partir do campo especificado.
	 *
	 * <p>M�todo para ordenar o resultado da consulta a partir do campo especificado.</p>
	 * @access public
	 * @param integer $psAlias Alias do campo
	 * @param integer $psOrder Ordem crescente ("+") ou decrescente ("-")
	 */
	public function setOrderBy($psAlias, $psOrder) {
		switch ($psOrder) {
			case "+":
				$this->caOrderBy[$psAlias] = "ASC";
				break;
			case "-":
				$this->caOrderBy[$psAlias] = "DESC";
				break;
			default:
				trigger_error("Invalid query order! got '{$psOrder}'" . " !", E_USER_WARNING);
				break;
		}
	}

	/**
	 * Reseta o campos que ordenam a consulta.
	 *
	 * <p>M�todo para resetar os campos que ordenam a consulta.</p>
	 * @access public
	 */
	public function resetOrderBy() {
		unset($this->caOrderBy);
	}

	/**
	 * Obtem o c�digo do orderby das colunas.
	 *
	 * <p>M�todo para obter o c�gigo order by das colunas da consulta.</p>
	 * @access private
	 */
	private function getOrderBy(){
		$miCountOrderBy = count($this->caOrderBy);
		$msOrder = " ";
		if ($miCountOrderBy) {
			$miI = 0;
			$msOrder .= "ORDER BY";
			foreach($this->caOrderBy as $msAlias => $msOp) {
				if ($this->getFieldByAlias($msAlias))
				$msOrder .= " ". $msAlias . " " . $msOp;
				else
				trigger_error("Trying to use order by in a column without alias or with an inexistent dbField!",E_USER_ERROR);
				if ($miI != $miCountOrderBy - 1) $msOrder .= ",";
				$miI++;
			}
		}
		return $msOrder . " ";
	}

	/**
	 * Executa um select no banco de dados.
	 *
	 * <p>M�todo para executar um select no banco de dados.</p>
	 * @access public
	 * @param integer $piLimit N�mero m�ximo de registros que deve retornar (opcional)
	 * @param integer $piOffset A partir de qual registro deve come�ar a retornar (opcional)
	 * @return integer N�mero de registros retornados
	 */
	public function select($piLimit = 0, $piOffset = 0){
		$this->ciCurrentRow = 0;
		$this->ciLimit = $piLimit;
		$miLimit = ($piLimit?$piLimit+1:0);

		$msSelect = "SELECT ";

		$miCount = count($this->caFields);
		$miI = 0;
		foreach ($this->caFields as $moField) {
			if ($moField->getAlias()) $msSelect .= $moField->getName() . " as " . $moField->getAlias();
			else $msSelect .= $moField->getName();
			if ($miI != $miCount - 1) $msSelect .= ", ";
			$miI++;
		}

		$msSelect .= " FROM " . $this->getTable();

		$msWhere = "";
		$mbFirstFilter = true;
		foreach ($this->caFields as $moField) {
			$msFilterSQL = $moField->getFiltersSQL();
			if ($mbFirstFilter && $msFilterSQL) {
				$msWhere .= $msFilterSQL ? $msFilterSQL : "";
				$mbFirstFilter = false;
			} else $msWhere .= $msFilterSQL ?  " AND " . $msFilterSQL : "";
		}

		if ($msWhere) $msSelect .= " WHERE " . $msWhere;

		$this->setQuery($msSelect);
		if ($this->execute($miLimit, $piOffset))
		return $this->coDatabase->getRowCount();
		else
		trigger_error("Invalid select statement: " . $msSelect, E_USER_ERROR);
	}

	/**
	 * Insere dados no banco de dados.
	 *
	 * <p>M�todo para inserir dados no banco de dados.</p>
	 * @access public
	 * @param string $psKey Nome da chave prim�ria
	 * @return integer Id do �ltimo registro inserido ou falso
	 */
	public function insert($psKey = '') {
		$maFieldsInfo = array();
		 
		//adicionado utf8_decode para fazer a convers�o de charset que vai ser gravado no banco
		foreach ($this->caFields as $moField) {
			if ($moField->isUpdatable()) {
				$msValue = '';
				if ($moField->mustDecode()) $msValue = html_entity_decode($moField->getValue(), ENT_QUOTES);
				else $msValue = $moField->getValue();
				$maFieldsInfo[] = array('name' => $moField->getName(), 'value' => $msValue, 'type' => $moField->getType());
			}
		}
	  
		if (count($maFieldsInfo)) {
			$mbRes = $this->coDatabase->autoInsert($this->getTable(), $maFieldsInfo);
			if ($mbRes)
			return $psKey ? $this->coDatabase->getLastId($this->getTable(), $psKey) : true;
			else
			return false;
		} else
		return false;
	}

	/**
	 * Atualiza dados no banco de dados.
	 *
	 * <p>M�todo para atualizar dados no banco de dados.</p>
	 * @access public
	 * @return integer N�mero de registros afetados pelo update ou falso em caso de falha
	 */
	public function update() {
		$maFieldsInfo = array();
		$msWhere = "";
		$mbFirstFilter = true;

		//adicionado utf8_decode para fazer a convers�o de charset que vai ser gravado no banco
		foreach ($this->caFields as $moField) {
			if($moField->isUpdatable()){
				$msValue = '';
				if ($moField->mustDecode()) $msValue = html_entity_decode($moField->getValue(), ENT_QUOTES);
				else $msValue = $moField->getValue();
				$maFieldsInfo[] = array('name' => $moField->getName(), 'value' => $msValue, 'type' => $moField->getType());
			}
			$msFilterSQL = $moField->getFiltersSQL();
			if ($mbFirstFilter && $msFilterSQL) {
				$msWhere .= $msFilterSQL ? $msFilterSQL : "";
				$mbFirstFilter = false;
			} else $msWhere .= $msFilterSQL ?  " AND " . $msFilterSQL : "";
		}
		if (count($maFieldsInfo)) {
			$mbRes = $this->coDatabase->autoUpdate($this->getTable(), $maFieldsInfo, $msWhere);
			if ($mbRes)
			return $this->coDatabase->getAffectedRows();
			else return false;
		} else
		return false;
	}

	/**
	 * Exclui dados do banco de dados.
	 *
	 * <p>M�todo para excluir dados do banco de dados.</p>
	 * @access public
	 * @return integer N�mero de registros afetados pelo update ou falso em caso de falha
	 */
	public function delete() {
		$msWhere = "";
		$mbFirstFilter = true;
		foreach ($this->caFields as $moField) {
			$msFilterSQL = $moField->getFiltersSQL();
			if ($mbFirstFilter && $msFilterSQL) {
				$msWhere .= $msFilterSQL ? $msFilterSQL : "";
				$mbFirstFilter = false;
			} else $msWhere .= $msFilterSQL ?  " AND " . $msFilterSQL : "";
		}
		$mbRes = $this->coDatabase->autoDelete($this->getTable(), $msWhere);
		if ($mbRes)
		return $this->coDatabase->getAffectedRows();
		else return false;
	}

	/**
	 * Indica se o resultado da consulta foi truncado.
	 *
	 * <p>Indica se o resultado da consulta foi truncado.</p>
	 * @access public
	 * @return boolean True, sse o resultado da consulta foi truncado
	 */
	public function isTruncated(){
		return ($this->ciLimit && $this->coDatabase->getRowCount() > $this->ciLimit);
	}

	/**
	 * Preenche um campo a partir de um objeto de interface.
	 *
	 * <p>Preenche um campo a partir de um objeto de interface.</p>
	 * @access public
	 * @param string $psFieldAlias Alias do campo
	 * @param string psObjectName Nome do objeto de interface
	 * @return boolean True sse o campo foi preenchido com sucesso
	 */
	public function fillFieldFromObject($psFieldAlias,$psObjectName){
		$moObject = FWDWebLib::getObject($psObjectName);
		if($moObject && isset($this->caFields[$psFieldAlias])){
			$moField = $this->caFields[$psFieldAlias];
			switch($moField->getType()){
				case DB_NUMBER:{
					if($moObject->getValue()!==''){
						$moField->setValue($moObject->getValue(), true, true);
						return true;
					}else{
						return false;
					}
				}
				case DB_STRING:{
					$moField->setValue($moObject->getValue(), true, true);
					return true;
				}
				case DB_DATETIME:{
					if(get_class($moObject)=='FWDCalendar'){
						$moField->setValue($moObject->getTimestamp(), true, true);
						return true;
					}
				}
			}
			trigger_error("Translation from '".get_class($moObject)."' object to '".$moField->getType()."' type not implemented.",E_USER_WARNING);
		}
		return false;
	}

	/**
	 * Preenche os campos a partir dos objetos de interface.
	 *
	 * <p>Preenche os campos a partir dos objetos de interface, quando houver
	 * objetos de interface com nomes iguais aos aliases dos campos.</p>
	 * @access public
	 * @return boolean True sse todos campos foram preenchidos com sucesso
	 */
	public function fillFieldsFromForm(){
		$mbReturn = true;
		foreach($this->caFields as $msAlias=>$moField){
			$mbReturn&= $this->fillFieldFromObject($msAlias,$msAlias);
		}
		return $mbReturn;
	}

	/**
	 * Preenche os campos a partir de um array associativo.
	 *
	 * <p>Preenche os campos a partir de um array associativo no formato alias=>valor.</p>
	 * @access public
	 * @return boolean True sse todos campos foram preenchidos com sucesso
	 */
	public function fillFieldsFromArray($paFields){
		$mbReturn = true;
		foreach($this->caFields as $msAlias=>$moField){
			if(isset($paFields[$msAlias])){
				if($moField->getType()==DB_DATETIME){
					$moField->setValue(FWDWebLib::dateToTimestamp($paFields[$msAlias]));
				}else{
					$moField->setValue($paFields[$msAlias]);
				}
			}else{
				$mbReturn = false;
			}
		}
		return $mbReturn;
	}

	/**
	 * Preenche um objeto de interface a partir de um campo do dataset.
	 *
	 * <p>Preenche um objeto de interface a partir de um campo do dataset.</p>
	 * @access public
	 * @param string psObjectName Nome do objeto de interface
	 * @param string $psFieldAlias Alias do campo
	 * @return boolean True sse o objeto foi preenchido com sucesso
	 */
	public function fillObjectFromField($psObjectName,$psFieldAlias){
		$moObject = FWDWebLib::getObject($psObjectName);
		if($moObject && isset($this->caFields[$psFieldAlias])){
			$moField = $this->caFields[$psFieldAlias];
			$moObject->setValue($moField->getValue());
			return true;
		}
		return false;
	}

	/**
	 * Preenche os objetos de interface a partir dos campos do dataset.
	 *
	 * <p>Preenche os objetos de interface a partir dos campos do dataset, quando
	 * houver objetos de interface com nomes iguais aos aliases dos campos.</p>
	 * @access public
	 * @return boolean True sse todos objetos foram preenchidos com sucesso
	 */
	public function fillFormFromFields(){
		$mbReturn = true;
		foreach($this->caFields as $msAlias=>$moField){
			$mbReturn&= $this->fillObjectFromField($msAlias,$msAlias);
		}
		return $mbReturn;
	}

	/**
	 * Retorna um array associativo com os valores dos campos.
	 *
	 * <p>Retorna um array associativo com os valores dos campos, no formato alias=>valor.</p>
	 * @access public
	 * @return array Array associativo com os valores dos campos
	 */
	public function getFieldValues(){
		$maReturn = array();
		foreach($this->caFields as $msAlias=>$moField){
			$maReturn[$msAlias] = $moField->getValue();
		}
		return $maReturn;
	}

}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDTempEvent. Implementa Eventos Temporais.
 *
 * <p>Classe que implementa Eventos Temporais na Framework FWD5.</p>
 * @package FWD5
 * @subpackage event
 */
class FWDTempEvent {

	/**
	 * Evento Temporal
	 * @var FWDEvent
	 * @access protected
	 */
	protected $coContent = null;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDTempEvent.</p>
	 * @access public
	 */
	public function __construct() {
	}

	/**
	 * Renderiza o Evento Temporal.
	 *
	 * <p>Renderiza o Evento Temporal. Re�ne em uma vari�vel string
	 * todas as informa��es necess�rias para implementar o Evento Temporal.</p>
	 * @access public
	 * @return string String que ir� disparar o Evento Temporal.
	 */
	public function render() {
		return $this->getContent()->render();
	}

	/**
	 * Adiciona um objeto de Evento.
	 *
	 * <p>Adiciona um objeto de Evento FWDTempEvent.</p>
	 * @access public
	 * @param object $poContent Evento.
	 */
	public function setObjFWDEvent(FWDEvent $poContent)	{
		$this->coContent = $poContent;
	}

	/**
	 * Retorna o objeto de Evento Temporal.
	 *
	 * <p>Retorna o objeto de Evento Temporal.</p>
	 * @access public
	 * @return object Evento Temporal
	 */
	public function getContent() {
		return $this->coContent;
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDEventHandler. Manipula eventos.
 *
 * <p>Classe que manipula eventos dentro da Framework FWD5.</p>
 * @package FWD5
 * @subpackage event
 */
class FWDEventHandler implements FWDCollectable {

	/**
	 * Array de eventos
	 * @var array
	 * @access protected
	 */
	protected $caContent = array();

	/**
	 * Alvo do evento
	 * @var string
	 * @access protected
	 */
	protected $csEvent = "";

	/**
	 * Elementos sobre os quais o evento atuar�
	 * @var string
	 * @access protected
	 */
	protected $csElem = "";

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDEventHandler. Seta os atributos de
	 * Alvo e Elementos do evento.</p>
	 * @access public
	 * @param string $psEvent Alvo do evento
	 * @param string $psElem Elementos sobre os quais o evento atuar�
	 */
	public function __construct($psEvent,$psElem = "") {
		$this->setAttrEvent($psEvent);
		$this->setAttrElem($psElem);
	}

	/**
	 * Gera c�digo para renderizar os eventos.
	 *
	 * <p>Retorna uma string que cont�m o c�digo para renderizar os eventos.</p>
	 * @access public
	 * @return string C�digo para renderizar os eventos
	 */
	public function render(){
		if(strtolower($this->getAttrEvent())=='onpressenter'){
			$msHeader = "onkeydown='e=(arguments.length?arguments[0]:window.event);if(e.keyCode==13){";
			$msFooter = "}'";
		}else{
			$msHeader = "{$this->getAttrEvent()}='";
			$msFooter = "'";
		}
		$msBody = '';
		foreach($this->getAttrContent() as $moEvent){
			$msBody.= " ".$moEvent->render($this->getAttrElem());
		}
		return $msHeader.$msBody.$msFooter;
	}

	/**
	 * Seta um evento.
	 *
	 * <p>Seta um evento.</p>
	 * @access public
	 * @param FWDEvent $poContent Evento.
	 */
	public function setAttrContent(FWDEvent $poContent) {
		$this->caContent[] = $poContent;
	}

	/**
	 * Seta alvo do evento.
	 *
	 * <p>Seta alvo do evento.</p>
	 * @access public
	 * @param string $psEvent Alvo do evento
	 */
	public function setAttrEvent($psEvent) {
		$this->csEvent = $psEvent;
	}

	/**
	 * Seta elementos do evento.
	 *
	 * <p>Seta os elementos sobre os quais o evento atuar�.</p>
	 * @access public
	 * @param string $psElem Elementos do evento
	 */
	public function setAttrElem($psElem) {
		$this->csElem = $psElem;
	}

	/**
	 * Retorna o array de eventos.
	 *
	 * <p>Retorna o array de eventos (objetos).</p>
	 * @access public
	 * @return array Array de eventos (objetos)
	 */
	public function getAttrContent() {
		return $this->caContent;
	}

	/**
	 * Retorna alvo do evento.
	 *
	 * <p>Retorna alvo do evento.</p>
	 * @access public
	 * @return string Alvo do evento
	 */
	public function getAttrEvent() {
		return $this->csEvent;
	}

	/**
	 * Retorna elementos do evento.
	 *
	 * <p>Retorna os elementos sobre os quais o evento atuar�.</p>
	 * @access public
	 * @return string Elementos do evento
	 */
	public function getAttrElem() {
		return $this->csElem;
	}

	/**
	 * Retorna o array de eventos do elemento e o pr�prio elemento.
	 *
	 * <p>Retorna o array de eventos do elemento e o pr�prio elemento.</p>
	 * @access public
	 * @return array Eventos do elemento e elemento
	 */
	public function collect() {
		$maOut = array($this);
		if ($this->caContent)
		$maOut = array_merge($maOut,$this->caContent);
		return $maOut;
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDAjaxEvent. Implementa Eventos Ajax.
 *
 * <p>Classe que implementa Eventos Ajax na Framework FWD5.</p>
 * @package FWD5
 * @subpackage event
 */
class FWDAjaxEvent extends FWDEvent {

	/**
	 * Prioridade do Evento Ajax
	 * @var integer
	 * @access private
	 */
	private $ciPriority = 3;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDAjaxEvent. Seta os atributos de
	 * alvo, fun��o, elementos e par�metros do evento.</p>
	 * @access public
	 * @param string $psEvent Alvo do evento
	 * @param string $psFunction Fun��o do evento
	 * @param string $psElements Elementos sobre os quais o evento atuar�
	 * @param string $psParameters Par�metros necess�rios a alguns eventos
	 */
	public function __construct($psEvent = "" ,$psFunction = "",$psElements = "",$psParameters = ""){
		parent::__construct($psEvent,$psFunction,$psElements,$psParameters);
	}

	/**
	 * Renderiza o Evento Ajax.
	 *
	 * <p>Renderiza o Evento Ajax. Re�ne em uma vari�vel string
	 * todas as informa��es necess�rias para implementar o Evento Ajax.</p>
	 * @access public
	 * @return string String que ir� disparar o Evento Ajax
	 */
	public function render() {
		return "trigger_event(\"{$this->getAttrName()}\",\"{$this->getAttrPriority()}\");";
	}

	/**
	 * Seta a prioridade do evento.
	 *
	 * <p>Seta a prioridade do evento (1=M�xima, 2=M�dia, 3=M�nima[default]).</p>
	 * @access public
	 * @param string $psPriority Prioridade do Evento Ajax
	 */
	public function setAttrPriority($psPriority) {
		if ($psPriority<0 || $psPriority>3)
		trigger_error("Ajax event priority must be '0', '1', '2' or '3' (got '{$psPriority}')",E_USER_ERROR);
		else
		$this->ciPriority = $psPriority;
	}

	/**
	 * Retorna a prioridade do evento.
	 *
	 * <p>Retorna a prioridade do evento (1=M�xima, 2=M�dia, 3=M�nima[default]).</p>
	 * @access public
	 * @param string psPriority Prioridade do Evento Ajax
	 */
	public function getAttrPriority() {
		return $this->ciPriority;
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDServerEvent. Implementa Eventos Servidor.
 *
 * <p>Classe que implementa Eventos Servidor na Framework FWD5.</p>
 * @package FWD5
 * @subpackage event
 */
class FWDServerEvent extends FWDAjaxEvent {

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDServerEvent.</p>
	 * @access public
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Seta a Fun��o do Evento.
	 *
	 * <p>Seta o atributo de Fun��o do Evento Servidor.</p>
	 * @access public
	 * @param string $psFunction Fun��o do Evento Servidor
	 */
	public function setAttrFunction($psFunction) {
		$this->csFunction = $psFunction;
		$this->setAttrName($psFunction);
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDClientEvent. Implementa Eventos Cliente.
 *
 * <p>Classe que implementa Eventos Cliente na Framework FWD5.</p>
 * @package FWD5
 * @subpackage event
 */
class FWDClientEvent extends FWDEvent {

	/**
	 * Evento Cliente
	 * @var string
	 * @access protected
	 */
	protected $csValue = "";

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDClientEvent. Seta os atributos de
	 * alvo, fun��o, elementos e par�metros do evento.</p>
	 * @access public
	 * @param string $psEvent Alvo do evento
	 * @param string $psFunction Fun��o do evento
	 * @param string $psElements Elementos sobre os quais o evento atuar�
	 * @param string $psParameters Par�metros necess�rios a alguns eventos
	 */
	public function __construct($psEvent = "" ,$psFunction = "",$psElements = "",$psParameters = ""){
		parent::__construct($psEvent,$psFunction,$psElements,$psParameters);
	}

	/**
	 * Renderiza o Evento Cliente.
	 *
	 * <p>Renderiza o Evento Cliente. Instancia um objeto de evento javascript
	 * com os valores dos atributos de fun��o, elementos e par�metros e renderiza
	 * tal evento.</p>
	 * @access public
	 * @return string String que ir� disparar o Evento Cliente
	 */
	public function render() {
		if (!$this->csValue){
			$msFunction = $this->getAttrFunction();
			$msElems = $this->getAttrElements();
			$msAttr = $this->getAttrParameters();
			$moJs = new FWDJsEvent($msFunction,$msElems,$msAttr);
			return "try{{$moJs->render()}}catch(e){debugException(e)}";
		}
		else
		return "try{{$this->getAttrValue()}}catch(e){debugException(e)}";
	}

	/**
	 * Este m�todo n�o deve fazer nada.
	 *
	 * <p>Esta classe n�o deve utilizar este m�todo.</p>
	 * @access public
	 */
	public function run() {
	}

	/**
	 * Atribui valor ao Evento Cliente.
	 *
	 * <p>Atribui valor ao Evento Cliente retirando espa�os em branco no
	 * inicio e/ou no fim da string.</p>
	 * @access public
	 * @param string $psValue Valor a ser atribu�do
	 */
	public function setAttrValue($psValue) {
		$this->csValue = trim($psValue);
	}

	/**
	 * Atribui valor, condicionalmente, ao Evento Cliente.
	 *
	 * <p>Atribui valor ao Evento Cliente, condicionando atrav�s do segundo par�metro.
	 * Se pbForce for FALSE, concatena o valor se a vari�vel tiver conte�do;
	 * se TRUE atribui mesmo que tenha conte�do (sobrescreve).</p>
	 * @access public
	 * @param string p$sValue Valor a ser atribu�do
	 * @param boolean $pbForce For�ar o valor mesmo que j� tenha conte�do
	 */
	public function setValue($psValue, $pbForce) {
		if (!isset($this->csValue) || !$this->csValue || $pbForce ){
			$this->setAttrValue($psValue);
		}
		else
		$this->csValue .= $psValue;
	}

	/**
	 * Retorna o valor do Evento Cliente.
	 *
	 * <p>Retorna o valor do Evento Cliente.</p>
	 * @access public
	 * @return string Evento Cliente
	 */
	public function getAttrValue() {
		return str_replace("'","\"",$this->csValue);
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

define("JS_NONE", 0);
define("JS_ERASE", 1);
define("JS_COLOR_BACKGROUND", 2);
define("JS_COLOR_FONT", 3);
define("JS_CHECK_VALUE", 4);
define("JS_RESIZE", 5);
define("JS_SET_CONTENT", 6);
define("JS_SHOW", 7);
define("JS_HIDE", 8);
define("JS_SHOW_MENU", 9);
define("JS_HIDE_MENU", 10);
define("JS_CREATE_ELEM", 11);	// server only
define("JS_EXAMPLE_TEMP", 12);
define("JS_REPOSITION", 13);
define("JS_CHANGE_CURSOR", 14);
define("JS_MOVEDOR", 15);
define("JS_CHANGE_TAB", 16);
define("JS_PAGE_LOAD", 17);
define("JS_SHOW_LOADING", 18);
define("JS_RESIZE_COL", 19);
define("JS_ACTIVATE_TAB", 21);
define("JS_SAVE_USER_PREFS", 22);
define("JS_POPULATE_GRID",24);
define("JS_GRID_EVENT",25);
define("JS_GRID_CHANGEPAGENEXT",26);
define("JS_GRID_CHANGEPAGEFIRST",27);
define("JS_GRID_CHANGEPAGEBACK",28);
define("JS_GRID_CHANGEPAGELAST",29);
define("JS_GRID_REFRESH",30);
define("JS_OPEN",31);
define("JS_TOOLTIP",32);
define("JS_CHANGE_CLASS",33);
define("JS_INVERT_TABITEM_CLASS",34);
define("JS_GRID_CORRECT_SELECTED_ROWS",35);
define("JS_ELEMENT_IN_CONTROLLER",36);
define("JS_HAS_CHANGED",37);
define("JS_SUBMIT",100);
define("JS_INPUT_HIDDEN_VALUE",150);
define("JS_REQUIRED_CHECK_AJAX",151);
define("JS_REQUIRED_CHECK_CLIENT",152);
define("JS_REQUIRED_CHECK_SERVER",153);
define("JS_MENU_ACTION_TARGET",154);
define("JS_HAS_POPUP_BLOCKER",155);
define("JS_CLEAR_SELECT",156);
define("JS_POPULATE_SELECT",157);
define("JS_REPLACE",158);
define("JS_DOWNLOAD",159);

/**
 * Classe FWDJsEvent. Implementa Eventos Javascript.
 *
 * <p>Classe que implementa Eventos Javascript na Framework FWD5.</p>
 * @package FWD5
 * @subpackage event
 */
class FWDJsEvent {

	/**
	 * Constante que representa o Evento Javascript
	 * @var integer
	 * @access protected
	 */
	protected $ciEvent;

	/**
	 * Elementos sobre os quais o evento atuar�
	 * @var string
	 * @access protected
	 */
	protected $csElements;

	/**
	 * Par�metros do evento
	 * @var string
	 * @access protected
	 */
	protected $csParameters;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDJsEvent. Seta o evento, os elementos
	 * sobre os quais o evento atuar� e os par�metros do evento.</p>
	 * @access public
	 * @param integer $piEvent Evento
	 * @param string $psElems Elementos sobre os quais o evento atuar�
	 * @param string $psAttr Par�metros do evento
	 */
	public function __construct($piEvent,$psElems,$psAttr){
		$this->ciEvent = $piEvent;
		$this->csElements = $psElems;
		$this->csParameters = $psAttr;
	}

	/**
	 * Renderiza o evento Javascript.
	 *
	 * <p>Renderiza o evento Javascript. Re�ne em uma vari�vel string
	 * todas as informa��es necess�rias para implementar o evento Javascript.</p>
	 * @access public
	 * @return string String que ir� disparar o evento Javascript
	 */
	public function render() {
		$msCode = "";
		$maElems = explode(" ",$this->csElements);
		$maAttr = explode(" ",$this->csParameters);
		switch($this->ciEvent){
			case JS_ERASE:
				foreach($maElems as $id)
				$msCode .= "js_erase(\"$id\");";
				break;
			case JS_COLOR_BACKGROUND:
				foreach($maElems as $key => $id)
				$msCode .= "js_color_background(\"$id\", \"$maAttr[$key]\");";
				break;
			case JS_COLOR_FONT:
				foreach($maElems as $key => $id)
				$msCode .= "js_color_font(\"$id\", \"$maAttr[$key]\");";
				break;
			case JS_CHECK_VALUE:
				foreach($maElems as $key => $id)
				$msCode .= "js_check_value(\"$id\",\"$maAttr[$key]\");";
				break;
			case JS_RESIZE:
				$msCode .= "js_resize(\"$maElems[0]\",\"$maAttr[0]\",\"$maAttr[1]\");";
				break;
			case JS_SET_CONTENT:
				foreach($maElems as $key => $id)
				$msCode .= "js_set_content(\"$id\",\"$this->csParameters\");";
				break;
			case JS_SHOW_MENU:
				$msCode .= "js_show_menu(\"$maElems[0]\",event);";
				break;
			case JS_HIDE_MENU:
				$msCode .= "js_hide_menu(\"$maElems[0]\",event);";
				break;
			case JS_SHOW:
				$msCode .= "js_show(\"$maElems[0]\");";
				break;
			case JS_HIDE:
				$msCode .= "js_hide(\"$maElems[0]\");";
				break;
			case JS_CREATE_ELEM:
				$msCode .= "js_create_elem(\"$maElems[0]\", \"".$this->csParameters."\");";
				break;
			case JS_EXAMPLE_TEMP:
				$msCode .= "js_temp($maAttr[0]);";
				break;
			case JS_CHANGE_CURSOR:
				$msCode .= "js_change_cursor(\"$maElems[0]\",\"$maAttr[0]\");";
				break;
			case JS_REPOSITION:
				$msCode .= "js_reposition(\"$maElems[0]\",\"$maAttr[0]\",\"$maAttr[1]\");";
				break;
			case JS_MOVEDOR:
				$msCode .= "dragStart(event,\"$maElems[0]\");";
				break;
			case JS_CHANGE_TAB:
				$msCode .= "soTabSubManager.changeTab(\"$maAttr[0]\",\"$maAttr[1]\");";
				break;
			case JS_INVERT_TABITEM_CLASS:
				$msCode .= "js_invert_tabitem_class(\"$maElems[0]\");";
				break;
			case JS_PAGE_LOAD:
				$msCode .= "js_page_load(\"$maElems[0]\",\"$maElems[1]\",\"$maAttr[0]\",\"$maAttr[1]\");";
				break;
			case JS_SHOW_LOADING:
				$msCode .= "js_show_loading(\"$maElems[0]\",\"$maElems[1]\");";
				break;
			case JS_RESIZE_COL:
				$msCode .= "js_resize_col(\"$maElems[0]\",\"$maElems[1]\",\"$maAttr[0]\",\"$maAttr[1]\",\"$maAttr[2]\");";
				break;
			case JS_POPULATE_GRID:
				$msCode .= "obj{$this->csElements}.js_populate_grid(\"{$this->csParameters}\");";
				break;
			case JS_GRID_EVENT:
				$msCode .= "obj{$this->csElements}.js_grid_event(\"{$this->csParameters}\");";
				break;
			case JS_GRID_CHANGEPAGENEXT:
				$msCode .= "obj{$this->csElements}.js_populate_grid_changepage(\"{$this->csParameters}\");";
				break;
			case JS_GRID_CHANGEPAGEBACK:
				$msCode .= "obj{$this->csElements}.js_populate_grid_changepage(\"{$this->csParameters}\");";
				break;
			case JS_GRID_CHANGEPAGEFIRST:
				$msCode .= "obj{$this->csElements}.js_populate_grid_changepage(\"{$this->csParameters}\");";
				break;
			case JS_GRID_CHANGEPAGELAST:
				$msCode .= "obj{$this->csElements}.js_populate_grid_changepage(\"{$this->csParameters}\");";
				break;
			case JS_GRID_REFRESH:
				$msCode .= "obj{$this->csElements}.js_populate_grid_changepage(\"{$this->csParameters}\");";
				break;
			case JS_ACTIVATE_TAB:
				$msCode .= "js_activate_tab(\"{$this->csElements}\",\"{$this->csParameters}\");";
				break;
			case JS_SAVE_USER_PREFS:
				$msCode.= "js_save_user_prefs(\"$maElems[0]\",\"$maAttr[0]\");";
				break;
			case JS_OPEN:
				$msCode.= "js_open(\"$maAttr[0]\",\"$maAttr[1]\",\"$maAttr[2]\",\"$maAttr[3]\",\"$maAttr[4]\");";
				break;
			case JS_TOOLTIP:
				$msCode.= "{$this->csElements} return escape(\"{$this->csParameters}\");";
				break;
			case JS_SUBMIT:
				if(isset($maAttr[1])){
					$msCode.= "js_submit(\"$maAttr[0]\",\"$maAttr[1]\");";
				}elseif(isset($maAttr[0])){
					$msCode.= "js_submit(\"$maAttr[0]\");";
				}else{
					$msCode.= "js_submit();";
				}
				break;
			case JS_MENU_ACTION_TARGET:
				$msCode.= "js_show_menu_action_target(\"$maElems[0]\");";
				break;
			case JS_INPUT_HIDDEN_VALUE:
				$msCode.= " js_show_grid_input_value(\"$maElems[0]\");";
				break;
			case JS_REQUIRED_CHECK_AJAX:
				$msCode.= "js_required_check_ajax(\"{$this->csElements}\",\"$this->csParameters\");";
				break;
			case JS_REQUIRED_CHECK_CLIENT:
				$msCode.= "js_required_check_client(\"{$this->csElements}\",\"$this->csParameters\");";
				break;
			case JS_REQUIRED_CHECK_SERVER:
				$msCode.= "js_required_check_server(\"{$this->csElements}\",\"$this->csParameters\");";
				break;
			case JS_CHANGE_CLASS:
				$msCode .= "js_change_class(\"$maElems[0]\",\"$maAttr[0]\");";
				break;
			case JS_GRID_CORRECT_SELECTED_ROWS:
				$msCode .= "js_grid_correct_selected_rows(\"$maElems[0]\")";
				break;
			case JS_ELEMENT_IN_CONTROLLER:
				$msCode .= "js_element_in_controller(\"$maElems[0]\",\"$maElems[1]\")";
				break;
			case JS_HAS_POPUP_BLOCKER:
				$msCode .= "js_has_popup_blocker()";
				break;
			case JS_CLEAR_SELECT:
				$msCode .= "js_clear_select(\"{$this->csElements}\")";
				break;
			case JS_POPULATE_SELECT:
				$msCode .= "js_populate_select(\"{$this->csElements}\",\"$this->csParameters\")";
				break;
			case JS_HAS_CHANGED:
				$msCode .= "if(!js_has_changed(\"$maElems[0]\")) return false;";
				break;
			case JS_REPLACE:
				$msCode .= "js_replace(\"{$this->csElements}\",\"$maAttr[0]\",\"$maAttr[1]\")";
				break;
			case JS_DOWNLOAD:
				$msCode.= "location=\"$maAttr[0]\";";
				break;
		}
		return $msCode;
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

define("MASK_NONE", 0);
define("MASK_DD_MM_YYYY_1", 1);
define("MASK_MM_DD_YYYY", 2);
define("MASK_DD_MM_YYYY_2", 3);
define("MASK_HH_MM_SS", 4);
define("MASK_HH_MM", 5);
define("MASK_INTEGER", 6);
define("MASK_FLOAT", 7);
define("MASK_DIGIT", 8);
define("MASK_LETTER", 9);
define("MASK_EMAIL", 10);
define("MASK_CEP", 11);
define("MASK_MONEY", 12);
define("MASK_IP", 13);

/**
 * Classe FWDMask. Implementa M�scaras para valida��o de entradas.
 *
 * <p>Classe que implementa m�scaras para valida��o de entradas (data,
 * hor�rio, valores, email, cep, etc).</p>
 * @package FWD5
 * @subpackage event
 */
class FWDMask {

	/**
	 * Constante que representa a m�scara
	 * @var integer
	 * @access protected
	 */
	protected $ciType;

	/**
	 * Evento javascript que dispara a valida��o da entrada (atrav�s da m�scara)
	 * @var string
	 * @access protected
	 */
	protected $csEvent = "OnBlur";

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDMask.</p>
	 * @access public
	 */
	public function __construct() {}

	/**
	 * Seta a m�scara.
	 *
	 * <p>Seta o atributo de m�scara (traduz a m�scara passada
	 * como par�metro de string para sua respectiva constante).</p>
	 * @access public
	 * @param string $psType M�scara
	 */
	public function setAttrType($psType) {
		$this->ciType = $this->translateStringToConstant($psType);
	}

	/**
	 * Seta o tipo da m�scara.
	 *
	 * <p>Seta o tipo da m�scara.</p>
	 * @access public
	 * @param integer $piType Constante de tipo de m�scara
	 */
	public function setType($piType){
		$this->ciType = $piType;
	}

	/**
	 * Retorna a m�scara.
	 *
	 * <p>Retorna a constante inteira referente a m�scara.</p>
	 * @access public
	 * @return integer M�scara
	 */
	public function getAttrType() {
		return $this->ciType;
	}

	/**
	 * Retorna o evento que valida a entrada utilizando a m�scara.
	 *
	 * <p>Retorna o evento que ir� disparar a valida��o da entrada, utilizando
	 * a m�scara.</p>
	 * @access public
	 * @return string Evento que realiza a valida��o da entrada
	 */
	public function getAttrEvent() {
		return $this->csEvent;
	}

	/**
	 * Retorna a express�o regular da m�scara.
	 *
	 * <p>Retorna a express�o regular da m�scara.</p>
	 * @access public
	 * @return string Express�o regular da m�scara
	 */
	public function getRegExp(){
		switch($this->getAttrType()){
			case MASK_DD_MM_YYYY_1:  // 31/12/2006
				return "/^(((3[01]|[0-2]\d)\/(0[13578]|10|12)|(30|[0-2]\d)\/(0[469]|11)|([01]\d|2[0-8])\/02)\/\d{4})|(29\/02\/((\d\d([2468][048]|0[48]|[13579][26]))|((([02468][048])|([13579][26]))00)))$/";
			case MASK_MM_DD_YYYY:  // 12/31/2006
				return "/^(((0[13578]|10|12)\/(3[01]|[0-2]\d)|(0[469]|11)\/(30|[0-2]\d)|02\/([01]\d|2[0-8]))\/\d{4})|(02\/29\/((\d\d([2468][048]|0[48]|[13579][26]))|((([02468][048])|([13579][26]))00)))$/";
			case MASK_DD_MM_YYYY_2:  // 31-Dec-2006
				return "/^([0-3]\d)\/([a-zA-Z]{3})\/(\d{4})$/";
			case MASK_HH_MM_SS :  // 23:59:59
				return  "/^([01]\d|2[0-3]):([0-5]\d):([0-5]\d)$/";
			case MASK_HH_MM:    // 23:59
				return  "/^([01]\d|2[0-3]):([0-5]\d)$/";
			case MASK_INTEGER:    // ...-2,-1,0,1,2...
				return  "/^(-{0,1}\d+)$/";
			case MASK_FLOAT:    // 0.3, -12.34, 1234.67, ...
				return "/^-?\d+(\.\d+)?$/";
			case MASK_DIGIT:    // 0,1,2,3,4...
				return  "/^(\d+)$/";
			case MASK_LETTER:    // a,b,c,...,z A,B,C,...,Z
				return "/^([A-Za-z]+)$/";
			case MASK_EMAIL:    // fulano.ciclano_outro-nome@fula-no.mail.com
				return  "/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/";
			case MASK_CEP:      // 91482-010
				return  "/^(\d){5}-(\d){3}$/";
			case MASK_MONEY: //2,445,200.14 || 1,000 || 50 ...
				return "/^\d{1,3}(,\d{3})*(\.\d\d){0,1}$/";
			case MASK_IP: // 123.123.123.123
				return "/(([1]?\d\d)|(2[0-4]\d)|(25[0-5]))\.(([1]?\d\d)|(2[0-4]\d)|(25[0-5]))\.(([1]?\d\d)|(2[0-4]\d)|(25[0-5]))\.(([1]?\d\d)|(2[0-4]\d)|(25[0-5]))$/";
			default:
				return "";
		}
	}

	/**
	 * Realiza a valida��o da entrada atrav�s da m�scara.
	 *
	 * <p>Re�ne em uma vari�vel string, contendo c�digo HTML/javascript,
	 * todas as informa��es necess�rias para implementar a valida��o da
	 * entrada atrav�s da m�scara.</p>
	 * @access public
	 * @param string $psElem Id do elemento
	 * @return string String que ir� disparar a valida��o da entrada
	 */
	public function render($psElem) {
		$msMask = $this->getRegExp();

		$msOut  = "elem=gebi(\"$psElem\"); re=$msMask; ";

		switch($this->getAttrType()){
			case MASK_DD_MM_YYYY_1:  // 31/12/2006
				$msOut .= " if(!re.exec(elem.value))";
				$msOut .= " if (elem.value.length>7) {";
				$msOut .= "   var temp1 = elem.value.slice(0,2);";
				$msOut .= "   var temp2 = elem.value.slice(2,4);";
				$msOut .= "   var temp3 = elem.value.slice(4,8);";
				$msOut .= "   var temp = \"\".concat(temp1,\"/\",temp2,\"/\",temp3);";
				$msOut .= "   if(re.exec(temp))";
				$msOut .= "     elem.value = temp;";
				$msOut .= " }";
				break;
			case MASK_MM_DD_YYYY:  // 12/31/2006
				$msOut .= " if(!re.exec(elem.value))";
				$msOut .= " if (elem.value.length>7) {";
				$msOut .= "   var temp1 = elem.value.slice(0,2);";
				$msOut .= "   var temp2 = elem.value.slice(2,4);";
				$msOut .= "   var temp3 = elem.value.slice(4,8);";
				$msOut .= "   var temp = \"\".concat(temp1,\"/\",temp2,\"/\",temp3);";
				$msOut .= "   if(re.exec(temp))";
				$msOut .= "     elem.value = temp;";
				$msOut .= " }";
				break;
			case MASK_DD_MM_YYYY_2:  // 31-Dec-2006
				$msOut .= " if(!re.exec(elem.value))";
				$msOut .= " if (elem.value.length>8) {";
				$msOut .= "   var temp1 = elem.value.slice(0,2);";
				$msOut .= "   var temp2 = elem.value.slice(2,5);";
				$msOut .= "   var temp3 = elem.value.slice(5,9);";
				$msOut .= "   var temp = \"\".concat(temp1,\"-\",temp2,\"-\",temp3);";
				$msOut .= "   if(re.exec(temp))";
				$msOut .= "     elem.value = temp;";
				$msOut .= " }";
				break;
			case MASK_HH_MM_SS :  // 23:59:59
				$msOut .= " if(!re.exec(elem.value))";
				$msOut .= " if (elem.value.length>5) {";
				$msOut .= "   var temp1 = elem.value.slice(0,2);";
				$msOut .= "   var temp2 = elem.value.slice(2,4);";
				$msOut .= "   var temp3 = elem.value.slice(4,6);";
				$msOut .= "   var temp = \"\".concat(temp1,\":\",temp2,\":\",temp3);";
				$msOut .= "   if(re.exec(temp))";
				$msOut .= "     elem.value = temp;";
				$msOut .= " }";
				break;
			case MASK_HH_MM:    // 23:59
				$msOut .= " if(!re.exec(elem.value))";
				$msOut .= " if (elem.value.length>3) {";
				$msOut .= "   var temp1 = elem.value.slice(0,2);";
				$msOut .= "   var temp2 = elem.value.slice(2,4);";
				$msOut .= "   var temp = \"\".concat(temp1,\":\",temp2);";
				$msOut .= "   if(re.exec(temp))";
				$msOut .= "     elem.value = temp;";
				$msOut .= " }";
				break;
			case MASK_INTEGER:    // ...-2,-1,0,1,2...
				break;
			case MASK_FLOAT:    // 0.3, -12.34, 1234.67, ...
				break;
			case MASK_DIGIT:    // 0,1,2,3,4...
				break;
			case MASK_LETTER:    // a,b,c,...,z A,B,C,...,Z
				break;
			case MASK_EMAIL:    // fulano.ciclano_outro-nome@fula-no.mail.com
				break;
			case MASK_CEP:      // 91482-010
				$msOut .= " if(!re.exec(elem.value))";
				$msOut .= " if (elem.value.length>7) {";
				$msOut .= "   var temp1 = elem.value.slice(0,5);";
				$msOut .= "   var temp2 = elem.value.slice(5,8);";
				$msOut .= "   var temp = \"\".concat(temp1,\"-\",temp2);";
				$msOut .= "   if(re.exec(temp))";
				$msOut .= "     elem.value = temp;";
				$msOut .= " }";
				break;
			case MASK_MONEY:
				$msOut .= "if(!re.exec(elem.value)) {";
				$msOut .= "  value = elem.value;";
				$msOut .= "  value = value.toString().replace(\",\",\"\",\"g\");";
				$msOut .= "  if(isNaN(value))  value = \"0\";";
				$msOut .= "  value = Math.floor(value*100+0.50000000001);";
				$msOut .= "  cents = value%100;";
				$msOut .= "  value = Math.floor(value/100).toString();";
				$msOut .= "  if(cents<10)  cents = \"0\" + cents;";
				$msOut .= "  for (var i = 0; i < Math.floor((value.length-(1+i))/3); i++)";
				$msOut .= "    value = value.substring(0,value.length-(4*i+3))+\",\"+value.substring(value.length-(4*i+3));";
				$msOut .= "  value += \".\" + cents;";
				$msOut .= "}";
				break;
			case MASK_IP:
				break;
			default:
				$msOut .= "";
				break;
		}

		$msOut .= " if(!re.exec(elem.value)) {";
		$msOut .= "   elem.style.borderRightColor = \"#FF0000\";";
		$msOut .= "   elem.style.borderWidth = \"1px\";";
		$msOut .= " }";
		$msOut .= " else {";
		$msOut .= "   elem.style.borderRightColor = \"#CCCCCC\";";
		$msOut .= "   elem.style.borderWidth = \"1px\";";
		$msOut .= " }";
		$msOut .= " return;";
		return $msOut;
	}

	/**
	 * Traduz a m�scara de string para sua respectiva constante.
	 *
	 * <p>Traduz a string referente a m�scara passada como par�metro
	 * em sua respectiva constante inteira.</p>
	 * @access public
	 * @param string $psString M�scara (string)
	 * @return integer M�scara (integer)
	 */
	public function translateStringToConstant($psString) {
		switch($psString){
			case "dd/mm/yyyy":
				return MASK_DD_MM_YYYY_1;
			case "mm/dd/yyyy":
				return MASK_MM_DD_YYYY;
			case "dd/MM/yyyy":
				return MASK_DD_MM_YYYY_2;
			case "hh:mm:ss":
				return MASK_HH_MM_SS;
			case "hh:mm":
				return MASK_HH_MM;
			case "integer":
				return MASK_INTEGER;
			case "float":
				return MASK_FLOAT;
			case "digit":
				return MASK_DIGIT;
			case "letter":
				return MASK_LETTER;
			case "email":
				return MASK_EMAIL;
			case "cep":
				return MASK_CEP;
			case "money":
				return MASK_MONEY;
			case "ip":
				return MASK_IP;
			default:
				return MASK_NONE;
		}
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDMaskAutoComplete. Implementa a funcionalidade 'AutoCompletar'.
 *
 * <p>Classe que implementa a funcionalidade 'AutoCompletar' de entradas
 * utilizando m�scaras. A verifica��o/valida��o da entrada atrav�s da m�scara
 * � realizada sempre que uma tecla � pressionada.</p>
 * @package FWD5
 * @subpackage event
 */
class FWDMaskAutoComplete extends FWDMask {

	/**
	 * Evento javascript que dispara a valida��o da entrada (atrav�s da m�scara)
	 * @var string
	 * @access protected
	 */
	protected $csEvent = "OnKeyPress";

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDMaskAutoComplete.</p>
	 * @access public
	 */
	public function __construct() {
	}

	/**
	 * Realiza a verifica��o/valida��o da entrada atrav�s da m�scara.
	 *
	 * <p>Re�ne em uma vari�vel string, contendo c�digo HTML/javascript,
	 * todas as informa��es necess�rias para implementar a funcionalidade
	 * AutoCompletar (valida��o da entrada atrav�s da m�scara a cada tecla
	 * pressionada).</p>
	 * @access public
	 * @param string $psElem Id do elemento
	 * @return string String que ir� disparar a valida��o da entrada
	 */
	public function render($psElem) {
		switch($this->getAttrType()){
			case MASK_DD_MM_YYYY_1:  // 31/12/2006
				$msCode  = "e=event;";
				$msCode .= "if(e.keyCode==e.which || e.which==0) return true;";
				$msCode .= "kc=e.keyCode?e.keyCode:e.which;";
				$msCode .= "if(String.fromCharCode(kc)==\" \") return false;";
				$msCode .= "if(String.fromCharCode(kc)!=0)";
				$msCode .= "if(!parseInt(String.fromCharCode(kc))) {";
				$msCode .= "  return false;";
				$msCode .= "}";
				$msCode .= "value=gebi(\"$psElem\").value;";
				$msCode .= "if(value.length==2||value.length==5) {";
				$msCode .= "  gebi(\"$psElem\").value += \"/\";";
				$msCode .= "}";
				$msCode .= "if(value.length > 9) {";
				$msCode .= "  return false;";
				$msCode .= "}";
				$msCode .= str_replace(array("\n","\r"),array(' ',' '),"
        d = String.fromCharCode(kc);
        
        daysInMonth=function(month,year){
          if(month==4||month==6||month==9||month==11){
            return 30;
          }else if(month==2){
            if(!year||year%400==0||(year%4==0&&year%100)){
              return 29;
            }else{
              return 28;
            }
          }else{
            return 31;
          }
        };
        
        var a = value.split(\"/\");
        if(a.length==1){
          day = a[0]+d;
          if(a[0].length==0){
            if(d>3) return false;
          }else{
            if(day>31||day==\"00\") return false;
          }
        }else if(a.length==2){
          day = a[0];
          month = a[1]+d;
          if(month.length==1&&month>1) return false;
          if(month==\"00\"||month>12) return false;
          if(daysInMonth(month)<day) return false;
        }else{
          day = a[0];
          month = a[1];
          year = a[2]+d;
          if(year.length==4&&daysInMonth(month,year)<day) return false;
        }
        
        ");
				break;
			case MASK_MM_DD_YYYY:  // 12/31/2006
				$msCode  = "e=event;";
				$msCode .= "if(e.keyCode==e.which || e.which==0) return true;";
				$msCode .= "kc=e.keyCode?e.keyCode:e.which;";
				$msCode .= "if(String.fromCharCode(kc)==\" \") return false;";
				$msCode .= "if(String.fromCharCode(kc)!=0)";
				$msCode .= "if(!parseInt(String.fromCharCode(kc))) {";
				$msCode .= "  return false;";
				$msCode .= "}";
				$msCode .= "value=gebi(\"$psElem\").value;";
				$msCode .= "if(value.length==2||value.length==5) {";
				$msCode .= "  gebi(\"$psElem\").value += \"/\";";
				$msCode .= "}";
				$msCode .= "if(value.length > 9) {";
				$msCode .= "  return false;";
				$msCode .= "}";
				$msCode .= str_replace(array("\n","\r",'  '),array(' ',' ',' '),"
        d = String.fromCharCode(kc);
        
        daysInMonth=function(month,year){
          if(month==4||month==6||month==9||month==11){
            return 30;
          }else if(month==2){
            if(!year||year%400==0||(year%4==0&&year%100)){
              return 29;
            }else{
              return 28;
            }
          }else{
            return 31;
          }
        };
        
        var a = value.split(\"/\");
        if(a.length==1){
          month = a[0]+d;
          if(a[0].length==0){
            if(d>1) return false;
          }else{
            if(month>12||month==\"00\") return false;
          }
        }else if(a.length==2){
          month = a[0];
          day = a[1]+d;
          if(day==\"00\") return false;
          if(day.length==1&&day>daysInMonth(month)/10) return false;
          if(daysInMonth(month)<day) return false;
        }else{
          month = a[0];
          day = a[1];
          year = a[2]+d;
          if(year.length==4&&daysInMonth(month,year)<day) return false;
        }
        
        ");
				break;
			case MASK_DD_MM_YYYY_2:  // 31-Dec-2006
				$msCode  = "e=event;";
				$msCode .= "if(e.keyCode==e.which || e.which==0) return true;";
				$msCode .= "kc=e.keyCode?e.keyCode:e.which;";
				$msCode .= "if(parseInt(String.fromCharCode(kc)) && (value.length==2 || value.length==3)) {";
				$msCode .= "  return false;";
				$msCode .= "}";
				$msCode .= "if(!parseInt(String.fromCharCode(kc))&& (value.length<2 || value.length>5))";
				$msCode .= "  return false;";
				$msCode .= "value=gebi(\"$psElem\").value;";
				$msCode .= "if(value.length==2||value.length==6) {";
				$msCode .= "  gebi(\"$psElem\").value += \"/\";";
				$msCode .= "}";
				$msCode .= "if(value.length > 10) {";
				$msCode .= "  return false;";
				$msCode .= "}";
				break;
			case MASK_HH_MM_SS:    // 23:59:59
				$msCode  = "e=event;";
				$msCode .= "if(e.keyCode==e.which || e.which==0) return true;";
				$msCode .= "kc=e.keyCode?e.keyCode:e.which;";
				$msCode .= "chr=String.fromCharCode(kc);";
				$msCode .= "if(chr==\" \") return false;";
				$msCode .= "if(chr!=0)";
				$msCode .= "if(!parseInt(chr))return false;";
				$msCode .= "if(value.length==0 && chr>\"2\")return false;";
				$msCode .= "if(value==2 && chr>\"3\")return false;";
				$msCode .= "value=gebi(\"$psElem\").value;";
				$msCode .= "if(value.length==2||value.length==5||value.length==3||value.length==6){";
				$msCode .= "  if(chr>\"5\"){return false}";
				$msCode .= "  else if(value.length==2||value.length==5){gebi(\"$psElem\").value += \":\";}";
				$msCode .= "}else if(value.length>7)return false;";
				break;
			case MASK_HH_MM:    // 23:59
				$msCode  = "e=event;";
				$msCode .= "if(e.keyCode==e.which || e.which==0) return true;";
				$msCode .= "kc=e.keyCode?e.keyCode:e.which;";
				$msCode .= "chr=String.fromCharCode(kc);";
				$msCode .= "if(chr==\" \") return false;";
				$msCode .= "if(chr!=0)";
				$msCode .= "if(!parseInt(chr))return false;";
				$msCode .= "if(value.length==0 && chr>\"2\")return false;";
				$msCode .= "if(value==2 && chr>\"3\")return false;";
				$msCode .= "value=gebi(\"$psElem\").value;";
				$msCode .= "if(value.length==2||value.length==3){";
				$msCode .= "  if(chr>\"5\"){return false}";
				$msCode .= "  else if(value.length==2){gebi(\"$psElem\").value += \":\";}";
				$msCode .= "}else if(value.length>4)return false;";
				break;
			case MASK_INTEGER:    // ...-2,-1,0,1,2...
				$msCode  = "e=event;";
				$msCode .= "if(e.keyCode==e.which || e.which==0) return true;";
				$msCode .= "kc=e.keyCode?e.keyCode:e.which;";
				$msCode .= "if(String.fromCharCode(kc)==\"-\" && value.length==0)";
				$msCode .= "  return;";
				$msCode .= "if(String.fromCharCode(kc)==\" \") return false;";
				$msCode .= "if(String.fromCharCode(kc)!=0)";
				$msCode .= "if(!parseInt(String.fromCharCode(kc))) {";
				$msCode .= "  return false;";
				$msCode .= "}";
				break;
			case MASK_FLOAT:    // 0.3, -12.34, 1234.67, ...
				$msCode  = "e=event;";
				$msCode .= "if(e.keyCode==e.which || e.which==0) return true;";
				$msCode .= "kc=e.keyCode?e.keyCode:e.which;";
				$msCode .= "if(String.fromCharCode(kc)==\"-\" && value.length==0)";
				$msCode .= "  return;";
				$msCode .= "if(String.fromCharCode(kc)==\".\" && value.indexOf(\".\")== -1)";
				$msCode .= "  return;";
				$msCode .= "if(String.fromCharCode(kc)==\" \") return false;";
				$msCode .= "if(String.fromCharCode(kc)!=0)";
				$msCode .= "if(!parseInt(String.fromCharCode(kc))) {";
				$msCode .= "  return false;";
				$msCode .= "}";
				break;
			case MASK_DIGIT:    // 0,1,2,3,4...
				$msCode  = "e=event;";
				$msCode .= "if(e.keyCode==e.which || e.which==0) return true;";
				$msCode .= "kc=e.keyCode?e.keyCode:e.which;";
				$msCode .= "if(String.fromCharCode(kc)==\" \") return false;";
				$msCode .= "if(String.fromCharCode(kc)!=0)";
				$msCode .= "if(!parseInt(String.fromCharCode(kc))) {";
				$msCode .= "  return false;";
				$msCode .= "}";
				break;
			case MASK_LETTER:    // a,b,c,...,z A,B,C,...,Z
				$msCode  = "e=event;";
				$msCode .= "if(e.keyCode==e.which || e.which==0) return true;";
				$msCode .= "kc=e.keyCode?e.keyCode:e.which;";
				$msCode .= "if(String.fromCharCode(kc)==0) return false;";
				$msCode .= "if(e.ctrlKey && kc==118) return false;";
				$msCode .= "if(parseInt(String.fromCharCode(kc))) {";
				$msCode .= "  return false;";
				$msCode .= "}";
				break;
			case MASK_EMAIL:    // fulano@fulanomail.com
				$msCode  = "e=event;";
				$msCode .= "if(e.keyCode==e.which || e.which==0) return true;";
				$msCode .= "kc=e.keyCode?e.keyCode:e.which;";
				$msCode .= "value=gebi(\"$psElem\").value;";
				$msCode .= "if(e.ctrlKey && kc==118) return false;";
				$msCode .= "if(String.fromCharCode(kc)==\"@\" && value.length < 1) {";
				$msCode .= "  return false;";
				$msCode .= "}";
				$msCode .= "if( (kc<45) || (kc==47) || (kc>57 && (kc<64)) || (kc>90 && kc<97 && kc!=95) || (kc>122 && kc<128)){";
				$msCode .= "  return false;";
				$msCode .= " }";
				$msCode .= "if(String.fromCharCode(kc)==\"@\" && value.indexOf(\"@\") > -1){";
				$msCode .= "  return false;";
				$msCode .= "}";
				break;
			case MASK_CEP:      // 91482-010
				$msCode  = "e=event;";
				$msCode .= "if(e.keyCode==e.which || e.which==0) return true;";
				$msCode .= "kc=e.keyCode?e.keyCode:e.which;";
				$msCode .= "if(String.fromCharCode(kc)==\" \") return false;";
				$msCode .= "if(String.fromCharCode(kc)!=0)";
				$msCode .= "if(!parseInt(String.fromCharCode(kc))) {";
				$msCode .= "  return false;";
				$msCode .= "}";
				$msCode .= "value=gebi(\"$psElem\").value;";
				$msCode .= "if(value.length==5) {";
				$msCode .= "  gebi(\"$psElem\").value += \"-\";";
				$msCode .= "}";
				$msCode .= "if(value.length > 8) {";
				$msCode .= "  return false;";
				$msCode .= "}";
				break;
			case MASK_MONEY:
				$msCode  = "
                    e=event;
                    value=gebi(\"$psElem\").value;
                    //pro�be o uso de setas e da tecla delete.                    
                    if(e.keyCode==37 || e.keyCode==39 || e.keyCode==46) return false;
                    if(e.keyCode!=8 && (e.keyCode==e.which || e.which==0)) return true;
                    value = value.replace(/[\.,]/g,\"\"); //remove dots and commas
                    value=value.replace(/^0*/,\"\"); //remove leading zeros
                    if (e.keyCode==8) {
                      tam = value.length;
                      if (tam==3) value=\"0.\" + value;
                      else if (tam==2) value=\"0.0\"+value;
                      else if (tam==1 || tam==0) value=\"0.000\";
                      else {                      
                        value=value.substring(0,tam-3) + \".\" + value.substring(tam-3);
                        tam = tam - 4;
                        for (i=tam;i>0;i--) {
                          if ((tam-i) > 1 && (tam-i)%3==2)
                            value=value.substring(0,i) + \",\" + value.substring(i);
                        }
                      }
                    } else {
                      kc=e.keyCode?e.keyCode:e.which;
                      if(String.fromCharCode(kc)==\" \") return false;
                      if(String.fromCharCode(kc)!=0 && !parseInt(String.fromCharCode(kc)))
                        return false;
                      value = value.replace(/[\.,]/g,\"\"); //remove dots and commas
                      value=value.replace(/^0*/,\"\"); //remove leading zeros
                      tam = value.length;
                      if (tam==0) value=\"0.0\";
                      else if (tam==1) value=\"0.\" + value;
                      else {
                        value=value.substring(0,tam-1) + \".\" + value.substring(tam-1);
                        tam = tam - 2;
                        for (i=tam;i>0;i--) {
                          if ((tam-i) > 1 && (tam-i)%3==2)
                            value=value.substring(0,i) + \",\" + value.substring(i);
                        }
                      }
                    }
                   ";
				break;
			case MASK_IP:
				$msCode  = "
      				e=event;
        			if(e.keyCode==e.which || e.which==0) return true;
        			kc=e.keyCode?e.keyCode:e.which;
        			if(String.fromCharCode(kc)==\" \") return false;
        			if(String.fromCharCode(kc)!=0){
        				if(!(parseInt(String.fromCharCode(kc))) && String.fromCharCode(kc) != \".\") {
			         	 	return false;
        				}
        			}
        			value=gebi(\"$psElem\").value;
        			values = value.split(\".\");
        			if (String.fromCharCode(kc) == \".\" && values.length > 3) return false;
        			var string = value + String.fromCharCode(kc);
        			if (values.length < 4 && values[values.length -1].length == 3 && String.fromCharCode(kc) != \".\"){
         				gebi(\"$psElem\").value += \".\";
        			}
        			return true;
        			";
				break;
			default:
				$msCode  = "alert(\"Oops... Invalid mask type.\");";
				break;
		}
		$msOut ="$msCode";
		return $msOut;
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDStartEvent. Singleton. Manipula eventos criados pelo usu�rio.
 *
 * <p>Classe singleton que manipula eventos criados pelo usu�rio na Framework FWD5.</p>
 * @package FWD5
 * @subpackage event
 */

//define as constantes de prioridade do tipo de evento beforeEvent
define( "HIGH",1);
define( "ABOVE_MIDDLE",2);
define( "MIDDLE",3);
define( "BELOW_MIDDLE",4);
define( "LOW",5);
class FWDStartEvent {

	/**
	 * Inst�ncia �nica da classe FWDStartEvent
	 * @staticvar FWDStartEvent
	 * @access private
	 */
	private static $coStartEvent = NULL;

	/**
	 * Estrutura que armazena os eventos ajax
	 * @var array
	 * @access protected
	 */
	protected $caAjaxEvent = array();

	/**
	 * Estrutura que armazena os eventos de submit
	 * @var array
	 * @access protected
	 */
	protected $caSubmitEvent = array();

	/**
	 * Estrutura que armazena os eventos que devem ser executados antes dos eventos ajax e submit
	 * @var array
	 * @access protected
	 */
	protected $caBeforeEvent = array();

	/**
	 * Estrutura que armazena os eventos que devem ser executados caso nao exista evento ajax e de submit
	 * a serem executados
	 * @var array
	 * @access protected
	 */
	protected $coAfterEvent = null;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDStartEvent.</p>
	 * @access private
	 */
	private function __construct() {}

	/**
	 * Retorna a inst�ncia �nica da classe FWDStartEvent (singleton).
	 *
	 * <p>Retorna a inst�ncia �nica da classe FWDStartEvent. Se a inst�ncia
	 * ainda n�o existir, cria a �nica inst�ncia. Se a inst�ncia j� existir,
	 * retorna a �nica inst�ncia (singleton).</p>
	 * @access public
	 * @return FWDStartEvent Inst�ncia �nica da classe FWDStartEvent
	 * @static
	 */
	public static function getInstance() {
		if (self::$coStartEvent == NULL) {
			self::$coStartEvent = new FWDStartEvent();
		}
		else {}	// inst�ncia �nica j� existe, retorne-a
		return self::$coStartEvent;
	}

	/**
	 * Exibe o conte�do da inst�ncia �nica FWDStartEvent.
	 *
	 * <p>Exibe na tela o conte�do de todos os atributos da �nica inst�ncia
	 * da classe FWDStartEvent.</p>
	 * @access public
	 */
	public function dump_instance() {
		echo "<br/><br/><br/><br/>--------------[ FWDStartEvent Singleton Dump <strong>BEGIN</strong> ]--------------<br/>";
		echo "<h3>coStartEvent:</h3>";
		var_dump(self::$coStartEvent);
		echo "<br/><h3>caEventArray:</h3>";
		var_dump($this->caAjaxEvent);
		echo "<br/><h3>caSubmitEvent:</h3>";
		var_dump($this->caSubmitEvent);
		echo "<br/><h3>caBeforeEventEvent:</h3>";
		var_dump($this->caBeforeEventEvent);
		echo "<br/><h3>caAfterEvent:</h3>";
		var_dump($this->caAfterEvent);

		echo "<h3></h3>--------------[ FWDStartEvent Singleton Dump <strong>END</strong> ]--------------<br/><br/><br/><br/>";
	}

	/**
	 * Adiciona um evento ajax criado pelo usu�rio.
	 *
	 * <p>Adiciona um evento ajax criado pelo usu�rio.</p>
	 * @access public
	 * @param object $poEvent Evento ajax criado pelo usu�rio
	 */
	public function addAjaxEvent(FWDRunnable $poEvent) {
		$this->caAjaxEvent[strtolower($poEvent->getAttrIdentifier())] = $poEvent;
		if(!(FWDWebLib::getObject($poEvent->getAttrIdentifier())))
		FWDWebLib::addObject($poEvent->getAttrIdentifier(), $poEvent);
	}

	/**
	 * Seta o evento de tela definido pelo usu�rio.
	 *
	 * <p>Seta o evento de tela definido pelo usu�rio</p>
	 * @access public
	 * @param object $poEvent Evento ajax criado pelo usu�rio
	 */
	public function addBeforeAjax(FWDRunnable $poEvent,$piPriority = MIDDLE) {
		FWDWebLib::attributeParser($piPriority,array("1"=>HIGH, "2"=>ABOVE_MIDDLE, "3"=>MIDDLE, "4"=>BELOW_MIDDLE, "5"=>LOW, ),"piPriority");
		if(!isset($this->caBeforeEvent[$piPriority])){
			$maRow = array();
			$maRow[strtolower($poEvent->getAttrIdentifier())] = $poEvent;
			$this->caBeforeEvent[$piPriority] =  $maRow;
		}else{
			$this->caBeforeEvent[$piPriority][strtolower($poEvent->getAttrIdentifier())] = $poEvent;
		}
		if(!(FWDWebLib::getObject($poEvent->getAttrIdentifier())))
		FWDWebLib::addObject($poEvent->getAttrIdentifier(), $poEvent);
	}

	/**
	 * Seta o evento de tela definido pelo usu�rio.
	 *
	 * <p>Seta o evento de tela definido pelo usu�rio</p>
	 * @access public
	 * @param object $poEvent Evento ajax criado pelo usu�rio
	 */
	public function setScreenEvent(FWDRunnable $poEvent) {
		$this->coAfterEvent = $poEvent;
		if(!(FWDWebLib::getObject($poEvent->getAttrIdentifier())))
		FWDWebLib::addObject($poEvent->getAttrIdentifier(), $poEvent);
	}

	/**
	 * Adiciona um evento submit criado pelo usu�rio.
	 *
	 * <p>Adiciona um evento submit criado pelo usu�rio.</p>
	 * @access public
	 * @param object $poEvent Evento submit criado pelo usu�rio
	 */
	public function addSubmitEvent(FWDRunnable $poEvent) {
		$this->caSubmitEvent[strtolower($poEvent->getAttrIdentifier())] = $poEvent;
		if(!(FWDWebLib::getObject($poEvent->getAttrIdentifier())))
		FWDWebLib::addObject($poEvent->getAttrIdentifier(), $poEvent);
	}

	/**
	 * Retorna o nome do evento ajax que foi disparado.
	 *
	 * <p>Retorna o nome do evento ajax que foi disparado.</p>
	 * @access public
	 */
	public function getAjaxEventName(){
		return FWDWebLib::getPOST("fwd_ajax_event_name");
	}

	/**
	 * Executa os eventos criados pelo usu�rio.
	 *
	 * <p>Executa os eventos criados pelo usu�rio.</p>
	 * @access public
	 */
	public function start() {
		$mbExecute = false;
		$msEvent = FWDWebLib::getPOST("fwd_ajax_event_name");
		$miAjax = FWDWebLib::getPOST("fwd_ajax_trigger");
		$msSubmitTrigger = FWDWebLib::getPOST("fwd_submit_trigger");


		if(isset($this->caBeforeEvent[HIGH])){
			foreach($this->caBeforeEvent[HIGH] as $moEvent)
			if(isset($this->caBeforeEvent[HIGH][strtolower($msEvent)])){
				FWDWebLib::getInstance()->writeFunction2Debug(get_class($moEvent),'run',array(),FWD_DEBUG_FWD1);
				$moEvent->run();
			}
		}
		if(isset($this->caBeforeEvent[ABOVE_MIDDLE]))
		foreach($this->caBeforeEvent[ABOVE_MIDDLE] as $moEvent){
			FWDWebLib::getInstance()->writeFunction2Debug(get_class($moEvent),'run',array(),FWD_DEBUG_FWD1);
			$moEvent->run();
		}
		if(isset($this->caBeforeEvent[MIDDLE]))
		foreach($this->caBeforeEvent[MIDDLE] as $moEvent){
			FWDWebLib::getInstance()->writeFunction2Debug(get_class($moEvent),'run',array(),FWD_DEBUG_FWD1);
			$moEvent->run();
		}
		if(isset($this->caBeforeEvent[BELOW_MIDDLE]))
		foreach($this->caBeforeEvent[BELOW_MIDDLE] as $moEvent){
			FWDWebLib::getInstance()->writeFunction2Debug(get_class($moEvent),'run',array(),FWD_DEBUG_FWD1);
			$moEvent->run();
		}
		if(isset($this->caBeforeEvent[LOW]))
		foreach($this->caBeforeEvent[LOW] as $moEvent){
			FWDWebLib::getInstance()->writeFunction2Debug(get_class($moEvent),'run',array(),FWD_DEBUG_FWD1);
			$moEvent->run();
		}


		if((isset($miAjax))&&(isset($msEvent))&&(isset($this->caAjaxEvent[strtolower($msEvent)]))){
			FWDWebLib::getInstance()->writeFunction2Debug(get_class($this->caAjaxEvent[strtolower($msEvent)]),'run',array(),FWD_DEBUG_FWD1);
			$this->caAjaxEvent[strtolower($msEvent)]->run();
			$mbExecute = true;
		}

		if ($mbExecute || isset($miAjax))
		exit();

		if((!isset($miAjax)) && (isset($msSubmitTrigger)) && (isset($this->caSubmitEvent[strtolower($msSubmitTrigger)])) ){
			FWDWebLib::getInstance()->writeFunction2Debug(get_class($this->caSubmitEvent[strtolower($msSubmitTrigger)]),'run',array(),FWD_DEBUG_FWD1);
			$this->caSubmitEvent[strtolower($msSubmitTrigger)]->run();
			$mbExecute=true;
		}

		if($mbExecute)
		exit();
			
		if(isset($this->coAfterEvent)){
			FWDWebLib::getInstance()->writeFunction2Debug(get_class($this->coAfterEvent),'run',array(),FWD_DEBUG_FWD1);
			$this->coAfterEvent->run();
		}
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDToolTip. Implementa ToolTips.
 *
 * <p>Classe que implementa ToolTips em elementos da FWD5. Um ToolTip � tratado
 * como um evento onMouseOver.</p>
 * @package FWD5
 * @subpackage event
 */
class FWDToolTip extends FWDEvent {

	/**
	 * Valor do ToolTip
	 * @var FWDString
	 * @access private
	 */
	private $coString = null;

	/**
	 * Classe CSS do ToolTip (wz_tooltip.js: ttFWDClass)
	 * @var string
	 * @access private
	 */
	private $csClass = "FWDToolTip";

	/**
	 * Tempo que o ToolTip demora para aparecer [milisegundos] (wz_tooltip.js: ttDelay)
	 * @var integer
	 * @access private
	 */
	private $ciShowDelay = 0;

	/**
	 * Tempo que o ToolTip demora para desaparecer [milisegundos] (wz_tooltip.js: ttTemp)
	 * @var integer
	 * @access private
	 */
	private $ciHideDelay = 0;

	/**
	 * Transpar�cia do ToolTip ([0..100], 100=solid) (wz_tooltip.js: ttOpacity)
	 * @var integer
	 * @access private
	 */
	private $ciOpacity = 100;

	/**
	 * ToolTip nao se move com o mouse (wz_tooltip.js: ttStatic)
	 * @var boolean
	 * @access private
	 */
	private $cbStatic = false;

	/**
	 * ToolTip nao desaparece no evento onMouseOut (wz_tooltip.js: ttSticky)
	 * @var boolean
	 * @access private
	 */
	private $cbSticky = false;

	/**
	 * Largura do ToolTip, em pixels (a altura � determinada automaticamente) (wz_tooltip.js: ttWidth)
	 * @var integer
	 * @access private
	 */
	private $ciWidth = 300;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDToolTip.</p>
	 * @access public
	 */
	public function __construct() {
		$this->coString = new FWDString();
		$this->setAttrEvent("onmouseover");
		$this->setAttrFunction("tooltip");
	}

	/**
	 * Renderiza o ToolTip (se possuir valor).
	 *
	 * <p>Renderiza o ToolTip. Instancia um objeto de evento javascript
	 * com os valores necess�rios e renderiza o evento (onMouseOver).</p>
	 * @access public
	 * @return string String que ir� disparar o Evento Cliente
	 */
	public function render() {
		if ($this->coString->getAttrString()) {
			$msValue = $this->coString->getAttrString();
			$msValue = html_entity_decode($msValue);
			$msValue = str_replace(array("\n","\r"),array(" ",""),$msValue);
			$msValue = str_replace('\\','\\\\',$msValue);
			$msValue = str_replace('"','\"',$msValue);
			$msReturn = '';
			if(strlen($msValue)>60){
				for($miI=0;$miI<strlen($msValue);$miI+=50){
					$msSub = substr($msValue,$miI,60);
					if(strpos($msSub,' ')===false)
					$msReturn .= substr($msSub,0,50) . ' ';
					else
					$msReturn .= substr($msSub,0,50);
				}
			}else{
				$msReturn = $msValue;
			}
			if ($this->coString->getAttrString()===false)
			trigger_error("Object FWDToolTip must have an non-empty object FWDString",E_USER_WARNING);

			if ($this->getAttrShowDelay() < 0)
			trigger_error("Object FWDToolTip: attribute ShowDelay must be a positive integer",E_USER_ERROR);

			if ($this->getAttrHideDelay() < 0)
			trigger_error("Object FWDToolTip: attribute HideDelay must be a positive integer",E_USER_ERROR);

			if ($this->getAttrOpacity() < 0 || $this->getAttrOpacity() > 100)
			trigger_error("Object FWDToolTip: attribute Opacity must be a positive integer less than or equal to 100",E_USER_ERROR);

			$msElems = "";
			$msElems .= "ttFWDClass=\"{$this->getAttrClass()}\"; ";
			$msElems .= "ttDelay={$this->getAttrShowDelay()}; ";
			$msElems .= "ttTemp={$this->getAttrHideDelay()}; ";
			$msElems .= "ttOpacity={$this->getAttrOpacity()}; ";
			$msElems .= $this->getAttrStatic() ? "ttStatic=true; " : "ttStatic=false; ";
			$msElems .= $this->getAttrSticky() ? "ttSticky=true; " : "ttSticky=false; ";
			$msElems .= ($this->getAttrWidth()<60) ? "ttWidth=60; " : "ttWidth={$this->getAttrWidth()};";

			$moJs = new FWDJsEvent($this->getAttrFunction(),$msElems,/*htmlentities*/($msReturn));
			return $moJs->render();
		}
	}

	/**
	 * Este m�todo n�o deve fazer nada.
	 *
	 * <p>Esta classe n�o deve utilizar este m�todo.</p>
	 * @access public
	 */
	public function run() {
	}

	/**
	 * Seta o valor do ToolTip.
	 *
	 * <p>M�todo que setar o valor do ToolTip.
	 * Se force for FALSE, concatena o valor se a vari�vel tiver conte�do;
	 * se TRUE atribui mesmo que tenha conte�do (sobrescreve).</p>
	 * @access public
	 * @param string $psValue Valor do ToolTip
	 * @param boolean $pbForce For�ar o valor mesmo que j� tenha conte�do
	 */
	public function setValue($psValue,$pbForce = false) {
		$this->coString->setValue($psValue,$pbForce);
	}

	/**
	 * Seta o valor do ToolTip.
	 *
	 * <p>M�todo que setar o valor do ToolTip. Metodo criado para que o tooltip funcione da tree</p>
	 * @access public
	 * @param string $psValue Valor da posi��o
	 */
	public function setAttrPosition($psValue) {
	}

	/**
	 * Seta o tempo que o ToolTip demora para aparecer.
	 *
	 * <p>M�todo que seta o tempo que o ToolTip demora para aparecer
	 * (onMouseOver).</p>
	 * @access public
	 * @param string $psShowDelay Tempo que o ToolTip demora para aparecer
	 */
	public function setAttrShowDelay($psShowDelay) {
		$this->ciShowDelay = $psShowDelay;
	}

	/**
	 * Retorna o tempo que o ToolTip demora para aparecer.
	 *
	 * <p>M�todo que retorna o tempo que o ToolTip demora para aparecer
	 * (onMouseOver).</p>
	 * @access public
	 * @return integer Tempo que o ToolTip demora para aparecer
	 */
	public function getAttrShowDelay() {
		return $this->ciShowDelay;
	}

	/**
	 * Seta o tempo que o ToolTip demora para desaparecer.
	 *
	 * <p>M�todo que seta o tempo que o ToolTip demora para desaparecer
	 * (onMouseOut).</p>
	 * @access public
	 * @param string $psHideDelay Tempo que o ToolTip demora para desaparecer
	 */
	public function setAttrHideDelay($psHideDelay) {
		$this->ciHideDelay = $psHideDelay;
	}

	/**
	 * Retorna o tempo que o ToolTip demora para desaparecer.
	 *
	 * <p>M�todo que retorna o tempo que o ToolTip demora para desaparecer
	 * (onMouseOut).</p>
	 * @access public
	 * @return integer Tempo que o ToolTip demora para desaparecer
	 */
	public function getAttrHideDelay() {
		return $this->ciHideDelay;
	}

	/**
	 * Seta a transpar�ncia do ToolTip.
	 *
	 * <p>M�todo que seta a transpar�ncia do ToolTip. Intervalo inteiro fechado
	 * de 0 a 100: 0=M�xima transpar�ncia, 100=M�nima transpar�ncia (solid).</p>
	 * @access public
	 * @param string $psOpacity Transpar�ncia do ToolTip
	 */
	public function setAttrOpacity($psOpacity) {
		$this->ciOpacity = $psOpacity;
	}

	/**
	 * Retorna a transpar�ncia do ToolTip.
	 *
	 * <p>M�todo que retorna a transpar�ncia do ToolTip.</p>
	 * @access public
	 * @return integer Transpar�ncia do ToolTip
	 */
	public function getAttrOpacity() {
		return $this->ciOpacity;
	}

	/**
	 * Seta o atributo Static do ToolTip.
	 *
	 * <p>M�todo que seta o valor booleano do atributo Static do ToolTip.
	 * Para o ToolTip mover-se junto com o mouse, cbStatic deve conter o valor
	 * booleano FALSE. Para n�o acompanhar o movimento do mouse, o atributo
	 * deve conter o valor booleano TRUE.</p>
	 * @access public
	 * @param string $psStatic Define se o ToolTip n�o deve mover-se junto com o mouse
	 */
	public function setAttrStatic($psStatic) {
		$this->cbStatic = FWDWebLib::attributeParser($psStatic,array("true"=>true, "false"=>false),"psStatic");
	}

	/**
	 * Retorna o atributo Static do ToolTip.
	 *
	 * <p>M�todo que retorna o atributo Static do ToolTip. Retorna FALSE se
	 * o ToolTip deve mover-se junto com o mouse. Retorna TRUE caso contr�rio.</p>
	 * @access public
	 * @return boolean Indica se o ToolTip n�o deve mover-se junto com o mouse
	 */
	public function getAttrStatic() {
		return $this->cbStatic;
	}

	/**
	 * Seta o atributo Sticky do ToolTip.
	 *
	 * <p>M�todo que seta o valor booleano do atributo Sticky do ToolTip.
	 * Para o ToolTip desaparecer no evento onMouseOut, cbSticky deve conter
	 * o valor booleano FALSE. Para n�o desaparecer, o atributo deve conter
	 * o valor booleano TRUE.</p>
	 * @access public
	 * @param string $psSticky Define se o ToolTip n�o deve desaparecer no evento onMouseOut
	 */
	public function setAttrSticky($psSticky) {
		$this->cbSticky = FWDWebLib::attributeParser($psSticky,array("true"=>true, "false"=>false),"psSticky");
	}

	/**
	 * Retorna o atributo Sticky do ToolTip.
	 *
	 * <p>M�todo que retorna o atributo Sticky do ToolTip. Retorna FALSO se
	 * o ToolTip deve desaparecer no evento onMouseOut. Retorna TRUE caso contr�rio.</p>
	 * @access public
	 * @return boolean Indica se o ToolTip n�o deve desaparecer no evento onMouseOut
	 */
	public function getAttrSticky() {
		return $this->cbSticky;
	}

	/**
	 * Seta a largura do ToolTip.
	 *
	 * <p>M�todo que seta a largura do ToolTip, em pixels.</p>
	 * @access public
	 * @param string $psWidth Largura do ToolTip, em pixels
	 */
	public function setAttrWidth($psWidth) {
		$this->ciWidth = $psWidth;
	}

	/**
	 * Retorna a largura do ToolTip.
	 *
	 * <p>M�todo que retorna a largura do ToolTip, em pixels.</p>
	 * @access public
	 * @return integer Largura do ToolTip, em pixels
	 */
	public function getAttrWidth() {
		return $this->ciWidth;
	}

	/**
	 * Seta a classe CSS do ToolTip.
	 *
	 * <p>M�todo que seta a classe CSS do ToolTip.</p>
	 * @access public
	 * @param string $psClass Classe CSS do ToolTip
	 */
	public function setAttrClass($psClass) {
		$this->csClass = $psClass;
	}

	/**
	 * Retorna a classe CSS do ToolTip.
	 *
	 * <p>M�todo que retorna a classe CSS do ToolTip.</p>
	 * @access public
	 * @return string Classe CSS do ToolTip
	 */
	public function getAttrClass() {
		return $this->csClass;
	}

	/**
	 * Adiciona um valor ao ToolTip.
	 *
	 * <p>M�todo para adicionar um valor ao ToolTip. Insere
	 * (ou concatena, caso o objeto coString ja exista) um valor no ToolTip
	 * (objeto FWDString).</p>
	 * @access public
	 * @param FWDString $poString Valor a ser adicionado (concatenado) ao ToolTip
	 */
	public function setObjFWDString(FWDString $poString) {
		$this->coString->setValue($poString->getAttrString());
		$msStringId = $poString->getAttrId();
		if(!empty($msStringId))
		$this->coString->setAttrId($msStringId);
	}

	/**
	 * Adiciona um link ao valor do ToolTip.
	 *
	 * <p>M�todo para adicionar um link ao valor do ToolTip. Insere
	 * (ou concatena, caso o objeto coString ja exista) um link no valor
	 * do ToolTip (objeto FWDString).</p>
	 * @access public
	 * @param FWDLink $poLink Link a ser adicionado (concatenado) ao ToolTip
	 */
	public function setObjFWDLink(FWDLink $poLink) {
		$msAnchorTag = $poLink->draw();
		$msAnchorTag = str_replace(array("'","\""), array("\"","\\\""), $msAnchorTag);
		$this->coString->setAttrNoEscape("true");
		$this->coString->setValue($msAnchorTag);
	}
}
?><?
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

define( "REQUIRED_CLIENT_EVENT", 1);
define( "REQUIRED_AJAX_EVENT", 2);

/**
 * Classe FWDRequiredCheck. Implementa a verifica��o da obrigatoriedade no preenchimento.
 *
 * <p>Classe que implementa a verifica��o de preenchimento de campos que s�o
 * considerados obrigat�rios.</p>
 * @package FWD5
 * @subpackage event
 */
class FWDRequiredCheck extends FWDEvent
{

	/**
	 * Nome do Evento. Obs: o nome dado pelo usu�rio ser� concatenado com "RequiredCheck"
	 * @var string
	 * @access protected
	 */
	protected $csName = "RequiredCheck";

	/**
	 * Elementos alvo do teste de "RequiredCheck", devem ser separadas por ":"
	 * @var string
	 * @access protected
	 */
	protected $csElements;

	/**
	 * Tipo do evento que deve executar o "RequiredCheck". Par�metros que devem ser passados s�o ("clientEvent" ou "ajaxEvent" ou "serverEvent")
	 * @var integer
	 * @access protected
	 */
	protected $ciEventType = REQUIRED_CLIENT_EVENT;

	/**
	 * Eventos que devem ser disparados quando a verifica��o "RequiredCheck" for Positiva
	 * @var string
	 * @access protected
	 */
	protected $csEventsOk ="";

	/**
	 * Eventos que devem ser disparados quando a verifica��o "RequiredCheck" for Negativa
	 * @var string
	 * @access protected
	 */
	protected $csEventsError="";

	/**
	 * Array de Eventos definidos dentro da tag "requiredcheck" no xml. OBS: (esses eventos devem ter Event = 'eventok' OU 'eventerror')
	 * @var array
	 * @access protected
	 */
	protected $caEvent = array();

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDRequiredCheck.</p>
	 * @access public
	 * @param string psElements Elementos que devem ser testados se est�o ou n�o preenchidos
	 */
	public function __construct($psElements = "") {
		$this->csElements = $psElements;
	}

	/**
	 * Seta o nome deste da inst�ncia da classe
	 *
	 * <p>M�todo utilizado para setar o nome da inst�ncia desta classe</p>
	 * @access public
	 * @param string $psName  Nome da inst�ncia da classe
	 */
	public function setAttrName($psName) {
		$this->csName = $psName;
	}

	/**
	 * Seta o tipo de evento que deve ser usado para o teste de preenchimento
	 *
	 * <p>M�todo para setar o tipo de evento que deve ser usado para fazer o teste
	 * de preenchimento. par�metros possiveis (clientEvent,ajaxEvent,serverEvent)</p>
	 * @access public
	 * @param string $psEventType  Tipo de evento que deve ser utilizado (clientEvent,ajaxEvent)
	 */
	public function setAttrEventType($psEventType) {
		$this->ciEventType = FWDWebLib::attributeParser($psEventType,array("clientEvent"=>REQUIRED_CLIENT_EVENT, "ajaxEvent"=>REQUIRED_AJAX_EVENT),"EventType");
	}

	/**
	 * Seta o nome dos elementos que se deve verificar se est�o preenchidos.
	 *
	 * <p>M�todo para setar o nome dos elementos que deve ser verificado se est�o ou
	 * n�o preenchidos , esses nomes devem estar separados por ":".</p>
	 * @access public
	 * @param string $psElements Elementos que devem ser testados se est�o ou n�o preenchidos
	 */
	public function setAttrElements($psElements) {
		$this->csElements = $psElements;
	}

	/**
	 * Seta a TRIGGER dos eventos que devem ser disparados em caso de verifica��o Positiva
	 *
	 * <p>seta a TRIGGER dos eventos que devem ser disparados em caso de verifica��o Positiva.
	 * Essas TRIGGERS devem estar separadas por ";".</p>
	 * @access public
	 * @param string $psEventsOk TRIGGERS de eventos que devem ser disparados em caso de verifica��o positiva
	 */
	public function setAttrEventsOk($psEventsOk) {
		$this->csEventsOk = $psEventsOk;
	}

	/**
	 * Seta a TRIGGER dos eventos que devem ser disparados em caso de verifica��o Negativa
	 *
	 * <p>seta a TRIGGER dos eventos que devem ser disparados em caso de verifica��o Negativa.
	 * Essas TRIGGERS devem estar separadas por ";".</p>
	 * @access public
	 * @param string $psEventError TRIGGERS de eventos que devem ser disparados em caso de verifica��o negativa
	 */
	public function setAttrEventsError($psEventError) {
		$this->csEventsError = $psEventError;
	}

	/**
	 * Seta os objetos de eventos definidos entre as tags da classe no XML
	 *
	 * <p>seta os objetos de eventos definidos entre as tags da classe no XML.
	 * OBS: o Event desses evento deve ser ("eventok" ou "eventerror") para qualquer outro Event, o evento n�o ser�
	 * renderizado nesta classe</p>
	 * @access public
	 * @param ObjFWDEvent $poAjaxEvent Eventos que devem ser tratados pela classe , devem ter Event = ("eventok" ou "eventerror")
	 */
	public function addObjFWDEvent($poAjaxEvent) {
		$this->addEvent($poAjaxEvent);
	}

	/**
	 * Seta um evento para o elemento.
	 *
	 * <p>M�todo para setar um evento para o elemento.</p>
	 * @access public
	 * @param FWDEvent $poEvent Evento
	 */
	public function addEvent(FWDEvent $poEvent) {
		$mskey = strtolower($poEvent->getAttrEvent());
		$msArrayKey = explode(":",$mskey);
		foreach($msArrayKey as $key)
		{
			if(!isset($this->caEvent[$key]))
			$this->caEvent[$key] = new FWDEventHandler($key,$this->getAttrName());
			$this->caEvent[$key]->setAttrContent($poEvent);
		}
	}

	/**
	 * Retorna os identificadores dos elementos que tem preenchimento obrigat�rio.
	 *
	 * <p>M�todo para retornar os identificadores dos elementos que devem ter
	 * preenchimento obrigat�rio</p>
	 * @access public
	 * @return string indentificadores dos elementos que devem tem preenchimento obrigat�rio.
	 */
	public function getAttrElements() {
		return $this->csElements;
	}

	/**
	 * Retorna o tipo de evento que deve ser utilizado para verificar se os campos est�o preenchidos.
	 *
	 * <p>M�todo para retornar o tipo de evento que deve ser utilizado para verificar se os campos
	 * est�o preenchidos</p>
	 * @access public
	 * @return integer Tipo de evento que deve ser utilizado para verificar se os campos est�o preenchidos.
	 */
	public function getAttrEventType() {
		return $this->ciEventType;
	}

	/**
	 * Renderiza o Evento RequiredCheck.
	 *
	 * <p>Renderiza o Evento RequiredCheck e dispara os eventos definidos para este evento caso a verifica��o
	 * seja possitiva ou negativa
	 * @access public
	 * @return string String que ir� disparar o Evento
	 */
	public function render()
	{
		if($this->ciEventType==REQUIRED_CLIENT_EVENT)
		{
			$msEventsOk = "";
			$msEventsError = "";

			foreach($this->caEvent as $moEventHandler)
			{

				$maEvents = $moEventHandler->getAttrContent();
					
				if($moEventHandler->getAttrEvent()=="eventok")
				{
					foreach($maEvents as $moEvent)
					{
						$msEventsOk .= $moEvent->render($moEventHandler->getAttrElem());
					}
				}
				elseif($moEventHandler->getAttrEvent()=="eventerror")
				{
					foreach($maEvents as $moEvent)
					{
						$msEventsError .= $moEvent->render($moEventHandler->getAttrElem());
					}
				}
				else
				{
					//evento n�o tem Event no padr�o adequando para esta classe , assim eh despresado
				}
			}

			$msEventsOk = str_replace(array('"',"\n"),array('\"',' '),$msEventsOk.$this->csEventsOk);
			$msEventsError = str_replace(array('"',"\n"),array('\"',' '),$msEventsError.$this->csEventsError);
			return "js_required_check_client(\"{$this->csElements}\",\"{$msEventsOk}\",\"{$msEventsError}\");";
				
		}

		elseif($this->ciEventType==REQUIRED_AJAX_EVENT)
		{
			return "trigger_event(\"{$this->getAttrName()}ajax\",\"3\");";
		}
		else
		{
				
		}
	}

	/**
	 * M�todo executado quando o XML do elemento termina de carregar.
	 *
	 * <p>M�todo executado quando o XML do elemento termina de carregar. Utilizado para criar os
	 * eventos a serem utilizados para verificar o preenchimento as vari�veis.</p>
	 *
	 * @access public
	 */
	public function execute()
	{
		$msEventsOk="";
		$msEventsError="";

		$StartEvent_instance = FWDStartEvent::getInstance();

		if($this->ciEventType==REQUIRED_AJAX_EVENT)
		{
			$moAjaxEvent = new FWDRequiredCheckAjax("{$this->getAttrName()}ajax","3");
			$moAjaxEvent->setAttrElements("{$this->getAttrElements()}");

			foreach($this->caEvent as $moEventHandler)
			{
				$maEvents = $moEventHandler->getAttrContent();
					
				if($moEventHandler->getAttrEvent()=="eventok")
				{
					foreach($maEvents as $moEvent)
					{
						$msEventsOk .= $moEvent->render($moEventHandler->getAttrElem());
					}

				}
				elseif($moEventHandler->getAttrEvent()=="eventerror")
				{
					foreach($maEvents as $moEvent)
					{
						$msEventsError .= $moEvent->render($moEventHandler->getAttrElem());
					}
				}
				else
				{
					//evento n�o tem Event no padr�o adequando para esta classe , assim eh despresado
				}
			}
			$moAjaxEvent->setAttrEventsOk("{$msEventsOk}{$this->csEventsOk}");
			$moAjaxEvent->setAttrEventsError("{$msEventsError}{$this->csEventsError}");
			$StartEvent_instance->addAjaxEvent($moAjaxEvent);
		}
	}
}
?><?
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDGridPopulate. Implementa Eventos Servidor para a FWDGrid.
 *
 * <p>Classe que implementa Eventos Servidor de preenchimento dos bot�es de pagina��o, corpo da grid,
 * menus de contexto da FWDGrid na Framework FWD5.</p>
 * @package FWD5
 * @subpackage eventClass
 */
class FWDGridPopulate extends FWDRunnable
{
	function run()
	{
		$moGrid = FWDWebLib::getObject($this->getAttrEventObj());
		if($moGrid)
		{
			if($moGrid->getAttrPopulate()){
				$moGrid->execEventPopulate();
			}
		}
		else { }
		//n�o foi encontrado no Post o objeto da grid cujo � indicado por "$this->getAttrEventObj()" assim
		//n�o ha nada pra fazer
	}
}
?><?
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDGridChagePageChange. Implementa o Evento Servidor de troca da p�gina atual da grid pelo textBox.
 *
 * <p>Classe que implementa Eventos Servidor para troca da p�gina atual pelo textBox  da FWDGrid
 * na Framework FWD5.</p>
 * @package FWD5
 * @subpackage eventClass
 */
class FWDGridChangePageChange extends FWDRunnable
{
	function run()
	{
		$moGrid = FWDWebLib::getObject($this->getAttrEventObj());
		if($moGrid)
		{
			$msInfoPages = FWDWebLib::getPOST($moGrid->getAttrName()."_gridpageinfo");
			$msTextChange = FWDWebLib::getPOST($moGrid->getAttrName()."_actual_page");
			$msPages="::";
			if($msInfoPages){
				$maInfoPage = explode(":",$msInfoPages);

				if($msTextChange > $maInfoPage[1])
				$maInfoPage[0] = $maInfoPage[1];
				else
				if($msTextChange < 1)
				$maInfoPage[0]=1;
				else
				$maInfoPage[0]= $msTextChange;
				 
				$msPages = "{$maInfoPage[0]}:{$maInfoPage[1]}:{$maInfoPage[2]}";
				$moGrid->setCurrentPage($maInfoPage[0]);
				$moGrid->setLastPage($maInfoPage[1]);
				$moGrid->setAttrRowsCount($maInfoPage[2]);
			}
			$moGrid->gridPopulateChange('change',$msPages);
		}
		else{ }
		//n�o foi encontrado no Post o objeto da grid cujo � indicado por "$this->getAttrEventObj()" assim
		//n�o ha nada pra fazer
	}
}
?><?
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDGridChangePageBack. Implementa Eventos Servidor para a FWDGrid.
 *
 * <p>Classe que implementa Eventos Servidor de backPage para a FWDGrid
 * na Framework FWD5.</p>
 * @package FWD5
 * @subpackage eventClass
 */

class FWDGridChangePageBack extends FWDRunnable
{
	function run()
	{
		$moGrid = FWDWebLib::getObject($this->getAttrEventObj());
		if($moGrid)
		{
			$msInfoPages = FWDWebLib::getPOST($moGrid->getAttrName()."_gridpageinfo");
			$msPages="::";
			if($msInfoPages){
				$maInfoPage = explode(":",$msInfoPages);
				if($maInfoPage[0]>1)
				$maInfoPage[0]= $maInfoPage[0]-1;
				else
				$maInfoPage[0]=1;
				$msPages = "{$maInfoPage[0]}:{$maInfoPage[1]}:{$maInfoPage[2]}";

				$moGrid->setCurrentPage($maInfoPage[0]);
				$moGrid->setLastPage($maInfoPage[1]);
				$moGrid->setAttrRowsCount($maInfoPage[2]);
			}
			$moGrid->gridPopulateChange('back',$msPages);
		}
		else{ }
		//n�o foi encontrado no Post o objeto da grid cujo � indicado por "$this->getAttrEventObj()" assim
		//n�o ha nada pra fazer
	}
}
?><?
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDGridChagePageFirst. Implementa Eventos Servidor para a FWDGrid.
 *
 * <p>Classe que implementa Eventos Servidor de firstPage para a FWDGrid
 * na Framework FWD5.</p>
 * @package FWD5
 * @subpackage eventClass
 */
class FWDGridChangePageFirst extends FWDRunnable
{
	function run()
	{
		$moGrid = FWDWebLib::getObject($this->getAttrEventObj());
		if($moGrid)
		{
			$msInfoPages = FWDWebLib::getPOST($moGrid->getAttrName()."_gridpageinfo");
			$msPages="::";
			if($msInfoPages){
				$maInfoPage = explode(":",$msInfoPages);

				$maInfoPage[0]=1;
				$msPages = "{$maInfoPage[0]}:{$maInfoPage[1]}:{$maInfoPage[2]}";

				$moGrid->setCurrentPage($maInfoPage[0]);
				$moGrid->setLastPage($maInfoPage[1]);
				$moGrid->setAttrRowsCount($maInfoPage[2]);
			}
			$moGrid->gridPopulateChange('first',$msPages);
		}
		else{ }
		//n�o foi encontrado no Post o objeto da grid cujo � indicado por "$this->getAttrEventObj()" assim
		//n�o ha nada pra fazer
	}
}
?><?
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDGridChagePageLast. Implementa Eventos Servidor para a FWDGrid.
 *
 * <p>Classe que implementa Eventos Servidor para lastPage  da FWDGrid
 * na Framework FWD5.</p>
 * @package FWD5
 * @subpackage eventClass
 */
class FWDGridChangePageLast extends FWDRunnable
{
	function run()
	{
		$moGrid = FWDWebLib::getObject($this->getAttrEventObj());
		if($moGrid)
		{
			$msInfoPages = FWDWebLib::getPOST($moGrid->getAttrName()."_gridpageinfo");
			$msPages="::";
			if($msInfoPages){
				$maInfoPage = explode(":",$msInfoPages);
				$maInfoPage[0]=$maInfoPage[1];
				$msPages = "{$maInfoPage[0]}:{$maInfoPage[1]}:{$maInfoPage[2]}";

				$moGrid->setCurrentPage($maInfoPage[0]);
				$moGrid->setLastPage($maInfoPage[1]);
				$moGrid->setAttrRowsCount($maInfoPage[2]);
			}
			$moGrid->gridPopulateChange('back',$msPages);
		}
		else { }
		//n�o foi encontrado no Post o objeto da grid cujo � indicado por "$this->getAttrEventObj()" assim
		//n�o ha nada pra fazer
	}
}
?><?
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDGridChagePageNext. Implementa Eventos Servidor para a FWDGrid.
 *
 * <p>Classe que implementa Eventos Servidor para nextPage  da FWDGrid
 * na Framework FWD5.</p>
 * @package FWD5
 * @subpackage eventClass
 */
class FWDGridChangePageNext extends FWDRunnable
{
	function run()
	{
		$moGrid = FWDWebLib::getObject($this->getAttrEventObj());
		if($moGrid)
		{
			$msInfoPages = FWDWebLib::getPOST($moGrid->getAttrName()."_gridpageinfo");
			$msPages="::";
			if($msInfoPages){
				$maInfoPage = explode(":",$msInfoPages);
				if($maInfoPage[0]< $maInfoPage[1])
				$maInfoPage[0]= $maInfoPage[0]+1;

				$msPages = "{$maInfoPage[0]}:{$maInfoPage[1]}:{$maInfoPage[2]}";

				$moGrid->setCurrentPage($maInfoPage[0]);
				$moGrid->setLastPage($maInfoPage[1]);
				$moGrid->setAttrRowsCount($maInfoPage[2]);
			}
			$moGrid->gridPopulateChange('first',$msPages);
		}
		else { }
		//n�o foi encontrado no Post o objeto da grid cujo � indicado por "$this->getAttrEventObj()" assim
		//n�o ha nada pra fazer
	}
}
?><?
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDGridRefresh. Implementa Eventos Servidor para a FWDGrid.
 *
 * <p>Classe que implementa Eventos Servidor para refresh  da FWDGrid
 * na Framework FWD5.</p>
 * @package FWD5
 * @subpackage eventClass
 */
class FWDGridRefresh extends FWDRunnable
{
	function run()
	{
		$moGrid = FWDWebLib::getObject($this->getAttrEventObj());
		if($moGrid)
		{
			if($moGrid->getAttrPopulate()){
				if(!$moGrid->getAttrRefreshOnCurrentPage()){
					$moGrid->setCurrentPage(1);
				}
				$moGrid->execEventPopulate('refresh');
			}else{
				echo " gobi('".$moGrid->getAttrName()."').coGlass.hide(); ";
			}
		}
		else { }
		//n�o foi encontrado no Post o objeto da grid cujo � indicado por "$this->getAttrEventObj()" assim
		//n�o ha nada pra fazer
	}
}
?><?
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDGridRefresh. Implementa Eventos Servidor para a FWDGrid.
 *
 * <p>Classe que implementa Eventos Servidor para exporta��o da FWDGrid
 * na Framework FWD5.</p>
 * @package FWD5
 * @subpackage eventClass
 */
class FWDGridExport extends FWDRunnable {

	public function run(){
		$msGridName = $this->getAttrEventObj();
		$moGrid = FWDWebLib::getObject($msGridName);
		$moGridExcel = new FWDGridExcel($msGridName,array($moGrid));
		$moGridExcel->execute();
	}

}
?><?
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDTreeAjaxExpand. Implementa evento ajax para a FWDTreeAjax.
 *
 * <p>Implementa o evento ajax que expande uma FWDTreeAjax, buscando seus filhos
 * no XML no servidor.</p>
 * @package FWD5
 * @subpackage eventClass
 */
class FWDTreeAjaxExpand extends FWDRunnable {

	/**
	 * Objeto dono do evento
	 * var FWDTreeAjax
	 * @access private
	 */
	private $coOwner = null;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDTreeAjaxExpand. Seta o identificador e o dono do
	 * evento.</p>
	 * @access public
	 * @param string $psIdentifier Identificador do evento do usu�rio
	 * @param FWDTreeAjax $poOwner Objeto dono do evento
	 */
	public function __construct($psIdentifier,$poOwner){
		parent::__construct($psIdentifier);
		$this->coOwner = $poOwner;
	}
	 
	/**
	 * M�todo que trata o evento
	 *
	 * <p>Carrega, do XML, os filhos da �rvore, os adiciona � �rvore e a expande.</p>
	 * @access public
	 */
	public function run(){
		$msTreeBaseName = $this->coOwner->getTreeBaseName();
		$moTreeBase = FWDWebLib::getObject($msTreeBaseName);
		$moTreeBase->setIds();
		echo $this->coOwner->expand();
	}
}
?><?
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDGridPopulate. Implementa Eventos Servidor para a FWDGrid.
 *
 * <p>Classe que implementa Eventos Servidor de preenchimento dos bot�es de pagina��o, corpo da grid,
 * menus de contexto da FWDGrid na Framework FWD5.</p>
 * @package FWD5
 * @subpackage eventClass
 */
class FWDGridColumnOrder extends FWDRunnable
{
	function run()
	{
		$maEventObj = explode(":",$this->getAttrEventObj());
		$moGrid = FWDWebLib::getObject($maEventObj[0]);
		if(($moGrid))
		{
			$miIndex = 1;
			if($moGrid->getAttrPopulate()){
				$msColumnOrder="";
				if(($moGrid instanceof FWDDBGrid)&&($moGrid->getDataSet())){
					$moDataSet = $moGrid->getDataSet();
					foreach($moGrid->getColumns() as $moColumn){
						if(($moColumn->getAttrAlias())&&($moColumn->getAttrAlias() == $maEventObj[1]))
						{
							if($moColumn->getOrderBy()=="+"){
								$moColumn->setOrderBy("desc");
								$msColumnOrder .= "-{$miIndex}:";
							}else{
								$moColumn->setOrderBy("asc");
								$msColumnOrder .= "+{$miIndex}:";
							}
							$moDataSet->setOrderBy($moColumn->getAttrAlias(),$moColumn->getOrderBy());
						}
						//else{}//coluna n�o est� ordenando a grid
						$miIndex++;
					}
					$moGrid->setDataSet($moDataSet);
					$moGrid->setAttrOrder($msColumnOrder);
				}
				else{ //ordena��o em grid com preenchimento manual
					foreach($moGrid->getColumns() as $moColumn){
						if($moColumn->getAttrName() == $maEventObj[1]){
							if($moColumn->getOrderBy()=="+"){
								$moColumn->setOrderBy("desc");
								$msColumnOrder .= "-{$miIndex}:";
							}else{
								$moColumn->setOrderBy("asc");
								$msColumnOrder .= "+{$miIndex}:";
							}
						}
						//else{}//coluna n�o est� ordenando a grid
						$miIndex++;
					}
					$moGrid->setAttrOrder($msColumnOrder);
				}
				$msInfoPages = FWDWebLib::getPOST($moGrid->getAttrName()."_gridpageinfo");
				$msPages="::";
				if($msInfoPages){
					$maInfoPage = explode(":",$msInfoPages);
					$msPages = "{$maInfoPage[0]}:{$maInfoPage[1]}:{$maInfoPage[2]}";

					$moGrid->setCurrentPage($maInfoPage[0]);
					$moGrid->setLastPage($maInfoPage[1]);
					$moGrid->setAttrRowsCount($maInfoPage[2]);
				}
				$moGrid->gridPopulateOrderColumn('populateordercolumn',$msPages);
			}else{
				echo " gobi('".$moGrid->getAttrName()."').coGlass.hide(); ";
			}
		}
		//else { }
		//n�o foi encontrado no Post o objeto da grid cujo � indicado por "$this->getAttrEventObj()" assim
		//n�o ha nada pra fazer
	}
}
?><?
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDGridChangePageBack. Implementa Eventos Servidor para a FWDGrid.
 *
 * <p>Classe que implementa Eventos Servidor de backPage para a FWDGrid
 * na Framework FWD5.</p>
 * @package FWD5
 * @subpackage eventClass
 */
class FWDRequiredCheckAjax extends FWDRunnable
{

	/**
	 * Alvos do evento  (separados por ":")
	 * @var string
	 * @access protected
	 */
	protected $csElements="";

	/**
	 * Eventos que devem ser disparados em caso de verifica��o "Required OK"  (separados por ";")
	 * @var string
	 * @access protected
	 */
	protected $csEventsOk ="";

	/**
	 * Eventos que devem ser disparados em caso de verifica��o "Required Error"  (separados por ";")
	 * @var string
	 * @access protected
	 */
	protected $csEventsError="";

	/**
	 * M�todo que trata o evento
	 *
	 * <p>Carrega, do XML, os valores dos elementos que devem ser tratados se foram preenchidos ou n�o,
	 * caso afirmativo, dispara o evento para desfazer as marca��es possivelmente feitas anteriormente e
	 * dispara os eventos que foram definidos no XML para serem executados em caso de todos os campos estarem
	 * preenchidos. Em caso negativo, troca a classe dos campos definidos como "label" dos campos que
	 * devem estar preenchidos, e depois dispara todos os eventos que devem ser executados em caso de pelo menos
	 * um dos campos n�o estarem preenchidos.</p>
	 * @access public
	 */
	public function run()
	{
		$moEvent = FWDWebLib::getObject($this->getAttrIdentifier());

		if($moEvent)
		{
			$msRequiredErrors = "";
			$msRequireOk="";
			$maElements = explode(':',$moEvent->csElements);
				
			foreach ($maElements as $msElementName)
			{
				$msElement = FWDWebLib::getPOST($msElementName);

				if(!$msElement)
				{
					$msRequiredErrors .= $msElementName . ":";
				}
				else
				{
					$msRequireOk .= $msElementName . ":";
				}
			}
				
			$moJs = new FWDJsEvent(JS_REQUIRED_CHECK_AJAX,$msRequiredErrors,$msRequireOk);
			echo $moJs->render();
				
			if($msRequiredErrors=="")
			{
				echo $this->csEventsOk;
			}
			else
			{
				echo $this->csEventsError;

			}
		}
		else
		{
			//n�o foi encontrado no Post o objeto do FWDRequiredCheck que � indicado por "$this->getAttrEventObj()" assim
			//n�o ha nada pra fazer
		}
	}

	/**
	 * Seta os eventos que devem ser disparados caso a verifica��o de "RequiredCheck" seja Positiva.
	 *
	 * <p>Seta os que devem ser disparados caso a verifica��o de "RequiredCheck" seja Positiva, ou seja
	 * os eventos que devem ser disparados caso todos os campos estejam preenchidos</p>
	 * @access public
	 * @param string $psEventsOk Eventos que devem ser disparados caso Verifica��o seja Positiva
	 */
	public function setAttrEventsOk($psEventsOk)
	{
		$this->csEventsOk = $psEventsOk;
	}

	/**
	 * Seta os eventos que devem ser disparados caso a verifica��o de "RequiredCheck" seja Negativa.
	 *
	 * <p>Seta os que devem ser disparados caso a verifica��o de "RequiredCheck" seja Negativa, ou seja
	 * os eventos que devem ser disparados caso pelo menos um dos campos n�o esteja preenchido</p>
	 * @access public
	 * @param string $psEventsError Eventos que devem ser disparados caso Verifica��o seja Negativa
	 */
	public function setAttrEventsError($psEventsError)
	{
		$this->csEventsError = $psEventsError;
	}

	/**
	 * Seta elementos do evento.
	 *
	 * <p>Seta os elementos sobre os quais o evento atuar�.</p>
	 * @access public
	 * @param string $psElements Elementos alvo do evento
	 */
	public function setAttrElements($psElements)
	{
		$this->csElements = $psElements;
	}
}
?><?
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDGridRefresh. Implementa Eventos Servidor para a FWDGrid.
 *
 * <p>Classe que implementa Eventos Servidor para refresh  da FWDGrid
 * na Framework FWD5.</p>
 * @package FWD5
 * @subpackage eventClass
 */
class FWDGridUserEdit extends FWDRunnable
{
	function run()
	{
		$moWebLib = FWDWebLib::getInstance();
		$moGrid = $moWebLib->getObject($this->getAttrEventObj());
		if($moGrid)
		{
			$msInfoFirst = FWDLanguage::getPHPStringValue('fwdGridUserEditErrorWidthGrid','Inconsist�ncias Encontradas: O somat�rio das larguras das colunas da grid deve ser menor ou igual a:');
			$msInfoSecond =FWDLanguage::getPHPStringValue('fwdGridUserEditErrorWidthGridExplain','por�m o valor obtido � de:') ;

			$moUserPref = $moWebLib->getObject($moGrid->getAttrPreferencesHandle());
			if($moUserPref){
				$msGridName = $moGrid->getAttrName();
				$msPrefCode = $moWebLib->getPOST("{$msGridName}_preferences_code_values");
				if($msPrefCode){
					$maPrefCode = FWDWebLib::unserializeString($msPrefCode);
					$maColumnProp = array();
					$maAuxColumnsProp = explode(';',$maPrefCode[2]);
					for($miI = 0;$miI<count($maAuxColumnsProp);$miI++){
						$maColumnPropAux = explode(':',$maAuxColumnsProp[$miI]);
						$maColumnProp[$maColumnPropAux[0]] = $maColumnPropAux[1];
					}
					$miContWidth = 0;
					$maColumnShow = (explode(':',$maPrefCode[1]));
					for($miI = 0;$miI<count($maColumnShow);$miI++)
					if(isset($maColumnProp[$maColumnShow[$miI]]))
					$miContWidth += $maColumnProp[$maColumnShow[$miI]];
					if($miContWidth > $moGrid->getObjFWDBox()->getAttrWidth()){
						echo "alert('{$msInfoFirst}      {$moGrid->getObjFWDBox()->getAttrWidth()}px {$msInfoSecond}      {$miContWidth}px');";
					}else{
						//ordenamento dos dados da grid
						$moUserPref->setOrderColumnsData($maPrefCode[0]);
						//ordem de exibi��o das colunas na grid
						$moUserPref->setOrderColumn($maPrefCode[1]);
						//propriedades das colunas {numCol:width:noWrap}*
						$moUserPref->setColumnsProp($maPrefCode[2]);
						//n�mero de linhas por p�gina
						$moUserPref->setRowsPerPage($maPrefCode[3]);
						//altura das linhas da grid
						$moUserPref->setRowsHeight($maPrefCode[4]);
						//tooltip da coluna
						$moUserPref->setToolTipCol($maPrefCode[5]);
						//apos setar todos os atributos no objeto de preferencias, salvar no banco
						$moUserPref->savePreferences($moGrid->getAttrName());
						echo "trigger_event('{$msGridName}_draw_user_edit',3);soPopUpManager.closePopUp('{$msGridName}_preferences');";
					}
				}
					
			}else
			trigger_error("Handle of GridUserPreferences don't exist in WebLib!",E_USER_WARNING);
		}
	}
}
?><?
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDGridRefresh. Implementa Eventos Servidor para a FWDGrid.
 *
 * <p>Classe que implementa Eventos Servidor para refresh  da FWDGrid
 * na Framework FWD5.</p>
 * @package FWD5
 * @subpackage eventClass
 */
class FWDGridDrawUserEdit extends FWDRunnable
{

	function run()
	{
		$moGrid = FWDWebLib::getObject($this->getAttrEventObj());
		if($moGrid->getAttrPopulate()){
			$moGrid->execEventPopulate("user_edit");
		}
		//else { }
		//n�o foi encontrado no Post o objeto da grid cujo � indicado por "$this->getAttrEventObj()" assim
		//n�o ha nada pra fazer
	}
}
?><?
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDGridRefresh. Implementa Eventos Servidor para a FWDGrid.
 *
 * <p>Classe que implementa Eventos Servidor para refresh  da FWDGrid
 * na Framework FWD5.</p>
 * @package FWD5
 * @subpackage eventClass
 */
class FWDGridUserEditDefaultValues extends FWDRunnable
{
	function run()
	{
		$moWebLib = FWDWebLib::getInstance();
		$moGrid = $moWebLib->getObject($this->getAttrEventObj());
		if($moGrid)
		{
			$moUserPref = $moWebLib->getObject($moGrid->getAttrPreferencesHandle());
			if($moUserPref){
				$moUserPref->resetPreferenses($moGrid->getAttrName());
			}else
			trigger_error("Handle of GridUserPreferences don't exist in WebLib!",E_USER_WARNING);
		}
	}
}
?><?
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDGridCreatePopupEditPref Implementa Eventos Servidor para a FWDGrid.
 *
 * <p>Classe que implementa Eventos Servidor para criar a popup de edi��o de preferencias da FWDGrid
 * na Framework FWD5.</p>
 * @package FWD5
 * @subpackage eventClass
 */
class FWDGridCreatePopupEditPref extends FWDRunnable
{
	function run()
	{
		$moGrid = FWDWebLib::getObject($this->getAttrEventObj());
		if($moGrid)
		{
			$msReturn = $moGrid->createHTMLPopup();
				
			$msGridCodeScape = str_replace('"','\"',$msReturn);
			$moStatic = new FWDStaticGrid(new FWDBox(0,1,22,718));
			$moStatic->setValue('Editar Prefer�ncias do Usu�rio');
			$moStatic->setAttrClass($moGrid->getAttrCSSClass().$moGrid->getCssClassTitlePanel());
			$moStatic->setAttrHorizontal('center');
			$moStatic->setAttrVertical('middle');
			$msStaticCode = str_replace('"','\"',$moStatic->draw());
				
			if(FWDWebLib::browserIsIE())
			echo "js_open_popup('{$moGrid->getAttrName()}_preferences','about:blank',\"{$msStaticCode}\",'true',381,720);var moPopup = soPopUpManager.getPopUpById('{$moGrid->getAttrName()}_preferences');moPopup.setHtmlContent(\"{$msGridCodeScape}\");moPopup.setCloseEvent(function(){objManagerGrifPref.cleanVars();});window.setTimeout(\"objManagerGrifPref.init('{$moGrid->getAttrName()}')\",100);";
			else
			echo "js_open_popup('{$moGrid->getAttrName()}_preferences','about:blank',\"{$msStaticCode}\",'true',382,722);var moPopup = soPopUpManager.getPopUpById('{$moGrid->getAttrName()}_preferences');moPopup.setHtmlContent(\"{$msGridCodeScape} \");moPopup.setCloseEvent(function(){objManagerGrifPref.cleanVars();});window.setTimeout(\"objManagerGrifPref.init('{$moGrid->getAttrName()}')\",100);";
		}
		//else { }
		//n�o foi encontrado no Post o objeto da grid cujo � indicado por "$this->getAttrEventObj()" assim
		//n�o ha nada pra fazer
	}
}
?><?
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDDBGridPackRefresh. Implementa o refresh das grids contidas em um FWDDBGridPack.
 *
 * <p>Classe que implementa o refresh das grids contidas em um FWDDBGridPack.</p>
 * @package FWD5
 * @subpackage eventClass
 */
class FWDDBGridPackRefresh extends FWDRunnable {
	public function run(){
		$moPack = FWDWebLib::getObject($this->getAttrEventObj());
		foreach($moPack->getElements() as $msElementId){
			$moGrid = FWDWebLib::getObject($msElementId);
			if($moGrid->getAttrPopulate()){
				if(!$moGrid->getAttrRefreshOnCurrentPage()){
					$moGrid->setCurrentPage(1);
				}
				$moGrid->execEventPopulate('refresh');
			}else{
				echo "gobi('{$msElementId}').hideGlass();";
			}
		}
	}
}

?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDViewGroup. Organiza as views verticalmente.
 *
 * <p>Viewgroup cl�ssica. Organiza cada view verticalmente, ou seja, uma
 * depois da outra, em camadas verticais.</p>
 * @package FWD5
 * @subpackage viewgroup
 */
class FWDViewGroup extends FWDContainer {

	/**
	 * Define se deve existir uma barra de rolagem no caso de um overflow
	 * @var boolean
	 * @access protected
	 */
	protected $cbScrollbar = false;

	/**
	 * Define a profundidade da view, para manipular sobreposi��o de elementos (zenith-index)
	 * @var integer
	 * @access protected
	 */
	protected $ciIndex = 0;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDViewGroup.</p>
	 * @access public
	 * @param FWDBox $pobox Box que define os limites da viewgroup
	 */
	public function __construct($poBox = null) {
		parent::__construct($poBox);
	}

	/**
	 * Desenha o cabe�alho da viewgroup.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o cabe�alho da viewgroup.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites da viewgroup
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawHeader($poBox = null) {
		$this->setAttrTabIndex(0);
			
		$msStyle = $this->getStyle();

		if ($this->ciIndex)
		$msStyle .= "z-index:".$this->getAttrIndex().";";
			
		$msScrollbar = ($this->getAttrScrollbar()?"overflow:auto;":"overflow:visible;");
		$msStyle .= $msScrollbar;
		$msStyle = "style='{$msStyle}'";
			
		return "<div {$this->getGlobalProperty()} {$msStyle} {$this->getEvents()}>\n";
	}

	/**
	 * Desenha o corpo da viewgroup.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o corpo da viewgroup.</p>
	 * @access public
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawBody(){
		$msOut = '';
		$msWarnings = '';
		$poBox = $this->getObjFWDBox();
		foreach($this->getContent() as $msView){
			if($msView instanceof FWDWarning){
				$msWarnings.= $msView->draw($poBox);
			}else{
				$msOut.= $msView->draw($poBox);
			}
		}
		return $msOut.$msWarnings;
	}

	/**
	 * Desenha o rodap� da viewgroup.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o rodap� da viewgroup.</p>
	 * @access public
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawFooter() {
		return "\n</div>\n";
	}

	/**
	 * Adiciona um calend�rio.
	 *
	 * <p>Adiciona um objeto de calend�rio da classe FWDCalendar
	 * na classe FWDViewGroup.</p>
	 * @access public
	 * @param FWDCalendar $poCalendar Calend�rio
	 */
	public function addObjFWDCalendar($poCalendar) {
		$this->addObjFWDView($poCalendar);
	}

	/**
	 * Seta a profundidade da viewgroup.
	 *
	 * <p>Seta a profundidade da viewgroup (zenith-index).</p>
	 * @access public
	 * @param integer $piIndex Profundidade da view
	 */
	public function setAttrIndex($piIndex) {
		$this->ciIndex = $piIndex;
	}

	/**
	 * Seta o valor booleano do atributo de barra de rolagem.
	 *
	 * <p>Seta o valor booleano do atributo que define se deve existir
	 * uma barra de rolagem no caso de um overflow. Para a barra de rolagem
	 * existir, o atributo deve conter o valor booleano TRUE. Para n�o existir,
	 * o atributo deve conter o valor booleano FALSE.</p>
	 * @access public
	 * @param string $psScrollbar Define se deve existir uma barra de rolagem no caso de um overflow
	 */
	public function setAttrScrollbar($psScrollbar) {
		$this->cbScrollbar = FWDWebLib::attributeParser($psScrollbar,array("true"=>true, "false"=>false),"psScrollbar");
	}

	/**
	 * Retorna a profundidade da view.
	 *
	 * <p>Retorna a profundidade da view (zenith-index).</p>
	 * @access public
	 * @return integer Profundidade da view
	 */
	public function getAttrIndex() {
		return $this->ciIndex;
	}

	/**
	 * Retorna o valor booleano do atributo de barra de rolagem.
	 *
	 * <p>Retorna o valor booleano TRUE se deve existir uma barra de rolagem
	 * no caso de um overflow. Retorna FALSE caso contr�rio.</p>
	 * @access public
	 * @return boolean Indica se deve existir uma barra de rolagem no caso de um overflow
	 */
	public function getAttrScrollbar() {
		return $this->cbScrollbar;
	}

	/**
	 * Retorna um array com todos sub-elementos do viewgroup
	 *
	 * <p>Retorna um array contendo todos elementos contidos no viewgroup ou em
	 * seus descendentes.</p>
	 * @access public
	 * @return array Array de sub-elementos
	 */
	public function collect() {
		$maOut = array();
		$maOut = array_merge($maOut,parent::collect());
		return $maOut;
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDDialog. Implementa o controle das views.
 *
 * <p>Classe que implementa o controle das views. Objeto principal da
 * framework. Controla as views como um form/dialog em uma estrutura
 * dirigida a eventos.</p>
 * @package FWD5
 * @subpackage viewgroup
 */
class FWDDialog extends FWDViewGroup {

	/**
	 * Evento temporal a ser executado
	 * @var FWDTempEvent
	 * @access protected
	 */
	protected $coTempEvent = null;

	/**
	 * Menu de contexto
	 * @var FWDMenu
	 * @access protected
	 */
	protected $caMenus = array();

	/**
	 * Array de parametros por GET
	 * @var array
	 * @access private
	 */
	private $caGetParameter = array();

	/**
	 * Array de parametros por SESSION
	 * @var array
	 * @access private
	 */
	private $caSessionParameter = array();

	/**
	 * Elemento que deve ser focado ao carregar a p�gina
	 * @var string
	 * @access private
	 */
	private $csFocus = "";

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDDialog.</p>
	 * @access public
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Retorna as propriedades globais do elemento.
	 *
	 * <p>M�todo para retornar as propriedades globais do elemento.</p>
	 * @access public
	 * @return string Propriedades globais do elemento
	 */
	public function getGlobalProperty(){
		$msAccessKey = (($this->csAccessKey)?"accesskey='{$this->csAccessKey}'":"");
		$msResize = (($this->cbResize)?"resize='true'":"");
		$msClass = (($this->csClass)?"class='{$this->csClass}'":"");
		$msId = 'dialog';
		$msValue = "name='{$msId}' id='{$msId}' {$msAccessKey} {$msResize} {$msClass}";
		return $msValue;
	}

	/**
	 * Desenha o cabe�alho da Dialog.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o cabe�alho da Dialog.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites da dialog
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawHeader($poBox = null) {
		$miTop = $this->coBox->getAttrTop();
		if($miTop == 0) {
			$this->coBox->setAttrTop(1);
		}
		return parent::drawHeader($poBox);
	}

	/**
	 * Desenha a tela.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML/javascript,
	 * todas as informa��es necess�rias para implementar o menu de contexto.</p>
	 * @access public
	 * @return string C�digo HTML/javascript que ser� inserido na p�gina
	 */
	public function drawBody() {
		$msCodeMenu = "";
		foreach($this->getMenus() as $moMenu){
			$msCodeMenu .= $moMenu->draw();
		}
		$msOut = parent::drawBody().$msCodeMenu;
		return $msOut;
	}

	/**
	 * Desenha o rodap� da Dialog.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o rodap� da Dialog.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites da dialog
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawFooter($poBox = null) {
		$msFocus = "";

		if ($this->getAttrFocus())
		$msFocus = "\n\n<script> gebi('{$this->getAttrFocus()}').focus(); </script>\n\n";
		else {
			$dialog = FWDWebLib::getObject($this->getAttrName());
			$collection = $dialog->collect();

			$msMin = 0;
			$msMinName = "";

			if(count($collection)>0) {
				foreach($collection as $key => $view) {
					if($view instanceof FWDView) {
						$temp = $view->getAttrTabIndex();
						if (($temp<$msMin) && ($temp>0)) {
							$msMin = $temp;
							$msMinName = $view->getAttrName();
						}
					}
				}
			}
			if($msMin)
			$msFocus = "\n\n<script> gebi('{$msMinName}').focus(); </script>\n\n";
		}

		$msOut = parent::drawFooter($poBox).$msFocus;
		return $msOut;
	}

	/**
	 * Seta um evento temporal.
	 *
	 * <p>Seta um evento temporal.</p>
	 * @access public
	 * @param FWDTempEvent $poTempEvent Evento temporal
	 */
	public function setObjFWDTempEvent($poTempEvent) {
		$this->coTempEvent = $poTempEvent;
	}

	/**
	 * Retorna o objeto de evento temporal.
	 *
	 * <p>Retorna o objeto de evento temporal.</p>
	 * @access public
	 * @return object Evento temporal
	 */
	public function getObjFWDTempEvent() {
		return $this->coTempEvent;
	}

	/**
	 * Renderiza evento temporal.
	 *
	 * <p>Renderiza evento temporal.</p>
	 * @access public
	 * @return string Depende do objeto de evento que foi adicionado
	 */
	public function renderLoad(){
		$moOnload = new FWDClientEvent('onLoad');
		$moOnload->setAttrValue('resize_popup();');
		$this->addEvent($moOnload);
		$msOnLoad="";
		$msOnUnload="";
		if(isset($this->caEvent["onload"])){
			$msOnLoad = $this->caEvent["onload"]->render();
		}
		if(isset($this->caEvent["onunload"])){
			$msOnUnload = $this->caEvent["onunload"]->render();
		}
		return $msOnLoad." ".$msOnUnload;
	}

	/**
	 * Adiciona um objeto de menu de contexto.
	 *
	 * <p>Adiciona um objeto de menu de contexto da classe FWDMenu
	 * na classe FWDDialog.</p>
	 * @access public
	 * @param FWDMenu $poMenu Menu de contexto
	 */
	public function addObjFWDMenu($poMenu) {
		$this->caMenus[] = $poMenu;
		$moEvent = new FWDClientEvent('OnClick','hidemenu',$poMenu->getAttrName());
		$this->addEvent($moEvent);
	}

	/**
	 * Retorna o array de objetos de menu de contexto.
	 *
	 * <p>Retorna o array com os menu de contexto.</p>
	 * @access public
	 * @return array Array de FWDMenu
	 */
	public function getMenus() {
		return $this->caMenus;
	}

	/**
	 * Adiciona um objeto de GET.
	 *
	 * <p>Adiciona um objeto de GET.</p>
	 * @access public
	 * @param FWDGetParameter $poGetParameter objeto GetParameter
	 */
	public function addObjFWDGetParameter(FWDGetParameter $poGetParameter) {
		$this->caGetParameter[] = $poGetParameter;
	}

	/**
	 * Adiciona um objeto SessionParameter.
	 *
	 * <p>Adiciona um objeto de SessionParameter.</p>
	 * @access public
	 * @param FWDSessionParameter $poSessionParameter Objeto SessionParameter
	 */
	public function addObjFWDSessionParameter(FWDSessionParameter $poSessionParameter) {
		$this->caSessionParameter[] = $poSessionParameter;
	}

	/**
	 * Retorna um array com todos sub-elementos da dialog
	 *
	 * <p>Retorna um array contendo todos elementos contidos na dialog ou em
	 * seus descendentes e os menus da dialog</p>
	 * @access public
	 * @return array Array de sub-elementos
	 */
	public function collect() {
		$maOut = array();
		$maOut = array_merge($maOut,$this->caGetParameter);
		$maOut = array_merge($maOut,$this->caSessionParameter);
		$maOut = array_merge($maOut,parent::collect());
		if($this->caMenus)
		foreach ($this->caMenus as $moMenu)
		$maOut = array_merge($maOut,$moMenu->collect());
		return $maOut;
	}

	/**
	 * Seta o elemento que deve ser focado ao carregar a p�gina.
	 *
	 * <p>Seta o elemento que deve ser focado ao carregar a p�gina.</p>
	 * @access public
	 * @param string $psFocus Elemento que deve ser focado
	 */
	public function setAttrFocus($psFocus) {
		$this->csFocus = $psFocus;
	}

	/**
	 * Retorna o elemento que deve ser focado ao carregar a p�gina.
	 *
	 * <p>Retorna o elemento que deve ser focado ao carregar a p�gina.</p>
	 * @access public
	 * @return string Elemento que deve ser focado
	 */
	public function getAttrFocus() {
		return $this->csFocus;
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDPanel. Cerca a view com uma borda mais trabalhada.
 *
 * <p>Classe similar a classe FWDViewGroup, mas cerca a view com uma borda
 * mais trabalhada.</p>
 * @package FWD5
 * @subpackage viewgroup
 */
class FWDPanel extends FWDViewGroup {

	/**
	 * Array das FWDViews que serao desenhadas no in�cio do panel
	 * @var array
	 * @access protected
	 */
	protected $caTitleViews = array();

	/**
	 * Inteiro para controlar a altura do t�tulo
	 * @var integer
	 * @access private
	 */
	private $ciTitleHeight = 0;

	private $ciTitleMarginLeft=10;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDPanel.</p>
	 * @access public
	 * @param FWDPanel $pobox Box que define os limites do panel
	 */
	public function __construct($poBox = null) {
		parent::__construct($poBox);
	}

	/**
	 * Desenha o corpo do panel.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o corpo do panel.</p>
	 * @access public
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawBody() {
		$msOut = "";

		foreach($this->caTitleViews as $moTitleView) {
			$this->alignTitle($moTitleView);
			$newDiv = new FWDDiv();
			$newBox = new FWDBox();
			$newBox->setAttrWidth($moTitleView->getObjFWDBox()->getAttrWidth());
			$newBox->setAttrLeft(0);
			$newBox->setAttrTop($moTitleView->getObjFWDBox()->getAttrTop());
			$newBox->setAttrHeight($moTitleView->getObjFWDBox()->getAttrHeight());
			$newDiv->setObjFWDBox($newBox);
			$newDiv->setClass($moTitleView->getAttrClass());
			if($moTitleView instanceof FWDStatic)
			if($moTitleView->getAttrMarginLeft()==0)
			$moTitleView->setAttrMarginLeft($this->ciTitleMarginLeft);

			$msOut .= $moTitleView->draw();
			$msOut .= $newDiv->draw();
		}

		foreach($this->getContent() as $moView) $msOut .= $moView->draw($this->getObjFWDBox());

		return $msOut;
	}

	/**
	 * Adiciona uma view.
	 *
	 * <p>Adiciona um objeto view da classe fwdView na classe fwdPanel.</p>
	 * @access public
	 * @param FWDView $poView View
	 * @param string $psTarget Alvo
	 */
	public function addObjFWDView($poView, $psTarget = "") {
		switch($psTarget) {
			case "title":
				$this->caTitleViews[] = $poView;
				break;
			default:
				parent::addObjFWDView( $poView, $psTarget);
		}
	}

	/**
	 * Retorna o array de views do t�tulo.
	 *
	 * <p>Retorna o array de views do t�tulo.</p>
	 * @access public
	 * @return array Array de views
	 */
	public function getTitles() {
		return $this->caTitleViews;
	}

	/**
	 * Posiciona e alinha a view dentro do panel.
	 *
	 * <p>Posiciona e alinha a view dentro do panel.</p>
	 * @access public
	 * @param FWDView $poView View
	 */
	public function alignTitle($poView) {
		$miTop = $this->ciTitleHeight;
		$miLeft = 0;
		if(FWDWebLib::browserIsIE())
		$miWidth = $this->getObjFWDBox()->getAttrWidth()-2;
		else
		$miWidth = $this->getObjFWDBox()->getAttrWidth();

		$miHeight = $poView->getObjFWDBox()->getAttrHeight();
		$this->ciTitleHeight += $miHeight;

		$poView->setObjFWDBox(new FWDBox($miLeft, $miTop, $miHeight, $miWidth));
	}

	/**
	 * Retorna um array com todos sub-elementos do panel
	 *
	 * <p>Retorna um array contendo todos elementos contidos no panel ou em
	 * seus descendentes.</p>
	 * @access public
	 * @return array Array de sub-elementos
	 */
	public function collect() {
		$maOut = array();
		$maOut = array_merge($maOut,$this->caTitleViews);
		$maOut = array_merge($maOut,parent::collect());
		return $maOut;
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDBox. Implementa uma box que limita o tamanho dos elementos.
 *
 * <p>Esta classe cont�m informa��es sobre uma box que limita o tamanho
 * dos elementos (margem � esquerda, margem superior, altura e largura).</p>
 * @package FWD5
 * @subpackage view
 */
class FWDBox extends FWDDrawing {

	/**
	 * Posi��o horizontal do canto superior esquerdo da box
	 * @var integerPosi��o horizontal do canto superior esquerdo da box
	 * @access protected
	 */
	protected $ciLeft = 0;

	/**
	 * Posi��o vertical do canto superior esquerdo da box
	 * @var integerPosi��o vertical do canto superior esquerdo da box
	 * @access protected
	 */
	protected $ciTop = 0;

	/**
	 * Altura da box, em pixels
	 * @var integer
	 * @access protected
	 */
	protected $ciHeight = 0;

	/**
	 * Largura da box, em pixels
	 * @var integerLargura da box, em pixels
	 * @access protected
	 */
	protected $ciWidth = 0;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDBox.</p>
	 * @access public
	 * @param integer $piLeft Posi��o horizontal do canto superior esquerdo da box
	 * @param integer $piTop Posi��o vertical do canto superior esquerdo da box
	 * @param integer $piHeight Altura da box
	 * @param integer $piWidth Largura da box
	 */
	public function __construct($piLeft = 0,$piTop = 0,$piHeight = 0,$piWidth = 0) {
		$this->setAttrLeft($piLeft);
		$this->setAttrTop($piTop);
		$this->setAttrHeight($piHeight);
		$this->setAttrWidth($piWidth);
	}

	/**
	 * Retorna o html referente a tag 'box'.
	 *
	 * <p>Retorna o html referente a tag 'box'.</p>
	 * @access public
	 * @return string Html da tag 'box'
	 */
	public function drawBody() {
		$msHeight = (($this->getAttrHeight())?"height:{$this->getAttrHeight()};":"");
		$msWidth = (($this->getAttrWidth())?"width:{$this->getAttrWidth()};":"");
		$msLeft = (($this->getAttrLeft())?"left:{$this->getAttrLeft()};":"");
		$msTop = "top:{$this->getAttrTop()};";
		$msOut = "{$msLeft}{$msTop}{$msHeight}{$msWidth}";
		return $msOut;
	}

	/**
	 * Seta largura da box, em pixels.
	 *
	 * <p>Seta largura da box, em pixels.</p>
	 * @access public
	 * @param integer $piWidth Largura da box, em pixels
	 */
	public function setAttrWidth($piWidth) {
		$this->ciWidth = $piWidth;
	}

	/**
	 * Seta altura da box, em pixels.
	 *
	 * <p>Seta altura da box, em pixels.</p>
	 * @access public
	 * @param integer $piHeight Altura da box, em pixels
	 */
	public function setAttrHeight($piHeight) {
		$this->ciHeight = $piHeight;
	}

	/**
	 * Seta posi��o horizontal do canto superior esquerdo da box.
	 *
	 * <p>Seta posi��o horizontal do canto superior esquerdo da box.</p>
	 * @access public
	 * @param integer $piLeft Posi��o horizontal do canto superior esquerdo da box
	 */
	public function setAttrLeft($piLeft) {
		$this->ciLeft = $piLeft;
	}

	/**
	 * Seta posi��o vertical do canto superior esquerdo da box.
	 *
	 * <p>Seta posi��o vertical do canto superior esquerdo da box.</p>
	 * @access public
	 * @param integer $piTop Posi��o vertical do canto superior esquerdo da box
	 */
	public function setAttrTop($piTop) {
		$this->ciTop = $piTop;
	}

	/**
	 * Retorna a largura da box, em pixels.
	 *
	 * <p>Retorna a largura da box, em pixels.</p>
	 * @access public
	 * @return integer Largura da box, em pixels
	 */
	public function getAttrWidth() {
		return $this->ciWidth;
	}

	/**
	 * Retorna a altura da box, em pixels.
	 *
	 * <p>Retorna a altura da box, em pixels.</p>
	 * @access public
	 * @return integer Altura da box, em pixels
	 */
	public function getAttrHeight() {
		return $this->ciHeight;
	}

	/**
	 * Retorna posi��o horizontal do canto superior esquerdo da box.
	 *
	 * <p>Retorna posi��o horizontal do canto superior esquerdo da box.</p>
	 * @access public
	 * @return integer Posi��o horizontal do canto superior esquerdo da box
	 */
	public function getAttrLeft() {
		return $this->ciLeft;
	}

	/**
	 * Retorna posi��o vertical do canto superior esquerdo da box.
	 *
	 * <p>Retorna posi��o vertical do canto superior esquerdo da box.</p>
	 * @access public
	 * @return integer Posi��o vertical do canto superior esquerdo da box
	 */
	public function getAttrTop() {
		return $this->ciTop;
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDString. Representa a tag 'string' do xml.
 *
 * <p>Classe que representa a tag 'string' do xml. Utilizada para
 * inserir textos no xml.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDString {

	/**
	 * String do xml
	 * @var string
	 * @access protected
	 */
	protected $csString = "";

	/**
	 * Id da string
	 * @var string
	 * @access protected
	 */
	protected $csId = "";

	/**
	 * Indica se o conte�do do elemento vai ter as aspas escapadas ou n�o
	 * @var boolean
	 * @access protected
	 */
	protected $cbNoEscape = false;

	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDString.</p>
	 * @access public
	 */
	public function __construct() {
	}

	/**
	 * Seta o valor da string.
	 *
	 * <p>M�todo para setar o valor da string.</p>
	 * @param string $psString String
	 */
	public function setAttrValue($psString) {
		$this->csString = ($psString);
	}

	/**
	 * Atribui valor, condicionalmente, � vari�vel value.
	 *
	 * <p>Atribui valor � vari�vel value, condicionando atrav�s do segundo par�metro.
	 * Se force for FALSE, concatena o valor se a vari�vel tiver conte�do;
	 * se TRUE atribui mesmo que tenha conte�do (sobrescreve).</p>
	 * @access public
	 * @param string $psValue Valor a ser atribu�do.
	 * @param boolean $pbForce For�ar o valor mesmo que j� tenha conte�do
	 */
	public function setValue($psString, $psForce = false) {
		/* Teste se est� dentro da fun��o xml_load, para que os valores sejam traduzidos
		 * quando o sistema estiver usando tradu��o.
		 */
		$moFWDWebLib = FWDWebLib::getInstance();
		if(!isset($this->csString) || !$this->csString || $psForce){
			if($moFWDWebLib->isXmlLoadRunning()) {
				if($psString=="" && !empty($this->csString))
				$psString = $this->csString;
				$this->setAttrValue(FWDLanguage::getXMLStringValue($this->csId,$psString));
			} else {
				$this->setAttrValue($psString);
			}
		}else{
			$this->setAttrValue($this->csString.$psString);
		}
	}

	/**
	 * Retorna o valor da string.
	 *
	 * <p>M�todo para retornar o valor da string.</p>
	 * @return string Valor da string
	 */
	public function getAttrString(){
		$msString = $this->csString;
		if($msString=="" && !empty($this->csId)) {
			$msString = FWDLanguage::getXMLStringValue($this->csId,$this->csString);
		}
		if(!$this->getAttrNoEscape()){
			$msString = str_replace(array("'",'"'),array('&#39;','&quot;'),$msString);
		}
		return $msString;
	}

	/**
	 * Seta o identificador da string.
	 *
	 * <p>M�todo para setar o identificador da string.</p>
	 * @param string $psId Identificador da string
	 */
	public function setAttrId($psId) {
		if ($psId)
		$this->csId = $psId;
		else
		trigger_error("Invalid string id: '{$psId}'",E_USER_WARNING);
	}

	/**
	 * Retorna o identificador da string.
	 *
	 * <p>M�todo para retornar o identificador da string.</p>
	 * @return string Identificador da string
	 */
	public function getAttrId() {
		return $this->csId;
	}

	/**
	 * Seta o atributo noescape
	 *
	 * <p>Seta o atributo noescape. True indica que o conte�do do elemento n�o
	 * deve ter as aspas escapadas e false, que deve.</p>
	 * @param string $pbNoEscape Indica se o conte�do do elemento vai ter as aspas escapadas ou n�o
	 */
	public function setAttrNoEscape($pbNoEscape){
		$this->cbNoEscape = FWDWebLib::attributeParser($pbNoEscape,array("true"=>true, "false"=>false),"noescape");
	}

	/**
	 * Retorna o valor do atributo noescape
	 *
	 * <p>Retorna o atributo noescape. True indica que o conte�do do elemento n�o
	 * deve ter as aspas escapadas e false, que deve.</p>
	 * @return string Indica se o conte�do do elemento vai ter as aspas escapadas ou n�o
	 */
	public function getAttrNoEscape(){
		return $this->cbNoEscape;
	}

	/**
	 * Retorna informa��es do objeto necess�rias para a tradu��o.
	 *
	 * <p>M�todo para retornar informa��es do objeto necess�rias para a tradu��o.</p>
	 * @access public
	 * @return array Array com a classe, valor e nome do objeto
	 */
	public function getTranslateTable() {
		if ($this->csId)
		return array( "{$this->csId}" => array("value" => $this->csString));
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDVariable. Representa a tag 'variable' do xml.
 *
 * <p>Classe que representa a tag 'variable' do xml. Utilizada para
 * criar uma vari�vel no xml.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDVariable extends FWDView implements FWDPostValue{

	/**
	 * Nome do label associado ao objeto
	 * @var string
	 * @access protected
	 */
	protected $csLabel = "";

	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDVariable.</p>
	 * @access public
	 */
	public function __construct() {
		parent::__construct();
		$this->setAttrDisplay("false");
		$this->setAttrNoEscape('true');
	}

	/**
	 * Retorna o html referente a tag 'variable'.
	 *
	 * <p>Retorna o html referente a tag 'variable'.</p>
	 * @param FWDBox $poBox Box limitante
	 * @access public
	 */
	public function drawBody($poBox = null) {
		if (!($msName = $this->getAttrName()))
		trigger_error("Class FWDVariable must have a name",E_USER_WARNING);

		$msValue = $this->getValue();

		$msLabel = '';
		if($this->csLabel) $msLabel = "label='{$this->csLabel}'";
			
		$msOut = "<input name='{$msName}' id='{$msName}' {$msLabel} type='hidden' value='{$msValue}'/>";
		return $msOut;
	}

	/**
	 * Atribui valor � vari�vel value.
	 *
	 * <p>Atribui valor � vari�vel value, devendo retirar espa�os em branco
	 * no inicio e/ou no fim da string.</p>
	 * @access public
	 * @param string $psValue Valor a ser atribu�do
	 */
	public function setAttrValue($psValue) {
		$this->setValue($psValue);
	}

	/**
	 * Seta o atributo noescape
	 *
	 * <p>Seta o atributo noescape. True indica que o conte�do do elemento n�o
	 * deve ter as aspas escapadas e false, que deve.</p>
	 * @param string $pbNoEscape Indica se o conte�do do elemento vai ter as aspas escapadas ou n�o
	 */
	public function setAttrNoEscape($pbNoEscape){
		$this->coString->setAttrNoEscape($pbNoEscape);
	}

	/**
	 * Seta o nome do Label do objeto.
	 *
	 * <p>M�todo para setar o nome do label do objeto, utilizado para a troca de cor, no caso do objeto ter
	 * preenchimento obrigat�rio e n�o estar preenchido.</p>
	 * @access public
	 * @param string $psLabel nome do label associado ao objeto
	 */
	public function setAttrLabel($psLabel) {
		$this->csLabel = $psLabel;
	}

}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDText. Implementa campos de texto.
 *
 * <p>Classe que implementa campos de texto (html tag 'input type=text').</p>
 * @package FWD5
 * @subpackage view
 */
class FWDText extends FWDView implements FWDPostValue {

	/**
	 * Define se o texto � do tipo password ou n�o
	 * @var boolean
	 * @access protected
	 */
	protected $cbPassword = false;

	/**
	 * Define se o campo est� desabilitado ou n�o
	 * @var boolean
	 * @access protected
	 */
	protected $cbDisabled = false;

	/**
	 * N�mero m�ximo de caracteres
	 * @var integer
	 * @access protected
	 */
	protected $ciMaxLength = 0;

	/**
	 * M�scara a ser aplicada sobre o texto
	 * @var FWDMask
	 * @access protected
	 */
	protected $coMask = null;

	/**
	 * Nome do label associado ao objeto
	 * @var string
	 * @access protected
	 */
	protected $csLabel = "";

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDText.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do campo de texto
	 */
	public function __construct($poBox = null) {
		parent::__construct($poBox);
	}

	/**
	 * Desenha o campo de texto.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML,
	 * todas as informa��es necess�rias para implementar o campo de texto.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do campo de texto
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawBody($poBox = null){
		$msOut = '';

		if($this->getAttrAlign()){
			$this->alignView($this->getAttrAlign(),$poBox);
		}

		$msStyle = $this->GetStyle();
		$msStyle = "style='{$msStyle}'";

		$msLabel = '';
		if($this->csLabel){
			$msLabel = "label='{$this->csLabel}'";
		}

		$msAttrDefault = $this->getGlobalProperty();

		$msType = $this->cbPassword ? "type='password'" : "type='text'";

		$msMaxLength = $this->ciMaxLength ? "maxlength='{$this->ciMaxLength}'" : "";

		$msDisabled = $this->cbDisabled ? "disabled" : "";

		$msValue = $this->getValue() ? "value='{$this->getValue()}'" : "";

		$msMaskEvent = "";
		if(isset($this->coMask)){
			$msMaskEvent = "onpaste='return false'".$this->coMask->getAttrEvent()."='".$this->coMask->render($this->getAttrName())."'";
		}

		$msOut .= "<input {$msType} {$msAttrDefault} {$this->getEvents()} {$msStyle} {$msLabel} {$msMaxLength} {$msDisabled} {$msValue} {$msMaskEvent}/>\n";

		return $msOut;
	}

	/**
	 * Desenha o rodap� do texto.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o rodap� do texto.</p>
	 * @access public
	 *
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawFooter(){
		if($this->csName){
			$msOut = "<script language='javascript'>";
			$msOut.= "gebi('{$this->csName}').object = new FWDText('{$this->csName}');";
			if(isset($this->coMask)){
				$msOut.= "gobi('{$this->csName}').setMaskRegExp({$this->coMask->getRegExp()});";
			}
			$msOut.= "</script> ";
			return $msOut;
		}else{
			return '';
		}
	}

	/**
	 * Atribui valor � vari�vel value.
	 *
	 * <p>Atribui valor � vari�vel value, devendo retirar espa�os em branco
	 * no inicio e/ou no fim da string.</p>
	 * @access public
	 * @param string $psValue Valor a ser atribu�do
	 */
	public function setAttrValue($psValue) {
		$this->coString->setAttrValue(trim($psValue));
	}

	/**
	 * Seta o campo como password.
	 *
	 * <p>M�todo para setar o campo como password.</p>
	 * @access public
	 * @param boolean $pbPassword Define se o texto � do tipo password ou n�o
	 */
	public function setAttrPassword($pbPassword) {
		$this->cbPassword = FWDWebLib::attributeParser($pbPassword,array("true"=>true, "false"=>false),"pbPassword");
	}

	/**
	 * Seta o campo como desabilitado.
	 *
	 * <p>M�todo para setar o campo como desabilitado.</p>
	 * @access public
	 * @param boolean $pbDisabled Define se o campo est� desabilitado ou n�o
	 */
	public function setAttrDisabled($pbDisabled) {
		$this->cbDisabled = FWDWebLib::attributeParser($pbDisabled,array("true"=>true, "false"=>false),"pbDisabled");
	}

	/**
	 * Seta o n�mero m�ximo de caracteres do campo.
	 *
	 * <p>M�todo para setar o n�mero m�ximo de caracteres do campo.</p>
	 * @access public
	 * @param integer $piMaxLength N�mero m�ximo de caracteres
	 */
	public function setAttrMaxLength($piMaxLength) {
		if($piMaxLength > 0)
		$this->ciMaxLength = $piMaxLength;
		else
		trigger_error("Invalid maxlength value: '{$piMaxLength}'",E_USER_WARNING);
	}

	/**
	 * Seta o nome do Label do objeto.
	 *
	 * <p>M�todo para setar o nome do label do objeto, utilizado para a troca de cor, no caso do objeto ter
	 * preenchimento obrigat�rio e n�o estar preenchido.</p>
	 * @access public
	 * @param string $psLabel nome do label associado ao objeto
	 */
	public function setAttrLabel($psLabel) {
		$this->csLabel = $psLabel;
	}

	/**
	 * Retorna o valor booleano do atributo Password.
	 *
	 * <p>Retorna o valor booleano TRUE se o texto est�tico deve ser exibido
	 * como password. Retorna FALSE caso contr�rio.</p>
	 * @access public
	 * @return boolean Indica se o texto � do tipo password ou n�o
	 */
	public function getAttrPassword() {
		return $this->cbPassword;
	}

	/**
	 * Retorna o valor booleano do atributo Disabled.
	 *
	 * <p>Retorna o valor booleano TRUE se o campo est� desabilitado.
	 * Retorna FALSE caso contr�rio.</p>
	 * @access public
	 * @return boolean Indica se o campo est� desabilitado ou n�o
	 */
	public function getAttrDisabled() {
		return $this->cbDisabled;
	}

	/**
	 * Retorna o n�mero m�ximo de caracteres do campo.
	 *
	 * <p>M�todo para retornar o n�mero m�ximo de caracteres do campo.</p>
	 * @access public
	 * @return integer N�mero m�ximo de caracteres
	 */
	public function getAttrMaxLength() {
		return $this->ciMaxLength;
	}

	/**
	 * Adiciona uma m�scara.
	 *
	 * <p>Adiciona um objeto m�scara da classe FWDMask na classe FWDText.</p>
	 * @access public
	 * @param FWDMask $poMask M�scara
	 */
	public function addObjFWDMask($poMask) {
		$this->coMask = $poMask;
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDMemo. Implementa �reas de texto.
 *
 * <p>Classe que implementa �reas de texto (html tag 'textarea').</p>
 * @package FWD5
 * @subpackage view
 */
class FWDMemo extends FWDView implements FWDPostValue {

	/**
	 * Define se a memo est� desabilitado ou n�o
	 * @var boolean
	 * @access protected
	 */
	protected $cbDisabled = false;

	/**
	 * Define se deve existir uma barra de rolagem no caso de um overflow
	 * @var boolean
	 * @access protected
	 */
	protected $cbScrollbar = true;

	/**
	 * Nome do label associado ao objeto
	 * @var string
	 * @access protected
	 */
	protected $csLabel = "";

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDMemo.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites da memo
	 */
	public function __construct($poBox = null) {
		parent::__construct($poBox);
	}

	/**
	 * Desenha o cabe�alho do memo.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o cabe�alho do memo.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do memo
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawHeader($poBox = null) {

		$msScrollbar = ($this->getAttrScrollbar()?"overflow:auto;":"overflow:hidden;");

		$msStyle = $this->getStyle();
		$msStyle = "style='{$msStyle}{$msScrollbar}'";

		$msAttrDefault = $this->getGlobalProperty();

		$msDisabled = $this->cbDisabled ? "disabled" : "";
		$msLabel = "";
		if($this->csLabel)
		$msLabel = "label='{$this->csLabel}'";

		return "<textarea {$msAttrDefault} {$this->getEvents()} {$msStyle} {$msLabel} {$msDisabled}>";
	}

	/**
	 * Desenha o corpo do memo.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o corpo do memo.</p>
	 * @access public
	 *
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawBody() {
		return $this->getValue();
	}

	/**
	 * Desenha o rodap� do memo.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o rodap� do memo.</p>
	 * @access public
	 *
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawFooter() {
		return "</textarea>\n";
	}

	/**
	 * Atribui valor � vari�vel value.
	 *
	 * <p>Atribui valor � vari�vel value, devendo retirar espa�os em branco
	 * no inicio e/ou no fim da string.</p>
	 * @access public
	 * @param string $psValue Valor a ser atribu�do
	 */
	public function setAttrValue($psValue) {
		$this->coString->setAttrValue(trim($psValue));
	}

	/**
	 * Seta a memo como desabilitada.
	 *
	 * <p>M�todo para setar a memo como desabilitada.</p>
	 * @access public
	 * @param boolean $pbDisabled Define se a memo est� desabilitada ou n�o
	 */
	public function setAttrDisabled($pbDisabled) {
		$this->cbDisabled = FWDWebLib::attributeParser($pbDisabled,array("true"=>true, "false"=>false),"pbDisabled");
	}

	/**
	 * Seta o nome do Label do objeto.
	 *
	 * <p>M�todo para setar o nome do label do objeto, utilizado para a troca de cor, no caso do objeto ter
	 * preenchimento obrigat�rio e n�o estar preenchido.</p>
	 * @access public
	 * @param string $psLabel nome do label associado ao objeto
	 */
	public function setAttrLabel($psLabel) {
		$this->csLabel = $psLabel;
	}

	/**
	 * Retorna o valor booleano do atributo Disabled.
	 *
	 * <p>Retorna o valor booleano TRUE se a memo est� desabilitada.
	 * Retorna FALSE caso contr�rio.</p>
	 * @access public
	 * @return boolean Indica se a memo est� desabilitada ou n�o
	 */
	public function getAttrDisabled() {
		return $this->cbDisabled;
	}

	/**
	 * Seta o valor booleano do atributo Scrollbar.
	 *
	 * <p>Seta o valor booleano do atributo que define se deve existir
	 * uma barra de rolagem no caso de um overflow. Para a barra de rolagem
	 * existir, o atributo deve conter o valor booleano TRUE. Para n�o existir,
	 * o atributo deve conter o valor booleano FALSE.</p>
	 * @access public
	 * @param string $pbScrollbar Define se deve existir uma barra de rolagem no caso de um overflow
	 */
	public function setAttrScrollbar($pbScrollbar) {
		$this->cbScrollbar = FWDWebLib::attributeParser($pbScrollbar,array("true"=>true, "false"=>false),"pbScrollbar");
	}

	/**
	 * Retorna o valor booleano do atributo Scrollbar.
	 *
	 * <p>Retorna o valor booleano TRUE se deve existir uma barra de rolagem
	 * no caso de um overflow. Retorna FALSE caso contr�rio.</p>
	 * @access public
	 * @return boolean Indica se deve existir uma barra de rolagem no caso de um overflow
	 */
	public function getAttrScrollbar() {
		return $this->cbScrollbar;
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDButton. Implementa um bot�o.
 *
 * <p>Classe que implementa bot�o.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDButton extends FWDView {

	/**
	 * Define se a memo est� desabilitado ou n�o
	 * @var booleanDefine se a memo est� desabilitado ou n�o
	 * @access protected
	 */
	protected $cbDisabled = false;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDButton.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites da imagem clic�vel
	 */
	public function __construct($poBox = null) {
		parent::__construct($poBox);
	}

	/**
	 * Desenha o bot�o.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o bot�o.</p>
	 * @access public
	 *
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawBody() {
		$msDisabled = $this->cbDisabled ? "disabled" : "";
		return "<input type='button' {$this->getGlobalProperty()} style='{$this->getStyle()}'  value='{$this->getValue()}' {$this->getEvents()} {$msDisabled}/>\n";
	}

	/**
	 * Seta a memo como desabilitada.
	 *
	 * <p>M�todo para setar a memo como desabilitada.</p>
	 * @access public
	 * @param boolean $pbDisabled Define se a memo est� desabilitada ou n�o
	 */
	public function setAttrDisabled($pbDisabled) {
		$this->cbDisabled = FWDWebLib::attributeParser($pbDisabled,array("true"=>true, "false"=>false),"pbDisabled");
	}

	/**
	 * Retorna o valor booleano do atributo Disabled.
	 *
	 * <p>Retorna o valor booleano TRUE se a memo est� desabilitada.
	 * Retorna FALSE caso contr�rio.</p>
	 * @access public
	 * @return boolean Indica se a memo est� desabilitada ou n�o
	 */
	public function getAttrDisabled() {
		return $this->cbDisabled;
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDCalendar. Implementa calend�rios customiz�vies em javascript.
 *
 * <p>Utiliza bibliotecas javascript para acessar o calend�rio. Atributos
 * customiz�veis atrav�s da passagem de par�metros para o arquivo
 * calendar-setup.js. Estilo customiz�vel atrav�s de arquivos .css.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDCalendar extends FWDDrawing implements FWDPostValue,FWDCollectable {

	/**
	 * Define se o calend�rio � ou n�o o primeiro que aparece na p�gina (necess�rio para carregar os arquivos javascript somente uma vez)
	 * @var boolean
	 * @access private
	 */
	private static $cbFirstCalendar = true;

	/**
	 * Define se o input onde a data � inserida � vis�vel ou n�o
	 * @var boolean
	 * @access private
	 */
	private $cbHidden = false;

	/**
	 * Nome do calend�rio ( 'name' do input e 'id' do img scr)
	 * @var string
	 * @access private
	 */
	private $csName = "";

	/**
	 * Define o valor do calend�rio atribu�do pelo usu�rio (POST) ou pelo programador (XML)
	 * @var string
	 * @access private
	 */
	private $csValue = "";

	/**
	 * Define se os dias s�o selecionados com um ou dois cliques
	 * @var string
	 * @access private
	 */
	private $csSingleClick = "false";

	/**
	 * Define o estilo (arquivo .css) do calend�rio (ex.: "blue" ir� carregar o arquivo "calendar-blue.css")
	 * @var string
	 * @access private
	 */
	private $csStyle = "win2k-cold-1";

	/**
	 * Define se a coluna com os n�meros das semanas deve ser vis�vel ou n�o
	 * @var string
	 * @access private
	 */
	private $csWeekNumbers = "false";

	/**
	 * Objeto EventHandler de eventos a executar (trigger = OnUpdate)
	 * @var FWDEventHandler
	 * @access private
	 */
	private $coEvent = null;
		
	/**
	 * Componente vertical do alinhamento do calendario
	 * @var string
	 * @access private
	 */
	private $csValign = "B";

	/**
	 * Componente horizontal do alinhamento do calendario
	 * @var string
	 * @access private
	 */
	private $csHalign = "l";

	/**
	 * Define se o trigger button � exibido antes ou depois do input field
	 * @var boolean
	 * @access private
	 */
	private $cbBeforeInput = false;

	/**
	 * Nome do label associado ao objeto
	 * @var string
	 * @access private
	 */
	private $csLabel = "";

	/**
	 * Define se o campo est� desabilitado ou n�o
	 * @var boolean
	 * @access protected
	 */
	protected $cbDisabled = false;

	/**
	 * Define se o timestamp retorna a data com hor�rio 00:00(false) ou 23:59(true)
	 * @var boolean
	 * @access private
	 */
	private $cbEndDate = false;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDCalendar.</p>
	 * @access public
	 */
	public function __construct(){
	}

	/**
	 * Desenha o cabe�alho do calend�rio.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML/javascript, todas
	 * as informa��es necess�rias para implementar o cabe�alho do calend�rio.</p>
	 * @access public
	 * @param FWDBox $poFWDBox Box que define os limites do calend�rio
	 * @return string C�digo HTML/javascript que ser� inserido na p�gina
	 */
	public function drawHeader($poFWDBox = null) {
		$moWebLib = FWDWebLib::getInstance();
		$msGFXRef = $moWebLib->getGfxRef();

		$msOut = "";

		$msType = ($this->cbHidden ? "hidden":"text");

		$msLabel = $this->csLabel ? "label='{$this->csLabel}'":"";
		$msClass = get_class($this);

		/*
		 * Monta o evento que vai ser disparado quando ocorrer o evento de Onclick
		 * no campo text associado ao calend�rio.
		 */
		$msEvent = "if (gebi(\"{$this->csName}\").value != \"\") {gebi(\"{$this->csName}\").value = \"\";{$this->getEvents()};}";

		$msTriggerButtonStyle = "cursor: pointer;";
		$msFakeButtonStyle = (FWDWebLib::browserIsIE()?'margin-left:-4;':''); // Contorna bug do IE que deixa a imagem deslocada

		if($this->cbDisabled){
			$msTriggerButtonStyle.= "display:none;";
			$msInputFieldDisabled = " disabled";
		}else{
			$msFakeButtonStyle.= "display:none;";
			$msInputFieldDisabled = "";
		}

		$msTriggerButton = "<img src='{$msGFXRef}icon-calendar.gif' id='".$this->csName."_trigger_button"."' style='{$msTriggerButtonStyle}' />\n"
		."<img src='{$msGFXRef}icon-calendar.gif' id='".$this->csName."_fake_button"."' style='{$msFakeButtonStyle}' />\n";
		$msInputField = "<input class='{$msClass} type='{$msType}' value='".$this->csValue."' id='".$this->csName."' name='".$this->csName."' {$msLabel} readonly='1' size='8' style='height:20px' onclick='{$msEvent}'{$msInputFieldDisabled}/>\n";

		$msOut .= $this->cbBeforeInput ? $msTriggerButton.$msInputField : $msInputField.$msTriggerButton;

		return $msOut;
	}

	/**
	 * Desenha o corpo do calend�rio.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML/javascript, todas
	 * as informa��es necess�rias para implementar o corpo do calend�rio.</p>
	 * @access public
	 * @param FWDBox $poFWDBox Box que define os limites do calend�rio
	 * @return string C�digo HTML/javascript que ser� inserido na p�gina
	 */
	public function drawBody($poFWDBox = null) {
		$moWebLib = FWDWebLib::getInstance();
		$msCSSRef = $moWebLib->getCSSRef();
		$msJsRef = $moWebLib->getJsRef();
		$msOut = '';
		if(self::$cbFirstCalendar){
			$msOut .= "<link rel='stylesheet' type='text/css' media='all' href='{$msCSSRef}css/nonauto/calendar-" . $this->csStyle . ".css' title='{$this->csStyle}' />\n";
			$msOut .= "<script type='text/javascript' src='{$msJsRef}calendar.js'></script>\n";
			$msOut .= "<script type='text/javascript' src='{$msJsRef}calendar-setup.js'></script>\n";
			$msOut .= "<script type='text/javascript' src='{$msJsRef}calendar-" . FWDLanguage::getPHPStringValue('locale', "pt_br") . ".js'></script>\n";
			self::$cbFirstCalendar = false;
		}else{
		}
		return $msOut;
	}

	/**
	 * Desenha o rodap� do calend�rio.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML/javascript, todas
	 * as informa��es necess�rias para implementar o rodap� do calend�rio.</p>
	 * @access public
	 * @param FWDBox $poFWDBox Box que define os limites do calend�rio
	 * @return string C�digo HTML/javascript que ser� inserido na p�gina
	 */
	public function drawFooter($poFWDBox = null) {
		$msOut = "";
		$msOut .= "<script type='text/javascript'>\n";
		$msOut .= "  gebi('{$this->csName}').object = new FWDCalendar('{$this->csName}');\n";
		$msOut .= "  function func_OnUpdate_{$this->csName}() { {$this->getEvents()} }\n";
		$msOut .= "  Calendar.setup({\n";
		$msOut .= "    inputField     :    '" . $this->csName . "',\n";
		$msOut .= "    ifFormat       :    '" . FWDLanguage::getPHPStringValue('mx_shortdate_format','') . "',\n";
		$msOut .= "    button         :    '" . $this->csName . "_trigger_button"."',\n";
		$msOut .= "    align          :    '" . $this->csValign . $this->csHalign . "',\n";
		$msOut .= "    singleClick    :    " . $this->csSingleClick . ",\n";
		$msOut .= "    weekNumbers    :    " . $this->csWeekNumbers . ",\n";
		$msOut .= "    onUpdate       :    func_OnUpdate_" . $this->csName . "\n";
		$msOut .= "  });\n";
		$msOut .= "</script>\n";

		return $msOut;
	}

	/**
	 * Seta o nome do Label do objeto.
	 *
	 * <p>M�todo para setar o nome do label do objeto, utilizado para a troca de cor, no caso do objeto ter
	 * preenchimento obrigat�rio e n�o estar preenchido.</p>
	 * @access public
	 * @param string $psLabel nome do label associado ao objeto
	 */
	public function setAttrLabel($psLabel) {
		$this->csLabel = $psLabel;
	}

	/**
	 * Define se o campo 'input' deve ser vis�vel ou n�o.
	 *
	 * <p>Para o campo 'input' (no qual � inserida a data selecionada no
	 * calend�rio) ser invis�vel, o atributo cbHidden deve conter o valor
	 * booleano TRUE. Para o ser vis�vel, o atributo cdHidden deve conter
	 * o valor booleano FALSE.</p>
	 * @access public
	 * @param string $pbHidden Flag para exibir/esconder o campo 'input'
	 */
	public function setAttrHidden($pbHidden) {
		$this->cbHidden = FWDWebLib::attributeParser($pbHidden,array("true"=>true, "false"=>false),"cbHidden");
	}

	/**
	 * Seta o nome do calend�rio.
	 *
	 * <p>A string contida no atributo csName � utilizada diretamente como
	 * 'name' do input, e concatenada ao sufixo '_target_button' como 'id'
	 * do img scr.</p>
	 * @access public
	 * @param string $psName Nome que identifica o calend�rio
	 */
	public function setAttrName($psName) {
		$this->csName = $psName;
	}

	/**
	 * Atribui valor � vari�vel value.
	 *
	 * <p>Atribui valor � vari�vel value retirando espa�os em branco no
	 * inicio e/ou no fim da string.</p>
	 * @access public
	 * @param string $psValue Valor a ser atribu�do
	 */
	public function setAttrValue($psValue) {
		$this->csValue = trim($psValue);
	}

	/**
	 * Define se os dias devem ser selecionados com um ou dois cliques.
	 *
	 * <p>Para selecionar os dias com um �nico clique, o atributo csSingleClick
	 * deve conter a string "true". Para selecionar com dois cliques,
	 * csSingleClick deve conter a string "false".</p>
	 * @access public
	 * @param string $psSingleClick Flag para ativar/desativar a sele��o dos dias com um �nico clique
	 */
	public function setAttrSingleClick($psSingleClick) {
		$this->csSingleClick = FWDWebLib::attributeParser($psSingleClick,array("true"=>"true", "false"=>"false"),"csSingleClick");
	}

	/**
	 * Seta o nome do arquivo de estilo a ser utilizado.
	 *
	 * <p>O atributo csStyle deve conter apenas o n�cleo do nome do arquivo .css.
	 * (i.e. ignorar o prefixo 'calendar-' e o sufixo '.css').
	 * Por exemplo: para carregar o arquivo 'calendar-blue.css', o atributo
	 * csStyle dever� conter a string "blue" .</p>
	 * @access public
	 * @param string $psStyle N�cleo do nome do arquivo .css
	 */
	public function setAttrStyle($psStyle) {
		$this->csStyle = FWDWebLib::attributeParser($psStyle,array("blue","blue2","brown","green","system","win2k-1","win2k-2","win2k-cold-1","win2k-cold-2"),"csStyle");
	}

	/**
	 * Define se a coluna referente aos n�meros das semanas deve ser vis�vel ou n�o.
	 *
	 * <p>Para a coluna referente aos n�meros das semanas ser vis�vel, o
	 * atributo csWeekNumbers deve conter a string "true". Para ser invis�vel,
	 * deve conter a string "false".</p>
	 * @access public
	 * @param string $psWeekNumbers Flag para ativar/desativar a coluna dos n�meros das semanas
	 */
	public function setAttrWeekNumbers($psWeekNumbers) {
		$this->csWeekNumbers = FWDWebLib::attributeParser($psWeekNumbers,array("true"=>"true", "false"=>"false"),"csWeekNumbers");
	}

	/**
	 * Define a componente vertical do alinhamento do calendario.
	 *
	 * <p>Utiliza o attributeParser para verificar a consist�ncia da entrada e
	 * ja realizar a convers�o para o formato utilizado internamente pelo calendario:
	 *  T -- completamente acima do elemento de refer�ncia (margem inferior do calendario alinhada � margem superior do elemento).
	 *	t -- acima do elemento mas pode sobrescrev�-lo (margem inferior do calendario alinhada � margem inferior do elemento).
	 *	C -- verticalmente centralizado em rela��o ao elemento de refer�ncia. Pode sobrescrev�-lo (depende do alinhamento horizontal).
	 *	b -- abaixo do elemento mas pode sobrescrev�-lo (margem superior do calendario alinhada � margem superior do elemento).
	 *	B -- completamente abaixo do elemento de refer�ncia (margem superior do calendario alinhada � margem inferior do elemento).</p>
	 * @access public
	 * @param string $psValign Componente vertical do alinhamento do calendario
	 */
	public function setAttrValign($psValign) {
		$this->csValign = FWDWebLib::attributeParser($psValign,array("top"=>"T", "center"=>"C", "bottom"=>"B"),"csValign");
	}

	/**
	 * Define a componente horizontal do alinhamento do calendario.
	 *
	 * <p>Utiliza o attributeParser para verificar a consist�ncia da entrada e
	 * ja realizar a convers�o para o formato utilizado internamente pelo calendario:
	 *	L -- completamente � esquerda do elemento de refer�ncia (margem direita do calendario alinhada � margem esquerda do elemento).
	 *	l -- � esquerda do elemento mas pode sobrescrev�-lo (margem esquerda do calendario alinhada � margem esquerda do elemento).
	 *	C -- horizontalmente centralizado em rela��o ao elemento de refer�ncia. Pode sobrescrev�-lo, dependendo do alinhamento vertical.
	 *	r -- � direita do elemento mas pode sobrescrev�-lo (margem direita do calendario alinhada � margem direita do elemento).
	 *	R -- completamente � direita do elemento de refer�ncia (margem esquerda do calendario alinhada � margem direita do elemento).</p>
	 * @access public
	 * @param string $psHalign Componente horizontal do alinhamento do calendario
	 */
	public function setAttrHalign($psHalign) {
		$this->csHalign = FWDWebLib::attributeParser($psHalign,array("left"=>"l", "center"=>"C", "right"=>"r"),"csHalign");
	}

	/**
	 * Define se o trigger button do calendario � exibido antes ou depois do input field.
	 *
	 * <p>Para o trigger button ser exibido antes (� esquerda) do input field,
	 * o atributo cbBeforeInput deve conter o valor booleano 'true'. Para ser
	 * exibido depois (� direita), o atributo cbBeforeInput deve conter o valor
	 * booleano 'false'.</p>
	 * @access public
	 * @param boolean $pbBeforeInput Define se o trigger button deve ser exibido antes ou depois do input field
	 */
	public function setAttrBeforeInput($pbBeforeInput) {
		$this->cbBeforeInput = FWDWebLib::attributeParser($pbBeforeInput,array("true"=>true, "false"=>false),"cbBeforeInput");
	}

	/**
	 * Seta o evento de OnUpdate, se existir.
	 *
	 * <p>Seta o evento de OnUpdate. Antes � realizada uma verifia��o
	 * de consist�ncia atrav�s da chamada do attributeParser.</p>
	 * @access public
	 * @param object $poEvent Objeto de evento
	 * @param string $psEvent Alvo do evento
	 */
	public function addEvent($poEvent,$psEvent) {
		if(!isset($this->coEvent)) {
			$this->coEvent = new FWDEventHandler("onupdate",$this->getAttrName());
		}
		FWDWebLib::attributeParser(strtolower($this->coEvent->getAttrEvent()),array("onupdate"),"psEvent");
		$this->coEvent->setAttrContent($poEvent);
	}

	/**
	 * Adiciona um objeto de evento (OnUpdate).
	 *
	 * <p>Adiciona um objeto de evento (OnUpdate) da classe FWDEvent
	 * na classe FWDCalendar.</p>
	 * @access public
	 * @param object $poEvent Objeto de evento
	 * @param string $psEvent Alvo do evento
	 */
	public function addObjFWDEvent($poEvent,$psEvent) {
		$this->addEvent($poEvent,$psEvent);
	}

	/**
	 * Retorna a string contendo os eventos adicionados no calend�rio.
	 *
	 * <p>Retorna a string contendo os eventos adicionados no calend�rio.</p>
	 * @access public
	 * @return string Eventos
	 */
	public function getEvents() {
		$msEvent = "";
		if(isset($this->coEvent)) {
			$maEvent = $this->coEvent->getAttrContent();
			foreach($maEvent as $moEvent) {
				$moEvent->setAttrSource("gebi('" . $this->csName . "')");
				$msEvent .= " " . $moEvent->render();
			}
		}
		else {
			$msEvent = "return 0;";
		}
		return $msEvent;
	}

	/**
	 * Atribui valor, condicionalmente, � vari�vel value.
	 *
	 * <p>Atribui valor � vari�vel value, condicionando atrav�s do segundo par�metro.
	 * Se pbForce for FALSE, n�o atribui o valor se a vari�vel tiver conte�do;
	 * se TRUE atribui mesmo que tenha conte�do.</p>
	 * @access public
	 * @param string $psValue Valor a ser atribu�do
	 * @param boolean $pbForce For�ar o valor mesmo que j� tenha conte�do
	 */
	public function setValue($psValue, $pbForce = true) {
		if (!isset($this->csValue) || !$this->csValue || $pbForce ){
			$this->setAttrValue($psValue);
		}
	}

	/**
	 * Retorna o valor da vari�vel value.
	 *
	 * <p>Retorna o valor da vari�vel value.</p>
	 * @access public
	 * @return string Data contida no objeto de calend�rio
	 */
	public function getValue() {
		return $this->csValue;
	}

	/**
	 * Seta a data baseado em um timestamp.
	 *
	 * <p>M�todo para setar a data baseado em um timestamp.</p>
	 * @access public
	 * @param integer $piTimestamp Timestamp
	 */
	public function setTimestamp($piTimestamp){
		$maDate = getdate($piTimestamp);
		$msDateFormat = FWDLanguage::getPHPStringValue('mx_shortdate_format','');
		if($msDateFormat == "%m/%d/%Y"){
			$msDay = $maDate['mon'] . "/" . $maDate['mday'] . "/" . $maDate['year'];
		}else{
			$msDay = $maDate['mday'] . "/" . $maDate['mon'] . "/" . $maDate['year'];
		}
		$this->setValue($msDay);
	}

	/**
	 * Retorna o timestamp da data.
	 *
	 * <p>M�todo para retornar o timestamp da data.</p>
	 * @access public
	 * @return integer Timestamp
	 */
	public function getTimestamp() {
		$miTimestamp = 0;
		if ($this->csValue) {
			$maDate = explode("/", $this->csValue);
			$msDateFormat = FWDLanguage::getPHPStringValue('mx_shortdate_format','');
			if ($msDateFormat == "%m/%d/%Y")
			$miTimestamp = mktime(0, 0, 0, $maDate[0], $maDate[1], $maDate[2]);
			else if ($msDateFormat == "%d/%m/%Y")
			$miTimestamp = mktime(0, 0, 0, $maDate[1], $maDate[0], $maDate[2]);
			else
			trigger_error("Invalid date format!", E_USER_WARNING);
		}
		if ($miTimestamp && $this->getAttrEndDate()) {
			$miTimestamp = $miTimestamp + 86399;
		}
		return $miTimestamp;
	}

	/**
	 * Retorna o nome atribuido ao calend�rio.
	 *
	 * <p>Retorna o nome atribuido ao calend�rio.</p>
	 * @access public
	 * @return string Nome do objeto de calend�rio
	 */
	public function getAttrName() {
		return $this->csName;
	}

	/**
	 * Retorna o dia atribuido ao calend�rio.
	 *
	 * <p>Retorna o dia atribuido ao calend�rio.</p>
	 * @access public
	 * @return integer Dia do objeto de calend�rio
	 */
	public function getDay() {
		$maDate = getdate($this->getTimestamp());
		return $maDate["mday"];
	}

	/**
	 * Retorna o m�s atribuido ao calend�rio.
	 *
	 * <p>Retorna o m�s atribuido ao calend�rio.</p>
	 * @access public
	 * @return integer M�s do objeto de calend�rio
	 */
	public function getMonth() {
		$maDate = getdate($this->getTimestamp());
		return $maDate["mon"];
	}

	/**
	 * Retorna o ano atribuido ao calend�rio.
	 *
	 * <p>Retorna o ano atribuido ao calend�rio.</p>
	 * @access public
	 * @return integer Ano do objeto de calend�rio
	 */
	public function getYear() {
		$maDate = getdate($this->getTimestamp());
		return $maDate["year"];
	}

	/**
	 * Retorna o array de eventos do calendario e o pr�prio calend�rio.
	 *
	 * <p>Retorna o array de eventos do calendario e o pr�prio calend�rio.</p>
	 * @access public
	 * @return array Eventos do calend�rio e o pr�prio calend�rio
	 */
	public function collect() {
		$maOut = array($this);
		if (isset($this->coEvent))
		$maOut = array_merge($maOut,$this->coEvent->getAttrContent());
		return $maOut;
	}

	/**
	 * Seta o campo como desabilitado.
	 *
	 * <p>M�todo para setar o campo como desabilitado.</p>
	 * @access public
	 * @param boolean $pbDisabled Define se o campo est� desabilitado ou n�o
	 */
	public function setAttrDisabled($pbDisabled){
		$this->cbDisabled = FWDWebLib::attributeParser($pbDisabled,array('true'=>true, 'false'=>false), 'disable');
	}

	/**
	 * Retorna o valor do atributo enddate
	 *
	 * <p>Retorna o valor do atributo enddate.</p>
	 * @access public
	 * @return boolean Valor do atributo enddate
	 */
	public function getAttrEndDate(){
		return $this->cbEndDate;
	}

	/**
	 * Seta o valor do atributo enddate
	 *
	 * <p>Seta o valor do atributo enddate.</p>
	 * @access public
	 * @param string $psEndDate Novo valor do atributo
	 */
	public function setAttrEndDate($psEndDate){
		$this->cbEndDate = FWDWebLib::attributeParser($psEndDate,array("true"=>true, "false"=>false),"psEndDate");
	}

}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDIcon. Implementa �cones (imagens).
 *
 * <p>Classe simples para manipula��o de �cones (imagens, html tag 'img scr').</p>
 * @package FWD5
 * @subpackage view
 */
class FWDIcon extends FWDView {

	/**
	 * Source do �cone (path relativo ao sistema de arquivos local)
	 * @var string
	 * @access protected
	 */
	protected $csSrc = "";

	/**
	 * Define a profundidade do �cone, para manipular sobreposi��o de elementos (zenith-index)
	 * @var integer
	 * @access protected
	 */
	protected $ciIndex = 0;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDIcon.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do �cone (imagem)
	 */
	public function __construct($poBox = null) {
		parent::__construct($poBox);
	}

	/**
	 * Desenha o cabe�alho do �cone.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o cabe�alho do �cone.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do �cone
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	protected function drawHeader($poFWDBox = null) {
		return "<img ";
	}

	/**
	 * Desenha o corpo do �cone.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o corpo do �cone.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do �cone
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	protected function drawBody($poFWDBox = null) {
		$msOut = "";

		$msSrc = $this->getAttrSrc();
		$msSrc = "src='{$msSrc}'";

		//$this->getImageBox(); /* Esse linha serve para funcionar com o XML compilado */
		$msStyle = $this->getStyle();

		if ($this->ciIndex != 0)
		$msStyle .= "z-index:".$this->getAttrIndex().";";
			
		$msAttrDefault = $this->getGlobalProperty();

		$msOut .= "{$msSrc} style='{$msStyle} ' {$msAttrDefault} {$this->getEvents()}";
		return $msOut;
	}

	/**
	 * Desenha o rodap� do �cone.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o rodap� do �cone.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do �cone
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	protected function drawFooter($poFWDBox = null) {
		return " />";
	}

	/**
	 * Seta o source do �cone.
	 *
	 * <p>Seta o source do �cone (path da imagem). Seta tamb�m a altura
	 * e a largura do �cone.</p>
	 * @access public
	 * @param string $psSrc Source do �cone
	 */
	public function setAttrSrc($psSrc) {
		$this->csSrc = $psSrc;
		//$this->getImageBox();
	}

	/**
	 * Seta a profundidade do �cone.
	 *
	 * <p>Seta a profundidade do �cone (zenith-index).</p>
	 * @access public
	 * @param integer $piIndex Profundidade do �cone
	 */
	public function setAttrIndex($piIndex) {
		$this->ciIndex = $piIndex;
	}

	/**
	 * Retorna o source do �cone.
	 *
	 * <p>Retorna o source do �cone (path da imagem).</p>
	 * @access public
	 * @return string Source do �cone
	 */
	public function getAttrSrc() {
		return $this->csSrc;
	}

	/**
	 * Retorna a profundidade do �cone.
	 *
	 * <p>Retorna a profundidade do �cone (zenith-index).</p>
	 * @access public
	 * @return integer Profundidade do �cone
	 */
	public function getAttrIndex() {
		return $this->ciIndex;
	}

	/**
	 * Seta a altura e a largura do �cone.
	 *
	 * <p>Seta a altura e a largura do �cone (imagem).</p>
	 * @access public
	 */
	public function getImageBox(){
		$msSrc = $this->csSrc;
		if(file_exists($msSrc)){
			$maBox = @getimagesize($msSrc);
			if ($maBox) {
				$this->coBox->setAttrWidth($maBox[0]);
				$this->coBox->setAttrHeight($maBox[1]);
			}
		}
	}

}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,	technics, statements, and computer programs
 * contain	unpublished	proprietary information of	Axur Communications,
 * Inc.,	and are	protected	by applied	copyright law.	They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without	the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes	codigos,	tecnicas, tratados e	programas de computador contem
 * informacao proprietaria	nao publicada pela Axur Communications, Inc.,
 * e sao	protegidas pelas leis	de direito registrado.	Essas, nao podem
 * ser dispostas	a terceiros, copiadas ou	duplicadas de qualquer forma,
 * no	todo ou	em parte,	sem	consentimento	previo	escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDStatic. Implementa texto est�tico.
 *
 * <p>Classe que implementa texto est�tico (html tag 'span').</p>
 * @package FWD5
 * @subpackage view
 */
class FWDStatic extends FWDView implements FWDPostValue {

	/**
	 * Define se deve existir uma barra de rolagem no caso de um overflow
	 * @var boolean
	 * @access protected
	 */
	protected $cbScrollbar = false;


	/**
	 * Define o tipo de alinhamento horizontal do texto contido no static
	 * @var string
	 * @access protected
	 */
	protected $csHorizontal = "left";

	/**
	 * Define o tipo de alinhamento vertical do texto contido no static
	 * @var boolean
	 * @access protected
	 */
	protected $csVertical = "top";

	/**
	 * Define a margem � esquerda do static
	 * @var boolean
	 * @access protected
	 */
	protected $ciMarginLeft=0;


	/**
	 * Define a margem � direita do static
	 * @var boolean
	 * @access protected
	 */
	protected $ciMarginRight=0;


	/**
	 * Define a margem de top do static
	 * @var boolean
	 * @access protected
	 */
	protected $ciMarginTop = 0;

	/**
	 * Define a margem de bottom do static
	 * @var boolean
	 * @access protected
	 */
	protected $ciMarginBottom=0;

	/**
	 * Define se o static deve ter nowrap ou n�o
	 * @var boolean
	 * @access protected
	 */
	protected $cbNoWrap = true;

	/**
	 * Define se o text associado com o static deve ser preenchido
	 * @var boolean
	 * @access protected
	 */
	protected $cbMustFill = false;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDStatic.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do texto est�tico
	 */
	public function __construct($poBox = null) {
		parent::__construct($poBox);
	}

	/**
	 * Desenha o cabe�alho do texto est�tico.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o cabe�alho do texto est�tico.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do texto est�tico
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawHeader($poBox = null) {
		$msScrollbar = ($this->getAttrScrollbar()?"overflow:auto;":"overflow:hidden;");
		$this->setAttrTabIndex(0);

		//recalcula as dimen��es da box de acordo com a margem definida
		$moBox=$this->getObjFWDBox();

		//clone da box para evitar o efeito "mem�ria" nas altera��es das dimen��es da box do static
		$moAuxBox = clone $moBox;
		if($moBox)
		{
			//margem a esquerda
			$moBox->setAttrWidth($moBox->getAttrWidth() - $this->ciMarginLeft);
			$moBox->setAttrLeft($moBox->getAttrLeft() + $this->ciMarginLeft);
				
			//margem a direita
			$moBox->setAttrWidth($moBox->getAttrWidth() - $this->ciMarginRight);
				
			//margem Top
			$moBox->setAttrTop($moBox->getAttrTop() + $this->ciMarginTop);
			$moBox->setAttrHeight($moBox->getAttrHeight() - $this->ciMarginTop);
				
			//margem Bottom
			//$moBox->setAttrTop($moBox->getAttrTop() + $this->ciMarginBottom);
			$moBox->setAttrHeight($moBox->getAttrHeight() - $this->ciMarginBottom);
		}
		else
		{
			//trigger error
		}
		$msTDclass="";
		if($this->cbNoWrap==true)
		$msTDclass .= "class='FWDTD'";
			
		$msAlign = "";
		$msAlign.= " align=\"".$this->csHorizontal."\" valign=\"" . $this->csVertical . "\" ";
		$msStyle = $this->getStyle();
		$msStyle = "style='{$msStyle}{$msScrollbar}'";
		$msAttrDefault = $this->getGlobalProperty();

		$msOut = "\n<table {$msAttrDefault} {$this->getEvents()} {$msStyle} > <tr> <td id='".$this->csName."_td' {$msAlign} {$msTDclass}>";

		$this->setObjFWDBox($moAuxBox);
		return $msOut;
	}

	/**
	 * Desenha o corpo do texto est�tico.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o corpo do texto est�tico.</p>
	 * @access public
	 *
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawBody() {
		$msValue = $this->getValue();
		if ($this->cbMustFill)
		$msValue = "<font color='red'>* </font>" . $msValue;
		return $msValue;
	}

	/**
	 * Desenha o rodap� do texto est�tico.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o rodap� do texto est�tico.</p>
	 * @access public
	 *
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawFooter() {
		$msReturn ="";
		$msReturn .= "</td></tr> </table> \n";
		if($this->csName)
		$msReturn .=" <script language='javascript'>gebi('{$this->csName}').object = new FWDStatic('{$this->csName}'); </script> ";

		return $msReturn;
	}

	/**
	 * Atribui valor � vari�vel value.
	 *
	 * <p>Atribui valor � vari�vel value, devendo retirar espa�os em branco
	 * no inicio e/ou no fim da string.</p>
	 * @access public
	 * @param string $psValue Valor a ser atribu�do
	 */
	public function setAttrValue($psValue) {
		$this->setValue(trim($psValue));
	}

	/**
	 * Concatena um valor na string.
	 *
	 * <p>M�todo para concatenar um valor na string.</p>
	 * @access public
	 * @param string $psValue Valor a ser concatenado
	 */
	public function concatValue($psValue) {
		$this->setValue(" " . trim($psValue), false);
	}

	/**
	 * Seta o valor booleano do atributo Scrollbar.
	 *
	 * <p>Seta o valor booleano do atributo que define se deve existir
	 * uma barra de rolagem no caso de um overflow. Para a barra de rolagem
	 * existir, o atributo deve conter o valor booleano TRUE. Para n�o existir,
	 * o atributo deve conter o valor booleano FALSE.</p>
	 * @access public
	 * @param string $pbScrollbar Define se deve existir uma barra de rolagem no caso de um overflow
	 */
	public function setAttrScrollbar($pbScrollbar) {
		$this->cbScrollbar = FWDWebLib::attributeParser($pbScrollbar,array("true"=>true, "false"=>false),"pbScrollbar");
	}

	/**
	 * Seta o valor booleano do atributo MustFill.
	 *
	 * <p>Seta o valor booleano do atributo que define se o campo text
	 * associado ao static deve ser preenchido.</p>
	 * @access public
	 * @param boolean $pbMustFill Define se o text associado com o static deve ser preenchido
	 */
	public function setAttrMustFill($pbMustFill) {
		$this->cbMustFill = FWDWebLib::attributeParser($pbMustFill,array("true"=>true, "false"=>false),"pbMustFill");
	}

	/**
	 * Seta o tipo de alinhamento horizontal do static.
	 *
	 * <p>Seta o tipo de alinhamento horizontal do static (valores possiveis -> left, center, right , justify)</p>
	 * @access public
	 * @param string $psHorizontal Define o tipo de alinhamento horizontal do static
	 */
	function setAttrHorizontal($psHorizontal)
	{
		$this->csHorizontal= FWDWebLib::attributeParser($psHorizontal,array("left","center","right","justify"),"psHorizontal");

	}

	/**
	 * Seta o tipo de alinhamento vertical do static.
	 *
	 * <p>Seta o tipo de alinhamento vertical do static (valores possiveis -> top, middle, bottom)</p>
	 * @access public
	 * @param string $psHorizontal Define o tipo de alinhamento horizontal do static
	 */
	function setAttrVertical($psVertical)
	{
		$this->csVertical = FWDWebLib::attributeParser($psVertical,array("top","middle","bottom"),"psVertical");
	}

	/**
	 * Retorna o valor do alinhamento horizontal.
	 *
	 * <p>Retorna o valor do alinhamento horizontal do static</p>
	 * @access public
	 * @return string Alinhamento horizontal do static
	 */
	function getAttrVertical()
	{
		return $this->csVertical;
	}

	/**
	 * Retorna o valor do alinhamento vertical.
	 *
	 * <p>Retorna o valor do alinhamento vertical do static</p>
	 * @access public
	 * @return string Alinhamento vertical do static
	 */
	function getAttrHorizontal()
	{
		return $this->csHorizontal;
	}


	/**
	 * Seta a margem left do static.
	 *
	 * <p>Seta o valor da margem left do static</p>
	 * @access public
	 * @param string $psMargin Define a margem left do static
	 */
	function setAttrMarginLeft($piMarginLeft)
	{
		if($piMarginLeft>=0)
		$this->ciMarginLeft = $piMarginLeft;
		else
		trigger_error("MarginLeft must be a positive integer, got ={$piMarginLeft}",E_USER_ERROR);

	}

	/**
	 * Seta a margem right do static.
	 *
	 * <p>Seta o valor da margem right do static</p>
	 * @access public
	 * @param string $psMargin Define a margem right do static
	 */
	function setAttrMarginRight($piMarginRight)
	{
		if($piMarginRight>=0)
		$this->ciMarginRight = $piMarginRight;
		else
		trigger_error("MarginRight must be a positive integer, got ={$piMarginRight}",E_USER_ERROR);
	}

	/**
	 * Seta a margem top do static.
	 *
	 * <p>Seta o valor da margem top do static</p>
	 * @access public
	 * @param string $psMargin Define a margem top do static
	 */
	function setAttrMarginTop($piMarginTop)
	{
		if($piMarginTop>=0)
		$this->ciMarginTop = $piMarginTop;
		else
		trigger_error("MarginTop must be a positive integer, got ={$piMarginTop}",E_USER_ERROR);
	}

	/**
	 * Seta a margem bottom do static.
	 *
	 * <p>Seta o valor da margem bottom do static</p>
	 * @access public
	 * @param string $psMargin Define a margem bottom do static
	 */
	function setAttrMarginBottom($piMarginBottom)
	{
		if($piMarginBottom>=0)
		$this->ciMarginBottom = $piMarginBottom;
		else
		trigger_error("MarginBottom must be a positive integer, got ={$piMarginBottom}",E_USER_ERROR);
	}

	/**
	 * Retorna o valor da margem left do static.
	 *
	 * <p>Retorna o valor da margem left do static</p>
	 * @access public
	 * @return integer Valor da margem left do static
	 */
	function getAttrMarginLeft()
	{
		return $this->ciMarginLeft;
	}

	/**
	 * Retorna o valor da margem top do static.
	 *
	 * <p>Retorna o valor da margem top do static</p>
	 * @access public
	 * @return integer Valor da margem top do static
	 */
	function getAttrMarginTop()
	{
		return $this->ciMarginTop;
	}

	/**
	 * Retorna o valor da margem right do static.
	 *
	 * <p>Retorna o valor da margem right do static</p>
	 * @access public
	 * @return integer Valor da margem right do static
	 */
	function getAttrMarginRight()
	{
		return $this->ciMarginRight;
	}

	/**
	 * Retorna o valor da margem bottom do static.
	 *
	 * <p>Retorna o valor da margem bottom do static</p>
	 * @access public
	 * @return integer Valor da margem bottom do static
	 */
	function getAttrMarginBottom()
	{
		return $this->ciMarginBottom;
	}

	/**
	 * Retorna o valor booleano do atributo Scrollbar.
	 *
	 * <p>Retorna o valor booleano TRUE se deve existir uma barra de rolagem
	 * no caso de um overflow. Retorna FALSE caso contr�rio.</p>
	 * @access public
	 * @return boolean Indica se deve existir uma barra de rolagem no caso de um overflow
	 */
	public function getAttrScrollbar() {
		return $this->cbScrollbar;
	}

	/**
	 * Retorna o valor booleano do atributo MustFill.
	 *
	 * <p>M�todo para retornar o valor booleano do atributo MustFill.</p>
	 * @access public
	 * @return boolean Indica se deve existir uma barra de rolagem no caso de um overflow
	 */
	public function getAttrMustFill() {
		return $this->cbMustFill;
	}

	/**
	 * Atribui valor NoEscape ao objeto string do Static.
	 *
	 * <p>Atribui valor NOEscape ao objeto FWDString do static para evitar que sejam escapados os "'" da string.</p>
	 * @access public
	 * @param boolean $pbNoEscape valor do noEscape ("true" ou "false")
	 */

	public function setAttrStringNoEscape($pbNoEscape) {
		$this->coString->setAttrNoEscape($pbNoEscape);
	}

	/**
	 * Adiciona um link ao valor do Static.
	 *
	 * <p>M�todo para adicionar um link ao valor do Static. Insere
	 * (ou concatena, caso o objeto coString ja exista) um link no valor
	 * do Static (objeto FWDString).</p>
	 * @access public
	 * @param FWDLink $poLink Link a ser adicionado (concatenado) ao Static
	 */
	public function setObjFWDLink(FWDLink $poLink) {
		$this->coString->setAttrNoEscape(true);
		if (isset($this->coString)) {
			$this->coString->setValue($poLink->draw());
		}
		else {
			$this->coString = $poLink->draw();
		}
	}

	public function setFWDIconCode($psIcon) {
		$this->coString->setAttrNoEscape(true);
		if (isset($this->coString)) {
			$this->coString->setValue($psIcon);
				
		}
	}

	public function setAttrNoWrap($pbNoWrap){
		$this->cbNoWrap = FWDWebLib::attributeParser($pbNoWrap,array("true"=>true, "false"=>false),"pbNoWrap");
	}

	public function getAttrNoWrap(){
		return $this->cbNoWrap;
	}

	/**
	 * Substitui uma string por outra no valor do static.
	 *
	 * <p>M�todo para substituir uma string por outra no valor do static.</p>
	 * @access public
	 * @param string $psSearch String que deve ser retirada
	 * @param string $psReplace String que deve ser inserida
	 */
	public function replace($psSearch, $psReplace) {
		$msNewValue = str_replace($psSearch, $psReplace, $this->getValue());
		$this->setValue($msNewValue);
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDWarning. Implementa uma alerta.
 *
 * <p>Classe que implementa alertas de aviso para o usu�rio.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDWarning extends FWDStatic {

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDWarning.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do texto est�tico
	 */
	public function __construct($poBox = null){
		parent::__construct($poBox);
		$this->cbNoWrap = false;
		$this->cbDisplay = false;
	}

	/**
	 * Desenha o cabe�alho do texto est�tico.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o cabe�alho do texto est�tico.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do texto est�tico
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawHeader($poBox = null){
		$this->setAttrTabIndex(0);

		$msGfxRef = FWDWebLib::getInstance()->getGfxRef();

		if($this->cbNoWrap==true){
			$msTDclass = "class='FWDWarning_TD_nowrap'";
		}else{
			$msTDclass = "class='FWDWarning_TD'";
		}

		$msAlign = " align=\"".$this->csHorizontal."\" valign=\"" . $this->csVertical . "\" ";
		$msStyle = "style='{$this->getStyle()}border-spacing:0px;border-collapse:collapse;'";

		$moEvent = new FWDClientEvent();
		$moEvent->setAttrEvent('onClick');
		$moEvent->setAttrValue("gobi('{$this->csName}').hide();");
		$this->addObjFWDEvent($moEvent);

		return "<table {$this->getGlobalProperty()} {$this->getEvents()} {$msStyle}>"
		."<tr>"
		."<td style='padding:0px;left;background-image: url({$msGfxRef}bg_warning_NW.gif);width:5'></td>"
		."<td style='padding:0px;background-image: url({$msGfxRef}bg_warning_N.gif);height:5'></td>"
		."<td style='padding:0px;background-image: url({$msGfxRef}bg_warning_NE.gif);width:5'></td>"
		."</tr>"
		."<tr>"
		."<td style='padding:0px;background-image: url({$msGfxRef}bg_warning_W.gif);width:5'></td>"
		."<td style='cursor: pointer;' id='{$this->csName}_td' {$msAlign} {$msTDclass}>";
	}

	/**
	 * Desenha o corpo do texto est�tico.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o corpo do texto est�tico.</p>
	 * @access public
	 *
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawBody(){
		return $this->getValue();
	}

	/**
	 * Desenha o rodap� do texto est�tico.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o rodap� do texto est�tico.</p>
	 * @access public
	 *
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawFooter(){
		$msGfxRef = FWDWebLib::getInstance()->getGfxRef();
		$msReturn = "</td>"
		."<td style='padding:0px;background-image: url({$msGfxRef}bg_warning_E.gif);width:5'></td>"
		."</tr>"
		."<tr>"
		."<td style='padding:0px;background-image: url({$msGfxRef}bg_warning_SW.gif);width:5'></td>"
		."<td style='padding:0px;background-image: url({$msGfxRef}bg_warning_S.gif);height:5'></td>"
		."<td style='padding:0px;background-image: url({$msGfxRef}bg_warning_SE.gif);width:5'></td>"
		."</tr>"
		."</table>";
		if($this->csName){
			$msReturn .=" <script language='javascript'>gebi('{$this->csName}').object = new FWDWarning('{$this->csName}'); </script> ";
		}
		return $msReturn;
	}

}

?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDItem. Implementa Itens.
 *
 * <p>Classe simples para manipula��o de Itens (utilizados em selects, em
 * radioboxes, etc).</p>
 * @package FWD5
 * @subpackage view
 */
class FWDItem extends FWDDrawing {

	/**
	 * Valor chave do Item
	 * @var string
	 * @access protected
	 */
	protected $csKey = "";
		
	/**
	 * Define se o Item est� marcado (checked) ou n�o
	 * @var boolean
	 * @access protected
	 */
	protected $cbCheck = false;

	/**
	 * Classe CSS
	 * @var string
	 * @access protected
	 */
	protected $csClass = '';

	/**
	 * Valor do elemento
	 * @var FWDString
	 * @access protected
	 */
	protected $coString = null;

	/**
	 * Tag do elemento (permiss�es: define se o elemento deve ser desenhado ou n�o)
	 * @var string
	 * @access protected
	 */
	protected $csTag = "";

	/**
	 * Nome do elemento
	 * @var string
	 * @access protected
	 */
	protected $csName = "";

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDItem.</p>
	 * @access public
	 */
	public function __construct() {
		$this->coString = new FWDString();
	}

	/**
	 * Desenha o cabe�alho do item.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o cabe�alho do item.</p>
	 * @access public
	 *
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawHeader() {
		$msSelected = (($this->getAttrCheck())?"selected":"");
		$msKey = $this->getAttrKey();
		$msName = ($this->csName?$this->csName:'');
		if($msName)
		$msName = "name='".$msName."'";

		if($this->csClass) $msClass = " class='{$this->csClass}'";
		else $msClass = '';
		return "<option $msName value='{$msKey}' {$msSelected}{$msClass}>";
	}

	/**
	 * Desenha o corpo do item.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o corpo do item.</p>
	 * @access public
	 *
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawBody() {
		return $this->getValue();
	}

	/**
	 * Desenha o rodap� do item.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o rodap� do item.</p>
	 * @access public
	 *
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawFooter() {
		return "</option>\n";
	}

	/**
	 * Seta o valor do item.
	 *
	 * <p>M�todo para setar o valor do item.</p>
	 * @access public
	 * @param FWDString $poString Seta o valor do item
	 */
	public function setObjFWDString(FWDString $poString) {
		$this->coString = $poString;
	}

	/**
	 * Seta o valor chave do Item.
	 *
	 * <p>Seta o valor chave do Item.</p>
	 * @access public
	 * @param string $psValue Valor chave do Item
	 */
	public function setAttrKey($psValue) {
		$this->csKey = $psValue;
	}

	/**
	 * Seta o Item como Checked/Not Checked.
	 *
	 * <p>Seta o valor booleano de Item marcado/n�o marcado.</p>
	 * @access public
	 * @param boolean $pbValue Define se o Item est� marcado ou n�o
	 */
	public function setAttrCheck($pbValue) {
		$this->cbCheck = $pbValue;
	}

	/**
	 * Atribui valor, condicionalmente.
	 *
	 * <p>Atribui valor ao item, condicionando atrav�s do segundo par�metro.
	 * Se force for FALSE, concatena o valor se a vari�vel tiver conte�do;
	 * se TRUE atribui mesmo que tenha conte�do (sobrescreve).</p>
	 * @access public
	 * @param string $psValue Valor a ser atribu�do
	 * @param boolean $pbForce For�ar o valor mesmo que j� tenha conte�do
	 */
	public function setValue($psValue, $pbForce = true) {
		$this->coString->setValue($psValue, $pbForce);
	}

	/**
	 * Seta o nome do elemento.
	 *
	 * <p>M�todo para setar o nome do elemento.</p>
	 * @access public
	 * @param string $psName Nome do elemento
	 */
	public function setAttrName($psName) {
		$this->csName = $psName;
	}

	/**
	 * Retorna o nome do elemento.
	 *
	 * <p>M�todo para retornar o nome do elemento.</p>
	 * @access public
	 * @return string Nome do elemento
	 */
	final public function getAttrName() {
		return $this->csName;
	}

	/**
	 * Retorna o valor chave do item.
	 *
	 * <p>Retorna o valor chave do item.</p>
	 * @access public
	 * @return string Valor chave do item
	 */
	public function getAttrKey() {
		return $this->csKey;
	}

	/**
	 * Retorna o valor booleano do atributo Check.
	 *
	 * <p>Retorna o valor booleano TRUE se o item est� marcado. Retorna
	 * FALSE caso contr�rio.</p>
	 * @access public
	 * @return boolean Indica se o Item est� marcado ou n�o
	 */
	public function getAttrCheck() {
		return $this->cbCheck;
	}

	/**
	 * Retorna o valor do item.
	 *
	 * <p>M�todo para retornar o valor do item.</p>
	 * @access public
	 * @return string Valor do item
	 */
	public function getValue() {
		return $this->coString->getAttrString();
	}

	/**
	 * Retorna o objeto de string do elemento.
	 *
	 * <p>M�todo para retornar o objeto de string do elemento.</p>
	 * @access public
	 * @return FWDString String do elemento
	 */
	public function getObjFWDString() {
		return $this->coString;
	}

	/**
	 * Retorna o valor do atributo class
	 *
	 * <p>Retorna o valor do atributo class.</p>
	 * @access public
	 * @return string Valor do atributo class
	 */
	public function getAttrClass(){
		return $this->csClass;
	}

	/**
	 * Seta o valor do atributo class
	 *
	 * <p>Seta o valor do atributo class.</p>
	 * @access public
	 * @param string $psClass Novo valor do atributo
	 */
	public function setAttrClass($psClass){
		$this->csClass = $psClass;
	}

	/**
	 * Seta a tag do elemento.
	 *
	 * <p>M�todo para setar a tag do elemento.</p>
	 * @access public
	 * @param string $psTag Tag do elemento
	 */
	public function setAttrTag($psTag) {
		$this->csTag = $psTag;
	}

	/**
	 * Retorna a tag do elemento.
	 *
	 * <p>M�todo para retornar a tag do elemento.</p>
	 * @access public
	 * @return string Tag do elemento
	 */
	public function getAttrTag() {
		return $this->csTag;
	}

}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDLink. Implementa Links.
 *
 * <p>Classe que implementa Links em elementos da FWD5.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDLink extends FWDDrawing {

	/**
	 * Uniform Resource Identifier do recurso Web
	 * @var string
	 * @access private
	 */
	private $csURI = "";

	/**
	 * Window do Link (atributo target da tag html 'a')
	 * @var string
	 * @access private
	 */
	private $csWindow = "_self";

	/**
	 * Classe CSS
	 * @var string
	 * @access private
	 */
	private $csClass = "FWDLink";

	/**
	 * Valor do Link
	 * @var FWDString
	 * @access private
	 */
	private $coString = null;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDLink.</p>
	 * @access public
	 */
	public function __construct() {
	}

	/**
	 * Desenha o cabe�alho do link.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o cabe�alho do link.</p>
	 * @access public
	 *
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawHeader() {
		$msURI = $this->getAttrURI();
		if (!$msURI)
		trigger_error("Object FWDLink must have an URI",E_USER_WARNING);

		$msWindow = $this->getAttrWindow();
		$msClass = $this->getAttrClass() ? $this->getAttrClass() : "";

		return "\n<a href='{$msURI}' target='{$msWindow}' class='{$msClass}' style='cursor:pointer' >\n";
	}

	/**
	 * Desenha o corpo do link.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o corpo do link.</p>
	 * @access public
	 *
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawBody() {
		$msOut = $this->coString->getAttrString();
		if (!$msOut)
		trigger_error("Object FWDLink must have a non-empty string",E_USER_WARNING);

		return $msOut;
	}

	/**
	 * Desenha o rodap� do link.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o rodap� do link.</p>
	 * @access public
	 *
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawFooter() {
		return "</a>\n";
	}

	/**
	 * Seta o URI do link.
	 *
	 * <p>M�todo para setar o URI do link.</p>
	 * @access public
	 * @param string $psURI URI do link
	 */
	public function setAttrURI($psURI) {
		$this->csURI = $psURI;
	}

	/**
	 * Retorna o URI do link.
	 *
	 * <p>M�todo para retornar o URI do link.</p>
	 * @access public
	 * @return string URI do link
	 */
	public function getAttrURI() {
		return $this->csURI;
	}

	/**
	 * Seta a Window do link.
	 *
	 * <p>M�todo para setar a Window do link.</p>
	 * @access public
	 * @param string $psWindow Window do link
	 */
	public function setAttrWindow($psWindow) {
		$this->csWindow = $psWindow;
	}

	/**
	 * Retorna a Window do link.
	 *
	 * <p>M�todo para retornar a Window do link.</p>
	 * @access public
	 * @return string Window do link
	 */
	public function getAttrWindow() {
		return $this->csWindow;
	}

	/**
	 * Seta a classe de CSS do link.
	 *
	 * <p>M�todo para setar a classe de CSS do link.</p>
	 * @access public
	 * @param string $psClass Classe
	 */
	public function setAttrClass($psClass) {
		$this->csClass = $psClass;
	}

	/**
	 * Retorna a classe de CSS do link.
	 *
	 * <p>M�todo para retornar a classe de CSS do link.</p>
	 * @access public
	 * @return string Classe de CSS
	 */
	public function getAttrClass() {
		return $this->csClass;
	}

	/**
	 * Adiciona um objeto String (valor do Link).
	 *
	 * <p>Adiciona um objeto String (valor do Link).</p>
	 * @access public
	 * @param FWDString $poString Seta o valor do Link
	 */
	public function addObjFWDString(FWDString $poString) {
		$this->coString = $poString;
	}
}
?><?
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDViewButton. Implementa um elemento clic�vel.
 *
 * <p>Classe que implementa um elemento clic�vel.</p>
 *
 * @package FWD5
 * @subpackage view
 */
class FWDViewButton extends FWDViewGroup {

	/**
	 * Atributo disabled do viewButton
	 * @var boolean
	 * @access protected
	 */
	private $cbDisabled = false;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDViewButton.</p>
	 * @access public
	 * @param FWDBox poBox Box que define os limites da �rea do elemento
	 */
	public function __construct($poBox = null) {
		parent::__construct($poBox);
	}

	/**
	 * Desenha o cabe�alho do viewbuttom.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o cabe�alho do viewbuttom.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do viewbuttom
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawHeader($poBox = null) {
		$this->setAttrTabIndex(0);

		$msOut = '';
		//div da imagem que fica por cima da div do viewbutton
		if(($this->cbDisplay==true))
		$mbShowIt = true;
		else
		$mbShowIt = false;
		 
		$msImgStyle = $this->coBox->draw();
		$msImgStyle.= $mbShowIt ? "display:block;" : "display:none;";
		$msImgStyle.= "position:{$this->getAttrPosition()};";
		$msImgStyle.= $this->csDebug ? $this->debugView() : '';
		$msOut .= "  <div id='{$this->csName}_dvImg' name='{$this->csName}_dvImg' class='FWDViewButtonTransp' style='{$msImgStyle}' {$this->getEvents()}></div>\n";

		//div para o viewButton disabled
		$msImgDisableStyle = $this->coBox->draw();
		$msImgDisableStyle.= "position:{$this->getAttrPosition()};";
		$msImgDisableStyle.= ($this->cbDisabled && $this->cbDisplay)? "display:block;" : "display:none;";
		if (FWDWebLib::browserIsIE())
		$msImgDisableStyle .= " Filter: Alpha(Opacity=50); ";
		else
		$msImgDisableStyle .= " opacity:0.5;";
		$msOut .= "  <div id='{$this->csName}_dvImgDisabled' name='{$this->csName}_dvImgDisabled' class='FWDViewButtonNotTransp' style='{$msImgDisableStyle}'></div>\n";

		$msOut .= "  <div {$this->getGlobalProperty()} style='{$this->getStyle()}'>\n";
		return $msOut;
	}
	 
	/**
	 * Desenha o rodap� do viewbutton.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o rodap� do viewbutton.</p>
	 * @access public
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawFooter() {
		if($this->csName)
		return "\n  </div> <script language='javascript'>gebi('{$this->csName}').object = new FWDViewButton('{$this->csName}','{$this->cbDisabled}'); </script> ";
		else
		return "\n  </div> \n";
	}
	 
	/**
	 * Seta o campo como desabilitado.
	 *
	 * <p>M�todo para setar o campo como desabilitado.</p>
	 * @access public
	 * @param boolean $pbDisabled Define se o campo est� desabilitado ou n�o
	 */
	public function setAttrDisabled($pbDisabled) {
		$this->cbDisabled = FWDWebLib::attributeParser($pbDisabled,array("true"=>true, "false"=>false),"pbDisabled");
	}

	/**
	 * Retorna o valor booleano do atributo Disabled.
	 *
	 * <p>Retorna o valor booleano TRUE se o campo est� desabilitado.
	 * Retorna FALSE caso contr�rio.</p>
	 * @access public
	 * @return boolean Indica se o campo est� desabilitado ou n�o
	 */
	public function getAttrDisabled() {
		return $this->cbDisabled;
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDTreeBase. Implementa �rvores em javascript.
 *
 * <p>Classe que implementa �rvores em javascript.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDTreeBase extends FWDView implements FWDCollectable {

	/**
	 * Id da �rvore (necess�rio para as fun��es em JavaScript que manipulam a �rvore)
	 * @var integer
	 * @access protected
	 */
	protected $ciTreeId = 0;

	/**
	 * Array de objetos FWDTree representando os nodos da �rvore
	 * @var array
	 * @access protected
	 */
	protected $caNodes = array();

	/**
	 * Array de objetos FWDController
	 * @var array
	 * @access protected
	 */
	protected $caControllers = array();

	/**
	 * Id do nodo (o id � usado pelo javascript)
	 * @var integer
	 * @access protected
	 */
	protected $csId;

	/**
	 * Indica se os nodos da �rvore est�o abertos por default
	 * @var boolean
	 * @access protected
	 */
	protected $cbExpandAll = false;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDTreeBase.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites da �rvore
	 */
	public function __construct($poBox = null){
		parent::__construct($poBox);
	}

	/**
	 * Desenha o cabe�alho do objeto.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o cabe�alho do objeto.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do objeto
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	protected function drawHeader($poBox = null){
		$msOut = '';
		if(count($this->caControllers)>0){
			foreach($this->caControllers as $moController){
				$msOut.= $moController->draw();
			}
		}
		$msOut.= "<div {$this->getGlobalProperty()} style='{$this->getStyle()}' {$this->getEvents()}>";
		return $msOut;
	}

	/**
	 * Desenha o corpo do objeto.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o corpo do objeto.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do objeto
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	protected function drawBody($poBox = null){
		$moWebLib = FWDWebLib::getInstance();
		$msGFX = $moWebLib->getGfxRef();
		$msOut = "<script> var gfx_ref = '$msGFX'; </script>\n";
		$miNumNodes = count($this->caNodes);
		for($miI=0;$miI<$miNumNodes-1;$miI++){
			$msOut.= $this->caNodes[$miI]->draw(false);
		}
		$msOut.= $this->caNodes[$miI]->draw(true);
		return $msOut;
	}

	/**
	 * Desenha o rodap� do objeto.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o rodap� do objeto.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do objeto
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	protected function drawFooter($poBox = null){
		return '</div>';
	}

	/**
	 * Retorna o Id da �rvore.
	 *
	 * <p>Retorna o Id da �rvore.</p>
	 * @access public
	 * @return integer Id da �rvore
	 */
	public function getTreeId(){
		return $this->ciTreeId;
	}

	/**
	 * Seta o Id da �rvore.
	 *
	 * <p>Seta o valor do atributo Id da �rvore.</p>
	 * @access public
	 * @param integer $piTreeId Id da �rvore
	 */
	public function setTreeId($piTreeId){
		$this->ciTreeId = $piTreeId;
	}

	/**
	 * Seta o atributo expandall.
	 *
	 * <p>Seta o valor do atributo expandall, que indica se os nodos da �rvore est�o abertos por default.</p>
	 * @access public
	 * @param string $psExpandAll Define se os nodos da �rvore est�o abertos por default
	 */
	public function setAttrExpandAll($psExpandAll){
		$this->cbExpandAll = FWDWebLib::attributeParser($psExpandAll,array("true"=>true, "false"=>false),"expandall");
	}

	/**
	 * Retorna o valor do atributo expandall.
	 *
	 * <p>Retorna o valor booleano do atributo expandall.</p>
	 * @access public
	 * @return boolean Indica se os nodos da �rvore est�o abertos por default
	 */
	public function getAttrExpandAll(){
		return $this->cbExpandAll;
	}

	/**
	 * Adiciona uma �rvore.
	 *
	 * <p>Adiciona uma �rvore com suporte a itens da classe FWDTree
	 * na classe FWDTreeBase.</p>
	 * @access public
	 * @param FWDTree $poTree �rvore a ser adicionada
	 */
	public function addObjFWDTree($poTree){
		$poTree->setTreeBaseName($this->getAttrName());
		$this->caNodes[] = $poTree;
	}

	/**
	 * Adiciona um Controller
	 *
	 * <p>Adiciona um objeto FWDController na classe FWDTreeBase.</p>
	 * @access public
	 * @param FWDController $poController Controller a ser adicionado
	 */
	public function addObjFWDController($poController){
		$this->caControllers[] = $poController;
	}

	/**
	 * Retorna um array com todos sub-elementos da �rvore e a pr�pria �rvore
	 *
	 * <p>Retorna um array contendo todos elementos contidos na �rvore ou em suas
	 * sub-�rvores (retorna tamb�m a pr�pria �rvore).</p>
	 * @access public
	 * @return array Array de sub-elementos
	 */
	public function collect(){
		$maOut = array($this);
		if(count($this->caNodes)>0){
			foreach($this->caNodes as $moTree){
				$maCollection = $moTree->collect();
				$maOut = array_merge($maOut,$maCollection);
			}
		}
		if(count($this->caControllers)>0){
			foreach($this->caControllers as $moController){
				$maCollection = $moController->collect();
				$maOut = array_merge($maOut,$maCollection);
			}
		}
		return $maOut;
	}

	/**
	 * Seta os ids de todos os nodos da �rvore
	 *
	 * <p>Percorre a �rvore setando os ids de todos os nodos.
	 * OBS: Os par�metros somente s�o usados na recurs�o.
	 * </p>
	 *
	 * @access public
	 * @param string $psParentId Id do pai
	 * @param integer $piNumber N�mero
	 */
	public function setIds($psParentId='',$piNumber=1){
		if($psParentId!='') $this->csId = $psParentId.'.'.$piNumber;
		elseif($this->csId=='') $this->csId = $piNumber;
		$miNumNodes = count($this->caNodes);
		for($miI=0;$miI<$miNumNodes;$miI++){
			$this->caNodes[$miI]->setIds($this->csId,$miI+1);
		}
	}

	/**
	 * M�todo executado quando o XML do elemento termina de carregar.
	 *
	 * <p>M�todo executado quando o XML do elemento termina de carregar. Seta os
	 * ids de todos os nodos da �rvore, mas s� se n�o estiver respondendo a um
	 * evento e sim carregando a tela.</p>
	 *
	 * @access public
	 */
	public function execute(){
		if(!FWDWebLib::getPOST("event_js")) $this->setIds();
	}

}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDTree. Implementa �rvores com suporte a itens.
 *
 * <p>Classe que implementa �rvores em javascript com suporte a itens (radio,check).</p>
 * @package FWD5
 * @subpackage view
 */
class FWDTree extends FWDTreeBase {

	/**
	 * Container que cont�m todos elementos presentes no nodo
	 * @var FWDViewgroup
	 * @access protected
	 */
	protected $coViewGroup = null;

	/**
	 * Nome da TreeBase a que a Tree pertence
	 * @var FWDTreeBaseName
	 * @access protected
	 */
	protected $csTreeBaseName;

	/**
	 * Indica se o nodo est� aberto inicialmente
	 * @var boolean
	 * @access protected
	 */
	protected $cbExpanded = null;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDTree.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites da tree
	 */
	public function __construct($poBox = null) {
		parent::__construct($poBox);
		$this->coViewGroup = new FWDViewgroup();
		$this->coViewGroup->setAttrClass('FWDTree_ViewGroup');
		$this->coViewGroup->setAttrPosition('relative');
	}

	/**
	 * Desenha em tela.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML/javascript,
	 * todas as informa��es necess�rias para exibir em tela o objeto.</p>
	 * @access public
	 * @param boolean $pbLastSibling Verdadeiro se for o �ltimo elemento
	 * @param FWDBox $poBox Box que define os limites do objeto
	 * @return string C�digo HTML/javascript que ser� inserido na p�gina
	 */
	public function draw($pbLastSibling,$poFWDBox = null){
		$msOut = "";
		if ($this->cbShouldDraw) {

			$msOut .= $this->drawHeader($poFWDBox);
			$msOut .= $this->drawBody($pbLastSibling,$poFWDBox);
			$msOut .=  $this->drawFooter($poFWDBox);

		}
		return $msOut;
	}

	/**
	 * Desenha o cabe�alho do objeto.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o cabe�alho do objeto.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do objeto
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	protected function drawHeader($poBox = null){
		return "<table border='0' {$this->getGlobalProperty()}>\n";
	}

	/**
	 * Desenha o corpo do objeto.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o corpo do objeto.</p>
	 * @access public
	 * @param boolean $pbLastSibling Verdadeiro se for o �ltimo elemento
	 * @param FWDBox $poBox Box que define os limites do objeto
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	protected function drawBody($pbLastSibling,$poBox = null){
		$moWebLib = FWDWebLib::getInstance();
		$msGFX = $moWebLib->getGfxRef();
		$msId = "{$this->getTreeBaseName()}_{$this->csId}";
		$miNumNodes = count($this->caNodes);
		$mbOpen = $this->getAttrExpanded();
		$msStyleWithBackground = "style='vertical-align:top;background:url({$msGFX}line.gif);background-repeat:repeat-y;background-position:top;'";
		$msStyleWithoutBackground = "style='vertical-align:top;'";
		$msOut = '';
		$msTreeClass = $this->getAttrClass();

		$maCollection = $this->coViewGroup->collect();
		if(count($maCollection)>0){
			foreach($maCollection as $msKey => $moView) $moView->setAttrPosition('relative');
		}
		$this->coViewGroup->setAttrName($this->getAttrName().'_vg');

		$msOut.= "<tr class='{$msTreeClass}_tr'>\n";
		$msOut.= "<td class='{$msTreeClass}_td' ".($pbLastSibling?$msStyleWithoutBackground:$msStyleWithBackground).">\n";
		if($miNumNodes>0){
			$msOut.= "<a href=\"javascript:toggle('$msId')\"><img id='{$msId}_img' src='{$msGFX}".($mbOpen?'minus':'plus').".gif'></a>\n";
		}else{
			$msOut.= "<img id='{$msId}_img' src='{$msGFX}join.gif'>\n";
		}
		$msOut.= "</td>\n";
		$msOut.= "<td class='{$msTreeClass}_td'>\n";

		$msOut.= $this->coViewGroup->drawHeader();
		$maContent = $this->coViewGroup->getContent();
		$mbNeedshift = false;
		foreach($maContent as $moContent){
			if($moContent instanceof FWDStatic)
			$moContent->setAttrClass('StaticTree');
			$msOut .= $moContent->draw();
		}
		$msOut .= $this->coViewGroup->drawFooter();

		$msOut.= "</td>\n";
		$msOut.= "</tr>\n";
		if($miNumNodes>0){
			$msOut.= "<tr id='{$msId}_tr' class='{$msTreeClass}_tr'".($mbOpen?'':' style="display:none"').">\n";
			$msOut.= "<td class='{$msTreeClass}_td' ".($pbLastSibling?$msStyleWithoutBackground:$msStyleWithBackground)."></td>\n";
			$msOut.= "<td class='{$msTreeClass}_td'>\n";
			for($miI=0;$miI<$miNumNodes-1;$miI++){
				$msOut.= $this->caNodes[$miI]->draw(false);
			}
			$msOut.= $this->caNodes[$miI]->draw(true);
			$msOut.= "</td>\n";
			$msOut.= "</tr>\n";
		}
		return $msOut;
	}

	/**
	 * Desenha o rodap� do objeto.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o rodap� do objeto.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do objeto
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	protected function drawFooter($poBox = null){
		return '</table>';
	}

	/**
	 * Adiciona uma �rvore.
	 *
	 * <p>Adiciona um objeto �rvore da classe FWDTree na classe FWDTree
	 * (�rvores podem conter outras �rvores).</p>
	 * @access public
	 * @param FWDTree $poTree �rvore a ser adicionada
	 * @deprecated 28/04/2006
	 */
	public function addObjFWDTree($poTree){
		$this->caNodes[] = $poTree;
	}

	/**
	 * Adiciona uma view
	 *
	 * <p>Adiciona uma view ao nodo raiz da �rvore.</p>
	 * @access public
	 * @param FWDView $poView View
	 */
	public function addObjFWDView($poView){
		$this->coViewGroup->addObjFWDView($poView);
	}

	/**
	 * Retorna um array com todos sub-elementos da �rvore
	 *
	 * <p>Retorna um array contendo todos elementos contidos no nodo da �rvore
	 * ou em suas sub-�rvores.</p>
	 * @access public
	 * @return array Array de sub-elementos
	 */
	public function collect(){
		$maOut = $this->coViewGroup->collect();
		$maOut = array_merge($maOut,parent::collect());
		return $maOut;
	}

	/**
	 * Seta o atributo expanded.
	 *
	 * <p>Seta o valor do atributo expanded, que indica se o nodo est� aberto inicialmente.</p>
	 * @access public
	 * @param string $psExpanded Define se o nodo est� aberto inicialmente
	 */
	public function setAttrExpanded($psExpanded){
		$this->cbExpanded = FWDWebLib::attributeParser($psExpanded,array("true"=>true, "false"=>false),"expanded");
	}

	/**
	 * Retorna o valor do atributo expanded.
	 *
	 * <p>Retorna o valor booleano do atributo expanded.</p>
	 * @access public
	 * @return boolean Indica se o nodo est� aberto inicialmente
	 */
	public function getAttrExpanded(){
		if($this->cbExpanded!==null){
			return $this->cbExpanded;
		}else{
			$moWebLib = FWDWebLib::getInstance();
			$moTreeBase = $moWebLib->getObject($this->getTreeBaseName());
			if(!$moTreeBase) trigger_error("treeBaseName undefined",E_USER_WARNING);
			return $moTreeBase->getAttrExpandAll();
		}
	}

	/**
	 * Seta o nome da TreeBase a que a Tree pertence
	 *
	 * <p>Seta o nome da TreeBase a que a Tree pertence (propaga recursivamente
	 * para todos descendentes).</p>
	 * @access public
	 * @param string $psTreeBaseName Nome da TreeBase a que a Tree pertence
	 */
	public function setTreeBaseName($psTreeBaseName){
		$this->csTreeBaseName = $psTreeBaseName;
		if(count($this->caNodes)>0){
			foreach($this->caNodes as $moNode){
				$moNode->setTreeBaseName($psTreeBaseName);
			}
		}
	}

	/**
	 * Retorna o nome da TreeBase a que a Tree pertence
	 *
	 * <p>Retorna o nome da TreeBase a que a Tree pertence.</p>
	 * @access public
	 * @return string Nome da TreeBase a que a Tree pertence
	 */
	public function getTreeBaseName(){
		return $this->csTreeBaseName;
	}

	/**
	 * Elimina todas as trees pertencentes � mesma.
	 *
	 * <p>Deleta as sub-�rvores desta.
	 * @access public
	 */
	public function cleanTree() {
		$this->caNodes = array();
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDTreeAjax. Implementa �rvores com suporte a eventos Ajax.
 *
 * <p>Classe que implementa �rvores em javascript com suporte a eventos Ajax.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDTreeAjax extends FWDTree {

	/**
	 * Fun��o do evento Ajax. OBS: N�o esta sendo usado, por enquanto.
	 * @var string
	 * @access protected
	 */
	protected $csFunction;

	/**
	 * Par�metros do evento Ajax. OBS: N�o esta sendo usado, por enquanto.
	 * @var string
	 * @access protected
	 */
	protected $csParameter;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDTreeAjax.</p>
	 * @access public
	 */
	public function __construct(){
		parent::__construct();
	}

	/**
	 * Desenha o corpo do objeto.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o corpo do objeto.</p>
	 * @access public
	 * @param boolean $pbLastSibling Verdadeiro se for o �ltimo elemento
	 * @param FWDBox $poBox Box que define os limites do objeto
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	protected function drawBody($pbLastSibling,$poBox = null){
		$moWebLib = FWDWebLib::getInstance();
		$msGFX = $moWebLib->getGfxRef();
		$msId = "{$this->getTreeBaseName()}_{$this->csId}";
		$miNumNodes = count($this->caNodes);
		$mbOpen = false;
		$msStyleWithBackground = "style='vertical-align:top;background:url({$msGFX}line.gif);background-repeat:repeat-y;background-position:top;'";
		$msStyleWithoutBackground = "style='vertical-align:top;'";
		if($miNumNodes>0){
			$moEvent = new FWDAjaxEvent('',$this->getAttrName().'_expand');
		}else{
			$moEvent = new FWDAjaxEvent('',$this->getAttrFunction());
		}
		$msEvent = str_replace("\"","'",$moEvent->render());
		$msOut = '';
		$msTreeClass = $this->getAttrClass();

		$maCollection = $this->coViewGroup->collect();
		if(count($maCollection)>0){
			foreach($maCollection as $key => $moView) $moView->setAttrPosition('relative');
		}
		$this->coViewGroup->setAttrName($this->getAttrName().'_vg');

		$msOut.= "<tr class='{$msTreeClass}_tr'>\n";
		$msOut.= "<td class='{$msTreeClass}_td' ".($pbLastSibling?$msStyleWithoutBackground:$msStyleWithBackground).">\n";
		if($miNumNodes>0){
			$msOut.= "<a id='{$msId}_a' href=\"javascript:toggle('$msId');$msEvent\"><img id='{$msId}_img' src='{$msGFX}".($mbOpen?'minus':'plus').".gif'></a>\n";
		}else{
			$msOut.= "<img id='{$msId}_img' src='{$msGFX}join.gif'>\n";
		}
		$msOut.= "</td>\n";
		$msOut.= "<td class='{$msTreeClass}_td'>\n";
		$msOut.= $this->coViewGroup->drawHeader();

		$maContent = $this->coViewGroup->getContent();
		$mbNeedshift = false;
		foreach($maContent as $moContent){
			if($moContent instanceof FWDStatic)
			$moContent->setAttrClass('StaticTree');
			$msOut .= $moContent->draw();
		}
		$msOut .= $this->coViewGroup->drawFooter();

		$msOut.= "</td>\n";
		$msOut.= "</tr>\n";
		if($miNumNodes>0){
			$msOut.= "<tr id='{$msId}_tr' class='{$msTreeClass}_tr'".($mbOpen?'':' style="display:none"').">\n";
			$msOut.= "<td class='{$msTreeClass}_td' ".($pbLastSibling?$msStyleWithoutBackground:$msStyleWithBackground)."></td>\n";
			$msOut.= "<td id='{$msId}_td' class='{$msTreeClass}_td'>\n";
			$msOut.= '<span class="FWDLoading" style="position:relative;">'.FWDLanguage::getPHPStringValue('FWDLoading','Carregando')."<img style='margin-top:9px' src='{$msGFX}loading.gif'/></span>";
			$msOut.= "</td>\n";
			$msOut.= "</tr>\n";
		}
		return $msOut;
	}

	/**
	 * Seta a fun��o do evento Ajax.
	 *
	 * <p>Seta a fun��o do evento Ajax.</p>
	 * @access public
	 * @param string $psFunction Fun��o do evento Ajax
	 */
	public function setAttrFunction($psFunction){
		$this->csFunction = $psFunction;
	}

	/**
	 * Retorna a fun��o do evento Ajax.
	 *
	 * <p>Retorna a fun��o do evento Ajax.</p>
	 * @access public
	 * @return string Fun��o do evento Ajax
	 */
	public function getAttrFunction(){
		return $this->csFunction;
	}

	/**
	 * Seta os par�metros do evento Ajax.
	 *
	 * <p>Seta os par�metros do evento Ajax.</p>
	 * @access public
	 * @param string $psParameter Par�metros do evento Ajax
	 */
	public function setAttrParameter($psParameter){
		$this->csParameter = $psParameter;
	}

	/**
	 * Retorna os par�metros do evento Ajax.
	 *
	 * <p>Retorna os par�metros do evento Ajax.</p>
	 * @access public
	 * @return string Par�metros do evento Ajax
	 */
	public function getAttrParameter(){
		return $this->csParameter;
	}

	/**
	 * M�todo executado quando o XML do elemento termina de carregar.
	 *
	 * <p>M�todo executado quando o XML do elemento termina de carregar. Se a
	 * �rvore tem filhos, independentemente do atributo function, o evento �
	 * FWDTreeAjaxExpand.</p>
	 *
	 * @access public
	 */
	public function execute(){
		if(count($this->caNodes)>0){
			$msEventName = $this->getAttrName().'_expand';
			$StartEvent_instance = FWDStartEvent::getInstance();
			$StartEvent_instance->addAjaxEvent(new FWDTreeAjaxExpand($msEventName,$this));
		}
	}

	/**
	 * Expande a �rvore, carregando seus filhos do XML
	 *
	 * <p>Retorna o c�digo em javascript que expande a �rvore, adicionando seus
	 * filhos carregados a partir do XML.</p>
	 *
	 * @access public
	 * @return string C�digo javascript
	 */
	public function expand(){
		$msId = "{$this->getTreeBaseName()}_{$this->csId}";
		$msContent = '';
		$miNumNodes = count($this->caNodes);
		for($miI=0;$miI<$miNumNodes-1;$miI++){
			$msContent.= $this->caNodes[$miI]->draw(false);
		}
		$msContent.= $this->caNodes[$miI]->draw(true);
		$msContent = str_replace(array("\n","\r","'"),array(" "," ","\'"),$msContent);
		$msOut = "gebi('{$msId}_td').innerHTML='{$msContent}';gebi('{$msId}_a').href=\"javascript:toggle('{$msId}');\";tt_Init(false,'');";
		return $msOut;
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDMenu. Implementa Menus de Contexto.
 *
 * <p>Classe que implementa menus de contexto na framework FWD.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDMenu extends FWDView {

	/**
	 * Menu de contexto (array de itens (objetos): FWDMenuItem, FWDMenuSeparator)
	 * @var array
	 * @access protected
	 */
	protected $caItems = array();

	/**
	 * Elementos sobre os quais o menu de contexto atuar�
	 * @var string
	 * @access protected
	 */
	protected $csElements = "";

	/**
	 * SubTarget do Menu de contexto (lines / headers)
	 * @var string
	 * @access protected
	 */
	protected $csSubElements = "";

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDMenu.</p>
	 * @access public
	 */
	public function __construct(){
	}

	/**
	 * Desenha o corpo menu de contexto.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML/javascript,
	 * todas as informa��es necess�rias para implementar o menu de contexto.</p>
	 * @access public
	 * @return string C�digo HTML/javascript que ser� inserido na p�gina
	 */
	public function draw() {
		$msCodeMenu="";
		if ($this->cbShouldDraw) {
			$msCodeMenu .= $this->drawHeader();
			$msCodeMenu .=  "\n<script language=\"JavaScript\">\n".$this->drawBody()." </script>\n\n";
			$msCodeMenu .= $this->drawFooter();
		}
		return $msCodeMenu;
	}

	/**
	 * Desenha o corpo menu de contexto.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML/javascript,
	 * todas as informa��es necess�rias para implementar o menu de contexto.</p>
	 * @access public
	 * @return string C�digo HTML/javascript que ser� inserido na p�gina
	 */
	public function drawHeader() {
		$msOut = "";
		$moBox = $this->getObjFWDBox();

		$miHeight = 0;
		$miWidth = 0;

		$maDrawItems = array();

		$mbHasBeforeItem = false;
		$miContItems = count($this->caItems);
		for($miI=0;$miI<$miContItems;$miI++){
			if ($this->caItems[$miI]->getShouldDraw()) {
				if($this->caItems[$miI] instanceof FWDMenuSeparator){
					if($mbHasBeforeItem==true){
						for($miJ=$miI+1;$miJ<$miContItems;$miJ++)
						if(($this->caItems[$miJ] instanceof FWDMenuItem)){
							if($this->caItems[$miJ]->getShouldDraw()){
								$maDrawItems[] = $this->caItems[$miI];
								$miHeight += $this->caItems[$miI]->getAttrHeight();
								$mbHasBeforeItem = false;
								break;
							}
						}else{
							break;
						}
					}else{}//falta um item antes do menu separator, assim ele n�o vai ser renderizado
				}else{
					$mbHasBeforeItem=true;
					$maDrawItems[] = $this->caItems[$miI];
					$miHeight += $this->caItems[$miI]->getAttrHeight();
				}
			}
		}

		$this->caItems = $maDrawItems;

		if ($moBox!=null)
		{
			$miWidth = $moBox->getAttrWidth();
				
			//caso nenhum dos itens de menu tenham largura , � utilizada a largura default de (116)
			if($miWidth==0)
			{
				$miWidth=198;
			}
			else
			{
				//pelo menos um item de menu tem largura definida
			}
		}
		else
		{
			$miWidth=198;
		}
		if (FWDWebLib::browserIsIE())
		$moMenuPanel = new FWDPanel(new FWDBox(0,0,$miHeight+2,$miWidth));
		else
		$moMenuPanel = new FWDPanel(new FWDBox(0,0,$miHeight,$miWidth));
		$moMenuPanel->setAttrClass("FWDMenu");
		$moMenuPanel->setAttrIndex(2);
		$moMenuPanel->setAttrName($this->getAttrName());

		$miNumElem = 0;
		$miSumHeight = 0;

		$moBox = $moMenuPanel->getObjFWDBox();

		foreach ($this->getAttrItems() as $moItem) {
			if (FWDWebLib::browserIsIE())
			$moBox = new FWDBox(0,$miSumHeight,$moItem->getAttrHeight(),$miWidth-2);
			else
			$moBox = new FWDBox(0,$miSumHeight,$moItem->getAttrHeight(),$miWidth);
			$miSumHeight += $moItem->getAttrHeight();
			$miNumElem++;
			$msName = $this->getAttrName()."_MenuElement".$miNumElem;
				
			$moItem->createMenuElement($moBox,$msName,array($miNumElem,count($this->caItems)),$this->getAttrName());
				
			$moMenuPanel->addObjFWDView($moItem);
		}

		$moMenuPanel->setAttrDisplay("false");

		$moEvent = new FWDClientEvent('OnClick','hidemenu',$this->getAttrName());
		$moMenuPanel->addEvent($moEvent);

		//input hidden utilizado para armazenar o valor da coluna da grid q sofreu a a��o do contextmenu
		$msInputs = " \n\n\n\n<input type=hidden id='{$this->getAttrName()}_action_target' value=''> \n";

		$msCodeMenu = $moMenuPanel->draw();

		$msOut .= $msInputs . $msCodeMenu;
		$msOut .= "<script language='javascript'>  gebi('dialog').onclick = function(){soMenuManager.hideMenus()}  </script>";

		return $msOut;
	}

	/**
	 * Desenha o corpo menu de contexto.
	 *
	 * <p>Re�ne em uma vari�vel string, o c�gido da fun��o javascript respons�vel por acionar
	 * o menu de contexto
	 * @return string C�digo javascript da fun��o do menu de contexto
	 */
	public function drawBody() {
		$msReturn = "";
		$msReturn .= "      soMenuManager.addMenu('{$this->getAttrName()}',window);\n";
		$msReturn .= "function {$this->getAttrName()}(event){\n";
		$msReturn .= "      var id='{$this->getAttrName()}';\n";
		$msReturn .= "      soMenuManager.hideMenus();\n";
		$msReturn .= "      js_show_menu(id,event);\n";
		$msReturn .= " }\n ";

		return $msReturn;
	}

	/**
	 * Obtem o codigo javascript para o menu de contexto.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo javascript,
	 * todas as informa��es necess�rias para o menu de context ser acionado
	 * nos elementos em que deve aparecer.</p>
	 * @access public
	 * @return string C�digo HTML/javascript que ser� inserido na p�gina
	 */
	public function drawFooter() {
		$msAllElements="";
		$maElements="";
		$msDiv="";

		$msAllElements = $this->getAttrElements();
		$maElements = explode(":",$msAllElements);

		foreach($maElements as $msElement)
		{
			if($msElement)
			{
				$msAllSubElements = $this->getAttrSubElements();

				if(!$msAllSubElements)
				{
					$msDiv .= "gebi('{$msElement}').oncontextmenu = {$this->getAttrName()};\n";
				}
				else
				{
					$maSubElement=explode(":",$msAllSubElements);
					foreach($maSubElement as $msSubElement)
					{
						if($msSubElement)
						{
							$msSubElementComplete = $msElement . "_" . $msSubElement;
							$msDiv .= "gebi('{$msSubElementComplete}').oncontextmenu = {$this->getAttrName()};\n";
						}
						else{ }
						//subtarget vazio, provavelmente gerado no explode se existia algum ":" a mais no $this->getAttrSubElement()
					}
				}//end else

			}//end if
			else { }//target vazio, provavelmente gerado no explode se existia algum ":" a mais no $this->getAttrElement()
		}//end foreach
		$msDiv = "\n<script language=\"JavaScript\">\n".$msDiv."</script>\n\n";
		return $msDiv;
	}

	/**
	 * Adiciona um item de menu.
	 *
	 * <p>Adiciona um objeto item de menu da classe FWDMenuItem
	 * na classe FWDMenu.</p>
	 * @access public
	 * @param FWDMenuItem $poItem Item de Menu
	 */
	public function setObjFWDMenuItem(FWDMenuItem $poItem) {
		$this->caItems[] = $poItem;
	}

	/**
	 * Adiciona um separador de itens.
	 *
	 * <p>Adiciona um objeto separador de itens da classe FWDMenuSeparator
	 * na classe FWDMenu.</p>
	 * @access public
	 * @param FWDMenuSeparator $poItem Separador de itens
	 */
	public function setObjFWDMenuSeparator(FWDMenuSeparator $poItem) {
		$this->caItems[] = $poItem;
	}

	/**
	 * Seta o valor do atributo SubElements.
	 *
	 * <p>Seta o valor do atributo SubTarget.</p>
	 * @access public
	 * @param string $psSubElements Sub-Elementos do Element do Menu de contexto
	 */
	public function setAttrSubTarget($psSubElements) {
		$this->csSubElements = $psSubElements;
	}

	/**
	 * Seta o valor do atributo Elements.
	 *
	 * <p>Seta o valor do atributo Elements.</p>
	 * @access public
	 * @param string $psElements Elementos sobre os quais o Menu de contexto atuar�
	 */
	public function setAttrElements($psElements) {
		$this->csElements = $psElements;
	}

	/**
	 * Adiciona valores (separados por ':') ao atributo Elements.
	 *
	 * <p>Adiciona valores (separados por ':') ao atributo Elements.</p>
	 * @access public
	 * @param string $psElements Elementos a serem adicionados
	 */
	public function addElements($psElements) {
		$this->csElements .= $psElements;
	}

	/**
	 * Retorna os �tens internos ao menu de contexto.
	 *
	 * <p>Retorna um array com os �tens internos ao menu de contexto.</p>
	 * @access public
	 * @return array Itens do Menu de contexto
	 */
	public function getAttrItems() {
		return $this->caItems;
	}

	/**
	 * Retorna o SubElement do Menu de contexto.
	 *
	 * <p>Retorna o SubElements do Menu de contexto. Ou seja sub-elementos do elemento definido em
	 * csElement. Utilizado principalmente na grid.</p>
	 * @access public
	 * @return string SubElements do Menu de contexto
	 */
	public function getAttrSubElements() {
		return $this->csSubElements;
	}

	/**
	 * Retorna o Elements do Menu de contexto.
	 *
	 * <p>Retorna o Elements do Menu de contexto.</p>
	 * @access public
	 * @return string Elements sobre os quais o Menu de contexto atuar�
	 */
	public function getAttrElements() {
		return $this->csElements;
	}

	/**
	 * M�todo usado para sobrescrever o setValue do pai (FWDView).
	 *
	 * <p>A Classe menu n�o tem valor para ser setado.</p>
	 * @access public
	 */
	public function setValue() {
	}

	/**
	 * Retorna o array de menuitens, e seus sub-elementos, e o pr�prio menu
	 *
	 * <p>Retorna o array de menuitens, e seus sub-elementos, e o pr�prio menu.</p>
	 * @access public
	 * @return array Array de menuitens, e seus sub-elementos, e o pr�prio menu
	 */
	public function collect() {
		$maOut = array();
		$maOut = array_merge($maOut,parent::collect());
		foreach ($this->caItems as $moView) {
			if ($moView instanceof FWDCollectable) {
				$maItems = $moView->collect();
				if (count($maItems)>0)
				$maOut = array_merge($maOut,$maItems);
			}
			else {
				$maOut[] = $moView;
			}
		}
		return $maOut;
	}

	/**
	 * Clona os itens do Menu de contexto.
	 *
	 * <p>Clona os itens do Menu de contexto..</p>
	 * @access public
	 */
	public function __clone() {
		$maNewItems = array();
		foreach ($this->caItems as $moItem)
		$maNewItems[] = clone $moItem;
		$this->caItems = $maNewItems;
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDMenuItem. Implementa itens para o menu de contexto.
 *
 * <p>Classe que implementa itens para o menu de contexto.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDMenuItem extends FWDDrawing implements FWDCollectable {

	/**
	 * Nome do MenuItem
	 * @var string
	 * @access protected
	 */
	protected $csName = "";

	/**
	 * classe do item
	 * @var string
	 * @access protected
	 */
	protected $csClass = "FWDMenuItem";

	/**
	 * classe do item (onMouseOver)
	 * @var string
	 * @access protected
	 */
	protected $csClassOver = "FWDMenuItemOver";

	/**
	 * Valor do MenuItem
	 * @var FWDString
	 * @access protected
	 */
	protected $coString = null;

	/**
	 * Box do MenuItem
	 * @var FWDBox
	 * @access protected
	 */
	protected $coBox = null;

	/**
	 * Altura do MenuItem
	 * @var integer
	 * @access protected
	 */
	protected $ciHeight = 25;

	/**
	 * Margem do �cone em rela��o ao texto do MenuItem
	 * @var integer
	 * @access protected
	 */
	protected $ciIconMargin = 30;

	/**
	 * Margem da sombra do menu (canto superior direito e inferior esquerdo)
	 * @var integer
	 * @access protected
	 */
	protected $ciShadowMargin = 10;

	/**
	 * Largura da sombra do menu (em pixels)
	 * @var integer
	 * @access protected
	 */
	protected $ciShadowWidth = 7;

	/**
	 * Evento onClick suportado pelo MenuItem
	 * @var FWDEvent
	 * @access protected
	 */
	protected $caEvent = array();

	/**
	 * �cone (imagem clic�vel) do MenuItem
	 * @var FWDIcon
	 * @access protected
	 */
	protected $coIcon = null;

	/**
	 * Tag do MenuItem (permiss�es: define se o elemento deve ser desenhado ou n�o)
	 * @var string
	 * @access protected
	 */
	protected $csTag = "";

	/**
	 * �ndice do MenuItem (array(X,Y): X=>�ndice deste menuitem, Y=> total de menuitems)
	 * @var array
	 * @access protected
	 */
	protected $caIndex = array(1,1);

	/**
	 * Nome do Menu
	 * @var string
	 * @access protected
	 */
	protected $csMenuName = "";

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDMenuItem.</p>
	 * @access public
	 */
	public function __construct() {
		$this->coString = new FWDString();
		$this->csClass = get_class($this);
	}

	/**
	 * Atribui altura ao MenuItem.
	 *
	 * <p>Atribui valor a v�riavel height, valor esse considerado como a altura do intem de menu.</p>
	 * @access public
	 * @param string $psValue Valor a ser atribu�do
	 */
	public function setAttrHeight($psHeight) {
		$this->ciHeight = $psHeight;
	}

	/**
	 * Seta a tag do elemento.
	 *
	 * <p>M�todo para setar a tag do elemento.</p>
	 * @access public
	 * @param string $psTag Tag do elemento
	 */
	public function setAttrTag($psTag) {
		$this->csTag = $psTag;
	}

	/**
	 * Adiciona uma string.
	 *
	 * <p>Adiciona um objeto string da classe FWDString na classe FWDMenuItem.</p>
	 * @access public
	 * @param FWDString $poString �cone
	 */
	public function addObjFWDString(FWDString $poString) {
		$this->coString = $poString;
	}

	/**
	 * Adiciona um �cone.
	 *
	 * <p>Adiciona um objeto �cone da classe FWDIcon na classe FWDMenuItem.</p>
	 * @access public
	 * @param FWDIcon $poIcon �cone
	 */
	public function addObjFWDIcon(FWDIcon $poIcon) {
		$this->coIcon = $poIcon;
	}

	/**
	 * Seta eventos do MenuItem.
	 *
	 * <p>Seta os eventos suportados pelo MenuItem.</p>
	 * @access public
	 * @param FWDEvent $poEvent Evento
	 */
	public function addEvent($poEvent) {
		$this->caEvent[] = $poEvent;
	}

	/**
	 * Adiciona um objeto de evento.
	 *
	 * <p>Adiciona um objeto de evento da classe FWDEvent
	 * na classe FWDMenuItem.</p>
	 * @access public
	 * @param FWDEvent $poEvent Evento
	 */
	public function addObjFWDEvent(FWDEvent $poEvent) {
		$this->addEvent($poEvent);
	}

	/**
	 * Retorna a altura do MenuItem.
	 *
	 * <p>Retorna a altura do MenuItem.</p>
	 * @access public
	 * @return integer Altura do intem
	 */
	public function getAttrHeight()	{
		return $this->ciHeight;
	}

	/**
	 * Retorna o evento onClick suportado pelo MenuItem.
	 *
	 * <p>Retorna o evento onClick suportado pelo MenuItem.</p>
	 * @access public
	 * @return FWDEvent Evento onClick suportado pelo MenuItem
	 */
	public function getAttrEvent() {
		return $this->caEvent;
	}

	/**
	 * Retorna o �cone do MenuItem.
	 *
	 * <p>Retorna o �cone do MenuItem.</p>
	 * @access public
	 * @return FWDIcon �cone do MenuItem
	 */
	public function getAttrIcon() {
		return $this->coIcon;
	}

	/**
	 * Retorna a tag do elemento.
	 *
	 * <p>M�todo para retornar a tag do elemento.</p>
	 * @access public
	 * @return string Tag do elemento
	 */
	public function getAttrTag() {
		return $this->csTag;
	}

	/**
	 * Retorna o valor do MenuItem.
	 *
	 * <p>Retorna o valor do MenuItem.</p>
	 * @access public
	 * @return string Valor do MenuItem
	 */
	public function getValue() {
		return $this->coString->getAttrString();
	}

	/**
	 * Seta a classe do MenuItem.
	 *
	 * <p>M�todo para setar a classe do MenuItem.</p>
	 * @access public
	 * @param string $psClass Classe do MenuItem
	 */
	public function setAttrClass($psClass) {
		$this->csClass = $psClass;
	}

	/**
	 * Retorna a classe do MenuItem.
	 *
	 * <p>M�todo para retornar a classe do MenuItem.</p>
	 * @access public
	 * @return string Classe do MenuItem
	 */
	public function getAttrClass() {
		return $this->csClass;
	}

	/**
	 * Seta a classe do MenuItem (onMouseOver).
	 *
	 * <p>M�todo para setar a classe do MenuItem (onMouseOver).</p>
	 * @access public
	 * @param string $psClassOver Classe do MenuItem (onMouseOver)
	 */
	public function setAttrClassOver($psClassOver) {
		$this->csClassOver = $psClassOver;
	}

	/**
	 * Retorna a classe do MenuItem (onMouseOver).
	 *
	 * <p>M�todo para retornar a classe do MenuItem (onMouseOver).</p>
	 * @access public
	 * @return string Classe do MenuItem (onMouseOver)
	 */
	public function getAttrClassOver() {
		return $this->csClassOver;
	}

	/**
	 * Atribui valor, condicionalmente.
	 *
	 * <p>Atribui valor ao item, condicionando atrav�s do segundo par�metro.
	 * Se force for FALSE, concatena o valor se a vari�vel tiver conte�do;
	 * se TRUE atribui mesmo que tenha conte�do (sobrescreve).</p>
	 * @access public
	 * @param string $psValue Valor a ser atribu�do
	 * @param boolean $pbForce For�ar o valor mesmo que j� tenha conte�do
	 */
	public function setValue($psValue, $pbForce = true) {
		$this->coString->setValue($psValue, $pbForce);
	}

	/**
	 * Cria o MenuItem.
	 *
	 * <p>Cria o MenuItem do menu de contexto. Seta o nome e a box do
	 * elemento.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do MenuItem
	 * @param string $psName Nome do MenuItem
	 * @param array $paIndex array com o �ndice deste menuitem e o total de menuitems
	 * @param string $psMenuName Nome do Menu
	 */
	public function createMenuElement($poBox,$psName,$paIndex,$psMenuName) {
		$this->csName = $psName;
		$this->coBox = $poBox;
		$this->caIndex = $paIndex;
		$this->csMenuName = $psMenuName;
	}

	/**
	 * Desenha o cabe�alho do MenuItem.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o cabe�alho do MenuItem.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do MenuItem
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawHeader($poBox = null) {
		$msGlobalProperty = "name='{$this->csName}' id='{$this->csName}' class='{$this->csClass}'";
		$msStyle  = "left:{$this->coBox->getAttrLeft()};";
		$msStyle .= "top:{$this->coBox->getAttrTop()};";
		$msStyle .= "height:{$this->coBox->getAttrHeight()};";
		$msStyle .= "width:{$this->coBox->getAttrWidth()};";
		$msStyle .= "position:absolute;overflow:hidden;";

		$msEvents  = "onmouseover='this.className=\"{$this->csClassOver}\"; this.parentNode.parentNode.oncontextmenu=\"return false;\"; ' ";
		$msEvents .= "onmouseout='this.className=\"{$this->csClass}\";      this.parentNode.parentNode.oncontextmenu={$this->csMenuName}; '";

		if ($this->caEvent) {
			$msEvents .= "onclick=' ";
			foreach ($this->caEvent as $moEvent) {
				$msEvents .= $moEvent->render();
			}
			$msEvents .= "'";
		}

		return "\n<div {$msGlobalProperty} style='{$msStyle}' {$msEvents} >\n";
	}

	/**
	 * Desenha o corpo do MenuItem.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o corpo do MenuItem.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do MenuItem
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawBody($poBox = null) {
		$msOut = "";

		if ($this->coIcon) {
			$this->coIcon->getImageBox();
				
			$miIconTop = ceil($this->coBox->getAttrHeight() / 2);
			$miIconTop -= ceil($this->coIcon->getObjFWDBox()->getAttrHeight() / 2);
			$this->coIcon->getObjFWDBox()->setAttrTop($miIconTop);
				
			$miIconLeft = ceil($this->ciIconMargin / 2);
			$miIconLeft -= ceil($this->coIcon->getObjFWDBox()->getAttrWidth() / 2);
			$this->coIcon->getObjFWDBox()->setAttrLeft($miIconLeft);
			$this->coIcon->setAttrName($this->csName."_icon");
			$msOut .= $this->coIcon->draw();
		}

		$moStaticBox = new FWDBox($this->ciIconMargin, 0, $this->coBox->getAttrHeight(), $this->coBox->getAttrWidth()-$this->ciIconMargin);
		$moStatic = new FWDStatic($moStaticBox);
		$moStatic->setAttrName($this->csName."_static");
		$moStatic->setObjFWDString($this->coString);
		$moStatic->setAttrHorizontal("left");
		$moStatic->setAttrVertical("middle");
		$msOut .= $moStatic->draw();

		return $msOut;
	}

	/**
	 * Desenha o rodap� do MenuItem.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o rodap� do MenuItem.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do MenuItem
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawFooter($poBox = null) {
		$msOut = "</div>\n";

		if ($this->caIndex[0]==1) {
			if ($this->caIndex[0]==$this->caIndex[1]) {
				$msName = "menu_shadow_".$this->caIndex[0]."_".$this->caIndex[1];
				$msClass = "FWDMenuShadowRight";
				$msStyle  = "left:".($this->coBox->getAttrWidth()+1).";";
				$msStyle .= "top:{$this->ciShadowMargin};";
				$msStyle .= "height:".($this->coBox->getAttrHeight()-$this->ciShadowMargin+1).";";
				$msStyle .= "width:{$this->ciShadowWidth};";
				$msStyle .= "position:absolute;overflow:hidden;";
				if (FWDWebLib::browserIsIE())
				$msStyle .= "Filter: Alpha(Opacity=40, FinishOpacity=10, Style=1, StartX=0, StartY=0, FinishX=800, FinishY=0)";
				$msOut .= "<div name='{$msName}' id='{$msName}' class='{$msClass}' style='{$msStyle}'></div>\n\n";
			}
			else {
				$msName = "menu_shadow_".$this->caIndex[0]."_".$this->caIndex[1];
				$msClass = "FWDMenuShadowRight";
				$msStyle  = "left:".($this->coBox->getAttrWidth()+1).";";
				$msStyle .= "top:{$this->ciShadowMargin};";
				$msStyle .= "height:".($this->coBox->getAttrHeight()-$this->ciShadowMargin).";";
				$msStyle .= "width:{$this->ciShadowWidth};";
				$msStyle .= "position:absolute;overflow:hidden;";
				if (FWDWebLib::browserIsIE())
				$msStyle .= "Filter: Alpha(Opacity=40, FinishOpacity=10, Style=1, StartX=0, StartY=0, FinishX=800, FinishY=0)";
				$msOut .= "<div name='{$msName}' id='{$msName}' class='{$msClass}' style='{$msStyle}'></div>\n\n";
			}
		}

		else {
			if ($this->caIndex[0]==$this->caIndex[1]) {
				$msName = "menu_shadow_".$this->caIndex[0]."_".$this->caIndex[1];
				$msClass = "FWDMenuShadowRight";
				$msStyle  = "left:".($this->coBox->getAttrWidth()+1).";";
				$msStyle .= "top:{$this->coBox->getAttrTop()};";
				$msStyle .= "height:".($this->coBox->getAttrHeight()+1).";";
				$msStyle .= "width:{$this->ciShadowWidth};";
				$msStyle .= "position:absolute;overflow:hidden;";
				if (FWDWebLib::browserIsIE())
				$msStyle .= "Filter: Alpha(Opacity=40, FinishOpacity=10, Style=1, StartX=0, StartY=0, FinishX=800, FinishY=0)";
				$msOut .= "<div name='{$msName}' id='{$msName}' class='{$msClass}' style='{$msStyle}'></div>\n\n";
			}
			else {
				$msName = "menu_shadow_".$this->caIndex[0]."_".$this->caIndex[1];
				$msClass = "FWDMenuShadowRight";
				$msStyle  = "left:".($this->coBox->getAttrWidth()+1).";";
				$msStyle .= "top:{$this->coBox->getAttrTop()};";
				$msStyle .= "height:{$this->coBox->getAttrHeight()};";
				$msStyle .= "width:{$this->ciShadowWidth};";
				$msStyle .= "position:absolute;overflow:hidden;";
				if (FWDWebLib::browserIsIE())
				$msStyle .= "Filter: Alpha(Opacity=40, FinishOpacity=10, Style=1, StartX=0, StartY=0, FinishX=800, FinishY=0)";
				$msOut .= "<div name='{$msName}' id='{$msName}' class='{$msClass}' style='{$msStyle}'></div>\n\n";
			}
		}

		if ($this->caIndex[0]==$this->caIndex[1]) {
			$msName = "menu_shadow_bottom";
			$msClass = "FWDMenuShadowBottom";
			$msStyle  = "left:{$this->ciShadowMargin};";
			$msStyle .= "top:".($this->coBox->getAttrTop()+$this->coBox->getAttrHeight()+1).";";
			$msStyle .= "height:{$this->ciShadowWidth};";
			$msStyle .= "width:".($this->coBox->getAttrWidth()-$this->ciShadowMargin+1).";";
			$msStyle .= "position:absolute;overflow:hidden;";
			if (FWDWebLib::browserIsIE())
			$msStyle .= "Filter: Alpha(Opacity=40, FinishOpacity=10, Style=1, StartX=0, StartY=0, FinishX=0, FinishY=800)";
			$msOut .= "<div name='{$msName}' id='{$msName}' class='{$msClass}' style='{$msStyle}'></div>\n";
				
			$msName = "menu_shadow_corner";
			$msClass = "FWDMenuShadowCorner";
			$msStyle  = "left:".($this->coBox->getAttrWidth()+1).";";
			$msStyle .= "top:".($this->coBox->getAttrTop()+$this->coBox->getAttrHeight()+1).";";
			$msStyle .= "height:{$this->ciShadowWidth};";
			$msStyle .= "width:{$this->ciShadowWidth};";
			$msStyle .= "position:absolute;overflow:hidden;";
			if (FWDWebLib::browserIsIE())
			$msStyle .= "Filter: Alpha(Opacity=40, FinishOpacity=10, Style=1, StartX=0, StartY=0, FinishX=60, FinishY=60)";
			$msOut .= "<div name='{$msName}' id='{$msName}' class='{$msClass}' style='{$msStyle}'></div>\n\n";
		}
		return $msOut;
	}

	/**
	 * Retorna o array de eventos do elemento e o pr�prio elemento.
	 *
	 * <p>M�todo para retornar o array de eventos do elemento e o pr�prio elemento.</p>
	 * @access public
	 * @return array Eventos do elemento e elemento
	 */
	public function collect() {
		$maOut = array($this);
		$maOut = array_merge($maOut,$this->caEvent);
		return $maOut;
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDMenuSeparator. Implementa um separador de itens para o menu de contexto.
 *
 * <p>Classe que implementa um separador de itens (div com suporte a eventos)
 * para o menu de contexto.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDMenuSeparator extends FWDMenuItem {

	/**
	 * Margem do separador
	 * @var integer
	 * @access protected
	 */
	protected $ciMargin = 5;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDMenuSeparator.</p>
	 * @access public
	 */
	public function __construct() {
		$this->csClass = get_class($this);
	}

	/**
	 * Cria o separador.
	 *
	 * <p>Cria o separador de itens do menu de contexto. Seta o nome,
	 * a box e o evento onClick.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do separador
	 * @param string $psName Elemento sobre o qual o evento onMouseover atuar�
	 * @param array $paIndex array com o �ndice deste menuitem e o total de menuitems
	 * @param string $psParent Elemento sobre o qual o evento onClick atuar�
	 */
	public function createMenuElement($poBox,$psName,$paIndex,$psParent) {
		$this->csName = $psName;
		$this->coBox = $poBox;
		$this->caIndex = $paIndex;
		$this->coEvent = new FWDClientEvent('onClick','hide',$psParent);
	}

	/**
	 * Desenha o cabe�alho do MenuSeparator.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o cabe�alho do MenuSeparator.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do MenuSeparator
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawHeader($poBox = null) {
		$msGlobalProperty = "name='{$this->csName}' id='{$this->csName}' ";
		$msStyle  = "left:{$this->coBox->getAttrLeft()};";
		$msStyle .= "top:{$this->coBox->getAttrTop()};";
		$msStyle .= "height:{$this->coBox->getAttrHeight()};";
		$msStyle .= "width:{$this->coBox->getAttrWidth()};";
		$msStyle .= "position:absolute;overflow:hidden;";

		$moEventOver = new FWDClientEvent('onMouseover','changecursor',$this->csName,'default');
		$msEvents  = "onmouseover='{$moEventOver->render()}' ";
		$msEvents .= "onclick='{$this->coEvent->render()}' ";

		return "\n<div {$msGlobalProperty} style='{$msStyle}' {$msEvents} >";
	}

	/**
	 * Desenha o corpo do MenuSeparator.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o corpo do MenuSeparator.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do MenuSeparator
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawBody($poBox = null) {
		$msGlobalProperty = "name='{$this->csName}_inner' id='{$this->csName}_inner' class='{$this->csClass}'";
		$msStyle  = "left:{$this->ciMargin};";
		$msStyle .= "top:".floor($this->coBox->getAttrHeight()/2).";";
		if (FWDWebLib::browserIsIE())
		$msStyle .= "height:2;";
		else
		$msStyle .= "height:0;";
		$msStyle .= "width:".($this->coBox->getAttrWidth()-(2*$this->ciMargin)).";";
		$msStyle .= "position:absolute;overflow:hidden;";

		return "\n<div {$msGlobalProperty} style='{$msStyle}' ></div>";
	}

	/**
	 * Desenha o rodap� do MenuSeparator.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o rodap� do MenuSeparator.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do MenuSeparator
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawFooter($poBox = null) {
		return parent::drawFooter();
	}

	/**
	 * Retorna o valor do MenuItem.
	 *
	 * <p>Retorna o valor do MenuItem.</p>
	 * @access public
	 * @return string Valor do MenuItem
	 */
	public function getValue(){
		return '';
	}

}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDSelect. Implementa menus drop down.
 *
 * <p>Classe que implementa menus drop down (html tag 'select').</p>
 * @package FWD5
 * @subpackage view
 */
class FWDSelect extends FWDView implements FWDPostValue, FWDCollectable {

	/**
	 * Define o n�mero de linhas do select
	 * @var integer
	 * @access protected
	 */
	protected $ciSize = 0;

	/**
	 * Define se o select suporta m�ltiplas sele��es
	 * @var boolean
	 * @access protected
	 */
	protected $cbMultiple = false;

	/**
	 * Array de objetos (FWDItem) do select
	 * @var array
	 * @access protected
	 */
	protected $caItem = array();

	/**
	 * Array de objetos (FWDOptgroup) do select
	 * @var array
	 * @access protected
	 */
	protected $caOptgroup = array();

	/**
	 * Nome do label associado ao objeto
	 * @var string
	 * @access protected
	 */
	protected $csLabel = "";

	/**
	 * Array com os valores do select
	 * @var array
	 * @access protected
	 */
	protected $caValues = array();

	/**
	 * Define se o select est� desabilitado ou n�o
	 * @var boolean
	 * @access protected
	 */
	protected $cbDisabled = false;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDSelect.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do select
	 */
	public function __construct($poBox = null) {
		parent::__construct($poBox);
	}

	/**
	 * Retorna as propriedades globais do elemento.
	 *
	 * <p>M�todo para retornar as propriedades globais do elemento.</p>
	 * @access public
	 * @return string Propriedades globais do elemento
	 */
	public function getGlobalProperty() {
		$msTabIndex = (($this->ciTabIndex)?"tabindex='{$this->ciTabIndex}'":"");
		$msAccessKey = (($this->csAccessKey)?"accesskey='{$this->csAccessKey}'":"");
		$msResize = (($this->cbResize)?"resize='true'":"");
		$msClass = (($this->csClass)?"class='{$this->csClass}'":"");

		$msName = $this->csName;
		if ($this->cbMultiple)
		$msName .= "[]";

		$msValue = "name='{$msName}' id='{$this->csName}' {$msTabIndex} {$msAccessKey} {$msResize} {$msClass}";
		return $msValue;
	}

	/**
	 * Desenha o cabe�alho do select.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o cabe�alho do select.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do select
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawHeader($poBox = null) {
		$msStyle = "style='{$this->getStyle()}'";

		$this->setAttrResize("false");

		$msMultiple = (($this->cbMultiple)?"multiple":"");

		$msSize = (($this->ciSize)?"size='{$this->ciSize}'":"");

		$msDisabled = $this->cbDisabled ? "disabled" : "";

		$msLabel="";
		if($this->csLabel)
		$msLabel = "label='{$this->csLabel}'";

		return "<select {$this->getGlobalProperty()} {$msSize} {$msDisabled} {$msMultiple} {$msStyle} {$msLabel} {$this->getEvents()}>\n";
	}

	/**
	 * Desenha o corpo do select.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o corpo do select.</p>
	 * @access public
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawBody() {
		$msOut = "";
		if (count($this->caOptgroup)) foreach ($this->caOptgroup as $moOptgroup) $msOut .= $moOptgroup->draw();
		else foreach ($this->caItem as $moItem) $msOut .= $moItem->draw();
		return $msOut;
	}

	/**
	 * Desenha o rodap� do select.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o rodap� do select.</p>
	 * @access public
	 *
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawFooter() {
		return "</select><script language='javascript'>new FWDSelect('{$this->csName}')</script>";
	}

	/**
	 * Adiciona um objeto de item.
	 *
	 * <p>Adiciona um objeto de item da classe FWDItem na classe FWDSelect.</p>
	 * @access public
	 * @param FWDItem $poItem Item
	 */
	public function addObjFWDItem(FWDItem $poItem) {
		$this->caValues[] = $poItem->getAttrKey();
		$this->caItem[$poItem->getAttrKey()] = $poItem;
	}

	/**
	 * Adiciona um objeto de optgroup.
	 *
	 * <p>Adiciona um objeto de optgroup da classe FWDOptgroup na classe FWDSelect.</p>
	 * @access public
	 * @param FWDOptgroup $poOptgroup Item
	 */
	public function addObjFWDOptgroup(FWDOptgroup $poOptgroup) {
		$poOptgroup->setSelect($this);
		$maItems = $poOptgroup->getItems();
		foreach ($maItems as $moItem)	$this->addObjFWDItem($moItem);
		$this->caOptgroup[] = $poOptgroup;
	}

	/**
	 * Seta um item do select como selecionado.
	 *
	 * <p>Seta um item (objeto) do select como selecionado.</p>
	 * @access public
	 * @param integer $piId Valor chave do item
	 */
	public function checkItem($piId) {
		if($this->cbMultiple) {
			if (isset($this->caItem[$piId]))
			$this->caItem[$piId]->setAttrCheck(true);
			else
			trigger_error("Invalid item key: '{$piId}'",E_USER_WARNING);
		}
		else {
			foreach($this->caItem as $moItem) {
				if($moItem->getAttrKey() == $piId)
				$moItem->setAttrCheck(true);
				else
				$moItem->setAttrCheck(false);
			}
		}
	}

	/**
	 * Seta o select como desabilitado.
	 *
	 * <p>M�todo para setar o select como desabilitado.</p>
	 * @access public
	 * @param boolean $pbDisabled Define se o select est� desabilitado ou n�o
	 */
	public function setAttrDisabled($pbDisabled) {
		$this->cbDisabled = FWDWebLib::attributeParser($pbDisabled,array("true"=>true, "false"=>false),"pbDisabled");
	}

	/**
	 * Retorna o valor booleano do atributo Disabled.
	 *
	 * <p>Retorna o valor booleano TRUE se o select est� desabilitado.
	 * Retorna FALSE caso contr�rio.</p>
	 * @access public
	 * @return boolean Indica se o select est� desabilitado ou n�o
	 */
	public function getAttrDisabled() {
		return $this->cbDisabled;
	}

	/**
	 * Seta o valor booleano do atributo de suporte a m�ltiplas sele��es.
	 *
	 * <p>Seta o valor booleano do atributo que define se m�ltiplas sele��es
	 * devem ser suportadas. Para o select suportar m�ltiplas sele��es,
	 * o atributo deve conter o valor booleano TRUE. Para n�o suportar,
	 * o atributo deve conter o valor booleano FALSE.</p>
	 * @access public
	 * @param string $psMultiple Define se m�ltiplas sele��es devem ser suportadas
	 */
	public function setAttrMultiple($psMultiple) {
		$this->cbMultiple = FWDWebLib::attributeParser($psMultiple,array("true"=>true, "false"=>false),"psMultiple");
	}

	/**
	 * Retorna o valor booleano do atributo de suporte a m�ltiplas sele��es.
	 *
	 * <p>Retorna o valor booleano TRUE se m�ltiplas sele��es devem ser
	 * suportadas. Retorna FALSE caso contr�rio.</p>
	 * @access public
	 * @return boolean Indica se m�ltiplas sele��es devem ser suportadas
	 */
	public function getAttrMultiple() {
		return $this->cbMultiple;
	}

	/**
	 * Seta o n�mero de linhas do select.
	 *
	 * <p>M�todo para setar o n�mero de linhas do select.</p>
	 * @access public
	 * @param integer $piSize Define o n�mero de linhas do select
	 */
	public function setAttrSize($piSize) {
		if ($piSize > 0)
		$this->ciSize = $piSize;
		else
		trigger_error("Invalid select size: '{$piSize}'",E_USER_WARNING);
	}

	/**
	 * Seta o nome do Label do objeto.
	 *
	 * <p>M�todo para setar o nome do label do objeto, utilizado para a troca de cor, no caso do objeto ter
	 * preenchimento obrigat�rio e n�o estar preenchido.</p>
	 * @access public
	 * @param string $psLabel nome do label associado ao objeto
	 */
	public function setAttrLabel($psLabel) {
		$this->csLabel = $psLabel;
	}

	/**
	 * Retorna o n�mero de linhas do select.
	 *
	 * <p>M�todo para retornar o n�mero de linhas do select.</p>
	 * @access public
	 * @return integer N�mero de linhas do select
	 */
	public function getAttrSize() {
		return $this->ciSize;
	}

	/**
	 * Retorna array de objetos do select.
	 *
	 * <p>Retorna array de objetos (itens) do select.</p>
	 * @access public
	 * @return array Array de objetos (FWDItem) do select
	 */
	public function getItems() {
		return $this->caItem;
	}

	/**
	 * Seta o valor de um item do select.
	 *
	 * <p>M�todo para setar o valor de um item do select. Caso o item n�o
	 * exista, ele � criado.</p>
	 * @access public
	 * @param integer $piId Id do Valor a ser atribu�do
	 * @param string $psValue Valor a ser atribu�do
	 */
	public function setItemValue($piId, $psValue = "") {
		if (!isset($this->caItem[$piId])) {
			$moItem = new FWDItem();
			$moItem->setAttrKey($piId);
			$moItem->setValue($psValue);
			$this->addObjFWDItem($moItem);
		}
		else {
			$this->caItem[$piId]->setValue($psValue);
		}
	}

	/**
	 * Atribui valor � vari�vel value.
	 *
	 * <p>M�todo para atribuir valor � vari�vel value.</p>
	 * @access public
	 * @param mixed $pmValue Valor a ser atribu�do
	 */
	public function setAttrValue($pmValue) {
		if(is_array($pmValue)){
			if($this->cbMultiple){
				foreach($pmValue as $msValue){
					$this->setItemValue($msValue);
					$this->checkItem($msValue);
				}
			}else{
				trigger_error('Trying to set multiple values in a FWDDBSelect not multiple.',E_USER_WARNING);
			}
		}else{
			$this->caValues = array($pmValue);
			$this->checkItem($pmValue);
		}
	}

	/**
	 * Atribui valor � vari�vel value.
	 *
	 * <p>M�todo para atribuir valor � vari�vel value.</p>
	 * @access public
	 * @param array $paValue Valor a ser atribu�do
	 */
	public function setValue($paValue, $pbForce = true) {
		$this->setAttrValue($paValue);
	}

	/**
	 * Retorna um array com as chaves de todos os items do select.
	 *
	 * <p>M�todo para retornar um array com as chaves de todos os items do select.</p>
	 * @access public
	 * @return array Chaves dos items
	 */
	public function getItemsKeys() {
		return array_keys($this->caItem);
	}

	/**
	 * Retorna um array com as chaves dos items selecionados.
	 *
	 * <p>M�todo para retornar um array com as chaves dos items selecionados.
	 * Caso seja sele��o simples, retorna um inteiro.</p>
	 * @access public
	 * @return array/integer Chaves dos items selecionados
	 */
	public function getValue() {
		if($this->cbMultiple){
			return $this->caValues;
		}else{
			if (count($this->caValues)) return $this->caValues[0];
			else return 0;
		}
	}

	/**
	 * Retorna um array com todos itens do select e o pr�prio select
	 *
	 * <p>Retorna um array contendo todos itens do select e o pr�prio select.</p>
	 * @access public
	 * @return array Array de itens do select e o pr�prio select
	 */
	public function collect() {
		$maOut = array($this);
		if ($this->caItem) $maOut = array_merge($maOut,$this->caItem);
		if ($this->caOptgroup) $maOut = array_merge($maOut,$this->caOptgroup);
		return $maOut;
	}

	/**
	 * Renderiza o c�digo javascript necess�rio para popular o select por Ajax.
	 *
	 * <p>Renderiza o c�digo javascript necess�rio para popular o select por Ajax.</p>
	 * @access public
	 */
	public function execEventPopulate(){
		echo "var soSel=gobi('{$this->csName}');";
		echo "soSel.clear();";
		foreach($this->caItem as $moItem){
			echo "soSel.addItem('{$moItem->getAttrKey()}','{$moItem->getValue()}','{$moItem->getAttrClass()}');";
		}
		echo "soSel=null;";
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDSelector.
 *
 * <p>Select no qual cada item corresponde a um elemento da tela, e s� o selecionado fica vis�vel.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDSelector extends FWDSelect {

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDSelector.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do select
	 */
	public function __construct($poBox = null){
		parent::__construct($poBox);

		$moEvent = new FWDClientEvent();
		$moEvent->setAttrEvent('onChange');
		$moEvent->setAttrValue("this.object.select(this.value);");
		$this->addObjFWDEvent($moEvent);
	}

	/**
	 * Desenha o rodap� do select.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o rodap� do select.</p>
	 * @access public
	 *
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawFooter(){
		$msSelectedElement = $this->getValue();
		$msJs = "new FWDSelector('{$this->csName}','{$msSelectedElement}');try{";
		foreach($this->caItem as $moItem){
			$msElement = $moItem->getAttrKey();
			$moElement = FWDWebLib::getObject($msElement);
			if($moElement && $msElement!=$msSelectedElement){
				$msJs.= "js_hide('{$msElement}');";
				$moElement->setAttrDisplay('false');
			}
		}
		if($msSelectedElement){
			$msJs.= "js_show('{$msSelectedElement}');";
			if(FWDWebLib::getObject($msSelectedElement)){
				FWDWebLib::getObject($msSelectedElement)->setAttrDisplay('true');
			}else{
				trigger_error("Object '$msSelectedElement' not found.",E_USER_ERROR);
			}
		}
		$msJs.= "}catch(e){}";
		return "</select><script language='javascript'>{$msJs}</script>";
	}

}

?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2007, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDOptgroup. Implementa um agrupador de itens do select.
 *
 * <p>Classe que implementa um agrupador de itens do select.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDOptgroup extends FWDDrawing {

	/**
	 * Valor do elemento
	 * @var FWDString
	 * @access protected
	 */
	protected $coString = null;

	/**
	 * Array com os item do optgroup
	 * @var array
	 * @access protected
	 */
	protected $caItemKey = array();

	/**
	 * Objeto FWDSelect ao qual o optgroup pertence
	 * @var array
	 * @access protected
	 */
	protected $coSelect = null;

	/**
	 * Indica se o optgroup est� desabilitado
	 * @var array
	 * @access protected
	 */
	protected $cbDisabled = false;

	/**
	 * Tag do elemento (permiss�es: define se o elemento deve ser desenhado ou n�o)
	 * @var string
	 * @access protected
	 */
	protected $csTag = "";

	/**
	 * Nome do elemento
	 *
	 * @var string
	 * @access protected
	 */
	protected $csName = "";

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDItem.</p>
	 * @access public
	 */
	public function __construct() {
		$this->coString = new FWDString();
	}

	/**
	 * Desenha o cabe�alho do optgroup.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o cabe�alho do optgroup.</p>
	 * @access public
	 *
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawHeader() {
		$msDisabled = '';
		if ($this->cbDisabled) $msDisabled = 'disabled';
		return "<optgroup $msDisabled label='{$this->getValue()}' name='{$this->getAttrName()}' id='{$this->getAttrName()}'>\n";
	}

	/**
	 * Desenha o corpo do optgroup.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o corpo do optgroup.</p>
	 * @access public
	 *
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawBody() {
		$msOut = '';
		foreach ($this->caItemKey as $msItemKey => $moItem) {
			$msOut .= $moItem->draw();
		}
		return $msOut;
	}

	/**
	 * Desenha o rodap� do optgroup.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o rodap� do optgroup.</p>
	 * @access public
	 *
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawFooter() {
		return "</optgroup>\n";
	}

	/**
	 * Seta o valor do optgroup.
	 *
	 * <p>M�todo para setar o valor do optgroup.</p>
	 * @access public
	 * @param FWDString $poString Seta o valor do optgroup
	 */
	public function setObjFWDString(FWDString $poString) {
		$this->coString = $poString;
	}

	/**
	 * Seta o select ao qual o optgroup pertence.
	 *
	 * <p>M�todo para setar o select ao qual o optgroup pertence.</p>
	 * @access public
	 * @param FWDSelect $poSelect Obejto do select
	 */
	public function setSelect(FWDSelect $poSelect) {
		$this->coSelect = $poSelect;
	}

	/**
	 * Adiciona um objeto de item.
	 *
	 * <p>Adiciona um objeto de item da classe FWDItem.</p>
	 * @access public
	 * @param FWDItem $poItem Item
	 */
	public function addObjFWDItem(FWDItem $poItem) {
		$this->caItemKey[$poItem->getAttrKey()] = $poItem;
	}

	/**
	 * Habilita/Desabilita o optgroup.
	 *
	 * <p>M�todo para habilitar/desabilitar o optgroup.</p>
	 * @access public
	 * @param boolean $pbValue Define se o optgroup est� habilitado ou n�o
	 */
	public function setAttrDisabled($pbValue) {
		$this->cbDisabled = $pbValue;
	}

	/**
	 * Verifica se o optgroup est� habilitado ou n�o.
	 *
	 * <p>M�todo para verificar se o optgroup est� habilitado ou n�o.</p>
	 * @access public
	 * @return boolean Define se o optgroup est� habilitado ou n�o
	 */
	public function getAttrDisabled() {
		return $this->cbDisabled;
	}

	/**
	 * Atribui valor, condicionalmente.
	 *
	 * <p>Atribui valor ao item, condicionando atrav�s do segundo par�metro.
	 * Se force for FALSE, concatena o valor se a vari�vel tiver conte�do;
	 * se TRUE atribui mesmo que tenha conte�do (sobrescreve).</p>
	 * @access public
	 * @param string $psValue Valor a ser atribu�do
	 * @param boolean $pbForce For�ar o valor mesmo que j� tenha conte�do
	 */
	public function setValue($psValue, $pbForce = true) {
		$this->coString->setValue($psValue, $pbForce);
	}

	/**
	 * Retorna o valor do item.
	 *
	 * <p>M�todo para retornar o valor do item.</p>
	 * @access public
	 * @return string Valor do item
	 */
	public function getValue() {
		return $this->coString->getAttrString();
	}

	/**
	 * Retorna o objeto de string do elemento.
	 *
	 * <p>M�todo para retornar o objeto de string do elemento.</p>
	 * @access public
	 * @return FWDString String do elemento
	 */
	public function getObjFWDString() {
		return $this->coString;
	}

	/**
	 * Retorna array de objetos do optgroup.
	 *
	 * <p>Retorna array de objetos (itens) do optgroup.</p>
	 * @access public
	 * @return array Array de objetos (FWDItem) do optgroup
	 */
	public function getItems() {
		return $this->caItemKey;
	}

	/**
	 * Seta a tag do elemento.
	 *
	 * <p>M�todo para setar a tag do elemento.</p>
	 * @access public
	 * @param string $psTag Tag do elemento
	 */
	public function setAttrTag($psTag) {
		$this->csTag = $psTag;
	}

	/**
	 * Retorna a tag do elemento.
	 *
	 * <p>M�todo para retornar a tag do elemento.</p>
	 * @access public
	 * @return string Tag do elemento
	 */
	public function getAttrTag() {
		return $this->csTag;
	}

	/**
	 * Seta o atributo name.
	 *
	 * <p>M�todo para setar o atributo name.</p>
	 * @access public
	 * @param string $psName Nome do elemento
	 */
	public function setAttrName($psName) {
		$this->csName = $psName;
	}

	/**
	 * Retorna o atributo name.
	 *
	 * <p>M�todo para retornar o atributo name.</p>
	 * @access public
	 * @return string Atributo name do elemento
	 */
	public function getAttrName() {
		return $this->csName;
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDController. Implementa um Controlador.
 *
 * <p>Classe que implementa um Controlador na Framework FWD5.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDController extends FWDView implements FWDPostValue {

	/**
	 * N�mero de itens do Controlador
	 * @var integer
	 * @access protected
	 */
	protected $ciItems = 0;

	/**
	 * Array de FWDItemControllers
	 * @var array
	 * @access protected
	 */
	protected $caContent = array();

	/**
	 * Nome do label associado ao objeto
	 * @var string
	 * @access protected
	 */
	protected $csLabel = "";

	/**
	 * Classe dos itens controlados pelo controller
	 * @var string
	 * @access protected
	 */
	protected $csItensClass = '';

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDController.</p>
	 * @access public
	 */
	public function __construct(){
		parent::__construct();
	}

	/**
	 * Desenha o cabe�alho do controller.
	 * @todo Otimizar baseado no indice do array
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o cabe�alho do controller.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do checkbox
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawHeader($poBox = null){
		$msExtraAttrs = '';
		$msValue = '';
		if($this->csItensClass=='FWDRadioBox'){
			foreach($this->getContent() as $msElemKey => $moElem){
				if($moElem->getAttrCheck()){
					$msValue = $msElemKey;
					break;
				}
			}
		}else{
			$maValue = array();
			$maCheckAll = array();
			$maItemKeys = array();
			foreach($this->getContent() as $msElemKey=>$moElem){
				if($moElem->getAttrCheck())
				$maValue[] = $msElemKey;
				$maItemKeys[] = $msElemKey;
				if($moElem->getAttrSelectAll()){
					$maCheckAll[] = $msElemKey;
				}
			}
			$msExtraAttrs.= " checkAll='".implode(':',$maCheckAll)."'";
			$msExtraAttrs.= " itemKeys='".implode(':',$maItemKeys)."'";
			$msValue = implode(':',$maValue);
		}
		$msExtraAttrs.= ($this->csLabel?" label='{$this->csLabel}'":'');

		$msOut = "<input type='hidden' id='{$this->csName}' name='{$this->csName}' value='$msValue'{$msExtraAttrs}/>\n"
		."<script type='text/javascript'>"
		."var soElem=new ".get_class($this)."('{$this->csName}');"
		."gebi('{$this->csName}').object=soElem;";
		foreach($this->getContent() as $msKey=>$_){
			$msOut.= "soElem.addElement('{$this->csName}_{$msKey}');";
		}
		$msOut.= "soElem=null;"
		."</script>";
		return $msOut;
	}

	/**
	 * Adiciona um Controlador de itens {checkbox ou radiobox}.
	 *
	 * <p>Adiciona um Controlodor de itens da classe FWDItemController
	 * na classe FWDController.</p>
	 * @access public
	 * @param FWDItemController $poItemController Controlador de itens
	 */
	public function addObjFWDItemController(FWDItemController $poItemController){
		$msItemClass = get_class($poItemController);

		if($this->csItensClass=='') $this->csItensClass = $msItemClass;
		if($this->csItensClass==$msItemClass){
			$this->setAttrItems($this->getAttrItems() + 1);
			$this->caContent[$poItemController->getAttrKey()] = $poItemController;
		}else{
			trigger_error('A Controller cannot control itens of different classes.',E_USER_ERROR);
		}

		/*
		 * � necess�rio chamar essa fun��o para funcionar com a utiliza��o do XML_STATIC.
		 */
		$this->rebuildValue();
	}

	/**
	 * Seta o n�mero de itens do Controlador.
	 *
	 * <p>Seta o n�mero de itens do Controlador.</p>
	 * @access public
	 * @param integer $piItems N�mero de itens do Controlador
	 */
	public function setAttrItems($piItems){
		$this->ciItems = $piItems;
	}

	/**
	 * Seta o nome do Label do objeto.
	 *
	 * <p>M�todo para setar o nome do label do objeto, utilizado para a troca de cor, no caso do objeto ter
	 * preenchimento obrigat�rio e n�o estar preenchido.</p>
	 * @access public
	 * @param string $psLabel nome do label associado ao objeto
	 */
	public function setAttrLabel($psLabel){
		$this->csLabel = $psLabel;
	}

	/**
	 * Seta a classe do tipo de itens do controlador.
	 *
	 * <p>Seta a classe do tipo de itens do controlador.</p>
	 * @access public
	 * @param integer $piItems N�mero de itens do Controlador
	 */
	public function setAttrItensClass($psItensClass){
		$this->csItensClass = FWDWebLib::attributeParser($psItensClass,array("checkbox"=>"FWDCheckBox", "radiobox"=>"FWDRadioBox"),"psItensClass");
	}

	/**
	 * Retorna um item, fornecida sua chave.
	 *
	 * <p>Retorna um item, fornecida sua chave.</p>
	 * @access public
	 * @param string $psKey Chave do Item
	 * @return object Item
	 */
	public function getItemByKey($psKey){
		if (isset($this->caContent[$psKey])) {
			return $this->caContent[$psKey];
		} else {
			return null;
		}
	}

	/**
	 * Retorna o n�mero de itens do Controlador.
	 *
	 * <p>Retorna o n�mero de itens do Controlador.</p>
	 * @access public
	 * @return integer N�mero de itens do Controlador
	 */
	public function getAttrItems(){
		return $this->ciItems;
	}

	/**
	 * Retorna um array com os itens selecionados.
	 *
	 * <p>Retorna um array com os itens selecionados.</p>
	 * @access public
	 * @return array Array de itens selecionados (Checked)
	 */
	public function getAllItemsCheck(){
		$maOut = array();
		$msElemCheck = explode(':',$this->getValue());
		foreach($msElemCheck as $msElem){
			if($msElem!='') $maOut[] = $this->getItemByKey($msElem);
		}
		return $maOut;
	}

	/**
	 * Seta o atributo Check de um item.
	 *
	 * <p>Seta o atributo Check (true, false) de um item do controlador.
	 * por este controle.</p>
	 * @access public
	 * @param string $psKey Chave do item que ser� marcado
	 */
	public function checkItem($psKey){
		$moItemController = $this->getItemByKey($psKey);
		if($moItemController instanceof FWDCheckBox){
			$moItemController->setAttrCheck("true");
			parent::setValue(":".$psKey);
			return;
		} elseif($moItemController instanceof FWDRadioBox) {
			$moItemController->setAttrCheck("true");
			parent::setValue(":".$moItemController->getAttrKey());
		} elseif(!($moItemController instanceof FWDCheckBox)){
			$moItemController->setAttrCheck("false");
		}
	}

	/**
	 * Atribui valor � vari�vel value.
	 *
	 * <p>M�todo para atribuir valor � vari�vel value.</p>
	 * @access public
	 * @param mixed $pmValue Valor a ser atribu�do
	 */
	public function setAttrValue($pmValue){
		if(!is_array($pmValue) && !empty($pmValue)){
			$paValue = explode(':',$pmValue);
		}
		elseif (!is_array($pmValue)) { // n�o h� valores a serem adicionados no controller
			return;
		}
		else {
			$paValue = $pmValue;
		}
		foreach($this->caContent as $moItem){
			$moItem->setAttrCheck('false');
		}

		foreach($paValue as $msValue){
			if(isset($this->caContent[$msValue])) {
				$this->checkItem($msValue);
			}
			elseif($this->csItensClass){
				$moItemController = new $this->csItensClass;
				$moItemController->setAttrName($this->getAttrName()."_".$msValue);
				$moItemController->setAttrKey($msValue);
				$moItemController->setAttrCheck(true);
				$moItemController->setAttrController($this->getAttrName());
				$moItemController->execute();
			}
		}
	}
	/**
	 * Atribui valor � vari�vel value.
	 *
	 * <p>M�todo para atribuir valor � vari�vel value.</p>
	 * @access public
	 * @param array $paValue Valor a ser atribu�do
	 */
	public function setValue($paValue, $pbForce = true){
		$this->setAttrValue($paValue);
	}

	/**
	 * Retorna o valor do controlador
	 *
	 * <p>M�todo para retornar o valor do controlador.</p>
	 * @access public
	 * @return string Valor do controlador
	 */
	public function getValue(){
		$msOut = "";
		foreach($this->getContent() as $msItemControllerKey => $moItemController){
			if ($moItemController instanceof FWDCheckBox && $moItemController->getAttrSelectAll())
			continue;
			if($moItemController->getAttrCheck()) $msOut.= ':'.$msItemControllerKey;
		}
		return $msOut;
	}

	/**
	 * Atualiza o valor do controlador
	 *
	 * <p>M�todo para atualizar o valor do controlador.</p>
	 * @access public
	 */
	public function rebuildValue(){
		$this->setValue(FWDWebLib::getPOST($this->getAttrName()));
	}

	/**
	 * Retorna todas as views do container.
	 *
	 * <p>M�todo para retornar todas as views do container.</p>
	 * @access public
	 * @return array Views
	 */
	public function getContent(){
		return $this->caContent;
	}

	/**
	 * Retorna o array de controladores e o pr�prio controlador
	 *
	 * <p>Retorna o array de controladores e o pr�prio controlador.</p>
	 * @access public
	 * @return array Array de controladores e o pr�prio controlador
	 */
	public function collect(){
		$maOut = array($this);
		$maOut = array_merge($maOut,$this->caContent);
		return $maOut;
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDRadioBox. Implementa Radio Boxes.
 *
 * <p>Classe que implementa Radio Boxes (imagens clic�veis com atributos
 * especiais e suporte a eventos). HTML tag 'input type="radio"'.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDRadioBox extends FWDItemController {

	private $cbDisabled = false;
	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDRadioBox.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites da RadioBox
	 */
	public function __construct($poBox = null){
		parent::__construct($poBox);
	}

	/**
	 * Desenha o cabe�alho do radiobox.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o cabe�alho do radiobox.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do radiobox
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawHeader($poBox = null){
		$moWebLib = FWDWebLib::getInstance();
		$msGFX = $moWebLib->getGfxRef();
		$msController = $this->getAttrController();
		$moController = FWDWebLib::getObject($msController);
		$msName = "{$msController}_{$this->getAttrKey()}";
		$msSrc = ($this->getAttrCheck()?$msGFX."radio.gif":$msGFX."uncheck.gif");
		$msJs = "gfx_radio(\"$msController\",\"{$this->getAttrKey()}\");";
		$msEventCode = '';
		$msCursor = '';
		if($this->cbDisabled==false){
			$moEvent = new FWDClientEvent();
			$moEvent->setAttrEvent("OnClick");
			$moEvent->setAttrValue($msJs);
			$this->addObjFWDEvent($moEvent);
			$msEventCode = $this->getEvents();
			$msCursor.= 'cursor:pointer;';
		}
		$msStyle = "{$this->getStyle()};{$msCursor}";
		return  "\n<img name='{$msName}' id='$msName' src='$msSrc' style='$msStyle' {$msEventCode}/>\n";
	}

	/**
	 * Desenha o rodap� do radiobox.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o rodap� do radiobox.</p>
	 * @access public
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawFooter(){
		$msName = "{$this->csController}_{$this->csKey}";
		$msDisabled = '';
		if($this->cbDisabled){
			$msDisabled.="gobi('{$msName}').disable();";
		}
		if($this->csName){
			return "<script language='javascript'>"
			."gebi('{$msName}').object = new FWDRadioBox('{$msName}','{$this->csController}','{$this->csKey}');"
			.$msDisabled
			."</script>";
		}else{
			return '';
		}
	}


	/**
	 * Retorna se o radiobox � readOnly.
	 *
	 * <p>Retorna se o radiobox � readOnly.</p>
	 * @access public
	 * @return boolean Retorna se o radiobox � readOnly.
	 */
	public function getAttrDisabled(){
		return $this->cbDisabled;
	}

	/**
	 * Define se o radio � readOnly.
	 *
	 * <p>Metodo que define se um radio � readOnly.</p>
	 * @access public
	 * @param boolean $pbReadOnly "true" ou "false"
	 */
	public function setAttrDisabled($pbDisable){
		$this->cbDisabled = FWDWebLib::attributeParser($pbDisable,array("true"=>true, "false"=>false),"pbDisable");
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDCheckBox. Implementa Check Boxes.
 *
 * <p>Classe que implementa Check Boxes (imagens clic�veis com atributos
 * especiais e suporte a eventos). HTML tag 'input type="checkbox"'.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDCheckBox extends FWDItemController {

	/**
	 * Define se todas as CheckBoxes devem ser selecionadas ou n�o
	 * @var boolean
	 * @access protected
	 */
	protected $cbSelectAll = false;

	/**
	 * Define se o checkbox est� desabilitado
	 * @var boolean
	 * @access protected
	 */
	protected $cbDisabled = false;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDCheckBox.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites da CheckBox
	 */
	public function __construct($poBox = null){
		parent::__construct($poBox);
	}

	/**
	 * Desenha o cabe�alho do checkbox.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o cabe�alho do checkbox.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do checkbox
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawHeader($poBox = null){
		$moWebLib = FWDWebLib::getInstance();
		$msGFX = $moWebLib->getGfxRef();
		$msController = $this->getAttrController();
		$msKey = $this->csKey;
		$msName = $msController."_".$msKey;
		$msSrc = ($this->getAttrCheck()?$msGFX."check.gif":$msGFX."uncheck.gif");
		$msJs = "gfx_check(\"$msController\",\"$msKey\");";
		$msClass = $this->getAttrClass();
		$moEvent = new FWDClientEvent();
		$moEvent->setAttrEvent("OnClick");
		$moEvent->setAttrValue($msJs);
		$this->addObjFWDEvent($moEvent);
		$msStyle = "{$this->getStyle()};cursor:pointer";
		return  "\n<img name='{$msName}' id='$msName' src='$msSrc' style='$msStyle' class='$msClass' {$this->getEvents()}/>\n";
	}

	/**
	 * Desenha o rodap� do checkbox.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o rodap� do checkbox.</p>
	 * @access public
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawFooter() {
		$msName = $this->csController."_".$this->csKey;
		$msDisabled = "";
		if($this->cbDisabled)
		$msDisabled .="gobi('{$msName}').disable();";
		if($this->csName)
		return "\n <script language='javascript'>gebi('{$msName}').object = new FWDCheckBox('{$msName}','{$this->csController}','{$this->csKey}');$msDisabled </script> ";
		else
		return "";
	}

	/**
	 * Seta o atributo de SelectAll.
	 *
	 * <p>Seta o valor booleano do atributo de SelectAll. Para que todas as
	 * Check Boxes sejam selecionadas, o atributo pbSelectAll deve conter o valor
	 * booleano TRUE. Caso contr�rio, deve conter o valor booleano FALSE.</p>
	 * @access public
	 * @param string $psSelectAll Define se todas as Check Boxes devem ser selecionadas
	 */
	public function setAttrSelectAll($psSelectAll){
		$this->cbSelectAll = FWDWebLib::attributeParser($psSelectAll,array("true"=>true, "false"=>false),"psSelectAll");
	}

	/**
	 * Retorna o valor booleano do atributo de SelectAll.
	 *
	 * <p>Retorna o valor booleano do atributo de SelectAll.</p>
	 * @access public
	 * @return boolean Indica se todas as Check Boxes devem ser selecionadas ou n�o
	 */
	public function getAttrSelectAll(){
		return $this->cbSelectAll;
	}

	/**
	 * Seta a memo como desabilitada.
	 *
	 * <p>M�todo para setar a memo como desabilitada.</p>
	 * @access public
	 * @param boolean $pbDisabled Define se a memo est� desabilitada ou n�o
	 */
	public function setAttrDisabled($pbDisabled) {
		$this->cbDisabled = FWDWebLib::attributeParser($pbDisabled,array("true"=>true, "false"=>false),"pbDisabled");
	}

	/**
	 * Retorna o valor booleano do atributo Disabled.
	 *
	 * <p>Retorna o valor booleano TRUE se a memo est� desabilitada.
	 * Retorna FALSE caso contr�rio.</p>
	 * @access public
	 * @return boolean Indica se a memo est� desabilitada ou n�o
	 */
	public function getAttrDisabled() {
		return $this->cbDisabled;
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDMessageBox. Implementa Message Boxes.
 *
 * <p>Classe que implementa Message Boxes.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDMessageBox extends FWDContainer {

	/**
	 * T�tulo da Message Box (texto est�tico)
	 * @var FWDStatic
	 * @access protected
	 */
	protected $coTitle = null;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDMessageBox.</p>
	 * @access public
	 * @param FWDPanel $pobox Box que define os limites da Message Box
	 */
	public function __construct($poBox = null) {
		parent::__construct($poBox);
	}

	/**
	 * Desenha a Message Box.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML,
	 * todas as informa��es necess�rias para implementar a Message Box.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites da messagebox
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawBody($poBox = null) {
		$msOut = "";

		if ($this->csDebug!="")
		$msOut .= $this->debugView();

		$moPanel = new FWDPanel($this->getObjFWDBox());
		$moPanel->setAttrDisplay("false");
		$moPanel->setAttrName($this->getAttrName());

		$moTitle = $this->getAttrTitle();
		$moPanel->addObjFWDView($moTitle,"title");

		$msMessage = $this->getMessage($this->getValue());
		$moPanel->addObjFWDView($msMessage);

		foreach ($this->caContent as $moButton) {
			$moPanel->addObjFWDView($moButton);
		}

		$msOut .= $moPanel->draw();

		return $msOut;
	}

	/**
	 * Adiciona um button.
	 *
	 * <p>Adiciona um button (imagem clic�vel) da classe FWDButton na
	 * classe FWDMessageBox.</p>
	 * @access public
	 * @param FWDButton $poButton Button
	 */
	public function addObjFWDButton($poButton) {
		$this->caContent[] = $poButton;
	}

	/**
	 * Cria e seta o T�tulo da Message Box.
	 *
	 * <p>Cria e seta o T�tulo da Message Box. Instancia um objeto da classe
	 * Static, seta o nome e o valor (string) do t�tulo.</p>
	 * @access public
	 * @param string $psValue Texto a ser inserido no T�tulo da MessageBox.
	 */
	public function setAttrTitle($psValue) {
		$moTitle = new FWDStatic(new FWDBox(50,100,100,100));
		$moTitle->setAttrName("title_".$this->getAttrName());
		$moTitle->setAttrValue($psValue);
		$this->coTitle = $moTitle;
	}

	/**
	 * Retorna o objeto de T�tulo.
	 *
	 * <p>Retorna o objeto de T�tulo (inst�ncia da classe Static) da
	 * Message Box.</p>
	 * @access public
	 * @return FWDStatic T�tulo da Message Box (texto est�tico)
	 */
	public function getAttrTitle() {
		return $this->coTitle;
	}

	/**
	 * Retorna o objeto de Mensagem.
	 *
	 * <p>Retorna o objeto de Mensagem (inst�ncia da classe Static) da
	 * Message Box.</p>
	 * @access public
	 * @param string $psValue Texto ser inserido na Message Box
	 * @return FWDStatic Mensagem da Message Box (texto est�tico)
	 */
	public function getMessage($psValue) {
		$moMessage = new FWDStatic(new FWDBox(50,50,100,100));
		$moMessage->setAttrName("message_".$this->getAttrName());
		$moMessage->setAttrValue($psValue);
		return $moMessage;
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDTabGroup. Implementa um grupo de Tabs.
 *
 * <p>Classe que implementa um grupo de Tabs.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDTabGroup extends FWDContainer {

	/**
	 * Altura do TabItem (em pixels)
	 * @var integer
	 * @access protected
	 */
	protected $ciTabItemHeight = 33;

	/**
	 * �ndice do TabItem que deve ser carregado por default
	 * @var integer
	 * @access protected
	 */
	protected $ciLoadTabItemIndex = 1;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDTabGroup.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do TabGroup
	 */
	public function __construct($poBox = null){
		parent::__construct($poBox);
	}

	/**
	 * Seta o valor do atributo LoadTabItemIndex.
	 *
	 * <p>Seta o valor do atributo LoadTabItemIndex.</p>
	 * @access public
	 * @param string $psLoadTabItemIndex Define qual TabItem deve ser carregado por default
	 */
	public function setAttrLoadTabItemIndex($psLoadTabItemIndex) {
		$this->ciLoadTabItemIndex = $psLoadTabItemIndex;
	}

	/**
	 * Retorna o valor do atributo LoadTabItemIndex.
	 *
	 * <p>Retorna o valor do atributo LoadTabItemIndex.</p>
	 * @access public
	 * @return integer Indica qual TabItem deve ser carregado por default
	 */
	public function getAttrLoadTabItemIndex() {
		$this->ciLoadTabItemIndex;
	}

	/**
	 * Desenha o cabe�alho da Tabgroup.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o cabe�alho da tabgroup.</p>
	 * @access public
	 */
	public function drawHeader(){
		if(($this->caContent[$this->ciLoadTabItemIndex-1]->getShouldDraw()==false)||($this->caContent[$this->ciLoadTabItemIndex-1]->getAttrDisplay()==false)){
			$miCont = 0;
			$this->ciLoadTabItemIndex = -1;
			foreach($this->getContent() as $moItem){
				$miCont++;
				if(($moItem->getShouldDraw()==true)&&($moItem->getAttrDisplay()==true)){
					$this->ciLoadTabItemIndex = $miCont;
					return "<script>soTabSubManager.initTabGroup('{$this->csName}',{$this->ciLoadTabItemIndex});</script>";
					break;
				}
			}
		}else{
			return "<script>soTabSubManager.initTabGroup('{$this->csName}',{$this->ciLoadTabItemIndex});</script>";
		}
	}

	/**
	 * Desenha o corpo da TabGroup.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para desenhar o corpo da TabGroup.</p>
	 * @access public
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawBody() {
		$moTabGroup = new FWDViewGroup($this->getObjFWDBox());
		$moTabGroup->setAttrDebug($this->getAttrDebug());
		$moTabGroup->setAttrName($this->getAttrName());
		$moTabGroup->setAttrClass($this->getAttrClass());
		$miActiveTabs = 0;
		foreach($this->getContent() as $moItem){
			if($moItem->getShouldDraw() && $moItem->getAttrDisplay()){
				$miActiveTabs++;
			}
		}
		$miLeft = 0;
		$moTabGroupBox = $this->getObjFWDBox();
		$miTabGroupAvaliableWidth = $moTabGroupBox->getAttrWidth();
		foreach($this->getContent() as $miNumElem=>$moItem){
			$miNumElem++;
			if ($moItem->getShouldDraw() && $moItem->getAttrDisplay()){
				$moTabItemBox = $moItem->getObjFWDBox();
				$msFrameName = $this->getAttrName()."_iframe_".$miNumElem;
				$msDivName = $this->getAttrName()."_content_".$miNumElem;
				$msElems = $msFrameName.' '.$msDivName;
				$moAuxBox = new FWDBox($miLeft, 0, $this->ciTabItemHeight, $moTabItemBox->getAttrWidth());
				if($moItem->getAttrClass()==''){
					$moItem->setAttrClass($this->getAttrClass()=="FWDTabGroup"?"FWDTabItem":"FWDSubTabItem");
				}
				$moTabitem = $moItem->createItem($moAuxBox, $this->getAttrName(), $miNumElem, $miActiveTabs, $msElems, $moItem->getAttrUrl(), $this->ciLoadTabItemIndex);
				$miLeft += $moTabItemBox->getAttrWidth();
				$moTabGroup->addObjFWDView($moTabitem);
				$miTabGroupAvaliableWidth -= $moTabitem->getObjFWDBox()->getAttrWidth();
				$miFrameHeight = $moTabGroupBox->getAttrHeight() - $this->ciTabItemHeight;
				$miFrameWidth = $moTabGroupBox->getAttrWidth();
				$moFrame = new FWDDiv(new FWDBox(0,$this->ciTabItemHeight-1,$miFrameHeight,$miFrameWidth));
				$moFrame->setName($msDivName);
				$moFrame->setClass('');
				$moFrame->setExtraStyle(($miNumElem==$this->ciLoadTabItemIndex) ? '':'display:none;');
				$msValue = "<iframe name='$msFrameName' id='$msFrameName' width='100%' height='100%' marginwidth=0 marginheight=0"
				." hspace=0 vspace=0 frameborder=0 style='z-index:-1;' src=''></iframe>\n";
				$moFrame->setValue($msValue);
				$moTabGroup->addObjFWDView($moFrame);
			}
		}
		$miFillDivWidth = $miTabGroupAvaliableWidth;
		if($this->getAttrClass()=="FWDTabGroup"){
			if(FWDWebLib::browserIsIE()){
				$miFillDivWidth-= 2;
			}else{
				$miFillDivWidth-= 4;
			}
		}
		$moFillDiv = new FWDDiv(new FWDBox($moTabGroupBox->getAttrWidth() - $miTabGroupAvaliableWidth,0,$this->ciTabItemHeight,$miFillDivWidth));
		$moFillDiv->setName($this->getAttrName()."_fill_div");
		$moFillDiv->setClass($this->getAttrClass()=="FWDTabGroup"?"FWDTabItem_notSelected":"FWDSubTabItem_notSelected");
		$moTabGroup->addObjFWDView($moFillDiv);
		return $moTabGroup->draw();
	}

	/**
	 * Desenha o rodap� da Tabgroup.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o rodap� da tabgroup.</p>
	 * @access public
	 */
	public function drawFooter(){
		if($this->ciLoadTabItemIndex>0){
			$miSelected = $this->ciLoadTabItemIndex;
			$msFrameName = "{$this->csName}_iframe_{$miSelected}";
			$msDivName = "{$this->csName}_content_{$miSelected}";
			$msUrl = $this->caContent[$miSelected-1]->getAttrUrl();
			return "<script language='javascript'>loadIframe(gebi('$msFrameName'),'$msUrl','$msDivName');</script>";
		}
		else {
			// N�o h� nenhuma aba a ser carregada
			// return "<script language='javascript'>alert('deu erro no tabgroup.php');</script>";
		}
		 
	}

	/**
	 * Adiciona um TabItem.
	 *
	 * <p>Adiciona um TabItem da classe FWDTabItem na classe FWDTabGroup.</p>
	 * @access public
	 *
	 * @param FWDTabItem $poTabItem Tabitem.
	 */
	public function addObjFWDTabItem(FWDTabItem $poTabItem) {
		$this->addObjFWDView($poTabItem);
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDTabItem. Implementa os itens da TabGroup.
 *
 * <p>Classe que implementa os itens da TabGroup.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDTabItem extends FWDView {

	/**
	 * Url para a qual a tab ir� redirecionar
	 * @var string
	 * @access protected
	 */
	protected $csUrl;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDTabItem. Overload do construtor da view,
	 * para n�o setar a classe css padr�o como sendo a mesma do objeto (para o
	 * tabitem poder 'herdar' a classe css de sua tabgroup, caso nenhuma classe
	 * seja especificada).</p>
	 * @access public
	 * @param string $pobox Box que define os limites do TabGroup
	 */
	public function __construct($poBox = null) {
		$this->coBox = new FWDBox();
		$this->coString = new FWDString();
		if ($poBox) {
			$this->coBox->setAttrLeft($poBox->getAttrLeft());
			$this->coBox->setAttrTop($poBox->getAttrTop());
			$this->coBox->setAttrHeight($poBox->getAttrHeight());
			$this->coBox->setAttrWidth($poBox->getAttrWidth());
		}
	}

	/**
	 * Cria o TabItem.
	 *
	 * <p>Cria o TabItem. Seta os eventos, o nome, as dimens�es.</p>
	 *
	 * <p>Cria o TabItem. Seta os eventos, o nome, as dimens�es e as cores.</p>
	 * @access public
	 * @param string $pobox Box que limita o tamanho do TabItem
	 * @param integer $piOffset Deslocamento da tab dentro do tabgroup
	 * @param integer $piActiveTabs N�mero de tabs ativas
	 * @param string $psElements Elementos que ser�o alvis dos eventos disparados pela tab
	 * @param string $psUrl Url
	 * @param integer $piLoadTabItemIndex �ndice do TabItem que deve ser carregado por default
	 * @return FWDTabItem TabItem
	 */
	public function createItem($poBox = null, $psName, $piOffset, $piActiveTabs, $psElements, $psUrl, $piLoadTabItemIndex) {

		$moViewButton = new FWDViewButton($poBox);

		if ($this->getObjFWDEvent()) {
			foreach ($this->getObjFWDEvent() as $moEvent)
			$moViewButton->setAttrEvent($moEvent);
		}

		if(!$this->getAttrDisplay())
		$moViewButton->setAttrDisplay("false");

		$moViewButton->setAttrDebug($this->getAttrDebug());
		$moViewButton->setAttrName($psName."_tab_".$piOffset);
		$moViewButton->setAttrClass("");

		$moStatic = new FWDStatic(new FWDBox(0, $poBox->getAttrTop(), $poBox->getAttrHeight(), $poBox->getAttrWidth()));
		$moStatic->setAttrName($psName."_tab_".$piOffset."_value");

		if ($piOffset == $piLoadTabItemIndex)
		$moStatic->setAttrClass($this->getAttrClass()."_selected");
		else
		$moStatic->setAttrClass($this->getAttrClass()."_notSelected");


		$moStatic->setValue($this->getValue());
		$moStatic->setAttrHorizontal("center");
		$moStatic->setAttrVertical("middle");

		$moViewButton->addObjFWDView($moStatic);

		$msParameters = $piActiveTabs.' '.$psUrl;
		$msTargetElements = $psElements.' '.$moViewButton->getAttrName();
		$moEventOnClick = new FWDClientEvent("onclick", "changetab", '', "$piOffset $psUrl");

		$moEventOnMouseOver = new FWDClientEvent("onmouseover", "inverttabitemclass", $moStatic->getAttrName());
		$moEventOnMouseOut = new FWDClientEvent("onmouseout", "inverttabitemclass", $moStatic->getAttrName());

		$moViewButton->addEvent($moEventOnClick);
		$moViewButton->addEvent($moEventOnMouseOver);
		$moViewButton->addEvent($moEventOnMouseOut);

		return $moViewButton;
	}

	/**
	 * Seta a Url de redirecionamento.
	 *
	 * <p>Seta a Url de redirecionamento.</p>
	 * @access public
	 * @param string $psUrl Url de redirecionamento
	 */
	public function setAttrUrl($psUrl) {
		$this->csUrl = $psUrl;
	}

	/**
	 * Retorna a Url de redirecionamento.
	 *
	 * <p>Retorna a Url de redirecionamento.</p>
	 * @access public
	 * @return string Url de redirecionamento
	 */
	public function getAttrUrl() {
		return $this->csUrl;
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDUserPreferences. Armazena as modifica��es feitas pelo usu�rio
 *
 * <p>Armazena as modifica��es feitas pelo usu�rio nos elementos mov�veis e/ou
 * redimension�veis.</p>
 *
 * @package FWD5
 * @subpackage view
 */
class FWDUserPreferences extends FWDButton implements FWDPostValue{

	/**
	 * String que cont�m os ids dos elementos edit�veis pelo usu�rio que devem ser armazenados separados por ':'
	 * @var string
	 * @access protected
	 */
	protected $csSource;

	/**
	 * Vari�vel que ir� conter o valor do elemento
	 * @var FWDVariable
	 * @access protected
	 */
	protected $coVariable;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDUserPreferences.</p>
	 *
	 * @access public
	 * @param FWDBox $poBox Box que define os limites da imagem clic�vel
	 */
	public function __construct($poBox = null){
		parent::__construct($poBox);
		$this->coVariable = new FWDVariable();
	}

	/**
	 * Desenha o elemento.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o elemento.</p>
	 *
	 * @access public
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawBody(){
		$msName = $this->getAttrName();
		$moEvent = new FWDClientEvent('onmousedown',JS_SAVE_USER_PREFS,$msName,$this->getAttrSource());
		$this->addEvent($moEvent);
		$this->setAttrName("button_$msName");
		$this->coVariable->setAttrName($msName);
		return parent::drawBody();
	}

	/**
	 * Desenha o rodap� do objeto.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o rodap� do objeto.</p>
	 *
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do objeto
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	protected function drawFooter($poBox = null){
		return $this->coVariable->draw();
	}

	/**
	 * Define os atributos das boxes dos elementos
	 *
	 * <p>Dada uma string contendo um array serializado contendo os atributos
	 * das boxes de elementos, redefine os atributos das boxes dos elementos
	 * contidos na string.</p>
	 *
	 * @access public
	 * @param string $psValue Array serializado
	 * @param boolean $pbForce Sem efeito
	 */
	public function setValue($psValue, $pbForce = true){
		if(strpos($psValue,":") === FALSE) {
			return false;
		}
		$maElemsAttrs = @FWDWebLib::unserializeString($psValue);
		if($maElemsAttrs!==false){
			$moWebLib = FWDWebLib::getInstance();
			for($i=0;$i<count($maElemsAttrs);$i++){
				$moElem = $moWebLib->getObject($maElemsAttrs[$i]['name']);
				if($moElem instanceof FWDView){
					$moBox = $moElem->getObjFWDBox();
					$moBox->setAttrTop($maElemsAttrs[$i]['top']);
					$moBox->setAttrLeft($maElemsAttrs[$i]['left']);
					$moBox->setAttrHeight($maElemsAttrs[$i]['height']);
					$moBox->setAttrWidth($maElemsAttrs[$i]['width']);
				}
			}
		}
	}

	/**
	 * Atribui valor � vari�vel value.
	 *
	 * <p>Atribui valor � vari�vel value, devendo retirar espa�os em branco
	 * no inicio e/ou no fim da string.</p>
	 * @access public
	 * @param string $psValue Valor a ser atribu�do
	 */
	public function setAttrValue($psValue) {
		$this->setValue($psValue);
	}

	/**
	 * Seta o valor do atributo source
	 *
	 * <p>Seta o valor do atributo source. O atributo source cont�m uma lista de
	 * ids de elementos separados por ':'.</p>
	 *
	 * @access public
	 * @param string $psSource O novo valor do atributo source
	 */
	public function setAttrSource($psSource){
		$this->csSource = $psSource;
	}

	/**
	 * Retorna o valor do atributo source
	 *
	 * <p>Retorna o valor do atributo source. O atributo source cont�m uma lista de
	 * ids de elementos separados por ':'.</p>
	 *
	 * @access public
	 * @return string O valor do atributo source
	 */
	public function getAttrSource(){
		return $this->csSource;
	}
}
?><?
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These codedf instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDGrid. Implementa uma Grid Simples.
 *
 * <p>Classe que implementa uma Grid tableless. Grid com preenchimento est�tico
 * de dados ou via Ajax. Via ajax, a grid utiliza pagina��o. Pode-se definir
 * menu de contexto para a grid ou para o cabe�alho e / ou para cada uma das
 * c�lulas da grid.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDGrid extends FWDGridAbstract
{

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDGrid.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites da Grid
	 */
	public function __construct($poBox = null){
		parent::__construct($poBox);
	}


	/**
	 * Desenha O bot�o de edi��o das propriedades da grid.
	 *
	 * <p>Retorna o c�digo HTML que desenha o bot�o de edi��o das propriedades das colunas da grid</p>
	 * @access public
	 * @return string C�digo HTML do bot�o de edi��o das propriedades da grid
	 */
	public function drawButtonEdit(){
		$moButtonEdit = new FWDViewButton();
		$moButtonEdit->setObjFWDBox(new FWDBox(0,0,$this->ciButtonEditHeight,$this->ciButtonEditWidth));
		$moButtonEdit->setAttrClass($this->csCSSClass.$this->csClassButtonEditGrid);
		$moButtonEdit->setAttrName("{$this->csName}_button_edit");
		$moEvent = new FWDClientEvent();
		$moEvent->setAttrValue("trigger_event('{$this->csName}_grid_open_popup_pref',3);");
		$moEvent->setAttrEvent("onClick");
		$moButtonEdit->addObjFWDEvent($moEvent);
		$moButtonEdit->setAttrTabIndex(0);
		return $moButtonEdit->draw();
	}

	/**
	 * Desenha o cabe�alho da Grid.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o cabe�alho da Grid.
	 * retorna o c�digo HTML do t�tulo da grid, dos bot�es de navega��o, do cabe�alho,
	 * e a div do corpo da grid</p>
	 * @access public
	 * @return string C�digo HTML que ser� inserido no cabe�alho da grid
	 */
	public function drawHeader()
	{
		$weblib_instance = FWDWebLib::getInstance();
		$gfx_ref = $weblib_instance->getGfxRef();
		$msReturn = "";
		$msColumnCode = "";
		$msPageButtonsCode = "";
		$msTitleCode="";
		$miSumHeaderHeight=0;
		$miTitleWidth=0;

		$miHeaderWidth = $this->coBox->getAttrWidth();

		$miHeaderColumnWidth = $this->coBox->getAttrWidth() - $this->ciRollerBarWidth -1;

		//arruma as dimen��es da box de cada uma das colunas
		$this->arrangeAllDimensions();

		$msStyle = $this->coBox->draw();
		$msStyle.= $this->cbDisplay ? '' : 'display:none;';
		$msStyle.= $this->csDebug ? $this->debugView() : '';
		$msClass = (($this->csClass)?"class='{$this->csClass}'":"");
		$msEvent=$this->getEvents();
		$msReturn .= "<div name='{$this->csName}' id='{$this->csName}' {$msClass} style='{$msStyle}' {$msEvent}>";

		//renderiza os headers das colunas
		$msColumnCode .= $this->drawHeaderColumns();
		//renderiza o titulo da grid
		$msReturn .= $this->drawGridTitle();

		if ((!$this->cbIsIE)&&($miSumHeaderHeight==0))
		$miSumHeaderHeight=-1;
		$miPageButtonLeftBegin = $this->coBox->getAttrWidth() - 237;
		$miPageButtonWidth= 220;
		$miAuxLeftButtomBegin = + 1;

		// bot�o de exporta��o pra Excel
		if($this->cbShowExcel){
			$moButtonExcel = new FWDViewButton();
			$miHeight = $this->ciButtonExcelHeight;
			$miAuxLeftButtomBegin += $this->ciButtonExcelWidth;
			$miLeft = $this->coBox->getAttrWidth() - $miAuxLeftButtomBegin;
			$miTop = floor(($this->ciTitleHeight - $miHeight)/2);
			$moButtonExcel->setObjFWDBox(new FWDBox($miLeft,$miTop,$miHeight,$this->ciButtonExcelWidth));
			$moButtonExcel->setAttrClass($this->csClassButtonExcel);
			$moButtonExcel->setAttrName("{$this->csName}_button_export");
			$moEvent = new FWDClientEvent();
			$moEvent->setAttrFunction("submit");
			$moEvent->setAttrEvent("onClick");
			$moEvent->setAttrParameters("{$this->csName}_button_export");
			$moButtonExcel->addObjFWDEvent($moEvent);
			$moButtonExcel->setAttrTabIndex(0);
			$msReturn.= " ".$moButtonExcel->draw();
			//atualiza onde os bot�es de pagina��o v�o aparecer
			$miPageButtonLeftBegin-=$this->ciButtonExcelWidth;
			$miAuxLeftButtomBegin += 8;
		}

		$msEditButtomCode = '';
		//bot�o de edi��o das propriedades da grid
		if(($this->cbShowEdit)&&($this->cbPopulateXML==true)&&($this->cbPopulate)){
			$msEditButtomCode .= $this->drawButtonEdit();
		}

		$msEditButtomCode = '';
		//bot�o de edi��o das propriedades da grid
		if(($this->cbShowEdit)&&($this->cbPopulateXML)&&($this->cbDinamicFill==false)){
			$msEditButtomCode .= $this->drawButtonEdit();
		}
		$mbDisplayDivEdit = !$this->cbDinamicFill?'block':'none';

		//atualiza onde os bot�es de pagina��o v�o aparecer
		if($this->cbShowEdit){
			$miLeft = $this->coBox->getAttrWidth() - $miAuxLeftButtomBegin - $this->ciButtonExcelWidth;
			$miTop = floor(($this->ciTitleHeight - $this->ciButtonExcelHeight)/2);
			$msReturn .="<div name='{$this->csName}_edit_dv_button' id='{$this->csName}_edit_dv_button' class='{$this->csCSSClass}{$this->csClassDivPageButton}' style='position:absolute;height:{$this->ciButtonExcelHeight};width:{$this->ciButtonExcelWidth};left:{$miLeft};top:{$miTop};overflow:hidden;display:{$mbDisplayDivEdit};'>{$msEditButtomCode}</div>";
			$miPageButtonLeftBegin-=$this->ciButtonEditWidth;
		}
		//desenha a div dos botoes de navega��o
		$msReturn .="<div name='{$this->csName}_page_buttons' id='{$this->csName}_page_buttons' class='{$this->csCSSClass}{$this->csClassDivPageButton}' style='position:absolute;height:1;width:{$miPageButtonWidth};left:{$miPageButtonLeftBegin};top:{$miSumHeaderHeight};overflow:hidden;display:none;'> </div>";

		$miSumHeaderHeight +=$this->ciTitleHeight;
		$miHeightColumn = $this->ciColumnHeight -1;

		if ($this->cbIsIE)
		$miSumHeaderHeight -= 2;
		else
		$miHeaderColumnWidth -=2;
		//cria a div dos headers das colunas da grid
		$msReturn .= "<div name='{$this->csName}_column_code' id='{$this->csName}_column_code' class='{$this->csCSSClass}{$this->csClassDivHeaderColumns}' style='position:absolute;height:{$miHeightColumn};width:{$miHeaderColumnWidth};left:-1;top:{$miSumHeaderHeight};' > {$msColumnCode} </div>";
		//cria o bot�o de refresh da grid
		$msReturn .= $this->drawButtonRefresh($miSumHeaderHeight,$miHeaderColumnWidth);
		//div para os botoes de pagina��o
		$msReturn .= "<div name='{$this->csName}_menu_acl' id='{$this->csName}_menu_acl'> </div>";

		//posiciona a div do body em rela��o a altura das divs anteriores..........
		$miBodyHeight = $this->coBox->getAttrHeight() - $this->ciColumnHeight - $miSumHeaderHeight - 1;
		$miBodyWidth = $this->coBox->getAttrWidth();
		$miBodyTop = $this->ciColumnHeight + $miSumHeaderHeight;

		if ($this->cbIsIE){
			$miBodyTop -=2;
			$miBodyHeight +=2;
		}
		$cbShowGlass = false;
		if($this->cbDinamicFill && $this->cbPopulateXML){
			$cbShowGlass = true;
		}
		if($this->cbRefreshOnCurrentPage){
			$mbRefreshCurr = 1;
		}else{
			$mbRefreshCurr = 2;
		}
		$msReturn .= "<input type=hidden id='{$this->csName}_page_button_columns_height' value='{$this->ciTitleHeight}'/>
		<input type=hidden name='{$this->csName}_row_over' id='{$this->csName}_row_over' value='{$this->csCSSClass}{$this->csClassBodyZebraOver}'/>
		<input type=hidden name='{$this->csName}_row_selected' id='{$this->csName}_row_selected' value='".$this->csCSSClass.$this->csClassBodyRowSelected."'/>
		<input type=hidden name='{$this->csName}_preferences_code_values' id='{$this->csName}_preferences_code_values' value=''/>
		<input type=hidden name='{$this->csName}_refresh_on_current_page' id='{$this->csName}_refresh_on_current_page' value='{$mbRefreshCurr}'/>
		<div name='{$this->csName}_body_content' id='{$this->csName}_body_content'
		class='{$this->csCSSClass}{$this->csClassDivBody}' style='position:absolute;height:{$miBodyHeight};width:{$miBodyWidth};left:-1;top:{$miBodyTop};' >
		<script language=\"JavaScript\">var obj{$this->csName} = new FWDGridScript('{$this->csName}','{$cbShowGlass}');</script>";

		return $msReturn ;
	}

	/**
	 * Desenha o Body da Grid via Ajax.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o corpo da Grid.
	 * retorna o c�digo HTML de cada uma das linhas da grid quando chamado via Ajax</p>
	 * @access public
	 * @return string C�digo HTML que ser� inserido na div do body da grid
	 */
	public function drawBodyAjax()
	{
		$this->cbPopulate=true;
		return $this->drawBody();
	}

	/**
	 * Desenha o Body da Grid.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o corpo da Grid.
	 * retorna o c�digo HTML de cada uma das linhas da grid</p>
	 * @access public
	 * @return string C�digo HTML que ser� inserido na div do body da grid
	 */
	function drawBody()
	{
		$msReturn="";

		if(($this->cbPopulate==true)&&($this->cbPopulateXML==true)){
			$miRelativeFirstRow = 0;
			$ciDataShift = 0;
			$miContDisplay=0;
			$ciShiftRow=0;
			$msIdsRowSelected = '';
			$msGridValue = "";
			$maRowsSelected = array();
			$msColumnActive="";
			$miIAux = 0;
			$miGridWidth = $this->coBox->getAttrWidth() - $this->ciRollerBarWidth;
			$miRowHeight=$this->ciRowHeight;
			$msUnitClass = $this->csCSSClass . $this->csClassBodyCellUnit;
			$miContSelectAux=0;
				
			//hakeia a sele��o de linhas da grid (isto � um hack, ou seja, um perigo!)
			if($this->cbHackExecHackSelect ==true)
			$this->setSelectRowsGrid();
				
			//cria um array associativo para testar se a linha tah selecionada
			foreach($this->caGridValue as $msValue)
			if($msValue!='')
			$maRowsSelected["{$msValue}"]=1;

			if(!($this instanceof FWDDBGrid)){
				if($this->csOrder)
				$this->orderGrid();
				$ciDataShift = $this->ciRowsPerPage * ($this->ciCurrentPage -1);
			}
			if($this->cbDinamicFill==true){
				$this->arrangeAllDimensions();
				$maxRowToDraw = min($this->ciRowsPerPage,($this->ciRowsCount - ($this->ciRowsPerPage * ($this->ciCurrentPage -1))));
				$ciShiftRow = $this->ciRowsPerPage * ($this->ciCurrentPage -1);
			}else{
				$maxRowToDraw =$this->ciRowsCount;
			}
			$maColumnAlias = array();
			$miCount = 1;
			foreach ($this->caColumns as $moColumn){
				$maColumnAlias[$moColumn->getAttrAlias()] = $miCount;
				$miCount++;
			}
			for($miCont = 1;$miCont <= count($this->caColumnDisplay); $miCont++)
			if($this->caColumnDisplay[$miCont]==1){
				if($this->caEventBlock[$miCont]==0)
				$msColumnActive .="{$miCont}:";
			}
			//else{ }	//a coluna n�o est� ativa ou est� com os eventos bloqueados , assim n�o eh necess�rio criar
			//os alvos do menu de contexto para esta colunas do corpo da grid
			$maCellBody = array();
			$miContCellDraw = 1;

			foreach($this->caOrderColumnShow as $miI){
				$moColumnBox = $this->caColumns[$miI]->getObjFWDBox();
				$moBox = new FWDBox($moColumnBox->getAttrLeft(),0, $miRowHeight+1,$moColumnBox->getAttrWidth());
				$moStatic = new FWDStaticGrid($moBox);
				if($this->caColumns[$miI]->getAttrColNoWrap()){
					$moStatic->setAttrClass($this->csCSSClass . $this->csClassStaticBodyCellUnit);
				}else{
					$moStatic->setAttrClass($this->csCSSClass . $this->csClassStaticBodyCellUnitNFix);
					$moStatic->setAttrNoWrap('false');
				}
				foreach($this->caColumns[$miI]->getContent() as $moView){
					if(($moView instanceof FWDStatic)||($moView instanceof FWDStaticGrid)){
						$moStatic->setAttrHorizontal($moView->getAttrHorizontal());
						$moStatic->setAttrVertical($moView->getAttrVertical());
						$moStatic->setAttrMarginLeft($moView->getAttrMarginLeft());
						$moStatic->setAttrMarginRight($moView->getAttrMarginRight());
						$moStatic->setAttrMarginTop($moView->getAttrMarginTop());
						$moStatic->setAttrMarginBottom($moView->getAttrMarginBottom());
					}
				}//end foreach
				$maCellBody[$miContCellDraw] = $moStatic;
				$miContCellDraw++;
			}//end foreach

			for($miI=1;$miI<=$maxRowToDraw;$miI++){
				$miContColumn = 1;
				$msLineCode="";
				$msOnClickLine="";
				$msSelectColumn='';
				$msRowName = $this->csName."_row_".($miI+$ciShiftRow);
				$msRowNameToCell = $this->csName.'_r'.($miI+$ciShiftRow);

				$mbCanSelect =true;
				$msZebra = (fmod($miI, 2)==0)?$this->csCSSClass . $this->csClassBodyZebra0:$this->csCSSClass . $this->csClassBodyZebra1;
				$msZebraAux = $msZebra;
				$miAuxTop = $miRowHeight*($miI -1)-1; ;

				if (($this->ciSelectType==GRID_SEL_SINGLE)||($this->ciSelectType==GRID_SEL_MULTIPLE) &&($mbCanSelect==true)&&($this->csSelectColumn!=0))
				$msOnClickLine = " obj{$this->csName}.sel_r(\"{$msRowName}\",{$this->ciSelectType},event); ";
				$miContCellColBody = 1;
				foreach($this->caOrderColumnShow as $miJ){
					$miAuxNumCol = $miJ+1;
					if(isset($this->caRows[$miI + $ciDataShift][$miAuxNumCol]))
					$maCellBody[$miContCellColBody]->setValue($this->caRows[$miI + $ciDataShift][$miAuxNumCol]);
					else
					$maCellBody[$miContCellColBody]->setValue('');
					if(isset($this->caRows[$miI + $ciDataShift])){
						$this->coDrawGrid->setItem( $miI + $ciShiftRow, $miAuxNumCol, $maCellBody[$miContCellColBody],$msUnitClass,$this->caRows[$miI + $ciDataShift],$this->caToolTip[$miAuxNumCol],$this->caToolTipSource[$miAuxNumCol],$maColumnAlias);
						if(($this->caEventBlock[$miAuxNumCol]==1)||($this->csDebug))
						$msLineCode .= $this->coDrawGrid->Draw($maCellBody[$miContCellColBody]->getObjFWDBox(),$msRowNameToCell.'_c'.$miAuxNumCol,"");
						else
						$msLineCode .= $this->coDrawGrid->Draw($maCellBody[$miContCellColBody]->getObjFWDBox(),$msRowNameToCell.'_c'.$miAuxNumCol,$msOnClickLine);
						//esclui os eventos q foram inseridos no drawItem
						$maCellBody[$miContCellColBody]->cleanEvents();
						//atribui o valor do static de volta no array da grid
						//??????????????????COMENTADO PARA FUNCIONAR NO AMS -> AQUI EHRA FEITA A C�PIA DO VALOR QUE EST� NA TELA PARA O ARRAY DA GRID????????????????????????????????????????????????????????
						//$this->caRows[$miI + $ciDataShift][$miAuxNumCol] = $maCellBody[$miContCellColBody]->getObjFWDString()->getAttrString();
					}
					$miContCellColBody++;
				}//end foreach

				//obtem os valores do actionTarget para a linha da grid
				$maSelCol = explode(":", $this->csSelectColumn);
				$miK = 1;
				foreach($maSelCol as $msSelCol) {
					if (isset($this->caRows[$miI + $ciDataShift][$msSelCol])){
						if ($miK != 1) $msSelectColumn .= ":".$this->caRows[$miI + $ciDataShift][$msSelCol];
						else $msSelectColumn .= $this->caRows[$miI + $ciDataShift][$msSelCol];
					}
					else {
						$mbCanSelect = false;
					}//foi definida uma coluna inexistente para a sele��o ou n�o foi definido sele��o para a grid
					$miK++;
				}
				if(($mbCanSelect)&& (isset($maRowsSelected["{$msSelectColumn}"]))){
					$msZebraAux = $this->csCSSClass.$this->csClassBodyRowSelected;
				}
				//codigo htm de cada linha da grid
				$msReturn .= "<div name='{$msRowName}' id='{$msRowName}' actionTarget='{$msSelectColumn}' classDefault='{$msZebra}'  class='{$msZebraAux}' onMouseOver='obj{$this->csName}.o_in(\"{$msRowName}\");' onMouseOut='obj{$this->csName}.o_out(\"{$msRowName}\"); ' style='height:{$miRowHeight};width:{$miGridWidth};left:0;top:{$miAuxTop};'> {$msLineCode} </div> ";
			}//end for
				

			if(!$this->coNoResultAlert){
				$moView = new FWDViewGroup(new FWDBox(0,0,40,$this->coBox->getAttrWidth()));
				$moView->setAttrClass($this->csCSSClass . 'BdNoInf');
				$moSt = new FWDStaticGrid(new FWDBox(0,0,40,$this->coBox->getAttrWidth()));
				$moSt->setValue(FWDLanguage::getPHPStringValue('fwdgrid_info_no_result_rows','Nenhum resultado encontrado'));
				$moSt->setAttrClass($this->csCSSClass . 'BdNoInfST');
				$moSt->setAttrVertical("middle");
				$moSt->setAttrNoWrap("false");
				$moSt->setAttrMarginLeft(10);
				$moView->addObjFWDView($moSt);
				$this->coNoResultAlert = $moView;
			}

			//cria a div informando que nenhum resultado foi encontrado
			if($maxRowToDraw<=0){
				$msReturn .= $this->coNoResultAlert->draw();
			}
				
			$msValuePopulate="";
			if($this->cbPopulateXML==true)
			$msValuePopulate="1";
			else
			$msValuePopulate="2";

			$msRowsIdSerialized="a:0:{}";
			$msGridValueSerialized="a:0:{}";
			if(($this->ciSelectType==GRID_SEL_SINGLE)||(($this->ciSelectType==GRID_SEL_MULTIPLE))){
				if(count($this->caGridRowsSelected)>=1)
				$msRowsIdSerialized = serialize($this->caGridRowsSelected);
				if(count($this->caGridValue)>=1)
				$msGridValueSerialized = serialize($this->caGridValue);
			}
			//else{} a grid n�o tem sele��o
				
			//inputhiddens do body da grid
			$msReturn .= " <input type=hidden name='{$this->csName}_ActiveColums' value='{$msColumnActive}' /><input type=hidden name='{$this->csName}_RowsDraw' value='1:{$miI}' />
<input type=hidden name='{$this->csName}_populatexml' id='{$this->csName}_populatexml' value='{$msValuePopulate}' />
<input type=hidden name='{$this->csName}_value_row_id' id='{$this->csName}_value_row_id' value='{$msRowsIdSerialized}' />
<input type=hidden name='{$this->csName}_value' id='{$this->csName}_value' value='{$msGridValueSerialized}' />";
	 }
		return $msReturn ;
	}

	/**
	 * cria os ids de de se��o de linhas da grid.
	 *
	 * <p>cria os ids de de se��o de linhas da grid e atribui nas variaveis de sele��o da grid,
	 * ap�s, dispara o evento de refresh da gridp>.
	 */
	public function setSelectRows($paSelectRows,$pbAddValues){
		$this->caHackSelectRows = $paSelectRows;
		$this->cbHackCleanSelect =$pbAddValues;
		$this->cbHackExecHackSelect =true;
		$this->execEventPopulate('refreshselect');
	}

	/**
	 * cria os ids de de se��o de linhas da grid.
	 *
	 * <p>cria os ids de de se��o de linhas da grid e atribui nas variaveis de sele��o da grid,
	 * ap�s, dispara o evento de refresh da gridp>.
	 */
	public function setSelectRowsGrid(){
		$ciDataShift = 0;
		$ciShiftRow = 0;
		if($this->cbHackCleanSelect==true){
			$this->caGridRowsSelected = array();
			$this->caGridValue = array();
		}
		if(!($this instanceof FWDDBGrid)){
			$ciDataShift = $this->ciRowsPerPage * ($this->ciCurrentPage -1);
		}
		if($this instanceof FWDDBGrid){
			$ciShiftRow = $this->ciRowsPerPage * ($this->ciCurrentPage -1);
		}
		$msRowName = $this->csName."_row_";
		$maAuxRowName = array();
		$maSelCol = explode(':',$this->csSelectColumn);
		for($miI=0;$miI<=count($this->caRows);$miI++){
			$msSelValue = '';
			foreach($maSelCol as $miSel){
				if(isset($this->caRows[$miI][$miSel])){
					if($msSelValue=='')
					$msSelValue .=$this->caRows[$miI][$miSel];
					else
					$msSelValue .=':' . $this->caRows[$miI][$miSel]+1;
				}
			}
			$maAuxRowName[$msSelValue] = $miI + $ciShiftRow;
		}
		$teste = implode(':',$maAuxRowName);

		foreach ($this->caHackSelectRows as $psSelRow){
			if(isset($maAuxRowName[$psSelRow])){
				$this->caGridRowsSelected[] = $msRowName . $maAuxRowName[$psSelRow] .":{$psSelRow}";
				$this->caGridValue[] = $psSelRow;
			}
		}
		$this->cbHackExecHackSelect =false;
	}

	/**
	 * Desenha o Footer da Grid.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o "p�" da Grid.
	 * retorna o c�digo HTML do "p�" da grid, incluindo o c�digo do menu de contexto, caso ele tenha sido definido dentro da grid</p>
	 * @access public
	 * @return string C�digo HTML que ser� inserido no "p�" da grid.
	 */
	function drawFooter($poBox = null)
	{

		$miCountColumns = count($this->caColumns);

		$msReturnAux = "";

		$msReturn ="</div>
<div name='{$this->csName}_contextmenu_header' id='{$this->csName}_contextmenu_header'> {$this->DrawMenu()} </div>
<input type=hidden name='{$this->csName}_columns_count' id='{$this->csName}_columns_count' value='{$miCountColumns}'/>
<input type=hidden name='{$this->csName}_rows_count' id='{$this->csName}_rows_count' value='{$this->ciRowsCount}'/> 
<script language=\"JavaScript\">
  moCheckCtrl{$this->csName} = gebi('{$this->csName}_col_ctrl');
	if(moCheckCtrl{$this->csName}) moCheckCtrl{$this->csName}.value = '';
	moInputHidden{$this->csName} = gebi('{$this->csName}_value'); 
	if(moInputHidden{$this->csName}) moInputHidden{$this->csName}.value = null;		
	moInputHidden{$this->csName}rowid = gebi('{$this->csName}_value_row_id'); 
	if(moInputHidden{$this->csName}rowid)	moInputHidden{$this->csName}rowid.value = null;		
	moInputColumnsCount{$this->csName} = gebi('{$this->csName}_columns_count'); 
	if(moInputColumnsCount{$this->csName}) moInputColumnsCount{$this->csName}.value = null;		
	moInputRowsCount{$this->csName} = gebi('{$this->csName}_rows_count'); 
	if(moInputRowsCount{$this->csName})	moInputRowsCount{$this->csName}.value = null;		
";
		if($this->cbDinamicFill)
		$msReturnAux .="trigger_event(\"{$this->csName}populate\",\"3\");";

		if($this->cbIsIE)
		$msReturnAux .= "		gebi('{$this->csName}').onselectstart=new Function (\"return false\");";
		else{
			$msReturnAux .= " gebi('{$this->csName}').onmousedown=disableselect; gebi('{$this->csName}').onclick=reEnable; ";
		}
		$msReturnAux .= "</script>";
		if($this->cbSelectionRequired){
			$msSelectionRequired = 'true';
		}else{
			$msSelectionRequired = 'false';
		}
		$msReturn.= $msReturnAux."
   </div>
   <script language='javascript'>
      var moElement = gebi('{$this->csName}');
      moElement.object = obj{$this->csName};
      moElement.object.cbIsMultiple = ".($this->ciSelectType==GRID_SEL_MULTIPLE?'true':'false').";
      moElement.object.setSelectionRequired($msSelectionRequired);
      moElement.object.ciRowsPerPage = {$this->ciRowsPerPage};
    </script>
";
		return $msReturn;
	}

	/**
	 * Desenha o Titulo da Grid.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para desenhar os headers das colunas da grid.</p>
	 * @access public
	 * @return string C�digo HTML que ser� inserido na div dos header das colunas da grid
	 */
	public function drawHeaderColumns(){
		$msColumnCode = "";

		foreach($this->caColumns as $moColumn){
			$moColumn->setOrderBy("");
			if(($moColumn->getAttrCanOrder())&&(!$moColumn->getAttrEventBlock()))
			$moColumn->setAttrClass($this->csCSSClass . $this->csClassColumnHeaderCanOrder);
			else
			$moColumn->setAttrClass($this->csCSSClass . $this->csClassColumnHeader);
		}

		$msOrder = $this->getAttrOrder();
		$maOrder = explode(":",$msOrder);
		foreach($maOrder as $msColOrder){
			$msColOrder=trim($msColOrder);
			if($msColOrder!=""){
				$miColumn="";
				$msChar = substr(trim($msColOrder),0,1);
				if(($msChar=="+")||($msChar=="-"))// ascendente
				$miColumn = substr(trim($msColOrder),1);
				else{//assumindo que soh veio um n�mero , ordena��o ascendente
					$miColumn = trim($msColOrder);
					$msChar="+";
				}
				$miColumn--;
				$moColumn = $this->caColumns[$miColumn];
				if($moColumn){
					if($moColumn->getAttrCanOrder()){
						if($msChar=="+"){
							$moColumn->setOrderBy("asc");
							$moColumn->setAttrClass($this->csCSSClass . $this->csClassColumnHeaderOrderASC);
						}
						elseif($msChar=="-"){
							$moColumn->setOrderBy("desc");
							$moColumn->setAttrClass($this->csCSSClass . $this->csClassColumnHeaderOrderDESC);
						}
						else{
							$moColumn->setOrderBy("asc");
							$moColumn->setAttrClass($this->csCSSClass . $this->csClassColumnHeaderOrderDESC);
						}
					}
				}else
				trigger_error("Trying to order an inexistent column! column number '{$miColumn}'",E_USER_WARNING);
			}//else{}//msColOrder vazio devido a um : a mais na string
				
		}
		foreach($this->caOrderColumnShow as $miI)
		{
			if($this->caColumns[$miI]->getAttrIconColumnOrderMarginLeft()==0)
			$this->caColumns[$miI]->setAttrIconColumnOrderMarginLeft($this->getAttrIconColumnOrderMarginLeft());
			else{}//foi atribuido um valor especifico para a margem esquerda da coluna

			if($this->caColumns[$miI]->getAttrIconColumnOrderMarginRight()==0)
			$this->caColumns[$miI]->setAttrIconColumnOrderMarginRight($this->getAttrIconColumnOrderMarginRight());
			else{}////foi atribuido um valor especifico para a margem direita da coluna
				
			$msColumnCode .= $this->caColumns[$miI]->drawHeader();
			foreach($this->caColumns[$miI]->getContent() as $moColumnView){
				if(($moColumnView instanceof FWDStatic)||($moColumnView instanceof FWDStaticGrid))
				$moColumnView->setAttrClass($this->csCSSClass . $this->csClassStaticColumnHeaders);
					
				$msColumnCode .= $moColumnView->draw();
			}
			$msColumnCode .= $this->caColumns[$miI]->drawFooter();
		}
		$msValuePopulate="";
		if($this->cbPopulateXML==true)
		$msValuePopulate="1";
		else
		$msValuePopulate="2";

		$msInputs = "<input type=hidden name='{$this->csName}_column_order' id='{$this->csName}_column_order' value='{$this->csOrder}' />
		<input type=hidden name='{$this->csName}_populatexml' id='{$this->csName}_populatexml' value='{$msValuePopulate}' />";

		return $msColumnCode . $msInputs;
	}

	/**
	 * Desenha o Titulo da Grid.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para desenhar o t�tulo da grid.
	 * retorna o c�digo HTML do t�tulo da grid</p>
	 * @access public
	 * @return string C�digo HTML que ser� inserido na div do titulo da grid
	 */
	public function drawGridTitle(){
		$msReturn = "";

		$miTitleWidth= $this->coBox->getAttrWidth();
		if($this->coGridTitle instanceof FWDViewGroup){
			$moBoxViewGroup = $this->coGridTitle->getObjFWDBox();
			if($moBoxViewGroup->getAttrHeight() > 0){
				$this->ciTitleHeight = $moBoxViewGroup->getAttrHeight();
			}
			if($this->coGridTitle){
				$msReturn .= "<div name='{$this->csName}_header_title' id='{$this->csName}_header_title' class='{$this->csCSSClass}{$this->csClassDivHeaderTitle}' style='position:absolute;height:{$this->ciTitleHeight};width:{$miTitleWidth};left:-1;top:-1;' >";
				foreach($this->coGridTitle->getContent() as $moViewGroupUnit)
				if(($moViewGroupUnit instanceof FWDStatic)||($moViewGroupUnit instanceof FWDStaticGrid))
				$moViewGroupUnit->setAttrClass($this->csCSSClass . $this->csClassStaticGridTitle);

				$msReturn .= $this->coGridTitle->drawBody();
				$msReturn .= "</div> ";
			}
			else{} //n�o tem TITLE da grid definida , assim n�o � necessario desenhar o TITLE
		}
		else{
			if($this->cbDinamicFill==true){
				$msReturn .= "<div name='{$this->csName}_header_title' id='{$this->csName}_header_title' class='{$this->csCSSClass}{$this->csClassDivHeaderTitle}' style='position:absolute;height:{$this->ciTitleHeight};width:{$miTitleWidth};left:-1;top:-1;' > </div> ";
			}else
			$this->ciTitleHeight=0;
		}
		return $msReturn;
	}

	/**
	 * retorna o evento que aciona o vidro na grid.
	 *
	 * <p>retorna o evento que aciona o vidro na grid, impedindo assim que algum
	 * evento seja executado enquanto outro evento � executado na grid</p>
	 * @access public
	 * @return FWDClientEvent evento cliente que aciona o vidro na grid
	 */
	public function getEventGlass(){
		$moBeforeEventGrid = new FWDClientEvent();
		$moBeforeEventGrid->setAttrValue(" gobi('{$this->csName}').showGlass();");
		$moBeforeEventGrid->setAttrEvent("onClick");
		return $moBeforeEventGrid;
	}
	/**
	 * Desenha o bot�o de refresh da grid.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para desenhar o bot�o de refresh da grid.
	 * retorna o c�digo HTML do t�tulo da grid</p>
	 * @access public
	 * @return string C�digo HTML que renderiza o bot�o de refresh da grid
	 */
	public function drawButtonRefresh($piHeight, $piWidth){
		$msReturn = "";
		$msReturnAux = '';

		if ($this->cbIsIE)
		$piWidth -= 2;
		$miButtonRefreshWidth = $this->ciRollerBarWidth+2;

		if($this->cbShowRefresh){
			$moButtonRefresh= new FWDViewButton();
			$miLeft = ceil(($this->ciRollerBarWidth - $this->ciButtonRefreshWidth)/2);
			$miTop = ceil(($this->ciColumnHeight - $this->ciButtonRefreshHeight)/2)-1;
			$moBoxRefresh = new FWDBox($miLeft,$miTop,$this->ciButtonRefreshHeight,$this->ciButtonRefreshWidth);
			$moButtonRefresh->setObjFWDBox($moBoxRefresh);
			$moButtonRefresh->setAttrClass($this->csCSSClass . $this->csClassButtonRefresh);
			$moButtonRefresh->setAttrName("{$this->csName}_button_refresh");
			$moEvent = new FWDServerEvent();
			$moEvent->setAttrFunction("{$this->csName}refresh");
			$moEvent->setAttrEvent("onClick");
			$moButtonRefresh->addObjFWDEvent($moEvent);
			$moButtonRefresh->addObjFWDEvent( $this->getEventGlass());


			$moButtonRefresh->setAttrTabIndex(0);
			$msReturnAux .= " ". $moButtonRefresh->draw();
		}
		$msReturn .= "<div name='{$this->csName}_button_refresh_div' id='{$this->csName}_button_refresh_div' class='{$this->csCSSClass}{$this->csClassDivRefreshButton}' style='position:absolute;height:{$this->ciColumnHeight};width:{$miButtonRefreshWidth};left:$piWidth;top:{$piHeight};overflow:hidden;'> {$msReturnAux}</div>";
		return $msReturn;
	}

	/**
	 * Desenha a Barra de Bot�es da grid.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar a barra de bot�es de pagina��o da grid.
	 * retorna o c�digo HTML da barra de botoes de pagina��o da grid</p>
	 * @access public
	 * @return string C�digo HTML que cont�m as inform��es da barra de bot�es de navega��o da grid
	 */
	function drawPageButtons()
	{
		$miDivTop=0;
		$miPageButtonHeight= $this->ciTitleHeight;
		$miPageCount = ceil($this->ciRowsCount / $this->ciRowsPerPage);
		$this->setLastPage($miPageCount);
		$miButtonHeight=$this->ciButtonChangeHeight;
		$miButtonWidth=$this->ciButtonChangeWidth;
		$miLeftBegin=0;
		$miButtonTop = floor(($miPageButtonHeight - $miButtonHeight) / 2);
		$miCumulativeWidth=0;
		$miProp = 1.5;
		$miButtonPropHeight = $miButtonHeight * $miProp;
		$miButtonPropTop =  $miButtonTop - 4;
		if($this->cbIsIE){
			$miButtonTop -=1;
		}else{
			$miButtonPropTop+=1;

		}
		$moEventNext = new FWDServerEvent();
		$moEventNext->setAttrFunction("{$this->csName}changepagenext");
		$moEventNext->setAttrEvent("onClick");
		$moEventNext->setAttrElements("{$this->csName}");

		$moEventBack = new FWDServerEvent();
		$moEventBack->setAttrFunction("{$this->csName}changepageback");
		$moEventBack->setAttrEvent("onClick");
		$moEventBack->setAttrElements("{$this->csName}");

		$moEventFirst = new FWDServerEvent();
		$moEventFirst->setAttrFunction("{$this->csName}changepagefirst");
		$moEventFirst->setAttrEvent("onClick");
		$moEventFirst->setAttrElements("{$this->csName}");

		$moEventLast = new FWDServerEvent();
		$moEventLast->setAttrFunction("{$this->csName}changepagelast");
		$moEventLast->setAttrEvent("onClick");
		$moEventLast->setAttrElements("{$this->csName}");

		$moEventText = new FWDClientEvent();
		$msJs = "obj{$this->csName}.checkKeyPressTextPageButton(event)";
		$moEventText->setAttrEvent("onKeyPress");
		$moEventText->setValue($msJs,"true");

		$moEventTextOnClick = new FWDClientEvent();
		$moEventTextOnClick->setAttrEvent("onClick");
		if($this->cbIsIE){
			$msReturnAux = " gebi('{$this->csName}_actual_page').onselectstart=new Function (\"return true;\");";
		}else{
			$msReturnAux = " gebi('{$this->csName}_actual_page').onmousedown=reEnable; gebi('{$this->csName}_actual_page').onclick=reEnable;";
		}
		$moEventTextOnClick->setValue($msReturnAux,"true");

		//static para o texto "P�gina"
		$moStaticBox= new FWDBox($miLeftBegin + $miCumulativeWidth,$miButtonPropTop,$miButtonPropHeight,50);
		$miCumulativeWidth += $moStaticBox->getAttrWidth();
		$moStaticPage = new FWDStaticGrid($moStaticBox);
		$moStaticString = new FWDString();
		$moStaticString->setAttrValue(FWDLanguage::getPHPStringValue('fwdgrid_page_label',"p�gina"));
		$moStaticPage->setObjFWDString($moStaticString);
		$moStaticPage->setAttrName("{$this->csName}_pagina");
		$moStaticPage->setAttrClass($this->csCSSClass . $this->csClassStaticHeaderPageButton);

		//objeto textBox para o n�mero da p�gina atual
		$moActualPage = new FWDText();
		$moTextBox = new FWDBox($miLeftBegin+ $miCumulativeWidth,$miButtonPropTop,$miButtonPropHeight,30);
		$miCumulativeWidth += $moTextBox->getAttrWidth() + 5;
		$moActualPage->setValue($this->ciCurrentPage,"true");
		$moActualPage->setObjFWDBox($moTextBox);
		$moActualPage->setAttrName("{$this->csName}_actual_page");
		$moActualPage->setAttrClass($this->csCSSClass . $this->csClassHeaderTextEdit);
		$moActualPage->addObjFWDEvent($moEventText);
		$moActualPage->addObjFWDEvent($moEventTextOnClick);

		//objeto string para a frase "de"
		$moStaticDe = new FWDStaticGrid();
		$moTotalPagesString = new FWDString();
		$moTotalPagesString->setAttrValue(FWDLanguage::getPHPStringValue('fwdgrid_number_page_of',"de"));
		$moStaticDe->setObjFWDString($moTotalPagesString);
		$moStaticBox2= new FWDBox($miLeftBegin+$miCumulativeWidth,$miButtonPropTop,$miButtonPropHeight,20);
		$miCumulativeWidth += $moStaticBox2->getAttrWidth();
		$moStaticDe->setObjFWDBox($moStaticBox2);
		$moStaticDe->setAttrName("{$this->csName}_de_text");
		$moStaticDe->setAttrClass($this->csCSSClass . $this->csClassStaticHeaderPageButton);

		//objeto string para a frase "#totalPages"
		$moStaticDe2 = new FWDStaticGrid();
		$moTotalPagesString = new FWDString();
		$moTotalPagesString->setAttrValue("{$this->getLastPage()}");
		$moStaticDe2->setObjFWDString($moTotalPagesString);
		$moStaticBox2= new FWDBox($miLeftBegin+$miCumulativeWidth,$miButtonPropTop,$miButtonPropHeight,36);
		$miCumulativeWidth += $moStaticBox2->getAttrWidth()+10;
		$moStaticDe2->setObjFWDBox($moStaticBox2);
		$moStaticDe2->setAttrName("{$this->csName}_de");
		$moStaticDe2->setAttrClass($this->csCSSClass . $this->csClassStaticHeaderPageButton);

		//bot�o para a p�gina inicial
		$moFirstPage = new FWDViewButton();
		$moButtonNavigateFP = new FWDBox($miLeftBegin+$miCumulativeWidth,$miButtonTop,$miButtonHeight,$miButtonWidth);
		$miCumulativeWidth += $moButtonNavigateFP->getAttrWidth()+6;
		$moFirstPage->setObjFWDBox($moButtonNavigateFP);
		$moFirstPage->addObjFWDEvent($moEventFirst);
		$moFirstPage->setAttrName("{$this->csName}_first_page");
		$moFirstPage->setAttrClass($this->csCSSClass . $this->csClassButtonFirstPage);

		//bot�o para pagina anterior
		$moBackPage = new FWDViewButton();
		$moButtonNavigateBP = new FWDBox($miLeftBegin+$miCumulativeWidth,$miButtonTop,$miButtonHeight,$miButtonWidth);
		$miCumulativeWidth += $moButtonNavigateBP->getAttrWidth()+6;
		$moBackPage->setObjFWDBox($moButtonNavigateBP);
		$moBackPage->addObjFWDEvent($moEventBack);
		$moBackPage->setAttrName("{$this->csName}_back_page");
		$moBackPage->setAttrClass($this->csCSSClass . $this->csClassButtonBackPage);

		//bot�o para prox. p�gina
		$moNextPage = new FWDViewButton();
		$moButtonNavigateNP = new FWDBox($miLeftBegin+$miCumulativeWidth,$miButtonTop,$miButtonHeight,$miButtonWidth);
		$miCumulativeWidth += $moButtonNavigateNP->getAttrWidth()+6;
		$moNextPage->setObjFWDBox($moButtonNavigateNP);
		$moNextPage->addObjFWDEvent($moEventNext);
		$moNextPage->setAttrName("{$this->csName}_next_page");
		$moNextPage->setAttrClass($this->csCSSClass . $this->csClassButtonNextPage);

		//bot�o para ultima p�gina
		$moLastPage = new FWDViewButton();
		$moButtonNavigateLP = new FWDBox($miLeftBegin+$miCumulativeWidth,$miButtonTop,$miButtonHeight,$miButtonWidth);
		$miCumulativeWidth += $moButtonNavigateLP->getAttrWidth()+6;
		$moLastPage->setObjFWDBox($moButtonNavigateLP);
		$moLastPage->addObjFWDEvent($moEventLast);
		$moLastPage->setAttrName("{$this->csName}_last_page");
		$moLastPage->setAttrClass($this->csCSSClass . $this->csClassButtonLastPage);

		$moFirstPage->addObjFWDEvent($this->getEventGlass());
		$moBackPage->addObjFWDEvent($this->getEventGlass());
		$moNextPage->addObjFWDEvent($this->getEventGlass());
		$moLastPage->addObjFWDEvent($this->getEventGlass());

			
		$msReturn =	$moStaticPage->draw() .
		$moActualPage->draw() .
		$moStaticDe->draw() .
		$moStaticDe2->draw() .
		$moFirstPage->draw() .
		$moBackPage->draw() .
		$moNextPage->draw() .
		$moLastPage->draw() .
		  " <input type=hidden id='{$this->csName}_gridpageinfo' name='{$this->csName}_gridpageinfo' value='{$this->getCurrentPage()}:{$this->getLastPage()}:{$this->getAttrRowsCount()}' /> ";

		//elimina todos os <enter> do c�digo para funcionar com o ajax
		if($this->getAttrDinamicFill()==true)
		$msReturn = $this->scapeStringToAjax($msReturn);

		return $msReturn;
	}


	/**
	 * Renderiza o Menu de Contexto definido dentro da grid.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o menu de contexto definido dentro da grid.
	 * Esse menu de contexto s� pode ter como subtarget os valores (lines e / ou headers). Esse m�todo arruma
	 * o subtarget do menu de contexto para todas as c�lulas do corpo da grid (lines) e / ou para cada um dos
	 * cabe�alhos das colunas do corpo da grid (headers).
	 * retorna o c�digo HTML do menu de contexto definido dentro da grid</p>
	 * @access public
	 * @return string C�digo HTML que cont�m as inform��es do menu de contexto
	 */
	public function drawMenu()
	{
		$msCodeMenu = "";

		foreach($this->caMenus as $moMenu){
			$msMenuAllElements = strtolower($moMenu->getAttrElements());
			$maElements = explode(":",$msMenuAllElements);
			$msAuxElements="";
			$msAuxName = $this->csName."_r";
			foreach($maElements as $msElement){
				if(($msElement=="lines")&&($this->cbPopulate==true)&&($this->cbPopulateXML==true))
				for($miJ=1;$miJ<=$this->ciRowsCount;$miJ++)
				for($miI=1;$miI<=$this->ciColumnsCount;$miI++)
				if( ($this->caEventBlock[$miI]==0) && ($this->caColumnDisplay[$miI]==1))
				$msAuxElements .= $msAuxName.$miJ."_c".$miI.":";
				else{ }	//nao � necess�rio criar o alvo para o menu pois ou a coluna est� "escondida"
				//ou os eventos dessa coluna est�o bloqueados
				elseif(($msElement=="headers")){
					foreach($this->caColumns as $moColumn)
					if((!$moColumn->getAttrEventBlock()==true)&&($moColumn->getAttrDisplay()))
					$msAuxElements .= $moColumn->getAttrName() .":";
					else { } //this column can't receive any event
				}//end elseif
				else {}	// ou foi definido um element ou subelement nao permitido ou nao deve-se desenhar
				// o codigo do menu para o body jah que a grid tem preenchimento via ajax
			}
			$moMenu->setAttrElements($msAuxElements);
				
			$msCodeMenu .= $moMenu->drawHeader() . "<script language=\"JavaScript\">".$this->drawBodyMenuGrid($moMenu->getAttrName())." </script>" . $moMenu->drawFooter();
		}//end foreach

		return $msCodeMenu . $this->drawMenuACL();;
	}

	/**
	 * Renderiza o Menu de Contexto ACL.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o menu de contexto ACL.
	 * retorna o c�digo HTML do menu de contexto definido ACL</p>
	 * @access public
	 * @return string C�digo HTML que cont�m as inform��es do menu de contexto ACL
	 */
	public function drawMenuACL()
	{
		$msCodeMenu="";
		//renderiza os menus com ACL
		foreach($this->caMenusACL as $moMenu){
			$msMenuAllElements = strtolower($moMenu->getAttrElements());
			$maElements = explode(":",$msMenuAllElements);
			$msAuxElements="";
			foreach($maElements as $msElement){
				if(strpos($msElement,'_acl_')){
					$miRowNumber = substr($msElement,strpos($msElement,"_acl_")+5);
					for ($miJ=1; $miJ <= count($this->caColumns); $miJ++) {
						if ( ($this->caEventBlock[$miJ]==0) && ($this->caColumnDisplay[$miJ]==1)) {
							$msAuxElements .= $this->csName ."_r{$miRowNumber}_c".$miJ.":";
						}else{ }	//nao � necess�rio criar o alvo para o menu pois ou a coluna est� "escondida"
						//ou os eventos dessa coluna est�o bloqueados
					}
				}
			}
			$moMenu->setAttrElements($msAuxElements);
			$msCodeMenu .= $moMenu->drawHeader(). "<script language=\"JavaScript\">".$this->drawBodyMenuGrid($moMenu->getAttrName())." </script>" . $moMenu->drawFooter();
		}
		if($this->cbDinamicFill==true)
		$msCodeMenu =  $this->scapeStringToAjax($msCodeMenu);
		return $msCodeMenu;
	}

	/**
	 * Renderiza o o Footer do Menu de Contexto definido dentro da grid e o draw dos menus de contexto ACL da grid.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar os menu de contexto definido dentro da grid.
	 * Esse menu de contexto s� pode ter como subtarget os valores (lines e / ou headers). Esse m�todo arruma
	 * o subtarget do menu de contexto para todas as c�lulas do corpo da grid (lines) e / ou para cada um dos
	 * cabe�alhos das colunas do corpo da grid (headers).
	 * retorna os c�digos javascript dos menus de contexto e os c�digos HTML dos menus de contexto ACL
	 *  definido dentro da grid</p>
	 * @access public
	 * @return string C�digo HTML todas as informa��es necess�rias para o funcionamento dos menus de contexto via ajax
	 */

	public function drawMenuAjax()
	{
		$msAuxElements=" ";
		$msAuxName = $this->csName."_r";
		$maCodeMenu = array();

		if($this->cbDinamicFill==true){
			$miShiftRow =$this->ciRowsPerPage * ($this->ciCurrentPage -1)+1;
			$miRowscount = $miShiftRow + min($this->ciRowsPerPage,$this->ciRowsCount - ($this->ciRowsPerPage*($this->ciCurrentPage -1)) );
		}else{
			$miShiftRow = 1;
			$miRowscount = $this->ciRowsCount+1;
		}

		foreach($this->caMenus as $moMenu){
			$msMenuAllElements = strtolower($moMenu->getAttrElements());
			$maElements = explode(":",$msMenuAllElements);
			foreach($maElements as $msElement)
			if(($msElement=="lines")&&($this->cbPopulate==true)&&($this->cbPopulateXML))
			for($miJ=$miShiftRow;$miJ<$miRowscount;$miJ++)
			for($miI=1;$miI<=$this->ciColumnsCount;$miI++)
			if( ($this->caEventBlock[$miI]==0) && ($this->caColumnDisplay[$miI]==1))
			$msAuxElements .=" gebi('".$msAuxName.$miJ."_c".$miI."').oncontextmenu=".$moMenu->getAttrName().";";
			else{ }	//nao � necess�rio criar o alvo para o menu pois ou a coluna est� "escondida"
			//ou os eventos dessa coluna est�o bloqueados
			elseif(($msElement=="headers"))
			foreach($this->caColumns as $moColumn)
			if((!$moColumn->getAttrEventBlock()==true)&&($moColumn->getAttrDisplay()))
			$msAuxElements .="gebi('{$moColumn->getAttrName()}').oncontextmenu = {$moMenu->getAttrName()}; ";
			else { } //this column can't receive any event
			else {}	// ou foi definido um element ou subelement nao permitido ou nao deve-se desenhar
			// o codigo do menu para o body jah que a grid tem preenchimento via ajax
		}

		$msCodeMenu=" ";
		$msCodeMenuFunction=" ";
		//renderiza os menus com ACL
		foreach($this->caMenusACL as $moMenu){
			$msMenuAllElements = strtolower($moMenu->getAttrElements());
			$maElements = explode(":",$msMenuAllElements);
			foreach($maElements as $msElement){
				if(strpos($msElement,'_acl_')){
					$miRowNumber = substr($msElement,strpos($msElement,"_acl_")+5);
					for ($miJ=1; $miJ <= count($this->caColumns); $miJ++)
					if ( ($this->caEventBlock[$miJ]==0) && ($this->caColumnDisplay[$miJ]==1))
					$msAuxElements .= "gebi(\"{$this->csName}_r{$miRowNumber}_c{$miJ}\").oncontextmenu = {$moMenu->getAttrName()}; ";
					else{ }	//nao � necess�rio criar o alvo para o menu pois ou a coluna est� "escondida"
					//ou os eventos dessa coluna est�o bloqueados
				}
			}
			$msCodeMenu .= " " . $moMenu->drawHeader();
			$msCodeMenuFunction .= $this->drawBodyMenuGrid($moMenu->getAttrName());
		}

		$maCodeMenu[] = $this->scapeStringToAjax($msCodeMenu);
		$maCodeMenu[] = $this->scapeStringToAjax($msCodeMenuFunction);
		$maCodeMenu[] = $this->scapeStringToAjax($msAuxElements);

		return 	$maCodeMenu;
	}//end class


	/**
	 * Desenha o corpo menu de contexto para a grid.
	 *
	 * <p>Re�ne em uma vari�vel string, o c�gido da fun��o javascript respons�vel por acionar
	 * o menu de contexto utilizado pela grid
	 * @return string C�digo javascript da fun��o do menu de contexto
	 */
	public function drawBodyMenuGrid($msMenuName) {
		return "
    soMenuManager.addMenu('{$msMenuName}',window);
    function {$msMenuName}(event){
    var id='{$msMenuName}';
    obj{$this->csName}.js_action_target_menu(id,event);
    soMenuManager.hideMenus(); 
    js_show_menu(id,event);}";
	}


	/**
	 * Renderiza a grid via ajax.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para renderizar os bot�es de p�gina��o, o conte�do do corpo,
	 * e os menus de contexto, incluindo os menus ACL da grid</p>
	 * @access public
	 * @return string C�digo HTML serializado que cont�m os bot�es de pag., o corpo, e os menus da grid
	 */
	public function execEventPopulate($psGridEvent = 'populategrid'){
		$msGridCode = array();
		$maGridCode[]=$psGridEvent;
		$this->caGridRowsSelected=array();
		$this->caGridValue = array();
		 
		//obtem o c�digo html dos bot�es de pagina��o da grid
		$this->cbPopulate=true;
		if($this->cbDinamicFill==true)
		$maGridCode[] = $this->drawPageButtons();
		else{
			$maGridCode[] = ' ';
			$this->arrangeAllDimensions();
		}
		//desenha o body da grid via ajax
		$maGridCode[] = $this->drawBody();

		$maGridButtomCode = array();
		if($psGridEvent == 'user_edit'){
			//desenha os headers das colunas
			$maGridCode[] = $this->drawHeaderColumns();
		}else{
			if(($this->cbShowEdit)&&($this->cbPopulateXML)){
				//bot�o de edi��o das propriedades da grid
				$maGridButtomCode[] = $this->drawButtonEdit();
				//desenha os headers das colunas
				$maGridButtomCode[] = $this->drawHeaderColumns();
			}
		}
		//obtem o c�digo html do menu de contexto
		$maGridCodeAux = array_merge($maGridCode,$this->drawMenuAjax());
		$maGridCodeAux = array_merge($maGridCodeAux,$maGridButtomCode);

		//escapa os /r e os /n
		for($miI=0;$miI<count($maGridCodeAux);$miI++){
			$maGridCodeAux[$miI] = $this->scapeStringToAjax($maGridCodeAux[$miI]);
		}

		$msGridCodeAux = serialize($maGridCodeAux);
		$msGridCodeAux = str_replace('\\','\\\\',$msGridCodeAux);
		$msGridCodeScape = str_replace('"','\"',$msGridCodeAux);
		$moJsGridEvent = new FWDJsEvent(JS_GRID_EVENT,$this->csName,$msGridCodeScape);
		echo $moJsGridEvent->render();
	}

	/**
	 * Renderiza a grid via ajax (utilizando pelos eventos changepage[first,next,back,last,change] e refresh).
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para renderizar o conte�do do corpo,
	 * e os menus de contexto, incluindo os menus ACL da grid</p>
	 * @access public
	 * @return string C�digo HTML que cont�m o corpo, e os menus da grid
	 */
	public function gridPopulateChange($psGridEvent,$psCodePage){
		$maGridCode = array();
		$maGridCode[] = $psGridEvent;

		$this->cbPopulate=true;
		//desenha o body da grid via ajax
		$maGridCode[] = $this->drawBody();
		//obtem o c�digo html do menu de contexto
		$maGridCode = array_merge($maGridCode,$this->drawMenuAjax());
		$maGridCode[] = $psCodePage;
		//escapa os \r e os \n
		for($miI=0;$miI<count($maGridCode);$miI++){
			$maGridCode[$miI] = $this->scapeStringToAjax($maGridCode[$miI]);
		}
		$msGridCodeAux = serialize($maGridCode);

		$msGridCodeAux = str_replace('\\','\\\\',$msGridCodeAux);
			
		$msGridCodeScape = str_replace('"','\"',$msGridCodeAux);
		$moJsGridEvent = new FWDJsEvent(JS_GRID_EVENT,$this->csName,$msGridCodeScape);
		echo $moJsGridEvent->render();
	}

	/**
	 * Renderiza a grid via ajax (utilizado para alterar o ordenamento das colunas da grid).
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para renderizar o conte�do dos headers das colunas, do corpo,
	 * e os menus de contexto, incluindo os menus ACL da grid</p>
	 * @access public
	 * @return string C�digo HTML que cont�m o corpo, e os menus da grid
	 */
	public function gridPopulateOrderColumn($psGridEvent,$psCodePage){
		$maGridCode = array();
		$maGridCode[] = $psGridEvent;

		if(($this->cbDinamicFill==false))
		$this->arrangeAllDimensions();

		$this->caGridRowsSelected=array();
		$this->caGridValue = array();

		$this->cbPopulate=true;
		//desenha o body da grid via ajax
		$maGridCode[] = $this->drawBody();
		//arruma as dimens�es caso a grid seja est�tica
		//renderiza as colunas
		$maGridCode[] = $this->drawHeaderColumns();
		//obtem o c�digo html do menu de contexto
		$maGridCode = array_merge($maGridCode,$this->drawMenuAjax());
		$maGridCode[] = $psCodePage;
		//escapa os \r e os \n
		for($miI=0;$miI<count($maGridCode);$miI++){
			$maGridCode[$miI] = $this->scapeStringToAjax($maGridCode[$miI]);
		}
		$msGridCodeAux = serialize($maGridCode);

		$msGridCodeAux = str_replace('\\','\\\\',$msGridCodeAux);
		$msGridCodeScape = str_replace('"','\"',$msGridCodeAux);
		$moJsGridEvent = new FWDJsEvent(JS_GRID_EVENT,$this->csName,$msGridCodeScape);
		echo $moJsGridEvent->render();
	}

	/**
	 * Ordena a grid que n�o � ordenada via BD
	 *
	 * <p>Ordena os dados que est�o no vetor de ordenamento da grid conforme o tipo de ordenamento
	 * que est� nas colunas da grid</p>
	 * @access public
	 * @return string C�digo HTML que cont�m o corpo, e os menus da grid
	 */
	function orderGrid()
	{

		$maRowAux= array();
		$maOrder = explode(":",$this->csOrder);
		$miCont = 0;
		if(($maOrder)&&($this->cbPopulateXML==true))
		foreach($maOrder as $msColOrder){
			$miCont++;
			if($msColOrder!=""){
				$miColumn="";
				$msChar = substr(trim($msColOrder),0,1);
				if(($msChar=="+")||($msChar=="-"))// ascendente
				$miColumn = substr(trim($msColOrder),1);
				else{//assumindo que soh veio um n�mero , ordena��o ascendente
					$miColumn = trim($msColOrder);
					$msChar="+";
				}
				$moColumn = $this->caColumns[$miColumn -1];
				if($moColumn)
				if(($moColumn->getAttrCanOrder()=="true")){
					$maOrder = array();
					for ($miI=1; $miI <= $this->ciRowsCount; $miI++) {
						if (isset($this->caRows[ $miI]))
						$maRow = &$this->caRows[$miI];
						else {
							$maOrder[$miI] = "";
							continue;
						}
						$maOrder[$miI] = isset( $maRow[$miColumn]) ? $maRow[$miColumn] : "";
					}

					if ($msChar == "+"){
						asort($maOrder);
					}else
					arsort($maOrder);
						
					$miI = 1;
					$maRowAux{$miCont} = array();
					foreach( $maOrder as $miKey => $msValue) {
						$maRowAux{$miCont}[$miI++] = $this->caRows[$miKey];
					}
					unset($this->caRows);
					unset($maOrder);
					$this->caRows = &$maRowAux{$miCont};
				}
				else trigger_error("Trying to order an inexistent column! column number '{$miColumn}'",E_USER_WARNING);
			}//else{}//msColOrder vazio devido a um : amais na string
		}
	}

	/**
	 * M�todo executado quando o XML do elemento termina de carregar.
	 *
	 * <p>M�todo executado quando o XML do elemento termina de carregar. Utilizado para
	 * ajustar n�mero da pagina atual e do n�mero de p�ginas da grid.</p>
	 *
	 * @access public
	 */
	public function execute()
	{
		$moEventHandler = FWDStartEvent::getInstance();
		$moEventHandler->addAjaxEvent(new FWDGridRefresh("{$this->csName}refresh",$this->csName));
		$moEventHandler->addSubmitEvent(new FWDGridExport("{$this->csName}_button_export",$this->csName));
		$moEventHandler->addAjaxEvent(new FWDGridChangePageFirst("{$this->csName}changepagefirst",$this->csName));
		$moEventHandler->addAjaxEvent(new FWDGridChangePageChange("{$this->csName}changepagechange",$this->csName));
		$moEventHandler->addAjaxEvent(new FWDGridPopulate("{$this->csName}populate",$this->csName));
		//$moEventHandler->addAjaxEvent(new FWDGridRefreshSelect("{$this->csName}refreshselect",$this->csName));

		if($this->cbShowEdit){
			$moEventHandler->addAjaxEvent(new FWDGridUserEdit("{$this->csName}_grid_preference_save",$this->csName));
			$moEventHandler->addAjaxEvent(new FWDGridDrawUserEdit("{$this->csName}_draw_user_edit",$this->csName));
			$moEventHandler->addAjaxEvent(new FWDGridUserEditDefaultValues("{$this->csName}_grid_preference_default",$this->csName));
			$moEventHandler->addAjaxEvent(new FWDGridCreatePopupEditPref("{$this->csName}_grid_open_popup_pref",$this->csName));
		}
			
		if($this->cbDinamicFill == true) {
			$msInfoPages = FWDWebLib::getPOST($this->csName."_gridpageinfo");
			if($msInfoPages){
				$maInfoPage = explode(":",$msInfoPages);
				$this->setCurrentPage($maInfoPage[0]);
				$this->setLastPage($maInfoPage[1]);
				$this->setAttrRowsCount($maInfoPage[2]);
			}
			//else { }//nao existe informa��o de paginas (pag. atual e quant. de pag.) da grid no html lido pelo ajax
			$moEventHandler->addAjaxEvent(new FWDGridChangePageNext("{$this->csName}changepagenext",$this->csName));
			$moEventHandler->addAjaxEvent(new FWDGridChangePageBack("{$this->csName}changepageback",$this->csName));
			$moEventHandler->addAjaxEvent(new FWDGridChangePageLast("{$this->csName}changepagelast",$this->csName));
			$moEventHandler->addAjaxEvent(new FWDGridChangePageFirst("{$this->csName}changepagefirst",$this->csName));
			$moEventHandler->addAjaxEvent(new FWDGridChangePageChange("{$this->csName}changepagechange",$this->csName));
		}
		//else { } // a grid n�o tem preenchimento via ajax, assim os eventos n�o precisam ser criados
		foreach($this->caColumns as $moColumn){
			if(($moColumn->getAttrAlias())&&($moColumn->getAttrCanOrder()=="true")&&(!$moColumn->getAttrEventBlock())&&($this instanceof FWDDBGrid))
			$moEventHandler->addAjaxEvent(new FWDGridColumnOrder("{$this->csName}_{$moColumn->getAttrAlias()}",$this->csName.":".$moColumn->getAttrAlias()));
			elseif(($moColumn->getAttrCanOrder()=="true")&&(!$moColumn->getAttrEventBlock())&&(!($this instanceof FWDDBGrid)))
			$moEventHandler->addAjaxEvent(new FWDGridColumnOrder("{$this->csName}_{$moColumn->getAttrName()}",$this->csName.":".$moColumn->getAttrName()));

			if($moColumn->getAttrCanOrder()=="true"){
				if($this instanceof FWDDBGrid)
				$msInfoOrderBy = FWDWebLib::getPOST("{$this->csName}_{$moColumn->getAttrAlias()}_orderby_value");
				else
				$msInfoOrderBy = FWDWebLib::getPOST("{$this->csName}_{$moColumn->getAttrName()}_orderby_value");
				if($msInfoOrderBy)
				$moColumn->setOrderBy($msInfoOrderBy);
			}
		}

		// reconfigura a grid conforme as preferencias do usu�rio
		if(($this->cbShowEdit==true)&&($this->csPreferencesHandle!=''))
		{
			$moHandlePref = FWDWebLib::getObject($this->csPreferencesHandle);
			if($moHandlePref){
				$moHandlePref->loadPrefereces($this->csName);
				//altera as colunas que ser�o exibidas e a ordem em que as colunas ser�o exibidas
				if($moHandlePref->getOrderColumn()){
					$this->caOrderColumnShow = explode(':',$moHandlePref->getOrderColumn());
					for($miI=0;$miI<count($this->caColumns);$miI++)
					$this->caColumns[$miI]->setAttrDisplay('false');
					for($miI=0;$miI<count($this->caOrderColumnShow);$miI++)
					if(isset($this->caColumns[$this->caOrderColumnShow[$miI]]))
					$this->caColumns[$this->caOrderColumnShow[$miI]]->setAttrDisplay('true');
				}else{
					for($miI=0;$miI<count($this->caColumns);$miI++)
					if($this->caColumns[$miI]->getAttrDisplay()==true)
					$this->caOrderColumnShow[]=$miI;
				}
				//altera as propriedades de largura e noWrap de cada uma das colunas da grid
				if($moHandlePref->getColumnsProp()){
					$maAuxColumnsProp = explode(';',$moHandlePref->getColumnsProp());
					for($miI = 0;$miI<count($maAuxColumnsProp);$miI++){
						$maColumnProp = explode(':',$maAuxColumnsProp[$miI]);
						$this->caColumns[$maColumnProp[0]]->getObjFWDBox()->setAttrWidth($maColumnProp[1]);
						if($maColumnProp[2]=='false')
						$this->caColumns[$maColumnProp[0]]->setAttrColNoWrap("true");
						else
						$this->caColumns[$maColumnProp[0]]->setAttrColNoWrap("false");
					}
				}
				//altera a ordena��o dos dados das colunas da grid
				if($moHandlePref->getOrderColumnsData()){
					$this->csOrder = $moHandlePref->getOrderColumnsData();
				}
				//altera o numero de linhas por p�gina
				if($moHandlePref->getRowsPerPage()){
					$this->ciRowsPerPage = $moHandlePref->getRowsPerPage();
				}
				//altera a altura das linhas
				if($moHandlePref->getRowsHeight()){
					$this->ciRowHeight = $moHandlePref->getRowsHeight();
				}
				//cria um tooltip na coluna alvo com o conte�do da coluna source
				if($moHandlePref->getToolTipCol()){
					for($miI=0;$miI<count($this->caColumns);$miI++)
					$this->caColumns[$miI]->setAttrToolTip('false');
					$maTTaux = explode(':',$moHandlePref->getToolTipCol());
					$this->caColumns[$maTTaux[1]-1]->setAttrToolTip('true');
					$this->caColumns[$maTTaux[1]-1]->setSourceToolTip($maTTaux[0]);
				}
			}else{}
		}else{
			$msInfoOrder = trim(FWDWebLib::getPOST("{$this->csName}_column_order"));
			if($msInfoOrder)
			$this->csOrder = $msInfoOrder;
		}

		$msPopulateValue = FWDWebLib::getPOST("{$this->csName}_populatexml");
		if($msPopulateValue=="1")
		$this->setAttrPopulate("true");
		elseif($msPopulateValue=="2")
		$this->setAttrPopulate("false");

		$msClassOver = FWDWebLib::getPOST("{$this->csName}_row_over");
		if($msClassOver){
			if($this->csCSSClass){
				$maClassOver = explode("$this->csCSSClass",$msClassOver);
				if(isset($maClassOver[1])){
					$this->csClassBodyZebraOver = $maClassOver[1];
				}
			}else
			$this->csClassBodyZebraOver = $msClassOver;
		}

		$msClassSelected = FWDWebLib::getPOST("{$this->csName}_row_selected");
		if($msClassSelected){
				
			if($this->csCSSClass){
				$maClassSelected = explode("$this->csCSSClass",$msClassSelected);
				if(isset($maClassSelected[1])){
					$this->csClassBodyRowSelected = $maClassSelected[1];
				}
			}else
			$this->csClassBodyRowSelected = $msClassSelected;
		}
		//value da grid contendo as linhas selecionadas da grid
		$auxRowsSelectedValue=FWDWebLib::convertToISO(FWDWebLib::getPOST("{$this->csName}_value"),true);
		//echo "alert('{$auxRowsSelectedValue}');";
		if(($auxRowsSelectedValue)&&($auxRowsSelectedValue!=":"))
		if(!(strpos($auxRowsSelectedValue,'a')===false)&&(strpos($auxRowsSelectedValue,'a')==0) )
		$this->caGridValue=FWDWebLib::unserializeString($auxRowsSelectedValue);
		else $this->caGridValue= array();
			
		//ids das linas com os selected columns da grid
		$auxRowsSelected = FWDWebLib::convertToISO(FWDWebLib::getPOST("{$this->csName}_value_row_id"),true);
		//echo "alert('{$auxRowsSelected}');";
		if(!(strpos($auxRowsSelected,'a')===false)&&(strpos($auxRowsSelected,'a')==0))
		$this->caGridRowsSelected=FWDWebLib::unserializeString($auxRowsSelected);
		else
		$this->caGridRowsSelected= array();

		$msAuxSelectHack = FWDWebLib::getPOST("{$this->csName}_hack_selected_rows");
		if($msAuxSelectHack){
			if(!(strpos($msAuxSelectHack,'a')===false)&&(strpos($msAuxSelectHack,'a')==0)){
				$maHackSel=FWDWebLib::unserializeString($msAuxSelectHack);
				if(($maHackSel[1]==1)||($maHackSel[1]==2)){
					$this->pbAddHackSelectedRows = $maHackSel[1];
					$this->caHackSelectRows = $maHackSel[0];
				}else{
					$this->pbAddHackSelectedRows = null;
					$this->caHackSelectRows = null;
				}
			}else{
				$this->pbAddHackSelectedRows = null;
				$this->caHackSelectRows = null;
			}
		}
	}

	public function scapeStringToAjax($psValue)
	{
		return str_replace(array("\n","\r"),array(' ',' '),$psValue);;
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDColumn. Implementa Colunas.
 *
 * <p>Classe que implementa Colunas.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDColumn extends FWDViewGrid {

	/**
	 * Define a profundidade da coluna, para manipular sobreposi��o de elementos (zenith-index)
	 * @var integer
	 * @access protected
	 */
	protected $ciIndex;

	/**
	 * Define se eventos devem ser bloqueados ou n�o
	 * @var boolean
	 * @access protected
	 */
	protected $cbBlockEvent=false;

	/**
	 * Define o alias da coluna para a DBGrid
	 * @var string
	 * @access protected
	 */
	protected $csAlias="";

	/**
	 * Define se a coluna pode ordenar a grid
	 * @var boolean
	 * @access protected
	 */
	protected $cbCanOrder=false;

	/**
	 * Define o tipo de ordenamento da coluna
	 * @var string
	 * @access protected
	 */
	protected $csOrderBy = "";

	/**
	 * Define se a coluna deve exibir um tooltip com as informa��es da c�lula
	 * @var protected
	 * @access protected
	 */
	protected $cbToolTip=false;

	/**
	 * Define O a coluna fonte da do conte�do da TT
	 * @var protected
	 * @access protected
	 */
	protected $ciSourceTT = '';
	/**
	 * Margem esquerda do �cone ascentende e descendente
	 * @var integer
	 * @access protected
	 */
	protected $ciIconColumnOrderMarginLeft =0;

	/**
	 * Margem do �cone at� o Static do �cone ascentende e descendente
	 * @var integer
	 * @access protected
	 */
	protected $ciIconColumnOrderMarginRight =0;


	/**
	 * Se a coluna pode ser editada ou n�o
	 * @var boolean
	 * @access protected
	 */
	protected $cbEdit = true;

	/**
	 * Armazena o conte�do do conteiner da coluna
	 * @var array
	 * @access protected
	 */
	protected $caContent = array();

	/**
	 * Se a coluna do corpo da grid deve ter nowrap ou n�o
	 * @var array
	 * @access protected
	 */
	protected $cbColNoWrap = true;

	/**
	 * Armazena o nome da grid que a coluna esta inserida
	 * @var string
	 * @access protected
	 */
	protected $csGridName = '';

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDColumn.</p>
	 * @access public
	 * @param FWDBox $poBox Box que limita o tamanho da coluna
	 */
	public function __construct($poBox = null) {
		parent::__construct($poBox);
	}

	/**
	 * Retorna o html do header da coluna.
	 *
	 * <p>Retorna o html do footer coluna.</p>
	 * @access public
	 * @param FWDBox $poBox Box que limita o tamanho da coluna
	 * @return string html do header da coluna
	 */
	public function drawHeader($poBox = null)
	{
		if((!$this->cbBlockEvent)){
			if(($this->cbCanOrder)&&($this->csAlias))
			{
				$moEvent = new FWDServerEvent();
				$moEvent->setAttrFunction("{$this->csGridName}_{$this->csAlias}");
				$moEvent->setAttrEvent("onClick");
				$this->addObjFWDEvent($moEvent);

				$moBeforeEventGrid = new FWDClientEvent();
				$moBeforeEventGrid->setAttrValue(" gobi('{$this->csGridName}').showGlass();");
				$moBeforeEventGrid->setAttrEvent("onClick");
				$this->addObjFWDEvent($moBeforeEventGrid);


			}else{
				if($this->getAttrCanOrder())	{
					$moEvent = new FWDServerEvent();
					$moEvent->setAttrFunction("{$this->csGridName}_{$this->csName}");
					$moEvent->setAttrEvent("onClick");
					$this->addObjFWDEvent($moEvent);

					$moBeforeEventGrid = new FWDClientEvent();
					$moBeforeEventGrid->setAttrValue(" gobi('{$this->csGridName}').showGlass();");
					$moBeforeEventGrid->setAttrEvent("onClick");
					$this->addObjFWDEvent($moBeforeEventGrid);
				}//else { } //a coluna n�o pode ordenar , n�o � necess�rio adicionar o evento na coluna
			}

			$msEvent=$this->getEvents();
		}else {// eventBlock = true - coluna nao deve ter eventos
			$msEvent='';
		}

		if(($this->csOrderBy=="+")||($this->csOrderBy=="-")){
			$msIcon = "";
			$moWebLib = FWDWebLib::getInstance();
			$csGFX = $moWebLib->getGfxRef();
			if($this->csOrderBy=="+")
			$msIcon=("{$csGFX}order_asc.gif");
			else
			$msIcon = ("{$csGFX}order_desc.gif");
			foreach($this->caContent as $moView){
				if(($moView instanceof FWDStatic)||($moView instanceof FWDStaticGrid)){
					$msLeftBlank = "";
					$msRightBlank = "";
					for($miI=1;$miI<=$this->ciIconColumnOrderMarginLeft;$miI++)
					$msLeftBlank .="&nbsp;";
					if($moView->getAttrHorizontal()=="right")
					for($miJ=1;$miJ<=$this->ciIconColumnOrderMarginRight;$miJ++)
					$msRightBlank .="&nbsp;";
					$moView->setFWDIconCode("{$msLeftBlank}<img src='{$msIcon}' class='FWDIcon'/>{$msRightBlank}");
				}
			}
		}
		else{}// a coluna nao deve ter o �cone de ordenamento

		$msStyle = $this->coBox->draw();
		$msStyle.= $this->cbDisplay ? '' : 'display:none;';
		$msClass = (($this->csClass)?"class='{$this->csClass}'":"");
		return "<div name='{$this->csName}' id='{$this->csName}' {$msClass} style='{$msStyle}' {$msEvent}>";
	}

	/**
	 * Retorna o html do body da coluna.
	 *
	 * <p>Retorna o html do body (do static e da string contida na coluna) coluna.</p>
	 * @access public
	 * @param FWDBox $poBox Box que limita o tamanho da coluna
	 * @return string html do body da coluna
	 */
	public function drawBody($poBox = null)
	{
		$msReturn="";
		$miMax = count ($this->caContent);
		for($miI = 0; $miI < $miMax; $miI++)
		$msReturn .= $this->caContent[$miI]->draw();

		return $msReturn;
	}

	/**
	 * Retorna o html do footer da coluna.
	 *
	 * <p>Retorna o html do footer da coluna.</p>
	 * @access public
	 * @param FWDBox $poBox Box que limita o tamanho da coluna
	 * @return string html do footer da coluna
	 */
	public function drawFooter($poBox = null)
	{
		if($this->csOrderBy == "+")
		$msOrderByValue="asc";
		elseif($this->csOrderBy == "-")
		$msOrderByValue="desc";
		else
		$msOrderByValue="";

		if($this->getAttrAlias()){
			return "</div> <input type=hidden name='{$this->csGridName}_{$this->getAttrAlias()}_orderby_value' id='{$this->csGridName}_{$this->getAttrAlias()}_orderby_value' value='{$msOrderByValue}' />";
		}else{
			return "</div> <input type=hidden name='{$this->csGridName}_{$this->getAttrName()}_orderby_value' id='{$this->csGridName}_{$this->getAttrName()}_orderby_value' value='{$msOrderByValue}' />";
		}
	}

	/**
	 * Seta o valor booleano do atributo BlockEvent.
	 *
	 * <p>Seta o valor booleano do atributo BlockEvent. Para bloquear eventos,
	 * o atributo deve conter o valor booleano TRUE. Para permitir eventos,
	 * deve conter o valor booleano FALSE.</p>
	 * @access public
	 * @param string $psBlockEvent Define se eventos devem ser bloqueados ou n�o
	 */
	public function setAttrEventBlock($psBlockEvent) {
		$this->cbBlockEvent = FWDWebLib::attributeParser($psBlockEvent,array("true"=>true, "false"=>false),"psBlockEvent");
	}

	/**
	 * Seta o valor do alias do atributo csAlias.
	 *
	 * <p>Seta o valor string do atributo Alias. Utilizado para a DBGrid para associar uma coluna
	 * da grid a uma coluna da consulta ao BD.</p>
	 * @access public
	 * @param string $psAlias Define o Alias da Coluna
	 */
	public function setAttrAlias($psAlias) {
		$this->csAlias = $psAlias;
	}

	/**
	 * Retorna a largura da coluna.
	 *
	 * <p>Retorna a largura da coluna (que � a mesma da box), em pixels.</p>
	 * @access public
	 * @return integer Largura da coluna
	 */
	public function getAttrWidth() {
		return $this->coBox->getAttrWidth();
	}

	/**
	 * Retorna o Alias da coluna.
	 *
	 * <p>Retorna o Alias da coluna , utilizado para a DBGrid , para associar uma coluna da grid a uma coluna
	 * de uma consulta ao BD.</p>
	 * @access public
	 * @return string Alias da coluna
	 */
	public function getAttrAlias() {
		return $this->csAlias;
	}

	/**
	 * Retorna o valor booleano do artributo EventBlock.
	 *
	 * <p>Retorna o valor booleano do artributo EventBlock.</p>
	 * @access public
	 * @return boolean Indica se eventos devem ser bloqueados ou n�o
	 */
	public function getAttrEventBlock() {
		return $this->cbBlockEvent;
	}

	/**
	 * Retorna se a coluna pode ou n�o ordenar.
	 *
	 * <p>Retorna se a coluna pode ou n�o ordenar o conte�do da grid.</p>
	 * @access public
	 * @return boolean Se a coluna pode ordenar ou n�o
	 */
	public function getAttrCanOrder(){
		return $this->cbCanOrder;
	}

	/**
	 * Seta o valor do atributo de CanOrder da coluna.
	 *
	 * <p>Seta o valor do atributo canOrder da coluna, utilizando somente se a grid for uma instancia de DBGrid.</p>
	 * @access public
	 * @param string $pbCanOrder Define se a coluna pode ou nao ordenar a grid
	 */
	public function setAttrCanOrder($pbCanOrder){
		$this->cbCanOrder = FWDWebLib::attributeParser($pbCanOrder,array("true"=>true, "false"=>false),"pbCanOrder");
	}

	/**
	 * Seta o valor do atributo de OrderBy da coluna.
	 *
	 * <p>Seta o valor do atributo OrderBy da coluna para ser utilizado para ordenar a dbGrid. tipos de
	 * ordenamentos possiveis (asc -> ascendente , desc -> descendente)</p>
	 * @access public
	 * @param string $psOrderBy Define o tipo de ordenamento da coluna da dbGrid
	 */
	public function setOrderBy($psOrderBy){
		$psOrderBy = trim(strtolower($psOrderBy));
		if(($psOrderBy == "asc")||($psOrderBy=='+'))
		$this->csOrderBy = "+";
		elseif(($psOrderBy=="desc")||($psOrderBy=="-"))
		$this->csOrderBy = "-";
		elseif($psOrderBy=="")
		$this->csOrderBy = "";
		else
		trigger_error("Trying to use a wrong orderBy, using '{$psOrderBy}' not ['asc','desc']",E_USER_ERROR);
	}

	/**
	 * Retorna o tipo de ordenamento da coluna.
	 *
	 * <p>Retorna o tipo de ordenamento da coluna da grid.</p>
	 * @access public
	 * @return string Tipo de ordenamento da coluna da grid
	 */
	public function getOrderBy()
	{
		return $this->csOrderBy;
	}

	/**
	 * Seta o valor do atributo ToolTip da coluna.
	 *
	 * <p>Seta o valor do atributo ToolTip da coluna para, caso necess�rio,
	 * exibir um tooltip com as informa��es da c�lula no evento onMouseOver.</p>
	 * @access public
	 * @param string $psToolTip Define se a coluna deve exibir um tooltip com as informa��es da c�lula
	 */
	public function setAttrToolTip($psToolTip){
		$this->cbToolTip = FWDWebLib::attributeParser($psToolTip,array("true" => true, "false" => false),"psToolTip");
	}

	/**
	 * Retorna o valor do atributo ToolTip da coluna.
	 *
	 * <p>Retorna o valor do atributo ToolTip da coluna para, caso necess�rio,
	 * exibir um tooltip com as informa��es da c�lula no evento onMouseOver.</p>
	 * @access public
	 * @return boolean Indica se a coluna deve exibir um tooltip ou n�o
	 */
	public function getAttrToolTip(){
		return $this->cbToolTip;
	}

	/**
	 * Seta o valor do atributo ToolTip da coluna.
	 *
	 * <p>Seta o valor do atributo source da ToolTip da coluna.</p>
	 * @access public
	 * @param string $piToolTipSource Seta o valor do atributo source da ToolTip da coluna.
	 */
	public function setSourceToolTip($piToolTipSource){
		$this->ciSourceTT = $piToolTipSource;
	}

	/**
	 * Retorna o source do tooltip da coluna.
	 *
	 * <p>Retorna o source do tooltip da coluna da grid.</p>
	 * @access public
	 * @return integer Indica qual o source do tooltip da coluna da grid
	 */
	public function getSourceToolTip(){
		return $this->ciSourceTT;
	}

	/**
	 * Seta o valor da Margem a esquerda do �cone de ordenamento.
	 *
	 * <p>Seta o valor da margem � esquerda do �cone de ordenamento do header das colunas da grid</p>
	 * @access public
	 * @param string $piIconMarginLeft Margem a esquerda do �cone de ordenamento da coluna
	 */
	public function setAttrIconColumnOrderMarginLeft($piIconMarginLeft){
		$this->ciIconColumnOrderMarginLeft = $piIconMarginLeft;
	}

	/**
	 * Seta a margem � direita do �cone de ordenamento.
	 *
	 * <p>Seta o valor da margem � direita (entre o �cone e o Static) do �cone de ordenamento do header das colunas da grid</p>
	 * @access public
	 * @param string $piIconMarginLeft Margem a direita do �cone de ordenamento da coluna
	 */
	public function setAttrIconColumnOrderMarginRight($piIconMarginRight){
		$this->ciIconColumnOrderMarginRight = $piIconMarginRight;
	}

	/**
	 * Retorna A margem � esquerda do Icone de Ordenamento do header das colunas da grid.
	 *
	 * <p>Retorna A margem � a esquerda do Icone de Ordenamento do header das colunas da grid..</p>
	 * @access public
	 * @return integer Margem � esquerda e o Static do header da coluna
	 */
	public function getAttrIconColumnOrderMarginLeft(){
		return $this->ciIconColumnOrderMarginLeft;
	}

	/**
	 * Retorna A margem � direita do Icone de Ordenamento do header das colunas da grid.
	 *
	 * <p>Retorna A margem � direita do Icone de Ordenamento do header das colunas da grid..</p>
	 * @access public
	 * @return integer Margem a direita do icone de ordenamento e o Static do header da coluna
	 */
	public function getAttrIconColumnOrderMarginRight(){
		return $this->ciIconColumnOrderMarginRight;
	}

	/**
	 * Seta se a coluna pode ser editada.
	 *
	 * <p>Seta se a coluna pode ser exibida pelas preferencias do usu�rio</p>
	 * @access public
	 * @param string $pbEdit se a coluna pode ser editada pelo usu�rio
	 */
	public function setAttrEdit($pbEdit){
		$this->cbEdit = FWDWebLib::attributeParser($pbEdit,array("true"=>true, "false"=>false),"pbEdit");
	}

	/**
	 * Retorna se a coluna pode ser Editada.
	 *
	 * <p>Retorna se a coluna pode ser editada pela grid quando o usu�rio esta mudando as
	 * preferencias de visualiza��o da grid.</p>
	 * @access public
	 * @return boolean Indica se a coluna pode ser editada
	 */
	public function getAttrEdit(){
		return $this->cbEdit;
	}

	/**
	 * Seta o nome da grid que a coluna pertence.
	 *
	 * <p>Seta o nome da grid que a coluna pertense, utilizado para formar o
	 * nome do evento de ordenamento das colunas, pois pode haver duas grids
	 * na mesma p�gina com colunas com o mesmo alias</p>
	 * @access public
	 * @param string $psGridName se a coluna pode ser editada pelo usu�rio
	 */
	public function setGridName($psGridName){
		$this->csGridName = $psGridName;
	}

	/**
	 * Retorna o nome da grid que a coluna pertence.
	 *
	 * <p>Retorna o nome da grid que a coluna pertence.</p>
	 * @access public
	 * @return string Nome da grid
	 */
	public function getGridName(){
		return $this->csGridName;
	}

	/**
	 * Seta se os statics do corpo da grid da coluna devem ter nowrap.
	 *
	 * <p>Seta se os statics do corpo da grid da coluna devem ter nowrap</p>
	 * @access public
	 * @param boolean $pbColNoWrap Margem a direita do �cone de ordenamento da coluna
	 */
	public function setAttrColNoWrap($pbColNoWrap){
		$this->cbColNoWrap = FWDWebLib::attributeParser($pbColNoWrap,array("true"=>true, "false"=>false),"pbColNoWrap");
	}

	/**
	 * Retorna se os statics do corpo da grid da coluna devem ter nowrap.
	 *
	 * <p>Retorna se os statics do corpo da grid da coluna devem ter nowrap.</p>
	 * @access public
	 * @return boolean Indica se o nowrap deve ser aplicado nos statics do corpo da grid
	 */
	public function getAttrColNoWrap(){
		return $this->cbColNoWrap;
	}

	/**
	 * Adiciona um Drawing.
	 *
	 * <p>Adiciona um Drawing no container.</p>
	 * @access public
	 * @param FWDDrawing $poDrawing View
	 */
	public function addObjFWDDrawing(FWDDrawing $poDrawing){
		$this->caContent[] = $poDrawing;
	}

	/**
	 * Retorna todas as views do container.
	 *
	 * <p>M�todo para retornar todas as views do container.</p>
	 * @access public
	 * @return array Views
	 */
	public function getContent(){
		return $this->caContent;
	}

	/**
	 * Retorna o valor do static da coluna.
	 *
	 * <p>Retorna o valor do static da coluna da grid.</p>
	 * @access public
	 * @return boolean Indica se a coluna pode ser exibida
	 */
	public function getValue($poBox = null)
	{
		$msReturn="";
		for($miI = 0; $miI < count ($this->caContent); $miI++) {
			if(($this->caContent[$miI] instanceof FWDStatic)||($this->caContent[$miI] instanceof FWDStaticGrid))
			$msReturn .= $this->caContent[$miI]->getObjFWDString()->getAttrString();
		}
		return $msReturn;
	}

	/**
	 * Retorna um array com todos sub-elementos do container
	 *
	 * <p>Retorna um array contendo todos elementos contidos no container ou em
	 * seus descendentes.
	 * OBS: O array pode conter elementos repetidos, caso haja algum ItemController
	 * que perten�a ao Container e a um Controller simultaneamente.
	 * </p>
	 * @access public
	 * @return array Array de sub-elementos
	 */
	public function collect() {
		$maOut = array();
		$maOut = array_merge($maOut,parent::collect());
		foreach ($this->caContent as $moView) {
			if ($moView instanceof FWDCollectable) {
				$maItems = $moView->collect();
				if (count($maItems)>0)
				$maOut = array_merge($maOut,$maItems);
			}
			else
			$maOut[] = $moView;
		}
		return $maOut;
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDDrawGrid. Implementa o Draw da Classe FWDGrid.
 *
 * <p>Classe que implementa o Draw que ser� utilizado pela Grid para
 * desenhar cada um dos elementos "c�lulas" da Grid, permitindo que
 * pelo PHP seja possivel sobrescrever o Draw para uma ou mais colunas,
 * desenhando algum outro elemento nessas colunas.</p>
 * @package FWD5
 * @subpackage view
 */

class FWDDrawGrid
{

	/**
	 * �ndice da linha do elemento que ser� desenhado, utilizado para o efeito de "zebra da grid"
	 * @var integer
	 * @access protected
	 */
	protected $ciRowIndex;

	/**
	 * �ndice da Coluna do elemento que ser� desenhado, utilizado para sobrescrever o Draw do Item da Grid
	 * @var integer
	 * @access protected
	 */
	protected $ciColumnIndex;

	/**
	 * Objeto que ser� desenhado na c�lula da grid.
	 * @var FWD"Object"
	 * @access protected
	 */
	protected $coCellBox;

	/**
	 * Css class para a div que conter� a c�lula da grid
	 * @var string
	 * @access protected
	 */
	protected $csDivUnitClass="";
	 
	/**
	 * Array que cont�m os dados da linha correspondente ao cellUnit sendo renderizado.
	 * @var Array
	 * @access protected
	 */
	protected $caData = "";
	 
	/**
	 * Define se deve ser exibido um tooltip com as informa��es da c�lula
	 * @var boolean
	 * @access protected
	 */
	protected $cbToolTip = false;

	/**
	 * Define o id da coluna da grid que ser� o source do Tooltip da grid
	 * @var boolean
	 * @access protected
	 */
	protected $ciTTSource;
	 
	/**
	 * Cont�m a string dos eventos de click da linha da grid
	 * @var boolean
	 * @access protected
	 */
	protected $bpRowEventBlock = false;
	 
	/**
	 * Array associativo com os alias das colunas da grid
	 * @var Array
	 * @access protected
	 */
	protected $caAlias = array();

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDDrawGrid.</p>
	 * @access public
	 * @param FWDBox $poBox Box que n�o � utilizado nesse contrutor, mantido aqui s� por padr�o
	 */
	public function __construct($poBox = null)
	{

	}

	/**
	 * Desenha o Item da FWDDrawGrid.
	 *
	 * <p>M�todo para desenhar o Item que j� foi setado no objeto da FWDDrawGrid. Este � o
	 * m�todo que � sobrescrito para desenhar em uma ou mais colunas um outro objeto, diferente
	 *  dos objetos das outras colunas da grid</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do objeto que ser� desenhado
	 */
	public function drawItem($poBox = null)
	{
		return $this->coCellBox->drawHeader($poBox) . $this->coCellBox->drawBody($poBox) .$this->coCellBox->drawFooter($poBox);
	}


	/**
	 * Desenha o Item da FWDDrawGrid.
	 *
	 * <p>M�todo utilizado para ajustar os valores da Div que que limitar� o desenho do Item da grid.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do objeto que ser� desenhado
	 * @param string $clickLine Linha selecionada
	 */
	public function draw($poBox = null,$psCellName,$msclickLine = "")
	{
		if($poBox)
		{
			$moOriginalBox = clone $poBox ;
			$msOriginalClass = $this->coCellBox->getAttrClass();
			$miLeft=$poBox->getAttrLeft();
			$miWidth=$poBox->getAttrWidth();
			$miHeight=$poBox->getAttrHeight();
			$miTop=$poBox->getAttrTop();

			$moAuxBox=$this->coCellBox->getObjFWDBox();
			$moAuxBox->setAttrLeft(0);
			$moAuxBox->setAttrTop(0);
			$moAuxBox->setAttrWidth($poBox->getAttrWidth());
			$moAuxBox->setAttrHeight($miHeight);
			$this->coCellBox->setObjFWDBox($moAuxBox);

			$msAuxReturn = $this->drawItem($poBox);

			$msRenderEventClickLine = "";
			if(!$this->bpRowEventBlock){
				$moEvent = new FWDClientEvent();
				$moEvent->setAttrEvent('onClick');
				$moEvent->setAttrValue($msclickLine);
				$maOtherEvents = $this->coCellBox->getArrayEvent();
				$this->coCellBox->cleanEvents();
				$this->coCellBox->addObjFWDEvent($moEvent);
				foreach($maOtherEvents as $moObjEventHandler){
					foreach($moObjEventHandler->getAttrContent() as $moObjEvent){
						$this->coCellBox->addObjFWDEvent($moObjEvent);
					}
				}
			}

			$moToolTipSt = null;
			if ($this->cbToolTip){
				if($this->ciTTSource == $this->ciColumnIndex){
					$msToolTipText = strip_tags($this->coCellBox->getObjFWDString()->getAttrString());
				}
				else
				$msToolTipText = $this->caData[$this->ciTTSource];

				$msToolTipText = trim($msToolTipText);
				if($msToolTipText!=''){
					$moToolTip = new FWDToolTip();
					$moToolTip->setAttrShowDelay(1000);
					//$msToolTipText = str_replace('\\','\\\\',$msToolTipText);
					//$msToolTipText = str_replace('"','\"',$msToolTipText);
					$moStr = new FWDString();
					$moStr->setAttrValue($msToolTipText);
					$moToolTip->setObjFWDString($moStr);
					$this->coCellBox->addObjFWDEvent($moToolTip);
				}
			}

			$msToolTipCode = '';
			$moToolTipSt = $this->coCellBox->getObjFWDToolTip();
			if($moToolTipSt){
				$msToolTipCode = $moToolTipSt->render();
			}
			$msStaticEvent = $this->coCellBox->getEvents();
			$msReturn =" <div id='$psCellName' class='{$this->csDivUnitClass}' {$msStaticEvent} {$msToolTipCode} style='height:{$miHeight};width:{$miWidth};left:{$miLeft};top:{$miTop};'>{$msAuxReturn}</div>";

			$this->coCellBox->setObjFWDBox($moOriginalBox);
			$this->coCellBox->setAttrClass($msOriginalClass);
			$this->bpRowEventBlock=false;
		}
		else
		{
			$msReturn = $this->drawItem($poBox);
		}
		return $msReturn;
	}

	/**
	 * Seta o Item que ser� desenhado na grid.
	 *
	 * <p>M�todo utilizado para setar na FWDDrawGrid o Objeto que ser� desenhado na grid e a linha correspondente desse objeto na gri.</p>
	 * @access public
	 * @param integer $piRowIndex que define a linha que pertence o objeto que ser� desenhado na grid
	 * @param FWD"Object" $poCellBox Objeto que ser� desenhado em uma c�lula da grid
	 */
	function setItem($piRowIndex,$piColumnIndex,$poCellBox,$poDivUnitClass,$paData,$piToolTip,$piSourceTT,$paAlias)
	{
		$this->cbToolTip   = $piToolTip;
		$this->ciRowIndex   = $piRowIndex;
		$this->coCellBox   = $poCellBox;
		$this->ciColumnIndex = $piColumnIndex;
		$this->csDivUnitClass = $poDivUnitClass;
		$this->caData = $paData;
		$this->ciTTSource = $piSourceTT;
		$this->caAlias = $paAlias;
	}

	/**
	 * Retorna o indice da coluna da grid correspondente ao alias passado.
	 *
	 * <p>Retorna o indice da coluna da grid correspondente ao alias passado.</p>
	 * @access public
	 * @param string $psAlias alias correspondente a coluna da grid que se quer descobrir o indice
	 * @return mixed index da coluna da grid correspondente ao alias passado para a fun��o
	 */
	public function getIndexByAlias($psAlias){
		$mxReturn = '';
		if($this->caAlias[$psAlias]){
			$mxReturn = $this->caAlias[$psAlias];
		}else{
			trigger_error("Trying to access an exist column index!",E_USER_ERROR);
		}
		return $mxReturn;
	}

	/**
	 * Retorna o alias da coluna da grid correspondente ao �ndice passado.
	 *
	 * <p>Retorna o alias da coluna da grid correspondente ao �ndice passado.</p>
	 * @access public
	 * @param integer $piIndex �ndice da coluna
	 * @return string Alias da coluna
	 */
	public function getAliasByIndex($piIndex){
		$mmAlias = array_search($piIndex,$this->caAlias);
		if($mmAlias===false){
			trigger_error("Inexistent column index '$piIndex'.",E_USER_ERROR);
		}
		return $mmAlias;
	}

	/**
	 * retorna o valor da coluna correspondende ao alias passapo para a fun��o
	 *
	 * <p>retorna o valor da coluna correspondende ao alias passapo para a fun��o.</p>
	 * @access public
	 * @param string $psAlias alias da coluna da grid que se quer obter o valor retornado pela query
	 * @return mixed index da coluna da grid correspondente ao alias passado para a fun��o
	 */
	public function getFieldValue($psAlias){
		$mxReturn = '';
		if(isset($this->caAlias[$psAlias])){
			if(isset($this->caData[$this->caAlias[$psAlias]])){
				$mxReturn = $this->caData[$this->caAlias[$psAlias]];
			}
		}else{
			trigger_error("Trying to access an inexistent alias ('$psAlias').",E_USER_ERROR);
		}
		return $mxReturn;
	}
}

?><?
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDGetParameter.
 *
 * <p>Classe utilizada para receber, via xml, par�metros do GET.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDGetParameter {

	/**
	 * Nome do par�metro
	 * @var string
	 * @access protected
	 */
	protected $csParameter;

	/**
	 * Valor do par�metro
	 * @var string
	 * @access protected
	 */
	protected $csValue;

	/**
	 * Nome do objeto
	 * @var string
	 * @access protected
	 */
	protected $csName;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDParamenter.</p>
	 * @access public
	 */
	public function __construct() {}

	/**
	 * Atribui o nome do par�metro.
	 *
	 * @access public
	 * @param string $psParameter Nome do parametro
	 */
	public function setAttrParameter($psParameter) {
		$this->csParameter = $psParameter;
	}

	/**
	 * Atribui valor � vari�vel value.
	 *
	 * <p>Atribui valor � vari�vel value, devendo retirar espa�os em branco
	 * no inicio e/ou no fim da string.</p>
	 * @access public
	 * @param string $psValue Valor a ser atribu�do
	 */
	public function setValue($psValue) {
		$this->csValue = trim($psValue);
	}

	/**
	 * Atribui nome ao objeto.
	 *
	 * @access public
	 * @param string $psName Nome do objeto
	 */
	public function setAttrName($psName) {
		$this->csName = $psName;
	}

	/**
	 * Retorna o valor da vari�vel parameter.
	 *
	 * @access public
	 * @return string Nome do par�metro
	 */
	public function getAttrParameter() {
		return $this->csParameter;
	}

	/**
	 * Retorna o valor da vari�vel value.
	 *
	 * @access public
	 * @return string Valor do objeto
	 */
	public function getValue() {
		return $this->csValue;
	}

	/**
	 * Retorna o nome do objeto.
	 *
	 * @access public
	 * @return string Nome do objeto
	 */
	public function getAttrName() {
		return $this->csName;
	}

	/**
	 * Pega o valor do par�metro passado por GET.
	 *
	 * @access public
	 */
	public function execute() {
		isset($_GET[$this->getAttrParameter()]) ? $this->setValue($_GET[$this->getAttrParameter()]) : "";
	}
}
?><?
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDSessionParameter.
 *
 * <p>Classe utilizada para receber, via xml, par�metros pela sess�o.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDSessionParameter {

	/**
	 * Id da sess�o
	 * @var string
	 * @access protected
	 */
	protected $csSessionId = "";

	/**
	 * Nome do atributo
	 * @var string
	 * @access protected
	 */
	protected $csAttribute = "";

	/**
	 * Valor do atributo
	 * @var string
	 * @access protected
	 */
	protected $csValue = "";

	/**
	 * Nome do objeto
	 * @var string
	 * @access protected
	 */
	protected $csName = "";

	/**
	 * Autoclean
	 * @var boolean
	 * @access protected
	 */
	protected $cbAutoClean = false;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDParamenter.</p>
	 * @access public
	 */
	public function __construct() {}

	/**
	 * Atribui o nome do par�metro.
	 *
	 * @access public
	 * @param string $psAttribute Nome do atributo
	 */
	public function setAttrAttribute($psAttribute) {
		$this->csAttribute = $psAttribute;
	}

	/**
	 * Seta o id da sess�o.
	 *
	 * @access public
	 * @param string $psSessionId Id da sess�o
	 */
	public function setAttrSessionId($psSessionId) {
		$this->csSessionId = $psSessionId;
	}

	/**
	 * Seta o atributo autoclean. Ou seja, limpa o valor do atributo ap�s utiliz�-lo.
	 *
	 * @access public
	 * @param string $pbAutoClean Nome do atributo
	 */
	public function setAttrAutoClean($pbAutoClean) {
		$this->cbAutoClean = FWDWebLib::attributeParser($pbAutoClean,array("true"=>true, "false"=>false),"pbAutoClean");
	}

	/**
	 * Atribui valor � vari�vel value.
	 *
	 * <p>Atribui valor � vari�vel value, devendo retirar espa�os em branco
	 * no inicio e/ou no fim da string.</p>
	 * @access public
	 * @param string $psValue Valor a ser atribu�do
	 */
	public function setValue($psValue) {
		$this->csValue = trim($psValue);
	}

	/**
	 * Atribui nome ao objeto.
	 *
	 * @access public
	 * @param string $psName Nome do objeto
	 */
	public function setAttrName($psName) {
		$this->csName = $psName;
	}

	/**
	 * Retorna o nome do atributo.
	 *
	 * @access public
	 * @return string Nome do par�metro
	 */
	public function getAttrAttribute() {
		return $this->csAttribute;
	}

	/**
	 * Retorna o id da sess�o.
	 *
	 * @access public
	 * @return string Id da sess�o
	 */
	public function getAttrSessionId() {
		return $this->csSessionId;
	}

	/**
	 * Retorna o valor da vari�vel value.
	 *
	 * @access public
	 * @return string Valor do objeto
	 */
	public function getValue() {
		return $this->csValue;
	}

	/**
	 * Retorna o nome do objeto.
	 *
	 * @access public
	 * @return string Nome do objeto
	 */
	public function getAttrName() {
		return $this->csName;
	}

	/**
	 * Retorna o estado do atributo autoclean.
	 *
	 * @access public
	 * @param string $pbAutoClean Nome do atributo
	 */
	public function getAttrAutoClean() {
		return $this->cbAutoClean;
	}

	/**
	 * Pega o valor do atributo passado por sess�o.
	 *
	 * @access public
	 */
	public function execute() {
		$moSession = FWDWebLib::getInstance()->getSessionById($this->csSessionId);
		if ($moSession->attributeExists($this->csAttribute)) {
			$msGetFunction = "getAttr" . $this->csAttribute;
			$this->csValue = $moSession->{$msGetFunction}();
			if ($this->cbAutoClean) $moSession->deleteAttribute($this->csAttribute);
		}
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDNodeBuilder. Classe que constr�i nodos.
 *
 * <p>Classe, usada pela DBTree, que constr�i nodos a partir de um id e um
 * valor obtidos atrav�s do DataSet.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDNodeBuilder {

	/**
	 * Recebe um id e um valor e constr�i um nodo
	 *
	 * <p>Recebe um id e um valor e constr�i um nodo (objeto FWDTree).</p>
	 * @access public
	 * @param string $psId Identificador
	 * @param string $psValue Valor
	 * @return FWDTree Nodo construido
	 */
	public function buildNode($psId, $psValue){
		$moNode = new FWDTree();
		$moNode->setAttrName($psId);
		$moStatic = new FWDStatic();
		$moStatic->setValue($psValue);
		$moNode->addObjFWDView($moStatic);
		return $moNode;
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDDBTreeBase. �rvore populada a partir do banco
 *
 * <p>Classe que implementa uma �rvore populada a partir do banco. Uma
 * DBTreeBase � sempre a raiz da �rvore e n�o pode ter filhos definidos no XML.
 * OBS: As defini��es das classes FWDDBTreeBase e FWDDBTree s�o praticamente
 * id�nticas, exceto que elas extendem classes diferentes, ent�o se uma
 * modifica��o for feita numa das duas classes, provavelmente, a modifica��o
 * deve ser replicada na outra.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDDBTreeBase extends FWDTreeBase {

	/**
	 * DataSet usado para construir a �rvore
	 * @var FWDDBDataSet
	 * @access protected
	 */
	protected $coDataSet = null;

	/**
	 * Objeto que constr�i os nodos da DbTree
	 * @var FWDNodeBuilder
	 * @access protected
	 */
	protected $coNodeBuilder = null;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDDBTreeBase.</p>
	 * @access public
	 */
	public function __construct(){
		parent::__construct();
		$this->coNodeBuilder = new FWDNodeBuilder();
	}

	/**
	 * Seta o DataSet
	 *
	 * <p>Seta o DataSet respons�vel pelo conte�do da �rvore.</p>
	 * @access public
	 * @param FWDDBDataSet $poDataSet Objeto DBDataSet
	 */
	public function setDataSet(FWDDBDataSet $poDataSet){
		$this->coDataSet = $poDataSet;
	}

	/**
	 * Retorna o DataSet
	 *
	 * <p>Retorna o DataSet respons�vel pelo conte�do da �rvore.</p>
	 * @access public
	 * @return FWDDBDataSet Objeto DataSet
	 */
	public function getDataSet(){
		return $this->coDataSet;
	}

	/**
	 * Seta o NodeBuilder
	 *
	 * <p>Seta o NodeBuilder que constr�i os nodos da DbTree.</p>
	 * @access public
	 * @param FWDNodeBuilder $poNodeBuilder Objeto DBNodeBuilder
	 */
	public function setNodeBuilder(FWDNodeBuilder $poNodeBuilder){
		$this->coNodeBuilder = $poNodeBuilder;
	}

	/**
	 * Retorna o NodeBuilder
	 *
	 * <p>Retorna o NodeBuilder que constr�i os nodos da DbTree.</p>
	 * @access public
	 * @return FWDNodeBuilder Objeto NodeBuilder
	 */
	public function getNodeBuilder(){
		return $this->coNodeBuilder;
	}

	/**
	 * Constr�i a �rvore
	 *
	 * <p>Monta a �rvore a partir do DataSet.</p>
	 * @access public
	 * @return boolean True, caso a opera��o seja bem sucedida
	 */
	public function buildTree(){
		if($this->coDataSet==null){
			trigger_error("FWDDBTreeBase object must have a NodeBuilder to be built.",E_USER_WARNING);
			return false;
		}else{
			$moDataSet = $this->coDataSet;
			$moDataSet->execute();
			$moNodeBuilder = $this->coNodeBuilder;
			$maCurrent = array($this);
			while($moDataSet->fetch()){
				$msId = $moDataSet->getFieldByAlias('id')->getValue();
				$miLevel = $moDataSet->getFieldByAlias('node_level')->getValue();
				$msValue = $moDataSet->getFieldByAlias('node_value')->getValue();
				$moNode = $moNodeBuilder->buildNode($msId,$msValue);
				$maCurrent[$miLevel]->addObjFwdTree($moNode);
				$maCurrent[$miLevel+1] = $moNode;
			}
			$this->setTreeBaseName();
			$this->setIds();
			return true;
		}
	}

	/**
	 * Seta o nome da TreeBase de todos descendentes
	 *
	 * <p>Seta o nome da TreeBase de todos descendentes.</p>
	 * @access public
	 * @param string $psTreeBaseName Nome da TreeBase a que a Tree pertence
	 */
	public function setTreeBaseName($psTreeBaseName=''){
		$msTreeBaseName = $psTreeBaseName;

		if(!$msTreeBaseName) $msTreeBaseName = $this->getAttrName();
		$this->csTreeBaseName = $msTreeBaseName;
		if(count($this->caNodes)>0){
			foreach($this->caNodes as $moNode){
				$moNode->setTreeBaseName($msTreeBaseName);
			}
		}
	}

}

?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDDBTree. Nodo de �rvore populada a partir do banco
 *
 * <p>Classe que implementa uma �rvore populada a partir do banco. Uma
 * DBTree n�o pode ser a raiz da �rvore e n�o pode ter filhos definidos no XML.
 * OBS: As defini��es das classes FWDDBTreeBase e FWDDBTree s�o praticamente
 * id�nticas, exceto que elas extendem classes diferentes, ent�o se uma
 * modifica��o for feita numa das duas classes, provavelmente, a modifica��o
 * deve ser replicada na outra.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDDBTree extends FWDTree {

	/**
	 * DataSet usado para construir a �rvore
	 * @var FWDDBDataSet
	 * @access protected
	 */
	protected $coDataSet = null;

	/**
	 * Objeto que constr�i os nodos da DbTree
	 * @var FWDNodeBuilder
	 * @access protected
	 */
	protected $coNodeBuilder = null;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDDBTree.</p>
	 * @access public
	 */
	public function __construct(){
		parent::__construct();
		$this->coNodeBuilder = new FWDNodeBuilder();
	}

	/**
	 * Seta o DataSet
	 *
	 * <p>Seta o DataSet respons�vel pelo conte�do da �rvore.</p>
	 * @access public
	 * @param FWDDBDataSet $poDataSet Objeto DBDataSet
	 */
	public function setDataSet(FWDDBDataSet $poDataSet){
		$this->coDataSet = $poDataSet;
	}

	/**
	 * Retorna o DataSet
	 *
	 * <p>Retorna o DataSet respons�vel pelo conte�do da �rvore.</p>
	 * @access public
	 * @return FWDDBDataSet Objeto DataSet
	 */
	public function getDataSet(){
		return $this->coDataSet;
	}

	/**
	 * Seta o NodeBuilder
	 *
	 * <p>Seta o NodeBuilder que constr�i os nodos da DbTree.</p>
	 * @access public
	 * @param FWDNodeBuilder $poNodeBuilder Objeto DBNodeBuilder
	 */
	public function setNodeBuilder(FWDNodeBuilder $poNodeBuilder){
		$this->coNodeBuilder = $poNodeBuilder;
	}

	/**
	 * Retorna o NodeBuilder
	 *
	 * <p>Retorna o NodeBuilder que constr�i os nodos da DbTree.</p>
	 * @access public
	 * @return FWDNodeBuilder Objeto NodeBuilder
	 */
	public function getNodeBuilder(){
		return $this->coNodeBuilder;
	}

	/**
	 * Constr�i a �rvore
	 *
	 * <p>Monta a �rvore a partir do DataSet.</p>
	 * @access public
	 * @return boolean True, caso a opera��o seja bem sucedida
	 */
	public function buildTree(){
		if($this->coDataSet==null){
			trigger_error("FWDDBTree object must have a NodeBuilder to be built.",E_USER_WARNING);
			return false;
		}else{
			$moDataSet = $this->coDataSet;
			$moDataSet->execute();
			$moNodeBuilder = $this->coNodeBuilder;
			$maCurrent = array($this);
			while($moDataSet->fetch()){
				$msId = $moDataSet->getFieldByAlias('id')->getValue();
				$miLevel = $moDataSet->getFieldByAlias('node_level')->getValue();
				$msValue = $moDataSet->getFieldByAlias('node_value')->getValue();
				if (isset($maCurrent[$miLevel]) || $miLevel == 0) {
					$moNode = $moNodeBuilder->buildNode($msId,$msValue);
					$maCurrent[$miLevel]->addObjFwdTree($moNode);
					$maCurrent[$miLevel+1] = $moNode;
				}
			}
			$this->setTreeBaseName();
			$this->setIds();
			return true;
		}
	}

	/**
	 * Seta o nome da TreeBase de todos descendentes
	 *
	 * <p>Seta o nome da TreeBase de todos descendentes.</p>
	 * @access public
	 * @param string $psTreeBaseName Nome da TreeBase a que a Tree pertence
	 */
	public function setTreeBaseName($psTreeBaseName=''){
		$msTreeBaseName = $psTreeBaseName;
		if(!$msTreeBaseName) $msTreeBaseName = $this->getTreeBaseName();
		if(!$msTreeBaseName) $msTreeBaseName = $this->getAttrName();
		$this->csTreeBaseName = $msTreeBaseName;
		if(count($this->caNodes)>0){
			foreach($this->caNodes as $moNode){
				$moNode->setTreeBaseName($msTreeBaseName);
			}
		}
	}

}

?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDSelectQuery. Implementa Querys para popular elementos FWDDBSelect.
 *
 * <p>Classe que implementa Querys para popular elementos FWDDBSelect com
 * informa��es do BD.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDSelectQuery extends FWDDBDataSet {

	/**
	 * Id do Item do Select ('id' a ser retornado pela Query)
	 * @var string
	 * @access private
	 */
	protected $csId = "";

	/**
	 * Nome do Item do Select ('value' a ser retornado pela Query)
	 * @var string
	 * @access private
	 */
	protected $csValue = "";

	/**
	 * Nome da tabela do banco em que encontram-se as colunas 'id' e 'value'
	 * @var string
	 * @access private
	 */
	protected $csTable = "";

	/**
	 * Cria a Query.
	 *
	 * <p>M�todo para criar a Query.</p>
	 * @access public
	 */
	public function execute() {
		$this->addFWDDBField(new FWDDBField($this->getId(), "select_id", DB_NUMBER));
		$this->addFWDDBField(new FWDDBField($this->getValue(), "select_value", DB_STRING));
		$this->setQuery("SELECT ".$this->getId()." as select_id, ".$this->getValue()." as select_value FROM ".$this->getTable());
		parent::execute();
	}

	/**
	 * Seta o Id da Query.
	 *
	 * <p>M�todo para setar o Id da Query.</p>
	 * @access public
	 * @param string $psId Id a ser retornado pela Query
	 */
	public function setId($psId) {
		$this->csId = $psId;
	}

	/**
	 * Retorna o Id da Query.
	 *
	 * <p>M�todo para retornar o Id da Query.</p>
	 * @access public
	 * @return string Id a ser retornado pela Query
	 */
	public function getId() {
		return $this->csId;
	}

	/**
	 * Seta o Value da Query.
	 *
	 * <p>M�todo para setar o Value da Query.</p>
	 * @access public
	 * @param string $psValue Value a ser retornado pela Query
	 */
	public function setValue($psValue) {
		$this->csValue = $psValue;
	}

	/**
	 * Retorna o Value da Query.
	 *
	 * <p>M�todo para retornar o Value da Query.</p>
	 * @access public
	 * @return string Value a ser retornado pela Query
	 */
	public function getValue() {
		return $this->csValue;
	}

	/**
	 * Seta o nome da tabela na qual ser� efetuada a Query.
	 *
	 * <p>M�todo para setar o nome da tabela na qual ser� efetuada a Query.</p>
	 * @access public
	 * @param string $psTable Nome da Tabela onde ser� efetuada a Query
	 */
	public function setTable($psTable) {
		$this->csTable = $psTable;
	}

	/**
	 * Retorna o nome da tabela na qual ser� efetuada a Query.
	 *
	 * <p>M�todo para retornar o nome da tabela na qual ser� efetuada a Query.</p>
	 * @access public
	 * @return string Nome da Tabela onde ser� efetuada a Query
	 */
	public function getTable() {
		return $this->csTable;
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDFile. Implementa campos de upload de arquivos
 *
 * <p>Classe que implementa campos de upload de arquivos (html tag 'input type=file').</p>
 * @package FWD5
 * @subpackage view
 */
class FWDFile extends FWDView {

	/**
	 * C�digo de erro que indica a aus�ncia de erros
	 * @const integer
	 * @access public
	 */
	const E_NONE = 0;

	/**
	 * C�digo de erro que indica que o tamanho do arquivo excede o tamanho m�ximo
	 * @const integer
	 * @access public
	 */
	const E_MAX_SIZE = 1;

	/**
	 * C�digo de erro que indica que a extens�o do arquivo n�o est� entre as
	 * extens�es permitidas
	 * @const integer
	 * @access public
	 */
	const E_EXTENSION = 2;

	/**
	 * C�digo de erro que indica que o upload s� foi feito parcialmente
	 * @const integer
	 * @access public
	 */
	const E_PARTIAL = 4;

	/**
	 * C�digo de erro que indica que n�o existe a pasta tempor�ria
	 * @const integer
	 * @access public
	 */
	const E_NO_TMP_DIR = 8;

	/**
	 * C�digo de erro que indica falha ao tentar escrever no disco
	 * @const integer
	 * @access public
	 */
	const E_CANT_WRITE = 16;

	/**
	 * Define se o campo est� desabilitado ou n�o
	 * @var boolean
	 * @access protected
	 */
	protected $cbDisabled = false;

	/**
	 * Nome do label associado ao objeto
	 * @var string
	 * @access protected
	 */
	protected $csLabel = '';

	/**
	 * Tamanho m�ximo do arquivo em bytes
	 * @var integer
	 * @access protected
	 */
	protected $ciMaxFileSize = 300000000;

	/**
	 * Array de extens�es permitidas
	 * @var array
	 * @access protected
	 */
	protected $caAllowedExts = array();

	/**
	 * C�digo de erro. String de bits, cada bit ligado indica um erro diferente.
	 * @var integer
	 */
	protected $ciErrorCode;

	/**
	 * Nome do arquivo tempor�rio
	 * @var string
	 * @access protected
	 */
	protected $csTempFileName = '';

	/**
	 * Nome do arquivo original
	 * @var string
	 * @access protected
	 */
	protected $csFileName = '';

	/**
	 * Tamanho do arquivo em bytes
	 * @var string
	 * @access protected
	 */
	protected $ciFileSize = '';

	/**
	 * Indica se foi feito o upload de um arquivo
	 * @var boolean
	 * @access protected
	 */
	protected $cbUploaded = false;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDFile.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do campo de texto
	 */
	public function __construct($poBox = null){
		parent::__construct($poBox);
		$this->ciErrorCode = FWDFile::E_NONE;
	}

	/**
	 * Desenha o campo de texto.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML,
	 * todas as informa��es necess�rias para implementar o campo de texto.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do campo de texto
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawBody($poBox = null){
		if($this->getAttrAlign()) $this->alignView($this->getAttrAlign(),$poBox);
		$msStyle = "style='{$this->getStyle()}'";
		$msDisabled = ($this->cbDisabled?'disabled':'');
		$msValue = ($this->getValue()?"value='{$this->getValue()}'":"");
		$msLabel = ($this->csLabel?"label='{$this->csLabel}'":'');
		$msOut = "<input type='hidden' name='MAX_FILE_SIZE' value='{$this->ciMaxFileSize}'/>\n";
		$msOut.= "<input type='file' {$this->getGlobalProperty()} $msStyle $msLabel {$this->getEvents()} {$msDisabled} {$msValue}/>\n";
		$msOut.= '<iframe style="display:none" name="'.$this->csName.'_iframe" id="'.$this->csName.'_iframe"></iframe>';
		return $msOut;
	}

	public function drawFooter($poBox = null){
		return "<script language='javascript'>new FWDFile('{$this->csName}');</script>";
	}

	/**
	 * Atribui valor � vari�vel value.
	 *
	 * <p>Atribui valor � vari�vel value, devendo retirar espa�os em branco
	 * no inicio e/ou no fim da string.</p>
	 * @access public
	 * @param string $psValue Valor a ser atribu�do
	 */
	public function setAttrValue($psValue){
		$this->setValue(trim($psValue));
	}

	/**
	 * Seta o nome do Label do objeto.
	 *
	 * <p>M�todo para setar o nome do label do objeto, utilizado para a troca de cor, no caso do objeto ter
	 * preenchimento obrigat�rio e n�o estar preenchido.</p>
	 * @access public
	 * @param string $psLabel nome do label associado ao objeto
	 */
	public function setAttrLabel($psLabel){
		$this->csLabel = $psLabel;
	}

	/**
	 * Seta o campo como desabilitado.
	 *
	 * <p>M�todo para setar o campo como desabilitado.</p>
	 * @access public
	 * @param boolean $pbDisabled Define se o campo est� desabilitado ou n�o
	 */
	public function setAttrDisabled($pbDisabled){
		$this->cbDisabled = FWDWebLib::attributeParser($pbDisabled,array("true"=>true, "false"=>false),"pbDisabled");
	}

	/**
	 * Seta o tamanho m�ximo do arquivo
	 *
	 * <p>Seta o tamanho m�ximo do arquivo em bytes.</p>
	 * @access public
	 * @param integer $piMaxFileSize Tamanho m�ximo do arquivo
	 */
	public function setAttrMaxFileSize($piMaxFileSize){
		if($piMaxFileSize > 0){
			$this->ciMaxFileSize = $piMaxFileSize;
		}else{
			trigger_error("Invalid maxfilesize value: '{$piMaxFileSize}'",E_USER_WARNING);
		}
	}

	/**
	 * Seta o atributo allowedexts
	 *
	 * <p>Seta o atributo allowedexts, que define as extens�es permitidas.</p>
	 * @access public
	 * @param string $psExtensions Extens�es permitidas separadas por ':'
	 */
	public function setAttrAllowedExts($psExtensions){
		if($psExtensions==''){
			$this->caAllowedExts = array();
		}else{
			$this->caAllowedExts = explode(':',strtolower($psExtensions));
		}
	}

	/**
	 * Testa se a extens�o do arquivo � v�lida
	 *
	 * <p>Testa se a extens�o do arquivo � v�lida e seta o c�digo de erro. Se um
	 * array de extens�es � passado como par�metro, testa se a extens�o pertence a
	 * esse array, sen�o testa se pertence ao array de extens�es previamente
	 * definido.</p>
	 * @access public
	 * @param array $paExtensions Array com as extens�es permitidas (opcional)
	 * @return boolean Indica se a extens�o � v�lida
	 */
	public function testExtension($paExtensions=null){
		if($paExtensions!=null) $this->caAllowedExts = $paExtensions;
		if(count($this->caAllowedExts)==0){ // Permite todas extens�es
			$mbReturn = true;
		}else{
			$mbReturn = in_array(strtolower($this->getFileExt()),$this->caAllowedExts);
		}
		if($mbReturn){
			$this->ciErrorCode&=~FWDFile::E_EXTENSION;
		}else{
			$this->ciErrorCode|= FWDFile::E_EXTENSION;
		}
		return $mbReturn;
	}

	/**
	 * Retorna o valor booleano do atributo Disabled.
	 *
	 * <p>Retorna o valor booleano TRUE se o campo est� desabilitado.
	 * Retorna FALSE caso contr�rio.</p>
	 * @access public
	 * @return boolean Indica se o campo est� desabilitado ou n�o
	 */
	public function getAttrDisabled(){
		return $this->cbDisabled;
	}

	/**
	 * Retorna o tamanho m�ximo do arquivo
	 *
	 * <p>Retorna o tamanho m�ximo do arquivo em bytes.</p>
	 * @access public
	 * @return integer Tamanho m�ximo do arquivo
	 */
	public function getAttrMaxFileSize(){
		return $this->ciMaxFileSize;
	}

	/**
	 * Retorna o nome do arquivo tempor�rio
	 *
	 * <p>Retorna o nome do arquivo tempor�rio.</p>
	 * @access public
	 * @return string Nome do arquivo tempor�rio
	 */
	public function getTempFileName(){
		return $this->csTempFileName;
	}

	/**
	 * Retorna o nome do arquivo original
	 *
	 * <p>Retorna o nome do arquivo original.</p>
	 * @access public
	 * @return string Nome do arquivo original
	 */
	public function getFileName(){
		return $this->csFileName;
	}

	/**
	 * Retorna o tamanho do arquivo
	 *
	 * <p>Retorna o tamanho do arquivo em bytes.</p>
	 * @access public
	 * @return integer Tamanho do arquivo
	 */
	public function getFileSize(){
		return $this->ciFileSize;
	}

	/**
	 * Retorna a extens�o do arquivo
	 *
	 * <p>Retorna a extens�o do arquivo.</p>
	 * @access public
	 * @return string Extens�o do arquivo
	 */
	public function getFileExt(){
		return substr($this->csFileName, strrpos($this->csFileName,'.')+1);
	}

	/**
	 * Indica se foi feito o upload de um arquivo
	 *
	 * <p>Indica se foi feito o upload de um arquivo.</p>
	 * @access public
	 * @return boolean True indica que um arquivo foi enviado com sucesso
	 */
	public function isUploaded(){
		return $this->cbUploaded;
	}

	/**
	 * Copia o arquivo
	 *
	 * <p>Copia o arquivo tempor�rio para o local indicado com o nome indicado.</p>
	 * @access public
	 * @param string $psPath Caminho do diret�rio destino
	 * @param string $psName Nome do arquivo
	 * @return boolean True, indica sucesso da opera��o
	 */
	public function copyTo($psPath,$psName){
		$msFullPath = "$psPath/$psName";
		if($this->cbUploaded && $this->ciErrorCode==FWDFile::E_NONE){
			if(move_uploaded_file($this->csTempFileName,$msFullPath)){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	/**
	 * Retorna o c�digo de erro
	 *
	 * <p>Retorna o c�digo de erro.</p>
	 * @access public
	 * @return integer C�digo de erro
	 */
	public function getErrorCode(){
		return $this->ciErrorCode;
	}

	/**
	 * Se foi feito o upload de um arquivo, pega suas informa��es
	 *
	 * <p>Se foi feito o upload de um arquivo, pega suas informa��es. M�todo
	 * executado quando o objeto acaba de ser carregado.</p>
	 * @access public
	 */
	public function execute(){
		if(isset($_FILES[$this->getAttrName()]) && $_FILES[$this->getAttrName()]['error']!=UPLOAD_ERR_NO_FILE){
			$this->cbUploaded = true;
			$this->ciErrorCode = FWDFile::E_NONE;
			$maFileInfo = $_FILES[$this->getAttrName()];
			$this->csFileName = $maFileInfo['name'];
			$this->csTempFileName = $maFileInfo['tmp_name'];
			$this->ciFileSize = $maFileInfo['size'];
			$this->testExtension();
			if($this->ciFileSize > $this->ciMaxFileSize || $maFileInfo['error']==UPLOAD_ERR_INI_SIZE || $maFileInfo['error']==UPLOAD_ERR_FORM_SIZE){
				$this->ciErrorCode|= FWDFile::E_MAX_SIZE;
			}
			switch($maFileInfo['error']){
				case UPLOAD_ERR_PARTIAL   : $this->ciErrorCode|= FWDFile::E_PARTIAL;    break;
				case UPLOAD_ERR_NO_TMP_DIR: $this->ciErrorCode|= FWDFile::E_NO_TMP_DIR; break;
				case UPLOAD_ERR_CANT_WRITE: $this->ciErrorCode|= FWDFile::E_CANT_WRITE; break;
			}
		}else{
			$this->cbUploaded = false;
		}
	}

}

?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDScrolling. Classe que implementa rolagem de texto sem barras de
 * rolagem.
 *
 * <p>Classe que implementa rolagem de texto sem barras de rolagem, o conte�do
 * do elemento rola quando o cursor do mouse � posicionado sobre uma das
 * setas.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDScrolling extends FWDView {

	/**
	 * Conte�do do Scrolling (� o Static que rola)
	 * @var FWDStatic
	 * @access protected
	 */
	protected $coContent = null;

	/**
	 * Indica a orienta��o da rolagem (vertical ou horizontal)
	 * @var string
	 * @access protected
	 */
	protected $csOrientation = 'horizontal';

	/**
	 * Velocidade da rolagem em pixels por segundo
	 * @var integer
	 * @access protected
	 */
	protected $ciSpeed = 100;

	/**
	 * Tamanho da aresta das imagens das setas (necessariamente quadradas)
	 * @var integer
	 * @access protected
	 */
	protected $ciImageSize = 9;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDScrolling.</p>
	 * @access public
	 */
	public function __construct(){
		parent::__construct();
	}

	/**
	 * Desenha o cabe�alho do objeto.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o cabe�alho do objeto.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do objeto
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawHeader($poBox = null){
		$this->setAttrTabIndex(0);
		$msStyle = "style='{$this->getStyle()}'";
		$msAttrDefault = $this->getGlobalProperty();
		$msEvents = $this->getEvents();
		$msOut = "<div $msAttrDefault $msStyle $msEvents>\n";
		return $msOut;
	}

	/**
	 * Desenha o corpo do objeto.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o corpo do objeto.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do objeto
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawBody(){
		$msOut = '';
		$miHeight = $this->coBox->getAttrHeight();
		$miWidth = $this->coBox->getAttrWidth();
		$moWebLib = FWDWebLib::getInstance();
		$msGfxRef = $moWebLib->getGfxRef();
		$moContentBox = $this->coContent->getObjFWDBox();
		$this->coContent->setAttrPosition('relative');
		$moContentBox->setAttrTop(0);
		$moContentBox->setAttrLeft(0);
		if($this->csOrientation=='vertical'){
			$moContentBox->setAttrHeight('');
			$moContentBox->setAttrWidth($this->coBox->getAttrWidth());
			$msDir1 = 'up';
			$msDir2 = 'down';
			$miBarWidth = $miWidth;
			$miBarHeight = $this->ciImageSize;
			$miWindowH = $miHeight - 2*$miBarHeight;
			$miWindowW = $miWidth;
			$miWindowTop = $miBarHeight;
			$miWindowLeft = 0;
			$miImageTop = 0;
			$miImageLeft = ($miWidth - $this->ciImageSize)/2;
			$miBar2Top = $miWindowH + $miBarHeight;
			$miBar2Left = 0;
			$msContentCode = $this->coContent->draw();
		}else{
			$moContentBox->setAttrWidth('');
			$moContentBox->setAttrHeight($this->coBox->getAttrHeight());
			$msDir1 = 'left';
			$msDir2 = 'right';
			$miBarWidth = $this->ciImageSize;
			$miBarHeight = $miHeight;
			$miWindowH = $miHeight;
			$miWindowW = $miWidth - 2*$this->ciImageSize;
			if(FWDWebLib::browserIsIE())
			$miWindowTop = -2;
			else
			$miWindowTop = -1;
			 
			$miWindowLeft = $this->ciImageSize;
			$miImageTop = ($miHeight - $this->ciImageSize)/2;
			$miImageLeft = 0;
			$miBar2Top = 0;
			$miBar2Left = $miWindowW + $miBarWidth;
			$msContentCode = str_replace('<td ','<td nowrap="nowrap" ',$this->coContent->draw());
		}
		$msOut.= "
      <div style='position:absolute;top:0;left:0;width:$miBarWidth;height:$miBarHeight;'>
        <img src='$msGfxRef$msDir1.gif' onMouseOver='startScroll(\"{$this->csName}\",\"$msDir1\",{$this->ciSpeed})' onMouseOut='stopScroll()' style='position:absolute;top:$miImageTop;left:$miImageLeft;'>
      </div>
      <div style='position:absolute;top:$miBar2Top;left:$miBar2Left;width:$miBarWidth;height:$miBarHeight;'>
        <img src='$msGfxRef$msDir2.gif' onMouseOver='startScroll(\"{$this->csName}\",\"$msDir2\",{$this->ciSpeed})' onMouseOut='stopScroll()' style='position:absolute;top:$miImageTop;left:$miImageLeft;'>
      </div>
      <div id='{$this->csName}_window' style='position:absolute;top:$miWindowTop;left:$miWindowLeft;width:$miWindowW;height:$miWindowH;overflow:hidden;'>
        <div id='{$this->csName}_content' style='position:absolute;left:0px;top:0px;'>
        $msContentCode
        </div>
      </div>
    ";
        return $msOut;
	}

	/**
	 * Desenha o rodap� do objeto.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o rodap� do objeto.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do objeto
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawFooter(){
		return "\n</div>\n";
	}

	/**
	 * Seta o Static que ser� o conte�do do Scrolling.
	 *
	 * <p>Seta o Static que ser� o conte�do do Scrolling.</p>
	 * @access public
	 * @param FWDStatic $poStatic Static que ser� o conte�do do Scrolling
	 */
	public function setObjFWDStatic(FWDStatic $poStatic){
		$poStatic->setAttrClass('FWDStaticScrolling');
		$this->coContent = $poStatic;
	}

	/**
	 * Retorna o valor do atributo orientation
	 *
	 * <p>Retorna o valor do atributo orientation.</p>
	 * @access public
	 * @return string Valor do atributo orientation
	 */
	public function getAttrOrientation(){
		return $this->csOrientation;
	}

	/**
	 * Seta o valor do atributo orientation
	 *
	 * <p>Seta o valor do atributo orientation.</p>
	 * @access public
	 * @param string $psOrientation Novo valor do atributo
	 */
	public function setAttrOrientation($psOrientation){
		$this->csOrientation = FWDWebLib::attributeParser($psOrientation,array("vertical"=>"vertical", "horizontal"=>"horizontal"),"orientation");
	}

	/**
	 * Retorna o valor do atributo speed
	 *
	 * <p>Retorna o valor do atributo speed.</p>
	 * @access public
	 * @return integer Valor do atributo speed
	 */
	public function getAttrSpeed(){
		return $this->ciSpeed;
	}

	/**
	 * Seta o valor do atributo speed
	 *
	 * <p>Seta o valor do atributo speed.</p>
	 * @access public
	 * @param integer $piSpeed Novo valor do atributo
	 */
	public function setAttrSpeed($piSpeed){
		$this->ciSpeed = $piSpeed;
	}

	/**
	 * Retorna um array com todos objetos do scrolling e o pr�prio scrolling
	 *
	 * <p>Retorna um array contendo todos objetos do scrolling e o pr�prio scrolling.</p>
	 * @access public
	 * @return array Array de objetos do scrolling e o pr�prio scrolling
	 */
	public function collect() {
		$maOut = array($this);
		if ($this->coContent)
		$maOut = array_merge($maOut,array($this->coContent));
		return $maOut;
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDChartDirector. Integra gr�ficos do ChartDirector com a FWD
 *
 * <p>Classe para integrar gr�ficos do ChartDirector com a FWD.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDChartDirector extends FWDView {

	/**
	 * Formato da imagem do gr�fico
	 * @var integer
	 * @access protected
	 */
	protected $ciFormat = PNG;

	/**
	 * Formato da imagem do gr�fico (usado pra indicar o content-type)
	 * @var string
	 * @access protected
	 */
	protected $csFormat = 'png';

	/**
	 * Objeto do gr�fico
	 * @var BaseChart
	 * @access protected
	 */
	protected $coChart = null;

	/**
	 * C�digo HTML do mapa
	 * @var string
	 * @access protected
	 */
	protected $csMapCode = '';

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDChartDirector.</p>
	 * @access public
	 */
	public function __construct(){
		parent::__construct();
	}

	/**
	 * Desenha o cabe�alho do gr�fico.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o cabe�alho do gr�fico.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do gr�fico
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	protected function drawHeader($poBox = null){
		return '<img ';
	}

	/**
	 * Desenha o corpo do gr�fico.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o corpo do gr�fico.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do gr�fico
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	protected function drawBody($poBox = null){
		if($this->coChart===null){
			$msSrc = '';
			if($this->cbDisplay){
				trigger_error('Chart object not set.',E_USER_ERROR);
			}
		}else{
			$msImgId = $this->getChartName();
			$msSrc = FWDWebLib::getInstance()->getLibRef()."FWDGetImg.php?img=$msImgId&id=".uniqid(session_id());
		}
		return "src='$msSrc' {$this->getGlobalProperty()} {$this->getEvents()} style='{$this->getStyle()}'";
	}

	/**
	 * Desenha o rodap� do gr�fico.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o rodap� do gr�fico.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do gr�fico
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	protected function drawFooter($poBox = null){
		if($this->csMapCode!=''){
			$msMapId = $this->csName.'_map';
			return " usemap='#$msMapId'/><map name='$msMapId'>{$this->csMapCode}</map>";
		}else{
			return "/>";
		}
	}

	/**
	 * Seta o c�digo HTML do mapa
	 *
	 * <p>Seta o c�digo HTML do mapa.</p>
	 * @access public
	 * @param string $psMapCode C�digo HTML do mapa
	 */
	public function setMapCode($psMapCode){
		$this->csMapCode = $psMapCode;
	}

	/**
	 * Seta o objeto chart
	 *
	 * <p>Seta o objeto chart.</p>
	 * @access public
	 * @param BaseChart $poChart Objeto chart
	 */
	public function setChart($poChart){
		$this->coChart = $poChart;
		$msImgId = $this->getChartName();
		FWDWebLib::getInstance()->checkSession();
		$_SESSION[$msImgId] = $this->coChart->makeChart2($this->ciFormat);
		$_SESSION[$msImgId.'_contentType'] = $this->csFormat;
	}

	/**
	 * Retorna o valor do atributo format
	 *
	 * <p>Retorna o valor do atributo format.</p>
	 * @access public
	 * @return integer Valor do atributo format
	 */
	public function getAttrFormat(){
		return $this->ciFormat;
	}

	/**
	 * Seta o valor do atributo format
	 *
	 * <p>Seta o valor do atributo format.</p>
	 * @access public
	 * @param string $psFormat Novo valor do atributo
	 */
	public function setAttrFormat($psFormat){
		$this->ciFormat = FWDWebLib::attributeParser($psFormat,array('png'=>PNG,'jpg'=>JPG,'gif'=>GIF),"psFormat");
		$this->csFormat = ($psFormat=='jpg'?'jpeg':$psFormat);
	}

	/**
	 * Pega o nome do chart
	 *
	 * <p>Pega o nome do chart.</p>
	 * @access public
	 * @return string Nome do chart
	 */
	public function getChartName() {
		return "fwdchart_{$this->csName}";
	}

}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDDiv. Implementa uma div simples (html tag 'div').
 *
 * <p>Classe que implementa uma div simples (html tag 'div').</p>
 * @package FWD5
 * @subpackage view
 */
class FWDDiv extends FWDDrawing {

	/**
	 * Box da div
	 * @var FWDBox
	 * @access protected
	 */
	protected $coBox = null;

	/**
	 * Classe CSS
	 * @var string
	 * @access protected
	 */
	protected $csClass = "";

	/**
	 * Valor da div (conteudo entre as tags 'div' e '/div')
	 * @var string
	 * @access protected
	 */
	protected $csValue = "";

	/**
	 * Nome da div (utilizada como 'name' e 'id' da div)
	 * @var string
	 * @access protected
	 */
	protected $csName = "";

	/**
	 * Par�metros extra para o atributo 'style'
	 * @var string
	 * @access protected
	 */
	protected $csExtraStyle = "";

	/**
	 * Atributo extra da div (pode conter mais de um atributo, separando por espa�os)
	 * @var string
	 * @access protected
	 */
	protected $csExtraAttribute = "";

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDDiv.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites da div
	 */
	public function __construct($poBox = null) {
		$this->coBox = $poBox;
	}

	/**
	 * Desenha o cabe�alho da div.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o cabe�alho da div.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites da div
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	protected function drawHeader($poBox = null) {
		$msGlobalProperty = "name='{$this->csName}' id='{$this->csName}' class='{$this->csClass}'";
		$msStyle  = "left:{$this->coBox->getAttrLeft()};";
		$msStyle .= "top:{$this->coBox->getAttrTop()};";
		$msStyle .= "height:{$this->coBox->getAttrHeight()};";
		$msStyle .= "width:{$this->coBox->getAttrWidth()};";
		$msStyle .= "position:absolute;";
		$msStyle .= $this->csExtraStyle;
		return "<div {$msGlobalProperty} style='{$msStyle}' {$this->csExtraAttribute} >\n";
	}

	/**
	 * Desenha o corpo da div.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o corpo da div.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites da div
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	protected function drawBody($poBox = null) {
		return $this->csValue;
	}

	/**
	 * Desenha o rodap� da div.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o rodap� da div.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites da div
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	protected function drawFooter($poBox = null) {
		return "</div>\n";
	}

	/**
	 * Seta a box da div.
	 *
	 * <p>Seta a box da div.</p>
	 * @access public
	 * @param FWDBox $poBox Box da div
	 */
	public function setObjFWDBox($poBox) {
		$this->coBox = $poBox;
	}

	/**
	 * Seta a classe da div.
	 *
	 * <p>Seta a classe da div.</p>
	 * @access public
	 * @param string $psClass Classe da div
	 */
	public function setClass($psClass) {
		$this->csClass = $psClass;
	}

	/**
	 * Seta o valor da div.
	 *
	 * <p>Seta o valor da div.</p>
	 * @access public
	 * @param string $psValue Valor da div
	 */
	public function setValue($psValue) {
		$this->csValue = $psValue;
	}

	/**
	 * Seta o nome da div.
	 *
	 * <p>Seta o nome da div.</p>
	 * @access public
	 * @param string $psValue Nome da div
	 */
	public function setName($psName) {
		$this->csName = $psName;
	}

	/**
	 * Seta par�metros extra para o atributo 'style'.
	 *
	 * <p>Seta par�metros extra para o atributo 'style'.</p>
	 * @access public
	 * @param string $psExtraStyle Par�metros extra para o atributo 'style'
	 */
	public function setExtraStyle($psExtraStyle) {
		$this->csExtraStyle = $psExtraStyle;
	}

	/**
	 * Seta o atributo extra da div.
	 *
	 * <p>Seta o atributo extra da div. Pode conter mais de um atributo
	 * (separados por espa�os em branco).</p>
	 * @access public
	 * @param string $psExtraAttribute Atributo extra da div
	 */
	public function setExtraAttribute($psExtraAttribute) {
		$this->csExtraAttribute = $psExtraAttribute;
	}

	/**
	 * Retorna a box da div.
	 *
	 * <p>Retorna a box da div.</p>
	 * @access public
	 * @return FWDBox Box da div
	 */
	public function getObjFWDBox() {
		return $this->coBox;
	}

	/**
	 * Retorna a classe da div.
	 *
	 * <p>Retorna a classe da div.</p>
	 * @access public
	 * @return string Classe da div
	 */
	public function getClass() {
		return $this->csClass;
	}

	/**
	 * Retorna o valor da div.
	 *
	 * <p>Retorna o valor da div.</p>
	 * @access public
	 * @return string Valor da div
	 */
	public function getValue() {
		return $this->csValue;
	}

	/**
	 * Retorna o nome da div.
	 *
	 * <p>Retorna o nome da div.</p>
	 * @access public
	 * @return string Nome da div
	 */
	public function getName() {
		return $this->csName;
	}

	/**
	 * Retorna par�metros extra para o atributo 'style'.
	 *
	 * <p>Retorna par�metros extra para o atributo 'style'.</p>
	 * @access public
	 * @return string Par�metros extra para o atributo 'style'
	 */
	public function getExtraStyle() {
		return $this->csExtraStyle;
	}

	/**
	 * Retorna atributo extra da div.
	 *
	 * <p>Retorna atributo extra da div. Pode conter mais de um atributo
	 * (separados por espa�os em branco).</p>
	 * @access public
	 * @return string Atributo extra da div
	 */
	public function getExtraAttribute() {
		return $this->csExtraAttribute;
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,	technics, statements, and computer programs
 * contain	unpublished	proprietary information of	Axur Communications,
 * Inc.,	and are	protected	by applied	copyright law.	They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without	the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes	codigos,	tecnicas, tratados e	programas de computador contem
 * informacao proprietaria	nao publicada pela Axur Communications, Inc.,
 * e sao	protegidas pelas leis	de direito registrado.	Essas, nao podem
 * ser dispostas	a terceiros, copiadas ou	duplicadas de qualquer forma,
 * no	todo ou	em parte,	sem	consentimento	previo	escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDStatic. Implementa texto est�tico.
 *
 * <p>Classe que implementa texto est�tico (html tag 'span').</p>
 * @package FWD5
 * @subpackage view
 */
class FWDStaticGrid extends FWDViewGrid{

	/**
	 * Define o tipo de alinhamento horizontal do texto contido no static
	 * @var string
	 * @access protected
	 */
	protected $csHorizontal = "left";

	/**
	 * Define o tipo de alinhamento vertical do texto contido no static
	 * @var boolean
	 * @access protected
	 */
	protected $csVertical = "top";

	/**
	 * Define a margem � esquerda do static
	 * @var boolean
	 * @access protected
	 */
	protected $ciMarginLeft=0;


	/**
	 * Define a margem � direita do static
	 * @var boolean
	 * @access protected
	 */
	protected $ciMarginRight=0;


	/**
	 * Define a margem de top do static
	 * @var boolean
	 * @access protected
	 */
	protected $ciMarginTop = 0;

	/**
	 * Define a margem de bottom do static
	 * @var boolean
	 * @access protected
	 */
	protected $ciMarginBottom=0;

	/**
	 * Define se o static deve ter nowrap ou n�o
	 * @var boolean
	 * @access protected
	 */
	protected $cbNoWrap = true;


	/**
	 * Valor do elemento
	 * @var FWDString
	 * @access protected
	 */
	protected $coString = null;

	/**
	 * Codigo HTML do Icone do Static
	 * @var String
	 * @access protected
	 */
	protected $csIconCode = '';

	/**
	 * Path do Icone que o static deve centralizar
	 * @var String
	 * @access protected
	 */
	protected $csIconSrc = '';

	/**
	 * Tooltip do Static
	 * @var Object FWDToolTip
	 * @access protected
	 */
	protected $coToolTip = null;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDView.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites da �rea do elemento
	 */
	public function __construct($poBox = null) {
		$this->coBox = new FWDBox();
		$this->coString = new FWDString();
		$this->setAttrClass(get_class($this));
		if ($poBox) {
			$this->coBox->setAttrLeft($poBox->getAttrLeft());
			$this->coBox->setAttrTop($poBox->getAttrTop());
			$this->coBox->setAttrHeight($poBox->getAttrHeight());
			$this->coBox->setAttrWidth($poBox->getAttrWidth());
		}
	}

	/**
	 * Desenha o cabe�alho do texto est�tico.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o cabe�alho do texto est�tico.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do texto est�tico
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawHeader($poBox = null) {

		//recalcula as dimen��es da box de acordo com a margem definida
		$moBox=$this->getObjFWDBox();

		//clone da box para evitar o efeito "mem�ria" nas altera��es das dimen��es da box do static
		$moAuxBox = clone $moBox;

		//margem a esquerda
		$moBox->setAttrWidth($moBox->getAttrWidth() - $this->ciMarginLeft);
		$moBox->setAttrLeft($moBox->getAttrLeft() + $this->ciMarginLeft);

		//margem a direita
		$moBox->setAttrWidth($moBox->getAttrWidth() - $this->ciMarginRight);

		//margem Top
		$moBox->setAttrTop($moBox->getAttrTop() + $this->ciMarginTop);
		$moBox->setAttrHeight($moBox->getAttrHeight() - $this->ciMarginTop);

		//margem Bottom
		//$moBox->setAttrTop($moBox->getAttrTop() + $this->ciMarginBottom);
		$moBox->setAttrHeight($moBox->getAttrHeight() - $this->ciMarginBottom);

		$msTDclass= $this->cbNoWrap?"class='FWDTD'":'';

		$msVerticalAlign = $this->csVertical? " valign='".$this->csVertical."'":'';
		$msHorizontalAlign = $this->csHorizontal?" align='".$this->csHorizontal."'":'';

		$msOut = " <table class='{$this->csClass}' style='position:absolute;{$moBox->draw()}'><tr><td {$msHorizontalAlign} {$msVerticalAlign} {$msTDclass}> ";

		$this->setObjFWDBox($moAuxBox);
		return $msOut;
	}

	/**
	 * Desenha o corpo do texto est�tico.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o corpo do texto est�tico.</p>
	 * @access public
	 *
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawBody() {
		if($this->csIconSrc==''){
			return $this->coString->getAttrString() . $this->csIconCode;
		}else{
			$msGfxRef = FWDWebLib::getInstance()->getGfxRef();
			$moIcon = new FWDIcon();
			$moIcon->setAttrSrc("{$msGfxRef}{$this->csIconSrc}");
			$moIcon->getImageBox();
			$miTop = ceil( ($this->coBox->getAttrHeight() - $moIcon->getObjFWDBox()->getAttrHeight()) / 2  );
			$miLeft = ceil( ($this->coBox->getAttrWidth() - $moIcon->getObjFWDBox()->getAttrWidth()) / 2  );
			$moIcon->getObjFWDBox()->setAttrTop($miTop);
			$moIcon->getObjFWDBox()->setAttrLeft($miLeft);
			return $moIcon->draw();
		}
	}

	/**
	 * Desenha o rodap� do texto est�tico.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o rodap� do texto est�tico.</p>
	 * @access public
	 *
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawFooter() {
		$msReturn =" ";
		return $msReturn .= "</table>";
	}

	/**
	 * Atribui valor, condicionalmente.
	 *
	 * <p>Atribui valor � view, condicionando atrav�s do segundo par�metro.
	 * Se force for FALSE, concatena o valor se a vari�vel tiver conte�do;
	 * se TRUE atribui mesmo que tenha conte�do (sobrescreve).</p>
	 * @access public
	 * @param string $psValue Valor a ser atribu�do
	 * @param boolean $pbForce For�ar o valor mesmo que j� tenha conte�do
	 */
	public function setValue($psValue, $pbForce = true) {
		$this->coString->setValue($psValue, $pbForce);
	}

	/**
	 * Seta o valor do elemento.
	 *
	 * <p>M�todo para setar o valor do elemento.</p>
	 * @access public
	 * @param FWDString $poString Seta o valor do elemento
	 */
	final public function setObjFWDString(FWDString $poString) {
		if (isset($this->coString)) {
			$this->coString->setValue($poString->getAttrString());
		}
		else {
			$this->coString = $poString;
		}
	}

	/**
	 * Retorna o objeto de string do elemento.
	 *
	 * <p>M�todo para retornar o objeto de string do elemento.</p>
	 * @access public
	 * @return FWDString String do elemento
	 */
	final public function getObjFWDString() {
		return $this->coString;
	}

	/**
	 * Retorna o valor do elemento.
	 *
	 * <p>M�todo para retornar o valor do elemento.</p>
	 * @access public
	 * @return string Valor do elemento
	 */
	public function getValue() {
		return $this->coString->getAttrString();
	}

	/**
	 * Seta o tipo de alinhamento horizontal do static.
	 *
	 * <p>Seta o tipo de alinhamento horizontal do static (valores possiveis -> left, center, right , justify)</p>
	 * @access public
	 * @param string $psHorizontal Define o tipo de alinhamento horizontal do static
	 */
	function setAttrHorizontal($psHorizontal)
	{
		$this->csHorizontal= FWDWebLib::attributeParser($psHorizontal,array("left","center","right","justify"),"psHorizontal");

	}

	/**
	 * Seta o tipo de alinhamento vertical do static.
	 *
	 * <p>Seta o tipo de alinhamento vertical do static (valores possiveis -> top, middle, bottom)</p>
	 * @access public
	 * @param string $psHorizontal Define o tipo de alinhamento horizontal do static
	 */
	function setAttrVertical($psVertical)
	{
		$this->csVertical = FWDWebLib::attributeParser($psVertical,array("top","middle","bottom"),"psVertical");
	}

	/**
	 * Retorna o valor do alinhamento horizontal.
	 *
	 * <p>Retorna o valor do alinhamento horizontal do static</p>
	 * @access public
	 * @return string Alinhamento horizontal do static
	 */
	function getAttrVertical()
	{
		return $this->csVertical;
	}

	/**
	 * Retorna o valor do alinhamento vertical.
	 *
	 * <p>Retorna o valor do alinhamento vertical do static</p>
	 * @access public
	 * @return string Alinhamento vertical do static
	 */
	function getAttrHorizontal()
	{
		return $this->csHorizontal;
	}

	/**
	 * Seta a margem left do static.
	 *
	 * <p>Seta o valor da margem left do static</p>
	 * @access public
	 * @param string $psMargin Define a margem left do static
	 */
	function setAttrMarginLeft($piMarginLeft)
	{
		$this->ciMarginLeft = $piMarginLeft;
	}

	/**
	 * Seta a margem right do static.
	 *
	 * <p>Seta o valor da margem right do static</p>
	 * @access public
	 * @param string $psMargin Define a margem right do static
	 */
	function setAttrMarginRight($piMarginRight)
	{
		$this->ciMarginRight = $piMarginRight;
	}

	/**
	 * Seta a margem top do static.
	 *
	 * <p>Seta o valor da margem top do static</p>
	 * @access public
	 * @param string $psMargin Define a margem top do static
	 */
	function setAttrMarginTop($piMarginTop)
	{
		$this->ciMarginTop = $piMarginTop;
	}

	/**
	 * Seta a margem bottom do static.
	 *
	 * <p>Seta o valor da margem bottom do static</p>
	 * @access public
	 * @param string $psMargin Define a margem bottom do static
	 */
	function setAttrMarginBottom($piMarginBottom)
	{
		$this->ciMarginBottom = $piMarginBottom;
	}

	/**
	 * Retorna o valor da margem left do static.
	 *
	 * <p>Retorna o valor da margem left do static</p>
	 * @access public
	 * @return integer Valor da margem left do static
	 */
	function getAttrMarginLeft()
	{
		return $this->ciMarginLeft;
	}

	/**
	 * Retorna o valor da margem top do static.
	 *
	 * <p>Retorna o valor da margem top do static</p>
	 * @access public
	 * @return integer Valor da margem top do static
	 */
	function getAttrMarginTop()
	{
		return $this->ciMarginTop;
	}

	/**
	 * Retorna o valor da margem right do static.
	 *
	 * <p>Retorna o valor da margem right do static</p>
	 * @access public
	 * @return integer Valor da margem right do static
	 */
	function getAttrMarginRight()
	{
		return $this->ciMarginRight;
	}

	/**
	 * Retorna o valor da margem bottom do static.
	 *
	 * <p>Retorna o valor da margem bottom do static</p>
	 * @access public
	 * @return integer Valor da margem bottom do static
	 */
	function getAttrMarginBottom()
	{
		return $this->ciMarginBottom;
	}

	/**
	 * Atribui valor NoEscape ao objeto string do Static.
	 *
	 * <p>Atribui valor NOEscape ao objeto FWDString do static para evitar que sejam escapados os "'" da string.</p>
	 * @access public
	 * @param boolean $pbNoEscape valor do noEscape ("true" ou "false")
	 */
	public function setAttrStringNoEscape($pbNoEscape) {
		$this->coString->setAttrNoEscape($pbNoEscape);
	}

	/**
	 * Atribui A classe css noWrap ao TD do Static.
	 *
	 * <p>Atribui valor booleando a variavel cbNoWrap para o static 'saber' se ele
	 * deve utilizar a classe css de noWrap em seu TD na hora de se desejar.</p>
	 * @access public
	 * @param boolean $pbNoWrap valor do cbnoWrap ("true" ou "false")
	 */
	public function setAttrNoWrap($pbNoWrap){
		$this->cbNoWrap = FWDWebLib::attributeParser($pbNoWrap,array("true"=>true, "false"=>false),"pbNoWrap");
	}

	/**
	 * Zera o array de eventos do static.
	 *
	 * <p>Zera o array de eventos do static</p>
	 * @access public
	 */
	public function cleanEvents(){
		$this->caEvent = array();
	}


	/**
	 * Atribui o c�digo html de um �cone no static
	 *
	 * <p>Atribui o c�digo html de um �cone no static.</p>
	 * @access public
	 * @param string $psIconCode
	 */
	public function setFWDIconCode($psIconCode) {
		$this->csIconCode = $psIconCode;
	}

	/**
	 * Atribui o source de um icone ao static
	 *
	 * <p>Atribui o source de um icone ao static.</p>
	 * @access public
	 * @param string $psIconSource
	 */
	public function setIconSrc($psIconSource){
		$this->csIconSrc = $psIconSource;
	}

	/**
	 * Atribui um ToolTip ao Static
	 *
	 * <p>Atribui um ToolTip ao Static.</p>
	 * @access public
	 * @param boolean $pbNoWrap valor do cbnoWrap ("true" ou "false")
	 */
	public function setObjFWDToolTip(FWDToolTip $poToolTip){
		$mskey = $poToolTip->getAttrEvent();
		$msArrayKey = explode(":",$mskey);
		foreach($msArrayKey as $msKey){
			if(!isset($this->caEvent[$msKey]))
			$this->caEvent[$msKey] = new FWDEventHandler($msKey,$this->csName);
			$this->caEvent[$msKey]->setAttrContent($poToolTip);
		}
	}

	/**
	 * Retorna o objeto de tooltipo do static.
	 *
	 * <p>Retorna o objeto de tooltip do static</p>
	 * @access public
	 * @return Object FWDToolTip tooltip do static
	 */
	public function getObjFWDToolTip(){
		return $this->coToolTip;
	}

	public function getArrayEvent(){
		return $this->caEvent;
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDHorizontalRule. Implementa uma linha horizontal (tag 'hr').
 *
 * <p>Classe que implementa uma linha horizontal (tag 'hr').</p>
 * @package FWD5
 * @subpackage view
 */
class FWDHorizontalRule extends FWDDrawing {

	/**
	 * Box do elemento
	 * @var FWDBox
	 * @access protected
	 */
	protected $coBox = null;

	/**
	 * Classe CSS
	 * @var string
	 * @access protected
	 */
	protected $csClass = "";

	/**
	 * Nome do elemento (utilizada como 'name' e 'id')
	 * @var string
	 * @access protected
	 */
	protected $csName = "";

	/**
	 * Par�metros extra para o atributo 'style'
	 * @var string
	 * @access protected
	 */
	protected $csExtraStyle = "";

	/**
	 * Atributo extra da div (pode conter mais de um atributo, separando por espa�os)
	 * @var string
	 * @access protected
	 */
	protected $csExtraAttribute = "";

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDHorizontalRule.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do elemento
	 */
	public function __construct($poBox = null){
		$this->coBox = $poBox;
	}

	/**
	 * Desenha o cabe�alho do elemento.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o cabe�alho do elemento.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do elemento
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	protected function drawHeader($poBox = null){
		return "<hr ";
	}

	/**
	 * Desenha o corpo do elemento.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o corpo do elemento.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do elemento
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	protected function drawBody($poBox = null){
		$msGlobalProperty = "name='{$this->csName}' id='{$this->csName}' class='{$this->csClass}'";
		$msStyle = "left:{$this->coBox->getAttrLeft()};";
		$miTop = (FWDWebLib::BrowserIsIE()? $this->coBox->getAttrTop()+6 : $this->coBox->getAttrTop());
		$msStyle.= "top:{$miTop};";
		$msStyle.= "height:{$this->coBox->getAttrHeight()};";
		$msStyle.= "width:{$this->coBox->getAttrWidth()};";
		$msStyle.= "position:absolute;";
		$msStyle.= $this->csExtraStyle;
		return "{$msGlobalProperty} style='{$msStyle}' {$this->csExtraAttribute}";
	}

	/**
	 * Desenha o rodap� do elemento.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o rodap� do elemento.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do elemento
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	protected function drawFooter($poBox = null){
		return "/>";
	}

	/**
	 * Seta a box do elemento.
	 *
	 * <p>Seta a box do elemento.</p>
	 * @access public
	 * @param FWDBox $poBox Box do elemento
	 */
	public function setObjFWDBox($poBox){
		$this->coBox = $poBox;
	}

	/**
	 * Seta a classe CSS do elemento.
	 *
	 * <p>Seta a classe CSS do elemento.</p>
	 * @access public
	 * @param string $psClass Classe do elemento
	 */
	public function setAttrClass($psClass){
		$this->csClass = $psClass;
	}

	/**
	 * Seta o nome do elemento.
	 *
	 * <p>Seta o nome do elemento.</p>
	 * @access public
	 * @param string $psName Nome do elemento
	 */
	public function setAttrName($psName){
		$this->csName = $psName;
	}

	/**
	 * Seta par�metros extra para o atributo 'style'.
	 *
	 * <p>Seta par�metros extra para o atributo 'style'.</p>
	 * @access public
	 * @param string $psExtraStyle Par�metros extra para o atributo 'style'
	 */
	public function setExtraStyle($psExtraStyle){
		$this->csExtraStyle = $psExtraStyle;
	}

	/**
	 * Seta o atributo extra do elemento.
	 *
	 * <p>Seta o atributo extra do elemento. Pode conter mais de um atributo
	 * (separados por espa�os em branco).</p>
	 * @access public
	 * @param string $psExtraAttribute Atributo extra do elemento
	 */
	public function setExtraAttribute($psExtraAttribute){
		$this->csExtraAttribute = $psExtraAttribute;
	}

	/**
	 * Retorna a box do elemento.
	 *
	 * <p>Retorna a box do elemento.</p>
	 * @access public
	 * @return FWDBox Box do elemento
	 */
	public function getObjFWDBox(){
		return $this->coBox;
	}

	/**
	 * Retorna a classe CSS do elemento.
	 *
	 * <p>Retorna a classe CSS do elemento.</p>
	 * @access public
	 * @return string Classe CSS do elemento
	 */
	public function getClass(){
		return $this->csClass;
	}

	/**
	 * Retorna o nome do elemento.
	 *
	 * <p>Retorna o nome do elemento.</p>
	 * @access public
	 * @return string Nome do elemento
	 */
	public function getAttrName(){
		return $this->csName;
	}

	/**
	 * Retorna par�metros extra para o atributo 'style'.
	 *
	 * <p>Retorna par�metros extra para o atributo 'style'.</p>
	 * @access public
	 * @return string Par�metros extra para o atributo 'style'
	 */
	public function getExtraStyle(){
		return $this->csExtraStyle;
	}

	/**
	 * Retorna atributo extra do elemento.
	 *
	 * <p>Retorna atributo extra do elemento. Pode conter mais de um atributo
	 * (separados por espa�os em branco).</p>
	 * @access public
	 * @return string Atributo extra do elemento
	 */
	public function getExtraAttribute(){
		return $this->csExtraAttribute;
	}

}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,	technics, statements, and computer programs
 * contain	unpublished	proprietary information of	Axur Communications,
 * Inc.,	and are	protected	by applied	copyright law.	They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without	the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes	codigos,	tecnicas, tratados e	programas de computador contem
 * informacao proprietaria	nao publicada pela Axur Communications, Inc.,
 * e sao	protegidas pelas leis	de direito registrado.	Essas, nao podem
 * ser dispostas	a terceiros, copiadas ou	duplicadas de qualquer forma,
 * no	todo ou	em parte,	sem	consentimento	previo	escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDStatic. Implementa texto est�tico.
 *
 * <p>Classe que implementa texto est�tico (html tag 'span').</p>
 * @package FWD5
 * @subpackage view
 */
class FWDMemoStatic extends FWDView{

	/**
	 * Define se deve existir uma barra de rolagem no caso de um overflow
	 * @var boolean
	 * @access protected
	 */
	protected $cbScrollbar = true;

	/**
	 * Define o tipo de alinhamento horizontal do texto contido no static
	 * @var string
	 * @access protected
	 */
	protected $csHorizontal = "left";

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDStatic.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do texto est�tico
	 */
	public function __construct($poBox = null) {
		parent::__construct($poBox);
	}

	/**
	 * Desenha o cabe�alho do texto est�tico.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o cabe�alho do texto est�tico.</p>
	 * @access public
	 * @param FWDBox $poBox Box que define os limites do texto est�tico
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawHeader($poBox = null) {
		$msScrollbar = ($this->getAttrScrollbar()?"overflow:auto;":"overflow:hidden;");
		$msAlign = "";
		$msAlign.= " align=\"".$this->csHorizontal."\" ";

		$msStyle = $this->getStyle();
		$msStyle = "style='{$msStyle}{$msScrollbar}'";
		$msAttrDefault = $this->getGlobalProperty();

		$msOut = "<div {$msAttrDefault} {$this->getEvents()} {$msStyle} align=\"".$this->csHorizontal."\">";

		return $msOut;
	}

	/**
	 * Desenha o corpo do texto est�tico.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o corpo do texto est�tico.</p>
	 * @access public
	 *
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawBody() {
		//return trim(str_replace("\n",' ',$this->getValue()));
		return trim($this->getValue());
	}

	/**
	 * Retorna o valor do elemento.
	 *
	 * <p>M�todo para retornar o valor do elemento.</p>
	 * @access public
	 * @return string Valor do elemento
	 */
	public function getValue() {
		$msReturn = $this->coString->getAttrString();
		if($this->cbScrollbar==true)
		$msReturn = str_replace("\n",'<br>',$msReturn);
			
		return $msReturn;
	}

	/**
	 * Desenha o rodap� do texto est�tico.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o rodap� do texto est�tico.</p>
	 * @access public
	 *
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawFooter() {
		if($this->csName)
		return "</div> <script language='javascript'>gebi('{$this->csName}').object = new FWDMemoStatic('{$this->csName}'); </script> ";
		else
		return "</div>";
	}

	/**
	 * Atribui valor � vari�vel value.
	 *
	 * <p>Atribui valor � vari�vel value, devendo retirar espa�os em branco
	 * no inicio e/ou no fim da string.</p>
	 * @access public
	 * @param string $psValue Valor a ser atribu�do
	 */
	public function setAttrValue($psValue) {
		$this->setValue(trim($psValue));
	}

	/**
	 * Seta o valor booleano do atributo Scrollbar.
	 *
	 * <p>Seta o valor booleano do atributo que define se deve existir
	 * uma barra de rolagem no caso de um overflow. Para a barra de rolagem
	 * existir, o atributo deve conter o valor booleano TRUE. Para n�o existir,
	 * o atributo deve conter o valor booleano FALSE.</p>
	 * @access public
	 * @param string $pbScrollbar Define se deve existir uma barra de rolagem no caso de um overflow
	 */
	public function setAttrScrollbar($pbScrollbar) {
		$this->cbScrollbar = FWDWebLib::attributeParser($pbScrollbar,array("true"=>true, "false"=>false),"pbScrollbar");
	}

	/**
	 * Atribui valor NoEscape ao objeto string do Static.
	 *
	 * <p>Atribui valor NOEscape ao objeto FWDString do static para evitar que sejam escapados os "'" da string.</p>
	 * @access public
	 * @param boolean $pbNoEscape valor do noEscape ("true" ou "false")
	 */

	public function setAttrStringNoEscape($pbNoEscape) {
		$this->coString->setAttrNoEscape($pbNoEscape);
	}

	/**
	 * Retorna o valor booleano do atributo Scrollbar.
	 *
	 * <p>Retorna o valor booleano TRUE se deve existir uma barra de rolagem
	 * no caso de um overflow. Retorna FALSE caso contr�rio.</p>
	 * @access public
	 * @return boolean Indica se deve existir uma barra de rolagem no caso de um overflow
	 */
	public function getAttrScrollbar() {
		return $this->cbScrollbar;
	}

	/**
	 * Seta o tipo de alinhamento horizontal do static.
	 *
	 * <p>Seta o tipo de alinhamento horizontal do static (valores possiveis -> left, center, right , justify)</p>
	 * @access public
	 * @param string $psHorizontal Define o tipo de alinhamento horizontal do static
	 */
	function setAttrHorizontal($psHorizontal)
	{
		$this->csHorizontal= FWDWebLib::attributeParser($psHorizontal,array("left","center","right","justify"),"psHorizontal");

	}

	/**
	 * Retorna o valor do alinhamento vertical.
	 *
	 * <p>Retorna o valor do alinhamento vertical do static</p>
	 * @access public
	 * @return string Alinhamento vertical do static
	 */
	function getAttrHorizontal()
	{
		return $this->csHorizontal;
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDPack. Pacote de elementos.
 *
 * <p>Empacota um grupo de elementos, permitindo que eles sejam manipulados como
 * se fossem um s�.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDPack extends FWDView {

	/**
	 * Lista dos ids dos elementos que pertencem ao pacote.
	 * @var array
	 * @access protected
	 */
	protected $caElements = array();

	/**
	 * Desenha o bot�o.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o bot�o.</p>
	 * @access public
	 *
	 * @return string C�digo HTML que ser� inserido na p�gina
	 */
	public function drawBody(){
		$msOut = "<input type='hidden' {$this->getGlobalProperty()}/>\n"
		."<script type='text/javascript'>"
		."gebi('{$this->csName}').object=new ".get_class($this)."('{$this->csName}');"
		."var soPack = gobi('{$this->csName}');";
		foreach($this->caElements as $msElementId){
			$msOut.= "soPack.addElement('{$msElementId}');";
		}
		$msOut.= "soPack = null;"
		."</script>";
		return $msOut;
	}

	public function getElements(){
		return $this->caElements;
	}

	/**
	 * Retorna o valor do atributo elements
	 *
	 * <p>Retorna o valor do atributo elements.</p>
	 * @access public
	 * @return array Valor do atributo elements
	 */
	public function getAttrElements(){
		return $this->caElements;
	}

	/**
	 * Seta o valor do atributo elements
	 *
	 * <p>Seta o valor do atributo elements.</p>
	 * @access public
	 * @param string $psElements Novo valor do atributo
	 */
	public function setAttrElements($psElements){
		$this->caElements = explode(':',$psElements);
	}

	/**
	 * Seta o valor booleano do atributo de display.
	 *
	 * <p>Seta o valor booleano do atributo que define se o elemento
	 * deve ser exibido. Para o elemento ser exibido, o atributo deve
	 * conter o valor booleano TRUE. Para n�o ser exibido, o atributo deve
	 * conter o valor booleano FALSE.</p>
	 * @access public
	 * @param string $psDisplay Define se o elemento deve ser exibido ou n�o
	 */
	public function setAttrDisplay($psDisplay){
		$this->cbDisplay = FWDWebLib::attributeParser($psDisplay,array("true"=>true, "false"=>false),"psDisplay");
		foreach($this->caElements as $msElementId){
			$moElement = FWDWebLib::getObject($msElementId);
			if($moElement){
				$moElement->setAttrDisplay($psDisplay);
			}
		}
	}

}

?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDDBGridPack. Pacote de DBGrids.
 *
 * <p>Empacota um grupo de elementos, permitindo que eles sejam manipulados como
 * se fossem um s�.</p>
 * @package FWD5
 * @subpackage view
 */
class FWDDBGridPack extends FWDPack {

	/**
	 * M�todo executado quando o XML do elemento termina de carregar.
	 *
	 * <p>M�todo executado quando o XML do elemento termina de carregar.
	 * Cria o evento de refresh das grids.</p>
	 *
	 * @access public
	 */
	public function execute(){
		$moEventHandler = FWDStartEvent::getInstance();
		$moEventHandler->addAjaxEvent(new FWDDBGridPackRefresh("{$this->csName}_refresh",$this->csName));
	}

}

?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDDBSelect. Popula elementos FWDDBSelect com informa��es do BD.
 *
 * <p>Classe que popula elementos FWDDBSelect com informa��es do BD.</p>
 * @package FWD5
 * @subpackage db
 */
class FWDDBSelect extends FWDSelect {
	/**
	 * Objeto de conex�o com o banco de dados
	 * @var FWDDBDataSet
	 * @access protected
	 */
	protected $coDataSet = NULL;

	/**
	 * Seta o DataSet.
	 *
	 * <p>M�todo para setar o objeto DataSet.</p>
	 * @param FWDDBDataSet $poDataSet DataSet
	 * @access public
	 * @deprecated
	 */
	public function setDataSet(FWDDBDataSet $poDataSet){
		$this->coDataSet = $poDataSet;
	}

	/**
	 * Seta o QueryHandler.
	 *
	 * <p>M�todo para setar o objeto QueryHandler.</p>
	 * @param FWDDBDataSet $poQueryHandler QueryHandler
	 * @access public
	 */
	public function setQueryHandler($poQueryHandler){
		$poQueryHandler->makeQuery();
		$this->coDataSet = $poQueryHandler->getDataset();
		$this->coDataSet->setQuery($poQueryHandler->getSQL());
	}

	/**
	 * Popula o FWDDBSelect.
	 *
	 * <p>M�todo para popular o FWDDBSelect com itens (linhas retornadas na query).</p>
	 * @access public
	 */
	public function populate(){
		$this->coDataSet->execute();
		while($this->coDataSet->fetch()){
			$moItem = new FWDItem();
			$msKey = $this->coDataSet->getFieldByAlias("select_id")->getValue();
			$msValue = $this->coDataSet->getFieldByAlias("select_value")->getValue();
			$moItem->setAttrKey($msKey);
			$moItem->setValue($msValue);
			if(in_array($msValue,$this->caValues)){
				$moItem->setAttrCheck('true');
			}
			$this->addObjFWDItem($moItem);
		}
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDDBGrid. Popula elementos FWDDBGrid com informa��es do BD.
 *
 * <p>Classe que popula elementos FWDDBGrid com informa��es do BD.</p>
 * @package FWD5
 * @subpackage db
 */
class FWDDBGrid extends FWDGrid {

	/**
	 * Objeto de conex�o com o banco de dados
	 * @var FWDDBDataSet
	 * @access protected
	 */
	protected $coDataSet = null;

	/**
	 * QueryHandler
	 * @var FWDDBQueryHandler
	 * @access protected
	 */
	protected $coQueryHandler = NULL;

	/**
	 * Numero m�ximo de linhas que devem ser exibidas pela grid
	 * @var string
	 * @access protected
	 */
	protected $ciMaxRow = 0;

	/**
	 * Seta o DataSet.
	 *
	 * <p>M�todo para setar o objeto DataSet.</p>
	 * @param FWDDBDataSet $poDataSet DataSet
	 * @access public
	 */
	public function setDataSet(FWDDBDataSet $poDataSet) {
		$this->coDataSet = $poDataSet;
	}

	/**
	 * Seta o n�mero maximo de linhax a serem exibidas.
	 *
	 * <p>M�todo utilizado para limitar o n�mero m�ximo de linhas que ser�o
	 * exibidas em uma dbGrid est�tica.</p>
	 * @param integer $piMaxRow numero maximo de linhas que ser�o exibidas
	 * @access public
	 */
	public function setAttrMaxRow($piMaxRow){
		$this->ciMaxRow = $piMaxRow;
	}

	/**
	 * Retorna o numero m�ximo de linhas que ser�o desenhadas.
	 *
	 * <p>M�todo para retornar o m�ximo de linhas que ser�o desenhadas em uma dbGrid
	 * est�tica.</p>
	 * @access public
	 * @return integer ciMaxRow
	 */
	public function getAttrMaxRow($piMaxRow){
		return $this->ciMaxRow;
	}

	/**
	 * Seta o QueryHandler.
	 *
	 * <p>M�todo para setar o objeto QueryHandler.</p>
	 * @param FWDDBQueryHandler $poQueryHandler QueryHandler
	 * @access public
	 */
	public function setQueryHandler(FWDDBQueryHandler $poQueryHandler) {
		$poQueryHandler->makeQuery();
		$this->coDataSet = $poQueryHandler->getDataset();
		$this->coDataSet->setQuery($poQueryHandler->getSQL());
		$this->coQueryHandler = $poQueryHandler;
	}

	/**
	 * Retorna o QueryHandler.
	 *
	 * <p>M�todo para retornar o objeto QueryHandler.</p>
	 * @access public
	 * @return FWDDBQueryHandler QueryHandler
	 */
	public function getQueryHandler() {
		return $this->coQueryHandler;
	}

	/**
	 * Retorna o DataSet.
	 *
	 * <p>M�todo para retornar o objeto DataSet.</p>
	 * @return FWDDBDataSet DataSet
	 * @access public
	 */
	public function getDataSet() {
		return $this->coDataSet;
	}

	/**
	 * Seta a Query.
	 *
	 * <p>M�todo para setar a Query do DataSet.</p>
	 * @param string $psQuery Query
	 * @access public
	 */
	public function setQuery($psQuery) {
		$this->coDataSet->setQuery($psQuery);
	}

	/**
	 * Desenha o Body da dbGrid.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para implementar o corpo da dbGrid.
	 * retorna o c�digo HTML de cada uma das linhas da grid</p>
	 * @access public
	 * @return string C�digo HTML que ser� inserido na div do body da dbGrid
	 */
	function drawBody($pbExecPopulate = true, $pbOrderColumn = true)
	{
		if(($this->cbPopulateXML==true)&&($this->cbPopulate==true)){
			$this->populate($pbExecPopulate,$pbOrderColumn);
		}
		return parent::drawBody();
	}

	/**
	 * Renderiza a grid via ajax.
	 *
	 * <p>Re�ne em uma vari�vel string, no formato de c�digo HTML, todas
	 * as informa��es necess�rias para renderizar os bot�es de p�gina��o, o conte�do do corpo,
	 * e os menus de contexto, incluindo os menus ACL da grid</p>
	 * @access public
	 * @return string C�digo HTML que cont�m os bot�es de pag., o corpo, e os menus da grid
	 */
	public function execEventPopulate($psGridEvent = 'populategrid'){
		$msHeaderPageButton="";
		$msBodyContent="";
		$maContextMenuCode = array();
		$maGridCode = array();

		if(!($psGridEvent=='refreshselect')){
			$maGridCode[] = $psGridEvent;
			$this->caGridRowsSelected=array();
			$this->caGridValue = array();
		}else{
			$maGridCode[] = 'refresh';
		}
		if($this->cbPopulateXML==true){
			//obtem o total de linhas retornadas na query e seta na grid
			if(!($psGridEvent=='refreshselect') && ($this->getAttrRefreshOnCurrentPage()==false)){
				$this->ciCurrentPage=1;
				$this->ciLastPage=1;
			}

			if(($this->cbDinamicFill==true)&&($this->cbPopulateXML)){
				if(!$this->getAttrRefreshOnCurrentPage()){
					$moDataSet = $this->getDataSet();
					$msOrder = $this->getAttrOrder();
					$maOrder = explode(":",$msOrder);
					if(($maOrder)&&($moDataSet)){
						foreach($maOrder as $msColOrder){
							$msColOrder=trim($msColOrder);
							if($msColOrder!=""){
								$miColumn="";
								$msChar = substr(trim($msColOrder),0,1);
								if(($msChar=="+")||($msChar=="-"))// ascendente
								$miColumn = substr(trim($msColOrder),1);
								else{//assumindo que soh veio um n�mero , ordena��o ascendente
									$miColumn = trim($msColOrder);
									$msChar="+";
								}
								$miColumn--;
								$moColumn = $this->caColumns[$miColumn];
								if($moColumn)
								if(($moColumn->getAttrAlias())&&($moColumn->getAttrCanOrder()))
								$moDataSet->setOrderBy($moColumn->getAttrAlias(),$msChar);
								else
								trigger_error("Trying to order an inexistent column! column number '{$miColumn}'",E_USER_WARNING);
							}//else{}//msColOrder vazio devido a um : amais na string
						}
					}
					if($this->coDataSet){
						$this->coDataSet->execute();
						$miTotalRows = $this->coDataSet->getRowCount();
						$this->ciRowsCount=$miTotalRows;
						$msHeaderPageButton = $this->drawPageButtons();
					}
					//desenha o body da grid via ajax
					$this->cbPopulate=true;
					$msBodyContent = $this->drawBody(false,false);
				}else{
					/*redesenha a grid sem trocar de p�gina*/
					if($this->coDataSet){
						$this->coDataSet->execute();
						$miTotalRows = $this->coDataSet->getRowCount();
						$this->ciRowsCount = $miTotalRows;
						$miCurrPage = $this->getCurrentPage();
						if( (( ($miCurrPage-1) * $this->ciRowsPerPage + 1 ) > $this->ciRowsCount ) && ( $miCurrPage > 1 ) ){
							$miNewActualPage = floor( $this->ciRowsCount / $this->ciRowsPerPage );
							$miNewActualPage = $miNewActualPage > 0 ? $miNewActualPage : 1;
							$this->setCurrentPage( $miNewActualPage );
						}
						$msHeaderPageButton = $this->drawPageButtons();
					}
					$this->cbPopulate=true;
					$msBodyContent = $this->drawBody();
				}
			}else{
				$this->cbPopulate=true;
				$this->arrangeAllDimensions();
				$msBodyContent = $this->drawBody(false,true);
			}
			//obtem o c�digo html do menu de contexto
			$maContextMenuCode = $this->drawMenuAjax();
		}

		$maGridCode[] = $msHeaderPageButton;
		$maGridCode[] = $msBodyContent;
		$maGridButtomCode = array();
		if($psGridEvent == 'user_edit'){
			//desenha os headers das colunas
			$maGridCode[] = $this->drawHeaderColumns();
		}else{
			if(($this->cbShowEdit)&&($this->cbPopulateXML)){
				//bot�o de edi��o das propriedades da grid
				$maGridButtomCode[] = $this->drawButtonEdit();
				//desenha os headers das colunas
				$maGridButtomCode[] = $this->drawHeaderColumns();
			}
		}
		//obtem o c�digo html do menu de contexto
		$maGridCodeAux = array_merge($maGridCode,$this->drawMenuAjax());
		$maGridCodeAux = array_merge($maGridCodeAux,$maGridButtomCode);
		//escapa os /r e os /n
		for($miI=0;$miI<count($maGridCodeAux);$miI++){
			$maGridCodeAux[$miI] = $this->scapeStringToAjax($maGridCodeAux[$miI]);
		}
		$msGridCodeAux = serialize($maGridCodeAux);

		$msGridCodeAux = str_replace('\\','\\\\',$msGridCodeAux);
		$msGridCodeScape = str_replace('"','\"',$msGridCodeAux);
		$moJsGridEvent = new FWDJsEvent(JS_GRID_EVENT,$this->csName,$msGridCodeScape);
		echo $moJsGridEvent->render();
	}

	/**
	 * Popula o FWDDBGrid.
	 *
	 * <p>M�todo para popular o FWDDBGrid com itens (linhas retornadas na query).</p>
	 * @access public
	 */
	public function populate($pbExecPopulate = true,$cbOrderColumn =true) {

		$moDataSet = $this->getDataSet();
		$msOrder = $this->getAttrOrder();
		$maOrder = explode(":",$msOrder);
		if(($maOrder)&&($moDataSet)&&($cbOrderColumn==true))
		foreach($maOrder as $msColOrder){
			$msColOrder=trim($msColOrder);
			if($msColOrder!=""){
				$miColumn="";
				$msChar = substr(trim($msColOrder),0,1);
				if(($msChar=="+")||($msChar=="-"))// ascendente
				$miColumn = substr(trim($msColOrder),1);
				else{//assumindo que soh veio um n�mero , ordena��o ascendente
					$miColumn = trim($msColOrder);
					$msChar="+";
				}
				$miColumn--;
				$moColumn = $this->caColumns[$miColumn];
				if($moColumn)
				if(($moColumn->getAttrAlias())&&($moColumn->getAttrCanOrder()))
				$moDataSet->setOrderBy($moColumn->getAttrAlias(),$msChar);
				else
				trigger_error("Trying to order an inexistent column! column number '{$miColumn}'",E_USER_WARNING);
			}//else{}//msColOrder vazio devido a um : amais na string
		}

		$this->setDataSet($moDataSet);
		if($this->cbPopulateXML==true){
			if (!$this->cbDinamicFill) {
				$this->coDataSet->execute();
				if($this->ciMaxRow>0)
				$miContMaxRowToDraw = min($this->coDataSet->getRowCount(),$this->ciMaxRow);
				else
				$miContMaxRowToDraw = 	$this->coDataSet->getRowCount();

				for ($miLin=1; $miLin<=$miContMaxRowToDraw; $miLin++) {
					$this->coDataSet->fetch();
					$miCol=0;
					foreach($this->caColumns as $moColumn){
						$miCol++;
						$moField = $this->coDataSet->getFieldByAlias($moColumn->getAttrAlias());
						if($moField){
							$this->setItem($miCol,$miLin,$moField->getValue());
						}
						//else{}//coluna sem dados ou os dados n�o vem do banco para esta coluna
					}
				}
			}
			else{
				$miOffSet = (($this->getCurrentPage()-1) * $this->ciRowsPerPage);
				/*descobrir se a p�gina do offset n�o esta v�zia, se tiver voltar � uma p�gina cheia*/
				if($pbExecPopulate==true)
				$this->coDataSet->execute($this->ciRowsPerPage, $miOffSet);

				$miContField=$this->coDataSet->getFieldCount();

				for ($miLin=1; $miLin<=min($this->ciRowsPerPage,$this->ciRowsCount-$miOffSet); $miLin++)
				{
					$this->coDataSet->fetch();
					$miCol=0;
					foreach($this->caColumns as $moColumn){
						$miCol++;
						$moField = $this->coDataSet->getFieldByAlias($moColumn->getAttrAlias());
						if($moField)
						$this->setItem($miCol,$miLin,$moField->getValue());
						if($miCol > $miContField)
						break;
					}//foreach
				}//for
			}
		}
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe para gerar relat�rios.
 *
 * <p>Classe para gerar relat�rios independentemente do formato.</p>
 * @package FWD5
 * @subpackage report
 */
class FWDReport extends FWDReportDrawing implements FWDCollectable {

	/**
	 * Nome do relat�rio
	 * @var string
	 * @access protected
	 */
	protected $csName = "";

	/**
	 * N�mero m�ximo de linhas que o relat�rio pode retornar
	 * @var integer
	 * @access protected
	 */
	protected $ciRowLimit = 0;

	/**
	 * Consulta
	 * @var string
	 * @access protected
	 */
	protected $csQuery = "";

	/**
	 * �rea do relat�rio
	 * @var FWDBox
	 * @access protected
	 */
	protected $coBox = null;

	/**
	 * Array de linhas do cabe�alho do relat�rio
	 * @var array
	 * @access protected
	 */
	protected $caHeaders = array();

	/**
	 * Array de linhas do rodap� do relat�rio
	 * @var array
	 * @access protected
	 */
	protected $caFooters = array();

	/**
	 * Primeiro n�vel do relat�rio
	 * @var FWDReportLevel
	 * @access protected
	 */
	protected $coLevel = null;

	/**
	 * Objeto para manipular o resultado da consulta
	 * @var FWDDBDataSet
	 * @access protected
	 */
	protected $coDataSet = null;

	/**
	 * Filtro do relat�rio
	 * @var FWDReportFilter
	 * @access protected
	 */
	protected $coFilter;

	/**
	 * Array de SuppressModels dos n�veis que podem ser suprimidos
	 * @var array
	 * @access protected
	 */
	protected $caSuppressModels = array();

	/**
	 * Contrutor.
	 *
	 * <p>Construtor da classe FWDReport.</p>
	 * @access public
	 */
	public function __construct(){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
	}

	/**
	 * Inicializa o relat�rio.
	 *
	 * <p>M�todo para inicializar o relat�rio.
	 * Necess�rio para funcionar com o XML compilado.</p>
	 * @access public
	 */
	protected function init() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		$this->setDataSet(new FWDDBDataSet(FWDWebLib::getConnection()));
	}

	/**
	 * Seta o nome do relat�rio.
	 *
	 * <p>M�todo para setar o nome do relat�rio.</p>
	 * @access public
	 * @param string psName Nome do relat�rio
	 */
	public function setAttrName($psName){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		$this->csName = $psName;
	}

	/**
	 * Retorna o nome do relat�rio.
	 *
	 * <p>M�todo para retornar o nome do relat�rio.</p>
	 * @access public
	 * @return string psName Nome do relat�rio
	 */
	public function getAttrName(){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		return $this->csName;
	}

	/**
	 * Seta o n�mero m�ximo de linhas que um relat�rio pode retornar.
	 *
	 * <p>M�todo para setar o n�mero m�ximo de linhas que um relat�rio pode retornar.</p>
	 * @access public
	 * @param integer piRowLimit M�ximo de linhas
	 */
	public function setAttrRowLimit($piRowLimit){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		$this->ciRowLimit = $piRowLimit;
	}

	/**
	 * Retorna o n�mero m�ximo de linhas que um relat�rio pode retornar.
	 *
	 * <p>M�todo para retornar o n�mero m�ximo de linhas que um relat�rio pode retornar.</p>
	 * @access public
	 * @return integer M�ximo de linhas do relat�rio
	 */
	public function getAttrRowLimit(){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		return $this->ciRowLimit;
	}

	/**
	 * Executa a query do relat�rio.
	 *
	 * <p>M�todo para executar a query do relat�rio.</p>
	 * @access public
	 */
	public function executeQuery(){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		if(isset( $this->coDataSet,$this->csQuery)){
			$this->coDataSet->setQuery($this->csQuery);
			$this->coDataSet->execute($this->ciRowLimit);
		}else{
			trigger_error("You must specify a dataset AND a query before running makeQuery()!",E_USER_WARNING);
		}
	}

	/**
	 * Seta o primeiro n�vel do relat�rio.
	 *
	 * <p>M�todo para setar o primeiro n�vel do relat�rio.</p>
	 * @access public
	 * @param FWDReportLevel poLevel Primeiro n�vel do relat�rio
	 * @param boolean $pbReplace Indica se o novo n�vel deve substituir o antigo, caso exista algum
	 */
	public function setObjFWDReportLevel(FWDReportLevel $poLevel, $pbReplace=false){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		if(!isset($this->coLevel) || $pbReplace){
			$this->coLevel = $poLevel;
		}else{
			trigger_error("Report level already defined!", E_USER_WARNING);
		}
	}

	/**
	 * Seta a �rea do relat�rio.
	 *
	 * <p>M�todo para setar a �rea do relat�rio.</p>
	 * @access public
	 * @param FWDBOX poBox �rea do relat�rio
	 */
	public function setObjFWDBox(FWDBox $poBox){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		$this->coBox = $poBox;
	}

	/**
	 * Seta o DataSet.
	 *
	 * <p>M�todo para setar o DataSet do relat�rio.</p>
	 * @access public
	 * @param FWDDBDataSet poDataSet DataSet
	 */
	public function setDataSet(FWDDBDataSet $poDataSet){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		$this->coDataSet = $poDataSet;
	}

	/**
	 * Retorna o DataSet.
	 *
	 * <p>M�todo para retornar o DataSet do relat�rio.</p>
	 * @access public
	 * @return FWDDBDataSet DataSet
	 */
	public function getDataSet(){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		return $this->coDataSet;
	}

	/**
	 * Seta o filtro do relat�rio.
	 *
	 * <p>Seta o filtro do relat�rio.</p>
	 * @access public
	 * @param FWDReportFilter $poFilter Filtro do relat�rio
	 */
	public function setFilter(FWDReportFilter $poFilter) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		$this->coFilter = $poFilter;
	}

	/**
	 * Retorna o filtro do relat�rio.
	 *
	 * <p>Retorna o filtro do relat�rio.</p>
	 * @access public
	 * @return ISMSReportFilter Filtro do relat�rio
	 */
	public function getFilter() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		return $this->coFilter;
	}

	/**
	 * Adiciona um campo ao relat�rio.
	 *
	 * <p>M�todo para adicionar um campo ao relat�rio.</p>
	 * @access public
	 * @param string psFieldName Nome do campo
	 * @param string psFieldAlias Alias do campo
	 */
	public function addField($psFieldName, $psFieldAlias){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		$this->coDataSet->addFWDDBField(new FWDDBField($psFieldName, $psFieldAlias, DB_UNKNOWN));
	}

	/**
	 * Adiciona uma linha ao cabe�alho/rodap� do relat�rio.
	 *
	 * <p>Adiciona uma linha ao cabe�alho/rodap� do relat�rio.</p>
	 * @access public
	 * @param FWDReportViewGroup poRepViewGroup Views
	 * @param string psTarget Cabe�alho (header) ou Rodap� (footer)
	 * @param boolean $pbFirst Indica que a linha deve sera adicionada no in�cio do cabe�alho/rodap�
	 */
	public function addObjFWDReportLine(FWDReportLine $poLine, $psTarget = "", $pbFirst = false){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		switch($psTarget){
			case "header":
				if($pbFirst){
					$this->caHeaders = array_merge(array($poLine),$this->caHeaders);
				}else{
					$this->caHeaders[] = $poLine;
				}
				break;
			case "footer":
				if($pbFirst){
					$this->caFooters = array_merge(array($poLine),$this->caFooters);
				}else{
					$this->caFooters[] = $poLine;
				}
				break;
			default:
				trigger_error("Invalid target! You must specify a target [header|footer] for FWDReportLine when it is a FWDReport's child", E_USER_NOTICE);
				break;
		}
	}

	/**
	 * Seta o objeto que vai desenhar o relat�rio.
	 *
	 * <p>M�todo para setar o objeto que vai desenhar o relat�rio. Tamb�m seta o
	 * objeto para o primeiro n�vel.</p>
	 * @access public
	 * @param FWDReportWriter poWriter Objeto que desenha o relat�rio
	 */
	public function setWriter(FWDReportWriter $poWriter){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		$this->coWriter = $poWriter;
		if($this->coLevel){
			$this->coLevel->setWriter($poWriter);
		}else{
			trigger_error("Report without any level!", E_USER_WARNING);
		}
	}

	/**
	 * Desenha o cabe�alho do relat�rio.
	 *
	 * <p>M�todo para desenhar o cabe�alho do relat�rio.</p>
	 * @access public
	 */
	public function drawHeader(){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		foreach($this->caHeaders as $moHeader){
			$this->coWriter->drawLine($moHeader,array());
		}
	}

	/**
	 * Desenha o corpo do relat�rio.
	 *
	 * <p>M�todo para desenhar o corpo do relat�rio.</p>
	 * @access public
	 */
	public function drawBody(){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		while($this->coDataSet->fetch()){
			$this->coLevel->draw($this->coDataSet,$this->coBox);
		}
		$this->coLevel->drawFooter($this->coBox,true);
	}

	/**
	 * Desenha o rodap� do relat�rio.
	 *
	 * <p>M�todo para desenhar o rodap� do relat�rio.</p>
	 * @access public
	 */
	public function drawFooter(){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		foreach($this->caFooters as $moFooter){
			$this->coWriter->drawLine($moFooter,array());
		}
	}

	/**
	 * Seta os pais dos n�veis
	 *
	 * <p>Faz com que todos os n�veis do relat�rio setem seus pais recursivamente.</p>
	 * @access public
	 */
	public function setParents(){
		$this->coLevel->setParentLevel();
	}

	/**
	 * Relaciona um objeto de SuppressModel a um n�vel.
	 *
	 * <p>Relaciona um objeto de SuppressModel a um n�vel.</p>
	 * @param string $psLevelId Id do n�vel
	 * @param FWDSuppressModel $poSuppressModel Objeto respons�vel por fazer os ajustes quando o n�vel � suprimido
	 */
	protected function addSuppressModel($psLevelId, FWDSuppressModel $poSuppressModel){
		$this->caSuppressModels[$psLevelId] = $poSuppressModel;
	}

	/**
	 * Suprime um n�vel
	 *
	 * <p>Faz com que um n�vel n�o seja renderizado no desenho do relat�rio.</p>
	 * @param string $psLevelId Id do n�vel a ser suprimido
	 */
	public function suppressLevel($psLevelId){
		if(isset($this->caSuppressModels[$psLevelId])){
			FWDWebLib::getObject($psLevelId)->suppress($this->caSuppressModels[$psLevelId],$this);
		}else{
			trigger_error("Missing SuppressModel for level '{$psLevelId}'.",E_USER_WARNING);
		}
	}

	/**
	 * M�todo executado quando acaba de carregar o XML
	 *
	 * <p>Faz com que todos os n�veis do relat�rio setem seus pais recursivamente.</p>
	 * @access public
	 */
	public function execute(){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		$this->setParents();
	}

	/**
	 * Retorna um array com todos sub-elementos do report
	 *
	 * <p>Retorna um array contendo todos elementos contidos no report ou em
	 * seus descendentes.</p>
	 * @access public
	 * @return array Array de sub-elementos
	 */
	public function collect(){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		$maOut = array($this);
		foreach($this->caHeaders as $moHeader){
			$maOut = array_merge($maOut,$moHeader->collect());
		}
		foreach($this->caFooters as $moFooter){
			$maOut = array_merge($maOut,$moFooter->collect());
		}
		$maOut = array_merge($maOut,$this->coLevel->collect());
		return $maOut;
	}

}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe que representa um n�vel de um relat�rio.
 *
 * <p>Classe que representa um n�vel de um relat�rio.</p>
 * @package FWD5
 * @subpackage report
 */
class FWDReportLevel implements FWDCollectable {

	/**
	 * Nome do n�vel
	 * @var string
	 * @access protected
	 */
	protected $csName = "";

	/**
	 * Subn�vel
	 * @var FWDReportLevel
	 * @access protected
	 */
	protected $coSubLevel = null;

	/**
	 * N�vel pai
	 * @var FWDReportLevel
	 * @access protected
	 */
	protected $coParentLevel = null;

	/**
	 * Indica se o sub-n�vel tem algum item
	 * @var boolean
	 * @access protected
	 */
	protected $cbSubLevelHasItens = false;

	/**
	 * Cabe�alho do n�vel
	 * @var FWDReportLine
	 * @access protected
	 */
	protected $coHeader = null;

	/**
	 * Corpo do n�vel
	 * @var FWDReportLine
	 * @access protected
	 */
	protected $coBody = null;

	/**
	 * N�vel buffer, utilizado para relat�rios montados manualmente
	 * @var FWDReportLine
	 * @access protected
	 */
	protected $caBuffer = array();

	/**
	 * Rodap� do n�vel
	 * @var FWDReportLine
	 * @access protected
	 */
	protected $coFooter = null;

	/**
	 * Iterador do n�vel
	 * @var FWDReportLevelIterator
	 * @access protected
	 */
	protected $coIterator = null;

	/**
	 * Deslocamento do n�vel (em rela��o a margem esquerda do n�vel pai)
	 * @var integer
	 * @access protected
	 */
	protected $ciOffset = 20;

	/**
	 * Margem deixada abaixo de um elemento desse n�vel
	 * @var integer
	 * @access protected
	 */
	protected $ciBottomMargin = 0;

	/**
	 * Campo para controle interno da zebra durante o desenho
	 * @var boolean
	 * @access private
	 */
	private $cbEvenLine = false;

	/**
	 * Contrutor.
	 *
	 * <p>Construtor da classe FWDReportLevel.</p>
	 * @access public
	 */
	public function __construct(){}

	/**
	 * Seta o nome do n�vel.
	 *
	 * <p>M�todo para setar o nome do n�vel.</p>
	 * @access public
	 * @param string psName Nome do n�vel
	 */
	public function setAttrName($psName){
		$this->csName = $psName;
	}

	/**
	 * Retorna o nome do relat�rio.
	 *
	 * <p>M�todo para retornar o nome do relat�rio.</p>
	 * @access public
	 * @return string Nome do relat�rio
	 */
	public function getAttrName(){
		return $this->csName;
	}

	/**
	 * Retorna o valor do atributo bottommargin
	 *
	 * <p>Retorna o valor do atributo bottommargin.</p>
	 * @access public
	 * @return integer Valor do atributo bottommargin
	 */
	public function getAttrBottomMargin(){
		return $this->ciBottomMargin;
	}

	/**
	 * Seta o valor do atributo bottommargin
	 *
	 * <p>Seta o valor do atributo bottommargin.</p>
	 * @access public
	 * @param integer $piBottomMargin Novo valor do atributo
	 */
	public function setAttrBottomMargin($piBottomMargin){
		$this->ciBottomMargin = $piBottomMargin;
	}

	/**
	 * Especifica qual ser� o objeto iterador para o n�vel.
	 *
	 * <p>M�todo para especificar qual ser� o objeto iterador para o n�vel.</p>
	 * @access public
	 * @param FWDReportLevelIterator poIterator Iterador do n�vel
	 */
	public function setLevelIterator(FWDReportLevelIterator $poIterator){
		$this->coIterator = $poIterator;
	}

	/**
	 * Seta o subn�vel do relat�rio.
	 *
	 * <p>M�todo para setar o subn�vel do relat�rio.</p>
	 * @access public
	 * @param FWDReportLevel poSubLevel Subn�vel do relat�rio
	 * @param boolean $pbReplace Indica se o novo n�vel deve substituir o antigo, caso exista algum
	 */
	public function setObjFWDReportLevel($poSubLevel, $pbReplace=false){
		if(!isset($this->coSubLevel) || $pbReplace){
			$this->coSubLevel = $poSubLevel;
		}else{
			trigger_error("Sublevel already defined!", E_USER_WARNING);
		}
	}

	/**
	 * Seta o cabe�alho/corpo/rodap� do n�vel.
	 *
	 * <p>M�todo para setar o cabe�alho/corpo/rodap� do n�vel.</p>
	 * @access public
	 * @param FWDReportViewGroup poRepViewGroup Views
	 * @param string psTarget Cabe�alho (header), Corpo (body = default), Rodap� (footer)
	 */
	public function setObjFWDReportLine(FWDReportLine $poLine, $psTarget = ""){
		switch($psTarget){
			case "header":
				$this->coHeader = $poLine;
				break;
			case "footer":
				$this->coFooter = $poLine;
				break;
			case "body": case "":
				$this->coBody = $poLine;
				break;
			case "buffer": case "":
				$this->caBuffer[] = $poLine;
				break;
			default:
				trigger_error("Invalid FWDReportViewGroup target", E_USER_NOTICE);
				break;
		}
	}

	/**
	 * Seta o objeto que vai desenhar o n�vel.
	 *
	 * <p>M�todo para setar o objeto que vai desenhar o n�vel. Tamb�m seta o
	 * objeto para o subn�vel (se existir).</p>
	 * @access public
	 * @param FWDReportWriter poWriter Objeto que desenha o n�vel
	 */
	public function setWriter(FWDReportWriter $poWriter){
		$this->coWriter = $poWriter;
		if($this->coSubLevel) $this->coSubLevel->setWriter($poWriter);
	}

	/**
	 * Seta o deslocamento do n�vel.
	 *
	 * <p>M�todo para setar o deslocamento do n�vel.</p>
	 * @access public
	 * @param integer piOffset Deslocamento
	 */
	public function setAttrOffset($piOffset){
		$this->ciOffset = $piOffset;
	}

	/**
	 * Retorna o deslocamento do n�vel.
	 *
	 * <p>M�todo para retornar o deslocamento do n�vel.</p>
	 * @access public
	 * @return integer Deslocamento
	 */
	public function getOffset(){
		return $this->ciOffset;
	}

	/**
	 * Testa se � o primeiro item do n�vel
	 *
	 * <p>Testa se � o primeiro item do n�vel.</p>
	 * @access public
	 * return boolean Indica se � o primeiro item do n�vel
	 */
	public function isFirst(){
		return !$this->coIterator->hasValue();
	}

	/**
	 * Desenha o corpo do n�vel.
	 *
	 * <p>M�todo para desenhar o corpo do n�vel.</p>
	 * @access public
	 * @param FWDDBDataSet poDataSet
	 * @param FWDBox poBox
	 */
	public function draw(FWDDBDataSet $poDataSet, FWDBox $poBox){
		if($this->coIterator->changedLevel($poDataSet)){
			if($this->isFirst()){
				$this->cbEvenLine = false;
				if($this->coHeader){
					$this->coWriter->drawLine($this->coHeader,$this->getOffsets());
				}
			}else{
				if(isset($this->coSubLevel)){
					if($this->cbSubLevelHasItens){
						$this->coSubLevel->drawFooter($poBox);
						$this->cbSubLevelHasItens = false;
					}
				}
				if($this->ciBottomMargin>0){
					$moBlankLine = new FWDReportLine();
					$moBlankLine->setAttrHeight($this->ciBottomMargin);
					$moBlankLine->setAttrBGColor('ffffff');
					$moBlankLine->addObjFWDReportCell(new FWDReportStatic());
					$this->coWriter->drawLine($moBlankLine,array());
				}
			}
			if(isset($this->coSubLevel)){
				$this->coSubLevel->clean();
			}
			$this->coIterator->advance($poDataSet);
			$this->coIterator->fetch($poDataSet);
			$this->coIterator->prepare($poDataSet);
			if($this->coBody){
				if($this->coParentLevel!=null) $this->coParentLevel->setSubLevelHasItens(true);
				$this->coBody->setEven($this->cbEvenLine);
				$this->coWriter->drawLine($this->coBody,$this->getOffsets());
				$this->cbEvenLine = !$this->cbEvenLine;
			}
		}
		else {
			$this->coIterator->incElements($poDataSet);
			$this->coIterator->fetch($poDataSet);
		}
		if(isset($this->coSubLevel)){
			$this->coSubLevel->draw($poDataSet, $poBox);
		}
	}

	/**
	 * Seta o n�vel pai
	 *
	 * <p>Seta o n�vel pai e faz com que todos seus descendentes fa�am o mesmo.
	 * O primeiro n�vel tem pai nulo.</p>
	 * @access public
	 * @param FWDReportLevel poParentLevel O n�vel pai
	 */
	public function setParentLevel(/*FWDReportLevel !PHP5.0.x*/ $poParentLevel = null){
		$this->coParentLevel = $poParentLevel;
		if(isset($this->coSubLevel)) $this->coSubLevel->setParentLevel($this);
	}

	/**
	 * Seta o atributo subLevelHasItens
	 *
	 * <p>Seta o atributo subLevelHasItens, que indica se o sub-n�vel tem algum
	 * item.</p>
	 * @access public
	 * @param boolean pbSubLevelHasItens Novo valor do atributo
	 */
	public function setSubLevelHasItens($pbSubLevelHasItens){
		$this->cbSubLevelHasItens = $pbSubLevelHasItens;
	}

	/**
	 * Desenha o rodap� do n�vel
	 *
	 * <p>Desenha o rodap� do n�vel.</p>
	 * @access public
	 * @param FWDBox poBox Box
	 * @param boolean pbRecursive Indica se vai desenhar os rodap�s dos sub-n�veis
	 */
	public function drawFooter($poBox,$pbRecursive=false){
		if($this->coFooter || $pbRecursive){
			if($pbRecursive && isset($this->coSubLevel) && $this->cbSubLevelHasItens){
				$this->coSubLevel->drawFooter($poBox,true);
			}
			if($this->coFooter) $this->coWriter->drawLine($this->coFooter,$this->getOffsets());
		}
	}

	/**
	 * Reseta o n�vel.
	 *
	 * <p>M�todo para resetar o n�vel.</p>
	 * @access public
	 */
	public function clean(){
		$this->coIterator->clean();
	}

	/**
	 * Retorna os deslocamentos dos n�veis superiores.
	 *
	 * <p>Retorna uma lista de pares representando a largura e a cor dos
	 * deslocamentos dos n�veis superiores.</p>
	 * @access public
	 * @return array Lista dos deslocamentos dos n�veis superiores
	 */
	public function getOffsets(){
		$maOffsets = array();
		$moLevel = $this;
		while($moLevel->coParentLevel!=null){
			$miWidth = $moLevel->getOffset();
			$moLevel = $moLevel->coParentLevel;
			if($miWidth>0){
				$msBGColor = $moLevel->coBody->getAttrBGColor();
				$maOffsets[] = array('width'=>$miWidth,'color'=>$msBGColor);
			}
		}
		return array_reverse($maOffsets);
	}

	/**
	 * Retorna o n�vel filho
	 *
	 * <p>Retorna o n�vel filho.</p>
	 * @return FWDReportLevel n�vel filho
	 */
	public function getSubLevel(){
		return $this->coSubLevel;
	}

	/**
	 * Retorna o n�vel pai
	 *
	 * <p>Retorna o n�vel pai.</p>
	 * @return FWDReportLevel n�vel pai
	 */
	public function getParentLevel(){
		return $this->coParentLevel;
	}

	/**
	 * Suprime o n�vel
	 *
	 * <p>Faz com que o n�vel n�o seja renderizado no desenho do relat�rio.</p>
	 * @param FWDSuppressModel $poSuppressModel Objeto respons�vel por fazer os ajustes necess�rios no layout dos n�veis
	 * @param FWDReport $poReport Relat�rio que cont�m os n�veis
	 */
	public function suppress($poSuppressModel, FWDReport $poReport){
		$moParent = $this->getParentLevel();
		if(!$moParent){
			$moParent = $poReport;
		}
		$poSuppressModel->makeAdjusts($this);
		$moParent->setObjFWDReportLevel($this->getSubLevel(),true);
		$poReport->setParents();
	}

	/**
	 * Retorna um array com todos sub-elementos do reportLevel
	 *
	 * <p>Retorna um array contendo todos elementos contidos no reportLevel
	 * ou em seus descendentes.</p>
	 * @access public
	 * @return array Array de sub-elementos
	 */
	public function collect() {
		$maOut = array($this);
		if($this->coHeader instanceof FWDCollectable) $maOut = array_merge($maOut,$this->coHeader->collect());
		if($this->coFooter instanceof FWDCollectable) $maOut = array_merge($maOut,$this->coFooter->collect());
		if($this->coBody instanceof FWDCollectable) $maOut = array_merge($maOut,$this->coBody->collect());
		if($this->coSubLevel && ($this->coSubLevel instanceof FWDCollectable))
		$maOut = array_merge($maOut,$this->coSubLevel->collect());
		foreach ($this->caBuffer as $moBuffer)
		if($moBuffer instanceof FWDCollectable) $maOut = array_merge($maOut,$moBuffer->collect());
		return $maOut;
	}

}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDReportLine. Representa uma linha de relat�rio
 *
 * <p>Representa uma linha de relat�rio.</p>
 * @package FWD5
 * @subpackage report
 */
class FWDReportLine implements FWDCollectable {

	/**
	 * Nome do elemento
	 * @var string
	 * @access protected
	 */
	protected $csName = "";

	/**
	 * Altura da linha
	 * @var integer
	 * @access protected
	 */
	protected $ciHeight = 20;

	/**
	 * Cor de fundo da linha. Pode ser qualquer cor v�lida de acordo com a classe FWDColor.
	 * @var string
	 * @access protected
	 */
	protected $csBGColor = 'white';

	/**
	 * Segunda cor de fundo da linha. Se usada, cria um efeito de zebra.
	 * @var string
	 * @access protected
	 */
	protected $csBGColor2 = '';

	/**
	 * Espa�amento vertical da linha (padding-top e padding-bottom)
	 * @var integer
	 * @access protected
	 */
	protected $ciLineSpacing = 0;

	/**
	 * Array ordenado contendo as c�lulas da linha
	 * @var array
	 * @access protected
	 */
	protected $caCells = array();

	/**
	 * Indica se a linha � par (usado para fazer zebrado)
	 * @var boolean
	 * @access protected
	 */
	protected $cbIsEven = false;

	/**
	 * Seta o valor do atributo IsEven
	 *
	 * <p>Seta o valor do atributo IsEven.</p>
	 * @access public
	 * @param boolean $pbIsEven Novo valor do atributo
	 */
	public function setEven($pbValue){
		$this->cbIsEven = $pbValue;
	}

	/**
	 * Retorna o nome da linha
	 *
	 * <p>Retorna o valor do atributo name.</p>
	 * @access public
	 * @return string Valor do atributo name
	 */
	public function getAttrName(){
		return $this->csName;
	}

	/**
	 * Seta o nome da linha
	 *
	 * <p>Seta o valor do atributo name.</p>
	 * @access public
	 * @param string $psName Novo valor do atributo
	 */
	public function setAttrName($psName){
		$this->csName = $psName;
	}

	/**
	 * Retorna a altura da linha
	 *
	 * <p>Retorna o valor do atributo height.</p>
	 * @access public
	 * @return integer Valor do atributo height
	 */
	public function getAttrHeight(){
		return $this->ciHeight;
	}

	/**
	 * Seta a altura da linha
	 *
	 * <p>Seta o valor do atributo height.</p>
	 * @access public
	 * @param integer $piHeight Novo valor do atributo
	 */
	public function setAttrHeight($piHeight){
		$this->ciHeight = $piHeight;
	}

	/**
	 * Retorna a cor de fundo da linha
	 *
	 * <p>Retorna a cor de fundo da linha.</p>
	 * @access public
	 * @return string Valor do atributo bgcolor
	 */
	public function getAttrBGColor(){
		if($this->cbIsEven && $this->csBGColor2){
			return $this->csBGColor2;
		}else{
			return $this->csBGColor;
		}
	}

	/**
	 * Seta a cor de fundo da linha
	 *
	 * <p>Seta a cor de fundo da linha.</p>
	 * @access public
	 * @param string $psBGColor Novo valor do atributo bgcolor
	 */
	public function setAttrBGColor($psBGColor){
		$this->csBGColor = $psBGColor;
	}

	/**
	 * Seta a segunda cor de fundo da linha
	 *
	 * <p>Seta a segunda cor de fundo da linha.</p>
	 * @access public
	 * @param string $psBGColor Novo valor do atributo bgcolor
	 */
	public function setAttrBGColor2($psBGColor){
		$this->csBGColor2 = $psBGColor;
	}

	/**
	 * Retorna o valor do atributo linespacing
	 *
	 * <p>Retorna o valor do atributo linespacing.</p>
	 * @access public
	 * @return integer Valor do atributo linespacing
	 */
	public function getAttrLineSpacing(){
		return $this->ciLineSpacing;
	}

	/**
	 * Seta o valor do atributo linespacing
	 *
	 * <p>Seta o valor do atributo linespacing.</p>
	 * @access public
	 * @param integer $piLineSpacing Novo valor do atributo
	 */
	public function setAttrLineSpacing($piLineSpacing){
		$this->ciLineSpacing = $piLineSpacing;
	}

	/**
	 * Adiciona uma c�lula � linha
	 *
	 * <p>Adiciona uma c�lula � linha.</p>
	 * @access public
	 * @param FWDReportCell $poCell C�lula a ser adicionada � linha
	 */
	public function addObjFWDReportCell(FWDReportCell $poCell){
		$this->caCells[] = $poCell;
	}

	/**
	 * Retorna um array com todas as c�lulas da linha
	 *
	 * <p>Retorna um array com todas as c�lulas da linha.</p>
	 * @access public
	 * @return array Array de c�lulas
	 */
	public function getCells(){
		return $this->caCells;
	}

	/**
	 * Retorna um array com todos sub-elementos do reportLine
	 *
	 * <p>Retorna um array contendo todos elementos contidos no reportLine
	 * ou em seus descendentes.</p>
	 * @access public
	 * @return array Array de sub-elementos
	 */
	public function collect(){
		$maOut = array($this);
		$maOut = array_merge($maOut,$this->caCells);
		return $maOut;
	}

}

?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDReportStatic. Uma c�lula de relat�rio contendo texto.
 *
 * <p>Representa uma c�lula de uma linha de relat�rio contendo texto.</p>
 * @package FWD5
 * @subpackage report
 */
class FWDReportStatic extends FWDReportCell {

	/**
	 * Objeto string contendo o texto da c�lula
	 * @var FWDString
	 * @access protected
	 */
	protected $coString = null;

	/**
	 * Alinhamento do texto dentro da c�lula (left|center|right)
	 * @var string
	 * @access protected
	 */
	protected $csAlign = 'left';

	/**
	 * Indica se o texto da c�lula est� em negrito
	 * @var boolean
	 * @access protected
	 */
	protected $cbBold = false;

	/**
	 * Indica se o texto da c�lula est� em it�lico
	 * @var boolean
	 * @access protected
	 */
	protected $cbItalic = false;

	/**
	 * Indica se o texto da c�lula est� sublinhado
	 * @var boolean
	 * @access protected
	 */
	protected $cbUnderline = false;

	/**
	 * Cor do texto da c�lula. Pode ser qualquer cor v�lida de acordo com a classe FWDColor.
	 * @var string
	 * @access protected
	 */
	protected $csColor = '000000';

	/**
	 * Tipo da fonte do texto da c�lula
	 * @var string
	 * @access protected
	 */
	protected $csFontFamily = 'Arial';

	/**
	 * Tamanho da fonte do texto da c�lula
	 * @var integer
	 * @access protected
	 */
	protected $ciFontSize = 8;

	/**
	 * Atribui um valor ao objeto string da c�lula.
	 *
	 * <p>Atribui um valor ao objeto string da c�lula.</p>
	 * @access public
	 * @param string $psValue Novo texto da c�lula
	 */
	public function setValue($psValue, $psForce = true){
		if($this->coString==null) $this->coString = new FWDString();
		$this->coString->setValue($psValue, $psForce);
	}

	/**
	 * Retorna o texto da c�lula.
	 *
	 * <p>Retorna o texto da c�lula.</p>
	 * @access public
	 * @return string Texto da c�lula
	 */
	public function getValue(){
		if($this->coString) return $this->coString->getAttrString();
		else return '';
	}

	/**
	 * Seta o valor do atributo noEscape do objeto string
	 *
	 * <p>Seta o valor do atributo noEscape do objeto string para evitar que os
	 * caracteres "'" sejam escapados.</p>
	 * @access public
	 * @param boolean $pbNoEscape Novo valor do atributo noEscape
	 */
	public function setAttrStringNoEscape($pbNoEscape){
		if($this->coString==null){
			$this->coString = new FWDString();
		}
		$this->coString->setAttrNoEscape($pbNoEscape);
	}

	/**
	 * Seta o valor do elemento.
	 *
	 * <p>M�todo para setar o valor do elemento.</p>
	 * @access public
	 * @param FWDString $poString Seta o valor do elemento
	 */
	public function setObjFWDString(FWDString $poString){
		if(isset($this->coString)){
			$this->coString->setValue($poString->getAttrString());
		}else{
			$this->coString = $poString;
		}
	}

	/**
	 * Retorna o valor do atributo align
	 *
	 * <p>Retorna o valor do atributo align.</p>
	 * @access public
	 * @return string Valor do atributo align
	 */
	public function getAttrAlign(){
		return $this->csAlign;
	}

	/**
	 * Seta o valor do atributo align
	 *
	 * <p>Seta o valor do atributo align.</p>
	 * @access public
	 * @param string $psAlign Novo valor do atributo
	 */
	public function setAttrAlign($psAlign){
		$this->csAlign = FWDWebLib::attributeParser($psAlign,array('left'=>'left','center'=>'center','right'=>'right'),"psAlign");
	}

	/**
	 * Retorna o valor do atributo bold
	 *
	 * <p>Retorna o valor do atributo bold.</p>
	 * @access public
	 * @return boolean Valor do atributo bold
	 */
	public function getAttrBold(){
		return $this->cbBold;
	}

	/**
	 * Seta o valor do atributo bold
	 *
	 * <p>Seta o valor do atributo bold.</p>
	 * @access public
	 * @param boolean $pbBold Novo valor do atributo
	 */
	public function setAttrBold($psBold){
		$this->cbBold = FWDWebLib::attributeParser($psBold,array("true"=>true, "false"=>false),"psBold");
	}

	/**
	 * Retorna o valor do atributo italic
	 *
	 * <p>Retorna o valor do atributo italic.</p>
	 * @access public
	 * @return boolean Valor do atributo italic
	 */
	public function getAttrItalic(){
		return $this->cbItalic;
	}

	/**
	 * Seta o valor do atributo italic
	 *
	 * <p>Seta o valor do atributo italic.</p>
	 * @access public
	 * @param boolean $pbItalic Novo valor do atributo
	 */
	public function setAttrItalic($psItalic){
		$this->cbItalic = FWDWebLib::attributeParser($psItalic,array("true"=>true, "false"=>false),"psItalic");
	}

	/**
	 * Retorna o valor do atributo underline
	 *
	 * <p>Retorna o valor do atributo underline.</p>
	 * @access public
	 * @return boolean Valor do atributo underline
	 */
	public function getAttrUnderline(){
		return $this->cbUnderline;
	}

	/**
	 * Seta o valor do atributo underline
	 *
	 * <p>Seta o valor do atributo underline.</p>
	 * @access public
	 * @param boolean $pbUnderline Novo valor do atributo
	 */
	public function setAttrUnderline($psUnderline){
		$this->cbUnderline = FWDWebLib::attributeParser($psUnderline,array("true"=>true, "false"=>false),"psUnderline");
	}

	/**
	 * Retorna o valor do atributo color
	 *
	 * <p>Retorna o valor do atributo color.</p>
	 * @access public
	 * @return string Valor do atributo color
	 */
	public function getAttrColor(){
		return $this->csColor;
	}

	/**
	 * Seta o valor do atributo color
	 *
	 * <p>Seta o valor do atributo color.</p>
	 * @access public
	 * @param string $psColor Novo valor do atributo
	 */
	public function setAttrColor($psColor){
		if(FWDColor::isValid($psColor)){
			$this->csColor = $psColor;
		}else{
			trigger_error('Invalid color name or code.',E_USER_WARNING);
		}
	}

	/**
	 * Retorna o valor do atributo fontfamily
	 *
	 * <p>Retorna o valor do atributo fontfamily.</p>
	 * @access public
	 * @return string Valor do atributo fontfamily
	 */
	public function getAttrFontFamily(){
		return $this->csFontFamily;
	}

	/**
	 * Seta o valor do atributo fontfamily
	 *
	 * <p>Seta o valor do atributo fontfamily.</p>
	 * @access public
	 * @param string $psFontFamily Novo valor do atributo
	 */
	public function setAttrFontFamily($psFontFamily){
		$this->csFontFamily = $psFontFamily;
	}

	/**
	 * Retorna o valor do atributo fontsize
	 *
	 * <p>Retorna o valor do atributo fontsize.</p>
	 * @access public
	 * @return integer Valor do atributo fontsize
	 */
	public function getAttrFontSize(){
		return $this->ciFontSize;
	}

	/**
	 * Seta o valor do atributo fontsize
	 *
	 * <p>Seta o valor do atributo fontsize.</p>
	 * @access public
	 * @param integer $piFontSize Novo valor do atributo
	 */
	public function setAttrFontSize($piFontSize){
		$this->ciFontSize = $piFontSize;
	}

}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDReportIcon. Uma c�lula de relat�rio contendo uma imagem.
 *
 * <p>Representa uma c�lula de uma linha de relat�rio contendo uma imagem.</p>
 * @package FWD5
 * @subpackage report
 */
class FWDReportIcon extends FWDReportCell {

	/**
	 * Source do �cone (path relativo ao sistema de arquivos local)
	 * @var string
	 * @access protected
	 */
	protected $csSrc = '';

	/**
	 * Altura do �cone (em pixels)
	 * @var integer
	 * @access protected
	 */
	protected $ciHeight = 0;

	/**
	 * Largura do �cone (em pixels)
	 * @var integer
	 * @access protected
	 */
	protected $ciWidth = 0;

	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDReportIcon. Seta a altura e a largura do
	 * �cone, caso n�o tenham sido especificadas.</p>
	 * @access public
	 */
	public function __construct(){
		if ($this->csSrc){
			$maBox = @getimagesize($this->csSrc);
			if (!$this->ciHeight) $this->ciHeight = $maBox[1];
			if (!$this->ciWidth) $this->ciWidth = $maBox[0];
		}
	}

	/**
	 * Seta o source do �cone.
	 *
	 * <p>Seta o source do �cone (path da imagem). Seta tamb�m a altura
	 * e a largura do �cone.</p>
	 * @access public
	 * @param string $psSrc Source do �cone
	 */
	public function setAttrSrc($psSrc){
		$this->csSrc = $psSrc;
	}

	/**
	 * Seta a altura do �cone.
	 *
	 * <p>Seta a altura do �cone, em pixels.</p>
	 * @access public
	 * @param string $piHeight Altura do �cone
	 */
	public function setAttrHeight($piHeight){
		$this->ciHeight = $piHeight;
	}

	/**
	 * Seta a largura do �cone.
	 *
	 * <p>Seta a largura do �cone, em pixels.</p>
	 * @access public
	 * @param string $piWidth Altura do �cone
	 */
	public function setAttrWidth($piWidth){
		$this->ciWidth = $piWidth;
	}

	/**
	 * Retorna o source do �cone.
	 *
	 * <p>Retorna o source do �cone (path da imagem).</p>
	 * @access public
	 * @return string Source do �cone
	 */
	public function getAttrSrc(){
		return $this->csSrc;
	}

	/**
	 * Retorna a altura do �cone.
	 *
	 * <p>Retorna a altura do �cone, em pixels.</p>
	 * @access public
	 * @return integer Altura do �cone
	 */
	public function getAttrHeight(){
		return $this->ciHeight;
	}

	/**
	 * Retorna a largura do �cone.
	 *
	 * <p>Retorna a largura do �cone, em pixels.</p>
	 * @access public
	 * @return integer Largura do �cone
	 */
	public function getAttrWidth(){
		return $this->ciWidth;
	}

	/**
	 * Retorna a extens�o do arquivo de imagem
	 *
	 * <p>Retorna a extens�o do arquivo de imagem.</p>
	 * @access public
	 * @return string Extens�o do arquivo
	 */
	public function getExtension(){
		return strtolower(substr($this->csSrc, strrpos($this->csSrc,'.')+1));
	}

}

?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe para escrever um relat�rio no formato HTML.
 *
 * <p>Classe para escrever um relat�rio no formato HTML.</p>
 * @package FWD5
 * @subpackage report
 */
class FWDReportHTMLWriter extends FWDReportWriter {

	/**
	 * Buffer de declara��es em CSS. Usado pra diminuir o n�mero de tags style.
	 * @var array
	 * @access protected
	 */
	protected $caCssOutput = array();

	/**
	 * Array associativo que armazena os identificadores das classes CSS. A chave � o md5 do conte�do da classe.
	 * @var array
	 * @access protected
	 */
	protected $caCssClasses = array();

	/**
	 * String que cont�m os "d�gitos" usados nos identificadores de classes CSS.
	 * @var string
	 * @access protected
	 */
	protected $csDigits;

	/**
	 * Armazena o n�mero de d�gitos (tamanho de $csDigits). Serve para evitar m�ltiplas chamadas de strlen().
	 * @var integer
	 * @access protected
	 */
	protected $ciNumDigits;

	/**
	 * Guarda o �ltimo identificador de classe CSS gerado.
	 * @var string
	 * @access protected
	 */
	protected $csLastId;

	/**
	 * Define o n�mero de linhas de um grupo. Ao final de cada grupo, caso seja
	 * necess�rio, uma tag style � escrita pelo flushCss.
	 * @staticvar integer
	 * @access protected
	 */
	protected static $LINE_GROUP_SIZE = 5;

	/**
	 * Contador de linhas. Indica a linha atual do grupo atual.
	 * @var integer
	 * @access protected
	 */
	protected $ciLineGroupCounter = 0;

	/**
	 * Contador de tags style. O IE s� permite 31 tags style.
	 * @var integer
	 * @access protected
	 */
	protected $ciStyleCounter = 0;

	/**
	 * Define se o HTML deve ser enviado via Download
	 * @var boolean
	 * @access protected
	 */
	protected $cbIsForDownload = false;

	/**
	 * Nome do arquivo (sem extens�o) a ser salvo
	 * @var string
	 * @access protected
	 */
	protected $csFileName;

	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDReportHTMLWriter.</p>
	 * @param string $psFileName Nome do arquivo (sem extens�o) a ser salvo
	 * @access public
	 */
	public function __construct($psFileName='report'){
		$this->csFileName = strtr($psFileName,'\/:*?"<>|','_________');
		$this->caCssOutput[] = "table{border-spacing:0;border-collapse:collapse}";
		$this->caCssOutput[] = 'td{margin:0;padding:0;vertical-align:top}';
		$this->caCssOutput[] = 'div{margin:0;padding:0;overflow:hidden;text-overflow:ellipsis}';
		//$this->csDigits = 'abcdefghijklmnopqrstuvwxyz��������������������������񧰺������';
		$this->csDigits = 'abcdefghijklmnopqrstuvwxyz';
		$this->ciNumDigits = strlen($this->csDigits);
		$this->csLastId = '';
	}

	/**
	 * Gera um novo identificador para classes CSS.
	 *
	 * <p>Gera um novo identificador para classes CSS.</p>
	 * @access protected
	 * @return string Identificador gerado
	 */
	protected function nextId(){
		if($this->csLastId==''){
			$msId = $this->csDigits[0];
		}else{
			$msId = $this->csLastId;
			$miNumDigits = strlen($msId);
			for($i=0;$i<$miNumDigits;$i++){
				$miIndex = strpos($this->csDigits,$msId[$i]);
				if($miIndex<$this->ciNumDigits-1){
					$msId[$i] = $this->csDigits[$miIndex+1];
					$this->csLastId = $msId;
					return $msId;
				}else{
					$msId[$i] = $this->csDigits[0];
				}
			}
			$msId.= $this->csDigits[0];
		}
		$this->csLastId = $msId;
		return $msId;
	}

	/**
	 * Retorna o identificador de uma classe CSS.
	 *
	 * <p>Retorna o identificador de uma classe CSS a partir de seu conte�do.
	 * Se a classe n�o existir ainda, ela � criada.</p>
	 * @access protected
	 * @return string Identificador gerado
	 */
	protected function getCssClass($psClassContent){
		$msHash = md5($psClassContent);
		if(!isset($this->caCssClasses[$msHash])){
			$msId = $this->nextId();
			$this->caCssClasses[$msHash] = $msId;
			$this->caCssOutput[] = ".$msId{".$psClassContent.'}';
		}
		return $this->caCssClasses[$msHash];
	}

	/**
	 * Define se o HTML ser� enviado para download
	 *
	 * <p>Define se o HTML ser� enviado para download</p>
	 * @access public
	 * @param boolean cbIsForDownload Define se o HTML ser� enviado para download
	 */
	public function setIsForDownload($pbIsForDownload){
		$this->cbIsForDownload = $pbIsForDownload;
	}

	/**
	 * Escreve na sa�da todas declara��es CSS que est�o bufferizadas.
	 *
	 * <p>Escreve na sa�da todas declara��es CSS que est�o bufferizadas em
	 * $caCssOutput e esvazia o buffer.</p>
	 * @param boolean Indica se � para for�ar o flush (s� o �ltimo deve for�ar)
	 * @access protected
	 */
	protected function flushCss($pbForceFlush=false){
		$this->ciLineGroupCounter++;
		// se 30 tags style j� tiverem sido escritas, s� permite mais uma (a com pbForceFlush = true)
		if($pbForceFlush || ($this->ciStyleCounter<30 && $this->ciLineGroupCounter==self::$LINE_GROUP_SIZE)){
			$this->ciLineGroupCounter = 0;
			if(count($this->caCssOutput)>0){
				echo "<style>";
				foreach($this->caCssOutput as $msOutput) echo $msOutput;
				echo "</style>";
				$this->caCssOutput = array();
				$this->ciStyleCounter++;
			}
		}
	}

	/**
	 * Gera o relat�rio.
	 *
	 * <p>M�todo para gerar o relat�rio.</p>
	 * @access public
	 */
	public function generateReport(){
		$this->flushCss(true);
		?>
</body>
</html>
		<?
	}

	/**
	 * Desenha uma linha do relat�rio
	 *
	 * <p>Desenha uma linha do relat�rio (representada por um objeto
	 * FWDReportLine).</p>
	 * @param FWDReportLine $poLine Linha a ser desenhada
	 * @param array $paOffsets Array de offsets dos n�veis superiores
	 * @access public
	 */
	public function drawLine(FWDReportLine $poLine,$paOffsets){
		if($this->cbIsForDownload) {
			$ssProtocol = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] && $_SERVER['HTTPS']!='off') ? 'https' : 'http';
			$ssPath = "$ssProtocol://{$_SERVER['SERVER_NAME']}{$_SERVER['PHP_SELF']}";
			$ssImgPath = substr($ssPath,0,strrpos($ssPath,'/')+1);
		}
		else {
			$ssImgPath = "";
		}
		if($this->csLastId==''){
			if($this->cbIsForDownload) {
				header("Cache-Control: ");// leave blank to avoid IE errors
				header("Pragma: ");// leave blank to avoid IE errors
				header("Content-type: application/octet-stream");
				header("Content-Disposition: attachment; filename=\"{$this->csFileName}.htm\"");
			}
			?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body topmargin=0 leftmargin=0 oncontextmenu='return true'>
			<?
		}
		$miNumOffsets = count($paOffsets);
		if($miNumOffsets>1){
			$maOffsets = array($paOffsets[0]);
			$miLastOffset = 0;
			for($i=1;$i<$miNumOffsets;$i++){
				if($paOffsets[$i]['color']==$maOffsets[$miLastOffset]['color']){
					$maOffsets[$miLastOffset]['width']+= $paOffsets[$i]['width'];
				}else{
					$maOffsets[] = $paOffsets[$i];
					$miLastOffset++;
				}
			}
		}else{
			$maOffsets = $paOffsets;
		}
		$msOffsetsHtml = '';
		for($i=0;$i<count($maOffsets);$i++){
			$miWidth = $maOffsets[$i]['width'];
			$msBGColor = $maOffsets[$i]['color'];
			if(FWDColor::nameToHex($msBGColor)===false) $msBGColor = "#$msBGColor";
			$msClass = $this->getCssClass("width:$miWidth;background:$msBGColor");
			$msOffsetsHtml.= "<td class=$msClass>&nbsp;";
		}

		$msBGColor = $poLine->getAttrBGColor();
		if(FWDColor::nameToHex($msBGColor)===false) $msBGColor = "#$msBGColor";
		$maCells = $poLine->getCells();

		$miLineSpacing = $poLine->getAttrLineSpacing();
		$msLineSpacingHtml = '';
		if($miLineSpacing){
			$msClass = $this->getCssClass("height:{$miLineSpacing};line-height:{$miLineSpacing}px;");
			$msLineSpacingHtml.= "<table><tr class={$msClass}>{$msOffsetsHtml}";
			$miWidth = 0;
			foreach($maCells as $moCell){
				$miWidth+= $moCell->getAttrWidth();
			}
			$msClass = $this->getCssClass("width:{$miWidth};background:{$msBGColor}");
			$msLineSpacingHtml.= "<td class={$msClass}></table>";
			echo $msLineSpacingHtml;
		}

		$miHeight = $poLine->getAttrHeight();
		$msClass = $this->getCssClass("height:$miHeight;line-height:{$miHeight}px;");
		echo "<table><tr class={$msClass}>{$msOffsetsHtml}";
		if(count($maCells)>0){
			foreach($maCells as $moCell){

				$miMarginLeft = $moCell->getAttrMarginLeft();
				$miMarginRight = $moCell->getAttrMarginRight();
				$miWidth = $moCell->getAttrWidth() - $miMarginLeft - $miMarginRight;

				if($miMarginLeft){
					$msClass = $this->getCssClass("width:{$miMarginLeft};background:{$msBGColor}");
					echo "<td class={$msClass}>";
				}

				if($moCell instanceof FWDReportStatic){
					$msStyle = '';
					if($moCell->getAttrBold()) $msStyle.= 'font-weight:bold;';
					if($moCell->getAttrItalic()) $msStyle.= 'font-style:italic;';
					if($moCell->getAttrUnderline()) $msStyle.= 'text-decoration:underline;';
					$msStyle.= "color:{$moCell->getAttrColor()};";
					$msStyle.= "text-align:{$moCell->getAttrAlign()};";
					$msStyle.= "font-family:{$moCell->getAttrFontFamily()};";
					$msStyle.= "font-size:{$moCell->getAttrFontSize()}pt;";
					$msStyle.= "width:{$miWidth};";
					$msStyle.= "background:$msBGColor";
					$msString = $moCell->getValue();
					$msClass = $this->getCssClass($msStyle);
					echo "<td class=$msClass><div class=$msClass>$msString";
				}elseif($moCell instanceof FWDReportIcon){
					$msClass = $this->getCssClass("width:{$miWidth};background:$msBGColor");
					echo "<td class=$msClass>";
					$msClass = $this->getCssClass("height:{$moCell->getAttrHeight()};width:{$miWidth}");
					$msSrc = $moCell->getAttrSrc();
					$miDotPos = strrpos($msSrc,'.');
					$msImgName = substr($msSrc,0,$miDotPos);
					$msExtension = strtolower(substr($msSrc,$miDotPos+1));
					if($msExtension=='png' && file_exists("$msImgName.gif")) $msSrc = "$msImgName.gif";
					echo "<img src='".$ssImgPath.$msSrc."'class=$msClass>";
				}else{
					trigger_error('This kind of ReportCell is not supported by HTMLWriter.',E_USER_WARNING);
				}

				if($miMarginRight){
					$msClass = $this->getCssClass("width:{$miMarginRight};background:{$msBGColor}");
					echo "<td class={$msClass}>";
				}

			}
		}else{
			echo "<td>";
		}
		echo "</table>";
		echo $msLineSpacingHtml;
		$this->flushCss();
	}

}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe para escrever um relat�rio no formato Doc.
 *
 * <p>Classe para escrever um relat�rio no formato Doc.</p>
 * @package FWD5
 * @subpackage report
 */
class FWDReportDocWriter extends FWDReportWriter {

	/**
	 * Acumula o c�digo html do relat�rio
	 * @var string
	 * @access protected
	 */
	protected $csReport;

	/**
	 * Nome do arquivo (sem extens�o) a ser salvo
	 * @var string
	 * @access protected
	 */
	protected $csFileName;

	/**
	 * Construtor
	 *
	 * <p>Instancia e inicializa o objeto que constr�i o arquivo Doc.</p>
	 * @param string $psFileName Nome do arquivo (sem extens�o) a ser salvo
	 * @access public
	 */
	public function __construct($psFileName='report'){
		$this->csFileName = strtr($psFileName,'\/:*?"<>|','_________');
		$this->csReport = '';
	}

	/**
	 * Gera o relat�rio.
	 *
	 * <p>M�todo para gerar o relat�rio.</p>
	 * @access public
	 */
	public function generateReport(){
		@header("Cache-Control: ");// leave blank to avoid IE errors
		@header("Pragma: ");// leave blank to avoid IE errors
		@header("Content-type: application/octet-stream");
		@header("Content-Disposition: attachment; filename=\"{$this->csFileName}.doc\"");
		echo $this->csReport;
		exit();
	}

	/**
	 * Desenha uma linha do relat�rio
	 *
	 * <p>Desenha uma linha do relat�rio (representada por um objeto
	 * FWDReportLine).</p>
	 * @param FWDReportLine $poLine Linha a ser desenhada
	 * @param array $paOffsets Array de offsets dos n�veis superiores
	 * @access public
	 */
	public function drawLine(FWDReportLine $poLine,$paOffsets){
		$miHeight = $poLine->getAttrHeight();
		$this->csReport.= "<table cellpadding='0' cellspacing='0' style='border-spacing:0px;border-collapse:collapse;'>\n<tr style='height:$miHeight;'>\n";
		for($miI=0;$miI<count($paOffsets);$miI++){
			$miWidth = $paOffsets[$miI]['width'];
			$msBGColor = $paOffsets[$miI]['color'];
			if(FWDColor::nameToHex($msBGColor)===false) $msBGColor = "#$msBGColor";
			$this->csReport.= "<td style='margin:0px;padding:0px;width:$miWidth;background:$msBGColor;'>&nbsp;</td>";
		}
		$msBGColor = $poLine->getAttrBGColor();
		if(FWDColor::nameToHex($msBGColor)===false) $msBGColor = "#$msBGColor";
		$maCells = $poLine->getCells();
		if(count($maCells)>0){
			foreach($maCells as $moCell){
				if($moCell instanceof FWDReportStatic){
					$msStyle = '';
					if($moCell->getAttrBold()) $msStyle.= 'font-weight:bold;';
					if($moCell->getAttrItalic()) $msStyle.= 'font-style:italic;';
					if($moCell->getAttrUnderline()) $msStyle.= 'text-decoration:underline;';
					$msStyle.= "color:{$moCell->getAttrColor()};";
					$msStyle.= "text-align:{$moCell->getAttrAlign()};";
					$msStyle.= "font-family:{$moCell->getAttrFontFamily()};";
					$msStyle.= "font-size:{$moCell->getAttrFontSize()}pt;";
					$msStyle.= "width:{$moCell->getAttrWidth()};";
					$msStyle.= "background:$msBGColor;";
					$msString = strip_tags(FWDWeblib::decodeUTF8($moCell->getValue()));
					$this->csReport.= "<td style='margin:0px;padding:0px;$msStyle'>$msString</td>\n";
				}else{
					$this->csReport.= "<td style='margin:0px;padding:0px;width:{$moCell->getAttrWidth()};background:$msBGColor;'></td>";
				}
			}
		}else{
			$this->csReport.= "<td style='margin:0px;padding:0px;'></td>\n";
		}
		$this->csReport.= "</tr>\n</table>\n";
	}

}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe para escrever um relat�rio no formato PDF.
 *
 * <p>Classe para escrever um relat�rio no formato PDF.</p>
 * @package FWD5
 * @subpackage report
 */
class FWDReportPDFWriter extends FWDReportWriter {

	/**
	 * Objeto que constr�i o arquivo PDF
	 * @var FPDF
	 * @access protected
	 */
	protected $coFpdf = null;

	/**
	 * Linhas que s�o usadas como cabe�alho das p�ginas
	 * @var array
	 * @access protected
	 */
	protected $caHeaders = array();

	/**
	 * N�mero de linhas do cabe�alho das p�ginas
	 * @var integer
	 * @access protected
	 */
	protected $ciHeaderLines = 0;

	/**
	 * Nome do arquivo
	 * @var string
	 * @access protected
	 */
	protected $csFileName = "";

	/**
	 * Contrutor
	 *
	 * <p>Instancia e inicializa o objeto que constr�i o arquivo PDF.
	 * A classe FPDF foi alterada para escalar o tamanho de tudo (inclusive fonte).</p>
	 * @param float $pfFactor Fator de escala para ajustar o tamanho do relat�rio
	 * @param integer $piHeaderLines N�mero de linhas do cabe�alho das p�ginas
	 * @param string $psFileName Nome do arquivo (sem o '.pdf')
	 * @access public
	 */
	public function __construct($pfFactor=0.77262693,$piHeaderLines=0, $psFileName='report'){
		$this->coFpdf = new FWDPDF('P',$pfFactor);
		$this->coFpdf->setWriter($this);
		$this->coFpdf->AliasNbPages();
		$this->coFpdf->AddPage();
		$this->coFpdf->SetFont('Arial','',10);
		$this->coFpdf->SetFillColor(255,255,255);
		$this->ciHeaderLines = $piHeaderLines;
		$this->csFileName = strtr($psFileName,'\/:*?"<>|','_________');
	}

	/**
	 * Gera o relat�rio.
	 *
	 * <p>M�todo para gerar o relat�rio.</p>
	 * @access public
	 */
	public function generateReport() {
		$this->coFpdf->Output("{$this->csFileName}.pdf", "D");
		exit();
	}

	/**
	 * Desenha uma linha do relat�rio
	 *
	 * <p>Desenha uma linha do relat�rio (representada por um objeto
	 * FWDReportLine).</p>
	 * @param FWDReportLine $poLine Linha a ser desenhada
	 * @param array $paOffsets Array de offsets dos n�veis superiores
	 * @access public
	 */
	public function drawLine(FWDReportLine $poLine,$paOffsets){
		if(count($this->caHeaders)<$this->ciHeaderLines){
			$this->caHeaders[] = $poLine;
		}
		$miHeight = $poLine->getAttrHeight();

		// Constr�i o array com as informa��es dos deslocamentos pra evitar re-convers�o das cores
		$maOffsetsInfo = array();
		for($i=0;$i<count($paOffsets);$i++){
			$miWidth = $paOffsets[$i]['width'];
			$msBGColor = $paOffsets[$i]['color'];
			$maRgb = FWDColor::nameToRgb($msBGColor);
			if($maRgb==false) $maRgb = FWDColor::hexToRgb($msBGColor);
			if($maRgb==false) $maRgb = array(255,255,255);
			$maOffsetsInfo[] = array(
        'width' => $miWidth,
        'color' => $maRgb
			);
		}

		// Obt�m a cor de fundo da linha
		$msBGColor = $poLine->getAttrBGColor();
		$maBGRgb = FWDColor::nameToRgb($msBGColor);
		if($maBGRgb==false) $maBGRgb = FWDColor::hexToRgb($msBGColor);
		if($maBGRgb==false) $maBGRgb = array(255,255,255);

		$maCells = $poLine->getCells();

		// Se a linha tem espa�amento, renderiza o espa�amento superior
		$miLineSpacing = $poLine->getAttrLineSpacing();
		if($miLineSpacing){
			foreach($maOffsetsInfo as $maOffsetInfo){
				$this->coFpdf->SetFillColor($maOffsetInfo['color'][0],$maOffsetInfo['color'][1],$maOffsetInfo['color'][2]);
				$this->coFpdf->Cell($maOffsetInfo['width'],$miLineSpacing,'',0,0,'L',1);
			}
			$miLineWidth = 0;
			foreach($maCells as $moCell){
				$miLineWidth+= $moCell->getAttrWidth();
			}
			$this->coFpdf->SetFillColor($maBGRgb[0],$maBGRgb[1],$maBGRgb[2]);
			$this->coFpdf->Cell($miLineWidth,$miLineSpacing,'',0,0,'L',1);
			$this->coFpdf->Ln();
		}

		// renderiza os deslocamentos
		foreach($maOffsetsInfo as $maOffsetInfo){
			$this->coFpdf->SetFillColor($maOffsetInfo['color'][0],$maOffsetInfo['color'][1],$maOffsetInfo['color'][2]);
			$this->coFpdf->Cell($maOffsetInfo['width'],$miHeight,'',0,0,'L',1);
		}

		if(count($maCells)>0){
			$miActualLines = 1;
			$maCellsInfo = array();

			$this->coFpdf->SetFillColor($maBGRgb[0],$maBGRgb[1],$maBGRgb[2]);

			// renderiza as c�lulas
			foreach($maCells as $moCell){

				$miMarginLeft = $moCell->getAttrMarginLeft();
				$miMarginRight = $moCell->getAttrMarginRight();
				$miWidth = $moCell->getAttrWidth() - $miMarginLeft - $miMarginRight;

				if($miMarginLeft){
					$this->coFpdf->Cell($miMarginLeft,$miHeight,'',0,0,'L',1);
				}

				if($moCell instanceof FWDReportStatic){
					$msFontStyle = '';
					if($moCell->getAttrBold()) $msFontStyle.= 'B';
					if($moCell->getAttrItalic()) $msFontStyle.= 'I';
					if($moCell->getAttrUnderline()) $msFontStyle.= 'U';
					$msColor = $moCell->getAttrColor();
					$maRgb = FWDColor::nameToRgb($msColor);
					if($maRgb==false) $maRgb = FWDColor::hexToRgb($msColor);
					if($maRgb==false) $maRgb = array(0,0,0);
					$msAlign = $moCell->getAttrAlign();
					if($msAlign=='right') $msAlign = 'R';
					elseif($msAlign=='center') $msAlign = 'C';
					else $msAlign = 'L';

					$moCell->setAttrStringNoEscape(true);

					$msString = strip_tags($moCell->getValue());
					$this->coFpdf->SetFont($moCell->getAttrFontFamily(),$msFontStyle,$moCell->getAttrFontSize());
					$this->coFpdf->SetTextColor($maRgb[0],$maRgb[1],$maRgb[2]);
					$miLines = $this->coFpdf->wordWrap($msString,$miWidth);
					$maString = explode("\n",$msString);
					$msString = $maString[0];
					$miActualLines = max($miActualLines,$miLines);
					$this->coFpdf->Cell($miWidth,$miHeight,$msString,0,0,$msAlign,1);

					$maCellsInfo[] = array(
            'fontFamily' => $moCell->getAttrFontFamily(),
            'fontStyle' => $msFontStyle,
            'fontSize' => $moCell->getAttrFontSize(),
            'textColor' => $maRgb,
            'align' => $msAlign,
            'width' => $moCell->getAttrWidth(),
            'marginleft' => $miMarginLeft,
            'marginright' => $miMarginRight,
            'lines' => $maString
					);

				}elseif($moCell instanceof FWDReportIcon){
					$msExtension = $moCell->getExtension();
					$msSrc = $moCell->getAttrSrc();
					if($msExtension=='gif'){
						$msSrc = substr($msSrc,0,strrpos($msSrc,'.')+1).'png';
						$msExtension = 'png';
					}
					if($msExtension=='jpg' || $msExtension=='jpeg' || $msExtension=='png'){
						$mfCurrentX = $this->coFpdf->GetX();
						$this->coFpdf->Cell($miWidth,$miHeight,'',0,0,'L',1);
						$this->coFpdf->Image($msSrc,$mfCurrentX,$this->coFpdf->GetY(),$miWidth,$miHeight);
					}else{
						trigger_error('PDFWriter only supports the following image formats: jpg, jpeg and png.',E_USER_WARNING);
					}
					$maCellsInfo[] = array('width' => $moCell->getAttrWidth());
				}else{
					trigger_error('This kind of ReportCell is not supported by PDFWriter.',E_USER_WARNING);
				}

				if($miMarginRight){
					$this->coFpdf->Cell($miMarginRight,$miHeight,'',0,0,'L',1);
				}

			}
			$this->coFpdf->Ln();

			// Se alguma coluna teve quebra de linha, renderiza quantas linhas forem necess�rias para terminar a string
			for($i=1;$i<$miActualLines;$i++){
				foreach($maOffsetsInfo as $maOffsetInfo){
					$this->coFpdf->SetFillColor($maOffsetInfo['color'][0],$maOffsetInfo['color'][1],$maOffsetInfo['color'][2]);
					$this->coFpdf->Cell($maOffsetInfo['width'],$miHeight,'',0,0,'L',1);
				}
				foreach($maCellsInfo as $maCellInfo){
					$this->coFpdf->SetFillColor($maBGRgb[0],$maBGRgb[1],$maBGRgb[2]);
					if(isset($maCellInfo['lines']) && isset($maCellInfo['lines'][$i])){
						$miMarginLeft = $maCellInfo['marginleft'];
						$miMarginRight = $maCellInfo['marginright'];
						$miWidth = $maCellInfo['width'] - $miMarginLeft - $miMarginRight;
						$this->coFpdf->SetFont($maCellInfo['fontFamily'],$maCellInfo['fontStyle'],$maCellInfo['fontSize']);
						$this->coFpdf->SetTextColor($maCellInfo['textColor'][0],$maCellInfo['textColor'][1],$maCellInfo['textColor'][2]);
						if($miMarginLeft){
							$this->coFpdf->Cell($miMarginLeft,$miHeight,'',0,0,'L',1);
						}
						$this->coFpdf->Cell($miWidth,$miHeight,$maCellInfo['lines'][$i],0,0,$maCellInfo['align'],1);
						if($miMarginRight){
							$this->coFpdf->Cell($miMarginRight,$miHeight,'',0,0,'L',1);
						}
					}else{
						$this->coFpdf->Cell($maCellInfo['width'],$miHeight,'',0,0,'L',1);
					}
				}
				$this->coFpdf->Ln();
			}

		}

		// renderiza o espa�amento inferior
		if($miLineSpacing){
			foreach($maOffsetsInfo as $maOffsetInfo){
				$this->coFpdf->SetFillColor($maOffsetInfo['color'][0],$maOffsetInfo['color'][1],$maOffsetInfo['color'][2]);
				$this->coFpdf->Cell($maOffsetInfo['width'],$miLineSpacing,'',0,0,'L',1);
			}
			$this->coFpdf->SetFillColor($maBGRgb[0],$maBGRgb[1],$maBGRgb[2]);
			$this->coFpdf->Cell($miLineWidth,$miLineSpacing,'',0,0,'L',1);
			$this->coFpdf->Ln();
		}

	}

	/**
	 * Desenha o cabe�alho da p�gina
	 *
	 * <p>Desenha o cabe�alho da p�gina.</p>
	 * @access public
	 */
	public function drawHeader(){
		foreach($this->caHeaders as $moHeader){
			$this->drawLine($moHeader,array());
		}
	}

}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Componente para auxiliar na gera��o do relat�rio.
 *
 * <p>Componente para auxiliar na gera��o desenho do relat�rio.</p>
 * @package FWD5
 * @subpackage report
 */
class FWDReportGenerator {
	/**
	 * Objeto do relat�rio
	 * @var FWDReport
	 * @access protected
	 */
	protected $coReport = null;

	/**
	 * Filtro do relat�rio
	 * @var FWDReportFilter
	 * @access protected
	 */
	protected $coReportFilter = null;

	/**
	 * Template do relat�rio
	 * @var FWDReportITemplate
	 * @access protected
	 */
	protected $coReportTemplate = null;

	/**
	 * Nome do relat�rio
	 * @var string
	 * @access protected
	 */
	protected $csReportName = "";

	/**
	 * Nome do arquivo do relat�rio
	 * @var string
	 * @access protected
	 */
	protected $csReportFilename = "";

	/**
	 * Seta o relat�rio.
	 *
	 * <p>M�todo para setar o relat�rio.</p>
	 * @access public
	 * @param FWDReport $poReport Objeto do relat�rio
	 */
	public function setReport(FWDReport $poReport) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		$this->coReport = $poReport;
	}

	/**
	 * Seta o filtro relat�rio.
	 *
	 * <p>M�todo para setar o filtro relat�rio.</p>
	 * @access public
	 * @param FWDReport $poReportFilter Filtro do relat�rio
	 */
	public function setReportFilter(FWDReportFilter $poReportFilter) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		$this->coReportFilter = $poReportFilter;
	}

	/**
	 * Seta o template relat�rio.
	 *
	 * <p>M�todo para setar o template relat�rio.</p>
	 * @access public
	 * @param FWDReportITemplate $poReportTemplate Template do relat�rio
	 */
	public function setReportTemplate(FWDReportITemplate $poReportTemplate) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		$this->coReportTemplate = $poReportTemplate;
	}

	/**
	 * Seta o nome relat�rio.
	 *
	 * <p>M�todo para setar o nome relat�rio.</p>
	 * @access public
	 * @param string $psReportName Nome do relat�rio
	 */
	public function setReportName($psReportName) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		$this->csReportName = $psReportName;
	}

	/**
	 * Seta o nome do arquivo do relat�rio.
	 *
	 * <p>M�todo para setar o nome do arquivo do relat�rio.</p>
	 * @access public
	 * @param string $psFilename Nome do arquivo do relat�rio
	 */
	public function setReportFilename($psFilename) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		$this->csReportFilename = $psFilename;
	}

	/**
	 * Retorna os tipos de formatos dispon�veis para relat�rio.
	 *
	 * <p>M�todo para retornar os tipos de formatos dispon�veis para relat�rio.</p>
	 * @access public
	 * @return array Formatos dispon�veis de relat�rios (Ex: array(id => 'nome', id2 => 'nome2'))
	 */
	public static function getAvailableFormats($paSupported = array()) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		$maReps = array();
		$mbAllSupported = count($paSupported) ? false : true;
		if ($mbAllSupported || in_array(REPORT_FILETYPE_HTML, $paSupported)) $maReps[REPORT_FILETYPE_HTML] = FWDLanguage::getPHPStringValue('report_html', 'HTML');
		if ($mbAllSupported || in_array(REPORT_FILETYPE_HTML_DOWNLOAD, $paSupported)) $maReps[REPORT_FILETYPE_HTML_DOWNLOAD] = FWDLanguage::getPHPStringValue('report_html_download', 'HTML Download');
		if ($mbAllSupported || in_array(REPORT_FILETYPE_WORD, $paSupported)) $maReps[REPORT_FILETYPE_WORD] = FWDLanguage::getPHPStringValue('report_word', 'Word');
		if ($mbAllSupported || in_array(REPORT_FILETYPE_EXCEL, $paSupported)) $maReps[REPORT_FILETYPE_EXCEL] = FWDLanguage::getPHPStringValue('report_excel', 'Excel');
		if ($mbAllSupported || in_array(REPORT_FILETYPE_PDF, $paSupported)) $maReps[REPORT_FILETYPE_PDF] = FWDLanguage::getPHPStringValue('report_pdf', 'PDF');
		return $maReps;
	}

	/**
	 * Retorna os tipos de classifica��es dispon�veis para relat�rio.
	 *
	 * <p>M�todo para retornar os tipos de classifica��es dispon�veis para relat�rio.</p>
	 * @access public
	 * @return array Classifica��es dispon�veis de relat�rios (Ex: array(id => 'classifica��o', id2 => 'classifica��o2'))
	 */
	public static function getAvailableClassifications() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		return array(
		REPORT_CLASS_CONFIDENTIAL => FWDLanguage::getPHPStringValue('report_confidential_report', 'Relat�rio Confidencial'),
		REPORT_CLASS_INSTITUTIONAL => FWDLanguage::getPHPStringValue('report_institutional_report', 'Relat�rio Institucional'),
		REPORT_CLASS_PUBLIC => FWDLanguage::getPHPStringValue('report_public_report', 'Relat�rio P�blico')
		);
	}

	/**
	 * M�todo para gerar o relat�rio.
	 *
	 * <p>M�todo para gerar o relat�rio.</p>
	 * @access public
	 */
	public function generate() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		/*
		 * Seta os atributos necess�rios para preencher o template.
		 */
		$this->coReportTemplate->setReportType($this->csReportName);
		$this->coReportTemplate->setReportPrivacy($this->coReportFilter->getClassificationName());
		$this->coReportTemplate->setReportComment($this->coReportFilter->getComment());

		/*
		 * Adiciona os headers do template no in�cio do relat�rio e o footer no final.
		 */
		$maHeaders = $this->coReportTemplate->getHeaders();
		foreach($maHeaders as $moHeader){
			$this->coReport->addObjFWDReportLine($moHeader,"header",true);
		}
		$this->coReport->addObjFWDReportLine($this->coReportTemplate->getFooter(), "footer");

		/*
		 * Seta o filtro e o tamanho da janela do relat�rio
		 */
		$this->coReport->setFilter($this->coReportFilter);
		$this->coReport->setObjFWDBox(new FWDBox(0,0,600,700));

		// Seta o limite de linhas
		$this->coReport->setAttrRowLimit($this->coReportFilter->getRowLimit());

		/*
		 * Gera o relat�rio de acordo com o tipo especificado.
		 */
		$msFileName = $this->csReportFilename ? $this->csReportFilename : $this->csReportName;
		$msFileName.= "-".date('Ymd');
		$msFactor = 0.75;
		$mbHTMLDownload = false;
		switch ($this->coReportFilter->getFileType()){
			case REPORT_FILETYPE_HTML_DOWNLOAD:
				$mbHTMLDownload = true;
			case REPORT_FILETYPE_HTML:
				$moWriter = new FWDReportHTMLWriter($msFileName);
				$moWriter->setIsForDownload($mbHTMLDownload);
				$this->coReport->setWriter($moWriter);
				$this->coReport->draw();
				break;
			case REPORT_FILETYPE_WORD:
				$this->coReport->setWriter(new FWDReportDocWriter($msFileName));
				$this->coReport->draw();
				break;
			case REPORT_FILETYPE_EXCEL:
				$this->coReport->init();
				$this->coReport->makeQuery();
				$moDataSetExcel = new FWDDataSetExcel($msFileName,$this->coReport->getDataSet());
				$moDataSetExcel->execute();
				break;
			case REPORT_FILETYPE_PDF:
				$this->coReport->setWriter(new FWDReportPDFWriter($msFactor,count($maHeaders)-1,$msFileName));
				$this->coReport->draw();
				break;
		}
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Interface FWDReportITemplate.
 *
 * <p>Interface para implementar o template default para header e footer de relat�rios.</p>
 * @package FWD5
 * @subpackage report
 */
interface FWDReportITemplate {
	/**
	 * Seta o nome e o ip do gestor.
	 *
	 * <p>Seta o nome e o ip do gestor.</p>
	 * @access public
	 */
	public function setManager();

	/**
	 * Seta a data do relat�rio.
	 *
	 * <p>Seta a data do relat�rio.</p>
	 * @access public
	 */
	public function setDate();

	/**
	 * Seta a o tipo do relat�rio.
	 *
	 * <p>Seta o tipo do relat�rio.</p>
	 * @access public
	 * @param string $psValue Tipo do relat�rio
	 */
	public function setReportType($psValue);

	/**
	 * Seta um coment�rio para o relat�rio.
	 *
	 * <p>Seta um coment�rio para o relat�rio.</p>
	 * @access public
	 * @param string $psValue Coment�rio
	 */
	public function setReportComment($psValue);

	/**
	 * Seta a classifica��o do relat�rio.
	 *
	 * <p>Seta a classifica��o do relat�rio.</p>
	 * @access public
	 * @param string $psValue Classifica��o do relat�rio
	 */
	public function setReportPrivacy($psValue);

	/**
	 * Retorna os Headers do relat�rio.
	 *
	 * <p>Retorna os Headers do relat�rio.</p>
	 * @access public
	 * @return array Headers do relat�rio
	 */
	public function getHeaders();

	/**
	 * Retorna o Footer do relat�rio.
	 *
	 * <p>Retorna o Footer do relat�rio.</p>
	 * @access public
	 * @return FWDReportLine Footer do relat�rio
	 */
	public function getFooter();
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

define("REPORT_CLASS_NONE", 7500);
define("REPORT_CLASS_CONFIDENTIAL", 7501);
define("REPORT_CLASS_INSTITUTIONAL", 7502);
define("REPORT_CLASS_PUBLIC", 7503);

define("REPORT_FILETYPE_HTML_DOWNLOAD", 7300);
define("REPORT_FILETYPE_HTML", 7301);
define("REPORT_FILETYPE_WORD", 7302);
define("REPORT_FILETYPE_EXCEL", 7303);
define("REPORT_FILETYPE_PDF", 7304);

/**
 * Classe FWDReportClassificator.
 *
 * <p>Classe para classifica��o de relat�rios.</p>
 * @package FWD5
 * @subpackage report
 */
class FWDReportClassificator {

	/**
	 * Tipo de classifica��o
	 * @var integer
	 * @access protected
	 */
	protected $ciClassification = REPORT_CLASS_CONFIDENTIAL;

	/**
	 * Tipo de arquivo
	 * @var integer
	 * @access protected
	 */
	protected $ciFileType = REPORT_FILETYPE_HTML;

	/**
	 * Array de tipos poss�veis de classifica��o
	 * @var array
	 * @access protected
	 */
	protected $caClassificationArray = array();

	/**
	 * Array de tipos poss�veis de arquivos para relat�rios
	 * @var array
	 * @access protected
	 */
	protected $caFileTypeArray = array();

	/**
	 * A quem se destina o relat�rio
	 * @var string
	 * @access protected
	 */
	protected $csDest = '';

	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDReportClassificator. Seta os valores
	 * dos tipos de classifica��o dos relat�rios, bem como os poss�veis
	 * tipos de arquivos para relat�rios.</p>
	 * @access public
	 */
	public function __construct() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		$this->caClassificationArray =
		array(
		REPORT_CLASS_NONE => '',
		REPORT_CLASS_CONFIDENTIAL	=> FWDLanguage::getPHPStringValue('report_confidencial', "Relat�rio Confidencial"),
		REPORT_CLASS_INSTITUTIONAL => FWDLanguage::getPHPStringValue('report_institutional', "Relat�rio Institucional"),
		REPORT_CLASS_PUBLIC => FWDLanguage::getPHPStringValue('report_public', "Relat�rio P�blico")
		);
		$this->caFileTypeArray =
		array(
		REPORT_FILETYPE_HTML_DOWNLOAD,
		REPORT_FILETYPE_HTML,
		REPORT_FILETYPE_WORD,
		REPORT_FILETYPE_EXCEL,
		REPORT_FILETYPE_PDF
		);
	}

	/**
	 * Seta o tipo de classifica��o.
	 *
	 * <p>Seta o tipo de classifica��o.</p>
	 * @access public
	 * @param integer $piClassification Tipo de classifica��o
	 */
	public function setClassification($piClassification) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		if (!array_key_exists($piClassification,$this->caClassificationArray))
		trigger_error("Invalid Report Classification: $piClassification.",E_USER_ERROR);
		$this->ciClassification = $piClassification;
	}

	/**
	 * Seta a classifica��o manualmente.
	 *
	 * <p>Seta a classifica��o manualmente.</p>
	 * @access public
	 * @param string $psClassification Classifica��o manual do relat�rio
	 */
	public function setManualClassification($psClassification) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		$this->caClassificationArray[0] = $psClassification;
		$this->ciClassification = 0;
	}

	/**
	 * Retorna o tipo de classifica��o.
	 *
	 * <p>Retorna o tipo de classifica��o.</p>
	 * @access public
	 * @return integer Tipo de classifica��o
	 */
	public function getClassification() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		return $this->ciClassification;
	}

	/**
	 * Seta o tipo de arquivo.
	 *
	 * <p>Seta o tipo de arquivo.</p>
	 * @access public
	 * @param integer $piFileType Tipo de arquivo
	 */
	public function setFileType($piFileType) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		if (!in_array($piFileType,$this->caFileTypeArray))
		trigger_error("Invalid Report FileType: $piFileType.",E_USER_ERROR);
		$this->ciFileType = $piFileType;
	}

	/**
	 * Retorna o tipo de arquivo.
	 *
	 * <p>Retorna o tipo de arquivo.</p>
	 * @access public
	 * @return integer Tipo de arquivo
	 */
	public function getFileType() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		return $this->ciFileType;
	}

	/**
	 * Retorna o array com os tipos poss�veis de classifica��o.
	 *
	 * <p>Retorna o array com os tipos poss�veis de classifica��o.</p>
	 * @access public
	 * @return array Tipos poss�veis de classifica��o
	 */
	public function getClassificationArray() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		return $this->caClassificationArray;
	}

	/**
	 * Retorna o array com os tipos poss�veis de arquivos.
	 *
	 * <p>Retorna o array com os tipos poss�veis de arquivos.</p>
	 * @access public
	 * @return array Tipos poss�veis de arquivos
	 */
	public function getFileTypeArray() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		return $this->caFileTypeArray;
	}

	/**
	 * Retorna o nome da classifica��o.
	 *
	 * <p>Retorna o nome da classifica��o.</p>
	 * @access public
	 * @return string Nome do tipo de classifica��o
	 */
	public function getClassificationName() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		if(!array_key_exists($this->ciClassification,$this->caClassificationArray))
		trigger_error("Invalid Report Classification: $this->ciClassification.",E_USER_ERROR);
		$msReturn = $this->caClassificationArray[$this->ciClassification];
		if ($msReturn && $this->csDest) {
			$msReturn .= ": ".$this->csDest;
		}
		return $msReturn;
	}

	/**
	 * Seta para quem se destina o relat�rio.
	 *
	 * <p>Seta para quem se destina o relat�rio.</p>
	 * @access public
	 * @param string $psTo Para quem se destina o relat�rio
	 */
	public function setDest($psDest) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		$this->csDest = $psDest;
	}

	/**
	 * Retorna para quem se destina o relat�rio.
	 *
	 * <p>Retorna para quem se destina o relat�rio.</p>
	 * @access public
	 * @return string para quem se destina o relat�rio
	 */
	public function getDest() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		return $this->csDest;
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDReportFilter.
 *
 * <p>Classe para filtragem de relat�rios.</p>
 * @package FWD5
 * @subpackage report
 */
class FWDReportFilter {

	/**
	 * Coment�rio
	 * @var string
	 * @access protected
	 */
	protected $csComment = '';

	/**
	 * Classificador
	 * @var ISMSReportClassificator
	 * @access protected
	 */
	protected $coClassificator = null;

	/**
	 * N�mero m�ximo de linhas a retornar da consulta
	 * @var integer
	 * @access protected
	 */
	protected $ciRowLimit;

	protected $caLevelsToSuppress = array();

	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDReportFilter.</p>
	 * @access public
	 */
	public function __construct() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		$this->coClassificator = new FWDReportClassificator();
	}

	/**
	 * Seta a classifica��o do relat�rio.
	 *
	 * <p>Seta a classifica��o do relat�rio.</p>
	 * @access public
	 * @param integer $piClassification Classifica��o do relat�rio
	 */
	public function setClassification($piClassification) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		$this->coClassificator->setClassification($piClassification);
	}

	/**
	 * Seta manualmente a classifica��o do relat�rio.
	 *
	 * <p>Seta manualmente a classifica��o do relat�rio.</p>
	 * @access public
	 * @param string $psClassification Classifica��o manual do relat�rio
	 */
	public function setManualClassification($psClassification) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		$this->coClassificator->setManualClassification($psClassification);
	}

	/**
	 * Retorna o nome da classifica��o do relat�rio.
	 *
	 * <p>Retorna o nome da classifica��o do relat�rio.</p>
	 * @access public
	 * @return string Nome da classifica��o do relat�rio
	 */
	public function getClassificationName() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		return $this->coClassificator->getClassificationName();
	}

	/**
	 * Seta o tipo de arquivo do relat�rio.
	 *
	 * <p>Seta o tipo de arquivo do relat�rio.</p>
	 * @access public
	 * @param integer $piFileType Tipo de arquivo do relat�rio
	 */
	public function setFileType($piFileType) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		$this->coClassificator->setFileType($piFileType);
	}

	/**
	 * Retorna o tipo de arquivo do relat�rio.
	 *
	 * <p>Retorna o tipo de arquivo do relat�rio.</p>
	 * @access public
	 * @return integer Tipo de arquivo do relat�rio
	 */
	public function getFileType() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		return $this->coClassificator->getFileType();
	}

	/**
	 * Seta o coment�rio do relat�rio.
	 *
	 * <p>Seta o coment�rio do relat�rio.</p>
	 * @access public
	 * @param string $psComment Coment�rio do relat�rio
	 */
	public function setComment($psComment) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		$this->csComment = $psComment;
	}

	/**
	 * Retorna o coment�rio do relat�rio.
	 *
	 * <p>Retorna o coment�rio do relat�rio.</p>
	 * @access public
	 * @return string coment�rio do relat�rio
	 */
	public function getComment() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		return $this->csComment;
	}

	/**
	 * Retorna o n�mero m�ximo de linhas que deve retornar da consulta
	 *
	 * <p>Retorna o n�mero m�ximo de linhas que deve retornar da consulta.</p>
	 * @access public
	 * @return integer N�mero m�ximo de linhas que deve retornar da consulta
	 */
	public function getRowLimit(){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		return $this->ciRowLimit;
	}

	/**
	 * Seta o n�mero m�ximo de linhas que deve retornar da consulta
	 *
	 * <p>Seta o n�mero m�ximo de linhas que deve retornar da consulta.</p>
	 * @access public
	 * @param integer $piRowLimit N�mero m�ximo de linhas que deve retornar da consulta
	 */
	public function setRowLimit($piRowLimit){
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		$this->ciRowLimit = $piRowLimit;
	}

	/**
	 * Indica que um n�vel deve ser suprimido.
	 *
	 * <p>Indica que um n�vel deve ser suprimido.</p>
	 * @access public
	 * @param string $psLevel Nome do n�vel
	 */
	public function suppressLevel($psLevel){
		$this->caLevelsToSuppress[$psLevel] = 1;
	}

	/**
	 * Indica que um n�vel n�o deve ser suprimido.
	 *
	 * <p>Indica que um n�vel n�o deve ser suprimido.</p>
	 * @access public
	 * @param string $psLevel Nome do n�vel
	 */
	public function unsuppressLevel($psLevel){
		unset($this->caLevelsToSuppress[$psLevel]);
	}

	/**
	 * Indica se um n�vel deve ou n�o ser suprimido.
	 *
	 * <p>Indica se um n�vel deve ou n�o ser suprimido.</p>
	 * @access public
	 * @return boolean True, sse o n�vel deve ser suprimido
	 */
	public function levelIsSuppressed($psLevel){
		return isset($this->caLevelsToSuppress[$psLevel]);
	}

	/**
	 * Seta para quem se destina o relat�rio.
	 *
	 * <p>Seta para quem se destina o relat�rio.</p>
	 * @access public
	 * @param string $psTo Para quem se destina o relat�rio
	 */
	public function setDest($psDest) {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		$this->coClassificator->setDest($psDest);
	}

	/**
	 * Retorna para quem se destina o relat�rio.
	 *
	 * <p>Retorna para quem se destina o relat�rio.</p>
	 * @access public
	 * @return string para quem se destina o relat�rio
	 */
	public function getDest() {
		$maParameters = func_get_args();
		FWDWebLib::getInstance()->writeFunction2Debug(__CLASS__,__FUNCTION__,$maParameters,FWD_DEBUG_FWD2,__FILE__,__LINE__);
		return $this->coClassificator->getDest();
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Interface FWDSuppressModel.
 *
 * <p>Interface para definir classes que fazem os ajustes em um relat�rio quando
 * um de seus n�veis � suprimido.</p>
 * @package FWD5
 * @subpackage report
 */
interface FWDSuppressModel {

	/**
	 * Faz os ajustes necess�rios.
	 *
	 * <p>Faz os ajustes necess�rios.</p>
	 * @access public
	 * @param FWDReportLevel $poReportLevel N�vel suprimido
	 */
	public function makeAdjusts(FWDReportLevel $poReportLevel);

}

?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDLiquidCellsSuppressModel.
 *
 * <p>Classe de SuppressModel que ajusta as larguras das c�lulas para ocupar o
 * espa�o deixado pelo offset do n�vel suprimido.</p>
 * @package FWD5
 * @subpackage report
 */
class FWDLiquidCellsSuppressModel implements FWDSuppressModel {

	/**
	 * Array de arrays de ids de c�lulas l�quidas. Cada sub-array cont�m os ids das c�lulas de uma mesma linha.
	 * @var array
	 * @access protected
	 */
	protected $caLiquidCells = array();

	/**
	 * Adiciona c�lulas l�quidas
	 *
	 * <p>Adiciona c�lulas l�quidas de uma mesma linha.</p>
	 * @access public
	 * @param string $psIdK Id de uma c�lula
	 */
	public function addCells(/* $psId1, $psId2, ... , $psIdN */){
		$this->caLiquidCells[] = func_get_args();
	}

	/**
	 * Faz os ajustes necess�rios.
	 *
	 * <p>Distribui igualmente o offset entre as c�lulas l�quidas de cada linha.</p>
	 * @access public
	 * @param FWDReportLevel $poReportLevel N�vel suprimido
	 */
	public function makeAdjusts(FWDReportLevel $poReportLevel){
		$moSubLevel = $poReportLevel->getSubLevel();
		if($moSubLevel){
			$miOffset = $moSubLevel->getOffset();
			foreach($this->caLiquidCells as $maLineCells){
				$miCells = count($maLineCells);
				$miCellOffset = floor($miOffset / $miCells);
				$miRest = $miOffset % $miCells;
				foreach($maLineCells as $msCell){
					$moCell = FWDWebLib::getObject($msCell);
					$miWidth = $moCell->getAttrWidth() + $miCellOffset;
					if($miRest--){
						$moCell->setAttrWidth($miWidth + 1);
					}else{
						$moCell->setAttrWidth($miWidth);
					}
				}
			}
		}
	}

}

?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDStringList. Descreve o tipo de dados StringList.
 *
 * <p>Classe que descreve o tipo de dados StringList. Uma StringList � uma lista
 * de strings identificadas pelo seu id organizadas pelos nomes dos arquivos de
 * onde elas foram extra�das.</p>
 * @abstract
 * @package FWD5
 * @subpackage translation
 */
class FWDStringList {

	/**
	 * Array que armazena a StringList em si
	 * @var array
	 * @access protected
	 */
	protected $caList;

	/**
	 * Construtor da FWDStringList.
	 *
	 * <p>Construtor da FWDStringList. Inicializa a StringList com uma lista vazia.</p>
	 * @access public
	 */
	public function __construct(){
		$this->caList = array();
	}

	/**
	 * Adiciona um arquivo � StringList.
	 *
	 * <p>Adiciona um arquivo � StringList.</p>
	 * @access public
	 * @param string $psFileName Nome do arquivo a ser adicionado
	 */
	public function addFile($psFileName){
		$this->caList[$psFileName] = array();
	}

	/**
	 * Remove um arquivo da StringList.
	 *
	 * <p>Remove um arquivo da StringList.</p>
	 * @access public
	 * @param string $psFileName Nome do arquivo a ser removido
	 */
	public function removeFile($psFileName){
		if(isset($this->caList[$psFileName])){
			unset($this->caList[$psFileName]);
		}else{
			trigger_error("Trying to remove inexistent file '$psFileName'.",E_USER_WARNING);
		}
	}

	/**
	 * Adiciona uma string � StringList.
	 *
	 * <p>Adiciona uma string � StringList.</p>
	 * @access public
	 * @param string $psFileName Nome do arquivo a que a string pertence
	 * @param string $psStringId Identificador da string a ser adicionada
	 * @param string $psStringValue Valor da string a ser adicionada
	 */
	public function addString($psFileName,$psStringId,$psStringValue){
		if(isset($this->caList[$psFileName])){
			$this->caList[$psFileName][$psStringId] = $psStringValue;
		}else{
			$this->caList[$psFileName] = array($psStringId => $psStringValue);
		}
	}

	/**
	 * Remove uma string da StringList.
	 *
	 * <p>Remove uma string da StringList.</p>
	 * @access public
	 * @param string $psFileName Nome do arquivo a que a string pertence
	 * @param string $psStringId Identificador da string a ser removida
	 */
	public function removeString($psFileName,$psStringId){
		if(isset($this->caList[$psFileName])){
			unset($this->caList[$psFileName][$psStringId]);
		}else{
			trigger_error("Trying to remove string from inexistent file '$psFileName'.",E_USER_WARNING);
		}
	}

	/**
	 * Seta o valor de uma string da StringList.
	 *
	 * <p>Seta o valor de uma string da StringList.</p>
	 * @access public
	 * @param string $psFileName Nome do arquivo a que a string pertence
	 * @param string $psStringId Identificador da string
	 * @param string $psStringValue Novo valor da string
	 */
	public function setString($psFileName,$psStringId,$psStringValue){
		if(isset($this->caList[$psFileName])){
			$this->caList[$psFileName][$psStringId] = $psStringValue;
		}else{
			trigger_error("Trying to set a string from inexistent file '$psFileName'.",E_USER_WARNING);
		}
	}

	/**
	 * Retorna o valor de uma string da StringList.
	 *
	 * <p>Retorna o valor de uma string da StringList.</p>
	 * @access public
	 * @param string $psFileName Nome do arquivo a que a string pertence
	 * @param string $psStringId Identificador da string
	 */
	public function getString($psFileName,$psStringId){
		if(isset($this->caList[$psFileName]) && isset($this->caList[$psFileName][$psStringId])){
			return $this->caList[$psFileName][$psStringId];
		}else{
			return false;
		}
	}

	/**
	 * Retorna um array associativo contendo todas as strings da StringList.
	 *
	 * <p>Retorna um array associativo contendo todas as strings da StringList.</p>
	 * @access public
	 * @return array Array associativo contendo todas as strings
	 */
	public function dump(){
		return $this->caList;
	}

}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Interface FWDTranslationDriver. Interface para classes que sabem ler e escrever StringList's
 *
 * <p>Interface para classes que sabem ler e escrever StringList's em algum meio
 * de armazenagem. Esses drivers s�o usados pela classe FWDTranslate.</p>
 *
 * @package FWD5
 * @subpackage translation
 */
interface FWDTranslationDriver {

	/**
	 * L� uma StringList.
	 *
	 * <p>L� uma StringList a partir do meio de armazenagem.</p>
	 * @access public
	 * @param string $psLanguage L�ngua que deve ser carregada do banco
	 * @return FWDStringList StringList lida
	 */
	public function load($psLanguage = "");

	/**
	 * Armazena uma StringList.
	 *
	 * <p>Armazena uma StringList no meio de armazenagem.</p>
	 * @access public
	 * @param FWDStringList $poStringList StringList a ser armazenada
	 */
	public function store(FWDStringList $poStringList);

}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDXMLDriver. Driver para ler e escrever StringList's em arquivos XML.
 *
 * <p>Driver para ler e escrever StringList's em arquivos XML.</p>
 *
 * @package FWD5
 * @subpackage translation
 */
class FWDXMLDriver extends FWDXMLParser implements FWDTranslationDriver {

	/**
	 * Arquivo associado ao driver
	 * @var string
	 * @access protected
	 */
	protected $csFileName;

	/**
	 * Usado durante o parse do arquivo
	 * @var string
	 * @access protected
	 */
	protected $csCurrentFile = '';

	/**
	 * Usado durante o parse do arquivo
	 * @var string
	 * @access protected
	 */
	protected $csCurrentString = '';

	/**
	 * StringList interna ao objeto. Usada durante as opera��es de leitura e escrita.
	 * @var FWDStringList
	 * @access protected
	 */
	protected $coStringList = null;

	/**
	 * Construtor da classe FWDXMLDriver.
	 *
	 * <p>Construtor da classe FWDXMLDriver. Associa um arquivo XML ao driver.</p>
	 * @access public
	 * @param string $psFileName Arquivo a ser associado ao driver
	 */
	public function __construct($psFileName){
		$this->csFileName = $psFileName;
	}

	/**
	 * Modifica o arquivo associado ao driver.
	 *
	 * <p>Modifica o arquivo associado ao driver.</p>
	 * @access public
	 * @param string $psFileName Arquivo a ser associado ao driver
	 */
	public function setFileName($psFileName){
		$this->csFileName = $psFileName;
	}

	/**
	 * L� uma StringList.
	 *
	 * <p>L� uma StringList a partir do arquivo XML associado ao objeto.</p>
	 * @access public
	 * @param string $psLanguage L�ngua que deve ser carregada do banco (N�o utilizada)
	 * @return FWDStringList StringList lida
	 */
	public function load($psLanguage = ""){
		$this->coStringList = new FWDStringList();
		if(!$this->parseFile($this->csFileName)){
			$this->coStringList = null;
		}
		return $this->coStringList;
	}

	/**
	 * Armazena uma StringList.
	 *
	 * <p>Armazena uma StringList no arquivo XML associado ao objeto.</p>
	 * @access public
	 * @param FWDStringList $poStringList StringList a ser armazenada
	 */
	public function store(FWDStringList $poStringList){
		if(!is_writable($this->csFileName)){
			trigger_error("Could not write to file '{$this->csFileName}'.",E_USER_ERROR);
			return false;
		}
		$msOutput = "<?xml version='1.0' encoding='ISO-8859-1'?>\n<root>\n";
		foreach($poStringList->dump() as $msFileName=>$maStrings){
			if(count($maStrings)>0){
				$msOutput.= "<file name='$msFileName'>\n";
				foreach($maStrings as $msId=>$msValue){
					$msOutput.= "  <string id='$msId'>$msValue</string>\n";
				}
				$msOutput.= "</file>\n";
			}
		}
		$msOutput.= "</root>";
		$mrFile = fopen($this->csFileName,'w');
		if($mrFile){
			fwrite($mrFile,$msOutput);
			fclose($mrFile);
		}
	}

	/**
	 * Handler chamado quando o parser encontra uma tag de in�cio de elemento
	 *
	 * <p>Handler chamado quando o parser encontra uma tag de in�cio de elemento.</p>
	 * @access protected
	 * @param resource $prParser Parser
	 * @param string $psTagName Nome da tag
	 * @param array $paAttributes Array associativo contendo os atributos do elemento
	 */
	protected function startHandler($prParser, $psTagName, $paAttributes){
		if($psTagName=='FILE'){
			$this->csCurrentFile = $paAttributes['NAME'];
			$this->coStringList->addFile($this->csCurrentFile);
		}elseif($psTagName=='STRING'){
			$this->csCurrentString = $paAttributes['ID'];
			$this->coStringList->addString($this->csCurrentFile,$this->csCurrentString,'');
		}
	}

	/**
	 * Handler chamado quando o parser encontra texto dentro de um elemento
	 *
	 * <p>Handler chamado quando o parser encontra texto dentro de um elemento.</p>
	 * @access protected
	 * @param resource $prParser Parser
	 * @param string $psData Conte�do de texto do elemento
	 */
	protected function dataHandler($prParser, $psData){
		if($this->csCurrentString!=''){
			$msString = $this->coStringList->getString($this->csCurrentFile,$this->csCurrentString);
			$msString.= $psData;
			$this->coStringList->setString($this->csCurrentFile,$this->csCurrentString,$msString);
		}
	}

	/**
	 * Handler chamado quando o parser encontra uma tag de fim de elemento
	 *
	 * <p>Handler chamado quando o parser encontra uma tag de fim de elemento.</p>
	 * @access protected
	 * @param resource $prParser Parser
	 * @param string $psTagName Nome da tag
	 */
	protected function endHandler($prParser, $psTagName){
		if($psTagName=='FILE'){
			$this->csCurrentFile = '';
		}elseif($psTagName=='STRING'){
			$msString = $this->coStringList->getString($this->csCurrentFile,$this->csCurrentString);
			$msString = trim(preg_replace('/[\s\n]+/',' ',$msString));
			$this->coStringList->setString($this->csCurrentFile,$this->csCurrentString,$msString);
			$this->csCurrentString = '';
		}
	}

}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDDBDriver. Driver para ler e escrever StringList's em banco.
 *
 * <p>Driver para ler e escrever StringList's em banco.</p>
 *
 * @package FWD5
 * @subpackage translation
 */
class FWDDBDriver implements FWDTranslationDriver {

	/**
	 * DataSet para ler e escrever no banco
	 * @var FWDDBDataSet
	 * @access protected
	 */
	protected $coDataSet = null;

	/**
	 * StringList interna ao objeto. Usada durante as opera��es de leitura e escrita.
	 * @var FWDStringList
	 * @access protected
	 */
	protected $coStringList = null;

	/**
	 * Boolean indica ao objeto se as strings que devem ser inseridas no banco s�o provenientes
	 * do php ou de outra fonte.
	 * @var cbStringsFromPHP
	 * @access protected
	 */
	protected $cbStringsFromPHP = false;
	/**
	 * Construtor da classe FWDDBDriver.
	 *
	 * <p>Construtor da classe FWDDBDriver. Associa um DataSet ao driver.</p>
	 * @access public
	 * @param FWDDBDataSet $poDataSet DataSet a ser associado ao driver
	 */
	public function __construct(FWDDBDataSet $poDataSet,$pbStringsFromPHP = false){
		$this->coDataSet = $poDataSet;
		$this->cbStringsFromPHP = $pbStringsFromPHP;
	}

	/**
	 * Modifica o DataSet associado ao driver.
	 *
	 * <p>Modifica o DataSet associado ao driver.</p>
	 * @access public
	 * @param FWDDBDataSet $poDataSet DataSet a ser associado ao driver
	 */
	public function setDataSet(FWDDBDataSet $poDataSet){
		$this->coDataSet = $poDataSet;
	}

	/**
	 * L� uma StringList.
	 *
	 * <p>L� uma StringList a partir do DataSet.</p>
	 * @access public
	 * @param string $psLanguage L�ngua que deve ser carregada do banco
	 * @return FWDStringList StringList lida
	 */
	public function load($psLanguage = ""){
		$this->coStringList = new FWDStringList();
		$this->coDataSet->select();
		while($this->coDataSet->fetch()){
			$msFileName = trim($this->coDataSet->getFieldByAlias('string_file')->getValue());
			$msId = trim($this->coDataSet->getFieldByAlias('string_id')->getValue());
			if ($psLanguage) $msValue = trim($this->coDataSet->getFieldByAlias('string_'.$psLanguage)->getValue());
			else $msValue = trim($this->coDataSet->getFieldByAlias('string_value')->getValue());
			$this->coStringList->addString($msFileName,$msId,$msValue);
		}
		return $this->coStringList;
	}

	/**
	 * Armazena uma StringList.
	 *
	 * <p>Armazena uma StringList no banco, atrav�s do DataSet.</p>
	 * @access public
	 * @param FWDStringList $poStringList StringList a ser armazenada
	 */
	public function store(FWDStringList $poStringList){
		$this->coDataSet->getFieldByAlias('string_file')->clearFilters();
		$this->coDataSet->getFieldByAlias('string_id')->clearFilters();
		$this->coDataSet->getFieldByAlias('string_value')->clearFilters();
		$this->coDataSet->getFieldByAlias('string_date_inclusion')->clearFilters();
		$this->coDataSet->getFieldByAlias('string_date_inclusion')->setValue(time());
		foreach($poStringList->dump() as $msFileName=>$maStrings){
			if(count($maStrings)>0){
				$moFieldFile = $this->coDataSet->getFieldByAlias('string_file');
				$moFieldFile->setValue($msFileName);
				foreach($maStrings as $msId=>$msValue){
					$moFieldId = $this->coDataSet->getFieldByAlias('string_id');
					$moFieldId->clearFilters();
					$moFieldId->addFilter(new FWDDBFilter('=',$msId));
					$moFieldId->setValue($msId);

					if($this->cbStringsFromPHP)
					$this->coDataSet->getFieldByAlias('string_value')->setValue(utf8_encode($msValue), true,true);
					else
					$this->coDataSet->getFieldByAlias('string_value')->setValue($msValue, true,true);

					$this->coDataSet->select();
					if($this->coDataSet->getRowCount() == 0){
						$this->coDataSet->insert();
					}else{
						$this->coDataSet->update();
					}
				}
			}
		}
		$this->coDataSet->getFieldByAlias('string_id')->clearFilters();
	}

}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDTranslate. Classe para ajudar na tradu��o das strings.
 *
 * <p>Classe para ajudar na tradu��o das strings. Ela extrai listas de strings
 * de arquivos XML, exporta e importa essas listas.</p>
 *
 * @package FWD5
 * @subpackage translation
 */
class FWDTranslate extends FWDXMLParser {

	/**
	 * Driver para fazer a leitura e/ou a escrita de StringList's em um meio de armazenamento
	 * @var FWDTranslationDriver
	 * @access protected
	 */
	protected $coDriver;

	/**
	 * StringList interna ao objeto. Usada durante as opera��es de leitura e escrita.
	 * @var FWDStringList
	 * @access protected
	 */
	protected $coStringList = null;

	/**
	 * Usado durante o parse do arquivo
	 * @var string
	 * @access protected
	 */
	protected $csFileBeingParsed = '';

	/**
	 * Usado durante o parse do arquivo
	 * @var array
	 * @access protected
	 */
	protected $caCurrentString = null;

	/**
	 * Construtor da classe FWDTranslate.
	 *
	 * <p>Construtor da classe FWDTranslate. Associa um driver ao objeto.</p>
	 * @access public
	 * @param FWDTranslationDriver $poDriver Driver a ser associado ao objeto
	 */
	public function __construct(FWDTranslationDriver $poDriver){
		$this->coDriver = $poDriver;
	}

	/**
	 * Modifica o driver associado ao objeto.
	 *
	 * <p>Modifica o driver associado ao objeto.</p>
	 * @access public
	 * @param FWDTranslationDriver $poDriver Driver a ser associado ao objeto
	 */
	public function setDriver(FWDTranslationDriver $poDriver){
		if($this->coStringList==null){
			$this->coDriver = $poDriver;
		}else{
			trigger_error("The driver can't be changed while an operation is in progress.",E_USER_WARNING);
		}
	}

	/**
	 * Lista os arquivos de uma pasta com uma determinada extens�o
	 *
	 * <p>Percorre uma pasta buscando arquivos com a extens�o dada e retorna uma
	 * lista com o caminho de cada um deles.</p>
	 * @access protected
	 * @param string $psPath Pasta a ser percorrida
	 * @param boolean $pbRecursive Indica se as subpastas devem ser percorridas tamb�m
	 * @param string $psExtension Extens�o desejada
	 * @return array Lista com os caminhos dos arquivos
	 */
	protected function listFiles($psPath,$pbRecursive=false,$psExtension){
		$maFiles = array();
		$maSubDirs = array();
		$moDir = dir($psPath);
		$psExtension = strtolower($psExtension);
		while(($msFileName=$moDir->read())!==false){
			$msFullPath = "$psPath/$msFileName";
			if(is_file($msFullPath)){
				$msExt = strtolower(substr($msFileName,strrpos($msFileName,'.')+1));
				if($msExt==$psExtension) $maFiles[] = $msFullPath;
			}elseif($pbRecursive && $msFileName!='.' && $msFileName!='..'){
				$maSubDirs[] = $msFileName;
			}
		}
		$moDir->close();
		if($pbRecursive){
			foreach($maSubDirs as $msDirName){
				$msFullPath = "$psPath/$msDirName";
				$maFiles = array_merge($maFiles,$this->listFiles($msFullPath,true,$psExtension));
			}
		}
		return $maFiles;
	}

	/**
	 * Extrai as strings de um arquivo XML
	 *
	 * <p>Extrai as strings de um arquivo XML e as inclui na StringList interna.</p>
	 * @access protected
	 * @param string $psFileName Arquivo XML
	 * @return boolean True, indica sucesso
	 */
	protected function singleExtractXMLStrings($psFileName){
		$this->csFileBeingParsed = $psFileName;
		$this->coStringList->addFile($psFileName);
		$mbParse = $this->parseFile($psFileName);
		$this->csFileBeingParsed = '';
		if(!$mbParse) $this->coStringList->removeFile($psFileName);
		return $mbParse;
	}

	/**
	 * Extrai as strings de um arquivo PHP
	 *
	 * <p>Extrai as strings de um arquivo PHP (chamadas de FWDLanguage::getPHPStringValue())
	 * e as inclui na StringList interna.
	 * As chamadas do m�todo getPHPStringValue com par�metros que n�o s�o strings
	 * constantes s�o consideradas inv�lidas e geram um warning.</p>
	 * @access protected
	 * @param string $psFileName Arquivo PHP
	 * @return boolean True, indica sucesso
	 */
	protected function singleExtractPHPStrings($psFileName){
		if(!file_exists($psFileName) || !is_readable($psFileName)){
			trigger_error("Could not open file '$psFileName'.",E_USER_WARNING);
			return false;
		}
		$this->csFileBeingParsed = $psFileName;
		$this->coStringList->addFile($psFileName);
		$mrFile = fopen($psFileName,'r');
		if(filesize($psFileName)>0)
		$msFileContent = fread($mrFile,filesize($psFileName));
		else{
			$msFileContent = '';
		}

		fclose($mrFile);
		$miFileSize = strlen($msFileContent);
		$i = 0;
		$msNeedle = 'FWDLanguage::getPHPStringValue';
		$miNeedleLength = strlen($msNeedle);
		$miParamStart=0;
		while($i<$miFileSize){
			$i = strpos($msFileContent,$msNeedle,$i);
			if($i===false) break;
			$mbInDblQuote = false;
			$mbInSngQuote = false;
			$i+= $miNeedleLength;
			$miStart = $i;
			$miEnd = -1;
			$mbValid = true;
			$maParams = array();
			while($miEnd==-1 && $i<$miFileSize){
				if(!$mbInSngQuote && !$mbInDblQuote){
					if(strpos(" '\"()\n\t,",$msFileContent[$i])===false) $mbValid = false;
				}
				switch($msFileContent[$i]){
					case '"':
						if(!$mbInSngQuote){
							if($mbInDblQuote){
								$mbInDblQuote = false;
								$maParams[] = str_replace("'","\'",stripslashes(substr($msFileContent,$miParamStart,$i - $miParamStart)));
							}else{
								$mbInDblQuote = true;
								$miParamStart = $i + 1;
							}
						}
						$i++;
						break;
					case "'":
						if(!$mbInDblQuote){
							if($mbInSngQuote){
								$mbInSngQuote = false;
								$maParams[] = substr($msFileContent,$miParamStart,$i - $miParamStart);
							}else{
								$mbInSngQuote = true;
								$miParamStart = $i + 1;
							}
						}
						$i++;
						break;
					case ')':
						$i++;
						if(!$mbInSngQuote && !$mbInDblQuote) $miEnd = $i;
						break;
					case "\\":
						$i++;
						if($mbInDblQuote || $mbInSngQuote) $i++;
						break;
					case '$':
						if($mbInDblQuote) $mbValid = false;
						$i++;
						break;
					default: $i++;
				}
			}
			if($miEnd==-1){
				$miEnd = $miFileSize;
				$mbValid = false;
			}elseif(count($maParams)!=2){
				$mbValid = false;
			}elseif(!preg_match('/^[\w\d_]+$/i',$maParams[0])){
				$mbValid = false;
			}
			if($mbValid){
				$this->coStringList->addString($this->csFileBeingParsed,$maParams[0],$maParams[1]);
			}else{
				$msMsg = "Invalid call of '$msNeedle' in file '{$this->csFileBeingParsed}: "
				.$msNeedle.substr($msFileContent,$miStart,$miEnd-$miStart);
				trigger_error($msMsg,E_USER_WARNING);
			}
		}
		$this->csFileBeingParsed = '';
		return true;
	}

	/**
	 * Extrai as strings de um arquivo ou de todos arquivos XML ou PHP numa pasta
	 *
	 * <p>Extrai as strings de um arquivo ou de todos arquivos XML ou PHP numa pasta e
	 * usa o driver para armazenar a StringList final.</p>
	 * @access public
	 * @param string $psFullPath Arquivo ou pasta
	 * @param boolean $pbRecursive Indica se as subpastas devem ser percorridas tamb�m
	 * @param string $psExtension Extens�o dos arquivos a serem parseados (php|xml).
	 */
	public function extractStringsFromFiles($psFullPath,$pbRecursive,$psExtension){
		$this->coStringList = new FWDStringList();
		if(!file_exists($psFullPath) || !is_readable($psFullPath)){
			trigger_error("Could not open file or directory '$psFullPath'.",E_USER_ERROR);
			return;
		}elseif(is_dir($psFullPath)){
			$maFiles = $this->listFiles($psFullPath,$pbRecursive,$psExtension);
			if($psExtension=='php'){
				foreach($maFiles as $msFilePath){
					$this->singleExtractPHPStrings($msFilePath);
				}
			}elseif($psExtension=='xml'){
				foreach($maFiles as $msFilePath){
					$this->singleExtractXMLStrings($msFilePath);
				}
			}else{
				trigger_error('Invalid file extension.',E_USER_ERROR);
			}
		}else{
			if($psExtension=='php'){
				$this->singleExtractPHPStrings($psFullPath);
			}elseif($psExtension=='xml'){
				$this->singleExtractXMLStrings($psFullPath);
			}else{
				trigger_error('Invalid file extension.',E_USER_ERROR);
			}
		}
		$this->coDriver->store($this->coStringList);
		$this->coStringList = null;
	}

	/**
	 * Extrai as strings de um arquivo XML ou de todos arquivos XML numa pasta
	 *
	 * <p>Extrai as strings de um arquivo XML ou de todos arquivos XML numa pasta e
	 * usa o driver para armazenar a StringList final.</p>
	 * @access public
	 * @param string $psFullPath Arquivo ou pasta
	 * @param boolean $pbRecursive Indica se as subpastas devem ser percorridas tamb�m
	 */
	public function extractXMLStrings($psFullPath,$pbRecursive=false){
		$this->extractStringsFromFiles($psFullPath,$pbRecursive,'xml');
	}

	/**
	 * Extrai as strings de um arquivo PHP ou de todos arquivos PHP numa pasta
	 *
	 * <p>Extrai as strings de um arquivo PHP ou de todos arquivos PHP numa pasta e
	 * usa o driver para armazenar a StringList final.</p>
	 * @access public
	 * @param string $psFullPath Arquivo ou pasta
	 * @param boolean $pbRecursive Indica se as subpastas devem ser percorridas tamb�m
	 */
	public function extractPHPStrings($psFullPath,$pbRecursive=false){
		$this->extractStringsFromFiles($psFullPath,$pbRecursive,'php');
	}

	/**
	 * Armazena num arquivo PHP, uma StringList lida a partir do driver
	 *
	 * <p>Armazena num arquivo PHP, uma StringList lida a partir do driver.</p>
	 * @access public
	 * @param string $psFileName Arquivo PHP, onde a StringList vai ser armazenada
	 * @param string $psLanguage L�ngua que deve ser carregada do banco
	 */
	public function storeToPHPFile($psFileName, $psLanguage=""){
		$this->coStringList = $this->coDriver->load($psLanguage);
		if($this->coStringList==null){
			trigger_error("Could not load StringList from TranslationDriver.",E_USER_ERROR);
		}else{
			if(!is_writable($psFileName)){
				trigger_error("Could not write to file '$psFileName'.",E_USER_ERROR);
				return false;
			}
			$msOutput = "<?php\n\$gaStrings = array(\n";
			foreach($this->coStringList->dump() as $msFileName=>$maStrings){
				if(count($maStrings)>0){
					$msOutput.= "\n/* '$msFileName' */\n\n";
					foreach($maStrings as $msId=>$msValue){
						if ($msValue)
						$msOutput.= "'$msId'=>'".str_replace("'","\'",stripslashes($msValue))."',\n";
					}
				}
			}
			$msOutput.= ");\n?>";
			$mrFile = fopen($psFileName,'w');
			if($mrFile){
				fwrite($mrFile,$msOutput);
				fclose($mrFile);
			}
			$this->coStringList = null;
		}
	}

	/**
	 * Handler chamado quando o parser encontra uma tag de in�cio de elemento
	 *
	 * <p>Handler chamado quando o parser encontra uma tag de in�cio de elemento.</p>
	 * @access protected
	 * @param resource $prParser Parser
	 * @param string $psTagName Nome da tag
	 * @param array $paAttributes Array associativo contendo os atributos do elemento
	 */
	protected function startHandler($prParser, $psTagName, $paAttributes){
		if($psTagName=='STRING'){
			if(isset($paAttributes['ID'])){
				$msId = $paAttributes['ID'];
				$this->caCurrentString = array('id'=>$msId,'value'=>'');
			}else{
				$msErrorMsg = "String element without id attribute in file {$this->csFileBeingParsed}"
				." at line ".xml_get_current_line_number($prParser);
				trigger_error($msErrorMsg,E_USER_WARNING);
			}
		} elseif ($psTagName=='B') {
			if (isset($this->caCurrentString['value'])) $this->caCurrentString['value'].='<b>';
		} elseif ($psTagName=='BR') {
			if (isset($this->caCurrentString['value'])) $this->caCurrentString['value'].='<br/>';
		}
	}

	/**
	 * Handler chamado quando o parser encontra texto dentro de um elemento
	 *
	 * <p>Handler chamado quando o parser encontra texto dentro de um elemento.</p>
	 * @access protected
	 * @param resource $prParser Parser
	 * @param string $psData Conte�do de texto do elemento
	 */
	protected function dataHandler($prParser, $psData){
		if($this->caCurrentString!=null){
			$msId = $this->caCurrentString['id'];
			$this->caCurrentString['value'].= $psData;
		}
	}

	/**
	 * Handler chamado quando o parser encontra uma tag de fim de elemento
	 *
	 * <p>Handler chamado quando o parser encontra uma tag de fim de elemento.</p>
	 * @access protected
	 * @param resource $prParser Parser
	 * @param string $psTagName Nome da tag
	 */
	protected function endHandler($prParser, $psTagName){
		if($this->caCurrentString!=null){
			if ($psTagName=='STRING') {
				$msId = $this->caCurrentString['id'];
				$msValue = trim(preg_replace('/[\s\n]+/',' ',$this->caCurrentString['value']));
				$msValue = utf8_encode($msValue);
				// testando pra ver se resolveu bug de encoding no windows
				//echo "era " .  $this->caCurrentString['value'] . " e ficou $msValue<br>";
				$this->coStringList->addString($this->csFileBeingParsed,$msId, $msValue);
				$this->caCurrentString = null;
			} elseif ($psTagName=='B') {
				if (isset($this->caCurrentString['value'])) $this->caCurrentString['value'].='</b>';
			}
		}
	}

}

?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe para tratamento de cores.
 *
 * <p>Classe para tratamento de cores em documentos XLS.</p>
 *
 * @package FWD5
 * @subpackage formats-excel
 */
class FWDExcelColors {

	/**
	 * Converte de rgb para o formato html.
	 *
	 * <p>M�todo para converter uma cor especificada em rgb
	 * para o formato html (ex: #ff8800).</p>
	 *
	 * @access public
	 * @param integer $piR Vermelho.
	 * @param integer $piG Verde.
	 * @param integer $piB Azul.
	 *
	 * @return string Html color (ex: #FF00FF).
	 *
	 * @static
	 */
	public static function color($piR, $piG, $piB) {
		$mscolor = "";
		if (($piR >=0 && $piR <= 255) && ($piG >=0 && $piG <= 255) && ($piB >=0 && $piB <= 255))
		$mscolor = "#" . str_pad(dechex(($piR<<16)|($piG<<8)|$piB), 6, "0", STR_PAD_LEFT);
		else
		$mscolor = "";

		return $mscolor;
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe para adicionar estilos em um arquivo XLS.
 *
 * <p>Classe que representa a tag "Style" de uma arquivo xml.</p>
 *
 * @package FWD5
 * @subpackage formats-excel
 */
class FWDExcelStyle {
		
	/**
	 * Identificador do estilo
	 * @var string
	 * @access private
	 */
	private $csid;

	/**
	 * Alinhamento do estilo
	 * @var FWDExcelStyleAlignment
	 * @access private
	 */
	private $coAlignment = null;

	/**
	 * Fonte do estilo
	 * @var FWDExcelStyleFont
	 * @access private
	 */
	private $coFont = null;

	/**
	 * Interior do estilo
	 * @var FWDExcelStyleInterior
	 * @access private
	 */
	private $coInterior = null;

	/**
	 * Formato de n�mero do estilo
	 * @var FWDExcelStyleNumberFormat
	 * @access private
	 */
	private $coNumberFormat = null;

	/**
	 * Array dos estilos das bordas
	 * @var FWDExcelStyleBorders
	 * @access private
	 */
	private $coBorders = null;
		
	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDExcelStyle.</p>
	 *
	 * @access public
	 * @param string $psid Identificador do estilo.
	 */
	public function __construct($psid) {
		$this->setId($psid);
	}

	/**
	 * Seta o id do estilo.
	 *
	 * <p>M�todo para setar o id do estilo.</p>
	 *
	 * @access public
	 * @param string $psid Id do estilo.
	 */
	public function setId($psid) {
		if ($psid) $this->csid = $psid;
		else trigger_error("Invalid style id",E_USER_ERROR);
	}

	/**
	 *  Retorna o id do estilo.
	 *
	 * <p>M�todo para retornar o id do estilo.</p>
	 *
	 * @access public
	 * @return string Id do estilo.
	 */
	public function getId() {
		return $this->csid;
	}

	/**
	 * Seta o objeto de alinhamento do estilo.
	 *
	 * <p>M�todo para setar o objeto de alinhamento do estilo.</p>
	 *
	 * @access public
	 * @param FWDExcelStyleAlignment $poAlignment Alinhamento do estilo.
	 */
	public function setAlignment(FWDExcelStyleAlignment $poAlignment) {
		$this->coAlignment = $poAlignment;
	}

	/**
	 *  Retorna o objeto de alinhamento do estilo.
	 *
	 * <p>M�todo para retornar o objeto de alinhamento do estilo.</p>
	 *
	 * @access public
	 * @return FWDExcelStyleAlignment Alinhamento do estilo.
	 */
	public function getAlignment() {
		return $this->coAlignment;
	}

	/**
	 * Seta o objeto de fonte do estilo.
	 *
	 * <p>M�todo para setar o objeto de fonte do estilo.</p>
	 *
	 * @access public
	 * @param FWDExcelStyleFont $poFont Fonte do estilo.
	 */
	public function setFont(FWDExcelStyleFont $poFont) {
		$this->coFont = $poFont;
	}

	/**
	 *  Retorna o objeto de fonte do estilo.
	 *
	 * <p>M�todo para retornar o objeto de fonte do estilo.</p>
	 *
	 * @access public
	 * @return FWDExcelStyleFont Fonte do estilo.
	 */
	public function getFont() {
		return $this->coFont;
	}

	/**
	 * Seta o objeto de interior do estilo.
	 *
	 * <p>M�todo para setar o objeto de interior do estilo.</p>
	 *
	 * @access public
	 * @param FWDExcelStyleInterior $poInterior Interior do estilo.
	 */
	public function setInterior(FWDExcelStyleInterior $poInterior) {
		$this->coInterior = $poInterior;
	}

	/**
	 *  Retorna o objeto de interior do estilo.
	 *
	 * <p>M�todo para retornar o objeto de interior do estilo.</p>
	 *
	 * @access public
	 * @return FWDExcelStyleInterior Interior do estilo.
	 */
	public function getInterior() {
		return $this->coInterior;
	}

	/**
	 * Seta o objeto de formato de n�mero do estilo.
	 *
	 * <p>M�todo para setar o objeto de formato de n�mero do estilo.</p>
	 *
	 * @access public
	 * @param FWDExcelStyleNumberFormat $poNumberFormat Formato de n�mero do estilo.
	 */
	public function setNumberFormat(FWDExcelStyleNumberFormat $poNumberFormat) {
		$this->coNumberFormat = $poNumberFormat;
	}

	/**
	 *  Retorna o objeto de formato de n�mero do estilo.
	 *
	 * <p>M�todo para retornar o objeto de formato de n�mero do estilo.</p>
	 *
	 * @access public
	 * @return FWDExcelStyleNumberFormat Formato de n�mero do estilo.
	 */
	public function getNumberFormat() {
		return $this->coNumberFormat;
	}

	/**
	 * Seta o objeto de bordas do estilo.
	 *
	 * <p>M�todo para setar o objeto de bordas do estilo.</p>
	 *
	 * @access public
	 * @param FWDExcelStyleBorders $poBorders Bordas do estilo.
	 */
	public function setBorders(FWDExcelStyleBorders $poBorders) {
		$this->coBorders = $poBorders;
	}

	/**
	 *  Retorna o objeto de bordas do estilo.
	 *
	 * <p>M�todo para retornar o objeto de bordas do estilo.</p>
	 *
	 * @access public
	 * @return FWDExcelStyleBorders Bordas do estilo.
	 */
	public function getBorders() {
		return $this->coBorders;
	}

	/**
	 * Retorna o xml que representa a tag "Style".
	 *
	 * <p>M�todo para retornar o c�digo xml para a tag "Style".</p>
	 *
	 * @access public
	 * @return string C�digo xml para tag "Style".
	 */
	public function createTag() {
		echo "<Style ss:ID=\"" . $this->csid . "\">";

		if($this->coAlignment) $this->coAlignment->createTag();
		if($this->coBorders) $this->coBorders->createTag();
		if($this->coFont) $this->coFont->createTag();
		if($this->coInterior) $this->coInterior->createTag();
		if($this->coNumberFormat) $this->coNumberFormat->createTag();
		echo "</Style>";
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe para alinhamento.
 *
 * <p>Classe que representa a tag "Alignment" de um estilo.</p>
 *
 * @package FWD5
 * @subpackage formats-excel
 */
class FWDExcelStyleAlignment {

	/**
	 * Array com os poss�veis valores para o atributo Vertical
	 * @var array
	 * @access private
	 */
	private $caverticalOptions = array("", "Bottom", "Top", "Center", "Justify", "Distributed");

	/**
	 * Array com os poss�veis valores para o atributo Horizontal
	 * @var array
	 * @access private
	 */
	private $cahorizontalOptions = array("", "Left", "Center", "Right", "Fill", "Justify", "Distributed", "CenterAcrossSelection");

	/**
	 * Alinhamento vertical
	 * @var string
	 * @access private
	 */
	private $csvertical = "";

	/**
	 * Alinhamento horizontal
	 * @var string
	 * @access private
	 */
	private $cshorizontal = "";

	/**
	 * Quebra de linha
	 * @var boolean
	 * @access private
	 */
	private $cbwrapText = false;

	/**
	 * �ngulo de rota��o {-90, 90}
	 * @var integer
	 * @access private
	 */
	private $cirotate = 0;
		
	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDExcelStyleAlignment.</p>
	 *
	 * @access public
	 */
	public function __construct() {}

	/**
	 * Seta o alinhamento vertical.
	 *
	 * <p>M�todo para setar o alinhamento vertical do estilo.</p>
	 *
	 * @access public
	 * @param string $psvalign Alinhamento vertical do estilo {Bottom | Top | Center | Justify | Distributed}.
	 */
	public function setVerticalAlignment($psvalign) {
		if(in_array($psvalign, $this->caverticalOptions)) $this->csvertical = $psvalign;
		else trigger_error("Invalid token: \"" . $psvalign . "\"",E_USER_ERROR);

	}

	/**
	 * Retorna o alinhamento vertical.
	 *
	 * <p>M�todo para retornar o alinhamento vertical do estilo.</p>
	 *
	 * @access public
	 * @return string Alinhamento vertical do estilo.
	 */
	public function getVerticalAlignment() {
		return $this->csvertical;
	}

	/**
	 * Seta o alinhamento horizontal.
	 *
	 * <p>M�todo para setar o alinhamento horizontal do estilo.</p>
	 *
	 * @access public
	 * @param string $pshalign Alinhamento horizontal do estilo {Left | Center | Right | Fill | Justify | Distributed | CenterAcrossSelection}.
	 */
	public function setHorizontalAlignment($pshalign) {
		if(in_array($pshalign, $this->cahorizontalOptions)) $this->cshorizontal = $pshalign;
		else trigger_error("Invalid token: \"" . $pshalign . "\"",E_USER_ERROR);
	}

	/**
	 * Retorna o alinhamento horizontal.
	 *
	 * <p>M�todo para retornar o alinhamento horizontal do estilo.</p>
	 *
	 * @access public
	 * @return string Alinhamento horizontal do estilo.
	 */
	public function getHorizontalAlignment() {
		return $this->cshorizontal;
	}

	/**
	 * Seta a op��o de quebra de linha.
	 *
	 * <p>M�todo para setar a op��o de quebra de linha do estilo.</p>
	 *
	 * @access public
	 * @param boolean $pbwrap Quebra de linha do estilo.
	 */
	public function setWrapText($pbwrap) {
		$this->cbwrapText = $pbwrap;
	}

	/**
	 * Retorna a op��o de quebra de linha.
	 *
	 * <p>M�todo para retornar a op��o de quebra de linha do estilo.</p>
	 *
	 * @access public
	 * @return boolean Op��o de quebra de linha do estilo.
	 */
	public function isWrapText() {
		return $this->cbwrapText;
	}

	/**
	 * Seta o �ngulo de rota��o.
	 *
	 * <p>M�todo para setar o �ngulo de rota��o do estilo.</p>
	 *
	 * @access public
	 * @param boolean $pirotate �ngulo de rota��o do estilo.
	 */
	public function setRotate($pirotate) {
		if(($pirotate >= -90) && ($pirotate <= 90)) $this->cirotate = $pirotate;
		else trigger_error("Value ". $pirotate . " out of bounds",E_USER_ERROR);
	}

	/**
	 * Retorna o �ngulo de rota��o.
	 *
	 * <p>M�todo para retornar o �ngulo de rota��o do estilo.</p>
	 *
	 * @access public
	 * @return integer �ngulo de rota��o do estilo.
	 */
	public function getRotate() {
		return $this->cirotate;
	}

	/**
	 * Seta os atributos de alinhamento.
	 *
	 * <p>M�todo para setar o alinhamento vertical e horizontal,
	 * quebra de texto e rota��o.</p>
	 * @access public
	 * @param string $psvalign Alinhamento vertical.
	 * @param string $pshalign Alinhamento horizontal.
	 * @param boolean $pbwrap Quebra de texto (default = false).
	 * @param integer $pirotate Rota��o (default = 0).
	 */
	public function setAttributes($psvalign, $pshalign, $pbwrap = false, $pirotate = 0) {
		$this->setVerticalAlignment($psvalign);
		$this->setHorizontalAlignment($pshalign);
		$this->setWrapText($pbwrap);
		$this->setRotate($pirotate);
	}

	/**
	 * Retorna o xml que representa a tag "Alignment".
	 *
	 * <p>M�todo para retornar o c�digo xml para a tag "Alignment".</p>
	 *
	 * @access public
	 * @return string C�digo xml para tag "Alignment".
	 */
	public function createTag() {
		echo "<Alignment";
		echo $this->cshorizontal ? " ss:Horizontal=\"".$this->cshorizontal."\"" : "";
		echo $this->csvertical ? " ss:Vertical=\"".$this->csvertical."\"" : "";
		echo $this->cbwrapText ? " ss:WrapText=\"".$this->cbwrapText."\"" : "";
		echo $this->cirotate ? " ss:Rotate=\"".$this->cirotate."\"" : "";
		echo "/>";
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe para bordas.
 *
 * <p>Classe que representa a tag "Borders" de um estilo.</p>
 *
 * @package FWD5
 * @subpackage formats-excel
 */
class FWDExcelStyleBorders {

	/**
	 * Array de FWDExcelStyleBorder.
	 * @var array
	 * @access private
	 */
	private $caBorders = array();

	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDExcelStyleBorder.</p>
	 *
	 * @access public
	 */
	public function __construct() {}

	/**
	 * Seta as bordas externas do estilo.
	 *
	 * <p>M�todo para setar as bordas externas do estilo</p>
	 *
	 * @param string $psline Tipo de linha.
	 * @param array $pacolor Cor (array (R,G,B)).
	 */
	public function setExternalBorders($psline, $pacolor) {
		$this->caBorders["Left"] = new FWDExcelStyleBorder();
		$this->caBorders["Right"] = new FWDExcelStyleBorder();
		$this->caBorders["Top"] = new FWDExcelStyleBorder();
		$this->caBorders["Bottom"] = new FWDExcelStyleBorder();

		$this->caBorders["Left"]->setAttributes("Left", $psline, $pacolor);
		$this->caBorders["Right"]->setAttributes("Right", $psline, $pacolor);
		$this->caBorders["Top"]->setAttributes("Top", $psline, $pacolor);
		$this->caBorders["Bottom"]->setAttributes("Bottom", $psline, $pacolor);
	}

	/**
	 * Seta uma borda espec�fica do estilo.
	 *
	 * <p>M�todo para setar uma borda espec�fica do estilo.</p>
	 *
	 * @param string $psposition Posi��o.
	 * @param string $psline Tipo de linha.
	 * @param array $pacolor Cor (array (R,G,B)).
	 */
	public function setBorder($psposition, $psline, $pacolor) {
		$this->caBorders[$psposition] = new FWDExcelStyleBorder();
		$this->caBorders[$psposition]->setLine($psline);
		$this->caBorders[$psposition]->setColor($pacolor);
	}

	/**
	 * Retorna uma borda espec�fica do estilo.
	 *
	 * <p>M�todo para retorna uma borda espec�fica do estilo.</p>
	 *
	 * @param string $psposition Posi��o da borda.
	 *
	 * @return FWDExcelStyleBorder
	 */
	public function getBorder($psposition) {
		if (array_key_exists($psposition, $this->caBorders))
		return $this->caBorders[$psposition];
		else trigger_error("Invalid border position: $psposition",E_USER_ERROR);
	}

	/**
	 * Retorna todas as bordas do estilo.
	 *
	 * <p>M�todo para retorna todas as bordas do estilo.</p>
	 *
	 * @param string $psposition Posi��o.
	 *
	 * @return array Array com todas as bordas do estilo (FWDExcelStyleBorder).
	 */
	public function getBorders($psposition) {
		return $this->caBorders[$psposition];
	}

	/**
	 * Retorna o xml que representa a tag "Borders".
	 *
	 * <p>M�todo para retornar o c�digo xml para a tag "Borders".</p>
	 *
	 * @access public
	 * @return string C�digo xml para tag "Borders".
	 */
	public function createTag() {
		echo "<Borders>";
		foreach($this->caBorders as $msborder) $msborder->createTag();
		echo "</Borders>";
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe para borda.
 *
 * <p>Classe que representa a tag "Border" de um estilo.</p>
 *
 * @package FWD5
 * @subpackage formats-excel
 */
class FWDExcelStyleBorder {

	/**
	 * Array com os poss�veis valores para o atributo Position
	 * @var array
	 * @access private
	 */
	private $capositionOptions = array("", "Bottom", "Left", "Right", "Top", "DiagonalLeft", "DiagonalRight");

	/**
	 * Array com os poss�veis valores para o atributo LineStyle
	 * @var array
	 * @access private
	 */
	private $calineOptions = array("", "Continuous", "Double", "Dash");

	/**
	 * Posi��o da borda.
	 * @var string
	 * @access private
	 */
	private $csposition = "";

	/**
	 * Tipo da borda.
	 * @var string
	 * @access private
	 */
	private $csline = "";

	/**
	 * Cor da borda.
	 * @var string
	 * @access private
	 */
	private $cscolor = "";

	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDExcelStyleBorder.</p>
	 *
	 * @access public
	 */
	public function __construct() {}

	/**
	 * Seta a posi��o da borda.
	 *
	 * <p>M�todo para setar a posi��o da borda.</p>
	 *
	 * @access public
	 * @param string $psposition Posi��o da borda {Bottom | Left | Right | Top | DiagonalLeft | DiagonalRight}.
	 */
	public function setPosition($psposition) {
		if(in_array($psposition, $this->capositionOptions)) $this->csposition = $psposition;
		else trigger_error("Invalid token: \"" . $psposition . "\"",E_USER_ERROR);
	}

	/**
	 * Retorna a posi��o da borda.
	 *
	 * <p>M�todo para retornar a posi��o da borda.</p>
	 *
	 * @access public
	 * @return string Posi��o da borda.
	 */
	public function getPosition() {
		return $this->csposition;
	}

	/**
	 * Seta o tipo da borda.
	 *
	 * <p>M�todo para setar o tipo da borda.</p>
	 *
	 * @access public
	 * @param string $psline Tipo da borda {Continuous | Double | Dash}.
	 */
	public function setLine($psline) {
		if(in_array($psline, $this->calineOptions)) $this->csline = $psline;
		else trigger_error("Invalid token: \"" . $psline . "\"",E_USER_ERROR);
	}

	/**
	 * Retorna o tipo da borda.
	 *
	 * <p>M�todo para retornar o tipo da borda.</p>
	 *
	 * @access public
	 * @return string Tipo da borda.
	 */
	public function getLine() {
		return $this->csline;
	}

	/**
	 * Seta a cor da borda.
	 *
	 * <p>M�todo para setar a cor da borda.</p>
	 *
	 * @access public
	 * @param integer $piR Cor da borda (Vermelho).
	 * @param integer $piG Cor da borda (Verde).
	 * @param integer $piB Cor da borda (Azul).
	 */
	public function setColor($piR, $piG, $piB) {
		if(($mscolor = FWDExcelColors::color($piR, $piG, $piB))) $this->cscolor = $mscolor;
		else trigger_error("Invalid rgb color: \"" . "($piR,$piG,$piB)" .  "\"",E_USER_ERROR);
	}

	/**
	 * Retorna a cor da borda.
	 *
	 * <p>M�todo para retornar a cor da borda.</p>
	 *
	 * @access public
	 * @return string Cor da borda.
	 */
	public function getColor() {
		return $this->cscolor;
	}

	/**
	 * Seta os atributos de borda.
	 *
	 * <p>M�todo para setar a posi��o, tipo de linha e cor da borda.</p>
	 *
	 * @access public
	 * @param string $psposition Posi��o da borda {Bottom | Left | Right | Top | DiagonalLeft | DiagonalRight}.
	 * @param string $psline Tipo da borda {Continuous | Double | Dash}.
	 * @param array $pacolor Cores (array (R,G,B)).
	 */
	public function setAttributes($psposition, $psline, $pacolor) {
		$this->setPosition($psposition);
		$this->setLine($psline);
		if((is_array($pacolor)) && (count($pacolor) == 3)) $this->setColor($pacolor[0], $pacolor[1], $pacolor[2]);
		else trigger_error("Invalid parameter: pacolor must be an array with 3 elements",E_USER_ERROR);
	}

	/**
	 * Retorna o xml que representa a tag "Border".
	 *
	 * <p>M�todo para retornar o c�digo xml para a tag "Border".</p>
	 *
	 * @access public
	 * @return string C�digo xml para tag "Border".
	 */
	public function createTag() {

		echo "<Border";
		echo $this->csposition ? " ss:Position=\"".$this->csposition."\"" : "";
		if ($this->csline == "Continuous") echo " ss:LineStyle=\"".$this->csline."\" ss:Weight=\"1\"";
		else echo $this->csline ? " ss:LineStyle=\"".$this->csline."\"" : "";
		echo $this->cscolor ? " ss:Color=\"".$this->cscolor."\"" : "";
		echo "/>";
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe para fonte.
 *
 * <p>Classe que representa a tag "Font" de um estilo.</p>
 *
 * @package FWD5
 * @subpackage formats-excel
 */
class FWDExcelStyleFont {

	/**
	 * Array com os poss�veis valores para o atributo Underline
	 * @var array
	 * @access private
	 */
	private $caunderlineOptions = array("", "Single", "Double", "SingleAccounting", "DoubleAccounting");

	/**
	 * Array com os poss�veis valores para o atributo VerticalAlign
	 * @var array
	 * @access private
	 */
	private $caverticalAlignOptions = array("", "Subscript", "Superscript");
		
	/**
	 * Nome da fonte.
	 * @var string
	 * @access private
	 */
	private $csname = "";

	/**
	 * Tipo de sublinhado.
	 * @var string
	 * @access private
	 */
	private $csunderline = "";

	/**
	 * Tipo alinhamento vertical.
	 * @var string
	 * @access private
	 */
	private $csverticalAlign = "";

	/**
	 * Negrito.
	 * @var boolean
	 * @access private
	 */
	private $cbbold = false;

	/**
	 * It�lico.
	 * @var boolean
	 * @access private
	 */
	private $cbitalic = false;

	/**
	 * Tachado.
	 * @var boolean
	 * @access private
	 */
	private $cbstrikeThrough = false;

	/**
	 * Cor da fonte.
	 * @var string
	 * @access private
	 */
	private $cscolor = "";

	/**
	 * Tamanho da fonte.
	 * @var integer
	 * @access private
	 */
	private $cisize = 0;

	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDExcelStyleFont.</p>
	 *
	 * @access public
	 */
	public function __construct() {}

	/**
	 * Seta o nome da fonte.
	 *
	 * <p>M�todo para setar o nome da fonte.</p>
	 *
	 * @access public
	 * @param string $psname Nome da fonte.
	 */
	public function setName($psname) {
		$this->csname = $psname;
	}

	/**
	 * Retorna o nome da fonte.
	 *
	 * <p>M�todo para retornar o nome da fonte.</p>
	 *
	 * @access public
	 * @return string Nome da fonte.
	 */
	public function getName() {
		return $this->csname;
	}

	/**
	 * Seta estilo da fonte como negrito.
	 *
	 * <p>M�todo para setar o estilo da fonte como negrito.</p>
	 *
	 * @access public
	 * @param boolean $pbbold Negrito.
	 */
	public function setBold($pbbold) {
		$this->cbbold = $pbbold;
	}

	/**
	 * Retorna o estilo da fonte (negrito ou n�o).
	 *
	 * <p>M�todo para retornar o estilo da fonte (negrito ou n�o).</p>
	 *
	 * @access public
	 * @return string Estilo da fonte (negrito ou n�o).
	 */
	public function isBold() {
		return $this->cbbold;
	}

	/**
	 * Seta estilo da fonte como it�lico.
	 *
	 * <p>M�todo para setar o estilo da fonte como it�lico.</p>
	 *
	 * @access public
	 * @param boolean $pbitalic It�lico.
	 */
	public function setItalic($pbitalic) {
		$this->cbitalic = $pbitalic;
	}

	/**
	 * Retorna o estilo da fonte (it�lico ou n�o).
	 *
	 * <p>M�todo para retornar o estilo da fonte (it�lico ou n�o).</p>
	 *
	 * @access public
	 * @return string Estilo da fonte (it�lico ou n�o).
	 */
	public function isItalic() {
		return $this->cbitalic;
	}

	/**
	 * Seta o tipo de sublinhado.
	 *
	 * <p>M�todo para setar o tipo de sublinhado.</p>
	 *
	 * @access public
	 * @param string $psunderline Tipo de sublinhado {Single | Double | SingleAccounting | DoubleAccounting}.
	 */
	public function setUnderline($psunderline) {
		if(in_array($psunderline, $this->caunderlineOptions)) $this->csunderline = $psunderline;
		else trigger_error("Invalid token: \"" . $psunderline . "\"",E_USER_ERROR);
	}

	/**
	 * Retorna o tipo de sublinhado.
	 *
	 * <p>M�todo para retornar o tipo de sublinhado.</p>
	 *
	 * @access public
	 * @return string Tipo de sublinhado.
	 */
	public function getUnderline() {
		return $this->csunderline;
	}

	/**
	 * Seta o tipo de alinhamento vertical.
	 *
	 * <p>M�todo para setar o tipo de alinhamento vertical.</p>
	 *
	 * @access public
	 * @param string $psverticalAlign Tipo de alinhamento vertical {Subscript | Superscript}.
	 */
	public function setVerticalAlign($psverticalAlign) {
		if(in_array($psverticalAlign, $this->caverticalAlignOptions)) $this->csverticalAlign = $psverticalAlign;
		else trigger_error("Invalid token: \"" . $psverticalAlign . "\"",E_USER_ERROR);
	}

	/**
	 * Retorna o tipo de alinhamento vertical.
	 *
	 * <p>M�todo para retornar o tipo de alinhamento vertical.</p>
	 *
	 * @access public
	 * @return string Tipo de alinhamento vertical.
	 */
	public function getVerticalAlign() {
		return $this->csverticalAlign;
	}

	/**
	 * Seta estilo da fonte como tachada.
	 *
	 * <p>M�todo para setar o estilo da fonte como tachada.</p>
	 *
	 * @access public
	 * @param boolean $pbstrikeThrough Tachado.
	 */
	public function setStrikeThrough($pbstrikeThrough) {
		$this->cbstrikeThrough = $pbstrikeThrough;
	}

	/**
	 * Retorna o estilo da fonte (tachado ou n�o).
	 *
	 * <p>M�todo para retornar o estilo da fonte (tachado ou n�o).</p>
	 *
	 * @access public
	 * @return string Estilo da fonte (tachado ou n�o).
	 */
	public function isStrikeThrough() {
		return $this->cbstrikeThrough;
	}

	/**
	 * Seta a cor da fonte.
	 *
	 * <p>M�todo para setar a cor da fonte.</p>
	 *
	 * @access public
	 * @param integer $piR Cor da fonte (Vermelho).
	 * @param integer $piG Cor da fonte (Verde).
	 * @param integer $piB Cor da fonte (Azul).
	 */
	public function setColor($piR, $piG, $piB) {
		if(($mscolor = FWDExcelColors::color($piR, $piG, $piB))) $this->cscolor = $mscolor;
		else trigger_error("Invalid rgb color: \"" . "($piR,$piG,$piB)" .  "\"",E_USER_ERROR);
	}

	/**
	 * Retorna a cor da fonte.
	 *
	 * <p>M�todo para retornar a cor da fonte.</p>
	 *
	 * @access public
	 * @return string Cor da fonte.
	 */
	public function getColor() {
		return $this->cscolor;
	}

	/**
	 * Seta o tamanho da fonte.
	 *
	 * <p>M�todo para setar o tamanho da fonte.</p>
	 *
	 * @access public
	 * @param integer $pisize Tamanho da fonte.
	 */
	public function setSize($pisize) {
		if ($pisize > 0) $this->cisize = $pisize;
		else trigger_error("Invalid font size: " . $pisize,E_USER_ERROR);
	}

	/**
	 * Retorna o tamanho da fonte.
	 *
	 * <p>M�todo para retornar o tamanho da fonte.</p>
	 *
	 * @access public
	 * @return integer Tamanho da fonte.
	 */
	public function getSize() {
		return $this->cisize;
	}

	/**
	 * Seta os atributos de fonte.
	 *
	 * <p>M�todo para setar o nome, a cor, o tamanho,
	 * o tipo de sublinhamento, negrito, it�lico, tachado.</p>
	 * @access public
	 *
	 * @param string $psname Nome da fonte.
	 * @param integer $pisize Tamanho da fonte.
	 * @param array $pacolor Cor da fonte (array(R,G,B)).
	 * @param string $psunderline Tipo de sublinhamento (default = "").
	 * @param string $psverticalAlign Alinhamento vertical (default = "").
	 * @param boolean $pbbold Negrito (default = false).
	 * @param boolean $pbitalic It�lico (default = false).
	 * @param boolean $pbstrike Tachado (default = false).
	 */
	public function setAttributes($psname, $pisize, $pacolor, $psunderline = "", $psverticalAlign = "", $pbbold = false, $pbitalic = false, $pbstrike = false) {
		$this->setName($psname);
		$this->setSize($pisize);
		if((is_array($pacolor)) && (count($pacolor) == 3)) $this->setColor($pacolor[0], $pacolor[1], $pacolor[2]);
		else trigger_error("Invalid parameter: pacolor must be an array with 3 elements",E_USER_ERROR);
		$this->setUnderline($psunderline);
		$this->setVerticalAlign($psverticalAlign);
		$this->setBold($pbbold);
		$this->setItalic($pbitalic);
		$this->setStrikeThrough($pbstrike);
	}
		
	/**
	 * Retorna o xml que representa a tag "Font".
	 *
	 * <p>M�todo para retornar o c�digo xml para a tag "Font".</p>
	 *
	 * @access public
	 * @return string C�digo xml para tag "Font".
	 */
	public function createTag() {

		echo "<Font";
		echo $this->csname ? " ss:FontName=\"".$this->csname."\"" : "";
		echo $this->cbbold ? " ss:Bold=\"".$this->cbbold."\"" : "";
		echo $this->cbitalic ? " ss:Italic=\"".$this->cbitalic."\"" : "";
		echo $this->cbstrikeThrough ? " ss:StrikeThrough=\"".$this->cbstrikeThrough."\"" : "";
		echo $this->csunderline ? " ss:Underline=\"".$this->csunderline."\"" : "";
		echo $this->csverticalAlign ? " ss:VerticalAlign=\"".$this->csverticalAlign."\"" : "";
		echo $this->cscolor ? " ss:Color=\"".$this->cscolor."\"" : "";
		echo $this->cisize ? " ss:Size=\"".$this->cisize."\"" : "";
		echo "/>";
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe para sombreamento.
 *
 * <p>Classe que representa a tag "Interior" de um estilo.</p>
 *
 * @package FWD5
 * @subpackage formats-excel
 */
class FWDExcelStyleInterior {

	/**
	 * Array com os poss�veis valores para o atributo Pattern
	 * @var array
	 * @access private
	 */
	private $capatternOptions = array("", "Solid", "Gray75", "Gray50", "Gray25", "Gray125", "Gray0625",
										"HorzStripe", "VertStripe", "ReverseDiagStripe", "DiagStripe",
										"DiagCross", "ThickDiagCross", "ThinHorzStripe", "ThinVertStripe",
										"ThinReverseDiagStripe", "ThinDiagStripe", "ThinHorzCross","ThinDiagCross");	

	/**
	 * Padr�o.
	 * @var string
	 * @access private
	 */
	private $cspattern = "";

	/**
	 * Cor do fundo.
	 * @var string
	 * @access private
	 */
	private $csbgColor = "";

	/**
	 * Cor do padr�o.
	 * @var string
	 * @access private
	 */
	private $cspatternColor = "";

	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDExcelStyleInterior.</p>
	 *
	 * @access public
	 */
	public function __construct() {}

	/**
	 * Seta o padr�o.
	 *
	 * <p>M�todo para setar o padr�o.</p>
	 *
	 * @access public
	 * @param string $pspattern Padr�o {Solid | Gray75 | Gray50 | Gray25 | Gray125 | Gray0625 |
	 * 									HorzStripe | VertStripe | ReverseDiagStripe | DiagStripe |
	 * 									DiagCross | ThickDiagCross | ThinHorzStripe | ThinVertStripe |
	 * 									ThinReverseDiagStripe | ThinDiagStripe | ThinHorzCross |ThinDiagCross}.
	 */
	public function setPattern($pspattern) {
		if(in_array($pspattern, $this->capatternOptions)) $this->cspattern = $pspattern;
		else trigger_error("Invalid token: \"" . $pspattern . "\"",E_USER_ERROR);
	}

	/**
	 * Retorna o padr�o.
	 *
	 * <p>M�todo para retornar o padr�o.</p>
	 *
	 * @access public
	 * @return string Padr�o.
	 */
	public function getPattern() {
		return $this->cspattern;
	}

	/**
	 * Seta a cor do fundo.
	 *
	 * <p>M�todo para setar a cor do fundo.</p>
	 *
	 * @access public
	 * @param integer $piR Cor do fundo (Vermelho).
	 * @param integer $piG Cor do fundo (Verde).
	 * @param integer $piB Cor do fundo (Azul).
	 */
	public function setBgColor($piR, $piG, $piB) {
		if(($mscolor = FWDExcelColors::color($piR, $piG, $piB))) $this->csbgColor = $mscolor;
		else trigger_error("Invalid rgb color: \"" . "($piR,$piG,$piB)" .  "\"",E_USER_ERROR);
	}

	/**
	 * Retorna a cor do fundo.
	 *
	 * <p>M�todo para retornar a cor do fundo.</p>
	 *
	 * @access public
	 * @return string Cor do fundo.
	 */
	public function getBgColor() {
		return $this->csbgColor;
	}

	/**
	 * Seta a cor do padr�o.
	 *
	 * <p>M�todo para setar a cor do padr�o.</p>
	 *
	 * @access public
	 * @param integer $piR Cor do padr�o (Vermelho).
	 * @param integer $piG Cor do padr�o (Verde).
	 * @param integer $piB Cor do padr�o (Azul).
	 */
	public function setPatternColor($piR, $piG, $piB) {
		if(($mscolor = FWDExcelColors::color($piR, $piG, $piB))) $this->cspatternColor = $mscolor;
		else trigger_error("Invalid rgb color: \"" . "($piR,$piG,$piB)" .  "\"",E_USER_ERROR);
	}

	/**
	 * Retorna a cor do padr�o.
	 *
	 * <p>M�todo para retornar a cor do padr�o.</p>
	 *
	 * @access public
	 * @return string Cor do padr�o.
	 */
	public function getPatternColor() {
		return $this->cspatternColor;
	}

	/**
	 * Seta os atributos de interior.
	 *
	 * <p>M�todo para setar a cor do fundo, o nome do padr�o e
	 * a cor do padr�o.</p>
	 *
	 * @access public
	 *
	 * @param array $pabgColor Cor do fundo (array(R,G,B)).
	 * @param string $pspattern Nome do padr�o.
	 * @param array $papatternColor Cor do padr�o (array(R,G,B)).
	 */
	public function setAttributes($pabgColor, $pspattern, $papatternColor) {
		$this->setPattern($pspattern);

		if((is_array($pabgColor)) && (count($pabgColor) == 3)) $this->setBgColor($pabgColor[0], $pabgColor[1], $pabgColor[2]);
		else trigger_error("Invalid parameter: pabgColor must be an array with 3 elements",E_USER_ERROR);

		if((is_array($papatternColor)) && (count($papatternColor) == 3)) $this->setPatternColor($papatternColor[0], $papatternColor[1], $papatternColor[2]);
		else trigger_error("Invalid parameter: papatternColor must be an array with 3 elements",E_USER_ERROR);
	}
		
	/**
	 * Retorna o xml que representa a tag "Interior".
	 *
	 * <p>M�todo para retornar o c�digo xml para a tag "Interior".</p>
	 *
	 * @access public
	 * @return string C�digo xml para tag "Interior".
	 */
	public function createTag() {

		echo "<Interior";
		echo $this->csbgColor ? " ss:Color=\"".$this->csbgColor."\"" : "";
		echo $this->cspattern ? " ss:Pattern=\"".$this->cspattern."\"" : "";
		echo $this->cspatternColor ? " ss:PatternColor=\"".$this->cspatternColor."\"" : "";
		echo "/>";
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe para formatos de n�meros.
 *
 * <p>Classe que representa a tag "NumberFormat" de um estilo.</p>
 *
 * @package FWD5
 * @subpackage formats-excel
 */
class FWDExcelStyleNumberFormat {
		
	/**
	 * Array com os poss�veis valores para moeda
	 * @var array
	 * @access private
	 */
	private $cacurrencyOptions = array("", "Real", "Dollar", "Pound", "Euro");

	/**
	 * Array com os poss�veis valores para o formato de um n�mero
	 * @var array
	 * @access private
	 */
	private $canumberFormatOptions = array("", "General", "Number", "Currency");

	/**
	 * Formato do n�mero
	 * @var string
	 * @access private
	 */
	private $csnumberFormat = "General";

	/**
	 * N�mero de casas decimais
	 * @var string
	 * @access private
	 */
	private $ciprecision = -1;

	/**
	 * Usar ponto para separar 1000 (1.000)
	 * @var boolean
	 * @access private
	 */
	private $cbseparator = false;

	/**
	 * Moeda
	 * @var string
	 * @access private
	 */
	private $cscurrency = "";

	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDExcelStyleNumberFormat.</p>
	 *
	 * @access public
	 */
	public function __construct() {}

	/**
	 * Seta a precis�o do n�mero.
	 *
	 * <p>M�todo para setar a precis�o do n�mero (n�mero de casas
	 * decimais ap�s a v�rgula).</p>
	 *
	 * @access public
	 * @param integer $piprecision Precis�o {0,30}.
	 */
	public function setPrecision($piprecision) {
		if (($piprecision >= 0) && ($piprecision <= 30)) $this->ciprecision = $piprecision;
		else trigger_error("Invalid precision: \"" . $piprecision . "\"",E_USER_ERROR);
	}

	/**
	 * Desabilita a precis�o.
	 *
	 * <p>M�todo para desabilitar a op��o de precis�o de um n�mero.</p>
	 *
	 * @access public
	 */
	public function unsetPrecision() {
		$this->ciprecision = -1;
	}

	/**
	 * Retorna a precis�o do n�mero.
	 *
	 * <p>M�todo para retornar a precis�o do n�mero.</p>
	 *
	 * @access public
	 * @return integer Precis�o do n�mero (-1 = n�o definida).
	 */
	public function getPrecision() {
		return $this->ciprecision;
	}

	/**
	 * Retorna a precis�o do n�mero no formato do xml.
	 *
	 * <p>M�todo para retornar a precis�o do n�mero no formato que vai ser inserido
	 * no xml.</p>
	 *
	 * @access private
	 * @return string Precis�o do n�mero.
	 */
	private function getPrecisionString() {
		$msprecision = "";
		if($this->ciprecision > -1) {
			$msprecision .= "0";
			for($i = 0; $i < $this->ciprecision; $i++) $i ? $msprecision .= "0" : $msprecision .= ".0";
			return $msprecision;
		}
		else return "";
	}

	/**
	 * Habilita o "." como separador.
	 *
	 * <p>M�todo para setar o "." como separador. Ex: 1000 -> 1.000</p>
	 *
	 * @access public
	 * @param integer $pbseparator Separador.
	 */
	public function setSeparator($pbseparator) {
		$this->cbseparator = $pbseparator;
	}

	/**
	 * Retorna se o n�mero � separado por um "." ou n�o.
	 *
	 * <p>M�todo para verificar se o n�mero � separado por um "." ou n�o.</p>
	 *
	 * @access public
	 * @return boolean Possui "." ou n�o.
	 */
	public function isSeparated() {
		return $this->cbseparator;
	}

	/**
	 * Retorna a string referente ao "." no formato xml.
	 *
	 * <p>M�todo para retornar a string referente ao "." no formato xml.</p>
	 *
	 * @access private
	 * @return string String no formato xml do ".".
	 */
	private function getSeparatorString() {
		if ($this->cbseparator) return "#,##";
		else return "";
	}

	/**
	 * Seta a moeda.
	 *
	 * <p>M�todo para setar a moeda que ser� usada.</p>
	 *
	 * @access public
	 * @param string $pscurrency Moeda {Real | Dollar | Pound | Euro}.
	 */
	public function setCurrency($pscurrency) {
		if(in_array($pscurrency, $this->cacurrencyOptions)) $this->cscurrency = $pscurrency;
		else trigger_error("Invalid token: \"" . $pscurrency . "\"",E_USER_ERROR);
	}

	/**
	 * Retorna a moeda.
	 *
	 * <p>M�todo para retornar a moeda que est� sendo usada.</p>
	 *
	 * @access public
	 * @return string Moeda.
	 */
	public function getCurrency() {
		return $this->cscurrency;
	}

	/**
	 * Seta o formato do n�mero.
	 *
	 * <p>M�todo para setar o formato do n�mero.</p>
	 *
	 * @access public
	 * @param string $psnumberFormat Formato {General | Number | Currency}.
	 */
	public function setFormat($psnumberFormat) {
		if(in_array($psnumberFormat, $this->canumberFormatOptions)) $this->csnumberFormat = $psnumberFormat;
		else trigger_error("",E_USER_ERROR);
	}

	/**
	 * Retorna o formato do n�mero.
	 *
	 * <p>M�todo para retornar o formato do n�mero.</p>
	 *
	 * @access public
	 * @return string Formato.
	 */
	public function getFormat() {
		return $this->csnumberFormat;
	}

	/**
	 * Retorna a string que representa a moeda no xml.
	 *
	 * <p>M�todo para retornar a string que representa a moeda no formato xml.</p>
	 *
	 * @access public
	 * @return string Moeda no formato xml.
	 */
	public function getCurrencyString() {
		switch ($this->cscurrency) {
				
			case "Real":
				return "&quot;R$ &quot;";
				break;
					
			case "Dollar":
				return "[$$-409]";
				break;

			case "Pound":
				return "[$£-809]";
				break;
					
			case "Euro":
				return "[$€-2]\ ";
				break;
					
			default:
				return "";
				break;
		}
	}

	/**
	 * Seta os atributos de formato de n�mero.
	 *
	 * <p>M�todo para setar o formato, a moeda, a precis�o e o separador.</p>
	 *
	 * @access public
	 *
	 * @param string $psformat Formato.
	 * @param string $pscurrency Moeda.
	 * @param integer $piprecision Precis�o (default = -1).
	 * @param boolean $pbseparator Separador (default = false).
	 */
	public function setAttributes($psformat, $pscurrency, $piprecision = -1, $pbseparator = false) {
		$this->setFormat($psformat);
		$this->setCurrency($pscurrency);
		$this->setPrecision($piprecision);
		$this->setSeparator($pbseparator);
	}

	/**
	 * Retorna o xml que representa a tag "NumberFormat".
	 *
	 * <p>M�todo para retornar o c�digo xml para a tag "NumberFormat".</p>
	 *
	 * @access public
	 * @return string C�digo xml para tag "NumberFormat".
	 */
	public function createTag() {

		$msformat = "";

		switch($this->csnumberFormat) {
				
			case "Number":
				$msformat = $this->getSeparatorString().$this->getPrecisionString();
				break;
					
			case "Currency":
				if (($mscur = $this->getCurrencyString())) {
					$this->setSeparator(true);
					$msformat = $mscur.$this->getSeparatorString().$this->getPrecisionString();
				}
				else trigger_error("Currency type not defined",E_USER_ERROR);
				break;
					
			default:
				$msformat = "";
				break;
					
		}

		echo "<NumberFormat";
		echo $msformat ? " ss:Format=\"".$msformat."\"" : "";
		echo "/>";
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe que representa uma tabela.
 *
 * <p>Classe que representa a tag "Table" de uma planilha.</p>
 *
 * @package FWD5
 * @subpackage formats-excel
 */
class FWDExcelWorksheetTable {

	/**
	 * Contador de linhas.
	 * @var integer
	 * @access private
	 */
	private $cirowCount = 0;

	/**
	 * Contador de colunas.
	 * @var integer
	 * @access private
	 */
	private $cicolumnCount = 0;

	/**
	 * Array de FWDExcelColumn.
	 * @var array
	 * @access private
	 */
	private $cacolumns = array();

	/**
	 * Array de FWDExcelRow.
	 * @var array
	 * @access private
	 */
	private $carows = array();
		
	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDExcelWorksheetTable.</p>
	 *
	 * @access public
	 */
	public function __construct() {}

	/**
	 * Seta o n�mero de linhas da tabela.
	 *
	 * <p>M�todo para setar o n�mero de linhas da tabela.</p>
	 *
	 * @access public
	 * @param integer $pirowCount N�mero de linhas.
	 */
	public function setRowCount($pirowCount) {
		if ($pirowCount > 0) $this->cirowCount = $pirowCount;
		else trigger_error("Each worksheet must have at least one row",E_USER_ERROR);
	}

	/**
	 *  Retorna o n�mero de linhas da tabela.
	 *
	 * <p>M�todo para retornar o n�mero de linhas da tabela.</p>
	 *
	 * @access public
	 * @return integer N�mero de linhas.
	 */
	public function getRowCount() {
		return $this->cirowCount;
	}

	/**
	 * Seta o n�mero de colunas da tabela.
	 *
	 * <p>M�todo para setar o n�mero de colunas da tabela.</p>
	 *
	 * @access public
	 * @param integer $picolumnCount N�mero de colunas.
	 */
	public function setColumnCount($picolumnCount) {
		if ($picolumnCount > 0) $this->cicolumnCount = $picolumnCount;
		else trigger_error("Each worksheet must have at least one column",E_USER_ERROR);
	}

	/**
	 *  Retorna o n�mero de colunas da tabela.
	 *
	 * <p>M�todo para retornar o n�mero de colunas da tabela.</p>
	 *
	 * @access public
	 * @return integer N�mero de colunas.
	 */
	public function getColumnCount() {
		return $this->cicolumnCount;
	}

	/**
	 * Adiciona uma coluna na tabela.
	 *
	 * <p>M�todo para adicionar uma coluna na tabela.</p>
	 *
	 * @access public
	 * @param FWDExcelColumn $pocolumn Coluna.
	 */
	public function addColumn(FWDExcelColumn $pocolumn) {
		$this->cacolumns[] = $pocolumn;
		$this->setColumnCount($this->getColumnCount()+1);
	}

	/**
	 * Adiciona um determinado n�mero de colunas.
	 *
	 * <p>M�todo para adicionar um determinado n�mero de colunas na tabela.</p>
	 *
	 * @access public
	 *
	 * @param array $picolParameters Array com a largura de cada coluna.
	 */
	public function addColumns($picolParameters) {
		for($i=0; $i < count($picolParameters); $i++)
		$this->cacolumns[] = new FWDExcelColumn($picolParameters[$i]);
		$this->setColumnCount($this->getColumnCount()+count($picolParameters));
	}

	/**
	 * Adiciona uma linha na tabela.
	 *
	 * <p>M�todo para adicionar uma linha na tabela.</p>
	 *
	 * @access public
	 * @param FWDExcelRow $porow Linha.
	 */
	public function addRow(FWDExcelRow $porow) {
		$this->carows[] = $porow;
		$this->setRowCount($this->getRowCount()+1);
	}

	/**
	 * Retorna o xml que representa a tag "Table".
	 *
	 * <p>M�todo para retornar o c�digo xml para a tag "Table".</p>
	 *
	 * @access public
	 * @return string C�digo xml para tag "Table".
	 */
	public function createTag() {

		echo "<Table ss:ExpandedColumnCount=\"" . $this->getColumnCount() . "\" ss:ExpandedRowCount=\"" . $this->getRowCount() . "\">";

		foreach($this->cacolumns as $column) $column->createTag();
		$msStyleArray = array();
		foreach($this->carows as $row) {
			foreach($this->cacolumns as $column) $msStyleArray[] = $column->getCellStyleId();
			$row->setColumnsStyleIds($msStyleArray);
			$row->createTag();
		}

		echo "</Table>";

	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe que representa uma planilha.
 *
 * <p>Classe que representa a tag "Worksheet" de uma arquivo xml.</p>
 *
 * @package FWD5
 * @subpackage formats-excel
 */
class FWDExcelWorksheet {
		
	/**
	 * Nome da planilha.
	 * @var string
	 * @access private
	 */
	private $csname = "";

	/**
	 * Tabela.
	 * @var FWDExcelWorksheetTable
	 * @access private
	 */
	private $coTable = null;

	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDExcelWorksheet.</p>
	 *
	 * @access public
	 *
	 * @param string $psname Nome da planilha.
	 */
	public function __construct($psname) {
		$this->setName($psname);
	}

	/**
	 * Seta o nome da planilha.
	 *
	 * <p>M�todo para setar o nome da planilha.</p>
	 *
	 * @access public
	 * @param string $psname Nome da planilha.
	 */
	public function setName($psname) {
		if ($psname) $this->csname = $psname;
		else trigger_error("Invalid worksheet name",E_USER_ERROR);
	}

	/**
	 *  Retorna o nome da planilha.
	 *
	 * <p>M�todo para retornar o nome da planilha.</p>
	 *
	 * @access public
	 * @return string Nome da planilha.
	 */
	public function getName() {
		return $this->csname;
	}

	/**
	 * Seta o objeto de tabela da planilha.
	 *
	 * <p>M�todo para setar o objeto de tabela da planilha.</p>
	 *
	 * @access public
	 * @param FWDExcelWorksheetTable $poTable Tabela.
	 */
	public function setTable(FWDExcelWorksheetTable $poTable) {
		$this->coTable = $poTable;
	}

	/**
	 *  Retorna o objeto de tabela da planilha.
	 *
	 * <p>M�todo para retornar o objeto de tabela da planilha.</p>
	 *
	 * @access public
	 * @return FWDExcelWorksheetTable Tabela da planilha.
	 */
	public function getTable() {
		return $this->coTable;
	}
		
	/**
	 * Retorna o xml que representa a tag "Style".
	 *
	 * <p>M�todo para retornar o c�digo xml para a tag "Style".</p>
	 *
	 * @access public
	 * @return string C�digo xml para tag "Style".
	 */
	public function createTag() {
		echo "<Worksheet ss:Name=\"" . $this->csname . "\">";
		if($this->coTable) $this->coTable->createTag();
		echo "</Worksheet>";
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe que representa uma c�lula.
 *
 * <p>Classe que representa a tag "Cell" de uma linha.</p>
 *
 * @package FWD5
 * @subpackage formats-excel
 */
class FWDExcelCell {

	/**
	 * Array com os poss�veis valores para o tipo da c�lula.
	 * @var array
	 * @access private
	 */
	private $catypeOptions = array("", "Number", "String");

	/**
	 * Id do estilo.
	 * @var string
	 * @access private
	 */
	private $csstyleId = "";

	/**
	 * Tipo.
	 * @var string
	 * @access private
	 */
	private $cstype = "";

	/**
	 * Valor.
	 * @var string
	 * @access private
	 */
	private $csvalue = "";

	/**
	 * Link.
	 * @var string
	 * @access private
	 */
	private $cslink = "";
		
	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDExcelCell.</p>
	 *
	 * @access public
	 *
	 * @param string $psvalue Valor.
	 */
	public function __construct($psvalue) {
		$this->setValue($psvalue);
	}

	/**
	 * Seta o identificador do estilo da c�lula.
	 *
	 * <p>M�todo para setar o identificador do estilo da c�lula.</p>
	 *
	 * @access public
	 * @param string $psstyleId Id do estilo.
	 */
	public function setStyleId($psstyleId) {
		$this->csstyleId = $psstyleId;
	}

	/**
	 * Retorna o identificador do estilo da c�lula.
	 *
	 * <p>M�todo para retornar o identificador do estilo da c�lula.</p>
	 *
	 * @access public
	 * @param string Identificador do estilo.
	 */
	public function getStyleId() {
		return $this->csstyleId;
	}

	/**
	 * Seta o tipo da c�lula.
	 *
	 * <p>M�todo para setar o tipo da c�lula.</p>
	 *
	 * @access public
	 * @param string $pstype Tipo {Number | String}.
	 */
	public function setType($pstype) {
		if (in_array($pstype, $this->catypeOptions)) $this->cstype = $pstype;
		else trigger_error("Invalid cell type: ",E_USER_ERROR);
	}

	/**
	 * Retorna o tipo da c�lula.
	 *
	 * <p>M�todo para retornar o tipo da c�lula.</p>
	 *
	 * @access public
	 * @param string Tipo da c�lula.
	 */
	public function getType() {
		return $this->cstype;
	}

	/**
	 * Seta o valor da c�lula.
	 *
	 * <p>M�todo para setar o valor da c�lula. Automaticamente seta o tipo da c�lula
	 * para "Number" ou "String".</p>
	 *
	 * @access public
	 * @param string $psvalue Valor.
	 */
	public function setValue($psvalue) {
		$this->csvalue = $psvalue;
		if(is_numeric($psvalue)) $this->setType("Number");
		else $this->setType("String");
	}

	/**
	 * Retorna o valor da c�lula.
	 *
	 * <p>M�todo para retornar o valor da c�lula.</p>
	 *
	 * @access public
	 * @param string Valor da c�lula.
	 */
	public function getValue() {
		return $this->csvalue;
	}

	/**
	 * Seta o link.
	 *
	 * <p>M�todo para setar o link.</p>
	 *
	 * @access public
	 * @param string $pslink Link.
	 */
	public function setLink($pslink) {
		$this->cslink = $pslink;
	}

	/**
	 * Retorna o link.
	 *
	 * <p>M�todo para retornar o link.</p>
	 *
	 * @access public
	 * @param string Link.
	 */
	public function getLink() {
		return $this->cslink;
	}

	/**
	 * Retorna o xml que representa a tag "Cell".
	 *
	 * <p>M�todo para retornar o c�digo xml para a tag "Cell".</p>
	 *
	 * @access public
	 * @return string C�digo xml para tag "Cell".
	 */
	public function createTag() {
		echo "<Cell";
		echo $this->csstyleId ? " ss:StyleID=\"".$this->csstyleId."\"" : "";
		echo $this->cslink ? " ss:HRef=\"".$this->cslink."\"" : "";
		echo ">";

		echo "<Data";
		echo $this->cstype ? " ss:Type=\"".$this->cstype."\"" : "";
		echo ">";
		echo $this->csvalue;
		echo "</Data>";
			
		echo "</Cell>";
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe que representa uma coluna de uma tabela.
 *
 * <p>Classe que representa a tag "Column" de uma tabela.</p>
 *
 * @package FWD5
 * @subpackage formats-excel
 */
class FWDExcelColumn {

	/**
	 * Largura.
	 * @var integer
	 * @access private
	 */
	private $ciwidth = 0;

	/**
	 * Id do estilo.
	 * @var string
	 * @access private
	 */
	private $csstyleId = "";

	/**
	 * Id do estilo da c�lula.
	 * @var string
	 * @access private
	 */
	private $cscellStyleId = "";
		
	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDExcelColumn.</p>
	 *
	 * @access public
	 *
	 * @param integer $piwidth Largura (default = 150).
	 */
	public function __construct($piwidth = 150) {
		$this->setWidth($piwidth);
	}

	/**
	 * Seta a largura da coluna.
	 *
	 * <p>M�todo para setar a largura da coluna.</p>
	 *
	 * @access public
	 * @param integer $piwidth Largura.
	 */
	public function setWidth($piwidth) {
		if ($piwidth > 0) $this->ciwidth = $piwidth;
		else trigger_error("Invalid width: ",E_USER_ERROR);
			
	}

	/**
	 * Retorna a largura da coluna.
	 *
	 * <p>M�todo para retornar a largura da coluna.</p>
	 *
	 * @access public
	 *
	 * @return integer Largura.
	 */
	public function getWidth() {
		return $this->ciwidth;
	}

	/**
	 * Seta o identificador do estilo da coluna.
	 *
	 * <p>M�todo para setar o identificador do estilo da coluna.
	 * Todas as c�lulas dessa coluna receber�o esse estilo (n�o apenas
	 * as c�lulas preenchidas).</p>
	 *
	 * @access public
	 * @param string $psstyleId Id do estilo.
	 */
	public function setStyleId($psstyleId) {
		$this->csstyleId = $psstyleId;
	}

	/**
	 * Retorna o identificador do estilo.
	 *
	 * <p>M�todo para retornar o identificador do estilo.</p>
	 *
	 * @access public
	 * @return string Identificador do estilo.
	 */
	public function getStyleId() {
		return $this->csstyleId;
	}

	/**
	 * Seta o identificador do estilo das c�lulas.
	 *
	 * <p>M�todo para setar o identificador do estilo das c�lulas. Somente
	 * as c�lulas preenchidas receber�o o estilo.</p>
	 *
	 * @access public
	 * @param string $pscellStyleId Id do estilo das c�lulas.
	 */
	public function setCellStyleId($pscellStyleId) {
		$this->cscellStyleId = $pscellStyleId;
	}

	/**
	 * Retorna o identificador do estilo das c�lulas.
	 *
	 * <p>M�todo para retornar o identificador do estilo das c�lulas.</p>
	 *
	 * @access public
	 * @return string Identificador do estilo das c�lulas.
	 */
	public function getCellStyleId() {
		return $this->cscellStyleId;
	}

	/**
	 * Retorna o xml que representa a tag "Column".
	 *
	 * <p>M�todo para retornar o c�digo xml para a tag "Column".</p>
	 *
	 * @access public
	 * @return string C�digo xml para tag "Column".
	 */
	public function createTag() {
		echo "<Column";
		echo $this->ciwidth ? " ss:Width=\"".$this->ciwidth."\"" : "";
		echo $this->csstyleId ? " ss:StyleID=\"".$this->csstyleId."\"" : "";
		echo "/>";
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe que representa uma linha de uma tabela.
 *
 * <p>Classe que representa a tag "Row" de uma tabela.</p>
 *
 * @package FWD5
 * @subpackage formats-excel
 */
class FWDExcelRow {

	/**
	 * Altura.
	 * @var integer
	 * @access private
	 */
	private $ciheight = 0;

	/**
	 * Array de FWDExcelCell.
	 * @var array
	 * @access private
	 */
	private $cacells = array();

	/**
	 * Id do estilo.
	 * @var string
	 * @access private
	 */
	private $csstyleId = "";

	/**
	 * Id do estilo das c�lulas da linha.
	 * @var string
	 * @access private
	 */
	private $cscellStyleId = "";

	/**
	 * Array de identificadores de estilo.
	 * @var array
	 * @access private
	 */
	private $cacolumnsStyles = array();
		
	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDExcelRow.</p>
	 *
	 * @access public
	 *
	 * @param integer $piheight Altura (default = 15).
	 */
	public function __construct($piheight = 15) {
		$this->setHeight($piheight);
	}

	/**
	 * Seta a altura da linha.
	 *
	 * <p>M�todo para setar a altura da linha.</p>
	 *
	 * @access public
	 * @param integer $piheight Altura.
	 */
	public function setHeight($piheight) {
		if ($piheight >= 0) $this->ciheight = $piheight;
		else trigger_error("Invalid height: ",E_USER_ERROR);
	}

	/**
	 * Retorna a altura da linha.
	 *
	 * <p>M�todo para retornar a altura da linha.</p>
	 *
	 * @access public
	 * @param integer altura.
	 */
	public function getHeight() {
		return $this->ciheight;
	}

	/**
	 * Adiciona uma c�lula.
	 *
	 * <p>M�todo para adicionar uma c�lula � linha.</p>
	 *
	 * @access public
	 * @param FWDExcelCell $pocell C�lula.
	 */
	public function addCell(FWDExcelCell $pocell) {
		$this->cacells[] = $pocell;
	}

	/**
	 * Seta o identificador do estilo.
	 *
	 * <p>M�todo para setar o identificador do estilo. Todas as c�lulas dessa
	 * linha receber�o esse estilo (n�o apenas as c�lulas preenchidas).</p>
	 *
	 * @access public
	 * @param string $psstyleId Id do estilo.
	 */
	public function setStyleId($psstyleId) {
		$this->csstyleId = $psstyleId;
	}

	/**
	 * Retorna o identificador do estilo.
	 *
	 * <p>M�todo para retornar o identificador do estilo.</p>
	 *
	 * @access public
	 * @param string Identificador do estilo.
	 */
	public function getStyleId() {
		return $this->csstyleId;
	}

	/**
	 * Seta o identificador do estilo das c�lulas.
	 *
	 * <p>M�todo para setar o identificador do estilo das c�lulas. Somente
	 * as c�lulas preenchidas receber�o o estilo.</p>
	 *
	 * @access public
	 * @param string $pscellStyleId Id do estilo.
	 */
	public function setCellStyleId($pscellStyleId) {
		$this->cscellStyleId = $pscellStyleId;
	}

	/**
	 * Retorna o identificador do estilo das c�lulas.
	 *
	 * <p>M�todo para retornar o identificador do estilo das c�lulas.</p>
	 *
	 * @access public
	 * @param string Identificador do estilo das c�lulas.
	 */
	public function getCellStyleId() {
		return $this->cscellStyleId;
	}

	/**
	 * Seta o array de identificadores dos estilos das colunas.
	 *
	 * <p>M�todo para setar o array de identificadores dos estilos das colunas.</p>
	 *
	 * @access public
	 * @param array $pacolumnsStyle Ids dos estilos das colunas.
	 */
	public function setColumnsStyleIds($pacolumnsStyle) {
		$this->cacolumnsStyle = $pacolumnsStyle;
	}

	/**
	 * Retorna o xml que representa a tag "Row".
	 *
	 * <p>M�todo para retornar o c�digo xml para a tag "Row".</p>
	 *
	 * @access public
	 * @return string C�digo xml para tag "Row".
	 */
	public function createTag() {
		echo "<Row";
		echo $this->ciheight ? " ss:Height=\"".$this->ciheight."\"" : "";
		echo $this->csstyleId ? " ss:StyleID=\"".$this->csstyleId."\"" : "";
		echo ">";

		foreach($this->cacells as $key => $cell) {
			$cell->createTag();
		}

		echo "</Row>";
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe para gera��o de documentos no formato do Excel (xls).
 *
 * <p>Classe que implementa fun��es b�sicas para gere��o de planilhas
 * no excel.</p>
 * @package FWD5
 * @subpackage formats-excel
 */
class FWDExcel {
		
	/**
	 * Nome do arquivo XLS
	 * @var string
	 * @access private
	 */
	private $csfilename;

	/**
	 * Array de FWDExcelStyle
	 * @var array
	 * @access private
	 */
	private $castyles = array();

	/**
	 * Array de FWDExcelWorksheet
	 * @var array
	 * @access private
	 */
	private $caworksheets = array();

	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDExcel.</p>
	 *
	 * @access public
	 * @param string $psfilename Nome do arquivo.
	 */
	public function __construct($psfilename = "") {
		$this->csfilename = $psfilename;
	}

	/**
	 * Seta o nome do arquivo XLS.
	 *
	 * <p>M�todo para setar o nome do arquirvo XLS.</p>
	 *
	 * @access public
	 * @param string $psname Nome do arquivo.
	 */
	public function setFilename($psname) {
		$this->csfilename = $psname;
	}

	/**
	 * Retorna o nome do arquivo XLS.
	 *
	 * <p>M�todo para retornar o nome do arquirvo XLS.</p>
	 *
	 * @access public
	 * @return string Nome do arquivo.
	 */
	public function getFilename() {
		return $this->csfilename;
	}

	/**
	 * Adiciona um estilo ao arquivo XLS.
	 *
	 * <p>M�todo para adicionar um estilo no arquirvo XLS.</p>
	 *
	 * @access public
	 * @param FWDExcelStyle $poStyle Estilo.
	 */
	public function addStyle(FWDExcelStyle $poStyle) {
		$this->castyles[] = $poStyle;
	}

	/**
	 * Adiciona uma planilha ao arquivo XLS.
	 *
	 * <p>M�todo para adicionar uma planilha no arquirvo XLS.</p>
	 *
	 * @access public
	 * @param FWDExcelWorksheet $poWorksheet Planilha.
	 */
	public function addWorksheet(FWDExcelWorksheet $poWorksheet) {
		$this->caworksheets[] = $poWorksheet;
	}

	/**
	 * Gera o arquivo XLS.
	 *
	 * <p>M�todo que gera o arquivo XLS constru�do.</p>
	 *
	 * @access public
	 */
	public function output() {

		$msfilename = $this->csfilename ? $this->csfilename : "excel";

		header('Cache-Control: ');// leave blank to avoid IE errors
		header('Pragma: ');// leave blank to avoid IE errors
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="'.$this->csfilename.'.xls"');

		echo '<?xml version="1.0"?>
			<?mso-application progid="Excel.Sheet"?>
			<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
			xmlns:o="urn:schemas-microsoft-com:office:office"
			xmlns:x="urn:schemas-microsoft-com:office:excel"
			xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
			xmlns:html="http://www.w3.org/TR/REC-html40">';

		if(count($this->castyles) > 0) {
			echo "<Styles>";
			foreach($this->castyles as $mastyle) $mastyle->createTag();
			echo "</Styles>";
		}

		if(count($this->caworksheets) > 0) {
			foreach($this->caworksheets as $maworksheet) $maworksheet->createTag();
		}
		else trigger_error("Excel document must have at least one worksheet",E_USER_ERROR);

		echo '</Workbook>';

		exit();
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe para gera��o eficiente de documentos no formato do Excel (xls).
 *
 * <p>Classe mais restrita, mas mais eficiente que a classe FWDExcel na gera��o
 * de planilhas no formato xls.</p>
 * @package FWD5
 * @subpackage formats-excel
 */
class FWDExcelWriter {

	/**
	 * Nome do arquivo XLS
	 * @var string
	 * @access private
	 */
	private $csFilename;

	/**
	 * Array de FWDExcelStyle
	 * @var array
	 * @access private
	 */
	private $caStyles = array();

	/**
	 * Array com as larguras das colunas da WorkSheet atual
	 * @var array
	 * @access private
	 */
	private $caColumnsWidths;

	/**
	 * Nome da WorkSheet atual
	 * @var string
	 * @access private
	 */
	private $csWorksheetName;

	/**
	 * Indica se h� uma WorkSheet aberta
	 * @var boolean
	 * @access private
	 */
	private $cbOpenWorksheet = false;

	/**
	 * Indica se o arquivo j� come�ou a ser escrito
	 * @var boolean
	 * @access private
	 */
	private $cbFileOutputStarted = false;

	/**
	 * Indica se a WorkSheet j� come�ou a ser escrita
	 * @var boolean
	 * @access private
	 */
	private $cbWorksheetOutputStarted = false;

	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDExcelWriter.</p>
	 *
	 * @access public
	 * @param string $psfilename Nome do arquivo.
	 */
	public function __construct($psFileName = ""){
		$this->csFilename = strtr($psFileName,'\/:*?"<>|','_________');
	}

	/**
	 * Retorna o nome do arquivo XLS.
	 *
	 * <p>M�todo para retornar o nome do arquirvo XLS.</p>
	 *
	 * @access public
	 * @return string Nome do arquivo.
	 */
	public function getFilename(){
		return $this->csFilename;
	}

	/**
	 * Adiciona um estilo ao arquivo XLS.
	 *
	 * <p>M�todo para adicionar um estilo no arquirvo XLS.</p>
	 *
	 * @access public
	 * @param FWDExcelStyle $poStyle Estilo.
	 */
	public function addStyle(FWDExcelStyle $poStyle){
		$this->caStyles[] = $poStyle;
	}

	/**
	 * Abre uma nova WorkSheet
	 *
	 * <p>Abre uma nova WorkSheet. Se j� existia uma aberta, ela � fechada.</p>
	 * @access public
	 * @param string $psName Nome da nova WorkSheet
	 */
	public function openWorksheet($psName){
		if($this->cbOpenWorksheet) $this->closeWorksheet();
		$this->csWorksheetName = $psName;
		$this->caColumnsWidths = array();
		$this->cbOpenWorksheet = true;
	}

	/**
	 * Fecha a WorkSheet atual
	 *
	 * <p>Fecha a WorkSheet atual.</p>
	 * @access public
	 */
	public function closeWorksheet(){
		if($this->cbOpenWorksheet){
			echo '</Table></Worksheet>';
			$this->cbOpenWorksheet = false;
			$this->cbWorksheetOutputStarted = false;
		}else{
			trigger_error('There is no open WorkSheet.',E_USER_WARNING);
		}
	}

	/**
	 * Adiciona uma coluna � WorkSheet atual
	 *
	 * <p>Adiciona uma coluna � WorkSheet atual.</p>
	 * @access public
	 * @param integer $piWidth Largura da coluna
	 */
	public function addColumn($piWidth){
		if($this->cbWorksheetOutputStarted){
			trigger_error('All columns must be added before first row is drawn.',E_USER_ERROR);
		}else{
			$this->caColumnsWidths[] = $piWidth;
		}
	}

	/**
	 * Adiciona uma linha � WorkSheet atual
	 *
	 * <p>Adiciona uma linha � WorkSheet atual.</p>
	 * @access public
	 * @param FWDExcelRow $poRow Linha a ser adicionada
	 */
	public function drawRow(FWDExcelRow $poRow){
		if($this->cbOpenWorksheet){
			if(!$this->cbFileOutputStarted){
				$this->drawHeader();
			}
			if(!$this->cbWorksheetOutputStarted){
				$this->drawWorksheetHeader();
			}
			$poRow->createTag();
		}else{
			trigger_error('To draw a row a WorkSheet must be open.',E_USER_ERROR);
		}
	}

	/**
	 * Desenha o cabe�alho do arquivo
	 *
	 * <p>Desenha o cabe�alho do arquivo.</p>
	 * @access private
	 */
	private function drawHeader(){
		$msFilename = $this->csFilename ? $this->csFilename : "excel";
		header('Cache-Control: ');// leave blank to avoid IE errors
		header('Pragma: ');// leave blank to avoid IE errors
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="'.$msFilename.'.xls"');
		echo '<?xml version="1.0" encoding="UTF-8"?>'
		.'<?mso-application progid="Excel.Sheet"?>'
		.'<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"'
		.' xmlns:o="urn:schemas-microsoft-com:office:office"'
		.' xmlns:x="urn:schemas-microsoft-com:office:excel"'
		.' xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"'
		.' xmlns:html="http://www.w3.org/TR/REC-html40">';
		if(count($this->caStyles) > 0){
			echo "<Styles>";
			foreach($this->caStyles as $moStyle) $moStyle->createTag();
			echo "</Styles>";
		}
		$this->cbFileOutputStarted = true;
	}

	/**
	 * Desenha o cabe�alho da WorkSheet atual
	 *
	 * <p>Desenha o cabe�alho da WorkSheet atual.</p>
	 * @access private
	 */
	private function drawWorksheetHeader(){
		echo "<Worksheet ss:Name=\"{$this->csWorksheetName}\"><Table>";
		foreach($this->caColumnsWidths as $miWidth){
			echo "<Column ss:Width=\"$miWidth\"/>";
		}
		$this->cbWorksheetOutputStarted = true;
	}

	/**
	 * Encerra o arquivo
	 *
	 * <p>Encerra o arquivo.</p>
	 * @access public
	 */
	public function endFile(){
		if($this->cbOpenWorksheet) $this->closeWorksheet();
		echo '</Workbook>';
		exit();
	}

}
?><?php
/**
 * Convert HTML to MS Word file
 * @author Harish Chauhan
 * @version 1.0.0
 * @name HTML_TO_DOC
 */

class HTML_TO_DOC
{
	private $docFile="";
	private $title="";
	private $htmlHead="";
	private $htmlBody="";


	/**
	 * Constructor
	 *
	 * @return void
	 */
	function HTML_TO_DOC()
	{
		$this->title="Untitled Document";
		$this->htmlHead="";
		$this->htmlBody="";
	}

	/**
	 * Set the document file name
	 *
	 * @param String $docfile
	 */

	function setDocFileName($docfile)
	{
		$this->docFile=$docfile;
		if(!preg_match("/\.doc$/i",$this->docFile))
		$this->docFile.=".doc";
		return;
	}

	function setTitle($title)
	{
		$this->title=$title;
	}

	/**
	 * Return header of MS Doc
	 *
	 * @return String
	 */
	function getHeader()
	{   // <meta http-equiv=Content-Type content="text/html; charset=utf-8">
		$return  = <<<EOH
			 <html xmlns:v="urn:schemas-microsoft-com:vml"
			xmlns:o="urn:schemas-microsoft-com:office:office"
			xmlns:w="urn:schemas-microsoft-com:office:word"
			xmlns="http://www.w3.org/TR/REC-html40">
			
			<head>			
			<meta http-equiv=Content-Type content="text/html;">
			<meta name=ProgId content=Word.Document>
			<meta name=Generator content="Microsoft Word 9">
			<meta name=Originator content="Microsoft Word 9">
			<!--[if !mso]>
			<style>
			v\:* {behavior:url(#default#VML);}
			o\:* {behavior:url(#default#VML);}
			w\:* {behavior:url(#default#VML);}
			.shape {behavior:url(#default#VML);}
			</style>
			<![endif]-->
			<title>$this->title</title>
			<!--[if gte mso 9]><xml>
			 <w:WordDocument>
			  <w:View>Print</w:View>
			  <w:DoNotHyphenateCaps/>
			  <w:PunctuationKerning/>
			  <w:DrawingGridHorizontalSpacing>9.35 pt</w:DrawingGridHorizontalSpacing>
			  <w:DrawingGridVerticalSpacing>9.35 pt</w:DrawingGridVerticalSpacing>
			 </w:WordDocument>
			</xml><![endif]-->
			<style>
			<!--
			 /* Font Definitions */
			@font-face
				{font-family:Verdana;
				panose-1:2 11 6 4 3 5 4 4 2 4;
				mso-font-charset:0;
				mso-generic-font-family:swiss;
				mso-font-pitch:variable;
				mso-font-signature:536871559 0 0 0 415 0;}
			 /* Style Definitions */
			p.MsoNormal, li.MsoNormal, div.MsoNormal
				{mso-style-parent:"";
				margin:0in;
				margin-bottom:.0001pt;
				mso-pagination:widow-orphan;
				font-size:7.5pt;
			        mso-bidi-font-size:8.0pt;
				font-family:"Verdana";
				mso-fareast-font-family:"Verdana";}
			p.small
				{mso-style-parent:"";
				margin:0in;
				margin-bottom:.0001pt;
				mso-pagination:widow-orphan;
				font-size:1.0pt;
			        mso-bidi-font-size:1.0pt;
				font-family:"Verdana";
				mso-fareast-font-family:"Verdana";}
			@page Section1
				{size:8.5in 11.0in;
				margin:1.0in 1.25in 1.0in 1.25in;
        mso-page-orientation:landscape;
				mso-header-margin:.5in;
				mso-footer-margin:.5in;
				mso-paper-source:0;}
			div.Section1
				{page:Section1;}
			-->
			</style>
			<!--[if gte mso 9]><xml>
			 <o:shapedefaults v:ext="edit" spidmax="1032">
			  <o:colormenu v:ext="edit" strokecolor="none"/>
			 </o:shapedefaults></xml><![endif]--><!--[if gte mso 9]><xml>
			 <o:shapelayout v:ext="edit">
			  <o:idmap v:ext="edit" data="1"/>
			 </o:shapelayout></xml><![endif]-->
			 $this->htmlHead
			</head>
			<body>
EOH;
			 return $return;
	}

	/**
	 * Return Document footer
	 *
	 * @return String
	 */
	function getFotter()
	{
		return "</body></html>";
	}

	/**
	 * Create The MS Word Document from given HTML
	 *
	 * @param String $html :: URL Name like http://www.example.com
	 * @param String $file :: Document File Name
	 * @param Boolean $download :: Wheather to download the file or save the file
	 * @return boolean
	 */

	function createDocFromURL($url,$file,$download=false)
	{
		if(!preg_match("/^http:/",$url))
		$url="http://".$url;
		$html=@file_get_contents($url);
		return $this->createDoc($html,$file,$download);
	}

	/**
	 * Create The MS Word Document from given HTML
	 *
	 * @param String $html :: HTML Content or HTML File Name like path/to/html/file.html
	 * @param String $file :: Document File Name
	 * @param Boolean $download :: Wheather to download the file or save the file
	 * @return boolean
	 */

	function createDoc($html,$file,$download=false)
	{
		/*if(is_file($html))
		 $html=@file_get_contents($html);*/
			
		$this->_parseHtml($html);
		$this->setDocFileName($file);
		$doc=$this->getHeader();
		$doc.=$this->htmlBody;
		$doc.=$this->getFotter();
			
		if($download)
		{
			@header("Cache-Control: ");// leave blank to avoid IE errors
			@header("Pragma: ");// leave blank to avoid IE errors
			@header("Content-type: application/octet-stream");
			@header("Content-Disposition: attachment; filename=\"$this->docFile\"");
			echo $doc;
			return true;
		}
		else
		{
			return $this->write_file($this->docFile,$doc);
		}
	}

	/**
	 * Parse the html and remove <head></head> part if present into html
	 *
	 * @param String $html
	 * @return void
	 * @access Private
	 */

	function _parseHtml($html)
	{
		$html=preg_replace("/<!DOCTYPE((.|\n)*?)>/ims","",$html);
		$html=preg_replace("/<script((.|\n)*?)>((.|\n)*?)<\/script>/ims","",$html);
		preg_match("/<head>((.|\n)*?)<\/head>/ims",$html,$matches);
		if(!isset($matches[1])) $matches[1] = '';
		$head=$matches[1];
		preg_match("/<title>((.|\n)*?)<\/title>/ims",$head,$matches);
		if(!isset($matches[1])) $matches[1] = '';
		$this->title = $matches[1];
		$html=preg_replace("/<head>((.|\n)*?)<\/head>/ims","",$html);
		$head=preg_replace("/<title>((.|\n)*?)<\/title>/ims","",$head);
		$head=preg_replace("/<\/?head>/ims","",$head);
		$html=preg_replace("/<\/?body((.|\n)*?)>/ims","",$html);
		$this->htmlHead=$head;
		$this->htmlBody=$html;
		return;
	}

	/**
	 * Write the content int file
	 *
	 * @param String $file :: File name to be save
	 * @param String $content :: Content to be write
	 * @param [Optional] String $mode :: Write Mode
	 * @return void
	 * @access boolean True on success else false
	 */

	function write_file($file,$content,$mode="w")
	{
		$fp=@fopen($file,$mode);
		if(!is_resource($fp))
		return false;
		fwrite($fp,$content);
		fclose($fp);
		return true;
	}

}

?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe que representa um texto.
 *
 * <p>Classe que representa um texto de um par�grafo.</p>
 *
 * @package FWD5
 * @subpackage formats-word
 */
class FWDWordText extends FWDWordObject {

	/**
	 * Texto.
	 * @var string
	 * @access private
	 */
	private $cstext = "";

	/**
	 * Link.
	 * @var string
	 * @access private
	 */
	private $cslink = "";

	/**
	 * Estilo.
	 * @var FWDWordStyle
	 * @access private
	 */
	private $coStyle = null;

	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDWordText.</p>
	 *
	 * @access public
	 *
	 * @param string $pstext Texto (opcional).
	 * @param FWDWordStyle $poStyle Estilo (opcional).
	 */
	public function __construct($pstext = "", $poStyle = null) {
		$this->cstext = $pstext;
		$this->coStyle = $poStyle;
	}

	/**
	 * Seta o texto.
	 *
	 * <p>M�todo para setar o texto.</p>
	 *
	 * @access public
	 *
	 * @param string $pstext Texto.
	 */
	public function setText($pstext) {
		$this->cstext = $pstext;
	}

	/**
	 * Retorna o texto.
	 *
	 * <p>M�todo para retornar o texto.</p>
	 *
	 * @access public
	 * @return string Texto.
	 */
	public function getText() {
		return $this->cstext;
	}

	/**
	 * Seta o link.
	 *
	 * <p>M�todo para setar o link.</p>
	 *
	 * @access public
	 *
	 * @param string $pslink Link.
	 */
	public function setLink($pslink) {
		$this->cslink = $pslink;
	}

	/**
	 * Retorna o link.
	 *
	 * <p>M�todo para retornar o link.</p>
	 *
	 * @access public
	 *
	 * @return string Link.
	 */
	public function getLink() {
		return $this->cslink;
	}

	/**
	 * Seta o objeto de estilo do texto.
	 *
	 * <p>M�todo para setar o objeto de estilo do texto.</p>
	 *
	 * @access public
	 *
	 * @param FWDWordStyle $poStyle Estilo.
	 */
	public function setStyle(FWDWordStyle $poStyle) {
		$this->coStyle = $poStyle;
	}

	/**
	 * Retorna o objeto de estilo do texto.
	 *
	 * <p>M�todo para retornar o objeto de estilo do texto.</p>
	 *
	 * @access public
	 *
	 * @param FWDWordStyle Estilo.
	 */
	public function getStyle() {
		return $this->coStyle;
	}

	/**
	 * Retorna o html que representa o par�grafo.
	 *
	 * <p>M�todo para retornar o html que representa o par�grafo.</p>
	 *
	 * @access public
	 *
	 * @return string C�digo html para o par�grafo.
	 */
	public function createHtml() {
		$mscontent = "";
		$mscontent .= $this->coStyle ? "<font ".$this->coStyle->createHtml().">" : "";
		$mscontent .= $this->cslink ? "<a href=\"".$this->cslink."\">" : "";
		$mscontent .= $this->cstext;
		$mscontent .= $this->cslink ? "</a>" : "";
		$mscontent .= $this->coStyle ? "</font>" : "";
			
		return $mscontent;
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe que representa um estilo.
 *
 * <p>Classe que representa um estilo de um arquivo word.</p>
 *
 * @package FWD5
 * @subpackage formats-word
 */
class FWDWordStyle {

	/**
	 * Array com os poss�veis valores para o alinhamento do par�grafo.
	 * @var array
	 * @access private
	 */
	private $caalignmentOptions = array("", "left", "center", "right", "justify");

	/**
	 * Alinhamento.
	 * @var string
	 * @access private
	 */
	private $csalignment = "";

	/**
	 * Nome da fonte.
	 * @var string
	 * @access private
	 */
	private $csfont = "";

	/**
	 * Tamanho da fonte.
	 * @var integer
	 * @access private
	 */
	private $cisize = 0;

	/**
	 * Cor da fonte.
	 * @var string
	 * @access private
	 */
	private $cscolor = "";

	/**
	 * Negrito.
	 * @var boolean
	 * @access private
	 */
	private $cbbold = false;

	/**
	 * It�lico.
	 * @var boolean
	 * @access private
	 */
	private $cbitalic = false;
		
	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDWordStyle.</p>
	 *
	 * @access public
	 */
	public function __construct() {}

	/**
	 * Seta o nome da fonte.
	 *
	 * <p>M�todo para setar o nome da fonte.</p>
	 *
	 * @access public
	 *
	 * @param string $psfont Nome da fonte.
	 */
	public function setFont($psfont) {
		$this->csfont = $psfont;
	}

	/**
	 * Retorna o nome da fonte.
	 *
	 * <p>M�todo para retornar o nome da fonte.</p>
	 *
	 * @access public
	 * @return string Nome da lista.
	 */
	public function getFont() {
		return $this->csfont;
	}

	/**
	 * Seta o tamanho da fonte.
	 *
	 * <p>M�todo para setar o tamanho da fonte.</p>
	 *
	 * @access public
	 * @param integer $pisize Tamanho da fonte.
	 */
	public function setSize($pisize) {
		if ($pisize > 0) $this->cisize = $pisize;
		else trigger_error("Invalid font size: ",E_USER_ERROR);
	}

	/**
	 * Retorna o tamanho da fonte.
	 *
	 * <p>M�todo para retornar o tamanho da fonte.</p>
	 *
	 * @access public
	 * @return integer Tamanho da fonte.
	 */
	public function getSize() {
		return $this->cisize;
	}

	/**
	 * Seta a cor da fonte.
	 *
	 * <p>M�todo para setar a cor da fonte.</p>
	 *
	 * @access public
	 * @param integer $piR Cor da fonte (Vermelho).
	 * @param integer $piG Cor da fonte (Verde).
	 * @param integer $piB Cor da fonte (Azul).
	 */
	public function setColor($piR, $piG, $piB) {
		if(($mscolor = FWDWordColors::color($piR, $piG, $piB))) $this->cscolor = $mscolor;
		else trigger_error("Invalid rgb color: \"" . "($piR,$piG,$piB)" .  "\"",E_USER_ERROR);
	}

	/**
	 * Retorna a cor da fonte.
	 *
	 * <p>M�todo para retornar a cor da fonte.</p>
	 *
	 * @access public
	 * @return string Cor da fonte.
	 */
	public function getColor() {
		return $this->cscolor;
	}

	/**
	 * Seta o texto como negrito.
	 *
	 * <p>M�todo para setar o texto como negrito.</p>
	 *
	 * @access public
	 *
	 * @param boolean $pbbold Negrito.
	 */
	public function setBold($pbbold) {
		$this->cbbold = $pbbold;
	}

	/**
	 * Retorna o estilo da fonte (negrito ou n�o).
	 *
	 * <p>M�todo para retornar o estilo da fonte (negrito ou n�o).</p>
	 *
	 * @access public
	 * @return string Estilo da fonte (negrito ou n�o).
	 */
	public function isBold() {
		return $this->cbbold;
	}

	/**
	 * Seta o texto como it�lico.
	 *
	 * <p>M�todo para setar o o texto como it�lico.</p>
	 *
	 * @access public
	 *
	 * @param boolean $pbitalic It�lico.
	 */
	public function setItalic($pbitalic) {
		$this->cbitalic = $pbitalic;
	}

	/**
	 * Retorna o estilo da fonte (it�lico ou n�o).
	 *
	 * <p>M�todo para retornar o estilo da fonte (it�lico ou n�o).</p>
	 *
	 * @access public
	 * @return string Estilo da fonte (it�lico ou n�o).
	 */
	public function isItalic() {
		return $this->cbitalic;
	}

	/**
	 * Seta o alinhamento.
	 *
	 * <p>M�todo para setar o alinhamento do estilo.</p>
	 *
	 * @access public
	 * @param string $psalign Alinhamento do estilo {Left | Center | Right | Justify}.
	 */
	public function setAlignment($psalign) {
		if(in_array($psalign, $this->caalignmentOptions)) $this->csalignment = $psalign;
		else trigger_error("Invalid token: \"" . $psalign . "\"",E_USER_ERROR);
	}

	/**
	 * Retorna o alinhamento.
	 *
	 * <p>M�todo para retornar o alinhamento do par�grafo.</p>
	 *
	 * @access public
	 * @return string Alinhamento do par�grafo.
	 */
	public function getAlignment() {
		return $this->csalignment;
	}

	/**
	 * Retorna o html que representa o estilo.
	 *
	 * <p>M�todo para retornar o c�digo html do estilo.</p>
	 *
	 * @access public
	 * @return string C�digo html do estilo.
	 */
	public function createHtml() {
		if ($this->csfont || $this->cisize || $this->cscolor || $this->cbitalic || $this->cbbold || $this->csalignment) {
			$mscontent = "style=\"";
			$mscontent .= $this->csalignment ? "text-align:{$this->csalignment};" : "";
			$mscontent .= $this->csfont ? "font-family:{$this->csfont};" : "";
			$mscontent .= $this->cisize ? "font-size:{$this->cisize}.0pt;" : "";
			$mscontent .= $this->cscolor ? "color:{$this->cscolor};" : "";
			$mscontent .= $this->cbitalic ? "font-style:italic;" : "";
			$mscontent .= $this->cbbold ? "font-weight:bold;" : "";
			$mscontent .= "\"";
		}
		else $mscontent = "";
		return $mscontent;
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe que representa um bloco de texto.
 *
 * <p>Classe que representa um bloco de texto de um documento.</p>
 *
 * @package FWD5
 * @subpackage formats-word
 */
class FWDWordTextBlock {
		
	/**
	 * Array de FWDWordObject.
	 * @var array
	 * @access private
	 */
	private $caobjects = array();

	/**
	 * Estilo.
	 * @var FWDWordStyle
	 * @access private
	 */
	private $coStyle = null;

	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDWordTextBlock.</p>
	 *
	 * @access public
	 */
	public function __construct() {}

	/**
	 * Seta o objeto de estilo do bloco de texto.
	 *
	 * <p>M�todo para setar o objeto de estilo do bloco de texto.</p>
	 *
	 * @access public
	 *
	 * @param FWDWordStyle $poStyle Estilo.
	 */
	public function setStyle(FWDWordStyle $poStyle) {
		$this->coStyle = $poStyle;
	}

	/**
	 * Retorna o objeto de estilo do bloco de texto.
	 *
	 * <p>M�todo para retornar o objeto de estilo do bloco de texto.</p>
	 *
	 * @access public
	 *
	 * @param FWDWordStyle Estilo.
	 */
	public function getStyle() {
		return $this->coStyle;
	}

	/**
	 * Adiciona objeto ao bloco de texto.
	 *
	 * <p>M�todo para adicionar um objeto ao bloco de texto. Pode ser um FWDWordText ou um FWDWordTable</p>
	 *
	 * @access public
	 *
	 * @param FWDWordObject $poValue Valor.
	 */
	public function addObj(FWDWordObject $poValue) {
		$this->caobjects[] = $poValue;
	}

	/**
	 * Adiciona objeto de texto ao bloco de texto.
	 *
	 * <p>M�todo para adicionar um objeto de texto ao bloco de texto.</p>
	 *
	 * @access public
	 *
	 * @param string $pstext Texto.
	 * @param FWDWordStyle $poStyle Estilo.
	 */
	public function addText($pstext, $poStyle = null) {
		$moText = new FWDWordText();
		$moText->setText($pstext);
		$poStyle ? $moText->setStyle($poStyle) : "";
		$this->caobjects[] = $moText;
	}

	/**
	 * Adiciona uma ou mais linhas ao bloco de texto.
	 *
	 * <p>M�todo para adicionar uma ou mais linhas ao bloco de texto.</p>
	 *
	 * @access public
	 *
	 * @param integer $pilines N�mero de linhas (default = 1).
	 */
	public function addLine($pilines = 1) {
		$mslines = "";
		for($miI=0;$miI<$pilines;$miI++) $mslines.="<br/>";
		$this->addText($mslines);
	}

	/**
	 * Retorna o html que representa o bloco de texto.
	 *
	 * <p>M�todo para retornar o html que representa o bloco de texto.</p>
	 *
	 * @access public
	 * @return string C�digo html para o bloco de texto.
	 */
	public function createHtml() {
		$mscontent = $this->coStyle ? "<div " . $this->coStyle->createHtml() . ">" : "<div>";

		if (count($this->caobjects)) foreach ($this->caobjects as $object) $mscontent .= $object->createHtml();

		$mscontent .= "</div>";
			
		return $mscontent;
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe que representa uma tabela.
 *
 * <p>Classe que representa uma tabela de um arquivo word.</p>
 *
 * @package FWD5
 * @subpackage formats-word
 */
class FWDWordTable extends FWDWordObject {

	/**
	 * Tamanho da borda.
	 * @var integer
	 * @access private
	 */
	private $ciborder = 0;

	/**
	 * Array de FWDWordRow.
	 * @var array
	 * @access private
	 */
	private $carows = array();
		
	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDWordTable.</p>
	 *
	 * @access public
	 */
	public function __construct() {}

	/**
	 * Seta o tamanho da borda.
	 *
	 * <p>M�todo para setar o tamanho da borda.</p>
	 *
	 * @access public
	 * @param integer $piborder Tamanho da borda.
	 */
	public function setBorder($piborder) {
		if ($piborder > 0) $this->ciborder = $piborder;
		else trigger_error("Invalid border size: " . $piborder,E_USER_ERROR);
	}

	/**
	 * Retorna o tamanho da borda.
	 *
	 * <p>M�todo para retornar o tamanho da borda.</p>
	 *
	 * @access public
	 * @return integer Tamanho da borda.
	 */
	public function getBorder() {
		return $this->ciborder;
	}

	/**
	 * Retorna o n�mero de linhas da tabela.
	 *
	 * <p>M�todo para retornar o n�mero de linhas da tabela.</p>
	 *
	 * @access public
	 * @return integer N�mero de linhas.
	 */
	public function getRowCount() {
		return count($this->carows);
	}

	/**
	 * Adiciona uma linha na tabela.
	 *
	 * <p>M�todo para adicionar uma linha na tabela.</p>
	 *
	 * @access public
	 * @param FWDWordRow $porow Linha.
	 */
	public function addRow(FWDWordRow $porow) {
		$this->carows[] = $porow;
	}

	/**
	 * Retorna o html que representa a tabela.
	 *
	 * <p>M�todo para retornar o c�digo html da tabela.</p>
	 *
	 * @access public
	 * @return string C�digo html da tabela.
	 */
	public function createHtml() {
		$mscontent = "<table";
		$mscontent .= $this->ciborder ? " border='" . $this->ciborder . "'>" : ">";
		foreach ($this->carows as $moRow) $mscontent .= $moRow->createHtml();
		$mscontent .= "</table>";
		return $mscontent;
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe que representa uma coluna de uma tabela.
 *
 * <p>Classe que representa a tag "td" de uma tabela.</p>
 *
 * @package FWD5
 * @subpackage formats-word
 */
class FWDWordColumn {

	/**
	 * Largura.
	 * @var integer
	 * @access private
	 */
	private $ciwidth = 0;

	/**
	 * Estilo.
	 * @var FWDWordStyle
	 * @access private
	 */
	private $coStyle = null;

	/**
	 * Valor.
	 * @var FWDWordObject
	 * @access private
	 */
	private $coValue = null;

	/**
	 * Cor do fundo.
	 * @var string
	 * @access private
	 */
	private $csbgColor = "";
		
	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDWordColumn.</p>
	 *
	 * @access public
	 *
	 * @param integer $piwidth Largura (default = 100).
	 */
	public function __construct($piwidth = 100) {
		$this->setWidth($piwidth);
	}

	/**
	 * Seta a largura da coluna.
	 *
	 * <p>M�todo para setar a largura da coluna.</p>
	 *
	 * @access public
	 * @param integer $piwidth Largura.
	 */
	public function setWidth($piwidth) {
		if ($piwidth > 0) $this->ciwidth = $piwidth;
		else trigger_error("Invalid column width: ",E_USER_ERROR);
	}

	/**
	 * Retorna a largura da coluna.
	 *
	 * <p>M�todo para retornar a largura da coluna.</p>
	 *
	 * @access public
	 * @param integer largura.
	 */
	public function getWidth() {
		return $this->ciwidth;
	}

	/**
	 * Seta o objeto de estilo da coluna.
	 *
	 * <p>M�todo para setar o objeto de estilo da coluna.</p>
	 *
	 * @access public
	 * @param FWDWordStyle $poStyle Estilo.
	 */
	public function setStyle(FWDWordStyle $poStyle) {
		$this->coStyle = $poStyle;
	}

	/**
	 * Retorna o objeto de estilo.
	 *
	 * <p>M�todo para retornar o objeto de estilo.</p>
	 *
	 * @access public
	 * @return FWDWordStyle Estilo.
	 */
	public function getStyle() {
		return $this->coStyle;
	}

	/**
	 * Seta o valor da coluna. Pode ser um FWDWordText ou um FWDWordTable.
	 *
	 * <p>M�todo para setar o objeto com o valor da coluna. Pode ser um FWDWordText ou um FWDWordTable.</p>
	 *
	 * @access public
	 * @param FWDWordObject $poValue Valor.
	 */
	public function setValue(FWDWordObject $poValue) {
		$this->coValue = $poValue;
	}

	/**
	 * Retorna o objeto de valor.
	 *
	 * <p>M�todo para retornar o objeto de valor.</p>
	 *
	 * @access public
	 * @return FWDWordObject Valor.
	 */
	public function getValue() {
		return $this->coValue;
	}

	/**
	 * Seta a cor do fundo.
	 *
	 * <p>M�todo para setar a cor do fundo.</p>
	 *
	 * @access public
	 * @param integer $piR Cor do fundo (Vermelho).
	 * @param integer $piG Cor do fundo (Verde).
	 * @param integer $piB Cor do fundo (Azul).
	 */
	public function setBgColor($piR, $piG, $piB) {
		if(($msbgColor = FWDWordColors::color($piR, $piG, $piB))) $this->csbgColor = $msbgColor;
		else trigger_error("Invalid rgb color: \"" . "($piR,$piG,$piB)" .  "\"",E_USER_ERROR);
	}

	/**
	 * Retorna a cor do fundo.
	 *
	 * <p>M�todo para retornar a cor do fundo.</p>
	 *
	 * @access public
	 * @return string Cor do fundo.
	 */
	public function getBgColor() {
		return $this->csbgColor;
	}

	/**
	 * Retorna o html que representa a tag "td".
	 *
	 * <p>M�todo para retornar o c�digo html para a tag "td".</p>
	 *
	 * @access public
	 * @return string C�digo html para tag "td".
	 */
	public function createHtml() {

		$mscontent = "<td";
		$mscontent .= $this->csbgColor ? " bgcolor=\"".$this->csbgColor."\"" : "";
		$mscontent .= $this->ciwidth ? " width=\"".$this->ciwidth."\"" : "";
		$mscontent .= $this->coStyle ? $this->coStyle->createHtml() : "";
		$mscontent .= ">";
		$mscontent .= "<div>";
		$mscontent .= $this->coValue ? $this->coValue->createHtml() : "";
		$mscontent .= "</div></td>";

		return $mscontent;
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe que representa uma linha de uma tabela.
 *
 * <p>Classe que representa a tag "tr" de uma tabela.</p>
 *
 * @package FWD5
 * @subpackage formats-word
 */
class FWDWordRow {

	/**
	 * Altura.
	 * @var integer
	 * @access private
	 */
	private $ciheight = 0;
		
	/**
	 * Estilo.
	 * @var FWDWordStyle
	 * @access private
	 */
	private $coStyle = null;

	/**
	 * Array de FWDWordColumn.
	 * @var array
	 * @access private
	 */
	private $cacolumns = array();

	/**
	 * Cor do fundo.
	 * @var string
	 * @access private
	 */
	private $csbgColor = "";

	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDWordRow.</p>
	 *
	 * @access public
	 *
	 * @param integer $piheight Altura (default = 15).
	 */
	public function __construct($piheight = 15) {
		$this->setHeight($piheight);
	}

	/**
	 * Seta a altura da linha.
	 *
	 * <p>M�todo para setar a altura da linha.</p>
	 *
	 * @access public
	 * @param integer $piheight Altura.
	 */
	public function setHeight($piheight) {
		if ($piheight >= 0) $this->ciheight = $piheight;
		else trigger_error("Invalid height: ",E_USER_ERROR);
	}

	/**
	 * Retorna a altura da linha.
	 *
	 * <p>M�todo para retornar a altura da linha.</p>
	 *
	 * @access public
	 * @param integer altura.
	 */
	public function getHeight() {
		return $this->ciheight;
	}

	/**
	 * Adiciona uma coluna na tabela.
	 *
	 * <p>M�todo para adicionar uma coluna na tabela.</p>
	 *
	 * @access public
	 * @param FWDWordColumn $pocolumn Coluna.
	 */
	public function addColumn(FWDWordColumn $pocolumn) {
		$this->cacolumns[] = $pocolumn;
	}

	/**
	 * Seta o objeto de estilo da linha.
	 *
	 * <p>M�todo para setar o objeto de estilo da linha.</p>
	 *
	 * @access public
	 * @param FWDWordStyle $poStyle Estilo.
	 */
	public function setStyle(FWDWordStyle $poStyle) {
		$this->coStyle = $poStyle;
	}

	/**
	 * Retorna o objeto de estilo.
	 *
	 * <p>M�todo para retornar o objeto de estilo.</p>
	 *
	 * @access public
	 * @return FWDWordStyle Estilo.
	 */
	public function getStyle() {
		return $this->coStyle;
	}

	/**
	 * Retorna o n�mero de colunas da linha.
	 *
	 * <p>M�todo para retornar o n�mero de colunas da linha.</p>
	 *
	 * @access public
	 * @return integer N�mero de colunas.
	 */
	public function getColumnCount() {
		return count($this->cacolumns);
	}

	/**
	 * Seta a cor do fundo.
	 *
	 * <p>M�todo para setar a cor do fundo.</p>
	 *
	 * @access public
	 * @param integer $piR Cor do fundo (Vermelho).
	 * @param integer $piG Cor do fundo (Verde).
	 * @param integer $piB Cor do fundo (Azul).
	 */
	public function setBgColor($piR, $piG, $piB) {
		if(($msbgColor = FWDWordColors::color($piR, $piG, $piB))) $this->csbgColor = $msbgColor;
		else trigger_error("Invalid rgb color: \"" . "($piR,$piG,$piB)" .  "\"",E_USER_ERROR);
	}

	/**
	 * Retorna a cor do fundo.
	 *
	 * <p>M�todo para retornar a cor do fundo.</p>
	 *
	 * @access public
	 * @return string Cor do fundo.
	 */
	public function getBgColor() {
		return $this->csbgColor;
	}

	/**
	 * Retorna o html que representa a tag "tr".
	 *
	 * <p>M�todo para retornar o c�digo html para a tag "tr".</p>
	 *
	 * @access public
	 * @return string C�digo html para tag "tr".
	 */
	public function createHtml() {
		$mscontent = "<tr";
		$mscontent .= $this->csbgColor ? " bgcolor=\"".$this->csbgColor."\"" : "";
		$mscontent .= $this->ciheight ? " height=\"".$this->ciheight."\"" : "";
		$mscontent .= $this->coStyle ? $this->coStyle->createHtml() : "";
		$mscontent .= ">";

		foreach($this->cacolumns as $column) $mscontent .= $column->createHtml();

		$mscontent .= "</tr>";

		return $mscontent;
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe para tratamento de cores.
 *
 * <p>Classe para tratamento de cores em documentos DOC.</p>
 *
 * @package FWD5
 * @subpackage formats-word
 */
class FWDWordColors {

	/**
	 * Converte de rgb para o formato html.
	 *
	 * <p>M�todo para converter uma cor especificada em rgb
	 * para o formato html (ex: #ff8800).</p>
	 *
	 * @access public
	 * @param integer $piR Vermelho.
	 * @param integer $piG Verde.
	 * @param integer $piB Azul.
	 *
	 * @return string Html color (ex: #FF00FF).
	 *
	 * @static
	 */
	public static function color($piR, $piG, $piB) {
		$mscolor = "";
		if (($piR >=0 && $piR <= 255) && ($piG >=0 && $piG <= 255) && ($piB >=0 && $piB <= 255))
		$mscolor = "#" . str_pad(dechex(($piR<<16)|($piG<<8)|$piB), 6, "0", STR_PAD_LEFT);
		else
		$mscolor = "";

		return $mscolor;
	}
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe para gera��o de documentos no formato do Word (doc).
 *
 * <p>Classe que implementa fun��es b�sicas para gere��o de documentos
 * no word.</p>
 * @package FWD5
 * @subpackage formats-word
 */
class FWDWord {
		
	/**
	 * Nome do arquivo DOC.
	 * @var string
	 * @access private
	 */
	private $csfilename;

	/**
	 * T�tulo do documento.
	 * @var string
	 * @access private
	 */
	private $cstitle;

	/**
	 * Array de FWDWordTextBlock.
	 * @var array
	 * @access private
	 */
	private $caBlocks = array();

	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDWord.</p>
	 *
	 * @access public
	 *
	 * @param string $psfilename Nome do arquivo (opcional).
	 * @param string $pstitle T�tulo do documento (opcional).
	 */
	public function __construct($psfilename = "", $pstitle = "") {
		$this->csfilename = $psfilename;
		$this->cstitle = $pstitle;
	}

	/**
	 * Seta o nome do arquivo DOC.
	 *
	 * <p>M�todo para setar o nome do arquirvo DOC.</p>
	 *
	 * @access public
	 * @param string $psname Nome do arquivo.
	 */
	public function setFilename($psname) {
		$this->csfilename = $psname;
	}

	/**
	 * Retorna o nome do arquivo DOC.
	 *
	 * <p>M�todo para retornar o nome do arquirvo DOC.</p>
	 *
	 * @access public
	 * @return string Nome do arquivo.
	 */
	public function getFilename() {
		return $this->csfilename;
	}

	/**
	 * Seta o t�tulo do documento.
	 *
	 * <p>M�todo para setar o t�tulo do documento.</p>
	 *
	 * @access public
	 * @param string $pstitle T�tulo do documento.
	 */
	public function setTitle($pstitle) {
		$this->cstitle = $pstitle;
	}

	/**
	 * Retorna o t�tulo do documento.
	 *
	 * <p>M�todo para retornar o t�tulo do documento.</p>
	 *
	 * @access public
	 * @return string T�tulo do documento.
	 */
	public function getTitle() {
		return $this->cstitle;
	}

	/**
	 * Adiciona objeto de bloco de texto ao documento.
	 *
	 * <p>M�todo para adicionar um objeto de bloco de texto ao documento.</p>
	 *
	 * @access public
	 *
	 * @param FWDWordTextBlock $poBlock Bloco de texto.
	 */
	public function addBlock(FWDWordTextBlock $poBlock) {
		$this->caBlocks[] = $poBlock;
	}

	/**
	 * Gera o arquivo DOC.
	 *
	 * <p>M�todo que gera o arquivo DOC constru�do.</p>
	 *
	 * @access public
	 */
	public function output() {

		$modoc = new HTML_TO_DOC();

		if ($this->csfilename) $modoc->setDocFileName($this->csfilename);
		else $modoc->setDocFileName("word");

		$mscontent = "<html>";

		$mscontent .= $this->cstitle ? "<head><title>".$this->cstitle."</title></head>" : "";

		$mscontent .= "<body></pre>";

		foreach ($this->caBlocks as $block)
		$mscontent .= $block->createHtml();

		$mscontent .= "</pre></body></html>";

		$modoc->createDoc($mscontent, $this->getFilename(), true);
	}
}
?><?
/*
 PHPWordLib version 2.0

 Description: The PHPWordLib is a simple (but quite usefull) converter that converts MS Word and RTF files to plain text. Some restrictions apply however:
 - Currently supports version of MS Word 97 or newer (97/2000/XP/2003/... etc)
 - Currently supports RTF (Rich Text Format) versions up to 1.5 (the currently latest version)
 - If the file is saved without accepting track changes the output text might not be fully correct
 - The file must be fully saved, i.e. if the file was fast saved then only part of the document will be retrieved
 - No international character sets support for MS Word files - only ANSI
 - Text in text boxes or other special controls is not retrieved when converting from MS Word

 Hope to have those supported at some time in the future...

 Written by: Bogomil Alexandrov, Peter Litov
 20.11.2004
 (C) Motion Ltd. 2004. All rights reserved!
 Sofia, Bulgaria
 http://www.motion-bg.com

 Can be distributed under GNU, but a contribution from your site (in the form of link to http://www.motion-bg.com) is highly appreciated!
 */

class PHPWordLib {

	function LoadFile($filename) {
		$f = fopen($filename, "rb");
		$contents = fread($f, filesize($filename));
		fclose($f);

		if (!$this->CheckWordFormat($contents)) $isWord = FALSE; else $isWord = TRUE;
		if (!$this->CheckRTFFormat($contents)) $isRTF = FALSE; else $isRTF = TRUE;

		if (!$isWord && !$isRTF) return FALSE; else return $contents;
	}

	function CheckWordFormat(&$contents) {
		// This function checks the format of the file. Currently only MS Word 97/2000/XP/2003/... (Windows) format using ANSI character set is supported, so for now we will return false on all others!
		if (ord($contents[512]) != 236  && ord($contents[513]) != 165) return FALSE;
		if (ord($contents[530]) != 0) return FALSE;
		if (ord($contents[532]) > 0) return FALSE;
		if (ord($contents[546]) != 98 && ord($contents[547]) != 106) return FALSE;

		return TRUE;
	}

	function CheckRTFFormat(&$contents) {
		// This function checks whether the format of the file is RTF (Rich Text Format). Currently supported version is up to 1.5
		if (substr($contents, 0, 6) == '{\rtf1') return TRUE; else return FALSE;
	}

	function GetWordPlainText(&$contents) {
		// This function will return the text of a MS Word document in a plain text format. Note: This will only get the main text for now, no text boxes, etc.
		$s1 = (ord($contents[536]) + (ord($contents[537]) << 8) + (ord($contents[538]) << 16) + (ord($contents[539]) << 24)) + 512;
		$s2 = (ord($contents[540]) + (ord($contents[541]) << 8) + (ord($contents[542]) << 16) + (ord($contents[543]) << 24)) + 512;
		if ($s2 > $s1) {
			$plaintext = substr($contents, $s1, $s2-$s1);

			$ss1 = strpos($plaintext, chr(19));
			if ($ss1) {
				$ss2 = strpos($plaintext, chr(21), $ss1);
				$plaintext = substr_replace($plaintext, '', $ss1, $ss2-$ss1+1);
			}
			$plaintext = str_replace(chr(0), "", $plaintext);
			$plaintext = str_replace(chr(1), "", $plaintext);
			$plaintext = str_replace(chr(2), "", $plaintext);
			$plaintext = str_replace(chr(3), "", $plaintext);
			$plaintext = str_replace(chr(4), "", $plaintext);
			$plaintext = str_replace(chr(5), "", $plaintext);
			$plaintext = str_replace(chr(6), "", $plaintext);
			$plaintext = str_replace(chr(7), "   ", $plaintext);
			$plaintext = str_replace(chr(8), "", $plaintext);
			$plaintext = str_replace(chr(11), "\n", $plaintext);
			$plaintext = str_replace(chr(12), "\n\n", $plaintext);
			$plaintext = str_replace(chr(13), "\n", $plaintext);
			$plaintext = str_replace(chr(14), "   ", $plaintext);
			$plaintext = str_replace(chr(15), "", $plaintext);
			$plaintext = str_replace(chr(16), "", $plaintext);
			$plaintext = str_replace(chr(17), "", $plaintext);
			$plaintext = str_replace(chr(18), "", $plaintext);
			$plaintext = str_replace(chr(20), "   ", $plaintext);
			$plaintext = str_replace(chr(22), "", $plaintext);
			$plaintext = str_replace(chr(23), "", $plaintext);
			$plaintext = str_replace(chr(24), "", $plaintext);
			$plaintext = str_replace(chr(25), "", $plaintext);
			$plaintext = str_replace(chr(26), "", $plaintext);
			$plaintext = str_replace(chr(27), "", $plaintext);
			$plaintext = str_replace(chr(28), "", $plaintext);
			$plaintext = str_replace(chr(29), "", $plaintext);
			$plaintext = str_replace(chr(30), "", $plaintext);
			$plaintext = str_replace(chr(31), "-", $plaintext);
			$plaintext = str_replace(chr(160), " ", $plaintext);

			return $plaintext;
		} else return false;
	}

	function ExtractRTFBlock($blockname) {
		global $contents;
		$blockname = '{\\' . $blockname;
		$s1 = strpos($contents, $blockname);
		$s2 = strpos($contents, '}}', $s1);
		return substr($contents, $s1, $s2-$s1+1);
	}

	function RemoveRTFBlock($blockname) {
		global $contents;
		$blockname = '{\\' . $blockname;
		$s1 = strpos($contents, $blockname);
		$s2 = strpos($contents, '}}', $s1);
		$part1 = substr($contents, 0, $s1-1);
		$part2 = substr($contents, $s2+1);
		return $part1 . $part2;
	}

	function GetRTFPlainText(&$contents) {
		$s1 = strpos($contents, '{', 1);
		$rtf_header = substr($contents, 0, $s1);
		if (substr($rtf_header, 0, 6) == '{\rtf1') $rtf_version = 1; else return FALSE;

		if (strpos($rtf_header, '\ansi')) $rtf_charset = 'ansi';
		if (strpos($rtf_header, '\mac')) $rtf_charset = 'mac';
		if (strpos($rtf_header, '\pc')) $rtf_charset = 'pc';

		$count = preg_match_all("(\\\ansicpg([0-9]+))", $rtf_header, $res);
		if ($count > 0) $rtf_codepage = $res[1][0];

		$count = preg_match_all("(\\\uc([0-9]{1}))", $rtf_header, $res);
		if ($count > 0) $rtf_unicodebytes = $res[1][0];

		$count = preg_match_all("(\\\deflang([0-9]+))", $rtf_header, $res);
		if ($count > 0) $rtf_deflang = $res[1][0];

		$count = preg_match_all("(\\\deflangfe([0-9]+))", $rtf_header, $res);
		if ($count > 0) $rtf_deflangfe = $res[1][0];

		$contents = $this->RemoveRTFBlock('fonttbl');
		$contents = $this->RemoveRTFBlock('colortbl');
		$info_block = $this->ExtractRTFBlock('info');
		$contents = $this->RemoveRTFBlock('info');
		$s1 = 1;
		while ($s1 > 0) {
			$contents = $this->RemoveRTFBlock('*');
			$s1 = strpos($contents, '\*');
		}

		$plain_text = $contents;
		$plain_text = str_replace("\n", "", $plain_text);
		$plain_text = str_replace("\r", "", $plain_text);
		$plain_text = str_replace('\pard', "\dummy", $plain_text);

		$plain_text = preg_replace("/(\\\\'([0-9a-zA-Z]*))/e", 'chr(hexdec("\\1"))', $plain_text); // Convert hexdecimal symbols into letters
		$plain_text = preg_replace("(\\\par([ ]){0,1})", "\n", $plain_text); // Convert \par to \n
		$plain_text = preg_replace("(\\\(sn|sv)+([a-zA-Z0-9 -:_\\\\])*([ ]){0,1})", "", $plain_text); // Clean up all unused control symbols
		$plain_text = preg_replace("(\\\([a-z])+([-0-9])*([ ]){0,1})", "", $plain_text); // Clean up all unused control symbols

		$plain_text = str_replace("{", "", $plain_text);
		$plain_text = str_replace("}", "", $plain_text);
		$plain_text = str_replace("  ", " ", $plain_text);

		return $plain_text;
	}

	function GetPlainText(&$contents) {
		if ($this->CheckWordFormat($contents)) return $this->GetWordPlainText($contents);
		if ($this->CheckRTFFormat($contents)) return $this->GetRTFPlainText($contents);
		return FALSE;
	}

}

?><?
// use tabstop=4

/*
 Rich Text Format - Parsing Class
 ================================

 (c) 2000 Markus Fischer
 <mfischer@josefine.ben.tuwien.ac.at>
 http://josefine.ben.tuwien.ac.at/~mfischer/

 Latest versions of this class can always be found at
 http://josefine.ben.tuwien.ac.at/~mfischer/developing/php/rtf/rtfclass.phps
 Testing suite is available at
 http://josefine.ben.tuwien.ac.at/~mfischer/developing/php/rtf/

 License: GPLv2

 Specification:
 http://msdn.microsoft.com/library/default.asp?URL=/library/specs/rtfspec.htm

 General Notes:
 ==============
 Unknown or unspupported control symbols are silently gnored

 Group stacking is still not supported :(
 group stack logic implemented; however not really used yet

 Example on how to use this class:
 =================================

 $r = new rtf( stripslashes( $rtf));
 $r->output( "xml");
 $r->parse();
 if( count( $r->err) == 0) // no errors detected
 echo $r->out;

 History:
 ========
 Sat Nov 25 09:52:12 CET 2000  mfischer
 First version which has useable but only well-formed xml output; rtf
 data structure is only logically rebuild, no real parsing yet

 Mon Nov 27 16:17:18 CET 2000  mfischer
 Wrote handler for \plain control word (thanks to Peter Kursawe for this
 one)

 Tue Nov 28 02:22:16 CET 2000  mfischer
 Implemented alignment (left, center, right) with HTML <DIV .. tags
 Also implemented translation for < and > character when outputting html or xml
 Mon Oct 25 14:15:03 CET 2004  smanciles
 Implemented parsing of special characteres for spanish and catalan (ú�...)
 Remarks:
 ========
 This class and all work done here is dedicated to Tatjana.
 */

/* was just a brainlag suggestion of my inner link; don't know if I'll use it */
class rtfState {
	var $bold;
	var $italic;
	var $underlined;
}

class rtf {
	var $rtf;    // rtf core stream
	var $len;    // length in characters of the stream (get performace due avoiding calling strlen everytime)
	var $err = array();    // array of error message, no entities on no error

	var $wantXML;  // convert to XML
	var $wantHTML;  // convert to HTML

	// the only variable which should be accessed from the outside
	var $out;    // output data stream (depends on which $wantXXXXX is set to true
	var $outstyles;  // htmlified styles (generated after parsing if wantHTML
	var $styles;  // if wantHTML, stylesheet definitions are put in here

	// internal parser variables --------------------------------
	// control word variables
	var $cword;    // holds the current (or last) control word, depending on $cw
	var $cw;    // are we currently parsing a control word ?
	var $cfirst;  // could this be the first character ? so watch out for control symbols

	var $flags = array();    // parser flags

	var $queue;    // every character which is no sepcial char, not belongs to a control word/symbol; is generally considered being 'plain'

	var $stack = array();  // group stack

	/* keywords which don't follw the specification (used by Word '97 - 2000) */
	// not yet used
	var $control_exception = array(
      "clFitText",
      "clftsWidth(-?[0-9]+)?",
      "clNoWrap(-?[0-9]+)?",
      "clwWidth(-?[0-9]+)?",
      "tdfrmtxtBottom(-?[0-9]+)?",
      "tdfrmtxtLeft(-?[0-9]+)?",
      "tdfrmtxtRight(-?[0-9]+)?",
      "tdfrmtxtTop(-?[0-9]+)?",
      "trftsWidthA(-?[0-9]+)?",
      "trftsWidthB(-?[0-9]+)?",
      "trftsWidth(-?[0-9]+)?",
      "trwWithA(-?[0-9]+)?",
      "trwWithB(-?[0-9]+)?",
      "trwWith(-?[0-9]+)?",
      "spectspecifygen(-?[0-9]+)?"
      );

      var $charset_table = array(
      "0"  =>  "ANSI",
      "1"  =>  "Default",
      "2"  =>  "Symbol",
      "77" =>  "Mac",
      "128" =>  "Shift Jis",
      "129" =>  "Hangul",
      "130" =>  "Johab",
      "134" =>  "GB2312",
      "136" =>  "Big5",
      "161" =>  "Greek",
      "162" =>  "Turkish",
      "163" =>  "Vietnamese",
      "177" =>  "Hebrew",
      "178" =>  "Arabic",
      "179" =>  "Arabic Traditional",
      "180" =>  "Arabic user",
      "181" =>  "Hebrew user",
      "186" =>  "Baltic",
      "204" =>  "Russion",
      "222" =>  "Thai",
      "238" =>  "Eastern European",
      "255" =>  "PC 437",
      "255" =>  "OEM"
      );

      /* note: the only conversion table used */
      var $fontmodifier_table = array(
      "bold"  =>  "b",
      "italic"  => "i",
      "underlined"  => "u",
      "strikethru"  => "strike"
      );

      /*
       Class Constructor:
       Takes as argument the raw RTF stream
       (Note under certain circumstances the stream has to be stripslash'ed before handling over)
       Initialises some class-global variables
       */
      function rtf( $data) {
      	$this->len = strlen( $data);
      	$this->rtf = $data;

      	$this->wantXML = false;
      	$this->wantHTML = false;

      	$this->out = "";
      	$this->outstyles = "";
      	$this->styles = array();
      	$this->text = "";

      	if( $this->len == 0)
      	array_push( $this->err, "No data in stream found");
      }

      function parserInit() {
      	/*
        Default values according to the specs
        */
      	$this->flags = array(
        "fontsize"  =>  24,
        "beginparagraph"  => true
      	);
      }

      /*
       Sets the output type
       */
      function output( $typ) {
      	switch( $typ) {
        case "xml": $this->wantXML = true; break;
        case "html": $this->wantHTML = true; break;
        default: break;
      	}
      }

      function parseControl( $control, $parameter) {
      	switch( $control) {
        // font table definition start
        case "fonttbl":
        	$this->flags["fonttbl"] = true;  // signal fonttable control words they are allowed to behave as expected
        	break;
        	// define or set font
        case "f":
        	if( $this->flags["fonttbl"]) {  // if its set, the fonttable definition is written to; else its read from
        		$this->flags["fonttbl_current_write"] = $parameter;
        	} else {
        		$this->flags["fonttbl_current_read"] = $parameter;
        	}
        	break;
        case "fcharset":
        	// this is for preparing flushQueue; it then moves the Queue to $this->fonttable .. instead to formatted output
        	$this->flags["fonttbl_want_fcharset"] = $parameter;
        	break;
        case "fs":
        	// sets the current fontsize; is used by stylesheets (which are therefore generated on the fly
        	$this->flags["fontsize"] = $parameter;
        	break;
        	// handle alignment
        case "qc":
        	$this->flags["alignment"] = "center";
        	break;
        case "qr":
        	$this->flags["alignment"] = "right";
        	break;
        	// reset paragraph settings ( only alignment)
        case "pard":
        	$this->flags["alignment"] = "";
        	break;
        	// define new paragraph (for now, thats a simple break in html)
        case "par":
        	// begin new line
        	$this->flags["beginparagraph"] = true;
        	if( $this->wantHTML) {
        		$this->out .= "</div>";
        	}
        	break;
        	// bold
        case "bnone":
        	$parameter = "0";
        case "b":
        	// haven'y yet figured out WHY I need a (string)-cast here ... hm
        	if( (string)$parameter == "0")
        	$this->flags["bold"] = false;
        	else
        	$this->flags["bold"] = true;
        	break;

        	// underlined
        case "ulnone":
        	$parameter = "0";
        case "ul":
        	if( (string)$parameter == "0")
        	$this->flags["underlined"] = false;
        	else
        	$this->flags["underlined"] = true;
        	break;

        	// italic
        case "inone":
        	$parameter = "0";
        case "i":
        	if( (string)$parameter == "0")
        	$this->flags["italic"] = false;
        	else
        	$this->flags["italic"] = true;
        	break;

        	// strikethru
        case "strikenone":
        	$parameter = "0";
        case "strike":
        	if( (string)$parameter == "0")
        	$this->flags["strikethru"] = false;
        	else
        	$this->flags["strikethru"] = true;
        	break;

        	// reset all font modifiers and fontsize to 12
        case "plain":
        	$this->flags["bold"] = false;
        	$this->flags["italic"] = false;
        	$this->flags["underlined"] = false;
        	$this->flags["strikethru"] = false;
        	$this->flags["fontsize"] = 12;

        	$this->flags["subscription"] = false;
        	$this->flags["superscription"] = false;
        	break;

        	// sub and superscription
        case "subnone":
        	$parameter = "0";
        case "sub":
        	if( (string)$parameter == "0")
        	$this->flags["subscription"] = false;
        	else
        	$this->flags["subscription"] = true;
        	break;

        case "supernone":
        	$parameter = "0";
        case "super":
        	if( (string)$parameter == "0")
        	$this->flags["superscription"] = false;
        	else
        	$this->flags["superscription"] = true;
        	break;

      	}
      }

      /*
       Dispatch the control word to the output stream
       */
      function flushControl() {
      	if( ereg( "^([A-Za-z]+)(-?[0-9]*) ?$", $this->cword, $match)) {

        $this->parseControl( $match[1], $match[2]);

        if( $this->wantXML) {
        	$this->out.="<control word=\"".$match[1]."\"";
        	if( strlen( $match[2]) > 0)
        	$this->out.=" param=\"".$match[2]."\"";
        	$this->out.="/>";
        }
      	}
      }

      /*
       If output stream supports comments, dispatch it
       */
      function flushComment( $comment) {
      	if( $this->wantXML || $this->wantHTML) {
        $this->out.="<!-- ".$comment." -->";
      	}
      }

      /*
       Dispatch start/end of logical rtf groups
       (not every output type needs it; merely debugging purpose)
       */
      function flushGroup( $state) {
      	if( $state == "open") {

        /* push onto the stack */
        array_push( $this->stack, $this->flags);

        if( $this->wantXML)
        $this->out.="<group>";
      	}
      	if( $state == "close") {

        /* pop from the stack */
        $this->last_flags = $this->flags;
        $this->flags = array_pop( $this->stack);

        $this->flags["fonttbl_current_write"] = ""; // on group close, no more fontdefinition will be written to this id
        // this is not really the right way to do it !
        // of course a '}' not necessarily donates a fonttable end; a fonttable
        // group at least *can* contain sub-groups
        // therefore an stacked approach is heavily needed
        $this->flags["fonttbl"] = false; // no matter what you do, if a group closes, its fonttbl definition is closed too

        if( $this->wantXML)
        $this->out.="</group>";
      	}
      }

      function flushHead() {
      	if( $this->wantXML)
      	$this->out.="<rtf>";
      }

      function flushBottom() {
      	if( $this->wantXML)
      	$this->out.="</rtf>";
      }


      function checkHtmlSpanContent( $command) {
      	reset( $this->fontmodifier_table);
      	while( list( $rtf, $html) = each( $this->fontmodifier_table)) {
        if(isset($this->flags[$rtf]) && $this->flags[$rtf]) {
        	if( $command == "start")
        	$this->out .= "<".$html.">";
        	else
        	$this->out .= "</".$html.">";
        }
      	}
      }
      /*
       flush text in queue
       */
      function flushQueue() {
      	if( strlen( $this->queue)) {
        // processing logic
        if(isset($this->flags["fonttbl_want_fcharset"]) && ereg( "^[0-9]+$", $this->flags["fonttbl_want_fcharset"])) {
        	$this->fonttable[$this->flags["fonttbl_want_fcharset"]]["charset"] = $this->queue;
        	$this->flags["fonttbl_want_fcharset"] = "";
        	$this->queue = "";
        }

        // output logic
        if( strlen( $this->queue)) {
        	/*
        	 Everything which passes this is (or, at leat, *should*) be only outputted plaintext
        	 Thats why we can safely add the css-stylesheet when using wantHTML
          */
        	if( $this->wantXML)
        	$this->out.= "<plain>".$this->queue."</plain>";

        	if( $this->wantHTML) {
        		// only output html if a valid (for now, just numeric;) fonttable is given
        		if(isset($this->flags["fonttbl_current_read"]) && ereg( "^[0-9]+$", $this->flags["fonttbl_current_read"])) {

        			if( $this->flags["beginparagraph"] == true) {
        				$this->flags["beginparagraph"] = false;
        				$this->out .= "<div align=\"";
        				switch( $this->flags["alignment"]) {
        					case "right":
        						$this->out .= "right";
        						break;
        					case "center":
        						$this->out .= "center";
        						break;
        					case "left":
        					default:
        						$this->out .= "left";
        				}
        				$this->out .= "\">";
        			}

        			/* define new style for that span */
        			if(isset($this->fonttable[$this->flags["fonttbl_current_read"]])){
        				$this->styles["f".$this->flags["fonttbl_current_read"]."s".$this->flags["fontsize"]] = "font-family:".$this->fonttable[$this->flags["fonttbl_current_read"]]["charset"]." font-size:".$this->flags["fontsize"].";";
        			}
        			/* write span start */
        			$this->out .= "<span class=\"f".$this->flags["fonttbl_current_read"]."s".$this->flags["fontsize"]."\">";

        			/* check if the span content has a modifier */
        			$this->checkHtmlSpanContent( "start");
        			/* write span content */
        			$this->out .= $this->queue;
        			/* close modifiers */
        			$this->checkHtmlSpanContent( "stop");
        			/* close span */
              "</span>";
        		}
        	}
        	$this->queue = "";
        }
      	}
      }

      /*
       handle special charactes like \'ef
       */
      function flushSpecial( $special) {
      	if( strlen( $special) == 2) {
        if( $this->wantXML)
        $this->out .= "<special value=\"".$special."\"/>";
        if( $this->wantHTML){
        	$this->out .= "<special value=\"".$special."\"/>";
        	switch( $special) {
        		case "c1": $this->out .= "&Aacute;"; break;
        		case "e1": $this->out .= "&aacute;"; break;
        		 
        		case "e3": $this->out .= "&atilde;"; break;
        		case "c3": $this->out .= "&Atilde;"; break;
        		case "f5": $this->out .= "&otilde;"; break;
        		case "d5": $this->out .= "&Otilde;"; break;
        		case "e2": $this->out .= "&acirc;"; break;
        		case "c2": $this->out .= "&Acirc;"; break;
        		case "ea": $this->out .= "&ecirc;"; break;
        		case "ca": $this->out .= "&Ecirc;"; break;
        		case "f4": $this->out .= "&ocirc;"; break;
        		case "d4": $this->out .= "&Ocirc;"; break;

        		case "c0": $this->out .= "&Agrave;"; break;
        		case "e0": $this->out .= "&agrave;"; break;
        		case "c9": $this->out .= "&Eacute;"; break;
        		case "e9": $this->out .= "&eacute;"; break;
        		case "c8": $this->out .= "&Egrave;"; break;
        		case "e8": $this->out .= "&egrave;"; break;
        		case "cd": $this->out .= "&Iacute;"; break;
        		case "ed": $this->out .= "&iacute;"; break;
        		case "cc": $this->out .= "&Igrave;"; break;
        		case "ec": $this->out .= "&igrave;"; break;
        		case "d3": $this->out .= "&Oacute;"; break;
        		case "f3": $this->out .= "&oacute;"; break;
        		case "d2": $this->out .= "&Ograve;"; break;
        		case "f2": $this->out .= "&ograve;"; break;
        		case "da": $this->out .= "&Uacute;"; break;
        		case "fa": $this->out .= "&uacute;"; break;
        		case "d9": $this->out .= "&Ugrave;"; break;
        		case "f9": $this->out .= "&ugrave;"; break;
        		case "80": $this->out .= "&#8364;"; break;
        		case "d1": $this->out .= "&Ntilde;"; break;
        		case "f1": $this->out .= "&ntilde;"; break;
        		case "c7": $this->out .= "&Ccedil;"; break;
        		case "e7": $this->out .= "&ccedil;"; break;
        		case "dc": $this->out .= "&Uuml;"; break;
        		case "fc": $this->out .= "&uuml;"; break;
        		case "bf": $this->out .= "&#191;"; break;
        		case "a1": $this->out .= "&#161;"; break;
        		case "b7": $this->out .= "&middot;"; break;
        		case "a9": $this->out .= "&copy;"; break;
        		case "ae": $this->out .= "&reg;"; break;
        		case "ba": $this->out .= "&ordm;"; break;
        		case "aa": $this->out .= "&ordf;"; break;
        		case "b2": $this->out .= "&sup2;"; break;
        		case "b3": $this->out .= "&sup3;"; break;
        	}

        }
      	}
      }

      /*
       Output errors at end
       */
      function flushErrors() {
      	if( count( $this->err) > 0) {
        if( $this->wantXML) {
        	$this->out .= "<errors>";
        	while( list($num,$value) = each( $this->err)) {
        		$this->out .= "<message>".$value."</message>";
        	}
        	$this->out .= "</errors>";
        }
      	}
      }

      function makeStyles() {
      	$this->outstyles = "<style type=\"text/css\"><!--\n";
      	reset( $this->styles);
      	while( list( $stylename, $styleattrib) = each( $this->styles)) {
        $this->outstyles .= ".".$stylename." { ".$styleattrib." }\n";
      	}
      	$this->outstyles .= "--></style>\n";
      }

      /*
       finally ..

       How this parser (is supposed) to work:
       ======================================
       This parse simple starts at the beginning of the rtf core stream, catches every
       controlling character {,} and \, automatically builds control words and control
       symbols during his livetime, trashes every other character into the plain text
       queue
       */
      function parse() {

      	$this->parserInit();

      	$i = 0;
      	$this->cw= false;  // flag if control word is currently parsed
      	$this->cfirst = false;// first control character ?
      	$this->cword = "";  // last or current control word ( depends on $this->cw

      	$this->queue = "";    // plain text data found during parsing

      	$this->flushHead();

      	while( $i < $this->len) {
        switch( $this->rtf[$i]) {
        	case "{":  if( $this->cw) {
        		$this->flushControl();
        		$this->cw= false; $this->cfirst = false;
        	} else
        	$this->flushQueue();

        	$this->flushGroup( "open");
        	break;
        	case "}":  if( $this->cw) {
        		$this->flushControl();
        		$this->cw= false; $this->cfirst = false;
        	} else
        	$this->flushQueue();

        	$this->flushGroup( "close");
        	break;
        	case "\\":  if( $this->cfirst) {  // catches '\\'
        		$this->queue .= '\\';
        		$this->cfirst = false;
        		$this->cw= false;
        		break;
        	}
        	if( $this->cw) {
        		$this->flushControl();
        	} else
        	$this->flushQueue();
        	$this->cw = true;
        	$this->cfirst = true;
        	$this->cword = "";
        	break;
        	default:
        		if( (ord( $this->rtf[$i]) == 10) || (ord($this->rtf[$i]) == 13)) break; // eat line breaks
        		if( $this->cw) {  // active control word ?
        			/*
        			Watch the RE: there's an optional space at the end which IS part of
        			the control word (but actually its ignored by flushControl)
        			*/
        			if( ereg( "^[a-zA-Z0-9-]?$", $this->rtf[$i])) { // continue parsing
        				$this->cword .= $this->rtf[$i];
        				$this->cfirst = false;
        			} else {
        				/*
        				 Control word could be a 'control symbol', like \~ or \* etc.
        				 */
        				$specialmatch = false;
        				if( $this->cfirst) {
        					if( $this->rtf[$i] == '\'') { // expect to get some special chars
        						$this->flushQueue();
        						$this->flushSpecial( $this->rtf[$i+1].$this->rtf[$i+2]);
        						$i+=2;
        						$specialmatch = true;
        						$this->cw = false; $this->cfirst = false; $this->cword = "";
        					} else
        					if( ereg( "^[{}\*]$", $this->rtf[$i])) {
        						$this->flushComment( "control symbols not yet handled");
        						$specialmatch = true;
        					}
        					$this->cfirst = false;
        				} else {
        					if( $this->rtf[$i] == ' ') {  // space delimtes control words, so just discard it and flush the controlword
        						$this->cw = false;
        						$this->flushControl();
        						break;
        					}
        				}
        				if( ! $specialmatch) {
        					$this->flushControl();
        					$this->cw = false; $this->cfirst = false;
        					/*
        					 The current character is a delimeter, but is NOT
        					 part of the control word so we hop one step back
        					 in the stream and process it again
        					 */
        					$i--;
        				}
        			}
        		} else {
        			// < and > need translation before putting into queue when XML or HTML is wanted
        			if( ($this->wantHTML) || ($this->wantXML)) {
        				switch( $this->rtf[$i]) {
        					case "<":
        						$this->queue .= "&lt;";
        						break;
        					case ">":
        						$this->queue .= "&gt;";
        						break;
        					default:
        						$this->queue .= $this->rtf[$i];
        						break;
        				}
        			} else
        			$this->queue .= $this->rtf[$i];
        		}

        }
        $i++;
      	}
      	$this->flushQueue();
      	$this->flushErrors();
      	$this->flushBottom();

      	if( $this->wantHTML) {
        $this->makeStyles();
      	}
      }
}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDGridExcel. Exporta objetos FWDGrid ou FWDDBGrid para o formato XLS.
 *
 * <p>Classe que exporta objetos FWDGrid ou FWDDBGrid para uma planilha do
 * excel (formato .XLS).</p>
 * @package FWD5
 * @subpackage Formats
 */
class FWDGridExcel {

	/**
	 * Array de FWDGrids e/ou FWDDBGrids
	 * @var array
	 * @access private
	 */
	private $caGrid = array();

	/**
	 * Nome do arquivo Excel (*.XLS)
	 * @var string
	 * @access private
	 */
	private $csFilename = '';

	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDGridExcel.</p>
	 *
	 * @access public
	 * @param string $psFilename Nome do arquivo
	 * @param array $paGrid Array de Grids e/ou DBGrids
	 */
	public final function __construct($psFilename = '',$paGrid){
		if($paGrid){
			$this->caGrid = $paGrid;
			$this->csFilename = $psFilename;
			foreach($paGrid as $moGrid){
				if(!($moGrid instanceof FWDGrid || $moGrid instanceof FWDDBGrid)){
					trigger_error("Object FWDGridExcel must have an array of FWDGrid and/or FWDDBGrid",E_USER_ERROR);
				}
			}
		}else{
			trigger_error("Object FWDGridExcel must have an array of FWDGrid and/or FWDDBGrid",E_USER_ERROR);
		}
	}

	/**
	 * Seta os valores da grid nos objetos do arquivo XLS.
	 *
	 * <p>M�todo para setar os valores da grid nos objetos do arquivo XLS.</p>
	 *
	 * @access public
	 */
	public final function execute(){
		$moExcelWriter = new FWDExcelWriter($this->csFilename);

		// Cria os estilos
		$moHeaderStyle = new FWDExcelStyle('h');
		$moOddRowStyle = new FWDExcelStyle('o');
		$moEvenRowStyle = new FWDExcelStyle('e');

		$moAlign = new FWDExcelStyleAlignment();
		$moAlign->SetAttributes('Center', 'Center');
		$moHeaderStyle->setAlignment($moAlign);
		$moOddRowStyle->setAlignment($moAlign);
		$moEvenRowStyle->setAlignment($moAlign);

		$moBorders = new FWDExcelStyleBorders();
		$moBorders->setExternalBorders('Continuous',array(0,0,0));
		$moHeaderStyle->setBorders($moBorders);
		$moOddRowStyle->setBorders($moBorders);
		$moEvenRowStyle->setBorders($moBorders);

		$moInteriorOddRow = new FWDExcelStyleInterior();
		$moInteriorOddRow->setAttributes(array(230,230,230), 'Solid', array(0,0,0));
		$moOddRowStyle->setInterior($moInteriorOddRow);

		$moInteriorEvenRow = new FWDExcelStyleInterior();
		$moInteriorEvenRow->setAttributes(array(200,200,200), 'Solid', array(0,0,0));
		$moEvenRowStyle->setInterior($moInteriorEvenRow);

		$moInteriorHeader = new FWDExcelStyleInterior();
		$moInteriorHeader->setAttributes(array(150,150,150), 'Solid', array(0,0,0));
		$moHeaderStyle->setInterior($moInteriorHeader);

		$moExcelWriter->addStyle($moHeaderStyle);
		$moExcelWriter->addStyle($moOddRowStyle);
		$moExcelWriter->addStyle($moEvenRowStyle);

		// Cria uma planilha para cada grid
		foreach($this->caGrid as $moGrid){
			// Abre uma planilha
			$moExcelWriter->openWorksheet($moGrid->getAttrName());
			// Se � uma DBGrid, popula
			if($moGrid instanceof FWDDBGrid){
				if($moGrid->GetAttrDinamicFill()){
					$moGrid->setAttrDinamicFill('false');
					$moGrid->populate();
					$moGrid->setAttrDinamicFill('true');
				} else {
					$moGrid->populate();
				}
			}
			// Define a altura das linhas
			$miRowHeight = ($moGrid->getAttrRowHeight()?$moGrid->getAttrRowHeight():50);
			// Cria as colunas
			$maColumns = $moGrid->getColumns();
			foreach($maColumns as $moColumn){
				$moExcelWriter->addColumn($moColumn->getAttrWidth()?$moColumn->getAttrWidth():150);
			}
			// Cria o cabe�alho
			$moHeader = new FWDExcelRow();
			$moHeader->setStyleId('h');
			foreach($maColumns as $moColumn){
				$moHeader->addCell(new FWDExcelCell($moColumn->getValue()));
			}
			$moExcelWriter->drawRow($moHeader);
			// Cria as linhas
			$miRowsCount = $moGrid->getAttrRowsCount();
			$miColsCount = count($maColumns);
			$mbOdd = true;
			for($i=1;$i<=$miRowsCount;$i++){
				$moExcelRow = new FWDExcelRow();
				if($mbOdd) $moExcelRow->setStyleId('o');
				else $moExcelRow->setStyleId('e');
				$mbOdd = !$mbOdd;
				$moExcelRow->setHeight($miRowHeight);
				for($j=1;$j<=$miColsCount;$j++){
					$moExcelRow->addCell(new FWDExcelCell($this->getCellValue($moGrid,$i,$j)));
				}
				$moExcelWriter->drawRow($moExcelRow);
			}
		}
		// Encerra o arquivo
		$moExcelWriter->endFile();
	}

	/**
	 * Retorna o valor de uma c�lula.
	 *
	 * <p>Retorna o valor de uma c�lula da grid. Deve ser sobrescrito para
	 * funcionar com grids que sobrescrevem o drawItem da DrawGrid.</p>
	 * @access protected
	 * @param FWDGrid $moGrid Grid
	 * @param integer $piRow Linha da c�lula
	 * @param integer $piCol Coluna da c�lula
	 */
	protected function getCellValue($moGrid,$piRow,$piCol){
		$msOut = $moGrid->getCellValue($piRow,$piCol);
		if($msOut===false) return '';
		else return $moGrid->getCellValue($piRow,$piCol);
	}

}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDDataSetExcel. Exporta objetos FWDDBDataSet para o formato XLS.
 *
 * <p>Classe que exporta objetos FWDDBDataSet para uma planilha no formato .xls.</p>
 * @package FWD5
 * @subpackage Formats
 */
class FWDDataSetExcel {

	/**
	 * DataSet a ser exportado
	 * @var FWDDBDataSet
	 * @access private
	 */
	private $coDataSet = null;

	/**
	 * Nome do arquivo Excel (*.xls)
	 * @var string
	 * @access private
	 */
	private $csFilename = "";

	/**
	 * Construtor.
	 *
	 * <p>Construtor da classe FWDDataSetExcel.</p>
	 *
	 * @access public
	 * @param string $psFilename Nome do arquivo
	 * @param FWDDBDataSet $poDataSet DataSet a ser exportado
	 */
	public function __construct($psFilename = '',FWDDBDataSet $poDataSet){
		$this->coDataSet = $poDataSet;
		$this->csFilename = $psFilename;
	}

	/**
	 * Exporta o DataSet
	 *
	 * <p>Faz o fetch das linhas do DataSet e exporta em formato de planilha (.xls).</p>
	 * @access public
	 */
	public function execute(){
		$moExcelWriter = new FWDExcelWriter($this->csFilename);

		// Cria os estilos
		$moHeaderStyle = new FWDExcelStyle('h'); // header
		$moOddRowStyle = new FWDExcelStyle('o'); // odd
		$moEvenRowStyle = new FWDExcelStyle('e'); // even

		$moAlign = new FWDExcelStyleAlignment();
		$moAlign->SetAttributes('Center', 'Center');
		$moHeaderStyle->setAlignment($moAlign);
		$moOddRowStyle->setAlignment($moAlign);
		$moEvenRowStyle->setAlignment($moAlign);

		$moBorders = new FWDExcelStyleBorders();
		$moBorders->setExternalBorders('Continuous',array(0,0,0));
		$moHeaderStyle->setBorders($moBorders);
		$moOddRowStyle->setBorders($moBorders);
		$moEvenRowStyle->setBorders($moBorders);

		$moInteriorOddRow = new FWDExcelStyleInterior();
		$moInteriorOddRow->setAttributes(array(230,230,230), 'Solid', array(0,0,0));
		$moOddRowStyle->setInterior($moInteriorOddRow);

		$moInteriorEvenRow = new FWDExcelStyleInterior();
		$moInteriorEvenRow->setAttributes(array(200,200,200), 'Solid', array(0,0,0));
		$moEvenRowStyle->setInterior($moInteriorEvenRow);

		$moInteriorHeader = new FWDExcelStyleInterior();
		$moInteriorHeader->setAttributes(array(150,150,150), 'Solid', array(0,0,0));
		$moHeaderStyle->setInterior($moInteriorHeader);

		$moExcelWriter->addStyle($moHeaderStyle);
		$moExcelWriter->addStyle($moOddRowStyle);
		$moExcelWriter->addStyle($moEvenRowStyle);

		// Abre uma planilha
		$moExcelWriter->openWorksheet($this->csFilename);
		// Cria as colunas
		$moFields = $this->coDataSet->getFields();
		foreach($moFields as $moField){
			$moExcelWriter->addColumn(150);
		}
		// Cria o cabe�alho
		$moHeader = new FWDExcelRow();
		$moHeader->setStyleId('h');
		foreach($moFields as $moField){
			$moHeader->addCell(new FWDExcelCell($moField->getAlias()));
		}
		$moExcelWriter->drawRow($moHeader);
		// Cria as linhas
		$mbOdd = true;
		while($this->coDataSet->fetch()){
			$moExcelRow = new FWDExcelRow();
			if($mbOdd) $moExcelRow->setStyleId('o');
			else $moExcelRow->setStyleId('e');
			$mbOdd = !$mbOdd;
			foreach($moFields as $moField){
				$moExcelRow->addCell(new FWDExcelCell($moField->getValue()));
			}
			$moExcelWriter->drawRow($moExcelRow);
		}
		// Encerra o arquivo
		$moExcelWriter->endFile();
	}

}
?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe que extende a FPDF para desenhar cabe�alho e rodap�.
 *
 * <p>Classe que extende a FPDF para desenhar cabe�alho e rodap�.</p>
 * @package FWD5
 * @subpackage report
 */
class FWDPDF extends FPDF {

	/**
	 * @var FWDReportPDFWriter Writer de PDF respons�vel por desenhar o cabe�alho das p�ginas.
	 * @access protected
	 */
	protected $coWriter = null;

	/**
	 * Seta o writer de PDF.
	 *
	 * <p>Seta o writer de PDF (respons�vel por desenhar o cabe�alho das p�ginas).</p>
	 * @access public
	 */
	public function setWriter(FWDReportPDFWriter $poWriter){
		$this->coWriter = $poWriter;
	}

	/**
	 * Desenha o cabe�alho da p�gina
	 *
	 * <p>Desenha o cabe�alho da p�gina.</p>
	 * @access public
	 */
	public function Header(){
		$this->coWriter->drawHeader();
	}

	/**
	 * Desenha o rodap� da p�gina
	 *
	 * <p>Desenha o rodap� da p�gina.</p>
	 * @access public
	 */
	public function Footer(){
		$this->SetY(-40);
		//Arial italic 8
		$this->SetFont('Arial','',8);
		//Page number
		$this->Cell(0,10,FWDLanguage::getPHPStringValue('fpdf_page',"P�gina").' '.$this->PageNo().'/{nb}',0,0,'R');
	}

	/**
	 * Quebra uma string em quantas linhas for necess�rio.
	 *
	 * <p>Quebra uma string em quantas linhas for necess�rio para, com a fonte
	 * atual, obedecer � restri��o de largura. A quebra � feita introduzindo
	 * caracteres "\n".</p>
	 * @access public
	 * @param string $text String a ser quebrada
	 * @param integer $maxwidth Limite de largura
	 * @return integer N�mero de linhas da string resultante
	 */
	public function wordWrap(&$text, $maxwidth){
		$text = trim($text);
		if($text==='') return 0;
		$space = $this->GetStringWidth(' ');
		$lines = explode("\n", $text);
		$text = '';
		$count = 0;
		foreach($lines as $line){
			$words = preg_split('/ +/', $line);
			$width = 0;
			foreach($words as $word){
				$wordwidth = $this->GetStringWidth($word);
				if($wordwidth > $maxwidth){
					// Word is too long, we cut it
					for($i=0; $i<strlen($word); $i++){
						$wordwidth = $this->GetStringWidth(substr($word, $i, 1));
						if($width + $wordwidth <= $maxwidth){
							$width += $wordwidth;
							$text .= substr($word, $i, 1);
						}else{
							$width = $wordwidth;
							$text = rtrim($text)."\n".substr($word, $i, 1);
							$count++;
						}
					}
				}elseif($width + $wordwidth <= $maxwidth){
					$width += $wordwidth + $space;
					$text .= $word.' ';
				}else{
					$width = $wordwidth + $space;
					$text = rtrim($text)."\n".$word.' ';
					$count++;
				}
			}
			$text = rtrim($text)."\n";
			$count++;
		}
		$text = rtrim($text);
		return $count;
	}

	/**
	 * Retorna o comprimento de uma string
	 *
	 * <p>Retorna o comprimento de uma string considerando a fonte atual.</p>
	 * @access public
	 * @param string $psString String a ser medida
	 * @return integer Comprimento da string
	 */
	public function GetStringWidth($psString){
		$miPogFactor = 0.5;
		return $miPogFactor + parent::GetStringWidth($psString);
	}

}

?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDXmlTextExtractor. Extrai texto de arquivos XML.
 *
 * <p>Classe para extrair texto de arquivos XML.</p>
 * @package FWD5
 * @subpackage base
 */
class FWDXmlTextExtractor extends FWDXMLParser {

	/**
	 * Controla quando espa�os devem ser introduzidos no texto extra�do
	 * @var boolean
	 * @access protected
	 */
	protected $cbAddSpace = false;

	/**
	 * Acumula o texto extra�do
	 * @var string
	 * @access protected
	 */
	protected $csContent = '';

	/**
	 * Handler chamado quando o parser encontra uma tag de in�cio de elemento
	 *
	 * <p>Handler chamado quando o parser encontra uma tag de in�cio de elemento.</p>
	 * @access protected
	 * @param resource $prParser Parser
	 * @param string $psTagName Nome da tag
	 * @param array $paAttributes Array associativo contendo os atributos do elemento
	 */
	protected function startHandler($prParser, $psTagName, $paAttributes){
		$this->cbAddSpace = true;
	}

	/**
	 * Handler chamado quando o parser encontra texto dentro de um elemento
	 *
	 * <p>Handler chamado quando o parser encontra texto dentro de um elemento.</p>
	 * @access protected
	 * @param resource $prParser Parser
	 * @param string $psData Conte�do de texto do elemento
	 */
	protected function dataHandler($prParser, $psData){
		if($this->cbAddSpace){
			$this->csContent.= ' '.$psData;
			$this->cbAddSpace = false;
		}else{
			$this->csContent.= $psData;
		}
	}

	/**
	 * Handler chamado quando o parser encontra uma tag de fim de elemento
	 *
	 * <p>Handler chamado quando o parser encontra uma tag de fim de elemento.</p>
	 * @access protected
	 * @param resource $prParser Parser
	 * @param string $psTagName Nome da tag
	 */
	protected function endHandler($prParser, $psTagName){
		$this->cbAddSpace = true;
	}

	/**
	 * Retorna o texto extra�do pelo parser.
	 *
	 * <p>Retorna o texto extra�do pelo parser.</p>
	 * @access public
	 * @return string Texto extra�do
	 */
	public function getContent(){
		return $this->csContent;
	}

}

?><?php
/**
 * FWD - FAST WEB DEVELOPMENT
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe FWDTextExtractor. Extrai texto de arquivos de v�rios formatos.
 *
 * <p>Classe para extrair texto de arquivos de v�rios formatos</p>
 * @package FWD5
 * @subpackage base
 */
class FWDTextExtractor {

	/**
	 * Lista de extens�es a serem ignoradas.
	 * @var array
	 * @access private
	 */
	private $caExtensionsToIgnore;

	/**
	 * Path para a pasta onde est�o os execut�veis usados para extrair texto de alguns formatos de arquivo.
	 * @var string
	 * @access private
	 */
	private $csExtractorsPath;

	/**
	 * Construtor da classe.
	 *
	 * <p>Construtor da classe FWDTextExtractor.</p>
	 * @access public
	 */
	public function __construct(){
		$this->caExtensionsToIgnore = array('jpg','jpeg','gif','png','bmp','exe');
		$msCurrentFile = $_SERVER['SCRIPT_FILENAME'];
		if(strtoupper(substr(PHP_OS,0,3)) === 'WIN'){
			$this->csExtractorsPath = FWDWebLib::getInstance()->getLibRef().'bin\\windows\\';
			$this->csExtractorsPath = strtr($this->csExtractorsPath,"/","\\");
		}else{
			$this->csExtractorsPath = FWDWebLib::getInstance()->getLibRef().'bin/linux/';
		}
	}

	/**
	 * Retira as tags de um trecho de c�digo XML.
	 *
	 * <p>Retira as tags de um trecho de c�digo XML. Retira tamb�m espa�os desnecess�rios.</p>
	 * @access private
	 * @param string $psString C�digo XML
	 */
	private function stripTags($psString){
		return trim(preg_replace('/([\n\s]+(&nbsp;)*)+/',' ',preg_replace('/<[^>]+>/',' ',$psString)));
	}

	/**
	 * Extrai texto de um arquivo.
	 *
	 * <p>Extrai texto de um arquivo.</p>
	 * @param string $psFileName Nome do arquivo
	 * @param string $psExtension Extens�o do arquivo (somente necess�rio se o nome do arquivo n�o tiver extens�o)
	 * @access public
	 */
	public function extractTextFromFile($psFileName,$psExtension=''){
		if($psExtension==''){
			$msExtension = strtolower(substr($psFileName,strrpos($psFileName,'.')+1));
		}else{
			$msExtension = strtolower($psExtension);
		}

		if(in_array($msExtension,$this->caExtensionsToIgnore)) return false;
		switch($msExtension){
			case 'dot':
			case 'doc':
				$moPHPWordLib = new PHPWordLib();
				$msContent = $moPHPWordLib->loadFile($psFileName);
				if($msContent===false){
					return false;
				}else{
					return $moPHPWordLib->GetPlainText($msContent);
				}
			case 'pdf':
				return shell_exec("{$this->csExtractorsPath}pdftotext $psFileName -");
			case 'rtf':
				$moRtf = new rtf((file_get_contents($psFileName)));
				$moRtf->output('html');
				$moRtf->parse();
				if(count($moRtf->err) == 0){
					return preg_replace(array('/<\/?div [^>]+>/','/(<[^>]+>)+/'),array(' ',''),$moRtf->out);
				}else{
					return false;
				}
			case 'xlsx':
				$moArchive = new ZipArchive();
				if($moArchive->open($psFileName)===true){
					$msContent = '';
					for($i=0;$i<$moArchive->numFiles;$i++){
						$maStat = $moArchive->statIndex($i);
						if(substr($maStat['name'],0,14)=='xl/worksheets/'){
							$msContent.= ' '.$this->stripTags($moArchive->getFromIndex($i));
						}
					}
					return $msContent;
				}else{
					return false;
				}
			case 'pptx':
			case 'ppsx':
				$moArchive = new ZipArchive();
				if($moArchive->open($psFileName)===true){
					$msContent = '';
					for($i=0;$i<$moArchive->numFiles;$i++){
						$maStat = $moArchive->statIndex($i);
						if(substr($maStat['name'],0,11)=='ppt/slides/'){
							$msContent.= ' '.$this->stripTags($moArchive->getFromIndex($i));
						}
					}
					return $msContent;
				}else{
					return false;
				}
			case 'docx':
				$moArchive = new ZipArchive();
				if($moArchive->open($psFileName)===true){
					$msContent = $moArchive->getFromName('word/document.xml');
					if($msContent===false) return false;
					$miStart = strpos($msContent,'<w:body');
					$miEnd = strrpos($msContent,'</w:body');
					$msContent = substr($msContent,$miStart,$miEnd - $miStart);
					return utf8_decode($this->stripTags($msContent));
				}else{
					return false;
				}
			case 'odt':
			case 'ods':
			case 'odp':
				$moArchive = new ZipArchive();
				if($moArchive->open($psFileName)===true){
					$msContent = $moArchive->getFromName('content.xml');
					if($msContent===false) return false;
					$miStart = strpos($msContent,'<office:body');
					$miEnd = strrpos($msContent,'</office:body');
					$msContent = substr($msContent,$miStart,$miEnd - $miStart);
					return utf8_decode($this->stripTags($msContent));
				}else{
					return false;
				}
			case 'xls':
				$msOutput = shell_exec("{$this->csExtractorsPath}xlhtml -nc -a -fw $psFileName");
				$msOutput = preg_replace(array('/<title>[^<]*<\/title>/i','/<[^>]+>/'),array('',' '),$msOutput);
				$msOutput = substr($msOutput,0,strrpos($msOutput,'Created with'));
				return preg_replace('/([\n\s]+(&nbsp;)*)+/',' ',$msOutput);
			case 'ppt':
			case 'pps':
				$msOutput = shell_exec("{$this->csExtractorsPath}ppthtml $psFileName");
				$msOutput = preg_replace(array('/<title>[^<]*<\/title>/i','/<[^>]+>/'),array('',' '),$msOutput);
				$msOutput = substr($msOutput,0,strrpos($msOutput,'Created with'));
				return preg_replace('/([\n\s]+(&nbsp;)*)+/',' ',$msOutput);
			case 'html':
				return $this->stripTags(file_get_contents($psFileName));
			case 'txt':
				return str_replace("\0","",file_get_contents($psFileName));
			default:
				$moXmlParser = new FWDXmlTextExtractor();
				if($moXmlParser->parseFile($psFileName,false)){
					return $this->stripTags($moXmlParser->getContent());
				}else{
					return false;
				}
		}
	}

}

?>