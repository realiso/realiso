<?php
/**
 * ACL - ACL MANAGER
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryACLTree.
 *
 * <p></p>
 * @package ACL
 * @subpackage handlers
 */
class QueryACLTree extends FWDDBQueryHandler {

  protected $ciSystem = 0;

  public function __construct($poDB=null){
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField('pkAcl'       ,'acl_id'         ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('fkParent'    ,'acl_parent_id'  ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('nLevel'      ,'acl_level'      ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('sTag'        ,'acl_tag'        ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('sName'       ,'acl_name'       ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('sDescription','acl_description',DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField('nOrder'      ,'acl_order'      ,DB_NUMBER));
  }

  public function getSystem(){
    return $this->ciSystem;
  }

  public function setSystem($piSystem){
    $this->ciSystem = $piSystem;
  }

  public function makeQuery(){
    if($this->ciSystem){
      $this->csSQL = "SELECT
                        pkAcl AS acl_id,
                        fkParent AS acl_parent_id,
                        nLevel AS acl_level,
                        sTag AS acl_tag,
                        sName AS acl_name,
                        sDescription AS acl_description,
                        nOrder AS acl_order
                      FROM get_acl_tree({$this->ciSystem},0,0)";
    }else{
      trigger_error("System id is not set.",E_USER_ERROR);
    }
  }

}

?>