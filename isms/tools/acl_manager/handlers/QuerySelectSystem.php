<?php
/**
 * ACL - ACL MANAGER
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QuerySelectSystem.
 *
 * <p></p>
 * @package ACL
 * @subpackage handlers
 */
class QuerySelectSystem extends FWDDBQueryHandler {

  protected $caExcludedIds = array();

  public function __construct($poDB=null){
    parent::__construct($poDB);
    $this->coDataSet->addFWDDBField(new FWDDBField('pksystem','select_id'   ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField('sname'   ,'select_value',DB_STRING));
  }

  public function setExcludedIds($paExcludedIds){
    $this->caExcludedIds = $paExcludedIds;
  }

  public function makeQuery(){
    $maFilters = array();

    if(count($this->caExcludedIds)>0){
      $maFilters[] = 'pksystem NOT IN ('.implode(',',$this->caExcludedIds).')';
    }
    
    if(count($maFilters)==0){
      $msWhere = '';
    }else{
      $msWhere = ' WHERE '.implode(' AND ',$maFilters);
    }
    $this->csSQL = "SELECT
                      pksystem AS select_id,
                      sname AS select_value
                    FROM acl_system
                    $msWhere
                    ORDER BY pkSystem";
  }

}

?>