<?php

include_once 'include.php';

$siSystemId = $_GET['system'];

$saTags = ACLAcl::getTagsFromSystem($siSystemId);
natcasesort($saTags);

header('Cache-Control: ');// leave blank to avoid IE errors
header('Pragma: ');// leave blank to avoid IE errors
header('Content-type: text/enriched');
header('Content-Disposition: attachment; filename="isms_profile_acl.sql"');

// Mensagem, de alerta
echo "/* Esse arquivo eh gerado automaticamente pelo sistema acl_manager. Ao atualizar esse arquivo, propagar as alteracoes para os scripts de criacao dos trials. */\n";

// D� permiss�o pros perfis especiais de acordo com os prefixos das tags
foreach($saTags as $ssTag){
  $ssPrefix = substr($ssTag, 0, strpos($ssTag, '.'));
  echo "INSERT INTO isms_profile_acl (fkprofile,stag) SELECT fkcontext, '{$ssTag}' FROM isms_profile WHERE nid = 8801;\n";
  if($ssPrefix=='M'){
    echo "INSERT INTO isms_profile_acl (fkprofile,stag) SELECT fkcontext, '{$ssTag}' FROM isms_profile WHERE nid = 8802;\n";
  }elseif($ssPrefix=='A'){
    echo "INSERT INTO isms_profile_acl (fkprofile,stag) SELECT fkcontext, '{$ssTag}' FROM isms_profile WHERE nid = 8803;\n";
  }
}

// ACLs referentes ao perfil 'Leitor de Documentos'
echo "INSERT INTO isms_profile_acl (fkprofile,stag) SELECT fkcontext, 'M.PM' FROM isms_profile WHERE nid = 8805;\n";
echo "INSERT INTO isms_profile_acl (fkprofile,stag) SELECT fkcontext, 'M.PM.12' FROM isms_profile WHERE nid = 8805;";

?>