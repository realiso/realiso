<?php

include_once 'include.php';

$siOldSystemId = $_POST['var_old_system'];
$siNewSystemId = $_POST['var_new_system'];
$ssAclMapping = $_POST['var_mapping'];
if($ssAclMapping){
  $saMapping = explode(',',$ssAclMapping);
}else{
  $saMapping = array();
}

$saOldTags = ACLAcl::getTagsFromSystem($siOldSystemId);
$saNewTags = ACLAcl::getTagsFromSystem($siNewSystemId);

$saTagsToInsert = array_diff($saNewTags,$saOldTags);
$saTagsToDelete = array_diff($saOldTags,$saNewTags);

header('Cache-Control: ');// leave blank to avoid IE errors
header('Pragma: ');// leave blank to avoid IE errors
header('Content-type: text/enriched');
header('Content-Disposition: attachment; filename="acl_hotfix_script.sql"');

// Mapeia as permiss�es (s� dos perfis definidos pelo usu�rio)
foreach($saMapping as $ssTags){
  list($ssTagFrom,$ssTagTo) = explode(':',$ssTags);
  echo "INSERT INTO isms_profile_acl (fkprofile,stag) "
      ."SELECT fkprofile,'$ssTagTo' "
      ."FROM isms_profile_acl pa JOIN isms_profile p ON (p.fkcontext = pa.fkprofile AND p.nid = 0) "
      ."WHERE pa.stag = '$ssTagFrom';\n";
}

// Deleta permiss�es para ACLs que n�o existem mais
foreach($saTagsToDelete as $ssTag){
  echo "DELETE FROM isms_profile_acl WHERE stag = '{$ssTag}';\n";
}

// D� permiss�o pras novas tags pros perfis especiais de acordo com os prefixos das tags
foreach($saTagsToInsert as $ssTag){
  $ssPrefix = substr($ssTag, 0, strpos($ssTag, '.'));
  echo "INSERT INTO isms_profile_acl (fkprofile,stag) SELECT fkcontext, '{$ssTag}' FROM isms_profile WHERE nid = 8801;\n";
  if($ssPrefix=='M'){
    echo "INSERT INTO isms_profile_acl (fkprofile,stag) SELECT fkcontext, '{$ssTag}' FROM isms_profile WHERE nid = 8802;\n";
  }elseif($ssPrefix=='A'){
    echo "INSERT INTO isms_profile_acl (fkprofile,stag) SELECT fkcontext, '{$ssTag}' FROM isms_profile WHERE nid = 8803;\n";
  }
}

?>