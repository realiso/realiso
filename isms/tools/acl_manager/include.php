<?

  ini_set('error_reporting',E_ALL);
  
  $sys_ref = "";
  $sys_ref_based_on_tab_main = "";
  
  $classes_ref = "classes/";
  $handlers_ref = "handlers/";
  
  $states_ref = $classes_ref . 'states/';
  
  $lib_ref = $sys_ref."lib/";
  $base_ref = $sys_ref."../../../fwd/src/";
  include $base_ref . "base/setup.php";

  include $classes_ref . "ACLSystem.php";
  include $classes_ref . "ACLAcl.php";

  $soWeblib = FWDWebLib::getInstance();

  $moConfig = new FWDConfig($sys_ref.'config.php');
  if(!$moConfig->load()){
    trigger_error('error_config_corrupt',E_USER_ERROR);
  }

  $soWebLib = FWDWebLib::getInstance();
  $soWebLib->setCSSRef($sys_ref."lib/");
  $soWebLib->setSysRef($sys_ref);
  $soWebLib->setSysJsRef($sys_ref."lib/");
  //$soWebLib->setJSRef($sys_ref."lib/");
  //$soWebLib->setGfxRef($sys_ref."gfx/");
  //$soWebLib->setSysRefBasedOnTabMain($sys_ref_based_on_tab_main);

?>