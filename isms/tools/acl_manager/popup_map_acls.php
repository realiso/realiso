<?

include_once 'include.php';
include_once $handlers_ref . 'QueryACLTree.php';

class ScreenBeforeEvent extends FWDRunnable {
  public function run(){
    $moStartEvent = FWDStartEvent::getInstance();
    $moStartEvent->setScreenEvent(new ScreenEvent(''));
  }
}

class ScreenEvent extends FWDRunnable {
  public function run(){
    $miOldSystemId = FWDWebLib::getObject('old_system')->getValue();
    $miNewSystemId = FWDWebLib::getObject('new_system')->getValue();
    
    FWDWebLib::getObject('var_old_system')->setValue($miOldSystemId);
    FWDWebLib::getObject('var_new_system')->setValue($miNewSystemId);
    
    $moSelect = FWDWebLib::getObject('new_acls');
    
    $maOldTags = ACLAcl::getTagsFromSystem($miOldSystemId);
    
    $moAcl = new ACLAcl();
    $moAcl->createFilter($miNewSystemId,'acl_system_id');
    $moAcl->select();
    
    while($moAcl->fetch()){
      $msTag = $moAcl->getFieldValue('acl_tag');
      
      if(!in_array($msTag,$maOldTags)){
        $moItem = new FWDItem();
        $moItem->setAttrKey($msTag);
        $moItem->setValue($msTag.' - '.$moAcl->getFieldValue('acl_name'));
        $moSelect->addObjFWDItem($moItem);
      }
    }
    
    FWDWebLib::getInstance()->dump_html(FWDWebLib::getObject('dialog'));
    
    $moHandler = new QueryACLTree();
    $moHandler->setSystem($miOldSystemId);
    $moHandler->makeQuery();
    $moHandler->executeQuery();
    
    ?>
      <script type="text/javascript">
        
        function map(paOldTags,paNewTags){
          if(paOldTags.length==1 && paNewTags.length>0){
            var moMapSelect = gobi('mapping');
            var msKey,msText;
            var msOldTag = soACLTree.getItemProperty(paOldTags[0],'tag');
            for(var i=0;i<paNewTags.length;i++){
              msKey = msOldTag+':'+paNewTags[i];
              msText = 'Perfis com permiss�o para '+msOldTag+' ganham permiss�o para '+paNewTags[i];
              moMapSelect.addItem(msKey,msText);
            }
          }else{
            alert('Para fazer um mapeamento, � preciso selecionar exatamente uma ACL antiga e uma ou mais ACLs novas.');
          }
        }
        
        var soACLTree = new FWDItemTree('old_acl_tree');
        soACLTree.addProperty('id');
        soACLTree.addProperty('parent');
        soACLTree.addProperty('tag');
        soACLTree.addProperty('name');
        soACLTree.addProperty('description');
        <?
          while($moHandler->fetch()){
            $miId = $moHandler->getFieldValue('acl_id');
            $miParentId = $moHandler->getFieldValue('acl_parent_id');
            $miParentId = ($miParentId?$miParentId:0);
            $miLevel = $moHandler->getFieldValue('acl_level');
            $msTag = $moHandler->getFieldValue('acl_tag');
            $msName = str_replace("'","\'",$moHandler->getFieldValue('acl_name'));
            $msDescription = str_replace("'","\'",$moHandler->getFieldValue('acl_description'));
            echo "soACLTree.addItem({$miId},{$miParentId},'{$msTag}','{$msName}','{$msDescription}');";
          }
        ?>
      </script>
    <?
  }
}

FWDStartEvent::getInstance()->addBeforeAjax(new ScreenBeforeEvent(''));
FWDWebLib::getInstance()->xml_load('popup_map_acls.xml');

?>