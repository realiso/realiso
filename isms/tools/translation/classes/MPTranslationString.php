<?php
/**
 * MINI PROJECTS
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe MPTranslationString.
 *
 * <p>Classe que representa as strings de um sistema.</p>
 * @package MINI PROJECTS
 * @subpackage classes
 */
class MPTranslationString extends MPTable {
	
 /**
  * Construtor.
  * 
  * <p>Construtor da classe MPTranslationString.</p>
  * @access public 
  */
  public function __construct(){		
  	parent::__construct("translation_strings");
  	
  	$this->coDataset->addFWDDBField(new FWDDBField("skKey",					  "string_key_id", 		      DB_NUMBER));
  	$this->coDataset->addFWDDBField(new FWDDBField("skSystem",				"string_system_id", 	    DB_NUMBER));
  	$this->coDataset->addFWDDBField(new FWDDBField("zId", 					  "string_id", 			        DB_STRING));
  	$this->coDataset->addFWDDBField(new FWDDBField("zValue", 				  "string_value", 		      DB_STRING));
  	$this->coDataset->addFWDDBField(new FWDDBField("zFile", 				  "string_file", 			      DB_STRING));
  	$this->coDataset->addFWDDBField(new FWDDBField("dDateInclusion", 	"string_date_inclusion",  DB_DATETIME));
  	$this->coDataset->addFWDDBField(new FWDDBField("nIsTranslated",		"string_is_translated",	  DB_NUMBER));
  	$this->coDataset->addFWDDBField(new FWDDBField("zPortuguesIsms", 	"string_portugues_isms",	DB_STRING));
  	$this->coDataset->addFWDDBField(new FWDDBField("zEnglishIsms", 		"string_english_isms", 		DB_STRING));
  	$this->coDataset->addFWDDBField(new FWDDBField("zSpanishIsms",  	"string_espanol", 		    DB_STRING));
  	$this->coDataset->addFWDDBField(new FWDDBField("zChineseIsms", 		"string_chinese_isms",		DB_STRING));
  	$this->coDataset->addFWDDBField(new FWDDBField("zPortuguesEms", 	"string_portugues_ems",		DB_STRING));
  	$this->coDataset->addFWDDBField(new FWDDBField("zPortuguesOhs", 	"string_portugues_ohs",		DB_STRING));  	
    $this->coDataset->addFWDDBField(new FWDDBField("zEnglishEms", 	  "string_english_ems",		  DB_STRING));
  	$this->coDataset->addFWDDBField(new FWDDBField("zEnglishOhs", 	  "string_english_ohs",		  DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField("zEnglishSox", 	  "string_english_sox",		  DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField("zEnglishPci", 	  "string_english_pci",		  DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField('zGermanSox',		'string_german_sox', 	DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField('zFrenchSox',		'string_french_sox', 	DB_STRING));
    $this->coDataset->addFWDDBField(new FWDDBField('zPortugues3380',		'string_portugues_3380', 	DB_STRING));
  }
  
 /**
  * Busca informa��es de uma string atrav�s de seu identificador.
  * 
  * <p>M�todo para buscar informa��es de uma string atrav�s de seu identificador.</p>
  * @access public 
  * @param string $psId Identificador 
  */ 
  public function fetchById($psId) {  	
  	return parent::fetchById($psId, "string_key_id");
  }
}
?>