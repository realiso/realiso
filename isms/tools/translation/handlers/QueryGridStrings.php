<?php
/**
 * MINI PROJECTS
 *
 * <p>These coded instructions,  technics, statements, and computer programs
 * contain  unpublished  proprietary information of  Axur Communications,
 * Inc.,  and are  protected  by applied  copyright law.  They may not be
 * disclosed to third parties, copied or duplicated in any form, in whole
 * or in part, without  the prior written consent of Axur Communications,
 * Inc.</p>
 * <p>Estes  codigos,  tecnicas, tratados e  programas de computador contem
 * informacao proprietaria  nao publicada pela Axur Communications, Inc.,
 * e sao  protegidas pelas leis  de direito registrado.  Essas, nao podem
 * ser dispostas  a terceiros, copiadas ou  duplicadas de qualquer forma,
 * no  todo ou  em parte,  sem  consentimento  previo  escrito pela Axur
 * Communications, Inc.</p>
 * @copyright Copyright (c) 2006, Axur Information Security
 * @link http://www.axur.com.br Axur Information Security
 */

/**
 * Classe QueryGridStrings.
 *
 * <p>Classe que representa o grid de strings.</p>
 * @package MINI PROJECTS
 * @subpackage handlers
 */
class QueryGridStrings extends FWDDBQueryHandler {
  
  protected $ciSystemId = 0;
  
  protected $csIdFilter = "";
  
  protected $csValueFilter = "";
  
  protected $csFileFilter = "";
  
  protected $csEnglishFilter = "";
  
  protected $csPortugueseFilter = "";
  
  protected $cbOnlyNotTranslatedFilter = false;
  
  protected $cbPortugueseNotNull = false;
  
  protected $cbEnglishNull = false;
  
  
  
  public function __construct($poDB) {
    parent::__construct($poDB);  

    $this->coDataSet->addFWDDBField(new FWDDBField("skKey"         ,"string_key_id"        ,DB_NUMBER));
    $this->coDataSet->addFWDDBField(new FWDDBField("zId"           ,"string_id"            ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("zValue"        ,"string_value"         ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("zPortuguesIsms"    ,"string_portuguese_isms"    ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("zEnglishIsms"      ,"string_english_isms"       ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("zFile"         ,"string_file"          ,DB_STRING));
    $this->coDataSet->addFWDDBField(new FWDDBField("dDateInclusion","string_date_inclusion",DB_DATETIME));
    $this->coDataSet->addFWDDBField(new FWDDBField("nIsTranslated" ,"string_is_translated" ,DB_NUMBER));
  }
  
  public function setIdFilter($psIdFilter) {    
    $this->csIdFilter = $psIdFilter;
  }
  
  public function setValueFilter($psValueFilter) {    
    $this->csValueFilter = $psValueFilter;
  }
  
  public function setFileFilter($psFileFilter) {    
    $this->csFileFilter = $psFileFilter;
  }
  
  public function setOnlyNotTranslatedFilter($pbOnlyNotTranslatedFilter) {    
    $this->cbOnlyNotTranslatedFilter = $pbOnlyNotTranslatedFilter;
  }
  
  public function setSystem($piSystemId) {    
    $this->ciSystemId = $piSystemId;
  }

  public function setEnglishFilter($psEnglishFilter) {
    $this->csEnglishFilter = $psEnglishFilter;
  }

  public function setPortugueseFilter($psPortugueseFilter) {
    $this->csPortugueseFilter = $psPortugueseFilter;
  }

  public function setPortugueseNotNull($pbPortugueseNotNull) {    
    $this->cbPortugueseNotNull = $pbPortugueseNotNull;
  }

  public function setEnglishNull($pbEnglishNull) {    
    $this->cbEnglishNull = $pbEnglishNull;
  }



  public function makeQuery() {            
    $this->csSQL = "
SELECT skKey as string_key_id, 
       zId as string_id, 
       zValue as string_value, 
       zFile as string_file, 
       dDateInclusion as string_date_inclusion, 
       nIsTranslated as string_is_translated,
       zPortuguesIsms as string_portuguese_isms,
       zEnglishIsms as string_english_isms
  FROM translation_strings 
  WHERE skSystem = {$this->ciSystemId}
";
    
    $this->csSQL .= $this->csIdFilter ? " AND UPPER(zId) like UPPER('%{$this->csIdFilter}%')" : "";
    $this->csSQL .= $this->csValueFilter ? " AND UPPER(zValue) like UPPER('%{$this->csValueFilter}%')" : "";
    $this->csSQL .= $this->csEnglishFilter ? " AND UPPER(zEnglishIsms) like UPPER('%{$this->csEnglishFilter}%')" : "";
    $this->csSQL .= $this->csPortugueseFilter ? " AND UPPER(zPortuguesIsms) like UPPER('%{$this->csPortugueseFilter}%')" : "";
    $this->csSQL .= $this->csFileFilter ? " AND UPPER(zFile) like UPPER('%{$this->csFileFilter}%')" : "";
    $this->csSQL .= $this->cbOnlyNotTranslatedFilter ? " AND (nIsTranslated = 0 OR nIsTranslated IS NULL)" : "";
    $this->csSQL .= $this->cbPortugueseNotNull ? " AND (zPortuguesIsms NOT LIKE '' AND zPortuguesIsms IS NOT NULL)" : "";
    $this->csSQL .= $this->cbEnglishNull ? " AND (zEnglishIsms LIKE '' OR zEnglishIsms IS NULL)" : "";
  } 
}
?>